#ifndef VACICONVVS_LHC_H
#define	VACICONVVS_LHC_H

// Icon for VVS_LHC

#include "VacIcon.h"

class EqpVV;
class FlashingRect;

class VACCTLEWOUTIL_EXPORT VacIconVVS_LHC : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVVS_LHC(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVVS_LHC();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

	// Override method of VacIcon 
	virtual void setDirection(VacIconContainer::Direction newDirection);
	virtual void setBeamDirection(VacIconContainer::BeamDirection newDirection);

protected:
	// Pointer to my device
	EqpVV *pEqp;

	// Rectangles for interlock symbols. Drawing can be easy using QPainter
	// coordinate transformations, but finding areas for tooltips is not so
	// evident. May be it is more convenient just to keep in meory coordinates
	// of all interlock rectangles

	QRect	vvBefore;
	QRect	vvAfter;


	// Pointer to flashing rectangle - created to show current pressure interlock
	FlashingRect	*pFlash;

	void setInterlockGeometry(void);

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
	virtual void drawInterlocks(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
	
	void setPainterForControl(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
	void setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
	QColor colorForInterlock(Eqp::CtlStatus eqpStatus, unsigned rr1, unsigned rr2, bool plcAlarm, unsigned bit);
};

#endif	// VACICONVVS_LHC_H

