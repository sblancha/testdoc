#ifndef EQPUSAGEFILTERITEM_H
#define	EQPUSAGEFILTERITEM_H

#include "VacCtlOnlineNotificationExport.h"

// Additionl filtering of equipment based on it's usage - this is single filter item

#include <QByteArray>

class VACCTLONLINENOTIFICATION_EXPORT EqpUsageFilterItem
{
public:
	EqpUsageFilterItem(int funcTypeId, const QByteArray &eqpAttrName, bool critReverse);
	~EqpUsageFilterItem();

	// Access
	inline int getTypeId(void) const { return typeId; }
	inline const QByteArray &getAttrName(void) const { return attrName; }
	inline bool isReverse(void) const { return reverse; }

	// Under 'normal' operation set() are not needed, but they are required for parsing
	inline void setAttrName(const QByteArray &name) { attrName = name; }
	inline void setReverse(bool flag) { reverse = flag; }

protected:
	// Functional type ID
	int		typeId;

	// Name of equipment attribute
	QByteArray	attrName;

	// Flag indicating if 'reverse' criteria shall be used
	bool		reverse;

};

#endif	// EQPUSAGEFILTERITEM_H
