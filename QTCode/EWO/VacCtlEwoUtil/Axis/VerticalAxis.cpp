//	Implementation of VerticalAxis class
/////////////////////////////////////////////////////////////////////////////////

#include "VerticalAxis.h"

#include <math.h>
#include "PlatformDef.h"
#include "DebugCtl.h"

#define maxOf2(a,b) ((a) > (b) ? (a) : (b))

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VerticalAxis::VerticalAxis(bool logAxis, bool axisOnTheLeft)
{
	logScale = logAxis;
	onLeftSide = axisOnTheLeft;
	grid = true;
	gridStyle = Qt::SolidLine;
	tickSize = 4;
	tickOutside = false;
	drawEvery = 1;
	if(logScale)
	{
		min = -10.0;
		max = 0.0;
	}
	else
	{
		min = -1.0;
		max = 1.0;
		linLabelFormat[0] = '\0';
	}
	needPrepare = true;
	minFixed = maxFixed = linLimitsFixed = false;
	temporaryLinear = false;
}

VerticalAxis::~VerticalAxis()
{
}

/*
**	FUNCTION
**		Set geometry of graph drawing area.
**
**	PARAMETERS
**		left	- left edge of graph area
**		top		- top edge of graph area
**		width	- width of graph area
**		height	- height of graph area
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::setGraphArea(const QRect &area)
{
	if(area != graphArea)
	{
		needPrepare = true;
	}
	graphArea = area;
}

/*
**	FUNCTION
**		Switch scale type from log to linear and back
**
**	PARAMETERS
**		setLog	- true if axis shall be switched to logarithmic,
**					false if axis shall be switched to linear
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::switchScaleType(bool setLog)
{
	if(logScale == setLog)
	{
		return;
	}
	logScale = setLog;
	temporaryLinear = true;
	needPrepare = true;
}

/*
**	FUNCTION
**		Set new limits for axis, version for logarithmic scale
**
**	PARAMETERS
**		newMin	- New minimum value
**		newMax	- New maximum value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::setLogLimits(int newMin, int newMax)
{
	if(!logScale)
	{
		return;
	}
	if((int)rint(min) != newMin)
	{
		needPrepare = true;
	}
	min = newMin;
	if((int)rint(max) != newMax)
	{
		needPrepare = true;
	}
	max = newMax;
}

/*
** FUNCTION
**		Set new limits for axis, version for linear scale
**
** PARAMETERS
**		newMin	- New minimum value
**		newMax	- New maximum value
**
** RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void VerticalAxis::setLinLimits(float newMin, float newMax)
{
	if(logScale)
	{
		return;
	}
	if(min != newMin)
	{
		needPrepare = true;
	}
	min = newMin;
	if(max != newMax)
	{
		needPrepare = true;
	}
	max = newMax;
}

/*
**	FUNCTION
**		Set fixed Y-coordinate for axis minimum
**
**	PARAMETERS
**		location	- Y-coordinate where minimum shall be fixed, or
**						-1 if minimum location shall not be fixed
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::fixMinAt(int location)
{
	if(location < 0)
	{
		if(minFixed)
		{
			needPrepare = true;
		}
		minFixed = false;
	}
	else
	{
		if(!minFixed)
		{
			needPrepare = true;
		}
		minFixed = true;
		if(minLocation != location)
		{
			needPrepare = true;
		}
		minLocation = location;
	}
}

/*
**	FUNCTION
**		Set fixed Y-coordinate for axis maximum
**
**	PARAMETERS
**		location	- Y-coordinate where maximum shall be fixed, or
**						-1 if minimum location shall not be fixed
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::fixMaxAt(int location)
{
	if(location < 0)
	{
		if(maxFixed)
		{
			needPrepare = true;
		}
		maxFixed = false;
	}
	else
	{
		if(!maxFixed)
		{
			needPrepare = true;
		}
		maxFixed = true;
		if(maxLocation != location)
		{
			needPrepare = true;
		}
		maxLocation = location;
	}
}

/*
**	FUNCTION
**		Prepare all axis drawing parameters
**
**	PARAMETERS
**		fontMetrics	- Font metrics that will be used during drawing
**
**	RETURNS
**		true if axis parameters have been changed,
**		false otherwise
**
**	CAUTIONS
**		None
*/
bool VerticalAxis::prepare(const QFontMetrics &fontMetrics)
{
	bool	result = needPrepare;

	needPrepare = false;

	if(!result)
	{
		return result;
	}

	if(logScale)
	{
		prepareLogScale(fontMetrics);
	}
	else
	{
		prepareLinScale(fontMetrics);
	}
	return result;
}

/*
**	FUNCTION
**		Redraw axis
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Color and font for axis shall be set in painter BEFORE calling this method.
**		Drawing area shall be cleared before calling this method.
*/
void VerticalAxis::draw(QPainter &painter)
{
	if(needPrepare)
	{
		prepare(painter.fontMetrics());
	}

	// Draw axis labels, ticks and subticks
	if(logScale)
	{
		drawLogScale(painter);
	}
	else
	{
		drawLinScale(painter);
	}
}

/*
**	FUNCTION
**		Get minimum and maximum of axis for drawing.
**		Integer values here are logarithms of min/max respectively
**		The method is used for logarithmic axis only
**
**	PARAMETERS
**		useMin	- Value to be used as minimum will be returned here
**		useMax	- Value to be used as maximum will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::getLimits(int &useMin, int &useMax)
{
	if((useMin = (int)rint(min)) > (useMax = (int)rint(max)))
	{
		useMin = (int)rint(max);
		useMax = (int)rint(min);
	}
	else if(min == max)
	{
		useMin = (int)rint(min) - 1;
		useMax = (int)rint(max) + 1;
	}
}

/*
**	FUNCTION
**		Set minimum and maximum of axis for drawing.
**		Integer values here are logarithms of min/max respectively
**		The method is used for logarithmic axis only
**
**	PARAMETERS
**		newMin	- New scale minimum
**		newMax	- New scale maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::setLimits(int newMin, int newMax)
{
	if((int)rint(min) != newMin)
	{
		needPrepare = true;
	}
	min = newMin;
	if((int)rint(max) != newMax)
	{
		needPrepare = true;
	}
	max = newMax;
}

/*
**	FUNCTION
**		Get minimum and maximum of axis for drawing.
**		The method is used for linear axis only
**
**	PARAMETERS
**		useMin	- Value to be used as minimum will be returned here
**		useMax	- Value to be used as maximum will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::getLimits(float &useMin, float &useMax)
{
	if((useMin = min ) > (useMax = max))
	{
		useMin = max;
		useMax = min;
	}
	else if(min == max)
	{
		if(min == 0)
		{
			useMin = (float)(min - 1.0);
			useMax = (float)(max + 1.0);
		}
		else
		{
			useMin = (float)(min / 1.1);
			useMax = (float)(min * 1.1);
		}
	}
}

/*
**	FUNCTION
**		Get minimum and maximum of axis for drawing.
**		The method is used for linear axis only
**
**	PARAMETERS
**		newMin	- New scale minimum
**		newMax	- New scale maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::setLimits(float newMin, float newMax)
{
	if(min != newMin)
	{
		needPrepare = true;
	}
	min = newMin;
	if(max != newMax)
	{
		needPrepare = true;
	}
	max = newMax;
}

/*
**	FUNCTION
**		Convert value to screen Y-coordinate
**
**	PARAMETERS
**		value	- value to be converted
**		screenY	- resulting Y-coordinate will be returned here
**
**	RETURNS
**		enumeration - position of value with respect to axis limits
**
**	CAUTIONS
**		None
*/
VerticalAxis::ValueRange VerticalAxis::valueToScreen(float value, int &screenY)
{
	if(logScale)
	{
		return valueToScreenLog(value, screenY);
	}
	return valueToScreenLin(value, screenY);
}

/*
** FUNCTION
**		Convert screen Y-coordinate to value
**
** PARAMETERS
**		screenY	- Y-coordinate on the screen
**
** RETURNS
**		Value corresponding to screen coordinate
**
** CAUTIONS
**		None
*/
double VerticalAxis::screenToValue(int screenY)
{
	double	result;

	if(logScale)
	{
		int	useMin, useMax;
		getLimits(useMin, useMax);
		double	valueLog = useMin + (double)(scaleBottom - screenY) / (double)(scaleBottom - scaleTop) *
			(useMax - useMin);
		result = pow(10.0, valueLog);
	}
	else
	{
		result = linScaleMin + (double)(scaleBottom - screenY) / (double)(scaleBottom - scaleTop) *
			(linScaleMax - linScaleMin);
	}
	return result;
}


/*
**	FUNCTION
**		Prepare all axis drawing parameters for logarithmic scale
**
**	PARAMETERS
**		fontMetrics	- Font metrics that will be used during drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::prepareLogScale(const QFontMetrics &fontMetrics)
{
	int		useMin, useMax;
	getLimits(useMin, useMax);

	// Count how many labeled ticks can be put in graph area
	int axisHeight;
	if(minFixed)
	{
		axisHeight = minLocation - graphArea.top();
	}
	else
	{
		axisHeight = graphArea.height();
	}
	drawEvery = 0;
	int totalLabelsHeight,
		labelNbr,
		nLabels;
	do
	{
		drawEvery++;
		gridLineStart = axisLocation = onLeftSide ? 0 : graphArea.right();
		totalLabelsHeight = labelNbr = nLabels = 0;
		scaleTop = maxFixed ? maxLocation : graphArea.top();
		scaleBottom = minFixed ? minLocation : graphArea.bottom() - 1;
		for(int value = useMin ; value <= useMax ; value++)
		{
			if(drawEvery > 1)
			{
				if(++labelNbr != drawEvery)
				{
					continue;
				}
				labelNbr = 0;
			}
			char label[64];
			makeLogLabel(value, label, sizeof(label) / sizeof(label[0]));
			QRect rect = fontMetrics.boundingRect(label);
			// If necessary - move scale top/bottom by half of string height
			if((value == useMin) && (!minFixed))
			{
				scaleBottom -= rect.height() / 2 - 1;
			}
			if((value == useMax) && (!maxFixed))
			{
				scaleTop = graphArea.top() + rect.height() / 2;
			}
			// Count total vertical space to be consumed by all labels
			totalLabelsHeight += rect.height();
			// Calculated available horizontal location for vertical line
			if(onLeftSide)
			{
				if(rect.width() > axisLocation)
				{
					gridLineStart = axisLocation = rect.width();
				}
			}
			else
			{
				if((axisLocation + rect.width()) > graphArea.right())
				{
					gridLineStart = axisLocation = graphArea.right() - rect.width();
				}
			}
			nLabels++;
		}
		if(nLabels < 2)
		{
			break;
		}
	}
	while(totalLabelsHeight >= axisHeight);
	if(onLeftSide)
	{
		gridLineStart = axisLocation += graphArea.left() + 2;
		if(tickOutside)
		{
			gridLineStart = axisLocation += tickSize;
		}
	}
	else
	{
		axisLocation -= 2;
		gridLineStart = axisLocation;
		if(tickOutside)
		{
			axisLocation -= tickSize;
			gridLineStart = axisLocation;
		}
	}
}

/*
**	FUNCTION
**		Prepare all axis drawing parameters for linear scale
**
**	PARAMETERS
**		fontMetrics	- Font metrics that will be used during drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::prepareLinScale(const QFontMetrics &fontMetrics)
{
	float	useMin, useMax;
	getLimits(useMin, useMax);

	// Move the whole values range to range 10...100 - then it's easy
	// to decide on scale parameters
	float full = useMax - useMin;
	float valueShift = 0;
	while(full > 100.)
	{
		full /= 10.0;
		valueShift += 1.0;
	}
	while(full < 10.)
	{
		full *= 10.0;
		valueShift -= 1.0;
	}

	// Decide what is the step for labeled ticks
	if(full < 15.0)
	{
		linLabelsStep = 2.0;
	}
	else if(full < 39.0)
	{
		linLabelsStep = 5.0;
	}
	else if(full < 80.0)
	{
		linLabelsStep = 10.0;
	}
	else
	{
		linLabelsStep = 10.0;
	}

	// Calculate final parameters for scale
	int nMin = (int)floor(useMin / pow((float)10.0, valueShift));
	int nMax = (int)ceil(useMax / pow((float)10.0, valueShift));
	if((nMax * pow((float)10.0, valueShift)) < useMax)
	{
		nMax++;
	}

	linScaleMin = linLabelsStart = (float)nMin;
	linScaleMax = (float)nMax;

	while(fmod(linLabelsStart, linLabelsStep) >= 1.0)
	{
		linLabelsStart += 1.0;
	}

	// Convert calculated values back to original range
	if(linLimitsFixed)
	{
		linScaleMin = useMin;
		linScaleMax = useMax;
	}
	else
	{
		linScaleMin *= (float)pow((float)10.0, valueShift);
		linScaleMax *= (float)pow((float)10.0, valueShift);
	}
	linLabelsStart *= (float)pow((float)10.0, valueShift);
	linLabelsStep *= (float)pow((float)10.0, valueShift);

	// Calculate format for labels. Use %f for 'reasonable' numbers, %e for very large and very small numbers
	float absValue = (float)maxOf2(fabs(linScaleMin), fabs(linScaleMax));
	if((absValue < 10000) && (absValue > 0.0001))	// Use %f format
	{
		float nDig = (float)log10(linLabelsStep);
		int nBeforeDot, nAfterDot = 0, pow;
		nBeforeDot = (int)log10(fabs(linLabelsStart));
		if(linLabelsStart < 0)
		{
			nBeforeDot++;
		}
		pow = (int)log10(fabs(linScaleMax));
		if(pow > nBeforeDot)
		{
			nBeforeDot = pow;
		}
		if(nDig < 0.0)
		{
			nAfterDot = - (int)floor(nDig);
		}
#ifdef Q_OS_WIN
		sprintf_s(linLabelFormat, sizeof(linLabelFormat) / sizeof(linLabelFormat[0]), "%%%d.%df", (nBeforeDot + nAfterDot + 1), nAfterDot);
#else
		sprintf(linLabelFormat, "%%%d.%df", (nBeforeDot + nAfterDot + 1), nAfterDot);
#endif
/*
printf("linLabelsStart %f linLabelsStep %f linScaleMax %f: nDig %f nBeforeDot %d nAfterDot %d: format <%s>\n",
linLabelsStart, linLabelsStep, linScaleMax, nDig, nBeforeDot, nAfterDot, linLabelFormat);
fflush(stdout);
*/
	}
	else
	{
		int nAfterDot = (int)rint(log10(absValue / linLabelsStep));
		// It looks like in Linux result is X.XXE+00, but in Windows - X.XXE+000
		// So, under Windows let's give one more digit
#ifdef Q_OS_WIN
		sprintf_s(linLabelFormat, sizeof(linLabelFormat) / sizeof(linLabelFormat[0]), "%%%d.%dE", (1 + nAfterDot + 1 + 5), nAfterDot);
#else
		sprintf(linLabelFormat, "%%%d.%dE", (1 + nAfterDot + 1 + 4), nAfterDot);
#endif
	}

	// Calculate screen space to be consumed by labels
	float value = linScaleMin;
	drawEvery = 1;	// TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	gridLineStart = axisLocation = onLeftSide ? 0 : graphArea.right();
	scaleTop = maxFixed ? maxLocation : graphArea.top();
	scaleBottom = minFixed ? minLocation : graphArea.bottom() - 1;
	while(value <= linScaleMax)
	{
		char	label[128];
		makeLinLabel(value, label, sizeof(label) / sizeof(label[0]));
		QRect rect = fontMetrics.boundingRect(label);
		// If necessary - move scale top/bottom by half of string height
		if((value == linScaleMin) && (!minFixed))
		{
			scaleBottom -= rect.height() / 2 - 1;
		}
		if((value == linScaleMax) && (!maxFixed))
		{
			scaleTop = graphArea.top() + rect.height() / 2;
		}
		// Calculated available horizontal location for vertical line
		if(onLeftSide)
		{
			if(rect.width() > axisLocation)
			{
				gridLineStart = axisLocation = rect.width();
			}
		}
		else
		{
			if((axisLocation + rect.width()) > graphArea.right())
			{
				gridLineStart = axisLocation = graphArea.right() - rect.width();
			}
		}
		value += linLabelsStep;
	}
	if(onLeftSide)
	{
		gridLineStart = axisLocation += graphArea.left() + 2;
		if(tickOutside)
		{
			axisLocation += tickSize;
			gridLineStart = axisLocation;
		}
	}
	else
	{
		axisLocation -= 2;
		gridLineStart = axisLocation;
		if(tickOutside)
		{
			axisLocation -= tickSize;
			gridLineStart = axisLocation;
		}
	}
}


/*
**	FUNCTION
**		Draw logarithmic scale
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::drawLogScale(QPainter &painter)
{
	const QColor &color = painter.pen().color();
	int	useMin, useMax;
	getLimits(useMin, useMax);
	double scaleHeight = useMax - useMin;

	int tickNbr = 0;
	QFontMetrics fm = painter.fontMetrics();
	for(int value = useMin ; value <= useMax ; value++)
	{
		int tickY = (int)rint(scaleTop + (scaleBottom - scaleTop) *
			(1. - ((value - useMin) / scaleHeight)));
		// Draw tick outside of graph area
		if(tickOutside)
		{
			painter.setPen(QPen(color));
			if(onLeftSide)
			{
				painter.drawLine(gridLineStart, tickY, gridLineStart - tickSize, tickY);
			}
			else
			{
				painter.drawLine(gridLineStart, tickY, gridLineStart + tickSize, tickY);
			}
		}
		// Draw either grid of ticks inside graph area - no reason to draw both
		if(grid)
		{
			painter.setPen(QPen(color, 0, (Qt::PenStyle)gridStyle));
			painter.drawLine(gridLineStart, tickY, gridLineEnd, tickY);
		}
		else if(tickSize && (!tickOutside))
		{
			painter.setPen(QPen(color));
			if(onLeftSide)
			{
				painter.drawLine(gridLineStart, tickY, gridLineStart + tickSize, tickY);
			}
			else
			{
				painter.drawLine(gridLineStart, tickY, gridLineStart - tickSize, tickY);
			}
		}
		bool drawLabel = true;
		if(drawEvery > 1)
		{
			if(++tickNbr == drawEvery)
			{
				tickNbr = 0;
			}
			else
			{
				drawLabel = false;
			}
		}
		if(drawLabel)
		{
			char label[64];
			makeLogLabel(value, label, sizeof(label) / sizeof(label[0]));
			QRect rect = fm.boundingRect(label);
			if(onLeftSide)
			{
				painter.drawText(axisLocation - rect.width() - (tickOutside ? tickSize : 0) - 2,
					tickY + (rect.height() >> 1)- fm.descent(), label);
			}
			else
			{
				painter.drawText(axisLocation + (tickOutside ? tickSize : 0) + 2,
					tickY + (rect.height() >> 1)- fm.descent(), label);
			}
		}
		// Draw subticks - no labels, no grid lines
		if(value == useMax)
		{
			break;
		}
		painter.setPen(QPen(color));
		for(int subTic = 2 ; subTic < 10 ; subTic++)
		{
			double subTickValue = log10(subTic * pow(10., value));
			int subTickY = (int)rint(scaleTop + (scaleBottom - scaleTop) *
				(1. - ((subTickValue - useMin) / scaleHeight)));
			if(fabs((float)(subTickY - tickY)) < 3)
			{
				break;	// too close to each other
			}
			int	lineSize = (int)((double)tickSize / ((subTic & 0x1) ? 2 : 1.5));
			if(lineSize)
			{
				if(onLeftSide)
				{
					painter.drawLine(gridLineStart, subTickY,
						gridLineStart + (tickOutside ? -lineSize : lineSize), subTickY);
				}
				else
				{
					painter.drawLine(gridLineStart, subTickY,
						gridLineStart + (tickOutside ? lineSize : -lineSize), subTickY);
				}
			}
			tickY = subTickY;
		}
	}
}

/*
**	FUNCTION
**		Draw linear scale
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxis::drawLinScale(QPainter &painter)
{
	const QColor &color = painter.pen().color();

	int tickNbr = 0;
	QFontMetrics fm = painter.fontMetrics();
	for(float value = linLabelsStart ; value <= linScaleMax ; value += linLabelsStep)
	{
		int tickY;
		valueToScreen(value, tickY);
		// Draw tick outside of graph area
		if(tickOutside)
		{
			painter.setPen(QPen(color));
			if(onLeftSide)
			{
				painter.drawLine(gridLineStart, tickY, gridLineStart - tickSize, tickY);
			}
			else
			{
				painter.drawLine(gridLineStart, tickY, gridLineStart + tickSize, tickY);
			}
		}
		// Draw either grid of ticks inside graph area - no reason to draw both
		if(grid)
		{
			painter.setPen(QPen(color, 0, (Qt::PenStyle)gridStyle));
			painter.drawLine(gridLineStart, tickY, gridLineEnd, tickY);
		}
		else if(tickSize && (!tickOutside))
		{
			painter.setPen(QPen(color));
			if(onLeftSide)
			{
				painter.drawLine(gridLineStart, tickY, gridLineStart + tickSize, tickY);
			}
			else
			{
				painter.drawLine(gridLineStart, tickY, gridLineStart - tickSize, tickY);
			}
		}
		bool drawLabel = true;
		if(drawEvery > 1)
		{
			if(++tickNbr == drawEvery)
			{
				tickNbr = 0;
			}
			else
			{
				drawLabel = false;
			}
		}
		if(drawLabel)
		{
			char label[64];
			makeLinLabel(value, label, sizeof(label) / sizeof(label[0]));
			QRect rect = fm.boundingRect(label);
			if(onLeftSide)
			{
				painter.drawText(axisLocation - rect.width() - (tickOutside ? tickSize : 0) - 2,
					tickY + (rect.height() >> 1) - fm.descent(), label);
			}
			else
			{
				painter.drawText(axisLocation + (tickOutside ? tickSize : 0) + 2,
					tickY + (rect.height() >> 1) - fm.descent(), label);
			}
		}
	}
}

/*
**	FUNCTION
**		Convert value to screen Y-coordinate for logarithmic scale
**
**	PARAMETERS
**		value	- value to be converted
**		screenY	- resulting Y-coordinate will be returned here
**
**	RETURNS
**		enumeration - position of value with respect to axis limits
**
**	CAUTIONS
**		None
*/
VerticalAxis::ValueRange VerticalAxis::valueToScreenLog(float value, int &screenY)
{
	if(value <= 0)
	{
		screenY = scaleBottom + 1;
		return BelowAxis;
	}
	int	useMin, useMax;
	getLimits(useMin, useMax);
	double valueLog = log10(value);
	if(valueLog < useMin)
	{
		screenY = scaleBottom + 1;
		return BelowAxis;
	}
	else if(valueLog > useMax)
	{
		screenY = scaleTop - 1;
		return AboveAxis;
	}
	screenY = scaleBottom - (int)rint((double)(scaleBottom - scaleTop) * (valueLog - useMin) /
		(useMax - useMin));
	return WithinRange;
}

/*
**	FUNCTION
**		Convert value to screen Y-coordinate for linear scale
**
**	PARAMETERS
**		value	- value to be converted
**		screenY	- resulting Y-coordinate will be returned here
**
**	RETURNS
**		enumeration - position of value with respect to axis limits
**
**	CAUTIONS
**		None
*/
VerticalAxis::ValueRange VerticalAxis::valueToScreenLin(float value, int &screenY)
{
	if(value < linScaleMin)
	{
		screenY = scaleBottom + 1;
		return BelowAxis;
	}
	else if(value > linScaleMax)
	{
		screenY = scaleTop - 1;
		return AboveAxis;
	}
	screenY = scaleBottom - (int)rint((double)(scaleBottom - scaleTop) * (value - linScaleMin) /
		(linScaleMax - linScaleMin));
	return WithinRange;
}

/*
**	FUNCTION
**		Prepare string representation of given logarithmic axis value.
**
**	PARAMETERS
**		value	- Value to be shown on axis
**		buf		- Pointer to character buffer where this method will put result
**
**	RETURNS
**		None
**
**	CAUTIONS
**		It is responsability of caller to allocate large enough memory block for buf.
*/
void VerticalAxis::makeLogLabel(int value, char *buf,
#ifdef Q_OS_WIN
	size_t bufLen)
#else
	size_t /* bufLen */)
#endif
{
	if((value < 0) || (value > 2))
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, bufLen, "1E%d", value);
#else
		sprintf(buf, "1E%d", value);
#endif
	}
	else
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, bufLen, "%.0f", (float)pow(10., (double)value));
#else
		sprintf(buf, "%.0f", (float)pow(10., (double)value));
#endif
	}
}

/*
**	FUNCTION
**		Prepare string representation of given linear axis value.
**
**	PARAMETERS
**		value	- Value to be shown on axis
**		buf		- Pointer to character buffer where this method will put result
**
** RETURNS
**		None
**
** CAUTIONS
**		It is responsability of caller to allocate large enough memory block for buf.
*/
void VerticalAxis::makeLinLabel(float value,  char *buf,
#ifdef Q_OS_WIN
	size_t bufLen)
#else
	size_t /* bufLen */)
#endif
{
#ifdef Q_OS_WIN
	sprintf_s(buf, bufLen, linLabelFormat, value);
#else
	sprintf(buf, linLabelFormat, value);
#endif
}

/*
**	FUNCTION
**		Dump axis parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None.
*/
void VerticalAxis::dump(FILE *pFile)
{
	if(!DebugCtl::isAxis())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	fprintf(pFile, "############## VERTICAL AXIS\n");
	fprintf(pFile, "  left %d top %d width %d height %d\n",
		graphArea.left(), graphArea.top(), graphArea.width(), graphArea.height());
	fprintf(pFile, "  min %f max %f\n", min, max);
	fprintf(pFile, "  axis %d scale: top %d bottom %d\n",
		axisLocation, scaleTop, scaleBottom);
	fprintf(pFile, "  gridLines: start %d end %d\n",
		gridLineStart, gridLineEnd);
	fprintf(pFile, "  minFixed %d minLocation %d drawEvery %d\n",
		minFixed, minLocation, drawEvery);
	fprintf(pFile, "  maxFixed %d maxLocation %d drawEvery %d\n",
		maxFixed, maxLocation, drawEvery);
	fprintf(pFile, "\n" );
}
