//	Implementation of MainLHCImage class

#include "MainLHCImage.h"

#include "LegendLabel.h"
#include "ExtendedLabel.h"

#include "VacMainView.h"

#include "VacType.h"
#include "Eqp.h"
#include "EqpType.h"
#include "DataPool.h"

#include <QPainter>
#include <QRect>
#include <QMessageBox>
#include <QFontMetrics>
#include <QPixmap>
#include <QMouseEvent>
#include <QApplication>

#include <math.h>

#include "PlatformDef.h"
#include "DebugCtl.h"

#define	BLUE_BEAM_LEGEND	"B1"
#define	RED_BEAM_LEGEND		"B2"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MainLHCImage::MainLHCImage(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	resetWasCalled = false;
	ipMarkerRad = minSegmentRad = 0;

	// Set reasonable defaults
	setMinimumWidth(200);
	setMinimumHeight(200);
	setMouseTracking(true);
	setAutoFillBackground(true);

	qrlColor.setRgb(128, 255, 255);
	cryoColor.setRgb(192, 192, 255);
	redBeamColor.setRgb(255, 0, 0);
	blueBeamColor.setRgb(0, 0, 255);
	crossBeamColor.setRgb(255, 255, 0);
	lssScale = 10;
	selectColor.setRgb(254, 159, 73);
	selectLineWidth = 3;
	defaultBeamColor.setRgb(71, 60, 208);
	beamTypeMethod = InnerOuterLines;
	beamTypeWidth = 3;
	connLineWidth = 3;
	qrlWidth = 5;
	cryoWidth = 20;

	pTimer = new QTimer(this);
	pTimer->setSingleShot(true);
	connect(pTimer, SIGNAL(timeout()), this, SLOT(timerDone()));
	pLabel = new ExtendedLabel(this);
	pLabel->setLabelType(ExtendedLabel::Transparent);
	pLabel->setBackColor(QColor(255, 255, 204, 192));
	pLabel->hide();

	redrawDelay = 500;
	pRedrawTimer = new QTimer(this);
	pTimer->setSingleShot(true);
	connect(pRedrawTimer, SIGNAL(timeout(void)), this, SLOT(redrawTimerDone(void)));

	// Reset members
	selectSegment = new LhcSegment(this);
	isDrawn = false;
	areSegmentsFromSectors = false;
	viewWidth = viewHeight = 0;

	// Connect to signal on equipment state changes
//	connect(&connection, SIGNAL(stateChanged(void)), this, SLOT(eqpStateChanged(void)));

	// Finally...
	makeImage();
}

MainLHCImage::~MainLHCImage()
{
	reset();
	while(!labels.isEmpty())
	{
		delete labels.takeFirst();
	}
	delete selectSegment;
}

void MainLHCImage::resizeEvent(QResizeEvent *event)
{
	makeImage();
	QWidget::resizeEvent(event);
}

void MainLHCImage::mouseMoveEvent(QMouseEvent *event)
{
	pTimer->stop();
	const char *pSectorName = sectorAtLocation(event->x(), event->y());
	if(pSectorName)
	{
		pLabel->setText(pSectorName);
		const QSize bound = pLabel->size();
		int x = event->x(), y = event->y();
		// 'direction' of label is always towards ring center
		if(x > ringCenterX)
		{
			x -= bound.width() - 2;
		}
		if(y > ringCenterY)
		{
			y -= bound.height() - 2;
		}
		/*
		if((x + 4 + bound.width()) < width())
		{
			x += 4;
		}
		else
		{
			x -= bound.width() + 2;
		}
		if((y + 4 + bound.height()) < height())
		{
			y += 4;
		}
		else
		{
			y -= bound.height() + 2;
		}
		*/
		pLabel->move(x, y);
		if(!pLabel->isVisible())
		{
			pLabel->show();
		}
		pTimer->start(1500);
	}
	else if(pLabel->isVisible())
	{
		pLabel->hide();
	}
}

void MainLHCImage::mousePressEvent(QMouseEvent *event)
{
	const char *pSectorName = sectorAtLocation(event->x(), event->y());
	if(pSectorName)
	{
		emit mousePress(event->button(), event->x(), event->y(), pSectorName);
	}
}

void MainLHCImage::mouseDoubleClickEvent(QMouseEvent *event)
{
	const char *pSectorName = sectorAtLocation(event->x(), event->y());
	if(pSectorName)
	{
		VacMainView *pMainView = VacMainView::getInstance();
		if(pMainView)
		{
			pMainView->emitSectorDoubleClick(pSectorName, DataEnum::Online);
		}
	}
}

void MainLHCImage::timerDone(void)
{
	if(pLabel->isVisible())
	{
		pLabel->hide();
	}
}

void MainLHCImage::redrawTimerDone(void)
{
	renewImage();
}

/*
**
**	FUNCTION
**		Slot to be invoked by MainLHCImageEqpConnect instance - state of one
**		of devices has been changed
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::eqpStateChanged(void)
{
	pRedrawTimer->stop();
	bool	immediateRedraw = false;
	int elapsed = drawTime.msecsTo(QTime::currentTime());
	if(elapsed < 0)	// Wrap over 24 hours?
	{
		immediateRedraw = true;
	}
	else if(elapsed > redrawDelay)
	{
		immediateRedraw = true;
	}
	if(immediateRedraw)
	{
		renewImage();
	}
	else
	{
		pRedrawTimer->start(redrawDelay);
	}
}

/*
**
**	FUNCTION
**		Remove all data collected
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::reset(void)
{
	resetWasCalled = true;
	while(!segments.isEmpty())
	{
		delete segments.takeFirst();
	}
	while(!arcSegments.isEmpty())
	{
		delete arcSegments.takeFirst();
	}
	while(!ipGeoms.isEmpty())
	{
		delete ipGeoms.takeFirst();
	}
	while(!connectedLines.isEmpty())
	{
		delete connectedLines.takeFirst();
	}
	areSegmentsFromSectors = false;
	viewWidth = viewHeight = 0;
	selSectors.clear();
	if(selectSegment)
	{
		delete selectSegment;
	}
	selectSegment = new LhcSegment(this);
	renewImage();
}

/*
**
**	FUNCTION
**		Prepare new image and display it - called when somthing has been
**		changed.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::renewImage(void)
{
	pRedrawTimer->stop();
	makeImage();
	update();
	drawTime.start();	// set to current time
}

/*
**
**	FUNCTION
**		Build new LHC image, set image as background for this widget
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::makeImage(void)
{
	QPixmap bgPixmap(size());
	QPalette pal = QApplication::palette();
	bgPixmap.fill(pal.color(QPalette::Window));
	QPainter painter(&bgPixmap);
	painter.setFont(font());
	draw(painter);
	pal.setBrush(QPalette::Window, bgPixmap);
	setPalette(pal);
	update();
}

/*
**
**	FUNCTION
**		Redraw the whole main view. First draw all vacuums segments, then
**		draw selection area.
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		0	- Nothing to draw, or drawing area has zero size
**		1	- Drawing was done successfully
**		-1	- Error (for example, no drawing parameters)
**
**	CAUTIONS
**		None
**
*/
short MainLHCImage::draw(QPainter &painter)
{
	if((!width()) || (!height()))
	{
		return 0;
	}
	if(!calculateGeometry())
	{
		drawNotReadyText(painter);
		return 0;	// Nothing to draw
	}

	if(!setNewGeometry(painter))
	{
		return 0;
	}
// printf("%s: draw() setNewGeometry\n", QTime::currentTime().toString("hh:mm:ss.zzz").ascii());

	FILE *pFile = NULL;
	if(DebugCtl::isMainView())
	{
#ifdef Q_OS_WIN
		const char *fileName = "C:\\LhcMainView.txt";
		if(!fopen_s(&pFile, fileName, "w"))
#else
		const char *fileName = "/home/kopylov/LhcMainView.txt";
		if((pFile = fopen(fileName, "w")))
#endif
		{
			dump(pFile);
		}
	}

	// Draw legend labels
	drawLegendLabels(painter);

	// Draw selection on LHC ring
	selectSegment->drawSelection(painter, selectColor, selectLineWidth);

	// Draw IP and ARC markers
	drawIpMarkers(painter);
	drawArcMarkers(painter);

	//	Draw legend (what R and B mean)
	drawLegend(painter);

	// Draw QRL and CRYO segments
	LhcSegment *pSegment;
	int idx;
	for(idx = 0 ; idx < segments.count() ; idx++)
	{
		pSegment = segments.at(idx);
		switch(pSegment->getVacType())
		{
		case VacType::Qrl:
		case VacType::Cryo:
			pSegment->draw(painter, false);
			break;
		}
	}

	// Draw selection on connected lines
	if(!selectSegment->getAngles().count())
	{
		drawSelection(painter);
	}

	// Draw beam vacuum segments.
	// Find maximum draw order for all segments
	int maxDrawOrder = 0, maxEqpOrder = 0, order;
	QColor interlockColor, interlockBorderColor;
	for(idx = 0 ; idx < segments.count() ; idx++)
	{
		pSegment = segments.at(idx);
		switch(pSegment->getVacType())
		{
		case VacType::RedBeam:
		case VacType::BlueBeam:
		case VacType::CrossBeam:
		case VacType::CommonBeam:
			if(pSegment->getEqp())
			{
				order = pSegment->getEqp()->getDrawOrder();
				if(order > maxDrawOrder)
				{
					maxDrawOrder = order;
				}
				if(!pSegment->isInterlockHidden())	// L.Kopylov 28.06.2012
				{
					if(pSegment->getEqp()->isSpecColor())
					{
						if(pSegment->getEqp()->getInterlockColor(DataEnum::Online, interlockColor, interlockBorderColor, order, pSegment->getNeighborEqpList()))
						{
							if(order > maxEqpOrder)
							{
								maxEqpOrder = order;
							}
						}
					}
				}
			}
			if(pSegment->getEqp2())
			{
				order = pSegment->getEqp2()->getDrawOrder();
				if(order > maxDrawOrder)
				{
					maxDrawOrder = order;
				}
				if(!pSegment->isInterlockHidden())	// L.Kopylov 28.06.2012
				{
					if(pSegment->getEqp2()->isSpecColor())
					{
						if(pSegment->getEqp2()->getInterlockColor(DataEnum::Online, interlockColor, interlockBorderColor, order, pSegment->getNeighborEqpList()))
						{
							if(order > maxEqpOrder)
							{
								maxEqpOrder = order;
							}
						}
					}
				}
			}
			break;
		}
	}
	if(pFile)
	{
		fprintf(pFile, "\n\n=============================== maxDrawOrder = %d ======================\n", maxDrawOrder);
	}

	// Draw segments according to draw order
	for(int drawOrder = 0 ; drawOrder <= maxDrawOrder ; drawOrder++)
	{
		if(pFile)
		{
			fprintf(pFile, "\n\n=============================== Order = %d ======================\n", drawOrder);
		}
		for(idx = 0 ; idx < segments.count() ; idx++)
		{
			pSegment = segments.at(idx);
			int	order = 0;
			QColor	color = defaultBeamColor;
			switch(pSegment->getVacType())
			{
			case VacType::RedBeam:
			case VacType::BlueBeam:
			case VacType::CrossBeam:
			case VacType::CommonBeam:
				if(pSegment->getEqp())
				{
					if(pSegment->getEqp()->isSpecColor())
					{
						order = pSegment->getEqp()->getDrawOrder();
						pSegment->getEqp()->getMainColor(color, DataEnum::Online);
					}
				}
				if(pSegment->getEqp2())
				{
					if(pSegment->getEqp2()->getDrawOrder() > order)
					{
						if(pSegment->getEqp2()->isSpecColor())
						{
							order = pSegment->getEqp2()->getDrawOrder();
							pSegment->getEqp2()->getMainColor(color, DataEnum::Online);
						}
					}
				}
				if(order == drawOrder)
				{
					pSegment->drawBeamSegment(painter, color, beamTypeMethod, beamTypeWidth, pFile);
				}
				break;
			}
		}
	}

	// Draw special colors of equipments according to draw order
	if(maxEqpOrder > 0)
	{
		for(int drawOrder = 1 ; drawOrder <= maxEqpOrder ; drawOrder++)
		{
			for(idx = 0 ; idx < segments.count() ; idx++)
			{
				pSegment = segments.at(idx);
				int	order = 0;
				QColor	color, borderColor;
				switch(pSegment->getVacType())
				{
				case VacType::RedBeam:
				case VacType::BlueBeam:
				case VacType::CrossBeam:
				case VacType::CommonBeam:
					if(pSegment->getEqp())
					{
						if(pSegment->isInterlockHidden())	// L.Kopylov 28.06.2012
						{
							continue;
						}
						if(pSegment->getEqp2())	// Segment after join
						{
							continue;
						}
						if(pSegment->getEqp()->isSpecColor())
						{
							pSegment->getEqp()->getInterlockColor(DataEnum::Online, color, borderColor, order, pSegment->getNeighborEqpList());
						}
					}
					if(order == drawOrder)
					{
						pSegment->drawBeamEqp(painter, color, borderColor, beamTypeWidth);
					}
					break;
				}
			}
		}
	}

	// Draw vacuum types for connected lines
	drawConnectedLinesTypes(painter);

	// Draw connected lines
	drawConnectedLines(painter);

	isDrawn = true;
	if(pFile)
	{
		fclose(pFile);
	}
	return 1;
}


/*
**
**	FUNCTION
**		Draw text indicating data are not initialized yet
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::drawNotReadyText(QPainter &painter)
{
	const char *message = "Not initialized yet";
	QRect bound = painter.boundingRect(0, 0, width(), height(), Qt::AlignCenter, message);
	painter.setPen(Qt::black);
	painter.drawText(width() / 2 - bound.width() / 2, height() / 2 - bound.height() / 2,
		message);
}

/*
**	FUNCTION
**		Draw legend labels
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHCImage::drawLegendLabels(QPainter &painter)
{
	for(int idx = labels.count() - 1 ; idx >= 0 ; idx--)
	{
		LegendLabel *pLabel = labels.at(idx);
		painter.drawText(pLabel->getX(), pLabel->getY(), pLabel->getText());
	}
}


/*
**
**	FUNCTION
**		Draw lines emanating from ring center to IP locations.
**		Draw labels with sector numbers (names are hardcoded!).
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::drawIpMarkers(QPainter &painter)
{
	// Calculate base radius for labels
	const QFontMetrics metrics = painter.fontMetrics();
	QRect bound = metrics.boundingRect("78");
	float labelRadius = (float)(1.5 * bound.width() / tan(M_PI / 8.0));

	painter.setPen(Qt::black);
	for(int n = 0 ; n < ipGeoms.count() ; n++)
	{
		// Draw line
		short	x = ringCenterX + (short)rint(ipMarkerRad * ipGeoms.at(n)->getCosValue()),
				y = ringCenterY - (short)rint(ipMarkerRad * ipGeoms.at(n)->getSinValue());
		painter.drawLine(ringCenterX, ringCenterY, x, y);
		// Draw label
		if(!labelRadius)
		{
			continue;
		}
		if(labelRadius > (ipMarkerRad / 2))
		{
			continue;
		}
		char	buf[16];
		if(n == (ipGeoms.count() - 1))
		{
#ifdef Q_OS_WIN
			sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d1", n + 1);
#else
			sprintf(buf, "%d1", n + 1);
#endif
			x = ringCenterX + (short)(labelRadius *
				(ipGeoms.at(n)->getCosValue() + ipGeoms.at(0)->getCosValue()) / 2.0);
			y = ringCenterY - (short)(labelRadius *
				(ipGeoms.at(n)->getSinValue() + ipGeoms.at(0)->getSinValue()) / 2.0);
		}
		else
		{
#ifdef Q_OS_WIN
			sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d%d", n + 1, n + 2);
#else
			sprintf(buf, "%d%d", n + 1, n + 2);
#endif
			x = ringCenterX + (short)(labelRadius *
				(ipGeoms.at(n)->getCosValue() + ipGeoms.at(n+1)->getCosValue()) / 2.0);
			y = ringCenterY - (short)(labelRadius *
				(ipGeoms.at(n)->getSinValue() + ipGeoms.at(n+1)->getSinValue()) / 2.0);
		}
		bound = metrics.boundingRect(buf);
		painter.drawText(x - bound.width() / 2, y + bound.height() / 2, buf);
	}
}

/*
**
**	FUNCTION
**		Draw lines highlighting ARC location.
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::drawArcMarkers(QPainter &painter)
{
	double	outerRadius = ipMarkerRad + 15,
			innerRadius = minSegmentRad - 15;

	painter.setPen(Qt::black);
	for(int idx = 0 ; idx < arcSegments.count() ; idx++)
	{
		LhcSegment *pArc = arcSegments.at(idx);
		QList<SineCosine *> &angles = pArc->getAngles();
		if(!angles.count())
		{
			continue;
		}
		SineCosine *pAngle = angles.first();
		// ARC start
		int	x1 = ringCenterX + (int)rint(innerRadius * pAngle->getCosValue()),
			y1 = ringCenterY - (int)rint(innerRadius * pAngle->getSinValue()),
			x2 = ringCenterX + (int)rint(outerRadius * pAngle->getCosValue()),
			y2 = ringCenterY - (int)rint(outerRadius * pAngle->getSinValue());
		painter.drawLine(x1, y1, x2, y2);
		pAngle = angles.last();
		x1 = ringCenterX + (int)rint(innerRadius * pAngle->getCosValue());
		y1 = ringCenterY - (int)rint(innerRadius * pAngle->getSinValue());
		x2 = ringCenterX + (int)rint(outerRadius * pAngle->getCosValue());
		y2 = ringCenterY - (int)rint(outerRadius * pAngle->getSinValue());
		painter.drawLine(x1, y1, x2, y2);
	}
}

/*
**
**	FUNCTION
**		Draw legend (meaning of R and B) in the lower left corner
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::drawLegend(QPainter &painter)
{
	const QFontMetrics metrics = painter.fontMetrics();
	QRect bound = metrics.boundingRect(RED_BEAM_LEGEND);
	painter.setPen(redBeamColor);
	painter.drawText(8, viewHeight - 2, RED_BEAM_LEGEND);
	int offset = bound.height() + 4;

	bound = metrics.boundingRect(BLUE_BEAM_LEGEND);
	painter.setPen(blueBeamColor);
	painter.drawText(8, viewHeight - offset, BLUE_BEAM_LEGEND);
}

/*
**
**	FUNCTION
**		Draw selected sectors on injection/dump lines
**
**	PARAMETERS
**		hDc			- DC handle
**		lineWidth	- Line width to be used for drawing
**		color		- Color to be used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::drawSelection(QPainter &painter)
{
	if(!selSectors.count())
	{
		return;
	}
	int	lineWidth = connLineWidth + (beamTypeWidth + selectLineWidth) * 2;
	painter.setPen(QPen(selectColor, lineWidth));
	// Draw
	for(int idx = 0 ; idx < selSectors.count() ; idx++)
	{
		Sector *pSector = selSectors.at(idx);
		const QList<SectorDrawPart *> &parts = pSector->getDrawParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			SectorDrawPart *pPart = parts.at(partIdx);
			drawSelPart(painter, pPart);
		}
	}
}

/*
**
**	FUNCTION
**		Draw one part of selected sector using previously set drawing parameters.
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		pDrawPart	- Pointer to part of sector to be drawn
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::drawSelPart(QPainter &painter, SectorDrawPart *pDrawPart)
{
	BeamLine	*pLine = pDrawPart->getLine();
	LhcConnectLine *pConnLine = NULL;
	for(int idx = 0 ; idx < connectedLines.count() ; idx++)
	{
		LhcConnectLine *pConn = connectedLines.at(idx);
		if(pConn->getLine() == pLine)
		{
			pConnLine = pConn;
			break;
		}
	}
	if(!pConnLine)
	{
		return;
	}
	if((!pConnLine->getCoeffX()) && (!pConnLine->getCoeffY()))
	{
		return;
	}
	BeamLinePart *pPart = pDrawPart->getStartPart();
	const QList<BeamLinePart *> &parts = pLine->getParts();
	int eqpIdx = pDrawPart->getStartEqpIdx();
	int			prevX, prevY;
	bool		isFirst = true;
	while(true)
	{
		Eqp	**eqpPtrs = pPart->getEqpPtrs();
		drawEqp(painter, pConnLine, eqpPtrs[eqpIdx], isFirst, prevX, prevY);
		if((pPart == pDrawPart->getEndPart()) && (eqpIdx == pDrawPart->getEndEqpIdx()))
		{
			break;
		}
		eqpIdx++;
		if(eqpIdx >= pPart->getNEqpPtrs())
		{
			int partIdx = parts.indexOf(pPart) + 1;
			if(partIdx >= parts.count())
			{
				if(pLine->isCircle())
				{
					partIdx = 0;
				}
				else
				{
					break;
				}
			}
			pPart = parts.at(partIdx);
			eqpIdx = 0;
		}
	}
}

/*
**
**	FUNCTION
**		Draw start coordinates of one device in connected line
**
**	PARAMETERS
**		painter		- painter used for drawing
**		pConnLine	- Pointer to connected line
**		pEqp		- Pointer to device
**		isFirst		- Flag indicating if this is the very first point of line
**		prevX		- Previously drawn X coordinate, value will be updated
**						by this function.
**		prevY		- Previously drawn Y coordinate, value will be updated
**						by this function.
**
** RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void MainLHCImage::drawEqp(QPainter &painter, LhcConnectLine *pConnLine, Eqp *pEqp,
	bool &isFirst, int &prevX, int &prevY)
{
	// Skip equipment with negative length - it comes from survey DB,
	// but I don't know what negative length means
	if(pEqp->getLength() < 0.0)
	{
		return;
	}
	int	x = pConnLine->getStartX() + (int)rint(
			(pEqp->getStart() - pConnLine->getStartPos()) * pConnLine->getCoeffX()),
		y = pConnLine->getStartY() + (int)rint((
			pEqp->getStart() - pConnLine->getStartPos()) * pConnLine->getCoeffY());

	if(isFirst)
	{
		isFirst = false;
	}
	else
	{
		if((x != prevX) || (y != prevY))
		{
			painter.drawLine(prevX, prevY, x, y);
		}
	}
	prevX = x;
	prevY = y;
}

/*
**
**	FUNCTION
**		Draw lines with color corresponding to vacuum types
**		for all injection/dump lines. Note that these markers
**		shall be drawn before LHC ring sectors.
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::drawConnectedLinesTypes(QPainter &painter)
{
	calculateConnLineGeometry();
	for(int idx = 0 ; idx < connectedLines.count() ; idx++)
	{
		LhcConnectLine *pLine = connectedLines.at(idx);
		if((!pLine->getCoeffX()) && (!pLine->getCoeffY()))
		{
			continue;
		}
		drawConnLineType(pLine, painter);
	}
}

/*
**
**	FUNCTION
**		Calculate drawing geometry for all injection/dump lines.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::calculateConnLineGeometry(void)
{
	// Calculate coefficients for converting eqp position in line to screen
	// coordinates
	for(int idx = 0 ; idx < connectedLines.count() ; idx++)
	{
		LhcConnectLine *pLine = connectedLines.at(idx);
		if(pLine->getLine()->isHidden())
		{
			pLine->setCoeffX(0);
			pLine->setCoeffY(0);
			continue;
		}
		if((pLine->getStartX() == pLine->getEndX()) && (pLine->getStartY() == pLine->getEndY()))
		{
			pLine->setCoeffX(0);
			pLine->setCoeffY(0);
			continue;
		}
		// Find position of the very first and the very last devices in line
		float	lastPos = 0;
		bool	gotFirst = false;
		const QList<BeamLinePart *> &parts = pLine->getLine()->getParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			if(!pPart->getNEqpPtrs())
			{
				continue;
			}
			if(!gotFirst)
			{
				pLine->setStartPos(pPart->getEqpPtrs()[0]->getStart());
				gotFirst = true;
			}
			lastPos = pPart->getEqpPtrs()[pPart->getNEqpPtrs() - 1]->getStart();
		}
		if((!gotFirst) || (lastPos == pLine->getStartPos()))
		{
			pLine->setCoeffX(0);
			pLine->setCoeffY(0);
			continue;
		}
		if(pLine->getStartX() != pLine->getEndX())
		{
			pLine->setCoeffX(((float)(pLine->getEndX() - pLine->getStartX())) /
				(lastPos - pLine->getStartPos()));
		}
		else
		{
			pLine->setCoeffX(0);
		}
		if(pLine->getStartY() != pLine->getEndY())
		{
			pLine->setCoeffY(((float)(pLine->getEndY() - pLine->getStartY())) /
				(lastPos - pLine->getStartPos()));
		}
		else
		{
			pLine->setCoeffY(0);
		}
	}
}

/*
**
**	FUNCTION
**		Draw lines with color corresponding to vacuum types
**		for one injection/dump line.
**
**	PARAMETERS
**		pLine	- Pointer to connected line
**		painter	- painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::drawConnLineType(LhcConnectLine *pLine, QPainter &painter)
{
	switch(pLine->getVacType())
	{
	case VacType::RedBeam:
		painter.setPen(QPen(redBeamColor, beamTypeWidth));
		break;
	case VacType::BlueBeam:
		painter.setPen(QPen(blueBeamColor, beamTypeWidth));
		break;
	case VacType::CrossBeam:
	case VacType::CommonBeam:
		painter.setPen(QPen(crossBeamColor, beamTypeWidth));
		break;
	default:
		return;
	}
	if(pLine->getStartX() == pLine->getEndX())	// Vertical line
	{
		painter.drawLine(pLine->getStartX() - beamTypeWidth, pLine->getStartY(),
			pLine->getEndX() - beamTypeWidth, pLine->getEndY());
		painter.drawLine(pLine->getStartX() + beamTypeWidth, pLine->getStartY(),
			pLine->getEndX() + beamTypeWidth, pLine->getEndY());
	}
	else if(pLine->getStartY() == pLine->getEndY())	// Horizontal line
	{
		painter.drawLine(pLine->getStartX(), pLine->getStartY() - beamTypeWidth,
			pLine->getEndX(), pLine->getEndY() - beamTypeWidth);
		painter.drawLine(pLine->getStartX(), pLine->getStartY() + beamTypeWidth,
			pLine->getEndX(), pLine->getEndY() + beamTypeWidth);
	}
}

/*
**
**	FUNCTION
**		Draw connected (injection/dump) lines on view
**
**	PARAMETERS
**		painter	- painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::drawConnectedLines(QPainter &painter)
{
	for(int idx = 0 ; idx < connectedLines.count() ; idx++)
	{
		LhcConnectLine *pLine = connectedLines.at(idx);
		if((!pLine->getCoeffX()) && (!pLine->getCoeffY()))
		{
			continue;
		}
		int maxDrawOrder = pLine->getLine()->maxDrawOrder();
		for(int drawOrder = 0 ; drawOrder <= maxDrawOrder ; drawOrder++)
		{
			drawConnectLine(painter, pLine, drawOrder);
		}
	}
}

/*
**
**	FUNCTION
**		Draw all parts of connected (ibjection/dump) beam line with
**		given draw order using color of special devices for drawing
**		different parts of line. All points in beam line are just
**		processed from first to last device
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		pConnLine	- Pointer to connected line to draw
**		drawOrder	- Parts of line with this draw order shall be drawn
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::drawConnectLine(QPainter &painter, LhcConnectLine *pConnLine, int drawOrder)
{
	int		prevX, prevY;
	bool	isFirst = true, isDrawing = false;	// drawOrder == 0 ? true : false;
	QColor	&color = defaultBeamColor;

	if((pConnLine->getStartX() == pConnLine->getEndX()) &&
		(pConnLine->getStartY() == pConnLine->getEndY()))
	{
		return;
	}

	const QList<BeamLinePart *> &parts = pConnLine->getLine()->getParts();
	int partIdx;

	// Valve interlocks
	for(partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);
		int	nEqpPtrs = pPart->getNEqpPtrs();
		Eqp	**eqpPtrs = pPart->getEqpPtrs();
		for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
		{
			Eqp *pEqp = eqpPtrs[eqpIdx];
			QColor interlockColor, interlockBorderColor;
			int order;
			if(pEqp->isSpecColor())
			{
				if(pEqp->getInterlockColor(DataEnum::Online, interlockColor, interlockBorderColor, order, NULL))	// TODO: list of neighbor Eqp
				{
					int	x = pConnLine->getStartX() + (int)rint(
							(pEqp->getStart() - pConnLine->getStartPos()) * pConnLine->getCoeffX()),
						y = pConnLine->getStartY() + (int)rint((
							pEqp->getStart() - pConnLine->getStartPos()) * pConnLine->getCoeffY());
					painter.setBrush(interlockColor);
					painter.setPen(interlockColor);
					int imageRad = 5;
					painter.drawPie(x - imageRad, y - imageRad, imageRad << 1, imageRad << 1,
						0, 5760);
					QPen pen(interlockBorderColor, 3);
					painter.setPen(pen);
					painter.drawArc(x - imageRad, y - imageRad, imageRad << 1, imageRad << 1,
						0, 5760);
				}
			}
		}
	}

	// Line image
	painter.setPen(QPen(color, connLineWidth));

	for(partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);
		int	nEqpPtrs = pPart->getNEqpPtrs();
		Eqp	**eqpPtrs = pPart->getEqpPtrs();
		for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
		{
			Eqp *pEqp = eqpPtrs[eqpIdx];
			if(isDrawing)
			{
				drawEqp(painter, pConnLine, pEqp, isFirst, prevX, prevY);
			}
			if(!pEqp->isSpecColor())
			{
				continue;
			}
			if(pEqp->getDrawOrder() != drawOrder)
			{
				isDrawing = false;
				continue;
			}
			else if(!isDrawing)
			{
				isFirst = true;
				drawEqp(painter, pConnLine, pEqp, isFirst, prevX, prevY);
				isDrawing = true;
			}
			QColor eqpColor;
			pEqp->getMainColor(eqpColor, DataEnum::Online);
			if(eqpColor != color)
			{
				color = eqpColor;
				painter.setPen(QPen(color, connLineWidth));
			}
		}
	}
}

/*
**
**	FUNCTION
**		Calculate all radii for segments, calculate coordinates of center
**
**	PARAMETERS
**		painter	- Painter that will be used for drawing
**
**	RETURNS
**		true	- radii calculation successful,
**		false	- calculation failed (too few space for drawing)
**
**	CAUTIONS
**		None
**
*/
bool MainLHCImage::setNewGeometry(QPainter &painter)
{
	double	radius;
	short	qrlOuterRad, qrlInnerRad, cryoOuterRad, cryoInnerRad, delta;
	short oldIpMarkerRad = ipMarkerRad, oldMinSegmentRad = minSegmentRad;

	// Find center and all radii
	viewWidth = width();
	viewHeight = height();
	if(viewWidth < viewHeight)
	{
		radius = (viewWidth - selectLineWidth) / 2.0;
	}
	else
	{
		radius = (viewHeight - selectLineWidth) / 2.0;
	}
	selectSegment->setOuterRad((short)rint(radius));
	ringCenterX = viewWidth / 2;
	ringCenterY = viewHeight / 2;

	qrlOuterRad = (short)(radius - selectLineWidth);
	qrlInnerRad = qrlOuterRad - (short)rint(radius * qrlWidth / 100.0);
	if((qrlOuterRad - qrlInnerRad) < 3)
	{
		qrlInnerRad = qrlOuterRad - 3;
	}
	if(qrlOuterRad <= 0)
	{
		return false;	// Not enough space
	}
	cryoOuterRad = qrlInnerRad - 3;
	cryoInnerRad = cryoOuterRad - (short)rint(radius * cryoWidth / 100.0);
	if((cryoOuterRad - cryoInnerRad) < 11)
	{
		cryoInnerRad = cryoOuterRad - 11;
	}
	if(cryoInnerRad <= 0)
	{
		return false;	// Not enough space
	}
	if(cryoInnerRad < 10)
	{
		cryoInnerRad = 10;
		delta = 1;
	}
	else
	{
		delta = -1;
	}
	if((cryoOuterRad - cryoInnerRad) % 5)
	{
		cryoInnerRad = cryoInnerRad + delta * ((cryoOuterRad - cryoInnerRad) % 5);
	}
	if(cryoInnerRad <= 0)
	{
		return false;	// Not enough space
	}
	delta = (cryoOuterRad - cryoInnerRad) / 5;
	selectSegment->setInnerRad(cryoInnerRad - selectLineWidth);
	if(selectSegment->getInnerRad() <= 0)
	{
		return false;	// Not enough space
	}
	ipMarkerRad = qrlOuterRad;
	minSegmentRad = cryoInnerRad;

	// Calculate parameters of all segments
	int maxSectHeight = 0, maxSectWidth = 0;

	for(int idx = 0 ; idx < segments.count() ; idx++)
	{
		LhcSegment *pSegment = segments.at(idx);
		switch(pSegment->getVacType())
		{
		case VacType::RedBeam:
			if(pSegment->isRedOut())
			{
				pSegment->setOuterRad(cryoOuterRad - delta);
			}
			else
			{
				pSegment->setOuterRad(cryoOuterRad - delta * 3);
			}
			pSegment->setInnerRad(pSegment->getOuterRad() - delta);
			pSegment->setTypeColor(redBeamColor);
			break;
		case VacType::BlueBeam:
			if(pSegment->isRedOut())
			{
				pSegment->setOuterRad(cryoOuterRad - delta * 3);
			}
			else
			{
				pSegment->setOuterRad(cryoOuterRad - delta);
			}
			pSegment->setInnerRad(pSegment->getOuterRad() - delta);
			pSegment->setTypeColor(blueBeamColor);
			break;
		case VacType::CrossBeam:
			pSegment->setOuterRad(cryoOuterRad - delta * 2);
			pSegment->setInnerRad(pSegment->getOuterRad() - delta);
			pSegment->setTypeColor(crossBeamColor);
			break;
		case VacType::CommonBeam:	// TODO !!!!!!!!!!!!!
			pSegment->setOuterRad(cryoOuterRad - delta * 2);
			pSegment->setInnerRad(pSegment->getOuterRad() - delta);
			pSegment->setTypeColor(crossBeamColor);
			break;
		case VacType::Cryo:
			pSegment->setOuterRad(cryoOuterRad);
			pSegment->setInnerRad(cryoInnerRad);
			pSegment->setTypeColor(cryoColor);
			break;
		case VacType::Qrl:
			pSegment->setOuterRad(qrlOuterRad);
			pSegment->setInnerRad(qrlInnerRad);
			pSegment->setTypeColor(qrlColor);
			break;
		}
		if(pSegment->getOtherLineEqp())
		{
			calculateOtherLineGeometry(pSegment);
		}

		// Maximum size of text label
		if(pSegment->getSector())
		{
			QRect bound = painter.fontMetrics().boundingRect(pSegment->getSector()->getName());
			if(bound.width() > maxSectWidth)
			{
				maxSectWidth = bound.width();
			}
			if(bound.height() > maxSectHeight)
			{
				maxSectHeight = bound.height();
			}
		}
	}

	if((oldIpMarkerRad != ipMarkerRad) || (oldMinSegmentRad != minSegmentRad))
	{
		searchForNeighborEqp();
		//qDebug("ipMarkerRad %d -> %d, minSegmentRad %d -> %d\n", oldIpMarkerRad, ipMarkerRad, oldMinSegmentRad, minSegmentRad);
	}

	QSize labelSize(maxSectWidth, maxSectHeight + 6);
	pLabel->setFixedSize(labelSize);
	return true;
}

/*
**
**	FUNCTION
**		Calculate geometry for injection/dump line connected to LHC ring
**		within given segment
**
**	PARAMETERS
**		pSegment	- Pointer to segment where other line is connected
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::calculateOtherLineGeometry(LhcSegment *pSegment)
{
	DataPool &pool = DataPool::getInstance();

	// Find required beam line
	BeamLine	*pBeamLine = pool.getLine(pSegment->getOtherLineEqp()->getName());
	if(!pBeamLine)
	{
		return;
	}

	// Check if this line is already in the list of connected lines
	LhcConnectLine *pLine = NULL;
	for(int idx = 0 ; idx < connectedLines.count() ; idx++)
	{
		LhcConnectLine *pConn = connectedLines.at(idx);
		if(pConn->getLine() == pBeamLine)
		{
			pLine = pConn;
			break;
		}
	}
	if(!pLine)
	{
		pLine = new LhcConnectLine(pBeamLine, pSegment->getVacType());
		connectedLines.append(pLine);
		pLine->connectEqp(this);
	}

	// Calculate geometry of connected line
	QList<SineCosine *> &angles = pSegment->getAngles();
	if(!angles.count())
	{
		return;	// Angles for segment were not calculated yet
	}
	// At least two angles in array nAngles - no reason to get out of array
	int startIdx = (angles.count() - 1) >> 1;
	double	cosValue = 0.5 * (angles.at(startIdx)->getCosValue() + angles.at(startIdx+1)->getCosValue()),
			sinValue = 0.5 * (angles.at(startIdx)->getSinValue() + angles.at(startIdx+1)->getSinValue());
	// Not all possible cases are considered here - just because not all possible
	// cases can be realized in LHC. In fact, LHC only has two injection lines and
	// two dump lines
	switch(pSegment->getOtherLineEqp()->getType())
	{
	case EqpType::OtherLineStart:	// Dump lines
		if(!strcmp(pLine->getLine()->getName(), "TD62"))
		{
			pLine->setStartX((short)rint(ringCenterX + pSegment->getInnerRad() * cosValue));
			pLine->setStartY((short)rint(ringCenterY - pSegment->getInnerRad() * sinValue));
			pLine->setEndX(pLine->getStartX() - (short)((float)pSegment->getInnerRad() * 0.4));
			pLine->setEndY(pLine->getStartY());
		}
		else
		{
			pLine->setStartX((short)rint(ringCenterX + pSegment->getOuterRad() * cosValue));
			pLine->setStartY((short)rint(ringCenterY - pSegment->getOuterRad() * sinValue));
			pLine->setEndX(viewWidth); // pLine->startX + (short)((float)pSegment->innerRad * 0.4);
			pLine->setEndY(pLine->getStartY());
		}
		break;
	case EqpType::OtherLineEnd:	// Injection lines
		pLine->setStartX((short)rint(ringCenterX + pSegment->getOuterRad() * cosValue));
		pLine->setEndX(pLine->getStartX());
		pLine->setStartY(viewHeight - 1);
		pLine->setEndY((short)rint(ringCenterY - pSegment->getOuterRad() * sinValue));
		break;
	}
}

void MainLHCImage::searchForNeighborEqp(void)
{
	//qDebug("searchForNeighborEqp() - START\n");
	int x, y, radius;
	QPolygon eqpPolygon(8);
	for(int idx = 0 ; idx < segments.count() ; idx++)
	{
		LhcSegment *pSegment = segments.at(idx);
		switch(pSegment->getVacType())
		{
		case VacType::RedBeam:
		case VacType::BlueBeam:
		case VacType::CrossBeam:
		case VacType::CommonBeam:
			break;
		default:
			continue;
		}

		pSegment->resetNeighborEqp();
		if(!pSegment->getBeamEqpGeometry(x, y, radius))
		{
			continue;
		}
		// Very raw interpolation of circle
		int radius07 = rint(0.7 * radius);
		eqpPolygon.setPoint(0, x + radius, y);
		eqpPolygon.setPoint(1, x + radius07, y + radius07);
		eqpPolygon.setPoint(2, x, y + radius);
		eqpPolygon.setPoint(3, x - radius07, y + radius07);
		eqpPolygon.setPoint(4, x - radius, y);
		eqpPolygon.setPoint(5, x - radius07, y - radius07);
		eqpPolygon.setPoint(6, x, y - radius);
		eqpPolygon.setPoint(7, x + radius07, y - radius07);
		// Find overlapping segments
		for(int otherIdx = 0 ; otherIdx < segments.count() ; otherIdx++)
		{
			if(otherIdx == idx)
			{
				continue;
			}
			LhcSegment *pOtherSegment = segments.at(otherIdx);
			pOtherSegment->checkForNeighborEqp(pSegment, eqpPolygon);
		}
	}
	//qDebug("searchForNeighborEqp() - FINISH\n");
}

/*
**
**	FUNCTION
**		Calculate geometry for drawing LHC.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		true	- success
**		false	- failure (nothing to draw)
**
**	CAUTIONS
**		None
**
*/
bool MainLHCImage::calculateGeometry(void)
{
	if(!resetWasCalled)
	{
		return false;	// Do not try to draw something useful before first reset() call
	}
	DataPool &pool = DataPool::getInstance();
	if(!pool.getNLhcIps())
	{
		return false;	// Geometry is based on IPs
	}
	if(!ipGeoms.count())
	{
		buildIpAngles();
	}

	// Build geometry of circle line
	if(buildSegments())	// Segments have been changed
	{
		LhcSegment *pSegment;
		int idx;
		for(idx = 0 ; idx < segments.count() ; idx++)
		{
			pSegment = segments.at(idx);
			pSegment->setStartAngleSet(false);
			pSegment->setEndAngleSet(false);
		}
		for(int ipIdx = 0 ; ipIdx < ipGeoms.count() ; ipIdx++)
		{
			calculateOctantAngles(ipIdx );
		}
		// Critical error detection
		for(idx = 0 ; idx < segments.count() ; idx++)
		{
			pSegment = segments.at(idx);
			char	buf[512];
			if(!pSegment->isStartAngleSet())
			{
#ifdef Q_OS_WIN
				sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "No start angle for %d type %d from %f to %f",
					idx, pSegment->getVacType(), pSegment->getStart(),
					pSegment->getEnd());
#else
				sprintf(buf, "No start angle for %d type %d from %f to %f",
					idx, pSegment->getVacType(), pSegment->getStart(),
					pSegment->getEnd());
#endif
				QMessageBox::warning(this, tr("WARNING"), tr(buf), QMessageBox::Ok, QMessageBox::NoButton);
			}
			if(!pSegment->isEndAngleSet())
			{
#ifdef Q_OS_WIN
				sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "No end angle for %d type %d from %f to %f",
					idx, pSegment->getVacType(), pSegment->getStart(),
					pSegment->getEnd());
#else
				sprintf(buf, "No end angle for %d type %d from %f to %f",
					idx, pSegment->getVacType(), pSegment->getStart(),
					pSegment->getEnd());
#endif
				QMessageBox::warning(this, "WARNING", buf, QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
			}
			if(pSegment->getStartAngle() > 10.0)
			{
#ifdef Q_OS_WIN
				sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "Start angle %f for %d (%d) type %d from %f to %f",
					pSegment->getStartAngle(), idx, segments.count(),
					pSegment->getVacType(), pSegment->getStart(), pSegment->getEnd());
#else
				sprintf(buf, "Start angle %f for %d (%d) type %d from %f to %f",
					pSegment->getStartAngle(), idx, segments.count(),
					pSegment->getVacType(), pSegment->getStart(), pSegment->getEnd());
#endif
				QMessageBox::warning(this, "WARNING", buf, QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
			}
			if(pSegment->getEndAngle() > 10.0)
			{
#ifdef Q_OS_WIN
				sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "End angle %f for %d (%d) type %d from %f to %f",
					pSegment->getEndAngle(), idx, segments.count(),
					pSegment->getVacType(), pSegment->getStart(), pSegment->getEnd());
#else
				sprintf(buf, "End angle %f for %d (%d) type %d from %f to %f",
					pSegment->getEndAngle(), idx, segments.count(),
					pSegment->getVacType(), pSegment->getStart(), pSegment->getEnd());
#endif
				QMessageBox::warning(this, "WARNING", buf, QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
			}
		}
		buildSegmentPoints();
/*
for(idx = 0 ; idx < segments.count() ; idx++)
{
	pSegment = segments.at(idx);
switch(pSegment->getVacType())
{
case VacType::RedBeam:
case VacType::BlueBeam:
case VacType::CrossBeam:
case VacType::CommonBeam:
	printf("Segment %d: vacType %d start %f end %f rel %f %f angles %f %f\n",
		idx, pSegment->getVacType(), pSegment->getStart(), pSegment->getEnd(),
		pSegment->getRelStart(), pSegment->getRelEnd(),
		pSegment->getStartAngle(), pSegment->getEndAngle());
	Sector *pSector = pSegment->getSector();
	printf("   %s len %f points %d\n", (pSector ? pSector->getName() : "null"), pSegment->getEndAngle() - pSegment->getStartAngle(),
		pSegment->getAngles().count());
	fflush(stdout);
	break;
}
}
*/
	}
	return true;
}


/*
**
**	FUNCTION
**		Calculate angles for all IPs
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::buildIpAngles(void)
{
// printf("%s: buildIpAngles() start\n", QTime::currentTime().toString("hh:mm:ss.zzz").ascii());
	DataPool &pool = DataPool::getInstance();
	int 	nLhcIps = pool.getNLhcIps();
	float	*lhcIps = pool.getLhcIps();
// printf("%s: buildIpAngles() got data\n", QTime::currentTime().toString("hh:mm:ss.zzz").ascii());
	ipGeoms.clear();
// printf("%s: buildIpAngles() clear\n", QTime::currentTime().toString("hh:mm:ss.zzz").ascii());
	for(int n = 0 ; n < nLhcIps ; n++)
	{
		ipGeoms.append(new SineCosine(2.0 * M_PI * lhcIps[n] / pool.getLhcRingLength()));
	}
// printf("%s: buildIpAngles() finish\n", QTime::currentTime().toString("hh:mm:ss.zzz").ascii());
}

/*
**	FUNCTION
**		Build all segments to be drawn for LHC ring image. Every
**		segment is a vacuum sector to be able to draw sectors
**		with individual color. However, there are some excpetions:
**		1) For QRL and CRYO - only vacuum sectors are drawn, i.e.
**			if no vacuum sectors - nothing will be drawn.
**		2) Beam vacuum can be drawn even without sectors - just
**			vacuum pipe regions.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		true	- if list of segments has been rebuilt
**		false	- if list of segments remain unchanged
**
**	CAUTIONS
**		None
**
*/
bool MainLHCImage::buildSegments(void)
{
	DataPool &pool = DataPool::getInstance();

	if(!segments.isEmpty())	// Segments were already built
	{
		// The following if() has been uncommented by LIK 20.02.2007
		// Unfortunately, I do not remember why it was commented out.
		// Probably, it was done for better support of VBasic test program where
		// list of sectors can be changed during program life time, but I'm not sure about that...
		if(areSegmentsFromSectors) 
		{
			return false;
		}
		if(!pool.getSectors().count())
		{
			return false;
		}
		// Need to rebuild segments
		while(!segments.isEmpty())
		{
			delete segments.takeFirst();
		}
		while(!arcSegments.isEmpty())
		{
			delete arcSegments.takeFirst();
		}
	}

	bool			hasBeamSectors = false;

	const QList<Sector *> &sectors = pool.getSectors();
	areSegmentsFromSectors = ! sectors.isEmpty();

	// segments for QRL and CRYO - pure sectors
	int idx;
	for(idx = 0 ; idx < sectors.count() ; idx++)
	{
		Sector *pSector = sectors.at(idx);
		switch(pSector->getVacType())
		{
		case VacType::Qrl:
		case VacType::Cryo:
			addSegment(pSector->getVacType(), pSector->getStart(), pSector->getEnd(), pSector);
			break;
		case VacType::RedBeam:
		case VacType::BlueBeam:
		case VacType::CrossBeam:
		case VacType::CommonBeam:
			hasBeamSectors = true;
			break;
		}
	}

	// Beam segments are built differently depending on presence of beam vacuum sectors
	if(hasBeamSectors)
	{
		buildSectorBeamSegments();
	}
	else
	{
		buildEmptyBeamSegments();
	}

	// Connect all segments and connection lines to equipment
	for(idx = 0 ; idx < segments.count() ; idx++)
	{
		LhcSegment *pSegment = segments.at(idx);
		pSegment->connectEqp();
	}
	return true;
}

/*
**
**	FUNCTION
**		Build all segments for beam vacuum - there is
**		at least one sector on beam vacuum. Segments are
**		built by combining beam location information with
**		sector borders from equipment.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::buildSectorBeamSegments(void)
{
	// Find circular line (hopefully, LHC is the only circular line)
	DataPool &pool = DataPool::getInstance();
	BeamLine *pLine = pool.getCircularLine();
	if(!pLine)	// No circle line in data pool - can not happen for LHC???
	{
		buildEmptyBeamSegments( );
		return;
	}

	// Everything is here - start building segments
	LhcSegment *pSegment;
	const QList<Sector *> &sectors = pool.getSectors();
	for(int idx = 0 ; idx < sectors.count() ; idx++)
	{
		Sector *pSector = sectors.at(idx);
		if(pSector->isOnRedBeam() && pSector->isOnBlueBeam() && pSector->isOnCrossBeam())	// Y-like sectors
		{
			if(pSector->getCrossStart() == pSector->getRedEnd())	// Joining beams
			{
				pSegment = addSegment(VacType::RedBeam, pSector->getRedStart(), pSector->getRedEnd(), pSector);
				pSegment->setEqp(pSector->getRedStartEqp());
				pSegment->setRedOut(pSector->isRedOut());

				pSegment = addSegment(VacType::BlueBeam, pSector->getBlueStart(), pSector->getBlueEnd(), pSector);
				pSegment->setEqp(pSector->getBlueStartEqp());
				pSegment->setRedOut(pSector->isRedOut());

				pSegment = addSegment(VacType::CrossBeam, pSector->getCrossStart(), pSector->getCrossEnd(), pSector);
				pSegment->setEqp(pSector->getRedStartEqp());
				pSegment->setEqp2(pSector->getBlueStartEqp());
				pSegment->setRedOut(pSector->isRedOut());
			}
			else	// Separating beams
			{
				pSegment = addSegment(VacType::RedBeam, pSector->getRedStart(), pSector->getRedEnd(), pSector);
				pSegment->setEqp(pSector->getCrossStartEqp());
				pSegment->setInterlockHidden(true);	// L.Kopylov 28.06.2012
				pSegment->setRedOut(pSector->isRedOut());

				pSegment = addSegment(VacType::BlueBeam, pSector->getBlueStart(), pSector->getBlueEnd(), pSector);
				pSegment->setEqp(pSector->getCrossStartEqp());
				pSegment->setInterlockHidden(true);	// L.Kopylov 28.06.2012
				pSegment->setRedOut(pSector->isRedOut());

				pSegment = addSegment(VacType::CrossBeam, pSector->getCrossStart(), pSector->getCrossEnd(), pSector);
				pSegment->setEqp(pSector->getCrossStartEqp());
				pSegment->setRedOut(pSector->isRedOut());
			}
		}
		else if(pSector->isOnRedBeam() && pSector->isOnBlueBeam())	// Common vacuum
		{
			pSegment = addSegment(VacType::RedBeam, pSector->getRedStart(), pSector->getRedEnd(), pSector);
			pSegment->setEqp(pSector->getRedStartEqp());
			pSegment->setRedOut(pSector->isRedOut());

			pSegment = addSegment(VacType::BlueBeam, pSector->getBlueStart(), pSector->getBlueEnd(), pSector);
			pSegment->setEqp(pSector->getBlueStartEqp());
			pSegment->setRedOut(pSector->isRedOut());
		}
		else if(pSector->isOnRedBeam())	// Pure RED beam
		{
			pSegment = addSegment(VacType::RedBeam, pSector->getRedStart(), pSector->getRedEnd(), pSector);
			pSegment->setEqp(pSector->getRedStartEqp());
			pSegment->setRedOut(pSector->isRedOut());
		}
		else if(pSector->isOnBlueBeam())	// Pure BLUE beam
		{
			pSegment = addSegment(VacType::BlueBeam, pSector->getBlueStart(), pSector->getBlueEnd(), pSector);
			pSegment->setEqp(pSector->getBlueStartEqp());
			pSegment->setRedOut(pSector->isRedOut());
		}
		else if(pSector->isOnCrossBeam())	// Pure CROSS beam
		{
			pSegment = addSegment(VacType::CrossBeam, pSector->getCrossStart(), pSector->getCrossEnd(), pSector);
			pSegment->setEqp(pSector->getCrossStartEqp());
			pSegment->setRedOut(pSector->isRedOut());
		}
	}
	// Connect to equipment. Multiple calls are safe because MainLHCImageEqpConnect
	// keeps connection state internally
	// LIK 19.05.2009 connection.connect();
}

/*
**
**	FUNCTION
**		Build all segments for beam vacuum - there are no
**		sectors on beam vacuum. Segments are built only
**		from beam location information.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::buildEmptyBeamSegments(void)
{
	DataPool &pool = DataPool::getInstance();
	const QList<LhcRegion *> &beamLocs = pool.getLhcBeamLocs();
	LhcRegion *pRegion;
	bool			isRedOut;
	LhcSegment *pSegment;
	for(int idx = 0 ; idx < beamLocs.count() ; idx++)
	{
		pRegion = beamLocs.at(idx);
		isRedOut = true;
		switch(pRegion->getType())
		{
		case LhcRegion::BlueOut:
			isRedOut = false;
			/* NOBREAK */
		case LhcRegion::RedOut:
			pSegment = addSegment(VacType::RedBeam, pRegion->getStart(), pRegion->getEnd(), NULL);
			pSegment->setRedOut(isRedOut);
			pSegment = addSegment(VacType::BlueBeam, pRegion->getStart(), pRegion->getEnd(), NULL);
			pSegment->setRedOut(isRedOut);
			break;
		case LhcRegion::Cross:
			addSegment(VacType::CrossBeam, pRegion->getStart(), pRegion->getEnd(), NULL);
			break;
		}
	}
}

/*
**
**	FUNCTION
**		Build for every segment list of points with pre-calculated values
**		of sine and cosine - they will be used for drawing.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::buildSegmentPoints(void)
{
	LhcSegment *pSegment;
	int	n;
	for(n = 0 ; n < segments.count() ; n++)
	{
		pSegment = segments.at(n);
		pSegment->buildPoints(n);
	}
	for(n = 0 ; n < arcSegments.count() ; n++)
	{
		pSegment = arcSegments.at(n);
		pSegment->buildArcPoints(n);
	}
}

/*
**
**	FUNCTION
**		Calculate angles within one octant
**
**	PARAMETERS
**		ipIdx	- index of IP before octant being processed
**
**	RETURNS
**		true	- success
**		false	- failure (nothing to draw)
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::calculateOctantAngles(short ipIdx )
{
	DataPool	&pool = DataPool::getInstance();
	float		*lhcIps = pool.getLhcIps();
	int			nLhcIps = pool.getNLhcIps();
	double		lower = lhcIps[ipIdx],
				upper = ipIdx < (nLhcIps - 1) ? lhcIps[ipIdx + 1] : pool.getLhcRingLength();
	double		range = 2.0 * M_PI * fabs(upper - lower) / pool.getLhcRingLength(),
				arcStart = 0.0,
				arcEnd = 0.0;

	// Find ARC after this IP - it will be a border for applying LSS scale
	const QList<LhcRegion *>	&lhcArcs = pool.getLhcArcs();
	LhcRegion	*pRegion;
	int idx;
	for(idx = 0 ; idx < lhcArcs.count() ; idx++)
	{
		pRegion = lhcArcs.at(idx);
		if((lower <= pRegion->getStart()) && (pRegion->getEnd() <= upper))
		{
			arcStart = pRegion->getStart();
			arcEnd = pRegion->getEnd();
			break;
		}
	}
	if(arcStart == arcEnd)
	{
		char errMsg[512];
#ifdef Q_OS_WIN
		sprintf_s(errMsg, sizeof(errMsg) / sizeof(errMsg[0]), "ipIdx %d lower %f upper %f: no arc %f %f", ipIdx, lower, upper,
			arcStart, arcEnd);
#else
		sprintf(errMsg, "ipIdx %d lower %f upper %f: no arc %f %f", ipIdx, lower, upper,
			arcStart, arcEnd);
#endif
		QMessageBox::critical(this, "ERROR", errMsg, QMessageBox::Ok,
			QMessageBox::NoButton, QMessageBox::NoButton);
	}

	// Process segments BEFORE arc (applying coefficient)
	double delta = 0.0;
	LhcSegment	*pSegment;
	for(idx = 0 ; idx < segments.count() ; idx++)
	{
		pSegment = segments.at(idx);
		if((lower <= pSegment->getStart()) && (pSegment->getStart() < arcStart))
		{
			pSegment->setRelStart((float)((pSegment->getStart() - lower) * lssScale));
		}
		if((lower <= pSegment->getEnd()) && (pSegment->getEnd() < arcStart))
		{
			pSegment->setRelEnd((float)((pSegment->getEnd() - lower) * lssScale));
		}
	}
	delta = (arcStart - lower) * lssScale;

	// Process segments WIHTIN arc (no coefficient)
	for(idx = 0 ; idx < segments.count() ; idx++)
	{
		pSegment = segments.at(idx);
		if((arcStart <= pSegment->getStart()) && (pSegment->getStart() <= arcEnd))
		{
			pSegment->setRelStart((float)((pSegment->getStart() - arcStart) + delta));
		}
		if((arcStart <= pSegment->getEnd()) && (pSegment->getEnd() <= arcEnd))
		{
			pSegment->setRelEnd((float)((pSegment->getEnd() - arcStart) + delta));
		}
	}

	// Add ARC segment
	pSegment = addArcSegment( (float)arcStart, (float)arcEnd, NULL );
	pSegment->setRelStart((float)delta);
	pSegment->setRelEnd((float)((arcEnd - arcStart) + delta));
	delta += arcEnd - arcStart;

	// Process segments AFTER arc (again, applying coefficient)
	for(idx = 0 ; idx < segments.count() ; idx++)
	{
		pSegment = segments.at(idx);
		if((arcEnd < pSegment->getStart()) && (pSegment->getStart() <= upper))
		{
			pSegment->setRelStart((float)((pSegment->getStart() - arcEnd) * lssScale + delta));
		}
		if((arcEnd < pSegment->getEnd()) && (pSegment->getEnd() <= upper))
		{
			pSegment->setRelEnd((float)((pSegment->getEnd() - arcEnd) * lssScale + delta));
		}
	}
   
	// Find final range of coordinates to be converted to angles
	double maxRelPos = (upper - arcEnd) * lssScale + delta;

	// Calculate all angles
	for(idx = 0 ; idx < segments.count() ; idx++)
	{
		pSegment = segments.at(idx);
		if((lower <= pSegment->getStart()) && (pSegment->getStart() <= upper) && (!pSegment->isStartAngleSet()))
		{
			pSegment->setStartAngle(ipGeoms.at(ipIdx)->getAngle() + pSegment->getRelStart() * range / maxRelPos);
			pSegment->setStartAngleSet(true);
		}
		if((lower <= pSegment->getEnd()) && (pSegment->getEnd() <= upper) && (!pSegment->isEndAngleSet()))
		{
			pSegment->setEndAngle(ipGeoms.at(ipIdx)->getAngle() + pSegment->getRelEnd() * range / maxRelPos);
			pSegment->setEndAngleSet(true);
		}
	}
	// Calculate all angles for ARCs
	for(idx = 0 ; idx < arcSegments.count() ; idx++)
	{
		pSegment = arcSegments.at(idx);
		if((lower <= pSegment->getStart()) && (pSegment->getStart() <= upper) && (!pSegment->isStartAngleSet()))
		{
			pSegment->setStartAngle(ipGeoms.at(ipIdx)->getAngle() + pSegment->getRelStart() * range / maxRelPos);
			pSegment->setStartAngleSet(true);
		}
		if((lower <= pSegment->getEnd()) && (pSegment->getEnd() <= upper) && (!pSegment->isEndAngleSet()))
		{
			pSegment->setEndAngle(ipGeoms.at(ipIdx)->getAngle() + pSegment->getRelEnd() * range / maxRelPos);
			pSegment->setEndAngleSet(true);
		}
	}
}

/*
**
**	FUNCTION
**		Add one segment to list of all segments, assign some parameters
**		to new segment.
**
**	PARAMETERS
**		vacType	- Vacuum type for new segment
**		start	- Start coordinate for new segment
**		end		- End coordinate for new segment
**		pSector	- Pointer to vacuum sector corresponding to this segment, or
**					NULL if segment does not correspond to vacuum sector
**
**	RETURNS
**		Pointer to segment just added
**
**	CAUTIONS
**		None
**
*/
LhcSegment *MainLHCImage::addSegment(int vacType, float start, float end, Sector *pSector)
{
	LhcSegment	*pSegment = new LhcSegment(this, vacType, start, end, pSector);
	segments.append(pSegment);
	return pSegment;
}

/*
**
**	FUNCTION
**		Add one segment to list of ARC segments, assign some parameters
**		to new segment.
**
**	PARAMETERS
**		start	- Start coordinate for new segment
**		end		- End coordinate for new segment
**		pSector	- Pointer to vacuum sector corresponding to this segment, or
**					NULL if segment does not correspond to vacuum sector
**
**	RETURNS
**		Pointer to segment just added
**
**	CAUTIONS
**		None
**
*/
LhcSegment *MainLHCImage::addArcSegment(float start, float end, Sector *pSector)
{
	LhcSegment	*pSegment = new LhcSegment(this, start, end, pSector);
	arcSegments.append(pSegment);
	return pSegment;
}



/*
**
**	FUNCTION
**		Find sector at given mouse pointer location
**
**	PARAMETERS
**		x	- Mouse pointer X-coordinate
**		y	- Mouse pointer Y-coordinate
**
**	RETURNS
**		Name sector at mouse pointer location, or
**		NULL if there is no sector at that location
**
**	CAUTIONS
**		None
**
*/
const char *MainLHCImage::sectorAtLocation(int x, int y)
{
	// Check injection/dump lines first - they are drawn on top of other lines
	const char *result = closestConnectLine(x, y);
	if(result)
	{
		return result;
	}

	double	dx = x - ringCenterX,
			dy = ringCenterY - y,
			angle;
	double radius = sqrt(dx * dx + dy * dy);
	if(dx == 0.0)
	{
		angle = dy > 0 ? M_PI / 2.0 : -M_PI / 2.0;
	}
	else
	{
		angle = atan2(dy, dx);
	}
	angle = -M_PI / 2.0 - angle;
	if(angle < 0.0)
	{
		angle += 2.0 * M_PI;
	}

	LhcSegment *pBestSegment = NULL;
	int bestInnerRad = -1;
	for(int idx = 0 ; idx < segments.count() ; idx++)
	{
		LhcSegment *pSegment = segments.at(idx);
		if(pSegment->getInnerRad() > radius)
		{
			continue;
		}
		if(pSegment->getOuterRad() < radius)
		{
			continue;
		}
		if(pSegment->getInnerRad() < bestInnerRad)
		{
			continue;
		}
		if(pSegment->getEndAngle() > (2.0 * M_PI))
		{
			if(((pSegment->getStartAngle() <= angle) && (angle <= (2.0 * M_PI))) ||
					((0.0 <= angle) && (angle < (pSegment->getEndAngle() - 2.0 * M_PI))))
			{
				pBestSegment = pSegment;
				bestInnerRad = pSegment->getInnerRad();
			}
		}
		else
		{
			if((pSegment->getStartAngle() <= angle) && (angle <= pSegment->getEndAngle()))
			{
				pBestSegment = pSegment;
				bestInnerRad = pSegment->getInnerRad();
			}
		}
	}
	if(pBestSegment)
	{
		Sector *pSector = pBestSegment->getSector();
		return pSector ? pSector->getName() : NULL;
	}
	return NULL;
}

/*
**
**	FUNCTION
**		Find name of sector on injection/dump lines closest to given screen coordinates.
**
**	PARAMETERS
**		x		- Screen X coordinate
**		y		- Screen Y coordinate
**
**	RETURNS
**		Name of closest sector;
**		NULL if screen point is too far from all sectors
**
**	CAUTIONS
**		None
**
*/
const char *MainLHCImage::closestConnectLine(int x, int y)
{
	const char	*result;
	for(int idx = 0 ; idx < connectedLines.count() ; idx++)
	{
		LhcConnectLine *pLine = connectedLines.at(idx);
		if((result = pLine->sectorAtLocation(x, y, connLineWidth)))
		{
			return result;
		}
	}
	return NULL;
}

/*
**
**	FUNCTION
**		Mark sectors in range between start and end sector as selected, unselect all
**		previously selected sectors
**
**	PARAMETERS
**		pStartSector	- Pointer to first sector to be selected
**		pEndSector		- Pointer to last sector to be selected
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void MainLHCImage::setRingSelection(Sector *pFirstSector, Sector *pLastSector)
{
	// Clear signle sector selection
	selSectors.clear();
	if((!pFirstSector) || (!pLastSector))
	{
		selectSegment->clear();
		renewImage();
		return;
	}

	// Find start and end segments
	LhcSegment *pStartSegment = NULL, *pEndSegment = NULL;
	LhcSegment *pSegment;
	int idx;
	for(idx = 0 ; idx < segments.count() ; idx++)
	{
		pSegment = segments.at(idx);
		if(pSegment->getSector() == pFirstSector)
		{
			pStartSegment = pSegment;
			break;
		}
	}
	if(!pStartSegment)
	{
		renewImage();
		return;
	}
	for(idx = 0 ; idx < segments.count() ; idx++)
	{
		pSegment = segments.at(idx);
		if(pSegment->getSector() == pLastSector)
		{
			pEndSegment = pSegment;
			break;
		}
	}
	if(!pEndSegment)
	{
		renewImage();
		return;
	}

	// Find angle range for selection
	selectSegment->setStartAngle(pStartSegment->getStartAngle());
	selectSegment->setEndAngle(pEndSegment->getEndAngle());
	if(selectSegment->getStartAngle() > selectSegment->getEndAngle())
	{
		selectSegment->setEndAngle(selectSegment->getEndAngle() + 2.0 * M_PI);
	}

	// Build array of angles for selection
	selectSegment->buildPoints(0);
	renewImage();
}

/*
**
**	FUNCTION
**		Mark sectors on injection/dump line as selected, unselect all
**		previously selected sectors
**
**	PARAMETERS
**		sectorList	- List of sector names to be selected
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void MainLHCImage::setLineSelection(QStringList &sectorList)
{
	selectSegment->clear();
	selSectors.clear();
	DataPool &pool = DataPool::getInstance();
	foreach(QString name, sectorList)
	{
		Sector *pSector;
		if((pSector = pool.findSectorData(name.toLatin1())))
		{
			selSectors.append(pSector);
		}
	}
	renewImage();
}

/*
**	FUNCTION
**		Add legend label to list of labels to be drawn
**
**	PARAMETERS
**		x		- X-coordinate for label
**		y		- Y-coordinate for label
**		text	- label text
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHCImage::addLegendLabel(int x, int y, const char *text)
{
	labels.append(new LegendLabel(x, y, text));
}


/*
**
**	FUNCTION
**		Dump parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainLHCImage::dump(FILE *pFile)
{
	if(!DebugCtl::isMainView())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	fprintf(pFile, "%d IPs:\n", ipGeoms.count());
	int	n = 0;
	for(n = 0 ; n < ipGeoms.count() ; n++)
	{
		SineCosine *pAngle = ipGeoms.at(n);
		pAngle->dump(pFile, n);
	}
	DataPool &pool = DataPool::getInstance();
	const QList<LhcRegion *> &regions = pool.getLhcArcs();
	fprintf(pFile, "\n\n%d LHC ARCs:\n", regions.count());
	for(n = 0 ; n < regions.count() ; n++)
	{
		LhcRegion *pRegion = regions.at(n);
		pRegion->dump(pFile, n);
	}
	fprintf(pFile, "\n\n%d segments: from sectors: %d\n", segments.count(), areSegmentsFromSectors);
	for(n = 0 ; n < segments.count() ; n++)
	{
		LhcSegment *pSegment = segments.at(n);
		pSegment->dump(pFile, n);
	}
	fprintf( pFile, "\n\n===================== Connected lines: ===================\n" );
	for(n = 0 ; n < connectedLines.count() ; n++)
	{
		LhcConnectLine *pLine = connectedLines.at(n);
		pLine->dump(pFile);
	}
}
