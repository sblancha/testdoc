#include "VacCtlOnlineNotificationPvss.h"

#include "NotificationConfig.h"
#include "ComplexCriteria.h"
#include "MessageFilteringRule.h"
#include "MessageQueue.h"

#include "SmsConfigParser.h"
#include "SmsConfig.h"

#include "DataPool.h"
#include "Eqp.h"
#include "FunctionalType.h"
#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include <IdExpr.hxx>
#include <BitVar.hxx>
#include <AnyTypeVar.hxx>
#include <UIntegerVar.hxx>
#include <FloatVar.hxx>

#include <QVariant>

/*
**
** FUNCTION
**		The following description has been borrowed from PVSS help:
**
**		The newExternHdl function has a fixed interface and must be
**		exported so that it can be called by the PVSS_II Manager.
**		The keyword '_declspec(dllexport)' has to be set only under
**		windows for exporting the function newExternHdl. Under Linux
**		this keyword is not necessary. 
**		The newExternHdl function must allocate a class object and return
**		the pointer to it.
**
** PARAMETERS
**		nextHdl	
**
** RETURNS
**		Pointer to new class instance
**
** CAUTIONS
**		None
*/

/*
BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl)
{

*/

	static FunctionListRec fnList[] =
	{
		// Every structure in array contains:
		// Return-Value    function name
		// parameter list
		// true == thread-save
    	{
			INTEGER_VAR, "VacSmsGetFuncTypes", 
			"(dyn_int &typeIds, dyn_string &typeNames)",
			false
		},
		{
			INTEGER_VAR, "VacSmsGetFuncTypeCriteria", 
			"(int typeId, dyn_int &funcTypes, dyn_int &types, dyn_int &subTypes, dyn_string &names)",
			false
		},
		{
			INTEGER_VAR, "VacSmsGetCriteriaValues", 
			"(int typeId, int critType, int critSubType, dyn_int &values, dyn_string &names)",
			false
		},
		{
			INTEGER_VAR, "VacSmsGetStdCriteriaMessage", 
			"(int funcType, int critType, int critSubType, int sourceType, bool reverse, string &message)",
			false
		},
  		{
			INTEGER_VAR, "VacSmsSetCriteria",
			"(string config, int joinType, dyn_int funcTypes, dyn_int types, dyn_int subtypes, dyn_float lowerLimits, dyn_float upperLimits, dyn_bool reverse, dyn_int minDurations)",
			false
		},
  		{
			INTEGER_VAR, "VacSmsSetUsageFilter",
			"(string config, dyn_int funcTypes, dyn_string attrNames, dyn_bool reverseFlags)",
			false
		},
		{
			INTEGER_VAR, "VacSmsSetMessage",
			"(string config, string message, string &errMsg)",
			false
		},
  		{
			INTEGER_VAR, "VacSmsSetScope",
			"(string config, int type, dyn_string names, string &errMsg)",
			false
		},
		{
			INTEGER_VAR, "VacSmsClearGrouping",
			"(string config, string &errMsg)",
			false
		},
		{
			INTEGER_VAR, "VacSmsSetDeadTime",
			"(string config, int deadTime, string &errMsg)",
			false
		},
		{
			INTEGER_VAR, "VacSmsSetGrouping",
			"(string config, int type, int interval, int count, string groupMsg, string &errMsg)",
			false
		},
		{
			INTEGER_VAR, "VacSmsSetConfigActive",
			"(string config, bool active, string &errMsg)",
			false
		},
		{
			INTEGER_VAR, "VacSmsIsConfigActive",
			"(string config, bool &active)",
			false
		},
		{
			INTEGER_VAR, "VacSmsSetConfigInitialized",
			"(string config, string &errMsg)",
			false
		},
		{
			INTEGER_VAR, "VacSmsDeleteConfig",
			"(string config)",
			false
		},
		{
			INTEGER_VAR, "VacSmsCheckGroupExpired",
			"()",
			false
		},
		{
			INTEGER_VAR, "VacSmsGetNextMessage",
			"(string &config, string &message, string &body)",
			false
		},

		// To/from file
		{
			INTEGER_VAR, "VacSmsConfigPrepareForWrite",
			"()",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigAdd",
			"(string configId)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigSetVisibleName",
			"(int configId, string name)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigSetMode",
			"(int configId, int mode)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigSetMainType",
			"(int configId, int type)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigSetUsageFilter",
			"(int configId, dyn_int funcTypes, dyn_string attrNames, dyn_bool reverseFlags)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigSetScopeType",
			"(int configId, int type)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigSetScopeItems",
			"(int configId, dyn_string itemNames)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigSetCritJoinType",
			"(int configId, int type)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigAddCriteria",
			"(int configId, int funcType, int type, int subType, bool reverse, float lLimit, float uLimit, int minDuration)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigSetMessage",
			"(int configId, string message)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigSetFiltering",
			"(int configId, int type, int time, int groupType, int groupTime, int indivLimit)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigSetGroupMessage",
			"(int configId, string message)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigSetRecipients",
			"(int configId, dyn_string names)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigWrite",
			"(string fileName, dyn_string &exceptionInfo)",
			false
		},

		{
			INTEGER_VAR, "VacSmsConfigParse",
			"(string fileName, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigFirstParsed",
			"()",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigNextParsed",
			"(int configId)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetVisibleName",
			"(int configId, int &mode, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetMode",
			"(int configId, int &mode, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetMainType",
			"(int configId, int &type, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetUsageFilter",
			"(int configId, dyn_int &funcTypes, dyn_string &attrNames, dyn_bool &reverseFlags, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetScopeType",
			"(int configId, int &type, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetScopeItems",
			"(int configId, dyn_string &items, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetCritJoinType",
			"(int configId, int &type, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetCriteria",
			"(int configId, dyn_int &funcTypes, dyn_int &types, dyn_int &subTypes, dyn_bool &reverseFlags, dyn_float &lLimits, dyn_float &uLimits, dyn_int &minDurations, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetMessage",
			"(int configId, string &message, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetFiltering",
			"(int configId, int &type, int &time, int &groupType, int &groupTime, int &indivLimit, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetGroupMessage",
			"(int configId, string &message, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "VacSmsConfigGetRecipients",
			"(int configId, dyn_string &items, dyn_string &exceptionInfo)",
			false
		},
		// Given functional type - used by other functional types?
		{
			INTEGER_VAR, "VacSmsConfigGetFuncTypeUsage",
			"(int funcTypeId, dyn_int &typeIdsUsingThis, dyn_string &typesNamesUsingThis, dyn_string &attrNames)",
			false
		},
	};

/*
	// this line counts the number of functions you want to implement.
  	PVSSulong funcCount = sizeof(fnList) / sizeof(FunctionListRec);

	// now allocate the new instance
	return new VacCtlOnlineNotificationPvss(nextHdl, funcCount, fnList);
}
*/

CTRL_EXTENSION(VacCtlOnlineNotificationPvss, fnList)

/*
**
** FUNCTION
**		The following description has been borrowed from PVSS help:
**
**		This function is called every time we use one of the above
**		function names in a Ctrl script.
**		The argument is an aggregaton of function name, function number, 
**		the arguments passed to the Ctrl function, the "thread" context 
**		and user defined data.
**
** PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
** RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
const Variable *VacCtlOnlineNotificationPvss::execute(ExecuteParamRec &param)
{
	enum
	{
		VacSmsGetFuncTypes = 0,
		VacSmsGetFuncTypeCriteria,
		VacSmsGetCriteriaValues,
		VacSmsGetStdCriteriaMessage,
		VacSmsSetCriteria,
		VacSmsSetUsageFilter,
		VacSmsSetMessage,
		VacSmsSetScope,
		VacSmsClearGrouping,
		VacSmsSetDeadTime,
		VacSmsSetGrouping,
		VacSmsSetConfigActive,
		VacSmsIsConfigActive,
		VacSmsSetConfigInitialized,
		VacSmsDeleteConfig,
		VacSmsCheckGroupExpired,
		VacSmsGetNextMessage,

		VacSmsConfigPrepareForWrite,
		VacSmsConfigAdd,
		VacSmsConfigSetVisibleName,
		VacSmsConfigSetMode,
		VacSmsConfigSetMainType,
		VacSmsConfigSetUsageFilter,
		VacSmsConfigSetScopeType,
		VacSmsConfigSetScopeItems,
		VacSmsConfigSetCritJoinType,
		VacSmsConfigAddCriteria,
		VacSmsConfigSetMessage,
		VacSmsConfigSetFiltering,
		VacSmsConfigSetGroupMessage,
		VacSmsConfigSetRecipients,
		VacSmsConfigWrite,

		VacSmsConfigParse,
		VacSmsConfigFirstParsed,
		VacSmsConfigNextParsed,
		VacSmsConfigGetVisibleName,
		VacSmsConfigGetMode,
		VacSmsConfigGetMainType,
		VacSmsConfigGetUsageFilter,
		VacSmsConfigGetScopeType,
		VacSmsConfigGetScopeItems,
		VacSmsConfigGetCritJoinType,
		VacSmsConfigGetCriteria,
		VacSmsConfigGetMessage,
		VacSmsConfigGetFiltering,
		VacSmsConfigGetGroupMessage,
		VacSmsConfigGetRecipients,

		VacSmsConfigGetFuncTypeUsage
	};

	// TODO for the different return types you need corresponding static Variables
	static IntegerVar	integerVar;
	int					coco;

	integerVar.setValue(0);
	param.thread->clearLastError( );
	switch (param.funcNum)
	{
	case VacSmsGetFuncTypes:
		coco = processGetFuncTypes(param);
		break;
	case VacSmsGetFuncTypeCriteria:
		coco = processGetFuncTypeCriteria(param);
		break;
	case VacSmsGetCriteriaValues:
		coco = processGetCriteriaValues(param);
		break;
	case VacSmsGetStdCriteriaMessage:
		coco = processGetStdCriteriaMessage(param);
		break;
	case VacSmsSetCriteria:
		coco = processSetCriteria(param);
		break;
	case VacSmsSetUsageFilter:
		coco = processSetUsageFilter(param);
		break;
	case VacSmsSetMessage:
		coco = processSetMessage(param);
		break;
	case VacSmsSetScope:
		coco = processSetScope(param);
		break;
	case VacSmsClearGrouping:
		coco = processClearGrouping(param);
		break;
	case VacSmsSetDeadTime:
		coco = processSetDeadTime(param);
		break;
	case VacSmsSetGrouping:
		coco = processSetGrouping(param);
		break;
	case VacSmsSetConfigActive:
		coco = processSetConfigActive(param);
		break;
	case VacSmsIsConfigActive:
		coco = processIsConfigActive(param);
		break;
	case VacSmsSetConfigInitialized:
		coco = processSetConfigInitialized(param);
		break;
	case VacSmsDeleteConfig:
		coco = processDeleteConfig(param);
		break;
	case VacSmsCheckGroupExpired:
		coco = processCheckGroupExpired(param);
		break;
	case VacSmsGetNextMessage:
		coco = processGetNextMessage(param);
		break;

	case VacSmsConfigPrepareForWrite:
		coco = processConfigPrepareForWrite(param);
		break;
	case VacSmsConfigAdd:
		coco = processConfigAdd(param);
		break;
	case VacSmsConfigSetVisibleName:
		coco = processSetVisibleName(param);
		break;
	case VacSmsConfigSetMode:
		coco = processConfigSetMode(param);
		break;
	case VacSmsConfigSetMainType:
		coco = processConfigSetMainType(param);
		break;
	case VacSmsConfigSetUsageFilter:
		coco = processConfigSetUsageFilter(param);
		break;
	case VacSmsConfigSetScopeType:
		coco = processConfigSetScopeType(param);
		break;
	case VacSmsConfigSetScopeItems:
		coco = processConfigSetScopeItems(param);
		break;
	case VacSmsConfigSetCritJoinType:
		coco = processConfigSetCritJoinType(param);
		break;
	case VacSmsConfigAddCriteria:
		coco = processConfigAddCriteria(param);
		break;
	case VacSmsConfigSetMessage:
		coco = processConfigSetMessage(param);
		break;
	case VacSmsConfigSetFiltering:
		coco = processConfigSetFiltering(param);
		break;
	case VacSmsConfigSetGroupMessage:
		coco = processConfigSetGroupMessage(param);
		break;
	case VacSmsConfigSetRecipients:
		coco = processConfigSetRecipients(param);
		break;
	case VacSmsConfigWrite:
		coco = processConfigWrite(param);
		break;

	case VacSmsConfigParse:
		coco = processConfigParse(param);
		break;
	case VacSmsConfigFirstParsed:
		coco = processConfigFirstParsed(param);
		break;
	case VacSmsConfigNextParsed:
		coco = processConfigNextParsed(param);
		break;
	case VacSmsConfigGetVisibleName:
		coco = processGetVisibleName(param);
		break;
	case VacSmsConfigGetMode:
		coco = processConfigGetMode(param);
		break;
	case VacSmsConfigGetMainType:
		coco = processConfigGetMainType(param);
		break;
	case VacSmsConfigGetUsageFilter:
		coco = processConfigGetUsageFilter(param);
		break;
	case VacSmsConfigGetScopeType:
		coco = processConfigGetScopeType(param);
		break;
	case VacSmsConfigGetScopeItems:
		coco = processConfigGetScopeItems(param);
		break;
	case VacSmsConfigGetCritJoinType:
		coco = processConfigGetCritJoinType(param);
		break;
	case VacSmsConfigGetCriteria:
		coco = processConfigGetCriteria(param);
		break;
	case VacSmsConfigGetMessage:
		coco = processConfigGetMessage(param);
		break;
	case VacSmsConfigGetFiltering:
		coco = processConfigGetFiltering(param);
		break;
	case VacSmsConfigGetGroupMessage:
		coco = processConfigGetGroupMessage(param);
		break;
	case VacSmsConfigGetRecipients:
		coco = processConfigGetRecipients(param);
		break;

	case VacSmsConfigGetFuncTypeUsage:
		coco = processGetFuncTypeUsage(param);
		break;

	default:
		coco = -1;
		break;
	}
	integerVar.setValue(coco);
	return &integerVar;
}

/*
**	FUNCTION
**		Process call to 	VacSmsGetFuncTypes
**		PVSS CTRL arguments are:
**			dyn_int &typeIds
**			dyn_string &typeNames
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processGetFuncTypes(ExecuteParamRec &param)
{
	Variable	*typeIds, *typeNames;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "typeIds",	DYNINTEGER_VAR,	NULL,	&typeIds },
		{ "typeNames",	DYNTEXT_VAR,	NULL,	&typeNames }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)typeIds)->clear();
	((DynVar *)typeNames)->clear();
	const QList<FunctionalType *> &typeList = FunctionalType::getTypes();
	for(int typeIdx = 0 ; typeIdx < typeList.count() ; typeIdx++)
	{
		FunctionalType *pType = typeList.at(typeIdx);
		if(!pType->getCriteria().isEmpty())
		{
			IntegerVar id(pType->getType());
			((DynVar *)typeIds)->append(id);
			TextVar name;
			name.setValue(pType->getDescription());
			((DynVar *)typeNames)->append(name);
		}
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to VacSmsGetFuncTypeCriteria()
**		PVSS CTRL arguments are:
**			int typeId
**			dyn_int &funcTypes
**			dyn_int &types
**			dyn_int &subTypes
**			dyn_string &names
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processGetFuncTypeCriteria(ExecuteParamRec &param)
{
	IntegerVar	typeId;
	Variable	*funcTypes, *types, *subTypes, *names;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "typeId",		INTEGER_VAR,	&typeId,	NULL },
		{ "funcTypes",	DYNINTEGER_VAR,	NULL,		&funcTypes },
		{ "types",		DYNINTEGER_VAR,	NULL,		&types },
		{ "subTypes",	DYNINTEGER_VAR,	NULL,		&subTypes },
		{ "names",		DYNTEXT_VAR,	NULL,		&names }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)funcTypes)->clear();
	((DynVar *)types)->clear();
	((DynVar *)subTypes)->clear();
	((DynVar *)names)->clear();

	FunctionalType *pType = FunctionalType::findTypeData(typeId.getValue());
	if(!pType)
	{
		return 0;
	}
	const QList<EqpMsgCriteria *> &allCriteria = pType->getCriteria();
	for(int idx = 0 ; idx < allCriteria.count() ; idx++)
	{
		EqpMsgCriteria *pCriteria = allCriteria.at(idx);
		IntegerVar funcType(pCriteria->getFunctionalType());
		((DynVar *)funcTypes)->append(funcType);
		IntegerVar type(pCriteria->getType());
		((DynVar *)types)->append(type);
		IntegerVar subType(pCriteria->getSubType());
		((DynVar *)subTypes)->append(subType);
		TextVar name;
		name.setValue(pCriteria->getName().toLatin1());
		((DynVar *)names)->append(name);
	}
	return allCriteria.count();
}

/*
**	FUNCTION
**		Process call to VacSmsGetCriteriaValues()
**		PVSS CTRL arguments are:
**			int typeId
**			int critType
**			int critSubType
**			dyn_int &values
**			dyn_string &names
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processGetCriteriaValues(ExecuteParamRec &param)
{
	IntegerVar	typeId, critType, critSubType;
	Variable	*values, *names;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "typeId",			INTEGER_VAR,	&typeId,		NULL },
		{ "critType",		INTEGER_VAR,	&critType,		NULL },
		{ "critSubType",	INTEGER_VAR,	&critSubType,	NULL },
		{ "values",			DYNINTEGER_VAR,	NULL,			&values },
		{ "names",		DYNTEXT_VAR,	NULL,		&names }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)values)->clear();
	((DynVar *)names)->clear();

	FunctionalType *pType = FunctionalType::findTypeData(typeId.getValue());
	if(!pType)
	{
		return 0;
	}
	QList<int> valueList;
	QList<QByteArray> labelList;
	pType->getCriteriaValues(critType.getValue(), critSubType.getValue(), valueList, labelList);
	int coco = 0;
	for(int idx = 0 ; idx < valueList.count() ; idx++)
	{
		if(idx >= labelList.count())
		{
			break;
		}
		coco++;
		IntegerVar value(valueList.at(idx));
		((DynVar *)values)->append(value);
		TextVar label;
		label.setValue(labelList.at(idx).constData());
		((DynVar *)names)->append(label);
	}
	return coco;
}


/*
**	FUNCTION
**		Process call to VacSmsGetStdCriteriaMessage
**		PVSS CTRL arguments are:
**			int funcType
**			int critType
**			int critSubType
**			int sourceType
**			bool reverse
**			string &message
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processGetStdCriteriaMessage(ExecuteParamRec &param)
{
	IntegerVar	funcType, critType, critSubType, sourceType;
	BitVar		reverse;
	Variable	*message;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "funcType",		INTEGER_VAR,	&funcType,		NULL },
		{ "critType",		INTEGER_VAR,	&critType,		NULL },
		{ "critSubType",	INTEGER_VAR,	&critSubType,	NULL },
		{ "sourceType",		INTEGER_VAR,	&sourceType,	NULL },
		{ "reverse",		BIT_VAR,		&reverse,		NULL },
		{ "message",		TEXT_VAR,		NULL,			&message }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	
	((TextVar *)message)->setValue("");

	FunctionalType *pType = FunctionalType::findTypeData(funcType.getValue());
	if(!pType)
	{
		return 0;
	}
	const QList<EqpMsgCriteria *> &allCriteria = pType->getCriteria();
	for(int idx = 0 ; idx < allCriteria.count() ; idx++)
	{
		EqpMsgCriteria *pCriteria = allCriteria.at(idx);
		if((pCriteria->getType() == critType.getValue()) &&
			(pCriteria->getSubType() == critSubType.getValue()))
		{
			const char *msg = pCriteria->getStdMessage(sourceType.getValue(), reverse.getValue() == PVSS_TRUE);
			if(msg)
			{
				((TextVar *)message)->setValue(msg);
			}
			break;
		}
	}
	return allCriteria.count();
}

/*
**	FUNCTION
**		Process call to VacSmsSetCriteria
**		PVSS CTRL arguments are:
**			string config
**			int joinType
**			dyn_int funcTypes
**			dyn_int types
**			dyn_int subtypes
**			dyn_float lowerLimits
**			dyn_float upperLimits
**			dyn_bool reverse
**			dyn_int minDurations
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processSetCriteria(ExecuteParamRec &param)
{
	TextVar		config;
	IntegerVar	joinType;
	DynVar		funcTypes, types, subTypes, lowerLimits, upperLimits, reverse, minDurations;

// qDebug("VacCtlOnlineNotificationPvss::processSetCriteria(): start\n");

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",			TEXT_VAR,		&config,		NULL },
		{ "joinType",		INTEGER_VAR,	&joinType,		NULL },
		{ "funcTypes",		DYNINTEGER_VAR,	&funcTypes,		NULL },
		{ "types",			DYNINTEGER_VAR,	&types,			NULL },
		{ "subTypes",		DYNINTEGER_VAR,	&subTypes,		NULL },
		{ "lowerLimits",	DYNFLOAT_VAR,	&lowerLimits,	NULL },
		{ "upperLimits",	DYNFLOAT_VAR,	&upperLimits,	NULL },
		{ "reverse",		DYNBIT_VAR,		&reverse,		NULL },
		{ "minDurations",	DYNINTEGER_VAR,	&minDurations,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

// qDebug("VacCtlOnlineNotificationPvss::processSetCriteria(): start, funcTypes %d\n", funcTypes.getNumberOfItems());
	if(!funcTypes.getNumberOfItems())
	{
		return -1;
	}
// qDebug("VacCtlOnlineNotificationPvss::processSetCriteria(): start, types %d\n", types.getNumberOfItems());
	if(funcTypes.getNumberOfItems() != types.getNumberOfItems())
	{
		return -1;
	}
// qDebug("VacCtlOnlineNotificationPvss::processSetCriteria(): start, subTypes %d\n", subTypes.getNumberOfItems());
	if(funcTypes.getNumberOfItems() != subTypes.getNumberOfItems())
	{
		return -1;
	}
// qDebug("VacCtlOnlineNotificationPvss::processSetCriteria(): start, lowerLimits %d\n", lowerLimits.getNumberOfItems());
	if(funcTypes.getNumberOfItems() != lowerLimits.getNumberOfItems())
	{
		return -1;
	}
// qDebug("VacCtlOnlineNotificationPvss::processSetCriteria(): start, upperLimits %d\n", upperLimits.getNumberOfItems());
	if(funcTypes.getNumberOfItems() != upperLimits.getNumberOfItems())
	{
		return -1;
	}
// qDebug("VacCtlOnlineNotificationPvss::processSetCriteria(): start, reverse %d\n", reverse.getNumberOfItems());
	if(funcTypes.getNumberOfItems() != reverse.getNumberOfItems())
	{
		return -1;
	}
// qDebug("VacCtlOnlineNotificationPvss::processSetCriteria(): start, minDurations %d\n", minDurations.getNumberOfItems());
	if(funcTypes.getNumberOfItems() != minDurations.getNumberOfItems())
	{
		return -1;
	}

	ComplexCriteria *pComplexCriteria = new ComplexCriteria(joinType.getValue());
	for(unsigned short idx = 0 ; idx < funcTypes.getNumberOfItems() ; idx++)
	{
		IntegerVar *pFuncType = (IntegerVar *)funcTypes.getAt(idx);
		IntegerVar *pType = (IntegerVar *)types.getAt(idx);
		IntegerVar *pSubType = (IntegerVar *)subTypes.getAt(idx);
		FloatVar *pLowerLimit = (FloatVar *)lowerLimits.getAt(idx);
		FloatVar *pUpperLimit = (FloatVar *)upperLimits.getAt(idx);
		BitVar *pReverse = (BitVar *)reverse.getAt(idx);
		IntegerVar *pMinDuration = (IntegerVar *)minDurations.getAt(idx);
/*
qDebug("VacCtlOnlineNotificationPvss::processSetCriteria(): adding criteria funcType %d type %d subType %d\n",
pFuncType->getValue(), pType->getValue(), pSubType->getValue());
*/
		EqpMsgCriteria *pCriteria = new EqpMsgCriteria(pFuncType->getValue(), pType->getValue(),
			pSubType->getValue(), "");
		pCriteria->setLowerLimit(pLowerLimit->getValue());
		pCriteria->setUpperLimit(pUpperLimit->getValue());
		pCriteria->setReverse(pReverse->getValue() == PVSS_TRUE);
		pCriteria->setMinDuration(pMinDuration->getValue());

		pComplexCriteria->addCriteria(pCriteria);
	}

	NotificationConfig *pConfig = NotificationConfig::findConfig(config.getValue());
	if(!pConfig)
	{
		QString errText = "Unknown config <";
		errText += config.getValue();
		errText += ">";
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}
	if(pConfig->isActivated())
	{
		delete pComplexCriteria;
		return -1;
	}
	pConfig->setCriteria(pComplexCriteria);
	return 0;
}

/*
**	FUNCTION
**		Process call to VacSmsSetUsageFilter
**		PVSS CTRL arguments are:
**			string config
**			dyn_int funcTypes
**			dyn_string attrNames
**			dyn_bool reverseFlags
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processSetUsageFilter(ExecuteParamRec &param)
{
	TextVar		config;
	DynVar		funcTypes, attrNames, reverseFlags;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",			TEXT_VAR,		&config,		NULL },
		{ "funcTypes",		DYNINTEGER_VAR,	&funcTypes,		NULL },
		{ "attrNames",		DYNTEXT_VAR,	&attrNames,		NULL },
		{ "reverseFlags",	DYNBIT_VAR,		&reverseFlags,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	if((funcTypes.getNumberOfItems() != attrNames.getNumberOfItems())
		|| (funcTypes.getNumberOfItems() != reverseFlags.getNumberOfItems()))
	{
		QString errText = "Mistamcthing lists lengths: funcTypes ";
		errText += QString::number(funcTypes.getNumberOfItems());
		errText += ", attrNames ";
		errText += QString::number(attrNames.getNumberOfItems());
		errText += ", reverseFlags ";
		errText += QString::number(reverseFlags.getNumberOfItems());
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}

	NotificationConfig *pConfig = NotificationConfig::findConfig(config.getValue());
	if(!pConfig)
	{
		QString errText = "Unknown config <";
		errText += config.getValue();
		errText += ">";
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}
	if(pConfig->isActivated())
	{
		QString errText = "Config <";
		errText += config.getValue();
		errText += "> is activated";
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}
	pConfig->clearUsageFilter();
	for(unsigned short idx = 0 ; idx < funcTypes.getNumberOfItems() ; idx++)
	{
		IntegerVar *pFuncType = (IntegerVar *)funcTypes.getAt(idx);
		TextVar *pAttrName = (TextVar *)attrNames.getAt(idx);
		BitVar *pReverse = (BitVar *)reverseFlags.getAt(idx);
		pConfig->addUsageFilterItem(pFuncType->getValue(), pAttrName->getValue(), pReverse->getValue() == PVSS_TRUE);
	}
	pConfig->finishUsageFilter();
	return 0;
}

/*
**	FUNCTION
**		Process call to VacSmsSetMessage
**		PVSS CTRL arguments are:
**			string config
**			string message
**			string &errMsg
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processSetMessage(ExecuteParamRec &param)
{
	TextVar		config, message;
	Variable	*errMsg;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",		TEXT_VAR,	&config,	NULL },
		{ "message",	TEXT_VAR,	&message,	NULL },
		{ "errMsg",		TEXT_VAR,	NULL,		&errMsg }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)errMsg)->setValue("");

	QString qtErrMsg;
		
	NotificationConfig *pConfig = NotificationConfig::findConfig(config.getValue());
	if(!pConfig)
	{
		QString errText = "Unknown config <";
		errText += config.getValue();
		errText += ">";
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}
	if(pConfig->isActivated())
	{
		((TextVar *)errMsg)->setValue("Can't set message: config is activated");
		return -1;
	}
	pConfig->setMessageRule(message.getValue(), qtErrMsg);
	((TextVar *)errMsg)->setValue(qtErrMsg.toLatin1());
	return qtErrMsg.isEmpty() ? 0 : -1;
}

/*
**	FUNCTION
**		Process call to VacSmsSetScope
**		PVSS CTRL arguments are:
**			string config
**			int type
**			dyn_string names
**			string &errMsg
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processSetScope(ExecuteParamRec &param)
{
	TextVar		config;
	IntegerVar	type;
	DynVar		names;
	Variable	*errMsg;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",		TEXT_VAR,		&config,	NULL },
		{ "type",		INTEGER_VAR,	&type,	NULL },
		{ "names",		DYNTEXT_VAR,	&names,	NULL },
		{ "errMsg",		TEXT_VAR,		NULL,		&errMsg }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)errMsg)->setValue("");

	NotificationConfig *pConfig = NotificationConfig::findConfig(config.getValue());
	if(!pConfig)
	{
		QString errText = "Unknown config <";
		errText += config.getValue();
		errText += ">";
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}
	if(pConfig->isActivated())
	{
		((TextVar *)errMsg)->setValue("Can't set scope: config is activated");
		return -1;
	}
	QStringList nameList;
	for(unsigned short idx = 0 ; idx < names.getNumberOfItems() ; idx++)
	{
		TextVar *pName = (TextVar *)names.getAt(idx);
		nameList.append(pName->getValue());
	}
	pConfig->setScope(type.getValue(), nameList);
	return 0;
}

/*
**	FUNCTION
**		Process call to VacSmsClearGrouping",
**		PVSS CTRL arguments are:
**			string config
**			string &errMsg
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processClearGrouping(ExecuteParamRec &param)
{
	TextVar		config;
	Variable	*errMsg;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",		TEXT_VAR,		&config,	NULL },
		{ "errMsg",		TEXT_VAR,		NULL,		&errMsg }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)errMsg)->setValue("");

	NotificationConfig *pConfig = NotificationConfig::findConfig(config.getValue());
	if(!pConfig)
	{
		QString errText = "Unknown config <";
		errText += config.getValue();
		errText += ">";
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}
	if(pConfig->isActivated())
	{
		((TextVar *)errMsg)->setValue("Can't clear grouping: config is activated");
		return -1;
	}
	MessageFilteringRule &msgFilter = pConfig->getMsgFilter();
	
	QString qtErrMsg;
	msgFilter.setType(MessageFilteringRule::None, qtErrMsg);
	((TextVar *)errMsg)->setValue(qtErrMsg.toLatin1());
	return qtErrMsg.isEmpty() ? 0 : -1;
}

/*
**	FUNCTION
**		Process call to VacSmsSetDeadTime
**		PVSS CTRL arguments are:
**			string config
**			int deadTime
**			string &errMsg
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processSetDeadTime(ExecuteParamRec &param)
{
	TextVar		config;
	IntegerVar	deadTime;
	Variable	*errMsg;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",		TEXT_VAR,		&config,	NULL },
		{ "deadTime",	INTEGER_VAR,	&deadTime,	NULL },
		{ "errMsg",		TEXT_VAR,		NULL,		&errMsg }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)errMsg)->setValue("");

	NotificationConfig *pConfig = NotificationConfig::findConfig(config.getValue());
	if(!pConfig)
	{
		QString errText = "Unknown config <";
		errText += config.getValue();
		errText += ">";
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}
	if(pConfig->isActivated())
	{
		((TextVar *)errMsg)->setValue("Can't set dead time: config is activated");
		return -1;
	}
	MessageFilteringRule &msgFilter = pConfig->getMsgFilter();
	
	QString qtErrMsg;
	msgFilter.setType(MessageFilteringRule::DeadTime, qtErrMsg);
	if(!qtErrMsg.isEmpty())
	{
		((TextVar *)errMsg)->setValue(qtErrMsg.toLatin1());
		return -1;
	}
	msgFilter.setDeadTime(deadTime.getValue(), qtErrMsg);
	((TextVar *)errMsg)->setValue(qtErrMsg.toLatin1());
	return qtErrMsg.isEmpty() ? 0 : -1;
}


/*
**	FUNCTION
**		Process call to VacSmsSetGrouping
**		PVSS CTRL arguments are:
**			string config
**			int type
**			int interval
**			int count
**			string groupMsg
**			string &errMsg
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processSetGrouping(ExecuteParamRec &param)
{
	TextVar		config, groupMsg;
	IntegerVar	type, interval, count;
	Variable	*errMsg;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",		TEXT_VAR,		&config,	NULL },
		{ "type",		INTEGER_VAR,	&type,		NULL },
		{ "interval",	INTEGER_VAR,	&interval,	NULL },
		{ "count",		INTEGER_VAR,	&count,		NULL },
		{ "groupMsg",	TEXT_VAR,		&groupMsg,	NULL },
		{ "errMsg",		TEXT_VAR,		NULL,		&errMsg }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)errMsg)->setValue("");

	NotificationConfig *pConfig = NotificationConfig::findConfig(config.getValue());
	if(!pConfig)
	{
		QString errText = "Unknown config <";
		errText += config.getValue();
		errText += ">";
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}
	if(pConfig->isActivated())
	{
		((TextVar *)errMsg)->setValue("Can't set grouping: config is activated");
		return -1;
	}
	MessageFilteringRule &msgFilter = pConfig->getMsgFilter();
	
	QString qtErrMsg;
	msgFilter.setType(MessageFilteringRule::Grouping, qtErrMsg);
	if(!qtErrMsg.isEmpty())
	{
		((TextVar *)errMsg)->setValue(qtErrMsg.toLatin1());
		return -1;
	}

	msgFilter.setGroupType(type.getValue(), qtErrMsg);
	if(!qtErrMsg.isEmpty())
	{
		((TextVar *)errMsg)->setValue(qtErrMsg.toLatin1());
		return -1;
	}

	msgFilter.setGroupInterval(interval.getValue(), qtErrMsg);
	if(!qtErrMsg.isEmpty())
	{
		((TextVar *)errMsg)->setValue(qtErrMsg.toLatin1());
		return -1;
	}

	msgFilter.setGroupLimit(count.getValue(), qtErrMsg);
	if(!qtErrMsg.isEmpty())
	{
		((TextVar *)errMsg)->setValue(qtErrMsg.toLatin1());
		return -1;
	}

	if(!strlen(groupMsg.getValue()))
	{
		((TextVar *)errMsg)->setValue("Empty group message");
		return -1;
	}
	msgFilter.setGroupText(groupMsg.getValue(), qtErrMsg);
	if(!qtErrMsg.isEmpty())
	{
		((TextVar *)errMsg)->setValue(qtErrMsg.toLatin1());
		return -1;
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to VacSmsSetConfigActive
**		PVSS CTRL arguments are:
**			string config
**			bool active
**			string &errMsg
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processSetConfigActive(ExecuteParamRec &param)
{
	TextVar		config;
	BitVar		active;
	Variable	*errMsg;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",		TEXT_VAR,	&config,	NULL },
		{ "active",		BIT_VAR,	&active,	NULL },
		{ "errMsg",		TEXT_VAR,	NULL,		&errMsg }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)errMsg)->setValue("");

	NotificationConfig *pConfig = NotificationConfig::findConfig(config.getValue());
	if(!pConfig)
	{
		QString errText = "Unknown config <";
		errText += config.getValue();
		errText += ">";
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}
	int coco = 0;
	if(active.getValue() == PVSS_TRUE)
	{
		QString qtErrMsg;
		pConfig->activate(qtErrMsg);
		((TextVar *)errMsg)->setValue(qtErrMsg.toLatin1());
		if(!qtErrMsg.isEmpty())
		{
			coco = -1;
		}
	}
	else
	{
		pConfig->deactivate();
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsIsConfigActive
**		PVSS CTRL arguments are:
**			string config
**			bool &active
**			string &errMsg
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processIsConfigActive(ExecuteParamRec &param)
{
	TextVar	config;
	Variable	*active;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",		TEXT_VAR,	&config,	NULL },
		{ "active",		BIT_VAR,		NULL,	&active }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	NotificationConfig *pConfig = NotificationConfig::findConfig(config.getValue());
	if(!pConfig)
	{
		QString errText = "Unknown config <";
		errText += config.getValue();
		errText += ">";
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}
	int coco = 0;
	if(pConfig->isActivated())
	{
		((BitVar *)active)->setValue(PVSS_TRUE);
	}
	else
	{
		((BitVar *)active)->setValue(PVSS_FALSE);
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsSetConfigInitialized
**		PVSS CTRL arguments are:
**			string config
**			string &errMsg
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processSetConfigInitialized(ExecuteParamRec &param)
{
	TextVar		config;
	Variable	*errMsg;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",		TEXT_VAR,		&config,	NULL },
		{ "errMsg",		TEXT_VAR,		NULL,		&errMsg }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)errMsg)->setValue("");

	NotificationConfig *pConfig = NotificationConfig::findConfig(config.getValue());
	if(!pConfig)
	{
		QString errText = "Unknown config <";
		errText += config.getValue();
		errText += ">";
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}
	if(!pConfig->isActivated())
	{
		((TextVar *)errMsg)->setValue("Can't set initialized: config is NOT activated");
		return -1;
	}
	pConfig->setInitialized();
	return 0;
}

/*
**	FUNCTION
**		Process call to VacSmsDeleteConfig()
**		PVSS CTRL arguments are:
**			string config
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processDeleteConfig(ExecuteParamRec &param)
{
	TextVar		config;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",		TEXT_VAR,		&config,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	NotificationConfig::deleteConfig(config.getValue());
	return 0;
}

/*
**	FUNCTION
**		Process call to VacSmsCheckGroupExpired
**		PVSS CTRL arguments are:
**			none
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processCheckGroupExpired(ExecuteParamRec & /* param */)
{
	NotificationConfig::checkMsgGroupExpire();
	return 0;
}

/*
**	FUNCTION
**		Process call to VacSmsGetNextMessage()
**		PVSS CTRL arguments are:
**			string &config
**			string &message
**			string &body
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processGetNextMessage(ExecuteParamRec &param)
{
	Variable	*config, *message, *body;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "config",		TEXT_VAR,	NULL,	&config },
		{ "message",	TEXT_VAR,	NULL,	&message },
		{ "body",		TEXT_VAR,	NULL,	&body }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)config)->setValue("");
	((TextVar *)message)->setValue("");

	int coco = 0;
	QString qtConfig, qtMessage, qtBody;
	if(MessageQueue::getInstance().getNext(qtConfig, qtMessage, qtBody))
	{
		coco = 1;
		((TextVar *)config)->setValue(qtConfig.toLatin1());
		((TextVar *)message)->setValue(qtMessage.toLatin1());
		((TextVar *)body)->setValue(qtBody.toLatin1());
	}
	return coco;
}



/*
**	FUNCTION
**		Process call to VacSmsConfigPrepareForWrite()
**		PVSS CTRL arguments are:
**			None
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigPrepareForWrite(ExecuteParamRec & /* param */)
{
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	parser.clear();
	return 1;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigAdd()
**		PVSS CTRL arguments are:
**			string configId
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigAdd(ExecuteParamRec &param)
{
	TextVar	configId;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	TEXT_VAR,	&configId,	NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	int coco = parser.addConfig(configId);
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigSetVisibleName()
**		PVSS CTRL arguments are:
**			int configId
**			string name
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processSetVisibleName(ExecuteParamRec &param)
{
	IntegerVar	configId;
	TextVar name;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,	NULL},
		{ "name",		TEXT_VAR,	&name,		NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = parser.setVisibleName(configId.getValue(), name, errMsg);
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigSetMode()
**		PVSS CTRL arguments are:
**			int configId
**			int mode
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigSetMode(ExecuteParamRec &param)
{
	IntegerVar	configId, mode;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,	NULL},
		{ "mode",		INTEGER_VAR,	&mode,		NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = parser.setMode(configId.getValue(), mode.getValue(), errMsg);
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigSetMainType()
**		PVSS CTRL arguments are:
**			int configId
**			int type
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigSetMainType(ExecuteParamRec &param)
{
	IntegerVar	configId, type; 

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,	NULL},
		{ "type",		INTEGER_VAR,	&type,		NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = parser.setMainType(configId.getValue(), type.getValue(), errMsg);
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigSetUsageFilter
**		PVSS CTRL arguments are:
**			string config
**			dyn_int funcTypes
**			dyn_string attrNames
**			dyn_bool reverseFlags
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigSetUsageFilter(ExecuteParamRec &param)
{
	IntegerVar	configId;
	DynVar		funcTypes, attrNames, reverseFlags;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,	NULL},
		{ "funcTypes",		DYNINTEGER_VAR,	&funcTypes,		NULL },
		{ "attrNames",		DYNTEXT_VAR,	&attrNames,		NULL },
		{ "reverseFlags",	DYNBIT_VAR,		&reverseFlags,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	if((funcTypes.getNumberOfItems() != attrNames.getNumberOfItems())
		|| (funcTypes.getNumberOfItems() != reverseFlags.getNumberOfItems()))
	{
		QString errText = "Mistamcthing lists lengths: funcTypes ";
		errText += QString::number(funcTypes.getNumberOfItems());
		errText += ", attrNames ";
		errText += QString::number(attrNames.getNumberOfItems());
		errText += ", reverseFlags ";
		errText += QString::number(reverseFlags.getNumberOfItems());
		showError(param, ErrClass::PARAMETERERROR, errText);
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = 0;
	for(unsigned short idx = 0 ; idx < funcTypes.getNumberOfItems() ; idx++)
	{
		IntegerVar *pFuncType = (IntegerVar *)funcTypes.getAt(idx);
		TextVar *pAttrName = (TextVar *)attrNames.getAt(idx);
		BitVar *pReverse = (BitVar *)reverseFlags.getAt(idx);
		coco = parser.addUsageFilter(configId.getValue(), pFuncType->getValue(), pAttrName->getValue(), pReverse->getValue() == PVSS_TRUE, errMsg);
		if(coco < 0)
		{
			return coco;
		}
	}
	return coco;
}


/*
**	FUNCTION
**		Process call to VacSmsConfigSetScopeType()
**		PVSS CTRL arguments are:
**			int configId
**			int type
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigSetScopeType(ExecuteParamRec &param)
{
	IntegerVar	configId, type; 

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,	NULL},
		{ "type",		INTEGER_VAR,	&type,		NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = parser.setScopeType(configId.getValue(), type.getValue(), errMsg);
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigSetScopeItems()
**		PVSS CTRL arguments are:
**			int configId
**			dyn_string itemNames
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigSetScopeItems(ExecuteParamRec &param)
{
	IntegerVar	configId; 
	DynVar		itemNames;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,	NULL},
		{ "itemNames",	DYNTEXT_VAR,	&itemNames,		NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = 0;
	for(unsigned short idx = 0 ; idx < itemNames.getNumberOfItems() ; idx++)
	{
		TextVar *pName = (TextVar *)itemNames.getAt(idx);
		if(parser.addScopeItem(configId.getValue(), pName->getValue(), errMsg) < 0)
		{
			coco = -1;
			break;
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigSetCritJoinType()
**		PVSS CTRL arguments are:
**			int configId
**			int type
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigSetCritJoinType(ExecuteParamRec &param)
{
	IntegerVar	configId, type; 

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,	NULL},
		{ "type",		INTEGER_VAR,	&type,		NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = parser.setCritJoinType(configId.getValue(), type.getValue(), errMsg);
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigAddCriteria()
**		PVSS CTRL arguments are:
**			int configId
**			int funcType
**			int type
**			int subType
**			bool reverse
**			float lLimit
**			float uLimit
**			int minDuration
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigAddCriteria(ExecuteParamRec &param)
{
	IntegerVar	configId, funcType, type, subType, minDuration; 
	BitVar		reverse;
	FloatVar	lLimit, uLimit;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,		NULL},
		{ "funcType",		INTEGER_VAR,	&funcType,		NULL},
		{ "type",			INTEGER_VAR,	&type,			NULL},
		{ "subType",		INTEGER_VAR,	&subType,		NULL},
		{ "reverse",		BIT_VAR,		&reverse,		NULL},
		{ "lLimit",			FLOAT_VAR,		&lLimit,		NULL},
		{ "uLimit",			FLOAT_VAR,		&uLimit,		NULL},
		{ "minDuration",	INTEGER_VAR,	&minDuration,	NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = parser.addCriteria(configId.getValue(), funcType.getValue(),
		type.getValue(), subType.getValue(), reverse.getValue() == PVSS_TRUE,
		lLimit.getValue(), uLimit.getValue(), minDuration.getValue(), errMsg);
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigSetMessage()
**		PVSS CTRL arguments are:
**			int configId
**			string message
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigSetMessage(ExecuteParamRec &param)
{
	IntegerVar	configId;
	TextVar message;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,	NULL},
		{ "message",	TEXT_VAR,		&message,	NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = parser.setMessage(configId.getValue(), message, errMsg);
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigSetFiltering()
**		PVSS CTRL arguments are:
**			int configId
**			int type
**			int time
**			int groupType
**			int groupTime
**			int indivLimit
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigSetFiltering(ExecuteParamRec &param)
{
	IntegerVar	configId, type, time, groupType, groupTime, indivLimit;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,		NULL},
		{ "type",		INTEGER_VAR,	&type,			NULL},
		{ "time",		INTEGER_VAR,	&time,			NULL},
		{ "groupType",	INTEGER_VAR,	&groupType,		NULL},
		{ "groupTime",	INTEGER_VAR,	&groupTime,		NULL},
		{ "indivLimit",	INTEGER_VAR,	&indivLimit,	NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = parser.setFiltering(configId.getValue(), type.getValue(), time.getValue(),
		groupType.getValue(), groupTime.getValue(), indivLimit.getValue(), errMsg);
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigSetGroupMessage()
**		PVSS CTRL arguments are:
**			int configId
**			string message
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigSetGroupMessage(ExecuteParamRec &param)
{
	IntegerVar	configId;
	TextVar message;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,	NULL},
		{ "message",	TEXT_VAR,		&message,	NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = parser.setGroupMessage(configId.getValue(), message, errMsg);
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigSetRecipients()
**		PVSS CTRL arguments are:
**			int configId
**			dyn_string itemNames
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigSetRecipients(ExecuteParamRec &param)
{
	IntegerVar	configId; 
	DynVar		names;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,	NULL},
		{ "names",		DYNTEXT_VAR,	&names,		NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = 0;
	for(unsigned short idx = 0 ; idx < names.getNumberOfItems() ; idx++)
	{
		TextVar *pName = (TextVar *)names.getAt(idx);
		if(parser.addRecipient(configId.getValue(), pName->getValue(), errMsg) < 0)
		{
			coco = -1;
			break;
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigWrite()
**		PVSS CTRL arguments are:
**			string fileName
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigWrite(ExecuteParamRec &param)
{
	TextVar fileName;
	Variable *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "fileName",		TEXT_VAR,		&fileName,	NULL},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int coco = parser.writeToFile(fileName, errMsg);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigParse()
**		PVSS CTRL arguments are:
**			string fileName
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigParse(ExecuteParamRec &param)
{
	TextVar fileName;
	Variable *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "fileName",		TEXT_VAR,		&fileName,	NULL},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QStringList errList;
	int coco = parser.parseFile(fileName, errList);
	if(!errList.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errList);
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigFirstParsed()
**		PVSS CTRL arguments are:
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigFirstParsed(ExecuteParamRec &param)
{
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	int coco = parser.firstParsedConfig();
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigNextParsed()
**		PVSS CTRL arguments are:
**			int configId
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigNextParsed(ExecuteParamRec &param)
{
	IntegerVar configId;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",	INTEGER_VAR,	&configId,	NULL}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	SmsConfigParser &parser = SmsConfigParser::getInstance();
	int coco = parser.nextParsedConfig(configId.getValue());
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetVisibleName()
**		PVSS CTRL arguments are:
**			int configId
**			string &name
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processGetVisibleName(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *name, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "name",			TEXT_VAR,		NULL,		&name},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	QString visibleName;
	int coco = parser.getVisibleName(configId.getValue(), visibleName, errMsg);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	else
	{
		((TextVar *)name)->setValue(visibleName.isEmpty() ? "" : visibleName.toLatin1());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetMode()
**		PVSS CTRL arguments are:
**			int configId
**			int &mode
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigGetMode(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *mode, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "mode",			INTEGER_VAR,	NULL,		&mode},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	unsigned machineMode;
	int coco = parser.getMode(configId.getValue(), machineMode, errMsg);
	((IntegerVar *)mode)->setValue(machineMode);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetMainType()
**		PVSS CTRL arguments are:
**			int configId
**			int &type
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigGetMainType(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *type, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "type",			INTEGER_VAR,	NULL,		&type},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int mainType = 0;
	int coco = parser.getMainType(configId.getValue(), mainType, errMsg);
	((IntegerVar *)type)->setValue(mainType);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetUsageFilter()
**		PVSS CTRL arguments are:
**			int configId
**			dyn_int &funcTypes
**			dyn_string &attrNames
**			dyn_bool &reverseFlags
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigGetUsageFilter(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *funcTypes, *attrNames, *reverseFlags, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "funcTypes",		DYNINTEGER_VAR,	NULL,		&funcTypes},
		{ "attrNames",		DYNTEXT_VAR,	NULL,		&attrNames},
		{ "reverseFlags",	DYNBIT_VAR,		NULL,		&reverseFlags},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)funcTypes)->clear();
	((DynVar *)attrNames)->clear();
	((DynVar *)reverseFlags)->clear();
	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	QList<int> funcTypesList;
	QList<QByteArray> attrNameList;
	QList<bool> reverseFlagsList;
	int coco = parser.getUsageFilter(configId.getValue(), funcTypesList, attrNameList,
		reverseFlagsList, errMsg);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	else
	{
		QList<int>::iterator intIter;
		for(intIter = funcTypesList.begin() ; intIter != funcTypesList.end() ; ++intIter)
		{
			addToDynIntVar((DynVar *)funcTypes, *intIter);
		}
		foreach(QByteArray name, attrNameList)
		{
			addToDynStringVar((DynVar *)attrNames, name.constData());
		}
		QList<bool>::iterator boolIter;
		for(boolIter = reverseFlagsList.begin() ; boolIter != reverseFlagsList.end() ; ++boolIter)
		{
			addToDynBoolVar((DynVar *)reverseFlags, *boolIter);
		}
	}
	return coco;
}


/*
**	FUNCTION
**		Process call to VacSmsConfigGetScopeType()
**		PVSS CTRL arguments are:
**			int configId
**			int &type
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigGetScopeType(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *type, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "type",			INTEGER_VAR,	NULL,		&type},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int scopeType = 0;
	int coco = parser.getScopeType(configId.getValue(), scopeType, errMsg);
	((IntegerVar *)type)->setValue(scopeType);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetScopeItems()
**		PVSS CTRL arguments are:
**			int configId
**			dyn_string &items
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigGetScopeItems(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *items, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "items",			DYNTEXT_VAR,	NULL,		&items},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)items)->clear();
	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	QStringList names;
	int coco = parser.getScopeItems(configId.getValue(), names, errMsg);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	else
	{
		foreach(QString name, names)
		{
			addToDynStringVar(((DynVar *)items), name.toLatin1());
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetCritJoinType()
**		PVSS CTRL arguments are:
**			int configId
**			int &type
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigGetCritJoinType(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *type, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "type",			INTEGER_VAR,	NULL,		&type},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int scopeType = 0;
	int coco = parser.getCritJoinType(configId.getValue(), scopeType, errMsg);
	((IntegerVar *)type)->setValue(scopeType);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetCriteria()
**		PVSS CTRL arguments are:
**			int configId
**			dyn_int &funcTypes
**			dyn_int &types
**			dyn_int &subTypes
**			dyn_bool &reverseFlags
**			dyn_float &lLimits
**			dyn_float &uLimits
**			dyn_int &minDurations
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigGetCriteria(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *funcTypes, *types, *subTypes, *reverseFlags, *lLimits,
		*uLimits, *minDurations, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "funcTypes",		DYNINTEGER_VAR,	NULL,		&funcTypes},
		{ "types",			DYNINTEGER_VAR,	NULL,		&types},
		{ "subTypes",		DYNINTEGER_VAR,	NULL,		&subTypes},
		{ "reverseFlags",	DYNBIT_VAR,		NULL,		&reverseFlags},
		{ "lLimits",		DYNFLOAT_VAR,	NULL,		&lLimits},
		{ "uLimits",		DYNFLOAT_VAR,	NULL,		&uLimits},
		{ "minDurations",	DYNINTEGER_VAR,	NULL,		&minDurations},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)funcTypes)->clear();
	((DynVar *)types)->clear();
	((DynVar *)subTypes)->clear();
	((DynVar *)reverseFlags)->clear();
	((DynVar *)lLimits)->clear();
	((DynVar *)uLimits)->clear();
	((DynVar *)minDurations)->clear();
	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	QList<int> funcTypesList, typesList, subTypesList, minDurationsList;
	QList<bool> reverseFlagsList;
	QList<float> lLimitsList, uLimitsList;
	int coco = parser.getCriteria(configId.getValue(), funcTypesList, typesList,
		subTypesList, reverseFlagsList, lLimitsList, uLimitsList,
		minDurationsList, errMsg);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	else
	{
		QList<int>::iterator intIter;
		for(intIter = funcTypesList.begin() ; intIter != funcTypesList.end() ; ++intIter)
		{
			addToDynIntVar((DynVar *)funcTypes, *intIter);
		}
		for(intIter = typesList.begin() ; intIter != typesList.end() ; ++intIter)
		{
			addToDynIntVar((DynVar *)types, *intIter);
		}
		for(intIter = subTypesList.begin() ; intIter != subTypesList.end() ; ++intIter)
		{
			addToDynIntVar((DynVar *)subTypes, *intIter);
		}
		for(intIter = minDurationsList.begin() ; intIter != minDurationsList.end() ; ++intIter)
		{
			addToDynIntVar((DynVar *)minDurations, *intIter);
		}
		QList<bool>::iterator boolIter;
		for(boolIter = reverseFlagsList.begin() ; boolIter != reverseFlagsList.end() ; ++boolIter)
		{
			addToDynBoolVar((DynVar *)reverseFlags, *boolIter);
		}
		QList<float>::iterator floatIter;
		for(floatIter = lLimitsList.begin() ; floatIter != lLimitsList.end() ; ++floatIter)
		{
			addToDynFloatVar((DynVar *)lLimits, *floatIter);
		}
		for(floatIter = uLimitsList.begin() ; floatIter != uLimitsList.end() ; ++floatIter)
		{
			addToDynFloatVar((DynVar *)uLimits, *floatIter);
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetMessage()
**		PVSS CTRL arguments are:
**			int configId
**			string &message
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigGetMessage(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *message, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "message",		TEXT_VAR,		NULL,		&message},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	((TextVar *)message)->setValue("");
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	QString msg;
	int coco = parser.getMessage(configId.getValue(), msg, errMsg);
	((TextVar *)message)->setValue(msg.toLatin1());
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetFiltering()
**		PVSS CTRL arguments are:
**			int configId
**			int &type
**			int &time
**			int &groupType
**			int &groupTime
**			int &indivLimit
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigGetFiltering(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *type, *time, *groupType, *groupTime, *indivLimit, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "type",			INTEGER_VAR,	NULL,		&type},
		{ "time",			INTEGER_VAR,	NULL,		&time},
		{ "groupType",		INTEGER_VAR,	NULL,		&groupType},
		{ "groupTime",		INTEGER_VAR,	NULL,		&groupTime},
		{ "indivLimit",		INTEGER_VAR,	NULL,		&indivLimit},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	int typeValue, timeValue, groupTypeValue, groupTimeValue, indivLimitValue;
	int coco = parser.getFiltering(configId.getValue(), typeValue, timeValue,
		groupTypeValue, groupTimeValue, indivLimitValue, errMsg);
	((IntegerVar *)type)->setValue(typeValue);
	((IntegerVar *)time)->setValue(timeValue);
	((IntegerVar *)groupType)->setValue(groupTypeValue);
	((IntegerVar *)groupTime)->setValue(groupTimeValue);
	((IntegerVar *)indivLimit)->setValue(indivLimitValue);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetGroupMessage()
**		PVSS CTRL arguments are:
**			int configId
**			string &message
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigGetGroupMessage(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *message, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "message",		TEXT_VAR,		NULL,		&message},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	((TextVar *)message)->setValue("");
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	QString msg;
	int coco = parser.getGroupMessage(configId.getValue(), msg, errMsg);
	((TextVar *)message)->setValue(msg.toLatin1());
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetREcipients()
**		PVSS CTRL arguments are:
**			int configId
**			dyn_string &items
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processConfigGetRecipients(ExecuteParamRec &param)
{
	IntegerVar configId;
	Variable *items, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "configId",		INTEGER_VAR,	&configId,	NULL},
		{ "items",			DYNTEXT_VAR,	NULL,		&items},
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)items)->clear();
	((DynVar *)exceptionInfo)->clear();
	SmsConfigParser &parser = SmsConfigParser::getInstance();
	QString errMsg;
	QStringList names;
	int coco = parser.getRecipients(configId.getValue(), names, errMsg);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg.toLatin1());
	}
	else
	{
		foreach(QString name, names)
		{
			addToDynStringVar(((DynVar *)items), name.toLatin1());
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacSmsConfigGetFuncTypeUsage()
**		PVSS CTRL arguments are:
**			int funcTypeId
**			dyn_int &typeIdsUsingThis
**			dyn_string &typesNamesUsingThis
**			dyn_string &attrNames
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlOnlineNotificationPvss::processGetFuncTypeUsage(ExecuteParamRec &param)
{
	IntegerVar funcTypeId;
	Variable *typeIdsUsingThis, *typeNamesUsingThis, *attrNames;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "funcTypeId",				INTEGER_VAR,	&funcTypeId,	NULL},
		{ "typesIdsUsingThis",		DYNINTEGER_VAR,	NULL,			&typeIdsUsingThis},
		{ "typesNamesUsingThis",	DYNTEXT_VAR,	NULL,			&typeNamesUsingThis},
		{ "attrNames",				DYNTEXT_VAR,	NULL,			&attrNames}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)typeIdsUsingThis)->clear();
	((DynVar *)typeNamesUsingThis)->clear();
	((DynVar *)attrNames)->clear();

	// There is at least one case where selecting one funcTypeId implicitely assumes selecting
	// also another one. So, let's use list
	QList<int> funcTypeList;
	buildMainFuncTypeList(funcTypeId.getValue(), funcTypeList);

	QList<int> typeIdList;
	QStringList attrNameList;
	DataPool &pool = DataPool::getInstance();
	const QHash<QByteArray, Eqp *> &allEqp = pool.getEqpDict();
	foreach(Eqp *pEqp, allEqp)
	{
		if(!pEqp->getDpType())
		{
			continue;
		}
		int funcType = pEqp->getFunctionalType();
		QHashIterator<QByteArray, QString> attrIter(pEqp->getAttrList());
		while(attrIter.hasNext())
		{
			attrIter.next();
			Eqp *pOtherEqp = pool.findEqpByDpName(attrIter.value().toLatin1().constData());
			if(pOtherEqp)
			{
				if(funcTypeList.contains(pOtherEqp->getFunctionalType()))
				{
					bool alreadyInList = false;
					for(int idx = typeIdList.count() - 1 ; idx >= 0 ; idx--)
					{
						if((typeIdList.at(idx) == funcType) && (attrNameList.at(idx) == attrIter.key()))
						{
							alreadyInList = true;
							break;
						}
					}
					if(!alreadyInList)
					{
						typeIdList.append(funcType);
						attrNameList.append(attrIter.key());
					}
				}
			}
		}
	}

	for(int idx = 0 ; idx < typeIdList.count() ; idx++)
	{
		int typeId = typeIdList.at(idx);
		FunctionalType *pFuncType = FunctionalType::findTypeData(typeId);
		if(pFuncType)
		{
			IntegerVar id;
			id.setValue(typeId);
			((DynVar *)typeIdsUsingThis)->append(id);
			TextVar name;
			name.setValue(pFuncType->getDescription());
			((DynVar *)typeNamesUsingThis)->append(name);
			TextVar attrName;
			attrName.setValue(attrNameList.at(idx).toLatin1().constData());
			((DynVar *)attrNames)->append(attrName);
		}
	}
	return typeIdList.count();
}

void VacCtlOnlineNotificationPvss::buildMainFuncTypeList(int funcTypeId, QList<int> &funcTypeList)
{
	funcTypeList.append(funcTypeId);
	switch(funcTypeId)
	{
	case FunctionalType::VPI:
		funcTypeList.append(FunctionalType::VRPI);
		break;
	default:
		break;
	}
}
