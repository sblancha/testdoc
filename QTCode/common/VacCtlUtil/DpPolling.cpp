//	Implementation of DpPolling class
//////////////////////////////////////////////////////////////////////////

#include "DpPolling.h"

DpPolling	DpPolling::instance;

DpPolling &DpPolling::getInstance(void) { return instance; }

/*
**	FUNCTION
**		Add DPE to list of pending connections if it is not there yet.
**
**	ARGUMENTS
**		dp			- DP name
**		dpe			- DPE name within DP, can be NULL if DP has no DPEs
**
**	RETURNS
**		true	- if DPE has been added to queue,
**		false	- DPE was not added to queue
**
**	CAUTIONS
**		None
*/
bool DpPolling::connect(const char *dp, const char *dpe)
{
	QByteArray dpeName(dp);
	if(dpe)
	{
		dpeName += ".";
		dpeName += dpe;
	}
	return getInstance().connect(dpeName);
}

/*
**	FUNCTION
**		Add DPE to list of pending disconnections if it is not there yet.
**
**	ARGUMENTS
**		dp			- DP name
**		dpe			- DPE name within DP, can be NULL if DP has no DPEs
**
**	RETURNS
**		true	- if DPE has been added to queue,
**		false	- DPE was not added to queue
**
**	CAUTIONS
**		None
*/
bool DpPolling::disconnect(const char *dp, const char *dpe)
{
	QByteArray dpeName(dp);
	if(dpe)
	{
		dpeName += ".";
		dpeName += dpe;
	}
	return getInstance().disconnect(dpeName);
}

//////////////////////////////////////////////////////////////////////////////////
////////////////////// Construction/destruction

DpPolling::DpPolling()
{
	pDpes = new QList<DpPollingItem *>();
	signalSent = false;
}
DpPolling::DpPolling(const DpPolling &source) : QObject()
{
	pDpes = new QList<DpPollingItem *>(*(source.pDpes));
	signalSent = false;
}

DpPolling::~DpPolling()
{
	while(!pDpes->isEmpty())
	{
		delete pDpes->takeFirst();
	}
	delete pDpes;
}

DpPolling & DpPolling::operator = (const DpPolling &other)
{
	*pDpes = *(other.pDpes);
	signalSent = false;
	return *this;
}

/*
**	FUNCTION
**		Add DPE to list of pending connections if it is not there yet.
**
**	ARGUMENTS
**		dpe			- DPE name = DP.DPE
**
**	RETURNS
**		true	- if DPE has been added to queue,
**		false	- DPE was not added to queue
**
**	CAUTIONS
**		None
*/
bool DpPolling::connect(const QByteArray &dpe)
{
	mutex.lock();
	bool result = true;
	/* Check may take too long on long lists 14.07.2009
	for(DpPollingItem *pItem = pDpes->first() ; pItem ; pItem = pDpes->next())
	{
		if(pItem->getDpe() == dpe)
		{
			if(!pItem->isToConnect())
			{
				pDpes->remove(pItem);
			}
			result = false;
			break;
		}
	}
	*/
	if(result)
	{
		pDpes->append(new DpPollingItem(dpe, true));
	}
	mutex.unlock();
	if(result)
	{
		if(!signalSent)
		{
			emit changed();
			signalSent = true;
		}
	}
	return result;
}

/*
**	FUNCTION
**		Add DPE to list of pending disconnections if it is not there yet.
**
**	ARGUMENTS
**		dpe			- DPE name = DP.DPE
**
**	RETURNS
**		true	- if DPE has been added to queue,
**		false	- DPE was not added to queue
**
**	CAUTIONS
**		None
*/
bool DpPolling::disconnect(const QByteArray &dpe)
{
	mutex.lock();
	bool result = true;
	/* Check may take too long on long lists 14.07.2009
	for(DpPollingItem *pItem = pDpes->first() ; pItem ; pItem = pDpes->next())
	{
		if(pItem->getDpe() == dpe)
		{
			if(pItem->isToConnect())
			{
				pDpes->remove(pItem);
			}
			result = false;
			break;
		}
	}
	*/
	if(result)
	{
		pDpes->append(new DpPollingItem(dpe, false));
	}
	mutex.unlock();
	if(result)
	{
		if(!signalSent)
		{
			emit changed();
			signalSent = true;
		}
	}	
	return result;
}

/*
**	FUNCTION
**		Get next DPE to be connected or disconnected
**
**	ARGUMENTS
**		dpe			- variable where DPE name will be written
**		connect		- Variable where connect/disconnect flag will be written
**
**	RETURNS
**		true	- if queue is not empty;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool DpPolling::getNext(QByteArray &dpe, bool &connect)
{
	mutex.lock();
	bool result;
	if(pDpes->isEmpty())
	{
		result = false;
		signalSent = false;
	}
	else
	{
		result = true;
		DpPollingItem *pItem = pDpes->takeFirst();
		dpe = pItem->getDpe();
		connect = pItem->isToConnect();
		delete pItem;
	}
	mutex.unlock();
	return result;
}

