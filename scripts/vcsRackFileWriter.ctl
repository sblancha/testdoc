

main()
{
	int result;
	string url = "http://vacmonscadai:8081/api/equipmentRackInfo?versionId=-57725";
	mapping mapResult;
	result = netGet(url, mapResult);
	dyn_anytype jSonResult = jsonDecode(mapResult["content"]);
	mapping equipmentRackInfo;
	string equipmentName, equipmentType, rack, posX, posY, height, width;
	//dyn_anytype topObjects = mappingKeys(jSonResult);
	
	file writeFile = fopen(getPath(DATA_REL_PATH) + "LHC.for_Racks", "w");
	int err = ferror(writeFile); //Output possible errors
   if ( err )
   {
     DebugN("Error no. ",err," occurred while opening file");
     return;
   }
	
	
	for(int i = 1; i <= dynlen(jSonResult); i++)
	{
		//get data
		rack = posX = posY = height = width = "";
		equipmentRackInfo = jSonResult[i]["equipmentRackInfo"];
		equipmentName = equipmentRackInfo["eqpName"];
		equipmentType = equipmentRackInfo["eqpType"];
		rack = equipmentRackInfo["rack"];
		posX = equipmentRackInfo["positionX"];
		posY = equipmentRackInfo["positionY"];
		height = equipmentRackInfo["height"];
		width = equipmentRackInfo["width"];
		
		
		fputs("#================ "+ equipmentName+" =====================\n",writeFile);
		fputs("EqpName=" + equipmentName + "\n", writeFile);
		fputs("EqpType=" + equipmentType + "\n",writeFile);
		fputs("Rack=" + rack + "\n", writeFile);
		fputs("PositionY=" + posY + "\n", writeFile);
		fputs("PositionX=" + posX + "\n", writeFile);
		fputs("SizeY=" + height + "\n", writeFile);
		fputs("SizeX=" + width + "\n", writeFile);
		fputs("\n", writeFile);
		
		dyn_anytype attrList = jSonResult[i]["detailList"];
		
		for(int j = 1; j<= dynlen(attrList);j++)
		{
			fputs(attrList[j]["detailName"] + "=" + attrList[j]["detailValue"] + "\n", writeFile);
		}
			
		
		
		fputs("\n", writeFile);
		fputs("EndEqp\n", writeFile);
		fputs("\n", writeFile);
		
		
	}
	
	fclose(writeFile);
	DebugTN(getTypeName(jSonResult[1]["equipmentRackInfo"]["height"]));
	
}
