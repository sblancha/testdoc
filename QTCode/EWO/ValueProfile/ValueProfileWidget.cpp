//	Implementation of ValueProfileWidget class
/////////////////////////////////////////////////////////////////////////////////

#include "ValueProfileWidget.h"

#include <QPainter>
#include <QPixmap>
#include <QMouseEvent>
#include <QApplication>

#include <math.h>


ValueProfileWidget::ValueProfileWidget(QWidget *parent, Qt::WindowFlags f)
	: QWidget(parent, f)
{
	setAutoFillBackground(true);
	setMouseTracking(true);
	highlightedBarDrawn = false;
	mouseX = -1;
	newDataArrived = false;
	marqueeWidth = 0;
	draggingMarquee = false;
	lastMarqueeStart = lastMarqueeEnd = -1;

	// Some default values to be shown at design time
	valueType = TypeFloat;
	min = max = 0;
	for(float arg = 0 ; arg < 10 ; arg += (float)0.01)
	{
		float value = (float)(1.E5 * (sin(arg) + 0.9));
		floatList.append(value);
		if(value > max)
		{
			max = value;
		}
		else if(value < min)
		{
			min = value;
		}
	}
	makeImage();
}

ValueProfileWidget::~ValueProfileWidget()
{
}

void ValueProfileWidget::setAxisColor(const QColor &color)
{
	axisColor = color;
	makeImage();
}

void ValueProfileWidget::setValuesColor(const QColor &color)
{
	valuesColor = color;
	makeImage();
}

void ValueProfileWidget::setBarHighlightColor(const QColor &color)
{
	if(highlightedBarDrawn)
	{
		update();
	}
	barHighlightColor = color;
	if(highlightedBarDrawn)
	{
		update();
	}
}

void ValueProfileWidget::setMarqueeWidth(int newWidth)
{
	if(marqueeWidth)
	{
		update();
	}
	marqueeWidth = newWidth < 0 ? 0 : newWidth;
	if(marqueeWidth)
	{
		marquee.setWidth(marqueeWidth);
		adjustMarqueeRect(marquee);
		marqueeWidth = marquee.width();
		update();
	}
}

void ValueProfileWidget::setMarqueeColor(const QColor &color)
{
	if(marqueeWidth)
	{
		update();
	}
	marqueeColor = color;
	if(marqueeWidth)
	{
		update();
	}
}

int ValueProfileWidget::setFloatValues(const QVariant &valueList, QString &errMsg)
{
	errMsg = "";
	if(!valueList.canConvert(QVariant::List))
	{
		errMsg = "setFloatValues(): argument is not a list";
		valueType = TypeNone;
		return -1;
	}
	valueType = TypeFloat;
	QList<QVariant>  varList = valueList.toList();
	floatList.clear();

	min = max = 0;
	for(int idx = 0 ; idx < varList.size() ; idx++)
	{
		float value = 0;
		if(!varList[idx].canConvert(QVariant::Double))
		{
			errMsg = "setFloatValues(): item ";
			errMsg = QString::number(idx + 1);
			errMsg += "in a list is not a float value";
		}
		else
		{
			value = (float)varList[idx].toDouble();
		}
/*
printf("ValueProfileWidget::setFloatValues(): next value %g\n", value);
fflush(stdout);
*/
		floatList.append(value);
		if(value > max)
		{
			max = value;
		}
		else if(value < min)
		{
			min = value;
		}
	}
	newDataArrived = true;
	makeImage();
	return floatList.count();
}

QStringList &ValueProfileWidget::getValuesInMarquee(void)
{
	valuesInMarquee.clear();
	if(marqueeWidth && floatList.count())
	{
		int start = lastMarqueeStart;
		if(start < 0)
		{
			return valuesInMarquee;
		}
		if(start >= (int)floatList.count())
		{
			start = floatList.count() - 1;
		}
		int end = lastMarqueeEnd;
		if(end >= (int)floatList.count())
		{
			end = floatList.count() - 1;
		}
		for(int n = start ; n <= end ; n++)
		{
			valuesInMarquee.append(QString::number((double)floatList[n]));
		}
	}
	return valuesInMarquee;
}

void ValueProfileWidget::resizeEvent(QResizeEvent * /* pEvent */)
{
	marquee.setTop(0);
	marquee.setHeight(height() - 1);
	makeImage();
}

void ValueProfileWidget::paintEvent(QPaintEvent * /* pEvent */)
{
	if(highlightedBarDrawn)
	{
		drawHighlightedBar();
	}
	drawMarquee();
}

void ValueProfileWidget::mousePressEvent(QMouseEvent *pEvent)
{
	if(pEvent->button() != Qt::LeftButton)
	{
		return;
	}
	if(draggingMarquee)
	{
		return;
	}
	if(!marqueeWidth)
	{
		return;
	}
	if(!marquee.contains(pEvent->x(), pEvent->y(), true))
	{
		return;
	}
	draggingMarquee = true;
}

void ValueProfileWidget::mouseReleaseEvent(QMouseEvent *pEvent)
{
	if(pEvent->button() != Qt::LeftButton)
	{
		return;
	}
	if(draggingMarquee)
	{
	}
	draggingMarquee = false;
	signalMarquueMove();
}

void ValueProfileWidget::mouseMoveEvent(QMouseEvent *pEvent)
{
	if(draggingMarquee)
	{
		moveMarquee(pEvent->x());
	}
	int newBarIndex = -1;
	float value = 0;
	mouseX = pEvent->x();
	QRect newBar;
	if(mouseX >= horOffset)
	{
		if(horCoeff > 0)
		{
			newBarIndex = barAtPosition(mouseX);
			if((0 <= newBarIndex) && (newBarIndex < (int)floatList.count()))
			{
				value = floatList[newBarIndex];
				newBar.setX(horOffset + (int)rint(horCoeff * (double)newBarIndex));
				newBar.setWidth(barWidth > 1 ? barWidth : 1);
				int y = zeroLevel - (int)rint(value * vertCoeff);
				if(value < 0)
				{
					newBar.setY(zeroLevel);
					newBar.setHeight(y - zeroLevel);
				}
				else
				{
					newBar.setY(y);
					newBar.setHeight(zeroLevel - y);
				}
			}
			else
			{
				newBarIndex = -1;
			}
		}
	}
	if((newBarIndex == barIndex) && (!newDataArrived))
	{
		return;
	}
	if(highlightedBarDrawn)
	{
		update();
	}
	barIndex = newBarIndex;
	highlightedBar = newBar;
	emit barHighlight(barIndex + 1, value);
	if(barIndex >= 0)
	{
		highlightedBarDrawn = true;
		update();
	}
	else
	{
		highlightedBarDrawn = false;
	}
	update();
}


void ValueProfileWidget::makeImage(void)
{
	QPixmap bgPixmap(width(), height());
	QPalette pal = QApplication::palette();
	bgPixmap.fill(pal.color(QPalette::Window));
	QPainter painter(&bgPixmap);
	draw(painter);
	pal.setBrush(QPalette::Window, bgPixmap);
	setPalette(pal);
	QMouseEvent event(QEvent::MouseMove, QPoint(mouseX, 0), Qt::NoButton, Qt::NoButton, Qt::NoModifier);
	mouseMoveEvent(&event);
	if(marqueeWidth)
	{
		adjustMarqueeRect(marquee);
		update();
		signalMarquueMove();
	}
	newDataArrived = false;
}

void ValueProfileWidget::draw(QPainter &painter)
{
	if((!width()) || (!height()))
	{
		return;
	}
	switch(valueType)
	{
	case TypeFloat:
		drawFloat(painter);
		break;
	default:
		break;
	}
}

void ValueProfileWidget::drawFloat(QPainter &painter)
{
	if(floatList.isEmpty())
	{
		return;
	}
	if(height() == 0)
	{
		return;
	}

	// Space for maximum label
	char maxLabel[32];
#ifdef Q_OS_WIN
	sprintf_s(maxLabel, sizeof(maxLabel) / sizeof(maxLabel[0]), "%7.1E", max);
#else
	sprintf(maxLabel, "%7.1E", max);
#endif
	QRect maxRect = painter.fontMetrics().boundingRect(maxLabel);

	int maxLevel = maxRect.height() >> 1;

	const char *zeroLabel = "0";
	QRect zeroRect = painter.fontMetrics().boundingRect(zeroLabel);

	if((maxRect.height() + zeroRect.height()) >= height())	// Draw without labels
	{
		maxLevel = 0;
	}
	if(maxRect.width() >= (width() - 10))
	{
		maxLevel = 0;
	}

	horCoeff = 0;
	if(width() != 0)
	{
		if(maxLevel > 0)
		{
			horCoeff = (double)(width() - 2 - maxRect.width()) / (double)floatList.count();
		}
		else
		{
			horCoeff = (double)(width() - 2) / (double)floatList.count();
		}
		if(horCoeff < 0)
		{
			horCoeff = 0;
		}
	}
	barWidth = (int)rint(horCoeff);
	zeroLevel = height() - 1;
	int bottom = zeroLevel;
	horOffset = 0;
	vertCoeff = 0;
	if((max - min) > 0)
	{
		if((height() != 0) && ((max - min) > 0))
		{
			vertCoeff = (double)(height() - 1 - maxLevel) / (max - min);
			if(vertCoeff < 0)
			{
				vertCoeff = 0;
			}
			else
			{
				zeroLevel = bottom - (int)rint(-min * vertCoeff);
				if(zeroLevel > (bottom - (zeroRect.height() >> 1)))
				{
					zeroLevel = bottom - (zeroRect.height() >> 1);
				}
			}
		}
		painter.setPen(valuesColor);
		QBrush brush(valuesColor);
		if(maxLevel > 0)
		{
			horOffset = maxRect.width() + 2;
		}
		for(int row = 0 ; row < floatList.count() ; row++)
		{
			int x = horOffset + (int)rint(horCoeff * (double)row);
			float value = floatList.at(row);
			int y = zeroLevel - (int)rint(value * vertCoeff);
			if(barWidth < 2)
			{
				painter.drawLine(x, zeroLevel, x, y);
			}
			else
			{
				if(value < 0)
				{
					painter.fillRect(x, zeroLevel, barWidth, (y - zeroLevel), brush);
				}
				else
				{
					painter.fillRect(x, y, barWidth, (zeroLevel - y), brush);
				}
			}
			/*
			printf("x %d y %d\n", x, y);
			fflush(stdout);
			*/
		}
	}

	// Labels
	painter.setPen(axisColor);
	if(maxLevel > 0)
	{
		painter.drawText(0, maxLevel + (maxRect.height() >> 1) + 1, maxLabel);
		int zeroStart = 0;
		if(horOffset > zeroRect.width())
		{
			zeroStart = horOffset - zeroRect.width() - 2;
		}
		painter.drawText(zeroStart, zeroLevel + (zeroRect.height() >> 1), zeroLabel);
		painter.drawLine(horOffset - 1, 0, horOffset - 1, height());
		painter.drawLine(horOffset - 2, maxLevel, horOffset, maxLevel);
		painter.drawLine(horOffset - 2, zeroLevel, horOffset, zeroLevel);
	}
	painter.drawLine(horOffset, zeroLevel, width(), zeroLevel);
}

void ValueProfileWidget::drawHighlightedBar(void)
{
	QPainter painter(this);
	painter.setCompositionMode(QPainter::RasterOp_SourceXorDestination);
	painter.fillRect(highlightedBar, barHighlightColor);
}

void ValueProfileWidget::drawMarquee(void)
{
	if(!marqueeWidth)
	{
		return;
	}
	QPainter painter(this);
	painter.setCompositionMode(QPainter::RasterOp_SourceXorDestination);
	painter.setPen(marqueeColor);
	painter.drawRect(marquee);
}

void ValueProfileWidget::moveMarquee(int newX)
{
	int delta = newX - mouseX;
	if(!delta)
	{
		return;
	}
	QRect newRect(marquee);
	newRect.translate(delta, 0);
	adjustMarqueeRect(newRect);
	if(marquee == newRect)
	{
		return;
	}
	update();
	marquee = newRect;
	update();
	signalMarquueMove();
}

void ValueProfileWidget::adjustMarqueeRect(QRect &rect)
{
	if(rect.width() > (width() - horOffset))
	{
		rect.setWidth(width() - horOffset);
	}
	if(rect.x() < horOffset)
	{
		rect.moveLeft(horOffset);
	}
	if(rect.right() > width())
	{
		rect.moveRight(width());
	}
}

void ValueProfileWidget::signalMarquueMove(void)
{
	if(!marqueeWidth)
	{
		return;
	}
	int start = barAtPosition(marquee.left());
	if(start < 0)
	{
		start = 0;
	}
	if((start == lastMarqueeStart) && (!newDataArrived))
	{
		return;
	}
	lastMarqueeStart = start;
	int end = barAtPosition(marquee.right());
	if(end >= (int)floatList.count())
	{
		end = floatList.count() - 1;
	}
	lastMarqueeEnd = end;
	newDataArrived = false;
	emit marqueeMove(start, end);
}

int ValueProfileWidget::barAtPosition(int x)
{
	return (int)rint((x - horOffset - (barWidth >> 1)) / horCoeff);
}


