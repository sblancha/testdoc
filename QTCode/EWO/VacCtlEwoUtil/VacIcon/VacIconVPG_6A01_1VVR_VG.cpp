//	Implementation of VacIconVPG_6A01_1VVR_VG class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPG_6A01_1VVR_VG.h"
#include "VacIconVPG_6A01.h"

#include "EqpProcess.h"

#include "DataPool.h"

#include <qpainter.h>
#include <QMouseEvent>


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
VacIconVPG_6A01_1VVR_VG::VacIconVPG_6A01_1VVR_VG(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	pIconVPG = pIconVVR = pIconVGR = pIconVGP = pIconVGF = NULL;
	setFixedSize(47, 65);
	setAreaGeometry();
}

VacIconVPG_6A01_1VVR_VG::~VacIconVPG_6A01_1VVR_VG()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for symbol areas rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_VG::setAreaGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		vvr.setRect(13, 2, 27, 21);
		vgr.setRect(0, 23, 21, 21);
		vgp.setRect(26, 23, 21, 21);
		pump.setRect(13, 44, 21, 21);
		break;
	case VacIconContainer::Down:
		//vvr.setRect(7, 42, 27, 21);
		vvr.setRect(13, 42, 27, 21);
		vgr.setRect(0, 21, 21, 21);
		vgp.setRect(26, 21, 21, 21);
		pump.setRect(13, 0, 21, 21);
		break;
	case VacIconContainer::Left:
		vvr.setRect(42, 13, 21, 27);
		vgr.setRect(21, 0, 21, 21);
		vgp.setRect(21, 26, 21, 21);
		pump.setRect(0, 13, 21, 21);
		break;
	case VacIconContainer::Right:
		vvr.setRect(2, 8, 21, 27);
		vgr.setRect(23, 0, 21, 21);
		vgp.setRect(23, 26, 21, 21);
		pump.setRect(44, 13, 21, 21);
		break;
	}
	if(pIconVVR)
	{
		pIconVVR->move(vvr.left(), vvr.top());
	}
	if(pIconVPG)
	{
		pIconVPG->move(pump.left(), pump.top());
	}
	if(pIconVGR)
	{
		pIconVGR->move(vgr.left(), vgr.top());
	}
	if(pIconVGP)
	{
		pIconVGP->move(vgp.left(), vgp.top());
	}
	if(pIconVGF)
	{
		switch(direction)
		{
		case VacIconContainer::Up:
		case VacIconContainer::Down:
			pIconVGF->move(vgr.left() + 3, vgr.top());
			break;
		case VacIconContainer::Left:
		case VacIconContainer::Right:
			pIconVGF->move(vgr.left(), vgr.top() + 3);
			break;
		}
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPG_6A01_1VVR_VG::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		return VacIconGeometry(47, 65, 23, 66, -1);
	case VacIconContainer::Down:
		return VacIconGeometry(47, 65, 23, 66, -1);
	default:
		break;
	}
	return VacIconGeometry(65, 47, 65, 23, -1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_VG::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(47);
		size.setHeight(65);
		break;
	default:
		size.setWidth(65);
		size.setHeight(47);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreaGeometry();
	if(pIconVPG)
	{
		pIconVPG->setDirection(newDirection);
	}
	if(pIconVVR)
	{
		pIconVVR->setDirection(calculateVvrDirection());
	}
	if(pIconVGR)
	{
		pIconVGR->setDirection(calculateVgrDirection());
	}
	if(pIconVGP)
	{
		pIconVGP->setDirection(calculateVgpDirection());
	}
	if(pIconVGF)
	{
		pIconVGF->setDirection(calculateVgrDirection());
	}
}

VacIconContainer::Direction VacIconVPG_6A01_1VVR_VG::calculateVvrDirection(void)
{
	VacIconContainer::Direction result = direction;
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		result = VacIconContainer::Left;
		break;
	default:
		result = VacIconContainer::Up;
		break;
	}
	return result;
}

VacIconContainer::Direction VacIconVPG_6A01_1VVR_VG::calculateVgrDirection(void)
{
	VacIconContainer::Direction result = direction;
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		result = VacIconContainer::Right;
		break;
	default:
		result = VacIconContainer::Up;
		break;
	}
	return result;
}

VacIconContainer::Direction VacIconVPG_6A01_1VVR_VG::calculateVgpDirection(void)
{
	VacIconContainer::Direction result = direction;
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		result = VacIconContainer::Left;
		break;
	default:
		result = VacIconContainer::Up;
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_VG::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pIconVPG)
	{
		delete pIconVPG;
		pIconVPG = NULL;
	}
	if(pIconVVR)
	{
		delete pIconVVR;
		pIconVVR = NULL;
	}
	if(pIconVGR)
	{
		delete pIconVGR;
		pIconVGR = NULL;
	}
	if(pIconVGP)
	{
		delete pIconVGP;
		pIconVGP = NULL;
	}
	if(pIconVGF)
	{
		delete pIconVGF;
		pIconVGF = NULL;
	}
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpProcess"), "VacIconVPG_6A01_1VVR_VG::setEqp", pEqp->getDpName());
		this->pEqp = (EqpProcess *)pEqp;
		pIconVPG = new VacIconVPG_6A01(this);
		pIconVPG->setEqp(pEqp);
		pIconVPG->move(pump.left(), pump.top());
		pIconVPG->setMode(mode);
		pIconVPG->setPipeColor(pipeColor);
		pIconVPG->setDirection(direction);
		pIconVPG->setDrawConnectionPipe(false);
		if(connected)
		{
			pIconVPG->connect();
		}

		QString attrName = pEqp->getAttrValue("VVR1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVVR = VacIcon::getIcon(pChild, this);
			}
		}
		if(!pIconVVR)
		{
			attrName = pEqp->getAttrValue("VVR2");
			if(!attrName.isEmpty())
			{
				Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
				if(pChild)
				{
					pIconVVR = VacIcon::getIcon(pChild, this);
				}
			}
		}
		if(pIconVVR)
		{
			pIconVVR->move(vvr.left(), vvr.top());
			pIconVVR->setMode(mode);
			pIconVVR->setPipeColor(pipeColor);
			pIconVVR->setDirection(calculateVvrDirection());
			if(connected)
			{
				pIconVVR->connect();
			}
		}

		attrName = pEqp->getAttrValue("VGR1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGR = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGR)
		{
			pIconVGR->move(vgr.left(), vgr.top());
			pIconVGR->setMode(mode);
			pIconVGR->setPipeColor(pipeColor);
			pIconVGR->setDirection(calculateVgrDirection());
			if(connected)
			{
				pIconVGR->connect();
			}
		}

		attrName = pEqp->getAttrValue("VGP1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGP = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGP)
		{
			pIconVGP->move(vgp.left(), vgp.top());
			pIconVGP->setMode(mode);
			pIconVGP->setPipeColor(pipeColor);
			pIconVGP->setDirection(calculateVgpDirection());
			if(connected)
			{
				pIconVGP->connect();
			}
		}

		attrName = pEqp->getAttrValue("VGF");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGF = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGF)
		{
			pIconVGF->setDrawConnectionPipe(false);
			switch(direction)
			{
			case VacIconContainer::Up:
			case VacIconContainer::Down:
				pIconVGF->move(vgr.left() + 5, vgr.top());
				break;
			case VacIconContainer::Left:
			case VacIconContainer::Right:
				pIconVGF->move(vgr.left(), vgr.top() + 5);
				break;
			}
			pIconVGF->setMode(mode);
			pIconVGF->setPipeColor(pipeColor);
			pIconVGF->setDirection(calculateVgrDirection());
			if(connected)
			{
				pIconVGF->connect();
			}
		}
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_VG::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	if(pIconVPG)
	{
		pIconVPG->setMode(newMode);
	}
	if(pIconVVR)
	{
		pIconVVR->setMode(newMode);
	}
	if(pIconVGR)
	{
		pIconVGR->setMode(newMode);
	}
	if(pIconVGP)
	{
		pIconVGP->setMode(newMode);
	}
	if(pIconVGF)
	{
		pIconVGF->setMode(newMode);
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set color used for pipe lines drawing
**
**	ARGUMENTS
**		newColor	- Color used for pipe drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_VG::setPipeColor(const QColor &newColor)
{
	if(pIconVPG)
	{
		pIconVPG->setPipeColor(newColor);
	}
	if(pIconVVR)
	{
		pIconVVR->setPipeColor(newColor);
	}
	if(pIconVGR)
	{
		pIconVGR->setPipeColor(newColor);
	}
	if(pIconVGP)
	{
		pIconVGP->setPipeColor(newColor);
	}
	if(pIconVGF)
	{
		pIconVGF->setPipeColor(newColor);
	}
	VacIcon::setPipeColor(newColor);
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6A01_1VVR_VG::connect(void)
{
	if(pIconVPG)
	{
		pIconVPG->connect();
	}
	if(pIconVVR)
	{
		pIconVVR->connect();
	}
	if(pIconVGR)
	{
		pIconVGR->connect();
	}
	if(pIconVGP)
	{
		pIconVGP->connect();
	}
	if(pIconVGF)
	{
		pIconVGF->connect();
	}
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6A01_1VVR_VG::disconnect(void)
{
	if(pIconVPG)
	{
		pIconVPG->disconnect();
	}
	if(pIconVVR)
	{
		pIconVVR->disconnect();
	}
	if(pIconVGR)
	{
		pIconVGR->disconnect();
	}
	if(pIconVGP)
	{
		pIconVGP->disconnect();
	}
	if(pIconVGF)
	{
		pIconVGF->disconnect();
	}
	return false;
}

/*
**	FUNCTION
**		Override VacIcon's method in order to have different menus for different
**		parts of icon
**
**	ARGUMENTS
**		pEvent	- Pointer to mouse event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_VG::mousePressEvent(QMouseEvent * /* pEvent */)
{
	// Do nothing
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPG_6A01_1VVR_VG::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	painter.setPen(pipeColor);
//	painter.eraseRect(0, 0, width(), height());

	switch(direction)
	{
	case VacIconContainer::Up:
		// Line from VVR to outside
		painter.fillRect(pump.center().x() - 1, 0, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, vvr.bottom() + 1, 3, pump.top() - vvr.bottom(), pipeColor);
		// Line connecting 2 gauges
		painter.fillRect(vgr.right() - 1, vgr.center().y() - 1, 9, 3, pipeColor);
		break;
	case VacIconContainer::Down:
		// Line from VVR to outside
		painter.fillRect(pump.center().x() - 1, vvr.bottom(), 3, 4, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, pump.bottom(), 3, vvr.top() - pump.bottom(), pipeColor);
		// Line connecting 2 gauges
		painter.fillRect(vgr.right() - 1, vgr.center().y() - 1, 9, 3, pipeColor);
		break;
	case VacIconContainer::Left:
		// Line from VVR to outside
		painter.fillRect(vvr.right(), pump.center().y() - 1, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.right(), pump.center().y() - 1, vvr.left() - pump.right(), 3, pipeColor);
		// Line connecting 2 gauges
		painter.fillRect(vgp.center().x() - 1, vgp.bottom() - 1, 3, 9, pipeColor);
		break;
	case VacIconContainer::Right:
		// Line from VVR to outside
		painter.fillRect(0, pump.center().y() - 1, 3, 3, pipeColor);
		// Connect pump to valves
		painter.fillRect(vvr.right(), pump.center().y() - 1, pump.left() - vvr.right() + 2, 3, pipeColor);
		// Line connecting 2 gauges
		painter.fillRect(vgp.center().x() - 1, vgp.bottom() - 1, 3, 9, pipeColor);
		break;
	}

	if(!pEqp)
	{
		painter.setPen(Qt::black);
		painter.drawRect(pump.left(), pump.top(), pump.width() - 1, pump.height() - 1);
		painter.drawRect(vgr.left(), vgr.top(), vgr.width() - 1, vgr.height() - 1);
		painter.drawRect(vgp.left(), vgp.top(), vgp.width() - 1, vgp.height() - 1);
		painter.drawRect(vvr.left(), vvr.top(), vvr.width() - 1, vvr.height() - 1);
	}
}
