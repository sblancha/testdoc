#ifndef	VACICONVPN_H
#define	VACICONVPN_H

//	Icon for VPN

#include "VacIcon.h"

class EqpVPN;

class VACCTLEWOUTIL_EXPORT VacIconVPN : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPN(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPN();

	virtual VacIconContainer::Direction getSlaveDirection(void) const;

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVPN *pEqp;

	// Font for drawing 'NEG' word
	static QFont	font;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter);
	virtual void setPainterForImage(QPainter &painter);
};

#endif	// VACICONVPN_H
