//	Implementation of VacIconVGF_STD_RI class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGF_STD_RI.h"

#include "EqpVG_STD.h"

#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVGF_STD_RI::VacIconVGF_STD_RI(QWidget *parent, Qt::WindowFlags f) :
	VacIconVG_STD(parent, f)
{
	QFont mFont("Times", 8, QFont::Bold);
	setFont(mFont);
	setFixedSize(21, 35);
}

VacIconVGF_STD_RI::~VacIconVGF_STD_RI()
{
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVGF_STD_RI::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		return VacIconGeometry(21, 35, 10, 38, 1);
		break;
	default:
		break;
	}
	return VacIconGeometry(35, 21, 38, 10, 1);
}

/*
**	FUNCTION
**		Override method of VacIconVG: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVGF_STD_RI::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(21);
		size.setHeight(35);
		break;
	default:
		size.setWidth(35);
		size.setHeight(21);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVGF_STD_RI::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
// L.Kopylov 08.10.2013  painter.eraseRect(0, 0, width(), height());
//	painter.fillRect(rect(), Qt::green);	// For testing
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Draw connecting pipe
	if(drawConnectionPipe)
	{
		switch(direction)
		{
		case VacIconContainer::Up:	// No rotation
			painter.fillRect((width() >> 1) - (pipeWidth >> 1), height() - 4, pipeWidth ? pipeWidth : 1, 5, pipeColor);
			break;
		case VacIconContainer::Down:
			painter.fillRect((width() >> 1) - (pipeWidth >> 1), 0, pipeWidth ? pipeWidth : 1, 5, pipeColor);
			break;
		case VacIconContainer::Left:
			painter.fillRect(width() - 4, (height() >> 1) - (pipeWidth >> 1), 5, pipeWidth ? pipeWidth : 1, pipeColor);
			break;
		case VacIconContainer::Right:	// 90 degrees clockwise
			painter.fillRect(0, (height() >> 1) - (pipeWidth >> 1), 5, pipeWidth ? pipeWidth : 1, pipeColor);
			break;
		}
	}

	// Pipe is ready - draw gauge image
	drawImage(painter);

	// Draw BlockedOFF state (if any)
	if(pEqp)
	{
		if(pEqp->isBlockedOff(mode))
		{
			QPen blackPen(Qt::black, 2);
			painter.setPen(blackPen);
			painter.drawLine((width() >> 1) - 10, (height() >> 1) - 10, (width() >> 1) + 10, (height() >> 1) + 10);
			painter.drawLine((width() >> 1) - 10, (height() >> 1) + 10, (width() >> 1) + 10, (height() >> 1) - 10);
		}
	}
}

void VacIconVGF_STD_RI::drawImage(QPainter &painter)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		drawImageVertical(painter, 0);
		break;
	case VacIconContainer::Down:
		drawImageVertical(painter, 1);
		break;
	case VacIconContainer::Left:
		drawImageHorizontal(painter, 0);
		break;
	case VacIconContainer::Right:
		drawImageHorizontal(painter, 1);
		break;
	}
}

void VacIconVGF_STD_RI::drawImageVertical(QPainter &painter, int offset)
{
	setPainterForMainImage(painter);

	// Filled circle for Pirani
	painter.setClipping(true);
	painter.setClipRect(0, 0 + offset, 21, 17);
	QPen pen = painter.pen();
	painter.setPen(Qt::NoPen);
	painter.drawPie(1, 1 + offset, 18, 18, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(1, 1 + offset, 18, 18, 0, 360 * 16);

	// Filled circle for Ion gauge
	painter.setClipRect(0, (height() >> 1) + offset, 21, 17);
	painter.setPen(Qt::NoPen);
	painter.drawPie(1, (height() >> 1) - 3 + offset, 18, 18, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(1, (height() >> 1) - 3 + offset, 18, 18, 0, 360 * 16);

	// Pirani symbol
	painter.setClipping(false);
	painter.setPen(Qt::black);
	QPolygon points(10);
	points.setPoint(0, 3, 9 + offset);
	points.setPoint(1, 5, 9 + offset);
	points.setPoint(2, 5, 13 + offset);
	points.setPoint(3, 8, 13 + offset);
	points.setPoint(4, 8, 7 + offset);
	points.setPoint(5, 12, 7 + offset);
	points.setPoint(6, 12, 13 + offset);
	points.setPoint(7, 15, 13 + offset);
	points.setPoint(8, 15, 9 + offset);
	points.setPoint(9, 17, 9 + offset);
	painter.drawPolyline(points);

	// Ion gauge symbol
	painter.setPen(Qt::black);
	painter.drawText(9, 27 + offset, "i");
}

void VacIconVGF_STD_RI::drawImageHorizontal(QPainter &painter, int offset)
{
	setPainterForMainImage(painter);

	// Filled circle for Pirani
	painter.setClipping(true);
	painter.setClipRect(0 + offset, 0, 17, 21);
	QPen pen = painter.pen();
	painter.setPen(Qt::NoPen);
	painter.drawPie(1 + offset, 1, 18, 18, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(1 + offset, 1, 18, 18, 0, 360 * 16);

	// Filled circle for Penning (close to connection point)
	painter.setClipRect((width() >> 1) + offset, 1, 17, 21);
	painter.setPen(Qt::NoPen);
	int baseVGI = (width() >> 1) - 3 + offset;
	painter.drawPie(baseVGI, 1, 18, 18, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(baseVGI, 1, 18, 18, 0, 360 * 16);

	// Pirani symbol
	painter.setClipping(false);
	painter.setPen(Qt::black);
	QPolygon points(10);
	points.setPoint(0, 3 + offset, 9);
	points.setPoint(1, 5 + offset, 9);
	points.setPoint(2, 5 + offset, 13);
	points.setPoint(3, 8 + offset, 13);
	points.setPoint(4, 8 + offset, 7);
	points.setPoint(5, 12 + offset, 7);
	points.setPoint(6, 12 + offset, 13);
	points.setPoint(7, 15 + offset, 13);
	points.setPoint(8, 15 + offset, 9);
	points.setPoint(9, 16 + offset, 9);
	painter.drawPolyline(points);

	// Ion gauge symbol
	painter.setPen(Qt::black);
	painter.drawText(baseVGI + 8, 15, "i");
}

