#ifndef FUNCTIONALTYPEPLC_H
#define	FUNCTIONALTYPEPLC_H

// Special functional type for gauges - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypePLC : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		OK = 1
	};

	FunctionalTypePLC(int type, const char *name, const char *description);
	virtual ~FunctionalTypePLC() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEPLC_H
