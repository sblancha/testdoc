//	Implementation of FunctionalTypeITL_CMNT class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeITL_CMNT.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include <stdio.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeITL_CMNT::FunctionalTypeITL_CMNT(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeITL_CMNT::fillCriteria(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CryoValid, "Cryo Source Valid");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: CRYO Source is Valid");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: CRYO Source is NOT Valid");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"CRYO Source(s) Valid in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"CRYO Source(s) NOT Valid in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"CRYO Source(s) Valid in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"CRYO Source(s) NOT Valid in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CryoMaintain, "CRYO Status Good");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: CRYO Status Good");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: CRYO Status BAD");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"CRYO Status Good in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"CRYO Status BAD in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"CRYO Status Good in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"CRYO Status BAD in main part {mainPart}");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeITL_CMNT::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case CryoValid:
			result = "CryoValid";
			break;
		case CryoMaintain:
			result = "CryoMaintain";
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
	return result;
}

int FunctionalTypeITL_CMNT::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "CryoValid"))
		{
			result = CryoValid;
		}
		else if(!strcasecmp(name, "CryoMaintain"))
		{
			result = CryoMaintain;
		}
		break;
	default:
		break;
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}

