// Common action definitions
#uses "vclMachine.ctl"	// Load CTRL library
#uses "vclEqpConfig.ctl"

// Name of DIP config DP to use
const string DEFAULT_DIP_CONFIG_DP = "DIPConfig_12";

// Currently published items/DPEs
global dyn_string gDsPublishItems;
global dyn_string gDsPublishDps;

void ReadDipPublishedDpes(dyn_string &exceptionInfo)
{
  dynClear(gDsPublishItems);
  dynClear(gDsPublishDps);

  dyn_dyn_string publishInfo;
  fwDIP_getAllPublications(DEFAULT_DIP_CONFIG_DP, publishInfo, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  if(dynlen(publishInfo) == 0)
  {
    return;  // No publications?
  }
  if(dynlen(publishInfo[fwDIP_OBJECT_DPES]) == dynlen(publishInfo[fwDIP_OBJECT_ITEM]))
  {
    for(int idx = dynlen(publishInfo[fwDIP_OBJECT_DPES]) ; idx > 0 ; idx--)
    {
      string dpName = dpSubStr(publishInfo[fwDIP_OBJECT_DPES][idx], DPSUB_DP);
      string itemName = publishInfo[fwDIP_OBJECT_ITEM][idx];
      dynAppend(gDsPublishDps, dpName);
      dynAppend(gDsPublishItems, itemName);
    }
  }
  else
  {
    fwException_raise(exceptionInfo, "ERRPR", "ReadDipPublishedDpes(): number of published items (" +
                      dynlen(publishInfo[fwDIP_OBJECT_ITEM]) + ") and number of DPEs (" +
                      dynlen(publishInfo[fwDIP_OBJECT_DPES]) + ") are different", "");
  }
  /*
  for(int idx = dynlen(publishInfo[fwDIP_OBJECT_ITEM]) ; idx > 0 ; idx--)
  {
    bool itemConfigured;
    dyn_string dpes, tags;
    int updateRate;
    string configDp;
    int exceptionInfoLen = dynlen(exceptionInfo);
    fwDIP_getItemPublication(publishInfo[fwDIP_OBJECT_ITEM][idx], itemConfigured, dpes, tags, updateRate,
                            configDp, exceptionInfo);
    if(dynlen(exceptionInfo) > exceptionInfoLen)
    {
      continue;
    }
    if(!itemConfigured)
    {
      continue;
    }
    if(dynlen(dpes) != 1)
    {
      continue;
    }
    mDipPublications[dpSubStr(dpes[1], DPSUB_DP_EL)] = publishInfo[fwDIP_OBJECT_ITEM][idx];
  }
  */
}

bool IsDpeDipPublishingCorrect(string dpName, string dpeName, string valueName, dyn_string &exceptionInfo)
{
  string publishName = LhcVacGetPublishingNameCMW(dpName) + "." + valueName;
  bool itemConfigured;
  dyn_string dpes, tags;
  int updateRate;
  string configDp;
  fwDIP_getItemPublication(publishName, itemConfigured, dpes, tags, updateRate,
                            configDp, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return false;
  }
  if(!itemConfigured)
  {
    return false;
  }
  if(dynlen(dpes) == 1)
  {
    if((dpSubStr(dpes[1], DPSUB_DP_EL)) == dpeName)
    {
      return true;
    }
  }  
  return false;
}

void SetDpeDipPublishing(string dpName, string dpeName, string valueName, dyn_string &exceptionInfo)
{
  string publishName = LhcVacGetPublishingNameCMW(dpName) + "." + valueName;
  bool itemConfigured;
  dyn_string dpes, tags;
  int updateRate;
  string configDp;
  fwDIP_getItemPublication(publishName, itemConfigured, dpes, tags, updateRate,
                            configDp, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  if(itemConfigured)
  {
    if(dynlen(dpes) == 1)
    {
      if((dpSubStr(dpes[1], DPSUB_DP_EL)) == dpeName)
      {
        return;
      }
    }
    fwDIP_unpublish(publishName, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      fwException_raise(exceptionInfo, "ERROR", "Failed to set DIP publication: SetDpeDipPublishing(" + dpeName +
        "): fwDIP_unpublish(" + publishName + ") failed", "");
      return;
    }
  }
  fwDIP_publishPrimitive(publishName, dpeName, 0, DEFAULT_DIP_CONFIG_DP, exceptionInfo, true);
  if(dynlen(exceptionInfo) > 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "Failed to set DIP publication: SetDpeDipPublishing(" + dpeName +
      "): fwDIP_publishPrimitive(" + publishName + ") failed", "");
  }
}

void RemoveAllDpDipPublishing(string dpName, dyn_string &exceptionInfo)
{
  int publishIdx = 0;
  while((publishIdx = dynContains(gDsPublishDps, dpName)) > 0)
  {
    fwDIP_unpublish(gDsPublishItems[publishIdx], exceptionInfo);
    dynRemove(gDsPublishDps, publishIdx);
    dynRemove(gDsPublishItems, publishIdx);
  }
}

// ProcessDpePublishing
/** Process publishing dpe for DIP
Usage: LHC Vacuum control internal

PVSS manager usage: VISION

  @param dpName: in, readable dpName
  @param dpeName: in, readable dpeName
  @param configDp: in, name of DIP config dp
  @param exceptionInfo: out, error message
  @return None.
  @author SME
*/
void ProcessDpePublishing(string dpeName, string itemName, dyn_string &exceptionInfo)
{
  bool          isConfigured;
  string        configDp;
  dyn_string    dpes,tags;
  int           updateRate;
// sme 
//   incorrect checking of the already published Dpe
// 

  fwDIP_getItemPublication(itemName, isConfigured, dpes, tags, updateRate,
    configDp, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("ProcessDpePublishing(): fwDIP_getItemPublication(" + itemName + ") failed:",
      exceptionInfo);
    return;
  }
  if( dynlen(dpes) == 1)
  {
    if((dpSubStr(dpes[1], DPSUB_DP_EL)) == dpeName)
    {
      return;
    }
  }  
  // perfoms publishing of DPE
  fwDIP_publishPrimitive(itemName, dpeName, 0, DEFAULT_DIP_CONFIG_DP, exceptionInfo, true);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("ProcessDpePublishing(): fwDIP_publishPrimitive(" + itemName + "," +
      dpeName + ") failed:", exceptionInfo);
  }
}

void UnpublishWrongPublishing( string dpeName, string itemName, dyn_string &exceptionInfo )
{
  bool 		 isConfigured;
  string 	 configDp;
  dyn_dyn_string currentPublicationInfo;
 
  // check the publishing name for specified dpe
  // if name is the same, return. If the name is not the same
  //   1. Unpublish this dpe with not actual publishing name
  //   2. Publish with new publishing name 
  fwDIP_getAllPublications(DEFAULT_DIP_CONFIG_DP, currentPublicationInfo, exceptionInfo);
  if( dynlen( exceptionInfo ) > 0 )
  {
    DebugTN( "LhcVacDIPConfig: fwDIP_getAllPublications(" + configDp + ") failed: "
      + exceptionInfo );
    return;
  }
  if( dynlen(currentPublicationInfo) > 0 )
  {  
    int index = dynContains(currentPublicationInfo[fwDIP_OBJECT_DPES], getSystemName()+ dpeName);
//DebugTN("Searching for publication of wrong DPE <" + (getSystemName()+ dpeName) + ">: " + index);
    if( index > 0 )
    {
//DebugTN("Published wrong as <" + currentPublicationInfo[fwDIP_OBJECT_ITEM][index] + ">, new <" + itemName + ">");
      fwDIP_unpublish(currentPublicationInfo[fwDIP_OBJECT_ITEM][index], exceptionInfo);
      if( dynlen( exceptionInfo ) > 0 )
      {
        DebugTN( "LhcVacDIPConfig: fwDIP_unpublish(" + dpeName + ") failed: "
          + exceptionInfo );
        return;
      }
    }
  }
  dynClear(currentPublicationInfo);
  dynClear(exceptionInfo);
  fwDIP_getAllPublications(DEFAULT_DIP_CONFIG_DP, currentPublicationInfo, exceptionInfo);
  if( dynlen( exceptionInfo ) > 0 )
  {
    DebugTN( "LhcVacDIPConfig: fwDIP_getAllPublications(" + configDp + ") failed: "
      + exceptionInfo );
    return;
  }
  if( dynlen(currentPublicationInfo) > 0 )
  {  
    int index = dynContains(currentPublicationInfo[fwDIP_OBJECT_ITEM], itemName);
//DebugTN("Searching for publication of wrong item <" + itemName + ">: " + index);
    if( index > 0 )
    {
//DebugTN("Published wrong as <" + currentPublicationInfo[fwDIP_OBJECT_ITEM][index] + ">, new <" + itemName + ">");
      fwDIP_unpublish(currentPublicationInfo[fwDIP_OBJECT_ITEM][index], exceptionInfo);
      if( dynlen( exceptionInfo ) > 0 )
      {
        DebugTN( "LhcVacDIPConfig: fwDIP_unpublish(" + dpeName + ") failed: "
          + exceptionInfo );
        return;
      }
    }
  }
}

// LhcVacGetPublishingName
/** Get publishing name for main part access mode
Usage: LHC Vacuum control internal

PVSS manager usage: VISION

  @param mainPart: in,  Main part name
  @return publishing name
  @author SME
*/
string LhcVacGetPublishingName(string mainPart)
{
  return "dip/VAC/" + glAccelerator + "/Access/" + mainPart;
}

// LhcVacGetPublishingNameCMW
/** Get publishing name for CMW
Usage: LHC Vacuum control internal

PVSS manager usage: VISION

  @param dpName: in,  dpName
  @return publishing name
  @author SME
*/
string LhcVacGetPublishingNameCMW(string dpName)
{
  string devName;
  dyn_string  exceptionInfo;
  LhcVacDisplayName(dpName, devName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return "dip/VAC/" + glAccelerator + "/" + dpName;
  }
  strreplace(devName, ".", "_");
  return "dip/VAC/" + glAccelerator + "/" + devName;
}

string LhcVacGetWrongPublishingNameCMW(string dpName)
{
  string devName;
  dyn_string  exceptionInfo;
  LhcVacDisplayName(dpName, devName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return "dip/VAC/" + glAccelerator + "/" + dpName;
  }
  return "dip/VAC/" + glAccelerator + "/" + devName;
}
