#ifndef MAINSPSIMAGE_H
#define MAINSPSIMAGE_H

// Widget drawing main view of PS, SPS and similar machines (NOT LHC)

#include "MainSpsImageEqpConnect.h"

#include "Sector.h"
#include "Eqp.h"
#include "ExtendedLabel.h"
#include "LegendLabel.h"

#include <QWidget>
#include <QList>
#include <QDateTime>

#include <stdio.h>

class LegendLabel;

class BeamLine;
class SectorDrawPart;

#include <QTimer>
#include <QPainter>
#include <QPoint>
#include <QHash>

class MainSpsImage : public QWidget
{
	Q_OBJECT

public:
	MainSpsImage(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	virtual ~MainSpsImage();

	void addLegendLabel(int x, int y, const char *text);
	void addRotatedLegendLabel(int x, int y, int angle, const char *text);

	// Accessors
	inline QColor &getDefaultColor(void) { return defaultColor; }
	inline void setDefaultColor(QColor &color) { defaultColor = color; }
	inline QColor &getSelectColor(void) { return selectColor; }
	inline void setSelectColor(QColor &color) { selectColor = color; }
	inline int getLineWidth(void) { return lineWidth; }
	inline void setLineWidth(int width) { lineWidth = width; }
	inline int getSelectLineWidth(void) { return selectLineWidth; }
	inline void setSelectLineWidth(int width) { selectLineWidth = width; }
	inline int getRedrawDelay(void) { return redrawDelay; }
	inline void setRedrawDelay(int delay) {  redrawDelay = delay < 200 ? 200 : delay; }
	inline int getViewWidth(void) { return width(); }
	inline int getViewHeight(void) { return height(); }
	void setSelectedSectors(QList<Sector *> &list)
	{
		selectedSectors = list;
		renewImage();
	}

	void renewImage(void);

signals:
	void mousePress(int button, int x, int y, const char *sectorName);

protected:
	// Default color for pipes drawing
	QColor	defaultColor;

	// Color for drawing selection
	QColor	selectColor;

	// Line width for pipes drawing
	int		lineWidth;

	// Line width for selection drawing
	int		selectLineWidth;

	// List of legend labels
	QList<LegendLabel *>	labels;

	// List of selected sectors
	QList<Sector *>		selectedSectors;

	// Label displaying name of sector under mouse pointer
	ExtendedLabel		*pLabel;

	// Timer to hide label
	QTimer			*pTimer;

	// Delay between receiving of 'state changed' signal from equipment
	// and redrawing the image. Delay is introduced in order not to redraw
	// image too often, but instead redraw it after reasonable delay
	// during which more signals could come
	int				redrawDelay;

	// Time when image was drawn
	QTime			drawTime;

	// Timer providing redraw after redrawDelay
	QTimer			*pRedrawTimer;

	// Width of this widget
	int				viewWidth;

	// Height of this widget
	int				viewHeight;

	// Geometry limits of equipment
	float			minX;
	float			minY;
	float			maxX;
	float			maxY;

	// Coefficients for recalculating equipment coordinates to screen coordinates
	float			coefX;
	float			coefY;
	float			offsetX;
	float			offsetY;

	// Borders for image drawing
	int				borderLeft;
	int				borderRight;
	int				borderTop;
	int				borderBottom;
	
	// To prevent infinite recursive calls when searching for color of parent line
	int				searchDepth;

	// Flag indicating if view has been drawn
	bool			drawn;

	// Instance of class providing connections to equipment
	MainSpsImageEqpConnect	connection;

	// Hash table to find neighbours of given valve, key is DP name of valve
	QHash<QByteArray, QList<Eqp *> *>	neighborValves;

	void resizeEvent(QResizeEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseDoubleClickEvent(QMouseEvent *event);

	void makeImage(void);
	void draw(QPainter &painter);
	bool findGeoLimits(void);
	void getBorders(void);
	bool setNewGeometry(QPainter &painter);
	void drawNotReadyText(QPainter &painter);
	void drawValveInterlocks(QPainter &painter);
	void drawValveInterlocksOnLine(QPainter &painter, BeamLine *pLine, int drawOrder);
	void drawLegendLabels(QPainter &painter);
	void drawSelection(QPainter &painter);
	void drawSelPart(QPainter &painter, SectorDrawPart *pSectorDrawPart);
	void drawLines(QPainter &painter);
	void drawCircleLine(QPainter &painter, BeamLine *pLine, short drawOrder);
	void drawStraightLine(QPainter &painter, BeamLine *pLine, short drawOrder);
	void findLineStartColor(BeamLine *pLine, QColor &color, short &drawOrder);
	BeamLine *findLineOfStart(const char *lineName, const char *parentPartName);
	void findColorInParent(BeamLine *pLine, const char *childLineName,
		QColor &color, short &drawOrder);
	void drawEqp(bool isReverse, Eqp *pEqp, QList<QPoint *> &points);
	void nextDrawPoint(float coordX, float coordY, QList<QPoint *> &points);
	void drawPolyline(QPainter &painter, QList<QPoint *> &pointList);

	const QString sectorAtLocation(int x, int y, bool pureSectorName = false);

	void buildNeighborValves(void);
	void buildNeighborsOfValve(Eqp *pValveEqp, const QList<BeamLine *> &lines);

private slots:
	void timerDone(void);
	void redrawTimerDone(void);
	void eqpStateChanged(void);
};

#endif	// MAINSPSIMAGE_H
