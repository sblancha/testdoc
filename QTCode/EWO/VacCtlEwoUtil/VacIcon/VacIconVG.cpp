//	Implementation of VacIconVG class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVG.h"
#include "VacMainView.h"

#include "EqpVG.h"

#include <QPainter>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0
#define COLOR_WARNING			255,255,0
#define COLOR_UNDERRANGE		0,180,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define RR1_PROTECTED	(0x00080000)
#define	RR1_ON			(0x00020000)
#define	RR1_OFF			(0x00010000)
#define	RR1_UNDERRANGE	(0x00000200)
#define	RR1_SELFPROT	(0x00000100)
#define	RR1_OTHER_WARN	(0x00001C00)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVG::VacIconVG(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 21);
}

VacIconVG::~VacIconVG()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVG::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(21, 21, 10, 24, 1);
}

VacIconContainer::Direction VacIconVG::getSlaveDirection(void) const
{
	VacIconContainer::Direction result = VacIconContainer::Up;
	switch(direction)
	{
	case VacIconContainer::Up:
		result = VacIconContainer::Up;
		break;
	case VacIconContainer::Down:
		result = VacIconContainer::Down;
		break;
	case VacIconContainer::Left:
		result = VacIconContainer::Right;
		break;
	case VacIconContainer::Right:
		result = VacIconContainer::Left;
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVG::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVG") || pEqp->inherits("EqpVG_PT"), "VacIconVG::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVG *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVG::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVG::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("PR");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVG::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVG::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVG::paintEvent(QPaintEvent * /* pEvent */)
{
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Gauge image is NOT rotated (does not depend on direction), but
	// connecting pipe IS rotated
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		painter.fillRect(10 - pipeWidth / 2, 18, pipeWidth ? pipeWidth : 1, 4, pipeColor);
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.fillRect(10 - pipeWidth / 2, 0, pipeWidth ? pipeWidth : 1, 4, pipeColor);
		break;
	case VacIconContainer::Left:	// 90 degrees clockwise
		painter.fillRect(0, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		break;
	case VacIconContainer::Right:	// 90 degrees conuterclockwise
		painter.fillRect(18, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		break;
	}
	// Pipe is ready - draw gauge image
	drawImage(painter, ctlStatus);
}

void VacIconVG::drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus)
{
	setPainterForMainImage(painter, ctlStatus);

	// Filled circle
	QPen pen = painter.pen();
	painter.setPen(Qt::NoPen);
	painter.drawPie(1, 1, 19, 19, 0, 360 * 16);
	painter.setPen(pen);
	// L.Kopylov 11.11.2011 Use drawEllipse() instead painter.drawArc(1, 1, 19, 19, 0, 360 * 16);
	painter.drawEllipse(1, 1, 19, 19);

	// type-specific part of image
	drawSpecific(painter);
}

void VacIconVG::setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus)
{
	QColor fillColor(COLOR_UNDEFINED);
	QColor lineColor = Qt::black;
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		fillColor.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		fillColor.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(pEqp)
		{
			pEqp->getMainColor(fillColor, mode);
		}
		/* Simplified approach - see above, main color of icon is the same as one of device. L.Kopylov 08.06.2015
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				// Line color - depends on errors/warnings
				if(rr1 & RR1_ERROR)
				{
					// lineColor.setRgb(COLOR_ERROR);
					fillColor.setRgb(COLOR_ERROR);
				}
				else if(rr1 & RR1_WARNING)
				{
					// Warnins 'underrange' and 'self-protection' are shown in special color
					// when gauge is ON and no other warnings
					if((rr1 & RR1_ON) && (!(rr1 & RR1_OFF)) && (!(rr1 & RR1_OTHER_WARN)))
					{
						// lineColor.setRgb(COLOR_WARNING);
						fillColor.setRgb(COLOR_UNDERRANGE);
					}
					else
					{
						fillColor.setRgb(COLOR_WARNING);
					}
				}
				// Fill color - depends on ON/OFF state (plus some warnings)
				else if(rr1 & RR1_ON)
				{
					fillColor.setRgb(COLOR_ON);
					if(rr1 & RR1_OFF)
					{
						fillColor.setRgb(COLOR_ERROR);
					}
					else if(rr1 & RR1_WARNING)
					{
						// Warnins 'underrange' and 'self-protection' are shown in special color
						// when gauge is ON and no other warnings
						if((rr1 & RR1_UNDERRANGE) || (rr1 & RR1_SELFPROT))
						{
							fillColor.setRgb(COLOR_UNDERRANGE);
						}
					}
				}
				else if(rr1 & RR1_OFF)
				{
					fillColor.setRgb(COLOR_OFF);
				}
			}
		}
		*/
		break;
	}
	painter.setBrush(fillColor);
	QPen pen(lineColor, lineColor == Qt::black ? 1 : 2);
	painter.setPen(pen);
}

void VacIconVG::drawInterlockSrcArrow(QPainter &painter, bool before, bool after)
{
	if(!(before || after))
	{
		return;
	}
	painter.resetTransform();
	painter.setPen(VacMainView::getSynPassiveLineColor());
	int start, end, pos;
	switch(direction)
	{
	case VacIconContainer::Down:
		start = x();
		end = start + width();
		pos = y() + height() + 5;
		painter.drawLine(start, pos, end, pos);
		if(before)
		{
			painter.drawLine(start, pos, start + 3, pos - 3);
			painter.drawLine(start, pos, start + 3, pos + 3);
		}
		if(after)
		{
			painter.drawLine(end, pos, end - 3, pos - 3);
			painter.drawLine(end, pos, end - 3, pos + 3);
		}
		break;
	case VacIconContainer::Up:	// 180 degrees
		start = x();
		end = start + width();
		pos = y() - 5;
		painter.drawLine(start, pos, end, pos);
		if(before)
		{
			painter.drawLine(start, pos, start + 3, pos - 3);
			painter.drawLine(start, pos, start + 3, pos + 3);
		}
		if(after)
		{
			painter.drawLine(end, pos, end - 3, pos - 3);
			painter.drawLine(end, pos, end - 3, pos + 3);
		}
		break;
	case VacIconContainer::Left:	// 90 degrees clockwise
		start = y();
		end = start + height();
		pos = x() + width() + 5;
		painter.drawLine(pos, start, pos, end);
		if(before)
		{
			painter.drawLine(pos, start, pos + 3, start + 3);
			painter.drawLine(pos, start, pos - 3, start + 3);
		}
		if(after)
		{
			painter.drawLine(pos, end, pos + 3, end - 3);
			painter.drawLine(pos, end, pos - 3, end - 3);
		}
		break;
	case VacIconContainer::Right:	// 90 degrees conuterclockwise
		start = y();
		end = start + height();
		pos = x() - 5;
		painter.drawLine(pos, start, pos, end);
		if(before)
		{
			painter.drawLine(pos, start, pos + 3, start + 3);
			painter.drawLine(pos, start, pos - 3, start + 3);
		}
		if(after)
		{
			painter.drawLine(pos, end, pos + 3, end - 3);
			painter.drawLine(pos, end, pos - 3, end - 3);
		}
		break;
	}
}
