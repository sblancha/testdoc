//	Implementation of VacIconVPR_VELO class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPR_VELO.h"

#include "EqpVPR_VELO.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define	COLOR_ERROR				255,102,255

// Colors for error/warning states
#define COLOR_GREEN				0,255,0
#define COLOR_YELLOW			255,255,0
#define COLOR_RED				255,0,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_ON			(0x00020000)
#define	RR1_OFF		(0x00010000)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVPR_VELO::VacIconVPR_VELO(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(25, 25);
}

VacIconVPR_VELO::~VacIconVPR_VELO()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPR_VELO::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(25, 25, 12, 11, 0);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPR_VELO::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVPR_VELO"), "VacIconVPR_VELO::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVPR_VELO *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPR_VELO::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPR_VELO::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPR_VELO::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPR_VELO::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPR_VELO::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Prepare painter's coordinate system for drawing
	// Coordinate system (0,0) is center of widget
	painter.translate(width() / 2, height() / 2);
	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	// Coordinate system is ready - draw
	drawImage(painter, ctlStatus, rr1, plcAlarm);
}

void VacIconVPR_VELO::drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	// Draw filled rectangle with main color
	setPainter(painter, ctlStatus, rr1, plcAlarm);
	painter.fillRect(-11, -11, 23, 23, painter.brush());
	painter.drawRect(-11, -11, 23, 23);

	// Draw circle
	painter.setPen(QPen(Qt::black, 1));
	painter.drawArc(-8, -8, 17, 17, 0, 360 * 16);

	// Draw arrow-like symbol
	painter.drawLine(0, -11, -4, -7);
	painter.drawLine(0, -11, 4, -7);
}

void VacIconVPR_VELO::setPainter(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	QColor color(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr1 & RR1_ON)
				{
					if(rr1 & RR1_OFF)
					{
						color.setRgb(COLOR_ERROR);
					}
					else
					{
						color.setRgb(COLOR_ON);
					}
				}
				else if(rr1 & RR1_OFF)
				{
					color.setRgb(COLOR_OFF);
				}
				else
				{
					color.setRgb(COLOR_UNDEFINED);
				}
			}
		}
		break;
	}
	QBrush brush(color);
	painter.setBrush(brush);
	painter.setPen(Qt::black);
}
