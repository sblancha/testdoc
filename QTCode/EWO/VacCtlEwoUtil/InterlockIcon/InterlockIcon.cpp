#include "InterlockIcon.h"
#include "InterlockIconVRVCL.h"
#include "InterlockIconVRVCX.h"
#include "InterlockIconVRIVC.h"
#include "InterlockIconVRGPT.h"
#include "InterlockIconVRPIT.h"
#include "InterlockIconVRIVD.h"

#include "qpainter.h"

InterlockIcon::InterlockIcon(QWidget * parent, Qt::WindowFlags f) :
QWidget(parent)
{
	this->parent = parent;
	QSize mySize(1220, 1720);
	setFixedSize(mySize);
	visible = true;
	beamColor = Qt::black;
}

InterlockIcon::~InterlockIcon()
{

}


InterlockIcon * InterlockIcon::getIcon(InterlockEqp * pIntEqp, QWidget * parent)
{
	
	InterlockIcon * pIcon = NULL;
	switch (pIntEqp->getType())
	{
	case VRVCL:
		pIcon = new InterlockIconVRVCL(parent);
		return pIcon;
	case VRVCX:
		pIcon = new InterlockIconVRVCX(parent);
		return pIcon;
	case VRIVC:
		pIcon = new InterlockIconVRIVC(parent);
		return pIcon;
	case VRGPT:
		pIcon = new InterlockIconVRGPT(parent);
		return pIcon;
	case VRPI:
		pIcon = new InterlockIconVRPIT(parent);
		return pIcon;
	case VRIVD:
		pIcon = new InterlockIconVRIVD(parent);
		return pIcon;
	}
}

void InterlockIcon::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.drawRect(0, 0, 10, 10);
}

void InterlockIcon::moveIcon(int x, int y)
{
	this->move(x, y);
}

bool InterlockIcon::operator==(const InterlockIcon &icon)
{
	if (this->name == icon.name)
	{
		return true;
	}

	return false;
	
}