//	Implementation of MessageGroup class
/////////////////////////////////////////////////////////////////////////////////

#include "MessageGroup.h"

#include <qtimer.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

MessageGroup::MessageGroup(void *id, const QString cfgName, int lifeTime) :
	configName(cfgName)
{
	groupId = id;
	this->lifeTime = lifeTime;
	if(this->lifeTime < 10)
	{
		this->lifeTime = 10;
	}
	createTime = QDateTime::currentDateTime();
	pSector = NULL;
	pMainPart = NULL;
}

/*
**	FUNCTION
**		Add message to list of pending messages in this group
**
**	ARGUMENTS
**		message	- Message to be added
**
**	RETURNS
**		Number of messages in this group
**
**	CAUTIONS
**		None
*/
int MessageGroup::addMessage(QString &message)
{
	if(messages.isEmpty())
	{
		messages.append(message);
	}
	else
	{
		QStringList::Iterator iter;
		for(iter = messages.begin() ; iter != messages.end() ; ++iter)
		{
			if((*iter) == message)
			{
				return messages.count();
			}
		}
		messages.append(message);
	}
	return messages.count();
}

/*
**	FUNCTION
**		Check if life time for this group is expired
**
**	ARGUMENTS
**		now	- current time
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
bool MessageGroup::isTimeExpired(const QDateTime &now)
{
	return createTime.secsTo(now) > lifeTime;
}

