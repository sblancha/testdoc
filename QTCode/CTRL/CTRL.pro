#   14.11.2016	L.Kopylov
#		Switch from Makefile-based to qmake-based compilation
#


TEMPLATE = subdirs

SUBDIRS = VacCtlPvssUtil \
	VacCtlHistoryDataPvss \
	VacCtlEqpDataPvss \
	VacCtlImportPvss \
	VacCtlResourcesPvss \
	VacCtlOnlineNotificationPvss
