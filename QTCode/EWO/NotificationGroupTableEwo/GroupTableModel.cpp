//	Implementation of GroupTableModel class
/////////////////////////////////////////////////////////////////////////////////

#include "GroupTableModel.h"
#include "GroupConfig.h"
#include <stdio.h>

GroupTableModel::GroupTableModel(QObject *parent) : QAbstractTableModel(parent) {
	connectRequestSent = false;
	pTimer = new QTimer(this);
	pTimer->setSingleShot(true);
	connect(pTimer, SIGNAL(timeout()), this, SLOT(timerDone()));
}

GroupTableModel::~GroupTableModel() {
	while(!rowDict.isEmpty()) {
		delete rowDict.takeFirst();
	}
	while(!pendingGroups.isEmpty())	{
		delete pendingGroups.takeFirst();
	}
}


void GroupTableModel::clear(void) {
	if(pTimer->isActive()) {
		pTimer->stop();
	}
	bool toNotify = rowDict.count() > 0;
	if(toNotify) {
		beginRemoveRows(QModelIndex(), 0, rowDict.count() - 1);
	}
	mutex.lock();
	while(!rowDict.isEmpty()) {
		SmsGroup *pGroup = rowDict.takeFirst();
		if(pGroup->isConnected()) {
			disconnectOnClear.push(pGroup->getDpName());
		}
		delete pGroup;
	}
	while(!pendingGroups.isEmpty())	{
		delete pendingGroups.takeFirst();
	}
	allGroups.clear();
	mutex.unlock();
	if(toNotify) {
		endRemoveRows();
	}
	if((!disconnectOnClear.isEmpty()) && (!connectRequestSent)) {
		connectRequestSent = true;
		emit connectRequest();
	}
}

void GroupTableModel::addGroup(const QString &dpName) {
	SmsGroup *pGroup = new SmsGroup(dpName);
	connect(pGroup, SIGNAL(changed(SmsGroup *)),
		this, SLOT(groupChanged(SmsGroup *)));
	connect(pGroup, SIGNAL(numMsgChanged(SmsGroup *, int)),
		this, SLOT(groupNumMsgChanged(SmsGroup *, int)));
	connect(pGroup, SIGNAL(numEventsChanged(SmsGroup *, int)),
		this, SLOT(groupNumEventsChanged(SmsGroup *, int)));
	addRow(pGroup);
	addToAllGroups(pGroup);
	addCallbackRequest();
}

void GroupTableModel::deleteGroup(const QString &dpName) {
	SmsGroup *pToDelete = allGroups.value(dpName);
	if(!pToDelete) {
		return;
	}
	if(pToDelete->isDeleted()) {
		return;
	}
printf("GroupTable::deleteGroup(): before allGroups %d rowDict %d\n", allGroups.count(), rowDict.count());
fflush(stdout);
	pToDelete->setDeleted(true);

	// Delete from table
	int row = pToDelete->getRow();
	if(row >= 0) {
		int rowsToDelete = 0;
		beginRemoveRows(QModelIndex(), row, row);
		for(int n = row ; n < rowDict.count() ; n++) {
			SmsGroup *pGroup = rowDict.at(n);
			if(pGroup->isDeleted()) {
				rowDict.takeAt(n);
				rowsToDelete++;
			}
			else {
/*
printf("GroupTable::deleteGroup(): change %d to %d\n", n, n - rowsToDelete);
fflush(stdout);
*/
				pGroup->setRow(n - rowsToDelete);
			}
		}
		endRemoveRows();
/*
printf("GroupTable::deleteGroup(): rowsToDelete %d\n", rowsToDelete);
fflush(stdout);
*/
	}

	// Delete from pendingGroups
	for(int idx = 0 ; idx < pendingGroups.count() ; idx++) {
		if(dpName == pendingGroups.at(idx)->getDpName()) {
			pendingGroups.takeAt(idx);
			break;
		}
	}

	// Delete all config 
	// NOT NEEDED, IN THIS TABLE THERE ARE ONLY GROUPS
	/*
	mutex.lock();
	if(pToDelete->isGroup())
	{
		const QList<SmsGroup *> &children = ((GroupGroup *)pToDelete)->getList();
		for(int idx = 0 ; idx < children.count() ; idx++)
		{
			SmsConfig *pConfig = children.at(idx);
			if(pConfig->isConnected() && pConfig->isDeleted())
			{
				disconnectOnClear.push(pConfig->getDpName());
			}
			allDict.take(pConfig->getDpName());
		}
	}
	*/
	if(pToDelete->isConnected()) {
		disconnectOnClear.push(pToDelete->getDpName());
	}
	allGroups.take(pToDelete->getDpName());

	// Delete from rowDict, delete instance
//	pToDelete = rowDict.take(pToDelete->getRow());
//	if(pToDelete)
//	{
		delete pToDelete;	// !!!!!!!!!
//	}
	//mutex.unlock();
	if((!disconnectOnClear.isEmpty()) && (!connectRequestSent)) {
		connectRequestSent = true;
		emit connectRequest();
	}
	printf("GroupTable::deleteGroup(): after allGroups %d rowDict %d\n", allGroups.count(), rowDict.count());
	fflush(stdout);
}

void GroupTableModel::setGroupOwner(const QString &dpName, const QString &owner) {
	SmsGroup *pGroup = allGroups.value(dpName);
	if(pGroup) {
		pGroup->setOwner(owner);
	}
}

void GroupTableModel::setGroupName(const QString &dpName, const QString &name) {
	SmsGroup *pGroup = allGroups.value(dpName);
	if(pGroup) {
		pGroup->setVisibleName(name);
	}
}

void GroupTableModel::setGroupId(const QString dpName, long id) {
	SmsGroup *pGroup = allGroups.value(dpName);
	if (pGroup) {
		pGroup->setId(id);
	}
}

void GroupTableModel::setGroupType(const QString dpName, int type) {
	SmsGroup *pGroup = allGroups.value(dpName);
	if (pGroup) {
		pGroup->setType(type);
	}
}

void GroupTableModel::setGroupScopes(const QString &dpName, const QStringList &scopes) {
	SmsGroup *pGroup = allGroups.value(dpName);
	if (pGroup) {
		pGroup->setScopes(scopes);
	}
}

void GroupTableModel::setGroupRecipients(const QString &dpName, const QStringList &recipients) {
	SmsGroup *pGroup = allGroups.value(dpName);
	if (pGroup) {
		pGroup->setRecipients(recipients);
	}
}
void GroupTableModel::setGroupError(const QString &dpName, const QString &error) {
	SmsGroup *pGroup = allGroups.value(dpName);
	if(pGroup) {
		pGroup->setError(error);
	}
}

void GroupTableModel::setGroupNumMsg(const QString &dpName, int numMsg) {
	SmsGroup *pGroup = allGroups.value(dpName);
	if(pGroup) {
		pGroup->setNumMsg(numMsg);
	}
}

void GroupTableModel::setGroupNumEvents(const QString &dpName, unsigned numEvents) {
	SmsGroup *pGroup = allGroups.value(dpName);
	if(pGroup) {
		pGroup->setNumEvents(numEvents);
	}
}

void GroupTableModel::setGroupFlags(const QString &dpName, bool active, bool activated, bool initialized) {
	SmsGroup *pGroup = allGroups.value(dpName);
	if(pGroup) {
		pGroup->setFlags(active, activated, initialized);
	}
}


void GroupTableModel::addRow(SmsGroup *pGroup) {
	// Add to list of pending
	pendingGroups.append(pGroup);
	if(!pTimer->isActive()) {
		pTimer->start(500);
	}
}

void GroupTableModel::timerDone(void) {
	if(pendingGroups.isEmpty()) {
		return;
	}
	beginInsertRows(QModelIndex(), rowDict.count(), rowDict.count() + pendingGroups.count() - 1);
	QMutexLocker locker(&mutex);
	while(!pendingGroups.isEmpty()) {
		SmsGroup *pGroup = pendingGroups.takeFirst();
		pGroup->setRow(rowDict.count());
		rowDict.append(pGroup);
	}
	endInsertRows();
}

void GroupTableModel::addCallbackRequest(void) {
	mutex.lock();
	if(connectRequestSent) {
		mutex.unlock();
		return;
	}
	mutex.unlock();
	connectRequestSent = true;
	emit connectRequest();
}

const QString GroupTableModel::nextDisconnectRequest(void) {
	bool hasPending = false;
	QString result;
	QMutexLocker locker(&mutex);
	if(!disconnectOnClear.isEmpty()) {
		result = disconnectOnClear.pop();
		return QString(result);
	}
	foreach(SmsGroup *pGroup, allGroups) {
		if(pGroup->isDeleted()) {
			if(pGroup->isConnected()) {
				if(result.isEmpty()) {
					result = pGroup->getDpName();
					pGroup->setConnected(false);
				}
			}
		}
		else if(!pGroup->isConnected()) {
			hasPending = true;
		}
		if(hasPending && (!result.isEmpty())) {
			break;
		}
	}
	if(!hasPending) {
		connectRequestSent = false;
	}
	return QString(result);
}

const QString GroupTableModel::nextConnectRequest(void) {
	bool hasPending = !disconnectOnClear.isEmpty();
	QString result;
	QMutexLocker locker(&mutex);
	foreach(SmsGroup *pGroup, allGroups) {
		if(pGroup->isDeleted()) {
			if(pGroup->isConnected()) {
				hasPending = true;
			}
		}
		else if(!pGroup->isConnected()) {
			if(result.isEmpty()) {
				result = pGroup->getDpName();
				pGroup->setConnected(true);
			}
			else {
				hasPending = true;
			}
		}
		if(hasPending && (!result.isEmpty())) {
			break;
		}
	}
	if(!hasPending) {
		connectRequestSent = false;
	}
	return QString(result);
}

const QString GroupTableModel::getGroupAtRow(int row) {
	if((0 <= row) && (row < rowDict.count())) {
		SmsGroup *pGroup = rowDict.at(row);
		if(pGroup) {
			return QString(pGroup->getDpName());
		}
	}
	return QString();
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Slots
/////////////////////////////////////////////////////////////////////////////////

void GroupTableModel::groupChanged(SmsGroup *pGroup) {
	int row = pGroup->getRow();
	if(row >= 0) {
		emit dataChanged(index(row, SmsGroup::Owner), index(row, SmsGroup::Msg));
	}
}

void GroupTableModel::groupNumMsgChanged(SmsGroup *pGroup, int /* delta */) {
	int row = pGroup->getRow();
	if(row >= 0) {
		emit dataChanged(index(row, SmsGroup::Msg), index(row, SmsGroup::Msg));
	}
}
void GroupTableModel::groupNumEventsChanged(SmsGroup *pGroup, int /* delta */) {
	int row = pGroup->getRow();
	if(row >= 0) {
		emit dataChanged(index(row, SmsGroup::Events), index(row, SmsGroup::Events));
	}
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Implementation of QAbstractTableModel's methods
/////////////////////////////////////////////////////////////////////////////////




int GroupTableModel::rowCount(const QModelIndex &parent) const {
	return parent.isValid() ? 0 : rowDict.count();
}

int GroupTableModel::columnCount(const QModelIndex &parent) const {
	return parent.isValid() ? 0 : 11;
}

QVariant GroupTableModel::data(const QModelIndex &index, int role) const {
	if(!index.isValid()) {
		return QVariant();
	}
	if((index.row() < 0) || (index.row() >= rowDict.count())) {
		return QVariant();
	}
	const SmsGroup *pGroup = rowDict.at(index.row());
	switch(role) {
	case Qt::TextAlignmentRole:
		return Qt::AlignCenter;
		break;
	case Qt::DisplayRole:	// Text to be shown
		switch(index.column()) {
		case SmsGroup::Id: {
				char buf[32];
				if(pGroup->getId() < 99999) {
#ifdef Q_OS_WIN
					sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%05d", pGroup->getId());
#else
					sprintf(buf, "%05d", pGroup->getId());
#endif
				}
				else {
#ifdef Q_OS_WIN
					sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d", pGroup->getId());
#else
					sprintf(buf, "%d", pGroup->getId());
#endif
				}
//				QString visibleId(buf);
				return QVariant(buf);
			}
			break;
		case SmsGroup::Owner:
			return QVariant(pGroup->getOwner());
		case SmsGroup::Name:
			return QVariant(pGroup->getVisibleName());
		case SmsGroup::Type:
			return QVariant(pGroup->getType());
		case SmsGroup::Scopes:
			return QVariant(pGroup->getScopes());
		case SmsGroup::Recipients:
			return QVariant(pGroup->getRecipients());
		case SmsGroup::ConfigNb:
			return QVariant(pGroup->getConfigNb());
		case SmsGroup::Error:
			return QVariant(pGroup->getError());
		case SmsGroup::State:
			return QVariant(pGroup->getState());
		case SmsGroup::Events:
			return QVariant(QString::number(pGroup->getNumEvents()));
		case SmsGroup::Msg:
			return QVariant(QString::number(pGroup->getNumMsg()));
		default:
			break;
		}
		break;
	default:
		break;
	}
	return QVariant();
}

QVariant GroupTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
	if((orientation != Qt::Horizontal) || (role != Qt::DisplayRole)) {
		return QVariant();
	}
	switch(section) {
	case SmsGroup::Id:
		return QVariant("ID");
	case SmsGroup::Owner:
		return QVariant("Owner");
	case SmsGroup::Name:
		return QVariant("Name");
	case SmsGroup::Type:
		return QVariant("Group Type");
	case SmsGroup::Scopes:
		return QVariant("Sectors/MainParts");
	case SmsGroup::Recipients:
		return QVariant("Recipients");
	case SmsGroup::ConfigNb:
		return QVariant("Configs");
	case SmsGroup::State:
		return QVariant("State");
	case SmsGroup::Events:
		return QVariant("Events");
	case SmsGroup::Msg:
		return QVariant("Sent");
	case SmsGroup::Error:
		return QVariant("Error");
	default:
		break;
	}
	return QVariant();
}
/*
void GroupTableModel::sort(int column, Qt::SortOrder order)
{
	if(rowDict.isEmpty())
	{
		return;
	}
	mutex.lock();
	SmsGroup::setSortColumn(column);
	SmsGroup::setSortOrder(order);
	qStableSort(rowDict.begin(), rowDict.end(), smsGroupsLessThen);
	for(int row = 0 ; row < rowDict.count() ; row++) {
		rowDict.at(row)->setRow(row);
	}
	mutex.unlock();
	emit dataChanged(index(0, SmsGroup::Owner), index(rowDict.count() - 1, SmsGroup::Msg));
}
*/

	

