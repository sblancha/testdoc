#ifndef DPCONNECTION_H
#define DPCONNECTION_H

#include "VacCtlUtilExport.h"

// Class buffering DP connection requests to be sent to PVSS

#include <qobject.h>
#include <qlist.h>
#include <qmutex.h>

class DpConnectItem
{
public:
	DpConnectItem(const char *dpeIn, bool toConnect) : dpe(dpeIn)
	{
		this->toConnect = toConnect;
	}
	~DpConnectItem() {}

	// Access
	inline const QByteArray &getDpe(void) const { return dpe; };
	inline bool isToConnect(void) { return toConnect; }

private:

	// DPE name
	QByteArray dpe;

	// Flag indicating if DPE shall be connected
	bool	toConnect;
};

class VACCTLUTIL_EXPORT DpConnection : public QObject
{
	Q_OBJECT

public:
	static bool connect(const char *dp, const char *dpe);
	static bool disconnect(const char *dp, const char *dpe);
	static DpConnection &getInstance(void);

	bool getNext(QByteArray &dpe, bool &connect);

	DpConnection();
	DpConnection(const DpConnection &source);
	DpConnection & operator = (const DpConnection &other);
	~DpConnection();

signals:
	void changed(void);

protected:

	bool connect(const QByteArray &dpe);
	bool disconnect(const QByteArray &dpe);

	// The only instance of this class
	static DpConnection	instance;

	// List of DPEs pending connection
	QList<DpConnectItem *> *pDpes;

	// Mutex to protect list from parallel access
	QMutex	mutex;

	// Flag indicating if signal on content change has been sent
	bool	signalSent;
};
 
#endif	// DPCONNECTION_H
