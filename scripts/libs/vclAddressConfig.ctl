// CTRL Library
// created from lhcVacAddressConfig.ctl for QT
// @SAA 18may10

//
// History...
//
// 16dec10 added dyn_uint into CalcDpeDataType() func @SAA
//

//
// VacSetAddress: 
//	
// Function sets device addresses (for one dpe)
//
// @param dpe: in, dpe name
// @param dpeInfo: in, all dpe information after import file parse
// @param active: in, set dpe address to acrive state (true) or not (false)
// @param exceptionInfo: out, contain errors if exist
//
// @return NONE
//
void VacSetAddress( string dpe, dyn_anytype dpeInfo, bool active, dyn_string &exceptionInfo )
{
  dyn_anytype config;

  switch( dpeInfo[VAC_DPE_ADDRESS_TYPE] ) 
  {
  case fwPeriphAddress_TYPE_CMW: // look fwPeriphAddressCMW.pnl			
     // MDF: in feb08 @SAA	

/*
  @SAA: see func _fwPeriphAddressCMW_set

1       driverNum = addressConfig[FW_PARAMETER_FIELD_DRIVER];
2	addressReference = addressConfig[fwPeriphAddress_REFERENCE];
3	addressSubindex = addressConfig[fwPeriphAddress_CMW_SUBINDEX];
4	mode = addressConfig[fwPeriphAddress_CMW_MODE]; //then be transformated 
5	startingTime = addressConfig[fwPeriphAddress_CMW_START];
6	intervalTime = addressConfig[fwPeriphAddress_CMW_INTERVAL];
7	dataType = addressConfig[fwPeriphAddress_DATATYPE]; at first get
8	active = addressConfig[fwPeriphAddress_ACTIVE];
9	bLowLevel = addressConfig[fwPeriphAddress_CMW_LOWLEVEL];
10	typeDriver = addressConfig[fwPeriphAddress_TYPE];
11	pollGroup = addressConfig[fwPeriphAddress_CMW_POLL_GROUP];
...
... be transformated
12	addressConfig[fwPeriphAddress_DIRECTION] = direction_CMW.number; //then be transformated
13	addressConfig[fwPeriphAddress_DATATYPE] = transformation_CMW.selectedPos; //then be transformated
14	addressConfig[fwPeriphAddress_CMW_MODE] =mode_CMW.number;
*/  
            
    //8 (active)
    // ??? fwPeriphAddress_ACTIVE or FW_PARAMETER_FIELD_ACTIVE
    //config[FW_PARAMETER_FIELD_ACTIVE] = active;
    config[fwPeriphAddress_ACTIVE] = active;

    //1 (driverNum)
    // ??? fwPeriphAddress_DRIVER_NUMBER or FW_PARAMETER_FIELD_DRIVER
    config[FW_PARAMETER_FIELD_DRIVER] = dpeInfo[VAC_DPE_ADDRESS_DRV_NUM]; 
    //config[fwPeriphAddress_DRIVER_NUMBER] = dpeInfo[VAC_DPE_ADDRESS_DRV_NUM];
          
    //2 (addressReference)
    // ??? fwPeriphAddress_REFERENCE or FW_PARAMETER_FIELD_ADDRESS
    //config[FW_PARAMETER_FIELD_ADDRESS] = dpeInfo[VAC_DPE_ADDRESS_REFERENCE]; 
    config[fwPeriphAddress_REFERENCE] = dpeInfo[VAC_DPE_ADDRESS_REFERENCE];
                                          
    //10 (typeDriver)
    // ??? FW_PARAMETER_FIELD_COMMUNICATION or fwPeriphAddress_TYPE
    //config[FW_PARAMETER_FIELD_COMMUNICATION] = dpeInfo[VAC_DPE_ADDRESS_TYPE];
    config[fwPeriphAddress_TYPE] = fwPeriphAddress_TYPE_CMW;

    //3 (addressSubindex) address subindex CMW [int]
    config[fwPeriphAddress_CMW_SUBINDEX] = 0; // LIK 11.04.2008 change to (=0)
          
    //9 (bLowLevel)
    // ??? fwPeriphAddress_CMW_LOWLEVEL or FW_PARAMETER_FIELD_LOWLEVEL
    //config[FW_PARAMETER_FIELD_LOWLEVEL] = dpeInfo[VAC_DPE_ADDRESS_OLD_NEW]; 
    config[fwPeriphAddress_CMW_LOWLEVEL]= dpeInfo[VAC_DPE_ADDRESS_OLD_NEW];

    //5 (startingTime)
    // already 0 @SAA???; look fwPeriphAddressCMW.pnl
    config[fwPeriphAddress_CMW_START] = 0;

    //12 (direction): 0, 1, 2 be transformated
    config[fwPeriphAddress_DIRECTION] = dpeInfo[VAC_DPE_ADDRESS_DIRECTION];

    //11 (pollGroup)
    // fwPeriphAddress_CMW_POLL_GROUP <- dpeInfo[VAC_DPE_ADDRESS_GROUP]
    // fwPeriphAddress_CMW_INTERVAL
    config[fwPeriphAddress_CMW_POLL_GROUP]="";
    
    //6 (intervalTime)	
    config[fwPeriphAddress_CMW_INTERVAL]=0;
    
    //7 (dataType): 1...9, will be transformate
    // by func CMWDpeAddressModeDataType_setTransformation->fwPeriphAddressCMW_setTransformation
    // ??? fwPeriphAddress_DATATYPE or FW_PARAMETER_FIELD_DATATYPE
    config[fwPeriphAddress_DATATYPE] = CalcDpeDataType( dpeInfo[VAC_DPE_ADDRESS_TYPE],
			dpeInfo[VAC_DPE_ADDRESS_TRANSTYPE], exceptionInfo );
    if( dynlen( exceptionInfo ) >= 1 ) { DebugN("vclAddressConfig:VacSetAddress() ERROR!!!"); return; }
            
    CMWDpeAddressModeDataType_setTransformation(
                        config[fwPeriphAddress_DIRECTION],    //12 (direction) be transformated
                        config[fwPeriphAddress_CMW_MODE],     //4,14 (mode) be transformated; LIK 11.04.2008 input value set to (=0) see func internal
                        config[fwPeriphAddress_DATATYPE],     //7->13 (dataType) be transformated
                        config[fwPeriphAddress_CMW_LOWLEVEL], //9 (bLowLevel) not be transformated
                        dpe, dpeInfo );
    
  break;
		
  case fwPeriphAddress_TYPE_OPCCLIENT:			

		config[FW_PARAMETER_FIELD_COMMUNICATION] = dpeInfo[VAC_DPE_ADDRESS_TYPE];
		config[FW_PARAMETER_FIELD_ACTIVE] = active; // can be used for S7 connection addressed where IP not defined
		config[FW_PARAMETER_FIELD_DRIVER] = dpeInfo[VAC_DPE_ADDRESS_DRV_NUM];
		CalcDpeAddressMode( dpeInfo[VAC_DPE_ADDRESS_TYPE],
                                    dpeInfo[VAC_DPE_ADDRESS_DIRECTION],
                                    config[FW_PARAMETER_FIELD_MODE] );
		config[FW_PARAMETER_FIELD_DATATYPE] = CalcDpeDataType( dpeInfo[VAC_DPE_ADDRESS_TYPE],
			dpeInfo[VAC_DPE_ADDRESS_TRANSTYPE], exceptionInfo );
		if( dynlen( exceptionInfo ) >= 1 ) return;	
		config[FW_PARAMETER_FIELD_LOWLEVEL] = dpeInfo[VAC_DPE_ADDRESS_OLD_NEW];

		// Only for OPC
		config[FW_PARAMETER_FIELD_ADDRESS] = dpeInfo[VAC_DPE_ADDRESS_REFERENCE];
		config[fwPeriphAddress_OPC_SERVER_NAME] = dpeInfo[VAC_DPE_ADDRESS_CONNECT];
		config[fwPeriphAddress_OPC_GROUP_IN] = dpeInfo[VAC_DPE_ADDRESS_GROUP];
		config[fwPeriphAddress_OPC_GROUP_OUT] = dpeInfo[VAC_DPE_ADDRESS_GROUP];
		
  break;
		
  case fwPeriphAddress_TYPE_S7:

		config[FW_PARAMETER_FIELD_COMMUNICATION] = dpeInfo[VAC_DPE_ADDRESS_TYPE];
		config[FW_PARAMETER_FIELD_ACTIVE] = active; // can be used for S7 connection addressed where IP not defined
		config[FW_PARAMETER_FIELD_DRIVER] = dpeInfo[VAC_DPE_ADDRESS_DRV_NUM];
		CalcDpeAddressMode( dpeInfo[VAC_DPE_ADDRESS_TYPE],
                                    dpeInfo[VAC_DPE_ADDRESS_DIRECTION],
                                    config[FW_PARAMETER_FIELD_MODE] );
		config[FW_PARAMETER_FIELD_DATATYPE] = CalcDpeDataType( dpeInfo[VAC_DPE_ADDRESS_TYPE],
			dpeInfo[VAC_DPE_ADDRESS_TRANSTYPE], exceptionInfo );
		if( dynlen( exceptionInfo ) >= 1 ) return;	
		config[FW_PARAMETER_FIELD_LOWLEVEL] = dpeInfo[VAC_DPE_ADDRESS_OLD_NEW];

  config[FW_PARAMETER_FIELD_INTERVAL] = 1000;  // Some dummy value, see https://its.cern.ch/jira/browse/ENS-16623

		// Only for S7
		config[FW_PARAMETER_FIELD_ADDRESS] = dpeInfo[VAC_DPE_ADDRESS_CONNECT] + "." + dpeInfo[VAC_DPE_ADDRESS_REFERENCE];
		config[fwPeriphAddress_S7_POLL_GROUP] = "_" + dpeInfo[VAC_DPE_ADDRESS_GROUP];
		
  break;

  case "":  // 14.06.2012 L.Kopylov: process case when DPE address has been removed
    break;

  default:
    fwException_raise( exceptionInfo, "ERROR",
			"VacSetAddress: Unsupported address type <" + dpeInfo[VAC_DPE_ADDRESS_TYPE] + "> for <" + dpe + ">", "" );		
    return;
  }

  // set ADDRESS
 switch( dpeInfo[VAC_DPE_ADDRESS_TYPE] )
 {
	case fwPeriphAddress_TYPE_CMW:
          	fwPeriphAddress_set( dpe, config, exceptionInfo, TRUE); // look fwPeriphAddressCMW.pnl 
		break;
	case fwPeriphAddress_TYPE_OPCCLIENT:			
		fwPeriphAddress_set( dpe, config, exceptionInfo, false );
		break;
	case fwPeriphAddress_TYPE_S7:
		// LIK 12.06.2007 Call specific function for S7 because it is much faster than generic one
		_fwPeriphAddressS7_set( dpe, config, exceptionInfo, false );
		break;
  case "":  // 14.06.2012 L.Kopylov: process case when DPE address has been removed
    fwPeriphAddress_delete(dpe, exceptionInfo);
    break;
 }
	
 if( dynlen( exceptionInfo ) >= 1 )
 {
   DebugN( "vclAddressConfig:VacSetAddress() - ERROR set address for dpe", dpe, exceptionInfo );
 }

}
//
// VacCompareDpeAddresses:
//	
// Function compare dpe addresses
//
// @param dpe: in, dpe name
// @param config: in, parameter from PVSS DB - from fwPeriphAddress_get function
// @param dpeInfo: in, all dpe information after import file parse
// @param exceptionInfo: out, contain errors if exist
//
// @return
//		true - if addresses different, false another case
//
bool VacCompareDpeAddresses( string dpe, dyn_anytype config, dyn_anytype dpeInfo, dyn_string &exceptionInfo )
{
  int addressMode, address_trans;
  int addressDirection; bool cmw_lowlevel; // Used only for CMW case
  
  //
  // get ADDRESS data type (TRANSTYPE)
  // it is compare different for CMW
  //
  // Check added by L.Kopylov to identify: address has disappeared for DPE
  if(dpeInfo[VAC_DPE_ADDRESS_TYPE] == "")
  {
    return config[FW_PARAMETER_FIELD_COMMUNICATION] != "";
  }
  address_trans = CalcDpeDataType( dpeInfo[VAC_DPE_ADDRESS_TYPE], dpeInfo[VAC_DPE_ADDRESS_TRANSTYPE], exceptionInfo );
  if( dynlen( exceptionInfo ) >= 1 )
  {
    DebugTN("CalcDpeDataType(" + dpe + ") failed:", exceptionInfo);
     return true;
  }

  //
  // compare ADDRESS TYPE
  //
  if( config[FW_PARAMETER_FIELD_COMMUNICATION] != dpeInfo[VAC_DPE_ADDRESS_TYPE] )
  {  
    DebugN("DPE_ADDRESS_TYPE: fromDB, fromImport, dpe",config[FW_PARAMETER_FIELD_COMMUNICATION],dpeInfo[VAC_DPE_ADDRESS_TYPE],dpe);
    return true;
  }
  //
  // compare DRV_NUM
  //
  if( config[FW_PARAMETER_FIELD_DRIVER] != dpeInfo[VAC_DPE_ADDRESS_DRV_NUM] )
  {
    DebugN("ADDRESS_DRV_NUM: fromDB, fromImport, dpe",config[FW_PARAMETER_FIELD_DRIVER],dpeInfo[VAC_DPE_ADDRESS_DRV_NUM],dpe);
    return true;
  }
  //
  // compare LOWLEVER or OLD_NEW
  //
  if( config[FW_PARAMETER_FIELD_LOWLEVEL] != dpeInfo[VAC_DPE_ADDRESS_OLD_NEW] )
  {
    DebugN("ADDRESS_OLD_NEW: fromDB, fromImport, dpe",config[FW_PARAMETER_FIELD_LOWLEVEL],dpeInfo[VAC_DPE_ADDRESS_OLD_NEW],dpe);
    return true;
  }
	
  // continue analyze depends from address type
  switch( dpeInfo[VAC_DPE_ADDRESS_TYPE] )
  {
  // -------------- OPC ---------------
  case fwPeriphAddress_TYPE_OPCCLIENT:
    
    if( config[FW_PARAMETER_FIELD_DATATYPE] != address_trans )
    {
      DebugN("address_trans: fromDB, fromImport, dpe",config[FW_PARAMETER_FIELD_DATATYPE],address_trans,dpe);
      return true;  
    }
    
    if( config[FW_PARAMETER_FIELD_ADDRESS] != dpeInfo[VAC_DPE_ADDRESS_REFERENCE] )
    {
      return true;
    }
    
    if( config[fwPeriphAddress_OPC_SERVER_NAME] !=  dpeInfo[VAC_DPE_ADDRESS_CONNECT] )
    {
      return true;
    }

    CalcDpeAddressMode( dpeInfo[VAC_DPE_ADDRESS_TYPE],dpeInfo[VAC_DPE_ADDRESS_DIRECTION],addressMode );
    if( config[FW_PARAMETER_FIELD_MODE] != addressMode )
    {
      return true;
    }
    
    if( config[FW_PARAMETER_FIELD_MODE] == DPATTR_ADDR_MODE_OUTPUT_SINGLE )
    {
      if( config[fwPeriphAddress_OPC_GROUP_OUT] != dpeInfo[VAC_DPE_ADDRESS_GROUP] )
          return true;
    }
    else
    {
      if( config[fwPeriphAddress_OPC_GROUP_IN] != dpeInfo[VAC_DPE_ADDRESS_GROUP] )
        return true;
    }
    
    break;
		
  // -------------- S7 ---------------
  case fwPeriphAddress_TYPE_S7:
    
    if( config[FW_PARAMETER_FIELD_DATATYPE] != address_trans )
    {
      DebugN("address_trans: fromDB, fromImport, dpe",config[FW_PARAMETER_FIELD_DATATYPE],address_trans,dpe);
      return true;  
    }
        
    CalcDpeAddressMode( dpeInfo[VAC_DPE_ADDRESS_TYPE],dpeInfo[VAC_DPE_ADDRESS_DIRECTION],addressMode );
    if( config[FW_PARAMETER_FIELD_MODE] != addressMode )
    {
      return true;
    }
    
    if( config[FW_PARAMETER_FIELD_ADDRESS] != 
        ( dpeInfo[VAC_DPE_ADDRESS_CONNECT] + "." + dpeInfo[VAC_DPE_ADDRESS_REFERENCE] ) )
    {
      return true;
    }

    // L.Kopylov 31.03.2014: in PVSS 3.11 config[fwPeriphAddress_S7_POLL_GROUP] is not pure
    // DP name like it was in 3.8; this is not even a string
    // L.Kopylov 31.03.2015: do not check poll group for output addresses
    if(addressMode != DPATTR_ADDR_MODE_OUTPUT)
    {
      string pollGroup = config[fwPeriphAddress_S7_POLL_GROUP];
      if(pollGroup != "")
      {
        if(dpExists(pollGroup))
        {
          pollGroup = dpSubStr(pollGroup, DPSUB_DP);
        }
      }
      if(pollGroup != ( "_" + dpeInfo[VAC_DPE_ADDRESS_GROUP] ) )
      {
        return true;
      }
    }
    
    break;
   
  // -------------- CMW ---------------
  case fwPeriphAddress_TYPE_CMW:
    /*
    if(dpe == "CIB_TZ76_U7_B1.Test1") // Debug notes
    {
      DebugN("vclAddressConfig:VacCompareDpeAddresses() dpe",dpe);
    }
    if(dpe == "CIB_TZ76_U7_B2.Test1") // Debug notes
    {
      DebugN("vclAddressConfig:VacCompareDpeAddresses() dpe",dpe);
    }
    */
    CMWDpeAddressModeDataType_setTransformation(
                           addressDirection,
                           addressMode, // @SAA not need to compare, not exist in import file
                           address_trans, // can be transformated here
                           cmw_lowlevel,
                           dpe, dpeInfo );
    // compare (address direction from DB) WITH (transformated address direction used to set address)
    // LIK - simple approach in order not to deal with complex logic
    // adding 1000 or 2000 on address setting
    // see (CMW_TRANS_OFFSET = 1000) in file fwPeripfAddressCMW.ctl
    //while(config[FW_PARAMETER_FIELD_DATATYPE] > 1000)
    // not need so it be transformated by CMWDpeAddressModeDataType_setTransformation
    //{
    //  config[FW_PARAMETER_FIELD_DATATYPE] -= 1000; // not need anymore @SAA feb11
    //}    
    if( config[FW_PARAMETER_FIELD_DATATYPE] != address_trans )
    {
      DebugN("address_trans: fromDB, fromImport, dpe",config[FW_PARAMETER_FIELD_DATATYPE],address_trans,dpe);
      return true;  
    }
    
    // compare (address direction from DB) WITH (transformated address direction used to set address)
    if( config[fwPeriphAddress_DIRECTION] != addressDirection )
    {
      DebugN("dpe",dpe,
             "address direction: From import file, Transformated, From DB:",
             dpeInfo[VAC_DPE_ADDRESS_DIRECTION],addressDirection,(int)config[fwPeriphAddress_DIRECTION]);
      return true;
    }                  
    // compare  ADDRESS_REFERENCE     
    if( config[FW_PARAMETER_FIELD_ADDRESS] != dpeInfo[VAC_DPE_ADDRESS_REFERENCE] )
    {
      return true;
    }
    break;

  default:
    DebugN( "INTERNAL ERROR - wrong case in function VacCompareDpeAddresses" );
    return true; // in wrong case suppose that addresses are different
  } // switch end
			
  return false; // addresses are equal
}
//
// CalcDpeAddressMode: 
//	
// Calculate dpe address mode
//
//  string @addressType:  in, address type (OPC or S7 or CMW)
//  int @addressDirection: in, address direction
//
// return NONE
//
//
void CalcDpeAddressMode( string addressType,
                         int addressDirection,
                         int &address_mode )
{

  switch( addressType )
  {

  case fwPeriphAddress_TYPE_OPCCLIENT:
    if( addressDirection == 1 )	// In
    {
      address_mode = DPATTR_ADDR_MODE_INPUT_SPONT;
    }
    else if( addressDirection == 2 )	// Out-individual
    {
      address_mode = DPATTR_ADDR_MODE_OUTPUT_SINGLE;
    }
    else
    {
      DebugN( "CalcDpeAddressMode: Unsupported address direction <" + addressDirection + "> for " + addressType, "" );
    }
  break;
		
  case fwPeriphAddress_TYPE_S7:
    if( addressDirection == 1 )	// In
    {
      address_mode = DPATTR_ADDR_MODE_INPUT_POLL;
    }
    else if( addressDirection == 2 )	// Out
    {
      address_mode = DPATTR_ADDR_MODE_OUTPUT;
    }
    else
    {
      DebugN( "CalcDpeAddressMode: Unsupported address direction <" + addressDirection + "> for " + addressType, "" );
    }
    break;
                		
  default:
    DebugN( "CalcDpeAddressMode: Unsupported address type <" + addressType + ">", "" );
    
  }
}
// only for case fwPeriphAddress_TYPE_CMW:
void CMWDpeAddressModeDataType_setTransformation(
                         int &addressDirection,
                         int &address_mode,
                         int &cmw_trans,
                         bool &cmw_lowlevel,
                         string dpe,
                         dyn_anytype dpeInfo )
{
  
  addressDirection = (dpeInfo[VAC_DPE_ADDRESS_DIRECTION] == 1 ? 0 : 1);
  address_mode =  0; // LIK 11.04.2008 change to (=0)
  
  _fwPeriphAddressCMW_setTransformation( 
      true, // @SAA add 1000 if false... or add 2000 if true to cmw_trans (look in function);see (CMW_TRANS_OFFSET = 1000) in file fwPeripfAddressCMW.ctl
      addressDirection, // be transformated
      address_mode, // be transformated
      cmw_trans, // @SAA DATATYPE add 1000 or 2000 see upper
      cmw_lowlevel,
      dpe);
                
  // very very old CODE ... changed so func _fwPeriphAddressCMW_setTransformation exist!!! @SAA
  //if( addressDirection == 1 )	// In
  //{
  //	return DPATTR_ADDR_MODE_INPUT_SPONT;
  //}
  //else if( addressDirection == 2 )	// Out
  //{
  //	return DPATTR_ADDR_MODE_OUTPUT;
  //}
  //else
  //{
  //	fwException_raise( exceptionInfo, "ERROR", "Unsupported address direction <" + addressDirection + "> for CMW" );
  //		
  //	return FAILURE;
  //}
}
//
// CalcDpeDataType: 
//	
// Calculate dpe data type
//
// @param	addressType:	in, address type (OPC or S7)
// @param transType:		in, address transaction type
// @param exceptionInfo:	out, contain errors if exist
//
// @return dpe data type or FAILURE if error
//
int CalcDpeDataType( string addressType, string transType, dyn_string &exceptionInfo )
{	
	switch( addressType )
	{
	case fwPeriphAddress_TYPE_CMW:
		switch( transType )
		{
		case "unknown":
			return 1;
			break;
		case "longlong":
			return 2;
			break;
		case "integer32":
		case "uinteger32":
		case "dyn_uint":
	 		return 3;
			break;
		case "integer16":
		case "uinteger16":
      case "dyn_short":
			return 4;
			break;
		case "float64":
			return 5;
			break;
		case "float32":
      case "dyn_float":
			return 6;
			break;
		case "boolean":
      case "dyn_bool":
			return 7;
			break;
		case "string":
      case "string24Chars":   
			return 8;
			break;
		case "char":
			return 9;
			break;
		default:
			return 1; // use "unknown" in this case
			break;
		}
		break;
		
	case fwPeriphAddress_TYPE_OPCCLIENT:
		switch( transType )
		{
		case "boolean":
			return 486;
			break;
		case "integer16":
			return 481;
			break;
		case "integer32":
	 		return 482;
			break;
		case "float32":
			return 484;
			break;
		case "float64":
			return 485;
			break;
		case "string":
      case "string24Chars":      
			return 487;
			break;
		case "uchar":
			return 483;
			break;
		case "uinteger16":
			return 488;
			break;
		case "uinteger32":
			return 489;
			break;
		default:
			fwException_raise( exceptionInfo, "ERROR",
				"CalcDpeDataType(): Wrong data type <" + transType + "> for address type <" + addressType + ">", "" );
			return FAILURE;
		}
		break;
		
	case fwPeriphAddress_TYPE_S7:
		switch( transType )
		{
		case "boolean":
			return 706;
			break;
		case "integer16":
			return 701;
			break;
		case "integer32":
	 		return 702;
			break;
		case "float32":
			return 705;
			break;
		case "float64":
			return 705;
			break;
		case "string":
      case "string24Chars":      
			return 707;
			break;
    case "byte":  
		case "uchar":
			return 704;
			break;
		case "uinteger16":
			return 703;
			break;
		case "uinteger32":
			return 708;
			break;
		default:
			fwException_raise( exceptionInfo, "ERROR",
				"CalcDpeDataType(): Wrong data type <" + transType + "> for address type <" + addressType + ">", "" );
			return FAILURE;
		}
		break;
		
	default:
		fwException_raise( exceptionInfo, "ERROR", "CalcDpeDataType(): Unsupported address type <" + addressType + ">", "" );
		return FAILURE;
	}
}

/*
main()
{

}
*/
