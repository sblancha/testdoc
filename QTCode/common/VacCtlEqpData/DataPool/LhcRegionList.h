#ifndef LHCREGIONLIST_H
#define LHCREGIONLIST_H

#include "VacCtlEqpDataExport.h"

// Specific QList holding LhcRegion classes. The purpose for introducing
// this class is to reimplement compareItems() method of QPtrList
// It is supposed that instances of this class ONLY keep references to LhcRegion

#include <qlist.h>

template<class type>
class VACCTLEQPDATA_EXPORT LhcRegionList : public QList<type>
{
protected:
	virtual int compareItems(QPtrCollection::Item item1, QPtrCollection::Item item2);

};

#endif	// LHCREGIONLIST_H
