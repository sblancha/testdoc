//	Implementation of SynView class
/////////////////////////////////////////////////////////////////////////////////

#include "SynView.h"

#include "VacMainView.h"

#include "DataPool.h"
#include "BeamLine.h"
#include "Sector.h"
#include "SectorDrawPart.h"
#include "DebugCtl.h"

#include <QPainter>
#include <QPaintEvent>

#include <math.h>	// for atan2()

#include <stdio.h>

// Space between top of draw area and first beam line
#define	TOP_MARGIN			(5)

// Space between last beam line and bottom of draw area
#define	BOTTOM_MARGIN		(5)

// Space between left side of draw area and start of first line
#define	LEFT_MARGIN			(10)

// Space between end of last line and right side of draw area
#define	RIGHT_MARGIN		(10)

// Vertical space between two neighbour lines
#define	INTER_LINE_SPACE	(5)

#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#endif

#ifndef min
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif

/*
**	FUNCTION
**		Create new instance of synoptic view
**
**	PARAMETERS
**		parent			- Pointer to parent widget
**		pFirstSector	- Pointer to first sector
**		pLastSector		- Pointer to last sector
**		mode			- Data acquisition mode for synoptic
**
**	RETURNS
**		Pointer to new instance of synoptic view,
**		NULL in case of error.
**
**	CAUTIONS
**		None
*/
SynView *SynView::create(QWidget *parent, Sector *pFirstSector, Sector *pLastSector,
	DataEnum::DataMode mode)
{
	// LEIR BTL specific: if one of sectors has draw part on survey line E01 - 
	// result produced by 'generic' algorithm is not good. In this case use special
	// view designed especially for this area
	DataPool &pool = DataPool::getInstance();
	bool isSpecialLeirBtl = false;
	Sector *pSector = pFirstSector;
	do
	{
		QList<SectorDrawPart *> &drawParts = pSector->getDrawParts();
		for(int drawPartIdx = 0 ; drawPartIdx < drawParts.count() ; drawPartIdx++)
		{
			SectorDrawPart *pPart = drawParts.at(drawPartIdx);
			QList<BeamLinePart *> parts = pPart->getLine()->getParts();
			for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
			{
				BeamLinePart *pLinePart = parts.at(partIdx);
				if(!strcmp(pLinePart->getName(), "E01"))
				{
					isSpecialLeirBtl = true;
					break;
				}
			}
			if(isSpecialLeirBtl)
			{
				break;
			}
		}
		if(isSpecialLeirBtl)
		{
			break;
		}
		if(pSector == pLastSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);

	// Create view
	SynView *pView = NULL;
	/* TODO ... or not TODO?
	if(isSpecialLeirBtl)
	{
		pView = SynBtlView::Create( startSectIdx, endSectIdx );
	}
	else
	TODO */
	{
		pView = new SynView(parent);
		pView->mode = mode;
		QList<BeamLine *> &lines = pool.getLines();
		for(int idx = 0 ; idx < lines.count() ; idx++)
		{
			BeamLine *pLine = lines.at(idx);
			SynLine *pSynLine = SynLine::create(pView, pLine, pFirstSector, pLastSector);
			if(pSynLine)
			{
				pSynLine->setMode(mode);
				pView->lines.append(pSynLine);
				connect(pSynLine, SIGNAL(coldexStateChanged(bool, bool)),
					pView, SLOT(coldexStateChange(bool, bool)));
			}
		}
	}
	return pView;
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SynView::SynView(QWidget *parent, Qt::WindowFlags f) :
	SynopticWidget(parent, f)
{
	totalWidth = totalHeight = viewStart = 0;
	pFirstConnLine = pSecondConnLine = NULL;
	mode = DataEnum::Online;
	redrawRequiredForMobile = false;

	//connect signals for clicking sectors
	QObject::connect(this, SIGNAL(mouseDown(int, int, int, int, int, int, int)),
		this, SLOT(synSectorClicked(int, int, int, int, int, int, int)));

	VacMainView *pMainView = VacMainView::getInstance();
	if (pMainView)
	{
		QObject::connect(this, SIGNAL(synSectorMouseDown(int, int, int, int, const char *, bool)),
			pMainView, SIGNAL(synSectorMouseDown(int, int, int, int, const char *, bool)));
	}
}

SynView::~SynView()
{
	while(!lines.isEmpty())
	{
		disconnectSectorSelection(lines.first());
		delete lines.takeFirst();
	}

}

/*
**	FUNCTION
**		Analyze which equipment types will be shown in view
**
**	PARAMETERS
**		allVacTypeMask	- OR'ed bit mask for all vacuum types
**		allCryoEqp		- Array all functional types on isolation vacuum
**							to be shown in this view
**		allBeamEqp		- Array all functional types on beam vacuum
**							to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allCryoEqpMask,
	VacEqpTypeMask &allBeamEqpMask)
{
	allVacTypeMask = 0x0;
	allCryoEqpMask.clear();
	allBeamEqpMask.clear();
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLine *pLine = lines.at(idx);
		pLine->analyzeTypes(allVacTypeMask, allCryoEqpMask, allBeamEqpMask);
	}
}

/*
**	FUNCTION
**		Prepare synoptic content for given equipment type mask
**
**	PARAMETERS
**		mask	- Equipment type mask to be applied
**
**	RETURNS
**		Recommended start of visible area
**
**	CAUTIONS
**		None
*/
int SynView::applyMask(VacEqpTypeMask &mask)
{
	// First fix in all lines equipment for further start calculation
	int idx;
	SynLine *pLine;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		pLine->fixStart(viewStart, mask);
	}

	// analyzeTypes() method must be called BEFORE drawing. However,
	// sometimes draw() method gets called before - probably, the reason
	// is drawing from Paint() or Resize() events of control which are fired
	// in unpredictable order. To make sure analyzeTypes() is called explicitely
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(!pLine->isAnalyzed())
		{
			unsigned dummy1;
			VacEqpTypeMask dummy2, dummy3;
			pLine->analyzeTypes(dummy1, dummy2, dummy3);
		}
	}

	buildView(mask);
	if((!totalWidth) || (!totalHeight))
	{
		return 0;
	}
	QSize mySize(totalWidth, totalHeight);
	setFixedSize(mySize);

	// Finally find recommened start of visible area
	int startMin = -1, startMax = -1;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		int lineMin, lineMax;
		pLine->findRecommendedStart(lineMin, lineMax);
		if(lineMin >= 0)
		{
			if(startMin >= 0)
			{
				if(lineMin > startMin)
				{
					startMin = lineMin;
				}
			}
			else
			{
				startMin = lineMin;
			}
		}
		if(lineMax > 0)
		{
			if(startMax >= 0)
			{
				if(lineMax < startMax)
				{
					startMax = lineMax;
				}
			}
			else
			{
				startMax = lineMax;
			}
		}
	}
	int result = 0;
	if((startMin >= 0) && (startMax >= 0))
	{
		result = (startMin + startMax) / 2;
		if(result < 0)
		{
			result = 0;
		}
	}
	return result;
}

bool SynView::findDevicePosition(Eqp *pEqp, int &position, bool &visible)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLine *pLine = lines.at(idx);
		if(pLine->findDevicePosition(pEqp, position, visible))
		{
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Set start position and width of view: activate icons which are
**		visible, deactivate icons which are not visible
**
**	PARAMETERS
**		viewStart	- Start position of visible part
**		viewWidth	- width of visible part
**		dragging	- true if this method is called from scroll bar dragging
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::setViewStart(int viewStart, int viewWidth, bool dragging)
{
	this->viewStart = viewStart;
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLine *pLine = lines.at(idx);
		pLine->setViewStart(viewStart, viewWidth, dragging);
	}
}

/*
**	FUNCTION
**		Check if tooltip shall be shown for given mouse pointer position
**
**	PARAMETERS
**		point	- Mouse pointer position
**		text	- Variable where tool tip text shall be written
**		rect	- Variable where boundaries for returned tooltip text shall
**					be written
**
**	RETURNS
**		true	- If tooltip text shall be shown for given mouse position;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SynView::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLine *pLine = lines.at(idx);
		if(pLine->getToolTip(point, text, rect))
		{
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Override Widget's paintEvent() method: draw all background lines
**
**	PARAMETERS
**		pEvent	- Pointer to event data
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::paintEvent(QPaintEvent *pEvent)
{
	const QRect &rect = pEvent->rect();
	if(rect.isNull())
	{
		return;
	}
	QPainter painter(this);
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLine *pLine = lines.at(idx);
		pLine->draw(painter, rect.left(), rect.right());
	}
	
	// Finally draw inter-line connect(s) if any
	if(pFirstConnLine)
	{
		painter.setPen(QPen(VacMainView::getSynPipeColor(), VacMainView::getSynPipeWidth()));
		painter.drawLine(interLineX, pFirstConnLine->getY(), interLineX, pSecondConnLine->getY());
	}
	
}

/*
**	FUNCTION
**		Rebuild geometries of all individual lines if necessary,
**		then build overall geometry of the whole view.
**
**	PARAMETERS
**		mask	- New equipment type mask
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::buildView(VacEqpTypeMask &mask)
{
	SynLine *pLine;
	int idx;

	// Check if new device lists shall be built
	if((!eqpMask.equals(mask)) || redrawRequiredForMobile)
	{
		redrawRequiredForMobile = false;
		eqpMask = mask;
		totalWidth = totalHeight = 0;
		for(idx = 0 ; idx < lines.count() ; idx++)
		{
			pLine = lines.at(idx);
			pLine->build(eqpMask, false);
		}

		for(idx = 0 ; idx < lines.count() ; idx++)
		{
			pLine = lines.at(idx);
			pLine->addAlarmIcons();
			pLine->addSectorNameSpace(true);
		}
	}
	if(DebugCtl::isSynoptic())
	{
		#ifdef Q_OS_WIN
			dump("C:\\SynInit.txt");
		#else
			dump("/home/kopylov/SynInit.txt");
		#endif
	}
	if(!totalWidth)
	{
		buildGeometry();
		// TODO adjustViewStart();
	}
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		pLine->finishGeometry();
	}
	if(DebugCtl::isSynoptic())
	{
		#ifdef Q_OS_WIN
			dump("C:\\SynConnect.txt");
		#else
			dump("/home/kopylov/SynConnect.txt");
		#endif
	}
}

/*
**	FUNCTION
**		Build overall geometry of the whole synoptic: summarize geometries
**		of individual lines, establish connection between lines.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::buildGeometry(void)
{
	buildVertRelations();
	buildHorRelations();

	// Line connecting two parts of LTE-LTL sector
	pFirstConnLine = NULL;
	int	connStartX1, connStartX2, connEndX1, connEndX2, idx;
	SynLine *pLine;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		const char *pOtherName = pLine->havePartAway(connStartX1, connEndX1);
		if(!pOtherName)
		{
			continue;
		}
		for(int otherIdx = 0 ; otherIdx < lines.count() ; otherIdx++)
		{
			SynLine *pOtherLine = lines.at(otherIdx);
			if(pOtherLine == pLine)
			{
				continue;
			}
			if(!strcmp(pOtherLine->getBeamLine()->getName(), pOtherName))
			{
				pOtherName = pOtherLine->havePartAway(connStartX2, connEndX2);
				if(pOtherName)
				{
					pFirstConnLine = pLine;
					pSecondConnLine = pOtherLine;
					connStartX1 = max(connStartX1, connStartX2);
					connEndX1 = min(connEndX1, connEndX2);
					interLineX = connStartX1 + (int)((connEndX1 - connStartX1) * 0.7);
					for(int moveIdx = 0 ; moveIdx < lines.count() ; moveIdx++)
					{
						SynLine *pMoveLine = lines.at(moveIdx);
						if((pMoveLine == pLine) || (pMoveLine == pOtherLine))
						{
							continue;
						}
						pMoveLine->movePartAfter(interLineX, 20);
					}
				}
				break;
			}
		}
		break;	// One connecting line only
	}

	checkForOverlap();

	// Not required for synoptic view, but essential for subclass - sector view
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		pLine->buildSectorIcons();
	}

	// Calculate total sizes of drawing, move all lines vertically to fit to limits
	if(!lines.count())
	{
		return;
	}

	// Extra icons for external targets of alarms
	/*
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		pLine->addAlarmIcons();
	}
	*/

	// Final vertical positions
	pLine = lines.at(0);
	int top = pLine->getY() - pLine->getAscent();
	int bottom = pLine->getY() + pLine->getDescent();
	int left = pLine->getStartX();
	int right = pLine->getEndX();
	for(idx = 1 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(top > (pLine->getY() - pLine->getAscent()))
		{
			top = pLine->getY() - pLine->getAscent();
		}
		if(bottom < (pLine->getY() + pLine->getDescent()))
		{
			bottom = pLine->getY() + pLine->getDescent();
		}
		if(left > pLine->getStartX())
		{
			left = pLine->getStartX();
		}
		if(right < pLine->getEndX())
		{
			right = pLine->getEndX();
		}
	}
	int deltaX = LEFT_MARGIN - left;
	int deltaY = TOP_MARGIN - top;
	if(deltaX || deltaY)
	{
		for(idx = 0 ; idx < lines.count() ; idx++)
		{
			pLine = lines.at(idx);
			if(deltaX)
			{
				QList<ChildSynLine *> *pLineChildren = pLine->moveHorizontally(deltaX, true);
				if(pLineChildren)
				{
					while(!pLineChildren->isEmpty())
					{
						delete pLineChildren->takeFirst();
					}
					delete pLineChildren;
				}
			}
			if(deltaY)
			{
				pLine->moveVertically(deltaY, true);
			}
		}
		interLineX += deltaX;
	}

	for (idx = 0; idx < lines.count(); idx++)
	{
		pLine = lines.at(idx);
		pLine->setSectorGeometry();
		connectSectorSelection(pLine);
	}

	totalHeight = (bottom - top) + TOP_MARGIN + BOTTOM_MARGIN;
	totalWidth = (right - left) + LEFT_MARGIN + RIGHT_MARGIN;
}

/*
**	FUNCTION
**		Calculate 'final' vertical positions for all lines.
**
**	PARAMETERS
**		None.
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::buildVertRelations(void)
{
	SynLine *pLine;
	int idx;

	// Different algorithm is used if all beam lines have manually specified
	// (in beam line configuration file) vertical position.
	// Note if at least for one line vertical position is not specified manually -
	// manual positions will not be used. All manual positions must be >= 1
	bool manual = true;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(pLine->getBeamLine()->getVerticalPos() < 1)
		{
			manual = false;
			break;
		}
	}
	if(manual)
	{
		manualVertRelations();
		return;
	}

	// No manually specified vertical positions - try to position lines automatically
	int nToProcess, prevToProcess = lines.count();
	do
	{
		nToProcess = 0;
		for(idx = 0 ; idx < lines.count() ; idx++)
		{
			pLine = lines.at(idx);
			if(pLine->isVertReady())
			{
				continue;
			}
			const Eqp *pConnEqp = pLine->getStartEqp() ? pLine->getStartEqp() : pLine->getEndEqp();
			if(DebugCtl::isSynoptic())
			{
#ifdef Q_OS_WIN
				qDebug("SynView::buildVertRelations(): line %d pConnEqp %s\n", idx, (pConnEqp ? pConnEqp->getName() : "NULL"));
#else
				printf("SynView::buildVertRelations(): line %d pConnEqp %s\n", idx, (pConnEqp ? pConnEqp->getName() : "NULL"));
				fflush(stdout);
#endif
			}
			if(pConnEqp)
			{
				int parentEqpIdx;
				SynLine *pParentLine = getParentLine(pLine, pConnEqp, parentEqpIdx);
				if(DebugCtl::isSynoptic())
				{
#ifdef Q_OS_WIN
					qDebug("SynView::buildVertRelations(): line %d pParentLine %lX parentEqpIdx %d\n", idx, (unsigned long)pParentLine, parentEqpIdx);
#else
					printf("SynView::buildVertRelations(): line %d pParentLine %lX parentEqpIdx %d\n", idx, (unsigned long)pParentLine, parentEqpIdx);
					fflush(stdout);
#endif
				}
				if(pParentLine && pParentLine->isVertReady())
				{
					int connType = 0, delta = 0;
					if(pLine->isReverse())
					{
						connType = pLine->getStartEqp() ?
								pLine->getBeamLine()->getEndType() : pLine->getBeamLine()->getStartType();
					}
					else
					{
						connType = pLine->getStartEqp() ?
								pLine->getBeamLine()->getStartType() : pLine->getBeamLine()->getEndType();
					}
					switch(connType)
					{
					case BeamLineConnType::RightOut:
					case BeamLineConnType::RightIn:
						delta = pParentLine->getY() + pParentLine->getDescent() +
							pLine->getAscent() + INTER_LINE_SPACE;
						break;
					case BeamLineConnType::LeftOut:
					case BeamLineConnType::LeftIn:
						delta = pParentLine->getY() -
							(pParentLine->getAscent() + pLine->getDescent()) - INTER_LINE_SPACE;
						break;
					}
					pLine->moveVertically(delta, false);
				}
				pLine->setVertReady(true);
			}
			else
			{
				pLine->setVertReady(true);	// Line without parent line(s)
			}
			if(!pLine->isVertReady())
			{
				nToProcess++;
			}
		}
		if(nToProcess == prevToProcess)
		{
			break;	// No progress
		}
		prevToProcess = nToProcess;
	} while(nToProcess);
}

/*
**	FUNCTION
**		Calculate 'final' vertical positions for all lines
**		using vertical positions supplied in beam line configuration file.
**
**	PARAMETERS
**		None.
**
**	RETURNS
**		None
**
**	CAUTIONS
**		This method shall only be called if all beam lines participating
**		in view have verticalPos >= 1
*/
void SynView::manualVertRelations(void)
{
	SynLine *pLine;
	int idx;

	// Find maximum verticalPos
	int	maxPosition = 0;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(pLine->getBeamLine()->getVerticalPos() > maxPosition)
		{
			maxPosition = pLine->getBeamLine()->getVerticalPos();
		}
	}

	// Move lines according to their positions
	int delta = 0;
	for(int pos = maxPosition ; pos > 0 ; pos --)
	{
		// Find offset for new vertical position - maximum ascent of
		// all lines with this position
		int maxAscent = 0,
			maxDescent = 0;
		for(idx = 0 ; idx < lines.count() ; idx++)
		{
			pLine = lines.at(idx);
			if(pLine->getBeamLine()->getVerticalPos() != pos)
			{
				continue;
			}
			if(pLine->getAscent() > maxAscent)
			{
				maxAscent = pLine->getAscent();
			}
			if(pLine->getDescent() > maxDescent)
			{
				maxDescent = pLine->getDescent();
			}
		}
		// Move all lines with current position
		delta += maxAscent;
		for(idx = 0 ; idx < lines.count() ; idx++)
		{
			pLine = lines.at(idx);
			if(pLine->getBeamLine()->getVerticalPos() != pos)
			{
				continue;
			}
			pLine->moveVertically(delta, false);
			pLine->setVertReady(true);
		}
		// Calculate new delta for lines with next positions
		delta += maxDescent + INTER_LINE_SPACE;
	}
}

/*
**	FUNCTION
**		Calculate 'final' horizontal positions for all lines.
**
**	PARAMETERS
**		None.
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::buildHorRelations(void)
{
	int	nToProcess, prevToProcess = lines.count();

	SynLine *pLine;
	int idx;
	do
	{
		nToProcess = 0;
		for(idx = 0 ; idx < lines.count() ; idx++)
		{
			pLine = lines.at(idx);
			if(pLine->isHorReady())
			{
				continue;
			}
			calcHorRelation(pLine);
			if(!pLine->isHorReady())
			{
				nToProcess++;
			}
		}
		if(nToProcess == prevToProcess)
		{
			break;	// No progress
		}
		prevToProcess = nToProcess;
	} while(nToProcess);
}

/*
**	FUNCTION
**		Calculate 'final' horizontal positions for given line
**
**	PARAMETERS
**		pLine	- Pointer to line being processed
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::calcHorRelation(SynLine *pLine)
{
	int		parentEqpIdx;
	bool		ready = true;
	SynLine	*pStartParent = NULL, *pEndParent = NULL;

	if(pLine->getStartEqp())
	{
		pStartParent = getParentLine(pLine, pLine->getStartEqp(), parentEqpIdx);
		if(pStartParent)
		{
			ready = calcConnectGeometry(pLine, pStartParent, parentEqpIdx, true);
		}
	}
	if(!ready)
	{
		return;
	}

	if(pLine->getEndEqp())
	{
		pEndParent = getParentLine(pLine, pLine->getEndEqp(), parentEqpIdx);
		if(pEndParent)
		{
			ready = calcConnectGeometry(pLine, pEndParent, parentEqpIdx, false);
		}
	}
	pLine->setHorReady(ready);

	if(pStartParent && (pStartParent == pEndParent) && pLine->getColdex())
	{
		pStartParent->setColdexLine(pLine);
	}
}

/*
**	FUNCTION
**		Calculate connection geometry of 'child' and 'parent' line for one connection,
**		move/expand lines to fit them together on drawing area
**
**	PARAMETERS
**		pLine			- Pointer to child line
**		pParentLine		- Pointer to parent line
**		parentEqpIdx	- Index of corresponding connection device in parent line
**		isStart			- true if start of child line is processed, false if finish is processed
**
** RETURNS
**		true if connection was processed successfully,
**		false otherwise
**
** CAUTIONS
**		None
*/
bool SynView::calcConnectGeometry(SynLine *pLine, SynLine *pParentLine, int parentEqpIdx, bool isStart)
{
	if(!pParentLine->isHorReady())
	{
		return false;
	}

	int endX = 0;
	if(isStart)
	{
		pLine->setStartConnectX(pParentLine->getEqpX(parentEqpIdx));
		pLine->setStartConnectY(pParentLine->getY());
	}
	else
	{
		endX = pParentLine->getEqpX(parentEqpIdx);
		pLine->setEndConnectX(endX);
		pLine->setEndConnectY(pParentLine->getY());
	}

	// If this is end connection and start connection of this line was already processed
	// (i.e. if line has both start and end connections) - adjust geometry of parent and
	// child lines: either move all equipment of parent line from connection point to
	// the end of line, or make child line longer
	if((!isStart) && pLine->getStartEqp())
	{
		endX -= 10;	// Use 10 pixels offset
		if(endX > pLine->getEndX())	// Child line is not long enough
		{
			pLine->setEndX(endX);
		}
		else	// Child line is too long
		{
			int delta = pLine->getEndX() - endX;
			QList<ChildSynLine *> *pChildren = pParentLine->moveLinePart(parentEqpIdx, delta);
			if(pChildren)
			{
				if(pChildren->count())
				{
					moveChildLines(pLine, delta, pChildren);
				}
				while(!pChildren->isEmpty())
				{
					delete pChildren->takeFirst();
				}
				delete pChildren;
			}
			pLine->setEndConnectX(pLine->getEndConnectX() + delta);
		}
	}
	else
	{
		int delta = 10;	// Use small offset in horizontal direction, alternative was:
					// offset = vertical spacing between lines centers
		if(isStart)
		{
			delta = pLine->getStartConnectX() - pLine->getStartXBeamLine() + delta;
		}
		else
		{
			delta = pLine->getEndConnectX() - pLine->getEndXBeamLine() - delta;
		}
		QList<ChildSynLine *> *pLineChildren = pLine->moveHorizontally(delta, false);
		if(pLineChildren)
		{
			if(pLineChildren->count())
			{
				moveChildLines(pLine, delta, pLineChildren);
			}
			while(!pLineChildren->isEmpty())
			{
				delete pLineChildren->takeFirst();
			}
			delete pLineChildren;
		}
	}
	
	return true;
}

/*
**	FUNCTION
**		It can happen that some child lines overlap after all geometry was calculated.
**		This can happen if two child lines emanating from parent line in the same
**		direction are too close to each other on the screen - such case was observed
**		in LINAC2 of PS complex.
**		This method shall find overlapping child lines and solve the problem by
**		moving lines together with connection point in parent line.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Recently only covers one particular case: two child lines STARTING on
**		parent line
*/
void SynView::checkForOverlap(void)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLine *pLine = lines.at(idx);
		if(pLine->getStartEqp())
		{
			for(int otherIdx = 0 ; otherIdx < lines.count() ; otherIdx++)
			{
				SynLine *pOtherLine = lines.at(otherIdx);
				if(pOtherLine == pLine)
				{
					continue;
				}
				if(!(pOtherLine->getStartEqp() || pOtherLine->getEndEqp()))
				{
					continue;
				}
				if((pOtherLine->getY() - pOtherLine->getAscent()) > (pLine->getY() + pLine->getDescent()))
				{
					continue;
				}
				if((pOtherLine->getY() + pOtherLine->getDescent()) < (pLine->getY() - pLine->getAscent()))
				{
					continue;
				}
				if(pOtherLine->getStartX() > pLine->getEndX())
				{
					continue;
				}
				if(pOtherLine->getEndX() < pLine->getStartX())
				{
					continue;
				}
				// Two lines overlap - decide which line to move
				SynLine *pToMove = NULL,
						*pToStay = NULL;
				int moveType = 0;
				if(DebugCtl::isSynoptic())
				{
#ifdef Q_OS_WIN
					qDebug("SynView::checkForOverlap(%s): check other line %s, startEqp in other line %s\n",
						pLine->getBeamLine()->getName(), pOtherLine->getBeamLine()->getName(),
						(pOtherLine->getStartEqp() ? pOtherLine->getStartEqp()->getName() : "NULL"));
#else
					printf("SynView::checkForOverlap(%s): check other line %s, startEqp in other line %s\n",
						pLine->getBeamLine()->getName(), pOtherLine->getBeamLine()->getName(),
						(pOtherLine->getStartEqp() ? pOtherLine->getStartEqp()->getName() : "NULL"));
					fflush(stdout);
#endif
				}
				if(pOtherLine->getStartEqp())
				{
					pToMove = pLine->getStartConnectX() < pOtherLine->getStartConnectX() ?
						pOtherLine : pLine;
					pToStay = pLine->getStartConnectX() < pOtherLine->getStartConnectX() ?
						pLine : pOtherLine;
					moveType = EqpType::OtherLineStart;
				}
				else	// Other line has end point - see if() above
				{
					pToMove = pLine->getStartConnectX() < pOtherLine->getEndConnectX() ?
						pOtherLine : pLine;
					pToStay = pLine->getStartConnectX() < pOtherLine->getEndConnectX() ?
						pLine : pOtherLine;
					moveType = pToMove == pLine ? EqpType::OtherLineStart : EqpType::OtherLineEnd;
				}
				if(pToMove->isReverse())
				{
					if(DebugCtl::isSynoptic())
					{
#ifdef Q_OS_WIN
						qDebug("SynView::checkForOverlap(%s): line to move is reverse\n",
							pLine->getBeamLine()->getName());
#else
						printf("SynView::checkForOverlap(%s): line to move is reverse\n",
							pLine->getBeamLine()->getName());
						fflush(stdout);
#endif
					}
					// Reverse move type also
					moveType = moveType == EqpType::OtherLineStart ? EqpType::OtherLineEnd : EqpType::OtherLineStart;
				}

				if(DebugCtl::isSynoptic())
				{
#ifdef Q_OS_WIN
					qDebug("SynView::checkForOverlap(%s): toMove %s toStay %s moveType %d\n",
						pLine->getBeamLine()->getName(), pToMove->getBeamLine()->getName(),
						pToStay->getBeamLine()->getName(), moveType);
#else
					printf("SynView::checkForOverlap(%s): toMove %s toStay %s moveType %d\n",
						pLine->getBeamLine()->getName(), pToMove->getBeamLine()->getName(),
						pToStay->getBeamLine()->getName(), moveType);
					fflush(stdout);
#endif
				}
				// int distance = pToStay->getEndX() - pToMove->getStartConnectX() + 10;
				int distance = pToStay->getEndX() - pToMove->getStartX() + 10;
				// Find in which line line to be moved is started
				for(int parentIdx = 0 ; parentIdx < lines.count() ; parentIdx++)
				{
					SynLine *pParent = lines.at(parentIdx);
					int eqpIdx = pParent->findConnectEqp(pToMove->getBeamLine()->getName(), moveType);
					if(eqpIdx >= 0)
					{
						if(DebugCtl::isSynoptic())
						{
#ifdef Q_OS_WIN
							qDebug("SynView::checkForOverlap(%s): move parent %s eqpIdx %d distance %d\n",
								pLine->getBeamLine()->getName(), pParent->getBeamLine()->getName(), eqpIdx, distance);
#else
							printf("SynView::checkForOverlap(%s): move parent %s eqpIdx %d distance %d\n",
								pLine->getBeamLine()->getName(), pParent->getBeamLine()->getName(), eqpIdx, distance);
							fflush(stdout);
#endif
						}
						/* L.Kopylov 23.05.2013 - new variant below
						pParent->movePartAfter(pParent->getEqpX(eqpIdx), distance);
						*/
						moveLineAndChildren(pParent, pParent, eqpIdx, distance);
						break;
					}
				}
				/* L.Kopylov 23.05.2013 - toMove is moved with parent - see above
printf("SynView::checkForOverlap(%s): move line toMove distance %d\n",
pLine->getBeamLine()->getName(), distance);
fflush(stdout);
				QList<ChildSynLine *> *pLineChildren = pToMove->moveHorizontally(distance, true);
				if(pLineChildren)
				{
					while(!pLineChildren->isEmpty())
					{
						delete pLineChildren->takeFirst();
					}
					delete pLineChildren;
				}
				*/
				break;
			}
		}
	}
}

void SynView::moveLineAndChildren(SynLine *pParent, SynLine *pTopMost, int eqpIdx, int distance)
{
	QList<ChildSynLine *> *pLineChildren;
	if(pTopMost == pParent)
	{
		pLineChildren = pParent->moveLinePart(eqpIdx > 1 ? eqpIdx - 1 : eqpIdx, distance);
	}
	else
	{
		pLineChildren = pParent->moveHorizontally(distance, true);
	}
	if(!pLineChildren)
	{
		return;
	}
	for(int childIdx = 0 ; childIdx < pLineChildren->count() ; childIdx++)
	{
		ChildSynLine *pLineChild = pLineChildren->at(childIdx);
		for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			SynLine *pLine = lines.at(lineIdx);
			if(pLine == pTopMost)
			{
				continue;
			}
			if(pLine->getBeamLine()->getName() == pLineChild->getName())
			{
				if(DebugCtl::isSynoptic())
				{
#ifdef Q_OS_WIN
					qDebug("SynView::moveLineAndChildren(%s): move child %s distance %d\n",
						pParent->getBeamLine()->getName(), pLine->getBeamLine()->getName(), distance);
#else
					printf("SynView::moveLineAndChildren(%s): move child %s distance %d\n",
						pParent->getBeamLine()->getName(), pLine->getBeamLine()->getName(), distance);
					fflush(stdout);
#endif
				}
				moveLineAndChildren(pLine, pTopMost, 0, distance);
				break;
			}
		}
	}
	while(!pLineChildren->isEmpty())
	{
		delete pLineChildren->takeFirst();
	}
	delete pLineChildren;
}

/*
**	FUNCTION
**		Move child line after parent line has been moved in order to have right
**		connection location
**
**	PARAMETERS
**		pParent		- Pointer to parent line
**		delta		- Offset to be applied to children locations
**		pChildren	- Pointer to list of children lines to move
**
**	RETURNS
**		NULL
**
**	CAUTIONS
**		None
*/
void SynView::moveChildLines(SynLine *pParent, long delta, QList<ChildSynLine *> *pChildren)
{
	if(!pChildren)
	{
		return;
	}
	for(int childIdx = 0 ; childIdx < pChildren->count() ; childIdx++)
	{
		ChildSynLine *pChild = pChildren->at(childIdx);
		for(int idx = 0 ; idx < lines.count() ; idx++)
		{
			SynLine *pLine = lines.at(idx);
			if(pLine == pParent)
			{
				continue;
			}
			if(pChild->getName() != pLine->getBeamLine()->getName())
			{
				continue;
			}
			QList<ChildSynLine *> *pLineChildren = pLine->moveHorizontally(delta, true);
			if(pLineChildren)
			{
				if(pLineChildren->count())
				{
					moveChildLines(pLine, delta, pLineChildren);
				}
				while(!pLineChildren->isEmpty())
				{
					delete pLineChildren->takeFirst();
				}
				delete pLineChildren;
			}
			break;
		}
	}
}

/*
**	FUNCTION
**		Find parent line for line with given index and given 'connection' equipment
**		in child line.
**
**	PARAMETERS
**		pChild			- Pointer to child line
**		pConnEqp		- Pointer to 'connection' device in child line
**		pParentEqpIdx	- Index of corresponding 'connection' device in parent
**							line will be returned here
**
** RETURNS
**		Pointer to parent line, or
**		NULL if parent line is not found
**
** CAUTIONS
**		None
*/
SynLine *SynView::getParentLine(SynLine *pChild, const Eqp *pConnEqp, int &parentEqpIdx)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLine *pLine = lines.at(idx);
		if(pLine == pChild)
		{
			continue;
		}
		parentEqpIdx = pLine->findConnectEqp(pChild->getBeamLine()->getName(),
			pConnEqp->getType() == EqpType::LineStart ? EqpType::OtherLineStart : EqpType::OtherLineEnd);
		if(parentEqpIdx >= 0)
		{
			return pLine;
		}
	}
	return NULL;
}

void SynView::coldexStateChange(bool isIn, bool isOut)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLine *pLine = lines.at(idx);
		pLine->setColdexState(isIn, isOut);
	}
	update();
}

/*
**	FUNCTION
**		Dump content for debugging purpose
**
**	PARAMETERS
**		fileName	- Name of output file
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::dump(const char *fileName)
{
	if(!DebugCtl::isSynoptic())
	{
		return;
	}
	FILE	*pFile;

#ifdef Q_OS_WIN
	if(fopen_s(&pFile, fileName, "w"))
	{
		return;
	}
#else
	if(!(pFile = fopen(fileName, "w")))
	{
		return;
	}
#endif

	fprintf(pFile, "eqpMask:\n");
	eqpMask.dump(pFile);
	fprintf(pFile, "\ntotalWidth %d totalHeight %d\n", totalWidth, totalHeight);
	if(pFirstConnLine)
	{
		fprintf(pFile, "Interline connect: from %s to %s @ %d\n",
			pFirstConnLine->getBeamLine()->getName(), pSecondConnLine->getBeamLine()->getName(),
			interLineX);
	}
	fprintf(pFile, "%d lines:\n", lines.count());
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLine *pLine = lines.at(idx);
		fprintf(pFile, "============================== Line %d:\n", idx);
		pLine->dump(pFile);
	}
	fclose(pFile);
}

/*
**	FUNCTION
**		Connects the SynView widget to the sectors of the lines and childlines in the synoptic
**
**	PARAMETERS
**		Pointer to the a SynLhcLine
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::connectSectorSelection(SynLine * pLine){
	for (int idx2 = 0; idx2 < pLine->getSectorLabelList().count(); idx2++){
		Sector *pSector = pLine->getSectorLabelList().at(idx2)->getSector();
		QObject::connect(pSector, SIGNAL(synSectSelectChanged(Sector *)),
			this, SLOT(forceRedraw()));
		QObject::connect(pSector, SIGNAL(synSectStateChanged(Sector *)), 
			this, SLOT(forceRedraw()));
	}


	return;
}

/*
**	FUNCTION
**		Disconnects the SynLhcView widget to the sectors of the lines and childlines in the synoptic
**
**	PARAMETERS
**		Pointer to the a SynLhcLine
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::disconnectSectorSelection(SynLine * pLine){
	for (int idx2 = 0; idx2 < pLine->getSectorLabelList().count(); idx2++){
		Sector *pSector = pLine->getSectorLabelList().at(idx2)->getSector();
		QObject::disconnect(pSector, SIGNAL(synSectSelectChanged(Sector *)),
			this, SLOT(forceRedraw()));
		QObject::disconnect(pSector, SIGNAL(synSectStateChanged(Sector *)), // [VACCO-948] [VACCO-1645]
			this, SLOT(forceRedraw()));
	}

	
	return;
}

/*
**	FUNCTION
**		QWidget event that occurs whenever a mouse button is pressed in a widget.
**
**	PARAMETERS
**		pEvent	- contains parameters that describe a mouse event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::mousePressEvent(QMouseEvent *pEvent)
{
	QPoint screenPoint = pEvent->pos();
	int ewoMouseX = screenPoint.x();
	int ewoMouseY = screenPoint.y();
	screenPoint = pEvent->globalPos();
	int globalMouseX = screenPoint.x();
	int globalMouseY = screenPoint.y();
	int buttonNbr = 0;

	switch (pEvent->button()){
	case Qt::LeftButton:
		buttonNbr = 1;
		break;
	case Qt::RightButton:
		buttonNbr = 2;
		break;
	default:
		break;
	}

	emit mouseDown(buttonNbr, mode, ewoMouseX, ewoMouseY, globalMouseX, globalMouseY, 0);
	return;
}

/*
**	FUNCTION	
**		Decides which action to do, in case the user pressed a mouse button on a sector
**
**	ARGUMENTS
**		button		- specifies wich mouse button was pressed
**		mode		-
**		x, y		- event coordinates
**		extra		-
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
bool SynView::synSectorClicked(int button, int mode, int localX, int localY, int globalX, int globalY, int extra)
{
	SynLhcSectorLabel *sectorSelected = getSectorFromXY(localX, localY);
	if (sectorSelected == NULL) return false;
	bool selection = sectorSelected->getSector()->isSelected();

	switch (button){
	case Qt::LeftButton: {
							 selection = !selection;
							 break;
	}
	case Qt::RightButton: {
							  selection = true;
							  break;
	}
	default:
		break;
	}

	sectorSelected->getSector()->setSelected(selection, false);
	emit synSectorMouseDown(button, mode, globalX, globalY, sectorSelected->getSector()->getPvssName(), sectorSelected->getSector()->isSelected());
	return true;
}

/*
**	FUNCTION
**		Checks if there is any sector in the region of the coordinates XY
**
**	ARGUMENTS
**		mouseX, mouseY		- panel coordinates
**
**	RETURNS
**		Pointer to the SynLhcSectorLabel in the region of the coords, if any
**
**	CAUTIONS
**		None
*/
SynLhcSectorLabel* SynView::getSectorFromXY(int mouseX, int mouseY)
{
	SynLhcSectorLabel *sectorSelected = NULL;
	for (int idx = 0; idx < lines.count(); idx++){
		SynLine *pLine = lines.at(idx);
		sectorSelected = pLine->getSectorFromXY(mouseX, mouseY);
		if (sectorSelected != NULL) break;
	}
	return sectorSelected;
}

/*
**	FUNCTION
**		Force redraw of this widget
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynView::forceRedraw()
{
	update();
}
/**
@brief PUBLIC FUNCTION Show mobiles not connected in all lines
*/
void SynView::showMobilesNotConnected(void) {
	for (int idx = 0; idx < lines.count(); idx++)
	{
		SynLine *pLine = lines.at(idx);
		pLine->showMobilesNotConnectedInLine();
	}
	// to force redraw:
	redrawRequiredForMobile = true;
	return;
}

/**
@brief PUBLIC FUNCTION Hide mobiles not connected in all lines
*/
void SynView::hideMobilesNotConnected(void) {
	for (int idx = 0; idx < lines.count(); idx++)
	{
		SynLine *pLine = lines.at(idx);
		pLine->hideMobilesNotConnectedInLine();
	}
	// to force redraw:
	redrawRequiredForMobile = true;
	return;
}