//	Implementation of ConfigTableModel class
/////////////////////////////////////////////////////////////////////////////////

#include "ConfigTableModel.h"
#include "GroupConfig.h"
#include <stdio.h>

ConfigTableModel::ConfigTableModel(QObject *parent) : QAbstractTableModel(parent)
{
	connectRequestSent = false;
	pTimer = new QTimer(this);
	pTimer->setSingleShot(true);
	connect(pTimer, SIGNAL(timeout()), this, SLOT(timerDone()));
}

ConfigTableModel::~ConfigTableModel()
{
	while(!rowDict.isEmpty())
	{
		delete rowDict.takeFirst();
	}
	while(!pendingConfigs.isEmpty())
	{
		delete pendingConfigs.takeFirst();
	}
}


void ConfigTableModel::clear(void)
{
	if(pTimer->isActive())
	{
		pTimer->stop();
	}
	bool toNotify = rowDict.count() > 0;
	if(toNotify)
	{
		beginRemoveRows(QModelIndex(), 0, rowDict.count() - 1);
	}
	mutex.lock();
	while(!rowDict.isEmpty())
	{
		SmsConfig *pConfig = rowDict.takeFirst();
		if(pConfig->isConnected())
		{
			disconnectOnClear.push(pConfig->getDpName());
		}
		delete pConfig;
	}
	while(!pendingConfigs.isEmpty())
	{
		delete pendingConfigs.takeFirst();
	}
	allDict.clear();
	mutex.unlock();
	if(toNotify)
	{
		endRemoveRows();
	}
	if((!disconnectOnClear.isEmpty()) && (!connectRequestSent))
	{
		connectRequestSent = true;
		emit connectRequest();
	}
}

void ConfigTableModel::addSimpleConfig(const QString &dpName)
{
	SmsConfig *pConfig = new SmsConfig(dpName);
	connect(pConfig, SIGNAL(changed(SmsConfig *)),
		this, SLOT(configChanged(SmsConfig *)));
	connect(pConfig, SIGNAL(numMsgChanged(SmsConfig *, int)),
		this, SLOT(configNumMsgChanged(SmsConfig *, int)));
	connect(pConfig, SIGNAL(numEventsChanged(SmsConfig *, int)),
		this, SLOT(configNumEventsChanged(SmsConfig *, int)));
	addRow(pConfig);
	addToDictionary(pConfig);
	addCallbackRequest();
}


void ConfigTableModel::addGroupConfig(const QString &dpName, const QStringList &simpleDpNames)
{
	GroupConfig *pConfig = new GroupConfig(dpName, simpleDpNames);
	connect(pConfig, SIGNAL(changed(SmsConfig *)),
		this, SLOT(configChanged(SmsConfig *)));
	connect(pConfig, SIGNAL(numMsgChanged(SmsConfig *, int)),
		this, SLOT(configNumMsgChanged(SmsConfig *, int)));
	connect(pConfig, SIGNAL(numEventsChanged(SmsConfig *, int)),
		this, SLOT(configNumEventsChanged(SmsConfig *, int)));
	addRow(pConfig);
	addToDictionary(pConfig);
	addCallbackRequest();
}

void ConfigTableModel::addToGroup(const QString &dpName, const QString &simpleDpName)
{
	SmsConfig *pConfig = allDict.value(dpName);
	if(!pConfig)
	{
		printf("ConfigTable::addToGroup(%s): group not found\n", dpName.toLatin1().constData());
		fflush(stdout);
		return;
	}
	if(!pConfig->isGroup())
	{
		printf("ConfigTable::addToGroup(%s): not a group\n", dpName.toLatin1().constData());
		fflush(stdout);
		return;
	}
	SmsConfig *pNewConfig = ((GroupConfig *)pConfig)->addSimpleConfig(simpleDpName);
	addToDictionary(pNewConfig);
	addCallbackRequest();
}

void ConfigTableModel::deleteConfig(const QString &dpName)
{
	SmsConfig *pToDelete = allDict.value(dpName);
	if(!pToDelete)
	{
		return;
	}
	if(pToDelete->isDeleted())
	{
		return;
	}
printf("ConfigTable::deleteConfig(): before allDict %d rowDict %d\n", allDict.count(), rowDict.count());
fflush(stdout);
	pToDelete->setDeleted(true);

	// Delete from table
	int row = pToDelete->getRow();
	if(row >= 0)
	{
		int rowsToDelete = 0;
		beginRemoveRows(QModelIndex(), row, row);
		for(int n = row ; n < rowDict.count() ; n++)
		{
			SmsConfig *pConfig = rowDict.at(n);
			if(pConfig->isDeleted())
			{
				rowDict.takeAt(n);
				rowsToDelete++;
			}
			else
			{
/*
printf("ConfigTable::deleteConfig(): change %d to %d\n", n, n - rowsToDelete);
fflush(stdout);
*/
				pConfig->setRow(n - rowsToDelete);
			}
		}
		endRemoveRows();
/*
printf("ConfigTable::deleteConfig(): rowsToDelete %d\n", rowsToDelete);
fflush(stdout);
*/
	}

	// Delete from pendingConfigs
	for(int idx = 0 ; idx < pendingConfigs.count() ; idx++)
	{
		if(dpName == pendingConfigs.at(idx)->getDpName())
		{
			pendingConfigs.takeAt(idx);
			break;
		}
	}

	// Delete from allDict
	mutex.lock();
	if(pToDelete->isGroup())
	{
		const QList<SmsConfig *> &children = ((GroupConfig *)pToDelete)->getList();
		for(int idx = 0 ; idx < children.count() ; idx++)
		{
			SmsConfig *pConfig = children.at(idx);
			if(pConfig->isConnected() && pConfig->isDeleted())
			{
				disconnectOnClear.push(pConfig->getDpName());
			}
			allDict.take(pConfig->getDpName());
		}
	}
	if(pToDelete->isConnected())
	{
		disconnectOnClear.push(pToDelete->getDpName());
	}
	allDict.take(pToDelete->getDpName());

	// Delete from rowDict, delete instance
//	pToDelete = rowDict.take(pToDelete->getRow());
//	if(pToDelete)
//	{
		delete pToDelete;	// !!!!!!!!!
//	}
	mutex.unlock();
	if((!disconnectOnClear.isEmpty()) && (!connectRequestSent))
	{
		connectRequestSent = true;
		emit connectRequest();
	}
printf("ConfigTable::deleteConfig(): after allDict %d rowDict %d\n", allDict.count(), rowDict.count());
fflush(stdout);
}

void ConfigTableModel::setConfigId(const QString dpName, int id)
{
	SmsConfig *pConfig = allDict.value(dpName);
	if(pConfig)
	{
		pConfig->setId(id);
	}
}

void ConfigTableModel::setConfigOwner(const QString &dpName, const QString &owner)
{
	SmsConfig *pConfig = allDict.value(dpName);
	if(pConfig)
	{
		pConfig->setOwner(owner);
	}
}

void ConfigTableModel::setConfigName(const QString &dpName, const QString &name)
{
	SmsConfig *pConfig = allDict.value(dpName);
	if(pConfig)
	{
		pConfig->setVisibleName(name);
	}
}

void ConfigTableModel::setConfigError(const QString &dpName, const QString &error)
{
	SmsConfig *pConfig = allDict.value(dpName);
	if(pConfig)
	{
		pConfig->setError(error);
	}
}

void ConfigTableModel::setConfigNumMsg(const QString &dpName, int numMsg)
{
	SmsConfig *pConfig = allDict.value(dpName);
	if(pConfig)
	{
		pConfig->setNumMsg(numMsg);
	}
}

void ConfigTableModel::setConfigNumEvents(const QString &dpName, unsigned numEvents)
{
	SmsConfig *pConfig = allDict.value(dpName);
	if(pConfig)
	{
		pConfig->setNumEvents(numEvents);
	}
}

void ConfigTableModel::setConfigFlags(const QString &dpName, bool active, bool activated, bool initialized)
{
	SmsConfig *pConfig = allDict.value(dpName);
	if(pConfig)
	{
		pConfig->setFlags(active, activated, initialized);
	}
}


void ConfigTableModel::addRow(SmsConfig *pConfig)
{
	// Add to list of pending
	pendingConfigs.append(pConfig);
	if(!pTimer->isActive())
	{
		pTimer->start(500);
	}
}

void ConfigTableModel::timerDone(void)
{
	if(pendingConfigs.isEmpty())
	{
		return;
	}
	beginInsertRows(QModelIndex(), rowDict.count(), rowDict.count() + pendingConfigs.count() - 1);
	QMutexLocker locker(&mutex);
	while(!pendingConfigs.isEmpty())
	{
		SmsConfig *pConfig = pendingConfigs.takeFirst();
		pConfig->setRow(rowDict.count());
		rowDict.append(pConfig);
	}
	endInsertRows();
}

void ConfigTableModel::addToDictionary(SmsConfig *pConfig)
{
	QMutexLocker locker(&mutex);
	if(pConfig->isGroup())
	{
		const QList<SmsConfig *> &children = ((GroupConfig *)pConfig)->getList();
		for(int idx = 0 ; idx < children.count() ; idx++)
		{
			SmsConfig *pItem = children.at(idx);
			allDict.insert(pItem->getDpName(), pItem);
		}
	}
	allDict.insert(pConfig->getDpName(), pConfig);
}

void ConfigTableModel::addCallbackRequest(void)
{
	mutex.lock();
	if(connectRequestSent)
	{
		mutex.unlock();
		return;
	}
	mutex.unlock();
	connectRequestSent = true;
	emit connectRequest();
}

const QString ConfigTableModel::nextDisconnectRequest(void)
{
	bool hasPending = false;
	QString result;
	QMutexLocker locker(&mutex);
	if(!disconnectOnClear.isEmpty())
	{
		result = disconnectOnClear.pop();
		return QString(result);
	}
	foreach(SmsConfig *pConfig, allDict)
	{
		if(pConfig->isDeleted())
		{
			if(pConfig->isConnected())
			{
				if(result.isEmpty())
				{
					result = pConfig->getDpName();
					pConfig->setConnected(false);
				}
			}
		}
		else if(!pConfig->isConnected())
		{
			hasPending = true;
		}
		if(hasPending && (!result.isEmpty()))
		{
			break;
		}
	}
	if(!hasPending)
	{
		connectRequestSent = false;
	}
	return QString(result);
}

const QString ConfigTableModel::nextConnectRequest(void)
{
	bool hasPending = !disconnectOnClear.isEmpty();
	QString result;
	QMutexLocker locker(&mutex);
	foreach(SmsConfig *pConfig, allDict)
	{
		if(pConfig->isDeleted())
		{
			if(pConfig->isConnected())
			{
				hasPending = true;
			}
		}
		else if(!pConfig->isConnected())
		{
			if(result.isEmpty())
			{
				result = pConfig->getDpName();
				pConfig->setConnected(true);
			}
			else
			{
				hasPending = true;
			}
		}
		if(hasPending && (!result.isEmpty()))
		{
			break;
		}
	}
	if(!hasPending)
	{
		connectRequestSent = false;
	}
	return QString(result);
}

const QString ConfigTableModel::getConfigAtRow(int row)
{
	if((0 <= row) && (row < rowDict.count()))
	{
		SmsConfig *pConfig = rowDict.at(row);
		if(pConfig)
		{
			return QString(pConfig->getDpName());
		}
	}
	return QString();
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Slots
/////////////////////////////////////////////////////////////////////////////////

void ConfigTableModel::configChanged(SmsConfig *pConfig)
{
	int row = pConfig->getRow();
	if(row >= 0)
	{
		emit dataChanged(index(row, SmsConfig::Owner), index(row, SmsConfig::Msg));
	}
}

void ConfigTableModel::configNumMsgChanged(SmsConfig *pConfig, int /* delta */)
{
	int row = pConfig->getRow();
	if(row >= 0)
	{
		emit dataChanged(index(row, SmsConfig::Msg), index(row, SmsConfig::Msg));
	}
}
void ConfigTableModel::configNumEventsChanged(SmsConfig *pConfig, int /* delta */)
{
	int row = pConfig->getRow();
	if(row >= 0)
	{
		emit dataChanged(index(row, SmsConfig::Events), index(row, SmsConfig::Events));
	}
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Implementation of QAbstractTableModel's methods
/////////////////////////////////////////////////////////////////////////////////




int ConfigTableModel::rowCount(const QModelIndex &parent) const
{
	return parent.isValid() ? 0 : rowDict.count();
}

int ConfigTableModel::columnCount(const QModelIndex &parent) const
{
	return parent.isValid() ? 0 : 8;
}

QVariant ConfigTableModel::data(const QModelIndex &index, int role) const
{
	if(!index.isValid())
	{
		return QVariant();
	}
	if((index.row() < 0) || (index.row() >= rowDict.count()))
	{
		return QVariant();
	}
	const SmsConfig *pConfig = rowDict.at(index.row());
	switch(role)
	{
	case Qt::DisplayRole:	// Text to be shown
		switch(index.column())
		{
		case SmsConfig::Id:
			{
				char buf[32];
				if(pConfig->getId() < 99999)
				{
#ifdef Q_OS_WIN
					sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%05d", pConfig->getId());
#else
					sprintf(buf, "%05d", pConfig->getId());
#endif
				}
				else
				{
#ifdef Q_OS_WIN
					sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d", pConfig->getId());
#else
					sprintf(buf, "%d", pConfig->getId());
#endif
				}
//				QString visibleId(buf);
				return QVariant(buf);
			}
			break;
		case SmsConfig::Owner:
			return QVariant(pConfig->getOwner());
		case SmsConfig::Group:
			if(!pConfig->isGroup())
			{
				return QVariant();
			}
			return QVariant(QString::number(pConfig->getNumChildren()));
		case SmsConfig::Config:
			return QVariant(pConfig->getVisibleName());
		case SmsConfig::Error:
			return QVariant(pConfig->getError());
		case SmsConfig::State:
			return QVariant(pConfig->getState());
		case SmsConfig::Events:
			return QVariant(QString::number(pConfig->getNumEvents()));
		case SmsConfig::Msg:
			return QVariant(QString::number(pConfig->getNumMsg()));
		default:
			break;
		}
		break;
	default:
		break;
	}
	return QVariant();
}

QVariant ConfigTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if((orientation != Qt::Horizontal) || (role != Qt::DisplayRole))
	{
		return QVariant();
	}
	switch(section)
	{
	case SmsConfig::Id:
		return QVariant("ID");
	case SmsConfig::Owner:
		return QVariant("Owner");
	case SmsConfig::Group:
		return QVariant("Group");
	case SmsConfig::Config:
		return QVariant("Configuration");
	case SmsConfig::Error:
		return QVariant("Error");
	case SmsConfig::State:
		return QVariant("State");
	case SmsConfig::Events:
		return QVariant("Events");
	case SmsConfig::Msg:
		return QVariant("Messages");
	default:
		break;
	}
	return QVariant();
}

bool smsConfigsLessThen(const SmsConfig *c1, const SmsConfig *c2)
{
	return *c1 < *c2;
}

void ConfigTableModel::sort(int column, Qt::SortOrder order)
{
	if(rowDict.isEmpty())
	{
		return;
	}
	mutex.lock();
	SmsConfig::setSortColumn(column);
	SmsConfig::setSortOrder(order);
	qStableSort(rowDict.begin(), rowDict.end(), smsConfigsLessThen);
	for(int row = 0 ; row < rowDict.count() ; row++)
	{
		rowDict.at(row)->setRow(row);
	}
	mutex.unlock();
	emit dataChanged(index(0, SmsConfig::Owner), index(rowDict.count() - 1, SmsConfig::Msg));
}


