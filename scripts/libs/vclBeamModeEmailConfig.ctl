// Configuration and auxilliary functions for E-mail notifications
// about vacuum equipment problems

#uses "email.ctl"	// Load standard PVSS CTRL library

// Name of DP type containing E-mail configuration
const string VAC_BEAM_MODE_EMAIL_CONFIG_DP_TYPE = "_VacBeamModeEmailConfig";

// Name of DP containing E-mail configuration
const string VAC_BEAM_MODE_EMAIL_CONFIG_DP = "_VacBeamModeEmailConfig";

// Set name of SMTP server
void VacBeamModeEmailSetServer(string serverName, dyn_string &exceptionInfo)
{
  string				configDp;
  dyn_errClass	err;

  dynClear(exceptionInfo);
  VacBeamModeEmailGetConfigDp(configDp, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSetWait(configDp + ".server", serverName);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "VacBeamModeEmailSetServer(): dpSetWait() failed", "");
  }
}
// Set name of client computer ('domain' for emSendMail())
void VacBeamModeEmailSetClient(string clientName, dyn_string &exceptionInfo)
{
  string				configDp;

  dynClear(exceptionInfo);
  VacBeamModeEmailGetConfigDp(configDp, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSetWait(configDp + ".client", clientName);
  dyn_errClass	err = getLastError();
  if(dynlen( err ) > 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "VacBeamModeEmailSetClient(): dpSetWait() failed", "" );
  }
}
// Set address of mail sender. It looks like sender shall be recognized
// by SMTP server
void VacBeamModeEmailSetSender(string senderName, dyn_string &exceptionInfo)
{
  string				configDp;

  dynClear(exceptionInfo);
  VacBeamModeEmailGetConfigDp(configDp, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSetWait(configDp + ".sender", senderName);
  dyn_errClass	err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "VacBeamModeEmailSetSender(): dpSetWait() failed", "");
    throwError(err);
  }
}
// Set address(es) of recipients to be notified about problems
// Every item in list shall be valid E-mail address
void VacBeamModeEmailSetRecipients(dyn_string recipients, dyn_string &exceptionInfo)
{
  string				configDp;  dyn_errClass	err;

  dynClear(exceptionInfo);
  VacBeamModeEmailGetConfigDp(configDp, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSetWait(configDp + ".recipients", recipients);
  dyn_errClass	err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "VacBeamModeEmailSetRecipients(): dpSetWait() failed", "");
    throwError(err);
  }
}
// Check if DP with email config exists, if not - create such DP
void VacBeamModeEmailGetConfigDp(string &configDpName, dyn_string &exceptionInfo)
{
  configDpName = "";
  if(dpExists(VAC_BEAM_MODE_EMAIL_CONFIG_DP))
  {
    configDpName = VAC_BEAM_MODE_EMAIL_CONFIG_DP;
    return;
  }
  if(dpCreate(VAC_BEAM_MODE_EMAIL_CONFIG_DP, VAC_BEAM_MODE_EMAIL_CONFIG_DP_TYPE) < 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "VacBeamModeEmailGetConfigDp(): dpCreate() failed", "" );
    return;
  }
  if(!dpExists(VAC_BEAM_MODE_EMAIL_CONFIG_DP))
  {
    fwException_raise(exceptionInfo, "ERROR", "VacBeamModeEmailGetConfigDp(): config DP not created", "" );
    return;
  }
  configDpName = VAC_BEAM_MODE_EMAIL_CONFIG_DP;
  // Set reasonable dafult values for just created DP
  VacBeamModeEmailSetServer("cernmx.cern.ch", exceptionInfo);
  VacBeamModeEmailSetClient("cern.ch", exceptionInfo);
  VacBeamModeEmailSetSender("vacin@cern.ch", exceptionInfo);
}
