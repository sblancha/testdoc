//	Implementation of MobileColorList class
/////////////////////////////////////////////////////////////////////////////////

#include "MobileColorList.h"

#include "VacType.h"

#include <QApplication>
#include <QColor>
#include <QPixmap>

MobileColorList	MobileColorList::instance;

/*
QPtrList<QColor>	MobileColorList::tColors;
QPtrList<QColor>	MobileColorList::prColors;
QPixmap 			*MobileColorList::qrlIcons[2];
QPixmap 			*MobileColorList::cryoIcons[2];
QPixmap 			*MobileColorList::redBeamIcons[2];
QPixmap 			*MobileColorList::blueBeamIcons[2];
QPixmap 			*MobileColorList::crossBeamIcons[2];
QPixmap 			*MobileColorList::anyBeamIcons[2];
QPtrList<QPixmap>	MobileColorList::prIcons;
QPtrList<QPixmap>	MobileColorList::tIcons;
*/

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Icons pixpamps //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
static const char *closed_blue_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #C0C0C0",
"@	c #0000FF",
"   .....       ",
"  .+@+@+.      ",
" .+@+@+@+......",
" .            .",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" .............."};

static const char *open_blue_xpm[] = {
"15 12 5 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #0000FF",
"@	c #000000",
"#	c #C0C0C0",
"   .....       ",
"  .     .      ",
" . .+.+. ......",
" . +.+.+.     .",
" . .+.+.+.+.+..",
".............+.",
".          .@..",
". #+#+#+#+#+@..",
" . #+#+#+#+#.@.",
" . +#+#+#+#+#@.",
"  . +#+#+#+#+. ",
"  ............ "};

static const char *closed_cyan_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #C0C0C0",
"@	c #00FFFF",
"   .....       ",
"  .+@+@+.      ",
" .+@+@+@+......",
" .            .",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" .............."};

static const char *open_cyan_xpm[] = {
"15 12 5 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #00FFFF",
"@	c #000000",
"#	c #C0C0C0",
"   .....       ",
"  .     .      ",
" . .+.+. ......",
" . +.+.+.     .",
" . .+.+.+.+.+..",
".............+.",
".          .@..",
". #+#+#+#+#+@..",
" . #+#+#+#+#.@.",
" . +#+#+#+#+#@.",
"  . +#+#+#+#+. ",
"  ............ "};

static const char *closed_gray_xpm[] = {
"15 12 3 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #C0C0C0",
"   .....       ",
"  .+.+.+.      ",
" .+.+.+.+......",
" .            .",
" . .+.+.+.+.+..",
" . +.+.+.+.+.+.",
" . .+.+.+.+.+..",
" . +.+.+.+.+.+.",
" . .+.+.+.+.+..",
" . +.+.+.+.+.+.",
" . .+.+.+.+.+..",
" .............."};

static const char *open_gray_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #000000",
"@	c #C0C0C0",
"   .....       ",
"  .     .      ",
" . ..... ......",
" . ......     .",
" . ............",
"...............",
".          .+..",
". @@@@@@@@@@+..",
" . @@@@@@@@@.+.",
" . @@@@@@@@@@+.",
"  . @@@@@@@@@. ",
"  ............ "};

static const char *closed_magenta_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #C0C0C0",
"@	c #FF00FF",
"   .....       ",
"  .+@+@+.      ",
" .+@+@+@+......",
" .            .",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" .............."};

static const char *open_magenta_xpm[] = {
"15 12 5 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #FF00FF",
"@	c #000000",
"#	c #C0C0C0",
"   .....       ",
"  .     .      ",
" . .+.+. ......",
" . +.+.+.     .",
" . .+.+.+.+.+..",
".............+.",
".          .@..",
". #+#+#+#+#+@..",
" . #+#+#+#+#.@.",
" . +#+#+#+#+#@.",
"  . +#+#+#+#+. ",
"  ............ "};

static const char *closed_red_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #C0C0C0",
"@	c #FF0000",
"   .....       ",
"  .+@+@+.      ",
" .+@+@+@+......",
" .            .",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" .............."};

static const char *open_red_xpm[] = {
"15 12 5 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #FF0000",
"@	c #000000",
"#	c #C0C0C0",
"   .....       ",
"  .     .      ",
" . .+.+. ......",
" . +.+.+.     .",
" . .+.+.+.+.+..",
".............+.",
".          .@..",
". #+#+#+#+#+@..",
" . #+#+#+#+#.@.",
" . +#+#+#+#+#@.",
"  . +#+#+#+#+. ",
"  ............ "};

static const char *closed_yellow_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #C0C0C0",
"@	c #FFFF00",
"   .....       ",
"  .+@+@+.      ",
" .+@+@+@+......",
" .            .",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" . +@+@+@+@+@+.",
" . @+@+@+@+@+@.",
" .............."};

static const char *open_yellow_xpm[] = {
"15 12 5 1",
" 	c #FFFFFF",
".	c #808080",
"+	c #FFFF00",
"@	c #000000",
"#	c #C0C0C0",
"   .....       ",
"  .     .      ",
" . .+.+. ......",
" . +.+.+.     .",
" . .+.+.+.+.+..",
".............+.",
".          .@..",
". #+#+#+#+#+@..",
" . #+#+#+#+#.@.",
" . +#+#+#+#+#@.",
"  . +#+#+#+#+. ",
"  ............ "};



static const char *pr1_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #CC00CC",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *pr2_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FF66CC",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *pr3_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FF00CC",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *pr4_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FF00FF",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *pr5_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #CC99FF",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *pr6_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #CC00FF",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *pr7_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #6600FF",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *pr8_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #0033FF",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *pr9_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #0066FF",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *pr10_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #0099FF",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *pr11_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #99CCFF",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};





static const char *tmp1_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FF0000",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp2_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #CC0000",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp3_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FF6666",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp4_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #CA6666",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp5_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #CC9999",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp6_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FF9966",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp7_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FF6600",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp8_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #CC6600",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp9_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FF9999",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp10_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FFCC00",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp11_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FFCC99",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp12_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #CC9966",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp13_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FFFF00",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp14_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FFFF99",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp15_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #CCFF00",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp16_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #FFFFCC",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp17_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #99FF33",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp18_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #33CC00",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp19_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #00FF00",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp20_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #99FF99",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp21_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #00FFCC",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp22_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #CCFFCC",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp23_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #00FFFF",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};

static const char *tmp24_xpm[] = {
"15 12 4 1",
" 	c #FFFFFF",
".	c #000000",
"+	c #808080",
"@	c #00CCFF",
"               ",
"               ",
".+.+.+.+.+.+.+.",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@",
".+.+.+.+.+.+.+."};



/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Static methods //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////


/*
**	FUNCTION
**		Return icon for given vacuum type and open/closed state
**
**	ARGUMENTS
**		vacType	- Vacuum type
**		open	- true if icon for open state is needed
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
QPixmap *MobileColorList::getVacIcon(int vacType, bool open)
{
	QPixmap *result = NULL;
	switch(vacType)
	{
	case VacType::Qrl:
		result = instance.qrlIcons[open ? 1 : 0];
		break;
	case VacType::Cryo:
	case VacType::DSL:
		result = instance.cryoIcons[open ? 1 : 0];
		break;
	case VacType::BlueBeam:
		result = instance.blueBeamIcons[open ? 1 : 0];
		break;
	case VacType::RedBeam:
		result = instance.redBeamIcons[open ? 1 : 0];
		break;
	case VacType::CrossBeam:
	case VacType::CommonBeam:
		result = instance.crossBeamIcons[open ? 1 : 0];
		break;
	default:
		result = instance.anyBeamIcons[open ? 1 : 0];
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Return icon for pressure measurement device with given index
**
**	ARGUMENTS
**		index	- Device index
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
QPixmap *MobileColorList::getPrIcon(int index)
{
	index = index % instance.prIcons.count();
	return instance.prIcons.at(index);
}

/*
**	FUNCTION
**		Return icon for bakeout channel with given channel number
**
**	ARGUMENTS
**		channel	- Bakeout channel [1...24]
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
QPixmap *MobileColorList::getBakeoutIcon(int channel)
{
	channel = (channel - 1) % instance.tIcons.count();
	return instance.tIcons.at(channel);
}

/*
**	FUNCTION
**		Return color for pressure measurement device with given index
**
**	ARGUMENTS
**		index	- Device index
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
QColor *MobileColorList::getPrColor(int index)
{
	index = index % instance.prColors.count();
	return instance.prColors.at(index);
}

/*
**	FUNCTION
**		Return color for bakeout channel with given channel number
**
**	ARGUMENTS
**		channel	- Bakeout channel [1...24]
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
QColor *MobileColorList::getBakeoutColor(int channel)
{
	channel = (channel - 1) % instance.tColors.count();
	return instance.tColors.at(channel);
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

MobileColorList::MobileColorList()
{
    if(qobject_cast<QGuiApplication *>(qApp))
	{
		if(tColors.isEmpty())
		{
			fillColors();
			fillVacPixmaps();
			fillPrPixmaps();
			fillTPixmaps();
		}
	}
}

MobileColorList::~MobileColorList()
{
	while(!tIcons.isEmpty())
	{
		delete tIcons.takeFirst();
	}
	while(!prIcons.isEmpty())
	{
		delete prIcons.takeFirst();
	}
	while(!prColors.isEmpty())
	{
		delete prColors.takeFirst();
	}
	while(!tColors.isEmpty())
	{
		delete tColors.takeFirst();
	}
}

/*
**	FUNCTION
**		Fill color tables used for both tree and graph
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileColorList::fillColors(void)
{
	tColors.append(new QColor(255, 0, 0));
	tColors.append(new QColor(204, 0, 0));
	tColors.append(new QColor(255, 102, 102));
	tColors.append(new QColor(204, 102, 102));
	tColors.append(new QColor(204, 153, 153));
	tColors.append(new QColor(255, 153, 102));
	tColors.append(new QColor(255, 102, 0));
	tColors.append(new QColor(204, 102, 0));
	tColors.append(new QColor(255, 153, 153));
	tColors.append(new QColor(255, 204, 0));

	tColors.append(new QColor(255, 204, 153));
	tColors.append(new QColor(204, 153, 102));
	tColors.append(new QColor(255, 255, 0));
	tColors.append(new QColor(255, 255, 153));
	tColors.append(new QColor(204, 255, 0));
	tColors.append(new QColor(255, 255, 204));
	tColors.append(new QColor(153, 255, 51));
	tColors.append(new QColor(51, 204, 0));
	tColors.append(new QColor(0, 255, 0));
	tColors.append(new QColor(153, 255, 153));

	tColors.append(new QColor(0, 255, 204));
	tColors.append(new QColor(204, 255, 204));
	tColors.append(new QColor(0, 255, 255));
	tColors.append(new QColor(0, 204, 255));

	prColors.append(new QColor(204, 0, 204));
	prColors.append(new QColor(255, 102, 204));
	prColors.append(new QColor(255, 0, 204));
	prColors.append(new QColor(255, 0, 255));
	prColors.append(new QColor(204, 153, 255));
	prColors.append(new QColor(204, 0, 255));
	prColors.append(new QColor(102, 0, 255));
	prColors.append(new QColor(0, 51, 255));
	prColors.append(new QColor(0, 153, 255));
	prColors.append(new QColor(0, 102, 255));
	prColors.append(new QColor(152, 204, 255));

}

/*
**	FUNCTION
**		Initialize pixmaps for vacuum types. 2 pixmaps per type:
**		0 = closed
**		1 = open
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileColorList::fillVacPixmaps(void)
{
	qrlIcons[0] = new QPixmap((const char **)closed_cyan_xpm);
	qrlIcons[1] = new QPixmap((const char **)open_cyan_xpm);

	cryoIcons[0] = new QPixmap((const char **)closed_magenta_xpm);
	cryoIcons[1] = new QPixmap((const char **)open_magenta_xpm);

	redBeamIcons[0] = new QPixmap((const char **)closed_red_xpm);
	redBeamIcons[1] = new QPixmap((const char **)open_red_xpm);

	blueBeamIcons[0] = new QPixmap((const char **)closed_blue_xpm);
	blueBeamIcons[1] = new QPixmap((const char **)open_blue_xpm);

	crossBeamIcons[0] = new QPixmap((const char **)closed_yellow_xpm);
	crossBeamIcons[1] = new QPixmap((const char **)open_yellow_xpm);

	anyBeamIcons[0] = new QPixmap((const char **)closed_gray_xpm);
	anyBeamIcons[1] = new QPixmap((const char **)open_gray_xpm);
}

/*
**	FUNCTION
**		Initialize pixmaps for pressure measurement equipment
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileColorList::fillPrPixmaps(void)
{
	prIcons.append(new QPixmap((const char **)pr1_xpm));
	prIcons.append(new QPixmap((const char **)pr2_xpm));
	prIcons.append(new QPixmap((const char **)pr3_xpm));
	prIcons.append(new QPixmap((const char **)pr4_xpm));
	prIcons.append(new QPixmap((const char **)pr5_xpm));
	prIcons.append(new QPixmap((const char **)pr6_xpm));
	prIcons.append(new QPixmap((const char **)pr7_xpm));
	prIcons.append(new QPixmap((const char **)pr8_xpm));
	prIcons.append(new QPixmap((const char **)pr9_xpm));
	prIcons.append(new QPixmap((const char **)pr10_xpm));
	prIcons.append(new QPixmap((const char **)pr11_xpm));
}

/*
**	FUNCTION
**		Initialize pixmaps for bakeout channels
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileColorList::fillTPixmaps(void)
{
	tIcons.append(new QPixmap((const char **)tmp1_xpm));
	tIcons.append(new QPixmap((const char **)tmp2_xpm));
	tIcons.append(new QPixmap((const char **)tmp3_xpm));
	tIcons.append(new QPixmap((const char **)tmp4_xpm));
	tIcons.append(new QPixmap((const char **)tmp5_xpm));
	tIcons.append(new QPixmap((const char **)tmp6_xpm));
	tIcons.append(new QPixmap((const char **)tmp7_xpm));
	tIcons.append(new QPixmap((const char **)tmp8_xpm));
	tIcons.append(new QPixmap((const char **)tmp9_xpm));
	tIcons.append(new QPixmap((const char **)tmp10_xpm));
	tIcons.append(new QPixmap((const char **)tmp11_xpm));
	tIcons.append(new QPixmap((const char **)tmp12_xpm));
	tIcons.append(new QPixmap((const char **)tmp13_xpm));
	tIcons.append(new QPixmap((const char **)tmp14_xpm));
	tIcons.append(new QPixmap((const char **)tmp15_xpm));
	tIcons.append(new QPixmap((const char **)tmp16_xpm));
	tIcons.append(new QPixmap((const char **)tmp17_xpm));
	tIcons.append(new QPixmap((const char **)tmp18_xpm));
	tIcons.append(new QPixmap((const char **)tmp19_xpm));
	tIcons.append(new QPixmap((const char **)tmp20_xpm));
	tIcons.append(new QPixmap((const char **)tmp21_xpm));
	tIcons.append(new QPixmap((const char **)tmp22_xpm));
	tIcons.append(new QPixmap((const char **)tmp23_xpm));
	tIcons.append(new QPixmap((const char **)tmp24_xpm));
}

