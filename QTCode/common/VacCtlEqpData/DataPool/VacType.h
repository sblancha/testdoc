#ifndef VACTYPE_H
#define VACTYPE_H

#include "VacCtlEqpDataExport.h"

// Vacuum types definitions. All values are single bit masks

class VACCTLEQPDATA_EXPORT VacType
{
public:
	enum
	{
		None = 0,			// Unknown vacuum type
		Beam = 1,			// Any beam vacuum type (for non-LHC machines)
		BlueBeam = 2,		// Blue beam vacuum type (LHC, .B)
		RedBeam = 4,		// Red beam vacuum type (LHC, .R)
		CrossBeam = 8,		// Cross beam vacuum type (LHC, .X)
		CommonBeam = 16,	// Cross beam vacuum type (LHC, .C)
		Qrl = 32,			// QRL vacuum type (LHC, .Q)
		Cryo = 64,			// CRYO vacuum type (LHC, .M)
		DSL = 128		// DSL vacuum type (LHC, .D)
	};

	static int decodeConn(const char *pString);
	static int decodeName(const char *pString);
	static bool isSingleBeam(unsigned type);
};
#endif	// VACTYPE_H
