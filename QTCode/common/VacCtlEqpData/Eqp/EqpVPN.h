#ifndef	EQPVPN_H
#define	EQPVPN_H

//	VPN - NEG pupm, chain of pumps is fully controlled by VP_NEG

#include "Eqp.h"
#include "EqpVP_NEG.h"	// strongly depends on VP_NEG

class VACCTLEQPDATA_EXPORT EqpVPN : public Eqp
{
	Q_OBJECT

public:
	EqpVPN(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVPN(const EqpVPN &src) : Eqp(src) {}
#endif
	~EqpVPN();

	virtual void postProcess(void);

	virtual void getMaster(QString &masterDp, int &masterChannel);
	virtual inline Eqp *getMaster(void) { return (Eqp *)pMaster; }
	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void setSelected(bool selected, bool isFromPvss);

#ifndef PVSS_SERVER_VERSION
	virtual DataEnum::ValueType getMainValueType(void) const { return pMaster ? pMaster->getMainValueType() : DataEnum::ValueTypeNone; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode mode);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
#endif

	virtual int getErrorCode(DataEnum::DataMode mode);
	virtual void getErrorString(DataEnum::DataMode mode, QString &result);

	// Device list support [VACCO-278]
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	// Access for essential values
	bool isPlcAlarm(DataEnum::DataMode mode) const { return pMaster ? pMaster->isPlcAlarm(mode) : 0; }

	// In fact, these values come from master
	virtual CtlStatus getCtlStatus(DataEnum::DataMode mode) const;
	unsigned getRR1(DataEnum::DataMode mode) const { return pMaster ? pMaster->getRR1(mode) : 0; }

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);
	virtual void selectChange(Eqp * /* pScr */) {}

	// VPN shall react to selection (via it's master - VP_NEG). Normally connection
	// to signal of VP_NEG is done in connectDpe() method (of VP_NEG in this case).
	// However, disconnect causes also disconnect from selection signal.
	// Even if second ('permanent') connection to selection signal is done -
	// disconnect() disconnects both (including 'permanent' connection).
	// In order to solve this problem two major changes were introduced:
	// 1) VPN has it's own NEW slot for processing selection signals from VP_NEG
	//		(see below)
	// 2) New virtual method postProcess() has been added to Eqp class, the
	//		purpose is to do what particular device needs AFTER all devices
	//		have been read from file.

	virtual void masterSelectChange(Eqp *pSrc);

protected:
	// Pointer to VP_NEG controlling this VPN
	EqpVP_NEG	*pMaster;

	// Number of connect requests to this Eqp - in order to decide if
	// we need to connect to/disconnect from master (VP_NEG)
	int		nOnlineConnects;
	int		nReplayConnects;
	int		nPollingConnects;
};

#endif	// EQPVPN_H
