#ifndef RACKICONVRGPK_H
#define RACKICONVRGPK_H

#include "RackIcon.h"

class Eqp;

class VACCTLEWOUTIL_EXPORT RackIconVRGPK : public RackIcon
{
	Q_OBJECT
public:
	RackIconVRGPK(QWidget * parent, Qt::WindowFlags f = 0);
	virtual ~RackIconVRGPK();

	virtual void setEqp(Eqp * pEqp);
	virtual bool connect(void);
	virtual bool disconnect(void);
protected:
	Eqp * pEqp; //pointer to equipment, if it exists
	virtual void paintEvent(QPaintEvent *pEvent);

};

#endif
