#ifndef LHCSEGMENT_H
#define	LHCSEGMENT_H

// Geometry of one vacuum type on LHC ring - the whole geometry
// of vacuum type on LHC ring is list of such segments

#include "SineCosine.h"

#include "InterfaceEqp.h"
#include "EqpMsgCriteria.h"

#include "EqpVACOK.h"

#include <qcolor.h>
#include <qlist.h>
#include <qpainter.h>
#include <QPolygon>

class MainLHCImage;

class Eqp;
class Sector;

class LhcSegment : public InterfaceEqp
{
	Q_OBJECT

public:
	LhcSegment(MainLHCImage *owner);
	LhcSegment(MainLHCImage *owner, float start, float end, Sector *pSector);
	LhcSegment(MainLHCImage *owner, int vacType, float start, float end, Sector *pSector);
	virtual ~LhcSegment();

	void clear();
	void connectEqp(void);

	void buildPoints(int index);
	void buildArcPoints(int index);

	void drawSelection(QPainter &painter, QColor &color, short lineWidth);
	void draw(QPainter &painter, bool fillOnly);
	void drawBeamSegment(QPainter &painter, QColor &color, short beamTypeMethod,
		short beamTypeWidth, FILE *pFile = NULL);
	void drawBeamEqp(QPainter &painter, const QColor &color, const QColor &borderColor, int beamTypeWidth);
	bool getBeamEqpGeometry(int &x, int &y, int &radius);
	void resetNeighborEqp(void);
	void checkForNeighborEqp(LhcSegment *pOtherSegment, const QPolygon &otherEqpPolygon);

	void dump(FILE *pFile, int index);

	// Access
	inline QList<SineCosine *> &getAngles(void) { return angles; }
	inline int getVacType(void) { return vacType; }
	inline Eqp *getEqp(void) { return pEqp; }
	inline void setEqp(Eqp *pEqp) { this->pEqp = pEqp; }
	inline Eqp *getEqp2(void) { return pEqp2; }
	inline Eqp *getOtherLineEqp(void) { return pOtherLineEqp; }
	inline void setEqp2(Eqp *pEqp2) { this->pEqp2 = pEqp2; }
	inline float getStart(void) { return start; }
	inline void setStart(float start) { this->start = start; }
	inline float getEnd(void) { return end; }
	inline void setEnd(float end) { this->end = end; }
	inline float getRelStart(void) { return relStart; }
	inline void setRelStart(float relStart) { this->relStart = relStart; }
	inline float getRelEnd(void) { return relEnd; }
	inline void setRelEnd(float relEnd) { this->relEnd = relEnd; }
	inline bool isStartAngleSet(void) { return startAngleSet; }
	inline void setStartAngleSet(bool set) { startAngleSet = set; }
	inline bool isEndAngleSet(void) { return endAngleSet; }
	inline void setEndAngleSet(bool set) { endAngleSet = set; }
	inline double getStartAngle(void) { return startAngle; }
	inline void setStartAngle(double angle) { startAngle = angle; }
	inline double getEndAngle(void) { return endAngle; }
	inline void setEndAngle(double angle) { endAngle = angle; }
	inline bool isRedOut(void) { return redOut; }
	inline void setRedOut(bool redOut) { this->redOut = redOut; }
	inline bool isInterlockHidden(void) const { return interlockHidden; }
	inline void setInterlockHidden(bool flag) { interlockHidden = flag; }

	inline short getOuterRad(void) { return outerRad; }
	inline void setOuterRad(short outerRad) { this->outerRad = outerRad; }
	inline short getInnerRad(void) { return innerRad; }
	inline void setInnerRad(short innerRad) { this->innerRad = innerRad; }

	inline Sector *getSector(void) { return pSector; }
	inline void setTypeColor(QColor &color) { typeColor = color; }

	inline const QList<Eqp *> *getNeighborEqpList(void) const { return pNeighborEqpList; }

public slots:
	// Override slot of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

signals:
	// Signal is emitted when state of one of devices has been changed
	void stateChanged(void);

protected:
	// Owner of this segment
	MainLHCImage	*owner;

	// Start angle of segment(s)
	double			startAngle;

	// End angle of segments
	double			endAngle;

	// start DCUM coordinate
	float			start;

	// End DCUM coordinate
	float			end;

	// Relative start coordinate
	float			relStart;

	// Relative end coordinate
	float			relEnd;

	// Color corresponding to vacuum type
	QColor			typeColor;

	// Pointer to device responsible for segment color
	// For beam vacuum: this is valve at the beginning of sector
	// For isol vacuum: this is NLOST alarm (logic = 1), to be shown on 'outer' radius
	Eqp				*pEqp;

	// Flag indicating if pEqp is connected by this segment
	bool			connectedEqp;

	// Pointer to 2nd device responsible for segment color
	// For beam vacuum: this is 2nd valve at the beginning of sector, only makes sense
	// for 'joining' vacuum (transition from R+B to X beam).
	// For isol vacuum: this is OK alarm (logic = 0), to be shown on 'inner' radius
	Eqp				*pEqp2;

	// Flag indicating if pEqp2 is connected by this segment
	bool			connectedEqp2;

	// Pointer to device corresponding to other line connection in this sector
	Eqp				*pOtherLineEqp;

	// Not NULL if this segment corresponds to vacuum sector
	Sector			*pSector;

	// List of segments
	QList<SineCosine *>	angles;

	// Outer radius [Pixels]
	short			outerRad;

	// Inner radius [Pixels]
	short			innerRad;

	// Vacuum type (see VacType.h)
	int				vacType;

	// Flag indicating if red beam is outer beam
	bool			redOut;

	// True if start angle has been set
	bool			startAngleSet;

	// True if end angle has been set
	bool			endAngleSet;

	// true if this segment has state color assigned
	bool			haveStateColor;

	// Flag indicating if this segment is connected to equipment
	bool			connected;

	// Flag indicating if equipment of this segment shall NOT be used to draw interlock circles.
	// This is important for R and B segments of Y-sector: spilt X to R+B. In this case R and B
	// segments have valve on X as equipment
	bool			interlockHidden;

	// List of neighbor devices: segments of these devices will be (partially) hidden if interlock
	// circle is drawn for device of this segment
	QList<Eqp *>	*pNeighborEqpList;

	void connectEqpBeam(void);
	void connectEqpIsol(void);
	void disconnectEqpBeam(void);
	void disconnectEqpIsol(void);
	void draw(QPainter &painter, short radInner, short radOuter, QColor &color, bool fillOnly);
	int buildBeamSegmentPolygones(short beamTypeMethod, short beamTypeWidth, QPolygon &inner, QPolygon &outer, QPolygon &middle, FILE *pFile);
	void addNeighborEqp(Eqp *pNeighborEqp);

	Eqp *findLineConnect(void);

};

#endif	// LHCSEGMENT_H
