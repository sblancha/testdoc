#ifndef	EqpVPG_MBLK_H
#define	EqpVPG_MBLK_H

//	EqpVPG_MBLK	- STD pumping group in LHC

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVPG_MBLK : public Eqp
{
	Q_OBJECT

public:
	EqpVPG_MBLK(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVPG_MBLK(const EqpVPG_MBLK &src) : Eqp(src) {}
#endif
	~EqpVPG_MBLK();

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
	virtual void getPartColor(int part, QColor &color, DataEnum::DataMode mode);
	virtual void getPartStateString(int part, QString &string, DataEnum::DataMode mode);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline unsigned getRR2(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr2Replay : rr2Online; }
	inline unsigned getRR3(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr3Replay : rr3Online; }
	inline unsigned getActiveStep(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? activeStepReplay : activeStepOnline; }
	inline unsigned getTurboPumpSpeed(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? turboPumpSpeedReplay : turboPumpSpeedOnline; }
	inline unsigned getTurboPumpCurrent(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? turboPumpCurrentReplay : turboPumpCurrentOnline; }

	inline unsigned getPrimaryPumpCurrent(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? primaryPumpCurrentReplay : primaryPumpCurrentOnline; }
	inline unsigned getVG1PhysicalValue(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? VG1PhysicalValueReplay : VG1PhysicalValueOnline; }
	inline unsigned getVG2PhysicalValue(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? VG2PhysicalValueReplay : VG2PhysicalValueOnline; }

	virtual int getErrorCode(DataEnum::DataMode mode);

	public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR2 DPE for online mode
	unsigned	rr2Online;

	// Last known value of RR3 DPE for online mode
	unsigned	rr3Online;

	// Last known value of ActiveStep DPE for online mode
	unsigned	activeStepOnline;

	//Last know value of TurboPumpSpeed DPE for online mode
	unsigned	turboPumpSpeedOnline;

	//Last known value of TurboPumpCurrent DPE for online mode
	double	turboPumpCurrentOnline;

	//Last known value of PrimaryPumpCurrent DPE for online mode
	double	primaryPumpCurrentOnline;

	//Last known value of VG1PhysicalValueOnline DPE for online mode
	double	VG1PhysicalValueOnline;

	//Last known value of VG2PhysicalValueOnline DPE for online mode
	double	VG2PhysicalValueOnline;


	// Last known value of 
	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for replay mode
	unsigned	rr2Replay;

	// Last known value of RR3 DPE for replay mode
	unsigned	rr3Replay;

	// Last known value of ActiveStep DPE for replay mode
	unsigned	activeStepReplay;

	// Last known value of TurboPumpSpeed DPE for replay mode
	unsigned	turboPumpSpeedReplay;

	// Last known value of TurboPumpCurrent DPE for replay mode
	double	turboPumpCurrentReplay;

	//Last known value of PrimaryPumpCurrent DPE for online mode
	double	primaryPumpCurrentReplay;

	//Last known value of VG1PhysicalValueOnline DPE for online mode
	double	VG1PhysicalValueReplay;

	//Last known value of VG2PhysicalValueOnline DPE for online mode
	double	VG2PhysicalValueReplay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, bool plcAlarm, QString &string);
	virtual void getPartColorPump(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVR1(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVR2(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorPP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorTMP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVD(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVI(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVT(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVG1(unsigned rr1, unsigned rr2, unsigned rr3, bool plcAlarm, QColor &color);
	virtual void getPartColorVG2(unsigned rr1, unsigned rr2, unsigned rr3, bool plcAlarm, QColor &color);
	virtual void getPartStateStringPump(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVR1(unsigned rr1, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVR2(unsigned rr1, bool plcAlarm, QString &string);
	virtual void getPartStateStringPP(unsigned rr1, bool plcAlarm, QString &string);
	virtual void getPartStateStringTMP(unsigned rr1, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVD(unsigned rr1, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVI(unsigned rr1, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVP(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVT(unsigned rr1, bool plcAlarm, QString &string);
#endif
};

#endif	// EqpVPG_MBLK_H
