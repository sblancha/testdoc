#ifndef	VACICONVP_IP_H
#define	VACICONVP_IP_H

//	Icon for VP_IP

#include "VacIcon.h"

class EqpVP_IP;

class VACCTLEWOUTIL_EXPORT VacIconVP_IP : public VacIcon	
{
	Q_OBJECT

public:
	// CONSTRUCTOR  (parent argument mandatory?)
	VacIconVP_IP(QWidget *parent, Qt::WindowFlags f = 0);
	// DESCTRUCTOR
	virtual ~VacIconVP_IP();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
    virtual VacIconContainer::Direction getSlaveDirection(void) const;
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);
	
protected:
	// Pointer to my device
	EqpVP_IP *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, unsigned rr1, bool blockedOff, bool plcAlarm);
	virtual void setPainterForImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
};

#endif	// VACICONVP_IP_H
