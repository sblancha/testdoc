//	Implementation of SynLhcLineBeam class
/////////////////////////////////////////////////////////////////////////////////

#include "SynLhcLineBeam.h"
#include "VacLinePart.h"

#include "DataPool.h"
#include "Sector.h"
#include "Eqp.h"
#include "EqpType.h"
#include "EqpMsgCriteria.h"

#include "VacIconContainer.h"
#include "VacIcon.h"

#include "VacMainView.h"
#include "DebugCtl.h"

#include <qwidget.h>
#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

SynLhcLineBeam::SynLhcLineBeam(QWidget *parent, int type, float start, float end) :
	SynLhcLine(parent, type, start, end)
{
	ascentThis = descentThis = 0;
}

SynLhcLineBeam::~SynLhcLineBeam()
{
	clear();
}

/*
**	FUNCTION
**		Clear all content of line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::clear(void)
{
	SynLhcLine::clear();
	while(!childLines.isEmpty())
	{
		delete childLines.takeFirst();
	}
}

/*
**	FUNCTION
**		Find name of item under mouse pointer.
**
**	PARAMETERS
**		mouseX	- X coordinate of mouse pointer location
**		mouseY	- Y coordinate of mouse pointer location
**
**	RETURNS
**		Name of equipment whose icon is under mouse pointer,
**		NULL if no device under pointer.
**
**	CAUTIONS
**		None
*/
const char *SynLhcLineBeam::closestItem(int mouseX, int mouseY)
{
	const char *result = SynLhcLine::closestItem(mouseX, mouseY);
	if(result)
	{
		return result;
	}
	/* TODO no such method in child line while transferring to Qt ???
	for(SynLhcInjDumpLine *pChildLine = childLines.first() ; pChildLine ; pChildLine = childLines.next())
	{
		if(result = pChildLine->closestItem(areaIdx, x, y, delta, pLabel))
		{
			return result;
		}
	}
	*/
	return NULL;
}

/*
**	FUNCTION
**		Analyze which equipment types will be shown in view
**
**	PARAMETERS
**		allVacTypeMask	- OR'ed bit mask for all vacuum types
**		allCryoEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**							to be shown in this view
**		allBeamEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**							to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allCryoEqpMask, VacEqpTypeMask &allBeamEqpMask )
{
	SynLhcLine::analyzeTypes(allVacTypeMask, allBeamEqpMask);
	for(int idx = childLines.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		pChildLine->analyzeTypes(allVacTypeMask, allCryoEqpMask, allBeamEqpMask);
	}
}

bool SynLhcLineBeam::findDevicePosition(Eqp *pEqp, int &eqpPosition, bool &eqpVisible)
{
	if(SynLhcLine::findDevicePosition(pEqp, eqpPosition, eqpVisible))
	{
		return true;
	}
	for(int idx = childLines.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		if(pChildLine->findDevicePosition(pEqp, eqpPosition, eqpVisible))
		{
			return true;
		}
	}
	return false;
}

void SynLhcLineBeam::setMode(DataEnum::DataMode newMode)
{
	SynLhcLine::setMode(newMode);
	for(int idx = childLines.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		pChildLine->setMode(newMode);
	}
}

/*
**	FUNCTION
**		Build all equipment to be shown on this line
**
**	PARAMETERS
**		vacTypeMask	- Bit mask for vacuum type selection
**		eqpMask		- Equipment type mask for equipment selection
**
**	RETURNS
**		Number of devices in this line
**
**	CAUTIONS
**		None
*/
int SynLhcLineBeam::build(unsigned vacTypeMask, VacEqpTypeMask &eqpMask)
{
	int coco = SynLhcLine::build(vacTypeMask, eqpMask), idx;
	VacEqpTypeMask emptyEqpMask;
	SynLhcInjDumpLine *pChildLine;
	for(idx = 0 ; idx < childLines.count() ; idx++)
	{
		pChildLine = childLines.at(idx);
		pChildLine->build(pChildLine->getVacType() & vacTypeMask ? eqpMask : emptyEqpMask, hideMagnets);
	}

	// L.Kopylov 27.01.2010 to process interlock sources on different lines
	/* L.Kopylov 17.01.2014
	int eqpIdx = 0;
	Eqp *pEqp = NULL;
	QString masterDp;
	bool before = false;
	*/

	// Orphan sources of this line - can be in children only
	/* L.Kopylov 06.04.2010
	while(nextOrphanInterlockSource(eqpIdx, before, &pEqp, &masterDp))
	{
		for(pChildLine = childLines.first() ; pChildLine ; pChildLine = childLines.next())
		{
			if(pEqp)
			{
				if(pChildLine->setInterlockSource(pEqp, before))
				{
					break;
				}
			}
			else if(masterDp)
			{
				if(pChildLine->setInterlockSource(masterDp, before))
				{
					break;
				}
			}
		}
	}
	*/

	// Orphan sources of child lines - can be in this line only
	/*
	for(idx = 0 ; idx < childLines.count() ; idx++)
	{
		pChildLine = childLines.at(idx);
		while(pChildLine->nextOrphanInterlockSource(eqpIdx, before, &pEqp, masterDp))
		{
			if(pEqp)
			{
				setInterlockSource(pEqp, before);
			}
			else if(!masterDp.isEmpty())
			{
				setInterlockSource(masterDp, before);
			}

		}
	}
	*/

	for(idx = 0 ; idx < childLines.count() ; idx++)
	{
		pChildLine = childLines.at(idx);
		pChildLine->addAlarmIcons();
		pChildLine->addSectorNameSpace(true);
	}
	return coco;
}

/*
**	FUNCTION
**		Join controllable equipment to groups. Criteria for LHC beam vacuum
**		is different from generic criteria:
**		devices belong to the same group if:
**		1) verticalPos != 0
**		2) devices are in the same vacuum sector
**		3) devices have the same TopParent attribute
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::buildEqpGroups()
{
	int groupId = 1, idx;
	SynLhcEqpItem *pItem;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		pItem->getConnPoint().setY(0);
		pItem->getRect().moveTop(pItem->getIconY());
	}
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		if(pItem->getType() != EqpType::VacDevice)
		{
			continue;
		}
		if(pItem->getGroupId())
		{
			continue;
		}
		QString firstTopParent = pItem->getEqp()->getAttrValue("TopParent");
		if(firstTopParent.isEmpty())
		{
			continue;
		}
		bool	found = false;
		for(int nextIdx = idx + 1 ; nextIdx < eqps.count() ; nextIdx++)
		{
			SynLhcEqpItem *pNext = eqps.at(nextIdx);
			if(pNext->getType() != EqpType::VacDevice)
			{
				continue;
			}
			QString topParent = pNext->getEqp()->getAttrValue("TopParent");
			if(firstTopParent != topParent)
			{
				continue;
			}
			found = true;
			pNext->setGroupId(groupId);
			if(pNext->getVerticalPos())
			{
				// 11.11.2011 L.Kopylov: Solenoids (VIES) shall not be moved vertically
				if(pNext->getEqp()->getFunctionalType() != FunctionalType::VIES)
				{
					if(eqpAboveLine)
					{
						pNext->moveVert(-COMMON_ACTIVE_OFFSET);
					}
					else
					{
						pNext->moveVert(COMMON_ACTIVE_OFFSET);
					}
				}
			}
		}
		if(!found)
		{
			continue;
		}
		pItem->setGroupId(groupId++);
		if(pItem->getVerticalPos())
		{
			// 11.11.2011 L.Kopylov: Solenoids (VIES) shall not be moved vertically
			if(pItem->getEqp()->getFunctionalType() != FunctionalType::VIES)
			{
				if(eqpAboveLine)
				{
					pItem->moveVert(-COMMON_ACTIVE_OFFSET);
				}
				else
				{
					pItem->moveVert(COMMON_ACTIVE_OFFSET);
				}
			}
		}
	}
}


/*
**	FUNCTION
**		Intialize synoptic line BEFORE building line geometry
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::init(void)
{
	ascent = descent = 0;
	ascentThis = descentThis = 0;	// Added when transferring to Qt
	SynLhcLine::init();
}

/*
**	FUNCTION
**		Calculate screen position for next device to be processed
**		If device is start/end of child line - calculate horizontal
**		geometry of that child line
**
**	PARAMETERS
**		coord		- Last processed world coordinate
**		connPos		- Last used connection point in screen coordinates
**		endCoord	- Last used end coordinate of element
**		endConnPos	- Last used and connection point
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::placeEqp(float &coord, int &connPos, float &endCoord, int &endConnPos)
{
	SynLhcEqpItem *pItem = eqps.at(curEqpIdx);

	SynLhcLine::placeEqp(coord, connPos, endCoord, endConnPos);
	if((pItem->getType() != EqpType::OtherLineStart) && (pItem->getType() != EqpType::OtherLineEnd))
	{
		return;
	}

	SynLhcInjDumpLine *pChildLine = NULL;
	for(int idx = 0 ; idx < childLines.count() ; idx++)
	{
		SynLhcInjDumpLine *pChild = childLines.at(idx);
		if(!strcmp(pChild->getBeamLine()->getName(), pItem->getEqp()->getName()))
		{
			pChildLine = pChild;
			break;
		}
	}
	if(!pChildLine)
	{
		return;
	}

	// Move child lines in horizontal direction
	bool isOtherLineEnd = false;
	switch(pItem->getType())
	{
	case EqpType::OtherLineStart:
		switch(pChildLine->getBeamLine()->getStartType())
		{
		case BeamLineConnType::LeftIn:
		case BeamLineConnType::RightIn:
			isOtherLineEnd = true;
			break;
		default:
			isOtherLineEnd = false;
			break;
		}
		break;
	case EqpType::OtherLineEnd:
		switch(pChildLine->getBeamLine()->getEndType())
		{
		case BeamLineConnType::LeftIn:
		case BeamLineConnType::RightIn:
			isOtherLineEnd = true;
			break;
		default:
			isOtherLineEnd = false;
			break;
		}
		break;
	default:
		return;
	}
	int delta = 0;
	if(isOtherLineEnd)
	{
		if(connPos < pChildLine->getEndXBeamLine()) 
		{
			connPos = pChildLine->getEndXBeamLine(); 
			pChildLine->setConnX(connPos);
			return;
		}
		delta = connPos - pChildLine->getEndXBeamLine(); 
	}
	else
	{
		delta = connPos - pChildLine->getStartXBeamLine(); 
		if(endX < (connPos + pChildLine->getEndXBeamLine())) 
		{
			endX = connPos + pChildLine->getEndXBeamLine(); 
		}
	}
	pChildLine->moveHorizontally(delta, true);
	pChildLine->setConnX(connPos);
}

/*
**	FUNCTION
**		The method is called after locations for everything on view
**		have been calculated. There are two major jobs to do:
**		1) Decide if one more sector label shall be added after
**		last sector border - items placement algorythm did not add
**		sector label after last sector border.
**		2) Decide if sector borders shall be drawn or not. Sector border
**		shall NOT be drawn if:
**		a) sector corresponds to line part AND
**		b) corresponding line part border shall be drawn
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::decideForSectorBorders(void)
{
	// TODO: the logic of this method is not clear at all. When converting
	// to Qt the logic has been preserved, but it looks like in reality this
	// method just does nothing. TO BE CHECKED

	int idx;
	SynLhcSectorLabel *pSect;
	for(idx = 0 ; idx < sectors.count() ; idx++)
	{
		pSect = sectors.at(idx);
		pSect->setDrawStart(true);
		pSect->setDrawEnd(true);
	}

	// Decide if one more sector label shall be added after the last one
	/* Problem when only one sector border on line - try another approach. LIK 10.03.2008
	if(!nSectors)
	{
		return;
	}
	if(!sectors[nSectors-1].drawEnd)
	{
		return;
	}
	*/
	if(!sectors.isEmpty())
	{
		pSect = sectors.last();
		if(!pSect->isDrawEnd())
		{
			return;
		}
	}
	// ^^^^ End of new approach
	SynLhcEqpItem *pItem = NULL;
	for(idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pEqp = eqps.at(idx);
		if(pEqp->isSectorBorder())
		{
			pItem = pEqp;
			break;
		}
	}
	if(!pItem)
	{
		return;
	}
	/* Not clear what was the idea behind this check LIK 10.03.2008
	if(nSectors)
	{
		if(pPool->sectors[eqp[eqpIdx].pEqp->Sector1Idx()] != sectors[nSectors-1].pSector)
		{
			return;
		}
	}
	*/
	Sector *pSector = pItem->getEqp()->getSectorAfter();
	int	sectStart = pItem->getConnPoint().x();
	QRect labelRect = parent->fontMetrics().boundingRect(pSector->getName());
	int sectEnd = sectStart + 2 * MIN_ACTIVE_SPACING + labelRect.width();
	if(labelRect.height() > maxSectHeight)
	{
		maxSectHeight = labelRect.height();
	}
	if(endX < sectEnd)
	{
		endX = sectEnd;
	}
	addSector(pSector, true, sectStart, sectEnd, pItem->getEqp()->getStart(), 12345678);
	pSect = sectors.last();
	pSect->setDrawStart(false);
	pSect->setDrawEnd(false);
}

/*
**	FUNCTION
**		Calculate vertical limits for line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::calcVerticalLimits(void)
{
	SynLhcLine::calcVerticalLimits();
	ascentThis = ascent;
	descentThis = descent;

	for(int idx = 0 ; idx < childLines.count() ; idx++)
	{
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		// Decide where child line shall be - above or below this line.
		// Criteria is very simple: always above for inner beam, always below for outer beam
		if(eqpAboveLine)
		{
			ascent += pChildLine->getAscent() + pChildLine->getDescent();
		}
		else
		{
			descent += pChildLine->getAscent() + pChildLine->getDescent();
		}
	}
}

/*
**	FUNCTION
**		Move all elements in line in vertical direction
**		Move also all child lines in vertical direction
**
**	PARAMETERS
**		delta	- The offset for movement
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::moveVertically(int &delta)
{
	// Move child lines in vertical direction - if child lines are above this line
	if(eqpAboveLine)
	{
		for(int idx = 0 ; idx < childLines.count() ; idx++)
		{
			SynLhcInjDumpLine *pChildLine = childLines.at(idx);
			pChildLine->moveVertically(delta + pChildLine->getAscent(), true);
		}
	}

	// Move this line itself
	SynLhcLine::moveVertically(delta);

	// Move child lines in vertical direction - if child lines are below this line
	if(!eqpAboveLine)
	{
		for(int idx = 0 ; idx < childLines.count() ; idx++)
		{
			SynLhcInjDumpLine *pChildLine = childLines.at(idx);
			pChildLine->moveVertically(y + descentThis + pChildLine->getAscent(), true);
		}
	}
}

/*
**	FUNCTION
**		Notify synoptic line that geometry calculation has been finished,
**		line shall move all active icons to calculated positions.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::finishGeometry(void)
{
	SynLhcLine::finishGeometry();
	for(int idx = 0 ; idx < childLines.count() ; idx++)
	{
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		pChildLine->finishGeometry();
	}
}




/*
**	FUNCTION
**		[VACCO-929]
**		Finds the start and end coordinates of each sector represented in the synoptic.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::setSectorGeometry(void)
{
	for (int idx = 0; idx < sectors.count(); idx++){
		SynLhcSectorLabel *pSectorLabel = sectors.at(idx);
		Sector *pSector = pSectorLabel->getSector();
		QList <Eqp *> &pSectEqpList = pSector->getEqpList();
		QList <Eqp *> startEqps, endEqps;

		// Checks each sectorLabel's border equipment
		for (int idx2 = 0; idx2 < pSectEqpList.count(); idx2++){
			Eqp *pSectEqp = pSectEqpList.at(idx2);
			if ((pSectEqp->isSectorBorder()) && (pSectEqp->getSectorAfter() == pSector)){
				startEqps.append(pSectEqp);
			}
			if ((pSectEqp->isSectorBorder()) && (pSectEqp->getSectorBefore() == pSector)){
				endEqps.append(pSectEqp);
			}
		}

		// Gets EqpItem equivalent to the Eqp that are sector borders.
		// Note: the EqpItem only contains the equipment in the synoptic view,
		//		so not all sector's equipments are in this list.
		QList <SynLhcEqpItem *> startEqpsItem, endEqpsItem;
		for (int idx3 = 0; idx3 < eqps.count(); idx3++){
			SynLhcEqpItem *pEqpItem = eqps.at(idx3);
			for (int idx4 = 0; idx4 < startEqps.count(); idx4++){
				if (startEqps.at(idx4) == pEqpItem->getEqp()){
					startEqpsItem.append(pEqpItem);
					continue;
				}
			}
			for (int idx4 = 0; idx4 < endEqps.count(); idx4++){
				if (endEqps.at(idx4) == pEqpItem->getEqp()){
					endEqpsItem.append(pEqpItem);
					continue;
				}
			}
		}

		// If the sector has both START and END equipments represented in the synoptic
		if ((startEqpsItem.count() == 1) && (endEqpsItem.count() == 1)){
			pSectorLabel->addToGraph(startEqpsItem.first()->getConnPoint(), endEqpsItem.first()->getConnPoint());
		}
		// If the sector has only the START equipment represented in the synoptic,
		//		it's necessary to go through the SynLine parts to know the end coords
		else if ((startEqpsItem.count() == 0) && (endEqpsItem.count() == 1)){
			QList <VacLinePart *> pPartList = getPartList();
			for (int idx3 = 0; idx3 < pPartList.count(); idx3++){
				VacLinePart *pPart = pPartList.at(idx3);
				if ((endEqpsItem.first()->getConnPoint().x() >= pPart->getStartX()) &&
					(endEqpsItem.first()->getConnPoint().x() <= pPart->getEndX())){

					pSectorLabel->addToGraph(pPart->getStartX(), y, endEqpsItem.first()->getConnPoint().x(), endEqpsItem.first()->getConnPoint().y());
					break;
				}
			}
		}
		// If the sector has only the END equipment represented in the synoptic,
		//		it's necessary to go through the SynLine parts to know the start coords
		else if ((startEqpsItem.count() == 1) && (endEqpsItem.count() == 0)){
			QList <VacLinePart *> pPartList = getPartList();
			for (int idx3 = 0; idx3 < pPartList.count(); idx3++){
				VacLinePart *pPart = pPartList.at(idx3);
				if ((startEqpsItem.first()->getConnPoint().x() >= pPart->getStartX()) &&
					(startEqpsItem.first()->getConnPoint().x() <= pPart->getEndX())){

					pSectorLabel->addToGraph(startEqpsItem.first()->getConnPoint().x(), startEqpsItem.first()->getConnPoint().y(), pPart->getEndX(), y);
					break;
				}
			}

		}
	}

	// Apply to child lines too
	for (int idx = 0; idx < childLines.count(); idx++)
	{
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		pChildLine->setSectorGeometry();
	}
}



/*
**	FUNCTION
**		[VACCO-929]
**		Checks if any sector is represented in the XY point (relative to the synoptic panel)
**
**	PARAMETERS
**		coordX	- panel x coordinate
**		coordY	- panel y coordinate
**
**	RETURNS
**		Pointer to the equivalent SynLhcSectorLabel, if any exists given the XY coords.
**		NULL if no sector is located in the region.
**
**	CAUTIONS
**		None
*/
SynLhcSectorLabel* SynLhcLineBeam::getSectorFromXY(int coordX, int coordY)
{
	SynLhcSectorLabel *sectorSelected = NULL;
	sectorSelected = SynLhcLine::getSectorFromXY(coordX, coordY);
	if (sectorSelected != NULL) return sectorSelected;

	for (int idx = 0; idx < childLines.count(); idx++){
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		sectorSelected = pChildLine->getSectorFromXY(coordX, coordY);
		if (sectorSelected != NULL) return sectorSelected;
	}
	return NULL;
}



/*
**	FUNCTION
**		[VACCO-929]
**		If a sector is selected, draws the selection lines around it.
**
**	PARAMETERS
**		painter	- Painter used for background drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::drawSectSelection(QPainter &painter)
{
	SynLhcLine::drawSectSelection(painter);
	for (int idx = 0; idx < childLines.count(); idx++)
	{
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		pChildLine->drawSectSelection(painter);
	}
	return;
}





/*
**	FUNCTION
**		Activate icons which are visible, deactivate icons which are not visible
**
**	PARAMETERS
**		viewStart	- Start position of visible part
**		viewWidth	- width of visible part
**		dragging	- true if this method is called from scroll bar dragging
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::setViewStart(int viewStart, int viewWidth, bool dragging)
{
	SynLhcLine::setViewStart(viewStart, viewWidth, dragging);
	for(int idx = 0 ; idx < childLines.count() ; idx++)
	{
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		pChildLine->setViewStart(viewStart, viewWidth, dragging);
	}
}

/*
**	FUNCTION
**		Check if given device shall appear in this synoptic line. Coordinate
**		of device is NOT checked.
**
**	PARAMETERS
**		pEqp	- Pointer to device in question
**		partIdx	- Index of line part, where device is located, will be returned here
**
**	RETURNS
**		true	- if device shall appear in this synoptic line;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SynLhcLineBeam::isMyEqp(Eqp *pEqp, int &partIdx)
{
	bool	result = false;

	switch(pEqp->getType())
	{
	case EqpType::VacDevice:
		break;
	case EqpType::OtherLineStart:
	case EqpType::OtherLineEnd:
		if(checkEqpVacType(pEqp, partIdx))
		{
			addChildLine(pEqp, partIdx);
		}
		/* NOBREAK */
	default:
		return false;
	}
	// Controllable vacuum equipment is checked here
	if(pEqp->isSkipOnSynoptic())
	{
		return false;
	}
	// Check if equipment is active or if show hiden mobile is selected
	if (!pEqp->isActive(mode) && !(isShowMobiles &&
								   ((pEqp->getFunctionalType() == FunctionalType::VPG) ||
									(pEqp->getFunctionalType() == FunctionalType::VRE))
								  )
		) { // Check if mobile 
		return false;
	}
	// Add device if it matches equipment type masks
	if (eqpMask.contains(pEqp->getFunctionalType()) || (pEqp->isSectorBorder() && alwaysIncludeSectorBorder))
	{
		int iconType = VacIcon::getIconType(pEqp);
		if(iconType != VacIconContainer::None)	// Here vacuum type does not matter
		{
			// If device has controllable parent - do not add device, it will be part of parent
			if(pEqp->getAttrValue("CtlParent").isEmpty())
			{
				result = checkEqpVacType(pEqp, partIdx);
			}
		}
	}
	if(!result)
	{
		return false;
	}
	if(!vacTypeMatchesMask(pEqp->getVacType()))
	{
		return false;
	}

	// Problem with CRYO thermometers: they belong to several vacuum types, hence,
	// they can appear on beam line which is hidden. So we'll find LHC beam location
	//	region where equipment is stitting and check if equipment on that region shall be visible
	// L.Kopylov 21.04.2008
	// Move CRYO thermometers to BeamCommon line L.Kopylov 18.05.2008
	if(!pEqp->isCryoThermometer())
	{
		return true;
	}
	return false;
}

/*
**	FUNCTION
**		Check if device shall appear in this line, criteria are vacuum type
**		and location of device
**
**	PARAMETERS
**		pEqp	- Pointer to device in question
**		partIdx	- Index of line part, where device is located, will be returned here
**
**	RETURNS
**		true	- if device shall appear in this synoptic line;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SynLhcLineBeam::checkEqpVacType(Eqp *pEqp, int &partIdx)
{
	float lhcRingLength = DataPool::getInstance().getLhcRingLength();
	int n = 0;
	for(n = 0 ; n < pPartList->count() ; n++)
	{
		VacLinePart *pPart = pPartList->at(n);
		if(!eqpMatchesPartType(pEqp, pPart->getVacType()))
		{
			continue;
		}
		float	eqpStart;
		// LIK 05.03.2008 - new variant below if( start < end )
		if(pPart->getEndPos() > lhcRingLength)	// part includes IP1
		{
			// It can be that even part includes IP1 - the required coordinate range
			// does NOT include IP1. Example: sector(s) just after IP1
			if(start > end)	// Required coordinate range includes IP1
			{
				if(pEqp->getStart() >= start)
				{
					eqpStart = pEqp->getStart();
				}
				else
				{
					eqpStart = pEqp->getStart() + lhcRingLength;
				}
			}
			else	// Required coordinate range does NOT include IP1
			{
				if(pEqp->getStart() <= pPart->getStartPos())
				{
					eqpStart = pEqp->getStart() + lhcRingLength;
				}
				else
				{
					eqpStart = pEqp->getStart();
				}
			}
		}
		else
		{
			eqpStart = pEqp->getStart();
		}
		if((pPart->getStartPos() <= eqpStart) && (eqpStart <= pPart->getEndPos()))
		{
			partIdx = n;
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Check if vacuum type of device matches vacuum type of line part
**
**	PARAMETERS
**		pEqp		- Pointer to device in question
**		partType	- Vacuum type of line part
**
**	RETURNS
**		true	- if device shall appear in this synoptic line;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SynLhcLineBeam::eqpMatchesPartType(Eqp *pEqp, int partType)
{
	if(partType & pEqp->getVacType())
	{
		return true;
	}
	if(pEqp->getVacType() & (VacType::CrossBeam | VacType::CommonBeam))
	{
		return true;
	}
	return false;
}

/*
**	FUNCTION
**		Decide which vacuum type of line part shall be added
**		for given region type
**
**	PARAMETERS
**		regionType	- Type of region
**
**	RETURNS
**		Enum corresponding to required vacuum type,
**		VacType::None if line part shall not be added.
**
** CAUTIONS
**		None
*/
int SynLhcLineBeam::vacTypeOfRegion(int regionType)
{
	int	result = VacType::None;
	switch(regionType)
	{
	case LhcRegion::BlueOut:
		if(type == OuterBeam)
		{
			result = VacType::BlueBeam;
		}
		else if(type == InnerBeam)
		{
			result = VacType::RedBeam;
		}
		break;
	case LhcRegion::RedOut:
		if(type == OuterBeam)
		{
			result = VacType::RedBeam;
		}
		else if(type == InnerBeam)
		{
			result = VacType::BlueBeam;
		}
		break;
	case LhcRegion::Cross:
		if(type == CrossBeam)
		{
			result = VacType::CrossBeam;
		}
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Calculate minimum allowed position for equipment at sector border
**
**	PARAMETERS
**		pItem	- Pointer to equipment item being processed
**		connPos	- Minimum allowed position not taking into account sector name
**
**	RETURNS
**		New minimum allowed position taking into account sector name
**
**	CAUTIONS
**		None
*/
int SynLhcLineBeam::calcSectorBorderPosition(SynLhcEqpItem *pItem, int connPos)
{
	int idx = eqps.indexOf(pItem);
	SynLhcEqpItem *pPrev = NULL;

	// Try to find previous sector border
	int sectStart = 0, sectEnd = 0, nLabels = 1;
	float sectEndPos = 0;
	bool found = false;
	for(--idx ; idx >= 0 ; idx--)
	{
		pPrev = eqps.at(idx);
		if(pPrev->isSectorBorder())
		{
			sectStart = sectEnd = pPrev->getConnPoint().x();
			found = true;
			nLabels = 2;
			sectEndPos = pPrev->getEqp()->getStart();
			// Sector start shall not be before start of current line part
			VacLinePart *pPart = pPartList->at(curPartIdx);
			if(pPart)
			{
				if(sectStart < pPart->getStartX())
				{
					sectStart = sectEnd = pPart->getStartX();
					nLabels = 1;
				}
			}
			break;
		}
	}
	if(!found)	// No previous sector border, use start of current line part
	{
		VacLinePart *pPart = pPartList->at(curPartIdx);
		if(pPart)
		{
			sectStart = sectEnd = pPart->getStartX();
			sectEndPos = pPart->getStartPos();
			found = true;
		}
	}
	if((!found) && (!pPartList->count()))
	{
		sectStart = sectEnd = VAC_LINE_SPACING;
		sectEndPos = 0.0;
		found = true;
	}
	if(found)
	{
		Sector *pSector = pItem->getEqp()->getSectorBefore();
		QRect labelRect = parent->fontMetrics().boundingRect(pSector->getName());
		sectEnd += 4 * MIN_ACTIVE_SPACING + nLabels * (labelRect.width() + 2);
		if(labelRect.height() > maxSectHeight)
		{
			maxSectHeight = labelRect.height();
		}
		if(sectEnd > connPos)
		{
			connPos = sectEnd;
		}
		else
		{
			sectEnd = connPos;
		}
		addSector(pSector, true, sectStart, sectEnd, sectEndPos, pItem->getStartOrder());
	}
	return connPos;
}

/*
**	FUNCTION
**		Draw line image: beam lines, active elements, sector labels etc...
**		Plus draw all child lines (if any)
**
**	PARAMETERS
**		painter		- painter used for drawing
**		viewStart	- Start X-coordinate of visible area
**		viewEnd		- End X-coordinate of visible area
**
**	RETURNS
**		always 1
**
**	CAUTIONS
**		None
*/
int SynLhcLineBeam::draw(QPainter &painter, int viewStart, int viewEnd)
{
	SynLhcLine::draw(painter, viewStart, viewEnd);
	if(!childLines.count())
	{
		return 1;
	}
	// Draw all child lines
	for(int idx = 0 ; idx < childLines.count() ; idx++)
	{
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		pChildLine->draw(painter, viewStart, viewEnd);
		setPainterForLine(painter, pChildLine->getVacType());
		painter.drawLine(pChildLine->getConnX(), pChildLine->getY(), pChildLine->getConnX(), y);
	}
	return 1;
}

/*
**	FUNCTION
**		Draw start of line part
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		lineStart	- X coordinate of line start
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::drawPartStart(QPainter &painter, int lineStart)
{
	painter.drawLine(lineStart, y, lineStart, crossLineY);
}

/*
**	FUNCTION
**		Draw end of line part
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		lineStart	- X coordinate of line start
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::drawPartEnd(QPainter &painter, int lineEnd)
{
	painter.drawLine(lineEnd, y, lineEnd, crossLineY);
}

/*
**	FUNCTION
**		Draw sector borders related to controllable device
**
**	PARAMETERS
**		pEqp		- Pointer to device at sector border
**		xCenter		- Calculated X coordinate of device icon's center
**		painter		- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::drawEqpSectorBorder(Eqp *pEqp, int xCenter, QPainter &painter)
{
	painter.setPen(VacMainView::getSynSectorColor());

	// Sector border
	painter.drawLine(xCenter, y - ascentThis, xCenter, y + descentThis);

	// Sector labels
	int txtY = eqpAboveLine ?
		y + descentThis :
		y - ascentThis + maxSectHeight;

	// Sector before device
	const char *sectName = pEqp->getSectorBefore()->getName();
	QString labelText = sectName;

	// [VACCO-929] [VACCO-948] [VACCO-1645] In case the sector is selected: sector's name is in bold
	painter.save();
	if (pEqp->getSectorBefore()->isSelected()){
		QFont boldFont(painter.font());
		boldFont.setBold(true);
		painter.setFont(boldFont);
	}

	QRect labelRect = painter.fontMetrics().boundingRect(labelText);
	int txtX = xCenter - labelRect.width() - 1;
	painter.drawText(txtX, txtY, labelText);
	painter.restore();

	// Sector after device
	sectName = pEqp->getSectorAfter()->getName();
	labelText = sectName;
	painter.save();
	if (pEqp->getSectorAfter()->isSelected()){
		QFont boldFont(painter.font());
		boldFont.setBold(true);
		painter.setFont(boldFont);
	}
	txtX = xCenter + 1;
	
	painter.drawText(txtX, txtY, labelText);
	painter.restore();
}

/*
**	FUNCTION
**		Add child (injection/dump) line to beam line
**
**	PARAMETERS
**		pConnEqp	- Pointer to child line connection device
**		partIdx		- Index of line part where connection is located
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::addChildLine(Eqp *pConnEqp, int partIdx)
{
	// Find child beam line
	BeamLine	*pLine = DataPool::getInstance().getLine(pConnEqp->getName());
	if(!pLine)
	{
		return;
	}

	// Create child synoptic line if it does not exist yet
	SynLhcInjDumpLine *pChildLine = NULL;
	for(int idx = 0 ; idx < childLines.count() ; idx++)
	{
		SynLhcInjDumpLine *pChild = childLines.at(idx);
		if(pChild->getBeamLine() == pLine)
		{
			pChildLine = pChild;
			break;
		}
	}
	if(!pChildLine)	// New child line
	{
		pChildLine = SynLhcInjDumpLine::create(parent, pLine, pConnEqp->getVacType());
		if(!pChildLine)
		{
			return;
		}
		childLines.append(pChildLine);
		QColor color;
		VacLinePart *pPart = pPartList->at(partIdx);
		int vacType = VacType::None;
		if(pPart)
		{
			vacType = pPart->getVacType();
		}
		setLineColor(vacType, color); 
		pChildLine->setPipeColor(color, true);
		pChildLine->setMode(mode);
		pChildLine->setAlwaysIncludeSectorBorder(alwaysIncludeSectorBorder && (vacTypeMask & vacType));
	}
	addEqp(pConnEqp, partIdx);
}

void SynLhcLineBeam::setAlwaysIncludeSectorBorder(bool flag) {
	alwaysIncludeSectorBorder = flag;
	for (int idx = 0; idx < childLines.count(); idx++) {
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		pChildLine->setAlwaysIncludeSectorBorder(flag);
	}
}

/*
**	FUNCTION
**		Write all parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeam::dump(FILE *pFile)
{
	if(!DebugCtl::isSynoptic())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	SynLhcLine::dump(pFile);
	if(!childLines.count())
	{
		return;
	}
	fprintf(pFile, "\n\n---------------- Child lines -----------------------\n");
	for(int idx = 0 ; idx < childLines.count() ; idx++)
	{
		SynLhcInjDumpLine *pChildLine = childLines.at(idx);
		pChildLine->dump(pFile);
	}
}