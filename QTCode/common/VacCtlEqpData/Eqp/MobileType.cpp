#include "MobileType.h"

#include <QString>

#include <stdio.h>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

/*
**
** FUNCTION
**		Parse value of mobile type read from file
**
** PARAMETERS
**		token			- String with mobile type value read from file
**		lineNo			- Current line number in file
**		errMsg			- Variable where error message shall be written
**							in case of errors.
**
*** RETURNS
**		Decoded mobile type, or
**		-1 in case of error
**
** CAUTIONS
**		None
*/
int MobileType::decode(const char *token, int lineNo, QString &errMsg)
{
	int		value;

	if(sscanf(token, "%d", &value) != 1)
	{
		errMsg = QString("Line %1: bad format for <MobileType> : <%2>").arg(lineNo).arg(token);
		return false;
	}
	if((value == OnFlange) || (value == OnSector) || (value == EqpList) ||
			(value == IsolSectorPR) || (value == PlcAlarm) || (value == PlcCtl))
	{
		return value;
	}
	else if ((value == Fixed) || (value == Container) || (value == Master) || (value == Instance))
	{
		return value;
	}
	errMsg = QString("Line %1: unknown value %2 for <MobileType> : <%3>").arg(lineNo).arg(value).arg(token);
	return -1;
}
