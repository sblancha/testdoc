//	Implementation of ValueProfile class
/////////////////////////////////////////////////////////////////////////////////

#include "ValueProfile.h"

EWO_PLUGIN(ValueProfile)



ValueProfile::ValueProfile(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new ValueProfileWidget(parent);
	connect(baseWidget, SIGNAL(barHighlight(int, float)), this, SLOT(barHighlighted(int, float)));
	connect(baseWidget, SIGNAL(marqueeMove(int, int)), this, SLOT(marqueeMoved(int, int)));

}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList ValueProfile::signalList(void) const
{
	QStringList list;
	list.append("barHighlight(int index, float value)");
	list.append("marqueeMove(int start, int end)");
	return list;
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList ValueProfile::methodList(void) const
{
	QStringList list;
	list.append("int setFloatValues(dyn_float list)");
	list.append("dyn_string getValuesInMarquee()");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ValueProfile::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	int coco = 0;
	if(name == "setFloatValues")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::List))
		{
			error = "argument 1 (list) is not dyn_XXX";
			return QVariant();
		}
		coco = baseWidget->setFloatValues(values[0], error);
		return QVariant(coco);
	}
	else if(name == "getValuesInMarquee")
	{
		QStringList &result = baseWidget->getValuesInMarquee();
		return QVariant(result);
	}
	else
	{
		error = "Uknown method ValueProfile.";
		error += name;
	}
	return QVariant();
}

void ValueProfile::barHighlighted(int index, float value)
{
	QList<QVariant> args;
	args.append(QVariant(index));
	args.append(QVariant((double)value));
	emit signal("barHighlight", args);
}

void ValueProfile::marqueeMoved(int start, int end)
{
	QList<QVariant> args;
	args.append(QVariant(start + 1));
	args.append(QVariant(end + 1));
	emit signal("marqueeMove", args);
}

