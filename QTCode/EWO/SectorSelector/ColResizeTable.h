#ifndef	COLRESIZETABLE_H
#define	COLRESIZETABLE_H

// Subclass of QTable that shall automatically resize column width

#include <qtablewidget.h>

class ColResizeTable : public QTableWidget
{
	Q_OBJECT

public:
	ColResizeTable(QWidget *parent = 0)
		: QTableWidget(parent)
	{
	}
	ColResizeTable(int nRows, int nCols, QWidget *parent = 0)
		: QTableWidget(nRows, nCols, parent)
	{
	}

protected:
	virtual void resizeEvent(QResizeEvent *pEvent);
};

#endif	// COLRESIZETABLE_H
