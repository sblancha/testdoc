#ifndef VACICONVV_PUL_H
#define	VACICONVV_PUL_H

// Icon for VV_PUL

#include "VacIcon.h"

class EqpVV_PUL;
class FlashingRect;

class VACCTLEWOUTIL_EXPORT VacIconVV_PUL : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVV_PUL(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVV_PUL();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setDirection(VacIconContainer::Direction newDirection);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVV_PUL *pEqp;

	// Flashing rectangle
	FlashingRect	*pFlash;

	// Flashing rectangle - used to display BlockedOFF state
	FlashingRect	*pBlockedRect;

	virtual void paintEvent(QPaintEvent *pEvent);
	void drawVertical(QPainter &painter);
	void drawHorizontal(QPainter &painter);
	void drawSymbols(QPainter &painter, bool vertical);
	void drawForced(QPainter &painter, bool vertical);
	void drawManual(QPainter &painter, bool vertical);
	void drawFullInterlock(QPainter &painter, bool vertical);
	void drawStartInterlock(QPainter &painter, bool vertical);
	void drawCmdOff(QPainter &painter, bool vertical);
	void drawCmdOn(QPainter &painter, bool vertical);
	void drawOpenEnable(QPainter &painter, bool isVertical);
	void drawCloseEnable(QPainter &painter, bool isVertical);
		
	void displayFlash(const QPolygon &points);
	void displayBlockedOFF(bool blockedOff, bool isVertical);

	void setPainterForControl(QPainter &painter);
	void setPainterForMainImage(QPainter &painter);
};

#endif	// VACICONVV_PUL_H
