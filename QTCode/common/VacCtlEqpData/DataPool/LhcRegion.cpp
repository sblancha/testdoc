//		Implementation of LhcRegion class
//////////////////////////////////////////////////////////////////////

#include "LhcRegion.h"

LhcRegion::LhcRegion(float start, float end, int type)
{
	this->start = start;
	this->end = end;
	this->type = type;
}

/*
**	FUNCTION
**		Check if coordinate is within coordinates range this region
**
**	EXTERNAL
**
**
**	PARAMETERS
**		coord	- Coordinate to check
**
**	RETURNS
**		true	- If equipment coordinate is within coordinates range of region;
**		false	- otherwise
**
**	CAUTIONS
**		Method is supposed to be used on circular line ONLY
**
** FUNCTION DEFINITION
*/
bool LhcRegion::isCoordInside(float coord)
{
	if(start < end)
	{
		if((start <= coord) && (coord <= end))
		{
			return true;
		}
	}
	else
	{
		if((start <= coord) || (coord <= end))
		{
			return true;
		}
	}
	return false;
}

/*
**
**	FUNCTION
**		Dump region parameters for output file for debugging purpose
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**		idx		- Index of this region
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcRegion::dump(FILE *pFile, int idx)
{
	fprintf(pFile, "  %d :  %f   %f   ", idx, start, end);
	switch(type)
	{
	case BlueOut:
		fprintf(pFile, "BLUE OUT\n");
		break;
	case RedOut:
		fprintf(pFile, "RED OUT\n");
		break;
	case Cross:
		fprintf(pFile, "CROSS\n");
		break;
	default:
		fprintf(pFile, "%d ??????\n", type);
		break;
	}
}
