//	Implementation of VacIconVBAR class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVBAR.h"

#include "Eqp.h"

#include "FunctionalType.h"
#include "VacEqpTypeMask.h"

#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVBAR::VacIconVBAR(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	// setAutoFillBackground(true);
	setFixedSize(11, 11);
}

VacIconVBAR::~VacIconVBAR()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		It is supposed that this method will not be used for VBAR
*/
VacIconGeometry VacIconVBAR::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(width(), height(), width() >> 1, height() >> 1, 0);
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask		- Vacuum equipment mask
**		forBeamVac	- Flag indicating if icon will be drawn on beam vacuum
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVBAR::getGeometry(const VacEqpTypeMask & /* mask */, bool /* forBeamVac */)
{
	return VacIconGeometry(width(), height(), width() >> 1, height() >> 1, 0);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVBAR::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		this->pEqp = pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVBAR::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVBAR::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVBAR::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVBAR::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVBAR::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	// QRect myRect = rect();
	// painter.fillRect(myRect, parent()->);

	// Hole between pipes
	painter.setPen(pipeColor);
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		painter.drawLine(2, 2, 2, 8);
		painter.drawLine(3, 2, 3, 8);
		painter.drawLine(7, 2, 7, 8);
		painter.drawLine(8, 2, 8, 8);
		painter.setPen(Qt::white);
		painter.drawLine(4, 2, 4, 8);
		painter.drawLine(6, 2, 6, 8);
		break;
	default:
		painter.drawLine(2, 2, 8, 2);
		painter.drawLine(2, 3, 8, 3);
		painter.drawLine(2, 7, 8, 7);
		painter.drawLine(2, 8, 8, 8);
		painter.setPen(Qt::white);
		painter.drawLine(2, 4, 8, 4);
		painter.drawLine(2, 6, 8, 6);
		break;
	}

	// Beam pipes
	painter.setPen(pipeColor);
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		painter.fillRect(0, 5 - (pipeWidth >> 1), 4, pipeWidth, pipeColor);
		painter.fillRect(7, 5 - (pipeWidth >> 1), 4, pipeWidth, pipeColor);
		break;
	default:
		painter.fillRect(5 - (pipeWidth >> 1), 0, pipeWidth, 4, pipeColor);
		painter.fillRect(5 - (pipeWidth >> 1), 7, pipeWidth, 4, pipeColor);
		break;
	}

	// Selection
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
}

