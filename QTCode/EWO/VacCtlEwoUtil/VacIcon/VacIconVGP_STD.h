#ifndef	VACICONVGP_STD_H
#define	VACICONVGP_STD_H

//	Icon for VG_STD - VGP

#include "VacIconVG_STD.h"

class VACCTLEWOUTIL_EXPORT VacIconVGP_STD : public VacIconVG_STD
{
public:
	VacIconVGP_STD(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG_STD(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGP_STD_H
