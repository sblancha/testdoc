
//LhcVacDeviceAction_VITL_CMNT:
/** Execute action for cryomaintain interlock source.
  It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
int LhcVacDeviceAction_VITL_CMNT(string dpName, string dpType, dyn_anytype  action,
  dyn_string &exceptionInfo)
{
  mapping       valuesToWrite;
  unsigned      act = action[1] & LVA_ACTION_MASK;
  mapping       mConnectDpes;
  switch(act)
  {
  case LVA_DEVICE_ON:
    valuesToWrite[".WR1"] = (unsigned)0x0600;  // MANUAL + ON
    break;
  case LVA_DEVICE_OFF:
    valuesToWrite[".WR1"] = (unsigned)0x0500;  // MANUAL + OFF
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["FULL_WORD"] = NO_VALUE_CHANGE;
    mConnectDpes["WDG"] = false;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["FULL_WORD"] = NO_VALUE_CHANGE;
    mConnectDpes["WDG"] = false;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VITL_CMNT: unknown action " + act, "");
    return LVA_FAILURE;
  }

  // Execute
  return LhcVacSetActionDpeValues(dpName, valuesToWrite, exceptionInfo);
}
