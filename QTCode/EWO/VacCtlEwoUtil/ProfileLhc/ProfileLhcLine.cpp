//	Implementation of ProfileLhcLine class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileLhcLine.h"
#include "ProfileLhcView.h"

#include "VacLinePart.h"

#include "DataPool.h"
#include "Eqp.h"

#include "VacMainView.h"

#include <math.h>

#include "PlatformDef.h"
#include "DebugCtl.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ProfileLhcLine::ProfileLhcLine(ProfileLhcView *parent, int type, float start, float end) :
	QObject(), LhcLineParts(start, end)
{
	this->parent = parent;
	this->type = type;
	pLogAxis = new VerticalAxis(true, true);
	pLinAxis = new VerticalAxis(false, false);
	vacTypeMask = 0x0;
	hidden = false;
	showSectorNames = false;
	axisMinLocation = -1;
}

ProfileLhcLine::~ProfileLhcLine()
{
	clear();
	delete pLinAxis;
	delete pLogAxis;
}

/*
**	FUNCTION
**		Clear content
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void ProfileLhcLine::clear(void)
{
	LhcLineParts::clear();
	while(!eqps.isEmpty())
	{
		delete eqps.takeFirst();
	}
	while(!cryoTs.isEmpty())
	{
		delete cryoTs.takeFirst();
	}
	while(!sectors.isEmpty())
	{
		delete sectors.takeFirst();
	}
}

/*
**	FUNCTION
**		Build equipment and line parts for profile line
**
**	PARAMETERS
**		allowedEqpMask	- Equipment type mask for all equipment potentially allowed on this line
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::build(VacEqpTypeMask &allowedEqpMask)
{
	this->allowedEqpMask = allowedEqpMask;
	clear();
	buildLineParts();
	if(!pPartList->count())
	{
		return;
	}
	buildEquipment();
}

/*
**	FUNCTION
**		Build all equipment that potentially can be shown in this line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::buildEquipment(void)
{
	// Find LHC circular line
	BeamLine	*pLine = DataPool::getInstance().getCircularLine();
	if(!pLine)
	{
		return;
	}

	// LHC ring consists of one part only
	if (pLine->getParts().isEmpty()) {
		return;
	}
	BeamLinePart *pPart = pLine->getParts().first();
	Eqp	**eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();

	// Find first device to add - only coordinate is considered for decision
	int	eqpIdx = 0;
	for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if((pEqp->getStart() + pEqp->getLength()) >= start)
		{
			break;
		}
	}

	// Add first portion of devices
	int	partIdx = 0;
	for( ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(start < end)
		{
			if(pEqp->getStart() > end)
			{
				break;
			}
		}
		if(pEqp->getStart() < start)
		{
			continue;
		}
		if(isMyEqp(pEqp, partIdx))
		{
			addEqp(pEqp, partIdx);
		}
	}
	if(start < end)
	{
		return;
	}

	// Add equipment after IP1
	for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(pEqp->getStart() > end)
		{
			break;
		}
		if(isMyEqp(pEqp, partIdx))
		{
			addEqp(pEqp, partIdx);
		}
	}
}

/*
**	FUNCTION
**		Add device to line
**
**	PARAMETERS
**		pEqp	- Pointer to device to be added
**		partIdx	- Index of line part where device is located
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::addEqp(Eqp *pEqp, int partIdx)
{
	VacLinePart *pPart = pPartList->at(partIdx);
	float lhcRingLength = DataPool::getInstance().getLhcRingLength();

	// depending on type of device (pressure/temperature) add to different arrays
	if(pEqp->isCryoThermometer())
	{
		if(!pEqp->getSectorBefore())	// Can't work with thermometers - value per SECTOR
		{
			return;
		}
		ProfileThermometer *pItem = new ProfileThermometer(pEqp, mode);
		cryoTs.append(pItem);
		if((pPart->getStartPos() > pPart->getEndPos()) || (pPart->getEndPos() > lhcRingLength))
		{
			if(pItem->getStart() < pPart->getStartPos())
			{
				pItem->setStart(pItem->getStart() + lhcRingLength);
			}
		}
		else if(pPart->isAfterIP1())	// ????? LIK 29.04.2008 .M sectors do not cross IP1
		{
			pItem->setStart(pItem->getStart() + lhcRingLength);
		}
		connect(pItem, SIGNAL(eqpDataChanged(ProfileThermometer *)),
			this, SLOT(thermoDataChange(ProfileThermometer *)));
		connect(pItem, SIGNAL(eqpSelectChanged(bool)), this, SIGNAL(eqpSelectChanged(bool)));
	}
	else
	{
		ProfileLhcLineItem *pItem = new ProfileLhcLineItem(pEqp, mode);
		eqps.append(pItem);

		if((pPart->getStartPos() > pPart->getEndPos()) || (pPart->getEndPos() > lhcRingLength))
		{
			if(pEqp->getStart() < pPart->getStartPos())
			{
				pItem->setStart(pItem->getStart() + lhcRingLength);
			}
		}
		else if(pPart->isAfterIP1())
		{
			pItem->setStart(pItem->getStart() + lhcRingLength);
		}
		connect(pItem, SIGNAL(eqpDataChanged(ProfileLhcLineItem *, bool)),
			this, SLOT(eqpDataChange(ProfileLhcLineItem *, bool)));
		connect(pItem, SIGNAL(visibilityChanged(ProfileLhcLineItem *)),
			this, SLOT(itemVisibilityChanged(ProfileLhcLineItem *)));
		connect(pItem, SIGNAL(eqpSelectChanged(bool)), this, SIGNAL(eqpSelectChanged(bool)));
		connect(pItem, SIGNAL(mobileStateChanged(ProfileLhcLineItem *)),
			this, SLOT(mobileStateChange(ProfileLhcLineItem *)));
	}
}

/*
**	FUNCTION
**		Analyze which equipment types will be shown in view
**
**	PARAMETERS
**		allVacTypeMask		- OR'ed bit mask for all vacuum types
**		allEqpMask			- Equipment type mask for all equipment to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allEqpMask)
{
	int idx;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		ProfileLhcLineItem *pEqpItem = eqps.at(idx);
		if(pEqpItem->getEqp()->isActive(mode))
		{
			allVacTypeMask |= pEqpItem->getEqp()->getVacType();
			allEqpMask.append(pEqpItem->getEqp()->getFunctionalType());
		}
	}

	for(idx = 0 ; idx < cryoTs.count() ; idx++)
	{
		ProfileThermometer *pThermoItem = cryoTs.at(idx);
		allVacTypeMask |= pThermoItem->getEqp()->getVacType();
		allEqpMask.append(pThermoItem->getEqp()->getFunctionalType());
	}
}

/*
**	FUNCTION
**		Calculate hide/show parameters for all devices using
**		new equipment type masks
**
**	PARAMETERS
**		eqpMask		- Equipment type mask for device types to show
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcLine::applyMask(VacEqpTypeMask &eqpMask)
{
	this->eqpMask = eqpMask;

	// Process pressure devices
	int idx;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		ProfileLhcLineItem *pEqpItem = eqps.at(idx);
		if(!pEqpItem->getEqp()->isActive(mode))
		{
			if(!pEqpItem->isHidden())
			{
				pEqpItem->hide();
			}
			continue;
		}
		if(eqpMask.contains(pEqpItem->getEqp()->getFunctionalType()))	// To be shown
		{
			if(pEqpItem->isHidden())
			{
				pEqpItem->show();
			}
		}
		else
		{
			if(!pEqpItem->isHidden())
			{
				pEqpItem->hide();
			}
		}
	}
	curEqpIdx = 0;

	// Process CRYO thermometers
	for(idx = 0 ; idx < cryoTs.count() ; idx++)
	{
		ProfileThermometer *pThermoItem = cryoTs.at(idx);
		if(eqpMask.contains(pThermoItem->getEqp()->getFunctionalType()))	// To be shown
		{
			if(pThermoItem->isHidden())
			{
				pThermoItem->show();
			}
		}
		else
		{
			if(!pThermoItem->isHidden())
			{
				pThermoItem->hide();
			}
		}
	}
	curCryoTIdx = 0;
}

/*
**	FUNCTION
**		The whole line shall be hidden - calculate toHide flag for all devices
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcLine::hide(void)
{
	// Process pressure devices
	int idx;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		ProfileLhcLineItem *pEqpItem = eqps.at(idx);
		if(!pEqpItem->isHidden())
		{
			pEqpItem->hide();
		}
	}

	// Process CRYO thermometers
	for(idx = 0 ; idx < cryoTs.count() ; idx++)
	{
		ProfileThermometer *pThermoItem = cryoTs.at(idx);
		if(!pThermoItem->isHidden())
		{
			pThermoItem->hide();
		}
	}
	hidden = true;
}

/*
**	FUNCTION
**		Find next position of element to be processed when
**		calculating bar geometry
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Position of next element to process, or
**		huge value if no more elements shall be processed
**
** CAUTIONS
**		None
*/
float ProfileLhcLine::nextPosition(void)
{
	float	result = HUGE_COORDINATE;

	for( ; curEqpIdx < eqps.count() ; curEqpIdx++)
	{
		ProfileLhcLineItem *pItem = eqps.at(curEqpIdx);
		if(pItem->shallBeVisible() && ((!pItem->isHidden()) || pItem->isAtSectorBorder()))
		{
			break;
		}
	}
	if(curEqpIdx < eqps.count())
	{
		result = eqps.at(curEqpIdx)->getStart();	// result = eqps.at(curEqpIdx)->getEqp()->getStart();
	}
	return result;
}

/*
**	FUNCTION
**		Assign bar index to current device in profile given
**		by curEqpIdx or by curCryoTIdx (depending on flag processCryoT)
**
**	PARAMETERS
**		coord	- Last processed physical location, value can be
**					changed by this method
**		barIdx	- Last used bar index, value can be changed by this
**					method.
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::placeItem(float &coord, int &barIdx)
{
	if(curEqpIdx >= eqps.count())
	{
		return;		// Can not happen, just to make sure
	}
	ProfileLhcLineItem *pItem = eqps.at(curEqpIdx);
	
	// Make sure bar numbers increase with coordinate
	if(pItem->shallBeVisible() && (!pItem->isHidden()) && (pItem->getStart() > coord))
	{
		if(coord >= 0)
		{
			barIdx++;
		}
		coord = pItem->getStart();
	}

	// Make sure bar does not overlap with prevfious visible bar
	if(barIdx < 0)
	{
		barIdx = 0;
	}
	else if(pItem->shallBeVisible() && (!pItem->isHidden()))
	{
		for(int prevIdx = curEqpIdx - 1 ; prevIdx >= 0 ; prevIdx--)
		{
			ProfileLhcLineItem *pPrev = eqps.at(prevIdx);
			if(pPrev->isHidden() || (!pPrev->shallBeVisible()))
			{
				continue;
			}
			if(barIdx <= pPrev->getBarIdx())
			{
				barIdx = pPrev->getBarIdx() + 1;
			}
			break;
		}
	}

	// Assign bar number, move to next equipment index
	pItem->setBarIdx(barIdx);
	curEqpIdx++;
}

/*
**	FUNCTION
**		Check if equipment with linear vertical scale exists in this line
**
**	PARAMETERS
**		hasLinearEqp	- Variable where true shall be written if such equipment exists
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::checkLinEqpExistence(bool &hasLinearEqp)
{
	if(hasLinearEqp)
	{
		return;	// Found in another line
	}
	for(int idx = cryoTs.count() - 1 ; idx >= 0 ; idx--)
	{
		ProfileThermometer *pItem = cryoTs.at(idx);
		if(!pItem->isHidden())
		{
			hasLinearEqp = true;
			return;
		}
	}
}

/*
**	FUNCTION
**		Set limits for logarithmic vertical axis on cryo vacuum
**
**	PARAMETERS
**		min	- New axis minimum
**		max	- New axis maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::setLogCryoLimits(int min, int max)
{
	switch(type)
	{
	case Qrl:
	case Cryo:
		pLogAxis->setLimits(min, max);
		break;
	default:
		break;
	}
}

/*
**	FUNCTION
**		Set limits for logarithmic vertical axis on beam vacuum
**
**	PARAMETERS
**		min	- New axis minimum
**		max	- New axis maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::setLogBeamLimits(int min, int max)
{
	switch(type)
	{
	case OuterBeam:
	case InnerBeam:
		pLogAxis->setLimits(min, max);
		break;
	default:
		break;
	}
}

/*
**	FUNCTION
**		Set limits for linear vertical axis
**
**	PARAMETERS
**		min	- New axis minimum
**		max	- New axis maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::setLinLimits(int min, int max)
{
	pLinAxis->setLimits(min, max);
}

/*
**	FUNCTION
**		Calculate geometry for vertical axis
**
**	PARAMETERS
**		axisLocation	- Maximum X-coordinate of vertical axis, value can be update
**							by this method
**		hasLinearEqp	- Flag indicating if there is linear equipment in at least one line
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::calcAxisGeometry(int &axisLocation)
{
	// TODO: in fact, pParam->yGrid is just ignored now
	pLogAxis->setGraphArea(area);
	pLogAxis->setGridStyle(Qt::SolidLine);
	pLogAxis->setTickOutside(false);
	pLogAxis->prepare(parent->fontMetrics());
	if(pLogAxis->getAxisLocation() > axisLocation)
	{
		axisLocation = pLogAxis->getAxisLocation();
	}
	if(parent->isHasLinearEqp())
	{
		pLinAxis->setGraphArea(area);
		pLinAxis->setGridStyle(Qt::SolidLine);
		pLinAxis->setTickOutside(false);
		pLinAxis->setLinLimitsFixed(true);
		pLinAxis->fixMinAt(pLogAxis->getScaleBottom());
		pLinAxis->fixMaxAt(pLogAxis->getScaleTop());
		pLinAxis->prepare(parent->fontMetrics());
	}
}

/*
**	FUNCTION
**		Set required horizontal location for logarithmic Y-axis
**		The method is used to make sure all axis of all graphs
**		will be at the same location, hence, all bar sizes/locations
**		will be the same in all graphs
**
**	PARAMETERS
**		axisLocation	- New location to set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::setAxisLocation(int axisLocation)
{
	int	delta = axisLocation - pLogAxis->getAxisLocation();
	/*
	if(!delta)
	{
		return;
	}
	*/
	area.setLeft(area.left() + delta);
	pLogAxis->setGraphArea(area);
	// qDebug("axisMinLocation %d\n", axisMinLocation);
	pLogAxis->fixMinAt(axisMinLocation);
	pLogAxis->prepare(parent->fontMetrics());

	// Even if linear axis is not used - preparing shall not do something wrong
	pLinAxis->setGraphArea(area);
	pLinAxis->fixMinAt(axisMinLocation);
	pLinAxis->prepare(parent->fontMetrics());
}

/*
**	FUNCTION
**		Calculate geometry for all bars in graph.
**
**	PARAMETERS
**		maxBarIdx	-	 Maximum bar index
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::calcBarGeometry(int &maxBarIdx)
{
	// Calculate graph area limits
	leftX = pLogAxis->getAxisLocation();
	if(parent->isHasLinearEqp())
	{
		rightX = pLinAxis->getAxisLocation() - 1;
	}
	else
	{
		rightX = area.right() - 1;
	}
	topLogY = pLogAxis->getScaleTop();
	bottomLogY = pLogAxis->getScaleBottom();
	topLinY = pLinAxis->getScaleTop();
	bottomLinY = pLinAxis->getScaleBottom();

	// Calculate vertical scale coefficients
	// double plotAreaHeight = bottomLogY - topLogY;
	int	useMin, useMax;
	pLogAxis->getLimits(useMin, useMax);
	double scaleHeight = useMax - useMin;
	double scaleMax = pow(10.0, useMax);
	double scaleMin = pow(10.0, useMin);

	// Calculate one bar width and horizontal scale coefficients
	double xScale = (double)(rightX - leftX) / (double)(maxBarIdx + 1);
	double oneBarWidth = xScale * ((double)VacMainView::getProfileBarWidth() / 100.0);
	useNarrowBar = oneBarWidth < 2.0;

	// Calculate geometry of all bars
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		ProfileLhcLineItem *pEqpItem = eqps.at(idx);
		double barNumber = pEqpItem->getBarIdx();
		QRect &rect = pEqpItem->getRect();
		if(useNarrowBar)
		{
			rect.setLeft((int)rint(leftX + (barNumber + 0.5) * xScale));
			rect.setRight(rect.left());
		}
		else
		{
			rect.moveLeft((int)rint(leftX + (barNumber + 0.5) * xScale - oneBarWidth / 2.));
			rect.setRight((int)rint(rect.left() + oneBarWidth));
		}
		if(pEqpItem->isHidden() || (!pEqpItem->shallBeVisible()))
		{
			continue;
		}
		barVerticalGeom(pEqpItem, scaleMin, scaleMax, scaleHeight, useMin);
	}

	// Calculate geometry of all CRYO thermometers

	// Calculation shall be done after bar geometry has been calculated for all lines.
	// So, calculation has been moved to separate method calcCryoTtGeometry().
	// L.Kopylov 29.04.2011
}

void ProfileLhcLine::calcCryoTtGeometry(void)
{
	int	useMin, useMax;
	pLinAxis->getLimits(useMin, useMax);
	for(int idx = 0 ; idx < cryoTs.count() ; idx++)
	{
		ProfileThermometer *pThermoItem = cryoTs.at(idx);
		QPoint &point = pThermoItem->getPoint();
		point.setX(parent->bestPositionForEqp(pThermoItem->getRealStart()));
		cryoTVerticalGeom(pThermoItem, useMin, useMax);
	}
}

/*
**	FUNCTION
**		Find best screen position for given world coordinate. The method is called
**		after all pressure measurement devices and sectors have been placed to find
**		screen position for thermometers.
**
**	PARAMETERS
**		coord		- Required coordinate (LHCLAYOUT DCUM)
**		bestDelta	- The best difference of world coordinate from found coordinate,
**						calculated by another line, can be updated by this line
**		screenCoord	- Calculated screen coordinate is returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcLine::bestPositionForEqp(float coord, float &bestDelta, int &screenCoord)
{
	if(bestDelta == 0.0)
	{
		return;
	}
	float	myBestDelta = bestDelta;
	ProfileLhcLineItem	*pBestItem = NULL;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		ProfileLhcLineItem *pItem = eqps.at(idx);
		if(pItem->isHidden() || (!pItem->shallBeVisible()))
		{
			continue;
		}
		float delta = (float)fabs(pItem->getRealStart() - coord);
		if(delta < myBestDelta)
		{
			myBestDelta = delta;
			pBestItem = pItem;
		}
	}

	// Two major variants are possible
	// 1) thermometer position is equal to position of pressure measurement device.
	//		In this case thermometer shall get coordinate in the middle of pressure
	//		measurement device (or in the middle of group if several devices have the
	//		same coordinate)
	// 2) thermometer position is before/after position of pressure measurement device.
	//		In this case thermometer shall get coordinate of left/right bar coordinate,
	//		here it also shall be taken into account that there can be a group of devices
	//		with the same coordinate
	if(myBestDelta < bestDelta)
	{
		int startIdx = eqps.indexOf(pBestItem);
		bestDelta = myBestDelta;
		if(myBestDelta == 0)	// Case 1)
		{
			QRect &rect = pBestItem->getRect();
			int	startCoord = rect.left(),
				endCoord = rect.right();
			for(int idx = startIdx + 1 ; idx < eqps.count() ; idx++)
			{
				ProfileLhcLineItem *pItem = eqps.at(idx);
				if(pItem->isHidden() || (!pItem->shallBeVisible()))
				{
					continue;
				}
				if(pItem->getRealStart() != coord)
				{
					break;
				}
				endCoord = pItem->getRect().right();
			}
			screenCoord = (startCoord + endCoord) >> 1;
			return;
		}
		// Case 2
		float bestCoord = pBestItem->getRealStart();
		if(pBestItem->getRealStart() < coord)	// thermometer after device
		{
			for(int idx = startIdx ; idx < eqps.count() ; idx++)
			{
				ProfileLhcLineItem *pItem = eqps.at(idx);
				if(pItem->isHidden() || (!pItem->shallBeVisible()))
				{
					continue;
				}
				if(pItem->getRealStart() != bestCoord)
				{
					break;
				}
				screenCoord = pItem->getRect().right();
			}
		}
		else	// Thermometer before device
		{
			for(int idx = startIdx ; idx >= 0 ; idx--)
			{
				ProfileLhcLineItem *pItem = eqps.at(idx);
				if(pItem->isHidden() || (!pItem->shallBeVisible()))
				{
					continue;
				}
				if(pItem->getRealStart() != bestCoord)
				{
					break;
				}
				screenCoord = pItem->getRect().left();
			}
		}
	}
}

/*
**	FUNCTION
**		Calculate vertical bar geometry for single profile item
**
**	PARAMETERS
**		pItem		- Pointer to profile item
**		scaleMin	- Minimum of vertical scale
**		scaleMax	- Maximum of vertical scale
**		scaleHeight	- Height of vertical scale in LOGARITHMIC units
**		useMin		- Logarithm of vertical scale minimum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcLine::barVerticalGeom(ProfileLhcLineItem *pItem,
	double scaleMin, double scaleMax, double scaleHeight, int useMin)
{
	QRect &rect = pItem->getRect();

	/* Always show the value even if it is not valid - LIK 30.07.2008
	if(pItem->isValid())
	*/
	{
		double value = pItem->getValue();
		if(value > scaleMin)
		{
			rect.setBottom(bottomLogY - 1);
		}
		else
		{
			rect.setBottom(bottomLogY + 3);
		}
		if(value <= scaleMax)
		{
			if(value > scaleMin)
			{
				rect.setTop((int)rint(topLogY + (bottomLogY - topLogY) *
					(1.0 - (log10(value) - useMin) / scaleHeight)));
			}
			else
			{
				rect.setTop(bottomLogY);
			}
		}
		else
		{
			rect.setTop(topLogY - 4);
		}
	}
	/* See comment above
	else
	{
		pItem->bottomY = bottom - 1;
		pItem->topY = topLogY + 2;
	}
	*/
}

/*
**	FUNCTION
**		Calculate vertical line geometry for single CRYO thermometer
**
**	PARAMETERS
**		pItem		- Pointer to thermometer
**		scaleMin	- Minimum of vertical scale
**		scaleMax	- Maximum of vertical scale
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcLine::cryoTVerticalGeom(ProfileThermometer *pItem, int scaleMin, int scaleMax)
{
	QPoint &point = pItem->getPoint();

	if(pItem->isValid())
	{
		double value = pItem->getValue();
		if(value < scaleMin)
		{
			point.setY(area.bottom() - 1);
		}
		else if(value <= scaleMax)
		{
			point.setY((int)rint(topLinY + (bottomLinY - topLinY) *
				(1.0 - (value - scaleMin) / (scaleMax - scaleMin))));
		}
		else
		{
			point.setY(topLinY - 4);
		}
	}
	else
	{
		point.setY(area.bottom() - 1);
	}
}

/*
**	FUNCTION
**		Draw profile line
**
**	PARAMETERS
**		painter				- Painter used for drawing
**		flashingOnly		- Flag indicating if only flashing bars shall be redrawn
**		useAltFlashColor	- Flag indicating if flashing bars shall be drawn using
**								alternative colors
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::draw(QPainter &painter, bool flashingOnly, bool useAltFlashColor)
{
//qDebug("ProfileLhcLine::draw: start\n");
	if(hidden)
	{
		return;
	}
	if(!flashingOnly)
	{
		nBarsToDraw = 0;
//qDebug("ProfileLhcLine::draw: eqp count %d\n", eqps.count());
		for(int idx = 0 ; idx < eqps.count() ; idx++)
		{
			ProfileLhcLineItem *pItem = eqps.at(idx);
			if((!pItem->isHidden()) && pItem->shallBeVisible())
			{
				nBarsToDraw++;
			}
		}
//qDebug("ProfileLhcLine::draw: nBarsToDraw %d\n", nBarsToDraw);

		// Draw sector areas
		QFontMetrics fm = painter.fontMetrics();
		buildSectors(fm);
//qDebug("ProfileLhcLine::draw: buildSectors\n");
		drawSectors(painter);
//qDebug("ProfileLhcLine::draw: drawSectors\n");
		// Draw sector labels
		if(showSectorNames)
		{
			for(int idx = 0 ; idx < sectors.count() ; idx++)
			{
				ProfileLineSector *pSector = sectors.at(idx);
				pSector->drawLabel(painter);
			}
		}
		// Draw rectangle, axis and Y-axis labels
		painter.setPen(Qt::black);
		QRect rect;
		rect.setTop(topLogY);
		rect.setBottom(pLogAxis->getScaleBottom() - 1);
		rect.setLeft(leftX);
		rect.setRight(rightX);
		painter.drawRect(rect);

		// Draw axis and labels
		pLogAxis->setGridLineEnd(parent->isHasLinearEqp() ? pLinAxis->getAxisLocation() : area.right());
		pLinAxis->setGridLineEnd(pLogAxis->getAxisLocation());
		pLogAxis->setGrid(!parent->isLinearSelected());
		pLogAxis->draw(painter);
		if(parent->isHasLinearEqp())
		{
			pLinAxis->setGrid(parent->isLinearSelected());
			pLinAxis->draw(painter);
		}
	}

//qDebug("ProfileLhcLine::draw: before drawBars\n");
	// Draw bars
	drawBars(painter, flashingOnly, useAltFlashColor);

//qDebug("ProfileLhcLine::draw: before drawLinear\n");
	// Draw CRYO thermometers
	drawLinear(painter);
//qDebug("ProfileLhcLine::draw: FINISH\n");
}

/*
**	FUNCTION
**		Add one sector area to list of sector areas in profile line
**
**	PARAMETERS
**		pSector		- Pointer to filled sector area parameters
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcLine::addSector(ProfileLineSector *pSector)
{
	if(sectors.count())	// Check if previously added sector is the same as new one
	{
		ProfileLineSector *pPrevSector = sectors.last();
		if(((pPrevSector->getSector1() == pSector->getSector1()) &&
			(pPrevSector->getSector2() == pSector->getSector2())) ||
			((pPrevSector->getSector2() == pSector->getSector1()) &&
			(pPrevSector->getSector1() == pSector->getSector2())))
		{
			pPrevSector->setRightX(pSector->getRightX());
			return;
		}
	}
	pSector->setTransition(pSector->getSector1() && pSector->getSector2() &&
		(pSector->getSector1() != pSector->getSector2()));
	ProfileLineSector *pNewSector = new ProfileLineSector(*pSector);
	sectors.append(pNewSector);
}

/*
**	FUNCTION
**		Calculate number of CRYO thermometers of given subtype
**		in one vacuum sector
**
**	PARAMETERS
**		pSector	- pointer to sector in question
**
**	RETURNS
**		Number of thermometers of given subtype in sector
**
**	CAUTIONS
**		None
**
*/
int ProfileLhcLine::numberOfCryoThermPerSector(Sector *pSector, int subType)
{
	int result = 0;
	for(int idx = 0 ; idx < cryoTs.count() ; idx++)
	{
		ProfileThermometer *pItem = cryoTs.at(idx);
		if(pItem->getSubType() != subType)
		{
			continue;
		}
		if((pSector->getStart() <= pItem->getRealStart()) && (pItem->getRealStart() <= pSector->getEnd()))
		{
			result++;
		}
	}
	return result;
}

int ProfileLhcLine::calculateSectorLabels(QFontMetrics &fontMetrics)
{
	if(!(showSectorNames && sectors.count()))
	{
		return -1;
	}
	int maxHeight = 0;
	int labelIdx = 0;
	for(int idx = 0 ; idx < sectors.count() ; idx++)
	{
		ProfileLineSector *pSector = sectors.at(idx);
		if(!pSector->needLabel())
		{
			continue;
		}
		pSector->calculateLabelRect(fontMetrics, area.bottom());
		int labelHeight = pSector->getLabelRect().height();
		if(labelHeight > maxHeight)
		{
			maxHeight = labelHeight;
		}
		if(labelIdx & 0x1)
		{
			pSector->moveLabelRectUp();
		}
		labelIdx++;
	}
	return area.bottom() - maxHeight * 2;
}

void ProfileLhcLine::calculateAxisMinLocation(QFontMetrics &fontMetrics)
{
	if(!showSectorNames)
	{
		axisMinLocation = -1;
		return;
	}
	QRect rect = fontMetrics.boundingRect("ABC.14789QMRB");
	axisMinLocation = area.bottom() - rect.height() * 2;

}

/*
**	FUNCTION
**		Draw background of sector areas
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcLine::drawSectors(QPainter &painter)
{
	if(!sectors.count())
	{
		return;
	}
	switch(VacMainView::getProfileSectorDrawMethod())
	{
	case 1:
		drawSectorsWithBorders(painter);
		break;
	default:
		drawSectorsHatchFill(painter);
		break;
	}
}

/*
**	FUNCTION
**		Calculate color for background of sector areas
**
**	PARAMETERS
**		vacType	- Vacuum type of sector
**
**	RETURNS
**		Color for sector area background
**
**	CAUTIONS
**		None
*/
QColor ProfileLhcLine::sectorBackColor(int vacType)
{
	QColor	result(parent->palette().color(QPalette::Window));
	switch(vacType)
	{
	case VacType::RedBeam:
		result = VacMainView::getProfileBackColorRedBeam();
		break;
	case VacType::BlueBeam:
		result = VacMainView::getProfileBackColorBlueBeam();
		break;
	case VacType::CrossBeam:
	case VacType::CommonBeam:
		result = VacMainView::getProfileBackColorCrossBeam();
		break;
	case VacType::Qrl:
		result = VacMainView::getProfileBackColorQrl();
		break;
	case VacType::Cryo:
		result = VacMainView::getProfileBackColorCryo();
		break;
	default:
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Draw bars
**
**	PARAMETERS
**		painter				- Painter used for drawing
**		flashBarsOnly		- true if only flashing bars shall be redrawn
**		useAltFlashColor	- true if flashing bars shall be drawn using alternative color
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::drawBars(QPainter &painter, bool flashBarsOnly, bool useAltFlashColor)
{
	const QColor &backColor = parent->palette().color(QPalette::Window);
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		ProfileLhcLineItem *pItem = eqps.at(idx);
		pItem->draw(painter, backColor, flashBarsOnly, useAltFlashColor);
	}
}

/*
**	FUNCTION
**		Draw thermometer values
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::drawLinear(QPainter &painter)
{
	for(int idx = 0 ; idx < cryoTs.count() ; idx++)
	{
		ProfileThermometer *pItem = cryoTs.at(idx);
		pItem->draw(painter);
	}
}

/*
**	FUNCTION
**		Decide which vacuum type of line part shall be added
**		for given region type
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Enum corresponding to required vacuum type,
**		VacType::None if line part shall not be added.
**
** CAUTIONS
**		None
*/
int ProfileLhcLine::vacTypeOfRegion(int regionType)
{
	int	result = VacType::None;
	switch(regionType)
	{
	case LhcRegion::BlueOut:
		if(type == OuterBeam)
		{
			result = VacType::BlueBeam;
		}
		else if(type == InnerBeam)
		{
			result = VacType::RedBeam;
		}
		break;
	case LhcRegion::RedOut:
		if(type == OuterBeam)
		{
			result = VacType::RedBeam;
		}
		else if(type == InnerBeam)
		{
			result = VacType::BlueBeam;
		}
		break;
	case LhcRegion::Cross:
		if((type == OuterBeam) || (type == InnerBeam))
		{
			result = VacType::CrossBeam;
		}
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Find profile item located under mouse pointer
**
**	PARAMETERS
**		x		- Mouse X coordinate
**		y		- Mouse Y coordinate
**		rect	- Variable where device area on profile will be written
**
**	RETURNS
**		Pointer to profile item under mouse pointer; or
**		NULL if there is no profile item under mouse pointer
**
**	CAUTIONS
**		None
*/
Eqp *ProfileLhcLine::findItemUnderMouse(int x, int y, QRect &rect)
{
	// First search for closets thermometer (they are drawn on top of pressure devices) ...
	Eqp *result = closestCryoT(x, y, rect);
	if(result)
	{
		return result;
	}

	if(x < pLogAxis->getAxisLocation())
	{
		return NULL;
	}

	// ... then search for closest bar for pressure devices
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		ProfileLhcLineItem *pItem = eqps.at(idx);
		if(pItem->isHidden() || (!pItem->shallBeVisible()))
		{
			continue;
		}
		QRect &barRect = pItem->getRect();
		if(barRect.contains(x, y))
		{
			rect = barRect;
			return pItem->getEqp();
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Return pointer to CRYO thermometer closest to mouse pointer.
**
**	PARAMETERS
**		x		- Mouse X coordinate
**		y		- Mouse Y coordinate
**		rect	- Variable where device area on profile will be written
**
**	RETURNS
**		Pointer to device closest to mouse pointer, or
**		NULL if there is no device at pointer position
**
**	CAUTIONS
**		None
*/
Eqp *ProfileLhcLine::closestCryoT(int x, int y, QRect &rect)
{
	if(x < pLogAxis->getAxisLocation())
	{
		return NULL;
	}

	for(int idx = 0 ; idx < cryoTs.count() ; idx++)
	{
		ProfileThermometer *pItem = cryoTs.at(idx);
		if(pItem->isHidden())
		{
			continue;
		}
		QRect thermoRect = pItem->getRect();
		if(thermoRect.contains(x, y))
		{
			rect = thermoRect;
			return pItem->getEqp();
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Slot activated when new data arrived for one of devices in this line.
**		Calculate new bar geometry for bar representing this device.
**		Emit changed signal if either bar or color has been changed
**
**	PARAMETERS
**		pItem			- Pointer to item - source of signal
**		colorChanged	- Flag indicated if bar color has changed
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::eqpDataChange(ProfileLhcLineItem *pItem, bool colorChanged)
{
	QRect originalRect(pItem->getRect());
	int	useMin, useMax;
	pLogAxis->getLimits(useMin, useMax);
	double scaleHeight = useMax - useMin;
	double scaleMax = pow(10.0, useMax);
	double scaleMin = pow(10.0, useMin);

	barVerticalGeom(pItem, scaleMin, scaleMax, scaleHeight, useMin);
	if(colorChanged || (originalRect != pItem->getRect()))
	{
		emit eqpDataChanged();
	}
}

/*
**	FUNCTION
**		Slot activated when activity of mobile device in this line has been changed.
**		Immediate redraw of view is needed if device was shown (or will be shown)
**		according to current equipment mask. Update of menu in dialog is needed
**		anyway.
**
**	PARAMETERS
**		pItem			- Pointer to item - source of signal
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::mobileStateChange(ProfileLhcLineItem *pItem)
{
	emit mobileStateChanged(eqpMask.contains(pItem->getEqp()->getFunctionalType()));
}

void ProfileLhcLine::itemVisibilityChanged(ProfileLhcLineItem * /* pItem */)
{
	emit eqpVisibilityChanged();
}

/*
**	FUNCTION
**		Slot activated when new data arrived for one of thermometers in this line.
**		Calculate new geometry for represention of this device.
**		Emit changed signal if geometry has been changed
**
**	PARAMETERS
**		pItem	- Pointer to item - source of signal
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::thermoDataChange(ProfileThermometer *pItem)
{
	QPoint originalPoint(pItem->getPoint());
	int	useMin, useMax;
	pLinAxis->getLimits(useMin, useMax);
	cryoTVerticalGeom(pItem, useMin, useMax);
	if(originalPoint != pItem->getPoint())
	{
		emit eqpDataChanged();
	}
}

/*
**	FUNCTION
**		Dump content of line to output file - for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for write
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLine::dump(FILE *pFile)
{
	if(!DebugCtl::isProfile())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	fprintf(pFile, "\n\n++++++++++++++ Line %d %s nParts %d nEqp %d nCryoT %d\n", type,
		(hidden ? "HIDDEN" : ""), pPartList->count(), eqps.count(), cryoTs.count());
	fprintf(pFile, " start %f end %f\n", start, end);
	fprintf(pFile, " left %d top %d right %d bottom %d\n", area.left(), area.top(),
		area.right(), area.bottom());

	fprintf(pFile, "   vacTypeMask %X\n", vacTypeMask);
	fprintf(pFile, "   ALLOWED EQP MASK:\n");
	allowedEqpMask.dump(pFile);
	fprintf(pFile, "\n   EQP MASK:\n");
	eqpMask.dump(pFile);

	fprintf(pFile, " PARTS:\n");
	int n;
	for(n = 0 ; n < pPartList->count() ; n++)
	{
		VacLinePart *pPart = pPartList->at(n);
		pPart->dump(pFile, n);
	}

	fprintf(pFile, " SECTORS:\n");
	for(n = 0 ; n < sectors.count() ; n++)
	{
		ProfileLineSector *pSect = sectors.at(n);
		pSect->dump(pFile, n);
	}

	fprintf(pFile, " EQUIPMENT:\n");
	for(n = 0 ; n < eqps.count() ; n++)
	{
		ProfileLhcLineItem *pEqpItem = eqps.at(n);
		pEqpItem->dump(pFile, n);
	}

	fprintf(pFile, " CRYO thermometers:\n");
	for(n = 0 ; n < cryoTs.count() ; n++)
	{
		ProfileThermometer *pThermoItem = cryoTs.at(n);
		pThermoItem->dump(pFile, n);
	}
}
