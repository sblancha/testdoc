
// List of DPEs for polling
dyn_string dpePollList;

// Period for polling
global int  pollPeriod;

// ID of thread performing polling
int pollThreadId;

// Flag indicating if polling is in pending state
bool pollPending;

// Process dialogRequest() event from any main view
void VacProcessDialogRequest(int type, unsigned vacType, dyn_string sectorList, int mode)
{
  unsigned act = 0;
  switch(type)
  {
  case QT_DIALOG_SYNOPTIC:
     act = LVA_OPEN_SYNOPTIC;
     break;
  case QT_DIALOG_PROFILE:
    act = LVA_OPEN_PROFILE;
    break;
  case QT_DIALOG_SECTOR:
    act = LVA_OPEN_SECTOR;
    break;
  }
  if(act == 0)
  {
    return;
  }
  dyn_string exceptionInfo;
  int useMode = mode == DIALOG_MODE_POLLING ? DIALOG_MODE_ONLINE : mode;
  LvaDialogAction(makeDynAnytype(act, mode), sectorList, "", vacType, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

// Process dpConnectRequest() event from any main view
void VacProcessDpConnectRequest(string callback = "")
{
  if(callback == "")
  {
    callback = "VacNormalNewValueCb";
  }

//  DebugTN("dpConnectRequest()"); 
  string dpe;
  bool connect;
  LhcVacNextDpConnect(dpe, connect);
  while(dpe != "")
  { 
//    DebugTN(dpe, connect);
    if(VacIsDriverOkDPE(dpe))
    {
      LhcVacNewOnlineValue(dpe, true, getCurrentTime());
    }
    else
    {
      string pureDpeName = dpSubStr(dpe, DPSUB_DP_EL);
      if(connect)
      {
        dpConnect(callback, dpe, pureDpeName + ":_original.._stime");
      }
      else
      {
        dpDisconnect(callback, dpe, pureDpeName + ":_original.._stime");
      }
    }
    LhcVacNextDpConnect(dpe, connect);
  }
}

void VacNormalNewValueCb(string valueDpe, anytype value, string stimeDpe, time stime)
{
  string dpeName = dpSubStr(valueDpe, DPSUB_DP_EL);
  LhcVacNewOnlineValue(dpeName, value, stime);
}

// Process dpeHistoryDepthRequest() event from any main view
void VacProcessDpeHistoryDepthRequest()
{
  string dpe;
  time startTime;
  int mobileType;
// DebugTN("dpeHistoryDepthRequest() start");
  LhcVacNextDpeHistoryDepthQuery(dpe, startTime, mobileType);
  while(dpe != "")
  {
// DebugTN("dpeHistoryDepthRequest(): <" + dpe + ">, type " + mobileType, startTime);
    VacEqpFindFirstArchiveTime(dpe, mobileType, startTime);
    LhcVacNextDpeHistoryDepthQuery(dpe, startTime, mobileType);
  }
}

// Process dpeHistoryQueryQueueChange() event from any main view
void VacProcessDpeHistoryQueryQueueChange()
{
// DebugTN("dpeHistoryQueryQueueChange() - start");
  int id, bitNbr;
  string dpe;
  bool bitState;
  time startTime, endTime;
  while(LhcVacNextHistoryQuery(id, dpe, bitState, bitNbr, startTime, endTime) > 0)
  {
// DebugTN("Next:", id, dpe, startTime, endTime);
    dyn_string exceptionInfo;
    VacProcessHistoryRequest(dpe, bitState, bitNbr, startTime, endTime, id, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("VacProcessHistoryRequest(" + id + "," + dpe + ") failed:", exceptionInfo);
    }
  }
}

// Process 'dpMouseDown()' event from any main view
void VacProcessDpMouseDown(int button, int mode, int x, int y, int extra, string dpName,
                           bool showSynopticMenu = true)
{
  // Select device anyway
  LhcVacSelectEqp(dpName);

  // Show popup menu for button 2
  if(button != 2)
  {
    return;
  }

  int  useMode = mode == DIALOG_MODE_POLLING ? DIALOG_MODE_ONLINE : mode;
  switch(extra)
  {
  case EQP_PART_VVR:  // Roughing valve of VPG 
    VacPopupMenuVVR(useMode, x, y, dpName);
    return;
  case EQP_PART_VVR1: // Roughing valve #1 of VPG 
    VacPopupMenuVVR1(useMode, x, y, dpName);
    return;
  case EQP_PART_VVR2: // Roughing valve #2 of VPG
    VacPopupMenuVVR2(useMode, x, y, dpName);
    return;
    case EQP_PART_VG1: // Gauge #1 of VPG 
    VacPopupMenuVG1(useMode, x, y, dpName);
    return;
  case EQP_PART_VG2: // Gauge #2 of VPG 
    VacPopupMenuVG2(useMode, x, y, dpName);
    return;
  default:
    break;
  }
  string dpType;
  dyn_string exceptionInfo;
  LhcVacPopupSymbolMenu(useMode, dpName, dpType, true, x, y, exceptionInfo, showSynopticMenu);
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

// process mouse events from documentation dialogs
void VacProcessDocumentationMouseDown(int button, int mode, int x, int y, int extra, string eqpName)
{
  
  if(button != 2)
	{
		return;
	}
  
	dyn_string exceptionInfo;
	LhcVacPopupDocumentationMenu(mode, eqpName, true, x, y, exceptionInfo);
	if(dynlen(exceptionInfo) > 0)
	{
		fwExceptionHandling_display(exceptionInfo);
	}
}

// Process eqpDoubleClick() event from any main view.
// Default behaviour is to open detail panel IF there is such item in default
// menu for DP type.
void VacProcessEqpDoubleClick(string dpName, int mode)
{
  dyn_string possibleActions, exceptionInfo;
  string dpType;
  int typeIdx = LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo);
  if(dynlen(exceptionInfo) == 0)
  {
    if(typeIdx > 0)
    {
      possibleActions = makeDynString(LVA_OPEN_DETAIL_PANEL, LVA_OPEN_EXP_AREA_DETAILS);
      int actIdx;
      for(actIdx = dynlen(glDevMenuActs[typeIdx]) ; actIdx > 0 ; actIdx--)
      {
        if(dynContains(possibleActions, glDevMenuActs[typeIdx][actIdx]) != 0)
        {
          break;
        }
      }
      if(actIdx <= 0)
      {
        return;  // No detail panel action for DP type
      }
      dyn_anytype act = makeDynAnytype(glDevMenuActs[typeIdx][actIdx], mode);
      LvaDialogAction(act, makeDynString(), dpName, 0, exceptionInfo);
    }
    else
    {
      fwException_raise(exceptionInfo, "ERROR", "VacProcessEqpDoubleClick(): unknown DP type for DP <" +
                        dpName + ">", "");
    }
  }
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

// Process historySaveMultiRequest() event from any main view
void VacProcessHistorySaveMultiRequest()
{
  if(!VacUserHasMinimumPrivilege())
  {
    return;
  }
  dyn_string exceptionInfo;
  LvaDialogAction(makeDynAnytype(LVA_OPEN_HISTORY_CONFIG, DIALOG_MODE_ONLINE, -2, false), makeDynString(),"", 0, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

// Process historySaveRequest() event from any main view
void VacProcessHistorySaveRequest(int historyId)
{
  if(!VacUserHasMinimumPrivilege())
  {
    return;
  }
  dyn_string exceptionInfo;
  LvaDialogAction(makeDynAnytype(LVA_OPEN_HISTORY_CONFIG, DIALOG_MODE_ONLINE, historyId, false), makeDynString(),"", 0, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

// Process mobileHistoryRequest() event from any main view
void VacProcessMobileHistoryRequest()
{
  time startTime, endTime;
  bool gotRequest = false;

  // Only the very last request shall be processed
  while(LhcVacNextMobileHistoryQuery(startTime, endTime) != 0)
  {
    gotRequest = true;
  }
  if(!gotRequest)
  {
    return;
  }
  DebugTN("mobileHistoryRequest():", startTime, endTime);

  string dpName = VAC_MOBILE_LIST_DP;
  if(!dpExists(dpName))
  {
    return;
  }

  dyn_dyn_string mobileDps;
  dyn_dyn_bool activeFlags;
  dyn_time  times1, times2;
  dpGetPeriod(startTime, endTime, 1, dpName + ".DP_Names", mobileDps, times1);
  dpGetPeriod(startTime, endTime, 1, dpName + ".State", activeFlags, times2);

  /*
  DebugTN("DPs:", mobileDps);
  DebugTN("FLAGs", activeFlags);
  DebugTN("times1", times1);
  DebugTN("times2", times2);
  */

  // Remove data AFTER end of time interval
  int listLen = dynlen(times1);
  if(listLen > dynlen(times2))
  {
    lestLen = dynlen(times2);
  }
  for(int n = listLen ; n > 0 ; n--)
  {
    if(times1[n] <= endTime)
    {
      break;
    }
    if(dynlen(mobileDps) >= n)
    {
      dynRemove(mobileDps, n);
    }
    if(dynlen(activeFlags) >= n)
    {
      dynRemove(activeFlags, n);
    }
  }

  // Apply remaining history data
  LhcVacSetMobileEqpHistory(mobileDps, activeFlags);
}

void VacPopupMenuVVR(int mode, int x, int y, string dpName)
{
  dyn_string menuString = makeDynString(
    "PUSH_BUTTON, Open, 1," + (mode == DIALOG_MODE_ONLINE ? "1" : "0"),
    "PUSH_BUTTON, Close, 2," + (mode == DIALOG_MODE_ONLINE ? "1" : "0"),
    "SEPARATOR",
    "PUSH_BUTTON,Details,3,1");
  int answer;
  popupMenuXY(menuString, x, y, answer);
  unsigned act = 0;
  bool deviceAction = true;
  switch(answer)
  {
  case 1:
    act = LVA_DEVICE_VVR_OPEN;
    break;
  case 2:
    act = LVA_DEVICE_VVR_CLOSE;
    break;
  case 3:
    act = LVA_OPEN_DETAIL_PANEL;
    deviceAction = false;
    break;
  }
  if(act == 0)
  {
    return;
  }
  dyn_string exceptionInfo;
  if(deviceAction)
  {
    LhcVacDeviceAction(makeDynAnytype(act, mode), makeDynString(), makeDynString(),
      dpName, 0, exceptionInfo);
  }
  else
  {
    LvaDialogAction(makeDynAnytype(act, mode), makeDynString(), dpName,
      0, exceptionInfo);
  }
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

void VacPopupMenuVVR1(int mode, int x, int y, string dpName)
{
  dyn_string menuString = makeDynString(
    "PUSH_BUTTON, Open VVR1, 1," + (mode == DIALOG_MODE_ONLINE ? "1" : "0"),
    "PUSH_BUTTON, Close VVR1, 2," + (mode == DIALOG_MODE_ONLINE ? "1" : "0"),
    "SEPARATOR",
    "PUSH_BUTTON,Details,3,1");
  int answer;
  popupMenuXY(menuString, x, y, answer);
  unsigned act = 0;
  bool deviceAction = true;
  switch(answer)
  {
  case 1:
    act = LVA_DEVICE_VVR1_OPEN;
    break;
  case 2:
    act = LVA_DEVICE_VVR1_CLOSE;
    break;
  case 3:
    act = LVA_OPEN_DETAIL_PANEL;
    deviceAction = false;
    break;
  }
  if(act == 0)
  {
    return;
  }
  dyn_string exceptionInfo;
  if(deviceAction)
  {
    LhcVacDeviceAction(makeDynAnytype(act, mode), makeDynString(), makeDynString(),
      dpName, 0, exceptionInfo);
  }
  else
  {
    LvaDialogAction(makeDynAnytype(act, mode), makeDynString(), dpName,
      0, exceptionInfo);
  }
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

void VacPopupMenuVVR2(int mode, int x, int y, string dpName)
{
  dyn_string menuString = makeDynString(
    "PUSH_BUTTON, Open VVR2, 1," + (mode == DIALOG_MODE_ONLINE ? "1" : "0"),
    "PUSH_BUTTON, Close VVR2, 2," + (mode == DIALOG_MODE_ONLINE ? "1" : "0"),
    "SEPARATOR",
    "PUSH_BUTTON,Details,3,1");
  int answer;
  popupMenuXY(menuString, x, y, answer);
  unsigned act = 0;
  bool deviceAction = true;
  switch(answer)
  {
  case 1:
    act = LVA_DEVICE_VVR2_OPEN;
    break;
  case 2:
    act = LVA_DEVICE_VVR2_CLOSE;
    break;
  case 3:
    act = LVA_OPEN_DETAIL_PANEL;
    deviceAction = false;
    break;
  }
  if(act == 0)
  {
    return;
  }
  dyn_string exceptionInfo;
  if(deviceAction)
  {
    LhcVacDeviceAction(makeDynAnytype(act, mode), makeDynString(), makeDynString(),
      dpName, 0, exceptionInfo);
  }
  else
  {
    LvaDialogAction(makeDynAnytype(act, mode), makeDynString(), dpName,
      0, exceptionInfo);
  }
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}


void VacPopupMenuVG1(int mode, int x, int y, string dpName)
{
  dyn_string menuString = makeDynString(
    "PUSH_BUTTON, Switch On VG1, 1," + (mode == DIALOG_MODE_ONLINE ? "1" : "0"),
    "PUSH_BUTTON, Switch Off VG1, 2," + (mode == DIALOG_MODE_ONLINE ? "1" : "0"),
    "SEPARATOR",
    "PUSH_BUTTON,Details,3,1");
  int answer;
  popupMenuXY(menuString, x, y, answer);
  unsigned act = 0;
  bool deviceAction = true;
  switch(answer)
  {
  case 1:
    act = LVA_DEVICE_VG1_ON;
    break;
  case 2:
    act = LVA_DEVICE_VG1_OFF;
    break;
  case 3:
    act = LVA_OPEN_DETAIL_PANEL;
    deviceAction = false;
    break;
  }
  if(act == 0)
  {
    return;
  }
  dyn_string exceptionInfo;
  if(deviceAction)
  {
    LhcVacDeviceAction(makeDynAnytype(act, mode), makeDynString(), makeDynString(),
      dpName, 0, exceptionInfo);
  }
  else
  {
    LvaDialogAction(makeDynAnytype(act, mode), makeDynString(), dpName,
      0, exceptionInfo);
  }
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

void VacPopupMenuVG2(int mode, int x, int y, string dpName)
{
  dyn_string menuString = makeDynString(
    "PUSH_BUTTON,Details,1,1");
  int answer;
  popupMenuXY(menuString, x, y, answer);
  unsigned act = 0;
  bool deviceAction = true;
  switch(answer)
  {
  case 1:
    act = LVA_OPEN_DETAIL_PANEL;
    deviceAction = false;
    break;
  }
  if(act == 0)
  {
    return;
  }
  dyn_string exceptionInfo;
  if(deviceAction)
  {
    LhcVacDeviceAction(makeDynAnytype(act, mode), makeDynString(), makeDynString(),
      dpName, 0, exceptionInfo);
  }
  else
  {
    LvaDialogAction(makeDynAnytype(act, mode), makeDynString(), dpName,
      0, exceptionInfo);
  }
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

void SetPollingPeriod(int newPeriod)
{
  // DebugTN("SetPollingPeriod(" + newPeriod + ")");
  if(newPeriod < pollPeriod)
  {
    if(pollThreadId > 0)
    {
      if(pollPending)
      {
        stopThread(pollThreadId);
        pollThreadId = 0;
      }
    }
  }
  pollPeriod = newPeriod;
  if(pollThreadId == 0)
  {
    if(dynlen(dpePollList) > 0)
    {
      pollThreadId = startThread("ExecuteDpePolling");
    }
  }
}

void ExecuteDpePolling()
{
  int pollIndex;
  while(true)
  {
    // Execute polling cycle
    pollPending = false;
    synchronized(dpePollList)
    {
      pollIndex = dynlen(dpePollList);
    }
    if(pollIndex == 0)
    {
      // DebugTN("Poll list is empty, stop polling");
      pollThreadId = 0;
      return;
    }
    while(pollIndex > 0)
    {
      string dpe;
      synchronized(dpePollList)
      {
        if(pollIndex > dynlen(dpePollList))
        {
          pollIndex = dynlen(dpePollList);
        }
        if(pollIndex > 0)
        {
          dpe = dpePollList[pollIndex--];
        }
        else
        {
          dpe = "";
        }
      }
      if(dpe == "")
      {
        break;
      }
      {
        anytype value;
        time timeStamp;
        dpGet(dpe, value, dpe + ":_original.._stime", timeStamp);
        // DebugTN(pollIndex, dpe, value);
        LhcVacNewOnlineValue(dpe, value, timeStamp);
      }
    }
/*
DebugTN("Cycle finish, total " + dynlen(dpePollList) + ", period " + pollPeriod);
if(dynlen(dpePollList) < 100)
{
  DebugTN(dpePollList);
}
*/
    // Polling cycle is finished - wait
    pollPending = true;
    int waitingTime = 0;
    while(waitingTime < pollPeriod)
    {
      delay(1);
      waitingTime++;
    }
  }
}

void ProcessPollingRequest()
{
  // Process all pending requests
  bool listGrow = false;
  string dpe;
  bool connect;
  LhcVacNextDpPolling(dpe, connect);
  while(dpe != "")
  {
    // DebugTN(dpe, connect);
    if(connect)
    {
      synchronized(dpePollList)
      {
        if(dynContains(dpePollList, dpe) <= 0)
        {
          dynAppend(dpePollList, dpe);
          listGrow = true;
        }
      }
    }
    else
    {
      synchronized(dpePollList)
      {
        int idx = dynContains(dpePollList, dpe);
        if(idx > 0)
        {
          dynRemove(dpePollList, idx);
        }
      }
    }
    LhcVacNextDpPolling(dpe, connect);
  }

  // Decide what to do with polling thread
  if(listGrow)
  {
    if(pollThreadId > 0)
    {
      stopThread(pollThreadId);
      pollThreadId = 0;
    }
  }

  if(pollThreadId == 0)
  {
    if(dynlen(dpePollList) > 0)
    {
      pollThreadId = startThread("ExecuteDpePolling");
    }
  }
}
// Write header of Excel file in XML format
//	WARNING: Styles section remains open, must be closed after
//	calling this function
int VacWriteExcelXmlHeader(file pFile)
{
	if(fprintf(pFile, "<?xml version=\"1.0\"?>\n" +
		"<?mso-application progid=\"Excel.Sheet\"?>\n" +
		"<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\n" +
		" xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n" +
		" xmlns:x=\"urn:schemas-microsoft-com:office:excel\"\n" +
		" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"\n" +
		" xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n" +
		" <DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">\n" +
		"  <Author>VacPVSS</Author>\n" +
		"  <LastAuthor>VacPVSS</LastAuthor>\n") < 0)
	{
		return -1;
	}
	time now = getCurrentTime();
	if(fprintf(pFile, "  <Created>%04d-%02d-%02dT%02d:%02d:%02dZ</Created>\n",
		year(now), month(now), day(now), hour(now), minute(now), second(now)) < 0)
	{
		return -1;
	}
	if(fprintf(pFile, "  <Company>CERN</Company>\n" +
		"  <Version>11.6568</Version>\n" +
		" </DocumentProperties>\n" +
		" <ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">\n" +
		"  <WindowHeight>10000</WindowHeight>\n" +
		"  <WindowWidth>10000</WindowWidth>\n" +
		"  <WindowTopX>105</WindowTopX>\n" +
		"  <WindowTopY>105</WindowTopY>\n" +
		"  <ProtectStructure>False</ProtectStructure>\n" +
		"  <ProtectWindows>False</ProtectWindows>\n" +
		" </ExcelWorkbook>\n" +
		" <Styles>\n" +
		"  <Style ss:ID=\"Default\" ss:Name=\"Normal\">\n" +
		"   <Alignment ss:Vertical=\"Bottom\"/>\n" +
		"   <Borders/>\n" +
		"   <Font/>\n" +
		"   <Interior/>\n" +
		"   <NumberFormat/>\n" +
		"   <Protection/>\n" +
		"  </Style>\n") < 0)
	{
		return -1;
	}
	return 0;
}
// Write footer of Excel file in XML format
//	WARNING: This function closes Table section
int VacWriteExcelXmlFooter(file pFile)
{
	if(fprintf(pFile, "  </Table>\n" +
		"  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">\n" +
		"   <PageSetup>\n" +
		"    <PageMargins x:Bottom=\"0.75\" x:Left=\"0.75\"\n" +
		"     x:Right=\"0.75\" x:Top=\"0.75\"/>\n" +
		"   </PageSetup>\n" +
		"   <Print>\n" +
		"    <ValidPrinterInfo/>\n" +
		"    <PaperSizeIndex>1</PaperSizeIndex>\n" +
		"    <HorizontalResolution>600</HorizontalResolution>\n" +
		"    <VerticalResolution>600</VerticalResolution>\n" +
		"   </Print>\n" +
		"   <Selected/>\n" +
		"   <Panes>\n" +
		"    <Pane>\n" +
		"     <Number>3</Number>\n" +
		"     <ActiveRow>1</ActiveRow>\n" +
		"     <ActiveCol>1</ActiveCol>\n" +
		"    </Pane>\n" +
		"   </Panes>\n" +
		"   <ProtectObjects>False</ProtectObjects>\n" +
		"   <ProtectScenarios>False</ProtectScenarios>\n" +
		"  </WorksheetOptions>\n" +
		" </Worksheet>\n" +
		"</Workbook>\n") < 0)
	{
		return -1;
	}
	return 0;
}

// Print content of Table control to file
string VacPrintTableToFile(string table, string title, bool showRowHeaders = false)
{
  // Open output file
  string fileName = tmpnam();
  file fileToPrint = fopen (fileName, "w+");
  int err = ferror(fileToPrint); // export error
  if(err!=0)
  {
    dyn_string exceptionInfo;
    fwException_raise(exceptionInfo,
	"ERROR", "VacPrintTableToFile(): failed to open tmp file " + fileName +
         ", error " + err, "");
    fwExceptionHandling_display(exceptionInfo);
    return "";
  }

  // Print title
//  fprintf(fileToPrint, "<qt>\n");
  fprintf(fileToPrint, "<html>\n");
  fprintf(fileToPrint, "<h2>%s</h2>\n", title);

  // Print table column headers
  int nColumns, nRows;
  getValue(table, "columnCount", nColumns);
  getValue(table, "lineCount", nRows);
  dyn_string colNames;
  dyn_bool colVisible;

  //fprintf(fileToPrint, "<table border=\"1\" bordercolor=\"#808080\"> \n  <tr bgcolor = \"#e0e0e0\">\n");
  fprintf(fileToPrint, "<table>\n  <tr bgcolor = \"#e0e0e0\">\n");
  if(showRowHeaders)
  {
    fprintf(fileToPrint, "  <th></th>\n");
  }
  for(int col = 0 ; col < nColumns ; col++)
  {
    // Get column name
    string name;
    getValue(table, "columnToName", col, name);
    dynAppend(colNames, name);

    // Get column visibility
    bool visible;
    getValue(table, "columnVisibility", col, visible);
    dynAppend(colVisible, visible);
    if(!visible)
    {
      continue;
    }

    // Get column header and write to output file
    name = "";
    getValue(table, "columnHeader", col, name);
    fprintf(fileToPrint, "  <th>%s</th>\n", name);
  }
  fprintf(fileToPrint, "  </tr>\n");

  // Print table content together with back colors
  for(int row = 0 ; row < nRows ; row++)
  {
    fprintf(fileToPrint, "  <tr>\n");
    if(showRowHeaders)
    {
      string header;
      getValue(table, "rowHeader", row, header);
      fprintf(fileToPrint, "   <td> <nobr>%s</nobr> </td>\n", header);
    }

    for(int col = 0 ; col < nColumns ; col++)
    {
      if(!colVisible[col+1])
      {
        continue;
      }
      string name, color;
      getValue(table, "cellValueRC", row, colNames[col+1], name);
      getValue(table, "cellBackColRC", row, colNames[col+1], color);
      string colorForPrint = ConvertColorStringForPrint(color);
      fprintf(fileToPrint, "   <td bgcolor = \"%s\"> <nobr>%s</nobr> </td>\n",
        colorForPrint, name);
    }
    fprintf(fileToPrint, "  </tr>\n");
  }

  // Finish table and page
  fprintf(fileToPrint, " </table> \n");
//  fprintf(fileToPrint, " </qt>  \n"); 
  fprintf(fileToPrint, " </html>  \n"); 
  fprintf(fileToPrint, "<< Page Break >>\n");
  fclose(fileToPrint); // close file

  return fileName;
}

string ConvertColorStringForPrint(string cs)
{
  int r, g, b;
  if(sscanf(cs, "[%d,%d,%d]", r, g, b) != 3)
  {
    if(sscanf(cs, "{%d,%d,%d}", r, g, b) != 3)
    {
      return "#FFFFFF";
    }
  }
  else
  {
    r *= 2.55;
    g *= 2.55;
    b *= 2.55;
  }

  string result;
  sprintf(result, "#%02X%02X%02X", r, g, b); 
  return result;
}

// Check if current user has at least minimum privilege, if not - show message

bool VacUserHasMinimumPrivilege()
{
  string userName = LhcVacGetUserName();
  if(userName == "")
  {
    return false;
  }
  dyn_string domains, longDomains, exceptionInfo;
  fwAccessControl_getAllDomains(domains, longDomains, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("VacUserHasMinimumPrivilege(): fwAccessControl_getAllDomains() failed:", exceptionInfo);
    return false;
  }
  string minPrivilege = glAllPrivileges[2];
  for(int n = dynlen(domains); n > 0; n--)
  {
    if((domains[n] + ":") == getSystemName())
    {
      continue;
    }
    if(domains[n] == "SYSTEM")
    {
      continue;
    }
    bool granted;
    fwAccessControl_checkUserPrivilege(userName, domains[n], minPrivilege, granted, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("VacUserHasMinimumPrivilege(): fwAccessControl_checkUserPrivilege() failed:", exceptionInfo);
      return false;
    }
    if(granted)
    {
      return true;
    }
  }
  dyn_float dfReturn;
  dyn_string dsReturn;
  if(userName == "")
  {
    userName = "NO USER";
  }
  ChildPanelOnCentralModalReturn("vision/dialogs/Privilege.pnl", "Missing Privilege",
    makeDynString("$1:" + "   Sorry, " + userName + " is NOT allowed to do this:\n at least privilege '" +
    minPrivilege + "' is required."), dfReturn, dsReturn);
  return false;
}

/*
** Temporary: check of DPE is DriverOK in order to bypass it's functionality
*/
bool VacIsDriverOkDPE(string dpeName)
{
  return false;
  dyn_string splitted = strsplit(dpeName, ".");
  if(dynlen(splitted) > 0)
  {
    if(splitted[dynlen(splitted)] == "DriverOK")
    {
      return true;
    }
  }
  return false;
}

void VacOpenSynopticForEqp(const string dpName, int mode, dyn_string &exceptionInfo)
{
  unsigned act = LVA_OPEN_SYNOPTIC;
  int eqpVac;
  string eqpSector1, eqpSector2, eqpMainPart;
  bool eqpIsBorder;
  LhcVacDeviceVacLocation(dpName, eqpVac, eqpSector1, eqpSector2, eqpMainPart, eqpIsBorder, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  //DebugTN("VacOpenSynopticForEqp(" + dpName + "): eqpVac " + eqpVac);
 
  // Some CRYO thermometers have vacType combining both CRYO and BEAM vacuums, for
  // such devices display both CRYO and BEAM synoptics
  if((eqpVac & (LVA_VACUUM_CRYO | LVA_VACUUM_BLUE_BEAM)) == (LVA_VACUUM_CRYO | LVA_VACUUM_BLUE_BEAM))
  {
    eqpVac = LVA_VACUUM_QRL | LVA_VACUUM_CRYO | LVA_VACUUM_BLUE_BEAM | LVA_VACUUM_RED_BEAM;
  }
  else if((eqpVac & (LVA_VACUUM_BLUE_BEAM | LVA_VACUUM_RED_BEAM | LVA_VACUUM_CROSS_BEAM | LVA_VACUUM_BEAMS_COMMON)) != 0)
  {
    eqpVac = LVA_VACUUM_BLUE_BEAM | LVA_VACUUM_RED_BEAM;
  }
  else if((eqpVac & (LVA_VACUUM_QRL | LVA_VACUUM_CRYO)) != 0)
  {
    eqpVac = LVA_VACUUM_QRL | LVA_VACUUM_CRYO;
  }
  else
  {
    eqpVac = LVA_VACUUM_ANY_BEAM;
  }
  dyn_string sectorList, mpList;
  LhcVacGetAllMainParts(mpList);
  string mpName = "";
  for(int mpIdx = dynlen(mpList) ; mpIdx > 0 ; mpIdx--)
  {
    dyn_string sectList;
    dynClear(sectList);
    LhcVacGetSectorsOfMainPart(mpList[mpIdx], sectList);
    if(dynContains(sectList, eqpSector1) > 0)
    {
      dynAppend(sectorList, sectList[1]);
      dynAppend(sectorList, sectList[dynlen(sectList)]);
      mpName = mpList[mpIdx];
      break;
    }
  }

  // Special case for LHC: if TD62.DR or TD68.DB is selected - better select LSSV6
  // Similarly: TI2 -> LSSV2, TI8 -> LSSV8
  string mpReplace = "";
  if((mpName == "TD62.DR") || (mpName == "TD68.DB"))
  {
    mpReplace = "LSSV6";
  }
  else if(mpName == "TI2")
  {
    mpReplace = "LSSV2";
  }
  else if(mpName == "TI8")
  {
    mpReplace = "LSSV8";
  }
  if(mpReplace != "")
  {
    dyn_string sectList;
    LhcVacGetSectorsOfMainPart(mpReplace, sectList);
    if(dynlen(sectList) > 0)
    {
      dynClear(sectorList);
      dynAppend(sectorList, sectList[1]);
      dynAppend(sectorList, sectList[dynlen(sectList)]);
    }
    else
    {
      DebugTN("VacOpenSynopticForEqp(" + dpName + "): not found sectors for replace MP " + mpReplace);
    }
  }
  if(dynlen(sectorList) == 0)
  {
    DebugTN("VacOpenSynopticForEqp(" + dpName + "): sectors not found");
    return;
  }
  LvaDialogAction(makeDynAnytype(act, mode), sectorList, dpName, eqpVac, exceptionInfo);
  if(dynlen(exceptionInfo) == 0)
  {
    LhcVacMakeDeviceVisible(dpName);
  }
}
