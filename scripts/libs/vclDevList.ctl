
/*
  Support for device list, introduced for piquet device list (VTL #2465)
*/

private const string PIQUET_DEV_LIST_CRIT_DP_TYPE = "_VacPiquetDevListCriteria";
private const string PIQUET_DEV_LIST_RESULT_DP_TYPE = "_VacPiquetDevListResult";
private const int MAX_PIQUET_DEV_LIST_RESULTS = 3;

// Constants - indices in dyn_anytype built from reference data
const int PDL_PLC_ALARM = 1;
const int PDL_STATE_DPE1_VALUE = 2;
const int PDL_STATE_DPE2_VALUE = 3;
const int PDL_VALUE_DPE_VALUE = 4;
const int PDL_STATE_NAME = 5;
const int PDL_STATE_COLOR = 6;
const int PDL_COMMENT = 7;

string LhcVacGetPiquetDevListCritDp(dyn_string &exceptionInfo)
{
  dynClear(exceptionInfo);
  string dpName = PIQUET_DEV_LIST_CRIT_DP_TYPE + "_Main";
  if(!dpExists(dpName))
  {
    dpCreate(dpName, PIQUET_DEV_LIST_CRIT_DP_TYPE);
    dyn_errClass err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListCritDp(): failed to create DP " + dpName +
                        ": " + err, "");
      return "";
    }
    bool success = false;
    for(int waitCount = 0 ; waitCount < 20 ; waitCount++)
    {
      if(dpExists(dpName))
      {
        success = true;
        break;
      }
      delay(0, 50);
    }
    if(!success)
    {
      fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListCritDp(): DP " + dpName +
                        " is not created", "");
      return "";
    }
  }
  return dpName;
}

string LhcVacGetPiquetDevListResultDpType()
{
  return PIQUET_DEV_LIST_RESULT_DP_TYPE;
}

// Decide which DP shall be used to save new 'reference', takes the one with
// the latest date of data, or creates new one.
// Checnges after demo for Fabien: at most 1 reference per day, so if there
// is already reference for today - take that one

string LhcVacGetPiquetDevListResultDp(dyn_string &exceptionInfo)
{
  time now = getCurrentTime();
  dynClear(exceptionInfo);
  dyn_string allDpNames = dpNames("*", PIQUET_DEV_LIST_RESULT_DP_TYPE);
  string oldDpName = "";
  time oldTime = getCurrentTime() + 365 * 24 * 60 * 60;
  for(int idx = dynlen(allDpNames) ; idx > 0 ; idx--)
  {
    time valueTime;
    dyn_string valueDps;
    string dpName = dpSubStr(allDpNames[idx], DPSUB_DP);
    dpGet(dpName + ".DpNames", valueDps,
          dpName + ".DpNames:_online.._stime", valueTime);
    if((year(valueTime) == year(now)) && (yearDay(valueTime) == yearDay(now)))
    {
      return dpName;  // Reference for today, shall be reused
    }
    if(dynlen(valueDps) == 0)
    {
      return dpName;  // Empty DP, without result - can be used
    }
    if(valueTime < oldTime)
    {
      oldTime = valueTime;
      oldDpName = dpName;
    }
  }
  if(dynlen(allDpNames) >= MAX_PIQUET_DEV_LIST_RESULTS)
  {
    return oldDpName;
  }

  // New DP shall be created
  string dpName = "";
  int idx = 0;
  while(true)
  {
    sprintf(dpName, "%s_%06d", PIQUET_DEV_LIST_RESULT_DP_TYPE, ++idx);
    if(!dpExists(dpName))
    {
      break;
    }
  }
  dpCreate(dpName, PIQUET_DEV_LIST_RESULT_DP_TYPE);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListResultDp(): failed to create DP " + dpName +
                      ": " + err, "");
    return "";
  }
  bool success = false;
  for(int waitCount = 0 ; waitCount < 20 ; waitCount++)
  {
    if(dpExists(dpName))
    {
      success = true;
      break;
    }
    delay(0, 50);
  }
  if(!success)
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListResultDp(): DP " + dpName +
                      " is not created", "");
    return "";
  }
  return dpName;
}

int LhcVacEqpTypeForDevList(string dpName, string &eqpType)
{
  int order = 0;
  string dpType;
  dyn_string exceptionInfo;
  LhcVacDpType(dpName, dpType, exceptionInfo);
  switch(dpType)
  {
  case "VLV":
  case "VLV_C0":
  case "VV_AO":
  case "VV_PULSED":
  case "VV_STD_6E01":
  case "VV_STD_IO":
  case "VVF_S":
  case "VVS_LHC":
  case "VVS_PS":
  case "VVS_S":
  case "VVS_SV":
    eqpType = "Valves (VVS)";
    order = 1;
    break;
  case "VP_STDIO":
  case "VPG_6A01":
  case "VPG_6E01":
  case "VPG_BGI":
  case "VPG_BP":
  case "VPG_EA":
  case "VPG_EA_C0":
  case "VPG_EA_NO_VLV_CTL":
  case "VPG_GIS":
  case "VPG_SA":
    eqpType = "Pumping Groups (VPG)";
    order = 2;
    break;
  case "VPI":
  case "VRPI_L":
  case "VRPI_W":
    eqpType = "Ion pumps (VPI)";
    order = 3;
    break;
  case "VG_DP":
  case "VG_STD":
  case "VGF_C":
  case "VGF_C0":
  case "VGFH_C":
  case "VGI":
  case "VGM_C":
  case "VGM_C0":
  case "VGP_C":
  case "VGP_C0":
  case "VGP_T":
  case "VGR_C":
  case "VGR_C0":
  case "VGR_T":
  case "VGR_T0":
    eqpType = "Gauges (VGP/R/M)";
    order = 4;
    break;
  default:
    eqpType = dpType;
    break;
  }
  return order;
}

bool LhcVacGetPiquetDevListRefData(string dpName, mapping &result, dyn_string &exceptionInfo)
{
  mappingClear(result);
  dynClear(exceptionInfo);
  time refTime;
  dyn_bool plcAlarms;
  dyn_uint stateDpe1Values, stateDpe2Values;
  dyn_string refDpNames, values, stateNames, stateColors, comments;
  dpGet(dpName + ".DpNames:_original.._stime", refTime,
        dpName + ".DpNames", refDpNames,
        dpName + ".PlcAlarms", plcAlarms,
        dpName + ".StateDpe1Values", stateDpe1Values,
        dpName + ".StateDpe2Values", stateDpe2Values,
        dpName + ".Values", values,
        dpName + ".StateNames", stateNames,
        dpName + ".StateColors", stateColors,
        dpName + ".Comments", comments);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListRefData(): dpGet() failed: " + err, "");
    return false;
  }
  int nRefDps = dynlen(refDpNames);

  // Check sizes of resulting lists - all must have the same size
  if(nRefDps != dynlen(plcAlarms))
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListRefData(): broken reference: " + nRefDps +
                      " DP(s), " + dynlen(plcAlarms) + " PLC alarm(s)", "");
    return false;
  }
  if(nRefDps != dynlen(stateDpe1Values))
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListRefData(): broken reference: " + nRefDps +
                      " DP(s), " + dynlen(stateDpe1Values) + " state DPE1 value(s)", "");
    return false;
  }
  if(nRefDps != dynlen(stateDpe2Values))
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListRefData(): broken reference: " + nRefDps +
                      " DP(s), " + dynlen(stateDpe2Values) + " state DPE2 value(s)", "");
    return false;
  }
  if(nRefDps != dynlen(values))
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListRefData(): broken reference: " + nRefDps +
                      " DP(s), " + dynlen(values) + " analog value(s)", "");
    return false;
  }
  if(nRefDps != dynlen(stateNames))
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListRefData(): broken reference: " + nRefDps +
                      " DP(s), " + dynlen(stateNames) + " state name(s)", "");
    return false;
  }
  if(nRefDps != dynlen(stateColors))
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListRefData(): broken reference: " + nRefDps +
                      " DP(s), " + dynlen(stateColors) + " state color(s)", "");
    return false;
  }
  if(nRefDps != dynlen(comments))
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetPiquetDevListRefData(): broken reference: " + nRefDps +
                      " DP(s), " + dynlen(comments) + " comment(s)", "");
    return false;
  }
  
  // Build result
  result["time"] = refTime;
  result["dpNames"] = refDpNames;
  dyn_dyn_anytype refData;
  for(int dpIdx = 1 ; dpIdx <= nRefDps ; dpIdx++)
  {
    dyn_anytype dpData;
    dynClear(dpData);
    dynAppend(dpData, (bool)plcAlarms[dpIdx]);
    dynAppend(dpData, (unsigned)stateDpe1Values[dpIdx]);
    dynAppend(dpData, (unsigned)stateDpe2Values[dpIdx]);
    dynAppend(dpData, (string)values[dpIdx]);
    dynAppend(dpData, (string)stateNames[dpIdx]);
    dynAppend(dpData, (string)stateColors[dpIdx]);
    dynAppend(dpData, (string)comments[dpIdx]);
    dynAppend(refData, dpData);
  }
  result["data"] = refData;
  return true;
}

bool LhcVacMatchesDeltaCondition(string dpName, mapping allRefData, dyn_string stateDpes,
                                 dyn_uint stateDpeValues, string &deltaMsg)
{
  if(!mappingHasKey(allRefData, "dpNames"))
  {
    return true;  // Empty reference = not DELTA mode
  }
  int refIdx = dynContains(allRefData["dpNames"], dpName);
  if(refIdx <= 0)
  {
    return true;  // DP was not in reference result; or mode is not delta
  }
  dyn_anytype refData = allRefData["data"][refIdx];
  if(dynlen(stateDpeValues) > 0)
  {
    if(refData[PDL_STATE_DPE1_VALUE] != stateDpeValues[1])
    {
      deltaMsg = "State changed";
      return true;
    }
  }
  if(dynlen(stateDpeValues) > 1)
  {
    if(refData[PDL_STATE_DPE2_VALUE] != stateDpeValues[2])
    {
      deltaMsg = "State changed";
      return true;
    }
  }
  
  // DP is in both old and new lists, states are equal - analyze change history
  int nChanges = CountStateChanges(stateDpes, allRefData["time"]);
  // DebugTN("Found " + nChanges + " changes for " + stateDpes);
  if(nChanges > 0)
  {
    deltaMsg = "Same state, but changed " + nChanges + " time" + (nChanges > 1 ? "s" : "");
    return true;
  }
  
  return false;
}

private int CountStateChanges(dyn_string dpeNames, time startTime)
{
  int result = 0;
  time now = getCurrentTime();
  for(int dpeIdx = dynlen(dpeNames) ; dpeIdx > 0 ; dpeIdx--)
  {
    result += CountDpeChanges(dpeNames[dpeIdx], startTime, now);
  }
  return result;
}

private int CountDpeChanges(string dpeName, time startTime, time now)
{
  //DebugTN("CountDpeChanges(" + dpeName + ") range:", startTime, now);
  int result = 0;
  while(startTime < now)
  {
    dyn_time times;
    dynClear(times);
    dyn_anytype values;
    time endTime = startTime + 8 * 60 * 60;  // Query in steps of 8 hours
    if(endTime > now)
    {
      endTime = now;
    }
    //DebugTN("Query for " + dpeName, startTime, endTime, now);
    dpGetPeriod(startTime, endTime, 0, dpeName, values, times);
    dyn_errClass err = getLastError();
    if(dynlen(err) > 0)
    {
      DebugTN("dpGetPeriod(" + dpeName + ") failed in range:", startTime, endTime);
    }
    else
    {
      result += dynlen(times);
    }
    startTime = endTime;
  }
  return result;
}
