#ifndef	SYNVIEWXML_H
#define	SYNVIEWXML_H

// Synoptic view for displaying manually built PVSS panel in XML format

#include "SynView.h"

#include "XmlSynoptic.h"

class SynViewXml : public SynView
{
	Q_OBJECT

public:
	SynViewXml(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	~SynViewXml();

	static SynViewXml *create(QWidget *parent, const QString &fileName, DataEnum::DataMode mode);

	// Dummy implementations of SynView methods 
	virtual void analyzeTypes(unsigned & /* allVacTypeMask */, VacEqpTypeMask & /* allCryoEqpMask */,
		VacEqpTypeMask & /* allBeamEqpMask */) {}
	virtual int applyMask(VacEqpTypeMask & /* mask */) { return 0; }

	// The following method shall be implemented by SectView - to be used instead of applyMask()
//	virtual void buildSectorView(void) { Q_ASSERT(0); }

	virtual void setViewStart(int /* viewStart */, int /* viewWidth */, bool /* dragging */) {}

	// Implement abstract method of SynopticWidget
	virtual bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	virtual void dump(const char * /* fileName */) {}

protected:
	XmlSynoptic	*pSynoptic;

	QString		errMsg;

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// SYNVIEWXML_H
