//	Implementation of VacIconVPG_6A01_1VVR_NO_VG class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPG_6A01_1VVR_NO_VG.h"
#include "VacIconVPG_6A01.h"

#include "EqpProcess.h"

#include "DataPool.h"

#include <qpainter.h>
#include <QMouseEvent>


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
VacIconVPG_6A01_1VVR_NO_VG::VacIconVPG_6A01_1VVR_NO_VG(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	pIconVPG = pIconVVR = NULL;
	setFixedSize(27, 47);
	setAreaGeometry();
}

VacIconVPG_6A01_1VVR_NO_VG::~VacIconVPG_6A01_1VVR_NO_VG()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for symbol areas rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_NO_VG::setAreaGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		vvr.setRect(0, 2, 27, 21);
		pump.setRect(0, 25, 21, 21);
		break;
	case VacIconContainer::Down:
		pump.setRect(0, 0, 21, 21);
		vvr.setRect(0, 23, 27, 21);
		break;
	case VacIconContainer::Left:
		pump.setRect(0, 6, 21, 21);
		vvr.setRect(23, 1, 21, 27);
		break;
	case VacIconContainer::Right:
		vvr.setRect(2, 1, 21, 27);
		pump.setRect(25, 6, 21, 21);
		break;
	}
	if(pIconVVR)
	{
		pIconVVR->move(vvr.left(), vvr.top());
	}
	if(pIconVPG)
	{
		pIconVPG->move(pump.left(), pump.top());
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPG_6A01_1VVR_NO_VG::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		return VacIconGeometry(27, 46, pump.center().x(), 47, -1);
		break;
	default:
		break;
	}
	return VacIconGeometry(46, 27, 27, pump.center().y(), -1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_NO_VG::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(27);
		size.setHeight(46);
		break;
	default:
		size.setWidth(46);
		size.setHeight(27);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreaGeometry();
	if(pIconVPG)
	{
		pIconVPG->setDirection(newDirection);
	}
	if(pIconVVR)
	{
		pIconVVR->setDirection(calculateVvrDirection());
	}
}

VacIconContainer::Direction VacIconVPG_6A01_1VVR_NO_VG::calculateVvrDirection(void)
{
	VacIconContainer::Direction result = direction;
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		result = VacIconContainer::Left;
		break;
	default:
		result = VacIconContainer::Up;
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_NO_VG::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pIconVPG)
	{
		delete pIconVPG;
		pIconVPG = NULL;
	}
	if(pIconVVR)
	{
		delete pIconVVR;
		pIconVVR = NULL;
	}
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpProcess"), "VacIconVPG_6A01_1VVR_NO_VG::setEqp", pEqp->getDpName());
		this->pEqp = (EqpProcess *)pEqp;
		pIconVPG = new VacIconVPG_6A01(this);
		pIconVPG->setEqp(pEqp);
		pIconVPG->move(pump.left(), pump.top());
		pIconVPG->setMode(mode);
		pIconVPG->setPipeColor(pipeColor);
		pIconVPG->setDirection(direction);
		if(connected)
		{
			pIconVPG->connect();
		}

		QString attrName = pEqp->getAttrValue("VVR1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVVR = VacIcon::getIcon(pChild, this);
			}
		}
		if(!pIconVVR)
		{
			attrName = pEqp->getAttrValue("VVR2");
			if(!attrName.isEmpty())
			{
				Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
				if(pChild)
				{
					pIconVVR = VacIcon::getIcon(pChild, this);
				}
			}
		}
		if(pIconVVR)
		{
			pIconVVR->move(vvr.left(), vvr.top());
			pIconVVR->setMode(mode);
			pIconVVR->setPipeColor(pipeColor);
			pIconVVR->setDirection(calculateVvrDirection());
			if(connected)
			{
				pIconVVR->connect();
			}
		}
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_NO_VG::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	if(pIconVPG)
	{
		pIconVPG->setMode(newMode);
	}
	if(pIconVVR)
	{
		pIconVVR->setMode(newMode);
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set color used for pipe lines drawing
**
**	ARGUMENTS
**		newColor	- Color used for pipe drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_NO_VG::setPipeColor(const QColor &newColor)
{
	if(pIconVPG)
	{
		pIconVPG->setPipeColor(newColor);
	}
	if(pIconVVR)
	{
		pIconVVR->setPipeColor(newColor);
	}
	VacIcon::setPipeColor(newColor);
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6A01_1VVR_NO_VG::connect(void)
{
	if(pIconVPG)
	{
		pIconVPG->connect();
	}
	if(pIconVVR)
	{
		pIconVVR->connect();
	}
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6A01_1VVR_NO_VG::disconnect(void)
{
	if(pIconVPG)
	{
		pIconVPG->disconnect();
	}
	if(pIconVVR)
	{
		pIconVVR->disconnect();
	}
	return false;
}

/*
**	FUNCTION
**		Override VacIcon's method in order to have different menus for different
**		parts of icon
**
**	ARGUMENTS
**		pEvent	- Pointer to mouse event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_1VVR_NO_VG::mousePressEvent(QMouseEvent * /* pEvent */)
{
	// Do nothing
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPG_6A01_1VVR_NO_VG::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
//	painter.eraseRect(0, 0, width(), height());

	switch(direction)
	{
	case VacIconContainer::Up:
		// Line from VVR to outside
		painter.fillRect(pump.center().x() - 1, 0, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, vvr.bottom() + 1, 3, pump.top() - vvr.bottom(), pipeColor);
		break;
	case VacIconContainer::Down:
		// Line from VVR to outside
		painter.fillRect(pump.center().x() - 1, vvr.bottom(), 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, pump.bottom(), 3, vvr.top() - pump.bottom(), pipeColor);
		break;
	case VacIconContainer::Left:
		// Line from VVR to outside
		painter.fillRect(vvr.right(), pump.center().y() - 1, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.right(), pump.center().y() - 1, vvr.left() - pump.right(), 3, pipeColor);
		break;
	case VacIconContainer::Right:
		// Line from VVR to outside
		painter.fillRect(0, pump.center().y() - 1, 3, 3, pipeColor);
		// Connect pump to valves
		painter.fillRect(vvr.right(), pump.center().y() - 1, pump.left() - vvr.right(), 3, pipeColor);
		break;
	}

	if(!pEqp)
	{
		painter.setPen(Qt::black);
		painter.drawRect(pump.left(), pump.top(), pump.width() - 1, pump.height() - 1);
		painter.drawRect(vvr.left(), vvr.top(), vvr.width() - 1, vvr.height() - 1);
	}
}
