//	Implementation of SynopticLhcDialog class
/////////////////////////////////////////////////////////////////////////////////

#include "SynopticLhcDialog.h"

#include "SynLhcView.h"

#include "Eqp.h"
#include "VacIcon.h"
#include "EqpMsgCriteria.h"

#include "VacMainView.h"
#include "Sector.h"
#include "DataPool.h"
#include "ResourcePool.h"

#include <qpushbutton.h>
#include <qmenu.h>
#include <qlabel.h>
#include <qcombobox.h>
#include <QScrollArea>
#include <QScrollBar>
#include <qlayout.h>
#include <qdesktopwidget.h>
#include <qapplication.h>

SynopticLhcDialog *SynopticLhcDialog::pLastInstance = NULL;

void SynopticLhcDialog::makeDeviceVisible(Eqp *pEqp)
{
	if(pLastInstance)
	{
		pLastInstance->ensureDeviceVisible(pEqp);
		pLastInstance = NULL;	// Only one call is accepted
	}
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

SynopticLhcDialog::SynopticLhcDialog(unsigned vacTypeMask, Sector *pFirstSector,
	Sector *pLastSector, DataEnum::DataMode mode) :
	QDialog(NULL /* VacMainView::getInstance() */, Qt::Window),
	LhcRingSelection()
{
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle("Synoptic LHC");
	this->vacTypeMask = vacTypeMask;
	this->pFirstSector = pFirstSector;
	this->pLastSector = pLastSector;
	this->mode = mode;
	pView = NULL;

	buildLayout();
	setTitle();
	buildInitialEqpMask();
	buildView(true);
	if(mode == DataEnum::Replay)
	{
		VacMainView::replayDialogOpened(this);
	}
	pLastInstance = this;
}

SynopticLhcDialog::~SynopticLhcDialog()
{
	if(pLastInstance == this)
	{
		pLastInstance = NULL;
	}
	if(mode == DataEnum::Replay)
	{
		VacMainView::replayDialogDeleted(this);
	}
}

/*
**	FUNCTION
**		Build initial equipment type mask
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticLhcDialog::buildInitialEqpMask(void)
{
	cryoEqpMask = allCryoEqpMask;
	beamEqpMask = allBeamEqpMask;
	ResourcePool &pool = ResourcePool::getInstance();
	if(pool.getEqpTypesMask("Synoptic.IsolationVacuumEqpTypes", cryoEqpMask) == ResourcePool::NotFound)
	{
		cryoEqpMask = allCryoEqpMask;
	}
	if(pool.getEqpTypesMask("Synoptic.BeamVacuumEqpTypes", beamEqpMask) == ResourcePool::NotFound)
	{
		beamEqpMask = allBeamEqpMask;
	}

	// Set items checked in menu by default
	const QList<FunctionalType *> &cryoTypeList = allCryoEqpMask.getList();
	FunctionalType *pItem;
	int idx;
	for(idx = 0 ; idx < cryoTypeList.count() ; idx++)
	{
		pItem = cryoTypeList.at(idx);
		for(int typeIdx = 0 ; typeIdx < cryoTypeActions.count() ; typeIdx++)
		{
			QAction *pAction = cryoTypeActions.at(typeIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				pAction->setChecked(cryoEqpMask.contains(pItem->getType()));
				break;
			}
		}
	}
	const QList<FunctionalType *> &beamTypeList = allBeamEqpMask.getList();
	for(idx = 0 ; idx < beamTypeList.count() ; idx++)
	{
		pItem = beamTypeList.at(idx);
		for(int typeIdx = 0 ; typeIdx < beamTypeActions.count() ; typeIdx++)
		{
			QAction *pAction = beamTypeActions.at(typeIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				bool contains = beamEqpMask.contains(pItem->getType());
				pAction->setChecked(contains);
				// Add related items if one is selected
				if(contains)
				{
					switch(pItem->getType())
					{
					case FunctionalType::CRYO_TT:
						beamEqpMask.append(FunctionalType::CRYO_TT_EXT);
						beamEqpMask.append(FunctionalType::CRYO_TT_SUM);
						break;
					case FunctionalType::PROCESS_VPG_6A01:
						beamEqpMask.append(FunctionalType::PROCESS_VPG_6E01);
						break;
					}
				}
				break;
			}
		}
	}
	// Add experimental area to beam vacuum equipment mask
	beamEqpMask.append(FunctionalType::EXP_AREA);
	// L.Kopylov 28.10.2011 Add BGI process
	beamEqpMask.append(FunctionalType::PROCESS_BGI_6B01);
	// L.Kopylov 16.02.2012 Add new VPG process
	cryoEqpMask.append(FunctionalType::PROCESS_VPG_6A01);
	cryoEqpMask.append(FunctionalType::PROCESS_VPG_6E01);
	// L.Kopylov 27.07.2013 Add new VPG process
	beamEqpMask.append(FunctionalType::PROCESS_VPG_6A01);
	beamEqpMask.append(FunctionalType::PROCESS_VPG_6E01);
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticLhcDialog::buildLayout(void)
{
	// 1) Main layout - menu, labels and combobox on top, scrolled view on bottom
	QVBoxLayout *mainBox = new QVBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);

	// 2) Horizontal layout with
	//	- buttons for activating popup menus
	//	- 2 icons for beam intensities
	//	- title label
	//	- ComboBox with main part selection
	QHBoxLayout *topBox = new QHBoxLayout();
	topBox->setSpacing(0);
	mainBox->addLayout(topBox);

	// 2.1 all popup menus
	pFilePb = new QPushButton("&File", this);
	pFilePb->setFlat(true);
	QSize size = pFilePb->fontMetrics().size(Qt::TextSingleLine, "File");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pFilePb->setFixedSize(size);
	buildFileMenu();
	pFilePb->setMenu(pFileMenu);
	topBox->addWidget(pFilePb);

	pViewPb = new QPushButton("&View", this);
	pViewPb->setFlat(true);
	size = pViewPb->fontMetrics().size(Qt::TextSingleLine, "View");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pViewPb->setFixedSize(size);
	buildViewMenu();
	pViewPb->setMenu(pViewMenu);
	topBox->addWidget(pViewPb);

	pHelpPb = new QPushButton("&Help", this);
	pHelpPb->setFlat(true);
	size = pHelpPb->fontMetrics().size(Qt::TextSingleLine, "Help");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pHelpPb->setFixedSize(size);
	buildHelpMenu();
	pHelpPb->setMenu(pHelpMenu);
	topBox->addWidget(pHelpPb);


	// 2.2 icons for beam intensity display
	QList<Eqp *> intList;
	getBeamIntesityEqp(intList);
	for(int idx = 0 ; idx < intList.count() ; idx++)
	{
		Eqp *pEqp = intList.at(idx);
		VacIcon *pIcon = VacIcon::getIcon(pEqp, this);
		if(pIcon)
		{
			pIcon->setEqp(pEqp);
			pIcon->setMode(mode);
			pIcon->connect();
			topBox->addSpacing(5);
			topBox->addWidget(pIcon);
		}
	}

	// 2.3 synoptic title
	pTitleLabel = new QLabel("Synoptic", this);
	topBox->addWidget(pTitleLabel, 10);	// The only resizable widget on top of dialog
	pTitleLabel->setAlignment(Qt::AlignHCenter);

	// 2.4 Combo box for main part selection
	buildMainPartCombo();
	topBox->addWidget(pCombo);

	// 3 Scroll view where synoptic will lie
	pScroll = new QScrollArea(this);
	mainBox->addWidget(pScroll, 10);	// Scroll view will consume all free space
	connect(pScroll->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(viewMove(int)));
}

/*
**	FUNCTION
**		Build 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticLhcDialog::buildFileMenu(void)
{
	pFileMenu = new QMenu(this);
	pFileMenu->addAction("Print...", this, SLOT(filePrint()));
	pFileMenu->addSeparator();
	pFileMenu->addAction("Close", this, SLOT(deleteLater()));
}

/*
**	FUNCTION
**		Build 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticLhcDialog::buildViewMenu(void)
{
	// First build masks containing all possible types for synoptic on
	// corresponding vacuum type
	allCryoEqpMask.append(FunctionalType::VGM);
	allCryoEqpMask.append(FunctionalType::VGR);
	allCryoEqpMask.append(FunctionalType::VGP);
	allCryoEqpMask.append(FunctionalType::VPG);
	allCryoEqpMask.append(FunctionalType::VPGF);
	allCryoEqpMask.append(FunctionalType::PROCESS_VPG_6A01);	// L.Kopylov 27.07.2013
	allCryoEqpMask.append(FunctionalType::VPGM);
	allCryoEqpMask.append(FunctionalType::VV);
	allCryoEqpMask.append(FunctionalType::CRYO_TT);
	allCryoEqpMask.append(FunctionalType::CRYO_TT_EXT);

	allBeamEqpMask.append(FunctionalType::VV);
	allBeamEqpMask.append(FunctionalType::VGM);
	allBeamEqpMask.append(FunctionalType::VGR);
	allBeamEqpMask.append(FunctionalType::VGP);
	allBeamEqpMask.append(FunctionalType::VGI);
	allBeamEqpMask.append(FunctionalType::VGF);
	allBeamEqpMask.append(FunctionalType::VGTR);
	allBeamEqpMask.append(FunctionalType::VPI);
	allBeamEqpMask.append(FunctionalType::VPG);
	allBeamEqpMask.append(FunctionalType::VPGF);
	allBeamEqpMask.append(FunctionalType::PROCESS_VPG_6A01);	// L.Kopylov 27.07.2013
	allBeamEqpMask.append(FunctionalType::VPGM);
	allBeamEqpMask.append(FunctionalType::CRYO_TT);
	allBeamEqpMask.append(FunctionalType::CRYO_TT_EXT);
	allBeamEqpMask.append(FunctionalType::CRYO_TT_SUM);
	allBeamEqpMask.append(FunctionalType::VPT100);
	allBeamEqpMask.append(FunctionalType::VIES);
	allBeamEqpMask.append(FunctionalType::VRJ_TC);
	allBeamEqpMask.append(FunctionalType::VPN);
	allBeamEqpMask.append(FunctionalType::VPS);
	allBeamEqpMask.append(FunctionalType::VRE);

	pViewMenu = new QMenu(this);

	// Vacuum type submenu
	pVacSubMenu = pViewMenu->addMenu("Vacuum");
	QAction *pAction = pVacSubMenu->addAction("QRL");
	pAction->setData(VacType::Qrl);
	pAction->setCheckable(true);
	pAction->setChecked(vacTypeMask & VacType::Qrl);
	vacTypeActions.append(pAction);

	pAction = pVacSubMenu->addAction("CRYO");
	pAction->setData(VacType::Cryo);
	pAction->setCheckable(true);
	pAction->setChecked(vacTypeMask & VacType::Cryo);
	vacTypeActions.append(pAction);

	pAction = pVacSubMenu->addAction("Blue Beam");
	pAction->setData(VacType::BlueBeam);
	pAction->setCheckable(true);
	pAction->setChecked(vacTypeMask & VacType::BlueBeam);
	vacTypeActions.append(pAction);

	pAction = pVacSubMenu->addAction("Red Beam");
	pAction->setData(VacType::RedBeam);
	pAction->setCheckable(true);
	pAction->setChecked(vacTypeMask & VacType::RedBeam);
	vacTypeActions.append(pAction);
	
	// Equipment on isolation vacuum submenu
	pCryoEqpSubMenu = pViewMenu->addMenu("Equipment on isol. vacuum");
	const QList<FunctionalType *> &cryoTypes = allCryoEqpMask.getList();
	FunctionalType *pItem;
	int idx;
	for(idx = 0 ; idx < cryoTypes.count() ; idx++)
	{
		pItem = cryoTypes.at(idx);
		QAction *pAction = pCryoEqpSubMenu->addAction(pItem->getDescription());
		pAction->setCheckable(true);
		pAction->setData(pItem->getType());
		pAction->setChecked(cryoEqpMask.contains(pItem->getType()));
		cryoTypeActions.append(pAction);
	}

	// Equipment on beam vacuum submenu
	pBeamEqpSubMenu = pViewMenu->addMenu("Equipment on beam vacuum");
	const QList<FunctionalType *> &beamTypes = allBeamEqpMask.getList();
	for(idx = 0 ; idx < beamTypes.count() ; idx++)
	{
		pItem = beamTypes.at(idx);

		// L.Kopylov 28.01.2010 Only 1 menu item for all thermometers
		QAction *pAction = NULL;
		switch(pItem->getType())
		{
		case FunctionalType::CRYO_TT:
			pAction = pBeamEqpSubMenu->addAction("CRYO Thermometers");
			break;
		case FunctionalType::CRYO_TT_EXT:
		case FunctionalType::CRYO_TT_SUM:
			break;	// No separate items
		case FunctionalType::PROCESS_VPG_6A01:
			pAction = pBeamEqpSubMenu->addAction("VPG Process");
			break;
		case FunctionalType::PROCESS_VPG_6E01:
			break;	// No separate items
		default:
			pAction = pBeamEqpSubMenu->addAction(pItem->getDescription());
			break;
		}
		if(pAction)
		{
			pAction->setCheckable(true);
			pAction->setData(pItem->getType());
			pAction->setChecked(beamEqpMask.contains(pItem->getType()));
			beamTypeActions.append(pAction);
		}
	}
	pViewMenu->addSeparator();
	// Add Show mobiles not connected
	QAction *pActShowMobs = NULL;
	pActShowMobs = pViewMenu->addAction("Show mobiles not connected", this, SLOT(showMobilesNC()));
	// Add Hide mobiles not connected
	QAction *pActHideMobs = NULL;
	pActHideMobs = pViewMenu->addAction("Hide mobiles not connected", this, SLOT(hideMobilesNC()));

	pViewMenu->addSeparator();
	pViewMenu->addAction("Sector...", this, SLOT(viewSector()));
	pViewMenu->addAction("Pressure Profile...", this, SLOT(viewProfile()));
	connect(pViewMenu, SIGNAL(triggered(QAction *)), this, SLOT(typeMenu(QAction *)));
}

/*
**	FUNCTION
**		Build 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticLhcDialog::buildHelpMenu(void)
{
	pHelpMenu = new QMenu(this);
	pHelpMenu->addAction("User manual...", this, SLOT(help()));
}

/*
**	FUNCTION
**		Build main part combo box, insert main part names which can be selected
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticLhcDialog::buildMainPartCombo(void)
{
	pCombo = new QComboBox(this);
	pCombo->setEditable(false);
	pCombo->clear();
	DataPool &pool = DataPool::getInstance();
	QStringList strList;
	pool.findMainPartNames(strList, true);
	QList<Sector *> &sectors = pool.getSectors();
	foreach(QString mpName, strList)
	{
		MainPart *pMainPart = pool.findMainPartData(mpName.toLatin1());
		unsigned vacType = pMainPart->getVacTypeMask();
		if(!(vacType & vacTypeMask))
		{
			continue;
		}
		bool skip = false;
		for(int idx = 0 ; idx < sectors.count() ; idx++)
		{
			Sector *pSector = sectors.at(idx);
			if(pSector->getSpecSynPanel().isEmpty())
			{
				continue;
			}
			if(pSector->isOuter())
			{
				continue;
			}
			if(pSector->isInMainPart(pMainPart))
			{
				skip = true;
				break;
			}
		}
		if(!skip)
		{
			// if(pool.areSectorsOfMainPartContinuous(pMainPart))
			{
				pCombo->addItem(mpName);
			}
		}
	}
	selectInitialMp();
	connect(pCombo, SIGNAL(activated(const QString &)),
		this, SLOT(comboActivated(const QString &)));
}

/*
**	FUNCTION
**		Set initial selection in main part combo box
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticLhcDialog::selectInitialMp(void)
{
	DataPool &pool = DataPool::getInstance();
	int nMps = pCombo->count();
	int *mps = (int *)calloc(nMps, sizeof(int)), n;
	Sector *pSector = pFirstSector;
	for(n = 0 ; n < nMps ; n++)
	{
		MainPart *pMainPart = pool.findMainPartData(pCombo->itemText(n).toLatin1());
		if(pSector->isInMainPart(pMainPart))
		{
			mps[n]++;
		}
	}
	pSector = pLastSector;
	for(n = 0 ; n < nMps ; n++)
	{
		MainPart *pMainPart = pool.findMainPartData(pCombo->itemText(n).toLatin1());
		if(pSector->isInMainPart(pMainPart))
		{
			mps[n]++;
		}
	}

	int bestIdx = -1, bestWeight = 0;
	for(n = 0 ; n < nMps ; n++)
	{
		if(mps[n] > bestWeight)
		{
			bestWeight = mps[n];
			bestIdx = n;
		}
	}
	free((void *)mps);
	pCombo->setCurrentIndex(bestIdx);
}

/*
**	FUNCTION
**		Set text and color for title label widget
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::setTitle(void)
{
	if(mode == DataEnum::Replay)
	{
		QPalette palette = pTitleLabel->palette();
		palette.setColor(QPalette::Window, Qt::cyan);
		pTitleLabel->setPalette(palette);
		pTitleLabel->setAutoFillBackground(true);
	}
	QString title = "Synoptic, sector";
	if(pFirstSector == pLastSector)
	{
		title += " ";
		title += pFirstSector->getName();
	}
	else
	{
		title += "s ";
		title += pFirstSector->getName();
		title += " ... ";
		title += pLastSector->getName();
	}
	pTitleLabel->setText(title);
}

/*
**	FUNCTION
**		Build synoptic view according to current selection
**
**	ARGUMENTS
**		init	- Flag indicating if method is called from constructor
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::buildView(bool init)
{
	if(pView)
	{
		delete pView;
		pView = NULL;
	}
	// Create instance of synoptic view
	pView = SynLhcView::create(pScroll->viewport(), pFirstSector, pLastSector, mode);

	if(!pView)
	{
		printf("SynopticLhcDialog::buildView(): NULL pView\n");
		fflush(stdout);
		return;
	}

	// Analyze equipment types in view, set menu accordingly
	setTypesMenu();

	// Build content of synoptic view with current selection mask
	pView->applyMask(vacTypeMask, cryoEqpMask, beamEqpMask);

	// Add view to scroller
	pView->show();
	pScroll->setWidget(pView);

	setMyWidth(init);
	pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
	connect(pView, SIGNAL(mobileStateChanged()), this, SLOT(mobileStateChange()));
}

void SynopticLhcDialog::ensureDeviceVisible(Eqp *pEqp)
{
	if(!pView)
	{
		return;
	}
	int position;
	bool visible;
	if(!pView->findDevicePosition(pEqp, position, visible))
	{
		return;	// No such device in view
	}
	//qDebug("SynopticLhcDialog::ensureDeviceVisible(%s): visible %d position %d vacType %d\n", pEqp->getDpName(), (int)visible, position, pEqp->getVacType());
	if(!visible)	// Make it visible - device type menus, vacuum menu already must correspond to device
	{
		int	funcType = pEqp->getFunctionalType();
		// Decision on where to apply mask is made on vacuum type for synoptic, not for device: some CRYO_TT are on both beam and cryo vacuums
		if(vacTypeMask & (VacType::Qrl | VacType::Cryo))
		{
			if(cryoEqpMask.contains(pEqp->getFunctionalType()))
			{
				qDebug("SynopticLhcDialog::ensureDeviceVisible(%s): must be visible on CRYO, but it is not\n", pEqp->getDpName());
				return;
			}
			//qDebug("SynopticLhcDialog::ensureDeviceVisible(%s): cryoEqpMask before %s\n", pEqp->getDpName(), cryoEqpMask.toString().toLatin1().constData());
			if(!allCryoEqpMask.contains(funcType))
			{
				return;
			}
			for(int typeIdx = 0 ; typeIdx < cryoTypeActions.count() ; typeIdx++)
			{
				QAction *pAction = cryoTypeActions.at(typeIdx);
				if(pAction->data().toInt() == funcType)
				{
					pAction->setChecked(true);
					cryoEqpMask.append(funcType);
					break;
				}
			}
		}
		if(vacTypeMask & (VacType::BlueBeam | VacType::RedBeam))
		{
			switch(funcType)
			{
			case FunctionalType::CRYO_TT:
			case FunctionalType::CRYO_TT_EXT:
			case FunctionalType::CRYO_TT_SUM:
				funcType = FunctionalType::CRYO_TT;
				break;
			case FunctionalType::PROCESS_VPG_6A01:
			case FunctionalType::PROCESS_VPG_6E01:
				funcType = FunctionalType::PROCESS_VPG_6A01;
				break;
			default:
				break;
			}
			if(!allBeamEqpMask.contains(funcType))
			{
				return;
			}
			if(beamEqpMask.contains(pEqp->getFunctionalType()))
			{
				qDebug("SynopticLhcDialog::ensureDeviceVisible(%s): must be visible on BEAM, but it is not\n", pEqp->getDpName());
				return;
			}
			//qDebug("SynopticLhcDialog::ensureDeviceVisible(%s): beamEqpMask before %s\n", pEqp->getDpName(), beamEqpMask.toString().toLatin1().constData());
			for(int typeIdx = 0 ; typeIdx < beamTypeActions.count() ; typeIdx++)
			{
				QAction *pAction = beamTypeActions.at(typeIdx);
				if(pAction->data().toInt() == funcType)
				{
					pAction->setChecked(true);
					beamEqpMask.append(funcType);
					break;
				}
			}
		}
		//qDebug("SynopticLhcDialog::ensureDeviceVisible(%s): cryoEqpMask after %s\n", pEqp->getDpName(), cryoEqpMask.toString().toLatin1().constData());
		//qDebug("SynopticLhcDialog::ensureDeviceVisible(%s): beamEqpMask after %s\n", pEqp->getDpName(), beamEqpMask.toString().toLatin1().constData());
		pView->applyMask(vacTypeMask, cryoEqpMask, beamEqpMask);
		setMyWidth(false);
		pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
	}
	if(!pView->findDevicePosition(pEqp, position, visible))
	{
		//qDebug("SynopticLhcDialog::ensureDeviceVisible(%s): not added to view after mask change\n", pEqp->getDpName());
		return;	// No such device in view
	}
	if(!visible)
	{
		//qDebug("SynopticLhcDialog::ensureDeviceVisible(%s): not visible after mask change\n", pEqp->getDpName());
		return;	// Still not visible
	}
	if((pScroll->horizontalScrollBar()->value() < position) && (position < (pScroll->horizontalScrollBar()->value() + pScroll->width())))
	{
		return;	// Must be visible already
	}
	int value = position - pScroll->width() / 2;
	if(value < 0)
	{
		value = 0;
	}
	pScroll->horizontalScrollBar()->setValue(value);
}

/*
**	FUNCTION
**		Set width of this dialog to best fit to synoptic content
**
**	ARGUMENTS
**		init	- true if method is called during initialization
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::setMyWidth(bool init)
{
	if(!pView)
	{
		return;
	}
	// Set own height according to size of synoptic view
	int scrollHeight;
	if(init)
	{
		scrollHeight = pView->minimumHeight() + pScroll->horizontalScrollBar()->height() / 2;
		scrollHeight += 2;
		pScroll->setMinimumHeight(scrollHeight);
	}
	else
	{
		int minHeight = pView->minimumHeight();
		int realHeight = pScroll->viewport()->height();
		scrollHeight = pView->minimumHeight() + pScroll->horizontalScrollBar()->height();
		scrollHeight += 2;
		pScroll->setMinimumHeight(scrollHeight);
		// resize(width(), height() + minHeight - realHeight);
		setMinimumHeight(height() + minHeight - realHeight);
	}

	// Adjust own width
	int viewWidth = pView->minimumWidth();
	QDesktopWidget *pDesktop = QApplication::desktop();
	const QRect &screen = pDesktop->screenGeometry(pDesktop->screenNumber(this));
	// LIK 26.04.2009 if(init)
	{
		if(viewWidth > screen.width())
		{
			resize(screen.width() - 50, scrollHeight + pCombo->height());
		}
		else
		{
			resize(viewWidth + 10, scrollHeight + pCombo->height());
		}
	}
}

/*
**	FUNCTION
**		Slot activated when 'Print' item is selected in 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::filePrint(void)
{
	VacMainView::printWidget(this);
}

/*
**	FUNCTION
**		Slot activated when 'Sector' item is selected in 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::viewSector(void)
{
	makeDialogRequest(VacMainView::DialogSector);
}

/*
**	FUNCTION
**		Slot activated when 'Profile' item is selected in 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::viewProfile(void)
{
	makeDialogRequest(VacMainView::DialogProfile);
}

/*
**	FUNCTION
**		Make request to main view to show another dialog for data
**		shown in this dialog
**
**	ARGUMENTS
**		type	- Type of dialog to be opened, see enum in VacMainView class
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::makeDialogRequest(int type)
{
	QStringList sectorList;
	sectorList.append(pFirstSector->getName());
	sectorList.append(pLastSector->getName());
	VacMainView *pMainView = VacMainView::getInstance();
	pMainView->dialogRequest(type, vacTypeMask, sectorList, mode);
}

/*
**	FUNCTION
**		Slot activated when 'User manual' item is selected in 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::help(void)
{
}

void SynopticLhcDialog::typeMenu(QAction *pAction)
{
	if(!pAction->data().isValid())
	{
		return;
	}
	if(vacTypeActions.indexOf(pAction) >= 0)
	{
		vacTypeMenu(pAction);
	}
	else if(cryoTypeActions.indexOf(pAction) >= 0)
	{
		cryoEqpTypeMenu(pAction);
	}
	else if(beamTypeActions.indexOf(pAction) >= 0)
	{
		beamEqpTypeMenu(pAction);
	}
}

/*
**	FUNCTION
**		Slot activated when one of vacuum type items is selected
**		in 'Vacuum' submenu of 'View' menu
**
**	ARGUMENTS
**		id	- Bit mask corresponding to selected vacuum type
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::vacTypeMenu(QAction *pAction)
{
	int id = pAction->data().toInt();
	if(vacTypeMask & id)
	{
		vacTypeMask &= ~id;
		pAction->setChecked(false);
	}
	else
	{
		vacTypeMask |= id;
		pAction->setChecked(true);
	}
	pView->applyMask(vacTypeMask, cryoEqpMask, beamEqpMask);
	setMyWidth(false);
	pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
}

/*
**	FUNCTION
**		Slot activated when one of equipment type items is selected
**		in 'Equipment on isolation vacuum' submenu of 'View' menu
**
**	ARGUMENTS
**		id	- Functional type corresponding to selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::cryoEqpTypeMenu(QAction *pAction)
{
	int id = pAction->data().toInt();
	if(cryoEqpMask.contains(id))
	{
		cryoEqpMask.remove(id);
		pAction->setChecked(false);
	}
	else
	{
		cryoEqpMask.append(id);
		pAction->setChecked(true);
	}
	pView->applyMask(vacTypeMask, cryoEqpMask, beamEqpMask);
	setMyWidth(false);
	pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
}

/*
**	FUNCTION
**		Slot activated when one of equipment type items is selected
**		in 'Equipment on beam vacuum' submenu of 'View' menu
**
**	ARGUMENTS
**		id	- Functional type corresponding to selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::beamEqpTypeMenu(QAction *pAction)
{
	int id = pAction->data().toInt();
	if(beamEqpMask.contains(id))
	{
		beamEqpMask.remove(id);
		// L.Kopylov 28.01.2010 1 menu item switches all CRYO thermometers
		switch(id)
		{
		case FunctionalType::CRYO_TT:
			beamEqpMask.remove(FunctionalType::CRYO_TT_EXT);
			beamEqpMask.remove(FunctionalType::CRYO_TT_SUM);
			break;
		case FunctionalType::PROCESS_VPG_6A01:
			beamEqpMask.remove(FunctionalType::PROCESS_VPG_6E01);
			break;
		}

		pAction->setChecked(false);
	}
	else
	{
		beamEqpMask.append(id);
		// L.Kopylov 28.01.2010 1 menu item switches all CRYO thermometers
		switch(id)
		{
		case FunctionalType::CRYO_TT:
			beamEqpMask.append(FunctionalType::CRYO_TT_EXT);
			beamEqpMask.append(FunctionalType::CRYO_TT_SUM);
			break;
		case FunctionalType::PROCESS_VPG_6A01:
			beamEqpMask.append(FunctionalType::PROCESS_VPG_6E01);
			break;
		}
		pAction->setChecked(true);
	}
	pView->applyMask(vacTypeMask, cryoEqpMask, beamEqpMask);
	setMyWidth(false);
	pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
}

/*
**	FUNCTION
**		Slot activated when item is selected in main part combo box
**
**	ARGUMENTS
**		name	- name of selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::comboActivated(const QString &name)
{
	DataPool &pool = DataPool::getInstance();
	MainPart *pMainPart = pool.findMainPartData(name.toLatin1());
	if(!pMainPart)	// No selection
	{
		return;
	}

	// Selection is done differently for LHC ring and injection/dump lines
	QStringList	sectorList;
	if(pMainPart->getVacTypeMask() == VacType::Beam)	// injection/dump lines
	{
		pool.findSectorsInMainPart(pMainPart->getName(), sectorList, true);
	}
	else	// LHC ring selection
	{
		QList<MainPart *> mpList;
		mpList.append(pMainPart);
		mainVacMask = vacMask = VacType::None;
		buildRangeFromMps(mpList, sectorList);
	}

	Sector *pNewStart = NULL, *pNewEnd = NULL;
	// DSL sector shall not be used as start or end sector
	foreach(QString sectName, sectorList)
	{
		Sector *pSector = pool.findSectorData(sectName.toLatin1());
		if(pSector->getVacType() == VacType::DSL)
		{
			continue;
		}
		if(!pNewStart)
		{
			pNewStart = pSector;
		}
		pNewEnd = pSector;
	}
	if((!pNewStart) || (!pNewEnd))
	{
		selectInitialMp();
		return;
	}
	pFirstSector = pNewStart;
	pLastSector = pNewEnd;
	buildView(false);
	setTitle();
}

/*
**	FUNCTION
**		Slot activated when scrollbar(s) are used to scroll synoptic view
**
**	ARGUMENTS
**		x	- New X-coordinate of visible part
**		y	- New Y-coordinate of visible part
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::viewMove(int x)
{
	if(pView)
	{
		pView->setViewStart(x, pScroll->width(),
			pScroll->horizontalScrollBar()->isSliderDown());
	}
}

void SynopticLhcDialog::resizeEvent(QResizeEvent * /*pEvent */)
{
	if(pView)
	{
		pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
	}
}

/*
**	FUNCTION
**		Slot activated when state of mobile equipment in one of lines has been changed
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::mobileStateChange(void)
{
	pView->applyMask(vacTypeMask, cryoEqpMask, beamEqpMask);
	setMyWidth(false);
	pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
	setTypesMenu();
}

/*
**	FUNCTION
**		Set sensitivitiy/check state of items in equipment type menus
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticLhcDialog::setTypesMenu(void)
{
	unsigned vacMask;
	VacEqpTypeMask cryoMask, beamMask;
	pView->analyzeTypes(vacMask, cryoMask, beamMask);
	const QList<FunctionalType *> &cryoTypes = allCryoEqpMask.getList();
	FunctionalType *pItem;
	int idx;
	for(idx = 0 ; idx < cryoTypes.count() ; idx++)
	{
		pItem = cryoTypes.at(idx);
		for(int actIdx = 0 ; actIdx < cryoTypeActions.count() ; actIdx++)
		{
			QAction *pAction = cryoTypeActions.at(actIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				pAction->setEnabled(cryoMask.contains(pItem->getType()));
				pAction->setChecked(cryoEqpMask.contains(pItem->getType()));
				break;
			}
		}			
	}
	const QList<FunctionalType *> &beamTypes = allBeamEqpMask.getList();
	for(idx = 0 ; idx < beamTypes.count() ; idx++)
	{
		pItem = beamTypes.at(idx);
		for(int actIdx = 0 ; actIdx < beamTypeActions.count() ; actIdx++)
		{
			QAction *pAction = beamTypeActions.at(actIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				pAction->setEnabled(beamMask.contains(pItem->getType()));
				pAction->setChecked(beamEqpMask.contains(pItem->getType()));
				break;
			}
		}			
	}
}
/**
@brief SLOT Connect mobile active change to rebuilt in case it changes
*/
void SynopticLhcDialog::eqpActiveChange(Eqp * pEqp, DataEnum::DataMode mode) {
	if (pEqp) {
		if (mode != DataEnum::Replay) {
			buildView(false);
		}
	}
}
/**
@brief SLOT Show mobiles not connected
*/
void SynopticLhcDialog::showMobilesNC(void) {
	if (pView) {
		pView->showMobilesNotConnected();
		// Force redraw of the view
		pView->applyMask(vacTypeMask, cryoEqpMask, beamEqpMask);
		setMyWidth(false);
		pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
		//buildView(false); // does not rebuild the view with new eqp
	}

}
/**
@brief SLOT Hide mobiles not connected
*/
void SynopticLhcDialog::hideMobilesNC(void) {
	if (pView) {
		pView->hideMobilesNotConnected();
		// Force redraw of the view
		buildView(false);
	}
}

