#ifndef SCREENPOINT_H
#define	SCREENPOINT_H

#include "VacCtlEwoUtilExport.h"

// Storage for auxilliary data used for history data drawing

class VACCTLEWOUTIL_EXPORT ScreenPoint
{
public:
	ScreenPoint();
	~ScreenPoint() {}
	void reset(void);

	// Access
	inline int getX(void) const { return x; }
	inline void setX(int coord) { x = coord; }
	inline int getY(void) const { return y; }
	inline void setY(int coord) { y = coord; }
	inline int getTimeRange(void) const { return timeRange; }
	inline void setTimeRange(int range) { timeRange = range; }
	inline bool isDrawing(void) const { return drawing; }
	inline void setDrawing(bool flag) { drawing = flag; }
	inline bool isPrevExists(void) const { return prevExists; }
	inline void setPrevExists(bool flag) { prevExists = flag; }
	inline bool isNegative(void) const { return negative; }
	inline void setNegative(bool flag) { negative = flag; }

protected:
	// Last calculated X-coordinate in window
	int		x;

	// Last calculated X-coordinate in window
	int		y;

	// Result of time->X conversion
	int		timeRange;

	// Flag indicating if drawing is in progress
	bool	drawing;

	// Flag indicating if previous point exists
	bool	prevExists;

	// Flag indicating if previous point is negative (ONLY if negative
	// values are allowed)
	bool	negative;
};

#endif	// SCREENPOINT_H
