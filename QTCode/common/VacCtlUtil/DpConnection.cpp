//	Implementation of DpConnection class
//////////////////////////////////////////////////////////////////////////

#include "DpConnection.h"

DpConnection	DpConnection::instance;

DpConnection &DpConnection::getInstance(void) { return instance; }

/*
**	FUNCTION
**		Add DPE to list of pending connections if it is not there yet.
**
**	ARGUMENTS
**		dp			- DP name
**		dpe			- DPE name within DP, can be NULL if DP has no DPEs
**
**	RETURNS
**		true	- if DPE has been added to queue,
**		false	- DPE was not added to queue
**
**	CAUTIONS
**		None
*/
bool DpConnection::connect(const char *dp, const char *dpe)
{
	QByteArray dpeName(dp);
	if(dpe)
	{
		dpeName += ".";
		dpeName += dpe;
	}
	return getInstance().connect(dpeName);
}

/*
**	FUNCTION
**		Add DPE to list of pending disconnections if it is not there yet.
**
**	ARGUMENTS
**		dp			- DP name
**		dpe			- DPE name within DP, can be NULL if DP has no DPEs
**
**	RETURNS
**		true	- if DPE has been added to queue,
**		false	- DPE was not added to queue
**
**	CAUTIONS
**		None
*/
bool DpConnection::disconnect(const char *dp, const char *dpe)
{
	QByteArray dpeName(dp);
	if(dpe)
	{
		dpeName += ".";
		dpeName += dpe;
	}
	return getInstance().disconnect(dpeName);
}

//////////////////////////////////////////////////////////////////////////////////
////////////////////// Construction/destruction

DpConnection::DpConnection()
{
	pDpes = new QList<DpConnectItem *>();
	signalSent = false;
}

DpConnection::DpConnection(const DpConnection &source) : QObject()
{
	pDpes = new QList<DpConnectItem *>(*(source.pDpes));
	signalSent = false;
}

DpConnection & DpConnection::operator = (const DpConnection &other)
{
	*pDpes = *(other.pDpes);
	signalSent = false;
	return *this;
}

DpConnection::~DpConnection()
{
	while(!pDpes->isEmpty())
	{
		delete pDpes->takeFirst();
	}
	delete pDpes;
}


/*
**	FUNCTION
**		Add DPE to list of pending connections if it is not there yet.
**
**	ARGUMENTS
**		dpe			- DPE name = DP.DPE
**
**	RETURNS
**		true	- if DPE has been added to queue,
**		false	- DPE was not added to queue
**
**	CAUTIONS
**		None
*/
bool DpConnection::connect(const QByteArray &dpe)
{
	mutex.lock();
	bool result = true;
	/* LIK 29.04.2009  This check significantly slows down start of
	ProfileAllSPS in online mode. Let's assume nobody opens dialog in
	order to close it immediately - before even connect events have
	been processed

	for(DpConnectItem *pItem = pDpes->first() ; pItem ; pItem = pDpes->next())
	{
		if(pItem->getDpe() == dpe)
		{
			if(!pItem->isToConnect())
			{
				pDpes->remove(pItem);
			}
			result = false;
			break;
		}
	}
	*/
	if(result)
	{
		pDpes->append(new DpConnectItem(dpe, true));
	}
	mutex.unlock();
	if(result)
	{
		if(!signalSent)
		{
			emit changed();
			signalSent = true;
		}
	}
	return result;
}

/*
**	FUNCTION
**		Add DPE to list of pending disconnections if it is not there yet.
**
**	ARGUMENTS
**		dpe			- DPE name = DP.DPE
**
**	RETURNS
**		true	- if DPE has been added to queue,
**		false	- DPE was not added to queue
**
**	CAUTIONS
**		None
*/
bool DpConnection::disconnect(const QByteArray &dpe)
{
	mutex.lock();
	bool result = true;
	/* LIK 29.04.2009  This check significantly slows down start of
	ProfileAllSPS in online mode. Let's assume nobody opens dialog in
	order to close it immediately - before even connect events have
	been processed

	for(DpConnectItem *pItem = pDpes->first() ; pItem ; pItem = pDpes->next())
	{
		if(pItem->getDpe() == dpe)
		{
			if(pItem->isToConnect())
			{
				pDpes->remove(pItem);
			}
			result = false;
			break;
		}
	}
	*/
	if(result)
	{
		pDpes->append(new DpConnectItem(dpe, false));
	}
	mutex.unlock();
	if(result)
	{
		if(!signalSent)
		{
			emit changed();
			signalSent = true;
		}
	}	
	return result;
}

/*
**	FUNCTION
**		Get next DPE to be connected or disconnected
**
**	ARGUMENTS
**		dpe			- variable where DPE name will be written
**		connect		- Variable where connect/disconnect flag will be written
**
**	RETURNS
**		true	- if queue is not empty;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool DpConnection::getNext(QByteArray &dpe, bool &connect)
{
	mutex.lock();
	bool result;
	if(pDpes->isEmpty())
	{
		result = false;
		signalSent = false;
	}
	else
	{
		result = true;
		DpConnectItem *pItem = pDpes->takeFirst();
		dpe = pItem->getDpe();
		connect = pItem->isToConnect();
		delete pItem;
	}
	mutex.unlock();
	return result;
}

