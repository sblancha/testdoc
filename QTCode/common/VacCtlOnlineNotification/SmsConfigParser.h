#ifndef SMSCONFIGPARSER_H
#define	SMSCONFIGPARSER_H

// Converter of SMS configuration from/to *.CSV file

#include "VacCtlOnlineNotificationExport.h"

#include <qlist.h>
#include <qstring.h>
#include <qstringlist.h>

class SmsConfig;

class SmsConfigParser
{
public:
	VACCTLONLINENOTIFICATION_EXPORT static SmsConfigParser &getInstance(void);

	VACCTLONLINENOTIFICATION_EXPORT void clear(void);
	VACCTLONLINENOTIFICATION_EXPORT int addConfig(const char *id);
	VACCTLONLINENOTIFICATION_EXPORT int setVisibleName(int configId, const char *name, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int setMode(int configId, unsigned mode, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int setMainType(int configId, int type, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int addUsageFilter(int configId, int funcType, const QByteArray &attrName, bool reverse, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int setScopeType(int configId, int type, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int addScopeItem(int configId, const char *pName, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int setCritJoinType(int configId, int type, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int addCriteria(int configId, int funcType, int type, int subType, bool reverse, float lLimit, float uLimit, int minDuration, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int setMessage(int configId, const char *message, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int setFiltering(int configId, int type, int time, int groupType, int groupTime, int indivLimit, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int setGroupMessage(int configId, const char *message, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int addRecipient(int configId, const char *recipient, QString &errMsg);
	
	VACCTLONLINENOTIFICATION_EXPORT int writeToFile(const char *fileName, QString &errMsg);

	VACCTLONLINENOTIFICATION_EXPORT int parseFile(const char *fileName, QStringList &errList);

	VACCTLONLINENOTIFICATION_EXPORT int firstParsedConfig(void);
	VACCTLONLINENOTIFICATION_EXPORT int nextParsedConfig(int configId);

	VACCTLONLINENOTIFICATION_EXPORT int getVisibleName(int configId, QString &name, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int getMode(int configId, unsigned &mode, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int getMainType(int configId, int &type, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int getUsageFilter(int configId, QList<int> &funcTypes, QList<QByteArray> &attrNames,
				QList<bool> &reverseFlags, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int getScopeType(int configId, int &type, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int getScopeItems(int configId, QStringList &items, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int getCritJoinType(int configId, int &type, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int getCriteria(int configId, QList<int> &funcTypes, QList<int> &types,
				QList<int> &subTypes, QList<bool> &reverseFlags,
				QList<float> &lLimits, QList<float> &uLimits, QList<int> &minDurations,
				QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int getMessage(int configId, QString &message, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int getFiltering(int configId, int &type, int &time, int &groupType, int &groupTime, int &indivLimit, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int getGroupMessage(int configId, QString &message, QString &errMsg);
	VACCTLONLINENOTIFICATION_EXPORT int getRecipients(int configId, QStringList &recipients, QString &errMsg);

	// Made constructor/destructor public for MS compiler
	SmsConfigParser(void);
	~SmsConfigParser();
	

protected:

	// The only instance of parser
	static SmsConfigParser	instance;

	// List of SmsConfigs
	QList<SmsConfig *>	list;

	// List of knwon machine modes
	QStringList			machineModes;

	// Bit mask 'all known machine modes'
	unsigned			allModesMask;

	void initMachineModes(void);

	int parseConfigLine(QString &line, int lineNo, QStringList &errList);

	QString lineNoError(int lineNo);
	int errorNotEnoughItems(int lineNo, const char *msg, QStringList &errList);

	SmsConfig *findConfig(int id, QString &errMsg);
};

#endif	// SMSCONFIGPARSER_H
