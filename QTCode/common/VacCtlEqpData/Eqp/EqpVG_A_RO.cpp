//	Implementation of EqpVG_A_RO class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVG_A_RO.h"

#include "DataPool.h"
#include "FunctionalTypeVG.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0
#define COLOR_WARNING			255,255,0
#define COLOR_UNDERRANGE		0,180,0

//	Main state bits to be analyzed
#define	RR1_VALID				(0x40000000)
#define RR1_ON					(0x02000000)
#define RR1_OFF					(0x01000000)
#define RR1_ERROR				(0x00800000)
#define	RR1_WARNING				(0x00400000)
#define RR1_WARNING_CODE_MARK	(0x0000FF00)
#define RR1_ERROR_CODE_MASK		(0x000000FF)
#define RR1_UNDERRANGE_MASK		(0x00007E00)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVG_A_RO::EqpVG_A_RO(const Eqp &source) : Eqp(source)
{
	rr1Online = rr1Replay = 0;
	prOnline = prReplay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::VG_A_RO;
}

EqpVG_A_RO::~EqpVG_A_RO()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_A_RO::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	addDpeNameToList(stateDpes, "RR1");
}

/*
**	FUNCTION
**		Return list of all DPEs, related to analog value of device. The list is used
**		to build value history
**
**	ARGUMENTS
**		valueDpes	- Variable where all value DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_A_RO::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	addDpeNameToList(valueDpes, "PR");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_A_RO::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "PR", mode);
	connectDpe(pDst, "objState", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_A_RO::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "PR", mode);
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "EqpStatus", mode);
	disconnectDpe(pDst, "objState", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_A_RO::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if (!strcmp(dpe, "RR1"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();

			if (rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if (!strcmp(dpe, "PR"))
	{
		if (value.canConvert(QVariant::Double))
		{
			double newValue = value.toDouble();
			if (prOnline == newValue)
			{
				return;
			}
			prOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "objState"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			int newValue = value.toUInt();
			if (objStateOnline == newValue)
			{
				return;
			}
			objStateOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_A_RO::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if (!strcmp(dpe, "RR1"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if (!strcmp(dpe, "PR"))
	{
		if (value.canConvert(QVariant::Int))
		{
			int newValue = value.toInt();
			if (prReplay == newValue)
			{
				return;
			}
			prReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "objState"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			int newValue = value.toUInt();
			if (objStateReplay == newValue)
			{
				return;
			}
			objStateReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_A_RO::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if (mode == DataEnum::Replay)
	{
		if (alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if (alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_A_RO::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if (isColorByCtlStatus(color, mode))
	{
		return;
	}
	if (mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, prReplay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, prOnline, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Return main value
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVG_A_RO::getMainValue(DataEnum::DataMode mode)
{
	return mode == DataEnum::Replay ? (float)prReplay : (float)prOnline;
}
#endif

/*
**	FUNCTION
**		Check if main value for gauge (pressure) is valid or not.
**		Latest known requirement: even invalid value shall be shown,
**		so the value is always considered to be valid
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
bool EqpVG_A_RO::isMainValueValid(DataEnum::DataMode /* mode */)
{
	return true;
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_A_RO::getMainColor(unsigned rr1, int /* pr */, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (rr1 & RR1_ERROR)
	{
		color.setRgb(COLOR_ERROR);
	}
	else if ((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if (rr1 & RR1_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr1 & RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else if (rr1 & RR1_ON)
	{
		if (((rr1 & RR1_UNDERRANGE_MASK) >> 8) == 126) {
			color.setRgb(COLOR_UNDERRANGE);
		}
		else color.setRgb(COLOR_ON);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_A_RO::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if (isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if (mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, prReplay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, prOnline, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_A_RO::getMainValueString(QString &string, DataEnum::DataMode mode)
{
	if (isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if (mode == DataEnum::Replay)
	{
		getMainValueString(rr1Replay, prReplay, plcAlarmReplay, string);
	}
	else
	{
		getMainValueString(rr1Online, prOnline, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_A_RO::getMainValueString(unsigned /* rr1 */, int pr, bool /* plcAlarm */, QString &string)
{
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d", pr);
#else
	sprintf(buf, "%d", pr);
#endif
	string = buf;
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_A_RO::getMainStateString(unsigned rr1, int /* pr */, bool plcAlarm, QString &string)
{
	if (plcAlarm)
	{
		string = "PLC Error";
	}
	else if (!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if (rr1 & RR1_ON)
	{
		if (rr1 & RR1_OFF)
		{
			string = "Error";
		}
		else
		{
			string = "ON";
		}
	}
	else if (rr1 & RR1_OFF)
	{
		string = "OFF";
	}
	else
	{
		string = "Undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_A_RO::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainValueString(valueString, mode);
	string += valueString;
}
#endif


/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_A_RO::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
	QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("RR1");
	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, 0, false, state);
	getMainColor(rr1, 0, false, color);
	while (!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpVG_A_RO::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	return rr1 & RR1_ERROR_CODE_MASK;
}


/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_A_RO::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if (!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if (pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "EqpStatus");
	addDpeNameToList(dpes, "RR1");
	addDpeNameToList(dpes, "PR");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history)
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVG_A_RO::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if (criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if (checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if (!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if (pPlc)
		{
			if (!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if (!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if ((rr1 & (RR1_ON | RR1_OFF)) == RR1_ON)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if ((rr1 & (RR1_ON | RR1_OFF)) == RR1_OFF)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if (criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if (criteria.isErrors())
	{
		if (rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if (criteria.isWarnings())
	{
		if (rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}

	// Check pressure if needed
	if (criteria.isUsePressureLimit())
	{
		float pr = mode == DataEnum::Replay ? prReplay : prOnline;
		if (pr > criteria.getPressureLimit())
		{
			return 1;
		}
	}

	// Last chance - criteria on pressure grow
	if (criteria.isUsePressureGrowLimit())
	{
		// Can not decide right now, more info is needed
		return 2;
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Return parameters for history query, required for device list
**
**	ARGUMENTS
**		dpe		- [out] DPE, for which history is required
**		start	- [out] start time of history query
**		end		- [out] end time of history query
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_A_RO::getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if (!criteria.isUsePressureGrowLimit())
	{
		dpe = "";
		return;
	}
	dpe = dpName;
	dpe += ".PR";
	start = criteria.getPressureStartTime();
	if (criteria.isPressureEndTimeNow())
	{
		end = QDateTime::currentDateTime();
	}
	else
	{
		end = criteria.getPressureEndTime();
	}
}
#endif

/*
**	FUNCTION
**		Return grow factor for main value (pressure) to check if
**		device matches device list criteria
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Pressure grow factor; or
**		0 if criteria does not contain pressure grow
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVG_A_RO::getGrowFactorForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if (!criteria.isUsePressureGrowLimit())
	{
		return 0.0;
	}
	return criteria.getPressureGrowLimit();
}

int EqpVG_A_RO::getSpikeFilterForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if (!criteria.isUsePressureGrowLimit())
	{
		return 0.0;
	}
	return criteria.getPressureSpikeInterval();
}

#endif


/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_A_RO::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if (pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if (pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if (checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	// Without PLC connection does not match any criteria
	if (plcAlarmOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// In invalid state - only matches Error/warning criteria
	if (!(rr1Online & RR1_VALID))
	{
		switch (pCriteria->getType())
		{
		case EqpMsgCriteria::Error:
			if (rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if (rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		default:
			pCriteria->setMatch(false, false, 0);
			return;
		}
	}

	// State is valid - decide based on criteria
	unsigned maskedRr1 = rr1Online & (RR1_ON | RR1_OFF);
	switch (pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch (pCriteria->getSubType())
		{
		case FunctionalTypeVG::Off:
			if (maskedRr1 == RR1_OFF)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVG::On:
			if (maskedRr1 == RR1_ON)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	case EqpMsgCriteria::Error:
		if (rr1Online & RR1_ERROR)
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else
		{
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	case EqpMsgCriteria::Warning:
		if (rr1Online & (RR1_ERROR | RR1_WARNING))
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else
		{
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	case EqpMsgCriteria::MainValue:
		if (maskedRr1 == RR1_ON)
		{
			// It can be (and it was observed) that callback for PR=10000
			// arrives before callback for RR1 with state=OFF. Reverse
			// situation can also occur when switching ON.
			// To avoid sending wrong messages in such cases value 10000
			// is considered to be invalid in all cases and does not change
			// criteria matching
			if (prOnline > 9999)
			{
				return;
			}
			if (pCriteria->isReverse())
			{
				if (prOnline > pCriteria->getUpperLimit())
				{
					pCriteria->setMatch(false, true, prOnline);
					return;
				}
				else if (prOnline < pCriteria->getLowerLimit())
				{
					pCriteria->setMatch(true, true, prOnline);
					return;
				}
				// Between lower and upper limit - no change
				return;
			}
			else
			{
				if (prOnline > pCriteria->getUpperLimit())
				{
					pCriteria->setMatch(true, true, prOnline);
					return;
				}
				else if (prOnline < pCriteria->getLowerLimit())
				{
					pCriteria->setMatch(false, true, prOnline);
					return;
				}
				// Between lower and upper limit - no change
				return;
			}
		}
		break;
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}

