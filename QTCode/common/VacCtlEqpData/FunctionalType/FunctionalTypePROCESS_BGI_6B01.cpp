//	Implementation of FunctionalTypePROCESS_BGI_6B01 class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypePROCESS_BGI_6B01.h"


#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypePROCESS_BGI_6B01::FunctionalTypePROCESS_BGI_6B01(int type, const char *name, const char *description) :
	FunctionalTypePROCESS(type, name, description)
{
}

QString FunctionalTypePROCESS_BGI_6B01::getStepResourceBase(void)
{
	return QString("VBGI_6B01.Step_");
}
