#ifndef FUNCTIONALTYPEINJ_6B02_H
#define FUNCTIONALTYPEINJ_6B02_H

// Special functional type for gas injection 6B02 - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeINJ_6B02 : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Nominal = 1,
		VvgFullOpen = 2
	};

	FunctionalTypeINJ_6B02(int type, const char *name, const char *description);
	virtual ~FunctionalTypeINJ_6B02() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEINJ_6B02_H
