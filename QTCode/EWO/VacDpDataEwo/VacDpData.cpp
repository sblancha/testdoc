//	Implementation of VacDpData class
/////////////////////////////////////////////////////////////////////////////////

#include "VacDpData.h"

#include "Eqp.h"
#include "DataPool.h"

#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
/////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

VacDpData::VacDpData(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	dpName = "<none>";
	setMinimumSize(3, 3);
	connect(&connection, SIGNAL(dpeValueChange(const char *, int, const QVariant &)),
		this, SIGNAL(newDpeValue(const char *, int, const QVariant &)));
}

VacDpData::~VacDpData()
{
}

/*
**	FUNCTION
**		Connect to given DPE of currently selected DP in current data
**		acquisition mode
**
**	ARGUMENTS
**		dpe	- DPE name
**
**	RETURNS
**		0	= success,
**		-1	= failure
**
**	CAUTIONS
**		None
*/
int	VacDpData::connectDpe(const char *dpe)
{
	findEqp();
	connection.connectDpe(dpe);
	update();
	return 0;
}

/*
**	FUNCTION
**		Find last known value of given DPE in current data acquisition mode
**
**	ARGUMENTS
**		dpe	- DPE name
**
**	RETURNS
**		0	= success,
**		-1	= failure
**
**	CAUTIONS
**		None
*/
const QVariant &VacDpData::getDpeValue(const char *dpe)
{
	static QVariant dummy;
	findEqp();
	Eqp *pEqp = connection.getEqp();
	if(!pEqp)
	{
		return dummy;
	}
	QDateTime timeStamp;
	return pEqp->getDpeValue(dpe, connection.getMode(), timeStamp);
}

bool VacDpData::isPlcAlarm(void)
{
	findEqp();
	Eqp *pEqp = connection.getEqp();
	if(!pEqp)
	{
		return false;
	}
	return pEqp->isPlcAlarm(connection.getMode());
}

void VacDpData::findEqp(void)
{
	if(connection.getEqp())
	{
		return;
	}
	if(dpName.isEmpty())
	{
		return;
	}
	DataPool &pool = DataPool::getInstance();
	connection.setEqp(pool.findEqpByDpName(dpName.toLatin1()));
}

void VacDpData::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	if(connection.getEqp())
	{
		painter.fillRect(rect(), connection.getMode() == DataEnum::Replay ? Qt::cyan : Qt::green);
	}
	else
	{
		painter.fillRect(rect(), Qt::blue);
	}
}

