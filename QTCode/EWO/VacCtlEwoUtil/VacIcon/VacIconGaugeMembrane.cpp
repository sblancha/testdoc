#include "VacIconGaugeMembrane.h"

#include <qpainter.h>

/**
@brief FUNCTION PROTECTED draw membrane symbol inside 
@param[in]  painter	Painter object
*/
void VacIconGaugeMembrane::drawSpecific(QPainter &painter) {
	painter.setPen(Qt::black);
	painter.drawLine(1, 10, 20, 10);
}
