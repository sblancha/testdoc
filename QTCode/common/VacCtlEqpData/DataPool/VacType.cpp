#include "VacType.h"

#include "PlatformDef.h"

#include <ctype.h>

/*
**
**	FUNCTION
**		Convert string representation of vacuum type in beam line
**		configuration file to vacuum type
**
**	PARAMETERS
**		pString	- Pointer to string representation of vacuum type
**
**	RETURNS
**		Enum value corresponding to string representation.
**
**	CAUTIONS
**		None
*/
int VacType::decodeConn(const char *pString)
{
	if(!strcasecmp( pString, "R"))
	{
		return RedBeam;
	}
	else if(!strcasecmp(pString, "B"))
	{
		return BlueBeam;
	}
	else if(!strcasecmp(pString, "M"))	// Support for DSL line(s)
	{
		return Cryo;
	}
	else if(!strcasecmp(pString, "D"))	// Support for DSL line(s)
	{
		return DSL;
	}
	return None;
}

/*
**
**	FUNCTION
**		Decode vacuum type ( subsystem ) from equipment/sector/etc. name.
**		This only works for LHC where vacuum type is a last
**		character of device name.
**		Some devices in LHC have a number at the end of name, for example,
**		bakeout racks and virtual VPCIs. The rule is:
**		<Name>.<VacTypeCharacter>.<number>
**
**	PARAMETERS
**		name	- Name from which vacuum type shall be decoded
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
int VacType::decodeName(const char *name)
{
	int	vacType = Beam;

	int	len = (int)strlen(name);

	if(len < 2)
	{
		return vacType;
	}

	// 27.02.2016 L.Kopylov: special check for TI2 and TI8 sectors - their names have changed
	if(strstr(name, "TI2") || strstr(name, "TI8"))
	{
		return vacType;
	}

	char	toAnalyze = '\0';
	if((name[len-2] == '.') && (!isdigit( name[len-1])))	// <Name>.<VacTypeCharacter>
	{
		toAnalyze = name[len-1];	// take last character of name
	}
	else	// skip number at the end of name
	{
		int	n;
		for(n = len - 1 ; n > 0 ; n--)
		{
			if(!isdigit( name[n]))
			{
				break;
			}
		}
		if(n > 0)
		{
			if(name[n] == '.')
			{
				toAnalyze = name[n-1];
			}
		}
	}
	switch(toAnalyze)
	{
	case 'M':
		vacType = Cryo;
		break;
	case 'Q':
		vacType = Qrl;
		break;
	case 'C':
		vacType = CommonBeam;
		break;
	case 'R':
		vacType = RedBeam;
		break;
	case 'B':
		vacType = BlueBeam;
		break;
	case 'X':
		vacType = CrossBeam;
		break;
	case 'D':
		vacType = DSL;
		break;
	}
	return vacType;
}

/*
**
**	FUNCTION
**		Check if given vacuum type only includes single beam vacuum type,
**		i.e. it is suitable for displaying in SPS-style synoptic, profile etc.
**
**	PARAMETERS
**		type	- Vacuum type mask
**
**	RETURNS
**		true	- If given vacuum type only includes single beam vacuum;
**		false	- Otherwise
**
** CAUTIONS
**		None
*/
bool VacType::isSingleBeam(unsigned type)
{
	if(!type)
	{
		return false;
	}
	if(type & (Qrl | Cryo | DSL | CrossBeam | CommonBeam))
	{
		return false;
	}
	if(type == Beam)
	{
		return true;
	}
	unsigned beamMasked = type & (RedBeam | BlueBeam | Beam);
	if(beamMasked == (RedBeam & Beam))
	{
		return true;
	}
	if(beamMasked == (BlueBeam & Beam))
	{
		return true;
	}
	return false;
}

