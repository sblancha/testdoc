//	Implementation of SectorViewDialog class
/////////////////////////////////////////////////////////////////////////////////

#include "SectorViewDialog.h"
#include "SectView.h"
#include "SectViewXml.h"

#include "VacMainView.h"
#include "Sector.h"
#include "DataPool.h"

#include <qpushbutton.h>
#include <qmenu.h>
#include <qlabel.h>
#include <qcombobox.h>
#include <qscrollarea.h>
#include <QScrollBar>
#include <qlayout.h>
#include <qdesktopwidget.h>
#include <qapplication.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

SectorViewDialog::SectorViewDialog(Sector *pFirstSector, Sector *pLastSector,
	DataEnum::DataMode mode) :
	QDialog(NULL /* VacMainView::getInstance() */, Qt::Window)
{
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle("Sector View");
	this->pFirstSector = pFirstSector;
	this->pLastSector = pLastSector;
	this->mode = mode;
	pView = NULL;

	buildLayout();
	setTitle();
	buildView(true);
	if(mode == DataEnum::Replay)
	{
		VacMainView::replayDialogOpened(this);
	}
}

SectorViewDialog::~SectorViewDialog()
{
	if(mode == DataEnum::Replay)
	{
		VacMainView::replayDialogDeleted(this);
	}
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SectorViewDialog::buildLayout(void)
{
	// 1) Main layout - menu, labels and combobox on top, scrolled view on bottom
	QVBoxLayout *mainBox = new QVBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);

	// 2) Horizontal layout with
	//	- buttons for activating popup menus
	//	- title label
	//	- ComboBox with main part selection
	QHBoxLayout *topBox = new QHBoxLayout();
	topBox->setSpacing(0);
	mainBox->addLayout(topBox);

	// 2.1 all popup menus
	pFilePb = new QPushButton("&File", this);
	pFilePb->setFlat(true);
	QSize size = pFilePb->fontMetrics().size(Qt::TextSingleLine, "File");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pFilePb->setFixedSize(size);
	buildFileMenu();
	pFilePb->setMenu(pFileMenu);
	topBox->addWidget(pFilePb);

	pViewPb = new QPushButton("&View", this);
	pViewPb->setFlat(true);
	size = pViewPb->fontMetrics().size(Qt::TextSingleLine, "View");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pViewPb->setFixedSize(size);
	buildViewMenu();
	pViewPb->setMenu(pViewMenu);
	topBox->addWidget(pViewPb);

	pHelpPb = new QPushButton("&Help", this);
	pHelpPb->setFlat(true);
	size = pHelpPb->fontMetrics().size(Qt::TextSingleLine, "Help");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pHelpPb->setFixedSize(size);
	buildHelpMenu();
	pHelpPb->setMenu(pHelpMenu);
	topBox->addWidget(pHelpPb);

	
	// 2.2 synoptic title
	pTitleLabel = new QLabel("Sector View", this);
	topBox->addWidget(pTitleLabel, 10);	// The only resizable widget on top of dialog
	pTitleLabel->setAlignment(Qt::AlignHCenter);

	// 2.3 Combo box for main part selection
	buildMainPartCombo();
	topBox->addWidget(pCombo);

	// 3 Scroll view where synoptic will lie
	pScroll = new QScrollArea(this);
	mainBox->addWidget(pScroll, 10);	// Scroll view will consume all free space
	connect(pScroll->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(viewMove(int)));
}

/*
**	FUNCTION
**		Build 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SectorViewDialog::buildFileMenu(void)
{
	pFileMenu = new QMenu(this);
	pFileMenu->addAction("Print...", this, SLOT(filePrint()));
	pFileMenu->addSeparator();
	pFileMenu->addAction("Close", this, SLOT(deleteLater()));
}

/*
**	FUNCTION
**		Build 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SectorViewDialog::buildViewMenu(void)
{
	pViewMenu = new QMenu(this);
	pViewMenu->addAction("Synoptic...", this, SLOT(viewSynoptic()));
	pViewMenu->addAction("Pressure Profile...", this, SLOT(viewProfile()));
}

/*
**	FUNCTION
**		Build 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SectorViewDialog::buildHelpMenu(void)
{
	pHelpMenu = new QMenu(this);
	pHelpMenu->addAction("User manual...", this, SLOT(help()));
}

/*
**	FUNCTION
**		Build main part combo box, insert main part names which can be selected
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SectorViewDialog::buildMainPartCombo(void)
{
	pCombo = new QComboBox(this);
	pCombo->setEditable(false);
	pCombo->clear();
	DataPool &pool = DataPool::getInstance();
	QStringList strList;
	pool.findMainPartNames(strList, true);
	const QList<Sector *> &sectors = pool.getSectors();
	foreach(QString mpName, strList)
	{
		bool skip = false;
		MainPart *pMainPart = pool.findMainPartData(mpName.toLatin1());
		unsigned vacType = pMainPart->getVacTypeMask();
		if(!VacType::isSingleBeam(vacType))
		{
			continue;
		}
		for(int idx = 0 ; idx < sectors.count() ; idx++)
		{
			Sector *pSector = sectors.at(idx);
			if(!pSector->getSpecSectPanel().isEmpty())
			{
				continue;
			}
			if(pSector->isOuter())
			{
				continue;
			}
			if(pSector->isInMainPart(pMainPart))
			{
				if(!pSector->getSpecSynPanel().isEmpty())
				{
					skip = true;
					break;
				}
			}
		}
		if(!skip)
		{
			if(pool.areSectorsOfMainPartContinuous(pMainPart))
			{
				pCombo->addItem(mpName);
			}
			else
			{
				printf("skip %s: sectors are not continuous\n", pMainPart->getName());
				fflush(stdout);
				for(int idx = 0 ; idx < sectors.count() ; idx++)
				{
					Sector *pSector = sectors.at(idx);
					if(!pSector->isInMainPart(pMainPart))
					{
						continue;
					}
					if(!pSector->getSpecSectPanel().isEmpty())
					{
						pCombo->addItem(mpName);
						break;
					}
				}
			}
		}
	}
	selectInitialMp();
	connect(pCombo, SIGNAL(activated(const QString &)),
		this, SLOT(comboActivated(const QString &)));
}

/*
**	FUNCTION
**		Set initial selection in main part combo box
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SectorViewDialog::selectInitialMp(void)
{
	DataPool &pool = DataPool::getInstance();
	int nMps = pCombo->count();
	int *mps = (int *)calloc(nMps, sizeof(int));
	Sector *pSector = pFirstSector;
	do
	{
		for(int n = 0 ; n < nMps ; n++)
		{
			MainPart *pMainPart = pool.findMainPartData(pCombo->itemText(n).toLatin1());
			if(pSector->isInMainPart(pMainPart))
			{
				mps[n]++;
			}
		}
		if(pSector == pLastSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);

	int bestIdx = -1, bestWeight = 0;
	for(int n = 0 ; n < nMps ; n++)
	{
		if(mps[n] > bestWeight)
		{
			bestWeight = mps[n];
			bestIdx = n;
		}
	}
	free((void *)mps);
	pCombo->setCurrentIndex(bestIdx);
}

/*
**	FUNCTION
**		Set text and color for title label widget
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorViewDialog::setTitle(void)
{
	if(mode == DataEnum::Replay)
	{
		QPalette palette = pTitleLabel->palette();
		palette.setColor(QPalette::Window, Qt::cyan);
		pTitleLabel->setPalette(palette);
		pTitleLabel->setAutoFillBackground(true);
	}
	QString title = "Sector View, sector";
	if(pFirstSector == pLastSector)
	{
		title += " ";
		title += pFirstSector->getName();
	}
	else
	{
		title += "s ";
		title += pFirstSector->getName();
		title += " ... ";
		title += pLastSector->getName();
	}
	pTitleLabel->setText(title);
}

/*
**	FUNCTION
**		Build synoptic view according to current selection
**
**	ARGUMENTS
**		init	- Flag indicating if method is called from constructor
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorViewDialog::buildView(bool init)
{
	if(pView)
	{
		delete pView;
		pView = NULL;
	}
	// Create instance of synoptic view
	if(pFirstSector->getSpecSectPanel().isEmpty())
	{
		pView = SectView::create(pScroll->viewport(), pFirstSector, pLastSector, mode);
	}
	else
	{
		pView = SectViewXml::create(pScroll->viewport(), pFirstSector->getSpecSectPanel(), mode);
	}

	// Analyze equipment types in view - this method must be called even if it makes no sense
	unsigned vacMask;
	VacEqpTypeMask beamMask, cryoMask;
	pView->analyzeTypes(vacMask, cryoMask, beamMask);

	// Build content of synoptic view with dummy selection mask
	pView->applyMask(beamMask);

	// Add view to scroller
	pView->show();
	pScroll->setWidget(pView);

	setMyWidth(init);
	pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
}

/*
**	FUNCTION
**		Set width of this dialog to best fit to sector view content
**
**	ARGUMENTS
**		init	- true if method is called during initialization
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorViewDialog::setMyWidth(bool init)
{
	if(!pView)
	{
		return;
	}
	// Set own height according to size of synoptic view
	int scrollHeight;
	if(init)
	{
		scrollHeight = pView->minimumHeight() + pScroll->horizontalScrollBar()->height() / 2;
		scrollHeight += 2;
		pScroll->setMinimumHeight(scrollHeight);
	}
	else
	{
		int minHeight = pView->minimumHeight();
		int realHeight = pScroll->viewport()->height();
		scrollHeight = pView->minimumHeight() + pScroll->horizontalScrollBar()->height();
		scrollHeight += 2;
		pScroll->setMinimumHeight(scrollHeight);
		// resize(width(), height() + minHeight - realHeight);
		setMinimumHeight(height() + minHeight - realHeight);
	}

	// Adjust own width
	int viewWidth = pView->minimumWidth();
	QDesktopWidget *pDesktop = QApplication::desktop();
	const QRect &screen = pDesktop->screenGeometry(pDesktop->screenNumber(this));
	// LIK 21.04.2009 if(init)
	{
		if(viewWidth > screen.width())
		{
			resize(screen.width() - 50, scrollHeight + pCombo->height());
		}
		else
		{
			resize(viewWidth + 10, scrollHeight + pCombo->height());
		}
	}
}

/*
**	FUNCTION
**		Slot activated when 'Print' item is selected in 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorViewDialog::filePrint(void)
{
	VacMainView::printWidget(this);
}

/*
**	FUNCTION
**		Slot activated when 'Synoptic' item is selected in 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorViewDialog::viewSynoptic(void)
{
	makeDialogRequest(VacMainView::DialogSynoptic);
}

/*
**	FUNCTION
**		Slot activated when 'Profile' item is selected in 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorViewDialog::viewProfile(void)
{
	makeDialogRequest(VacMainView::DialogProfile);
}

/*
**	FUNCTION
**		Make request to main view to show another dialog for data
**		shown in this dialog
**
**	ARGUMENTS
**		type	- Type of dialog to be opened, see enum in VacMainView class
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorViewDialog::makeDialogRequest(int type)
{
	QStringList sectorList;
	unsigned vacType = 0;
	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pFirstSector;
	do
	{
		vacType |= pSector->getVacType();
		sectorList.append(pSector->getName());
		if(pSector == pLastSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);
	VacMainView *pMainView = VacMainView::getInstance();
	pMainView->dialogRequest(type, vacType, sectorList, mode);
}

/*
**	FUNCTION
**		Slot activated when 'User manual' item is selected in 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorViewDialog::help(void)
{
}

/*
**	FUNCTION
**		Slot activated when item is selected in main part combo box
**
**	ARGUMENTS
**		name	- name of selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorViewDialog::comboActivated(const QString &name)
{
	DataPool &pool = DataPool::getInstance();
	QStringList sectList;
	pool.findSectorsInMainPart(name.toLatin1(), sectList, true);
	if(sectList.isEmpty())
	{
		bool specPanel = false;
		pool.findSectorsInMainPart(name.toLatin1(), sectList, false);
		for(int idx = 0 ; idx < sectList.count() ; idx++)
		{
			QString sectName = sectList.at(idx);
			Sector *pSector = pool.findSectorData(sectName.toLatin1());
			if(pSector)
			{
				if(!pSector->getSpecSynPanel().isEmpty())
				{
					specPanel = true;
					break;
				}
			}
		}
		if(!specPanel)
		{
			selectInitialMp();
			return;
		}
	}
	if (sectList.isEmpty()) {
		selectInitialMp();
		return;
	}
	Sector *pNewStart = pool.findSectorData(sectList.first().toLatin1()),
			*pNewEnd = pool.findSectorData(sectList.last().toLatin1());
	if((!pNewStart) || (!pNewEnd))
	{
		selectInitialMp();
		return;
	}
	pFirstSector = pNewStart;
	pLastSector = pNewEnd;
	buildView(false);
	setTitle();
}

/*
**	FUNCTION
**		Slot activated when scrollbar(s) are used to scroll synoptic view
**
**	ARGUMENTS
**		x	- New X-coordinate of visible part
**		y	- New Y-coordinate of visible part
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorViewDialog::viewMove(int x)
{
	if(pView)
	{
		pView->setViewStart(x, pScroll->width(),
			pScroll->horizontalScrollBar()->isSliderDown());
	}
}

void SectorViewDialog::resizeEvent(QResizeEvent * /*pEvent */)
{
	if(pView)
	{
		pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
	}
}

