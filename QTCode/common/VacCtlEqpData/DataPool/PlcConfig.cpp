//		Implementation of PlcConfig class
/////////////////////////////////////////////////////////////////////////

#include "PlcConfig.h"
#include "DataPool.h"
#include "Eqp.h"

#include "PlatformDef.h"

#include <QString>
#include <QStringList>

QList<PlcConfig *>	*PlcConfig::pInstanceList = new QList<PlcConfig *>();

void PlcConfig::clear(void)
{
	while(!pInstanceList->isEmpty())
	{
		delete pInstanceList->first();
	}
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

PlcConfig::PlcConfig()
{
	name = NULL;
	pFlanges = new QStringList();
	pSectors = new QStringList();
	// Add itself to list of instances
	pInstanceList->append(this);
}

PlcConfig::~PlcConfig()
{
	delete pSectors;
	delete pFlanges;
	int idx = pInstanceList->indexOf(this);
	if(idx != -1)
	{
		pInstanceList->takeAt(idx);
	}
}

/*
**
**	FUNCTION
**		Create new PlcConfig instance from data read in file, or use these data
**		for previously created instance with the same PLC name.
**
**	PARAMETERS
**		pFile		- Pointer to file being read.
**		lineNo		- Line number in file.
**		tokens		- Pointer to array of tokens in file.
**		nTokens		- Number of tokens.
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**		0	- In case of success;
**		-1	- In case of error.
**
**	CAUTIONS
**		None
*/
int PlcConfig::createFromFile(FILE *pFile, int &lineNo,
	char **tokens, int nTokens, QStringList &errList)
{
	PlcConfig	*pPlc = NULL;

	CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "PlcConfig::createFromFile");

	if(!(pPlc = findPlc(tokens[1])))
	{
		pPlc = new PlcConfig();
		pPlc->name = DataPool::getInstance().addMultiUseString(tokens[1]);
	}
	if(pPlc->parseFile(pFile, lineNo, errList) < 0)
	{
		delete pPlc;
		return -1;
	}
	return 0;
}

/*
**
**	FUNCTION
**		Find instance of CPlcConfig with given PLC name.
**
**	PARAMETERS
**		plcName	- Name of PLC to be found
**
**	RETURNS
**		Pointer to instance with required name, or
**		NULL if such instance was not found
**
**	CAUTIONS
**		None
*/
PlcConfig *PlcConfig::findPlc(const char *plcName)
{
	if(!plcName)
	{
		return NULL;
	}
	for(int idx = 0 ; idx < pInstanceList->count() ; idx++)
	{
		PlcConfig *pPlc = pInstanceList->at(idx);
		if(!strcmp(pPlc->name, plcName))
		{
			return pPlc;
		}
	}
	return NULL;
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errMsg	- Variable where error message shall be written
**					in case of errors.
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int PlcConfig::processLineTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	const QString & /* longValue */, QStringList &errList)
{
	int		n;

	if(!strcmp(tokens[0], "Flange"))	// New flange
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "PlcConfig::ProcessLineTokens");
		if(pFlanges->contains(tokens[1], Qt::CaseInsensitive))
		{
			PARSING_ERR3("Line %d: Duplicating flange <%s> for PLC <%s>", lineNo, tokens[1], name);
		}
		pFlanges->append(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Sector"))	// New sector, sector name can contain spaces ("Unknown sector")
	{
		CHECK_MIN_TOKEN_COUNT(nTokens, 2, lineNo, "PlcConfig::ProcessLineTokens");
		char	sectorName[512];
		sectorName[0] = '\0';
		for(n = 1 ; n < nTokens ; n++)
		{
			if(n > 1)
			{
#ifdef Q_OS_WIN
				strcat_s(sectorName, sizeof(sectorName) / sizeof(sectorName[0]), " ");
#else
				strcat(sectorName, " ");
#endif
			}
#ifdef Q_OS_WIN
			strcat_s(sectorName, sizeof(sectorName) / sizeof(sectorName[0]), tokens[n]);
#else
			strcat(sectorName, tokens[n]);
#endif
		}
		if(pSectors->contains(sectorName))
		{
			PARSING_ERR3("Line %d: Duplicating sector <%s> for PLC <%s>", lineNo, sectorName, name);
		}
		pSectors->append(sectorName);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "EndPlcConfig"))	// End of PLC config
	{
		return 0;
	}
	PARSING_ERR2("Line %d: Unexpected <%s>", lineNo, tokens[0]);
}
