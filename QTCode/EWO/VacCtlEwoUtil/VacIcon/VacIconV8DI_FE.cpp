//	Implementation of VacIconV8DI_FE class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconV8DI_FE.h"

#include "EqpV8DI_FE.h"

#include "FunctionalType.h"
#include "VacEqpTypeMask.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			153,153,255

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconV8DI_FE::VacIconV8DI_FE(QWidget *parent, Qt::WindowFlags f) :
VacIcon(parent, f)
{
	pEqp = NULL;
	QRect labelRect = fontMetrics().boundingRect("Unknown DI Meaning");
	setFixedSize(labelRect.width() + 60, labelRect.height() + 2);
}

VacIconV8DI_FE::~VacIconV8DI_FE()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		It is supposed that this method will not be used for V8DI_FE
*/
VacIconGeometry VacIconV8DI_FE::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(width(), height(), width() >> 1, height() >> 1, 1);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconV8DI_FE::setEqp(Eqp *pEqp)
{
	disconnect();
	if (pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpV8DI_FE"), "VacIconV8DI_FE::setEqp", pEqp->getDpName());
		this->pEqp = (EqpV8DI_FE *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconV8DI_FE::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconV8DI_FE::connect(void)
{
	if (connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("RR2");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconV8DI_FE::disconnect(void)
{
	bool result = false;
	if (connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconV8DI_FE::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if (pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconV8DI_FE::paintEvent(QPaintEvent * /* pEvent */)
{
	QString sDescription = "Unknown DI";
	QString	sValue = "???";
	if (pEqp)
	{
		sDescription = pEqp->getAttrValue("di0Description");
	}
	QColor color(COLOR_UNDEFINED);
	if (pEqp)
	{
		switch (pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			sValue = "N/C";
			break;
		default:
			pEqp->getItlDi0String(sValue, mode);
			break;
		}
		pEqp->getItlDi0Color(color, mode);
	}
	QString label = sDescription + " = " + sValue;
	QPainter painter(this);
	QRect myRect = rect();
	painter.fillRect(myRect, color);
	if (pEqp)
	{
		if (pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
	QRect labelRect = painter.fontMetrics().boundingRect(label);
	painter.drawText(myRect.center().x() - (labelRect.width() >> 1),
		myRect.center().y() + (labelRect.height() >> 1),
		label);
}


