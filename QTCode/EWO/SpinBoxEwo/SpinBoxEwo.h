#ifndef	SPINBOXEWO_H
#define	SPINBOXEWO_H

// Simple EWO INTERFACE FOR QTimeEdit widget

#include <BaseExternWidget.hxx>

#include <qspinbox.h>

class EWO_EXPORT SpinBoxEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	SpinBoxEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void valueChange(int value);
		
private:
	QSpinBox	*baseWidget;
	bool		setting;
};

#endif	// SPINBOXEWO_H
