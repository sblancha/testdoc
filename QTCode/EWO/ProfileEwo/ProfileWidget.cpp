//	Implementation of ProfileWidget class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileWidget.h"
#include "ExtendedLabel.h"


#include "ProfileImage.h"

#include "VacMainView.h"
#include "ResourcePool.h"
#include "DataPool.h"

#include <qlayout.h>
#include <qdesktopwidget.h>
#include <qapplication.h>
#include <qcursor.h>


/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

ProfileWidget::ProfileWidget(QWidget *parent, Qt::WindowFlags f) :
	QWidget(parent, f),
	eqpTypeResource("Profile.BeamVacuumEqpTypes"),
	axisMinResource("Profile.BeamVacuumScaleMin"),
	axisMaxResource("Profile.BeamVacuumScaleMax")
{
	pView = NULL;
	pLabel = NULL;
	showLabel = true;
	dataReadyCalled = false;
}

ProfileWidget::~ProfileWidget()
{
}

void ProfileWidget::dataReady(void)
{
	dataReadyCalled = true;
	buildInitialEqpMask();
	buildLayout();

	/*
	const QList<VacMainView *> &mainList = VacMainView::getInstanceList();
	for(int idx = 0 ; idx < mainList.count() ; idx++)
	{
		VacMainView *pInstance = mainList.at(idx);
		if(pInstance->isPvssEvents())
		{
			QObject::connect(this, SIGNAL(profileDpDown(int, int, int, int, int, const char *)),
				pInstance, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
		}
	}
	*/
	VacMainView *pMainView = VacMainView::getInstance();
	if(pMainView)
	{
		QObject::connect(this, SIGNAL(profileDpDown(int, int, int, int, int, const char *)),
			pMainView, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
	}
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileWidget::buildLayout(void)
{
	if(mainPartName.isEmpty())
	{
		if(startSectorName.isEmpty() || endSectorName.isEmpty())
		{
			errorText = "No main part or start/end sectors";
			update();
			return;
		}
	}

	// 1) Main layout - label (optional) and profile image
	QVBoxLayout *pMainBox = (QVBoxLayout *)layout();
	if(!pMainBox)
	{
		pMainBox = new QVBoxLayout(this);
		pMainBox->setContentsMargins(0, 0, 0, 0);
		pMainBox->setSpacing(0);
	}

	// 2) Label
	if(showLabel)
	{
		QString labelToShow(labelText);
		if(labelToShow.isEmpty())
		{
			if((!startSectorName.isEmpty()) && (!endSectorName.isEmpty()))
			{
				labelToShow = startSectorName;
				labelToShow += " - ";
				labelToShow = endSectorName;
			}
			else
			{
				labelToShow = mainPartName;
			}
		}
		pLabel = new ExtendedLabel(labelToShow, this);
		pLabel->setLabelType(ExtendedLabel::Active);
		pMainBox->addWidget(pLabel);
		pLabel->setAlignment(Qt::AlignHCenter);

		pLabel->show();
	}

	// Findlimits for new profile
	int min = -10, max = -6;
	ResourcePool &resourcePool = ResourcePool::getInstance();
	QString resourceName = axisMinResource;
	resourceName += ".";
	resourceName += mainPartName;
	if(resourcePool.getIntValue(resourceName, min) == ResourcePool::NotFound)
	{
		if(resourcePool.getIntValue(axisMinResource, min) == ResourcePool::NotFound)
		{
			min = -10;
		}
	}
	resourceName = axisMaxResource;
	resourceName += ".";
	resourceName += mainPartName;
	if(resourcePool.getIntValue(resourceName, max) == ResourcePool::NotFound)
	{
		if(resourcePool.getIntValue(axisMaxResource, max) == ResourcePool::NotFound)
		{
			max = -6;
		}
	}

	// Find first and last sector to show
	DataPool &pool = DataPool::getInstance();

	// If both start and end sectors are specified - ranges is built
	// from sectors, otherwise range is build from main part
	Sector *pStartSector = NULL, *pEndSector = NULL;
	if((!startSectorName.isEmpty()) && (!endSectorName.isEmpty()))
	{
		findStartEndFromSectors(pool, &pStartSector, &pEndSector);
	}
	else if(!mainPartName.isEmpty())
	{
		findStartEndFromMainPart(pool, &pStartSector, &pEndSector);
	}

	if(pStartSector && pEndSector)
	{
		pView = ProfileImage::create(this, pStartSector, pEndSector, DataEnum::Online, allEqpMask, errorText);
	}
	else
	{
		pView = NULL;
	}
	if(!pView)
	{
		errorText = "Failed to build profile for ";
		if((!startSectorName.isEmpty()) && (!endSectorName.isEmpty()))
		{
			errorText += "sectors from ";
			errorText += startSectorName;
			errorText += " to ";
			errorText += endSectorName;
		}
		else
		{
			errorText += "main part ";
			errorText += mainPartName;
		}
		update();
		return;
	}
	pView->setMinimumSize(200, 40);
	pMainBox->addWidget(pView, 10);	// Image will consume all free space

	// Prepare and show view
	pView->setLimits(min, max);
	pView->show();
	pView->applyMask(eqpMask);

	// Connect to essential signals of view
	QObject::connect(pView, SIGNAL(dpMouseDown(int, int, int, int, const char *)),
		this, SLOT(dpMouseDown(int, int, int, int, const char *)));

	setMinimumSize(pMainBox->minimumSize());
}

/*
**	FUNCTION
**		Find start and end sectors using specified sector names
**
**	ARGUMENTS
**		pool				- Data pool
**		ppStartSector	- Pointer to start sector will be returned here
**		ppEndSector		- Pointer to end sector will be returned here
**
**	RETURNS
**		None, errors will be written to errorText
**
**	CAUTIONS
**		None
*/
void ProfileWidget::findStartEndFromSectors(DataPool &pool, Sector **ppStartSector, Sector **ppEndSector)
{
	*ppStartSector = pool.findSectorData(startSectorName.toLatin1());
	if(!(*ppStartSector))
	{
		errorText = "Unknown start sector: <";
		errorText += startSectorName;
		errorText += ">";
		update();
		return;
	}
	*ppEndSector = pool.findSectorData(endSectorName.toLatin1());
	if(!(*ppEndSector))
	{
		errorText = "Unknown end sector: <";
		errorText += endSectorName;
		errorText += ">";
		update();
		return;
	}
}

/*
**	FUNCTION
**		Find start and end sectors using specified main part name
**
**	ARGUMENTS
**		pool				- Data pool
**		ppStartSector	- Pointer to start sector will be returned here
**		ppEndSector		- Pointer to end sector will be returned here
**
**	RETURNS
**		None, errors will be written to errorText
**
**	CAUTIONS
**		None
*/
void ProfileWidget::findStartEndFromMainPart(DataPool &pool, Sector **ppStartSector, Sector **ppEndSector)
{
	MainPart *pMainPart = pool.findMainPartData(mainPartName.toLatin1());
	if(!pMainPart)
	{
		errorText = "Unknown main part <";
		errorText += mainPartName;
		errorText += ">";
		update();
		return;
	}

	QStringList sectList;
	pool.findSectorsInMainPart(mainPartName.toLatin1(), sectList, true);
	if(!sectList.count())
	{
		errorText = "No sectors found in main part <";
		errorText += mainPartName;
		errorText += ">";
		update();
		return;
	}

	*ppStartSector = pool.findSectorData(sectList.first().toLatin1());
	if(!(*ppStartSector))
	{
		errorText = "Unknown start sector of main part: <";
		errorText += sectList.first();
		errorText += ">";
		update();
		return;
	}

	*ppEndSector = pool.findSectorData(sectList.last().toLatin1());
	if(!ppStartSector)
	{
		errorText = "Unknown end sector of main part: <";
		errorText += sectList.first();
		errorText += ">";
		update();
		return;
	}
}

/*
**	FUNCTION
**		Build initial equipment type mask
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileWidget::buildInitialEqpMask(void)
{
	// First build mask containing all possible types for profile
	allEqpMask.append(FunctionalType::VGM);
	allEqpMask.append(FunctionalType::VGR);
	allEqpMask.append(FunctionalType::VGP);
	allEqpMask.append(FunctionalType::VGI);
	allEqpMask.append(FunctionalType::VGF);
	allEqpMask.append(FunctionalType::VPI);
	allEqpMask.append(FunctionalType::VPGMPR);
	allEqpMask.append(FunctionalType::VGTR);

	// Then read default settings from resource
	eqpMask = allEqpMask;
	ResourcePool &pool = ResourcePool::getInstance();
	QString resourceName = eqpTypeResource;
	resourceName += ".";
	resourceName += mainPartName;
	if(pool.getEqpTypesMask(resourceName, eqpMask) == ResourcePool::NotFound)
	{
		if(pool.getEqpTypesMask(eqpTypeResource, eqpMask) == ResourcePool::NotFound)
		{
			eqpMask = allEqpMask;
		}
	}
}

/*
**	FUNCTION
**		Slot activated when mouse pointer was pressed on one of devices in profile
**
**	ARGUMENTS
**		button	- Mouse button number
**		mode	- Data acquisition mode
**		x		- Mouse X-coordinate in screen coordinate system
**		y		- Mouse Y-coordinate in screen coordinate system
**		dpName	- Name of DP under mouse pointer
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileWidget::dpMouseDown(int button, int mode, int x, int y, const char *dpName)
{
	emit profileDpDown(button, mode, x, y, 0, dpName);
}


void ProfileWidget::setMainPartName(const QString &name)
{
	if(name.isEmpty())
	{
		return;
	}
	mainPartName = name;
	if(!dataReadyCalled)
	{
		return;
	}
	if(pView)
	{
		delete pView;
	}
	if(pLabel)
	{
		delete pLabel;
	}
	buildLayout();
}

void ProfileWidget::paintEvent(QPaintEvent *event)
{
	QWidget::paintEvent(event);
	if(pView)
	{
		return;
	}
	QString message("Not initialized yet");
	if(!errorText.isEmpty())
	{
		message = errorText;
	}
	QPainter painter(this);
	QRect bound = painter.boundingRect(0, 0, width(), height(), Qt::AlignCenter, message);
	painter.setPen(Qt::black);
	painter.drawText(width() / 2 - bound.width() / 2, height() / 2 - bound.height() / 2,
		message);

}

