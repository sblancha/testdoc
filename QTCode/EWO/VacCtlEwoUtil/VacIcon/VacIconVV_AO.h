#ifndef	VACICONVV_AO_H
#define	VACICONVV_AO_H

//	Icon for VV_AO

#include "VacIcon.h"

class EqpVV_AO;

class VACCTLEWOUTIL_EXPORT VacIconVV_AO : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVV_AO(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVV_AO();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setDirection(VacIconContainer::Direction newDirection);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);


protected:
	// Pointer to my device
	EqpVV_AO *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);

	void drawHorizontal(QPainter &painter);
	void drawVertical(QPainter &painter);
	void drawSymbols(QPainter &painter, bool vertical);
	void drawManual(QPainter &painter, bool vertical);

	void setPainterForControl(QPainter &painter);
	int setPainterForMainImage(QPainter &painter);
};

#endif	// VACICONVV_AO_H
