#ifndef	EQPVRCG_H
#define	EQPVRCG_H

//	Class EqpVRCG - TPG300, not controllable, just have to implement method getSlaves()

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVRCG : public Eqp
{
public:
	EqpVRCG(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVRCG(const EqpVRCG &src) : Eqp(src) {}
#endif
	~EqpVRCG() {}

	virtual void getSlaves(QStringList &slaves);
	virtual void postProcess(void);
};

#endif	// EQPVRCG_H
