/** @defgroup gr_vcsMobileWL vcsMobileWL.ctl
vcsMobileWL.ctl is a scritp to activation or deactivation of wireless mobile equipment. \n
Two types of mobile device are remote controlled : \n
  - Pumping group station \n
  - Bake-out Cabinet: Bake-out control system \n
@paragraph
SIM card data points are permanently connected to the PLC Server through the S7 driver. \n
The script is connected to the SIM card data points. \n 
In case of new mobile?s connection detected, script gets the type of the mobile (pumping group or bake-out cabinet) 
and the position from the SIM card data point. \n
It builds three lists of data points to link SIM card with the correct master data point (pumping group or bake-out cabinet) 
and with mobile?s functional position data point. \n 
Then the script copy the configurations of the master data point to the functional position data point. \n 
Configurations are peripheral addresses, alert handling settings?  \n
Finally, the mobile equipment class instance of the functional position is activated in the shared libraries data pool. 
*/
#uses "VacCtlEqpDataPvss"	    // C++ CTRL Library
#uses "VacCtlImportPvss"	    // C++ CTRL Library
#uses "vclGeneric.ctl"         // WCCOA CTRL Library
#uses "vclMobile.ctl"	       // WCCOA CTRL Library
#uses "vclMachine.ctl"	       // WCCOA CTRL Library
#uses "vclDeviceFileParser.ctl"// WCCOA CTRL Library
#uses "vclSectors.ctl"	       // WCCOA CTRL Library
#uses "vclDevCommon.ctl"	    // WCCOA CTRL Library
#uses "vclAddressConfig.ctl"   // WCCOA CTRL Library

/// @brief Communication DP Type
const string COMMUNICATION_DP_TYPE = "V_TCP";
/// @brief equipement code dpe name
const string EQP_CODE_DPE_NAME = ".eqpCode";
/// @brief GeoPort name without prefix
const string GEO_PORT_NAME = "GeoPort_";
/// @brief GeoPort prefix for pumping group
const string VPG_GEOPORT_PREFIX = "vpg";  
/// @brief GeoPort prefix for bakeout rack
const string VRE_GEOPORT_PREFIX = "vre";  

/// @brief Internal DP with the list of wireless position instance DP name that are actived
const string WL_ACTIVE_DP_LIST = "_VacWlDpList";
const string WL_EWO_CONNECTED_DP_LIST = "_VacWlActiveDpList";

/// @brief Connection ID Write register dpe name 
const string CONN_ID_WR = "wrConnId";
/// @brief SCADA order ID Write register dpe name 
const string ORDER_ID_WR = "wrOrderId";
/// @brief SCADA order ID Read register dpe name 
const string CONN_ID_RR = "connId";


/// @brief Code to send to FE through WR1 to notify eqp is activated in SCADA 
const unsigned SCADA_ACTIVATED_WR1 = 0x0400u;

/// @brief Code to send to FE through WR1 to notify eqp is deactivated in SCADA 
const unsigned SCADA_DEACTIVATED_WR1 = 0x0800u;

/// @brief Global to calculate order ID -- NOT USED
int G_orderId = 0;

/// @brief Global to proceed with a clear/deactivation of all WL mobiles
bool G_isClearRequested = FALSE;

/// @brief List of TCP DPs connected
dyn_string		  actTcpDps;
/// @brief Connection status for active TCP connection
dyn_int			  iTcpConnSts;
/// @brief Equipment type of mobile connected to TCP (see vclMobile.ctl)
dyn_uint			  uTcpConnEqpTypes;
/// @brief Master DP names for all active TCP connection
dyn_string		  masterDps;
/// @brief Eqp position instance DP names for all active TCP connection
dyn_string		  instanceDps;


/// @brief List of all TCP communication DPs
dyn_string		  allTcpDps;
/// @brief Flag indicating if initalization is still in progress
bool				  isInit;
/// @brief Flag indicating if TCP list DP has been changed
bool				  isActTcpDpsChanged;
/// @brief Queue of data to be processed
dyn_dyn_anytype  queue;
/// @brief Flag to enable debugging output
const bool		  isDebugFlag = true;

/**
@brief Infinite loop process to activate or deactivate wireless mobile equipment.
@copydoc vcsMobileWL
*/                              
main()
{
  dyn_string		exceptionInfo;
  dyn_errClass	   err;
  string          errMessage; 
  int             ret; //return value of function (0 is OK)
  DebugTN("vcsMobileWl.ctl: ----- Wireless Connection Script Starting V2019-10-15");
  
  // Init DLL/SO data pools
  ret = InitDynLibStaticDataPools();
  if(ret != 0) {
      errMessage = "vcsMobileWl.ctl: +++++ FATAL: InitDynLibStaticDataPools() failed: script stopped";
      err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return;
  }
  // --
  // Create internal DP as container of actived wireless mobile dps (if not exists)
  if(! dpExists(WL_ACTIVE_DP_LIST)) {
    dpCreate(WL_ACTIVE_DP_LIST, WL_EWO_CONNECTED_DP_LIST);
    err = getLastError();
    if(dynlen(err) > 0) {
      errMessage = "vcsMobileWl.ctl: +++++ FATAL: dpCreate(" + WL_ACTIVE_DP_LIST + ", " + WL_ACTIVE_DP_LIST + ") failed";
      err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return;
    }
    if(! dpExists(WL_ACTIVE_DP_LIST)) {
      errMessage = "vcsMobileWl.ctl: +++++ FATAL: DP <" + WL_ACTIVE_DP_LIST + "> was not created";
      err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return;
    }
  }
  // Create internal EWO Connected DP as container of actived wireless mobile dps (if not exists)
  if(! dpExists(WL_EWO_CONNECTED_DP_LIST)) {
    dpCreate(WL_EWO_CONNECTED_DP_LIST, WL_EWO_CONNECTED_DP_LIST);
    err = getLastError();
    if(dynlen(err) > 0) {
      errMessage = "vcsMobileWl.ctl: +++++ FATAL: dpCreate(" + WL_EWO_CONNECTED_DP_LIST + ", " + WL_EWO_CONNECTED_DP_LIST + ") failed";
      err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return;
    }
    if(! dpExists(WL_EWO_CONNECTED_DP_LIST)) {
      errMessage = "vcsMobileWl.ctl: +++++ FATAL: DP <" + WL_EWO_CONNECTED_DP_LIST + "> was not created";
      err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return;
    }
  }
  // --
  // Clean active/connected list in internal DP
  dynClear(actTcpDps);
  dynClear(iTcpConnSts);
  dynClear(uTcpConnEqpTypes);   
  dynClear(masterDps); 
  dynClear(instanceDps);   
  ret = SetDataWlActiveDpList(actTcpDps, iTcpConnSts, uTcpConnEqpTypes, masterDps, instanceDps);    

  // Get list of all available V_TCP Front End communication DPs
  allTcpDps = dpNames("*", COMMUNICATION_DP_TYPE);
  err = getLastError();
  if(dynlen(err) > 0) {
    throwError(err);
    errMessage = "vcsMobileWl.ctl: +++++ FATAL: dpNames(\"*\", " + COMMUNICATION_DP_TYPE + ") failed:";
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return;
  }
  
  // dpConnect all TCP: Call back function: TcpDpConnectCB() 
  if(dynlen(allTcpDps) == 0) {
    errMessage = "vcsMobileWl.ctl: +++++ FATAL: no <" + COMMUNICATION_DP_TYPE + "> in the system, exiting...";
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return;
  }
  isInit = true; 
  isInit = false;  // TO DO REMOVE
  isActTcpDpsChanged = false;
  if(isDebugFlag) {
    DebugTN("vcsMobileWL.ctl: ----- " + dynlen(allTcpDps) + " TCP DPs");
  }
  for(int n = 1 ; n <= dynlen(allTcpDps) ; n++) {
    string dpName = dpSubStr(allTcpDps[n], DPSUB_DP);
    if(isDebugFlag) {
      DebugTN("vcsMobileWL.ctl: ----- Connecting to # " + n);
    }
    dpConnectUserData("TcpDpConnectCB", allTcpDps[n], TRUE,
      allTcpDps[n] + ".RR1:_online.._value",
      allTcpDps[n] + ".typeId:_online.._value",
      allTcpDps[n] + ".positionId:_online.._value");
  }
    
  int ewoConnectWlTimer = 0;   
  // Infinite LOOP: process data from queue if queue is not empty, process done by function ProcessCbData()
  while(true) {
    //First check if clear is required
    if (isInit) {
      ClearInstanceDP(exceptionInfo); 
      isInit = FALSE; 
    }
    if(dynlen(queue) > 0) {
      if(isDebugFlag) {
        DebugTN("vcsMobileWL.ctl: Processing " + dynlen(queue) + " events from queue");
      }
    }
    while(dynlen(queue) > 0) {
      //First check if clear is required
      if (G_isClearRequested) {
        dynClear(queue);
        ClearInstanceDP(exceptionInfo); 
        G_isClearRequested = FALSE;
        break;
      }
      if(isDebugFlag) {
        DebugTN("vcsMobileWL.ctl: queue length = " + dynlen(queue));
      }
      //Process the first data row in queue built by call back TcpDpConnectCB
      //queue[x][1] : dp pure name
      //queue[x][2] : typeId
      //queue[x][3] : positionId
      //queue[x][4] : machineId
      //queue[x][5] : State  
      ret = ProcessCbData(queue[1][1], queue[1][2], queue[1][3], queue[1][4], queue[1][5]);
      if (ret == 0)      
        dynRemove(queue, 1); //remove data processed from queue if processCbData() successfull
      else {
        errMessage = "vcsMobileWL.ctl: ProcessCbData() failed for dp :" + queue[1][1];
        err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
        throwError(err);
      }
        
    }
    ewoConnectWlTimer ++;
    if(ewoConnectWlTimer > 60) {
      ewoConnectWlTimer = 0;
      if(isDebugFlag) {
        DebugTN("vcsMobileWL.ctl: Check Ewo Dp List need to be updated");
      }
      CopyDpListToDpEwoList();   
    }  
    delay(1);    
  }
  //DebugTN("vcsMobileWl.ctl: +++++ End of script"); 
}

/**
@brief Get WireLess active DP data from internal datapoint list, return 0 if successfull
@param[out]  tcpDpNames           Communication TCP DP names         
@param[out]  tcpConnSts           Connection status for active TCP connection
@param[out]  tcpConnEqpTypes      Equipment type of mobile connected to TCP (see vclMobile.ctl)
@param[out]  masterDpNames        Master DP names for all active TCP connection    
@param[out]  posInstanceDpNames   Eqp position instance DP names for all active TCP connection   
*/
int GetDataWlActiveDpList(dyn_anytype &tcpDpNames, dyn_anytype &tcpConnSts, dyn_anytype &tcpConnEqpTypes,dyn_anytype &masterDpNames,dyn_anytype &posInstanceDpNames) {
    
  dpGet(
      WL_ACTIVE_DP_LIST + ".tcpDpNames", tcpDpNames,
      WL_ACTIVE_DP_LIST + ".tcpConnSts", tcpConnSts,
      WL_ACTIVE_DP_LIST + ".tcpConnEqpTypes", tcpConnEqpTypes,
      WL_ACTIVE_DP_LIST + ".masterDpNames", masterDpNames,
      WL_ACTIVE_DP_LIST + ".posInstanceDpNames", posInstanceDpNames);
  bool isDynlenCoherent = TRUE;
  if (dynlen(tcpDpNames) != dynlen(tcpConnSts))
    isDynlenCoherent = FALSE;
  if (dynlen(tcpDpNames) != dynlen(tcpConnEqpTypes))
    isDynlenCoherent = FALSE;
  if (dynlen(tcpDpNames) != dynlen(masterDpNames))
    isDynlenCoherent = FALSE;
  if (dynlen(tcpDpNames) != dynlen(posInstanceDpNames))
    isDynlenCoherent = FALSE;
  
  if (isDynlenCoherent)
    return 0;
  else {
    string errMessage = "vcsMobileWL.ctl: +++++ FATAL: dpe dynlen of "+ WL_ACTIVE_DP_LIST +" are different";
    dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return -1;  
  }
}

/**
@brief Set WireLess active DP data to internal datapoint list, return 0 if successfull
@param[in]  tcpDpNames           Communication TCP DP names         
@param[in]  tcpConnSts           Connection status for active TCP connection
@param[in]  tcpConnEqpTypes      Equipment type of mobile connected to TCP (see vclMobile.ctl)
@param[in]  masterDpNames        Master DP names for all active TCP connection    
@param[in]  posInstanceDpNames   Eqp position instance DP names for all active TCP connection   
*/
int SetDataWlActiveDpList(dyn_anytype tcpDpNames, dyn_anytype tcpConnSts, dyn_anytype tcpConnEqpTypes,dyn_anytype masterDpNames,dyn_anytype posInstanceDpNames) {

  bool isDynlenCoherent = TRUE;
  if (dynlen(tcpDpNames) != dynlen(tcpConnSts))
    isDynlenCoherent = FALSE;
  if (dynlen(tcpDpNames) != dynlen(tcpConnEqpTypes))
    isDynlenCoherent = FALSE;
  if (dynlen(tcpDpNames) != dynlen(masterDpNames))
    isDynlenCoherent = FALSE;
  if (dynlen(tcpDpNames) != dynlen(posInstanceDpNames))
    isDynlenCoherent = FALSE;
  
  if (!isDynlenCoherent) {
    string errMessage = "vcsMobileWL.ctl: +++++ FATAL: dynlen to be set to "+ WL_ACTIVE_DP_LIST +" are different";
    dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return -1;  
  }
  
  dpSetWait(
      WL_ACTIVE_DP_LIST + ".tcpDpNames", tcpDpNames,
      WL_ACTIVE_DP_LIST + ".tcpConnSts", tcpConnSts,
      WL_ACTIVE_DP_LIST + ".tcpConnEqpTypes", tcpConnEqpTypes,
      WL_ACTIVE_DP_LIST + ".masterDpNames", masterDpNames,
      WL_ACTIVE_DP_LIST + ".posInstanceDpNames", posInstanceDpNames);
  dyn_errClass err = getLastError();    
  if(dynlen(err) > 0) {
    throw(err);
    errMessage = "vcsMobileWL.ctl: dpSet " + WL_ACTIVE_DP_LIST + " error : ";
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);      
    return -1;
  }  
  return 0;
}

/**
@brief Delete row of data in WL_ACTIVE_DP_LIST internal DP, return 0 if successfull
@param[in]  tcpDpName           Communication TCP DP name    
*/
int DelRowWlActiveDpList(anytype tcpDpName){ 
  int ret = -1;
  dyn_anytype tcpDpNames, dps2, dps3, dps4, dps5;
  ret = GetDataWlActiveDpList(tcpDpNames, dps2, dps3, dps4, dps5);
  if (ret!=0) {
    string errMessage = "vcsMobileWL.ctl: +++++ FATAL: error in DelRowWlActiveDpList()";
    dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return -1;  
  }  
  dyn_anytype tcpDpNamesNew, dps2New, dps3New, dps4New, dps5New;
  for (int n = 1; n <= dynlen(tcpDpNames); n++) {
    if (tcpDpName != tcpDpNames[n]) {
      dynAppend(tcpDpNamesNew, tcpDpNames[n]);
      dynAppend(dps2New, dps2[n]);
      dynAppend(dps3New, dps3[n]);
      dynAppend(dps4New, dps4[n]);
      dynAppend(dps5New, dps5[n]);
    }  
  }
  ret = SetDataWlActiveDpList(tcpDpNamesNew, dps2New, dps3New, dps4New, dps5New);
  if (ret!=0) {
    string errMessage = "vcsMobileWL.ctl: +++++ FATAL: error in DelRowWlActiveDpList()";
    dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
  }
  return ret;  
}

/**
@brief Insert row for new activation in internal datapoint list, return 0 if successfull
@param[in]  tcpDpPureName        Communication TCP DP name         
@param[in]  state                Connection status
@param[in]  typeId               Equipment type of mobile connected to TCP (see vclMobile.ctl)
@param[in]  masterDpPureName     Master DP name for all active TCP connection    
@param[in]  instanceDpName       Eqp position instance DP name  
*/
int InsertRowWlActiveDpList(string tcpDpPureName, unsigned state, unsigned typeId, string masterDpPureName, string instanceDpName){ 
  int ret = -1;
  dyn_anytype dps1, dps2, dps3, dps4, dps5;
  ret = GetDataWlActiveDpList(dps1, dps2, dps3, dps4, dps5);
  if (ret!=0) {
    string errMessage = "vcsMobileWL.ctl: +++++ FATAL: error in InsertRowWlActiveDpList()";
    dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return -1;  
  }  
  dynAppend(dps1, tcpDpPureName);
  dynAppend(dps2, state);
  dynAppend(dps3, typeId);
  dynAppend(dps4, masterDpPureName);
  dynAppend(dps5, instanceDpName);
  ret = SetDataWlActiveDpList(dps1, dps2, dps3, dps4, dps5);
  if (ret!=0) {
    string errMessage = "vcsMobileWL.ctl: +++++ FATAL: error in InsertRowWlActiveDpList()";
    dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
  }
  return ret;  
}


/**
@brief Clear instanceDP with no connection
*/
void ClearInstanceDP(dyn_string &exceptionInfo) {
  int ret; 
  ret = GetDataWlActiveDpList(actTcpDps, iTcpConnSts, uTcpConnEqpTypes, masterDps, instanceDps);
  dyn_string mobileInstanceDpNames;
  dynClear(mobileInstanceDpNames);
  AppendDpNamesMobileTypeFiltered(mobileInstanceDpNames, VPG_DP_TYPE, VAC_MOBILE_TYPE_POSINSTANCE ,exceptionInfo);      
  AppendDpNamesMobileTypeFiltered(mobileInstanceDpNames, VRER_DP_TYPE, VAC_MOBILE_TYPE_POSINSTANCE ,exceptionInfo);
  //DebugTN("vcsMobileWL.ctl: Clear: mobileInstanceDpNames : ", mobileInstanceDpNames);
  //Check if this list is in active dp list if not reset RR1 and deactive addr config
  for (int n = dynlen(mobileInstanceDpNames); n > 0; n--) {
    bool isConnected = FALSE;        
    for (int i = dynlen(instanceDps); i >0; i--) {
      if (mobileInstanceDpNames[n] ==  instanceDps[i]) {
        isConnected = TRUE;
        break;
      }
    }
    if (!isConnected) {
      //set addr config not active
      DebugTN("vcsMobileWL.ctl: Clear: mobile Dp Names not connected to disactivate : ", mobileInstanceDpNames[n]);
      SetAddrActiveConfig (mobileInstanceDpNames[n], FALSE, exceptionInfo);
      //Reset RR1
      dpSet( mobileInstanceDpNames[n] + ".RR1", 0);
      //deactive alarm handling
      int alertType = 0;      
      dpGet(mobileInstanceDpNames[n] + ".AL1:_alert_hdl.._type", alertType);
      if (alertType != 0) {
        dpSet(mobileInstanceDpNames[n] +".AL1:_alert_hdl.._active", FALSE);
      }
    } 
  }
}

/**
@brief CALL BACK FUNCTION Queue the processes when new value from TCP datapoints arrived
@param[in]  dpTypeId      Name of dp1 dpConnect 2nd argument
@param[in]  typeId        Value of dp1
@param[in]  dpPositionId  Name of dp2 dpConnect 3rd argument
@param[in]  positionId    Value of dp2
@param[in]  dpMachineId   Name of dp3 dpConnect 4th argument
@param[in]  machineId     Value of dp3
@param[in]  dpState       Name of dp4 dpConnect 5th argument
@param[in]  State         Value of dp4
*/
void TcpDpConnectCB(string dpTcpName,
                    string dpState, unsigned State,
                    string dpTypeId, unsigned typeId,
                    string dpPositionId, unsigned positionId)
{
  unsigned machineId;
  dpGet(dpTcpName + ".machineId:_online.._value", machineId);
  if(isDebugFlag) {
    DebugTN("vcsMobileWL.ctl: ++++++++++ Call back append Queue");
    DebugTN("vcsMobileWL.ctl: machineId:", machineId);
    DebugTN("vcsMobileWL.ctl: typeId:", typeId);
    DebugTN("vcsMobileWL.ctl: positionId:", positionId);
  }  
  dynAppend(queue, makeDynAnytype((string)dpSubStr(dpTypeId, DPSUB_DP),
    (unsigned)typeId, (unsigned)positionId, (unsigned)machineId, (unsigned)State));
}

/**
@brief Process new data from callback, return 0 if successfull
@param[in]  dpPureName    TCP dp pure name
@param[in]  typeId        Mobile type code
@param[in]  positionId    Mobile position code
@param[in]  machineId     Machine code (where the mobile is connected)
@param[in]  State         Mobile communication state
*/
int ProcessCbData(string dpPureName, unsigned typeId, 
                   unsigned positionId, unsigned machineId, unsigned State) {
  int ret = 0;  
  if(isDebugFlag) {
    DebugTN("vcsMobileWL.ctl: ++++++++++ ProcessCallbackData()",
      dpSubStr(dpPureName, DPSUB_DP_EL), " type= " + typeId, " pos= " + positionId, " state= " + State);
  }

  // Action according to State
  //DebugTN("----------------------------------STATE :", State);
  //DebugTN("----------------------------------MASK :", FE_RR1_MOBILE_READY_MASK);
  if (State & FE_RR1_MOBILE_READY_MASK) { // Communication status reported from Front End is READY = Front End fully connected
    ret = ActivateConnection(dpPureName, typeId, positionId, machineId, State);
    if(isDebugFlag) {    
      DebugTN("vcsMobileWL.ctl: ------ ActivateConnection() finished :" + dpPureName + " Ret error:", ret);
    }
    if ( ret !=0 ) { // Clear all WL mobiles
      G_isClearRequested = TRUE;
    }
  }
  else { // Communication status reported from Front End is NOT READY = Front End not fully connected
    ret = DeactivateConnection(dpPureName);
    if(isDebugFlag) {
      DebugTN("vcsMobileWL.ctl: ------ DeactivateConnection() finished :", dpPureName);
    }
  }
  
  if(isDebugFlag) {
    DebugTN("vcsMobileWL.ctl: ------ ProcessCbData() finish", dpPureName);
  }
  return ret;
}

/**
@brief Copy Dpe config from masterDp to instanceDp, active periph address and add dp in WL_ACTIVE_DP_LIST
@param[in]  tcpDpPureName    TCP dp pure name
@param[in]  typeId        Mobile type code
@param[in]  positionId    Mobile position code
@param[in]  machineId     Machine code (where the mobile is connected)
@param[in]  state         Mobile communication state
*/
int ActivateConnection(string tcpDpPureName, unsigned typeId, 
                        unsigned positionId, unsigned machineId, unsigned state) {
  int ret = 0;  
  dyn_string	  exceptionInfo;
  ret = GetDataWlActiveDpList(actTcpDps, iTcpConnSts, uTcpConnEqpTypes, masterDps, instanceDps);
  for( int n = dynlen(actTcpDps); n > 0; n--) {   
    if( tcpDpPureName == actTcpDps[n] ) {
      if(isDebugFlag) {
          DebugTN("vcsMobileWL.ctl: ------ " + tcpDpPureName + " already connected, only send SCADA Ready");
      }
      unsigned connId =0;   
      //dpset V_TCP WR1 to notify Mobile is activated in SCADA
      dpGet(tcpDpPureName + "." + CONN_ID_RR , connId);
      dpSet(tcpDpPureName + "." + CONN_ID_WR, connId); 
      G_orderId ++;
      dpSet(tcpDpPureName + "." + ORDER_ID_WR, G_orderId);
      dpSet(tcpDpPureName + ".WR1", SCADA_ACTIVATED_WR1); 
      return 0;
    }
  }
  dyn_string  masterDps;
  dyn_string  allMasterTypeDps;
  dyn_errClass err;
  string errMessage;
  bool	 isBakeoutRack; // Mobile connected is a bakeout rack
  if(positionId == 0) {
    if(isDebugFlag) {
      DebugTN("vcsMobileWL.ctl: ActivateConnection : positionId is 0");
    }   
    return ret;
  }
  if(typeId == 0) {
    if(isDebugFlag) {
      DebugTN("vcsMobileWL.ctl: ActivateConnection : typeId is 0");
    }   
    return ret;
  }
    
  // Find Mobile PLC from TCP
  string mobilePlcDpName = "";
  //DebugTN("tcpDpPureName = ", tcpDpPureName);
  LhcVacGetAttribute(tcpDpPureName, "PLC", mobilePlcDpName);
  //DebugTN("mobilePlcDpName = ", mobilePlcDpName);  

  // Find dps master from TCP
  LhcVacGetByAttrValue("PLC", tcpDpPureName, masterDps);  
  if(isDebugFlag) {  
    DebugTN("vcsMobileWL.ctl: --- masterDps = ", masterDps);  
    DebugTN("typeId = ", typeId);
    DebugTN("positionId = ", positionId);
  }   
  // Find unique masterDp and geoPortPrefix dedicated to eqp type
  string masterDpPureName = "";
  string geoPortPrefix = "";  
  switch ((int) typeId) {
    case VAC_MOBILE_EQP_TYPE_VPGMA:
    case VAC_MOBILE_EQP_TYPE_VPGMB:
    case VAC_MOBILE_EQP_TYPE_VPGMC:
    case VAC_MOBILE_EQP_TYPE_VPGMD:
    case VAC_MOBILE_EQP_TYPE_VPGME:
      for (int n = dynlen(masterDps); n > 0; n--) {
        if (dpTypeName(masterDps[n]) == VPG_DP_TYPE)
          masterDpPureName = masterDps[n];       
      }
      if(isDebugFlag) { 
        DebugTN("vcsMobileWL.ctl: --- VP_GU masterDp = ", masterDpPureName); 
      } 
      geoPortPrefix = VPG_GEOPORT_PREFIX;
		dpSet( masterDpPureName + EQP_CODE_DPE_NAME, typeId); 	
      isBakeoutRack = false; // Mobile connected is not a bakeout rack      
      break;
    case VAC_MOBILE_EQP_TYPE_BAKEOUT_RACK:
    case VAC_MOBILE_EQP_TYPE_VREMA:
    case VAC_MOBILE_EQP_TYPE_VREMB:
      isBakeoutRack = true; // Mobile connected is a bakeout rack
		    for (int n = dynlen(masterDps); n > 0; n--) {
        if (dpTypeName(masterDps[n]) == VRER_DP_TYPE)
          masterDpPureName = masterDps[n];       
      }
      if(isDebugFlag) { 
        DebugTN("vcsMobileWL.ctl: --- VR_ER masterDp = ", masterDpPureName);
      }  
      geoPortPrefix = VRE_GEOPORT_PREFIX;
      break;
    default:
      errMessage = "vcsMobileWL.ctl: ActivateConnection() Equipment type unknown :" + typeId;
      err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return 0;    
  }  
  
  // dpSet masterDp (.eqpCode)
  //DebugTN("masterDpPureName = ", masterDpPureName);
  if (masterDpPureName == "") {
    errMessage = "vcsMobileWL.ctl: FATAL: No master Dp found!";
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return 0;
  }
  // Find instanceDp
  string instanceDpName = "";
  if(isDebugFlag) {  
    DebugTN("attr name : ", geoPortPrefix + GEO_PORT_NAME + positionId);
  }
  LhcVacGetAttribute(mobilePlcDpName, geoPortPrefix + GEO_PORT_NAME + positionId, instanceDpName);
  if (instanceDpName == "") {
     errMessage = "vcsMobileWL.ctl: ActivateConnection() No instance Dp Name found!";
     err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
     throwError(err);    
     return 0;
	}
	// Set eqp type only for VPG
  if ( !isBakeoutRack ) { // Mobile is VPG
    dpSetWait( masterDpPureName + EQP_CODE_DPE_NAME, typeId);     
    dpSetWait(instanceDpName + EQP_CODE_DPE_NAME, typeId);
    err = getLastError();    
    if(dynlen(err) > 0) {
      throw(err);
      errMessage = "vcsMobileWL.ctl: set eqp type error : " + instanceDpName;
      err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);      
      return 0;
    }
  } 
  // Set Alarm handling
  int configType = 0;  
  dpGet(instanceDpName +".AL1:_alert_hdl.._type", configType);
  if(configType == 0) {
    dpSetWait(instanceDpName +".AL1:_alert_hdl.._type", DPCONFIG_ALERT_BINARYSIGNAL,
              instanceDpName +".AL1:_alert_hdl.._class", "alert.",
              instanceDpName +".AL1:_alert_hdl.._text0", "Ok",
              instanceDpName +".AL1:_alert_hdl.._text1", "Alert",
              instanceDpName +".AL1:_alert_hdl.._orig_hdl", FALSE);
    err = getLastError();    
    if(dynlen(err) > 0) {  
      throw(err);
      errMessage = "vcsMobileWL.ctl: set alarm handling type error : " + instanceDpName;
      err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);      
    }
  }
  dpSetWait(instanceDpName +".AL1:_alert_hdl.._active", TRUE);
  err = getLastError();  
  if(dynlen(err) > 0) {  
    throw(err);
    errMessage = "vcsMobileWL.ctl: active alarm handling error : " + instanceDpName;
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);      
  }
  // Remove active WR1 Periph Addr of Master
  dpSet(masterDpPureName + ".WR1" + ":_address.._active", FALSE);

  // Copy config from masterDp to instanceDp
  CopyAddrConfigAllDpe(masterDpPureName, instanceDpName, exceptionInfo); 
  //DebugTN("vcsMobileWL.ctl: dpCopy ret: " + ret);   
  err = getLastError();    
  if(dynlen(err) > 0) { 
    throw(err);
    errMessage = "vcsMobileWL.ctl: dpCopy error : " + masterDpPureName + " to " + instanceDpName;
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);       
    return 0;
  }
  //DebugTN("vcsMobileWL.ctl: dpCopy : " + masterDpPureName + " to " + instanceDpName);   
  // Active periph Address of instance DP
  SetAddrActiveConfig (instanceDpName, TRUE, exceptionInfo);
  err = getLastError();    
  if(dynlen(err) > 0) {  
    throw(err);
    errMessage = "vcsMobileWL.ctl: +++++++ FATAL: ADDRESS CONFIG SET ACTIVE ERROR!! : " + instanceDpName;
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);    
    return -1; // if set addr is not working this may mean disconnection of instanceDpName has not been properly done and it need a clean up
  }
	// --- ACTIVATE DP OF CHILDS ONLY FOR BAKEOUT RACK (VR_EC or VR_EA)  
  if(isBakeoutRack) {  
    string childMasterDpName;
    string childInstanceDpName;
    string sZero = "0"; 
    // VR_EC: bakeout regulation channel  
    for( int i = 1; i < 25; i++) {
      if( i > 9 ) sZero = ""; 
      childMasterDpName =  masterDpPureName + "_C" + sZero + (string)i;     
      childInstanceDpName = instanceDpName + "_C" + sZero + (string)i;
      // Copy config from masterDp to instanceDp
      CopyAddrConfigAllDpe(childMasterDpName, childInstanceDpName, exceptionInfo); 
      //DebugTN("vcsMobileWL.ctl: dpCopy ret: " + ret);
      //DebugTN("-----------------childMasterDpName: ", childMasterDpName);    
      //DebugTN("-----------------childInstanceDpName: ", childInstanceDpName);  
      err = getLastError();    
      if(dynlen(err) > 0) {   
        throw(err);
        errMessage = "vcsMobileWL.ctl: dpCopy error : " + childMasterDpName + " to " + childInstanceDpName;
        err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
        throwError(err);       
        return 0;
      }
      //DebugTN("vcsMobileWL.ctl: dpCopy : " + childMasterDpName + " to " + childInstanceDpName);   
      // Active periph Address of instance DP
      SetAddrActiveConfig (childInstanceDpName, TRUE, exceptionInfo);
    }
    // VR_EA: bakeout alarm channel  
    for( int i = 25; i < 33; i++) {
      childMasterDpName =  masterDpPureName + "_A" + i;     
      childInstanceDpName = instanceDpName + "_A" + i;
      // Copy config from masterDp to instanceDp
      CopyAddrConfigAllDpe(childMasterDpName, childInstanceDpName, exceptionInfo); 
      //DebugTN("vcsMobileWL.ctl: dpCopy ret: " + ret);   
      err = getLastError();    
      if(dynlen(err) > 0) {  
        throw(err);
        errMessage = "vcsMobileWL.ctl: dpCopy error : " + childMasterDpName + " to " + childInstanceDpName;
        err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
        throwError(err);    
        return 0;
      }
      //DebugTN("vcsMobileWL.ctl: dpCopy : " + childMasterDpName + " to " + childInstanceDpName);   
      // Active periph Address of instance DP
      SetAddrActiveConfig (childInstanceDpName, TRUE, exceptionInfo);
    }
  }
  // Update WL_ACTIVE_DP_LIST to set eqp active and showed it on synoptic
  InsertRowWlActiveDpList(tcpDpPureName, state, typeId, masterDpPureName, instanceDpName); 
  
  // Send back FE notification mobile is activated in SCADA  
  unsigned uConnId =0;   
  if (ret == 0) {
    //dpset V_TCP WR1 to notify Mobile is activated in SCADA
    dpGet(tcpDpPureName + "." + CONN_ID_RR , uConnId);
    dpSet(tcpDpPureName + "." + CONN_ID_WR, uConnId); 
    G_orderId ++;
    dpSet(tcpDpPureName + "." + ORDER_ID_WR, G_orderId);
    dpSet(tcpDpPureName + ".WR1", SCADA_ACTIVATED_WR1);
  }
  if(isDebugFlag) {
    DebugTN("vcsMobileWl.ctl: ----- " + masterDpPureName + " connected to ", instanceDpName);  
  }
  return ret;
}    

/**
@brief Set not active periph address and delete dp in WL_ACTIVE_DP_LIST
@param[in]  tcpDpPureName    TCP dp pure name
*/
int DeactivateConnection(string tcpDpPureName) {
  int ret = 0;  
  dyn_string	exceptionInfo;
  if(isDebugFlag) {
    DebugTN("vcsMobileWL.ctl: TCP Dp deactivation procedure : " + tcpDpPureName);
  }   
  //Find masterDP connected to this TCP
  ret = GetDataWlActiveDpList(actTcpDps, iTcpConnSts, uTcpConnEqpTypes, masterDps, instanceDps);
  int tcpFindNb = 0;
  int rowIndexToDel = 0;
  string instanceDpToDeact = "";  
  for (int n = 1; n <= dynlen(actTcpDps); n++) {
    if (actTcpDps[n] == tcpDpPureName) {
      tcpFindNb ++;
      rowIndexToDel = n;
      instanceDpToDeact = instanceDps[n];
      if(isDebugFlag) {
        DebugTN("vcsMobileWL.ctl: Mobile Dp to deactivate : " + instanceDpToDeact);
      }   
      //Set not active periph address
      SetAddrActiveConfig (instanceDpToDeact, FALSE, exceptionInfo);
      //Reset RR1
      dpSet(instanceDpToDeact + ".RR1", 0);
      //deactive alarm handling
      dpSet(instanceDpToDeact +".AL1:_alert_hdl.._active", FALSE);
      //Reset Childs if bake-out
      if (dpTypeName(instanceDpToDeact) == VRER_DP_TYPE) {      
        string childInstanceDpName;
        string sZero = "0"; 
        // VR_EC: bakeout regulation channel  
        for( int i = 1; i < 25; i++) {
          if( i > 9 ) sZero = ""; 
          childInstanceDpName = instanceDpToDeact + "_C" + sZero + (string)i;
          //Set not active periph address
          SetAddrActiveConfig (childInstanceDpName, FALSE, exceptionInfo);
          //Reset RR1
          dpSet(childInstanceDpName + ".RR1", 0);
        }
        // VR_EA: bakeout alarm channel  
        for( int i = 25; i < 33; i++) {
          childInstanceDpName = instanceDpToDeact + "_A" + i;
          //Set not active periph address
          SetAddrActiveConfig (childInstanceDpName, FALSE, exceptionInfo);
          //Reset RR1
          dpSet(childInstanceDpName + ".RR1", 0);
        }
      }
    }  
  }
  if(tcpFindNb = 0) {
    if(isDebugFlag) {
          DebugTN("vcsMobileWL.ctl: ------ " + tcpDpPureName + " already discconnected");
    }
  }   
  else if(tcpFindNb > 1) {
    string errMessage = "vcsMobileWL.ctl: ERROR More than 1 TCP Dp has been found!!";
    dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    DelRowWlActiveDpList(tcpDpPureName);
  }
  else {  //Delete row in WL active DP list
    DelRowWlActiveDpList(tcpDpPureName);
  }
  // Send back FE notification mobile is deactivated in SCADA  
  unsigned uConnId = 0; 
  //dpset V_TCP WR1 to notify Mobile is activated in SCADA
  dpGet(tcpDpPureName + "." + CONN_ID_RR , uConnId);
  dpSet(tcpDpPureName + "." + CONN_ID_WR, uConnId); 
  dpSet(tcpDpPureName + ".WR1", SCADA_DEACTIVATED_WR1);
  return ret;  
}    
/**
@brief Set WireLess Ewo connect DP list
*/
int CopyDpListToDpEwoList() {
  // Get value from Dp List WL_ACTIVE_DP_LIST
  dyn_string dpEwoListName = makeDynString(".tcpDpNames", ".tcpConnSts", ".tcpConnEqpTypes", ".masterDpNames", ".posInstanceDpNames"); 
  anytype val1, val2, val3, val4, val5;
  GetDataWlActiveDpList(val1,  val2,  val3, val4, val5);
  dyn_anytype dpEwoListValue = makeDynAnytype(val1,  val2,  val3, val4, val5);
  // -- 
  // Check length coherence
  bool isDynlenCoherent = TRUE;
  for( int i = dynlen(dpEwoListValue); i < 1; i--) {
    if (dynlen(dpEwoListValue[1]) != dynlen(dpEwoListValue[i])) {
      isDynlenCoherent = FALSE;
    }
  }
  if (!isDynlenCoherent) {
    string errMessage = "vcsMobileWL.ctl: +++++ FATAL: copyDpListToDpEwoList() dynlen to be set to "+ WL_ACTIVE_DP_LIST +" are different";
    dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return -1;  
  }
  // --
  // Check and if different set WL_EWO_CONNECTED_DP_LIST    
  bool isNeedSet = false;  
  for( int i = dynlen(dpEwoListValue); i > 0; i--) {
    anytype dpValue;
    dpGet(WL_EWO_CONNECTED_DP_LIST + dpEwoListName[i], dpValue);
    if(!((string)dpValue == (string)dpEwoListValue[i])) {
      isNeedSet = true;
      if(isDebugFlag) {
          DebugTN("vcsMobileWL.ctl:" + WL_EWO_CONNECTED_DP_LIST + dpEwoListName[i] + ": " + dpValue + " different from " + WL_ACTIVE_DP_LIST + 
                  ": " + dpEwoListValue[i] );
      }
      break;
    }
  }
  if(isNeedSet) {
    dpSetWait(
        WL_EWO_CONNECTED_DP_LIST + dpEwoListName[1], dpEwoListValue[1],
        WL_EWO_CONNECTED_DP_LIST + dpEwoListName[2], dpEwoListValue[2],
        WL_EWO_CONNECTED_DP_LIST + dpEwoListName[3], dpEwoListValue[3],
        WL_EWO_CONNECTED_DP_LIST + dpEwoListName[4], dpEwoListValue[4],
        WL_EWO_CONNECTED_DP_LIST + dpEwoListName[5], dpEwoListValue[5]);
    dyn_errClass err = getLastError();    
    if(dynlen(err) > 0) {
      throw(err);
      errMessage = "vcsMobileWL.ctl: dpSet " + WL_EWO_CONNECTED_DP_LIST + " error : ";
      err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);      
      return -1;
    }
  }
  return 0;
}











