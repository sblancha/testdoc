V 12
1
LANG:1 0 
PANEL,-1 -1 1500 675 N "_3DFace" 1
"$dataPart"
"main()
{
  // Get machine name
  dyn_string exceptionInfo;
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
    InitPb.text = \"Failed to read machine name\";
    return;
  }

  LhcVacStartLongAct(\"ProgressBar\");
  // Read all resources
  LhcVacStartLongActPart(10.);
  VacResourcesParseFile(PROJ_PATH + \"/data/VacCtlResourcesWeb.txt\", glAccelerator, exceptionInfo);
  LhcVacFinishLongActPart();
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }

  // Check if we shall display only part of all data
  if(isDollarDefined(\"$dataPart\"))
  {
    DebugTN(\"$dataPart\", $dataPart);
    LhcVacSetDataPart($dataPart);
  }

  // Initialize static (survey) accelerator data
  LhcVacStartLongActPart(20.);
  int coco = LhcVacEqpInitPassive(false, PROJ_PATH + \"/data/\", glAccelerator, exceptionInfo);
  LhcVacFinishLongActPart();
  if(coco < 0)
  {
    fwExceptionHandling_display(exceptionInfo);
    InitPb.text = \"Failed to initialize survey data\";
    LhcVacStartLongAct(\"\");
    return;
  }

  // Initialize device functionalities
  LhcVacStartLongActPart(10.);
  LhcVacInitDevFunc();
  LhcVacFinishLongActPart();

  LhcVacStartLongActPart(60.);
  coco = LhcVacRenewVacEqpData(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN(exceptionInfo);
    ConfigProblemText.visible = true;
  }
  else
  {
    ConfigProblemText.visible = false;
  }
		
  // Verify timestamp
  string pvssTimeStamp, dllTimeStamp;
  LhcVacGetDataTimeStamp(pvssTimeStamp, exceptionInfo);
  LhcVacGetTimeStamp(dllTimeStamp);
  if(pvssTimeStamp != dllTimeStamp)
  {
    ChildPanelOnCentralModal(\"vision/MessageInfo1\", \"WARNING\",
      makeDynString(\"PVSS data <\" + pvssTimeStamp +
      \">\\n DLL data <\" + dllTimeStamp + \">\"));
  }

  LhcVacFinishLongActPart();
  LhcVacStartLongAct(\"\");

  ShowLegendLabels();
  Layout.dataReady();	// Finish all data
  LayerOff(2);
  LayerOn(1);

  InitAccesModeIcons();

  // Connect to data reload progress DPE
  string dpeName = \"_VacMachineInfo.ImportInProgress\";
  if(dpExists(dpeName))
  {
    dpConnect(\"DataReloadCb\", dpeName);
  }

  /* UNICOS calls added to solve problem of \"UNICOS panels have controls not enabled\",
  ** solution provided by F.Bernard
  */
  /*
  unGraphicalFrame_getLibsAndFunctions(makeDynString(), g_dsLibs, g_dsHMIFunctions); 
  unGraphicalFrame_initHMI(); 
  */
  /* End of UNICOS by F.Bernard */

  Profile.dataReady();
  ProfilePart1.dataReady();
  ProfilePart2.dataReady();
  ProfilePart3.dataReady();
  ProfilePart4.dataReady();
  ProfilePart5.dataReady();
}" 0
 E E E E 1 -1 -1 0  10 50
""0  1
E "#uses \"VacCtlEqpDataPvss\"  // Load .so or DLL

#uses \"vclResources.ctl\"   // Load CTRL library
#uses \"vclProgress.ctl\"    // Load CTRL library
#uses \"vclEqpConfig.ctl\"   // Load CTRL library
#uses \"vclReplay.ctl\"      // Load CTRL library
#uses \"vclUi.ctl\"          // Load CTRL library (@SAA Help Support)
        
/* Variables added to solve problem of \"UNICOS panels have controls not enabled\",
** solution provided by F.Bernard
*/
dyn_string g_dsDeviceType; 
int g_iCommitTime, g_iWarningTime; 
dyn_string g_dsAutologoutUsers; 
dyn_string g_dsConfiguredFrontEndType; 
dyn_string g_dsLibs, g_dsHMIFunctions; 
/* End of variable by F.Bernard */


// Names of menus and commands behind every menu item
dyn_string    menuNames;
dyn_dyn_uint  menuItems;


// Read from resource and add to layout all geneda labels
void ShowLegendLabels()
{
  int n = 1;
  while(true)
  {
    string resource = glAccelerator + (isDollarDefined(\"$dataPart\") ? \"_\" + $dataPart : \"\") + \"_Legend\" + n;
    int x = VacResourcesGetValue(resource + \".X\", VAC_RESOURCE_INT, -1);
    if(x < 0)
    {
      break;
    }
    int y = VacResourcesGetValue(resource + \".Y\", VAC_RESOURCE_INT, -1);
    if(y < 0)
    {
      break;
    }
    string text = VacResourcesGetValue(resource + \".Text\", VAC_RESOURCE_STRING, \"\");
    if(text == \"\")
    {
      break;
    }
    Layout.addLegendLabel(x, y, text);
    n++;
  }
}

// Initialize all access mode icons
void InitAccesModeIcons()
{
  dyn_string mainParts;
  LhcVacGetAllMainParts(mainParts);
  string dataPart;
  if(isDollarDefined(\"$dataPart\"))
  {
    dataPart = $dataPart;
  }

  int iconIdx = 1;
  for(int n = dynlen(mainParts) ; n > 0 ; n--)
  {
    string resourceBase = \"AccessMode.\" +
      (dataPart != \"\" ? (dataPart + \".\") : \"\") + mainParts[n] + \".\";
    int x = VacResourcesGetValue(resourceBase + \"CoordinateX\", VAC_RESOURCE_INT, -1);
    if(x < 0)
    {
      continue;
    }
    int y = VacResourcesGetValue(resourceBase + \"CoordinateY\", VAC_RESOURCE_INT, -1);
    if(y < 0)
    {
      continue;
    }
    // Coordinates are defined - does DP exist?
    string dpName, dpeName;
    LhcVacGetDpeForAccessMode(mainParts[n], dpName, dpeName);
    if(!dpExists(dpeName))
    {
      // It is responsability of vcsAccessMode.ctl script to create
      // and PUBLISH via DIP access mode DPs - we use them 'as is'
      continue;
    }

    // Eveything exists - activate icon
    shape icon = getShape(\"Access\" + iconIdx);
    if(icon == 0)
    {
      DebugTN(\"MainSps.pnl:InitAccesModeIcons(): Access icon # \" +
        (iconIdx - 1) + \" does not exist\");
      break;
    }
    icon.dpName = dpName;
    icon.connect();
    icon.position(x, y);
    iconIdx++;
  }
  // Hide unused access icons
  for( ; iconIdx <= 20 ; iconIdx++)
  {
    shape icon = getShape(\"Access\" + iconIdx);
    if(icon == 0)
    {
      break;
    }
    icon.visible(false);
  }
}

// Callback for DPs which were requested by Layout EWO
void NewValueCb(string dpe, anytype value, string dpeTs, time ts)
{
  string dpeName = dpSubStr(dpe, DPSUB_DP_EL);
  LhcVacNewOnlineValue(dpeName, value, ts);
}

// Callback to be called when data import started on server
void DataReloadCb(string dpe, bool state)
{
  if(state)
  {
    LayerOn(8);
  }
}
" 0
 2
"CBRef" "1"
"EClose" E
""
DISPLAY_LAYER, 0 0 1 0 1 0 1 0 1 0 1 0 1 0 0 0
LAYER, 0 
1
LANG:1 0 
29 66
"Layout"
""
1 5 19 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
63 0 0 0 0 0
E E E
0
1
LANG:1 0 

2
"profileBarWidth" "int 95"
"readOnly" "bool TRUE"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  5 23 536 554
15 MainViewSps.ewo
1
0 "dpConnectRequest" "()"
"  DebugTN(\"dpConnectRequest()\"); 
  string dpe; 
  bool connect; 
  LhcVacNextDpConnect(dpe, connect); 
  while(dpe != \"\") 
  { 
//    DebugTN(dpe, connect); 
    if(connect) 
    { 
      dpConnect(\"NewValueCb\", dpe, dpe + \":_original.._stime\"); 
    } 
    else 
    { 
      dpDisconnect(\"NewValueCb\", dpe, dpe + \":_original.._stime\"); 
    }     
    LhcVacNextDpConnect(dpe, connect); 
  }"
E15 7
"CLOCK1"
""
1 0.999999999999861 1 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
2 0 0 0 0 0
E E E
1
1
LANG:1 0 

0
1
LANG:1 25 Sans,-1,15,5,50,0,0,0,0,0
0  1 1 175 21
E 1 1 "%H:%M:%S" "%d.%m.%Y"
1 1 0 1
1
LANG:1 25 Sans,-1,15,5,50,0,0,0,0,0
0  1
LANG:1 25 Sans,-1,15,5,50,0,0,0,0,0
0 
2 10
"ConfigProblemText"
""
1 198 -2 E E E 1 E 0 E N "STD_used" E N "_Transparent" E E
 E E
5 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  1 E 198 -2 343 18
0 2 2 "0s" 0 0 0 64 0 0  198 -2 1
1
LANG:1 25 Sans,-1,15,5,50,0,0,0,0,0
0 1
LANG:1 22 Configuration problems
1 88 2 "" 5
0
1 89 2 "" 4
0
29 31
"Access1"
""
1 1 448 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
20 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  1 448 21 462
14 VacIconEwo.ewo
0
E29 32
"Access2"
""
1 1 465 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
22 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  1 465 21 479
14 VacIconEwo.ewo
0
E29 33
"Access3"
""
1 1 482 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
24 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  1 482 21 496
14 VacIconEwo.ewo
0
E29 34
"Access4"
""
1 1 500 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
26 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  1 500 21 514
14 VacIconEwo.ewo
0
E29 35
"Access5"
""
1 1 517 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
28 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  1 517 21 531
14 VacIconEwo.ewo
0
E29 36
"Access6"
""
1 25 448 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
30 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  25 448 45 462
14 VacIconEwo.ewo
0
E29 37
"Access7"
""
1 25 465 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
32 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  25 465 45 479
14 VacIconEwo.ewo
0
E29 38
"Access8"
""
1 25 482 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
34 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  25 482 45 496
14 VacIconEwo.ewo
0
E29 39
"Access9"
""
1 25 500 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
36 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  25 500 45 514
14 VacIconEwo.ewo
0
E29 40
"Access10"
""
1 25 517 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
38 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  25 517 45 531
14 VacIconEwo.ewo
0
E29 41
"Access11"
""
1 49 448 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
40 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  49 448 69 473
14 VacIconEwo.ewo
0
E29 42
"Access12"
""
1 49 465 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
42 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  49 465 69 479
14 VacIconEwo.ewo
0
E29 43
"Access13"
""
1 49 482 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
44 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  49 482 69 496
14 VacIconEwo.ewo
0
E29 44
"Access14"
""
1 49 500 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
46 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  49 500 69 514
14 VacIconEwo.ewo
0
E29 45
"Access15"
""
1 49 517 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
48 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  49 517 69 531
14 VacIconEwo.ewo
0
E29 46
"Access16"
""
1 73 448 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
50 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  73 448 93 462
14 VacIconEwo.ewo
0
E29 47
"Access17"
""
1 73 465 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
52 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  73 465 93 479
14 VacIconEwo.ewo
0
E29 48
"Access18"
""
1 73 482 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
54 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  73 482 93 496
14 VacIconEwo.ewo
0
E29 49
"Access19"
""
1 73 500 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
56 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  73 500 93 514
14 VacIconEwo.ewo
0
E29 50
"Access20"
""
1 73 517 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
58 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  73 517 93 531
14 VacIconEwo.ewo
0
E29 70
"Profile"
""
1 543 0 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
64 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  543 0 1092 674
20 ProfileAllSpsEwo.ewo
0
E1 90 4 "" 7
0
1 91 4 "" 6
0
1 92 4 "" 5
0
1 93 4 "" 8
0
29 77
"ProfilePart1"
""
1 1102 1 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
65 0 0 0 0 0
E E E
0
1
LANG:1 0 

2
"mainPartName" "string TT10"
"startSectorName" "string "
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  1102 1 1495 135
14 ProfileEwo.ewo
0
E29 78
"ProfilePart2"
""
1 1102 135 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
67 0 0 0 0 0
E E E
0
1
LANG:1 0 

4
"labelText" "string TT20+TDC2"
"endSectorName" "string 2040"
"mainPartName" "string "
"startSectorName" "string 2002"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  1102 135 1495 269
14 ProfileEwo.ewo
0
E29 85
"ProfilePart3"
""
1 1102 270 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
69 0 0 0 0 0
E E E
0
1
LANG:1 0 

4
"labelText" "string "
"endSectorName" "string "
"mainPartName" "string TT40"
"startSectorName" "string "
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  1102 270 1495 404
14 ProfileEwo.ewo
0
E29 86
"ProfilePart4"
""
1 1102 406 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
71 0 0 0 0 0
E E E
0
1
LANG:1 0 

4
"labelText" "string "
"endSectorName" "string "
"mainPartName" "string TI8"
"startSectorName" "string "
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  1102 405 1495 539
14 ProfileEwo.ewo
0
E29 87
"ProfilePart5"
""
1 1102 540 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
73 0 0 0 0 0
E E E
0
1
LANG:1 0 

4
"labelText" "string TT60+TI2"
"endSectorName" "string 1204"
"mainPartName" "string "
"startSectorName" "string 6001"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  1102 540 1495 674
14 ProfileEwo.ewo
0
E0
LAYER, 1 
1
LANG:1 0 
29 13
"ProgressBar"
""
1 0 280 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
7 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"progress" "int 0"
"totalSteps" "int 1000"
"centerIndicator" "bool TRUE"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  0 280 534 300
11 ProgressBar
0
E13 15
"InitPb"
""
1 170 300 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
9 0 0 0 0 0
E E E
1
1
LANG:1 0 

0
1
LANG:1 25 Sans,-1,15,5,50,0,0,0,0,0
0  168 298 522 332

T 
1
LANG:1 28 Initializing, please wait...
E E E E
0
LAYER, 2 
1
LANG:1 0 
0
LAYER, 3 
1
LANG:1 0 
0
LAYER, 4 
1
LANG:1 0 
0
LAYER, 5 
1
LANG:1 0 
0
LAYER, 6 
1
LANG:1 0 
0
LAYER, 7 
1
LANG:1 0 
13 16
"RealoadPb1"
""
1 20 181 E E E 1 E 1 E N "alarmGingUnq" E N "_Button" E E
 E E
10 0 0 0 0 0
E E E
1
1
LANG:1 0 

0
1
LANG:1 28 Courier,-1,20,5,75,0,0,0,0,0
0  18 179 535 232

T 
1
LANG:1 33 Data reload on server in progress
E E E E
13 17
"ReloadPb2"
""
1 25 341 E E E 1 E 1 E N "alarmGingUnq" E N "_Button" E E
 E E
12 0 0 0 0 0
E E E
1
1
LANG:1 0 

0
1
LANG:1 28 Courier,-1,20,5,75,0,0,0,0,0
0  23 339 531 390

T 
1
LANG:1 28 Please exit the application.
E E E E
0
3 2 "LogList" -1
"objects\\UN_MESSAGETEXT\\unMessageText.pnl" 20 562 T 19 1.38815558049194 0 3.27027027027027 0 -1248.64864864865
0
3 4 "PANEL_REF5" -1
"vision\\objects\\spsVacIcon_UnAlrm.pnl" 428 1 T 65 1 0 1 17 0
0
0
