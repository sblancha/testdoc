//	Implementation of VacIconConnection class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconConnection.h"

#include "Eqp.h"

/*
**	FUNCTION
**		Connect to all esssential DPEs of specified device
**
**	ARGUMENTS
**		pEqp	- Pointer to device to which we shall connect
**		dpes	- List of DPE names which are of interest for icon. Note
**					Eqp will emit signals for ALL DPE changes, but we'll
**					only report to icon changes of these DPEs
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconConnection::connect(Eqp *pEqp, const QStringList &dpes, DataEnum::DataMode mode)
{
	this->dpes = dpes;
	this->mode = mode;
	pEqp->connect(this, mode);
}

/*
**	FUNCTION
**		Disconnect from specified device
**
**	ARGUMENTS
**		pEqp	- Pointer to device from which we shall discconnect
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconConnection::disconnect(Eqp *pEqp)
{
	pEqp->disconnect(this, mode);
	dpes.clear();
}

/*
**	FUNCTION
**		Slot signalled by device when DPE value is changed. If DPE
**		is in list of DPEs of interest - emit signal for icon
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconConnection::dpeChange(Eqp * /* pSrc */, const char *dpeName,
	DataEnum::Source source, const QVariant & /* value */, DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	switch(mode)
	{
	case DataEnum::Online:
	case DataEnum::Polling:
		if(this->mode == DataEnum::Replay)
		{
			return;
		}
		break;
	case DataEnum::Replay:
		if(this->mode != DataEnum::Replay)
		{
			return;
		}
		break;
	}
	// Only analyze DPEs of device itself, signals from parent or PLC are just
	// translated to icon
	if(source == DataEnum::Own)
	{
		bool dpeOfInterest = false;
		foreach(const QString dpe, dpes)
		{
			if(dpe == dpeName)
			{
				dpeOfInterest = true;
				break;
			}
		}
		if(!dpeOfInterest)
		{
			return;
		}
	}
	emit stateChanged();
}

/*
**	FUNCTION
**		Slot signalled by device when device selection is changed
**
**	ARGUMENTS
**		pSrc		- Pointer to object - source of signal
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconConnection::selectChange(Eqp * /* pSrc */)
{
	emit selectChanged();
}

