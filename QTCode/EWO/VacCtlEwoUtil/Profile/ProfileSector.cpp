//	Implementation of ProfileSector class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileSector.h"

#include "Sector.h"

#include <QPainter>

/////////////////////////////////////////////////////////////////////////////////
///////////////////////// Construction/destruction

ProfileSector::ProfileSector(Sector *pSector)
{
	this->pSector = pSector;
	left = right = -1;
	leftWithinDevice = rightWithinDevice = false;
}

ProfileSector::ProfileSector(const ProfileSector &source)
{
	pSector = source.pSector;
	left = source.left;
	right = source.right;
	leftWithinDevice = source.leftWithinDevice;
	rightWithinDevice = source.rightWithinDevice;
}

void ProfileSector::calculateLabelRect(QPainter &painter, int areaBottom)
{
	if(!pSector)
	{
		labelRect.setWidth(1);
		labelRect.setHeight(1);
		labelRect.setBottom(areaBottom);
		labelRect.setTop(areaBottom - 1);
		return;
	}
	labelRect = painter.fontMetrics().boundingRect(pSector->getName());
	// qDebug("Bounding rect for %s is (%d %d) (%d x %d)\n", pSector->getName(), labelRect.left(), labelRect.top(), labelRect.width(), labelRect.height());
	labelRect.setWidth(labelRect.width() + 6);
	//labelRect.setHeight(labelRect.height() + 2);
	labelRect.moveBottom(areaBottom);
	int center = (left + right) >> 1;
	labelRect.moveLeft(center - (labelRect.width() >> 1));
	//qDebug("Final rect for %s is (%d %d) (%d x %d)\n", pSector->getName(), labelRect.left(), labelRect.top(), labelRect.width(), labelRect.height());
}

void ProfileSector::moveLabelRectUp(void)
{
	labelRect.moveTop(labelRect.top() - labelRect.height());
}

void ProfileSector::drawLabel(QPainter &painter)
{
	if(pSector)
	{
		painter.setPen(Qt::blue);
		// qDebug("Draw sector label %s at (%d %d) (%d x %d)\n", pSector->getName(), labelRect.left(), labelRect.top(), labelRect.width(), labelRect.height());
		painter.drawText(labelRect, Qt::AlignCenter, pSector->getName());
	}
}

