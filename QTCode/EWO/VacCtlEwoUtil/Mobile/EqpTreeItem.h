#ifndef	EQPTREEITEM_H
#define	EQPTREEITEM_H

// Item of tree view representing device (pressure or bakeout)

#include <QTreeWidget>

#include "MobileTreeItemTypes.h"


class Eqp;

class EqpTreeItem : public QTreeWidgetItem
{
public:
	EqpTreeItem(QTreeWidgetItem *parent, Eqp *pEqp, int index);
	virtual ~EqpTreeItem();

	// Access
	inline Eqp *getEqp(void) const { return pEqp; }
	inline int getIndex(void) const { return index; }

protected:

	// Pointer to device for this item
	Eqp		*pEqp;

	// Index of this device in tree
	int		index;
};

#endif	// EQPTREEITEM_H
