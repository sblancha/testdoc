# Project file for qmake utility to build VacReplayCtl EWO
#
#	14.01.2009	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = VacReplayCtl

INCLUDEPATH += ../../common/Platform \
	../../common/VacCtlHistoryData

LIBS += -lVacCtlHistoryData \
	-lVacCtlEqpData

HEADERS = VacReplayCtl.h \
	VacReplayCtlEwo.h

SOURCES = VacReplayCtl.cpp \
	VacReplayCtlEwo.cpp

