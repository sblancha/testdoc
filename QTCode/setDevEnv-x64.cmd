@echo off
echo Setting environment for WinCC OA 3.15 development
echo ------------------------------------------ 
echo .
call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" amd64
REM -- WINCC-281, prevent linker error TRK0002
set _IsNativeEnvironment=true
REM --------------------------------------------------------------------------------
set PVSS_DIR=C:\Siemens\Automation\WinCC_OA\3.15
REM ----- Optionally, set up the environment for Qt; Note that WinCCOA needs it as QT_DIR:
REM ----- Commercial edition should be put in C:\Qt\qt-5.5.0
REM ----- Whereas the opensource edition installs by default into C:\Qt\Qt5.5.0\5.5\msvc2013_64
REM set QTDIR=C:\Qt\Qt5.5.0\5.5\msvc2013_64
set QTDIR=C:\Qt\qt-5.5.0
set QT_DIR=%QTDIR%
set API_ROOT=%PVSS_DIR%\api
REM ---- we will need to have "findstr", and since Windows 7 it is not in the path,
REM ---- and may be found in C:\Windows\System32
REM ---- We also need utilities such as "sed" that are in the WinCC OA bin folder,
REM ---- so we need to add it to the path
set PATH=%PVSS_DIR%\bin;%PVSS_DIR%\api;%QTDIR%\bin;C:\Windows\System32;%PATH%
call %API_ROOT%\checkApiEnv.cmd x64
