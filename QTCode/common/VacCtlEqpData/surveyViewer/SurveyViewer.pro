# Project file for Survey data viewer
#
#	26.08.2008	L.Kopylov
#		Initial version
#

TEMPLATE = app

TARGET = surveyViewer

QT += widgets

INCLUDEPATH += 	../../VacCtlUtil \
	../../Platform \
	../../VacCtlHistoryData \
	../ResourcePool \
	../FunctionalType \
	../DataPool \
	../Eqp \
	../

LIBS += -L../../../bin -lVacCtlEqpData
 
unix:CONFIG += qt thread release warn_on
windows:CONFIG += qt thread release warn_on

#DEFINES += _VACDEBUG
#DEFINES += _CRT_SECURE_NO_DEPRECATE


HEADERS = LineViewer.h \
				SurveyViewer.h

SOURCES = LineViewer.cpp \
				SurveyViewer.cpp \
				main.cpp
