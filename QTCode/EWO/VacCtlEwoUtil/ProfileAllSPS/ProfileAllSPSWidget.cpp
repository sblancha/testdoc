//	Implementation of ProfileAllSPSWidget class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileAllSPSWidget.h"
#include "ExtendedLabel.h"


#include "ProfileImage.h"

#include "VacMainView.h"
#include "ResourcePool.h"
#include "DataPool.h"
#include "Eqp.h"

#include <qlayout.h>
#include <qdesktopwidget.h>
#include <qapplication.h>
#include <qmenu.h>
#include <qcursor.h>
#include <QMouseEvent>


/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

ProfileAllSPSWidget::ProfileAllSPSWidget(QWidget *parent, Qt::WindowFlags f) :
	QWidget(parent, f)
{
	setWindowTitle("Pressure Profile - all of SPS");

	pMainPart = NULL;
}

ProfileAllSPSWidget::~ProfileAllSPSWidget()
{
}

void ProfileAllSPSWidget::dataReady(void)
{
	buildInitialEqpMask();
	buildLayout();

	/*
	const QList<VacMainView *> &mainList = VacMainView::getInstanceList();
	for(int idx = 0 ; idx < mainList.count() ; idx++)
	{
		VacMainView *pInstance = mainList.at(idx);
		if(pInstance->isPvssEvents())
		{
			QObject::connect(this, SIGNAL(profileDpDown(int, int, int, int, int, const char *)),
				pInstance, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
		}
	}
	*/
	VacMainView *pMainView = VacMainView::getInstance();
	if(pMainView)
	{
		QObject::connect(this, SIGNAL(profileDpDown(int, int, int, int, int, const char *)),
			pMainView, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
	}
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileAllSPSWidget::buildLayout(void)
{
	// 1) Main layout - 6 labels and profile images
	QVBoxLayout *pMainBox = new QVBoxLayout(this);
	pMainBox->setContentsMargins(0, 0, 0, 0);
	pMainBox->setSpacing(0);

	// 2) For every sextant - label and profile image
	// Set limits for new profile
	int min = -10, max = -6;
	ResourcePool &resourcePool = ResourcePool::getInstance();
	QString resourceName = "ProfileAllSPS.BeamVacuumScaleMin";
	if(resourcePool.getIntValue(resourceName, min) == ResourcePool::NotFound)
	{
		min = -10;
	}
	resourceName = "ProfileAllSPS.BeamVacuumScaleMax";
	if(resourcePool.getIntValue(resourceName, max) == ResourcePool::NotFound)
	{
		max = -6;
	}

	DataPool &pool = DataPool::getInstance();
	QString mpName;
	for(int n = 1 ; n <= 6 ; n++)
	{
		QString mpName = "TS" + QString::number(n);
		MainPart *pMainPart = pool.findMainPartData(mpName.toLatin1());
		if(!pMainPart)
		{
			continue;
		}
		Sector *pFirstSector = NULL;
		Sector *pLastSector = NULL;
		const QList<Sector *> &sectors = pool.getSectors();
		for(int idx = 0 ; idx < sectors.count() ; idx++)
		{
			Sector *pSector = sectors.at(idx);
			// Ignore outer sectors
			if(pSector->isOuter())
			{
				continue;
			}
			// Ignore DSL sector(s)
			if(pSector->getVacType() == VacType::DSL)
			{
				continue;
			}
			const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
			for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
			{
				MainPartMap *pMap = maps.at(mapIdx);
				if(pMap->getMainPart() != pMainPart)
				{
					continue;
				}
				if(!pFirstSector)
				{
					pFirstSector = pSector;
				}
				pLastSector = pSector;
				break;
			}
		}
		if(!pFirstSector)
		{
			continue;
		}

		// Main part and sector range have been found - build widgets
		ExtendedLabel *pLabel = new ExtendedLabel(mpName, this);
		pLabel->setLabelType(ExtendedLabel::Active);
		pMainBox->addWidget(pLabel);
		pLabel->setAlignment(Qt::AlignHCenter);
		QObject::connect(pLabel, SIGNAL(mousePressed(QMouseEvent *)),
			this, SLOT(labelPressed(QMouseEvent *)));
		mpLabels.append(pLabel);
		pLabel->show();

		QString errMsg;
		ProfileImage *pView = ProfileImage::create(this, pFirstSector, pLastSector, DataEnum::Online, allEqpMask, errMsg);
		if(!pView)
		{
			// TODO: show error message
			continue;
		}
		pView->setMinimumSize(400, 80);
		pMainBox->addWidget(pView, 10);	// Image will consume all free space
		images.append(pView);

		// Prepare and show view
		pView->setLimits(min, max);
		pView->show();
		pView->applyMask(eqpMask);

		// Connect to essential signals of view
		QObject::connect(pView, SIGNAL(limitsChanged(int, int)),
			this, SLOT(limitsChange(int, int)));
		QObject::connect(pView, SIGNAL(dpMouseDown(int, int, int, int, const char *)),
			this, SLOT(dpMouseDown(int, int, int, int, const char *)));
	}

	setMinimumSize(pMainBox->minimumSize());
}


/*
**	FUNCTION
**		Build initial equipment type mask
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileAllSPSWidget::buildInitialEqpMask(void)
{
	// First build mask containing all possible types for profile
	allEqpMask.append(FunctionalType::VGM);
	allEqpMask.append(FunctionalType::VGR);
	allEqpMask.append(FunctionalType::VGP);
	allEqpMask.append(FunctionalType::VGI);
	allEqpMask.append(FunctionalType::VGF);
	allEqpMask.append(FunctionalType::VPI);
	allEqpMask.append(FunctionalType::VPGMPR);
	allEqpMask.append(FunctionalType::VGTR);

	// Then read default settings from resource
	eqpMask = allEqpMask;
	ResourcePool &pool = ResourcePool::getInstance();
	QString resourceName = "Profile.BeamVacuumEqpTypes";
	if(pool.getEqpTypesMask(resourceName, eqpMask) == ResourcePool::NotFound)
	{
		eqpMask = allEqpMask;
	}
}

/*
**	FUNCTION
**		Slot activated when vertical limits have been changed in one of graphs
**
**	ARGUMENTS
**		min	- New minimum value
**		max	- New maximum value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllSPSWidget::limitsChange(int min, int max)
{
	for(int idx = 0 ; idx < images.count() ; idx++)
	{
		ProfileImage *pImage = images.at(idx);
		pImage->setLimits(min, max);
	}
}

/*
**	FUNCTION
**		Slot activated when mouse pointer was pressed on one of devices in profile
**
**	ARGUMENTS
**		button	- Mouse button number
**		mode	- Data acquisition mode
**		x		- Mouse X-coordinate in screen coordinate system
**		y		- Mouse Y-coordinate in screen coordinate system
**		dpName	- Name of DP under mouse pointer
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllSPSWidget::dpMouseDown(int button, int mode, int x, int y, const char *dpName)
{
	emit profileDpDown(button, mode, x, y, 0, dpName);
}

/*
**	FUNCTION
**		Slot activated when one of TS 'labels' is pressed
**
**	ARGUMENTS
**		e	- Pointer to mouse event that caused signal to emit
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllSPSWidget::labelPressed(QMouseEvent *e)
{
	if(e->button() != Qt::RightButton)
	{
		return;
	}

	// Find in which TS button was pressed
	ExtendedLabel *pLabel = NULL;
	int idx;
	for(idx = 0 ; idx < mpLabels.count() ; idx++)
	{
		ExtendedLabel *pSearchLabel = mpLabels.at(idx);
		if(pSearchLabel->underMouse())
		{
			pLabel = pSearchLabel;
			break;
		}
	}
	if(!pLabel)
	{
		return;
	}
	QString label = pLabel->text();
	DataPool &pool = DataPool::getInstance();
	pMainPart = pool.findMainPartData(label.toLatin1());

	// Find which equipment types are present in all profiles
	unsigned vacMask;
	VacEqpTypeMask allTsMask;
	for(idx = 0 ; idx < images.count() ; idx++)
	{
		ProfileImage *pView = images.at(idx);
		VacEqpTypeMask tsMask;
		tsMask.clear();
		pView->analyzeTypes(vacMask, tsMask);
		const QList<FunctionalType *> &types = tsMask.getList();
		for(int typeIdx = 0 ; typeIdx < types.count() ; typeIdx++)
		{
			FunctionalType *pType = types.at(typeIdx);
			if(allEqpMask.contains(pType->getType()))
			{
				allTsMask.append(pType->getType());
			}
		}
	}

	// Start building menu. First part - equipment types
	QMenu *pMenu = new QMenu(pLabel);
	QString caption("<b>Equipment</b>");
	pMenu->addAction(caption);
	const QList<FunctionalType *> &types = allEqpMask.getList();
	for(idx = 0 ; idx < types.count() ; idx++)
	{
		FunctionalType *pType = types.at(idx);
		if(allTsMask.contains(pType->getType()))
		{
			QAction *pAction = pMenu->addAction(pType->getDescription());
			pAction->setCheckable(true);
			pAction->setData(pType->getType());
			pAction->setChecked(eqpMask.contains(pType->getType()));
		}
	}

	// 2nd part - extra menu items
	if(pMainPart)
	{
		pMenu->addSeparator();
		pSynopticMenuAction = pMenu->addAction("Synoptic...");
	}
	connect(pMenu, SIGNAL(triggered(QAction *)), this, SLOT(tsMenu(QAction *)));

	// Finally - show menu
	pMenu->exec(QCursor::pos());
	delete pMenu;
}

/*
**	FUNCTION
**		Slot activated when item in popup menu is selected
**
**	ARGUMENTS
**		item	- ID of item, positive values are functional type IDs
**					to be added/removed from profile;
**					negative values have special meaning (see method
**					labelPressed() where menu is built).
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllSPSWidget::tsMenu(QAction *pAction)
{
	if(pAction == pSynopticMenuAction)	// Show synoptic
	{
		if(!pMainPart)
		{
			return;
		}
		QStringList sectorList;
		unsigned vacType = 0;
		DataPool &pool = DataPool::getInstance();
		const QList<Sector *> &sectors = pool.getSectors();
		for(int idx = 0 ; idx < sectors.count() ; idx++)
		{
			Sector *pSector = sectors.at(idx);
			if(pSector->isInMainPart(pMainPart))
			{
				vacType |= pSector->getVacType();
				sectorList.append(pSector->getName());
			}
		};
		VacMainView *pMainView = VacMainView::getInstance();
		pMainView->dialogRequest(VacMainView::DialogSynoptic, vacType, sectorList, DataEnum::Online);
		return;
	}

	if(!pAction->data().isValid())
	{
		return;
	}
	int item = pAction->data().toInt();
	if(allEqpMask.contains(item))	// Eqp type selection
	{
		if(eqpMask.contains(item))
		{
			eqpMask.remove(item);
		}
		else
		{
			eqpMask.append(item);
		}
		for(int idx = 0 ; idx < images.count() ; idx++)
		{
			ProfileImage *pView = images.at(idx);
			pView->applyMask(eqpMask);
		}
		return;
	}

	// Other items have special meaning
}
