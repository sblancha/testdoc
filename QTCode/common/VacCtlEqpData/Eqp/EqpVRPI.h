#ifndef EQPVRPI_H
#define EQPVRPI_H

//	VRPI - power supply for ion pumps

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVRPI : public Eqp
{
	Q_OBJECT

public:
	EqpVRPI(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVRPI(const EqpVRPI &src) : Eqp(src) {}
#endif
	~EqpVRPI();

	virtual void setSelected(bool selected, bool isFromPvss);
	virtual void getSlaves(QStringList &slaves);
	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Method to be called by VPI: VPI supplies pressure value
	virtual void checkCriteria(EqpMsgCriteria *pCriteria, float pr);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline bool isBlockedOff(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? blockedOffReplay : blockedOffOnline; }
	bool isValueValid(DataEnum::DataMode mode);
	virtual int getErrorCode(DataEnum::DataMode mode);

	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of BlockedOff DPE for online mode
	bool		blockedOffOnline;

	// Last known value of BlockedOff DPE for replay mode
	bool		blockedOffReplay;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

	// Flag indicating if this device have BlockedOff DPE (VELO VRPI has no one)
	bool		haveBlockedOff;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, bool blockedOff, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, bool blockedOff, bool plcAlarm, QString &string);
#endif

	virtual bool isValueValid(bool plcAlarm, unsigned rr1);

};

#endif	// EQPVRPI_H
