#
# Project file for qmake utility to build VacDpDataEwo EWO
#
#	03.01.2009	L.Kopylov
#		Initial version
#
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = VacDpDataEwo

INCLUDEPATH += ../../common/VacCtlUtil \
		../../common/Platform \
		../../common/VacCtlEqpData \
		../../common/VacCtlEqpData/DataPool \
		../../common/VacCtlEqpData/Eqp \
		../VacCtlEwoUtil

HEADERS = VacDpDataConnection.h \
	VacDpData.h \
	VacDpDataEwo.h

SOURCES = VacDpDataConnection.cpp \
	VacDpData.cpp \
	VacDpDataEwo.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlHistoryData \
	-lVacCtlEqpData \
	-lVacCtlUtil

