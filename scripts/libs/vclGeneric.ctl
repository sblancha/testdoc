// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Type: Library
Name: vclGeneric.ctl
Language: WCCOA CTRL

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: Definition of generic functions

Version : 1.0    
  
Limitations: No
 
Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------

/**
@brief Initialize DLL/SO data pools
@details Return 0 if successfull, -1 if passive failed, 1 if active failed, 2 if profibus mobile failed.\n
It calls the 3 C++ Libraries API:
  - LhcVacEqpInitPassive()
  - LhcVacEqpInitActive()
  - VacImportParseFile()	
*/
int InitDynLibStaticDataPools()
{
  dyn_string	exceptionInfo;
  long			ret;
  bool			isLhcData;

  // Set accelerator name to be used by other components
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("InitDynLibStaticDataPools(): Failed to read machine name: " + exceptionInfo);
    return -1;
  }
  DebugTN("InitDynLibStaticDataPools(): Machine is " + glAccelerator);
  isLhcData = glAccelerator == "LHC" ? true : false;
  // Initalize passive machine data
  ret = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
  if(ret < 0)
  {
    DebugTN("InitDynLibStaticDataPools(): LhcVacEqpInitPassive() failed: " + exceptionInfo);
    return -1;
  }
  DebugTN("InitDynLibStaticDataPools(): LhcVacEqpInitPassive() done");

  // Initialzie active equipment information
  ret = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
    exceptionInfo);
  if(ret < 0)
  {
    DebugTN("InitDynLibStaticDataPools(): LhcVacEqpInitActive() failed: " + exceptionInfo);
    return 1;
  }
  DebugTN("InitDynLibStaticDataPools(): LhcVacEqpInitActive() done");

  // Initialize mobile equipment addressing information
  ret = VacImportParseFile(PROJ_PATH + "/data/" + glAccelerator + "_Addresses.mobile", glAccelerator, true, exceptionInfo);
  if(ret < 0)
  {
    DebugTN("InitDynLibStaticDataPools(): VacImportParseFile() failed: " + exceptionInfo);
    return 2;
  }
  //DebugTN("InitDynLibStaticDataPools(): VacImportParseFile() done", ret, dynlen(exceptionInfo) + " errors");
  DebugTN("InitDynLibStaticDataPools(): VacImportParseFile() done", ret, dynlen(exceptionInfo) + " errors", exceptionInfo);

  DebugTN("InitDynLibStaticDataPools(): Done");
  return 0;
}

/**
@brief Get all DPEs name of a given DP
@param[in]   dpName          Name of DP for which all DPE names are required
@param[out]  allDpeNames     List of all DPE names for given DP will be returned here
@param[out]  allDpeTypes     Code of dpe variable type
@param[out]  exceptionInfo   Details of exception will be written here
*/
void GetDpeListOfDpType(string dpName, dyn_string &allDpeNames, dyn_int &allDpeTypes, dyn_string &exceptionInfo)
{
  string dpType = dpTypeName(dpName);
  dyn_dyn_string	names;
  dyn_dyn_int			types;
  dyn_string			dpeTree, localExceptionInfo;

  dynClear(allDpeNames);
  dynClear(allDpeTypes);
  dynClear(exceptionInfo);

  // Get DP type structure for working DP
  if(dpTypeGet(dpType, names, types) < 0)
  {
    dyn_errClass err = getLastError();
    DebugTN("vcsMobileEqp.ctl: ERROR: dpTypeGet(" + dpType + ") failed: " + StdErrorText(err));
    DebugTN(err);
    fwException_raise(exceptionInfo, "ERROR", "BuildDpeListForDp(): dpTypeGet(" + dpType +
      ") failed for DP <" + dpName + ">","");
    return;
  }
  int dpeIdx, typeIdx, nDpes = dynlen(names);
  for(dpeIdx = 1 ; dpeIdx <= nDpes ; dpeIdx++)
  {
    typeIdx = dynlen(types[dpeIdx]);
    int type = types[dpeIdx][typeIdx];
    if((type == DPEL_STRUCT) || // Structure
       (type == DPEL_BIT32_STRUCT) || (type == DPEL_BLOB_STRUCT) || (type == DPEL_BOOL_STRUCT) ||
      (type == DPEL_CHAR_STRUCT) || (type == DPEL_DPID_STRUCT) || (type == DPEL_DYN_BIT32_STRUCT) ||
      (type == DPEL_DYN_BLOB_STRUCT) || (type == DPEL_DYN_BOOL_STRUCT) || (type == DPEL_DYN_CHAR_STRUCT) ||
      (type == DPEL_DYN_DPID_STRUCT) || (type == DPEL_DYN_FLOAT_STRUCT) || (type == DPEL_DYN_INT_STRUCT) ||
      (type == DPEL_DYN_LANGSTRING_STRUCT) || (type == DPEL_DYN_STRING_STRUCT) || (type == DPEL_DYN_TIME_STRUCT) ||
      (type == DPEL_DYN_UINT_STRUCT) || (type == DPEL_FLOAT_STRUCT) || (type == DPEL_INT_STRUCT) ||
      (type == DPEL_LANGSTRING_STRUCT) || (type == DPEL_STRING_STRUCT) || (type == DPEL_TIME_STRUCT) ||
      (type == DPEL_UINT_STRUCT))
   {
      if(dynlen(dpeTree) >= typeIdx)
      {
        dpeTree[typeIdx] = names[dpeIdx][typeIdx];
      }
      else
      {
        dynAppend(dpeTree, names[dpeIdx][typeIdx]);
      }
      continue; 
    }
    string dpeName = "";
    for(int n = 2 ; n < typeIdx ; n++)	// Start from 2 because 1 is DP type name
    {
      if(dpeName != "")
      {
        dpeName += ".";
      }
      dpeName += dpeTree[n];
    }
    if(dpeName != "")
    {
      dpeName += ".";
    }
    dpeName += names[dpeIdx][typeIdx];
    dynAppend(allDpeNames, dpeName);
    dynAppend(allDpeTypes, type);
  }
}

/**
@brief Active or desactive address config of all dpe for a dp
@param[in]        dpName        DP name         
@param[in]        setActive     If TRUE active, if FALSE desactive
@param[in, out]   exceptionInfo Exception Info
*/
void SetAddrActiveConfig (string dpName, bool setActive, dyn_string &exceptionInfo) {
  dyn_string allDpeNames;
  dyn_int allDpeTypes;
  string dpeFullName;
  
  GetDpeListOfDpType(dpName, allDpeNames, allDpeTypes, exceptionInfo);
  //DebugTN("list dpe: ", allDpeNames);
  bool addrExist, isAddrActive;
  dyn_anytype config;

  for (int n = 1; n <= dynlen(allDpeNames); n++) {
    dpeFullName = dpName + "." + allDpeNames[n];
    //DebugTN("SetAddrActiveConfig(): n=", n);
    fwPeriphAddress_get(dpeFullName, addrExist, config, isAddrActive, exceptionInfo);
    if (addrExist) {
      dpSetWait(dpeFullName + ":_address.._active", setActive);
      //DebugTN("dpeFullName: ", dpeFullName);
    }
  }
}

/**
@brief Copy address config of all dpe
@details Return dpCopyConfig() err code
@param[in]        dpSource     Config copied from         
@param[in]        dpTarget     Config copied to
@param[in, out]   exceptionInfo Exception Info
*/
int CopyAddrConfigAllDpe(string dpSource, string dpTarget, dyn_string &exceptionInfo) {
  int ret = -1;
  int dpCopyRet;
  dyn_string allDpeNames;
  dyn_int allDpeTypes;
  
  GetDpeListOfDpType(dpSource, allDpeNames, allDpeTypes, exceptionInfo);
  for (int n = dynlen(allDpeNames); n > 0; n--) { 
    dpCopyConfig(dpSource + "." + allDpeNames[n], dpTarget + "." + allDpeNames[n], makeDynString("_distrib", "_address", "_smooth", "_cmd_conv", "_msg_conv"), dpCopyRet);
    if (dpCopyRet != 0)
      ret = dpCopyRet;    
  }
  return ret;
}
