//	Implementation of EqpTreeItem class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpTreeItem.h"
#include "MobileColorList.h"
#include "BakeoutChannelTreeItem.h"

#include "Eqp.h"
#include "MobileType.h"

#include <qpixmap.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

EqpTreeItem::EqpTreeItem(QTreeWidgetItem *parent, Eqp *pEqp, int index)
	: QTreeWidgetItem(parent, MobileTreeItemEqp)
{
	this->pEqp = pEqp;
	this->index = index;
	setText(0, pEqp->getVisibleName());
	QPixmap *pm = MobileColorList::getPrIcon(index);
	setIcon(0, QIcon(*pm));
	setFlags(flags() | Qt::ItemIsUserCheckable);
	setCheckState(0, Qt::Unchecked);
}

EqpTreeItem::~EqpTreeItem()
{
}

