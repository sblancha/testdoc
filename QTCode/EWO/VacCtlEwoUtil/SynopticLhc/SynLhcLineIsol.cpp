//	Implementation of SynLhcLineIsol class
/////////////////////////////////////////////////////////////////////////////////

#include "SynLhcLineIsol.h"
#include "VacLinePart.h"

#include "DataPool.h"
#include "Eqp.h"
#include "EqpType.h"
#include "EqpMsgCriteria.h"

#include "VacIcon.h"
#include "VacMainView.h"
#include "DebugCtl.h"

#include <qwidget.h>
#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

SynLhcLineIsol::SynLhcLineIsol(QWidget *parent, int type, float start, float end) :
	SynLhcLine(parent, type, start, end)
{
}

SynLhcLineIsol::~SynLhcLineIsol()
{
}

/*
**	FUNCTION
**		Analyze which equipment types will be shown in view
**
**	PARAMETERS
**		allVacTypeMask	- OR'ed bit mask for all vacuum types
**		allCryoEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**							to be shown in this view
**		allBeamEqpMask	- Total equipment type mask for all equipment on beam vacuum
**							to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineIsol::analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allCryoEqpMask, VacEqpTypeMask & /* allBeamEqpMask */)
{
	analyzeTypes(allVacTypeMask, allCryoEqpMask);
}

/*
**	FUNCTION
**		Analyze which equipment types will be shown in view
**
**	PARAMETERS
**		allVacTypeMask	- OR'ed bit mask for all vacuum types
**		allEqpMask		- Total equipment type mask for all equipment to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineIsol::analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allEqpMask)
{
	if(!pPartList->count())
	{
		buildLineParts();
	}

	// Preserve current mask settings
	unsigned	oldVacTypeMask = vacTypeMask;
	VacEqpTypeMask	oldEqpMask = eqpMask;

	// Set new masks allowing all equipment
	vacTypeMask = 0xFFFFFFFF;
	eqpMask = VacEqpTypeMask::getAllTypes();
	doNotAddEqp = true;

	// All parts are pure vacuum sectors
	for(int linePartIdx = 0 ; linePartIdx < pPartList->count() ; linePartIdx++)
	{
		VacLinePart *pPart = pPartList->at(linePartIdx);
		Sector *pSector = pPart->getSector();

		// .Q and .M sectors - LHC ring, DSL - other lines
		BeamLine *pLine = NULL;
		bool	useDSL = false;
		if((pSector->getVacType() == VacType::Qrl) || (pSector->getVacType() == VacType::Cryo))
		{
			pLine = DataPool::getInstance().getCircularLine();
		}
		else	// other vac. types - DSL, it is expected that line only contains one sector
		{
			QList<BeamLine *> &lines = DataPool::getInstance().getLines();
			for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
			{
				BeamLine *pBeamLine = lines.at(lineIdx);
				if(pBeamLine->isCircle())
				{
					continue;
				}
				QList<BeamLinePart *> &lineParts = pBeamLine->getParts();
				for(int partIdx = 0 ; partIdx < lineParts.count() ; partIdx++)
				{
					BeamLinePart *pLinePart = lineParts.at(partIdx);
					QList<Eqp *> &eqpList = pLinePart->getEqpList();
					for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
					{
						Eqp *pEqp = eqpList.at(eqpIdx);
						if(pEqp->getSectorBefore() == pSector)
						{
							pLine = pBeamLine;
							break;
						}
					}
					if(pLine)
					{
						break;
					}
				}
				if(pLine)
				{
					break;
				}
			}
			useDSL = true;
		}
		if(!pLine)
		{
			continue;
		}

		QList<BeamLinePart *> &lineParts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < lineParts.count() ; partIdx++)
		{
			BeamLinePart *pLinePart = lineParts.at(partIdx);
			Eqp **eqpPtrs = pLinePart->getEqpPtrs();
			int nEqpPtrs = pLinePart->getNEqpPtrs();
			for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if(isMyEqp(pEqp, linePartIdx))
				{
					if(useDSL)
					{
						allEqpMask.append(pEqp->getFunctionalType());
					}
					else
					{
						allVacTypeMask |= pEqp->getVacType() & (VacType::Qrl | VacType::Cryo);
						allEqpMask.append(pEqp->getFunctionalType());
					}
				}
			}
		}
	}

	// Restor original masks
	vacTypeMask = oldVacTypeMask;
	eqpMask = oldEqpMask;
	doNotAddEqp = false;
}

/*
**	FUNCTION
**		Build list of equipment for this line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineIsol::buildEquipment(void)
{
	// All parts are pure vacuum sectors
	for(int linePartIdx = 0 ; linePartIdx < pPartList->count() ; linePartIdx++)
	{
		VacLinePart *pPart = pPartList->at(linePartIdx);
		Sector *pSector = pPart->getSector();
/*
qDebug("SynLhcLineIsol::buildEquipment(): sector %s\n", pSector->getName());
*/

		// .Q and .M sectors - LHC ring, DSL - other lines
		BeamLine *pLine = NULL;
		bool	useDSL = false;
		if((pSector->getVacType() == VacType::Qrl) || (pSector->getVacType() == VacType::Cryo))
		{
			pLine = DataPool::getInstance().getCircularLine();
		}
		else	// other vac. types - DSL, it is expected that line only contains one sector
		{
			QList<BeamLine *> &lines = DataPool::getInstance().getLines();
			for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
			{
				BeamLine *pBeamLine = lines.at(lineIdx);
				if(pBeamLine->isCircle())
				{
					continue;
				}
				QList<BeamLinePart *> &lineParts = pBeamLine->getParts();
				for(int partIdx = 0 ; partIdx < lineParts.count() ; partIdx++)
				{
					BeamLinePart *pLinePart = lineParts.at(partIdx);
					QList<Eqp *> &eqpList = pLinePart->getEqpList();
					for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
					{
						Eqp *pEqp = eqpList.at(eqpIdx);
						if(pEqp->getSectorBefore() == pSector)
						{
							pLine = pBeamLine;
							break;
						}
					}
					if(pLine)
					{
						break;
					}
				}
				if(pLine)
				{
					break;
				}
			}
			useDSL = true;
		}
		if(!pLine)
		{
			continue;
		}

		// Coordinates for equipment on DSL line will be changed to be within
		// coordinate range of DSL sector.
		// It is also important that equipment with the same 'real' coordinates
		// will have the same 'synoptic' coordinates in order to join such
		// equipment to groups
		float	dslEqpCoord = pPart->getRealStart(),
				lastRealEqpCoord = -DataPool::getInstance().getLhcRingLength();

		QList<BeamLinePart *> &lineParts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < lineParts.count() ; partIdx++)
		{
			BeamLinePart *pLinePart = lineParts.at(partIdx);
			Eqp **eqpPtrs = pLinePart->getEqpPtrs();
			int nEqpPtrs = pLinePart->getNEqpPtrs();
			for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if(isMyEqp(pEqp, linePartIdx))
				{
					// For DSL line replace real coordinate with some coordinate lying
					// within VERY short coordinate range for sector
					if(useDSL)
					{
						float eqpCoord = pEqp->getStart();
						if(lastRealEqpCoord != eqpCoord)
						{
							dslEqpCoord += (float)0.001;
							lastRealEqpCoord = eqpCoord;
						}
						pEqp->setStart(dslEqpCoord);
						addEqp(pEqp, linePartIdx);
						pEqp->setStart(eqpCoord);
					}
					else
					{
						addEqp(pEqp, linePartIdx);
					}
				}
			}
		}
		// L.Kopylov 13.12.2012 Add equipment of DSL which is located on LHC ring
		/*
		qDebug("SynLhcLineIsol::buildEquipment(): useDSL = %d\n", (int)useDSL);
		*/
		if(useDSL)
		{
			pLine = DataPool::getInstance().getCircularLine();
			if(!pLine)
			{
				continue;
			}
			QList<BeamLinePart *> &lineParts = pLine->getParts();
			for(int partIdx = 0 ; partIdx < lineParts.count() ; partIdx++)
			{
				BeamLinePart *pLinePart = lineParts.at(partIdx);
				Eqp **eqpPtrs = pLinePart->getEqpPtrs();
				int nEqpPtrs = pLinePart->getNEqpPtrs();
//				qDebug("SynLhcLineIsol::buildEquipment(): check %d devices on line\n", nEqpPtrs);
				for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(isMyEqp(pEqp, linePartIdx))
					{
						float eqpCoord = pEqp->getStart();
						if(lastRealEqpCoord != eqpCoord)
						{
							dslEqpCoord += (float)0.001;
							lastRealEqpCoord = eqpCoord;
						}
						pEqp->setStart(dslEqpCoord);
						addEqp(pEqp, linePartIdx);
						pEqp->setStart(eqpCoord);
					}
				}
			}
		}

	}
}

bool SynLhcLineIsol::findDevicePosition(Eqp *pEqpToFind, int &eqpPosition, bool &eqpVisible)
{
	//qDebug("SynLhcLineIsol::findDevicePosition(%s): funcType %d, searching...\n", pEqpToFind->getDpName(), pEqpToFind->getFunctionalType());
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		if(pItem->getEqp() == pEqpToFind)
		{
			eqpPosition = pItem->getRect().x();
			eqpVisible = true;
			return true;
		}
	}

	//qDebug("SynLhcLineIsol::findDevicePosition(%s): not found, searching in hidden...\n", pEqpToFind->getDpName());
	// Not found... equipment may be hidden by equipment type mask
	eqpVisible = false;
	eqpPosition = 0;
	bool result = false;

	VacEqpTypeMask	oldEqpMask = eqpMask;
	eqpMask = VacEqpTypeMask::getAllTypes();
	doNotAddEqp = true;

	// All parts are pure vacuum sectors
	for(int linePartIdx = 0 ; linePartIdx < pPartList->count() ; linePartIdx++)
	{
		VacLinePart *pPart = pPartList->at(linePartIdx);
		Sector *pSector = pPart->getSector();

		// .Q and .M sectors - LHC ring, DSL - other lines
		BeamLine *pLine = NULL;
		if((pSector->getVacType() == VacType::Qrl) || (pSector->getVacType() == VacType::Cryo))
		{
			pLine = DataPool::getInstance().getCircularLine();
		}
		else	// other vac. types - DSL, it is expected that line only contains one sector
		{
			QList<BeamLine *> &lines = DataPool::getInstance().getLines();
			for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
			{
				BeamLine *pBeamLine = lines.at(lineIdx);
				if(pBeamLine->isCircle())
				{
					continue;
				}
				QList<BeamLinePart *> &lineParts = pBeamLine->getParts();
				for(int partIdx = 0 ; partIdx < lineParts.count() ; partIdx++)
				{
					BeamLinePart *pLinePart = lineParts.at(partIdx);
					QList<Eqp *> &eqpList = pLinePart->getEqpList();
					for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
					{
						Eqp *pEqp = eqpList.at(eqpIdx);
						if(pEqp->getSectorBefore() == pSector)
						{
							pLine = pBeamLine;
							break;
						}
					}
					if(pLine)
					{
						break;
					}
				}
				if(pLine)
				{
					break;
				}
			}
		}
		if(!pLine)
		{
			continue;
		}

		QList<BeamLinePart *> &lineParts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < lineParts.count() ; partIdx++)
		{
			BeamLinePart *pLinePart = lineParts.at(partIdx);
			Eqp **eqpPtrs = pLinePart->getEqpPtrs();
			int nEqpPtrs = pLinePart->getNEqpPtrs();
			for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if(pEqp == pEqpToFind)
				{
					result = isMyEqp(pEqp, linePartIdx);
					// qDebug("SynLhcLineIsol::findDevicePosition(%s): found in hidden, result %d\n", pEqpToFind->getDpName(), (int)result);
					break;
				}
			}
		}
		if(result)
		{
			break;
		}
	}
	eqpMask = oldEqpMask;
	doNotAddEqp = false;
	return result;
}

/*
**	FUNCTION
**		The method is called after locations for everything on view
**		have been calculated. There are two major jobs to do:
**		1) Decide if one more sector label shall be added after
**		last sector border - items placement algorythm did not add
**		sector label after last sector border.
**		2) Decide if sector borders shall be drawn or not. Sector border
**		shall NOT be drawn if:
**		a) sector corresponds to line part AND
**		b) corresponding line part border shall be drawn
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineIsol::decideForSectorBorders(void)
{
	SynLhcSectorLabel *pLabel;
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		pLabel = sectors.at(sectIdx);
		VacLinePart *pPart = NULL;
		for(int idx = 0 ; idx < pPartList->count() ; idx++)
		{
			VacLinePart *pLinePart = pPartList->at(idx);
			if((pLinePart->getStartX() == pLabel->getStart()) &&
				(pLinePart->getEndX() == pLabel->getEnd()))
			{
				pPart = pLinePart;
				break;
			}
		}
		if(!pPart)
		{
			pLabel->setDrawStart(pLabel->getSector()->getStart() >= start);
			pLabel->setDrawEnd(pLabel->getSector()->getEnd() <= end);
		}
		else
		{
			if(pPart->isDrawStart())	// Start of line part is drawn - start of sector is not needed
			{
				pLabel->setDrawStart(false);
			}
			else if(pPart->getRealStart() < start)	// Start of line part is not drawn because it is not visible
			{
				pLabel->setDrawStart(false);
			}
			else
			{
				pLabel->setDrawStart(true);
			}

			if(pPart->isDrawEnd())	// End of line part is drawn - start of sector is not needed
			{
				pLabel->setDrawEnd(false);
			}
			else if(pPart->getRealEnd() > end)	// End of line part is not drawn because it is not visible
			{
				pLabel->setDrawEnd(false);
			}
			else
			{
				pLabel->setDrawEnd(true);
			}
		}
	}

	// Check if there is enough space after the very last sector border to draw sector label
	if(sectors.isEmpty())
	{
		return;
	}
	pLabel = sectors.last();
	Sector *pSector = pLabel->getSector();
	int	sectStart = pLabel->getStart();
	int	sectEnd = sectStart;
	QRect labelRect = parent->fontMetrics().boundingRect(pSector->getName());
	sectEnd = sectStart + 2 * MIN_ACTIVE_SPACING + labelRect.width();
	if(labelRect.height() > maxSectHeight)
	{
		maxSectHeight = labelRect.height();
	}
	if(endX < sectEnd)
	{
		endX = sectEnd;
	}
}

/*
**	FUNCTION
**		Check if sector shall be used for drawing line parts of this line
**
**	PARAMETERS
**		pSect	- Pointer to sector in question
**
**	RETURNS
**		true	- if sector can be used for this synoptic line
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SynLhcLineIsol::isMySector(Sector *pSect)
{
	switch(pSect->getVacType())
	{
	case VacType::Qrl:
		if(type == Qrl)
		{
			return true;
		}
		break;
	case VacType::Cryo:
		if(type == Cryo)
		{
			return true;
		}
		break;
	}
	return false;
}

/*
**	FUNCTION
**		Check if given device shall appear in this synoptic line. Isolation vacuum
**		is sector-based, so sector is a main criteria here
**
**	PARAMETERS
**		pEqp	- Pointer to device in question
**		partIdx	- Index of line part being checked
**
**	RETURNS
**		true	- If device shall be used in this synoptic;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SynLhcLineIsol::isMyEqp(Eqp *pEqp, int &partIdx)
{
	switch(pEqp->getType())
	{
	case EqpType::VacDevice:
		break;
	default:
		return false;
	}

	/*
	if(!strcmp(pEqp->getDpName(), "VGP_474_7R3_D"))
	{
		qDebug("SynLhcLineIsol::isMyEqp(%s): 0\n", pEqp->getDpName());
	}
	*/

	// Controllable vacuum equipment is checked here
	if(pEqp->isSkipOnSynoptic())
	{
		return false;
	}
	// Check if equipment is active or if show hiden mobile is selected
	if (!pEqp->isActive(mode) && !(isShowMobiles &&
									((pEqp->getFunctionalType() == FunctionalType::VPG) ||
									 (pEqp->getFunctionalType() == FunctionalType::VRE))
								   )
		) { // Check if mobile 
		return false;
	}
	// Add device if it matches equipment type masks
	if(eqpMask.contains(pEqp->getFunctionalType()))
	{
		int iconType = VacIcon::getIconType(pEqp);
		if(iconType != VacIconContainer::None)	// Here vacuum type does not matter
		{
			/*
			if(!strcmp(pEqp->getDpName(), "VGP_474_7R3_D"))
			{
				qDebug("SynLhcLineIsol::isMyEqp(%s): 2\n", pEqp->getDpName());
			}
			*/

			// If device has controllable parent - do not add device, it will be part of parent
			if(pEqp->getAttrValue("CtlParent").isEmpty())
			{
				/*
				if(!strcmp(pEqp->getDpName(), "VGP_474_7R3_D"))
				{
					qDebug("SynLhcLineIsol::isMyEqp(%s): 3\n", pEqp->getDpName());
				}
				*/

				// Check sector
				VacLinePart *pPart = pPartList->at(partIdx);
				if((pEqp->getSectorBefore() == pPart->getSector()) ||
					(pEqp->getSectorAfter() == pPart->getSector()))
				{
					/*
					if(!strcmp(pEqp->getDpName(), "VGP_474_7R3_D"))
					{
						qDebug("SynLhcLineIsol::isMyEqp(%s): 4\n", pEqp->getDpName());
					}
					*/

					if(!doNotAddEqp)	// Otherwise we are just analyzing eqp. types
					{
						// It can be that the same device (sitting on sector border)
						// has been added already
						for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
						{
							SynLhcEqpItem *pItem = eqps.at(idx);
							if(pItem->getEqp() == pEqp)
							{
								return false;
							}
						}
					}
					// Find in which part equipment is located
					// part index is known in advance partIdx = findPartIdx(pEqp);
					/*
					if(!strcmp(pEqp->getDpName(), "VGP_474_7R3_D"))
					{
						qDebug("SynLhcLineIsol::isMyEqp(%s): 5, vacType %d\n", pEqp->getDpName(), pEqp->getVacType());
					}
					*/

					return vacTypeMatchesMask(pEqp->getVacType());
				}
				else
				{
					/*
					if(!strcmp(pEqp->getDpName(), "VGP_474_7R3_D"))
					{
						qDebug("SynLhcLineIsol::isMyEqp(%s): 4 sector %s does not match %s\n", pEqp->getDpName(), pPart->getSector()->getName(), pEqp->getSectorBefore()->getName());
					}
					*/
				}
			}
		}
	}
	return false;
}

/*
**	FUNCTION
**		Calculate minimum allowed position for equipment at sector border
**
**	PARAMETERS
**		pItem	- Pointer to equipment item being processed
**		connPos	- Minimum allowed position not taking into account sector name
**
**	RETURNS
**		New minimum allowed position taking into account sector name
**
**	CAUTIONS
**		None
*/
int SynLhcLineIsol::calcSectorBorderPosition(SynLhcEqpItem *pItem, int connPos)
{
	// Try to find previous sector border
	int idx;
	SynLhcEqpItem *pOther;
	for(idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		pOther = eqps.at(idx);
		if(pOther == pItem)
		{
			break;
		}
	}

	int sectStart = 0, sectEnd = 0;
	bool found = false;
	float sectEndPos = 0;
	for(idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		pOther = eqps.at(idx);
		if(pOther->isSectorBorder())
		{
			sectStart = sectEnd = pOther->getConnPoint().x();
			found = true;
			sectEndPos = pItem->getEqp()->getStart();
			// Sector start shall not be before start of current line part
			VacLinePart *pPart = pPartList->at(curPartIdx);
			if(pPart)
			{
				if(sectStart < pPart->getStartX())
				{
					sectStart = sectEnd = pPart->getStartX();
				}
			}
			break;
		}
	}

	if(!found)	// No previous sector border, use start of current line part
	{
		VacLinePart *pPart = pPartList->at(curPartIdx);
		if(pPart)
		{
			sectStart = sectEnd = pPart->getStartX();
			sectEndPos = pPart->getStartPos();
			found = true;
		}
	}
	else	// Can't extend behind line part
	{
		VacLinePart *pPart = pPartList->at(curPartIdx);
		if(pPart)
		{
			if(pPart->getStartX() > sectStart)
			{
				sectStart = sectEnd = pPart->getStartX();
				sectEndPos = pPart->getStartPos();
			}
		}
	}
	if((!found) && (!pPartList->count()))
	{
		sectStart = sectEnd = VAC_LINE_SPACING;
		sectEndPos = 0.0;
		found = true;
	}
	if(found)
	{
		Sector *pSector = pItem->getEqp()->getSectorBefore();
		const char *sectName = pSector->getName();
		QRect labelRect = parent->fontMetrics().boundingRect(sectName);
		sectEnd += 2 * MIN_ACTIVE_SPACING + labelRect.width();
		if(labelRect.height() > maxSectHeight)
		{
			maxSectHeight = labelRect.height();
		}
		if(sectEnd > connPos)
		{
			connPos = sectEnd;
		}
		else
		{
			sectEnd = connPos;
		}
		addSector(pSector, true, sectStart, sectEnd, sectEndPos, pItem->getStartOrder());
	}
	return connPos;
}

/*
**	FUNCTION
**		Calculate vertical limits for line. In contasrt to 'generic' method
**		(SynLhcLine) this only takes into account equipment on correct side
**		of line - in order not to take into account valves on CRYO line.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineIsol::calcVerticalLimits(void)
{
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		int yPos = pItem->getRect().top();
		if(yPos < 0)
		{
			if((abs(yPos) > ascent) && eqpAboveLine)
			{
				ascent = abs(yPos);
			}
		}
		else
		{
			if((abs(yPos) > descent) && (!eqpAboveLine))
			{
				descent = abs(yPos);
			}
		}
		yPos = pItem->getConnPoint().y();
		if(yPos < 0)
		{
			if((abs(yPos) > ascent) && eqpAboveLine)
			{
				ascent = abs(yPos);
			}
		}
		else
		{
			if((abs(yPos) > descent) && (!eqpAboveLine))
			{
				descent = abs(yPos);
			}
		}
		yPos = pItem->getRect().bottom();
		if(yPos < 0)
		{
			if((abs(yPos) > ascent) && eqpAboveLine)
			{
				ascent = abs(yPos);
			}
		}
		else
		{
			if((abs(yPos) > descent) && (!eqpAboveLine))
			{
				descent = abs(yPos);
			}
		}
	}
	// Add vertical space for sector labels
	if(eqpAboveLine)
	{
		// if( yDescent < ( maxSectHeight + 4 ) ) yDescent = maxSectHeight + 4;
		if(maxSectHeight)
		{
			descent += maxSectHeight + 14;
		}
	}
	else
	{
		// if( yAscent < ( maxSectHeight + 4 ) ) yAscent = maxSectHeight + 4;
		if(maxSectHeight)
		{
			ascent += maxSectHeight + 14;
		}
	}
}

/*
**	FUNCTION
**		Draw start of line part
**
**	PARAMETERS
**		painter	- Painter used for drawing
**		coord	- X coordinate of line start
**
**	RETURNS
**		None
**
**	CAUTIONS
**		It is expected that appropriate pan is set in painter before
**		invoking this method
*/
void SynLhcLineIsol::drawPartStart(QPainter &painter, int coord)
{
	QPolygon points(4);
	points.setPoint(0, coord + 2, y - LINE_START_END_HEIGHT - 2);
	points.setPoint(1, coord, y - LINE_START_END_HEIGHT);
	points.setPoint(2, coord, y + LINE_START_END_HEIGHT);
	points.setPoint(3, coord + 2, y + LINE_START_END_HEIGHT + 2);
	painter.drawPolyline(points);
	/*
	painter.drawLine(coord + 2, y - LINE_START_END_HEIGHT - 2,
		coord, y - LINE_START_END_HEIGHT);
	painter.drawLine(coord, y - LINE_START_END_HEIGHT,
		coord, y + LINE_START_END_HEIGHT);
	painter.drawLine(coord, y + LINE_START_END_HEIGHT,
		coord + 2, y + LINE_START_END_HEIGHT + 2);
	*/
}

/*
**	FUNCTION
**		Draw end of line part
**
**	PARAMETERS
**		painter	- Painter used for drawing
**		coord	- X coordinate of line end
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void SynLhcLineIsol::drawPartEnd(QPainter &painter, int coord)
{
	QPolygon points(4);
	points.setPoint(0, coord - 2, y - LINE_START_END_HEIGHT - 2);
	points.setPoint(1, coord, y - LINE_START_END_HEIGHT);
	points.setPoint(2, coord, y + LINE_START_END_HEIGHT);
	points.setPoint(3, coord - 2, y + LINE_START_END_HEIGHT + 2);
	painter.drawPolyline(points);
	/*
	painter.drawLine(coord - 2, y - LINE_START_END_HEIGHT - 2,
		coord, y - LINE_START_END_HEIGHT);
	painter.drawLine(coord, y - LINE_START_END_HEIGHT,
		coord, y + LINE_START_END_HEIGHT);
	painter.drawLine(coord, y + LINE_START_END_HEIGHT,
		coord - 2, y + LINE_START_END_HEIGHT + 2);
	*/
}

/*
**	FUNCTION
**		Draw sector borders and sector labels
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		viewStart	- Start X-coordinate of visible area
**		viewEnd		- End X-coordinate of visible area
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineIsol::drawSectLabels(QPainter &painter, int viewStart, int viewEnd)
{
	painter.setPen(VacMainView::getSynSectorColor());
	for(int idx = 0 ; idx < sectors.count() ; idx++)
	{
		SynLhcSectorLabel *pSect = sectors.at(idx);
		if(pSect->getEnd() < viewStart)
		{
			continue;
		}
		if(pSect->getStart() > viewEnd)
		{
			continue;
		}
		// Sector borders
		int sectStart = pSect->getStart();
		if(pSect->isDrawStart())
		{
			painter.drawLine(sectStart, y - ascent, sectStart, y + descent);
		}
		int sectEnd = pSect->getEnd();
		if(pSect->isDrawEnd())
		{
			painter.drawLine(sectEnd, y - ascent, sectEnd, y + descent);
		}

		// Label is drawn in the middle of visible sector area
		sectStart += MIN_ACTIVE_SPACING;
		sectEnd -= MIN_ACTIVE_SPACING;
		QRect labelRect = painter.fontMetrics().boundingRect(pSect->getSector()->getName());
		int txtX = sectStart + ((sectEnd - sectStart - labelRect.width()) >> 1);
		/* LIK 02.06.2009
		if(txtX > (viewEnd - MIN_ACTIVE_SPACING - labelRect.width()))
		{
			txtX = viewEnd - MIN_ACTIVE_SPACING - labelRect.width();
		}
		if(txtX < (pSect->getStart() + MIN_ACTIVE_SPACING))
		{
			txtX = pSect->getStart() + MIN_ACTIVE_SPACING;
		}
		if(txtX > (pSect->getEnd() - labelRect.width() - MIN_ACTIVE_SPACING))
		{
			txtX = pSect->getEnd() - labelRect.width() - MIN_ACTIVE_SPACING;
		}
		*/
		/*
		txtY = eqpAboveLine ?
			lineY + pParam->beamLineWidth :
			lineY - pParam->beamLineWidth - maxSectHeight;
		*/
		int txtY = eqpAboveLine ?
			y + descent :
			y - ascent + maxSectHeight;
		painter.drawText(txtX, txtY, pSect->getSector()->getName());
	}
}

/*
**	FUNCTION
**		[VACCO-929]
**		Finds the start and end coordinates of each sector represented in the synoptic.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineIsol::setSectorGeometry(void)
{
	for (int idx = 0; idx < sectors.count(); idx++){
		SynLhcSectorLabel *pSectorLabel = sectors.at(idx);
		pSectorLabel->addToGraph(pSectorLabel->getStart(), y, pSectorLabel->getEnd(), y);
	}
}
