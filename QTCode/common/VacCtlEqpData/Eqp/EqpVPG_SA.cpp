//	Implementation of EqpVPG_SA class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVPG_SA.h"

#include "DataPool.h"
#include "FunctionalTypeVPGF.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define	COLOR_ERROR				255,102,102
#define	COLOR_WARNING			255,255,0
#define	COLOR_TMP_ACCELERATE	255,255,0

#define	COLOR_OPEN				0,255,0
#define	COLOR_CLOSED			255,0,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_ON			(0x00020000)
#define	RR1_OFF			(0x00010000)
#define RR1_AL3			(0x00008000)
#define RR1_AL2			(0x00004000)
#define RR1_AL1			(0x00002000)
#define RR1_ERROR_CODE_MASK	(0x000000FF)

#define	RR2_VVD_OPEN			(0x20000000)
#define	RR2_VVD_CLOSED			(0x10000000)
#define	RR2_VVI_OPEN			(0x08000000)
#define	RR2_VVI_CLOSED			(0x04000000)
#define	RR2_VVP_OPEN			(0x02000000)
#define	RR2_VVP_CLOSED			(0x01000000)
#define	RR2_TMP_SPEED_OK		(0x00400000)
#define	RR2_TMP_STAND_BY		(0x00200000)
#define	RR2_TMP_OK				(0x00100000)
#define	RR2_TMP_ON				(0x00080000)
#define	RR2_TMP_START_ENABLE	(0x00040000)
#define	RR2_PP_OK				(0x00020000)
#define	RR2_PP_ON				(0x00010000)
#define	RR2_VVR_ERROR			(0x00008000)
#define	RR2_VVR_WARNING			(0x00004000)
#define	RR2_VVR_INTERLOCK		(0x00002000)
#define	RR2_VVR_OPEN	(0x00000200)
#define	RR2_VVR_CLOSED	(0x00000100)


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

EqpVPG_SA::EqpVPG_SA(const Eqp &source) : Eqp(source)
{
	rr1Online = rr1Replay = rr2Online = rr2Replay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::VPGF;
}

EqpVPG_SA::~EqpVPG_SA()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPG_SA::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "RR2");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPG_SA::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "RR2", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPG_SA::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "RR2", mode);
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "EqpStatus", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPG_SA::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Online == newValue)
			{
				return;
			}
			rr2Online = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Replay == newValue)
			{
				return;
			}
			rr2Replay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPG_SA::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, rr2Replay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, rr2Online, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getMainColor(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(rr1 & RR1_ERROR)
	{
		color.setRgb(COLOR_ERROR);
	}
	// No errors - check ON/OFF state
	else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if(rr1 & RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else if(rr1 & RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, rr2Online, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, rr2Online, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getMainStateString(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
		return;
	}
	if(rr1 & RR1_ERROR)	// Still different strings for ON/OFF
	{
		if(rr1 & RR1_ON)
		{
			if(rr1 & RR1_OFF)
			{
				string = "Error";
			}
			else
			{
				string = "ON (error)";
			}
		}
		else if(rr1 & RR1_OFF)
		{
			string = "OFF (error)";
		}
		else
		{
			string = "Error";
		}
	}
	// No errors - check ON/OFF state
	else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		string = "Error";
	}
	else if(rr1 & RR1_WARNING)
	{
		if(rr1 & RR1_ON)
		{
			if(rr1 & RR1_OFF)
			{
				string = "Error";
			}
			else
			{
				string = "ON (warning)";
			}
		}
		else if(rr1 & RR1_OFF)
		{
			string = "OFF (warning)";
		}
		else
		{
			string = "Undefined (warning)";
		}
	}
	else if(rr1 & RR1_ON)
	{
		string = "ON";
	}
	else if(rr1 & RR1_OFF)
	{
		string = "OFF";
	}
	else
	{
		string = "Undefined";
	}

	// add to string state of VVR
	if((rr2 & RR2_VVR_OPEN) && (rr2 & RR2_VVR_CLOSED))
	{
		string += ", VVR error";
	}
	else if(rr2 & RR2_VVR_OPEN)
	{
		string += ", VVR open";
	}
	else if(rr2 & RR2_VVR_CLOSED)
	{
		string += ", VVR closed";
	}
	else
	{
		string += ", VVR undefined";
	}
}
#endif


/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("RR1");
	dpeNames.append("RR2");
	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	if (values.isEmpty()) {
		return;
	}
	pVariant = values.takeFirst();
	unsigned rr2 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, rr2, false, state);
	getMainColor(rr1, rr2, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif


/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "EqpStatus");
	addDpeNameToList(dpes, "RR1");
	addDpeNameToList(dpes, "RR2");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVPG_SA::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if((rr1 & (RR1_ON | RR1_OFF)) == RR1_ON)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if((rr1 & (RR1_ON | RR1_OFF)) == RR1_OFF)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if(rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if(criteria.isWarnings())
	{
		if(rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}
	if(criteria.isAlerts())
	{
		if(rr1 & (RR1_AL1 | RR1_AL2 | RR1_AL3))
		{
			return true;
		}
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Calculate color for given part of device
**
**	ARGUMENTS
**		part	- which part of device is requested (see EqpPart enum in Eqp.h)
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartColor(int part, QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	unsigned useRr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	unsigned useRr2 = mode == DataEnum::Replay ? rr2Replay : rr2Online;
	bool useAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	switch(part)
	{
	case EqpPartPump:
		getPartColorPump(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVVR:
		getPartColorVVR(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartPP:
		getPartColorPP(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartTMP:
		getPartColorTMP(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVVD:
		getPartColorVVD(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVVI:
		getPartColorVVI(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVVP:
		getPartColorVVP(useRr1, useRr2, useAlarm, color);
		break;
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for pump part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartColorPump(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if((rr1 & (RR1_ON | RR1_OFF)) == RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else if((rr1 & (RR1_ON | RR1_OFF)) == RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else if(rr1 & (RR1_ON | RR1_OFF))
	{
		color.setRgb(COLOR_ERROR);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for VVR part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartColorVVR(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr2 & RR2_VVR_OPEN)
	{
		if(rr2 & RR2_VVR_CLOSED)
		{
			color.setRgb(COLOR_ERROR);
		}
		else
		{
			color.setRgb(COLOR_OPEN);
		}
	}
	else if(rr2 & RR2_VVR_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for PP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartColorPP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr2 & RR2_PP_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else
	{
		color.setRgb(COLOR_OFF);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for TMP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartColorTMP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr2 & RR2_TMP_ON)
	{
		if(rr2 & RR2_TMP_SPEED_OK)
		{
			color.setRgb(COLOR_ON);
		}
		else
		{
			color.setRgb(COLOR_TMP_ACCELERATE);
		}
	}
	else
	{
		color.setRgb(COLOR_OFF);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for VVD part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartColorVVD(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr2 & RR2_VVD_OPEN)
	{
		if(rr2 & RR2_VVD_CLOSED)
		{
			color.setRgb(COLOR_ERROR);
		}
		else
		{
			color.setRgb(COLOR_OPEN);
		}
	}
	else if(rr2 & RR2_VVD_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for VVI part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartColorVVI(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr2 & RR2_VVI_OPEN)
	{
		if(rr2 & RR2_VVI_CLOSED)
		{
			color.setRgb(COLOR_ERROR);
		}
		else
		{
			color.setRgb(COLOR_OPEN);
		}
	}
	else if(rr2 & RR2_VVI_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for VVP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartColorVVP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr2 & RR2_VVP_OPEN)
	{
		if(rr2 & RR2_VVP_CLOSED)
		{
			color.setRgb(COLOR_ERROR);
		}
		else
		{
			color.setRgb(COLOR_OPEN);
		}
	}
	else if(rr2 & RR2_VVP_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif



/*
**	FUNCTION
**		Calculate state string for given part of device
**
**	ARGUMENTS
**		part	- which part of device is requested (see EqpPart enum in Eqp.h)
**		string	- Variable where resulting string will be put
**		mode	- Required data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartStateString(int part, QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	unsigned useRr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	unsigned useRr2 = mode == DataEnum::Replay ? rr2Replay : rr2Online;
	bool useAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	switch(part)
	{
	case EqpPartPump:
		getPartStateStringPump(useRr1, useRr2, useAlarm, string);
		break;
	case EqpPartVVR:
		getPartStateStringVVR(useRr1, useRr2, useAlarm, string);
		break;
	case EqpPartPP:
		getPartStateStringPP(useRr1, useRr2, useAlarm, string);
		break;
	case EqpPartTMP:
		getPartStateStringTMP(useRr1, useRr2, useAlarm, string);
		break;
	case EqpPartVVD:
		getPartStateStringVVD(useRr1, useRr2, useAlarm, string);
		break;
	case EqpPartVVI:
		getPartStateStringVVI(useRr1, useRr2, useAlarm, string);
		break;
	case EqpPartVVP:
		getPartStateStringVVP(useRr1, useRr2, useAlarm, string);
		break;
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for pump part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartStateStringPump(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if((rr1 & (RR1_ON | RR1_OFF)) == RR1_ON)
	{
		string = "VPG ON";
	}
	else if((rr1 & (RR1_ON | RR1_OFF)) == RR1_OFF)
	{
		string = "VPG OFF";
	}
	else
	{
		string = "VPG undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for VVR part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartStateStringVVR(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr2 & RR2_VVR_OPEN)
	{
		if(rr2 & RR2_VVR_CLOSED)
		{
			string = "VVR ERROR";
		}
		else
		{
			string = "VVR OPEN";
		}
	}
	else if(rr2 & RR2_VVR_CLOSED)
	{
		string = "VVR CLOSED";
	}
	else
	{
		string = "VVR undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for PP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartStateStringPP(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr2 & RR2_PP_ON)
	{
		string = "PP ON";
		if(!(rr2 & RR2_PP_OK))
		{
			string += " (not OK)";
		}
	}
	else
	{
		string = "PP OFF";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for TMP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartStateStringTMP(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr2 & RR2_TMP_ON)
	{
		string = "TMP ON";
		if(!(rr2 & RR2_TMP_OK))
		{
			string += " (not OK)";
		}
		if(!(rr2 & RR2_TMP_SPEED_OK))
		{
			string += " (speed not OK)";
		}
	}
	else
	{
		string = "TMP OFF";
		if(!(rr2 & RR2_TMP_START_ENABLE))
		{
			string += " (start disable)";
		}
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for VVD part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartStateStringVVD(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr2 & RR2_VVD_OPEN)
	{
		if(rr2 & RR2_VVD_CLOSED)
		{
			string = "VVD ERROR";
		}
		else
		{
			string = "VVD OPEN";
		}
	}
	else if(rr2 & RR2_VVD_CLOSED)
	{
		string = "VVD CLOSED";
	}
	else
	{
		string = "VVD undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for VVI part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartStateStringVVI(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr2 & RR2_VVI_OPEN)
	{
		if(rr2 & RR2_VVI_CLOSED)
		{
			string = "VVI ERROR";
		}
		else
		{
			string = "VVI OPEN";
		}
	}
	else if(rr2 & RR2_VVI_CLOSED)
	{
		string = "VVI CLOSED";
	}
	else
	{
		string = "VVI undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for VVP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPG_SA::getPartStateStringVVP(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr2 & RR2_VVP_OPEN)
	{
		if(rr2 & RR2_VVP_CLOSED)
		{
			string = "VVP ERROR";
		}
		else
		{
			string = "VVP OPEN";
		}
	}
	else if(rr2 & RR2_VVP_CLOSED)
	{
		string = "VVP CLOSED";
	}
	else
	{
		string = "VVP undefined";
	}
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpVPG_SA::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	return rr1 & RR1_ERROR_CODE_MASK;
}





/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPG_SA::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if(checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	// Without PLC connection does not match any criteria
	if(plcAlarmOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// In invalid state - only matches Error/warning criteria
	if(!(rr1Online & RR1_VALID))
	{
		switch(pCriteria->getType())
		{
		case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		default:
			pCriteria->setMatch(false, false, 0);
			return;
		}
	}

	// State is valid - decide based on criteria
	unsigned maskedRr1 = rr1Online & (RR1_ON | RR1_OFF);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case FunctionalTypeVPGF::On:
			if(maskedRr1 == RR1_ON)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVPGF::Off:
			if(maskedRr1 == RR1_OFF)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVPGF::VvrOpen:
			if((rr2Online & (RR2_VVR_OPEN | RR2_VVR_CLOSED)) == RR2_VVR_OPEN)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVPGF::VvrClosed:
			if((rr2Online & (RR2_VVR_OPEN | RR2_VVR_CLOSED)) == RR2_VVR_CLOSED)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVPGF::TmpOn:
			if(rr2Online & RR2_TMP_ON)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVPGF::TmpOff:
			if(!(rr2Online & RR2_TMP_ON))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}

