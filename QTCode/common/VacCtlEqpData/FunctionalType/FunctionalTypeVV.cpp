//	Implementation of FunctionalTypeVV class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVV.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

#include <stdio.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeVV::FunctionalTypeVV(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVV::fillCriteria(FunctionalType *pType)
{
	// Own criteria
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Closed, "Valve CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector} is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector} is NOT CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart} is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart} is NOT CLOSED");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Open, "Valve OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector} is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector} is NOT OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart} is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart} is NOT OPEN");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CurIntlLocalPressure, "INTL Local PR (LHC)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} INTL Local PR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} no INTL Local PR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: INTL Local PR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: no INTL Local PR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: INTL Local PR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: no INTL Local PR");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CurIntlLocalTemp, "INTL Local T (LHC)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} INTL Local T");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} no INTL Local T");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: INTL Local T");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: no INTL Local T");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: INTL Local T");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: no INTL Local T");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CurIntlPrevValve, "INTL previous Valve (LHC)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} INTL previous Valve");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} no INTL previous Valve");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: INTL previous Valve");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: no INTL previous Valve");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: INTL previous Valve");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: no INTL previous Valve");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CurIntlNextValve, "INTL next Valve (LHC)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} INTL next Valve");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} no INTL next Valve");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: INTL next Valve");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: no INTL next Valve");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: INTL next Valve");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: no INTL next Valve");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CurIntlDeltaPBefore, "INTL delta P before (SPS)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} INTL delta P before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} no INTL delta P before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: INTL delta P before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: no INTL delta P before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: INTL delta P before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: no INTL delta P before");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CurIntlDeltaPAfter, "INTL delta P after (SPS)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} INTL delta P after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} no INTL delta P after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: INTL delta P after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: no INTL delta P after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: INTL delta P after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: no INTL delta P after");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CurIntlGlobalBefore, "INTL global before (SPS)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} INTL global before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} no INTL global before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: INTL global before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: no INTL global before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: INTL global before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: no INTL global before");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CurIntlGlobalAfter, "INTL global after (SPS)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} INTL global after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} no INTL global after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: INTL global after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: no INTL global after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: INTL global after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: no INTL global after");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CurIntlLocalBefore, "INTL local before (SPS & PS)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} INTL local before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} no INTL local before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: INTL local before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: no INTL local before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: INTL local before");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: no INTL local before");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, CurIntlLocalAfter, "INTL local after (SPS & PS)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} INTL local after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} no INTL local after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: INTL local after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: no INTL local after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: INTL local after");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: no INTL local after");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, PrSourcesOK, "Valve PR sources OK (LHC)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} PR sources OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} PR sources BAD");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: PR sources OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: PR sources BAD");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: PR sources OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: PR sources BAD");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "Valve ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Valve in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Valve in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Valve in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Valve in main part {mainPart}: no error");

	// Criteria for related equipment
	pType = FunctionalType::findTypeData(FunctionalType::VGP);
	if(pType)
	{
		pType->fillCriteria(this);
	}
	pType = FunctionalType::findTypeData(FunctionalType::VPI);
	if(pType)
	{
		pType->fillCriteria(this);
	}
	pType = FunctionalType::findTypeData(FunctionalType::CRYO_TT);
	if(pType)
	{
		pType->fillCriteria(this);
	}	
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeVV::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case Closed:
			result = "Closed";
			break;
		case Open:
			result = "Open";
			break;
		case PrSourcesOK:
			result = "PrSourcesOK";
			break;
		case CurIntlLocalPressure:
			result = "CurIntlLocalPressure";
			break;
		case CurIntlLocalTemp:
			result = "CurIntlLocalTemp";
			break;
		case CurIntlPrevValve:
			result = "CurIntlPrevValve";
			break;
		case CurIntlNextValve:
			result = "CurIntlNextValve";
			break;
		case CurIntlDeltaPBefore:
			result = "CurIntlDeltaPBefore";
			break;
		case CurIntlDeltaPAfter:
			result = "CurIntlDeltaPAfter";
			break;
		case CurIntlGlobalBefore:
			result = "CurIntlGlobalBefore";
			break;
		case CurIntlGlobalAfter:
			result = "CurIntlGlobalAfter";
			break;
		case CurIntlLocalBefore:
			result = "CurIntlLocalBefore";
			break;
		case CurIntlLocalAfter:
			result = "CurIntlLocalAfter";
			break;
		default:
			break;
		}
		break;
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeVV::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "Closed"))
		{
			result = Closed;
		}
		else if(!strcasecmp(name, "Open"))
		{
			result = Open;
		}
		else if(!strcasecmp(name, "PrSourcesOK"))
		{
			result = PrSourcesOK;
		}
		else if(!strcasecmp(name, "CurIntlLocalPressure"))
		{
			result = CurIntlLocalPressure;
		}
		else if(!strcasecmp(name, "CurIntlLocalTemp"))
		{
			result = CurIntlLocalTemp;
		}
		else if(!strcasecmp(name, "CurIntlPrevValve"))
		{
			result = CurIntlPrevValve;
		}
		else if(!strcasecmp(name, "CurIntlNextValve"))
		{
			result = CurIntlNextValve;
		}
		else if(!strcasecmp(name, "CurIntlDeltaPBefore"))
		{
			result = CurIntlDeltaPBefore;
		}
		else if(!strcasecmp(name, "CurIntlDeltaPAfter"))
		{
			result = CurIntlDeltaPAfter;
		}
		else if(!strcasecmp(name, "CurIntlGlobalBefore"))
		{
			result = CurIntlGlobalBefore;
		}
		else if(!strcasecmp(name, "CurIntlGlobalAfter"))
		{
			result = CurIntlGlobalAfter;
		}
		else if(!strcasecmp(name, "CurIntlLocalBefore"))
		{
			result = CurIntlLocalBefore;
		}
		else if(!strcasecmp(name, "CurIntlLocalAfter"))
		{
			result = CurIntlLocalAfter;
		}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}


/*
**	FUNCTION
**		Find list of devices with given functional type, 'related' to given
**		device of this functional type. In generic class -just dummy function
**		that returns nothing
**
**	PARAMETERS
**		pEqp		- Pointer to device of this functional type
**		otherType	- ID of other functional type - related devices of that type
**						are required
**		relatedList	- List where all found related devices shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVV::findRelatedEqp(Eqp *pEqp, int otherType, QList<Eqp *> &relatedList)
{
	relatedList.clear();
	switch(otherType)
	{
	case FunctionalType::VGP:
	case FunctionalType::VGR:
		findRelatedEqpVG(pEqp, otherType, relatedList);
		break;
	case FunctionalType::VRPI:
		findRelatedEqpVRPI(pEqp, otherType, relatedList);
		break;
	case FunctionalType::CRYO_TT:
	case FunctionalType::CRYO_TT_SUM:
		findRelatedEqpCRYO_TT(pEqp, otherType, relatedList);
		break;
	default:
		printf("FunctionalTypeVV::findRelatedEqp(): unsupported otherType %d\n", otherType);
		fflush(stdout);
		break;
	}
}

void FunctionalTypeVV::findRelatedEqpVG(Eqp *pEqp, int otherType, QList<Eqp *> &relatedList)
{
	DataPool &pool = DataPool::getInstance();
	QString attrValue = pEqp->getAttrValue("GaugeBefore");
	if(!attrValue.isEmpty())
	{
		Eqp *pOtherEqp = pool.findEqpByDpName(attrValue.toLatin1());
		if(pOtherEqp)
		{
			if(pOtherEqp->getFunctionalType() == otherType)
			{
				relatedList.append(pOtherEqp);
			}
		}
	}
	attrValue = pEqp->getAttrValue("GaugeAfter");
	if(!attrValue.isEmpty())
	{
		Eqp *pOtherEqp = pool.findEqpByDpName(attrValue.toLatin1());
		if(pOtherEqp)
		{
			if(pOtherEqp->getFunctionalType() == otherType)
			{
				relatedList.append(pOtherEqp);
			}
		}
	}
}

void FunctionalTypeVV::findRelatedEqpVRPI(Eqp *pEqp, int otherType, QList<Eqp *> &relatedList)
{
	DataPool &pool = DataPool::getInstance();
	QString attrValue = pEqp->getAttrValue("PumpBefore");
	if(!attrValue.isEmpty())
	{
		Eqp *pOtherEqp = pool.findEqpByDpName(attrValue.toLatin1());
		if(pOtherEqp)
		{
			if(pOtherEqp->getFunctionalType() == otherType)
			{
				relatedList.append(pOtherEqp);
			}
		}
	}
	attrValue = pEqp->getAttrValue("PumpAfter");
	if(!attrValue.isEmpty())
	{
		Eqp *pOtherEqp = pool.findEqpByDpName(attrValue.toLatin1());
		if(pOtherEqp)
		{
			if(pOtherEqp->getFunctionalType() == otherType)
			{
				relatedList.append(pOtherEqp);
			}
		}
	}
}

void FunctionalTypeVV::findRelatedEqpCRYO_TT(Eqp *pEqp, int otherType, QList<Eqp *> &relatedList)
{
	DataPool &pool = DataPool::getInstance();
	QStringList thermoNames;
	QString errMsg;
	pool.findThermometersForValve(pEqp->getDpName(), thermoNames, errMsg);
	for(int idx = 0 ; idx < thermoNames.size() ; idx++)
	{
		Eqp *pOtherEqp = pool.findEqpByDpName(thermoNames.at(idx).toLatin1());
		if(pOtherEqp)
		{
			if(pOtherEqp->getFunctionalType() == otherType)
			{
				relatedList.append(pOtherEqp);
			}
		}
	}		
}

