//	Implementation of VacIconVGF class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGF.h"

#include "EqpVG.h"

#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVGF::VacIconVGF(QWidget *parent, Qt::WindowFlags f) :
	VacIconVG(parent, f)
{
	setFixedSize(21, 35);
}

VacIconVGF::~VacIconVGF()
{
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVGF::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		return VacIconGeometry(21, 35, 10, 38, 1);
		break;
	default:
		break;
	}
	return VacIconGeometry(35, 21, 38, 10, 1);
}

/*
**	FUNCTION
**		Override method of VacIconVG: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVGF::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(21);
		size.setHeight(35);
		break;
	default:
		size.setWidth(35);
		size.setHeight(21);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVGF::paintEvent(QPaintEvent * /* pEvent */)
{
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Prepare painter's coordinate system for drawing
	// Coordinate system (0,0) is center of widget
	painter.translate(width() / 2, height() / 2);

	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}

	// Draw connecting pipe
	painter.fillRect(10 - pipeWidth / 2, 31, pipeWidth ? pipeWidth : 1, 4, pipeColor);

	// Pipe is ready - draw gauge image
	drawImage(painter, ctlStatus);
}

void VacIconVGF::drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus)
{
	setPainterForMainImage(painter, ctlStatus);

	// Filled circle for Pirani (far from connection point)
	painter.setClipping(true);
	painter.setClipRect(-10, -16, 21, 17);
	QPen pen = painter.pen();
	painter.setPen(Qt::NoPen);
	painter.drawPie(-9, -16, 19, 19, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(-9, -16, 19, 19, 0, 360 * 16);

	// Filled circle for Penning (close to connection point)
	painter.setClipRect(-10, 0, 21, 17);
	painter.setPen(Qt::NoPen);
	painter.drawPie(-9, -2, 19, 19, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(-9, -2, 19, 19, 0, 360 * 16);

	// Pirani symbol
	painter.setClipping(false);
	painter.setPen(Qt::black);
	QPolygon points(10);
	points.setPoint(0, -7, -8);
	points.setPoint(1, -5, -8);
	points.setPoint(2, -5, -4);
	points.setPoint(3, -2, -4);
	points.setPoint(4, -2, -10);
	points.setPoint(5, 2, -10);
	points.setPoint(6, 2, -4);
	points.setPoint(7, 5, -4);
	points.setPoint(8, 5, -8);
	points.setPoint(9, 7, -8);
	painter.drawPolyline(points);

	// Penning symbol
	QPen blackWide(Qt::black, 2);
	painter.setPen(blackWide);
	points.remove(4, 6);
	points.setPoint(0, -4, 6);
	points.setPoint(1, -4, 11);
	points.setPoint(2, 5, 11);
	points.setPoint(3, 5, 6);
	painter.drawPolyline(points);

	painter.drawArc(-1, 5, 3, 3, 0, 360 * 16);
}

