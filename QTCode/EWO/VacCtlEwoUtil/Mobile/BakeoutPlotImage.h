#ifndef BAKEOUTPLOTIMAGE_H
#define	BAKEOUTPLOTIMAGE_H

// Class providing mouse intercation with bakeout rack profile image

#include <qwidget.h>

class BakeoutPlotImage: public QWidget
{
	Q_OBJECT

public:
	BakeoutPlotImage(QWidget *parent);
	~BakeoutPlotImage();

signals:
	void resized(void);
	void pointerMoved(QMouseEvent *pEvent);
	void mouseLeave(void);

protected:
	virtual void resizeEvent(QResizeEvent *pEvent);
	virtual void mouseMoveEvent(QMouseEvent *pEvent);
	virtual void leaveEvent(QEvent *pEvent);

};

#endif	// BAKEOUTPLOTIMAGE_H
