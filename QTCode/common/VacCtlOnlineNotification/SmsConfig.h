#ifndef SMSCONFIG_H
#define	SMSCONFIG_H

// Class holding one SMS configuration to read from/write to CSV file

#include "EqpMsgCriteria.h"
#include "EqpUsageFilterItem.h"

#include <qstring.h>
#include <qlist.h>
#include <qstringlist.h>

#include <stdio.h>

class SmsConfigCriteria;

class SmsConfig
{
public:
	SmsConfig(void);
	SmsConfig(const char *externalId);
	~SmsConfig();

	void addScopeItem(const QString &name);
	void addUsageFilter(int funcType, const QByteArray &attrName, int reverse);
	void addCriteria(int funcType, int type, int subType, bool reverse, float lowerLimit, float upperLimit, int minDuration);
	void addRecipient(const char *pName);

	void writeToFile(FILE *pFile, QStringList &machineModes, unsigned allModesMask);

	int parseVisibleName(QString &string, int lineNo, QStringList &errList);
	int parseMachineMode(QString &string, int lineNo, QStringList &machineModes, QStringList &errList);
	int parseMainType(QString &string, int lineNo, QStringList &errList);
	int parseUsageFilterTypes(QString &string, int lineNo, QStringList &errList);
	int parseUsageFilterAttrNames(QString &string, int lineNo, QStringList &errList);
	int parseUsageFilterReverseFlags(QString &string, int lineNo, QStringList &errList);
	int parseScopeType(QString &string, int lineNo, QStringList &errList);
	int parseScopeItems(QString &string, int lineNo, QStringList &errList);
	int parseCritJoinType(QString &string, int lineNo, QStringList &errList);
	int parseCritEqpTypes(QString &string, int lineNo, QStringList &errList);
	int parseCritTypes(QString &string, int lineNo, QStringList &errList);
	int parseCritSubTypes(QString &string, int lineNo, QStringList &errList);
	int parseCritReverseFlags(QString &string, int lineNo, QStringList &errList);
	int parseCritUpperLimits(QString &string, int lineNo, QStringList &errList);
	int parseCritLowerLimits(QString &string, int lineNo, QStringList &errList);
	int parseCritMinDurations(QString &string, int lineNo, QStringList &errList);
	int parseFilteringType(QString &string, int lineNo, QStringList &errList);
	int parseFilteringTime(QString &string, int lineNo, QStringList &errList);
	int parseGroupType(QString &string, int lineNo, QStringList &errList);
	int parseGroupTime(QString &string, int lineNo, QStringList &errList);
	int parseGroupIndivLimit(QString &string, int lineNo, QStringList &errList);
	int parseMessage(QString &string, int lineNo, QStringList &errList);
	int parseGroupMessage(QString &string, int lineNo, QStringList &errList);
	int parseRecipients(QString &string, int lineNo, QStringList &errList);

	// Access
	inline const QString &getExtId(void) const { return extId; }
	inline int getId(void) const { return id; }
	inline const QString &getVisibleName(void) const { return visibleName; }
	inline void setVisibleName(const QString &value) { visibleName = value; }
	inline unsigned getMode(void) const { return mode; }
	inline void setMode(unsigned value) { mode = value; }
	inline int getFuncType(void) const { return funcType; }
	inline void setFuncType(int value) { funcType = value; }
	inline const QList<EqpUsageFilterItem *> &getUsageFilterList(void) const { return usageFilterList; }
	inline int getScopeType(void) const { return scopeType; }
	inline void setScopeType(int value) { scopeType = value; }
	inline const QStringList &getScopeItems(void) const { return scopeItems; }
	inline int getCritJoinType(void) const { return critJoinType; }
	inline void setCritJoinType(int value) { critJoinType = value; }
	inline const QList<EqpMsgCriteria *> &getCriteria(void) const { return critList; }
	inline const QString &getMessage(void) const { return message; }
	inline void setMessage(const char *value) { message = value; }
	inline const QString &getGroupMessage(void) const { return groupMessage; }
	inline void setGroupMessage(const char *value) { groupMessage = value; }
	inline int getFilterType(void) const { return filterType; }
	inline void setFilterType(int value) { filterType = value; }
	inline int getFilterTime(void) const { return filterTime; }
	inline void setFilterTime(int value) { filterTime = value; }
	inline int getGroupType(void) const { return groupType; }
	inline void setGroupType(int value) { groupType = value; }
	inline int getGroupTime(void) const { return groupTime; }
	inline void setGroupTime(int value) { groupTime = value; }
	inline int getIndivLimit(void) const { return indivLimit; }
	inline void setIndivLimit(int value) { indivLimit = value; }
	inline const QStringList &getRecipients(void) const { return recipients; }
	
protected:

	// To assign IDs for new instances
	static int lastId;

	// Instance 'external' ID (DP name)
	QString		extId;

	// Instance internal ID
	int			id;

	// Visible name of configuration
	QString		visibleName;

	// Machine mode
	unsigned	mode;

	// Main functional type
	int			funcType;

	// Scope type
	int			scopeType;

	// Scope items
	QStringList	scopeItems;

	// List of usage filter items - the whole filter is not needed
	QList<EqpUsageFilterItem *>	usageFilterList;

	// Criteria join type
	int			critJoinType;

	// List of criteria
	QList<EqpMsgCriteria *>	critList;

	// Message
	QString		message;

	// Message for group
	QString		groupMessage;

	// Filtering type
	int			filterType;

	// Filtering time
	int			filterTime;

	// Grouping type (for filterType = GROUPING)
	int			groupType;

	// Grouping time
	int			groupTime;

	// Limit for individual messages (for filterType = GROUPING)
	int			indivLimit;

	// List of recipients
	QStringList	recipients;

	void init(void);
	void writeScopeItems(FILE *pFile);
	const char *getScopeItemName(const char *pName);

	void writeMachineMode(FILE *pFile, QStringList &machineModes, unsigned allModesMask);
	void writeUsageFilterFuncTypes(FILE *pFile);
	void writeUsageFilterAttrNames(FILE *pFile);
	void writeUsageFilterReverse(FILE *pFile);
	void writeCriteriaFuncTypes(FILE *pFile);
	void writeCriteriaTypes(FILE *pFile);
	void writeCriteriaReverse(FILE *pFile);
	void writeCriteriaSubTypes(FILE *pFile);
	void writeCriteriaUpperLimits(FILE *pFile);
	void writeCriteriaLowerLimits(FILE *pFile);
	void writeCriteriaMinDurations(FILE *pFile);

	QString lineNoError(int lineNo);
	int checkCriteraLength(int lineNo, int critIdx, const char *msg, QStringList &errList);
	int checkUsageFilterLength(int lineNo, int critIdx, const char *msg, QStringList &errList);
};

#endif	// SMSCONFIG_H
