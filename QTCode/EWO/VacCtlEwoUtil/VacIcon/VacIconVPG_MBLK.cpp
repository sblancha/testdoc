//	Implementation of VacIconVPG_MBLK class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPG_MBLK.h"

#include "EqpVPG_MBLK.h"

#include "DataPool.h"

#include <qpainter.h>
#include <QMouseEvent>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OPEN				0,255,0
#define COLOR_CLOSED			255,0,0
#define	COLOR_ERROR				255,102,255
#define	COLOR_WARNING			255,255,102

// Colors for control and interlocks
#define COLOR_GREEN				0,255,0
#define COLOR_YELLOW			255,255,0
#define COLOR_RED				255,0,0
#define	COLOR_WHITE				255,255,255

//	Main state bits to be analyzed
#define	RR1_VALID			(0x40000000)
#define RR1_ERROR			(0x00800000) 
#define	RR1_WARNING			(0x00400000) 
#define	RR1_ON				(0x02000000) 
#define	RR1_OFF				(0x01000000) 
#define RR1_VVI_OPEN		(0x00000400)
#define RR1_VVI_CLOSED		(0x00000800)
#define RR1_TMP_NOMINAL		(0x20000000) 
#define RR1_TMP_ON			(0x00200000) 
#define RR1_VPP_ON			(0x00010000)
#define RR1_VVR1_CLOSED		(0x00000002)
#define RR1_VVR1_OPEN		(0x00000001)

#define RR3_VPG_MODE		(0x0000F000)

//color configurations for pump
#define STATE_GROUP_UNDEFINED   -1 //state not found
#define STATE_GROUP_TRIANGLES    0 //green and white triangles
#define STATE_GROUP_FULL_GREEN   1 // full green
#define STATE_GROUP_FULL_WHITE   2 // full white
#define STATE_GROUP_RED_CIRCLE   3 // white with red circle
#define STATE_GROUP_GREEN_CIRCLE 4 // white with green circle

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
VacIconVPG_MBLK::VacIconVPG_MBLK(QWidget *parent, Qt::WindowFlags f) :
VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(20, 53);
	setAreaGeometry();
}

VacIconVPG_MBLK::~VacIconVPG_MBLK()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for symbol areas rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_MBLK::setAreaGeometry(void)
{
	switch (direction)
	{
	case VacIconContainer::Up:
		vvr.setRect(1, 2, 17, 17);
		pump.setRect(1, 30, 17, 17);
		break;
	case VacIconContainer::Down:
		vvr.setRect(20, 34, 17, 17);
		pump.setRect(20, 6, 17, 17);
		break;
	case VacIconContainer::Left:
		vvr.setRect(34, 1, 17, 17);
		pump.setRect(6, 1, 17, 17);
		break;
	case VacIconContainer::Right:
		vvr.setRect(2, 20, 17, 17);
		pump.setRect(30, 20, 17, 17);
		break;
	}

}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPG_MBLK::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch (direction)
	{
	case VacIconContainer::Up:
		return VacIconGeometry(38, 53, 9, 54, -1);
		break;
	case VacIconContainer::Down:
		return VacIconGeometry(38, 53, 28, 54, -1);
		break;
	default:
		break;
	}
	return VacIconGeometry(53, 38, 54, 9, -1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_MBLK::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch (newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(38);
		size.setHeight(53);
		break;
	default:
		size.setWidth(53);
		size.setHeight(38);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreaGeometry();
	setGaugeDirection();
}

/*
**	FUNCTION
**		Set direction for gauge icons based on direction for this icon
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_MBLK::setGaugeDirection(void)
{
	VacIconContainer::Direction	vgrDirection = VacIconContainer::Up,
		vgpDirection = VacIconContainer::Up;
	switch (direction)
	{
	case VacIconContainer::Up:
		vgrDirection = VacIconContainer::Left;
		vgpDirection = VacIconContainer::Down;
		break;
	case VacIconContainer::Down:
		vgrDirection = VacIconContainer::Right;
		vgpDirection = VacIconContainer::Up;
		break;
	case VacIconContainer::Left:
		vgrDirection = VacIconContainer::Down;
		vgpDirection = VacIconContainer::Left;
		break;
	case VacIconContainer::Right:
		vgrDirection = VacIconContainer::Up;
		vgpDirection = VacIconContainer::Right;
		break;
	}

}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_MBLK::setEqp(Eqp *pEqp)
{
	disconnect();
	if (pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVPG_MBLK"), "VacIconVPG_MBLK::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVPG_MBLK *)pEqp;

	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_MBLK::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Set color used for pipe lines drawing
**
**	ARGUMENTS
**		newColor	- Color used for pipe drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_MBLK::setPipeColor(const QColor &newColor)
{
	VacIcon::setPipeColor(newColor);
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_MBLK::connect(void)
{
	if (connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("RR2");
	dpes.append("RR3");
	dpes.append("ActiveStep");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_MBLK::disconnect(void)
{
	bool result = false;
	if (connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_MBLK::getToolTip(const QPoint &pos, QString &text, QRect &rect)
{
	if (pEqp && connected)
	{
		// Where is the point? If in one of valve - show valve text,
		// otherwise show valve name and state
		if (vvr.contains(pos))
		{
			text = pEqp->getVisibleName();
			QString state;
			pEqp->getPartStateString(Eqp::EqpPartVVR, state, mode);
			text += ": ";
			text += state;
			rect = vvr;
		}
		else if (pump.contains(pos))
		{
			pEqp->getToolTipString(text, mode);
			rect = pump;
		}
	}
	else
	{
		rect = this->rect();
		text = "Not connected";
	}
	return true;
}

/*
**	FUNCTION
**		Override VacIcon's method in order to have different menus for different
**		parts of icon
**
**	ARGUMENTS
**		pEvent	- Pointer to mouse event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_MBLK::mousePressEvent(QMouseEvent *pEvent)
{
	if (pEqp)
	{
		QPoint mousePoint(pEvent->x(), pEvent->y());
		QPoint screenPoint = mapToGlobal(mousePoint);
		int buttonNbr = 0;
		switch (pEvent->button())
		{
		case Qt::LeftButton:
			buttonNbr = 1;
			break;
		case Qt::RightButton:
			buttonNbr = 2;
			break;
		default:
			break;
		}
		if (buttonNbr)
		{
			if (vvr.contains(mousePoint))
			{
				emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), Eqp::EqpPartVVR1, pEqp->getDpName());
			}
			else
			{
				emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), 0, pEqp->getDpName());
			}
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPG_MBLK::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0, rr2 = 0, rr3 = 0;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if (pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		rr2 = pEqp->getRR2(mode);
		rr3 = pEqp->getRR3(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if (pEqp)
	{
		if (pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	drawPipes(painter);

	drawVvr(painter, ctlStatus, rr1, rr2, rr3, plcAlarm);

	drawPump(painter, ctlStatus, rr1, rr2, rr3, plcAlarm);

	if (!pEqp)
	{
		painter.setPen(Qt::black);
	}
}

void VacIconVPG_MBLK::drawPipes(QPainter &painter)
{
	switch (direction)
	{
	case VacIconContainer::Up:
		// Line from VVR to outside
		painter.fillRect(vvr.center().x() - 1, 0, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, vvr.bottom() + 1,
			3, pump.top() - vvr.bottom(), pipeColor);
		break;
	case VacIconContainer::Down:
		// Line from VVR to outside
		painter.fillRect(vvr.center().x() - 1, vvr.bottom(), 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, pump.bottom(),
			3, vvr.top() - pump.bottom(), pipeColor);
		break;
	case VacIconContainer::Left:
		// Line from VVR to outside
		painter.fillRect(vvr.right(), vvr.center().y() - 1, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.right(), pump.center().y() - 1,
			vvr.left() - pump.right(), 3, pipeColor);
		break;
	case VacIconContainer::Right:
		// Line from VVR to outside
		painter.fillRect(0, vvr.center().y() - 1, 3, 3, pipeColor);
		// Connect pump to valves
		painter.fillRect(vvr.right(), pump.center().y() - 1,
			pump.left() - vvr.right(), 3, pipeColor);
		break;
	}
}

void VacIconVPG_MBLK::drawVvr(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, unsigned rr3, bool plcAlarm)
{
	// Save painter coordinate system - interlocks will be drawn
	// in original (non-translated) coordinate system
	painter.save();

	// Prepare painter's coordinate system for image drawing
	// Coordinate system (0,0) is center of valve symbol
	painter.translate(vvr.center().x(), vvr.center().y());

	// Turn coordinate system according to direction
	switch (direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	// Draw valve image
	setPainterForVvr(painter, ctlStatus, rr1, rr2, plcAlarm);

	QPolygon points(4);
	points.setPoint(0, -8, -8);
	points.setPoint(1, 8, -8);
	points.setPoint(2, -8, 8);
	points.setPoint(3, 8, 8);
	painter.drawPolygon(points);

	// Restore coordinate system to original
	painter.restore();
}

void VacIconVPG_MBLK::drawPump(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, unsigned rr3, bool plcAlarm)
{
	QColor	color;
	painter.setPen(Qt::black);
	bool drawRect = true;
	int seqStep = 0;// (rr3 & RR3_VPG_MODE) >> 12u;
	int drawState;

	if (pEqp)
	{
		seqStep = pEqp->getActiveStep(mode);//
	}

	switch (seqStep)
	{
	case 3:
	case 5:
		drawState = STATE_GROUP_TRIANGLES;
		break;
	case 4:
	case 12:
	case 13:
	case 14:
		drawState = STATE_GROUP_FULL_GREEN;
		break;
	case 0:
		drawState = STATE_GROUP_FULL_WHITE;
		break;
	case 6:
	case 7:
	case 8:
	case 9:
	case 10:
	case 11:
	case 15:
		drawState = STATE_GROUP_RED_CIRCLE;
		break;
	case 1:
	case 2:
		drawState = STATE_GROUP_GREEN_CIRCLE;
		break;
	default:
		drawState = STATE_GROUP_UNDEFINED;
		break;
	}

	switch (ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		painter.fillRect(pump, color);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		painter.fillRect(pump, color);
		break;
	default:
		if (plcAlarm)
		{
			color.setRgb(COLOR_UNDEFINED);
			painter.fillRect(pump, color);
		}
		else if (!(rr1 & RR1_VALID))
		{
			color.setRgb(COLOR_UNDEFINED);
			painter.fillRect(pump, color);
		}
		else if (rr1 & RR1_ERROR)
		{
			color.setRgb(COLOR_ERROR);
			painter.fillRect(pump, color);
		}
		else if (rr1 & RR1_WARNING)
		{
			color.setRgb(COLOR_WARNING);
			painter.fillRect(pump, color);
		}
		else
		{
			unsigned maskedOnOff = rr1 & (RR1_ON | RR1_OFF);
			if (maskedOnOff == (RR1_ON | RR1_OFF))	// Both ON and OFF
			{
				color.setRgb(COLOR_ERROR);
				painter.setBrush(color);
				painter.drawRect(pump);
			}
			else if (!maskedOnOff) 	// Neither ON nor OFF
			{
				color.setRgb(COLOR_UNDEFINED);
				painter.setBrush(color);
				painter.drawRect(pump);
			}
			else if (drawState == STATE_GROUP_FULL_GREEN)
			{
				color.setRgb(COLOR_GREEN);
				painter.fillRect(pump, color);
			}
			else if (drawState == STATE_GROUP_TRIANGLES)
			{
				QPolygon points(3);
				points.setPoint(0, pump.left(), pump.top());
				points.setPoint(1, pump.right(), pump.top());
				points.setPoint(2, pump.left(), pump.bottom());
				color.setRgb(COLOR_GREEN);
				painter.setBrush(color);
				painter.drawPolygon(points);
				points.setPoint(0, pump.right(), pump.top());
				points.setPoint(1, pump.right(), pump.bottom());
				points.setPoint(2, pump.left(), pump.bottom());
				color.setRgb(COLOR_WHITE);
				painter.setBrush(color);
				painter.drawPolygon(points);
				drawRect = false;
			}
			else if (drawState == STATE_GROUP_RED_CIRCLE)
			{
				color.setRgb(COLOR_WHITE);
				painter.fillRect(pump, color);

				color.setRgb(COLOR_RED);
				painter.setBrush(color);
				painter.setPen(Qt::NoPen);
				painter.drawPie(pump.left() + 2, pump.top() + 2, pump.width() - 5, pump.height() - 5,
					0, 360 * 16);
			}
			else if (drawState == STATE_GROUP_GREEN_CIRCLE)
			{
				color.setRgb(COLOR_WHITE);
				painter.fillRect(pump, color);

				color.setRgb(COLOR_GREEN);
				painter.setBrush(color);
				painter.setPen(Qt::NoPen);
				painter.drawPie(pump.left() + 2, pump.top() + 2, pump.width() - 5, pump.height() - 5,
					0, 360 * 16);
			}
			else if (drawState == STATE_GROUP_FULL_WHITE)
			{
				color.setRgb(COLOR_WHITE);
				painter.fillRect(pump, color);
			}
			else
			{
				color.setRgb(COLOR_ERROR);
				painter.fillRect(pump, color);
			}
		}
		break;
	}

	// Draw pump rectangle, circle and line
	painter.setPen(Qt::black);
	painter.setBrush(Qt::NoBrush);
	if (drawRect)
	{
		painter.drawRect(pump.left(), pump.top(), pump.width() - 1, pump.height() - 1);
	}
	painter.drawArc(pump.left() + 2, pump.top() + 2, pump.width() - 5, pump.height() - 5,
		0, 360 * 16);
	QPoint center = pump.center();
	painter.drawLine(center.x() - 3, center.y(), center.x() + 3, center.y());
}

void VacIconVPG_MBLK::setPainterForVvr(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm)
{
	QColor fillColor(COLOR_UNDEFINED);
	switch (ctlStatus)
	{
	case Eqp::NotControl:
		fillColor.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		fillColor.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if (!plcAlarm)
		{
			if (rr1 & RR1_VALID)
			{
				if (rr1 & RR1_VVR1_OPEN)
				{
					if (rr1 & RR1_VVR1_CLOSED)
					{
						fillColor.setRgb(COLOR_ERROR);
					}
					else
					{
						fillColor.setRgb(COLOR_OPEN);
					}
				}
				else if (rr1 & RR1_VVR1_CLOSED)
				{
					fillColor.setRgb(COLOR_CLOSED);
				}
			}
		}
		break;
	}
	painter.setBrush(fillColor);
	painter.setPen(Qt::black);
}

