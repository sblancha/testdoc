#ifndef MAINLHC_H
#define MAINLHC_H

// Main LHC View widget

#include "VacMainView.h"

#include "LhcRingSelection.h"
#include "MainLHCImage.h"

#include <qcombobox.h>
#include <qtablewidget.h>
#include <qpushbutton.h>
#include <qcheckbox.h>
#include <qgroupbox.h>

class MainLHC : public VacMainView, protected LhcRingSelection
{
	Q_OBJECT

	////////////////////////////////////////////////////////////////////////////
	///////////////////// Properties exposed to PVSS panel /////////////////////
	////////////////////////////////////////////////////////////////////////////
    Q_PROPERTY(QFont mainFont READ font WRITE setMainFont);

	//--------------------- Properties of VacMainView ---------------------------
	Q_PROPERTY(QColor synPipeColor READ getSynPipeColor WRITE setSynPipeColor);
	Q_PROPERTY(QColor synAltPipeColor READ getSynAltPipeColor WRITE setSynAltPipeColor);
	Q_PROPERTY(QColor synPipePartColor READ getSynPipePartColor WRITE setSynPipePartColor);
	Q_PROPERTY(QColor synPassiveLineColor READ getSynPassiveLineColor WRITE setSynPassiveLineColor);
	Q_PROPERTY(QColor synPassiveFillColor READ getSynPassiveFillColor WRITE setSynPassiveFillColor);
	Q_PROPERTY(QColor synSectorColor READ getSynSectorColor WRITE setSynSectorColor);
	Q_PROPERTY(int synPipeWidth READ getSynPipeWidth WRITE setSynPipeWidth);
	Q_PROPERTY(QColor graphAxisColor READ getGraphAxisColor WRITE setGraphAxisColor);
	Q_PROPERTY(int profileBarWidth READ getProfileBarWidth WRITE setProfileBarWidth);
	Q_PROPERTY(QColor profileAltSectorColor READ getProfileAltSectorColor WRITE setProfileAltSectorColor);
	Q_PROPERTY(QColor profileAltBarColor READ getProfileAltBarColor WRITE setProfileAltBarColor);
	Q_PROPERTY(int profileRedrawDelay READ getProfileRedrawDelay WRITE setProfileRedrawDelay);
	Q_PROPERTY(int pollingPeriod READ getPollingPeriod WRITE setPollingPeriod);
	Q_PROPERTY(bool pvssEvents READ isPvssEvents WRITE setPvssEvents);


	//--------------------- Properties specific to this class -------------------
	Q_PROPERTY(QColor mainQrlColor READ getQrlColor WRITE setQrlColor);
	Q_PROPERTY(QColor mainCryoColor READ getCryoColor WRITE setCryoColor);
	Q_PROPERTY(QColor mainRedBeamColor READ getRedBeamColor WRITE setRedBeamColor);
	Q_PROPERTY(QColor mainBlueBeamColor READ getBlueBeamColor WRITE setBlueBeamColor);
	Q_PROPERTY(QColor mainCrossBeamColor READ getCrossBeamColor WRITE setCrossBeamColor);
	Q_PROPERTY(QColor mainDefaultBeamColor READ getDefaultBeamColor WRITE setDefaultBeamColor);
	Q_PROPERTY(QColor mainSelectColor READ getSelectColor WRITE setSelectColor);
	Q_PROPERTY(int mainLssScale READ getLssScale WRITE setLssScale);
	Q_PROPERTY(int mainQrlWidth READ getQrlWidth WRITE setQrlWidth);
	Q_PROPERTY(int mainCryoWidth READ getCryoWidth WRITE setCryoWidth);
	Q_PROPERTY(int mainConnLineWidth READ getConnLineWidth WRITE setConnLineWidth);
	Q_PROPERTY(BeamTypeDrawMethod mainBeamDrawTypeMethod
			READ getBeamTypeMethod WRITE setBeamTypeMethod);
	Q_PROPERTY(int mainBeamTypeWidth READ getBeamTypeWidth WRITE setBeamTypeWidth);
	Q_PROPERTY(int mainRedrawDelay READ getRedrawDelay WRITE setRedrawDelay);
	Q_PROPERTY(int mainViewWidth READ getViewWidth);
	Q_PROPERTY(int mainViewHeight READ getViewHeight);
	Q_PROPERTY(bool readOnly READ isReadOnly WRITE setReadOnly);

	// It looks like Q_ENUMS can only register enum declared in THIS class,
	// so let's make here a copy of MainLHCImage::BeamTypeDrawMethod enum
	Q_ENUMS(BeamTypeDrawMethod);

	// Access methods for properties above

public:
	enum BeamTypeDrawMethod
	{
		None = 0,
		InnerOuterLines = 1,
		MiddleLine = 2
	};
	inline const QColor &getQrlColor(void) const { return pImage->getQrlColor(); }
	inline void setQrlColor(QColor &color) { pImage->setQrlColor(color); }
	inline const QColor &getCryoColor(void) const { return pImage->getCryoColor(); }
	inline void setCryoColor(QColor &color) { pImage->setCryoColor(color); }
	inline const QColor &getRedBeamColor(void) const { return pImage->getRedBeamColor(); }
	inline void setRedBeamColor(QColor &color) { pImage->setRedBeamColor(color); }
	inline const QColor &getBlueBeamColor(void) const { return pImage->getBlueBeamColor(); }
	inline void setBlueBeamColor(QColor &color) { pImage->setBlueBeamColor(color); }
	inline const QColor &getCrossBeamColor(void) const { return pImage->getCrossBeamColor(); }
	inline void setCrossBeamColor(QColor &color) { pImage->setCrossBeamColor(color); }
	inline const QColor &getSelectColor(void) const { return pImage->getSelectColor(); }
	inline void setSelectColor(QColor &color) { pImage->setSelectColor(color); }
	inline const QColor &getDefaultBeamColor(void) const { return pImage->getDefaultBeamColor(); }
	inline void setDefaultBeamColor(QColor &color) { pImage->setDefaultBeamColor(color); }
	inline int getLssScale(void) const { return (int)pImage->getLssScale(); }
	inline void setLssScale(int scale) { pImage->setLssScale(scale); }
	inline int getQrlWidth(void) const { return pImage->getQrlWidth(); }
	inline void setQrlWidth(int width) { pImage->setQrlWidth(width); }
	inline int getCryoWidth(void) const { return pImage->getCryoWidth(); }
	inline void setCryoWidth(int width) { pImage->setCryoWidth(width); }
	inline int getConnLineWidth(void) const { return pImage->getConnLineWidth(); }
	inline void setConnLineWidth(int width) { pImage->setConnLineWidth(width); }
	inline BeamTypeDrawMethod getBeamTypeMethod(void) const { return (BeamTypeDrawMethod)pImage->getBeamTypeMethod(); }
	inline void setBeamTypeMethod(BeamTypeDrawMethod method) { pImage->setBeamTypeMethod((MainLHCImage::BeamTypeDrawMethod)method); }
	inline int getBeamTypeWidth(void) const { return pImage->getBeamTypeWidth(); }
	inline void setBeamTypeWidth(int width) { pImage->setBeamTypeWidth(width); }
	inline int getRedrawDelay(void) const { return pImage->getRedrawDelay(); }
	inline void setRedrawDelay(int delay) { pImage->setRedrawDelay(delay); }
	inline int getViewWidth(void) const { return pImage->getViewWidth(); }
	inline int getViewHeight(void) const { return pImage->getViewHeight(); }

	inline bool isReadOnly(void) const { return readOnly; }
	void setReadOnly(bool flag);
	
	// Accessors for VacMainView properties - unfortunately, inherited methods
	// are not recognized by macro
	inline const QColor &getSynPipeColor(void) const { return VacMainView::getSynPipeColor(); }
	inline void setSynPipeColor(QColor &color) { VacMainView::setSynPipeColor(color); }
	inline const QColor &getSynAltPipeColor(void) const { return VacMainView::getSynAltPipeColor(); }
	inline void setSynAltPipeColor(QColor &color) { VacMainView::setSynAltPipeColor(color); }
	inline const QColor &getSynPipePartColor(void) const { return VacMainView::getSynPipePartColor(); }
	inline void setSynPipePartColor(QColor &color) { VacMainView::setSynPipePartColor(color); }
	inline const QColor &getSynPassiveLineColor(void) const { return VacMainView::getSynPassiveLineColor(); }
	inline void setSynPassiveLineColor(QColor &color) { VacMainView::setSynPassiveLineColor(color); }
	inline const QColor &getSynPassiveFillColor(void) const { return VacMainView::getSynPassiveFillColor(); }
	inline void setSynPassiveFillColor(QColor &color) { VacMainView::setSynPassiveFillColor(color); }
	inline const QColor &getSynSectorColor(void) const { return VacMainView::getSynSectorColor(); }
	inline void setSynSectorColor(QColor &color) { VacMainView::setSynSectorColor(color); }
	inline int getSynPipeWidth(void) const { return VacMainView::getSynPipeWidth(); }
	inline void setSynPipeWidth(int width) { VacMainView::setSynPipeWidth(width); }
	inline const QColor &getGraphAxisColor(void) const { return VacMainView::getGraphAxisColor(); }
	inline void setGraphAxisColor(QColor &color) { VacMainView::setGraphAxisColor(color); }
	inline int getProfileBarWidth(void) const { return VacMainView::getProfileBarWidth(); }
	inline void setProfileBarWidth(int width) { VacMainView::setProfileBarWidth(width); }
	inline const QColor &getProfileAltSectorColor(void) const { return VacMainView::getProfileAltSectorColor(); }
	inline void setProfileAltSectorColor(QColor &color) { VacMainView::setProfileAltSectorColor(color); }
	inline const QColor &getProfileAltBarColor(void) const { return VacMainView::getProfileAltBarColor(); }
	inline void setProfileAltBarColor(QColor &color) { VacMainView::setProfileAltBarColor(color); }
	inline int getProfileRedrawDelay(void) const { return VacMainView::getProfileRedrawDelay(); }
	inline void setProfileRedrawDelay(int width) { VacMainView::setProfileRedrawDelay(width); }
	inline int getPollingPeriod(void) const { return VacMainView::getPollingPeriod(); }
	inline void setPollingPeriod(int period) { VacMainView::setPollingPeriod(period); }
	inline bool isPvssEvents(void) const { return VacMainView::isPvssEvents(); }
	inline void setPvssEvents(bool flag) { VacMainView::setPvssEvents(flag); }


public:
	MainLHC(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~MainLHC();

	void addLegendLabel(int x, int y, const char *text)
	{
		pImage->addLegendLabel(x, y, text);
	}

	int dataReady(void);
	inline bool isSelected(void) { return pTable->rowCount() > 0; }
	inline bool isContSelection(void) { return pTable->rowCount() > 0; }	// All selections are continuous in LHC
	QStringList getSelectedSectors(bool allSectors);

	// Implement abstract methods of VacMainView
	virtual void dialogRequest(int type, unsigned vacTypes, const QStringList &sectorList, DataEnum::DataMode mode);

	// Access
	inline unsigned getVacTypeMask(void) { return vacMask; }
	virtual void setMainFont(const QFont &font);

signals:
	void dialogRequested(int type, unsigned vacType, const QStringList &sectorList, DataEnum::DataMode mode);


signals:
	void sectorMousePress(int button, int x, int y, const char *sectorName);

protected:
	///////////////////////////////// Widgets /////////////////////////////////
	
	// Widget with image of LHC
	MainLHCImage	*pImage;

	// 'Label' for main parts combo box
	QGroupBox		*pMpBorder;

	// Combo box for main part selection
	QComboBox		*pCombo;

	// 'Label' for sectors list
	QGroupBox 		*pSectorBorder;

	// List of selected sectors
	QTableWidget			*pTable;

	// Button to add sector at the beginning of sector list
	QPushButton		*pAddStartPb;

	// Button to remove sector at the beginning of sector list
	QPushButton		*pDelStartPb;

	// Button to add sector at the end of sector list
	QPushButton		*pAddEndPb;

	// Button to remove sector at the end of sector list
	QPushButton		*pDelEndPb;

	// Check box - red beam
	QCheckBox		*pRedCb;

	// Check box - blue beam
	QCheckBox		*pBlueCb;

	// Check box - CRYO
	QCheckBox		*pCryoCb;

	// Check box - QRL
	QCheckBox		*pQrlCb;

	// This control is read-only - for WWW presentation
	bool			readOnly;

	virtual void resizeEvent(QResizeEvent *event);

	///////////////////////////// Current selection //////////////////////////////

	// true if there is any selection
	bool	selected;

	// Flag indicating if LHC ring sectors are selected
	bool	ringSelection;


	void buildLayout(void);
	void buildMpCombo(void);

	void changeVacSelection(int vacType, bool isSet);

	void selectFromSector(Sector *pSector);

	void setRingSelection(QStringList &sectorNames);
	void setLineSelectionFromTable(void);
	void setLineSelection(QStringList &sectorNames);

	void setMainPartName(void);
	void setSectorButtonsSens(void);
	void setCheckBoxesSens(void);
	bool tableContains(const char *name);
	void buildRangeFromSector(Sector *pSector);

	bool canExtendRange(bool changeAtStart);
	void changeRangeLimits(bool changeAtStart, int direction);
	void changeRangeType(void);
	bool areRingRangesOverlap(float outerStart, float outerEnd,
		float innerStart, float innerEnd, bool isRecursiveCall);
	void setTableCellText(int row, int col, const QString &text);

private slots:
	void imageMousePress(int button, int x, int y, const char *sectorName);
	void comboActivated(const QString &string);
	void addAtStart(void);
	void delAtStart(void);
	void delAtEnd(void);
	void addAtEnd(void);
	void switchRedCb(void);
	void switchBlueCb(void);
	void switchCryoCb(void);
	void switchQrlCb(void);
};

#endif	// MAINLHC_H
