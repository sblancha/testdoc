#ifndef	MOBILEHISTORYIMAGE_H
#define	MOBILEHISTORYIMAGE_H

// Class providing mobile equipment history image drawing

// Scale step for temperature axis
#define	TEMP_SCALE_STEP	(10)

#include "MobileHistoryEqpData.h"

#include "TimeAxisWidget.h"

#include <QWidget>

class VerticalAxisWidget;
class PlotImage;

class HistoryProcessor;

#include <QLabel>
#include <QTimer>

class MobileHistoryImage : public QWidget
{
	Q_OBJECT

public:
	MobileHistoryImage(QWidget *parent);
	~MobileHistoryImage();

	MobileHistoryEqpData *addEqp(Eqp *pEqp, int historyDepth, const QColor &color);
	MobileHistoryEqpData *addEqp(Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType channelValueType, int historyDepth, const QColor &color);
	int getMaximumDepth(void);
	void setTimeLimits(const QDateTime &start, const QDateTime &end);
	bool addHistoryValue(int id, const char *dpe, const QDateTime &time, const QVariant &value);
	bool finishHistoryRange(int id, const char *dpe);

	void rebuildImage(void);

	void showNextRange(const QDateTime &start, const QDateTime &end);
	void hideNextRange(void);

	const char *getDpName(int index);
	const Eqp *removeItem(int index);
	void removeAllItems(void);
	int getEqpIndex(Eqp *pEqp);
	int getEqpIndex(Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType channelValueType);
	void clearItemsUsed(void);
	void setItemUsed(int index);
	void removeUnusedItems(void);
	void setAbsStartTime(Eqp *pEqp, const QDateTime &newTime);

	void selectEqp(Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType channelValueType);
	void unselectAllRacks(void);

	void addExportValues(HistoryProcessor &processor);

	// Access
	inline bool isEmpty(void) const { return eqps.isEmpty(); }
	inline const QDateTime &getTimeMin(void) const { return pTimeAxis->getMin(); }
	inline const QDateTime &getTimeMax(void) const { return pTimeAxis->getMax(); }
	inline bool isOnline(void) const { return online; }
	void setOnline(bool flag);
	inline int getDepth(void) const { return depth; }
	void setDepth(int value);

signals:
	void imageUpdated(void);
	void valueAtPointerChanged(const char *timeStr, const char *valueStr);
	void scalesChanged(void);


protected:

	// Vertical axis for pressures
	VerticalAxisWidget	*pPressureAxis;

	// Vertical axis for temperatures
	VerticalAxisWidget	*pTempAxis;

	// Vertical axis for state values
	VerticalAxisWidget	*pStateAxis;

	// Time axis
	TimeAxisWidget		*pTimeAxis;

	// Widget with image
	PlotImage		*pImage;

	// Label for displaying next time range
	QLabel						*pLabel;

	// List of all devices shown in history
	QList<MobileHistoryEqpData *>	eqps;

	// Pointer to last found device with given DPE name, used to keep
	// reference to found DPE during massive offline data arrival
	MobileHistoryEqpData		*pCurrentEqp;

	// Timestamp of last image drawing
	QTime						drawTime;

	// Timer to redraw image after online value arrival
	QTimer						*pTimer;

	// Timer to keep online graph alive if no new values arrive for long time
	QTimer						*pAliveTimer;

	// Mutex to avoid device deleting from graph in the middle of operation with that device
	QMutex						mutex;
 
	// Depth of history [min]
	int					depth;

	// Flag indicating if history is in online mode
	bool				online;

	// In the worth case actions with axis from rebuildImage() method may result in calling rebuildImage() method
	// again (recursively) via signals of axis. To avoid this - let's use flag which is set when
	// entering rebuildImage() and cleared on exit.
	//	07.08.2012	L.Kopylov
	bool				buildingImage;

	void setInitialLimitsForItem(MobileHistoryEqpData *pItem);
	void draw(QPainter &painter);
	void calculateVerticalLimits(VerticalAxisWidget *pAxis);
	void calculateAxisActivity(void);
	void drawVerticalAxies(QPainter &painter);
	void drawTimeAxis(QPainter &painter);

	void movePointerTo(int newX);
	void showValueAtPointer(void);

	void setZoomingTime(const QRect &zoomRect);
	void setZoomingVertical(const QRect &zoomRect);

	void rebuildWithDelay(void);

	virtual void resizeEvent(QResizeEvent *event);

	void dump(const char *fileName);

private slots:
	void onlineValueArrived(void);
	void redrawTimerDone(void);
	void aliveTimerDone(void);

	void imageResized(void);
	void imagePointerMoved(int location);
	void imageZoomFinished(const QRect &zoomRect);

	void scaleChanged(bool dragging);
};

#endif	// MOBILEHISTORYIMAGE_H
