#ifndef DATAENUM_H
#define DATAENUM_H

#include "VacCtlUtilExport.h"

// Definition of common enums for equipment data

class VACCTLUTIL_EXPORT DataEnum
{
public:

	// Mode of data acquistion
	enum DataMode
	{
		Online = 0,
		Replay = 1,
		Polling = 2
	};

	// Type of value sent in signal dpeChanged
	enum Source
	{
		Own = 0,	// One of device's own DPEs have been changed
		Parent = 1,	// One of control parent's DPEs have been changed
		Plc = 2		// PLC alive state have been changed
	};

	// Type of main device value, used to decide which axis shall be used
	// in history and profile graphs
	enum ValueType
	{
		ValueTypeNone = 0,
		Pressure = 1,
		HighPressure = 18,	// High pressure devices - like for VGTR_CO
		CryoTemperature = 2,
		Intensity = 3,
		Temperature = 4,	// High temperature - like bakeout etc.
		Energy = 5,			// Beam energy - [GeV]
		BeamCurrent = 6,	// Beam current [mA]
		CritEnergy = 7,		// LHC critical energy [eV]
		PhF = 8,			// Photon flux [m-1 * s-1]
		PhDm = 9,			// Photon dose, [m-1]
		PhDmA = 10,			// Photon dose, [mA * h]
		BitState = 11,		// [0 or 1]
		CurrentVRPM = 12,	// Power supply current
		VoltageVRPM = 13,	// Power supply voltage
		FillNumber = 14,    // LHC fill number
		VvSetPoint = 15,	// Valve setpoint (VVG in Linac 4, 0...10 V)
		Percentage = 16,	// % for rotation speed
		LowCurrent = 17,	// for gauge measurement current
		Status = 19,		// Status (integer) to be displayed as a value. Introduced for BAKEOUT state
		HighVoltage = 20,	// High Voltage value, i.e. ion pump high voltage power supply - [V] form 0 to 8-9kV
		mA = 21             // milli-Amps value - linear scale, i.e. emission current of VGI
	};

	// Type of the alarm arrow in synoptic
	enum AlarmArrowType
	{
		nullArrowType = 0,
		vvs = 1,	// alarm for VVS 
		user = 2	// user type alarm arrow for external alarm 
	};
	// Direction of the alarm arrow in synoptic
	enum AlarmArrowVvsDirection
	{
		nullArrowDirection = 0,
		before = 1,	// Alarm for VVS located before in synoptic
		after = 2	// Alarm for VVS located after in synoptic
	};

	static const char *getUnits(ValueType valueType);


};
	
#endif	// DATAENUM_H
