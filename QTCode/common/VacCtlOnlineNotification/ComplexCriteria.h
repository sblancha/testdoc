#ifndef	COMPLEXCRITERIA_H
#define	COMPLEXCRITERIA_H

#include "VacCtlOnlineNotificationExport.h"

// (Combined) criteria for one functional type

#include <qlist.h>
#include <qdatetime.h>

class EqpMsgCriteria;

class Eqp;

class VACCTLONLINENOTIFICATION_EXPORT ComplexCriteria
{
public:
	// How single criteria are joined
	typedef enum
	{
		None = 0,
		And = 1,
		Or = 2
	} CriteriaJoin;

	static const char *joinTypeToString(int type);
	static int joinTypeParse(const char *string, QString &errMsg);
	
	ComplexCriteria();
	ComplexCriteria(const ComplexCriteria &source);
	ComplexCriteria(int type);
	virtual ~ComplexCriteria();
	ComplexCriteria & operator = (const ComplexCriteria &other);

	void addCriteria(EqpMsgCriteria *pCriteria) { pSimpleCriteriaList->append(pCriteria); }
	void addCriteria(ComplexCriteria *pCriteria) { pComplexCriteriaList->append(pCriteria); }
	void getFunctionalTypes(int **typeArray, int &nTypes);

	virtual void checkDevice(Eqp *pEqp);
	virtual bool matches(void);
	virtual void resetPendingMatch(void);
	virtual bool checkPendingMatch(const QDateTime &now);

	const QDateTime getEventTime(void) const;
	bool isMatchByValue(void) const;
	float getEventValue(void) const;
	int getMinDuration(void) const;

	EqpMsgCriteria *getValueCriteria(void);

	// Access
	inline int getJoinType(void) const { return joinType; }
	inline void setJoinType(int type) { joinType = type; }

protected:
	// How criteria are combined (see CriteriaJoin enum above)
	int		joinType;

	// List of simple criteria
	QList<EqpMsgCriteria *>	*pSimpleCriteriaList;

	// List of complex criteria
	QList<ComplexCriteria *>	*pComplexCriteriaList;

	void addFunctionalTypeToArray(int type, int **typeArray, int &nTypes);
};

#endif	// COMPLEXCRITERIA_H
