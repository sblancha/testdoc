//	Implementation of SingleEqpSelectorWidget class
////////////////////////////////////////////////////////////////////////////////

#include "SingleEqpSelectorWidget.h"

#include "DataPool.h"
#include "Eqp.h"
#include "FunctionalType.h"

#include <qlayout.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qtablewidget.h>
#include <QHeaderView>

////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

SingleEqpSelectorWidget::SingleEqpSelectorWidget(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	lastRow = -1;

	// Build control layout. Horizontal layout: filter on the left, table on the right side
	QHBoxLayout *pMainLayout = new QHBoxLayout(this);
	pMainLayout->setContentsMargins(0, 0, 0, 0);
	pMainLayout->setSpacing(4);

	// Vertical layout with filter text and 2 buttons
	QVBoxLayout *pCtlLayout = new QVBoxLayout();
	pCtlLayout->setSpacing(0);
	pMainLayout->addLayout(pCtlLayout);

	// Horizontal layout with filter  text and button
	/* Move filter button below filter text
	QHBoxLayout *pFilterLayout = new QHBoxLayout(pCtlLayout, 2, "Filter layout");

	pFilterText = new QLineEdit(this, "Filter text");
	pFilterLayout->addWidget(pFilterText, 10);

	pFilterButton = new QPushButton(">>", this, "Filter button");
	pFilterLayout->addWidget(pFilterButton);
	connect(pFilterButton, SIGNAL(clicked()), this, SLOT(filterButtonClicked()));
	*/
	pFilterText = new QLineEdit(this);
	pCtlLayout->addWidget(pFilterText, 10);

	QHBoxLayout *pFilterLayout = new QHBoxLayout();
	pFilterLayout->setSpacing(2);
	pCtlLayout->addLayout(pFilterLayout);
	pFilterLayout->addStretch(10);

	pFilterButton = new QPushButton(">>", this);
	pFilterLayout->addWidget(pFilterButton);
	connect(pFilterButton, SIGNAL(clicked()), this, SLOT(filterButtonClicked()));

	pCtlLayout->addStretch(10);

	// Horizontal layout to have table button on the right (close to table)
	QHBoxLayout *pTableButtonLayout = new QHBoxLayout();
	pTableButtonLayout->setSpacing(2);
	pCtlLayout->addLayout(pTableButtonLayout);

	QPushButton *pRemoveSelectedPb = new QPushButton("Remove selected", this);
	pTableButtonLayout->addWidget(pRemoveSelectedPb);
	connect(pRemoveSelectedPb, SIGNAL(clicked()), this, SIGNAL(removeSelected()));

	QPushButton *pRemoveAllPb = new QPushButton("Remove all", this);
	pTableButtonLayout->addWidget(pRemoveAllPb);
	connect(pRemoveAllPb, SIGNAL(clicked()), this, SIGNAL(removeAll()));

	pTableButtonLayout->addStretch(10);

	pTableButton = new QPushButton("Add selected", this);
	pTableButtonLayout->addWidget(pTableButton);
	connect(pTableButton, SIGNAL(clicked()), this, SLOT(tableButtonClicked()));
	pTableButton->setEnabled(false);

	pTable = new QTableWidget(0, 2, this);
	pMainLayout->addWidget(pTable);
	pTable->setSelectionMode(QAbstractItemView::MultiSelection);
	pTable->hideColumn(1);	// Column 1 (DP name) is not visible
	pTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
//	pTable->setColumnReadOnly(0, true);
	pTable->horizontalHeader()->hide();
//	pTable->setTopMargin(0);
	connect(pTable, SIGNAL(itemSelectionChanged()), this, SLOT(tableSelectionChanged()));
}


SingleEqpSelectorWidget::~SingleEqpSelectorWidget()
{
}

void SingleEqpSelectorWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);

	/*
	// Fix width of device list
	pTable->setMaximumWidth(pTable->width());
	// Fix widht of the only column
	pTable->setColumnWidth(0, pTable->width() - 4);
	*/
}

/*
**	FUNCTION
**		Slot receiving clicked() signal of filter button. Clear content
**		of table, find devices with name (VISIBLE!) matching new filter
**		and add their visible names + DP names to table
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SingleEqpSelectorWidget::filterButtonClicked(void)
{
	// Clear table
	pTable->setRowCount(0);
	lastRow = -1;
	pTableButton->setEnabled(false);

	// Get filter text
	QString filter = pFilterText->text();
	if(filter.isEmpty())
	{
		return;
	}

	// First put all found names to lists. If put them directly to table - table
	// is flickering too much
	QStringList dpNames;
	QStringList names;
	DataPool &pool = DataPool::getInstance();
	pool.findEqpMatchingMask(filter, dpNames, names);

	if(names.count())
	{
		int nRows = names.count();
		pTable->setRowCount(nRows);
		for(int row = 0 ; row < nRows ; row++)
		{
			QTableWidgetItem *pItem = new QTableWidgetItem(names.at(row));
			pItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			pTable->setItem(row, 0, pItem);
			pItem = new QTableWidgetItem(dpNames.at(row));
			pItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			pTable->setItem(row, 1, pItem);
		}
	}
	pTable->resizeColumnToContents(0);
}


/*
**	FUNCTION
**		Slot receiving selectionChanged() signal of table.
**		Sets sensitivity of button depending on selection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SingleEqpSelectorWidget::tableSelectionChanged(void)
{
	pTableButton->setEnabled(pTable->selectedItems().count() > 0);
	lastRow = -1;
}

/*
**	FUNCTION
**		Slot receiving clicked() signal of push button, related to table.
**		Build list of selected DP names and fires event
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SingleEqpSelectorWidget::tableButtonClicked(void)
{
	if(pTable->selectedItems().count() > 0)
	{
		emit addEqpRequest();
	}
	else
	{
		pTableButton->setEnabled(false);
	}
}

/*
**	FUNCTION
**		Find next row, selected in table, return DP name in that row
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		DP name in next selected row; or
**		NULL if no more rows selected
**
**	CAUTIONS
**		None
*/
const QString SingleEqpSelectorWidget::getNextSelectedDp(void)
{
	lastRow++;
	for( ; lastRow < pTable->rowCount() ; lastRow++)
	{
		if(pTable->item(lastRow, 0)->isSelected())
		{
			return pTable->item(lastRow, 1)->text();
		}
	}
	lastRow = -1;
	return QString();
}
