#include "InterlockView.h"

#include "DataPool.h"
#include "InterlockChain.h"
#include "qpainter.h"

InterlockView::InterlockView(QWidget *parent, Qt::WindowFlags f) :
QWidget(parent, f)
{
	QSize mySize(1600, 1200);
	this->resize(mySize);
	
	valveName = "";

}

InterlockView::~InterlockView()
{
	qDeleteAll(iconList);
	iconList.clear();

	qDeleteAll(iconConnectionList);
	iconConnectionList.clear();
}


InterlockView * InterlockView::create(QWidget *parent, QString buildingName)
{
	InterlockView * pView = NULL;

	pView = new InterlockView(parent);
	pView->valveName = buildingName;
	//pView->rack = rack;
	//pView->addWidget(rack);
	return pView;
}

void InterlockView::paintEvent(QPaintEvent *pEvent)
{
}

void InterlockView::setViewStart(int viewStart, int viewWidth, bool dragging){

}

void InterlockView::showChain(QString valveName)
{
	this->valveName = valveName;
	DataPool &pool = DataPool::getInstance();
	InterlockChain * pChain = pool.getInterlockChain(valveName);
	this->pChain = pChain;

	if (pChain == NULL)
	{
		return;
	}

	QColor beamColor;
	if (valveName.endsWith("R"))
	{
		beamColor = Qt::red;
	}
	else if (valveName.endsWith("B"))
	{
		beamColor = Qt::blue;
	}
	else
	{
		beamColor = Qt::black;
	}
	
	for (int i = 0; i < pChain->getEquipmentCount(); i++)
	{
		if (pChain->isEqpAtIndexVisible(i))
		{
			bool iconExists = this->iconExists(pChain->getEquipmentPtrAt(i)->getName());
			if (!iconExists)
			{
				InterlockIcon * pIcon = InterlockIcon::getIcon(pChain->getEquipmentPtrAt(i), this);
				pIcon->setName(pChain->getEquipmentPtrAt(i)->getName());
				pIcon->setAttributes(pChain->getEquipmentPtrAt(i)->getAttributeMapRef());
				pIcon->setBeamColor(beamColor);

				iconList.append(pIcon);

			}
		
				
		}
		
	}

	
	setIconPositions();
	drawConnectionLines();
}

/* Function set icon position_

Place icons in page according to the interlock chain structure.
Function returns -1 if there are problems in the chain information 
such as missing links 

*/
int InterlockView::setIconPositions()
{
	if (iconList.length() == 0)
	{
		return -1;
	}

	InterlockIcon * valveController;
	QList<InterlockIcon *> vrivcList;
	QList<InterlockIcon *> vrivdList;
	QList<InterlockIcon *> interlockSources;
	for (int i = 0; i < iconList.length(); i++)
	{
		switch (iconList.at(i)->getIconClass())
		{
		case Control:
			valveController = iconList.at(i);
			break;
		case InterlockSource:
			interlockSources.append(iconList.at(i));
			break;
		case TypeCInterlock:
			vrivcList.append(iconList.at(i));
			break;
		case TypeDInterlock:
			vrivdList.append(iconList.at(i));
			break;
		}
	}

	//sort for easier line drawing
	int vrivdCount = vrivdList.length();
	int vrivcCount = vrivcList.length();



	bool hasVrivd = vrivdCount > 0 ? true : false;
	if (hasVrivd)
	{
		QList<InterlockConnection> vrivdConnections = pChain->getSortedChildConnections(valveController->getName());
		int sortIndex = 0;

		for (int i = 0; i < vrivdCount; i++)
		{
			for (int j = 0; j < vrivdConnections.length(); j++)
			{
				if (QString::compare(vrivdConnections.at(j).childEquipment->getName(), vrivdList.at(i)->getName()) == 0)
				{
					vrivcList.move(i, sortIndex);
					sortIndex++;
					break;
				}
			}
		}


		sortIndex = 0;
		for (int i = 0; i < vrivdCount; i++)
		{
			QList<InterlockConnection> vrivcConnections = pChain->getSortedChildConnections(vrivdList.at(i)->getName());
			
			for (int k = 0; k < vrivcConnections.length(); k++)
			{
				for (int j = 0; j < vrivcCount; j++)
				{
					if (QString::compare(vrivcConnections.at(k).childEquipment->getName(), vrivcList.at(j)->getName()) == 0)
					{
						vrivcList.move(j, sortIndex);
						sortIndex++;
						break;
					}
			}
			}
		}
		
			
		valveController->moveIcon(this->width() * 7 / 8, this->height() / 2);
		if (vrivcCount == 1)
		{
			vrivcList.at(0)->moveIcon(150, this->height() / 2);
		}
		else if (vrivcCount == 2)
		{
			vrivcList.at(0)->moveIcon(150, this->height() * 3 / 7);
			vrivcList.at(1)->moveIcon(150, this->height() * 6 / 7);
		}
		else if (vrivcCount == 3)
		{
			this->resize(this->width(), this->height()*1.5);
			vrivcList.at(0)->moveIcon(150, this->height() * 3 / 11);
			vrivcList.at(1)->moveIcon(150, this->height() * 7 / 11);
			vrivcList.at(2)->moveIcon(150, this->height() * 10 / 11);
		}

		for (int i = 0; i < vrivdCount; i++)
		{
			vrivdList.at(i)->moveIcon(this->width() * 2 / 3 , this->height() * (i + 1) / (vrivdCount + 1));
		}
	}
	else
	{
		valveController->moveIcon(this->width() * 4 / 5, this->height() / 2);
		if (vrivcCount == 1)
		{
			vrivcList.at(0)->moveIcon(200, this->height() / 2);
		}
		else if (vrivcCount == 2)
		{
			vrivcList.at(0)->moveIcon(200, this->height() * 3 / 7);
			vrivcList.at(1)->moveIcon(200, this->height() * 6 / 7);
		}
		else if (vrivcCount == 3)
		{
			this->resize(this->width(), this->height()*1.5);
			vrivcList.at(0)->moveIcon(200, this->height() * 3 / 11);
			vrivcList.at(1)->moveIcon(200, this->height() * 7 / 11);
			vrivcList.at(2)->moveIcon(200, this->height() * 10 / 11);
		}
	}

	for (int i = 0; i < vrivcList.length(); i++)
	{
		QList<InterlockConnection> vrivcConnections = pChain->getSortedChildConnections(vrivcList.at(i)->getName());

		int positionIndex = 0;
		for (int k = 0; k < interlockSources.length(); k++)
		{
			for (int j = 0; j < vrivcConnections.length(); j++)
			{
				if (QString::compare(vrivcConnections.at(j).childEquipment->getName(), interlockSources.at(k)->getName()) == 0)
				{
					int verticalPosition = i ==  0 ? 30 : (i * this->height()/vrivcList.length());
					interlockSources.at(k)->moveIcon(positionIndex * 300 + 150, verticalPosition);
					positionIndex++;
					break;
				}
			}
		}
	}
	
	

}

void InterlockView::drawConnectionLines()
{

	QList<InterlockConnection> connections = pChain->getAllConnections();
	InterlockConnection currentConnection;
	for (int i = 0; i < connections.length(); i++)
	{
		InterlockConnection currentConnection = connections.at(i);
		QString parentName = currentConnection.parentEquipment->getName();
		QString childName = currentConnection.childEquipment->getName();
		InterlockIcon *parentIcon = this->getIcon(parentName);
		InterlockIcon *childIcon = this->getIcon(childName);
		QPoint lineStart, lineEnd;
		int startOffset, endOffset;
		bool writeParentCable = false;
		bool writeChildCable = false;
		InterlockIconConnection::Direction startDirection, endDirection;

		if (parentIcon == NULL)
		{
			if (childIcon != NULL)
			{
				lineEnd = childIcon->getConnectionPoint(currentConnection.childConnector);
				childIcon->getConnectionExtra(currentConnection.childConnector, endDirection, endOffset, writeChildCable);
				QString displayString = parentName + "\n" + currentConnection.parentConnector;
				InterlockIconConnection * pConnection = new InterlockIconConnection(lineEnd, endDirection, displayString, this);
				iconConnectionList.append(pConnection);
			}
			continue; // ADD HERE CODE TO DRAW JUST ARROW WITH NAME
		}
		lineStart = parentIcon->getConnectionPoint(currentConnection.parentConnector);
		parentIcon->getConnectionExtra(currentConnection.parentConnector, startDirection, startOffset, writeParentCable);
		InterlockIconConnection * pConnection;
		if (childIcon != NULL)
		{		
			
			lineEnd = childIcon->getConnectionPoint(currentConnection.childConnector);
			childIcon->getConnectionExtra(currentConnection.childConnector, endDirection, endOffset, writeChildCable);

			if (parentName.left(5).compare(childName.left(5)) == 0)
			{
				endOffset == 0;
				QString displayString = childName + "\n" + currentConnection.childConnector;
				pConnection = new InterlockIconConnection(lineStart, startDirection, displayString, this);
				iconConnectionList.append(pConnection);
				displayString = parentName + "\n" + currentConnection.parentConnector;
				pConnection = new InterlockIconConnection(lineEnd, endDirection, displayString, this);
				iconConnectionList.append(pConnection);
				continue;
			}
			
			pConnection = new InterlockIconConnection(lineStart, lineEnd, this);
			pConnection->setStartOffset(startDirection, startOffset);
			pConnection->setEndOffset(endDirection, endOffset);
			pConnection->setCableNumber(currentConnection.cable);
			pConnection->setWriteCables(writeParentCable, writeChildCable);
			pConnection->calculateGeometry();
		}
		else
		{
			endOffset == 0;
			QString displayString = childName + "\n" + currentConnection.childConnector;
			pConnection = new InterlockIconConnection(lineStart, startDirection, displayString, this);
		}

		
		iconConnectionList.append(pConnection);
		
	}

	fixConnectionConflicts();

}

InterlockIcon * InterlockView::getIcon(QString iconName)
{
	for (int i = 0; i < iconList.length(); i++)
	{
		if (QString::compare(iconName, iconList.at(i)->getName()) == 0)
		{
			return iconList.at(i);
		}
	}

	return NULL;
}

void InterlockView::fixConnectionConflicts()
{
	for (int i = 0; i < iconConnectionList.length() - 1; i++)
	{
		for (int j = i + 1; j < iconConnectionList.length(); j++)
		{
			iconConnectionList.at(i)->fixOverlapping(*iconConnectionList.at(j));

		}
		
	}
}

void InterlockView::setVisibleIcons()
{

}

QRect InterlockView::getBoundingRect()
{
	int xmin = this->width(), xmax = 0, ymin = this->height(), ymax = 0;
	for (int i = 0; i < iconList.length(); i++)
	{
		QRect iconRect = iconList.at(i)->getBoundingRect();

		if (iconRect.left() < xmin)
			xmin = iconRect.left();

		if (iconRect.right() > xmax)
			xmax = iconRect.right();

		if (iconRect.top() < ymin)
			ymin = iconRect.top();

		if (iconRect.bottom() > ymax)
			ymax = iconRect.bottom();
	
	}

	return QRect(QPoint(xmin, ymin), QPoint(xmax, ymax));
}

bool InterlockView::iconExists(QString name)
{
	for (int j = 0; j < iconList.size(); j++)
	{
		if (iconList.at(j)->getName() == name)
		{
			return true;
		}
	}

	return false;
}