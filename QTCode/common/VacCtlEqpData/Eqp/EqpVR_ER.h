// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Component Name: VacCtlEqpData librairy
Class: Equipment Vacuum contRol - bakE-out Rack (VR_ER)
Language: C++\Qt

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: This file contains the defined classes for the equipment Vacuum contRol - bakE-out Rack (VR_ER)

Limitations: To be used with vacuum framework.

Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------
#ifndef EQPVR_ER_H
#define	EQPVR_ER_H

//	Vacuum contRol - bakE-out Rack (VR_ER)

#include "Eqp.h"

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_NOEQP_VRER			255,192,203		//pink
#define COLOR_INVALID_VRER			63,127,255  	//blue
#define COLOR_OFF_VRER				255,255,255		//white
#define COLOR_ON_VRER				0,255,0			//green
//#define	COLOR_ERROR				255, 0, 0	//finally red not used for error
#define	COLOR_ERROR_VRER			220, 80, 200	//darkviolet
#define	COLOR_WARNING_VRER			255,165,0		//orange

#define	COLOR_SERVICES_VRER			255,165,0		//orange

//	Main state bits to be analyzed
#define	RR1_VALID_VRER				(0x4000) // 0x40 00
#define RR1_ERROR_VRER				(0x0080) // 0x00 80
#define	RR1_WARNING_VRER			(0x0040) // 0x00 40
#define	RR1_ON_VRER					(0x0200) // 0x00 02
#define	RR1_OFF_VRER				(0x0100) // 0x00 01
#define	RR1_GLOB_PCS_VRER			(0x0002) // 0x00 00
#define	RR1_GLOB_SAFE_VRER			(0x0001) // 0x00 00

class VACCTLEQPDATA_EXPORT EqpVR_ER : public Eqp
{
	Q_OBJECT

public:
	EqpVR_ER(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVR_ER(const EqpVR_ER &src) : Eqp(src) {}
#endif
	~EqpVR_ER();

	virtual void setActive(DataEnum::DataMode mode, bool active);

	virtual void getMaster(QString &masterDp); // no master channel , int &masterChannel);
	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isComAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? comAlarmReplay : comAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	virtual int getErrorSt() const { return errorStOnline; }

	public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE 
	unsigned	rr1Online;
	unsigned	rr1Replay;

	// Last knwon state of TCP connection to mobile (true == alarm) for online mode
	bool		comAlarmOnline;
	bool		comAlarmReplay;

	// Last known value of error code status for online mode
	unsigned	errorStOnline;
	unsigned	errorStReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, bool comAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, bool comAlarm, QString &string);
	virtual void getToolTipString(unsigned rr1, bool comAlarm, QString &string);

private:

#endif
};


#endif	// EQPVR_ER_H
