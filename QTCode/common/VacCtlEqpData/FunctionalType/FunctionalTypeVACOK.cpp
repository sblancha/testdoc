//	Implementation of FunctionalTypeVACOK class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVACOK.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

#include <stdio.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeVACOK::FunctionalTypeVACOK(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVACOK::fillCriteria(FunctionalType *pType)
{
	// Own criteria
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, OK, "CRYO alarm is OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"CRYO alarm in sector {sector} is OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"CRYO alarm in sector {sector} is NOT OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"CRYO alarm in main part {mainPart} is OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"CRYO alarm in main part {mainPart} is NOT OK");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeVACOK::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		result = "OK";
		break;
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeVACOK::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "OK"))
		{
			result = OK;
		}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}

