#ifndef	DPEVALUE_H
#define	DPEVALUE_H

#include "VacCtlHistoryDataExport.h"

//	Class holding one DPE value at given moment in time

#include <qdatetime.h>
#include <qvariant.h>

class VACCTLHISTORYDATA_EXPORT DpeValue
{
public:
	DpeValue();
	DpeValue(QDateTime ts, QVariant value);
	~DpeValue();
	DpeValue & operator = (const DpeValue &other);
	bool operator < (const DpeValue &other);

	// Access
	inline const QDateTime &getTs(void) const { return ts; }
	inline const QVariant &getValue(void) const { return value; }

protected:
	// Timestamp
	QDateTime	ts;

	// DPE value
	QVariant	value;
};

#endif	// DPEVALUE_H
