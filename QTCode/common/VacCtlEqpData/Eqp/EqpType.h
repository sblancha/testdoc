#ifndef EQPTYPE_H
#define EQPTYPE_H

#include "VacCtlEqpDataExport.h"

//	Equipment type declarations (and methods?)

class VACCTLEQPDATA_EXPORT EqpType
{
public:
	enum
	{
		None = 0,			// Uninitialized equipment type
		Passive = 1,		// Passive equipment (non-controllable in principle, pure background)
		LineStart = 2,		// Pseudo-equipment - start of this beam line in another beam line
		LineEnd = 3,		// Pseudo-equipment - end of this beam line in another beam line
		OtherLineStart = 4,	// Pseudo-equipment - start of another beam line in this beam line
		OtherLineEnd = 5,	// Pseudo-equipment - end of another beam line in this beam line
		VacDevice = 6,		// All controllable vacuum devices
		SectorBefore = 7,	// Controllable pseudo-device: vacuum sector before real device (sector view)
		SectorAfter = 8,	// Controllable pseudo-device: vacuum sector after real device (sector view)
		Aux = 9				// Non-controllable pseudo-device that NOT came from input files, but have been
							// added to synoptic to improve appearance
	};

	static const char *getName(int type);
};

#endif	// EQPTYPE_H
