//	Implementation of VacIconVINT_VELO class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVINT_VELO.h"

#include "EqpVINT_VELO.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OK				102,255,102
#define	COLOR_BAD				255,102,102

//	Main state bits to be analyzed
#define	RR1_INTERLOCK	(0x0001)


/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVINT_VELO::VacIconVINT_VELO(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	QRect labelRect = fontMetrics().boundingRect("00-00-00 00:00:00.000");
	setFixedSize(labelRect.width() + 2, (labelRect.height() + 2) * 2);
}

VacIconVINT_VELO::~VacIconVINT_VELO()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		It is supposed that this method will not be used for CRYO_TT
*/
VacIconGeometry VacIconVINT_VELO::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(width(), height(), width() >> 1, height() >> 1, 1);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVINT_VELO::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVINT_VELO"), "VacIconVINT_VELO::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVINT_VELO *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVINT_VELO::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVINT_VELO::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("TS1[1]");
	dpes.append("TS1[2]");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVINT_VELO::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVINT_VELO::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVINT_VELO::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	QRect myRect = rect();
	painter.eraseRect(myRect);
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Top label - device name on state's background
	const char *label = "Not connected";
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
			color.setRgb(COLOR_NOT_CONTROL);
			label = "Not Control";
			break;
		case Eqp::NotConnected:
			color.setRgb(COLOR_NOT_CONNECTED);
			label = "Not Connected";
			break;
		default:
			{
				label = pEqp->getVisibleName();
				unsigned rr1 = pEqp->getRR1(mode);
				if(rr1 & RR1_INTERLOCK)
				{
					color.setRgb(COLOR_BAD);
				}
				else
				{
					color.setRgb(COLOR_OK);
				}
			}
			break;
		}
	}
	painter.fillRect(1, 1, width() - 2, height() / 2 - 1, color);
	QRect labelRect = painter.fontMetrics().boundingRect(label);
	painter.drawText(myRect.center().x() - (labelRect.width() >> 1),
		labelRect.height() + 1, label);

	// Bottom label - timestamp string on colorless background
	QString tsLabel;
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			break;
		default:
			tsLabel = pEqp->getTsString(mode);
			break;
		}
	}
	else
	{
		tsLabel = "00-00-00 00:00:00.000";
	}
	labelRect = painter.fontMetrics().boundingRect(tsLabel);
	painter.drawText(myRect.center().x() - (labelRect.width() >> 1),
		myRect.height() - 1, tsLabel);
}

