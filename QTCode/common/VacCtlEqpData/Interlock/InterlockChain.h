#ifndef INTERLOCKCHAIN_H
#define INTERLOCKCHAIN_H
#include "VacCtlEqpDataExport.h"
#include <qlist.h>

#include "InterlockEqp.h"

struct VACCTLEQPDATA_EXPORT InterlockConnection{
	InterlockEqp * parentEquipment;
	QString parentConnector;
	InterlockEqp * childEquipment;
	QString childConnector;
	QString cable;
};

class VACCTLEQPDATA_EXPORT InterlockChain {

public:
	

	InterlockChain(QString valveName);
	~InterlockChain();

	inline void addEquipemnt(InterlockEqp * pEqp){ listOfEquipment.append(pEqp); };
	int addConnection(InterlockEqp * pParentEqp, QString parentConnector, InterlockEqp * pChildEqp, QString childConnector, QString cable);
	int addConnection(InterlockConnection connection);
	inline void chainFinished(){ isChainLoaded = true; };
	inline QString getValveName(){ return valveName; };
	inline int getEquipmentCount(){ return listOfEquipment.length(); };
	InterlockEqp * getEquipmentPtrAt(int index);
	InterlockEqp * getEquipmentPtr(QString equipmentName);
	QList<InterlockConnection> getSortedChildConnections(QString childName);
	inline QList<InterlockConnection> getAllConnections(){return listOfConnections; };
	static void sortConnectionList(QList<InterlockConnection> &listToSort);
	void setVisibilityMasks();
	bool isEqpAtIndexVisible(int index);

private:
	static bool connectionSort(InterlockConnection &conn1, InterlockConnection &conn2);
	QString valveName;
	QList<InterlockEqp *> listOfEquipment;
	QList<bool> visibilityMasks;
	QList<InterlockConnection> listOfConnections;
	bool isChainLoaded;
	
};

#endif