#include "EqpEXT_ALARM.h"

#include "FunctionalType.h"
#include "DataPool.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
EqpEXT_ALARM::EqpEXT_ALARM(const Eqp &source) : Eqp(source)
{
	functionalType = FunctionalType::EXT_ALARM;
}

EqpEXT_ALARM::~EqpEXT_ALARM()
{
}

void EqpEXT_ALARM::connect(InterfaceEqp *pDst, DataEnum::DataMode /* mode */)
{
	connectSelection(pDst);
}

void EqpEXT_ALARM::disconnect(InterfaceEqp *pDst, DataEnum::DataMode /* mode */)
{
	disconnectSelection(pDst);
}

void EqpEXT_ALARM::postProcess(void)
{
	DataPool &pool = DataPool::getInstance();
	const QHash<QByteArray, Eqp *> &eqpDict = pool.getEqpDict();
	for(int idx = 1 ; idx <= 5 ; idx++)
	{
		QString attrName("Source");
		attrName += QString::number(idx);

		QString sourceDp = getAttrValue(attrName.toLatin1().constData());
		if(sourceDp.isEmpty())
		{
			continue;
		}
		Eqp *pSourceEqp = pool.findEqpByDpName(sourceDp.toLatin1().constData());
		if(pSourceEqp)
		{
			if(pSourceEqp->getFunctionalType() != FunctionalType::VRPI) {
				pSourceEqp->addExtTarget(this);
				if (pSourceEqp->getFunctionalType() == FunctionalType::VA) {
					pSourceEqp->setIntlSourceExt(true);
				}
				continue;
			}

		}
		// If we are here - source may be VRPI
		foreach(Eqp *pEqp , eqpDict)
		{
			if(pEqp->getFunctionalType() != FunctionalType::VPI)
			{
				continue;
			}
			QString masterName = pEqp->getAttrValue("MasterName");
			if(!masterName.isEmpty())
			{
				if(pSourceEqp)
				{
					if(masterName == sourceDp)
					{
						pEqp->addExtTarget(this);
					}
				}
				else
				{
					if(masterName.startsWith(sourceDp))
					{
						pEqp->addExtTarget(this);
					}
				}
			}
		}
	}
}

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpEXT_ALARM::getMainStateString(QString &string, DataEnum::DataMode /* mode */)
{
	string = getAttrValue("Description");
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpEXT_ALARM::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	string = getVisibleName();
	string += ": ";
	QString stateString;
	getMainStateString(stateString, mode);
	string += stateString;
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpEXT_ALARM::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	getMainStateStringForMenu(string, mode);
}
#endif
