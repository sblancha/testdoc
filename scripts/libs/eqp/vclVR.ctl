
// Different controlles, 1st one is VR_GT then VR_PI then VR_ER(bakeout rack)

//LhcVacDeviceAction_VR_GT:
/** Execute action for gauge controller.
  It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
int LhcVacDeviceAction_VR_GT(string dpName, string dpType, dyn_anytype  action,
  dyn_string &exceptionInfo)
{
  mapping       valuesToWrite;
  bool		      possible;  
  unsigned      processType, act = action[1] & LVA_ACTION_MASK;
  mapping       mConnectDpes;
  
  switch(act)
  {
  case LVA_DEVICE_ON:
    valuesToWrite[".WR1"] = 0x02000000u;
    break;
  case LVA_DEVICE_OFF:
    valuesToWrite[".WR1"] = 0x01000000u;
    break;
  case LVA_DEVICE_RESET:
    valuesToWrite[".WR1"] = 0x80000000u;
    break;
  case LVA_DEVICE_SET_CMD:
    if(dynlen(action) != 4)
    {
      fwException_raise(exceptionInfo, "ERROR",
        "LhcVacDeviceAction_VR_GT(): unexpected number of parameters for command " +
        dynlen(action), "");
      return LVA_FAILURE;
    }
    valuesToWrite[".WR1"] = 0x04000000u |                 // WRITE_CMD bit
                            ((action[2] & 0xFF) << 16) |  // SLOT
                            ((action[3] & 0xFF) << 8);    // INDEX
    valuesToWrite[".WR_Value"] = action[4];
    break;
  case LVA_DEVICE_SAVEH:
    valuesToWrite[".WR1"] = 0x20000000u;
    break;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VR_GT: unknown action " + act, "");
    return LVA_FAILURE;
  }

  // Check if action is possible - TODO
  possible = true;
  if(!possible)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: Action forbidden for  <" + dpName + ">", "");
    return LVA_FAILURE;
  }	
  // Execute
  return LhcVacSetActionDpeValues(dpName, valuesToWrite, exceptionInfo);
}

//LhcVacDeviceAction_VR_PI:
/** Execute action for sublimation pump.
  It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param act: in, enum for action to be executed (LVA_xxx).	
  @param dpName: in, DP name
	@param dpType: in, DP type

	@param exceptionInfo: out, details of any exceptions are returned here
*/
int LhcVacDeviceAction_VR_PI(dyn_anytype action, string dpName, dyn_string &exceptionInfo)
{
  mapping      mapWrVal;
  unsigned     processType, act = action[1] & LVA_ACTION_MASK;
  switch(act) {
  case LVA_DEVICE_PROFIBUS:
    mapWrVal[".WR1"] = 0x0800u; //PROFIBUS WR bit 08-00_00-00 
    break;
  case LVA_DEVICE_LOCAL:
    mapWrVal[".WR1"] = 0x1000u; //LOCAL WR bit 10-00_00-00 
    break;
  case LVA_DEVICE_REMOTE_RESET:
    mapWrVal[".WR1"] = 0x0100u; //REMOTE_RESET WR bit 01-00_00-00 
    break;
  case LVA_DEVICE_POWER_CYCLE:
    mapWrVal[".WR1"] = 0x0200u; //POWER_CYCLE WR bit 02-00_00-00 
    break;
  case LVA_DEVICE_RESET_WRS:
    mapWrVal[".WR1"] = 0x0000u; 
    mapWrVal[".WR2"] = 0x0000u; 
    break;
  case LVA_VR_PI_RD_HWERRORCODE:
    mapWrVal[".WR1"] = 0x70u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_RD_FIRMWAREVERS:
    mapWrVal[".WR1"] = 0x72u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_RD_HWCTRLCONFIG:
    mapWrVal[".WR1"] = 0x71u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_RD_AUTOSTART:
    mapWrVal[".WR1"] = 0x74u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_RD_FANMODE:
    mapWrVal[".WR1"] = 0x73u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_RD_PRUNIT:
    mapWrVal[".WR1"] = 0x76u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_RD_FILTER:
    mapWrVal[".WR1"] = 0x16u; 
    break;
  case LVA_VR_PI_WR_AUTOSTART_OFF:
    mapWrVal[".WR2"] = 0x0u;    
    mapWrVal[".WR1"] = 0x11u; 
    break;
  case LVA_VR_PI_WR_AUTOSTART_LASTKNOWN:
    mapWrVal[".WR2"] = 0x1u;    
    mapWrVal[".WR1"] = 0x11u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_WR_FANMODE_ONLY:
    mapWrVal[".WR2"] = 0x0u;
    mapWrVal[".WR1"] = 0x12u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_WR_FANMODE_ALWAYS:
    mapWrVal[".WR2"] = 0x1u;
    mapWrVal[".WR1"] = 0x12u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_WR_PRUNIT_MBAR:
    mapWrVal[".WR2"] = 0x0u;
    mapWrVal[".WR1"] = 0x17u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_WR_PRUNIT_CURRENT:
    mapWrVal[".WR2"] = 0x1u;
    mapWrVal[".WR1"] = 0x17u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_WR_FILTER_FAST:
    mapWrVal[".WR2"] = 0x0u;
    mapWrVal[".WR1"] = 0x15u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_WR_FILTER_MEDIUM:
    mapWrVal[".WR2"] = 0x1u;
    mapWrVal[".WR1"] = 0x15u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VR_PI_WR_FILTER_SLOW:
    mapWrVal[".WR2"] = 0x2u;
    mapWrVal[".WR1"] = 0x15u; 
    //DebugTN("ORDER :", act);
    break;
  
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VR_PI: unknown action " + act, "");
    return LVA_FAILURE;
  }

  // Execute
  return LhcVacSetActionDpeValues(dpName, mapWrVal, exceptionInfo);
}

/**
  @brief Execute action for Relay ion pump controller
  @param act: in, enum for action to be executed (LVA_xxx).	
  @param dpName: in, DP name
  @param dpType: in, DP type
  @param exceptionInfo: out, details of any exceptions are returned here
*/
int LhcVacDeviceAction_VA_RI(dyn_anytype action, string dpName, dyn_string &exceptionInfo)
{
  mapping      mapWrVal;
  unsigned     processType, act = action[1] & LVA_ACTION_MASK;
  switch(act) {
  case LVA_VA_RI_UPDATE:
    mapWrVal[".WR1"] = 0x71u; 
    break;  
  case LVA_VA_RI_SOURCE_NEWVALUE_2HV:
    mapWrVal[".WR1"] = 0x12u; 
    break;
  case LVA_VA_RI_SOURCE_NEWVALUE_4HV:
    mapWrVal[".WR1"] = 0x12u; 
    break;
  case LVA_VA_RI_SOURCE_LASTORDER:
    mapWrVal[".WR1"] = 0x12u; 
    break;
  case LVA_VA_RI_SOURCE_DEFAULT:
    mapWrVal[".WR1"] = 0x12u; 
    break;
  case LVA_VA_RI_THRESHOLD_NEWVALUE:
    mapWrVal[".WR1"] = 0x14u;
    break;
  case LVA_VA_RI_THRESHOLD_LASTORDER:
    mapWrVal[".WR1"] = 0x14u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VA_RI_THRESHOLD_DEFAULT:
    mapWrVal[".WR1"] = 0x14u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VA_RI_HYST_NEWVALUE:
    mapWrVal[".WR1"] = 0x15u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VA_RI_HYST_LASTORDER:
    mapWrVal[".WR1"] = 0x15u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VA_RI_HYST_DEFAULT:
    mapWrVal[".WR1"] = 0x15u; 
    //DebugTN("ORDER :", act);
    break;
  case LVA_VA_RI_SET_UNIT_MBAR:
    mapWrVal[".WR1"] = 0x13u;
    mapWrVal[".WR2"] = 0;
    //DebugTN("ORDER :", act);
    break;
  case LVA_DEVICE_RESET_WRS:
    mapWrVal[".WR1"] = 0x0000u; 
    mapWrVal[".WR2"] = 0x0000u; 
    break;  
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VA_RI: unknown action " + act, "");
    return LVA_FAILURE;
  }

  // Execute
  return LhcVacSetActionDpeValues(dpName, mapWrVal, exceptionInfo);
}


/**
 * @brief Execute action for the bakeout rack VR_ER
 * 
 * It has been checked before call that this DP type understands this action.
 * @param act Enum for action to be executed (LVA_xxx).	
 * @param dpName DP name
 * @param dpType DP type
 *	@param exceptionInfo Details of any exceptions are returned here
 */
int LhcVacDeviceAction_VRE(string dpName, dyn_anytype action, dyn_string &exceptionInfo) {
  mapping      mapWrVal;
  unsigned     processType, act = action[1] & LVA_ACTION_MASK;
  switch(act) {
  case LVA_DEVICE_STANDBY:
    mapWrVal[".WR1"] = 0x20010000u; //
    break;
  case LVA_DEVICE_STOP:
    mapWrVal[".WR1"] = 0x20020000u; //
    break;
  case LVA_DEVICE_START:
    mapWrVal[".WR1"] = 0x20040000u; //
    break;
  case LVA_DEVICE_SAFEOFF:
    mapWrVal[".WR1"] = 0x20080000u; //
    break;
  case LVA_DEVICE_SAFEON:
    mapWrVal[".WR1"] = 0x20100000u; //
    break;    
  case LVA_DEVICE_PCSOFF:
    mapWrVal[".WR1"] = 0x20200000u; //
    break;
  case LVA_DEVICE_PCSON:
    mapWrVal[".WR1"] = 0x20400000u; //
    break;
  case LVA_DEVICE_RESET:
    mapWrVal[".WR1"] = 0xA0000000u; //
    break;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VRE: unknown action " + act, "");
    return LVA_FAILURE;
  }

  // Execute
  return LhcVacSetActionDpeValues(dpName, mapWrVal, exceptionInfo);
}
