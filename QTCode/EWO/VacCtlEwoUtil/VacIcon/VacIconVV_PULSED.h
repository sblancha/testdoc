#ifndef VACICONVV_PULSED_H
#define	VACICONVV_PULSED_H

// Icon for VV_PULSED

#include "VacIcon.h"

class EqpVV;

class VACCTLEWOUTIL_EXPORT VacIconVV_PULSED : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVV_PULSED(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVV_PULSED();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVV *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter);
	
	void setPainterForMainImage(QPainter &painter);
};

#endif	// VACICONVV_PULSED_H
