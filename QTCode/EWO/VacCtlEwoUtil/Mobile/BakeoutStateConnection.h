#ifndef	BAKEOUTSTATECONNECTION_H
#define	BAKEOUTSTATECONNECTION_H

// Class providing connection to RR1 DPE of bakeout rack

#include "InterfaceEqp.h"

class Eqp;

class BakeoutStateConnection : public InterfaceEqp
{
	Q_OBJECT

public:
	BakeoutStateConnection();
	~BakeoutStateConnection();

	// Access
	void setEqp(Eqp *pNewEqp);
	Eqp *getEqp(void) { return pEqp; }
	unsigned getRr1(void) const { return rr1; }
	bool isPlcAlarm(void) const { return plcAlarm; }

public slots:	// Implement slots of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);
		
signals:	// Define own signals
	void changed(void);

protected:
	// Pointer to device
	Eqp			*pEqp;

	// Last known RR1 value
	unsigned	rr1;

	// Last known PLC alarm value
	bool		plcAlarm;
};

#endif	// BAKEOUTSTATECONNECTION_H
