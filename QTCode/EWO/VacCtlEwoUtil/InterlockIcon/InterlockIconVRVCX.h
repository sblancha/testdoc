#ifndef	INTERLOCKICONVRVCX_H
#define	INTERLOCKICONVRVCX_H

#include "qwidget.h"
#include "InterlockIcon.h"

class InterlockIconVRVCX : public InterlockIcon
{
	Q_OBJECT
public:
	InterlockIconVRVCX(QWidget * parent = NULL);
	inline InterlockIconClass getIconClass() { return Control; };
	void moveIcon(int x, int y);
	QPoint getConnectionPoint(QString connectorName);
	void getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber);
	QRect getBoundingRect();
	virtual ~InterlockIconVRVCX();
private:
	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif