#ifndef FUNCTIONALTYPEITL_CMNT_H
#define	FUNCTIONALTYPEITL_CMNT_H

// Special functional type for CryoMaintain interlock source - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeITL_CMNT : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		CryoValid = 1,
		CryoMaintain = 2
	};

	FunctionalTypeITL_CMNT(int type, const char *name, const char *description);
	virtual ~FunctionalTypeITL_CMNT() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);
};

#endif	// FUNCTIONALTYPEITL_CMNT_H
