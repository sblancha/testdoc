//	Implementation of VacIconINJ_6B02	class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconINJ_6B02.h"

#include "EqpINJ_6B02.h"

#include <qpainter.h>
#include <QMouseEvent>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define	COLOR_ERROR				255,102,255
#define	COLOR_WARNING			255,255,102

// Colors for control and interlocks
/*
#define COLOR_GREEN				0,255,0
#define COLOR_YELLOW			255,255,0
#define COLOR_RED				255,0,0
#define	COLOR_WHITE				255,255,255
*/

//	Main state bits to be analyzed
//	Main state bits to be analyzed
#define	RR1_VALID			(0x40000000)
#define RR1_ERROR			(0x00800000)
#define	RR1_WARNING			(0x00400000)
#define	RR1_OFF				(0x01000000)
#define	RR1_ON				(0x02000000)


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
VacIconINJ_6B02::VacIconINJ_6B02(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(19, 41);
	setAreaGeometry();
}

VacIconINJ_6B02::~VacIconINJ_6B02()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for symbol areas rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconINJ_6B02::setAreaGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		valve.setRect(1, 3, 17, 17);
		pump.setRect(1, 23, 17, 17);
		break;
	case VacIconContainer::Down:
		pump.setRect(1, 1, 17, 17);
		valve.setRect(1, 20, 17, 17);
		break;
	case VacIconContainer::Left:
		pump.setRect(1, 1, 17, 17);
		valve.setRect(20, 1, 17, 17);
		break;
	case VacIconContainer::Right:
		valve.setRect(3, 1, 17, 17);
		pump.setRect(23, 1, 17, 17);
		break;
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconINJ_6B02::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		return VacIconGeometry(19, 41, 9, 44, -1);
		break;
	default:
		break;
	}
	return VacIconGeometry(41, 19, 41, 9, -1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconINJ_6B02::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(19);
		size.setHeight(41);
		break;
	default:
		size.setWidth(41);
		size.setHeight(19);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreaGeometry();
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconINJ_6B02::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpINJ_6B02"), "VacIconINJ_6B02::setEqp", pEqp->getDpName());
		this->pEqp = (EqpINJ_6B02 *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconINJ_6B02::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconINJ_6B02::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconINJ_6B02::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconINJ_6B02::getToolTip(const QPoint &pos, QString &text, QRect &rect)
{
	if(pEqp && connected)
	{
		// Where is the point? If in one of valve - show valve text,
		// otherwise show group name and state
		if(valve.contains(pos))
		{
			text = pEqp->getVisibleName();
			QString state;
			pEqp->getPartStateString(Eqp::EqpPartVVG, state, mode);
			text += ": ";
			text += state;
			rect = valve;
		}
		else if(pump.contains(pos))
		{
			pEqp->getToolTipString(text, mode);
			rect = pump;
		}
	}
	else
	{
		rect = this->rect();
		text = "Not connected";
	}
	return true;
}

/*
**	FUNCTION
**		Override VacIcon's method in order to have different menus for different
**		parts of icon
**
**	ARGUMENTS
**		pEvent	- Pointer to mouse event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconINJ_6B02::mousePressEvent(QMouseEvent *pEvent)
{
	if(pEqp)
	{
		QPoint mousePoint(pEvent->x(), pEvent->y());
		QPoint screenPoint = mapToGlobal(mousePoint);
		int buttonNbr = 0;
		switch(pEvent->button())
		{
		case Qt::LeftButton:
			buttonNbr = 1;
			break;
		case Qt::RightButton:
			buttonNbr = 2;
			break;
		default:
			break;
		}
		if(buttonNbr)
		{
			//if(valve.contains(mousePoint))
			//{
			//	emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), Eqp::EqpPartVVG, pEqp->getDpName());
			//}
			//else
			{
				emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), 0, pEqp->getDpName());
			}
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconINJ_6B02::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	drawValve(painter);

	drawPump(painter, ctlStatus, rr1, plcAlarm);
}

void VacIconINJ_6B02::drawValve(QPainter &painter)
{
	// Save painter coordinate system - interlocks will be drawn
	// in original (non-translated) coordinate system
	painter.save();

	// Prepare painter's coordinate system for image drawing
	// Coordinate system (0,0) is center of valve symbol
	painter.translate(valve.center().x(), valve.center().y());

	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	// Draw vacuum pipe - two short lines
	painter.fillRect(-(pipeWidth >> 1), -12, pipeWidth, 4, pipeColor);
	painter.fillRect(-(pipeWidth >> 1), 8, pipeWidth, 4, pipeColor);
	
	// Draw valve image
	setPainterForValve(painter);

	QPolygon points(4);
	points.setPoint(0, -8, -8);
	points.setPoint(1, 8, -8);
	points.setPoint(2, -8, 8);
	points.setPoint(3, 8, 8);
	painter.drawPolygon(points);

	// Arrow symbol on top of valve
	QPen arrowPen(Qt::black, 0);
	painter.setPen(arrowPen);
	painter.drawLine(8, 3, -8, -3);
	painter.drawLine(8, 3, 6, 4);
	painter.drawLine(8, 3, 7, 0);

	// Restore coordinate system to original
	painter.restore();
}

void VacIconINJ_6B02::drawPump(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	QColor	color(COLOR_UNDEFINED);
	painter.setPen(Qt::black);
	bool drawRect = true;
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		painter.fillRect(pump, color);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		painter.fillRect(pump, color);
		break;
	default:
		if(plcAlarm)
		{
			color.setRgb(COLOR_UNDEFINED);
			painter.fillRect(pump, color);
		}
		else if(!(rr1 & RR1_VALID))
		{
			color.setRgb(COLOR_UNDEFINED);
			painter.fillRect(pump, color);
		}
		else if(rr1 & RR1_ERROR)
		{
			color.setRgb(COLOR_ERROR);
			painter.fillRect(pump, color);
		}
		else if(rr1 & RR1_WARNING)
		{
			color.setRgb(COLOR_WARNING);
			painter.fillRect(pump, color);
		}
		else
		{
			if((rr1 & (RR1_ON | RR1_OFF)) == (RR1_ON | RR1_OFF))	// Both ON and OFF - error
			{
				color.setRgb(COLOR_ERROR);
				painter.fillRect(pump, color);
			}
			else if(rr1 & (RR1_ON | RR1_OFF))	// Either ON or OFF
			{
				// ON - basic color is green. However, it is completely green if NOMINAL
				bool fullOn = (rr1 && RR1_ON) != 0;
				if(fullOn)
				{
					color.setRgb(COLOR_ON);
					painter.fillRect(pump, color);
				}
				else
				{
					QPolygon points(3);
					points.setPoint(0, pump.left(), pump.top());
					points.setPoint(1, pump.right(), pump.top());
					points.setPoint(2, pump.left(), pump.bottom());
					color.setRgb(COLOR_ON);
					painter.setBrush(color);
					painter.drawPolygon(points);
					points.setPoint(0, pump.right(), pump.top());
					points.setPoint(1, pump.right(), pump.bottom());
					points.setPoint(2, pump.left(), pump.bottom());
					color.setRgb(COLOR_OFF);
					painter.setBrush(color);
					painter.drawPolygon(points);
					drawRect = false;
				}
			}
			else
			{
				color.setRgb(COLOR_UNDEFINED);
				painter.fillRect(pump, color);
			}
		}
		break;
	}

	// Draw pump rectangle, circle and line
	painter.setPen(Qt::black);
	painter.setBrush(Qt::NoBrush);
	if(drawRect)
	{
		painter.drawRect(pump.left(), pump.top(), pump.width() - 1, pump.height() - 1);
	}
	painter.drawArc(pump.left() + 2, pump.top() + 2, pump.width() - 5, pump.height() - 5,
			0, 360 * 16);
	QPoint center = pump.center();
	painter.drawLine(center.x() - 3, center.y(), center.x() + 3, center.y());
}

void VacIconINJ_6B02::setPainterForValve(QPainter &painter)
{
	QColor fillColor(COLOR_UNDEFINED);
	if(pEqp)
	{
		pEqp->getPartColor(Eqp::EqpPartVVG, fillColor, mode);
	}
	painter.setBrush(fillColor);
	painter.setPen(Qt::black);
}

