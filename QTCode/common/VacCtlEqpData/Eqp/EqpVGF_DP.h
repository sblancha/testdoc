#ifndef	EQPVGF_DP_H
#define EQPVGF_DP_H

//	VGF_DP - VG Profibus Full Range VGF(R+P), very similar to VG_STD, so just override few methods

#include "EqpVG_STD.h"


class VACCTLEQPDATA_EXPORT EqpVGF_DP : public EqpVG_STD
{
	Q_OBJECT

public:
	EqpVGF_DP(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVGF_DP(const EqpVGF_DP &src) : EqpVG_STD(src) {}
#endif
	~EqpVGF_DP();

protected:

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, float pr, bool plcAlarm, QColor &color);
#endif

};

#endif	// EQPVGF_DP_H
