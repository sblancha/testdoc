// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Component Name: VacCtlEqpData librairy
Class: Equipment TCP Communication (V_TCP)
Language: C++

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: This file contains the defined classes for the TCP Communication control and integrity control

Limitations: To be used with vacuum framework.

Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------

#ifndef EQPV_TCP_H
#define EQPV_TCP_H

#include "EqpPLC.h"

class VACCTLEQPDATA_EXPORT EqpV_TCP : public EqpPLC
{
	Q_OBJECT

public:
	EqpV_TCP(const Eqp &source);
#ifdef Q_OS_WIN
	EqpV_TCP(const EqpV_TCP &src) : EqpPLC(src) {}
#endif
	~EqpV_TCP() {}

	virtual void getStateDpes(QStringList &stateDpes);
	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual bool isAlive(DataEnum::DataMode mode) const;

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

protected:
	// Last known value of the only DPE for online mode
	bool	alarmOnline;

	// Last known value of the only DPE for replay mode
	bool	alarmReplay;

	// Last known value of RR1 DPE 
	unsigned	rr1Online;
	unsigned	rr1Replay;

	// Timestamp for alarm in online mode
	QDateTime	timeStampOnline;

	// Timestamp for alarm in replay mode
	QDateTime	timeStampReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(int alarm, unsigned rr1, QColor &color);
	virtual void getMainStateString(int alarm, unsigned rr1, QString &string);
#endif

};

#endif	// EQPV_TCP_H


