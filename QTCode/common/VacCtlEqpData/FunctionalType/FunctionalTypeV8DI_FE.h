#ifndef FUNCTIONALTYPEV8DI_FE_H
#define	FUNCTIONALTYPEV8DI_FE_H

// Special functional type for Digital Input monitor for interlock signals - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeV8DI_FE : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Active = 1,
		Unacknowledged = 2
	};

	FunctionalTypeV8DI_FE(int type, const char *name, const char *description);
	virtual ~FunctionalTypeV8DI_FE() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);
};

#endif	// FUNCTIONALTYPEV8DI_FE_H
