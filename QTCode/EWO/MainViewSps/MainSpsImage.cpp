//	Implementation of MainSpsImage class
/////////////////////////////////////////////////////////////////////////////////

#include "MainSpsImage.h"
#include "LegendLabel.h"

#include "VacMainView.h"

#include "ResourcePool.h"

#include "DataPool.h"
#include "BeamLine.h"
#include "BeamLinePart.h"
#include "Eqp.h"
#include "EqpType.h"
#include "Sector.h"
#include "SectorDrawPart.h"

#include <QTimer>
#include <QPixmap>
#include <QPoint>
#include <QPainter>
#include <QMouseEvent>
#include <QApplication>

#include <math.h>	// for rint()

#include "PlatformDef.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MainSpsImage::MainSpsImage(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	// Set reasonable defaults
	setMinimumWidth(200);
	setMinimumHeight(200);
	setMouseTracking(true);
	setAutoFillBackground(true);

	defaultColor.setRgb(0, 0, 255);
	selectColor.setRgb(255, 255, 255);

	lineWidth = 3;
	selectLineWidth = 2;

	borderLeft = borderRight = borderTop = borderBottom = 0;

	pTimer = new QTimer(this);
	pTimer->setSingleShot(true);
	connect(pTimer, SIGNAL(timeout()), this, SLOT(timerDone()));
	pLabel = new ExtendedLabel(this);
	pLabel->setLabelType(ExtendedLabel::Transparent);
	pLabel->setBackColor(QColor(255, 255, 204, 204));
	pLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	pLabel->hide();

	redrawDelay = 500;
	pRedrawTimer = new QTimer(this);
	pRedrawTimer->setSingleShot(true);
	connect(pRedrawTimer, SIGNAL(timeout(void)), this, SLOT(redrawTimerDone(void)));

	viewWidth = viewHeight = 0;
	drawn = false;

	// Connect to signal on equipment state changes
	connect(&connection, SIGNAL(stateChanged(void)), this, SLOT(eqpStateChanged(void)));

	// Finally...
	makeImage();
}

MainSpsImage::~MainSpsImage()
{
	while(!labels.isEmpty())
	{
		delete labels.takeFirst();
	}
	QHashIterator<QByteArray, QList<Eqp *> *> iter(neighborValves);
	while(iter.hasNext())
	{
		iter.next();
		QList<Eqp *> *pList = iter.value();
		if(pList != NULL)
		{
			pList->clear();
			delete pList;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////
// Event processing
/////////////////////////////////////////////////////////////////////////////////
void MainSpsImage::resizeEvent(QResizeEvent *event)
{
	makeImage();
	QWidget::resizeEvent(event);
}

void MainSpsImage::mouseMoveEvent(QMouseEvent *event)
{
	pTimer->stop();
	const QString sectorName = sectorAtLocation(event->x(), event->y());
	if(!sectorName.isEmpty())
	{
		pLabel->setText(sectorName);
		const QSize bound = pLabel->size();
		int x = event->x(), y = event->y();
		if((x + 4 + bound.width()) < width())
		{
			x += 4;
		}
		else
		{
			x -= bound.width() + 2;
		}
		if((y + 4 + bound.height()) < height())
		{
			y += 4;
		}
		else
		{
			y -= bound.height() + 2;
		}
		pLabel->move(x, y);
		if(!pLabel->isVisible())
		{
			pLabel->show();
		}
		pTimer->start(1500);
	}
	else if(pLabel->isVisible())
	{
		pLabel->hide();
	}
}

void MainSpsImage::mousePressEvent(QMouseEvent *event)
{
	const QString sectorName = sectorAtLocation(event->x(), event->y(), true);
	if(!sectorName.isEmpty())
	{
		QPoint mousePoint(event->x(), event->y());
		QPoint screenPoint = mapToGlobal(mousePoint);
		emit mousePress(event->button(), screenPoint.x(), screenPoint.y(), sectorName.toLatin1());
	}
}

void MainSpsImage::mouseDoubleClickEvent(QMouseEvent *event)
{
	const QString sectorName = sectorAtLocation(event->x(), event->y(), true);
	if(!sectorName.isEmpty())
	{
		VacMainView *pMainView = VacMainView::getInstance();
		if(pMainView)
		{
			pMainView->emitSectorDoubleClick(sectorName, DataEnum::Online);
		}
	}
}

void MainSpsImage::timerDone(void)
{
	if(pLabel->isVisible())
	{
		pLabel->hide();
	}
}

void MainSpsImage::redrawTimerDone(void)
{
	renewImage();
}

/*
**
**	FUNCTION
**		Slot to be invoked by MainSpsImageEqpConnect instance - state of one
**		of devices has been changed
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainSpsImage::eqpStateChanged(void)
{
	pRedrawTimer->stop();
	bool	immediateRedraw = false;
	int elapsed = drawTime.msecsTo(QTime::currentTime());
	if(elapsed < 0)	// Wrap over 24 hours?
	{
		immediateRedraw = true;
	}
	else if(elapsed > redrawDelay)
	{
		immediateRedraw = true;
	}
	if(immediateRedraw)
	{
		renewImage();
	}
	else
	{
		pRedrawTimer->start(redrawDelay);
	}
}

/*
**
**	FUNCTION
**		Prepare new image and display it - called when something has been
**		changed.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainSpsImage::renewImage(void)
{
	pRedrawTimer->stop();
	makeImage();
	drawTime.start();	// set to current time
}

/*
**
**	FUNCTION
**		Build new LHC image, set image as background for this widget
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainSpsImage::makeImage(void)
{
	QPixmap bgPixmap(size());
	QPalette pal = QApplication::palette();
	bgPixmap.fill(pal.color(QPalette::Window));
	QPainter painter(&bgPixmap);
	painter.setFont(font());
	draw(painter);
	pal.setBrush(QPalette::Window, bgPixmap);
	setPalette(pal);
}

/*
**
**	FUNCTION
**		Redraw the whole main view.
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		0	- Nothing to draw, or drawing area has zero size
**		1	- Drawing was done successfully
**		-1	- Error (for example, no drawing parameters)
**
**	CAUTIONS
**		None
**
*/
void MainSpsImage::draw(QPainter &painter)
{
	drawn = false;
	if((!width()) || (!height()))
	{
		return;
	}
	if(!findGeoLimits())
	{
		drawNotReadyText(painter);
		return;	// Nothing to draw
	}

	getBorders();

	if(!setNewGeometry(painter))
	{
		drawNotReadyText(painter);
		return;
	}

	// Drawing must be done in right order (bottom to top)
	drawLegendLabels(painter);
	drawSelection(painter);
	drawLines(painter);
	drawValveInterlocks(painter);
	drawn = true;
}

void MainSpsImage::getBorders(void)
{
	ResourcePool &pool = ResourcePool::getInstance();
	if(pool.getIntValue("MainViewBorderLeft", borderLeft) != ResourcePool::OK)
	{
		borderLeft = 0;
	}
	if(pool.getIntValue("MainViewBorderRight", borderRight) != ResourcePool::OK)
	{
		borderRight = 0;
	}
	if(pool.getIntValue("MainViewBorderTop", borderTop) != ResourcePool::OK)
	{
		borderTop = 0;
	}
	if(pool.getIntValue("MainViewBorderBottom", borderBottom) != ResourcePool::OK)
	{
		borderBottom = 0;
	}
}

/*
**	FUNCTION
**		Find limits of (X,Y) coordinates for all beam lines
**
**	PARAMETERS
**		None
**
**	RETURNS
**		true	- if there are some data and they can be drawn;
**		false	- otherwise.
**
**	CAUTIONS
**		None
*/
bool MainSpsImage::findGeoLimits(void)
{
	DataPool &pool = DataPool::getInstance();
	const QList<BeamLine *> &lines = pool.getLines();
	bool	isFirst = true;
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		if(pLine->isHidden())
		{
			continue;
		}
		if(isFirst)
		{
			isFirst = false;
			minX = pLine->getMinX();
			minY = pLine->getMinY();
			maxX = pLine->getMaxX();
			maxY = pLine->getMaxY();
		}
		else
		{
			if(pLine->getMinX() < minX)
			{
				minX = pLine->getMinX();
			}
			if(pLine->getMinY() < minY)
			{
				minY = pLine->getMinY();
			}
			if(pLine->getMaxX() > maxX)
			{
				maxX = pLine->getMaxX();
			}
			if(pLine->getMaxY() > maxY)
			{
				maxY = pLine->getMaxY();
			}
		}
	}
	if(isFirst)
	{
		return false;	// No beam lines at all
	}
	if((minX == maxX) || (minY == maxY))
	{
		return false;	// Nothing to draw
	}
	// Connect to equipment. Multiple calls are safe because MainSpsImageEqpConnect
	// keeps connection state internally
	connection.connect();
	return true;
}

/*
** FUNCTION
**		Set new geometry parameters (width, height) of drawing area,
**		calculate coefficients for translating world to screen coordinates
**		if new sizes are different from previously used ones.
**
** PARAMETERS
**		painter	- Painter that will be used for drawing
**
** RETURNS
**		true	- if geometry was calculated successfully;
**		false	- otherwise
**
** CAUTIONS
**		None
**
*/
bool MainSpsImage::setNewGeometry(QPainter & /* painter */)
{
	viewWidth = width();
	viewHeight = height();

	// Fix old conversion parameters - in order to decide if new neighbor search is needed
	float oldCoefX = coefX,
		oldCoefY = coefY,
		oldOffsetX = offsetX,
		oldOffsetY = offsetY;

	// Calculate new conversion coefficients
	int empty = lineWidth + selectLineWidth + 2;
	coefX = (float)((viewWidth - empty * 2 - (borderLeft + borderRight)) / (maxX - minX));
	coefY = (float)((viewHeight - empty * 2 - (borderTop + borderBottom)) / (maxY - minY));
	offsetX = empty + borderLeft;
	offsetY = empty + borderTop;

	if(coefX > coefY)
	{
		coefX = coefY;
		offsetX = (float )((viewWidth - (maxX - minX) * coefX) / 2. + 2.);
	}
	else if(coefX < coefY)
	{
		coefY = coefX;
		offsetY = (float)((viewHeight - (maxY - minY) * coefY) / 2. + 2.);
	}

	if((oldCoefX != coefX) || (oldCoefY != coefY) || (oldOffsetX != offsetX) || (oldOffsetY != offsetY))
	{
		buildNeighborValves();
	}
	return true;
}

/*
**	FUNCTION
**		Draw text indicating data are not initialized yet
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainSpsImage::drawNotReadyText(QPainter &painter)
{
	const char *message = "Not initialized yet";
	QRect bound = painter.boundingRect(0, 0, width(), height(), Qt::AlignCenter, message);
	painter.setPen(Qt::black);
	painter.drawText(width() / 2 - bound.width() / 2, height() / 2 - bound.height() / 2,
		message);
}

/*
**	FUNCTION
**		Draw legend labels
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSpsImage::drawLegendLabels(QPainter &painter)
{
	for(int idx = 0 ; idx < labels.count() ; idx++)
	{
		LegendLabel *pLabel = labels.at(idx);
		if(pLabel->getAngle())
		{
			painter.save();
			painter.translate(pLabel->getX(), pLabel->getY());
			painter.rotate(90);
			painter.drawText(0, 0, pLabel->getText());
			painter.restore();
		}
		else
		{
			painter.drawText(pLabel->getX(), pLabel->getY(), pLabel->getText());
		}
	}
}

/*
**	FUNCTION
**		Draw selected sectors.
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSpsImage::drawSelection(QPainter &painter)
{
	if(!selectedSectors.count())
	{
		return;
	}
	QPen selectPen(selectColor, lineWidth + selectLineWidth * 2, Qt::SolidLine,
		Qt::RoundCap, Qt::RoundJoin);
	painter.setPen(selectPen);
	for(int idx = 0 ; idx < selectedSectors.count() ; idx++)
	{
		Sector *pSector = selectedSectors.at(idx);
		const QList<SectorDrawPart *> &parts = pSector->getDrawParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			SectorDrawPart *pPart = parts.at(partIdx);
			drawSelPart(painter, pPart);
		}
	}
}

/*
**	FUNCTION
**		Draw one part of selected sector using previously set drawing parameters.
**
**	PARAMETERS
**		painter			- Painter used for drawing
**		pSectorDrawPart	- Pointer to part of sector to be drawn
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSpsImage::drawSelPart(QPainter &painter, SectorDrawPart *pSectorDrawPart)
{
	BeamLine		*pLine = pSectorDrawPart->getLine();
	BeamLinePart	*pPart = pSectorDrawPart->getStartPart();
	bool			isReverse = pPart->isReverse();
	Eqp				**eqpPtrs = pPart->getEqpPtrs();
	int				nEqpPtrs = pPart->getNEqpPtrs();
	int				eqpIdx = pSectorDrawPart->getStartEqpIdx();

	if(pLine->isVirtualLine())
	{
		return;	// L.Kopylov 14.04.2014
	}

	// Position iterator to start part
	const QList<BeamLinePart *> &parts = pLine->getParts();
	int partIdx;
	for(partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pSearch = parts.at(partIdx);
		if(pSearch == pPart)
		{
			break;
		}
	}
	// Draw - first put all coordinates for polyline to list
	QList<QPoint *>	pointList;
	while(true)
	{
		drawEqp(isReverse, eqpPtrs[eqpIdx], pointList);
		if((pPart == pSectorDrawPart->getEndPart()) && (eqpIdx == pSectorDrawPart->getEndEqpIdx()))
		{
			break;
		}
		eqpIdx++;
		if(eqpIdx >= nEqpPtrs)
		{
			partIdx++;
			if(partIdx >= parts.count())
			{
				if(pLine->isCircle())
				{
					partIdx = 0;
				}
				else
				{
					break;
				}
			}
			pPart = parts.at(partIdx);
			eqpPtrs = pPart->getEqpPtrs();
			nEqpPtrs = pPart->getNEqpPtrs();
			isReverse = pPart->isReverse();
			eqpIdx = 0;
		}
	}
	drawPolyline(painter, pointList);
	while(!pointList.isEmpty())
	{
		delete pointList.takeFirst();
	}
}

/*
**	FUNCTION
**		Draw all beam lines using draw order and color of special devices
**		for drawing different parts of lines.
**
**	PARAMETERS
**		painter		-Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainSpsImage::drawLines(QPainter &painter)
{
	DataPool &pool = DataPool::getInstance();
	const QList<BeamLine *> &lines = pool.getLines();

	// Find maximum draw order
	int		maxDrawOrder = 0, lineIdx;
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		if(pLine->isHidden())
		{
			continue;
		}
		if(pLine->isHideOnMainView())
		{
			continue;
		}
		int drawOrder = pLine->maxDrawOrder();
		if(drawOrder > maxDrawOrder)
		{
			maxDrawOrder = drawOrder;
		}
	}

	// Draw lines in right order
	for(int drawOrder = 0 ; drawOrder <= maxDrawOrder ; drawOrder++)
	{
		for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			BeamLine *pLine = lines.at(lineIdx);
			if(pLine->isHidden())
			{
				continue;
			}
			if(pLine->isHideOnMainView())
			{
				continue;
			}
			if(pLine->isCircle())
			{
				drawCircleLine(painter, pLine, drawOrder);
			}
			else
			{
				drawStraightLine(painter, pLine, drawOrder);
			}
		}
	}
}

void MainSpsImage::drawValveInterlocks(QPainter &painter)
{
	DataPool &pool = DataPool::getInstance();
	const QList<BeamLine *> &lines = pool.getLines();

	// Find maximum draw order
	int		maxDrawOrder = 0, lineIdx;
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		if(pLine->isHidden())
		{
			continue;
		}
		if(pLine->isHideOnMainView())
		{
			continue;
		}
		QList<BeamLinePart *> &parts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			QList<Eqp *> &eqpList = pPart->getEqpList();
			for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)	// Coordinate order is not important
			{
				Eqp *pEqp = eqpList.at(eqpIdx);
				if(pEqp->isSpecColor())
				{
					QColor color, borderColor;
					int order;
					if(pEqp->getInterlockColor(DataEnum::Online, color, borderColor, order, neighborValves.value(pEqp->getDpName())))
					{
						if(order >maxDrawOrder)
						{
							maxDrawOrder = order;
						}
					}
				}
			}
		}
	}
	if(maxDrawOrder == 0)
	{
		return;
	}

	// Draw lines in right order
	for(int drawOrder = 1 ; drawOrder <= maxDrawOrder ; drawOrder++)
	{
		for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			BeamLine *pLine = lines.at(lineIdx);
			if(pLine->isHidden())
			{
				continue;
			}
			if(pLine->isHideOnMainView())
			{
				continue;
			}
			drawValveInterlocksOnLine(painter, pLine, drawOrder);
		}
	}
}

void MainSpsImage::drawValveInterlocksOnLine(QPainter &painter, BeamLine *pLine, int drawOrder)
{
	QList<BeamLinePart *> &parts = pLine->getParts();
	for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)	// Coordinate order is not important
		{
			Eqp *pEqp = eqpList.at(eqpIdx);
			if(pEqp->isSpecColor())
			{
				QColor color, borderColor;
				int order;
				if(pEqp->getInterlockColor(DataEnum::Online, color, borderColor, order, neighborValves.value(pEqp->getDpName())))
				{
					if(order != drawOrder)
					{
						continue;
					}
					int	x = (int)rint((pEqp->getStartX() - minX) * coefX + offsetX),
						y = (int)rint(viewHeight - (pEqp->getStartY() - minY) * coefY - offsetY);
					painter.setBrush(color);
					painter.setPen(color);
					painter.drawPie(x - 5, y - 5, 11, 11, 0, 5760);
					painter.setPen(QPen(borderColor, 2));
					painter.drawArc(x - 5, y - 5, 11, 11, 0, 5760);
				}
			}
		}
	}
}

/*
**	FUNCTION
**		Draw all parts of cilrcular beam line with given draw order
**		using color of special devices for drawing different parts of
**		line. The major difference from drawing straight line is: it
**		can be necessary to start drawing not from the first device
**		in order to draw circle as closed path
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		pLine		- Pointer to beam line to draw
**		drawOrder	- Parts of line with this draw order shall be drawn
**
** RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void MainSpsImage::drawCircleLine(QPainter &painter, BeamLine *pLine, short drawOrder)
{
	const QList<BeamLinePart *> &parts = pLine->getParts();
	QColor	color = defaultColor;

	// Find where to start drawing
	BeamLinePart	*pStartPart = NULL;
	int				startEqpIdx = 0, partIdx;
	BeamLinePart *pPart;
	for(partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		pPart = parts.at(partIdx);
		Eqp	**eqpPtrs = pPart->getEqpPtrs();
		int nEqpPtrs = pPart->getNEqpPtrs();
		for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
		{
			Eqp *pEqp = eqpPtrs[eqpIdx];
			if(pEqp->isSpecColor() && (pEqp->getDrawOrder() == drawOrder))
			{
				pStartPart = pPart;
				startEqpIdx = eqpIdx;
				pEqp->getMainColor(color, DataEnum::Online);
				break;
			}
		}
	}
	if(!pStartPart)
	{
		if(drawOrder)
		{
			return;
		}
		// drawOrder = 0 - start from ANY beam part
		for(partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			pPart = parts.at(partIdx);
			if(pPart->getNEqpPtrs())
			{
				pStartPart = pPart;
				startEqpIdx = 0;
				break;
			}
		}
		if(!pStartPart)
		{
			return;
		}
	}

	// Draw
	QPen pen(color, lineWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
	painter.setPen(pen);
	pPart = pStartPart;
	int eqpIdx = startEqpIdx;
	bool isReverse = pPart->isReverse();
	Eqp	**eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	QList<QPoint *> pointList;
	bool isDrawing = true;
	while(true)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx++];
		if(isDrawing)
		{
			drawEqp(isReverse, pEqp, pointList);
		}
		if(eqpIdx >= nEqpPtrs)
		{
			partIdx++;
			if(partIdx >= parts.count())
			{
				partIdx = 0;
			}
			pPart = parts.at(partIdx);
			eqpIdx = 0;
			isReverse = pPart->isReverse();
			eqpPtrs = pPart->getEqpPtrs();
			nEqpPtrs = pPart->getNEqpPtrs();
		}
		if((pPart == pStartPart) && (eqpIdx == startEqpIdx))
		{
			break;
		}
		if(!pEqp->isSpecColor())
		{
			continue;
		}
		if(pEqp->getDrawOrder() != drawOrder)
		{
			drawPolyline(painter, pointList);
			isDrawing = false;
			continue;
		}
		else if(!isDrawing)
		{
			drawEqp(isReverse, pEqp, pointList);
			isDrawing = true;
		}
		QColor newColor;
		pEqp->getMainColor(newColor, DataEnum::Online);
		if(newColor != color)	// Start polyline with new color
		{
			drawPolyline(painter, pointList);
			color = newColor;
			pen.setColor(newColor);
			painter.setPen(pen);
			drawEqp(isReverse, pEqp, pointList);	
		}
	}
	drawPolyline(painter, pointList);
	while(!pointList.isEmpty())
	{
		delete pointList.takeFirst();
	}
}

/*
**	FUNCTION
**		Draw all parts of straight (== not cilrcular) beam line with
**		given draw order using color of special devices for drawing
**		different parts of line. All points in beam line are just
**		processed from first to last device
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		pLine		- Pointer to beam line to draw
**		drawOrder	- Parts of line with this draw order shall be drawn
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MainSpsImage::drawStraightLine(QPainter &painter, BeamLine *pLine, short drawOrder)
{
	const QList<BeamLinePart *> &parts = pLine->getParts();

	QColor color = defaultColor;
	short startDrawOrder = 0;
	findLineStartColor(pLine, color, startDrawOrder);
	bool isDrawing = startDrawOrder == drawOrder;
	QList<QPoint *> pointList;
	QPen pen(color, lineWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
	painter.setPen(pen);
	for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);
		bool	isReverse = pPart->isReverse();
		Eqp **eqpPtrs = pPart->getEqpPtrs();
		int nEqpPtrs = pPart->getNEqpPtrs();
		for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
		{
			Eqp	*pEqp = eqpPtrs[eqpIdx];
			if(isDrawing)
			{
				drawEqp(isReverse, pEqp, pointList);
			}
			if(!pEqp->isSpecColor())
			{
				continue;
			}
			if(pEqp->getDrawOrder() != drawOrder)
			{
				drawPolyline(painter, pointList);
				isDrawing = false;
				continue;
			}
			else if(!isDrawing)
			{
				drawEqp(isReverse, pEqp, pointList);
				isDrawing = true;
			}
			QColor newColor;
			pEqp->getMainColor(newColor, DataEnum::Online);
			if(newColor != color)
			{
				drawPolyline(painter, pointList);
				color = newColor;
				pen.setColor(color);
				painter.setPen(pen);
				drawEqp(isReverse, pEqp, pointList);
			}		
		}
	}
	drawPolyline(painter, pointList);
}

/*
**	FUNCTION
**		Find color and draw order to be used when drawing start of straight
**		line (up to first valve). If this line starts at another line - color
**		and draw order of parent line at point of connection is used
**
**	PARAMETERS
**		pLine		- Pointer to beam line to be drawn
**		color		- Color to be used for start of line will be put here
**		drawOrder	- Draw order for start of line will be put here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Color and draw order shall be set to default values BEFORE calling
**		this method: the method will set defaults if there are no special
**		values.
*/
void MainSpsImage::findLineStartColor(BeamLine *pLine, QColor &color, short &drawOrder)
{
	const QList<BeamLinePart *> &parts = pLine->getParts();
	for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);
		Eqp **eqpPtrs = pPart->getEqpPtrs();
		int nEqpPtrs = pPart->getNEqpPtrs();
		for(int n = 0 ; n < nEqpPtrs ; n++)
		{
			Eqp *pEqp = eqpPtrs[n];
			if(pEqp->isSpecColor())
			{
				return;
			}
			if(pEqp->getType() == EqpType::LineStart)
			{
				BeamLine *pParent = findLineOfStart(pLine->getName(), pEqp->getName());
				if(pParent)
				{
					searchDepth = 0;
					findColorInParent(pParent, pLine->getName(), color, drawOrder);
				}
				return;
			}
		}
	}
}

BeamLine *MainSpsImage::findLineOfStart(const char *lineName, const char *parentPartName)
{
	DataPool &pool = DataPool::getInstance();
	const QList<BeamLine *> &lines = pool.getLines();
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		const QList<BeamLinePart *> &parts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			if(strcmp(pPart->getName(), parentPartName))
			{
				continue;
			}
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if(pEqp->getType() == EqpType::OtherLineStart)
				{
					if(!strcmp(pEqp->getName(), lineName))
					{
						return pLine;
					}
				}
			}
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Find color and draw order to be used when drawing start of straight
**		line (up to first valve). If this line starts at another line - color
**		and draw order of parent line at point of connection is used
**
**	PARAMETERS
**		pLine			- Pointer to parent line
**		childLineName	- Name of child line
**		color			- Color to be used for start of line will be put here
**		drawOrder		- Draw order for start of line will be put here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Color and draw order shall be set to default values BEFORE calling
**		this method: the method will set defaults if there are no special
**		values.
*/
void MainSpsImage::findColorInParent(BeamLine *pLine, const char *childLineName,
	QColor &color, short &drawOrder)
{
	if(searchDepth > 10)	// Reasonable???
	{
		return;
	}
	bool	gotColor = false,
			startFound = false;
	const QList<BeamLinePart *> &parts = pLine->getParts();
	for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);
		Eqp **eqpPtrs = pPart->getEqpPtrs();
		int nEqpPtrs = pPart->getNEqpPtrs();
		for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
		{
			Eqp *pEqp = eqpPtrs[eqpIdx];
			if(pEqp->getType() == EqpType::OtherLineStart)
			{
				if(!strcmp(pEqp->getName(), childLineName))
				{
					startFound = true;
					break;
				}
			}
			else if(pEqp->isSpecColor())
			{
				pEqp->getMainColor(color, DataEnum::Online);
				drawOrder = pEqp->getDrawOrder();
				gotColor = true;
			}
		}
		if(startFound)
		{
			break;
		}
	}
	if(gotColor || (!startFound))
	{
		return;
	}

	// We did not find color. There can be two reasons:
	// 1) Parent line is circular line and valve is before 'line start'
	// 2) Parent line itself starts at another line and color shall be taken
	//	from that parent line
	if(pLine->isCircle())
	{
		for(int partIdx = parts.count() - 1 ; partIdx >= 0 ; partIdx--)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			for(int eqpIdx = nEqpPtrs - 1 ; eqpIdx >= 0 ; eqpIdx--)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if(pEqp->isSpecColor())
				{
					pEqp->getMainColor(color, DataEnum::Online);
					drawOrder = pEqp->getDrawOrder();
					return;
				}
			}
		}
	}
	else
	{
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			for(int n = 0 ; n < nEqpPtrs ; n++)
			{
				Eqp *pEqp = eqpPtrs[n];
				if(pEqp->getType() == EqpType::LineStart)
				{
					BeamLine *pParent = findLineOfStart(pLine->getName(), pEqp->getName());
					if(pParent)
					{
						findColorInParent(pParent, pLine->getName(), color, drawOrder);
					}
					return;
				}
			}
		}
	}
}

/*
**	FUNCTION
**		Put start and (optionally) end coordinates of one device to list of points
**		for polyline drawing
**
**	PARAMETERS
**		isReverse	- true if part of beam line containing equipment shall
**						be drawn in reverse order (from end to start).
**		pEqp		- Pointer to device
**		points		- List of polyline point coordinates where new coordinates
**						shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSpsImage::drawEqp(bool isReverse, Eqp *pEqp, QList<QPoint *> &points)
{
	// Skip equipment with negative length - it comes from survey DB,
	// but I don't know what negative length means
	if(pEqp->getLength() < 0.0)
	{
		return;
	}
	if(isReverse)
	{
		nextDrawPoint(pEqp->getEndX(), pEqp->getEndY(), points);
		if(pEqp->getLength() > 0.0)
		{
			nextDrawPoint(pEqp->getStartX(), pEqp->getStartY(), points);
		}
	}
	else
	{
		nextDrawPoint(pEqp->getStartX(), pEqp->getStartY(), points);
		if(pEqp->getLength() > 0.0)
		{
			nextDrawPoint(pEqp->getEndX(), pEqp->getEndY(), points);
		}
	}
}

/*
**	FUNCTION
**		Convert given world coordinates to screen coordinates;
**		adde resulting screen coordinates to list of points for polyline
**		drawing (ONLY if new coordinates differ from previous ones)
**
**	PARAMETERS
**		coordX		- World X coordinate
**		coordY		- World Y coordinate
**		points		- List of polyline point coordinates where new coordinates
**						shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSpsImage::nextDrawPoint(float coordX, float coordY, QList<QPoint *> &points)
{
	int	x = (int)rint((coordX - minX) * coefX + offsetX),
		y = (int)rint(viewHeight - (coordY - minY) * coefY - offsetY);

	QPoint *pPoint = points.isEmpty() ? NULL : points.last();
	if(!pPoint)
	{
		points.append(new QPoint(x, y));
	}
	else if((pPoint->x() != x) || (pPoint->y() != y))
	{
		points.append(new QPoint(x, y));
	}
}

/*
**	FUNCTION
**		Draw polyline using given painter and list of points. Drawing
**		parameters (pen) are set in painter before calling this method
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		pointList	- List of points, forming polyline
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSpsImage::drawPolyline(QPainter &painter, QList<QPoint *> &pointList)
{
	if(pointList.isEmpty())
	{
		return;
	}
	QPolygon points(pointList.count());
	for(int idx = 0 ; idx < pointList.count() ; idx++)
	{
		QPoint *pPoint = pointList.at(idx);
		points.setPoint(idx, *pPoint);
	}
	painter.drawPolyline(points);
	while(!pointList.isEmpty())
	{
		delete pointList.takeFirst();
	}
}

/*
**
**	FUNCTION
**		Find sector at given mouse pointer location
**
**	PARAMETERS
**		x	- Mouse pointer X-coordinate
**		y	- Mouse pointer Y-coordinate
**		pureSectorName	- true if only sector name (without main part) is needed
**
**	RETURNS
**		Name sector at mouse pointer location, or
**		NULL if there is no sector at that location
**
**	CAUTIONS
**		None
**
*/
const QString MainSpsImage::sectorAtLocation(int x, int y, bool pureSectorName)
{
	static QString resultName;

	if(!drawn)
	{
		return QString();
	}
	// Convert screen coordinates to world coordinates
	float	worldX = (x - offsetX) / coefX + minX,
			worldY = (viewHeight - y - offsetY) / coefY + minY,
			worldDelta = lineWidth / coefX,
			bestDelta = maxX - minX;
	worldDelta *= worldDelta;
	if((maxY - minY) > bestDelta)
	{
		bestDelta = maxY - minY;
	}
	bestDelta *= bestDelta;
	// Find closets equipment
	DataPool &pool = DataPool::getInstance();
	const QList<BeamLine *> &lines = pool.getLines();
	BeamLine		*pBestLine = NULL;
	BeamLinePart	*pBestPart = NULL;
//	Eqp	*pBestEqp = NULL;
	int				bestEqpIdx = -1;
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		if(pLine->isHidden())
		{
			continue;
		}
		const QList<BeamLinePart *> &parts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			Eqp	**eqpPtrs = pPart->getEqpPtrs();
			int	nEqpPtrs = pPart->getNEqpPtrs();
			for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				float realDelta = (pEqp->getStartX() - worldX) * (pEqp->getStartX() - worldX) +
					(pEqp->getStartY() - worldY) * (pEqp->getStartY() - worldY);
				if(realDelta < bestDelta)
				{
					bestDelta = realDelta;
					pBestLine = pLine;
					pBestPart = pPart;
					bestEqpIdx = eqpIdx;
//					pBestEqp = pEqp;
				}
				if(!pEqp->getLength())
				{
					continue;
				}
				realDelta = (pEqp->getEndX() - worldX) * (pEqp->getEndX() - worldX) +
					(pEqp->getEndY() - worldY) * (pEqp->getEndY() - worldY);
				if(realDelta < bestDelta)
				{
					bestDelta = realDelta;
					pBestLine = pLine;
					pBestPart = pPart;
					bestEqpIdx = eqpIdx;
//					pBestEqp = pEqp;
				}
			}
		}
	}
	if(bestDelta > worldDelta)
	{
		return NULL;
	}
	if(!pBestLine)
	{
		return NULL;
	}
/*
if(pBestEqp)
{
printf("MainSpsImage::sectorAtLocation(): best eqp %s @ (%8.3f %8.3f)\n",
	pBestEqp->getName(), pBestEqp->getStartX(), pBestEqp->getStartY());
fflush(stdout);
}
*/
	// Find sector corresponding to best equipment location
	const QList<Sector *> &sectors = pool.getSectors();
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		const QList<SectorDrawPart *> &drawParts = pSector->getDrawParts();
		for(int drawPartIdx = 0 ; drawPartIdx < drawParts.count() ; drawPartIdx++)
		{
			SectorDrawPart *pDrawPart = drawParts.at(drawPartIdx);
			if(pDrawPart->getLine() != pBestLine)
			{
				continue;
			}

			const QList<BeamLinePart *> &parts = pBestLine->getParts();
			bool gotStart = false;
			for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
			{
				BeamLinePart *pPart = parts.at(partIdx);
				if(pPart == pDrawPart->getStartPart())
				{
					gotStart = true;
				}
				if(pPart == pBestPart)
				{
					if(!gotStart)
					{
						break;
					}
					if(pDrawPart->isCircleClose())
					{
						if(bestEqpIdx <= pDrawPart->getEndEqpIdx())
						{
							if(!pureSectorName)
							{
								const QList<MainPartMap *> &mapList = pSector->getMainPartMaps();
								if (!mapList.isEmpty()) {
									MainPartMap *pMap = mapList.first();
									resultName = "";
									resultName += pSector->getName();
									resultName += " (";
									resultName += pMap->getMainPart()->getName();
									resultName += ")";
									return QString(resultName);
								}
							}
							return pSector->getName();
						}
						if(bestEqpIdx >= pDrawPart->getStartEqpIdx())
						{
							if(!pureSectorName)
							{
								const QList<MainPartMap *> &mapList = pSector->getMainPartMaps();
								if (!mapList.isEmpty()) {
									MainPartMap *pMap = mapList.first();
									resultName = "";
									resultName += pSector->getName();
									resultName += " (";
									resultName += pMap->getMainPart()->getName();
									resultName += ")";
									return QString(resultName);
								}
							}
							return pSector->getName();
						}
					}
					else
					{
						if(pDrawPart->getStartPart() == pDrawPart->getEndPart())
						{
							if(pDrawPart->getStartEqpIdx() > bestEqpIdx)
							{
								continue;
							}
							if(pDrawPart->getEndEqpIdx() < bestEqpIdx)
							{
								continue;
							}
						}
						else
						{
							if(pDrawPart->getStartPart() == pBestPart)
							{
								if(bestEqpIdx < pDrawPart->getStartEqpIdx())
								{
									continue;
								}
							}
							else if(pDrawPart->getEndPart() == pBestPart)
							{
								if(bestEqpIdx > pDrawPart->getEndEqpIdx())
								{
									continue;
								}
							}
						}
						if(!pureSectorName)
						{
							const QList<MainPartMap *> &mapList = pSector->getMainPartMaps();
							if (!mapList.isEmpty()) {
								MainPartMap *pMap = mapList.first();
								resultName = "";
								resultName += pSector->getName();
								resultName += " (";
								resultName += pMap->getMainPart()->getName();
								resultName += ")";
								return QString(resultName);
							}
						}
						return pSector->getName();
					}
				}
				if(pPart == pDrawPart->getEndPart())
				{
					break;
				}
			}
		}
	}
	return QString();
}

/*
**	FUNCTION
**		Add legend label to list of labels to be drawn
**
**	PARAMETERS
**		x		- X-coordinate for label
**		y		- Y-coordinate for label
**		text	- label text
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSpsImage::addLegendLabel(int x, int y, const char *text)
{
	labels.append(new LegendLabel(x, y, text));
}

/*
**	FUNCTION
**		Add legend label to list of labels to be drawn
**
**	PARAMETERS
**		x		- X-coordinate for label
**		y		- Y-coordinate for label
**		angle	- rotation angle for label
**		text	- label text
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSpsImage::addRotatedLegendLabel(int x, int y, int angle, const char *text)
{
	labels.append(new LegendLabel(x, y, angle, text));
}

void MainSpsImage::buildNeighborValves(void)
{
	//qDebug("buildNeighborValves() - start\n");
	DataPool &pool = DataPool::getInstance();
	const QList<BeamLine *> &lines = pool.getLines();

	int		lineIdx;
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		if(pLine->isHidden())
		{
			continue;
		}
		if(pLine->isHideOnMainView())
		{
			continue;
		}
		const QList<BeamLinePart *> &parts = pLine->getParts();

		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
			{
				Eqp	*pEqp = eqpPtrs[eqpIdx];
				if(pEqp->isSpecColor())
				{
					buildNeighborsOfValve(pEqp, lines);
				}
			}
		}
	}
	//qDebug("buildNeighborValves() - finish\n");
}

void MainSpsImage::buildNeighborsOfValve(Eqp *pValveEqp, const QList<BeamLine *> &lines)
{
	int	valveX = (int)rint((pValveEqp->getStartX() - minX) * coefX + offsetX),
		valveY = (int)rint(viewHeight - (pValveEqp->getStartY() - minY) * coefY - offsetY);
	QList<Eqp *> *pNeighborList = neighborValves.value(pValveEqp->getDpName());
	if(pNeighborList == NULL)
	{
		pNeighborList = new QList<Eqp *>();
	}
	else
	{
		pNeighborList->clear();
	}

	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		if(pLine->isHidden())
		{
			continue;
		}
		if(pLine->isHideOnMainView())
		{
			continue;
		}
		const QList<BeamLinePart *> &parts = pLine->getParts();

		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
			{
				Eqp	*pEqp = eqpPtrs[eqpIdx];
				if(!pEqp->isSpecColor())
				{
					continue;
				}
				if(pEqp == pValveEqp)
				{
					continue;
				}
				int	x = (int)rint((pEqp->getStartX() - minX) * coefX + offsetX),
					y = (int)rint(viewHeight - (pEqp->getStartY() - minY) * coefY - offsetY);
				float delta = ((float)(x - valveX)) * ((float)(x - valveX)) + ((float)(y - valveY)) * ((float)(y - valveY));
				if(delta <= 40)
				{
					pNeighborList->append(pEqp);
					//qDebug("%s is neighbor for %s\n", pEqp->getDpName(), pValveEqp->getDpName());
				}
			}
		}
	}

	if(pNeighborList->isEmpty())
	{
		neighborValves.remove(pValveEqp->getDpName());
		delete pNeighborList;
	}
	else
	{
		neighborValves.insert(pValveEqp->getDpName(), pNeighborList);
	}
}

