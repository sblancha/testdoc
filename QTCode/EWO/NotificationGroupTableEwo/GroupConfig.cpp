//	Implementation of GroupConfig class
/////////////////////////////////////////////////////////////////////////////////

#include "GroupConfig.h"
#include <stdio.h>

GroupConfig::GroupConfig(const QString &groupDpName, const QStringList &simpleDpNames)
	: SmsConfig(groupDpName)
{
	foreach(QString simpleName, simpleDpNames)
	{
		addGroup(simpleName);
	}
}

GroupConfig::~GroupConfig()
{
	while(!list.isEmpty())
	{
		delete list.takeFirst();
	}
}

SmsGroup *GroupConfig::addGroup(const QString &dpName)
{
	SmsGroup *pGroup = new SmsGroup(dpName);
	list.append(pGroup);
	connect(pGroup, SIGNAL(changed(SmsGroup *)),
		this, SLOT(configStateChange(SmsGroup *)));
	connect(pGroup, SIGNAL(numMsgChanged(SmsGroup *, int)),
		this, SLOT(groupNumMsgChange(SmsGroup *, int)));
	calculateState();
	emit changed(this);
	return pGroup;
}

void GroupConfig::removeGroup(const QString &dpName)
{
	bool isChanged = false;
	for(int idx = 0 ; idx < list.count() ; idx++)
	{
		SmsGroup *pGroup = list.at(idx);
		if(pGroup->getDpName() == dpName)
		{
			if(!pGroup->isDeleted())
			{
				pGroup->setDeleted(true);
				isChanged = true;
			}
		}
	}
	if(isChanged)
	{
		calculateState();
		emit changed(this);
	}
}

void GroupConfig::setDeleted(bool flag)
{
	if(deleted == flag)
	{
		return;
	}
	deleted = flag;
	for(int idx = 0 ; idx < list.count() ; idx++)
	{
		SmsGroup *pGroup = list.at(idx);
		pGroup->setDeleted(flag);
	}
}

void GroupConfig::calculateState(void)
{
	if(deleted)
	{
		return;
	}
	int nActive = 0, nActivated = 0, nInitialized = 0;
	int newNumMsg = 0, newNumErrors = 0;
	for(int idx = 0 ; idx < list.count() ; idx++)
	{
		SmsGroup *pGroup = list.at(idx);
		if(!pGroup->isDeleted())
		{
			if(pGroup->isActive())
			{
				nActive++;
			}
			if(pGroup->isActivated())
			{
				nActivated++;
			}
			if(pGroup->isInitialized())
			{
				nInitialized++;
			}
			newNumMsg += pGroup->getNumMsg();
			if(!pGroup->getError().isEmpty())
			{
				newNumErrors++;
			}
		}
	}

	bool change = false;
	if(numMsg != newNumMsg)
	{
		numMsg = newNumMsg;
		change = true;
	}
	if(newNumErrors != numErrors)
	{
		numErrors = newNumErrors;
		if(numErrors)
		{
			error = QString::number(numErrors);
			error += " errors";
		}
		else
		{
			error = "";
		}
		change = true;
	}
	QString newState;
	QColor newColor;
	if(!nActive)
	{
		newState = "Inactive";
		newColor.setRgb(COLOR_INACTIVE);
	}
	else if(nInitialized && (nInitialized == (int)list.count()))
	{
		newState = "Running";
		newColor.setRgb(COLOR_RUNNING);
	}
	else
	{
		char buf[64];
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d,%d,%d", nActive, nActivated, nInitialized);
#else
		sprintf(buf, "%d,%d,%d", nActive, nActivated, nInitialized);
#endif
		newState = buf;
		newColor.setRgb(COLOR_STARTING);
	}
	if((newState != state) || (newColor != color))
	{
		state = newState;
		color = newColor;
		change = true;
	}
	if(change)
	{
		emit changed(this);
	}
}

void GroupConfig::configStateChange(SmsConfig * /* pSrc */)
{
	calculateState();
}

void GroupConfig::configNumMsgChange(SmsConfig *pSrc, int delta)
{
	if(pSrc->isDeleted())
	{
		return;
	}
	if(delta)
	{
		numMsg += delta;
		emit changed(this);
	}
}
