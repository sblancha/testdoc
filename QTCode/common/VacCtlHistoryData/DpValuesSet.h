#ifndef	DPVALUESSET_H
#define	DPVALUESSET_H

#include "VacCtlHistoryDataExport.h"

//	Class holding all required DPE values for one DP

#include "DpeValuesSet.h"

class VACCTLHISTORYDATA_EXPORT DpValuesSet
{
public:
	DpValuesSet(const char *dpName);
	~DpValuesSet();

	void append(const char *dpeName, const QDateTime &ts, const QVariant &value);
	void sort(void);
	DpeValuesSet *findDpeData(const char *dpeName);

	// Access
	inline const QString &getName(void) const { return name; }
	inline int getDpeCount(void) const { return pDpes->count(); }
	inline const QList<DpeValuesSet *> &getDpes(void) const { return *pDpes; }

protected:
	// DP name
	QString					name;

	// List of DPEs with their values
	QList<DpeValuesSet *>	*pDpes;

};

#endif	// DPVALUESSET_H
