//	Implementation of VacIconVVF_S class
/////////////////////////////////////////////////////////////////////////////////


#include "VacIconVVF_S.h"

#include "EqpVV.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OPEN				0,255,0
#define COLOR_CLOSED			255,0,0
#define	COLOR_ERROR				255,102,255

// Colors for control and interlocks
#define COLOR_GREEN				0,255,0
#define COLOR_YELLOW			255,255,0
#define COLOR_RED				255,0,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define	RR1_OPEN		(0x00020000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_CLOSED		(0x00010000)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVVF_S::VacIconVVF_S(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 21);
}

VacIconVVF_S::~VacIconVVF_S()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVVF_S::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(21, 21, 10, 10, 0);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVF_S::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVV"), "VacIconVVF_S::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVV *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVF_S::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVVF_S::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVVF_S::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVVF_S::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVVF_S::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	/*
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
	*/
	painter.save();

	// Prepare painter's coordinate system for drawing
	// Coordinate system (0,0) is center of widget
	painter.translate(width() / 2, height() / 2);
	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	// Coordinate system is ready - draw
	drawImage(painter, ctlStatus, rr1, plcAlarm);

	// Draw selection ON TOP of image
	painter.restore();	// Original coordinate system - see save() above
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
}

void VacIconVVF_S::drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	// Draw vacuum pipe - two short lines
	QPen pen(pipeColor, pipeWidth);
	painter.setPen(pen);
	painter.drawLine(-14, 0, -9, 0);
	painter.drawLine(9, 0, 15, 0);	// FlatCapStyly does not cover the end point of line,
									// hence end point is 14+1=15
	
	// Draw valve image
	setPainterForControl(painter, ctlStatus, rr1, plcAlarm);

	// 1) Rectangle connecting big rectangle on top with valve image
	QPolygon points(4);
	points.setPoint(0, -2, -6);
	points.setPoint(1, 2, -6);
	points.setPoint(2, 2, 0);
	points.setPoint(3, -2, 0);
	painter.drawPolygon(points);

	// 2) big rectangle on top
	points.setPoint(0, -4, -9);
	points.setPoint(1, 4, -9);
	points.setPoint(2, 4, -6);
	points.setPoint(3, -4, -6);
	painter.drawPolygon(points);

	// 3) Valve image
	setPainterForMainImage(painter, ctlStatus, rr1, plcAlarm);

	points.setPoint(0, -9, -9);
	points.setPoint(1, 9, 9);
	points.setPoint(2, 9, -9);
	points.setPoint(3, -9, 9);
	painter.drawPolygon(points);

}

void VacIconVVF_S::setPainterForControl(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	QColor color(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr1 & RR1_ERROR)
				{
					color.setRgb(COLOR_RED);
				}
				else if(rr1 & RR1_WARNING)
				{
					color.setRgb(COLOR_YELLOW);
				}
				else
				{
					color.setRgb(COLOR_GREEN);
				}
			}
		}
		break;
	}
	painter.setBrush(color);
	painter.setPen(Qt::black);
}

void VacIconVVF_S::setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	QColor color(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr1 & RR1_OPEN)
				{
					if(rr1 & RR1_CLOSED)
					{
						color.setRgb(COLOR_ERROR);
					}
					else
					{
						color.setRgb(COLOR_OPEN);
					}
				}
				else if(rr1 & RR1_CLOSED)
				{
					color.setRgb(COLOR_CLOSED);
				}
				else
				{
					color.setRgb(COLOR_UNDEFINED);
				}
			}
		}
		break;
	}
	QBrush brush(color);
	painter.setBrush(brush);
	painter.setPen(Qt::black);
}
