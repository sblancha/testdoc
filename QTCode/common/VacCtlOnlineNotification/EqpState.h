#ifndef	EQPSTATE_H
#define	EQPSTATE_H

#include "VacCtlOnlineNotificationExport.h"

// Connects to one (or more) equipments, issues signal when all of equipments
// match supplied criteria

#include "InterfaceEqp.h"
#include "EqpMsgCriteria.h"

#include "ComplexCriteria.h"
#include "Eqp.h"
#include <qlist.h>


class VACCTLONLINENOTIFICATION_EXPORT EqpState : public InterfaceEqp
{
	Q_OBJECT

public:
	EqpState(Eqp *pEqp, const ComplexCriteria &sourceCriteria);
	~EqpState();

	void addEqp(Eqp *pEqp);

	void activate(void);
	void deactivate(void);

	void resetPendingMatch(void);
	void checkPendingMatch(const QDateTime &now);

	// Access
	inline Eqp *getFirstEqp(void) { return pEqpList->first(); }
	inline ComplexCriteria &getCriteria(void) { return criteria; }
	inline ComplexCriteria *getCriteriaPtr(void) { return &criteria; }

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value,
		DataEnum::DataMode mode, const QDateTime &timeStamp);
	virtual void mobileStateChange(Eqp *pSrc, DataEnum::DataMode mode);

signals:
	void stateChanged(EqpState *pSrc, bool match);
	
protected:
	// Criteria to be checked
	ComplexCriteria	criteria;

	// List of devices to be checked, 1st device in list is 'main' device
	QList<Eqp *>	*pEqpList;

	// Flag indicating if all equipment matches criteria
	bool			match;

	// Flag indicating if this instance is activated (i.e. connected to equipment)
	bool			activated;
};

#endif	// EQPSTATE_H
