
#uses "vclMachine.ctl"    // Load CTRL library
#uses "vclProgress.ctl"   // Load CTRL library
#uses "vclDevCommon.ctl"  // Load CTRL library
#uses "vclActions.ctl"    // Load CTRL library
#uses "vclResources.ctl"  // Load ctrl library


// Definitions for different groups of DP types
// a DP_TYPE_GROUP are Dp types with the same widget contextual menu, single and group orders
const int VAC_DP_TYPE_GROUP_VV = 1;            // Valves
const int VAC_DP_TYPE_GROUP_VGM = 2;           // Membrane gauges
const int VAC_DP_TYPE_GROUP_VGR = 3;           // Pirani gauges
const int VAC_DP_TYPE_GROUP_VGP = 4;           // Pening gauges
const int VAC_DP_TYPE_GROUP_VGI = 5;           // Ionization gauges
const int VAC_DP_TYPE_GROUP_VGF = 6;           // Full range gauges
const int VAC_DP_TYPE_GROUP_VPI = 7;           // Ion pumps
const int VAC_DP_TYPE_GROUP_VRPI = 8;          // Ion pump power supplies
const int VAC_DP_TYPE_GROUP_VPGF = 9;          // Fixed pumping groups
const int VAC_DP_TYPE_GROUP_VPGM = 10;         // Mobile pumping groups
const int VAC_DP_TYPE_GROUP_VPGMPR = 11;       // Pressure (gauge) of mobile pumping groups
const int VAC_DP_TYPE_GROUP_VACOK = 12;        // VAC_OK
const int VAC_DP_TYPE_GROUP_VV_VELO = 13;      // Valves in  VELO detector
const int VAC_DP_TYPE_GROUP_VPT_VELO = 14;     // Turbo pumps in VELO detector
const int VAC_DP_TYPE_GROUP_VPR_VELO = 15;     // Rough pumps in VELO detector
const int VAC_DP_TYPE_GROUP_VGA_VELO = 16;     // Absolute pressure gauge in VELO detector
const int VAC_DP_TYPE_GROUP_VGD_VELO = 17;     // Diff. pressure gauge in VELO detector
const int VAC_DP_TYPE_GROUP_VINT_VELO = 18;    // Interlock monitoring in VELO detector
const int VAC_DP_TYPE_GROUP_VPRO_VELO = 19;    // Process status monitoring in VELO detector
const int VAC_DP_TYPE_GROUP_VOPS_VELO = 20;    // Over pressure switch in VELO detector
const int VAC_DP_TYPE_GROUP_EXP_AREA = 21;     // Experimental area vacuum system
const int VAC_DP_TYPE_GROUP_ACCESS_MODE = 22;  // Access mode control DP
const int VAC_DP_TYPE_GROUP_AUX_EQP = 23;      // AUXILLIARY equipment: most probably such equipment will
                                               // never be controlled by human, but DP types must be known (at least for
                                               // import, may be for something else...)
const int VAC_DP_TYPE_GROUP_CRYO_TT = 24;      // Main cryogenic thermometers
const int VAC_DP_TYPE_GROUP_CRYO_TT_EXT = 25;  // Extra cryogenic thermometers
const int VAC_DP_TYPE_GROUP_CRYO_TT_SUM = 26;  // Summary of CRYO thermometers
const int VAC_DP_TYPE_GROUP_VV_ANA = 27;       // Analog valve
const int VAC_DP_TYPE_GROUP_VGTR = 28;         // Pressure transmitter
const int VAC_DP_TYPE_GROUP_BEAM_INTENS = 29;  // Beam intensity measurements
const int VAC_DP_TYPE_GROUP_SECT_VPI_SUM = 30; // Summary of VPIs in vacuum sector
const int VAC_DP_TYPE_GROUP_ACCESS_ZONE = 31;  // Access zone control DP
const int VAC_DP_TYPE_GROUP_VPT100 = 32;       // PT100 thermometers
const int VAC_DP_TYPE_GROUP_VBIC = 33;         // BIC controller
const int VAC_DP_TYPE_GROUP_VRPM = 34;         // Solenoid power supply
const int VAC_DP_TYPE_GROUP_VIES = 35;         // Solenoid
const int VAC_DP_TYPE_GROUP_VP_STDIO = 36;     // Standard pump
const int VAC_DP_TYPE_GROUP_VV_AO = 37;        // New analog valve
const int VAC_DP_TYPE_GROUP_VG_STD = 38;       // Sandard gauge
const int VAC_DP_TYPE_GROUP_VPG_6A01 = 39;     // VPG of BGI, driven by sequencer
const int VAC_DP_TYPE_GROUP_BGI_6B01 = 40;     // BGI, driven by sequencer
const int VAC_DP_TYPE_GROUP_VV_STD_IO = 41;
const int VAC_DP_TYPE_GROUP_PLC = 42; // PLC
const int VAC_DP_TYPE_GROUP_INJ_6B02 = 43;     // Process Gas Injection with RVG 050 (Linac4 LEBT)
const int VAC_DP_TYPE_GROUP_EXT_ALARM = 44;    // External system - target for vacuum alarm
const int VAC_DP_TYPE_GROUP_VPG_6E01 = 45;     // VPG for Linac2, driven by sequencer

const int VAC_DP_TYPE_GROUP_V8DI_FE = 46;      // Digital Input monitor for interlock signals
const int VAC_DP_TYPE_GROUP_TC = 47;           // Themrocouples: VITE (temperature value) and VRJ_TC (remote IO)

const int VAC_DP_TYPE_GROUP_VPN = 48;          // Single NEG pump
const int VAC_DP_TYPE_GROUP_VP_NEG = 49;       // Controllable chain of NEG pumps
const int VAC_DP_TYPE_GROUP_VPNMUX = 50;       // Multiplexor for VP_NEGs

const int VAC_DP_TYPE_GROUP_VG_DP = 51;        // Vacuum Gauge with Profibus interface
const int VAC_DP_TYPE_GROUP_VPC_HCCC = 52;     // Vacuum Pump Cryo Controller HSR HCC

const int VAC_DP_TYPE_GROUP_VV_PUL = 53;		  // Vacuum Valve pulsed
const int VAC_DP_TYPE_GROUP_ALARM_DO = 54;	  // Alarm Digital Output generator

const int VAC_DP_TYPE_GROUP_VPS = 55;		     // Sublimation Pump Power Supply
const int VAC_DP_TYPE_GROUP_VPS_PROCESS = 56;  // Sublimation Process for  VPS

const int VAC_DP_TYPE_GROUP_ITL_CMNT = 57;	  // Software Interlock with CRYO systems for VVS
const int VAC_DP_TYPE_GROUP_ITL_CMUX = 58;	  // Multiplexer for SW CRYO ITL for VVS

const int VAC_DP_TYPE_GROUP_VR_GT = 59;		  // VR_GT device for TPG300 control
const int VAC_DP_TYPE_GROUP_VG_PT = 60;		  // VG_PT device controlled by TPG300

const int VAC_DP_TYPE_GROUP_VA_OK = 61; 	     // Vacuum Alarm OK

const int VAC_DP_TYPE_GROUP_VP_TP = 62;		  // Vacuum Pump Turbo Profibus interface 
const int VAC_DP_TYPE_GROUP_VPG_MBLK = 64;     // VPG for Linac2, driven by sequencer
const int VAC_DP_TYPE_GROUP_VA_RI  = 65;       // Vacuum Alarm 

const int VAC_DP_TYPE_GROUP_VSECTOR = 66; 	  // [VACCO-929] [VACCO-948] [VACCO-1645]
const int VAC_DP_TYPE_GROUP_VPG = 67; 		     // Vacuum Pump - Group
const int VAC_DP_TYPE_GROUP_VRE = 68;          // Vacuum contRol - BakE-out
const int VAC_DP_TYPE_GROUP_VR_PI = 69;        // Vacuum contRol - Agilent ion pump controller
const int VAC_DP_TYPE_GROUP_VG_A_RO = 70;      // Vacuum Generic Gauge Analog, Read Only
const int VAC_DP_TYPE_GROUP_VOPS_2PS = 71;     // Vacuum Double Overpressure Switch
const int VAC_DP_TYPE_GROUP_VG = 72;           // Generic group for Vacuum Gauge
const int VAC_DP_TYPE_GROUP_V_TCP = 73;        // Comunication device V_TCP
const int VAC_DP_TYPE_GROUP_VG_UC = 74;        // Vacuum Gauge Unified Compact registers
const int VAC_DP_TYPE_GROUP_VG_UF = 75;        // Vacuum Gauge Unified Full registers
const int VAC_DP_TYPE_GROUP_VELO_PROCESS = 76; // VELO Process Group

// List of all supported privileges - privilege names, configured
// in JCOP access control panels, MUST BE THE SAME!!!!
// ATTENTION: privileges in this list must be in ASCENDING order, i.e.
// privilege with index N must be higher than privilege with index (N-1)
global dyn_string glAllPrivileges;

// Static parameters for vacuum equipment.
// Configuration consists of list of all supported DP types:
global dyn_string glLhcVacDpTypes;

// Group enumeration for this DP type
global dyn_int glDpTypeGroups;

// Popup menus for equipment
// items = menu items in format required by popupMenu() - except for first
// two items of menu which are always device name and separator.
// Acts - action to be executed for every item in menu.
// Number of items and acts must match.
global dyn_dyn_string  glDevMenuItems;
global dyn_dyn_uint    glDevMenuActs;

// List of actions allowed for particular DP type
global dyn_dyn_uint	glDevAllowedActs;

// Name for every allowed action. Two names are supplied for every action:
// 1 - Name for single device action
// 2 - Name for group action
global dyn_dyn_string	glDevAllowedActNames;

// Extra parameters for every allowed action, 4 items are required
// per every allowed action:
// 1 - Privilege level required to execute action for single device
// 2 - Privilege level required to execute action for group of devices
// 3 - Is logging message required for single device action
// 4 - Is logging message required for group action
// where privilege levels are:
//    0 = action is not allowed/not supported
//    1... - index of privilege in glAllPrivileges list
global dyn_dyn_int  glDevAllowedActConfig;

//LhcVacInitDevFunc:
/** Initialize static equipment configuration for all supported DP types.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param none
  @return none
	
*/
void LhcVacInitDevFunc()
{
  int   typeIndex, listLen, n;
  dyn_string exceptionInfo;

  if(dynlen(glLhcVacDpTypes) > 0)
  {
    return;  // To avoid multiple calls
  }

  // Privilege names, used by vacuum control
  glAllPrivileges = makeDynString(
    "monitor",
    "operator",
    "expert",
    "admin");

  //==========================================================================================
  //========= DP Type specific information ===================================================
  //==========================================================================================

  //======= VGM_C0 - Compact Piezo gauge without control =====================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGM_C0");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGM);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);


  //======= VGM_C - Compact Piezo gauge with control =====================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGM_C");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGM);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,History,6,1",
    "PUSH_BUTTON,State History,7,1",
    "PUSH_BUTTON,Bit History,8,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON all Full Range Gauges",
    "Switch Gauge OFF", "Switch OFF all Full Range Gauges",
    "Set to Auto Gauge", "Set to Auto all Full Range Gauges",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VGF_C - Compact Full range gauge ============================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGF_C");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,History,6,1",
    "PUSH_BUTTON,State History,7,1",
    "PUSH_BUTTON,Bit History,8,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON all Full Range Gauges",
    "Switch Gauge OFF", "Switch OFF all Full Range Gauges",
    "Set to Auto Gauge", "Set to Auto all Full Range Gauges",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VGF_C0 - Compact Full range gauge READ ONLY ===================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGF_C0");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  
  //======= VG_UC - Vacuum Gauge Unified Compact register ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VG_UC");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VG_UC);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1",
    "SEPARATOR",
    "PUSH_BUTTON,Help,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
    0,
	 LVA_OPEN_EQP_HELP);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_RESET,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED,
    LVA_DEVICE_SET_ALERT_THRESH);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON multiple normally On Unified Gauges",
    "Switch Gauge OFF", "Switch OFF multiple normally On Unified Gauges",    
    "Reset Gauge", "Reset multiple normally On Unified Gauges",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "",
    "Set Alert Threshold", "Set Alert Threshold for multiple normally On Unified Gauges");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
   //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin
    2, 3, 0, 1,
    3, 3, 1, 1,
    2, 3, 0, 1,
    3, 0, 1, 0,
    3, 0, 1, 0,
    2, 3, 0, 1);  

    //======= VG_UF - Vacuum Gauge Unified Full register ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VG_UF");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VG_UF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Force On(expert),3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Lock remote orders,5,1",
    "PUSH_BUTTON,Unlock remote orders,6,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,8,1",
    "PUSH_BUTTON,History,9,1",
    "PUSH_BUTTON,State History,10,1",
    "PUSH_BUTTON,Bit History,11,1",
    "SEPARATOR",
    "PUSH_BUTTON,Help,13,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_FORCED_ON,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_SET_BLOCKED_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_CLEAR_BLOCKED_OFF,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
    0,
	 LVA_OPEN_EQP_HELP);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_RESET,
    LVA_DEVICE_FORCED_ON,
    LVA_DEVICE_SET_BLOCKED_OFF,
    LVA_DEVICE_CLEAR_BLOCKED_OFF,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED,
    
    LVA_DEVICE_SET_ALERT_THRESH,
    LVA_DEVICE_SET_FILTER_TIME,
    LVA_DEVICE_SET_PROT_CHAN_SOURCE,
    LVA_DEVICE_SET_PROT_UP_THRESH,
    LVA_DEVICE_SET_PROT_LOW_THRESH);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON multiple Unified Gauges",
    "Switch Gauge OFF", "Switch OFF multiple Unified Gauges",
    "Reset Gauge", "Reset multiple Unified Gauges",
    "Switch Gauge ON Forced", "",
    "Set gauge Blocked OFF", "",
    "Clear gauge Blocked OFF", "",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "",
    
    "Set Alert Threshold", "Set Alert Threshold for multiple Unified Gauges",
    "Set Filter Time", "Set Filter Time for multiple Unified Gauges",
    "Set Protection Channel", "Set Protection Channel for multiple Unified Gauges",
    "Set Protection up threshold", "Set Protection up threshold for multiple Unified Gauges",
    "Set Protection low threshold", "Se Protection low threshold for multiple Unified Gauges");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
   //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin    
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 0,
    3, 0, 1, 0,
    3, 0, 1, 0,
    
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1);  
  
  //======= VGP_T - TPG300-controlled Penning gauge ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGP_T");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "PUSH_BUTTON,Forced ON,4,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,6,1",
    "PUSH_BUTTON,TPG300,7,1",
    "PUSH_BUTTON,History,8,1",
    "PUSH_BUTTON,State History,9,1",
    "PUSH_BUTTON,Bit History,10,1",
	"SEPARATOR",
	"PUSH_BUTTON,Help,12,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON_AUTO,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    LVA_DEVICE_MASK | LVA_DEVICE_FORCED_ON,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_CONFIG_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
	0,
	LVA_OPEN_EQP_HELP);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON_AUTO,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_FORCED_ON,
    LVA_DEVICE_VRGT_CONFIG,
    LVA_DEVICE_SET_BLOCKED_OFF,
    LVA_DEVICE_CLEAR_BLOCKED_OFF,
    LVA_TPG300_HOT_START_DISABLE,
    LVA_TPG300_HOT_START_ENABLE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON all Penning Gauges",
    "Switch Gauge OFF", "Switch OFF all Penning Gauges",
    "Set to Auto Gauge", "Set to Auto all Penning Gauges",
    "Switch Gauge ON Forced", "Switch all Penning Gauges ON Forced",
    "Change TPG300 configuration", "Impossible",
    "Set gauge Blocked OFF", "",
    "Clear gauge Blocked OFF", "",
    "Disable Hot Start for TPG300", "",
    "Enable Hot Start for TPG300", "",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 3, 0, 1,
    4, 4, 0, 1,
    4, 0, 0, 0,
    3, 0, 1, 0,
    3, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VGR_T - TPG300-controlled Pirani gauge ====================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGR_T");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGR);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,TPG 300,6,1",
    "PUSH_BUTTON,History,7,1",
    "PUSH_BUTTON,State History,8,1",
    "PUSH_BUTTON,Bit History,9,1",
	"SEPARATOR",
	"PUSH_BUTTON,Help,11,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_CONFIG_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
	0,
	LVA_OPEN_EQP_HELP);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_VRGT_CONFIG,
    LVA_TPG300_HOT_START_DISABLE,
    LVA_TPG300_HOT_START_ENABLE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON all Pirani Gauges",
    "Switch Gauge OFF", "Switch OFF all Pirani Gauges",
    "Set to Auto Gauge", "Set to Auto all Pirani Gauges",
    "Change TPG300 configuration", "Impossible",
    "Disable Hot Start for TPG300", "",
    "Enable Hot Start for TPG300", "",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 3, 0, 1,
    4, 0, 0, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //========  - TPG300-controlled Pirani gauge (read-only - North Area) ===========
  typeIndex = dynAppend(glLhcVacDpTypes, "VGR_T0");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGR);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,TPG 300,2,1",
    "PUSH_BUTTON,History,3,1",
    "PUSH_BUTTON,State History,4,1",
    "PUSH_BUTTON,Bit History,5,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_CONFIG_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_VRGT_CONFIG,
    LVA_TPG300_HOT_START_DISABLE,
    LVA_TPG300_HOT_START_ENABLE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Change TPG300 configuration", "Impossible",
    "Disable Hot Start for TPG300", "",
    "Enable Hot Start for TPG300", "",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 0, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======= VGP_C - Compact Penning gauge ===================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGP_C");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,History,6,1",
    "PUSH_BUTTON,State History,7,1",
    "PUSH_BUTTON,Bit History,8,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_SET_BLOCKED_OFF,
    LVA_DEVICE_CLEAR_BLOCKED_OFF,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON all Penning Gauges",
    "Switch Gauge OFF", "Switch OFF all Penning Gauges",
    "Set to Auto Gauge", "Set to Auto all Penning Gauges",
    "Set gauge Blocked OFF", "",
    "Clear gauge Blocked OFF", "",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 3, 0, 1,
    3, 0, 1, 0,
    3, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======= VGP_C0 - Compact Penning gauge Read-only ====================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGP_C0");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VGR_C - Compact Prrani gauge with control ==================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGR_C");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGR);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON all Penning Gauges",
    "Switch Gauge OFF", "Switch OFF all Penning Gauges",
    "Set to Auto Gauge", "Set to Auto all Penning Gauges",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VGFH_C - Full range gauge with control ==================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGFH_C");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON all Full range Gauges",
    "Switch Gauge OFF", "Switch OFF all Full range  Gauges",
    "Set to Auto Gauge", "Set to Auto all Full range Gauges",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VGR_C0 - Compact Prrani gauge without control ==============================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGR_C0");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGR);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VGI - Ion gauge =========================================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGI");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGI);
  glDevMenuItems[typeIndex] = makeDynString(
    "CASCADE_BUTTON,Degasing,1",
    "CASCADE_BUTTON,Modulation,1",
    "CASCADE_BUTTON,Filament,1",
    "CASCADE_BUTTON,Measuring,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,6,1",
    "PUSH_BUTTON,History,7,1",
    "PUSH_BUTTON,State History,8,1",
    "PUSH_BUTTON,Bit History,9,1",
    "Degasing",
    "PUSH_BUTTON,On,11,1",
    "PUSH_BUTTON,Off,12,1",
    "Modulation",
    "PUSH_BUTTON,On,14,1",
    "PUSH_BUTTON,Off,15,1",
    "Filament",
    "PUSH_BUTTON,1st,17,1",
    "PUSH_BUTTON,2nd,18,1",
    "Measuring",
    "PUSH_BUTTON,On,20,1",
    "PUSH_BUTTON,Off,21,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    0,
    0,
    0,
    0,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_DEGAS_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_DEGAS_OFF,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_MODUL_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_MODUL_OFF,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_FILAMENT_1,
    LVA_DEVICE_MASK | LVA_DEVICE_FILAMENT_2,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_MEASURE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_MEASURE_OFF);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_DEGAS_ON,
    LVA_DEVICE_DEGAS_OFF,
    LVA_DEVICE_MODUL_ON,
    LVA_DEVICE_MODUL_OFF,
    LVA_DEVICE_FILAMENT_1,
    LVA_DEVICE_FILAMENT_2,
    LVA_DEVICE_MEASURE_ON,
    LVA_DEVICE_MEASURE_OFF,
    // Extra actions - setting special values
    LVA_DEVICE_RESET,
    LVA_VGI_SET_EMISSION_CURRENT,
    LVA_VGI_SET_RESIDUAL_PRESSURE,
    LVA_VGI_SET_MODULATION_FACTOR,
    LVA_VGI_SET_SENSIBILITY,
    LVA_VGI_SET_GAIN,
    LVA_VGI_SET_DEGAS_TIME,
    LVA_VGI_SET_MODULATION_TIME,
    LVA_VGI_SET_DEGAS_CURRENT,
    LVA_VGI_SET_GRID_VOLTAGE,
    LVA_VGI_SET_ANALOG_OUT_OFFSET,
    LVA_VGI_SET_OVER_PANGE_PRESSURE,
    // Expert actions
    LVA_VGI_ENABLE_LOCAL,
    LVA_VGI_DISABLE_LOCAL,
    LVA_VGI_AUTO_GAIN,
    LVA_VGI_DEFAULT_PARAM,
    LVA_VGI_MBAR_UNIT,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED
    );
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Degasing ON", "Set Degasing ON all Ion Gauges",
    "Set Degasing OFF", "Set Degasing OFF all Ion Gauges",
    "Set Modulation ON", "Set Modulation ON for all Ion Gauges",
    "Set Modulation OFF", "Set Modulation OFF for all Ion Gauges",
    "Set Filament #1", "Set Filament #1 for all Ion Gauges",
    "Set Filament #2", "Set Filament #2 for all Ion Gauges",
    "Set Measuring ON", "Set Measuring ON for all Ion Gauges",
    "Set Measuring OFF", "SetMeasuring OFF for all Ion Gauges",
    // Extra actions - setting special values
    "Reset", "",
    "Set Emission Current", "",
    "Set Residual Pressure", "",
    "Set Modulation Factor", "",
    "Set Sensibility", "",
    "Set Gain", "",
    "Set Degassing Time", "",
    "Set Modulation Time", "",
    "Set Filament Degassing Current", "",
    "Set Grid Voltage", "",
    "Set Analog Output Offset", "",
    "Set OverRange Pressure", "",
    // Expert actions
    "Enable Local", "",
    "Disable Local", "",
    "Auto Gain", "",
    "Default Param", "",
    "mbar Unit", "",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", ""
    );
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 2, 0, 1,
    // Extra actions - setting special values
    3, 0, 0, 0,  // LVA_DEVICE_RESET
    3, 0, 0, 0,  // LVA_VGI_SET_EMISSION_CURRENT
    3, 0, 0, 0,  // LVA_VGI_SET_RESIDUAL_PRESSURE,
    3, 0, 0, 0,  // LVA_VGI_SET_MODULATION_FACTOR,
    3, 0, 0, 0,  // LVA_VGI_SET_SENSIBILITY,
    3, 0, 0, 0,  // LVA_VGI_SET_GAIN,
    3, 0, 0, 0,  // LVA_VGI_SET_DEGAS_TIME,
    3, 0, 0, 0,  // LVA_VGI_SET_MODULATION_TIME,
    3, 0, 0, 0,  // LVA_VGI_SET_DEGAS_CURRENT,
    3, 0, 0, 0,  // LVA_VGI_SET_GRID_VOLTAGE,
    3, 0, 0, 0,  // LVA_VGI_SET_ANALOG_OUT_OFFSET,
    3, 0, 0, 0,  // LVA_VGI_SET_OVER_PANGE_PRESSURE,
    // Expert actions
    3, 0, 0, 0,  // LVA_VGI_ENABLE_LOCAL,
    3, 0, 0, 0,  // LVA_VGI_DISABLE_LOCAL,
    3, 0, 0, 0,  // LVA_VGI_AUTO_GAIN,
    3, 0, 0, 0,  // LVA_VGI_DEFAULT_PARAM,
    3, 0, 0, 0,  // LVA_VGI_MBAR_UNIT
    4, 0, 1, 0,
    4, 0, 1, 0
    );

  //======= VGTR_CO - Read-only pressure transmitter =======================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGTR_C0");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGTR);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_NV_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Transmitter Connected", "",
    "Set Transmitter Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VPI - Ion Pump ================================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPI");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPI);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,History,6,1",
    "PUSH_BUTTON,State History,7,1",
    "PUSH_BUTTON,Bit History,8,1",
	"SEPARATOR",
	"PUSH_BUTTON,Help,10,1");
	glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
	0,
	LVA_OPEN_EQP_HELP);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Start Ion Pump", "Start all Ion Pumps",
    "Stop Ion Pump", "Stop all Ion Pumps",
    "Set to Auto Ion Pump", "Set to Auto all Ion Pumps",
    "Set Pump Connected", "",
    "Set Pump Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 3, 0, 1,
    2, 2, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VP_IP - Ion Pump Generic ================================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VP_IP");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VRPI);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,History,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,Bit History,7,1",
	"SEPARATOR",
	"PUSH_BUTTON,Controller,9,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
	 0,
	 LVA_OPEN_VP_IP_CONTROLLER);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Start Ion Pump", "Start all Ion Pumps",
    "Stop Ion Pump", "Stop all Ion Pumps",
    "Set Pump Connected", "",
    "Set Pump Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 3, 0, 1,
    2, 2, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);	

  //======== VRPI_W - Power supply for ion pump type WimKo ===================
  typeIndex = dynAppend(glLhcVacDpTypes, "VRPI_W");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VRPI);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,Bit History,7,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_SET_BLOCKED_OFF,
    LVA_DEVICE_CLEAR_BLOCKED_OFF,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Start Ion Pump controlled by", "Start all Ion Pumps",
    "Stop Ion Pump controlled by", "Stop all Ion Pumps",
    "Set to Auto Ion Pump controlled by", "Set to Auto all Ion Pumps",
    "Set power supply Blocked OFF", "",
    "Clear power supply Blocked OFF", "",
    "Set Ion Pump Connected", "",
    "Set Ion Pump Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 3, 0, 1,
    2, 2, 0, 1,
    2, 3, 0, 1,
    3, 0, 1, 0,
    3, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VRPI_L - Power supply for ion pump type LEP modified =============
  typeIndex = dynAppend(glLhcVacDpTypes, "VRPI_L");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VRPI);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,Bit History,7,1",
	"SEPARATOR",
	"PUSH_BUTTON,Help,9,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
	0,
	LVA_OPEN_EQP_HELP);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_SET_BLOCKED_OFF,
    LVA_DEVICE_CLEAR_BLOCKED_OFF,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Start Ion Pump controlled by", "Start all Ion Pumps",
    "Stop Ion Pump controlled by", "Stop all Ion Pumps",
    "Set to Auto Ion Pump controlled by", "Set to Auto all Ion Pumps",
    "Set power supply Blocked OFF", "",
    "Clear power supply Blocked OFF", "",
    "Set Ion Pump Connected", "",
    "Set Ion Pump Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 3, 0, 1,
    2, 2, 0, 1,
    2, 3, 0, 1,
    3, 0, 1, 0,
    3, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);
	
	 //======== VR_PI_CHAN - Channel for ion pump controller with profibus interface =============
  typeIndex = dynAppend(glLhcVacDpTypes, "VR_PI_CHAN");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VRPI);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Step Order,3,1",
    "PUSH_BUTTON,3KV Fixed Order,4,1",
    "PUSH_BUTTON,5KV Fixed Order,5,1",
	"SEPARATOR",
    "PUSH_BUTTON,Details,7,1",
    "PUSH_BUTTON,State History,8,1",
    "PUSH_BUTTON,Bit History,9,1",
	"SEPARATOR",
	"PUSH_BUTTON,Help,10,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_STEP,
    LVA_DEVICE_MASK | LVA_DEVICE_3KV_FIXED,
    LVA_DEVICE_MASK | LVA_DEVICE_5KV_FIXED,
	0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
	0,
	LVA_OPEN_EQP_HELP);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
	 LVA_DEVICE_STEP,
    LVA_DEVICE_3KV_FIXED,
    LVA_DEVICE_5KV_FIXED,
    LVA_DEVICE_SET_BLOCKED_OFF,
    LVA_DEVICE_CLEAR_BLOCKED_OFF,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Start Ion Pump controlled by", "Start all Ion Pumps",
    "Stop Ion Pump controlled by", "Stop all Ion Pumps",
    "Set to Step Ion Pump controlled by", "Set to Step all Ion Pumps",
    "Set to 3kV Fixed controlled by", "Set to 3kV Fixed all Ion Pumps",
    "Set to 5kV Fixed controlled by", "Set to 5kV Fixed all Ion Pumps",
    "Set power supply Blocked OFF", "",
    "Clear power supply Blocked OFF", "",
    "Set Ion Pump Connected", "",
    "Set Ion Pump Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 3, 0, 1,
    2, 2, 0, 1,
    3, 3, 0, 1,
    3, 3, 0, 1,
    3, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0,
	 4, 0, 1, 0,
    4, 0, 1, 0);

//======== VR_PI - Power supply for ion pump with Profibus interface =============
  typeIndex = dynAppend(glLhcVacDpTypes, "VR_PI");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VR_PI);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_PROFIBUS,
    LVA_DEVICE_LOCAL,
    LVA_DEVICE_REMOTE_RESET,
    LVA_DEVICE_POWER_CYCLE,
    LVA_VR_PI_RD_HWERRORCODE,
    LVA_VR_PI_RD_FIRMWAREVERS,
    LVA_VR_PI_RD_HWCTRLCONFIG,
    LVA_VR_PI_RD_AUTOSTART,
    LVA_VR_PI_RD_FANMODE,
    LVA_VR_PI_RD_PRUNIT,
    LVA_VR_PI_RD_FILTER,
    LVA_VR_PI_WR_AUTOSTART_OFF,
    LVA_VR_PI_WR_AUTOSTART_LASTKNOWN,
    LVA_VR_PI_WR_FANMODE_ONLY,
    LVA_VR_PI_WR_FANMODE_ALWAYS,
    LVA_VR_PI_WR_PRUNIT_MBAR,
    LVA_VR_PI_WR_PRUNIT_CURRENT,
    LVA_VR_PI_WR_FILTER_FAST,
    LVA_VR_PI_WR_FILTER_MEDIUM,
    LVA_VR_PI_WR_FILTER_SLOW,
    LVA_DEVICE_RESET_WRS);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Profibus mode", "Set All Profibus mode",
    "Set Local mode", "Set All Local mode",
    "Remote Reset", "All Remote Reset",
    "Power Cycle", "All Power Cycle",  
    "Read Hardware Erreur", "Read All - Hardware Erreur",
    "Read Firmware Version", "Read All - Firmware Version",
    "Read Hardware Controller Configuration", "Read All - Hardware Controller Configuration",
    "Read Auto Start mode", "Read All - Auto Start mode",
    "Read Fan mode", "Read All - Fan mode",
    "Read Pressure unit", "Read All - Pressure unit",
    "Read Filter", "Read All - Filter",
    "Set Auto Start: Off", "Set All - ",
    "Set Auto Start: Last Known State", "Set All - ",
    "Set Fan Mode: Only amps>1mA", "Set All - Fan Mode: Only amps>1mA",
    "Set Fan Mode: Always On", "Set All - Fan Mode: Always On",
    "Set Pressure Unit: mbar", "Set All - Pressure Unit: mbar",
    "Set Pressure Unit: current", "Set All - Pressure Unit: current",
    "Set Filter: Fast", "Set All - Filter: Fast",
    "Set Filter: Medium", "Set All - Filter: Medium",
    "Set Filter: Slow", "Set All - Filter: Slow",
    "Reset Communication", "Reset All communication");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
   //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin
    3, 3, 0, 1,
    3, 3, 0, 1,
    3, 3, 0, 1,
    3, 3, 1, 1,    
    //Read    
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    //Set
    3, 3, 1, 1,
    3, 3, 1, 1,
    3, 3, 1, 1,
    3, 3, 1, 1,
    3, 3, 1, 1,
    3, 3, 1, 1,
    3, 3, 1, 1,
    3, 3, 1, 1,
    3, 3, 1, 1,
    3, 0, 0, 1);  
    
  //======== VRGT - Configurator for TPG300. TPG300 itself nas no DP type. ==============
  typeIndex = dynAppend(glLhcVacDpTypes, "VRGT");
  dynAppend(glDpTypeGroups, 0);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();


  //======== VVS_PS - Sector valves in PS SVCU_PS ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VVS_PS");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "OPEN all Valves",
    "CLOSE Valve", "CLOSE All Valves",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VVS_PSB_SR - Sector valves on single ring of PSB ===================
  // This DP type shall work exactly as VVS_PS with the following exceptions:
  // - valves do not appear on main view, synoptic etc.
  // - valves do not generate alarms for LASER
  // - there are no group actions for these valves
  typeIndex = dynAppend(glLhcVacDpTypes, "VVS_PSB_SR");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "",
    "CLOSE Valve", "",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 0, 0, 0,
    2, 0, 0, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VVS_PSB_SUM - 'Summary' valve for 2-4 valves on single rings of PSB (VVS_PSB_SR) ===
  // Such device works exactly as VVS_PS, just detail panel is different
  typeIndex = dynAppend(glLhcVacDpTypes, "VVS_PSB_SUM");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "OPEN all Valves",
    "CLOSE Valve", "CLOSE All Valves");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1);

  //======== VVS_S - Sector valves in SPS SVCU_SPS ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VVS_S");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "OPEN all Valves",
    "CLOSE Valve", "CLOSE All Valves",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VVS_SV - Sector valves in SPS SVCU_SPS & VAT =========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VVS_SV");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_RESET,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "OPEN all Valves",
    "CLOSE Valve", "CLOSE All Valves",
    "RESET Valve", "RESET All Valves",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VVF_S - Fast valves in SPS SVCU_SPS ====================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VVF_S");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "OPEN all Valves",
    "CLOSE Valve", "CLOSE All Valves",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VVS_LHC - Sector valves in LHC ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VVS_LHC");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1",
	  "SEPARATOR",
	"PUSH_BUTTON,Interlock Diagram,8,1",
	  "PUSH_BUTTON,Help,9,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
	  0,
	  LVA_OPEN_INTERLOCKS,
	  LVA_OPEN_EQP_HELP);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_ACTION_VV_TEMP_INTERLOCK,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED,
	  LVA_VVS_LHC_EDIT_THRESHOLD_ON);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "OPEN all Valves",
    "CLOSE Valve", "CLOSE All Valves",
    "Set Temperature Interlock Limit", "",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "",
	"Edit Valve Thresholds", "Edit all Valves Thresholds");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    3, 0, 0, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
	  3, 3, 1, 1);

  //======== VSector - Vacuum Sectors ================================
  // [VACCO-929] [VACCO-948] [VACCO-1645]
  typeIndex = dynAppend(glLhcVacDpTypes, "VSector");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VSECTOR);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Set Operational,1,1",
    "PUSH_BUTTON,Set On Work,2,1",
    "PUSH_BUTTON,Set Vented,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_VSECTOR_OPERATIONAL,
    LVA_DEVICE_MASK | LVA_VSECTOR_ONWORK,
    LVA_DEVICE_MASK | LVA_VSECTOR_VENTED);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_VSECTOR_OPERATIONAL,
    LVA_VSECTOR_ONWORK,
    LVA_VSECTOR_VENTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Sector Operational", "Set all Sectors Operational",
    "Set Sector On Work", "Set all Sectors On Work",
    "Set Sector Vented", "Set all Sectors Vented");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 3, 1, 1,
    3, 3, 1, 1,
    3, 3, 1, 1);
  
  //======== VV_STD_IO ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VV_STD_IO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV_STD_IO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_RESET,
    LVA_DEVICE_FORCE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "",
    "CLOSE Valve", "",
    "Set MANUAL mode", "",
    "Set AUTO mode", "",
    "Reset", "",
    "Set FORCE mode", "",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 0, 1, 1,
    3, 0, 1, 1,
    3, 0, 0, 1,
    3, 0, 0, 1, 
    3, 0, 0, 1,
    3, 0, 1, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

	//======== VV_PUL ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VV_PUL");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV_PUL);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_RESET,
    LVA_DEVICE_FORCE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "",
    "CLOSE Valve", "",
    "Set MANUAL mode", "",
    "Set AUTO mode", "",
    "Reset", "",
    "Set FORCE mode", "",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 0, 1, 1,
    3, 0, 1, 1,
    3, 0, 0, 1,
    3, 0, 0, 1, 
    3, 0, 0, 1,
    3, 0, 1, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);
	
    //======== VV_STD_6E01 ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VV_STD_6E01");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV_STD_IO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_RESET,
    LVA_DEVICE_SET_BLOCKED_OFF,
    LVA_DEVICE_CLEAR_BLOCKED_OFF,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "",
    "CLOSE Valve", "",
    "Set MANUAL mode", "",
    "Set AUTO mode", "",
    "Reset", "",
    "Set Blocked CLOSED State", "",
    "Clear Blocked CLOSED State", "",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 0, 1, 1,
    2, 0, 1, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    2, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);



    //======== VLV_ANA - analog valves in LHC ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VLV_ANA");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV_ANA);
  glDevMenuItems[typeIndex] = makeDynString(
// LIK 03.11.2010 request by FB    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,1,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,3,1",
    "PUSH_BUTTON,State History,4,1",
    "PUSH_BUTTON,Bit History,5,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
// LIK 03.11.2010 request by FB    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_PART_OPEN,
    LVA_DEVICE_PART_CLOSE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "",
    "CLOSE Valve", "", 
    "Partialy OPEN Valve", "",
    "Partialy CLOSE Valve", "",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    0, 0, 0, 0,
    2, 0, 0, 1, 
    2, 0, 0, 1,
    2, 0, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VV_AO - new analog valves in VPG & BGI ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VV_AO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV_AO);
  glDevMenuItems[typeIndex] = makeDynString(
// LIK 03.11.2010 request by FB    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,1,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,3,1",
    "PUSH_BUTTON,History,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
// LIK 03.11.2010 request by FB    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_NV_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_RESET,
    LVA_DEVICE_FORCE,
    LVA_DEVICE_PART_OPEN,
    LVA_DEVICE_PART_CLOSE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "CLOSE Valve", "", 
    "Auto mode", "", 
    "Manual mode", "", 
    "Reset","",
    "Force", "",
    "Partialy OPEN Valve", "",
    "Partialy CLOSE Valve", "",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 0, 0, 1,
    3, 0, 0, 1, 
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1, 
    3, 0, 0, 1,
    3, 0, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VLV - valves controlled by PLC ====================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VLV");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "PUSH_BUTTON,Auto,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,Bit History,7,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "OPEN all Valves",
    "CLOSE Valve", "CLOSE All Valves",
    "Set Valve to Auto", "Set to Auto all valves",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VLV_C0 - read only valves controlled by PLC =========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VLV_C0");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1",
    "PUSH_BUTTON,Bit History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VCLDX - COLDEX ======================================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VCLDX");
  dynAppend(glDpTypeGroups, 0);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set COLDEX Connected", "",
    "Set COLDEX Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======= VPLC - dp used for PLC health control ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPLC");
  dynAppend(glDpTypeGroups, 0);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();
  
  //======= VPLC_PF - dp used for PLC of control type VPGF_L ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPLC_PF");
  dynAppend(glDpTypeGroups, 0);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //======= V_FrontEnd_aliveCounter - dp used for SIMCARD Communication health control ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "V_FrontEnd_aliveCounter");
  dynAppend(glDpTypeGroups, 0);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();  
  
  //======= V_TCP - dp used for SIMCARD Communication control ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "V_TCP");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_V_TCP);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt(
      LVA_V_TCP_ENABLE,
      LVA_V_TCP_DISABLE,
      LVA_V_TCP_DISCONNECT);
  glDevAllowedActNames[typeIndex] = makeDynString(
      "Enable device", "",
      "Disable device", "",
      "Disconnect device", "");
   //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin 
  glDevAllowedActConfig[typeIndex] = makeDynInt(
      4, 0, 1, 0,
      4, 0, 1, 0,
      4, 0, 1, 0);   
  
  //======= VPLC_A - DP used for PLC control =====================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPLC_A");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_PLC);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_PLC_PING,
    LVA_PLC_RESET,
    LVA_PLC_INIT,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
      "PING PLC", "",
      "RESET PLC", "",
      "INIT Mobile equipment DBs", "",
      "Set PLC Connected", "",
      "Set PLC Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======= VPLC_A0 - DP used for PLC control (Read only) ========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPLC_A0");
  dynAppend(glDpTypeGroups, 0);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set PLC Connected", "",
    "Set PLC Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VPG_EA - Pumping group - SPS North Area Expertiments ===============
  typeIndex = dynAppend(glLhcVacDpTypes, "VPG_EA");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Start,1,1" ,
    "PUSH_BUTTON,Stop,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Open Valve,4,1" ,
    "PUSH_BUTTON,Close Valve,5,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,7,1",
    "PUSH_BUTTON,State History,8,1",
    "PUSH_BUTTON,Bit History,9,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,	
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "START pumping group",  "START all pumping groups",
    "STOP pumping group", "STOP all pumping groups",
    "OPEN roughing valve of pumping group", "",
    "CLOSE roughing valve of pumping group", "",
    "Set pumping group Connected", "",
    "Set pumping group Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VPG_EA_NO_VLV_CTL - Pumping group - SPS North Area Expertiments - valve is not controlled ===============
  typeIndex = dynAppend(glLhcVacDpTypes, "VPG_EA_NO_VLV_CTL");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Start,1,1" ,
    "PUSH_BUTTON,Stop,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "START pumping group",  "START all pumping groups",
    "STOP pumping group", "STOP all pumping groups",
    "Set pumping group Connected", "",
    "Set pumping group Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VPG_EA_C0 - Pumping group - SPS North Area Expertiments - Read only ===============
  typeIndex = dynAppend(glLhcVacDpTypes, "VPG_EA_C0");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1",
    "PUSH_BUTTON,Bit History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set pumping group Connected", "",
    "Set pumping group Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VPG_BP - By-Pass fixed pumping group ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPG_BP");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "CASCADE_BUTTON, Restart mode, 1 ",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,Bit History,7,1",
    "Restart mode",  // from here level 2
    "PUSH_BUTTON, Last known state, 9, 1",
    "PUSH_BUTTON, VPG stopped PP on, 10, 1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_RESTART_MODE_VPG_LAST_KNOWN_STATE,
    LVA_DEVICE_MASK | LVA_DEVICE_RESTART_MODE_VPG_STOPPED_PP_ON);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_RESTART_MODE_VPG_LAST_KNOWN_STATE,
    LVA_DEVICE_RESTART_MODE_VPG_STOPPED_PP_ON,
    LVA_DEVICE_TMP_STANDBY_OFF,
    LVA_DEVICE_TMP_STANDBY_ON,
    LVA_DEVICE_VVR_FORCED_OFF,
    LVA_DEVICE_VVR_FORCED_ON,
    LVA_DEVICE_VVR1_OPEN,
    LVA_DEVICE_VVR1_CLOSE,
    LVA_DEVICE_VVR2_OPEN,
    LVA_DEVICE_VVR2_CLOSE,
    LVA_DEVICE_VPG_PP_FORCED_OFF,
    LVA_ACTION_VPG_SERVICE_DATE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "START pumping group", "START all pumping groups",
    "STOP pumping group", "STOP all pumping groups",
    "Set pumping group AUTO", "",
    "Restart mode: Last known state", "",
    "Restart mode: VPG stopped & PP On", "",
    "TMP stand-by: OFF", "",
    "TMP stand-by: ON", "",
    "VVRs forced open enable: OFF", "",
    "VVRs forced open enable: ON", "",
    "Open VVR1", "Open VVR1 in all pumping groups",
    "Close VVR1", "Close VVR1 in all pumping group",
    "Open VVR2", "Open VVR2 in all pumping groups",
    "Close VVR2", "Close VVR2 in all pumping group",
    "Forced OFF PP of VPG", "",
    "Set service date", "",
    "Set pumping group Connected", "",
    "Set pumping group Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 0, 0, 0,
    3, 0, 0, 0,
    3, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    3, 0, 1, 0,
    3, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VPG_SA - Stand-Alone fixed pumping group ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPG_SA");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "CASCADE_BUTTON, Restart mode, 1 ",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1",
    "Restart mode",  // from here level 2
    "PUSH_BUTTON, Last known state, 8, 1",
    "PUSH_BUTTON, VPG stopped and PP On, 9, 1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
     0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_RESTART_MODE_VPG_LAST_KNOWN_STATE,
    LVA_DEVICE_MASK | LVA_DEVICE_RESTART_MODE_VPG_STOPPED_PP_ON);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_RESTART_MODE_VPG_LAST_KNOWN_STATE,
    LVA_DEVICE_RESTART_MODE_VPG_STOPPED_PP_ON,
    LVA_DEVICE_TMP_STANDBY_OFF,
    LVA_DEVICE_TMP_STANDBY_ON,
    LVA_DEVICE_VVR_FORCED_OFF,
    LVA_DEVICE_VVR_FORCED_ON,
    LVA_DEVICE_VVR_OPEN,
    LVA_DEVICE_VVR_CLOSE,
    LVA_DEVICE_VPG_PP_FORCED_OFF,
    LVA_ACTION_VPG_SERVICE_DATE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "START pumping group",  "START all pumping groups",
    "STOP pumping group", "STOP all pumping groups",
    "Set pumping group AUTO", "",
    "Restart mode: Last known state", "",
    "Restart mode: VPG stopped & PP On", "",
    "TMP stand-by: OFF", "",
    "TMP stand-by: ON", "",
    "VVRs forced open enable: OFF", "",
    "VVRs forced open enable: ON", "",
    "Open VVR", "Open VVR in all pumping groups",
    "Close VVR", "Close VVR in all pumping groups",
    "Forced OFF PP of VPG", "",
    "Set service date", "",
    "Set pumping group Connected", "",
    "Set pumping group Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 0, 0, 0,
    3, 0, 0, 0,
    3, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 3, 0, 1,
    2, 3, 0, 1,
    3, 0, 1, 0,
    3, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== CRYO_VACOK - Vacuum interlocks for CRYO ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "CRYO_VACOK");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VACOK);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,State History,1,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set CRYO Interlocks Connected", "",
    "Set CRYO Interlocks Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== CRYO_VACOK_RO - REad-only vacuum interlocks for CRYO ===================
  typeIndex = dynAppend(glLhcVacDpTypes, "CRYO_VACOK_RO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VACOK);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,State History,1,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set CRYO VACOK Interlocks Connected", "",
    "Set CRYO VACOK Interlocks Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //==================================================================================
  //===================== Mobile equipment ===================================
  //==================================================================================
  //======== VMOB - Mobile equipment container ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VMOB");
  dynAppend(glDpTypeGroups, 0);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //======== VPGMPR - pressure of mobile pumping group ==================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPGMPR");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGMPR);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "SEPARATOR",
    "PUSH_BUTTON,History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_OPEN_PR_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "",
    "Switch Gauge OFF", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 0, 0, 0,
    2, 0, 0, 0);

  //======== VPGMA - status of mobile pumping group type A ==================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPGMA");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGM);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //======== VPGMB - status of mobile pumping group type B ==================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPGMB");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGM);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Start Group,1,1" ,
    "PUSH_BUTTON,Stop Group,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Open Valve,4,1" ,
    "PUSH_BUTTON,Close Valve,5,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,7,1",
    "PUSH_BUTTON,State History,8,1",
    "PUSH_BUTTON,Bit History,9,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_VVR_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_VVR_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,	
    LVA_DEVICE_VVR_OPEN,
    LVA_DEVICE_VVR_CLOSE);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "START pumping group",  "",
    "STOP pumping group", "",
    "OPEN VVR", "",
    "CLOSE VVR", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    1, 0, 0, 0,
    1, 0, 0, 0,
    1, 0, 0, 0,
    1, 0, 0, 0);

  //======== VPGMC - status of mobile pumping group type C ==================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPGMC");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGM);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Start Group,1,1" ,
    "PUSH_BUTTON,Stop Group,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Open Valve,4,1" ,
    "PUSH_BUTTON,Close Valve,5,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,7,1",
    "PUSH_BUTTON,State History,8,1",
    "PUSH_BUTTON,Bit History,9,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_VVR_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_VVR_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,	
    LVA_DEVICE_VVR_OPEN,
    LVA_DEVICE_VVR_CLOSE);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "START pumping group",  "",
    "STOP pumping group", "",
    "OPEN VVR", "",
    "CLOSE VVR", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    1, 0, 0, 0,
    1, 0, 0, 0,
    1, 0, 0, 0,
    1, 0, 0, 0);

  //======== VPGMD - status of mobile pumping group type D ==================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPGMD");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGM);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Start Group,1,1" ,
    "PUSH_BUTTON,Stop Group,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Open Valve,4,1" ,
    "PUSH_BUTTON,Close Valve,5,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,7,1",
    "PUSH_BUTTON,State History,8,1",
    "PUSH_BUTTON,Bit History,9,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_VVR_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_VVR_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,	
    LVA_DEVICE_VVR_OPEN,
    LVA_DEVICE_VVR_CLOSE);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "START pumping group",  "",
    "STOP pumping group", "",
    "OPEN VVR", "",
    "CLOSE VVR", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    1, 0, 0, 0,
    1, 0, 0, 0,
    1, 0, 0, 0,
    1, 0, 0, 0);
	
  //======== VP_GU - Vacuum Pump - Group Unified ==================
  typeIndex = dynAppend(glLhcVacDpTypes, "VP_GU");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPG);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Start Pump,1,1" ,
    "PUSH_BUTTON,Stop and Vent,2,1",
    "PUSH_BUTTON,Stop with Primary Pump ON,3,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Acknowledge Alert,5,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,7,1",
    "PUSH_BUTTON,State History,8,1",
    "PUSH_BUTTON,Bit History,9,1",
    "PUSH_BUTTON,Pressure History,10,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_START,
    LVA_DEVICE_MASK | LVA_DEVICE_VENT,
    LVA_DEVICE_MASK | LVA_DEVICE_STOP_PP_ON,
    0,    
    LVA_ALERT_ACKNOWLEDGE,
	  0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
    LVA_OPEN_PR_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_START,
    LVA_DEVICE_VENT,
    LVA_DEVICE_STOP_PP_ON,	
    LVA_DEVICE_VVR1_OPEN,
    LVA_DEVICE_VVR1_CLOSE,
	  LVA_DEVICE_RESET,
	
	  LVA_DEVICE_VVR2_OPEN,
    LVA_DEVICE_VVR2_CLOSE,
    
    LVA_DEVICE_VVR1_UNFORCE,
    LVA_DEVICE_VVR2_UNFORCE,
	
	  LVA_DEVICE_VG1_ON,
	  LVA_DEVICE_VG1_OFF,
	
  	LVA_DEVICE_VPP_RESET,
	  LVA_DEVICE_VPT_RESET,
	
	  LVA_DEVICE_BAKE_ON,
	  LVA_DEVICE_BAKE_OFF,
    
    LVA_DEVICE_TIGHT_TEST_ON,
    LVA_DEVICE_TIGHT_TEST_OFF,
    LVA_DEVICE_RESET_VPP_ALR,
    LVA_DEVICE_PURGE,
	  LVA_ALERT_ACKNOWLEDGE);
  
	glDevAllowedActNames[typeIndex] = makeDynString(
    "START pumping group",  "START all selected Unified Pumping group",
    "STOP and VENT pumping group", "STOP and VENT all selected Unified Pumping group",
    "STOP with Primary Pump ON", "STOP with Primary Pump ON all selected Unified Pumping group",
	  "OPEN VVR1", "OPEN VVR1 of all selected Unified Pumping group",
    "CLOSE VVR1", "CLOSE VVR1 of all selected Unified Pumping group",
	  "RESET Error&Warning", "Reset Error&Warning all selected Unified Pumping group",

	  "OPEN VVR2", "OPEN VVR2 of all selected Unified Pumping group",
    "CLOSE VVR2", "CLOSE VVR2 of all selected Unified Pumping group",
    
    "UNFORCE VVR1", "UNFORCE VVR1 of all selected Unified Pumping group",
    "UNFORCE VVR2", "UNFORCE VVR2 of all selected Unified Pumping group",
	
	  "SWITCH ON Gauge1", "SWITCH ON Gauges1 of all selected Unified Pumping group",
	  "SWITCH OFF Gauge1", "SWITCH OFF Gauges1 of all selected Unified Pumping group",
	
	  "RESET Thermal Protection", "RESET Thermal Protection of all selected Unified Pumping group",
	  "RESET Turbomolecular Controller Error", "RESET Turbomolecular Error of all selected Unified Pumping group",
	
	  "START Bake-out",  "START Bake-out of all selected Unified Pumping group",
	  "STOP Bake-out",  "STOP Bake-out of all selected Unified Pumping group",
    "Enable Static Vacuum Test Mode",  "Enable Static Vacuum Test Mode of all selected Unified Pumping group",     
    "Disable Static Vacuum Test Mode",  "Disable Static Vacuum Test Mode of all selected Unified Pumping group",
    "Reset VPP alarm",  "Reset VPP alarm of all selected Unified Pumping group",
    "PURGE mode", "PURGE mode of all selected Unified Pumping group",
		"Acknowledge Alert", "Acknowledge of all selected Unified Pumping group");
   //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 1, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 1, 1,
	
    2, 3, 0, 1,
    2, 3, 0, 1,
    
    2, 3, 0, 1,
    2, 3, 0, 1,
	
	  2, 3, 0, 1,
	  2, 2, 0, 1,
	
	  2, 3, 0, 1,
	  2, 3, 0, 1,
	
	  3, 3, 1, 1,
	  3, 3, 1, 1,
    
    3, 3, 1, 1,
	  3, 3, 1, 1,
    3, 3, 1, 1,
    3, 0, 1, 1,
	  2, 3, 0, 1);
	
  //======== VBAKEOUT - Bake-out rack ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VBAKEOUT");
  dynAppend(glDpTypeGroups, 0);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //======== _VacMobileList - list of active mobile equipment ========
  typeIndex = dynAppend(glLhcVacDpTypes, "_VacMobileList");
  dynAppend(glDpTypeGroups, 0);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //======= VGP_VELO Penning gauge for VELO===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGP_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======= VGP_VELO_BEAM Penning gauge for VELO (beam chamber)===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGP_VELO_BEAM");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VGP_VELO_DETECTOR Penning gauge for VELO (detector chamber)===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGP_VELO_DETECTOR");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VGR_VELO Pirani gauge for VELO===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGR_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGR);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VGR_VELO_BEAM Pirani gauge for VELO (beam chamber)===================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGR_VELO_BEAM");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGR);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VGR_VELO_DETECTOR Pirani gauge for VELO (detector chamber)=======================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGR_VELO_DETECTOR");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGR);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VVG_VELO  Gate valve in the VELO detector===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VVG_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV_VELO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VVP_VELO  Process valve in the VELO detector===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VVP_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV_VELO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VVS_VELO  Safety valve in the VELO detector===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VVS_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV_VELO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VRPI_VELO  Power supply for ion pump in the VELO detector===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VRPI_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VRPI);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Pump Connected", "",
    "Set Pump Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VPT_VELO  Turbo pump in the VELO detector===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPT_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPT_VELO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Pump Connected", "",
    "Set Pump Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VPR_VELO  Rough pump in the VELO detector===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPR_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPR_VELO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Pump Connected", "",
    "Set Pump Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VGA_VELO Absolute gauge for VELO ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGA_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGA_VELO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VGD_VELO Diferential gauge for VELO ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGD_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGD_VELO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VINT_VELO Interlock monitoring for VELO ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VINT_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VINT_VELO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,State History,1,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Interlock for VELO Connected", "",
    "Set Interlock for VELO Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VPRO_VELO Process status in VELO ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPRO_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPRO_VELO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,State History,1,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Process status in VELO Connected", "",
    "Set Process status in VELO Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======= VOPS_VELO over pressure switch in VELO ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VOPS_VELO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VOPS_VELO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Pressure switch in VELO Connected", "",
    "Set Pressure switch in VELO Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);
 	
  //======= EXP_AREA ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "EXP_AREA");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_EXP_AREA);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_EXP_AREA_DETAILS);
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();
 	
  //======= COLDEX ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "COLDEX");
  dynAppend(glDpTypeGroups, 0);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1",
    "PUSH_BUTTON,Bit History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set COLDEX Connected", "",
    "Set COLDEX Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======== VPG_GIS - Stand-Alone fixed pumping group in GIS 1, 2, 5, 8 ==========
  typeIndex = dynAppend(glLhcVacDpTypes, "VPG_GIS");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "CASCADE_BUTTON, Restart mode, 1 ",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,State History,7,1",
    "Restart mode",  // from here level 2
    "PUSH_BUTTON, Auto, 9, 1",
    "PUSH_BUTTON, Manual & VPG stopped, 10, 1",
    "PUSH_BUTTON, Manual & VPG started, 11, 1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_RESTART_MODE_AUTO,
    LVA_DEVICE_MASK | LVA_DEVICE_RESTART_MODE_MANUAL_VPG_STOP,
    LVA_DEVICE_MASK | LVA_DEVICE_RESTART_MODE_MANUAL_VPG_START);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_RESTART_MODE_AUTO,
    LVA_DEVICE_RESTART_MODE_MANUAL_VPG_STOP,
    LVA_DEVICE_RESTART_MODE_MANUAL_VPG_START,
    LVA_DEVICE_TMP_STANDBY_OFF,
    LVA_DEVICE_TMP_STANDBY_ON,
    LVA_DEVICE_VVR_OPEN,
    LVA_DEVICE_VVR_CLOSE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "START pumping group",  "",
    "STOP pumping group", "",
    "Set pumping group AUTO", "",
    "Restart mode: Auto", "",
    "Restart mode: Manual & VPG stopped", "",
    "Restart mode: Manual & VPG started", "",
    "TMP stand-by: OFF", "",
    "TMP stand-by: ON", "",
    "Open VVR", "",
    "Close VVR", "",
    "Set pumping group Connected", "",
    "Set pumping group Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    3, 0, 0, 0,
    3, 0, 0, 0,
    3, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VPG_BGI - Stand-Alone fixed pumping group in BGI IP4 ==========
  typeIndex = dynAppend(glLhcVacDpTypes, "VPG_BGI");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "CASCADE_BUTTON, Restart mode, 1 ",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,State History,7,1",
    "Restart mode",  // from here level 2
    "PUSH_BUTTON, Auto, 8, 1",
    "PUSH_BUTTON, Manual & VPG stopped, 9, 1",
    "PUSH_BUTTON, Manual & VPG started, 10, 1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
    0,
    LVA_DEVICE_MASK | LVA_DEVICE_RESTART_MODE_AUTO,
    LVA_DEVICE_MASK | LVA_DEVICE_RESTART_MODE_MANUAL_VPG_STOP,
    LVA_DEVICE_MASK | LVA_DEVICE_RESTART_MODE_MANUAL_VPG_START);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_RESTART_MODE_AUTO,
    LVA_DEVICE_RESTART_MODE_MANUAL_VPG_STOP,
    LVA_DEVICE_RESTART_MODE_MANUAL_VPG_START,
    LVA_DEVICE_TMP_STANDBY_OFF,
    LVA_DEVICE_TMP_STANDBY_ON,
    LVA_DEVICE_VVR_OPEN,
    LVA_DEVICE_VVR_CLOSE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "START pumping group",  "",
    "STOP pumping group", "",
    "Set pumping group AUTO", "",
    "Restart mode: Auto", "",
    "Restart mode: Manual & VPG stopped", "",
    "Restart mode: Manual & VPG started", "",
    "TMP stand-by: OFF", "",
    "TMP stand-by: ON", "",
    "Open VVR", "",
    "Close VVR", "",
    "Set pumping group Connected", "",
    "Set pumping group Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    3, 0, 0, 0,
    3, 0, 0, 0,
    3, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    2, 0, 0, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== _VacAccessMode - indicates access mode ====================================
  typeIndex = dynAppend(glLhcVacDpTypes, "_VacAccessMode");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_ACCESS_MODE);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Off,1,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_ACCESS_MODE_OFF);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_ACCESS_MODE_ON,
    LVA_ACCESS_MODE_OFF);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch ACCESS MODE ON", "" ,
    "Switch ACCESS MODE OFF", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 0, 1, 0,
    2, 0, 1, 0);

  //======== VSECT_VPI_SUM - summary of VPIs in one sector =============================
  typeIndex = dynAppend(glLhcVacDpTypes, "VSECT_VPI_SUM");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_SECT_VPI_SUM);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Synoptic...,1,1",
    "PUSH_BUTTON,Profile...,2,1",
    "SEPARATOR",
    "PUSH_BUTTON,Start all Ion Pumps...,4,1",
    "PUSH_BUTTON,Stop all Ion Pumps...,5,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_SYNOPTIC,
    LVA_OPEN_PROFILE,
    0,
    LVA_DEVICE_MASK | LVA_SECTOR_ACTION_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_SECTOR_ACTION_MASK | LVA_DEVICE_OFF);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch ON all Ion Pumps", "" ,
    "Switch OFF all Ion Pumps", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    1, 0, 1, 0,
    1, 0, 1, 0);

  //======== VELO_Alarm - calculated alarms for VELO vacuum system ====================
  typeIndex = dynAppend(glLhcVacDpTypes, "VELO_Alarm");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_AUX_EQP);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  
  //======== VG_IsolVacExport - calculated pressure in isolation vacuum sectors to be exported via CMW 
  typeIndex = dynAppend(glLhcVacDpTypes, "VG_IsolVacExport");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_AUX_EQP);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //======== VCRYO_TT - cryogenic thermometers ======================
  typeIndex = dynAppend(glLhcVacDpTypes, "VCRYO_TT");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_CRYO_TT);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,History,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //======== VCRYO_TT_Ext - extra cryogenic thermometers ======================
  typeIndex = dynAppend(glLhcVacDpTypes, "VCRYO_TT_Ext");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_CRYO_TT_EXT);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,History,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set thermometer Connected", "",
    "Set thermometer Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);

  //======== VBEAM_Intens - beam intensity measurement ======================
  typeIndex = dynAppend(glLhcVacDpTypes, "VBEAM_Intens");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_BEAM_INTENS);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,History,1,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_PR_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //======== VCRYO_TT_SUM - summary of CRYO thermometers in area ============
  typeIndex = dynAppend(glLhcVacDpTypes, "VCRYO_TT_SUM");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_CRYO_TT_SUM);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL);
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //======== VBEAM_PATH - summary of valves along beam path (for LASER alarm) ==
  typeIndex = dynAppend(glLhcVacDpTypes, "VBEAM_PATH");
  dynAppend(glDpTypeGroups, 0x0u);
  glDevMenuItems[typeIndex] = makeDynString("");
  glDevMenuActs[typeIndex] = makeDynUInt(0);
  glDevAllowedActs[typeIndex] = makeDynUInt(0);
  glDevAllowedActNames[typeIndex] = makeDynString("", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(0, 0, 0, 0);

  //======== VACCESS_ZONE - access zone in LHC 
  typeIndex = dynAppend(glLhcVacDpTypes, "VACCESS_ZONE");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_ACCESS_ZONE);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Start Access,1,1",
    "PUSH_BUTTON,Finish Access,2,1");

  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Disable Access to Zone", "",
    "Enable Access to Zone", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 0, 1, 0,
    2, 0, 1, 0);

  //======= VPT100_CO - Read-only thermometer on dump line =======================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPT100_C0");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPT100);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set thermometer Connected", "",
    "Set thermometer Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);
  //===================BeamParam types=============================================
  //===================Float=============================================
   
  typeIndex = dynAppend(glLhcVacDpTypes, "VBeamParamFloat");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_AUX_EQP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,History,1,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_PR_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //===================Int=============================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VBeamParamInt");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_AUX_EQP);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();
  
  //===================DynBool=============================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VBeamParamDynBool");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_AUX_EQP);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //===================DynFloat=============================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VBeamParamDynFloat");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_AUX_EQP);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();
  
  //===================String=============================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VBeamParamString");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_AUX_EQP);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();
  
  //======== VBIC - Beam interlock controller (is using for valve test mode). ==============
  typeIndex = dynAppend(glLhcVacDpTypes, "VBIC");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VBIC);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();
  //======== VRPM - Solenoid power supply . ==============
  typeIndex = dynAppend(glLhcVacDpTypes, "VRPM");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VRPM);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "PUSH_BUTTON,Shutdown,4,1",
    "PUSH_BUTTON,CircuitBreakerOn,5,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,7,1",
    "PUSH_BUTTON,History,8,1",
    "PUSH_BUTTON,State History,9,1",
    "PUSH_BUTTON,Bit History,10,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    LVA_DEVICE_MASK | LVA_DEVICE_VRPM_REMOTE_SHUTDOWN,
    LVA_DEVICE_MASK | LVA_DEVICE_VRPM_CB_ON,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_VRPM_SET_CURRENT,
    LVA_DEVICE_VRPM_SET_VOLTAGE,
    LVA_DEVICE_VRPM_REMOTE_SHUTDOWN,
    LVA_DEVICE_VRPM_CB_ON,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Solenoid Power Supply ON", "Switch ON all Solenoid Power Supplies",
    "Switch Solenoid Power Supply OFF", "Switch OFF all Solenoid Power Supplies",
    "Set to Auto Solenoid Power Supply", "Set to Auto all Solenoid Power Supply",
    "Set Current for Solenoid Power Supply","",
    "Set Voltage for Solenoid Power Supply","",
    "Remote Shutdown","Remote Shutdown all Solenoid Power Supply",
    "Switch Circuit Breaker ON","Switch ON all Circuit Breakers",
    "Set Solenoid Power Supply Connected", "",
    "Set Solenoid Power Supply Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 2, 0, 1,
    2, 2, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0 );
  
  
  //======== VIES - solenoid ================================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VIES");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VIES);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "PUSH_BUTTON,Auto,3,1",
    "PUSH_BUTTON,Shutdown,4,1",
    "PUSH_BUTTON,CircuitBreakerOn,5,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,7,1",
    "PUSH_BUTTON,History,8,1",
    "PUSH_BUTTON,State History,9,1",
    "PUSH_BUTTON,Bit History,10,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    LVA_DEVICE_MASK | LVA_DEVICE_VRPM_REMOTE_SHUTDOWN,
    LVA_DEVICE_MASK | LVA_DEVICE_VRPM_CB_ON,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_VRPM_REMOTE_SHUTDOWN,
    LVA_DEVICE_VRPM_CB_ON,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Solenoid Power Supply ON", "Switch ON all Solenoid Power Supplies",
    "Switch Solenoid Power Supply OFF", "Switch OFF all Solenoid Power Supplies",
    "Set to Auto Solenoid Power Supply", "Set to Auto all Solenoid Power Supply",
    "Remote Shutdown","Remote Shutdown all Solenoid Power Supply",
    "Switch Circuit Breaker ON","Switch ON all Circuit Breakers",
    "Set Solenoid Power Supply Connected", "",
    "Set Solenoid Power Supply Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 2, 0, 1,
    2, 2, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

 //======== VGI_PS_CMW - Penning gauge for PS controlled by CMW ============================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGI_PS_CMW");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,History,5,1",
    "PUSH_BUTTON,State History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON all Penning Gauges",
    "Switch Gauge OFF", "Switch OFF all Penning Gauges");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1);
 //======== VGP_PS_CMW - Penning gauge for SPS controlled by CMW ============================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGP_PS_CMW");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,History,5,1",
    "PUSH_BUTTON,State History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON all Penning Gauges",
    "Switch Gauge OFF", "Switch OFF all Penning Gauges");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1);
//======== VGR_PS_CMW - Pirani gauge for SPS controlled by CMW ============================
  typeIndex = dynAppend(glLhcVacDpTypes, "VGR_PS_CMW");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VGP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,History,5,1",
    "PUSH_BUTTON,State History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "Switch ON all Pirani Gauges",
    "Switch Gauge OFF", "Switch OFF all Pirani Gauges");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1);
//======== VVS_PS_CMW - Sector valve for SPS controlled by CMW ============================
  typeIndex = dynAppend(glLhcVacDpTypes, "VVS_PS_CMW");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "OPEN all Valves",
    "CLOSE Valve", "CLOSE All Valves");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1);
  
  //======== VPI - Ion Pump for SPS controlled by CMW================================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPI_PS_CMW");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPI);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1",
    "PUSH_BUTTON,Off,2,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,History,5,1",
    "PUSH_BUTTON,State History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Start Ion Pump", "Start all Ion Pumps",
    "Stop Ion Pump", "Stop all Ion Pumps");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 2, 0, 1,
    2, 2, 0, 1);
//======== VV_PULSED - Sector valve for SPS controlled by CMW ============================
  typeIndex = dynAppend(glLhcVacDpTypes, "VV_PULSED");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VV);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Open,1,1" ,
    "PUSH_BUTTON,Close,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_OPEN,
    LVA_DEVICE_MASK | LVA_DEVICE_CLOSE,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_OPEN,
    LVA_DEVICE_CLOSE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "OPEN Valve", "",
    "CLOSE Valve", "",
    "Set Valve Connected", "",
    "Set Valve Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 0, 0, 1,
    3, 0, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);
  
 
//======== VP_STDIO - Vacuum pump standard IO ============================
  typeIndex = dynAppend(glLhcVacDpTypes, "VP_STDIO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VP_STDIO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1" ,
    "PUSH_BUTTON,Off,2,1", 
    "PUSH_BUTTON,Auto,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,Bit History,7,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_RESET,
    LVA_DEVICE_FORCE,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_MANUAL,
    LVA_ACTION_VPG_SERVICE_DATE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);

  glDevAllowedActNames[typeIndex] = makeDynString(
    "START standard IO vacuum Pump", "Start all standard IO  Pumps",
    "STOP standard IO vacuum Pump", "Stop all standard IO  Pumps",
    "RESET","",
    "Set FORCE mode", "",  
    "Set AUTO mode", "",
    "Set MANUAL mode", "",
    "Set service date", "",
    "Set Vacuum pump Connected", "",
    "Set Vacuum pump Not Connected", "");

  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 0, 1, 1,
    3, 0, 1, 1,
    2, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

//======== VP_TP - Vacuum pump turbo profibus interface ============================
  typeIndex = dynAppend(glLhcVacDpTypes, "VP_TP");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VP_TP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1" ,
    "PUSH_BUTTON,Off,2,1", 
    "PUSH_BUTTON,Auto,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,Bit History,7,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_RESET,
    LVA_DEVICE_FORCE,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_MANUAL,
    LVA_ACTION_VPG_SERVICE_DATE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED,
    LVA_DEVICE_SPD_TO_ROTATION_SP,
    LVA_DEVICE_SPD_TO_NOMINAL);

  glDevAllowedActNames[typeIndex] = makeDynString(
    "START Turbo pump", "Start all Turbo pumps",
    "STOP  Turbo pump", "Stop all Turbo pumps",
    "RESET","",
    "Set FORCE mode", "",  
    "Set AUTO mode", "",
    "Set MANUAL mode", "",
    "Set service date", "",
    "Set Vacuum pump Connected", "",
    "Set Vacuum pump Not Connected", "",
    "Set Speed to a new Rotation Set Point", "",
    "Set Speed to nominal", "");

  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 0, 1, 1,
    3, 0, 1, 1,
    2, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
    3, 0, 1, 1,
    3, 0, 1, 1);
	

//======== VPG_6A01 - Pumping group of BGI, driven by sequencer =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPG_6A01");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPG_6A01);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Pump,1,1" ,
    "PUSH_BUTTON,Stop VPG (Vent VPG),2,1", 
    "PUSH_BUTTON,Stop VPG (Vent Turbo only),3,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,Bit History,7,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_VPG_PUMP,
    LVA_DEVICE_MASK | LVA_VPG_VENT,
    LVA_DEVICE_MASK | LVA_VPG_VENT_TURBO,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_RESET,
    LVA_DEVICE_FORCE,
    LVA_DEVICE_PROC_SET_MODE,
    LVA_VPG_PUMP,
    LVA_VPG_VENT,
    LVA_VPG_VENT_TURBO,
    LVA_DEVICE_VVR1_OPEN,
    LVA_DEVICE_VVR2_OPEN,
    LVA_DEVICE_VVR1_CLOSE,
    LVA_DEVICE_VVR2_CLOSE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED,
    LVA_DEVICE_PS_PROT_ON,
    LVA_DEVICE_PS_PROT_OFF,
    LVA_DEVICE_VVR1_MANUAL,
    LVA_DEVICE_VVR2_MANUAL
    );
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Re-Start VPG", "Re-Start all VPG",
    "Stop VPG with PP On (no venting)", "Stop all VPG with PP On (no venting)",
    "Auto","",
    "Manual","Set all VPG manual",
    "Reset Errors&Warnings", "Reset all VPG Errors&Warnings",
    "Force", "",
    "Set Mode", "",
    "Pump", "",
    "Stop VPG (Vent VPG)", "Stop all VPG (Vent VPG)",
    "Stop VPG (Vent Turbo only)", "Stop all VPG (Vent Turbo only)",
    "", "Open all VVR1",
    "", "Open all VVR2",
    "", "Close all VVR1",
    "", "Close all VVR2",
    "Set VPG Connected", "",
    "Set VPG Not Connected", "",
    "Switch ON Pressure Switch Protection", "",
    "Switch OFF Pressure Switch Protection", "",
    "", "Set all VVR1 manual",
    "", "Set all VVR2 manual");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
 //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin
    3, 3, 1, 1,
    3, 3, 1, 1,
    3, 0, 0, 1,
    3, 3, 1, 1,
    3, 3, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 3, 0, 1,
    3, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0,
    3, 0, 0, 0,
    3, 0, 0, 0,
    2, 3, 0, 1,
    2, 3, 0, 1);

  //======== VPG_6E01 - Pumping group for Linac2, driven by sequencer =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPG_6E01");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPG_6E01);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Pump,1,1" ,
    "PUSH_BUTTON,Stop VPG,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_VPG_PUMP,
    LVA_DEVICE_MASK | LVA_DEVICE_STOP_VPG,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_RESET,
    LVA_DEVICE_BYPASS,
    LVA_DEVICE_PROC_SET_MODE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Start VPG Process", "Start all VPG groups processes",
    "Stop VPG Process", "",
    "Manual","",
    "Reset Errors&Warnings", "Reset all VPG Errors&Warnings",
    "Bypass HW interlocks", "",
    "Set Mode", "",
    "Set VPG Connected", "",
    "Set VPG Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 0, 0, 1,
    3, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 3, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);
  
   //======== VPG_MBLK - Pumping Group for PLC 1200 ================= APAIVAER
  typeIndex = dynAppend(glLhcVacDpTypes, "VPG_MBLK");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPGF);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Pump,1,1" ,
    "PUSH_BUTTON,Stop VPG,2,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,State History,5,1",
    "PUSH_BUTTON,Bit History,6,1"
    );
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_VPG_SET_MODE_PUMP,
    LVA_DEVICE_MASK | LVA_VPG_SET_MODE_STOP,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_VVR1_OPEN,
    LVA_DEVICE_VVR1_CLOSE,
    LVA_VPG_MBLK_VVR1_FORCE,
    LVA_VPG_MBLK_VVR1_UNFORCE,    
    LVA_DEVICE_VVR2_OPEN,
    LVA_DEVICE_VVR2_CLOSE,
    LVA_VPG_MBLK_VVR1_FORCE,
    LVA_VPG_MBLK_VVR1_UNFORCE,
    LVA_VPG_MBLK_VG1_ON,
    LVA_VPG_MBLK_VG1_OFF,
    LVA_VPG_MBLK_VG2_ON,
    LVA_VPG_MBLK_VG2_OFF,
    LVA_DEVICE_RESET,
    LVA_VPG_MBLK_REMOTE,
    LVA_VPG_MBLK_LOCAL,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_VPG_SET_MODE_OFF,
    LVA_VPG_SET_MODE_STOP,
    LVA_VPG_SET_MODE_PUMP,
    LVA_VPG_SET_MODE_VENT_TURBO,
    LVA_VPG_SET_MODE_VENT_ALL,
    LVA_VPG_SET_MODE_LEAK_DET,
    LVA_VPG_SET_MODE_LARGE_LEAK_DET,
    LVA_VPG_MBLK_RESET_VPP_THERM,
    LVA_VPG_MBLK_RESET_TMP_ERR);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Start VPG Process", "",
    "Stop VPG Process", "",
    "Open VVR1","",
    "Close VVR1","",
    "Force VVR1","",
    "Unforce VVR1","",
    "Open VVR2","",
    "Close VVR2","",
    "Force VVR2","",
    "Unforce VVR2","",
    "VG1 ON","",
    "VG1 OFF","",
    "VG2 ON","",
    "VG2 OFF","",
    "VPG RESET","",
    "VPG REMOTE","",
    "VPG LOCAL","",
    "VPG MANUAL","",
    "VPG ON","",
    "VPG OFF","",
    "Set mode OFF","",
    "Set mode STOP","",
    "Set mode PUMP","",
    "Set mode VENT TURBO","",
    "Set mode VENT ALL","",
    "Set mode LEAK DETECTION","",
    "Set mode LARGE LEAK DETECTION","",
    "RESET VPP THERM","",
    "RESET TMP ERR","");
 
  //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin 
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1);

//======== VBGI_6B01 - BGI, driven by sequencer =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VBGI_6B01");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_BGI_6B01);
  glDevMenuItems[typeIndex] = makeDynString(
//    "PUSH_BUTTON,On,1,1" ,
//    "PUSH_BUTTON,Off,2,1", 
//    "PUSH_BUTTON,Auto,4,1", 
//    "SEPARATOR",
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1",
    "PUSH_BUTTON,Bit History,3,1");
  
  glDevMenuActs[typeIndex] = makeDynUInt(
//    LVA_DEVICE_MASK | LVA_DEVICE_ON,
//    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
//    LVA_DEVICE_MASK | LVA_DEVICE_AUTO,
//    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_RESET,
    LVA_DEVICE_FORCE,
    LVA_DEVICE_PART_OPEN,
    LVA_DEVICE_PART_CLOSE,
    LVA_DEVICE_PROC_SET_MODE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED
    );

  glDevAllowedActNames[typeIndex] = makeDynString(
    "Start BGI", "",
    "Stop BGI", "",
    "Reset","",
    "Force", "",
    "Open+", "",
    "Open-", "",
    "Set Mode", "",
    "Set VPG Connected", "",
    "Set VPG Not Connected", "");

  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 0, 0, 1,
    2, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    2, 0, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);   // Operator allowed only to select mode 1 and mode 2, expert allowed to select all ?enable? mode
  
  

  //======== VG_STD ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VG_STD");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VG_STD);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1" ,
    "PUSH_BUTTON,Off,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,History,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,Bit History,7,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_VG_STD_REMOTE,
    LVA_VG_STD_FORCE,
    LVA_DEVICE_RESET,
    LVA_VG_STD_MANUAL,
    LVA_VG_STD_LOCAL,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "",
    "Switch Gauge OFF", "",
    "Set AUTO mode", "",
    "Set REMOTE mode", "",
    "Set FORCE mode", "",
    "Reset", "",
    "Set MANUAL mode", "",
    "Set LOCAL mode", "",
    "Set VPG Connected", "",
    "Set VPG Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 0, 1, 1,
    2, 0, 1, 1,
    3, 0, 0, 1,
    3, 0, 0, 1, 
    3, 0, 0, 1, 
    3, 0, 0, 1, 
    3, 0, 0, 1,
    3, 0, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0 );

//======== VINJ_6B02 - Process Gas Injection with RVG 050 (Linac4 LEBT) =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VINJ_6B02");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_INJ_6B02);
  glDevMenuItems[typeIndex] = makeDynString(
    "SEPARATOR",
    "PUSH_BUTTON,Details,2,1",
    "PUSH_BUTTON,Setpoint History,3,1",
    "PUSH_BUTTON,State History,4,1",
    "PUSH_BUTTON,Bit History,5,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
     0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_SETPOINT_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_RESET,
    LVA_DEVICE_FORCE,
    LVA_DEVICE_PROC_SET_MODE,
    LVA_DEVICE_PART_OPEN,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "On Order", "Switch ON all groups processes",
    "Off Order", "",
    "Auto Order","",
    "Manual Order","",
    "Reset Order", "Reset all groups processes",
    "Force Order", "",
    "Set Mode", "",
    "Set VVG SetPoint", "",
    "Set VINJ_6B02 Connected", "",
    "Set VINJ_6B02 Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 3, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 3, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //=================== External target for vacuum alarms ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VEXT_ALARM");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_EXT_ALARM);
  glDevMenuItems[typeIndex] = makeDynString("SEPARATOR");
  glDevMenuActs[typeIndex] = makeDynUInt(0);
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();
  
  
  
  //======== V8DI_FE - Digital interlock monitoring =================
  typeIndex = dynAppend(glLhcVacDpTypes, "V8DI_FE");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_V8DI_FE);
  glDevMenuItems[typeIndex] = makeDynString(
    "SEPARATOR",
    "PUSH_BUTTON,Details,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
     0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ACK,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Acknowledge", "",
    "Set DI  monitoring Connected", "",
    "Set DI  monitoring Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 0, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);


  //======== VRJ_TC - Remote IO for thermocouples =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VRJ_TC");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_TC);
  glDevMenuItems[typeIndex] = makeDynString(
    "SEPARATOR",
    "PUSH_BUTTON,Details,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
     0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Thermocouples IO Connected", "",
    "Set Thermocouples IO Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VITE - Temperature measured by thermocouples =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VITE");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_TC);
  glDevMenuItems[typeIndex] = makeDynString(
    "SEPARATOR",
    "PUSH_BUTTON,History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    0,
    LVA_OPEN_PR_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();

  //======== VPN - Single NEG pump =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPN");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPN);
  glDevMenuItems[typeIndex] = makeDynString("SEPARATOR",
    "PUSH_BUTTON,Details,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
     0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set VPN Connected", "",
    "Set VPN Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VP_NEG - Controllable chain of NEG pumps =================
  
  typeIndex = dynAppend(glLhcVacDpTypes, "VP_NEG");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VP_NEG);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_VPN_SETMODE,
    LVA_DEVICE_VPN_MANUAL_VOLT_SETPOINT,
    LVA_DEVICE_VPN_MANUAL_CUR_SETPOINT,
    LVA_DEVICE_VPN_MANUAL_TIME_SETPOINT,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "ON", "",
    "OFF", "",
    "AUTO","",
    "Set Mode","",
    "Set VOLT Setpoint (manual)", "",
    "Set CURRENT Setpoint (manual)", "",
    "Set TIMEOUT Setpoint (manual)", "",
    "Set VP_NEG Connected", "",
    "Set VP_NEG Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 0, 1, 1,
    3, 0, 1, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);
  
  //======== VPNMUX - Multiplexor for VP_NEGs =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPNMUX");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPNMUX);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_RESET,
    LVA_DEVICE_FORCE,
    LVA_DEVICE_SETMUX_CHANNEL,
    LVA_DEVICE_VPN_MANUAL_VOLT_SETPOINT,
    LVA_DEVICE_VPN_MANUAL_CUR_SETPOINT,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(   
    "ON", "",
    "OFF", "",
    "AUTO", "",
    "MANUAL", "",
    "RESET", "",
    "FORCE", "",
    "Set MUX Channel", "",
    "Set VOLT Setpoint (manual)", "",
    "Set CURRENT Setpoint (manual)", "",
    "Set MUX Connected", "",
    "Set MUX Not Connected", "" );
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 0, 1, 1,
    3, 0, 1, 1,
    3, 0, 0, 1,
    3, 0, 0, 1, 
    3, 0, 0, 1, 
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== VG_DP VG Profibus ===============================
  typeIndex = dynAppend(glLhcVacDpTypes, "VG_DP");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VG_DP);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On,1,1" ,
    "PUSH_BUTTON,Off,2,1", 
    "SEPARATOR",
    "PUSH_BUTTON,Details,4,1",
    "PUSH_BUTTON,History,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,Bit History,7,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_ON,
    LVA_DEVICE_MASK | LVA_DEVICE_OFF,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_SET_LOW_THRESHOLD,
    LVA_DEVICE_SET_HIGH_THRESHOLD,
    LVA_DEVICE_ENABLE_PENNING,
    LVA_DEVICE_DISABLE_PENNING,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON", "",
    "Switch Gauge OFF", "",
    "Set AUTO mode", "",
    "Set MANUAL mode", "",
    "Set Low Pressure Threshold", "",
    "Set High Pressure Threshold", "",
    "Enable Penning Gauge", "",
    "Disable Penning Gauge", "",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 0, 0, 1,
    2, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    3, 0, 0, 1,
    4, 0, 1, 0,
    4, 0, 1, 0);

	
//======== VPC_HCCC - Vacuum Pump Cryo HRS-HCC Controller =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPC_HCCC");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPC_HCCC);
  glDevMenuItems[typeIndex] = makeDynString(
    "SEPARATOR",
    "PUSH_BUTTON,Details,2,1",
    "PUSH_BUTTON,Setpoint History,3,1",
    "PUSH_BUTTON,State History,4,1",
    "PUSH_BUTTON,Bit History,5,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
     0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_SETPOINT_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_VPC_HCCC_COOLDOWN,
    LVA_DEVICE_OFF,
	LVA_DEVICE_VPC_HCCC_REGEN, 
    LVA_DEVICE_AUTO,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_RESET,
	LVA_DEVICE_VPC_HCCC_RESET_COMP,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "On Order", "Start Cooldown",
    "Off Order", "Regeneration then stop",
	  "Regen Order", "Regeneration then restart",
    "Auto Order","",
    "Manual Order","",
    "Reset Order", "Reset errors",
	  "Get Params", "Force Get Parameters",
    "Set Pump Connected", "",
    "Set Pump Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(  
    //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin
    2, 3, 0, 1, //On Order
    3, 0, 0, 1, //Off Order
    3, 0, 0, 1, //Regen Order
    3, 0, 0, 1, //Auto Order
	  3, 0, 0, 1, //Manual Order 
    3, 3, 0, 1, //Reset Order
	  3, 0, 0, 1, //Force Get Parameters
    4, 0, 1, 0,
    4, 0, 1, 0);

  //======== ALARM_DO - Digital Output Alarm Generator =================
  typeIndex = dynAppend(glLhcVacDpTypes, "ALARM_DO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_ALARM_DO);
  glDevMenuItems[typeIndex] = makeDynString(
    "SEPARATOR",
    "PUSH_BUTTON,Details,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
     0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Alarm Generator Connected", "",
    "Set Alarm Generator Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);
//======== VPS - Sublimation Pump Power Supply =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPS");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPS);
  glDevMenuItems[typeIndex] = makeDynString(
    "SEPARATOR",
    "PUSH_BUTTON,Details,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
     0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_MANUAL,
    LVA_DEVICE_AUTO,
    LVA_DEVICE_RESET,
    LVA_DEVICE_VPS_SWITCH_FILAMENT,    
    LVA_DEVICE_VPS_RESET_COUNTERS,
    LVA_DEVICE_VPS_FILAMENT1,
    LVA_DEVICE_VPS_FILAMENT2,
	  LVA_DEVICE_VPS_SET_DEGASSING_MODE, 
    LVA_DEVICE_VPS_SET_SUBLIMATION_MODE,
    LVA_DEVICE_VPS_SET_DEGASSING_CURRENT,
    LVA_DEVICE_VPS_SET_SUBLIMATION_TIME,
    LVA_DEVICE_VPS_SET_SUBLIMATION_RATE,
    LVA_DEVICE_VPS_APPLY_SUBLIMATION,
    LVA_DEVICE_VPS_APPLY_DEGASSING,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "VPS On(manual)", "All VPS On (manual)",
    "VPS Off(manual)", "All VPS Off (manual)",
	  "Set VPS Manual", "Set All VPS Manual",
    "Set VPS Auto","Set All VPS Auto",
    "Reset VPS Errors + Warnings","Reset All VPS Errors + Warnings",
    "Switch VPS Filamentation","Switch All VPS Filamentation",
    "Reset VPS counters","Reset All VPS counters",
    "Set VPS Filament 1","Set All VPS Filament 1",
    "Set VPS Filament 2","Set All VPS Filament 2",
    "Set VPS Set Degassing mode","Set All VPS Degassing mode",
    "Set VPS Sublimation mode","Set All VPS Sublimation mode",
    "Set VPS Degassing Current","Set All VPS Degassing Current",
    "Set VPS Sublimation Time","Set All VPS Sublimation Time",
    "Set VPS Sublimation Rate","Set All VPS Sublimation Rate",
    "Apply VPS Sublimation mode","Apply All VPS Sublimation mode",
    "Apply VPS Degassing mode","Apply All VPS Degassing mode",
    "Set VPS Connected", "",
    "Set VPS Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(  
  //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin
    2, 3, 0, 1, //On Order
    2, 3, 0, 1, //Off Order
    2, 0, 0, 1, //Manual
    2, 0, 0, 1, //Auto
    2, 3, 0, 1, //Reset Errors
 	  3, 0, 0, 1, //Switch filament
 	  3, 0, 0, 1, //Reset counters
    2, 3, 0, 1, //Filament1
	  2, 3, 0, 1, //Filament2
	  3, 0, 0, 1, //Set Degassing mode
	  2, 3, 0, 1, //Set Sublimation mode   
	  3, 0, 0, 1, //Degassing current    
	  2, 0, 0, 1, //Sublimation Time    
	  2, 0, 0, 1, //Sublimation Rate   
    2, 0, 0, 1,  //Apply sublimation mode
    3, 0, 0, 1, //Apply degassing mode
    4, 0, 1, 0,
    4, 0, 1, 0);

//======== VPS_Process - Sublimation process =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VPS_Process");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VPS_PROCESS);
  glDevMenuItems[typeIndex] = makeDynString(
    "SEPARATOR",
    "PUSH_BUTTON,Details,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
     0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_VPS_PROCESS_START_NOW,
    LVA_DEVICE_RESET,
    LVA_DEVICE_VPS_PROCESS_APPLY_SET,
    LVA_DEVICE_VPS_PROCESS_START_ONCE,
    LVA_DEVICE_VPS_PROCESS_APPLY_START,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Start Process", "",
    "Stop Process", "Stop all VPS Processes",
	  "Start Cycling Now", "",
    "Reset Process", "Reset All VPS Processes",
    "Apply Settings for Cycling", "",
    "Run Sublimation Once Right Now", "",
    "Apply Settings for Cycling and RUN", "",
    "Set VPS Process Connected", "",
    "Set VPS Process Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(  
  //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin
    2, 0, 0, 1, //On
    2, 3, 0, 1, //Off
    2, 0, 0, 1, //Start Now
    3, 3, 0, 1, //Reset
    2, 0, 0, 1, //Apply Settings
	  2, 0, 0, 1, //Run Once
	  2, 0, 0, 1, //Apply settings and run
    4, 0, 1, 0,
    4, 0, 1, 0
    );


//======== VITL_CMNT - CryoMaintain interlcok source =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VITL_CMNT");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_ITL_CMNT);
  glDevMenuItems[typeIndex] = makeDynString(
    "SEPARATOR",
    "PUSH_BUTTON,Details,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
     0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED
    );
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Enable CryoMaintain Source", "Enable All CryoMaintain Sources",
    "Disable CryoMaintain Source", "Disable All CryoMaintain Sources",
    "Set CryoMaintain Connected", "",
    "Set CryoMaintain Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(  
    3, 3, 0, 1, //On
    3, 3, 0, 1, //Off
    4, 0, 1, 0,
    4, 0, 1, 0);
 
//======== VITL_CMUX - CryoMaintain interlcok multiplexer =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VITL_CMUX");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_ITL_CMUX);
  glDevMenuItems[typeIndex] = makeDynString(
    "SEPARATOR",
    "PUSH_BUTTON,Details,2,1",
    "PUSH_BUTTON,State History,3,1",
    "PUSH_BUTTON,Bit History,4,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
     0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set CryoMaintain MUX Connected", "",
    "Set CryoMaintain MUX Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);
 
//======== VR_GT - Control for TPG300 =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VR_GT");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VR_GT);
  glDevMenuItems[typeIndex] = makeDynString();
  glDevMenuActs[typeIndex] = makeDynUInt();
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_ON,  
    LVA_DEVICE_OFF,
    LVA_DEVICE_RESET,
    LVA_DEVICE_SET_CMD,
	LVA_DEVICE_SAVEH);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch VR_GT ON", "",
    "Switch VR_GT OFF", "",
    "Reset VR_GT Errors", "",
    "Setting TPG300", "",
	"Save Hotstart", "Save Hotstart all TPG300");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    3, 0, 0, 0,
    3, 0, 1, 0,
    3, 0, 1, 0,
    3, 0, 0, 0,
	 3, 3, 1, 1);
  //======= VG_PT - TPG300-controlled Penning gauge ================================
  typeIndex = dynAppend(glLhcVacDpTypes, "VG_PT");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VG_PT);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,On Forced,1,1",
    "PUSH_BUTTON,On Protect,2,1",
    "PUSH_BUTTON,Off,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,TPG300,6,1",
    "PUSH_BUTTON,History,7,1",
    "PUSH_BUTTON,State History,8,1",
    "PUSH_BUTTON,Bit History,9,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_VG_PT_ON_FORCED,
    LVA_DEVICE_MASK | LVA_DEVICE_VG_PT_ON_PROTECT,
    LVA_DEVICE_MASK | LVA_DEVICE_VG_PT_OFF,
    0,
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_CONFIG_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_VG_PT_ON_FORCED,
    LVA_DEVICE_VG_PT_ON_PROTECT,
    LVA_DEVICE_VG_PT_OFF,
    LVA_DEVICE_VRGT_CONFIG,
    LVA_DEVICE_SET_BLOCKED_OFF,
    LVA_DEVICE_CLEAR_BLOCKED_OFF,
    LVA_TPG300_HOT_START_DISABLE,
    LVA_TPG300_HOT_START_ENABLE,
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch Gauge ON (Forced)", "Switch All Gauges ON (Forced)",
    "Switch Gauge ON (Protect)", "Switch All Gauges ON (Protect)",
    "Switch Gauge OFF", "Switch All Gauges OFF",
    "Change TPG300 configuration", "Impossible",
    "Set gauge Blocked OFF", "",
    "Clear gauge Blocked OFF", "",
    "Disable Hot Start for TPG300", "",
    "Enable Hot Start for TPG300", "",
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 0, 1,
    2, 3, 0, 1,
    2, 3, 0, 0,
    4, 0, 0, 0,
    3, 0, 1, 0,
    3, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0,
    4, 0, 1, 0);
   //======== VA_OK - Vacuum Alarm OK - read only eqp =================
  typeIndex = dynAppend(glLhcVacDpTypes, "VA_OK");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VA_OK);
  glDevMenuItems[typeIndex] = makeDynString(
    "SEPARATOR",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    0,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();
  
  //======== VR_ER - Vacuum contRol - bakE-out Rack ==================
  typeIndex = dynAppend(glLhcVacDpTypes, "VR_ER");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VRE);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Reset errors,1,1",
    "SEPARATOR",
    "PUSH_BUTTON,Acknowledge Alert,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Details,5,1",
    "PUSH_BUTTON,State History,6,1",
    "PUSH_BUTTON,Bit History,7,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_MASK | LVA_DEVICE_RESET,
    0,
	 LVA_ALERT_ACKNOWLEDGE,
	 0,
	 LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_STANDBY,
    LVA_DEVICE_STOP,
    LVA_DEVICE_START,	
    LVA_DEVICE_SAFEOFF,	
    LVA_DEVICE_SAFEON,	
    LVA_DEVICE_PCSOFF,	
    LVA_DEVICE_PCSON,	
    LVA_DEVICE_RESET,
		LVA_ALERT_ACKNOWLEDGE);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "STANDBY bake-out rack",  "STANDBY All bake-out racks",
    "DECREASE SP bake-out rack",  "DECREASE SP All bake-out racks",
    "START bake-out rack",  "START All bake-out racks",
    "SAFEOFF bake-out rack",  "SAFEOFF All bake-out racks",
    "SAFEON bake-out rack",  "SAFEON All bake-out racks",
    "PCSOFF bake-out rack",  "PCSOFF All bake-out racks",
    "PCSON bake-out rack",  "PCSON All bake-out racks",
    "RESET bake-out rack",  "RESET All bake-out racks",
	"Acknowledge Alert", "Acknowledge of All bake-out racks");
  //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    2, 3, 1, 1,
    3, 3, 1, 1,
    3, 3, 1, 1,
    3, 3, 1, 1,
    2, 3, 1, 1,
    3, 3, 1, 1,
    2, 3, 1, 1,
    2, 3, 1, 1,
		2, 3, 0, 0);
  
  //======== VR_EC - Vacuum contRol - bakE-out regulation Channel ==================
  typeIndex = dynAppend(glLhcVacDpTypes, "VR_EC");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VRE);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Process Value History,1,1",
    "PUSH_BUTTON,Set Point History,2,1",    
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_PR_HISTORY,
	 LVA_OPEN_SETPOINT_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();
  
  //======== VR_EA - Vacuum contRol - bakE-out Alarm channel ==================
  typeIndex = dynAppend(glLhcVacDpTypes, "VR_EA");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VRE);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Process Value History,1,1",
    "PUSH_BUTTON,Alarm Threshold History,2,1",    
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_PR_HISTORY,
	 LVA_OPEN_SETPOINT_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt();
  glDevAllowedActNames[typeIndex] = makeDynString();
  glDevAllowedActConfig[typeIndex] = makeDynInt();
  
  //======================================================================= 
  
  
  //======= VG_A_RO Generic Analog Gauge Read Only ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VG_A_RO");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VG_A_RO);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,History,2,1",
    "PUSH_BUTTON,State History,3,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_PR_HISTORY,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Gauge Connected", "",
    "Set Gauge Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
    4, 0, 1, 0,
    4, 0, 1, 0);
  //--------------------------------------------------------------------------
  
   //======== VA_RI - Vacuum Alarm - Relay Ion pump controller ==================
  typeIndex = dynAppend(glLhcVacDpTypes, "VA_RI");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VA_RI);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1",
    "PUSH_BUTTON,Bit History,3,1",
    "SEPARATOR",
    "PUSH_BUTTON,Acknowledge Alert,5,1");      
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY,
    LVA_OPEN_BIT_HISTORY,
    0,
	 LVA_ALERT_ACKNOWLEDGE);
  glDevAllowedActs[typeIndex] = makeDynUInt(
      LVA_VA_RI_SOURCE_NEWVALUE_2HV,
      LVA_VA_RI_SOURCE_NEWVALUE_4HV,
      LVA_VA_RI_SOURCE_LASTORDER,
      LVA_VA_RI_SOURCE_DEFAULT,
      LVA_VA_RI_THRESHOLD_NEWVALUE,
      LVA_VA_RI_THRESHOLD_LASTORDER,
      LVA_VA_RI_THRESHOLD_DEFAULT,
      LVA_VA_RI_HYST_NEWVALUE,
      LVA_VA_RI_HYST_LASTORDER,
      LVA_VA_RI_HYST_DEFAULT,
      LVA_VA_RI_UPDATE,
      LVA_VA_RI_SET_UNIT_MBAR,
      LVA_DEVICE_RESET_WRS,
			LVA_ALERT_ACKNOWLEDGE);
  glDevAllowedActNames[typeIndex] = makeDynString(
      "Set Relay Source with new value", "Set All Relays Source with new value",
      "Set Relay Source with new value", "Set All Relays Source with new value",
      "Set Relay Source with last order value", "Set All Relays Source with last order value",
      "Set Relay Source with default value", "Set All Relays Source with default value",
      "Set Relay Threshold with new value", "Set All Relays Threshold with new value",
      "Set Relay Threshold with last order value", "Set All Relays Threshold with last order value",
      "Set Relay Threshold with default value", "Set All Relays Threshold with default value",
      "Set Relay Hysteresis with new value", "Set All Relays Hysteresis with new value",
      "Set Relay Hysteresis with last order value", "Set All Relays Hysteresis with last order value",
      "Set Relay Hysteresis with default value", "Set All Relays Hysteresis with default value",
      "Update parameter values", "Update All Relays parameter values",
      "Set Relay Unit mbar", "Set All Relays Unit mbar",
      "Reset Communication", "Reset All communication",
		"Acknowledge Alert", "Acknowledge of All relays");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
  //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin      
      3, 0, 1, 1,
      3, 0, 1, 1,
      3, 3, 1, 1,
      3, 3, 1, 1,
      3, 3, 1, 1,
      3, 3, 1, 1,
      3, 3, 1, 1,
      3, 3, 1, 1,
      3, 3, 1, 1,
      3, 3, 1, 1,
      2, 3, 0, 0,
      2, 3, 0, 0,
      2, 3, 0, 0,
	  2, 3, 0, 0);
  //======================================================================= 
  
  //======= VOPS_2PS Double over pressure switch ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VOPS_2PS");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VOPS_2PS);
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_SET_CONNECTED,  
    LVA_DEVICE_SET_NOT_CONNECTED);
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Set Pressure switch in VELO Connected", "",
    "Set Pressure switch in VELO Not Connected", "");
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     4, 0, 1, 0,
     4, 0, 1, 0);
  
  //======= VELO_P_Interlocks VELO Interlock Management Control Type ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VELO_P_Interlocks");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VELO_PROCESS);
  
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
   
  glDevAllowedActs[typeIndex] = makeDynUInt(
	LVA_ITL_GIS_V5_UNFORCE,
    LVA_ITL_GIS_V5_FORCE,
    LVA_ITL_MOTION_UNFORCE,
    LVA_ITL_MOTION_FORCE,
    LVA_ITL_COOLING_UNFORCE,  
    LVA_ITL_COOLING_FORCE,
    LVA_ITL_UPS_UNFORCE,
    LVA_ITL_UPS_FORCE,
    LVA_ITL_VVS_1R8_UNFORCE,
    LVA_ITL_VVS_1R8_FORCE,
    LVA_ITL_ACLSMOKE_UNFORCE,
    LVA_ITL_ACLSMOKE_FORCE,
    LVA_ITL_WTRCOOL_UNFORCE,
    LVA_ITL_WTRCOOL_FORCE,
    LVA_ITL_GIS_UNFORCE,
    LVA_ITL_GIS_FORCE,
    LVA_ITL_VVS_1L8_UNFORCE,
    LVA_ITL_VVS_1L8_FORCE,
    LVA_ITL_OP_TRIGGERED_RESET_CMD	
    );
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Interlock GIS-V5 Unforce", "",
    "Interlock GIS-V5 Force", "",
    "Interlock Motion Unforce", "",
    "Interlock Motion-V5 Force", "",
    "Interlock Cooling Unforce", "",
    "Interlock Cooling Force", "",
    "Interlock UPS Unforce", "",
    "Interlock UPS Force", "",
    "Interlock VVS 1R8 Unforce", "",
    "Interlock VVS 1R8 Force", "",
    "Interlock Alc Smoke Unforce", "",
    "Interlock Alc Smoke Force", "",
    "Interlock Water Cooling Force", "",
    "Interlock Water Cooling Unforce", "",
    "Interlock GIS Force", "",
    "Interlock GIS Unforce", "",
    "Interlock VVS 1L8 Force", "",
    "Interlock VVS 1L8 Unforce", "",
    "Interlock Op Triggered Reset Command", ""
    );
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,	 
     3, 0, 1, 0     
     );
	 
  //======= VELO_P_Main VELO PROCESS MAIN Control Type ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VELO_P_Main");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VELO_PROCESS); 
  
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
   
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_DEVICE_RESET,  
    LVA_DISABLE_MAN_CONTROL,
    LVA_ENABLE_MAN_CONTROL,
    LVA_DEVICE_ON,
    LVA_DEVICE_OFF,
    LVA_SKIP_STEP,
    LVA_BALANCE,
    LVA_EVACUATE,
    LVA_DEVICE_VENT,
    LVA_PREPARE,
    LVA_FORCE_DELAY_EVAC_MEAS,
    LVA_VELO_START_IONPUMP,
    LVA_FORCE_DELAY_VENT_MEAS,
    LVA_VELO_SMOG_INJECT,
    LVA_VELO_SMOG_START,
    LVA_VELO_SMOG_DISABLE,
    LVA_VELO_SMOG_ENABLE,
    LVA_VELO_SMOG_SET_TURBO_SPEED,
    LVA_START_BAKEOUT,
    LVA_END_BAKEOUT,
    LVA_VELO_SMOG_FINISH,
    LVA_VELO_STOP_IONPUMP,
    LVA_ACT_RP_TOGGLE,
	LVA_IGN_RP101_TOGGLE,
	LVA_IGN_RP201_TOGGLE,
	LVA_IGN_PI102_TOGGLE,
	LVA_IGN_PI202_TOGGLE,
	LVA_IGN_PI421_TOGGLE,
	LVA_IGN_PI422_TOGGLE,
	LVA_IGN_PE421_TOGGLE,
	LVA_IGN_PE422_TOGGLE,
	LVA_IGN_PI411_TOGGLE,
	LVA_IGN_PI412_TOGGLE,
	LVA_IGN_PE411_TOGGLE,
	LVA_IGN_PE412_TOGGLE,
	LVA_IGN_IP431_TOGGLE,
	LVA_IGN_IP441_TOGGLE,
	LVA_OPP_10_TOGGLE,
	LVA_OPP_10_RESET,
	LVA_OPP_25_TOGGLE,
	LVA_OPP_25_RESET
    );
	
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Reset Process", "",
    "Disable Manual Control", "",
    "Enable Manual Control", "",
    "Switch VELO Main Process ON", "",
    "Switch VELO Main Process OFF", "",
    "Skip VELO process step", "",
    "Activate balancing mode", "",
    "Start evacuation process", "",
    "Start venting process", "",
    "Start prepare process", "",
    "Force valve delay measurement for evacuation","",
    "Start ion pumps","",
    "Force valve delay measurement for venting", "",
    "Start SMOG injection", "",
    "Start SMOG process", "",
    "Disable SMOG process", "",
    "Enable SMOG process", "",
    "Set turbo pump speed", "",
    "Enable Bakeout Process", "",
    "Finish Bakeout Process", "",
    "Finish SMOG injection", "",
    "Stop ion pumps","",
    "Toggle active primary pump","",
	"Toggle RP101 ignore","",
	"Toggle RP201 ignore","",
	"Toggle PI102 ignore","",
	"Toggle PI202 ignore","",
	"Toggle PI421 ignore","",
	"Toggle PI422 ignore","",
	"Toggle PE421 ignore","",
	"Toggle PE422 ignore","",
	"Toggle PI411 ignore","",
	"Toggle PI412 ignore","",
	"Toggle PE411 ignore","",
	"Toggle PE412 ignore","",
	"Toggle IP431 ignore","",
	"Toggle IP441 ignore","",
	"Toggle -10/+10 Overpressure Protection","",
	"Reset -10/+10 Overpressure Trigger","",
	"Toggle +2/-5 Overpressure Protection","",
	"Trigger +2/-5 Overpressure Protection",""
    );
  //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin    
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,	
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0,
	 3, 0, 1, 0		 
     );
  
    //======= VELO_P_Bakeout VELO BAKEOUT PROCESS Control Type ===========================
  typeIndex = dynAppend(glLhcVacDpTypes, "VELO_P_Bakeout");
  dynAppend(glDpTypeGroups, VAC_DP_TYPE_GROUP_VELO_PROCESS); 
  
  glDevMenuItems[typeIndex] = makeDynString(
    "PUSH_BUTTON,Details,1,1",
    "PUSH_BUTTON,State History,2,1");
  glDevMenuActs[typeIndex] = makeDynUInt(
    LVA_OPEN_DETAIL_PANEL,
    LVA_OPEN_STATE_DETAILS_HISTORY);
   
  glDevAllowedActs[typeIndex] = makeDynUInt(
    LVA_VELO_PS300_BAKE_OFF_CMD,  
    LVA_VELO_PS300_BAKE_ON_CMD,
    LVA_VELO_VESSEL_BAKE_OFF_CMD,
    LVA_VELO_VESSEL_BAKE_ON_CMD,
    LVA_VELO_PS300_ITL_UNFORCE,
    LVA_VELO_PS300_ITL_FORCE,
    LVA_VELO_VESSEL_ITL_UNFORCE,
    LVA_VELO_VESSEL_ITL_FORCE,
    LVA_PS300_GV302_SET_TEMP_SETPOINT,
    LVA_PS300_TP301_SET_TEMP_SETPOINT,
    LVA_PS300_BAKEOUT_SET_HYST_VAL
    );
  glDevAllowedActNames[typeIndex] = makeDynString(
    "Switch OFF PS300 Bakeout", "",
    "Switch ON PS300 Bakeout", "",
    "Switch OFF VESSEL Bakeout", "",
    "Switch ON VESSEL Bakeout", "",
    "Unforce PS300 Interlock", "",
    "Force PS300 Interlock", "",
    "Unforce VESSEL Interlock", "",
    "Force VESSEL Interlock", "",
    "Set PS300-GV302 Temperature Setpoint", "",
    "Set PS300-TP301 Temperature Setpoint", "",
    "Set PS300 Bakeout Hysteresis Value", ""
    );
  //Single Action, Group Action, Comment Require Single, Comment Require Group
	//0: Not possible
	//1: Monitor
	//2: Operator
	//3: Expert
	//4: Admin    
  glDevAllowedActConfig[typeIndex] = makeDynInt(
     3, 0, 1, 0,  
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0,
     3, 0, 1, 0
     );
  
  CheckActionListConsistency();  
}

// check consistency for global lists(glDevAllowedActs,glDevAllowedActNames,glDevAllowedActConfig)   
void CheckActionListConsistency()
{
  int dpTypeLen = dynlen(glLhcVacDpTypes);
  for( int n = 1; n <= dpTypeLen; n++)
  {
    int actionLen = dynlen(glDevAllowedActs[n]);
    int actionNamesLen = dynlen(glDevAllowedActNames[n]);
    int actionConfigLen = dynlen(glDevAllowedActConfig[n]);
    int menuItemLen = dynlen(glDevMenuItems[n]);
    int menuActionLen = dynlen(glDevMenuActs[n]);                        
    if( menuItemLen != menuActionLen)
    {
      DebugTN( "INTERNAL : glDevMenuItems[] doesn't corresponds to glDevMenuActs[] for dpType '" + glLhcVacDpTypes[n] + "'");
    }
    if( actionNamesLen != (actionLen*2))
    {
      DebugTN( "INTERNAL : glDevAllowedActNames is not correct for dpType '" + glLhcVacDpTypes[n] + "'");
    }
    if( actionConfigLen != (actionLen*4))
    {
      DebugTN( "INTERNAL : glDevAllowedActConfig is not correct for dpType '" + glLhcVacDpTypes[n] + "'");
    }    
  }
}
//LhcVacGetAllEqpDpTypes:
/** Return list of all DP types used for vacuum equipment

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param none
  @return:  list of all DP types for vacuum equipment
*/
dyn_string LhcVacGetAllEqpDpTypes()
{
  return glLhcVacDpTypes;
}

/** Reload all vacuum equipment data, register it in DLL

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param exceptionInfo: out, details of any exceptions are returned here.

  @return 0 - in case of success (even not 100% success), -1 in case of fatal error
  @author L.Kopylov
*/
long LhcVacRenewVacEqpData(dyn_string &exceptionInfo)
{
  LhcVacStartLongActPart(50.);
  int coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
    exceptionInfo);
  LhcVacFinishLongActPart();
  if(coco < 0)
  {
    DebugTN("LhcVacEqpInitActive() failed:", exceptionInfo);
    return -1;
  }

  // Register PVSS functional types for DP types
  LhcVacStartLongActPart(40.);
  int nDpTypes = dynlen(glLhcVacDpTypes);
  for(int n = 1 ; n < nDpTypes ; n++)
  {
    LhcVacSetPvssFunctionalType(glLhcVacDpTypes[n], glDpTypeGroups[n]);
    LhcVacReportLongAct(n, nDpTypes);
  }
  LhcVacFinishLongActPart();

  // Set special synoptics flags for 'complex' sectors
  LhcVacStartLongActPart(10.);
  dyn_string  mainParts;
  dyn_dyn_string sectors, domains;
  LhcVacGetAllSectors(mainParts, sectors, domains, exceptionInfo);
  int nMps = dynlen(mainParts);
  for(int mpIdx = 1 ; mpIdx <= nMps ; mpIdx++)
  {
    string fileName = LhcVacGetSpecSynoptic(sectors[mpIdx]);
    if(fileName != "")
    {
      strreplace(fileName, ".pnl", ".xml");
      string realFileName = getPath(PANELS_REL_PATH, fileName);
      if(realFileName == "")
      {
        realFileName = fileName;  // 'File not found' is better than nothing
      }
//DebugTN("file for " + mainParts[mpIdx] + " is " + fileName);
      for(int sectIdx = dynlen(sectors[mpIdx]) ; sectIdx > 0 ; sectIdx--)
      {
        /* L.Kopylov 25.05.2012
        LhcVacSetSectorSpecSynoptic(sectors[mpIdx][sectIdx]);
        */
        LhcVacSetSectorSpecSynPanel(sectors[mpIdx][sectIdx], realFileName);
      }
    }
    fileName = LhcVacGetSpecSectView(sectors[mpIdx]);
    if(fileName != "")
    {
//DebugTN("original file for " + mainParts[mpIdx] + " is " + fileName);
      fileName = getPath(PANELS_REL_PATH, fileName);
      strreplace(fileName, ".pnl", ".xml");
//DebugTN("file for " + mainParts[mpIdx] + " is " + fileName);
      for(int sectIdx = dynlen(sectors[mpIdx]) ; sectIdx > 0 ; sectIdx--)
      {
        LhcVacSetSectorSpecSectPanel(sectors[mpIdx][sectIdx], fileName);
      }
    }
    LhcVacReportLongAct(mpIdx, nMps);
  }

  LhcVacFinishLongActPart();
  return 0;
}

//LhcVacGetDevicePanel:
/** Get file name and panel name to display detailed device information.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name
  @param dpType: in/out, DP type.
  @param useDpName: out, DP name to be used when opening detail panel, not necessary
                    equal to dpName (for example, VPI details are shown in VPCI panel)
  @param useDpType: out, DP type to be used when opening detail panel
  @param fileName: out, file name with panel will be returned here
  @param panelName: out, required panel name will be returned here
  @param exceptionInfo: out, details of any exceptions are returned here
*/
int LhcVacGetDevicePanel(const string dpName, string dpType, string &useDpName,
                         string &useDpType, string &fileName, string &panelName,
                         dyn_string &exceptionInfo)
{
  LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return LVA_FAILURE;
  }
  useDpName = dpName;
  useDpType = dpType;
  
  //PARTICULAR CASE
  if(dpType == "COLDEX")
  {
	  fileName = VacResourcesGetValue(dpName + ".PanelFile", VAC_RESOURCE_STRING, "");
	  panelName = VacResourcesGetValue(dpName + ".PanelTitle", VAC_RESOURCE_STRING, "");
	  return LVA_OK;
  }	  
  
  //GENERIC CASE
  fileName = VacResourcesGetValue(dpType + ".PanelFile", VAC_RESOURCE_STRING, "");
  panelName = VacResourcesGetValue(dpType + ".PanelTitle", VAC_RESOURCE_STRING, "");
  if(fileName == "")
  {
    // GIS and similar may have resource DP_TYPE.DP_NAME.PanelFile
    fileName = VacResourcesGetValue(dpType + "." + dpName + ".PanelFile", VAC_RESOURCE_STRING, "");
    panelName = VacResourcesGetValue(dpType + "." + dpName + ".PanelTitle", VAC_RESOURCE_STRING, "");
    if(fileName != "")
    {
      return LVA_OK;
    }
    // Get panel for master (if any)
    string master;
    int channel;
    LhcVacEqpMaster(dpName, master, channel);
    if(master == "")  // get panel for slave (if any)
    {
      dyn_string slaveDpNames;
      LhcVacEqpSlaves(dpName, slaveDpNames);
      if(dynlen(slaveDpNames) == 0)
      {
        fwException_raise(exceptionInfo,
          "ERROR", "LhcVacGetDevicePanel: No panel for <" + dpName + "> of type <" + dpType + ">", "");
        return LVA_FAILURE;
      }
      int listLen = dynlen(slaveDpNames), n;
      for(n = 1; n <= listLen; n++)
      {
        int mobileType;
        bool isActive;
        LhcVacMobileProp(slaveDpNames[n], mobileType, isActive, exceptionInfo);
        if(dynlen(exceptionInfo) > 0)
        {
          return LVA_FAILURE;
        }
        if(isActive)
        {
          break;
        }
      }
      if(n > listLen)
      {
        fwException_raise(exceptionInfo,
          "ERROR", "LhcVacGetDevicePanel: No panel for <" + dpName + "> of type <" + dpType + ">", "");
        return LVA_FAILURE;
      }
      LhcVacGetDpTypeIndex(slaveDpNames[n], useDpType, exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        return LVA_FAILURE;
      }
      fileName = VacResourcesGetValue(useDpType + ".PanelFile", VAC_RESOURCE_STRING, "");
      if(fileName == "")
      {
        fwException_raise(exceptionInfo,
          "ERROR", "LhcVacGetDevicePanel: No panel for <" + dpName + "> and slave <" + slaveDpNames[n] + ">", "");
        return LVA_FAILURE;
      }
      panelName = VacResourcesGetValue(useDpType + ".PanelTitle", VAC_RESOURCE_STRING, "");
      useDpName = slaveDpNames[n];
    }
    else
    {
      useDpType= "";
      LhcVacGetDpTypeIndex(master, useDpType, exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        return LVA_FAILURE;
      }
      fileName = VacResourcesGetValue(useDpType + ".PanelFile", VAC_RESOURCE_STRING, "");
      if(fileName == "")
      {
        fwException_raise(exceptionInfo,
          "ERROR", "LhcVacGetDevicePanel: No panel for <" + dpName + "> and master <" + master + ">", "");
        return LVA_FAILURE;
      }
      panelName = VacResourcesGetValue(useDpType + ".PanelTitle", VAC_RESOURCE_STRING, "");
      useDpName = master;
    }
  }
  return LVA_OK;
}

//LhcVacGetControllerPanel:
/** Get file name and panel name to display detailed device information.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name
  @param dpType: in/out, DP type.
  @param controllerDpName: out, DP name to be used when opening controller detail panel
  @param controllerDpType: out, DP type to be used when opening controller detail panel
  @param fileName: out, file name with panel will be returned here
  @param panelName: out, required panel name will be returned here
  @param exceptionInfo: out, details of any exceptions are returned here
*/
int LhcVacGetControllerPanel(const string dpName, string dpType, string &controllerDpName,
                         string &controllerDpType, string &fileName, string &panelName,
                         dyn_string &exceptionInfo)
{
  LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return LVA_FAILURE;
  }
  fileName = VacResourcesGetValue(dpType + ".ControllerPanelFile", VAC_RESOURCE_STRING, "");
  panelName = VacResourcesGetValue(dpType + ".ControllerPanelTitle", VAC_RESOURCE_STRING, "");
  if(fileName == "")
  {
	fwException_raise(exceptionInfo,
		"ERROR", "LhcVacGetControllerPanel: No Controller panel for <" + dpName + "> of type <" + dpType + ">", "");
    return LVA_FAILURE;
  }
    
  // Get controllerDpName and controllerDpType from master
  string master;
  int channel;
  LhcVacEqpMaster(dpName, master, channel);
  //DebugTN("dpname:",dpName);
  //DebugTN("master:",master);
  //DebugTN("channel:",channel);
  if(master == "") 
  {
    fwException_raise(exceptionInfo,
	  "ERROR", "LhcVacGetControllerPanel: No master for <" + dpName + "> of type <" + dpType + ">", "");
    return LVA_FAILURE;
  }
  else
  {
    controllerDpType= "";
    LhcVacGetDpTypeIndex(master, controllerDpType, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return LVA_FAILURE;
    }
    controllerDpName = master;
  }
  return LVA_OK;
}
/****************************************************************************************
************* Set of functions to retrieve static equipment configuration ***************
****************************************************************************************/
//LhcVacGetDevActName:
/** Return descriptive string for single device action
Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param act: in, enumeration for action to be executed.
  @param dpName: in, DP name for action.
  @param dpType: in, DP type.
  @param result: out, descriptive text is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacGetDevActName(const dyn_uint act, const string dpName, const string dpType, string &result,
  dyn_string &exceptionInfo)
{
  string  actDpName, actDpType;
  int  typeIndex, actIndex;

  result = "UNKNOWN ACTION ???";

  LhcVacGetDpForAct(dpName, dpType, actDpName, actDpType, exceptionInfo, act[1]);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }

  if((typeIndex = LhcVacGetDpTypeIndex(actDpName, actDpType, exceptionInfo)) <= 0)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacGetDevActName: unsupported DP type <" + actDpType + ">", "");
    return;
  }
  if((actIndex = dynContains(glDevAllowedActs[typeIndex], (act[1] & LVA_ACTION_MASK))) <= 0)
  {
    fwException_raise(exceptionInfo,
    "ERROR", "LhcVacGetDevActName: act " + (act[1] & LVA_ACTION_MASK) + " is not allowed for DP type <" +
    actDpType + ">", "");
    return;
  }
  actIndex = actIndex * 2 - 1;
  if(actIndex > dynlen(glDevAllowedActNames[typeIndex]))
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacGetDevActName: no name for act " + (act[1] & LVA_ACTION_MASK) + " DP type <" +
      actDpType + ">", "");
    return;
  }
  result = glDevAllowedActNames[typeIndex][actIndex];
  if(dynlen(act) > 1)
  {
    string addResult;
    BuildCompaundActName(act[2], dpType, addResult, exceptionInfo);
    result = result + addResult;
  }
}
//Build compaund action name for any dpTypes
void BuildCompaundActName(const int actionArgument, const string dpType, string &result,
  dyn_string &exceptionInfo)
{
  if((dpType == "VPG_6A01") || (dpType == "VBGI_6B01"))
  {
    string modeName = VacResourcesGetValue(dpType + ".Mode_" + actionArgument + ".ShortName", VAC_RESOURCE_STRING, "");

    if(modeName == "")
    {
      return;  // No such mode
    }
    else
    {
      result = " (" + modeName + ")";  
    }
    
  }
  else if((dpType == "VLV_ANA") ||
          (dpType == "VV_AO"))
  {
    result = " (" + actionArgument + ")";   
  }
  else
  {
    result = "";
  }
  
}
//LhcVacGetGroupActName:
/** Return descriptive string for group action
Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param act: in, enumeration for action to be executed.
  @param dpTypeList: in, list of DP types to be affected by action.
  @param result: out, descriptive text is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacGetGroupActName(const unsigned act, const dyn_string dpTypeList, string &result,
	dyn_string &exceptionInfo)
{
  string  actDpType;
  int  typeIndex, actIndex;

  result = "UNKNOWN ACTION ???";

  // Decide on first DP type
  if(dynlen(dpTypeList) < 1)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacGetGroupActName: No DP type(s) to act on", "");
    return;
  }
  actDpType = dpTypeList[1];
  if((typeIndex = LhcVacGetDpTypeIndex("", actDpType, exceptionInfo)) <= 0)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacGetGroupActName: unsupported DP type <" + actDpType + ">", "");
    return;
  }
  if((actIndex = dynContains(glDevAllowedActs[typeIndex], (act & LVA_ACTION_MASK))) <= 0)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacGetGroupActName: act " + (act & LVA_ACTION_MASK) + " is not allowed for DP type <" +
      actDpType + ">", "");
    return;
  }
  actIndex *= 2;
  if(actIndex > dynlen(glDevAllowedActNames[typeIndex]))
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacGetGroupActName: no name for act " + (act & LVA_ACTION_MASK) + " DP type <" +
      actDpType + ">", "");
    return;
  }
  result = glDevAllowedActNames[typeIndex][actIndex];
}

//LhcVacNeedDevActComment:
/** Check if action to be executed for single device requires mandatory comment text.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param act: in, action to be executed.
  @param dpName: in, DP name for which action shall be executed.
  @param exceptionInfo: out, details of any exceptions are returned here

  @return: true if comment is required, false otherwise.
*/
bool LhcVacNeedDevActComment(unsigned act, string dpName, dyn_string &exceptionInfo)
{
  int  typeIndex, actIndex, cnfgIndex;
  string  dpType, actDpName, actDpType;

  if((dpType = dpTypeName(dpName)) == "")
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacNeedDevActComment: dpTypeName(" + dpName + ") failed", "");
    return false;
  }
  LhcVacGetDpForAct(dpName, dpType, actDpName, actDpType, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return false;
  }

  if((typeIndex = LhcVacGetDpTypeIndex(actDpName, actDpType, exceptionInfo)) <= 0)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacNeedDevActComment: unsupported DP type <" + actDpType + ">", "");
    return false;
  }
  if((actIndex = dynContains(glDevAllowedActs[typeIndex], (act & LVA_ACTION_MASK))) <= 0)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacNeedDevActComment: act " + (act & LVA_ACTION_MASK) + " is not allowed for DP type <" +
      actDpType + ">", "");
    return false;
  }
  cnfgIndex = (actIndex - 1) * 4 + 3;
  if(cnfgIndex > dynlen(glDevAllowedActConfig[typeIndex]))
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacNeedDevActComment: act " + (act & LVA_ACTION_MASK) + "for DP type <" +
      actDpType + ">: invalid action configuration", "");
    return false;
  }
  return glDevAllowedActConfig[typeIndex][cnfgIndex] != 0;
}

//LhcVacNeedGroupActComment:
/** Check if action to be executed for group of devices requires mandatory comment text.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param act: in, action to be executed.
  @param dpTypeList: in, list of DP types for which action shall be executed.
  @param exceptionInfo: out, details of any exceptions are returned here

  @return: true if comment is required, false otherwise.
*/
bool LhcVacNeedGroupActComment(unsigned act, dyn_string dpTypeList, dyn_string &exceptionInfo)
{
  int  typeIndex, actIndex;

  // Comment is mandatory if it mandatory for at least one DP type in list
  for(int n = dynlen(dpTypeList) ; n > 0 ; n--)
  {
    if((typeIndex = LhcVacGetDpTypeIndex("", dpTypeList[n], exceptionInfo)) <= 0)
    {
      fwException_raise(exceptionInfo,
        "ERROR", "LhcVacNeedGroupActComment: unsupported DP type <" + dpTypeList[n] + ">", "");
      return false;
    }
    if((actIndex = dynContains(glDevAllowedActs[typeIndex], (act & LVA_ACTION_MASK))) <= 0)
    {
      fwException_raise(exceptionInfo,
        "ERROR", "LhcVacNeedGroupActComment: act " + (act & LVA_ACTION_MASK) + " is not allowed for DP type <" +
        dpTypeList[n] + ">", "");
      return false;
    }
    int cnfgIndex = (actIndex - 1) * 4 + 4;

    if(cnfgIndex > dynlen(glDevAllowedActConfig[typeIndex]))
    {
      fwException_raise(exceptionInfo,
        "ERROR", "LhcVacNeedGroupActComment: act " + (act & LVA_ACTION_MASK) + "for DP type <" +
        dpTypeList[n] + ">: invalid action configuration", "");
      return false;
    }
    if(glDevAllowedActConfig[typeIndex][cnfgIndex] != 0)
    {
      return true;
    }
  }
  return false;
}

//LhcVacErrMsgForCode:
/** Return error message for given error code for given device

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name.
  @param dpType: in, DP type, may be empty.
  @param errCode: in, error code in range 1...255.
  @param errMsg: out, error message for error code is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here.

  @return: true if comment is required, false otherwise.
*/
void LhcVacErrMsgForCode(string dpName, string dpType, int errCode, string &errMsg,
  dyn_string &exceptionInfo)
{
  int  typeIndex, errIndex;

  // First try to get device-specific error message
  if((typeIndex = LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo)) < 1)
  {
    errMsg = "LhcVacErrMsgForCode: unsupported DP type <" + dpType + "> of <" + dpName + ">";
    return;
  }
  errMsg = VacResourcesGetValue(dpType + ".Error_" + errCode, VAC_RESOURCE_STRING, "");
  if(errMsg == "")
  {
    // Try to get common error message
    errMsg = VacResourcesGetValue("Error_" + errCode, VAC_RESOURCE_STRING, "");
    if(errMsg == "")
    {
      errMsg = "Unknown error code " + errCode;
    }
  }
}

//LhcVacWarningForCode:
/** Return warning message for given warning code for given device

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name.
  @param dpType: in, DP type, may be empty.
  @param errCode: in, warning code in range 1...255.
  @param errMsg: out, warning message for warning code is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here.

  @return: true if comment is required, false otherwise.
*/
void LhcVacWarningForCode(string dpName, string dpType, int errCode, string &errMsg,
  dyn_string &exceptionInfo)
{
  int  typeIndex;

  // First try to get device-specific error message
  if((typeIndex = LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo)) < 1)
  {
    errMsg = "LhcVacWarningForCode: unsupported DP type <" + dpType + "> of <" + dpName + ">";
    return;
  }
  errMsg = VacResourcesGetValue(dpType + ".Warning_" + errCode, VAC_RESOURCE_STRING, "");
  if(errMsg == "")
  {
    // Try to get common error message
    errMsg = VacResourcesGetValue("Warning_" + errCode, VAC_RESOURCE_STRING, "");
    if(errMsg == "")
    {
      errMsg = "Unknown warning code " + errCode;
    }
  }
}

//LhcVacModeStatusForCode:
/** Return string description of mode status for given mode code and device

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name.
  @param dpType: in, DP type, may be empty.
  @param code: in, mode code
  @param shortName: out, short name for given mode code is returned here.
  @param longName: out, long name for given mode code is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here.

  @return: true if comment is required, false otherwise.
*/
void LhcVacModeStatusForCode(string dpName, string dpType, int code,
  string &shortName, string &longName, dyn_string &exceptionInfo)
{
  int  typeIndex;

  // First try to get device-specific error message
  if((typeIndex = LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo)) < 1)
  {
    errMsg = "LhcVacModeStatusForCode: unsupported DP type <" + dpType + "> of <" + dpName + ">";
    return;
  }
  shortName = VacResourcesGetValue(dpType + ".Mode_" + code + ".ShortName", VAC_RESOURCE_STRING, "");
  if(shortName == "")
  {
    shortName = "Unknown mode " + code;
  }
  longName = VacResourcesGetValue(dpType + ".Mode_" + code + ".LongName", VAC_RESOURCE_STRING, "");
  if(longName == "")
  {
    longName = "Unknown long name for mode " + code;
  }
}

//LhcVacSeqStepForCode:
/** Return string description of sequencer active step for given step code and device

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name.
  @param dpType: in, DP type, may be empty.
  @param code: in, step code
  @param name: out, name for given mode code is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here.

  @return: true if comment is required, false otherwise.
*/
void LhcVacSeqStepForCode(string dpName, string dpType, int code,
  string &name, dyn_string &exceptionInfo)
{
  int  typeIndex;

  // First try to get device-specific error message
  if((typeIndex = LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo)) < 1)
  {
    errMsg = "LhcVacSeqStepForCode: unsupported DP type <" + dpType + "> of <" + dpName + ">";
    return;
  }
  name = VacResourcesGetValue(dpType + ".Step_" + code, VAC_RESOURCE_STRING, "");
  if(name == "")
  {
    name = "Unknown step " + code;
  }
}

//LhcVacVpgStatusForCode:
/** Return string description of LHC type VPG stauts

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name.
  @param dpType: in, DP type, may be empty.
  @param code: in, step code
  @param name: out, name for given mode code is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here.

  @return: true if comment is required, false otherwise.
*/
void LhcVacVpgStatusForCode(string dpName, string dpType, int code,
  string &name, dyn_string &exceptionInfo)
{
  int  typeIndex;

  // First try to get device-specific error message
  if((typeIndex = LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo)) < 1)
  {
    errMsg = "LhcVacSeqStepForCode: unsupported DP type <" + dpType + "> of <" + dpName + ">";
    return;
  }
  name = VacResourcesGetValue(dpType + ".VpgStatus_" + code, VAC_RESOURCE_STRING, "");
  if(name == "")
  {
    name = "Unknown status " + code;
  }
}

//LhcVacDpTypeGroup:
/** Return DP type group enumeration for given DP type

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpType: in, DP type name.

  @return: group for this DP type
*/
int LhcVacDpTypeGroup(string dpType)
{
  int dpTypeIdx = dynContains(glLhcVacDpTypes, dpType);
  if(dpTypeIdx <= 0)
  {
    return 0;
  }
  return glDpTypeGroups[dpTypeIdx];
}

// Find index corresponding to DP type for given DP
// Returns -1 if DP type is not found
int LhcVacGetDpTypeIndex(string dpName, string &dpType, dyn_string &exceptionInfo)
{
  if(dpType == "")
  {
    if(dpName == "")
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacGetDpTypeIndex: no DP name", "");
      return -1;
    }
    if((dpType = dpTypeName(dpName)) == "")
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacGetDpTypeIndex: No DP type for <" + dpName + ">", "");
      return -1;
    }
  }
  return(dynContains(glLhcVacDpTypes, dpType));
}

//LhcVacCheckDevActPrivileges:
/** Check if current operator has enough privileges to execute action for device.
	If not enough privileges - display login dialog.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param act: in, action to be executed.
  @param dpName: in, DP name to act on.
  @param showDialog: in, if true - show dialog explaining that privilege of current user
                        is not enough.
  @param result: out, true if current operator has enough privileges to execute action.
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacCheckDevActPrivileges(dyn_anytype act, string dpName, bool showDialog, bool &result,
  dyn_string &exceptionInfo)
{
  result = false;
  string dpType;
  if((dpType = dpTypeName(dpName)) == "")
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacCheckDevActPrivileges: dpTypeName(" + dpName + ") failed", "");
    return;
  }
  string actDpName, actDpType;
  LhcVacGetDpForAct(dpName, dpType, actDpName, actDpType, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }

  // Find privilege required to execute the action
  int typeIndex;
  if((typeIndex = LhcVacGetDpTypeIndex(actDpName, actDpType, exceptionInfo)) <= 0)
  {
    fwException_raise(exceptionInfo, "ERROR",
      "LhcVacCheckDevActPrivileges: unsupported DP type <" + actDpType + ">", "");
    return;
  }
  int actIndex;
  if((actIndex = dynContains(glDevAllowedActs[typeIndex], (act[1] & LVA_ACTION_MASK))) <= 0)
  {
    fwException_raise(exceptionInfo, "ERROR",
      "LhcVacCheckDevActPrivileges: act " + (act[1] & LVA_ACTION_MASK) + " is not allowed for DP type <" +
       actDpType + ">", "");
    return;
  }
  int cnfgIndex = (actIndex - 1) * 4 + 1;

  if(cnfgIndex > dynlen(glDevAllowedActConfig[typeIndex]))
  {
    fwException_raise(exceptionInfo, "ERROR",
      "LhcVacCheckDevActPrivileges: act " + (act[1] & LVA_ACTION_MASK) + "for DP type <" +
      actDpType + ">: invalid action configuration", "");
    return;
  }
  int privRequired = glDevAllowedActConfig[typeIndex][cnfgIndex];

  // Switching ON any blocked OFF device rquires expert privilege L.Kopylov 24.11.2009
  bool blockedOff = false;
  if(dpExists(actDpName + ".BlockedOFF"))
  {
    switch(act[1] & LVA_ACTION_MASK)
    {
    case LVA_DEVICE_ON:
    case LVA_DEVICE_START:
    case LVA_DEVICE_AUTO:
    case LVA_DEVICE_MEASURE_ON:
    case LVA_DEVICE_FORCED_ON:
    case LVA_DEVICE_ON_AUTO:
    case LVA_DEVICE_OPEN:
      dpGet(actDpName + ".BlockedOFF", blockedOff);
      break;
    }
  }
  if(blockedOff)
  {
    if(privRequired < 3)
    {
      privRequired = 3;
    }
    string devName;
    LhcVacDisplayName(actDpName, devName, exceptionInfo);
    if(devName == "")
    {
      devName = actDpName;
    }
    dynAppend(glActSpecialMessages, devName + ": BlockedOFF");
  }

  int dpTypeGroup = LhcVacDpTypeGroup(dpType);
  switch(dpTypeGroup)
  {
  case VAC_DP_TYPE_GROUP_VGM:  // all gauges (except VGI) + master for VPGM
    break;
  case VAC_DP_TYPE_GROUP_VGR:
  case VAC_DP_TYPE_GROUP_VGP:
  case VAC_DP_TYPE_GROUP_VG_PT:
    LhcVacGetRequiredActPriv_VGR_P(dpName, act, privRequired, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VGF:
  case VAC_DP_TYPE_GROUP_VPGMPR:
    break;
  case VAC_DP_TYPE_GROUP_VGI:
    LhcVacGetRequiredActPriv_VGI(dpName, act, privRequired, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VG_STD:  // Vacuum Gauge Standard - VG_STD
    LhcVacGetRequiredActPriv_VG_STD(dpName, act, privRequired, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VP_TP:	
  case VAC_DP_TYPE_GROUP_VPI:  // all ion pumps
  case VAC_DP_TYPE_GROUP_VRPI:
  case VAC_DP_TYPE_GROUP_VP_STDIO:
    LhcVacGetRequiredActPriv_VP(dpName, act, privRequired, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VV:  // all valves
  case VAC_DP_TYPE_GROUP_VV_STD_IO:
  case VAC_DP_TYPE_GROUP_VV_PUL:
  case VAC_DP_TYPE_GROUP_VV_ANA:
  case VAC_DP_TYPE_GROUP_VV_AO:
    LhcVacGetRequiredActPriv_VV(dpName, act, privRequired, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPG:   // All generic VPG
    LhcVacGetRequiredActPriv_VP(dpName, act, privRequired, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPGF:  // All fixed pumping groups
  case VAC_DP_TYPE_GROUP_VPGM:  // All mobile pumping groups
    break;
  case VAC_DP_TYPE_GROUP_VRPM:  // Solenoid power supply
  case VAC_DP_TYPE_GROUP_VIES:  // Solenoid
    break;
  case VAC_DP_TYPE_GROUP_VPG_6A01:      // VPG , driven by sequencer
  case VAC_DP_TYPE_GROUP_BGI_6B01:      // BGI, driven by sequencer
  case VAC_DP_TYPE_GROUP_INJ_6B02:
    LhcVacGetRequiredActPriv_Process(dpName, act, privRequired, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VP_NEG:        // Controllable chain of NEG pumps    
  case VAC_DP_TYPE_GROUP_VPNMUX:        // Multiplexor for VP_NEGs
  case VAC_DP_TYPE_GROUP_VG_DP:        // Vacuum Gauge with Profibus interface
  case VAC_DP_TYPE_GROUP_VPC_HCCC:     // HRS cryo controller, no privelege level overwrite needed
  case VAC_DP_TYPE_GROUP_VSECTOR:      // [VACCO-929] [VACCO-948] [VACCO-1645]
    break;

  }
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  // Find domain where device belons - domains are specified per sector
  string domain, mainPart;
  dyn_string domains;
  switch(act[1] & LVA_ACTION_MASK)
  {
  case LVA_ACCESS_MODE_OFF:
  case LVA_ACCESS_MODE_ON:
    LhcVacGetMainPartForAccessDp(dpName, mainPart, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return;
    }
    LhcVacDomainsOfMainPart(mainPart, domains, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return;
    }
    break;
  // Find domain for access zone
  case LVA_DEVICE_CLOSE:
  case LVA_DEVICE_OPEN:
    if((dpTypeName(dpName)) == "VACCESS_ZONE")
    {
      string   mainPart, sector1, sector2;
      bool     isBorder;
      int      devVacuum;

      LhcVacDeviceVacLocation(dpName, devVacuum, sector1, sector2, mainPart, isBorder, exceptionInfo);
      if(mainPart == "")
      {
        fwException_raise(exceptionInfo, "ERROR",
          "LhcVacCheckDevActPrivileges: main part doesn't exist for access zone <" +
          dpName + ">", "");
          return;
      }
      LhcVacDomainsOfMainPart(mainPart, domains, exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        return;
      }
    }
    else
    {
      LhcVacDeviceDomain(dpName, domains, exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        return;
      }
    }
    break;
  default:
    LhcVacDeviceDomain(dpName, domains, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return;
    }
    break;
  }
  // L.Kopylov 10.11.2011 Attribute Domain of device overrides domain(s) found above
  string attrValue;
  LhcVacGetAttribute(dpName, "Domain", attrValue);
  if(attrValue != "")
  {
    dynClear(domains);
    dynAppend(domains, attrValue);
  }

  if((privRequired < 1) || (privRequired > dynlen(glAllPrivileges)))
  {
    fwException_raise(exceptionInfo, "ERROR",
      "LhcVacCheckDevActPrivileges(): bad privilege value " + privRequired + " for DP type <" +
      actDpType + ">", "");
    return;
  }
  string privilegeName = glAllPrivileges[privRequired];
  if(myManType() == CTRL_MAN)
  {
    // CTRL is allowed to execute actions at operator privilege
    if(privRequired > 2)
    {
      fwException_raise(exceptionInfo, "ERROR",
        "LhcVacCheckDevActPrivileges(): privilege " + privilegeName + " is required", "");
    }
    else
    {
      result = true;
    }
    
    return;
  }
  string userName = LhcVacGetUserName();
  for(int n = dynlen(domains); n > 0; n--)
  {
    // Check if current privileges are enough, if not - display login dialog
    bool granted = false;
    if(userName != "")
    {
      fwAccessControl_checkUserPrivilege(userName, domains[n], privilegeName, granted, exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        return;
      }
    }
    if(!granted)
    {
      if(showDialog)
      {
        dyn_float dfReturn;
        dyn_string dsReturn;
        if(userName == "") {
          userName = "NO USER";
        }
        ChildPanelOnCentralModalReturn("vision/dialogs/Privilege.pnl", "Missing Privilege for Action",
          makeDynString("$1:" + "   Sorry, " + userName + " is NOT allowed to do this:\n privilege '" +
          privilegeName + "' in domain '" + domains[n] + "' is required."), dfReturn, dsReturn);
// L.Kopylov 06.07.2011       return;
      }
      return;  // L.Kopylov 06.07.2011
    }
  }
  result = true;
}

//LhcVacGetUserName:
/** Find user name for privilege check. The resulting name is either current OS
  user name (for all PVSS users except for vacadmin), or vacadmin if PVSS is vacadmin

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
*/
string LhcVacGetUserName()
{
  string userName = getUserName();
  /* L.Kopylov 20.06.2011
  if(strtoupper(userName) != "VACADMIN")
  {
    if(_WIN32)
    {
      string fullName, userDesc, dummyPass;
      dyn_string userGroup;
      string osUserName = "";
      for(int n = 0 ; n < 10 ; n++)
      {
        getCurrentOSUser(osUserName, fullName, userDesc, userGroup, dummyPass);
        if(osUserName != "")
        {
          userName = osUserName;
          break;
        }
        delay(0, 100);
      }
    }
    else
    {
      string osUserName;
      LhcVacLinuxUserName(osUserName);
      if(osUserName != "")
      {
        userName = osUserName;
      }
    }
  }
  */
  return userName;
}

//LhcVacUserHasMaxPrivilege:
/** Check if current user has MAXIMUM privilege in at least one domain

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
*/
bool LhcVacUserHasMaxPrivilege()
{
  return LhcVacUserHasPrivilege(dynlen(glAllPrivileges));
}

bool LhcVacUserHasPrivilege(int privLevel)
{
  if((privLevel < 1) || (privLevel > dynlen(glAllPrivileges)))
  {
    DebugTN("LhcVacUserHasPrivilege(): invalid privilege level " + privLevel);
    return false;
  }
  string userName = LhcVacGetUserName();
  if(userName == "")
  {
    return false;
  }
  dyn_string domains, longDomains, exceptionInfo;
  fwAccessControl_getAllDomains(domains, longDomains, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("LhcVacUserHasMaxPrivilege(): fwAccessControl_getAllDomains() failed:", exceptionInfo);
    return false;
  }
  string maxPrivilege = glAllPrivileges[privLevel];
  for(int n = dynlen(domains); n > 0; n--)
  {
    if((domains[n] + ":") == getSystemName())
    {
      continue;
    }
    if(domains[n] == "SYSTEM")
    {
      continue;
    }
    bool granted;
    fwAccessControl_checkUserPrivilege(userName, domains[n], maxPrivilege, granted, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("LhcVacUserHasMaxPrivilege(): fwAccessControl_checkUserPrivilege() failed:", exceptionInfo);
      return false;
    }
    if(granted)
    {
      return true;
    }
  }
  return false;
}
/**
  @brief Check if user has correct privilege for this datapoint
  @return User is granted
  @param[in] dpName                  the datapoint name to get the correct domains
  @param[in] requiredPrivilegeName   the privilege level name, "operator", "expert", "admin"
  @param[in] showDialog              popup message required in case not enough privilege
  @param[in] actMessage              descritption in popup of what is not allowed
  @param[in][out] exceptionInfo      exception Info
*/
bool LhcVacIsUserGranted (string dpName, string requiredPrivilegeName, bool showDialog, string actMessage, 
                          dyn_string exceptionInfo) {
  // get domains
  dyn_string domains;
  LhcVacDeviceDomain(dpName, domains, exceptionInfo);
  //DebugTN("domains : ", domains);
  // attribute Domain of device overrides domain(s) found above
  string attrValue;
  LhcVacGetAttribute(dpName, "Domain", attrValue);
  if(attrValue != "") {
    dynClear(domains);
    dynAppend(domains, attrValue);
  }
  // get user level privileges in domain
  string userName = LhcVacGetUserName();
  for(int n = dynlen(domains); n > 0; n--) {
    // Check if current privileges are enough
    bool granted = false;
    if(userName != "") {
      fwAccessControl_checkUserPrivilege(userName, domains[n], requiredPrivilegeName, granted, exceptionInfo);
      if(dynlen(exceptionInfo) > 0) {
        granted = false;
      }
    }
    if(!granted) {
      if(showDialog) {
        dyn_float dfReturn;
        dyn_string dsReturn;
        if(userName == "") {
          userName = "NO USER";
        }
        ChildPanelOnCentralModalReturn("vision/dialogs/Privilege.pnl", "Missing Privilege for Menu Action",
          makeDynString("$1:" + "   Sorry, " + userName + " is NOT allowed to " + actMessage + " '" +
          requiredPrivilegeName + "' in domain '" + domains[n] + "' is required."), dfReturn, dsReturn);
      }
    }
    if(granted) {
      return true;
    }
  }
  return false;
}














