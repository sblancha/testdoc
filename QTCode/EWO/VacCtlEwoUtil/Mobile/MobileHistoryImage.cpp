//	Implementation of MobileHistoryImage class
/////////////////////////////////////////////////////////////////////////////////

#include "MobileHistoryImage.h"

#include "VerticalAxisWidget.h"
#include "HistoryProcessor.h"
#include "History/PlotImage.h"

#include "VacMainView.h"
#include "DataPool.h"
#include "DebugCtl.h"

#include <QLayout>
#include <QPixmap>
#include <QLabel>
#include <QTimer>
#include <QMenu>
#include <QCursor>
#include <QMessageBox>
#include <QMouseEvent>
#include <QApplication>

#include <math.h>

#define ORIGINAL_DEPTH	(24 * 60)

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction


MobileHistoryImage::MobileHistoryImage(QWidget *parent) : QWidget(parent, 0)
{
	buildingImage = false;
	setMinimumWidth(500);
	setMinimumHeight(450);

	// Vertical layout: image + vertical axies on top, time axis on bottom
	QVBoxLayout *pMainLayout = new QVBoxLayout(this);
	pMainLayout->setContentsMargins(0, 0, 0, 0);
	pMainLayout->setSpacing(0);

	QHBoxLayout *pLayout = new QHBoxLayout();
	pLayout->setSpacing(0);
	pMainLayout->addLayout(pLayout, 10);	// Takes all available height

	pTempAxis = new VerticalAxisWidget(DataEnum::Temperature, this);
	pTempAxis->setLeftOfPlot(true);
	pLayout->addWidget(pTempAxis);
	connect(pTempAxis, SIGNAL(limitsChanged(bool)), this, SLOT(scaleChanged(bool)));
	
	pImage = new PlotImage(this);
	pLayout->addWidget(pImage, 10);	// Takes all available width
	connect(pImage, SIGNAL(resized(void)), this, SLOT(imageResized(void)));
	connect(pImage, SIGNAL(pointerMoved(int)), this, SLOT(imagePointerMoved(int)));
	connect(pImage, SIGNAL(zoomRectFinished(const QRect &)), this, SLOT(imageZoomFinished(const QRect &)));

	pPressureAxis = new VerticalAxisWidget(DataEnum::Pressure, this);
	pPressureAxis->setLeftOfPlot(false);
	pLayout->addWidget(pPressureAxis);
	connect(pPressureAxis, SIGNAL(limitsChanged(bool)), this, SLOT(scaleChanged(bool)));
	
	pStateAxis = new VerticalAxisWidget(DataEnum::Status, this);
	pStateAxis->setLeftOfPlot(false);
	pLayout->addWidget(pStateAxis);
	connect(pStateAxis, SIGNAL(limitsChanged(bool)), this, SLOT(scaleChanged(bool)));

	pTimeAxis = new TimeAxisWidget(this);
	pTimeAxis->show();
	pMainLayout->addWidget(pTimeAxis);
	connect(pTimeAxis, SIGNAL(limitsChanged(bool)), this, SLOT(scaleChanged(bool)));

	pLabel = new QLabel(this);
	QColor labelBg(255, 255, 204);
	QPalette palette = pLabel->palette();
	palette.setColor(QPalette::Window, labelBg);
	pLabel->setPalette(palette);
	pLabel->setAutoFillBackground(true);
	pLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	pLabel->hide();

	pCurrentEqp = NULL;

	online = true;
	depth = ORIGINAL_DEPTH;

	pTimer = new QTimer(this);
	pTimer->setSingleShot(true);
	connect(pTimer, SIGNAL(timeout(void)), this, SLOT(redrawTimerDone(void)));

	pAliveTimer = new QTimer(this);
	pAliveTimer->setSingleShot(false);
	connect(pAliveTimer, SIGNAL(timeout(void)), this, SLOT(aliveTimerDone(void)));
	pAliveTimer->start(1000);

	// Finally...
	rebuildImage();
}

MobileHistoryImage::~MobileHistoryImage()
{
	while(!eqps.isEmpty())
	{	
		delete eqps.takeFirst();
	}
}

/////////////////////////////////////////////////////////////////////////////////
// Event processing
/////////////////////////////////////////////////////////////////////////////////
void MobileHistoryImage::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

/*
**	FUNCTION
**		Add new device to history if it is not there yet
**
**	PARAMETERS
**		pEqp			- Pointer to device to be added
**		historyDepth	- History depth [sec] for this device
**		color			- Color to be used for new device
**
**	RETURNS
**		1	= device has been added,
**		0	= device is already in history
**
**	CAUTIONS
**		None
**
*/
MobileHistoryEqpData *MobileHistoryImage::addEqp(Eqp *pEqp, int historyDepth, const QColor &color)
{
	MobileHistoryEqpData *pItem;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		if(pItem->getEqp() == pEqp)
		{
			return pItem;	// Already exists
		}
	}
	QDateTime startTime = QDateTime::currentDateTime();
	QDateTime absStartTime(startTime.addSecs(-historyDepth));
	pItem = new MobileHistoryEqpData(pEqp, absStartTime);
	connect(pItem, SIGNAL(onlineValueArrived(void)), this, SLOT(onlineValueArrived(void)));
	eqps.append(pItem);
	pItem->setColor(color);

	// Set time limits for new device - this will force device to query archived
	// data for itself.
	setInitialLimitsForItem(pItem);

	// Finally force redrawing of image
	rebuildImage();
	return pItem;
}

/*
**	FUNCTION
**		Add new device to history if it is not there yet
**
**	PARAMETERS
**		pEqp			- Pointer to device to be added
**		channel			- Channel number in bakeout rack
**		setT			- true if temperature setting is required
**		historyDepth	- History depth [sec] for this device
**		color			- Color to be used for new device
**
**	RETURNS
**		1	= device has been added,
**		0	= device is already in history
**
**	CAUTIONS
**		None
**
*/
MobileHistoryEqpData *MobileHistoryImage::addEqp(Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType channelValueType, int historyDepth, const QColor &color)
{
	MobileHistoryEqpData *pItem;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		if ((pItem->getEqp() == pEqp) && (pItem->getChannel() == channel) && (pItem->getChannelValueType() == channelValueType))
		{
			return pItem;	// Already exists
		}
	}
	QDateTime startTime = QDateTime::currentDateTime();
	QDateTime absStartTime(startTime.addSecs(-historyDepth));
	pItem = new MobileHistoryEqpData(pEqp, absStartTime, channel, channelValueType);
	connect(pItem, SIGNAL(onlineValueArrived(void)), this, SLOT(onlineValueArrived(void)));
	eqps.append(pItem);
	pItem->setColor(color);

	// Set time limits for new device - this will force device to query archived
	// data for itself.
	setInitialLimitsForItem(pItem);

	// Finally force redrawing of image
	rebuildImage();
	return pItem;
}

/*
**	FUNCTION
**		Find maximum history depth [sec] for all devices in histort
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Maximum history depth
**
**	CAUTIONS
**		None
*/
int MobileHistoryImage::getMaximumDepth(void)
{
	int result = 0;
	QDateTime now(QDateTime::currentDateTime());
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		MobileHistoryEqpData *pItem = eqps.at(idx);
		int depth = pItem->getAbsStartTime().secsTo(now);
		if(depth > result)
		{
			result = depth;
		}
	}
	return result;
}

/*
**	FUNCTION
**		Set history to online mode
**
**	PARAMETERS
**		flag	- New online mode flag
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::setOnline(bool flag)
{
	online = flag;
	rebuildImage();
}

/*
**	FUNCTION
**		Set depth of history
**
**	PARAMETERS
**		value	- New history depth [minutes]
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::setDepth(int value)
{
	depth = value;
	QDateTime timeMin(pTimeAxis->getMin()),
		timeMax(pTimeAxis->getMax());
	if(online)	// always extend to the past
	{
		timeMin = timeMax.addSecs(-depth * 60);
	}
	else	// Extend to the future whenever possible
	{
		timeMax = timeMin.addSecs(depth * 60);
		QDateTime now(QDateTime::currentDateTime());
		if(timeMax > now)
		{
			timeMax = now;
			timeMin = timeMax.addSecs(-depth * 60);
		}
	}
	pTimeAxis->setLimits(timeMin, timeMax);

	// Notify all devices about time limits change so that will query for
	// archived data if necessary
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		MobileHistoryEqpData *pItem = eqps.at(idx);
		QDateTime endTime = timeMax > pItem->getCreated() ? pItem->getCreated() : timeMax;
		if(timeMin < endTime)
		{
			pItem->setTimeLimits(timeMin, endTime);
		}
	}

	// Finally redraw the image
	rebuildImage();
}

/*
**	FUNCTION
**		Set new time limits
**
**	PARAMETERS
**		start	- New start time
**		end		- New end time
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::setTimeLimits(const QDateTime &start, const QDateTime &end)
{
	pTimeAxis->setLimits(start, end);
	depth = start.secsTo(end) / 60;

	// Notify all devices about time limits change so that will query for
	// archived data if necessary
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		MobileHistoryEqpData *pItem = eqps.at(idx);
		QDateTime endTime = end > pItem->getCreated() ? pItem->getCreated() : end;
		if(start < endTime)
		{
			pItem->setTimeLimits(start, endTime);
		}
	}

	// Finally redraw the image
	rebuildImage();
}

/*
**	FUNCTION
**		Set initial time limits for history item in order to force item to
**		query data in 'initial' and current time range
**
**	PARAMETERS
**		pItem	- Pointer to history item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::setInitialLimitsForItem(MobileHistoryEqpData *pItem)
{
	QDateTime startTime(pItem->getCreated());
	QTime zeroTime(0, 0, 0);
	startTime.setTime(zeroTime);
	pItem->setTimeLimits(startTime, pItem->getCreated());

	// time from beginning of that date to default history depth
	QDateTime defaultStartTime(QDateTime::currentDateTime().addSecs(-ORIGINAL_DEPTH * 60));
	if(defaultStartTime < startTime)
	{
		defaultStartTime.setTime(zeroTime);
		pItem->setTimeLimits(defaultStartTime, startTime);
	}

	// ...then set currently displayed limits
	startTime = pTimeAxis->getMin();
	QDateTime endTime = pTimeAxis->getMax();
	if(endTime > pItem->getCreated())
	{
		endTime = pItem->getCreated();
	}
	if(startTime < endTime)
	{
		pItem->setTimeLimits(startTime, endTime);
	}
}

/*
**	FUNCTION
**		Add one archived value to given range of given DPE
**
**	PARAMETERS
**		id		- Range ID
**		dpe		- DPE name
**		time	- Value's timestamp
**		value	- Value from archive
**
**	RETURNS
**		true	- If such DPE has been found;
**		false	- DPE is not found, hence, further data for it are not needed
**
**	CAUTIONS
**		None
*/
bool MobileHistoryImage::addHistoryValue(int id, const char *dpe, const QDateTime &time, const QVariant &value)
{
	mutex.lock();
	if(pCurrentEqp)
	{
		if(pCurrentEqp->getDpeName() == dpe)
		{
			pCurrentEqp->addHistoryValue(id, time, value);
			mutex.unlock();
			return true;
		}
	}
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		pCurrentEqp = eqps.at(idx);
		if(pCurrentEqp->getDpeName() == dpe)
		{
			pCurrentEqp->addHistoryValue(id, time, value);
			mutex.unlock();
			return true;
		}
	}
	pCurrentEqp = NULL;
	mutex.unlock();
	return false;
}

/*
**	FUNCTION
**		Notify that all data for one range of one DPE have been added
**
**	PARAMETERS
**		id		- Range ID
**		dpe		- DPE name
**
**	RETURNS
**		true	- If such DPE has been found;
**		false	- DPE is not found, hence, further data for it are not needed
**
**	CAUTIONS
**		None
*/
bool MobileHistoryImage::finishHistoryRange(int id, const char *dpe)
{
	if(pCurrentEqp)
	{
		if(pCurrentEqp->getDpeName() == dpe)
		{
			pCurrentEqp->finishHistoryRange(id);
			return true;
		}
	}
	bool result = false;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		pCurrentEqp = eqps.at(idx);
		if(pCurrentEqp->getDpeName() == dpe)
		{
			pCurrentEqp->finishHistoryRange(id);
			result = true;
			break;
		}
	}
	if(!result)
	{
		pCurrentEqp = NULL;
	}
	return result;
}

/*
**	FUNCTION
**		Show label, displaying time range when dragging scroll bar - that
**		range will be set after scroll bar slider is released.
**
**	PARAMETERS
**		start	- New start time
**		end		- New end time
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::showNextRange(const QDateTime &start, const QDateTime &end)
{
	QString text = start.toString("yyyy-MM-dd hh:mm:ss");
	text += "   ";
	text += end.toString("yyyy-MM-dd hh:mm:ss");
	pLabel->setText(text);
	if(pLabel->isHidden())
	{
		pLabel->adjustSize();
		pLabel->move(width() / 2 - pLabel->width() / 2, height() - pLabel->height());
		pLabel->show();
	}
}

/*
**	FUNCTION
**		Hide label, displaying time range when dragging scroll bar.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::hideNextRange(void)
{
	pLabel->hide();
}

/*
**	FUNCTION
**		Find DP name of device with given index
**
**	PARAMETERS
**		index	- device index
**
**	RETURNS
**		Pointer to DP name; or
**		NULL in case of wrong index
**
**	CAUTIONS
**		None
*/
const char *MobileHistoryImage::getDpName(int index)
{
	MobileHistoryEqpData *pItem = eqps.at(index);
	if(pItem)
	{
		return pItem->getEqp()->getDpName();
	}
	return NULL;
}

/*
**	FUNCTION
**		Find index of given device in list of devices
**
**	PARAMETERS
**		pEqp	- Pointer to device to be found
**
**	RETURNS
**		Index of device in list; or
**		-1 if device not found
**
**	CAUTIONS
**		None
*/
int MobileHistoryImage::getEqpIndex(Eqp *pEqp)
{
	for(int index = 0 ; index < eqps.count() ; index++)
	{
		MobileHistoryEqpData *pItem = eqps.at(index);
		if(pItem->getEqp() == pEqp)
		{
			return index;
		}
	}
	return -1;
}

/*
**	FUNCTION
**		Find index of given bakeout rack in list of devices
**
**	PARAMETERS
**		pEqp	- Pointer to device to be found
**		channel	- Rack channel number
**		setT	- true if temperature setting is required
**
**	RETURNS
**		Index of device in list; or
**		-1 if device not found
**
**	CAUTIONS
**		None
*/
int MobileHistoryImage::getEqpIndex(Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType channelValueType)
{
	for(int index = 0 ; index < eqps.count() ; index++)
	{
		MobileHistoryEqpData *pItem = eqps.at(index);
		if ((pItem->getEqp() == pEqp) && (pItem->getChannel() == channel) && (pItem->getChannelValueType() == channelValueType))
		{
			return index;
		}
	}
	return -1;
}

/*
**	FUNCTION
**		Clear 'used' flag of all items
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::clearItemsUsed(void)
{
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		MobileHistoryEqpData *pItem = eqps.at(idx);
		pItem->setUsed(false);
	}
}

/*
**	FUNCTION
**		Set 'used' flag for item with given index
**
**	PARAMETERS
**		index	- Item index
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::setItemUsed(int index)
{
	mutex.lock();
	if((0 <= index) && (index < eqps.count()))
	{
		MobileHistoryEqpData *pItem = eqps.at(index);
		if(pItem)
		{
			pItem->setUsed(true);
		}
	}
	mutex.unlock();
}

/*
**	FUNCTION
**		Remove all items which have no 'used' flag set
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::removeUnusedItems(void)
{
	mutex.lock();
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		MobileHistoryEqpData *pItem = eqps.at(idx);
		if(!pItem->isUsed())
		{
			if(pCurrentEqp == pItem)
			{
				pCurrentEqp = NULL;
			}
			delete eqps.takeAt(idx);
		}
	}
	mutex.unlock();
}


/*
**	FUNCTION
**		Set absolute start history time for given device
**
**	PARAMETERS
**		pEqp	- Pointer to device
**		newTime	- New absolute start time
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::setAbsStartTime(Eqp *pEqp, const QDateTime &newTime)
{
	for(int index = 0 ; index < eqps.count() ; index++)
	{
		MobileHistoryEqpData *pItem = eqps.at(index);
		if(pItem->getEqp() == pEqp)
		{
			pItem->setAbsStartTime(newTime);

			// Set time limits for new device - this will force device to query archived
			// data for itself.
			setInitialLimitsForItem(pItem);
		}
	}
	rebuildImage();
}

/*
**	FUNCTION
**		Remove item with given index from list of devices
**
**	PARAMETERS
**		index	- device index
**
**	RETURNS
**		Pointer to device in item that will be removed
**
**	CAUTIONS
**		None
*/
const Eqp *MobileHistoryImage::removeItem(int index)
{
	mutex.lock();
	const Eqp *pEqp = NULL;
	if((0 <= index) && (index < eqps.count()))
	{
		MobileHistoryEqpData *pItem = eqps.at(index);
		if(pItem)
		{
			if(pCurrentEqp == pItem)
			{
				pCurrentEqp = NULL;
			}
			pEqp = pItem->getEqp();
			eqps.removeAt(index);
		}
	}
	mutex.unlock();
	return pEqp;
}

/*
**	FUNCTION
**		Remove all items from list of devices
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::removeAllItems(void)
{
	mutex.lock();
	while(!eqps.isEmpty())
	{
		delete eqps.takeFirst();
	}
	pCurrentEqp = NULL;
	mutex.unlock();
	rebuildImage();
}

/*
**	FUNCTION
**		Unselect all bakeout racks in graph
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::unselectAllRacks(void)
{
	for(int index = 0 ; index < eqps.count() ; index++)
	{
		MobileHistoryEqpData *pItem = eqps.at(index);
		pItem->setSelected(false);
	}
}

/*
**	FUNCTION
**		Select given bakeout rack channel in graph
**
**	PARAMETERS
**		pEqp	- Pointer to device to be selected
**		channel	- Channel number to be selected
**		setT	- Flag indicating if temperature setting shall be selected
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::selectEqp(Eqp *pEqpToSelect, int channel, enum BakeoutWorkDp::ChannelValueType channelValueType)
{
	// Unselect all pressure devices
	QHashIterator<QByteArray, Eqp *> eqpIter(DataPool::getInstance().getEqpDict());
	while(eqpIter.hasNext())
	{
		eqpIter.next();
		Eqp *pEqp = eqpIter.value();
		pEqp->setSelected(false, true);
	}
//qDebug("MobileHistoryImage::selectEqp(%s, %d, %s)\n", pEqpToSelect->getName(), channel, setT ? "true" : "false");
	// Select rack channel
	for(int index = 0 ; index < eqps.count() ; index++)
	{
		MobileHistoryEqpData *pItem = eqps.at(index);
		if ((pItem->getEqp() == pEqpToSelect) && (pItem->getChannel() == channel) && (pItem->getChannelValueType() == channelValueType))
		{
			pItem->setSelected(true);
//			qDebug("    FOUND\n");
		}
		else
		{
			pItem->setSelected(false);
		}
	}
}

/*
**	FUNCTION
**		Add all visible values to class that will export them to Excel file
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::addExportValues(HistoryProcessor &processor)
{
	QDateTime minTime = pTimeAxis->getMin();
	QDateTime maxTime = pTimeAxis->getMax();
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		MobileHistoryEqpData *pItem = eqps.at(idx);
		pItem->addExportValues(minTime, maxTime, processor);
	}
}

/*
**	FUNCTION
**		Slot activated when new online value arrived for one of devices.
**		Activate timer for redrawing the image, or redraw image immediately -
**		depending on how much time passed since last redraw
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::onlineValueArrived(void)
{
	if(online)
	{
		rebuildWithDelay();
	}
}

void MobileHistoryImage::rebuildWithDelay(void)
{
	pTimer->stop();
	int elapsed = drawTime.msecsTo(QTime::currentTime());
	drawTime = QTime::currentTime();

	if(elapsed < 0)	// wrap around 24 hours
	{
		rebuildImage();
	}
	else if(elapsed > 1000)
	{
		rebuildImage();
	}
	else
	{
		pTimer->start(300);
	}
}

/*
**	FUNCTION
**		Slot activated when redraw timeout expired
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::redrawTimerDone(void)
{
	rebuildImage();
}

/*
**	FUNCTION
**		Slot activated periodically by alive timer
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::aliveTimerDone(void)
{
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_UNIX
		printf("MobileHistoryImage::aliveTimerDone()\n");
		fflush(stdout);
#else
		qDebug("MobileHistoryImage::aliveTimerDone()\n");
	#endif
	}

	if(!online)
	{
		return;
	}
	int elapsed = drawTime.secsTo(QTime::currentTime());

	if(elapsed > 0)	// < 0 = wrap around 24 hours
	{
		QDateTime axisMax = pTimeAxis->getMax();
		int oldX, newX;
		pTimeAxis->timeToScreen(axisMax, oldX);
		pTimeAxis->timeToScreen(axisMax.addSecs(-elapsed), newX);
//		qDebug("elapsed %d oldX %d newX %d", elapsed, oldX, newX);
		if(oldX == newX)
		{
			return;
		}
	}
	else if(elapsed == 0)
	{
		return;
	}
//	qDebug("Redraw by alive timer");
	drawTime = QTime::currentTime();
	rebuildImage();
}

/*
**	FUNCTION
**		Build new history image, set image as background for this widget
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::rebuildImage(void)
{
	// To avoid recursive calls
	if(buildingImage)
	{
		return;
	}
	buildingImage = true;
	mutex.lock();
	pTimer->stop();
	if(online)
	{
		QDateTime now(QDateTime::currentDateTime());
		QDateTime start(now.addSecs(-60 * depth));
		pTimeAxis->setLimits(start, now);
	}
	QPixmap bgPixmap(pImage->size());
	QPalette pal = QApplication::palette();
	bgPixmap.fill(pal.color(QPalette::Window));
	QPainter painter(&bgPixmap);
	draw(painter);
	mutex.unlock();
	buildingImage = false;
	pal.setBrush(QPalette::Window, bgPixmap);
	pImage->setPalette(pal);

	emit imageUpdated();
}

/*
**	FUNCTION
**		Redraw the whole graph area.
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::draw(QPainter &painter)
{
	if(eqps.count())
	{
		calculateVerticalLimits(pPressureAxis);
		calculateVerticalLimits(pTempAxis);
		calculateVerticalLimits(pStateAxis);
	}

	calculateAxisActivity();
	drawVerticalAxies(painter);
	drawTimeAxis(painter);

	showValueAtPointer();

	// Draw data
	// First draw all devices except for selected one(s)
	MobileHistoryEqpData *pDevice;
	int idx;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pDevice = eqps.at(idx);
		if(pDevice->isSelected())
		{
//qDebug("MobileHistoryImage::draw(%s, %d, %s) is selected\n", pDevice->getEqp()->getName(), pDevice->getChannel(), pDevice->isSetT() ? "true" : "false");
			continue;
		}
		switch(pDevice->getEqp()->getMainValueType())
		{
		case DataEnum::Pressure:
			pDevice->draw(painter, pTimeAxis, pPressureAxis);
			break;
		default:	// Here are 2 variants again: either temperature or status
			switch (pDevice->getChannelValueType()) {
			case BakeoutWorkDp::ChannelValueTypeStatus:
				pDevice->draw(painter, pTimeAxis, pStateAxis);
				break;
			default:
				pDevice->draw(painter, pTimeAxis, pTempAxis);
				break;
			}
			break;
		}
	}

	// Then draw selected devices - thus they will always appear on top of non-selected
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pDevice = eqps.at(idx);
		if(!pDevice->isSelected())
		{
			continue;
		}
		switch(pDevice->getEqp()->getMainValueType())
		{
		case DataEnum::Pressure:
			pDevice->draw(painter, pTimeAxis, pPressureAxis);
			break;
		default:	// Here are 2 variants again: either temperature or status
			switch (pDevice->getChannelValueType()) {
			case BakeoutWorkDp::ChannelValueTypeStatus:
				pDevice->draw(painter, pTimeAxis, pStateAxis);
				break;
			default:
				pDevice->draw(painter, pTimeAxis, pTempAxis);
				break;
			}
			break;
		}
	}

	// Draw graph area border
	painter.setPen(VacMainView::getGraphAxisColor());
	painter.drawRect(0, 0, pImage->width() - 1, pImage->height() - 1);

	if(DebugCtl::isHistory())
	{
		const char *fileName;
		#ifdef Q_OS_WIN
			fileName = "C:\\MobileHistoryView.txt";
		#else
			fileName = "/home/kopylov/MobileHistoryView.txt";
		#endif
		dump(fileName);
	}
}

/*
**	FUNCTION
**		Calculate limits for vertical axis when graph with
**		values is drawn first time.
**		The idea is:
**		graph shall appear with vertical limits set 'correctly'
**		for the data it displays, but later it works in
**		'manual vertical scale' mode.
**		The requirement came from the fact that, for example,
**		range of value for VGM and VGI are VERY different, and
**		it is not convenient if graph is always opened with the
**		same vertical limits.
**
**	PARAMETERS
**		valueType	- Type of values to check, every value type uses own axis
**		limitReady	- Flag indicating if limits for this value type are ready
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MobileHistoryImage::calculateVerticalLimits(VerticalAxisWidget *pAxis)
{
	if(pAxis->isInitialLimitsSet())
	{
		return;
	}

	int valueType = pAxis->getValueType();
	float	min = HISTORY_HIGH_VALUE, max = HISTORY_LOW_VALUE;
	int		maxValues = 0;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		MobileHistoryEqpData *pDevice = eqps.at(idx);
		switch(valueType)
		{
		case DataEnum::Pressure:
			if(pDevice->getEqp()->getMainValueType() != valueType)
			{
				continue;
			}
			break;
		default:
			if(pDevice->getEqp()->getMainValueType() == DataEnum::Pressure)
			{
				continue;
			}
			break;
		}						
		float eqpMin, eqpMax;
		int nValues = pDevice->findLimits(eqpMin, eqpMax);
		if(nValues > maxValues)
		{
			maxValues = nValues;
		}
		if(eqpMin < min)
		{
			min = eqpMin;
		}
		if(eqpMax > max)
		{
			max = eqpMax;
		}
	}
	if(min > max)
	{
		return;
	}
	if(min <= 0.0)
	{
		if(valueType == DataEnum::Temperature)
		{
			min = (float)1.0;
		}
		else
		{
			return;
		}
	}
	if(valueType == DataEnum::Temperature)
	{
		min = 0;
	}
	pAxis->setInitialLimits(min, max, maxValues);
}

void MobileHistoryImage::calculateAxisActivity(void)
{
	HistoryEqpData *pSelected = NULL;
	int idx;
	for(idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		HistoryEqpData *pDevice = eqps.at(idx);
		if(pDevice->isSelected())
		{
			pSelected = pDevice;
			break;
		}
	}
	if(pSelected)
	{
		if (pSelected->getValueType() == DataEnum::Pressure) {
			pTempAxis->setActive(false);
			pStateAxis->setActive(false);
			pPressureAxis->setActive(true);
		}
		else if (pSelected->getValueType() == DataEnum::Temperature) {
			pPressureAxis->setActive(false);
			pStateAxis->setActive(false);
			pTempAxis->setActive(true);
		}
		else if (pSelected->getValueType() == DataEnum::Status) {
			pPressureAxis->setActive(false);
			pTempAxis->setActive(false);
			pStateAxis->setActive(true);
		}
		else {	// Though this shall not happen
			pPressureAxis->setActive(false);
			pTempAxis->setActive(false);
			pStateAxis->setActive(false);
		}
	}
	else
	{
		pPressureAxis->setActive(false);
		pStateAxis->setActive(false);
		pTempAxis->setActive(true);
	}
}

/*
**	FUNCTION
**		Draw all vertical axies, claculate graph area for time axis
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		Graph area for time axis
**
**	CAUTIONS
**		None
**
*/
void MobileHistoryImage::drawVerticalAxies(QPainter &painter)
{
	QRect plotRect = pImage->rect();

	pTempAxis->drawOnPlot(painter, plotRect);
	pPressureAxis->drawOnPlot(painter, plotRect);
	pStateAxis->drawOnPlot(painter, plotRect);
}

/*
**	FUNCTION
**		Draw time axis
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		areaRect	- Graph area for time axis
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MobileHistoryImage::drawTimeAxis(QPainter &painter)
{
	pTimeAxis->setGraphArea(pImage->geometry());
	pTimeAxis->setGrid(true);
	pTimeAxis->setGridStyle(Qt::SolidLine);
	pTimeAxis->setTickSize(4);
	pTimeAxis->setTickOutside(false);
	pTimeAxis->update();
	pTimeAxis->drawOnPlotArea(painter);
}

void MobileHistoryImage::showValueAtPointer(void)
{
	QDateTime	time;
	QString		timeString;
	time = pTimeAxis->screenToTime(pImage->getPointerLocation(), timeString);

	// Find selected device
	MobileHistoryEqpData *pData = NULL;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		if(eqps.at(idx)->isSelected())
		{
			pData = eqps.at(idx);
			break;
		}
	}
	if(!pData)
	{
		emit valueAtPointerChanged(timeString.toLatin1(), NULL);
		return;
	}
	float value;
	if(!pData->getValueAt(time, value))
	{
		emit valueAtPointerChanged(timeString.toLatin1(), NULL);
		return;
	}
	char buf[256];
	switch(pData->getValueType())
	{
	case DataEnum::Pressure:
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%8.2E", value);
#else
		sprintf(buf, "%8.2E", value);
#endif
		break;
	case DataEnum::Status:
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d", (int)value);
#else
		sprintf(buf, "%d", (int)value);
#endif
		break;
	default:	// 3rd variant is temperature only
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%8.2f", value);
#else
		sprintf(buf, "%8.2f", value);
#endif
		break;
	}
	emit valueAtPointerChanged(timeString.toLatin1(), buf);
}

/*
**	FUNCTION
**		Set time scales according to current zoom rectangle
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MobileHistoryImage::setZoomingTime(const QRect &zoomRect)
{
	if(online)
	{
		bool	switchToOffline = false;
		if(zoomRect.right() < (pTimeAxis->getScaleRight() - 10))	// Switch to offline in order to exactly match zoom rectangle
		{
			int answer = QMessageBox::question(this, "History mode switch",
				"Switch to OFFLINE history mode?", QMessageBox::Yes,
				QMessageBox::No | QMessageBox::Default | QMessageBox::Escape);
			if(answer == QMessageBox::Yes)
			{
				switchToOffline = true;
			}
		}
		if(switchToOffline)
		{
			online = false;
			QString label;
			QDateTime startTime = pTimeAxis->screenToTime(zoomRect.left(), label);
			QDateTime endTime = pTimeAxis->screenToTime(zoomRect.right(), label);
			pTimeAxis->setLimits(startTime, endTime);
		}
		else
		{
			QString label;
			QDateTime startTime = pTimeAxis->screenToTime(zoomRect.left(), label);
			int newDepth = startTime.secsTo(QDateTime::currentDateTime());
			newDepth /= 60;
			if(newDepth < 1)
			{
				newDepth = 1;
			}
			depth = newDepth;
		}
	}
	else
	{
		QString label;
		QDateTime startTime = pTimeAxis->screenToTime(zoomRect.left(), label);
		QDateTime endTime = pTimeAxis->screenToTime(zoomRect.right(), label);
		pTimeAxis->setLimits(startTime, endTime);
	}
}

/*
**	FUNCTION
**		Set vertical scale according to current zoom rectangle.
**		Zooming operation updates only ONE vertical axis of all.
**		Decision on which axis shall be updated is taken using the
**		following algorithm:
**		1) If only one axis is visible - that axis is updated
**		2) If more than one axis is visible - decision is taken based
**			on LAST selected device
**
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void MobileHistoryImage::setZoomingVertical(const QRect &zoomRect)
{
	if(pPressureAxis->isActive()) {
		pPressureAxis->setZooming(zoomRect);
	}
	else if (pStateAxis->isActive()) {
		pStateAxis->setZooming(zoomRect);
	}
	else {
		pTempAxis->setZooming(zoomRect);
	}
}

void MobileHistoryImage::imageResized(void)
{
	onlineValueArrived();	// To force redrawing, probably with delay
}

void MobileHistoryImage::imagePointerMoved(int /* location */)
{
	showValueAtPointer();
}

void MobileHistoryImage::imageZoomFinished(const QRect &zoomRect)
{
	if((!zoomRect.width()) || (!zoomRect.height()))
	{
		return;
	}

	setZoomingTime(zoomRect);
	setZoomingVertical(zoomRect);
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("MobileHistoryImage::imageZoomFinished()\n");
#else
		printf("MobileHistoryImage::imageZoomFinished()\n");
		fflush(stdout);
#endif
	}
	rebuildImage();
	emit scalesChanged();
}

void MobileHistoryImage::scaleChanged(bool dragging)
{
	if(dragging)
	{
		return;
	}
	onlineValueArrived();	// To force redrawing, probably with delay
}


/*
**	FUNCTION
**		Dump history image parameters to output file for debugging
**
**	PARAMETERS
**		fileName	- Name of output file
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryImage::dump(const char *fileName)
{
	if(!DebugCtl::isHistory())
	{
		return;
	}
	FILE	*pFile;
#ifdef Q_OS_WIN
	if(fopen_s(&pFile, fileName, "w"))
	{
		return;
	}
#else
	if(!(pFile = fopen(fileName, "w")))
	{
		return;
	}
#endif
	fprintf(pFile, "WIDTH %d HEIGHT %d\n", width(), height());
	fprintf(pFile, "============ Time axis ===================\n");
	pTimeAxis->dump(pFile);
	fprintf(pFile, "============ Pressure axis ===================\n");
	pPressureAxis->dump(pFile);
	fprintf(pFile, "============ Temperature axis ===================\n");
	pTempAxis->dump(pFile);
	fprintf(pFile, "============ State axis ===================\n");
	pStateAxis->dump(pFile);
	fclose(pFile);
}


