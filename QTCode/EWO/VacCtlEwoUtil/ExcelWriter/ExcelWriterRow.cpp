//	Implementation of ExcelWriterRow class
/////////////////////////////////////////////////////////////////////////////////

#include "ExcelWriterRow.h"

#include <errno.h>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

ExcelWriterRow::ExcelWriterRow(int row)
{
	this->row = row;
}

ExcelWriterRow::~ExcelWriterRow()
{
	while(!cells.isEmpty())
	{
		delete cells.takeFirst();
	}
}

/*
**	FUNCTION
**		Write row and all cells in it to output file
**
**	ARGUMENTS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
*		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriterRow::write(FILE *pFile)
{
	if(!pFile)
	{
		return QString();
	}
	int coco = fprintf(pFile, "   <Row>\n");
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	for(int col = 0 ; col < cells.count() ; col++)
	{
		ExcelWriterCell *pCell = cells.at(col);
		QString error = pCell->write(pFile);
		if(!error.isEmpty())
		{
			return error;
		}
	}
	coco = fprintf(pFile, "   </Row>\n");
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	return QString();
}

/*
**	FUNCTION
**		Write row and all cells in it to output CSV file
**
**	ARGUMENTS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
*		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriterRow::writeCsv(FILE *pFile)
{
	if(!pFile)
	{
		return QString("No open file");
	}
	for(int col = 0 ; col < cells.count() ; col++)
	{
		ExcelWriterCell *pCell = cells.at(col);
		QString error = pCell->writeCsv(pFile, col == 0);
		if(!error.isEmpty())
		{
			return error;
		}
	}
	int coco = fprintf(pFile, "\n");
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}	
	return QString();
}

/*
**	FUNCTION
**		Set format for cell with given column number
**
**	ARGUMENTS
**		column	- Column number of cell
**		format	- format to be set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterRow::setCellFormat(int column, ExcelWriterCell::Format format)
{
	ExcelWriterCell *pCell = findCell(column);
	if(pCell)
	{
		pCell->setFormat(format);
	}
}

/*
**	FUNCTION
**		Set color for cell with given column number
**
**	ARGUMENTS
**		column	- Column number of cell
**		color	- color to be set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterRow::setCellColor(int column, const QColor &color)
{
	ExcelWriterCell *pCell = findCell(column);
	if(pCell)
	{
		pCell->setColor(color);
	}
}

/*
**	FUNCTION
**		Set string to be shown for cell with given column number
**
**	ARGUMENTS
**		column	- Column number of cell
**		string	- string to be set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterRow::setCellValue(int column, const QString &string)
{
	ExcelWriterCell *pCell = findCell(column);
	if(pCell)
	{
		pCell->setString(string);
	}
}

/*
**	FUNCTION
**		Set date+time to be shown for cell with given column number
**
**	ARGUMENTS
**		column	- Column number of cell
**		dateTime	- date+time to be set
**		withMsec	- Flag indicating if milliseconds shall appear in output
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterRow::setCellValue(int column, const QDateTime &dateTime, bool withMsec)
{
	ExcelWriterCell *pCell = findCell(column);
	if(pCell)
	{
		if(withMsec)
		{
			pCell->setDateTimeWithMsec(dateTime);
		}
		else
		{
			pCell->setDateTime(dateTime);
		}
	}
}

/*
**	FUNCTION
**		Set floating point value to be shown for cell with given column number
**
**	ARGUMENTS
**		column	- Column number of cell
**		value	- value to be set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterRow::setCellValue(int column, float value)
{
	ExcelWriterCell *pCell = findCell(column);
	if(pCell)
	{
		pCell->setValue(value);
	}
}

/*
**	FUNCTION
**		Set integer value to be shown for cell with given column number
**
**	ARGUMENTS
**		column	- Column number of cell
**		value	- value to be set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterRow::setCellValue(int column, int value)
{
	ExcelWriterCell *pCell = findCell(column);
	if(pCell)
	{
		pCell->setIntValue(value);
	}
}

/*
**	FUNCTION
**		Set comment to be shown for cell with given column number
**
**	ARGUMENTS
**		column	- Column number of cell
**		comment	- comment to be set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterRow::setCellComment(int column, const QString &comment)
{
	ExcelWriterCell *pCell = findCell(column);
	if(pCell)
	{
		pCell->setComment(comment);
	}
}

/*
**	FUNCTION
**		Return all 'non-default' formats for all cells in this row.
**		'Non-default' means:
**		- either color is not white
**		- or format is Float
**
**	ARGUMENTS
**		formats	- Variable where all formats will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterRow::getCellFormats(QList<ExcelCellFormat *> &formats)
{
	for(int col = 0 ; col < cells.count() ; col++)
	{
		ExcelWriterCell *pCell = cells.at(col);
		ExcelCellFormat cellFormat = pCell->getFormat();
		if(cellFormat.getColor() == Qt::white)
		{
			if(cellFormat.getFormat() == ExcelWriterCell::String)
			{
				continue;
			}
		}
		bool found = false;
		for(int idx = 0 ; idx < formats.count() ; idx++)
		{
			ExcelCellFormat *pFormat = formats.at(idx);
			if((cellFormat.getColor() == pFormat->getColor()) &&
				(cellFormat.getFormat() == pFormat->getFormat()))
			{
				found = true;
				break;
			}
		}
		if(!found)
		{
			formats.append(new ExcelCellFormat(cellFormat));
		}
	}
}

/*
**	FUNCTION
**		Set style index for all cells with given color
**
**	ARGUMENTS
**		color	- Cell color to find
**		index	- Style index for this color
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterRow::setCellStyleIndex(const ExcelCellFormat &format, int index)
{
	for(int col = 0 ; col < cells.count() ; col++)
	{
		ExcelWriterCell *pCell = cells.at(col);
		if(pCell->getFormat() == format)
		{
			pCell->setStyleIndex(index);
		}
	}
}

/*
**	FUNCTION
**		Find cell with given column number. If not found - create
**		new cell and add it to list
**
**	ARGUMENTS
**		column	- Column number to find
**
**	RETURNS
**		Pointer to cell with given column number; or
**		NULL if column number is invalid (must be 1...255)
**
**	CAUTIONS
**		None
*/
ExcelWriterCell *ExcelWriterRow::findCell(int column)
{
	if((column < 1) || (column > 255))
	{
		return NULL;
	}
	ExcelWriterCell *pCell;
	for(int col = 0 ; col < cells.count() ; col++)
	{
		pCell = cells.at(col);
		if(pCell->getColumn() == column)
		{
			return pCell;
		}
	}
	pCell = new ExcelWriterCell(column);
	cells.append(pCell);
	return pCell;
}
