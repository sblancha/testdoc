#ifndef	FUNCTIONALTYPEVPI_H
#define	FUNCTIONALTYPEVPI_H

// Special functional type for VPI - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVPI : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Off = 1,
		On = 2
	};

	FunctionalTypeVPI(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVPI() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVPI_H
