  #uses "vclDevCommon.ctl"
  #uses "CtrlRDBAccess"
  #uses "VacCtlEqpDataPvss"

  /*
  *  Pressure interface for vacmon Script - vcsVacmonDailyPressures.ctl
  *  Purpose: This script sends pressure related data to the vacmon DB so that it can be displayed to the end user 
  *           It sends the current pressure, and the maximum and minimum in the last 24 hours.   
  *  @Author - Andre Manuel Paiva e Rocha (andre.rocha@cern.ch)
  *  @Copyright - CERN 2017
  *
  *  @param machine The machine name to be sent along with the pressures
  *
  */

  // Constants

  // Database settings
  const string DRIVER = "QOCI";
  const string DATABASE = "cerndb1";
  const string USER = "vacmon";
  const string PASSW = "vacmon2017ICM";

  // Main state bits masks to be analyzed
  const uint RR1_PRVALID = 0x00040000u;

  // Debug
  const bool TEST_MODE = false;


  /*
  **  FUNCTION
  **		Insert pressure of the equipment in the vacmon DB.
  **  ARGUMENTS
  **    string - Connection name represented by a datapoint type.
  **    mapping - Values to insert into DB. They are: displayName, dcum, currentPressure, maxPressure, minPressure, machine, mainPart and sector.
  **  RETURNS
  **		Boolean
  **  CAUTIONS
  **		None
  */
  bool insertPressureValDB(string connectionName, mapping values) {
    string errTxt;
    dbCommand cmd;
    string cmdText;  
  
    cmdText = "INSERT INTO VACMON_PRESSURES (DEVICEDISPLAYNAME, MAXPRESSURE, MINPRESSURE, CURRENTPRESSURE, DEVICETIMESTAMP, DCUM, MACHINE, MAINPART, SECTOR) VALUES(" +
              ":displayName,:maxPressure,:minPressure,:currentPressure,:currentTime,:dcum,:machine,:mainPart,:sector)";
  
    fwDbStartCommand(connectionName, cmdText, cmd);  
    if (fwDbCheckError(errTxt, cmd)){DebugTN("vcsVacmonDailyPressures.ctl: DB Error",errTxt);return false;};
 
    fwDbBindParams(cmd, values);
    if (fwDbCheckError(errTxt, cmd)){DebugTN("vcsVacmonDailyPressures.ctl: DB Error",errTxt);return false;};
  
    fwDbExecuteCommand(cmd);
    if (fwDbCheckError(errTxt, cmd)){DebugTN("vcsVacmonDailyPressures.ctl: DB Error",errTxt);return false;};
      
    fwDbFinishCommand(cmd);
    if (fwDbCheckError(errTxt)){DebugTN("vcsVacmonDailyPressures.ctl: DB Error",errTxt);return false;};
  
    return true;
  
  }


  /*
  **  FUNCTION
  **		Everytime this function runs, max and min current values of pressure
  **    are sent to the vacmon database.
  **  ARGUMENTS
  **    string - A datapoint type.
  **  RETURNS
  **		None
  **  CAUTIONS
  **		None
  */
  processEqp(string dpPattern) {
    dyn_string allDPs, exceptionInfo;
    string dpName, displayName;
    dyn_errClass err;
    mapping values;
    time currentTime, last24hTime;
    string stringCurrentTime, stringLast24hTime;
    int validPressures, notValidPressures;
  
  	//Initialize DB connection
    if (!TEST_MODE) {
      if(!setDBConnection(dpPattern, DATABASE, USER, PASSW, DRIVER)) {
        DebugFN("level1", "vcsVacmonDailyPressures.ctl: Could not establish connection with the database - Exiting program");
        return; // Initialization of DB access parameters 
      }  
    }
    
    //Get all DPs from pattern
    allDPs = dpNames(dpPattern + "*");
    
    for(uint i = 1; i< dynlen(allDPs); i++) {
      string dpName = allDPs[i];
      string dpNameMaster = dpName;
      string dpSubStringName = dpSubStr(dpName, DPSUB_DP);
      string dpSystemDetail = dpSubStr(dpName, DPSUB_SYS);
      string masterDp = getRR1(dpSubStringName);  
      
      DebugFN("level1", "vcsVacmonDailyPressures.ctl dpName: ", dpName);
      
      if (masterDp != "") {
        dpNameMaster = dpSystemDetail + masterDp;
      }
      DebugFN("level1", "vcsVacmonDailyPressures.ctl dpNameMaster: ", dpName);
      
      uint RR1;
      dpGet(dpNameMaster + ".RR1", RR1);
      DebugFN("level1", "vcsVacmonDailyPressures.ctl PR is Valid: ", checkPRValidStatus(RR1));
    	if (checkPRValidStatus(RR1)) {
        validPressures++;
    		//Get the display name
    		LhcVacDisplayName(dpSubStringName, displayName, exceptionInfo);
    		if(dynlen(exceptionInfo) > 0) {
    		  DebugTN("vcsVacmonDailyPressures.ctl:", exceptionInfo);
    		}
    		values["displayName"] = displayName;
		
    		//Get the DCUM
    		float dcum;
    		LhcVacDeviceLocation(dpSubStringName, dcum, exceptionInfo);
    		if(dynlen(exceptionInfo) > 0) {
    		  DebugTN("vcsVacmonDailyPressures.ctl:", exceptionInfo);
    		}
    		values["dcum"] = dcum;

    		//Current Time
    		currentTime = getCurrentTime();
    		last24hTime = currentTime - 60*60*24; 
    		values["currentTime"] = currentTime;
	  
    		//Get current pressure    
    		float pressure;
    		dpGet(dpName + ".PR", pressure);
    		err = getLastError();
    		if(dynlen(err) > 0) {
    			DebugTN("vcsVacmonDailyPressures.ctl: " + err);      
    		}
	   
    		values["currentPressure"] = pressure;
		
    		dyn_float allPressures;       //Contains the values
    		dyn_time t;          //Contains the source times of the values
    		int ant;             //The function returns -1 in case of errors
    		string gaugeName = dpSubStringName + ".PR";
    		//DebugN("GaugeName: " + gaugeName);
	 
    		dpGetPeriod(last24hTime, currentTime, 0, gaugeName, allPressures, t); 
    		err = getLastError();
    		if(dynlen(err) > 0) {
    			DebugTN("vcsVacmonDailyPressures.ctl: " + err);      
    		}
		
    		if (dynlen(allPressures) == 0) /* Is executed if a query error occurs or no values exist */
    		{
    		  values["minPressure"] = pressure;
    		  values["maxPressure"] = pressure;
    		  //DebugTN("vcsVacmonDailyPressures.ctl: dpGetPeriod caused an error or no values exist");
    		}
    		else {
    		  values["minPressure"] = dynMin(allPressures);
    		  values["maxPressure"] = dynMax(allPressures);
    		}   

    		//TODO: Get maximum and minimum pressure values using RDB
    	//     string query = "SELECT '_original.._value' FROM '" + dpSubStringName + ".PR" + "' TIMERANGE("; 
    	//     string query = "SELECT 'MAX(_original.._value)','MIN(_original.._value)' FROM '" + dpSubStringName + ".PR" + "' TIMERANGE("; 
    	//     string stringLast24hTime = formatTime("%Y.%m.%d %H:%M:%S", last24hTime);
    	//     query += "\"" + stringLast24hTime + "\",";
    	//     string stringCurrentTime = formatTime("%Y.%m.%d %H:%M:%S", getCurrentTime());
    	//     query += "\"" + stringCurrentTime + "\",1,0)";
    	//     
    	//     dyn_dyn_anytype result;
    	//     dpQuery(query, result);
    	// 
    	//     DebugFN("level1", "Result size: " + dynlen(result));
    	//     
    	//     dyn_float allPressures;
    	//     for (i = 2; i <= dynlen(result); i++) {
    	//           allPressures = result[i][2];
    	//     }
    	// 
			

    		//Get machine
    		values["machine"] = glAccelerator;
		
    		//Get mainpart and sector
    		int vac;
    		string sector1, sector2, mainPart;
    		bool isBorder;
    		LhcVacDeviceVacLocation(dpSubStringName, vac, sector1, sector2, mainPart, isBorder, exceptionInfo);
    		if(dynlen(exceptionInfo) > 0) {
    		  DebugTN("vcsVacmonDailyPressures.ctl:", exceptionInfo);
    		}
    		values["mainPart"] = mainPart;
        values["sector"] = sector1;
		
    		DebugFN("level1", "Values is: ", values);    
		
    		//Send values to the vacmon DB    
        if (!TEST_MODE) {
    		 if(!insertPressureValDB(dpPattern, values)) {
    		   DebugTN("vcsVacmonDailyPressures.ctl: The following value could not be sent to the database => " + values);    
    		 }
        }
    	}
      else {
        notValidPressures++;
      }
      
    }
    
    DebugTN("Valid Pressures: " + validPressures);
    DebugTN("Not Valid Pressures: " + notValidPressures);
    
    //Close connection
    fwDbCloseConnection(dpPattern);
  }


  /*
  **	FUNCTION
  **		Everytime this function runs, the max, min a current values of pressure
  **    are sent to the vacmon database
  **	ARGUMENTS
  **    	Array of strings with all datapoint patterns (to be converted in ex: VG*).
  **  RETURNS
  **		int
  **  CAUTIONS
  **		None
  */
  int main(dyn_string dpPatterns) {
  
    DebugTN("Starting vacmon daily pressures script vcsVacmonDailyPressures.ctl");
  
    InitStaticData();
   
    if(glAccelerator == "") {
      DebugTN("vcsVacmonDailyPressures.ctl: machine not properly configured. Exiting..");
      return -1; 
    }
  
    // Due to a problem in the RDB API (only ON in the LHC project) we need to read
    // from the file archiving in LHC
  
  //   if(glAccelerator == "LHC") {
  //     setQueryRDBDirect(false);
  //   }
  //   else {
  //     setQueryRDBDirect(true);
  //   }
  
    setQueryRDBDirect(false);
    
    if (dynlen(dpPatterns) == 0) {
      DebugTN("vcsVacmonDailyPressures.ctl: No arguments. Need at least one device. Exiting..");
      return -1; 
    }
  
    dyn_string dpPatternsArray = strsplit(dpPatterns,",");

    //DebugTN(dpPatternsArray);
     for(int i=1; i <= dynlen(dpPatternsArray); i++) {
       startThread("processEqp", dpPatternsArray[i]);
     }
  
  }


  /*
  **	FUNCTION
  **		Everytime this function runs, the max, min a current values of pressure
  **    	are sent to the vacmon database
  **	ARGUMENTS
  **    	None
  **  RETURNS
  **		true/false
  **  CAUTIONS
  **		None
  */
  bool InitStaticData()
  {
  	dyn_string	exceptionInfo;
  	long		coco;
  	bool		isLhcData;
  	// Set accelerator name to be used by other components
  	LhcVacGetMachineId( exceptionInfo );
  	if( dynlen( exceptionInfo ) > 0 )
  	{
  		DebugTN( "vcsVacmonDailyPressures.ctl: Failed to read machine name: " + exceptionInfo );
  		return false;
  	}
  	DebugTN( "vcsVacmonDailyPressures.ctl: Machine is " + glAccelerator );
  	VacResourcesParseFile( PROJ_PATH + "/data/VacResources.txt", glAccelerator, exceptionInfo );
  	isLhcData = glAccelerator == "LHC" ? true : false;
  	// Initalize passive machine data
  	coco = LhcVacEqpInitPassive( isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo );
  	if( coco < 0 )
  	{
  		DebugTN( "vcsVacmonDailyPressures.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo );
  		return false;
  	}
  	DebugTN( "vcsVacmonDailyPressures.ctl: LhcVacEqpInitPassive() done" );

  	// Initialzie active equipment information
  	coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
  			exceptionInfo );
  	if( coco < 0 )
  	{
  		DebugTN( "vcsVacmonDailyPressures.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo );
  		return false;
  	}
  	DebugTN( "vcsVacmonDailyPressures.ctl: LhcVacEqpInitActive() done" );

  	DebugTN( "vcsVacmonDailyPressures.ctl: InitStaticData() done" );
  	LhcVacInitDevFunc();
  	return true;
  }

  /*
  **	FUNCTION
  **		Sets the DB connection
  **	ARGUMENTS
  **		connectionName(string) - by default defined in the top of the file
  **    	database(string) - by default defined in the top of the file
  **    	username(string) - by default defined in the top of the file
  **    	password(string) - by default defined in the top of the file
  **    	driver(string) - by default defined in the top of the file
  **  RETURNS
  **		true/false
  **  CAUTIONS
  **		None
  */
  bool setDBConnection(string connectionName, string database, string username, string password, string driver){
  
  
    //DebugTN("The parameters to start the connection are : " + connectionName + " " + database + " " + username + " " + password + " " + driver);

    //check if CtrlRDBAccess library was installed and loaded
    if (!isFunctionDefined("fwDbOption")) {
       DebugTN("vcsVacmonDailyPressures.ctl ERROR: CtrlRDBAccess library is not available. Check your installation and config file");
       return -1;
    }
    
    string errTxt;
    dbConnection dbCon;

    // check if connection with that name already present
    int rc = fwDbGetConnection(connectionName, dbCon);
  
    if (rc == -1){
      fwDbCheckError(errTxt,dbCon);
      DebugTN("vcsVacmonDailyPressures.ctl ERROR Getting the connection", errTxt);
      return false;
    } 
    else if (rc == 1) {
      // connection with such name does not exist yet. Create it
      string connectString = "Driver=" + driver + ";Database=" + database + ";User=" + username + ";Password=" + password + ";";
    
      fwDbOpenConnection(connectString, dbCon,connectionName);
 
      if (fwDbCheckError(errTxt,dbCon)){
        DebugTN("vcsVacmonDailyPressures.ctl ERROR WHILE OPENING DATABASE", errTxt);
        return false;
      };
    }

    return true;
  }
  
  /*
  **	FUNCTION
  **		Check if datapoint has read register 1 to read PR. If not, return the master Dp.
  **	ARGUMENTS
  **		string dp. dpName without the configuration system prefix.
  **  RETURNS
  **		string - masterDp
  **  CAUTIONS
  **		None
  */
  string getRR1(string dpSubStringName) {
    string masterDp = "";
    int masterChannel;
    int response = LhcVacEqpMaster(dpSubStringName, masterDp, masterChannel);
       
  	if (masterDp != "") {
      DebugFN("level1", "vcsVacmonDailyPressures.ctl masterDp", masterDp);
      DebugFN("level1", "vcsVacmonDailyPressures.ctl masterChannel", masterChannel);
  	}
    else {
      DebugFN("level1", "vcsVacmonDailyPressures.ctl LhcVacEqpMaster: ", "Dp not found");
    }
    return masterDp;
  }

  /*
  **	FUNCTION
  **		Check if pressure valid status is OK in read register 1 to perform reading in PR. 
  **	ARGUMENTS
  **		rr1(uint) - device's read register 1
  **  RETURNS
  **		true/false
  **  CAUTIONS
  **		None
  */
  bool checkPRValidStatus(uint rr1) {
  	if (rr1 & RR1_PRVALID) {
  		return true;
  	}
  	return false;
  }
