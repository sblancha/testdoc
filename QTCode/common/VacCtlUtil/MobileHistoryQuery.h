#ifndef	MOBILEHISTORYEQUERY_H
#define	MOBILEHISTORYEQUERY_H

#include "VacCtlUtilExport.h"

// Class buffering mobile equipment connection history query to be sent to PVSS
// It is supposed that only one dialog uses this functionality, so only one
// (the most recent) query is sent, all others are discarded

#include <qobject.h>
#include <qmutex.h>
#include <qdatetime.h>

class VACCTLUTIL_EXPORT MobileHistoryQuery : public QObject
{
	Q_OBJECT

public:
	static MobileHistoryQuery &getInstance(void);

	void add(const QDateTime &start, const QDateTime &end);
	void clear(void);
	bool getNext(QDateTime &start, QDateTime &end);

	MobileHistoryQuery();
	~MobileHistoryQuery();

signals:
	void changed(void);

protected:

	// The only instance of this class
	static MobileHistoryQuery	instance;

	// Start time of pending request
	QDateTime	startTime;

	// End time of pending request
	QDateTime	endTime;

	// Mutex to protect from parallel access
	QMutex	mutex;

	// Flag indicating if request is pending
	bool	requestPending;

	// Flag indicating if signal on content change has been sent
	bool	signalSent;
};


#endif	// MOBILEHISTORYEQUERY_H
