/**
* @brief Class implementation for widget Gauge Full Range Pirani - Penning
* @see VacIcon
*/
#ifndef	VACICONGAUGEDUALRP_H
#define	VACICONGAUGEDUALRP_H

#include "VacIconVG_U.h"

class VACCTLEWOUTIL_EXPORT VacIconGaugeDualRP : public VacIconVG_U {
public:
	VacIconGaugeDualRP(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG_U(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONGAUGEDUALRP_H
