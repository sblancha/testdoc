#
# Project file for qmake utility to build Profile PS/SPS EWO
#
#	02.11.2010	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = ProfileEwo

QT += printsupport

INCLUDEPATH += ../VacCtlEwoUtil \
	../VacCtlEwoUtil/Profile \
	../VacCtlEwoUtil/Axis \
	../../common/VacCtlUtil \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/Platform \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp \
	../../common/VacCtlEqpData/ResourcePool

HEADERS = ProfileWidget.h \
	ProfileEwo.h

SOURCES = ProfileWidget.cpp \
	ProfileEwo.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlHistoryData \
	-lVacCtlEqpData \
	-lVacCtlUtil

