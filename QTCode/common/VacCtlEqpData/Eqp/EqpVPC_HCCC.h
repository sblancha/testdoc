#ifndef	EQPVPC_HCCC_H
#define	EQPVPC_HCCC_H

//	Vacuum Pump Cryogenic with HRS-HCC Controller

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVPC_HCCC: public Eqp
{
	Q_OBJECT

public:
	EqpVPC_HCCC(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVPC_HCCC(const EqpVPC_HCCC &src) : Eqp(src) {}
#endif
	~EqpVPC_HCCC();

	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	//void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
	// Special color of interlock for device, returns true if there is special color
	//virtual bool getInterlockColor(DataEnum::DataMode mode, QColor &color, int &order);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Online? plcAlarmOnline : false; } // Replay functionality not developped for this equipment
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Online? rr1Online : false; } // Replay functionality not developped for this equipment
	virtual int getErrorCode(DataEnum::DataMode mode);
	//inline bool isBlockedOff(DataEnum::DataMode mode) const
	//{
	//	return mode == DataEnum::Replay ? blockedOffReplay : blockedOffOnline;
	//}

	//inline bool isHaveBlockedOff(void) { return haveBlockedOff; }
	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR2 DPE for replay mode
	//unsigned	rr1Replay;

	// Last knwon value of BlockedOFF DPE for online mode
	//bool		blockedOffOnline;

	// Last known value of BlockedOFF DPE for replay mode
	//bool		blockedOffReplay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	//bool		plcAlarmReplay;

	// Flag indicating if this device has BlockedOFF DPE
	//bool		haveBlockedOff;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, bool plcAlarm, QString &string);
#endif
	
};

#endif	// EQPVPC_HCCC_H
