//	Implementation of SynLhcInjDumpLine class
/////////////////////////////////////////////////////////////////////////////////

#include "SynLhcInjDumpLine.h"

#include "DataPool.h"
#include "BeamLine.h"
#include "Eqp.h"
#include "EqpMsgCriteria.h"

#include "VacEqpTypeMask.h"

/*
**	FUNCTION
**		Create new instance of SynLhcInjDumpLine for beam line and return it.
**		Note that new instance has only limits for equipment selection, NOT equipment
**		itself - equipment and geometry will be built by method Build().
**
**	PARAMETERS
**		parent			- Widget where line will be drawn
**		pLine			- Pointer to beam line for which synoptic line shall be built
**		parentVacType	- Vacuum type of parent line
**
**	RETURNS
**		Pointer to new instance of SynLhcInjDumpLine if given beam line shall be at least
**			partially presented in synoptic;
**		NULL if given beam line shall not be presented in synoptic
**
**	CAUTIONS
**		None
*/
SynLhcInjDumpLine *SynLhcInjDumpLine::create(QWidget *parent, BeamLine *pLine, int parentVacType)
{
	SynLhcInjDumpLine *pSynLine = new SynLhcInjDumpLine();

	// Find equipment range for line
	QList<BeamLinePart *> &lineParts = pLine->getParts();
	bool found = false;
	BeamLinePart *pPart;
	int idx;
	for(idx = 0 ; idx < lineParts.count() ; idx++)
	{
		pPart = lineParts.at(idx);
		if(pPart->getNEqpPtrs())
		{
			found = true;
			pSynLine->pStartPart = pPart;
			pSynLine->startEqpIdx = 0;
			break;
		}
	}
	for(idx = lineParts.count() - 1 ; idx >= 0 ; idx--)
	{
		pPart = lineParts.at(idx);
		if(pPart->getNEqpPtrs())
		{
			pSynLine->pEndPart = pPart;
			pSynLine->endEqpIdx = pPart->getNEqpPtrs() - 1;
			break;
		}
	}
	if(!found)
	{
		delete pSynLine;
		return NULL;
	}
	pSynLine->parent = parent;
	pSynLine->pBeamLine = pLine;
	pSynLine->metersToScreen = 1.0 / DataPool::getInstance().getMinPassiveEqpVisibleWidth();
	pSynLine->vacType = parentVacType;
	unsigned allVacTypeMask;
	VacEqpTypeMask cryoEqpMask, beamEqpMask;
	pSynLine->analyzeTypes(allVacTypeMask, cryoEqpMask, beamEqpMask);

	return pSynLine;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SynLhcInjDumpLine::SynLhcInjDumpLine() : SynLine()
{
	reverse = false;
	alwaysIncludeSectorBorder = true;
}

SynLhcInjDumpLine::~SynLhcInjDumpLine()
{

}

void SynLhcInjDumpLine::checkForReverse(void)
{
	reverse = false;
	if(pBeamLine->getStartLine())
	{
		switch(pBeamLine->getStartType())
		{
		case BeamLineConnType::LeftIn:
		case BeamLineConnType::RightIn:
			reverse = true;
			break;
		}
	}
	if(pBeamLine->getEndLine())
	{
		switch(pBeamLine->getEndType())
		{
		case BeamLineConnType::LeftOut:
		case BeamLineConnType::RightOut:
			reverse = true;
			break;
		}
	}
}
