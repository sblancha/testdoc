//	Implementation of SineCosine class

#include "SineCosine.h"

#include <math.h>

#include "PlatformDef.h"

SineCosine::SineCosine(double angle)
{
	setAngle(angle);
}

void SineCosine::setAngle(double angle)
{
	this->angle = angle;
	sinValue = sin(-angle - M_PI / 2.0);
	cosValue = cos(-angle - M_PI / 2.0);
}

void SineCosine::dump(FILE *pFile, int index)
{
	fprintf(pFile, "      %02d: %f (%f %f) pos %f\n", index, angle, cosValue, sinValue, pos);
}
