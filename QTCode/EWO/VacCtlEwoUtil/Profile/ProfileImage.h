#ifndef PROFILEIMAGE_H
#define PROFILEIMAGE_H

#include "VacCtlEwoUtilExport.h"

//	Class performing profile drawing on itself, formerly ProfileView

#include "ProfileItem.h"
#include "ProfileSector.h"

#include "VerticalAxis.h"

#include "VacEqpTypeMask.h"
#include "EqpMsgCriteria.h"

#include <QWidget>
#include <QList>
#include <QDateTime>


#include <stdio.h>

class BeamLine;
class Sector;
class SectorDrawPart;

class ExtendedLabel;
#include <QSpinBox>
#include <QTimer>

class ProfileImage : public QWidget
{
	Q_OBJECT

public:
	~ProfileImage();

	VACCTLEWOUTIL_EXPORT static ProfileImage *create(QWidget *parent, Sector *pStartSector, Sector *pEndSector,
		DataEnum::DataMode mode, VacEqpTypeMask &allEqpMask, QString &errMsg);

	VACCTLEWOUTIL_EXPORT void clear(void);
	VACCTLEWOUTIL_EXPORT void analyzeTypes(unsigned &vacTypeMask, VacEqpTypeMask &beamEqpMask);
	VACCTLEWOUTIL_EXPORT void setLimits(int newMin, int newMax);
	VACCTLEWOUTIL_EXPORT void applyMask(const VacEqpTypeMask &newMask);
	VACCTLEWOUTIL_EXPORT bool isShowSectorNames(void) const { return showSectorNames; }
	VACCTLEWOUTIL_EXPORT void setShowSectorNames(bool flag) { if(flag != showSectorNames) { showSectorNames = flag; rebuildImage(); } }


signals:
	void eqpMaskChanged(void);
	void dpMouseDown(int button, int mode, int x, int y, const char *dpName);
	void limitsChanged(int min, int max);

private slots:
	void eqpDataChange(ProfileItem *pSrc, bool colorChanged);
	void eqpSelectChange(ProfileItem *pSrc);
	void mobileVisibilityChanged(void);
	void itemVisibilityChanged(ProfileItem *pSrc);
	void spinBoxChange(int value);
	void redrawTimerDone(void);
	void rebuildImage(void);

protected:
	//	Pointer to start sector of profile
	Sector					*pStartSector;

	// Pointer to end sector of profile
	Sector					*pEndSector;

	// Equipment type mask used for profile building
	VacEqpTypeMask			eqpMask;

	// Equipment type mask holding all allowed device types in profile
	VacEqpTypeMask			allowedEqpMask;

	// List of all equipment items in profile
	QList<ProfileItem *>	eqps;

	// List of all vacuum sector areas in profile
	QList<ProfileSector *>	sectors;

	// List of all vacuum sectors used during profile building
	QList<Sector *>		sectorUsage;

	// List of sectors draw parts which were already passed during profile building
	QList<SectorDrawPart *>	sectorPartPass;

	// Vertical axis
	VerticalAxis			*pAxis;

	// Label for displaying information on graph bars
	ExtendedLabel			*pLabel;

	// Timer to hide label after 'reasonable' delay
	QTimer					*pTimer;

	// Timestamp of last image drawing
	QTime						drawTime;

	// Timer to redraw image after online value arrival
	QTimer					*pRedrawTimer;

	// SpinBox for updating scale settings
	QSpinBox				*pSpinBox;

	// Total amount of bars to be drawn
	int						nBarsToDraw;

	// Widget width used for last drawing
	int						viewWidth;

	// Widget height used for last drawing
	int						viewHeight;

	// Data acquisition mode for this view
	DataEnum::DataMode		mode;

	// Flag indicating if equipment type mask was applied at leat once
	bool					eqpMaskApplied;

	// Flag indicating if equipment mask has been changed
	bool					newEqpMaskApplied;

	// Flag indicating if redraw is required because visibility state equipment has
	// been changed
	bool					redrawRequired;

	// Flag indicating if only flashing bars shall be drawn
	bool					flashBarsOnly;

	// Flag indicating if flashing bars shall be drawn with alternative color
	bool					useAltColor;

	// Flag indicating if SpinBox is used to edit scale minimum
	bool					editScaleMin;

	// Flag indicating that I'm setting SpinBox initial value in code
	bool					settingSpinBox;

	// Flag inidcating if sector names shall be shown beneath graph area
	bool					showSectorNames;

	ProfileImage(QWidget *parent = NULL, Qt::WindowFlags f = 0);

	int build(void);
	BeamLine *findMainLine();
	void buildEqpOnLine(Sector *pSector, SectorDrawPart *pDrawPart);
	void addDevice(Eqp *pEqp);

	bool prepare(QPainter &painter);
	void drawSectors(QPainter &painter);
	void drawBars(QPainter &painter);
	void buildBarGeom(void);
	bool barVerticalGeom(ProfileItem *pItem, double scaleMin, double scaleMax,
			double scaleHeight, int useMin);
	void buildSectors(QPainter &painter, const QRect &viewRect);
	void addSector(ProfileSector &source, bool withinDevice, Sector *pSector);
	int calculateSectorLabels(QPainter &painter, const QRect &viewRect);

	virtual void mouseMoveEvent(QMouseEvent *pEvent);
	virtual void mousePressEvent(QMouseEvent *pEvent);
	virtual void mouseDoubleClickEvent(QMouseEvent *pEvent);
	virtual void resizeEvent(QResizeEvent *event);

	ProfileItem *findItemUnderMouse(int x, int y);
	void showBarLabel(ProfileItem *pItem, int y);
	void showScaleControl(int y);

	void initiateRedraw(void);

	virtual void leaveEvent(QEvent *pEvent);

	void dump(const char *fileName);
};

#endif	// PROFILEIMAGE_H
