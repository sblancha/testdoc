//	Implementation of VacDpDataConnection class
/////////////////////////////////////////////////////////////////////////////////

#include "VacDpDataConnection.h"

#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
////////////////// Construction/destruction

VacDpDataConnection::VacDpDataConnection()
{
	pEqp = NULL;
	mode = DataEnum::Online;
}

VacDpDataConnection::~VacDpDataConnection()
{
	disconnectAll();
}

/*
**	FUNCTION
**		Set new reference to equipment
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacDpDataConnection::setEqp(Eqp *pEqp)
{
	if(this->pEqp != pEqp)
	{
		disconnectAll();
	}
	this->pEqp = pEqp;
}

/*
**	FUNCTION
**		Connect to requested DPE of device
**
**	ARGUMENTS
**		dpeName	- Name of DPE to be connected
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacDpDataConnection::connectDpe(const char *dpeName)
{
	if(!pEqp)
	{
		return;
	}
	for(int idx = 0 ; idx < dpes.count() ; idx++)
	{
		QByteArray *pDpe = dpes.at(idx);
		if((*pDpe) == dpeName)
		{
			return;	// Already connected
		}
	}
	dpes.append(new QByteArray(dpeName));
	pEqp->connectDpe(this, dpeName, mode);
}

/*
**	FUNCTION
**		Disconnect from all previously connected DPEs
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacDpDataConnection::disconnectAll(void)
{
	if(!pEqp)
	{
		return;
	}
	for(int idx = 0 ; idx < dpes.count() ; idx++)
	{
		QByteArray *pDpe = dpes.at(idx);
		pEqp->disconnectDpe(this, pDpe->constData(), mode);
	}
	while(!dpes.isEmpty())
	{
		delete dpes.takeFirst();
	}
}

/*
**	FUNCTION
**		Slot activated when one of DPEs of device have been changed,
**		check if mode and DPE are of interest, if so - emit corresponding
**		signal.
**
**	ARGUMENTS
**		pSrc	- Pointer to source of signal
**		dpeName	- Name of DPE whose value has been changed
**		source	- Enumerated source type (Own/Master/Plc)
**		value	- New value of DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacDpDataConnection::dpeChange(Eqp * /* pSrc */, const char *dpeName,
	DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	switch(mode)
	{
	case DataEnum::Online:
	case DataEnum::Polling:
		if(this->mode == DataEnum::Replay)
		{
			return;
		}
		break;
	case DataEnum::Replay:
		if(this->mode != DataEnum::Replay)
		{
			return;
		}
		break;
	}
	if(source == DataEnum::Own)
	{
		for(int idx = 0 ; idx < dpes.count() ; idx++)
		{
			QByteArray *pDpe = dpes.at(idx);
			if((*pDpe) == dpeName)
			{
				emit dpeValueChange(dpeName, source, value);
				break;
			}
		}
	}
	else
	{
		emit dpeValueChange(dpeName, source, value);
	}
}

