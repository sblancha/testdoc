//	Implementation of DateEditTableItem class
/////////////////////////////////////////////////////////////////////////////////

#include "DateEditTableItem.h"

#include "NullDateEdit.h"

#include <qdatetime.h>

QWidget *DateEditTableItem::createEditor(void) const
{
	NullDateEdit *pEditor = new NullDateEdit(date, table()->viewport());
	pEditor->setOrder(QDateEdit::DMY);
	pEditor->setSeparator("-");
	return pEditor;
}

void DateEditTableItem::setContentFromEditor(QWidget *pEditor)
{
	date = ((NullDateEdit *)pEditor)->date();
	if(date.isNull())
	{
		setText("");
	}
	else
	{
		setText(date.toString("dd-MM-yyyy"));
	}
}

