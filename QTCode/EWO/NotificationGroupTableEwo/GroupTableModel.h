#ifndef GROUPTABLEMODEL_H
#define GROUPTABLEMODEL_H

// Data model for table of SMS configurations
#include "SmsGroup.h"
#include "SmsConfig.h"

#include <QAbstractTableModel>
#include <qlist.h>
#include <qhash.h>
#include <qstack.h>
#include <qmutex.h>
#include <qtimer.h>

class GroupTableModel : public QAbstractTableModel {
	Q_OBJECT

public:
	GroupTableModel(QObject *parent = 0);
	~GroupTableModel();

	void clear(void);
	void addGroup(const QString &dpName);
//	void addGroupConfig(const QString &dpName, const QStringList &simpleDpNames);
//	void addToGroup(const QString &dpName, const QString &simpleDpName);
	void deleteGroup(const QString &dpName);
	void setGroupOwner(const QString &dpName, const QString &owner);
	void setGroupName(const QString &dpName, const QString &name);
	void setGroupId(const QString dpName, long id);
	void setGroupType(const QString dpName, int type);
	void setGroupScopes(const QString &dpName, const QStringList &scopes);
	void setGroupRecipients(const QString &dpName, const QStringList &recipients);
	void setGroupError(const QString &dpName, const QString &error);
	void setGroupNumMsg(const QString &dpName, int numMsg);
	void setGroupNumEvents(const QString &dpName, unsigned numEvents);
	void setGroupFlags(const QString &dpName, bool active, bool activated, bool initialized);

	int getNumRows(void) { return rowDict.count(); }
	const QString getGroupAtRow(int row);

	const QString nextConnectRequest(void);
	const QString nextDisconnectRequest(void);

	// Implementation of QAbstractTableModel's methods
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant data(const QModelIndex &index, int role) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	//void sort(int column, Qt::SortOrder order);

signals:
	void connectRequest(void);

protected:
	// Content of table - all rows
	QList<SmsGroup *>	rowDict;

	// Pending items to be added to table - in order not to refresh very often
	QList<SmsGroup *>	pendingGroups;

	// All configurations
	QHash<QString, SmsConfig *>	allConfigs;

	// All groups
	QHash<QString, SmsGroup *>	allGroups;

	// Mutex to protect connect/disconnect queues from parallel access
	QMutex		mutex;

	// Timer for delay view notification
	QTimer			*pTimer;

	// List of DP names to be disconnected first, before other connect
	// requests can be processed
	QStack<QString>	disconnectOnClear;

	// Flag indicating if connectRequest() was emitted and there are still pending requests
	bool		connectRequestSent;

	void addRow(SmsGroup *pGroup);
	void addToAllGroups(SmsGroup *pGroup){ allGroups.insert(pGroup->getDpName(), pGroup); }
	void addToAllConfigs(SmsConfig *pConfig){ allConfigs.insert(pConfig->getDpName(), pConfig); }
	void addCallbackRequest(void);

private slots: 
	void groupChanged(SmsGroup *pGroup);
	void groupNumMsgChanged(SmsGroup *pGroup, int delta);
	void groupNumEventsChanged(SmsGroup *pGroup, int delta);
	void timerDone(void);

};

#endif	// GROUPTABLEMODEL_H