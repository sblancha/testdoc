#ifndef	HISTORYEQPVALUESSET_H
#define	HISTORYEQPVALUESSET_H

// Class holding set of analog values for one device

#include "HistoryEqpValue.h"

#include <QList>
#include <QString>
#include <QDateTime>

class Eqp;

class HistoryEqpValuesSet
{
public:
	HistoryEqpValuesSet(Eqp *pEqp, const QString &bitDpeName, int bitNumber, const QString &bitVisibleName);
	HistoryEqpValuesSet(Eqp *pEqp, const QString &dpeName);
	~HistoryEqpValuesSet();

	void clear(void) { values.clear(); previousValue = 0; previousValueSet = false; }
	void addValue(const QDateTime &time, float value);

	bool getTimeAt(int timeIndex, QDateTime &time);
	float getValueAt(const QDateTime &time);

	// Access
	inline Eqp *getEqp(void) const { return pEqp; }
	inline const QString &getDpe(void) const { return dpe; }
	inline const QString &getBitDpe(void) const { return bitDpe; }
	inline int getBitNbr(void) const { return bitNbr; }
	inline const QString &getBitName(void) const { return bitName; }
	inline int getColumn(void) const { return column; }
	inline void setColumn(int column) { this->column = column; }
	inline int getIndex(void) const { return index; }
	inline void setIndex(int index) { this->index = index; }
	inline void setStartIndex(void) { index = previousValueSet ? -1 : 0; }
	inline void setPreviousValue(const QDateTime &ts, float value) { previousTs = ts; previousValue = value; previousValueSet = true; }

protected:
	// Pointer to device for which history is stored
	Eqp							*pEqp;

	// DPE name: there may be several DPEs per device, for example, BAKEOUT rack
	QString						dpe;

	// DPE name - source of bit history
	QString						bitDpe;

	// Bit number for bit history
	int							bitNbr;

	// Visible bit name for bit history
	QString						bitName;

	// List of history values for device
	QList<HistoryEqpValue *>	values;

	// Row number in Excel file - this class just stores the value, but
	// does not use it
	int							column;

	// Index of value in list of values - this class just stores the value, but
	// does not use it
	int					index;

	// Timestamp of previous value
	QDateTime					previousTs;

	// The last value before first in list of values
	float						previousValue;

	// Flag indicating if previous value was set
	bool						previousValueSet;
};

#endif	// HISTORYEQPVALUESSET_H
