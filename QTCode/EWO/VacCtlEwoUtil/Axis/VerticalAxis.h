#ifndef	VERTICALAXIS_H
#define	VERTICALAXIS_H

//	Vertical axis for different graphs. This class is NOT QWidget, but it
//	can draw into prepared QPainter on behalf of real QWidget

#include <qpainter.h>

#include <stdio.h>

class VerticalAxis
{
public:
	VerticalAxis(bool logAxis, bool axisOnTheLeft);
	virtual ~VerticalAxis();

	void setLogLimits(int newMin, int newMax);
	void setLinLimits(float newMin, float newMax);
	void fixMinAt(int location);
	void fixMaxAt(int location);
	void draw(QPainter &painter);
	bool prepare(const QFontMetrics &fontMetrics);

	void getLimits(int &useMin, int &useMax);
	void setLimits(int newMin, int newMax);
	void getLimits(float &useMin, float &useMax);
	void setLimits(float newMin, float newMax);

	// Value returned by valueToScreen method
	typedef enum
	{
		BelowAxis = -1,
		WithinRange = 0,
		AboveAxis = 1
	} ValueRange;

	// Convert value to screen coordinate
	ValueRange valueToScreen(float value, int &screenY);

	// Convert screen coordinate to value
	double screenToValue(int screenY);

	// Switch (temporaryly) axis type
	void switchScaleType(bool setLog);

	// Access
	virtual inline const QRect &getGraphArea(void) const { return graphArea; }
	virtual void setGraphArea(const QRect &area);

	virtual inline bool isGrid(void) const { return grid; }
	virtual inline void setGrid(bool flag) { grid = flag; }

	virtual inline int getGridStyle(void) const { return gridStyle; }
	virtual inline void setGridStyle(int style) { gridStyle = style; }

	virtual inline int getTickSize(void) const { return tickSize; }
	virtual inline void setTickSize(int size) { tickSize = size; }

	virtual inline bool isTickOutside(void) const { return tickOutside; }
	virtual inline void setTickOutside(bool flag)
	{
		if(flag != tickOutside)
		{
			needPrepare = true;
		}
		tickOutside = flag;
	}

	virtual inline void setGridLineStart(int coord) { gridLineStart = coord; }
	virtual inline void setGridLineEnd(int coord) { gridLineEnd = coord; }
	virtual inline int getAxisLocation(void) { return axisLocation; }
	virtual inline int getScaleTop(void) { return scaleTop; }
	virtual inline int getScaleBottom(void) { return scaleBottom; }
	virtual inline int isLinLimitsFixed(void) const { return linLimitsFixed; }
	virtual inline void setLinLimitsFixed(bool flag) { linLimitsFixed = flag; }

	virtual inline bool isLogScale(void) { return logScale; }
	virtual inline bool isTemporaryLinear(void) { return temporaryLinear; }

	void dump(FILE *pFile);

protected:
	// Graph area to use
	QRect		graphArea;

	// Fixed location of minimum
	int			minLocation;

	// Fixed location of maximum
	int			maxLocation;

	// Location of grid line start
	int			gridLineStart;

	// Location of grid lines end
	int			gridLineEnd;

	// Grid stype
	int			gridStyle;

	// Ticks size
	int			tickSize;

	// Axis minimum = 10 ^ min for log axis
	float		min;

	// Axis maximum = 10 ^ max for log axis
	float		max;

	// Horixontal location of axis
	int			axisLocation;

	// Y coordinate corresponding to max
	int			scaleTop;

	// Y coordinate corresponding to min
	int			scaleBottom;


	// Step between two labeled ticks in logarithmic scale, normally it is 1, but
	// can be more if there are too many ticks in scale range
	int			drawEvery;

	// Minimum of linear scale, can differ from min of values
	float		linScaleMin;

	// Maximum of linear scale, can differ from max of values
	float		linScaleMax;

	// Value of the very first labeledd tick in linear scale
	float		linLabelsStart;

	// Step between two labeled ticks in linear scale
	float		linLabelsStep;

	// Format used to print labels for linear axis
	char		linLabelFormat[32];



	// Flag indicating scale type: logarihtmic (true) or linear (false)
	bool		logScale;

	// Flag indicating if 'normal logarithmic" axis is temporary switched to linear
	bool		temporaryLinear;

	// Flag indicating if axis is draw on the left (true) or right (false) side of graph
	bool		onLeftSide;

	// Flag indicating if axis parameters shall be recalculated
	bool		needPrepare;

	// Flag indicating if minimum label is at fixed location
	bool		minFixed;

	// Flag indicating if maximum label is at fixed location
	bool		maxFixed;

	// Flag indicating if grid shall be used
	bool		grid;

	// Flag indicating if ticks shall be drawn outside grapg area
	bool		tickOutside;

	// Flag indicating if limits for linear scale shall be fixed, i.e.
	// linScaleMin == min and linScaleMax == max ALWAYS
	bool		linLimitsFixed;

	void prepareLogScale(const QFontMetrics &fontMetrics);
	void prepareLinScale(const QFontMetrics &fontMetrics);

	void drawLogScale(QPainter &painter);
	void drawLinScale(QPainter &painter);

	void makeLogLabel(int value, char *label, size_t bufLen);
	void makeLinLabel(float value, char *label, size_t bufLen);

	ValueRange valueToScreenLin(float value, int &screenY);
	ValueRange valueToScreenLog(float value, int &screenY);
};

#endif	// VERTICALAXIS_H
