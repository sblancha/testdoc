#ifndef	VACICONVPG_STD_H
#define	VACICONVPG_STD_H

// Icon for VPG_STD	- LHC STD pumping group

#include "VacIcon.h"

class EqpVPG_STD;

class VACCTLEWOUTIL_EXPORT VacIconVPG_STD : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPG_STD(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPG_STD();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

	// Override method of VacIcon 
	virtual void setDirection(VacIconContainer::Direction newDirection);
	virtual void setPipeColor(const QColor &newColor);

protected:
	// Pointer to my device
	EqpVPG_STD *pEqp;

	// Areas for pump and valve symbols. Drawing can be easy using QPainter
	// coordinate transformations, but finding areas for tooltips is not so
	// evident. May be it is more convenient just to keep in memory coordinates
	// of all distinct areas

	QRect	pump;
	QRect	vvr;
	QRect	vgr;
	QRect	vgp;

	VacIcon	*pIconVGR;
	VacIcon	*pIconVGP;

	void setAreaGeometry(void);
	void setGaugeDirection(void);

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawPipes(QPainter &painter);
	virtual void drawVvr(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
	virtual void drawPump(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
	
	void setPainterForVvr(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);

	// Override VacIcon's reaction on mouse press
	virtual void mousePressEvent(QMouseEvent *pEvent);
};

#endif	// VACICONVPG_STD_H
