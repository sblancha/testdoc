#ifndef DATAPOOL_H
#define DATAPOOL_H

#include "VacCtlEqpDataExport.h"

// Data pool - holds all information about machine (beam lines, equipment, sectors etc.)

#include <qlist.h>
#include <qstringlist.h>
#include <qhash.h>
#include <qmutex.h>

#include "FileParser.h"
#include "NamePool.h"
#include "MainPart.h"
#include "Sector.h"
#include "LhcRegion.h"
#include "PlcConfig.h"

class InterlockChain;
class Rack;

#include "BeamLine.h"

#include "MobileType.h"

class DataPool : public FileParser
{
public:

	VACCTLEQPDATA_EXPORT static DataPool &getInstance(void);

	///////////////////////////////////////////////////////////////////////////////////////
	/////////////////// Data initialization/cleanup ///////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	VACCTLEQPDATA_EXPORT void	reset(void);
	VACCTLEQPDATA_EXPORT void setDataPart(const char *newName);
	VACCTLEQPDATA_EXPORT int initPassive(bool isLhcFormat, bool withoutConfig, const char *pathName, const char *machineName,
		QStringList &errList);
	VACCTLEQPDATA_EXPORT int initActive(const char *machine, const char *fileName, QStringList &errList);
	VACCTLEQPDATA_EXPORT int initRackData(const char *machine, const char *fileName, QStringList &errList);
	VACCTLEQPDATA_EXPORT int initInterlockData(const char *machine, const char *fileName, QStringList &errList);
	VACCTLEQPDATA_EXPORT void setMinSectorLength(const char *lineName, int minLength);
	VACCTLEQPDATA_EXPORT void clearData(bool clearAll);
	int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);
	VACCTLEQPDATA_EXPORT int addGlobalDevice(const char *dpName, const char *dpeName, const char *visibleName, int funcType, QString &errMsg);

	///////////////////////////////////////////////////////////////////////////////////////
	/////////////////// Helper methods  //////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	char *addString(const char *pString);
	char *addMultiUseString(const char *pString);


	///////////////////////////////////////////////////////////////////////////////////////
	/////////////////// Search for required data from pool ///////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	VACCTLEQPDATA_EXPORT LhcRegion *regionAtLocation(float coord);
	VACCTLEQPDATA_EXPORT virtual bool coordInLine(char *pName, char *pForLine, int connectVacType, float coord, bool isStart,
		float &resultX, float &resultY);

	VACCTLEQPDATA_EXPORT virtual BeamLine *getLine(const char *pName);
	VACCTLEQPDATA_EXPORT virtual BeamLine *getCircularLine(void);

	VACCTLEQPDATA_EXPORT virtual QList<Eqp *> getMobileTypeEqps(int mType);
	
	VACCTLEQPDATA_EXPORT virtual Sector *findSectorData(const char *sectorName);
	VACCTLEQPDATA_EXPORT virtual MainPart *findMainPartData(const char *mpName);
	VACCTLEQPDATA_EXPORT virtual const QString findDomain(const char *name, bool addIfNew);
	VACCTLEQPDATA_EXPORT virtual void findMainPartNames(QStringList &result, bool visibleOnly = false);
	VACCTLEQPDATA_EXPORT virtual void findSectorsInMainPart(const char *mpName, QStringList &result, bool visibleOnly = false);
	VACCTLEQPDATA_EXPORT virtual int findThermometersForValve(const char *valveDp, QStringList &result, QString &errMsg);
	VACCTLEQPDATA_EXPORT virtual int findNeighbourValves(const char *valveDpName, QStringList &valvesBefore, QStringList &valvesAfter, QString &errMsg);

	VACCTLEQPDATA_EXPORT virtual void findEqpMatchingMask(const QString &filter, QStringList &dpNames, QStringList &eqpNames);
	VACCTLEQPDATA_EXPORT bool nameMatchesFilter(const char *pName, const char *pFilter);

#ifndef PVSS_SERVER_VERSION
	VACCTLEQPDATA_EXPORT virtual Sector *findNeighbourSector(const char *sectorName, MainPart *pMainPart, int direction);
	VACCTLEQPDATA_EXPORT virtual Sector *findNeighbourSector(Sector *pStartSector, MainPart *pMainPart, int direction);
	
#endif

	///////////////////////////////////////////////////////////////////////////////////////
	/////////////////// Auxilliary methods ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	VACCTLEQPDATA_EXPORT void replaceSectors(QList<Sector *> &newSectors);
	VACCTLEQPDATA_EXPORT bool isSectorListContinuous(QList<Sector *> &sectorList);
	VACCTLEQPDATA_EXPORT bool areSectorsOfMainPartContinuous(MainPart *pMainPart);
	VACCTLEQPDATA_EXPORT bool isEqpInTheArc(Eqp *pEqp);

	// Access
	VACCTLEQPDATA_EXPORT inline const char *getDataPart(void) { return dataPart; }
	VACCTLEQPDATA_EXPORT inline bool isLhcFormat(void) { return lhcFormat; }
	VACCTLEQPDATA_EXPORT inline float getMinPassiveEqpVisibleWidth(void) { return minPassiveEqpVisibleWidth; }
	VACCTLEQPDATA_EXPORT inline const char *getTimeStamp(void) { return timeStamp; }

	VACCTLEQPDATA_EXPORT inline QList<BeamLine *> &getLines(void) { return lines; }
	VACCTLEQPDATA_EXPORT inline QList<MainPart *> &getMainParts(void) { return mainParts; }
	VACCTLEQPDATA_EXPORT inline QList<Sector *> &getSectors(void) { return sectors; }
	VACCTLEQPDATA_EXPORT Sector *findSectorByDpName(const char *dpName); // [VACCO-1645]
	VACCTLEQPDATA_EXPORT inline QStringList &getDomains(void) { return domains; }
	VACCTLEQPDATA_EXPORT inline QList<Eqp *> &getCtlList(void) { return ctlList; }
	VACCTLEQPDATA_EXPORT inline QList<Eqp *> &getOrderedEqpList(void) { return orderedEqpList; }
	VACCTLEQPDATA_EXPORT inline Eqp *findEqpByDpName(const char *dpName) { return eqpDict.value(dpName); }
	VACCTLEQPDATA_EXPORT Eqp *findEqpByVisibleName(const char *name);
	VACCTLEQPDATA_EXPORT QList<Eqp *> findEqpsByMasterDp(QString masterDp);
	VACCTLEQPDATA_EXPORT Eqp *findPlc(const QString &dpName);
	VACCTLEQPDATA_EXPORT inline QHash<QByteArray, Eqp *> &getEqpDict(void) { return eqpDict; }
	VACCTLEQPDATA_EXPORT QString getRackBuildingOfEquipment(const QString &eqpDisplayName);
	VACCTLEQPDATA_EXPORT inline QList<Rack *> &getRackList(void){ return rackList; };
	VACCTLEQPDATA_EXPORT InterlockChain *getInterlockChain(QString valveName);
	//VACCTLEQPDATA_EXPORT void resetRackList(void);

	VACCTLEQPDATA_EXPORT inline float *getLhcIps(void) { return lhcIps; }
	VACCTLEQPDATA_EXPORT inline int getNLhcIps(void) { return nLhcIps; }
	VACCTLEQPDATA_EXPORT inline QList<LhcRegion *>	&getLhcArcs(void) { return lhcArcs; }
	VACCTLEQPDATA_EXPORT inline QList<LhcRegion *>	&getLhcBeamLocs(void) { return lhcBeamLocs; }
	VACCTLEQPDATA_EXPORT inline float getLhcRingLength(void) { return lhcRingLength; }
	

	VACCTLEQPDATA_EXPORT inline const QString &getPicturePath(void) const { return picturePath; }
	VACCTLEQPDATA_EXPORT inline void setPicturePath(const char *path) { picturePath = path; }

	VACCTLEQPDATA_EXPORT void dump(FILE *pFile);

	DataPool();
	virtual ~DataPool();

protected:
	// The only available instance
	static DataPool	pool;

	///////////////////////////////// To prevent initialization by multiple threads
	QMutex			mutex;

	// Data pool status - see DataPoolStatusEnum in DataPool.cpp
	typedef enum
	{
		StatusClean = 0,
		StatusPassiveOK = 1,
		StatusPassiveFailed = 2,
		StatusActiveOK = 3,
		StatusActiveFailed = 4
	} DataPoolStatus;

	DataPoolStatus	status;

	bool rackDataLoaded;
	bool interlockDataLoaded;

	///////////////////////////////// General parameters /////////////////////////

	// TRUE if data are 'pure survey' data, i.e. no beam lines configuration
	bool				pureSurvey;

	// Machine name
	char		*machine;

	// Name of data part used by this pool, can be NULL
	char		*dataPart;

	// Timestamp string of generated file
	char		*timeStamp;

	// Flag indicating if data pool contains data in LHC format
	bool		lhcFormat;

	// Minimum width of passive equipment to be visible on synoptic
	float		minPassiveEqpVisibleWidth;

	///////////////////////////////// Name pools ///////////////////////////////////
	// Two name pools: one for passive, another one for active equipment
	NamePool	pasNamePool, actNamePool;

	//////////////////////////////// Basic machine data ////////////////////////////
	// List of beam lines
	QList<BeamLine *>		lines;

	// List of sectors
	QList<Sector *>		sectors;

	// Dictionary for fast sector lookup
	QHash<QByteArray, Sector *>		sectorDict;

	// List of main parts
	QList<MainPart *>		mainParts;

	// List of domains
	QStringList		domains;

	// Equipment which is not located on beam lines - controllers, PLCs etc.
	QList<Eqp *>			ctlList;

	// Equipment dictionary - keys are DP names
	QHash<QByteArray, Eqp *>			eqpDict;

	//list of racks
	QList<Rack *> rackList;

	QList<InterlockChain *> interlockChainList;
	// List of all equipment (with physical locations + controllers) ordered
	// according to geography. The list has been added 27.05.2010 in order
	// to improve performance of EqpSelector control
	QList<Eqp *>			orderedEqpList;

	// Path to pictures directory - required to load pixmaps
	QString					picturePath;

	//////////////////////////////// Basic machine data - extra for LHC ////////////////
	// LHC special: length of LHC ring
	float				lhcRingLength;

	// LHC special: locations for IPs
	float				*lhcIps;

	// LHC special:	number of IPs
	int					nLhcIps;

	// LHC specific: locations for arcs
	QList<LhcRegion *>	lhcArcs;

	// LHC specific: beam pipe location regions
	QList<LhcRegion *>	lhcBeamLocs;

	//////////////////////////////// Auxilliary for file parsing and others ////////
	// Buffer used for building file name from components
	char	*fileNameBuffer;

	// Type of input file being parsed
	int		parseType;

	// Completion code of parsing procedure - stored outside any method
	int		parseCoco;

	// Current index of beam line - to support outer calls FirstBeamLine/NextBeamLine
	int		currentLineIdx;

///////////////////////////////////////////////////////////////////////////////////////
/////////////////// Data initialization/cleanup ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

	bool canInitPassive(int &coco, QStringList &errList);
	int parsePassive(bool isLhcFormat, bool withoutConfig, const char *pathName, const char *machineName,
		QStringList &errList);
	bool canInitActive(int &coco, QStringList &errList);
	int parseActive(const char *machine, const char *fileName, QStringList &errList);
	int parseRackData(const char *machine, const char *fileName, QStringList &errList);
	int parseInterlockData(const char *machine, const char *fileName, QStringList &errList);
	char *buildFileName(const char *pathName, const char *machineName, const char *suffix);
	void addFileNameToMsg(QStringList &errList, const char *pFileName);
	bool pasPostProcess(QStringList &errList);
	void pasPostProcessLinac2(void);
#ifndef PVSS_SERVER_VERSION
	void correctSectorBorders(void);
#endif
	bool actPostProcess(QStringList &errList);
	void setExtraSectors(void);	// L.Kopylov 11.03.2013
	void setExtraSectorsNonLHC(void);
	void setExtraSectorsLHC(void);

#ifndef PVSS_SERVER_VERSION
	void buildBeamSectorBorders(void);
#endif
	void buildAuxEquipment(void);
	void addSector(Sector *pSector);

	void buildDpNameDictionary(void);
	void buildOrderedEqpList(void);
	int processConfigTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		QStringList &errList);
	int processEqpTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		QStringList &errList);
	int processActTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		QStringList &errList);
	int processLhcGlobalTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		QStringList &errList);
	int processRackDataTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		QStringList &errList);
	int processInterlockDataTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		QStringList &errList);

	void calculateAccessPointDistance(void);
	void calculateAccessPointDistanceLhc(void);
	void calculateAccessPointDistanceLhcCircleLine(BeamLine *pLine);
	void calculateAccessPointDistanceLhcLine(BeamLine *pLine);

	void setRackEqpLinks(Rack &rack);

///////////////////////////////////////////////////////////////////////////////////////
/////////////////// Data search ///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

#ifndef PVSS_SERVER_VERSION
	Sector *neighbourSectorAnyMainPart(Sector *pStartSector, int direction);
	Sector *neighbourSectorInMainPart(Sector *pStartSector, MainPart *pMainPart, int direction);
#endif

	int findThermometersNearValve(BeamLinePart *pPart, Eqp *pValve, int valveIdx, int direction,
		QStringList &result);
	int findNeigourValvesLhc(Eqp *pValve, QStringList &valvesBefore, QStringList &valvesAfter, QString &errMsg);
	int findNeigourValvesNonLhc(Eqp *pValve, QStringList &valvesBefore, QStringList &valvesAfter, QString &errMsg);
	void findValvesBefore(BeamLine *pStartLine, BeamLinePart *pStartPart, int startEqpIdx, QStringList &valves);
	void findValvesAfter(BeamLine *pStartLine, BeamLinePart *pStartPart, int startEqpIdx, QStringList &valves);
	BeamLine *findStartLine(QList<BeamLine *>::ConstIterator &linesIn, BeamLine *pStartLine);
	BeamLinePart *findStartLinePart(const QList<BeamLinePart *> &parts, QList<BeamLinePart *>::ConstIterator &partsIn, BeamLinePart *pStartPart);
	int findRackByName(QString rackName);
	static bool rackNameLessThan(Rack * firstRack, Rack * secondRack);
	void sortRackList(void);
	

};

#endif	// DATAPOOL_H
