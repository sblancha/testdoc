//	Implementation of ProfileLhcSingleBeamLine class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileLhcSingleBeamLine.h"
#include "ProfileLhcView.h"

#include "VacLinePart.h"

#include "LhcRegion.h"
#include "DataPool.h"
#include "Eqp.h"
#include "EqpType.h"

#include "VacMainView.h"

#include <math.h>

#include "PlatformDef.h"
#include "DebugCtl.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ProfileLhcSingleBeamLine::ProfileLhcSingleBeamLine(ProfileLhcView *parent, int vacType, MainPart *pMainPart)
	: ProfileLhcLineBeam(parent, ProfileLhcLine::OuterBeam, 0, 0)
{
	this->vacType = vacType;
	this->pMainPart = pMainPart;
	mode = DataEnum::Polling;
}

ProfileLhcSingleBeamLine::~ProfileLhcSingleBeamLine()
{
}

/*
**	FUNCTION
**		Decide which vacuum type of line part shall be added
**		for given region type
**
**	PARAMETERS
**		regionType	- Type of region
**
**	RETURNS
**		Enum corresponding to required vacuum type,
**		VacType::None if line part shall not be added.
**
**	CAUTIONS
**		None
*/
int ProfileLhcSingleBeamLine::vacTypeOfRegion(int regionType)
{
	int	result = VacType::None;
	switch(regionType)
	{
	case LhcRegion::BlueOut:
	case LhcRegion::RedOut:
		result = vacType;
		break;
	case LhcRegion::Cross:
		result = VacType::CrossBeam;
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Check if given device shall appear in this profile line. Coordinate
**		of device is NOT checked.
**
**	PARAMETERS
**		pEqp	- Pointer to device in question
**		partIdx	- Index of line part, where device is located, will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
bool ProfileLhcSingleBeamLine::isMyEqp(Eqp *pEqp, int &partIdx)
{
	if(pEqp->getType() != EqpType::VacDevice)
	{
		return false;
	}
	if(!pEqp->isActive(DataEnum::Online))
	{
		return false;	// Mobile equipment - not activated
	}
	if(pEqp->isSkipOnSynoptic())
	{
		return false;
	}

	// Controllable vacuum equipment is checked here
	if(!allowedEqpMask.contains(pEqp->getFunctionalType()))
	{
		if(!pEqp->isSectorBorder())
		{
			return false;
		}
	}

	// Check vacuum type and location
	if(pEqp->getMainPart() != pMainPart)
	{
		return false;
	}
	if(pEqp->getVacType() != vacType)
	{
		switch(pEqp->getVacType())	// Always use .X and .C
		{
		case VacType::CrossBeam:
		case VacType::CommonBeam:
			break;
		default:
			return false;
		}
	}
	// Find line part where device is located
	float	eqpStart;
	if(start < end)
	{
		eqpStart = pEqp->getStart();
	}
	else
	{
		if(pEqp->getStart() >= start)
		{
			eqpStart = pEqp->getStart();
		}
		else
		{
			eqpStart = pEqp->getStart() + DataPool::getInstance().getLhcRingLength();
		}
	}
	for(partIdx = 0 ; partIdx < pPartList->count() ; partIdx++)
	{
		VacLinePart *pPart = pPartList->at(partIdx);
		if((pPart->getStartPos() <= eqpStart) && (eqpStart <= pPart->getEndPos()))
		{
			break;
		}
	}
	return true;
}

/*
**	FUNCTION
**		Calculate geometry for all bars in graph.
**
**	PARAMETERS
**		maxViewBarIdx	- Maximum bar index in view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcSingleBeamLine::calcBarGeometry(int &maxViewBarIdx)
{
	// Calculate graph area limits
	leftX = pLogAxis->getAxisLocation();
	if(parent->isHasLinearEqp())
	{
		rightX = pLinAxis->getAxisLocation() - 1;
	}
	else
	{
		rightX = area.right() - 1;
	}
	topLogY = pLogAxis->getScaleTop();
	bottomLogY = pLogAxis->getScaleBottom();
	topLinY = pLinAxis->getScaleTop();
	bottomLinY = pLinAxis->getScaleBottom();

	// Calculate max bar index
	int maxBarIdx = 0, idx;
	ProfileLhcLineItem *pItem;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		if(!pItem->isHidden())
		{
			pItem->setBarIdx(maxBarIdx++);
		}
	}
	if(!maxBarIdx)
	{
		return;
	}
	if(maxBarIdx > maxViewBarIdx)
	{
		maxViewBarIdx = maxBarIdx;
	}

	// Calculate vertical scale coefficients
	// double plotAreaHeight = bottomLogY - topLogY;
	int	useMin, useMax;
	pLogAxis->getLimits(useMin, useMax);
	double scaleHeight = useMax - useMin;
	double scaleMax = pow(10.0, useMax);
	double scaleMin = pow(10.0, useMin);

	// Calculate one bar width and horizontal scale coefficients
	double xScale = (double)(rightX - leftX) / (double)(maxBarIdx);
	double oneBarWidth = xScale * ((double)VacMainView::getProfileBarWidth() / 100.0);
	useNarrowBar = oneBarWidth < 2.0;

	// Calculate geometry of all bars
	float	barCenter = (float)(leftX + 0.5 * xScale);
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		QRect &rect = pItem->getRect();
		if(useNarrowBar)
		{
			rect.setLeft((int)barCenter);
			rect.setWidth(0);
		}
		else
		{
			rect.moveLeft((int)rint(barCenter - oneBarWidth / 2.));
			rect.setRight((int)rint(rect.left() + oneBarWidth));
		}
		if(pItem->isHidden())
		{
			continue;
		}
		barCenter += (float)xScale;
		barVerticalGeom(pItem, scaleMin, scaleMax, scaleHeight, useMin);
	}

	// Recalculate geometry of sector borders: middle between previous/next bars
	int itemIdx = 0;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		if(pItem->isAtSectorBorder())
		{
			int i;
			for(i = itemIdx - 1 ; i >= 0 ; i--)
			{
				ProfileLhcLineItem *pPrev = eqps.at(i);
				if(!pPrev->isHidden())
				{
					pItem->getRect().moveLeft(pPrev->getRect().right());
					break;
				}
			}
			int count = (int)eqps.count();
			for(i = itemIdx + 1 ; i < count ; i++)
			{
				ProfileLhcLineItem *pNext = eqps.at(i);
				if(!pNext->isHidden())
				{
					pItem->getRect().setRight(pNext->getRect().left());
					break;
				}
			}
		}
	}

	// Calculate geometry of all CRYO thermometers
	pLinAxis->getLimits(useMin, useMax);
	ProfileThermometer *pThermoItem;
	for(idx = 0 ; idx < cryoTs.count() ; idx++)
	{
		pThermoItem = cryoTs.at(idx);
		QPoint &point = pThermoItem->getPoint();
		point.setX(parent->bestPositionForEqp(pThermoItem->getRealStart()));
		cryoTVerticalGeom(pThermoItem, useMin, useMax);
	}
}

/*
**	FUNCTION
**		Dump content of line to output file - for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for write
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcSingleBeamLine::dump(FILE *pFile)
{
	if(!DebugCtl::isProfile())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	ProfileLhcLine::dump(pFile);
	fprintf(pFile, "   ------------- SingleBeamLine vacType %X\n", vacType);
}
