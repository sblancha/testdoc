#include "SectorDrawPart.h"

#include <stdlib.h>

////////////////////////////////////////////////////////////////////////////////
// Construction/Destruction
////////////////////////////////////////////////////////////////////////////////
SectorDrawPart::SectorDrawPart()
{
	startEqpIdx = endEqpIdx = -1;
	pLine = NULL;
	pStartPart = pEndPart = NULL;
	drawOrder = 0;
	pColor = NULL;
	circleClose = passed = false;
}

SectorDrawPart::~SectorDrawPart()
{
}

