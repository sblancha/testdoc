//	Implementation of VacReplayCtl class
/////////////////////////////////////////////////////////////////////////////////

#include "VacReplayCtl.h"

#include "ReplayPool.h"

#include <QPainter>

#include <stdio.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

VacReplayCtl::VacReplayCtl(QWidget *parent, Qt::WindowFlags flags) :
		QWidget(parent, flags)
{
	calculateMinSize();
	ReplayPool &pool = ReplayPool::getInstance();
	connect(&pool, SIGNAL(dpeListChanged()), this, SLOT(poolDpeListChange()));
}

VacReplayCtl::~VacReplayCtl()
{
}

/*
**	FUNCTION
**		Calculate and set for this widget minimum allowed size:
**		Make sure size if large enough to show number of DPEs (4 digits max)
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacReplayCtl::calculateMinSize(void)
{
	QRect rect = fontMetrics().boundingRect("9999");
	QSize minSize(rect.width() + 4, rect.height() + 4);
	setFixedSize(minSize);
}

/*
**	FUNCTION
**		Override QWidget's method: recalculate minimum size after font change
**
**	ARGUMENTS
**		newFont	- New font to be set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacReplayCtl::setFont(const QFont &newFont)
{
	QWidget::setFont(newFont);
	calculateMinSize();
}

/*
**	FUNCTION
**		Slot recieving dpeListChanged() signal of replay pool
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacReplayCtl::poolDpeListChange(void)
{
	update();
	emit dpeListChanged();
}


void VacReplayCtl::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	painter.fillRect(rect(), Qt::cyan);
	int dpeCount = ReplayPool::getInstance().getDpeCount();
	if(dpeCount)
	{
		char buf[32];
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d", dpeCount);
#else
		sprintf(buf, "%d", dpeCount);
#endif
		painter.drawText(2, height() - 2, buf);
	}
}

