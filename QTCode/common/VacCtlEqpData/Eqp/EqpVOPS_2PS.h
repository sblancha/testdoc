#ifndef	EqpVOPS_2PS_H
#define	EqpVOPS_2PS_H

//	Double overpressure switch 

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVOPS_2PS : public Eqp
{
	Q_OBJECT

public:
	EqpVOPS_2PS(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVOPS_2PS(const EqpVOPS_2PS &src) : Eqp(src) {}
#endif
	~EqpVOPS_2PS();

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
	virtual void getPartColor(int part, QColor &color, DataEnum::DataMode mode);
	virtual void getPartStateString(int part, QString &string, DataEnum::DataMode mode);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }

	public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, bool plcAlarm, QString &string);
	virtual void getPartColorA(unsigned rr1, bool plcAlarm, QColor &color);
	virtual void getPartColorB(unsigned rr1, bool plcAlarm, QColor &color);

	virtual void getPartStateStringA(unsigned rr1, bool plcAlarm, QString &string);
	virtual void getPartStateStringB(unsigned rr1, bool plcAlarm, QString &string);
#endif
};

#endif	// EqpVOPS_2PS_H
