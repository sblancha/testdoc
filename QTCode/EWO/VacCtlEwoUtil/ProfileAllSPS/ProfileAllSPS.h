#ifndef PROFILEALLSPS_H
#define	PROFILEALLSPS_H

// Dialog displaying pressure profile of all sextants of SPS ring

#include "ProfileAllSPSWidget.h"

#include <qdialog.h>

class ProfileAllSPS : public QDialog
{
	Q_OBJECT

public:
	ProfileAllSPS();
	~ProfileAllSPS();

protected:
	// Widget with images
	ProfileAllSPSWidget	*pWidget;

	virtual void resizeEvent(QResizeEvent *e);
};

#endif	// PROFILEALLSPS_H
