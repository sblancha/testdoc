# Project file for qmake utility to build DateEditEwo EWO
#
#	01.04.2010	L.Kopylov
#		Initial version
#
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = TimeEditEwo

HEADERS = TimeEditEwo.h

SOURCES = TimeEditEwo.cpp

