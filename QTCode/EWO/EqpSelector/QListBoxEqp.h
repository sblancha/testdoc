#ifndef	QLISTBOXEQP_H
#define	QLISTBOXEQP_H

// Subclass of QListBoxText, holding reference to one Eqp and
// displaying visible name of this Eqp in QListBox

#include <qlistwidget.h>

class Eqp;

class QListBoxEqp : public QListWidgetItem
{
public:
	QListBoxEqp(QListWidget *pListBox, Eqp *pEqp);
//	QListBoxEqp(Eqp *pEqp);
	~QListBoxEqp();

	// Access
	Eqp *getEqp(void) { return pEqp; }

protected:
	// Pointer to device
	Eqp		*pEqp;
};

#endif	// QLISTBOXEQP_H
