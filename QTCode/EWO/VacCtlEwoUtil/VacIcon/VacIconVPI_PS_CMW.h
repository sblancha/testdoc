#ifndef	VACICONVPI_PS_CMW_H
#define	VACICONVPI_PS_CMW_H

//	Icon for VPI_PS_CMW

#include "VacIcon.h"

class EqpVP_VG_PS_CMW;

class VACCTLEWOUTIL_EXPORT VacIconVPI_PS_CMW : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPI_PS_CMW(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPI_PS_CMW();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVP_VG_PS_CMW *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter);
	virtual void setPainterForImage(QPainter &painter);
};

#endif	// VACICONVPI_PS_CMW_H
