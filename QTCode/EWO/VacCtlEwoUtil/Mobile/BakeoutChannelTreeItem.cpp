//	Implementation of BakeoutChannelTreeItem class
/////////////////////////////////////////////////////////////////////////////////

#include "BakeoutChannelTreeItem.h"

#include "MobileColorList.h"
#include "MobilePool.h"

#include <QPixmap>

#include <stdio.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

BakeoutChannelTreeItem::BakeoutChannelTreeItem(QTreeWidgetItem *parent, int channel, enum BakeoutWorkDp::ChannelValueType valueType)
	: QTreeWidgetItem(parent, MobileTreeItemBakeoutChannel)
{
	this->channel = channel;
	this->valueType = valueType;
	QPixmap *pm = MobileColorList::getBakeoutIcon(channel);
	setIcon(0, QIcon(*pm));
	setText(0, MobilePool::findBakeoutChannelDpeName(channel, valueType));
}

BakeoutChannelTreeItem::~BakeoutChannelTreeItem()
{
}

