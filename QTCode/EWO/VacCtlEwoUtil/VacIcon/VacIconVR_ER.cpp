/**
* @brief Class implementation for widget Vacuum contRol - bakE-out Rack (VR_ER)
* @see VacIcon
*/
#include "VacIconVR_ER.h"
#include "VacMainView.h"
#include "EqpVR_ER.h"
#include <qpainter.h>

#include <QMouseEvent>

VacIconVR_ER::VacIconVR_ER(QWidget *parent, Qt::WindowFlags f) :
VacIcon(parent, f) {
	pEqp = NULL;
	setFixedSize(22, 22);
}

VacIconVR_ER::~VacIconVR_ER() {
	disconnect();
}
/**
@brief FUNCTION Calculate geometry for symbol areas rectangles for current direction and beamDirection.
*/
VacIconGeometry VacIconVR_ER::getGeometry(const VacEqpTypeMask & /* mask */) {
	return VacIconGeometry(22, 22, 11, 11, 0); //(int width, int height, int x /*origin for pipe*/, int y /*origin for pipe*/, int verticalPos /*insertion offset*/)
}
/**
@brief FUNCTION Set reference to equipment for this icon
@param[in]   pEqp	Pointer to device to be used by this icon
*/
void VacIconVR_ER::setEqp(Eqp *pEqp) {
	disconnect();
	if (pEqp) {
		Q_ASSERT_X(pEqp->inherits("EqpVR_ER"), "VacIconVR_ER::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVR_ER *)pEqp;
	}
	forceRedraw();
}
/**
@brief FUNCTION Set new data acquisition mode for this icon
@param[in]   newMode	New data aqn mode (online, replay...)
*/
void VacIconVR_ER::setMode(DataEnum::DataMode newMode) {
	mode = newMode;
	forceRedraw();
}
/**
@brief FUNCTION Connect to DPEs of device, return true if done
*/
bool VacIconVR_ER::connect(void)
{
	if (connected || (!pEqp)){
		return false;
	}
	QStringList dpes;
	dpes.append("RR1");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}
/**
@brief FUNCTION Disconnect DPEs of device, return true if done
*/
bool VacIconVR_ER::disconnect(void) {
	bool result = false;
	if (connected && pEqp) {
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}
/**
@brief FUNCTION Calculate text and rectangle for tooltip of this icon, return true if tooltip shown
@param[in]  pos		Position of mouse pointer WITHIN this widget
@param[out]	text	Variable where this method shall write tooltip text to be shown
@param[out]	rect	Variable where this method shall write rectangle for visibility	of returned tooltip: tooltip will be hidden when mouse pointer left
*/
bool VacIconVR_ER::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if (pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}
/**
@brief FUNCTION Icon drawing implementation
@param[in]  pEvent	Pointer to paint event
*/
void VacIconVR_ER::paintEvent(QPaintEvent * /* pEvent */) {
	unsigned rr1 = 0;
	bool comAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if (pEqp) {
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		comAlarm = pEqp->isComAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	// Whatever direction the icon remain the same

	// Save coordinate ref to be restore after the draw
	painter.save();

	setPainterForImage(painter, ctlStatus, comAlarm);

	// Blinking behavior
	if (pEqp)
		isBlinking = pEqp->isAlertNotAck();
	else
		isBlinking = false;
	if (isBlinking && !isBlinkVisible) {
		painter.restore();	// Original coordinate system - see save() above
		return; // hiden phase of blinking
	}
		
	drawImage(painter, rr1, comAlarm);

	// Draw selection ON TOP of image
	painter.restore();	// Original coordinate system - see save() above
	if (pEqp) {
		if (pEqp->isSelected()) {
			drawSelection(painter);
		}
	}
}

void VacIconVR_ER::drawImage(QPainter &painter, unsigned /* rr1 */, bool /* comAlarm */) {
	// No pipe to be drawn for this icon

	// Whatever direction the icon remain the same
	// Draw the Rectangle
	painter.setPen(Qt::black);
	int xRect = 1; //UpperLeftCorner x
	int yRect = 1; //UpperLeftCorner y
	int wRect = width() - 2*xRect;
	int hRect = height() - yRect -2;
	painter.drawRect(xRect, yRect, wRect, hRect);//(int x, int y, int width, int height) x=0, y=0 -> center; x axis goes to right, y axis goes to bottom
	
	// Draw the Bakeout symbol
	painter.setPen(QPen(Qt::black, 1));
	int borderOffset = 3;
	// Draw bottom line
	int x1Surface = xRect + borderOffset;
	int y1Surface = yRect + hRect - borderOffset;
	int x2Surface = xRect + wRect - borderOffset;
	painter.drawLine(x1Surface, y1Surface, x2Surface, y1Surface);
	
	// Draw hot waves
	painter.setPen(QPen(Qt::black, 1));
	painter.translate(xRect + borderOffset + 3, yRect + (hRect - (borderOffset + 2)) );// move reference to start of first wave
	QPointF startWave, midWave, c1Wave, endWave, c2Wave;
	QPainterPath waveA, waveB;
	//for loop for each wave (3 waves)
	for (int i = 0; i < 3; i++) {
		startWave = QPointF(0, 0);
		waveA = QPainterPath(startWave);
		midWave = QPointF(0, -6);
		c1Wave = QPointF(3, -3);
		waveA.quadTo(c1Wave, midWave);
		endWave = QPointF(0, -12);
		c2Wave = QPointF(-3, -9);
		waveB = QPainterPath(midWave);
		waveB.quadTo(c2Wave, endWave);
		painter.drawPath(waveA);
		painter.drawPath(waveB);
		painter.translate(4, 0);// move reference to start of next wave
	}
	return;
}


void VacIconVR_ER::setPainterForImage(QPainter &painter, Eqp::CtlStatus ctlStatus, bool comAlarm) {
	QColor	color(COLOR_INVALID_VRER);
	switch (ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if (!comAlarm) {
			if (pEqp) {
				pEqp->getMainColor(color, mode);
			}
			else {
				color.setRgb(COLOR_NOEQP_VRER);
			}
		}
		break;
	}
	painter.setBrush(color);
	painter.setPen(Qt::gray);
}