//	Implementation of QListBoxEqp class
////////////////////////////////////////////////////////////////////////////////

#include "QListBoxEqp.h"

#include "Eqp.h"


QListBoxEqp::QListBoxEqp(QListWidget *pListBox, Eqp *pEqp) :
	QListWidgetItem(pEqp->getVisibleName(), pListBox, UserType)
{
	this->pEqp = pEqp;
}

/*
QListBoxEqp::QListBoxEqp(Eqp *pEqp) :
	QListWidgetItem(pEqp->getVisibleName())
{
	this->pEqp = pEqp;
}
*/

QListBoxEqp::~QListBoxEqp()
{
}

