//	Implementation of XmlSynViewer class
/////////////////////////////////////////////////////////////////////////////////

#include "XmlSynViewer.h"

XmlSynViewer::XmlSynViewer(QWidget *parent, const QString &fileName) : QWidget(parent)
{
	QString errMsg;
	pSynoptic = XmlSynoptic::createFromFile(this, fileName, DataEnum::Online, errMsg);
	printf("Result is %X, errMsg %s\n", (unsigned)pSynoptic, errMsg.toAscii().constData());
	if(pSynoptic)
	{
		setFixedWidth(pSynoptic->getWidth());
		setFixedHeight(pSynoptic->getHeight());
	}
}

XmlSynViewer::~XmlSynViewer()
{
	if(pSynoptic)
	{
		delete pSynoptic;
	}
}

void XmlSynViewer::paintEvent(QPaintEvent *pEvent)
{
	QWidget::paintEvent(pEvent);
	if(pSynoptic)
	{
		QPainter painter(this);
		pSynoptic->draw(painter);
	}
}

