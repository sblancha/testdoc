/**
* @brief Class implementation for Vacuum Alarm - Relay of Ion pump controller (VA_RI)
* @see Eqp
*/
#include "EqpVA_RI.h"
#include "EqpVR_PI_CHAN.h"

#include "DataPool.h"
#include "FunctionalTypeVA.h"
#include "EqpMsgCriteria.h"
#include "DpConnection.h"
#include "DevListCriteria.h"
#include "ResourcePool.h"
#include "MobileType.h"
#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif
#include <QColor>

EqpVA_RI::EqpVA_RI(const Eqp &source) : Eqp(source) {
	rr1Online = rr1Replay = 0;
	alertActive = alertNotAck = false;
	functionalType = FunctionalType::VA;
	channelSourceDef = getAttrValue("Source").toInt();
	thresholdDef = getAttrValue("Threshold").toDouble();
	hysteresisPctDef = getAttrValue("Hysteresis").toInt();
	masterDp = getAttrValue("MasterName");
	relayNb = getAttrValue("Relay").toInt();
}
EqpVA_RI::~EqpVA_RI(){
}
/**
@brief After all devices have been read from file - find master and attach to source
*/
void EqpVA_RI::postProcess(void) {
	const QString masterDp = getAttrValue("MasterName");
	Eqp *pMaster;
	if (!masterDp.isEmpty()) {
		DataPool &pool = DataPool::getInstance();
		Eqp *pEqp = pool.findEqpByDpName(masterDp.toLatin1());
		if (pEqp) {
			Q_ASSERT(pEqp->inherits("EqpVR_PI"));
			pMaster = (EqpVR_PI *)pEqp;
			// No need to stay PERMANENTLY connected to master's selection
			// selected = pMaster->isSelected();
			// QObject::connect(pMaster, SIGNAL(selectChanged(Eqp *)), this, SLOT(masterSelectChange(Eqp *)));
		}
	}
	attachToSourceChange(true);
}

/**
@brief FUNCTION OVERRIDE Return list of all DPEs, related to state of device for state history.
@param[out]   stateDpes		list of name of dpes used to calculate state.
*/
void EqpVA_RI::getStateDpes(QStringList &stateDpes) {
	stateDpes.clear();
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "errorSt");
}
/**
@brief FUNCTION Connect DPEs of interest for widget animation.
@param[in]   *pDst		Class to be notified about change in this device.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVA_RI::connect(InterfaceEqp *pDst, DataEnum::DataMode mode) {
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "AL1:_alert_hdl.._ack_possible", mode);
	connectDpe(pDst, "channelSourceSt", mode);
	connectDpe(pDst, "thresholdSt", mode);
	connectDpe(pDst, "hysteresisPctSt", mode);
}
/**
@brief FUNCTION Disconnect DPEs of interest for animation.
@param[in]   *pDst		Class to be notified about change in this device.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVA_RI::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode) {
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "AL1:_alert_hdl.._ack_possible", mode);
	disconnectDpe(pDst, "channelSourceSt", mode);
	disconnectDpe(pDst, "thresholdSt", mode);
	disconnectDpe(pDst, "hysteresisPctSt", mode);
}
/**
@brief FUNCTION Process new online value arrived from front end to update widget animation.
@param[in]   dpe		name of dpe updated.
@param[in]	 value		new value of the dpe.
*/
void EqpVA_RI::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp) {
	if (!strcmp(dpe, "RR1"))	{
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (rr1Online == newValue)
				return;
			rr1Online = newValue;
		}
	}
	else if (!strcmp(dpe, "channelSourceSt"))	{
		if (value.canConvert(QVariant::Int)) {
			int newValue = value.toInt();
			if (channelSourceStOnline == newValue)
				return;
			channelSourceStOnline = newValue;
			attachToSourceChange(false);
			emit positionChanged(this, DataEnum::Online);
		}
	}
	else if (!strcmp(dpe, "thresholdSt"))	{
		if (value.canConvert(QVariant::Double)) {
			double newValue = value.toDouble();
			if (thresholdStOnline == newValue)
				return;
			thresholdStOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "hysteresisPctSt"))	{
		if (value.canConvert(QVariant::Int)) {
			int newValue = value.toInt();
			if (hysteresisPctStOnline == newValue)
				return;
			hysteresisPctStOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "AL1")) {
		if (value.canConvert(QVariant::Bool)) {
			bool newValue = value.toBool();
			if (alertNotAck == newValue)
				return;
			alertNotAck = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}
#ifndef PVSS_SERVER_VERSION
/**
@brief FUNCTION Process new replay value arrived from front end to update widget animation.
@param[in]   dpe		name of dpe updated.
@param[in]	 value		new value of the dpe.
*/
void EqpVA_RI::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp) {
	if (!strcmp(dpe, "RR1")) {
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (rr1Replay == newValue)
				return;
			rr1Replay = newValue;
		}
	}
	else if (!strcmp(dpe, "channelSourceSt"))	{
		if (value.canConvert(QVariant::Int)) {
			int newValue = value.toInt();
			if (channelSourceStReplay == newValue)
				return;
			channelSourceStReplay = newValue;
			attachToSourceChange(false);
		}
	}
	else if (!strcmp(dpe, "thresholdSt"))	{
		if (value.canConvert(QVariant::Double)) {
			double newValue = value.toDouble();
			if (thresholdStReplay == newValue)
				return;
			thresholdStReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "hysteresisPctSt"))	{
		if (value.canConvert(QVariant::Int)) {
			int newValue = value.toInt();
			if (hysteresisPctStReplay == newValue)
				return;
			hysteresisPctStReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif
/**
@brief SLOT FUNCTION activated by communication object when communication state change
@param[in]   pSrc		Pointer of emmiter (communication object).
@param[in]   dpeName	DPE change source (see enum above)
@param[in]	 value		New value of the dpe.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVA_RI::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp) {
	bool alarm = !pSrc->isAlive(mode);
	if (mode == DataEnum::Replay) {
		if (alarm == plcAlarmReplay) {
			return;
		}
		plcAlarmReplay = alarm;
	}
	else {
		if (alarm == plcAlarmOnline) {
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}
/**
@brief FUNCTION Return main color of device
@param[out]   color		main color value
@param[in]	  mode		Aqn mode (online, replay...)
*/
#ifndef PVSS_SERVER_VERSION
void EqpVA_RI::getMainColor(QColor &color, DataEnum::DataMode mode) {
	if (mode == DataEnum::Replay)
		getMainColor(rr1Replay, plcAlarmReplay, isParamNotDef(), color);
	else
		getMainColor(rr1Online, plcAlarmOnline, isParamNotDef(), color);
}
#endif
/**
@brief PROTECTED FUNCTION Calculate main color of device
@details Calculate color according to RR1:
@param[in]   state			State value
@param[in]	 comAlarm	Alarm value of the Tcp communication object
@param[out]	 color			Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVA_RI::getMainColor(unsigned rr1, bool comAlarm, bool isNotDefaultParameters, QColor &color) {
	// First check Communication is Ok and eqp valid
	if (comAlarm || !(rr1 & RR1_16B_GP_VALID)) {
		color.setRgb(COLOR_GP_INVALID);
	}
	// Then check if error
	else if (rr1 & RR1_16B_GP_ERROR) {
		color.setRgb(COLOR_GP_ERROR);
	}
	// Then check if forced
	else if (rr1 & RR1_FORCED) {
		color.setRgb(COLOR_GP_WARNING);
	}
	// Then check if alarm is BAD
	else if (rr1 & RR1_16B_GP_OFF) {
		if (isNotDefaultParameters) color.setRgb(COLOR_GP_ERROR);
		else color.setRgb(COLOR_GP_BAD);
	}
	else if (rr1 & RR1_16B_GP_ON){
		if (isNotDefaultParameters) color.setRgb(COLOR_GP_ON);
		else color.setRgb(COLOR_GP_UR);
	}
	else {
		color.setRgb(COLOR_GP_INVALID);
	}
}
#endif
/**
@brief FUNCTION Return main state string of device
@param[out]   string	main state string value
@param[in]	  mode		Aqn mode (online, replay...)
*/
#ifndef PVSS_SERVER_VERSION
void EqpVA_RI::getMainStateString(QString &string, DataEnum::DataMode mode) {
	if (mode == DataEnum::Replay) {
		getMainStateString(rr1Replay, plcAlarmReplay, string);
	}
	else {
		getMainStateString(rr1Online, plcAlarmOnline, string);
	}
}
#endif
/**
@brief PROTECTED FUNCTION Calculate main state string of device
@details String value get from ressource file
@param[in]   rr1			read register 1 value
@param[in]   state			State value
@param[in]	 comAlarm	Alarm value of the Tcp communication object
@param[out]	 string			Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVA_RI::getMainStateString(unsigned rr1, bool comAlarm, QString &string)
{
	string = "";
	if (comAlarm) {
		string = "Communication Error";
		return;
	}
	if (!(rr1 & RR1_16B_GP_VALID))	{
		string = "Invalid";
		return;
	}
	if (rr1 & RR1_16B_GP_ERROR) {
		string = "ERROR";
		return;
	}
	if (rr1 & RR1_FORCED) {
		string = "FORCED ";
	}
	if (rr1 & RR1_16B_GP_OFF) {
		string += "BAD";
		return;
	}
	if (rr1 & RR1_16B_GP_ON) {
		string += "OK";
		return;
	}
	string += "Undefined";
}
#endif
/**
@brief FUNCTION Calculate main state string for contextual menu of device
@details Same as getMainStateString(), ":"+PR already in mainState
@param[in]	  mode		Aqn mode (online, replay...)
@param[out]	 string		Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVA_RI::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode) {
	getMainStateString(string, mode);
}
#endif
/**
@brief FUNCTION Calculate tool tip
@param[in]	  mode		Aqn mode (online, replay...)
@param[out]	 string		Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVA_RI::getToolTipString(QString &string, DataEnum::DataMode mode) {
	if (mode == DataEnum::Replay) {
		getToolTipString(rr1Replay, plcAlarmReplay, string);
	}
	else {
		getToolTipString(rr1Online, plcAlarmOnline, string);
	}
}
#endif
/**
@brief PROTECTED FUNCTION Calculate tool tip
@param[in]   rr1			read register 1 value
@param[in]   state			State value
@param[in]	 comAlarm	Alarm value of the Tcp communication object
@param[out]	 string			Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVA_RI::getToolTipString(unsigned rr1, bool comAlarm, QString &string) {
	QString stateString;
	getMainStateString(rr1, comAlarm, stateString);
	string = getVisibleName();
	string += ": ";
	string += stateString;
}
#endif
/**
@brief FUNCTION Calculate main state string and color for moment in the past, data are taken from state archive data pool
@param[in]   pPool			Pointer to state history data pool
@param[in]	 ts				Moment when state is needed
@param[out]	 stateString	Variable where resulting state string will be written
@param[out]	 color			Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpVA_RI::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
	QString &stateString, QColor &color) {
	stateString = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("RR1");
	QList<QVariant *> values;
	if (!pPool->getDpValues(dpName, ts, dpeNames, values))
		return;
	if (values.isEmpty())
		return;
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, false, stateString);
	getMainColor(rr1, false, false, color);
	while (!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif
/**
@brief Check if status parameters differ from defaults
*/
bool EqpVA_RI::isParamNotDef() {
	if (mode == DataEnum::Replay) {
		return false;
	}
	else {
		if (channelSourceDef != channelSourceStOnline) return true;
		if (((thresholdDef / thresholdStOnline) > 1.1) || ((thresholdDef / thresholdStOnline) < 0.9)) return true;
		if (hysteresisPctDef != hysteresisPctStOnline) return true;
	}
	return false;
}
/**
@brief FUNCTION Decide which DPEs shall be read to check if this device matches current device list criteria or not
@param[out]	 dpes		Variable where all required DPEs will be added
*/
#ifndef PVSS_SERVER_VERSION
void EqpVA_RI::getDpesForDevList(QStringList &dpes) {
	dpes.clear();
	const QString plcName = findPlcName();
	if (!plcName.isEmpty())	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if (pPlc) {
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "RR1");
}
#endif
/**
@brief FUNCTION Return code to check if device in recent state matches device list criteria or not
@details Return 0 = Device does not match criteria; 1 = Device matches criteria; 2 = More information is needed (history)-NOT for valves
*/
#ifndef PVSS_SERVER_VERSION
int EqpVA_RI::checkDevListCrit(void) {
	DevListCriteria &criteria = DevListCriteria::getInstance();
	DataEnum::DataMode mode = criteria.getMode();
	if (criteria.isEmpty())	{
		return 1;	// No criteria - all devices are needed
	}
	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if (!plcName.isEmpty())	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if (pPlc) {
			if (!pPlc->isAlive(mode)) {
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}
	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if (!(rr1 & RR1_16B_GP_VALID)) {
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	unsigned state = DevListCriteria::EqpStateOther;
	if ((rr1 & (RR1_16B_GP_ON | RR1_16B_GP_OFF)) == RR1_16B_GP_ON) {
		state = DevListCriteria::EqpStateNormal;
	}
	else if ((rr1 & (RR1_16B_GP_ON | RR1_16B_GP_OFF)) == RR1_16B_GP_OFF) {
		state = DevListCriteria::EqpStateAbnormal;
	}
	if (criteria.generalStateMatches(state)) {
		return 1;
	}
	return 0;
}
#endif
/**
@brief FUNCTION Check if device matches criteria for E-mail/SMS notification
@param[in, out]   pCriteria	Pointer to criteria to check
*/
void EqpVA_RI::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if (pCriteria->getFunctionalType() != functionalType) {
		return;
	}
	if (pCriteria->getType() == EqpMsgCriteria::None) {
		return;
	}
	// Without PLC connection does not match any criteria
	if (plcAlarmOnline) {
		return;
	}
	// In invalid state does not match any criteria
	if (!(rr1Online & RR1_16B_GP_VALID)) {
		return;
	}
	// State is valid - decide based on criteria
	unsigned maskedRr1 = rr1Online & (RR1_16B_GP_ON | RR1_16B_GP_OFF);
	switch (pCriteria->getType()) {
	case EqpMsgCriteria::MainState:
		switch (pCriteria->getSubType()) {
		case FunctionalTypeVA::Bad:
			if (maskedRr1 == RR1_16B_GP_OFF) {
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else {
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVA::Ok:
			if (maskedRr1 == RR1_16B_GP_ON) {
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else {
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	default:
		break;
	}
}
/**
@brief PUBLIC Find ion pumps and addTargetExt if needed, required to put icon on the correct ion pumps
@param[in]   channelSource	channel number of the source of alarm
*/
void EqpVA_RI::attachToSourceChange(bool isInit) {
	int channelSource;
	bool isNeededSynViewUpdate = false;
	if (isInit) {
		channelSource = channelSourceDef;
	}
	else {
		channelSource = channelSourceStOnline;
	}
	DataPool &pool = DataPool::getInstance();
	// first detach to all
	foreach (Eqp *pEqp, pool.getOrderedEqpList()) {
		if (pEqp->removeExtTargets(this) > 0) {
			isNeededSynViewUpdate = true;
		}
	}
	// find channel eqps of the master (=controller)
	QList<Eqp *> channels = pool.findEqpsByMasterDp(this->getMasterDp());
	// find the channel
	Eqp * pChannel = NULL;
	foreach (Eqp * pEqp, channels) {
		if (!(pEqp->inherits("EqpVR_PI_CHAN"))){
			continue;
		}
		EqpVR_PI_CHAN * pChan = (EqpVR_PI_CHAN *)pEqp;
		if (pChan->getChannelNb() == channelSource) {
			pChannel = pChan;
			break;
		}
	}
	if (!pChannel) {
		return;
	}
	// find ion pump connected to this channel
	QList<Eqp *> ionPumps = pool.findEqpsByMasterDp(pChannel->getDpName());
	// check if ion pump eqp VP_IP has this as external target if not addExtTarget
	foreach (Eqp * pIonPump, ionPumps) {
		bool isAlreadyTarget = false;
		foreach (Eqp * pTargets, pIonPump->getExtTargets()) {
			if (this == pTargets) {
				isAlreadyTarget = true;
			}
		}
		if (!isAlreadyTarget) {
			pIonPump->addExtTarget(this);
			isNeededSynViewUpdate = true;
		}
	}
	if (isNeededSynViewUpdate) {
		emit positionChanged(this, DataEnum::Online);
	}
}
/**
*@brief used by API LhcVacEqpMaster()
*/
void EqpVA_RI::getMaster(QString &masterDp, int &masterChannel)
{
	masterDp = getAttrValue("MasterName");
	if (masterDp.isEmpty()){
		masterChannel = 0;
		return;
	}
	masterChannel = relayNb;
}
/**
*@brief used by API LhcVacEqpTargets()
*/
void EqpVA_RI::getTargets(QStringList &targets) {
	DataPool &pool = DataPool::getInstance();
	// Build list of attribute given targets relationship
	QList<QString> vvsItlAttrList;
	vvsItlAttrList.append("PrevInterlockSource1");
	vvsItlAttrList.append("PrevInterlockSource2");
	vvsItlAttrList.append("PrevInterlockSource3");
	vvsItlAttrList.append("PrevInterlockSource4");
	vvsItlAttrList.append("PrevInterlockSource5");
	vvsItlAttrList.append("PrevInterlockSource6");
	vvsItlAttrList.append("PrevInterlockSource7");
	vvsItlAttrList.append("PrevInterlockSource8");
	vvsItlAttrList.append("NextInterlockSource1");
	vvsItlAttrList.append("NextInterlockSource2");
	vvsItlAttrList.append("NextInterlockSource3");
	vvsItlAttrList.append("NextInterlockSource4");
	vvsItlAttrList.append("NextInterlockSource5");
	vvsItlAttrList.append("NextInterlockSource6");
	vvsItlAttrList.append("NextInterlockSource7");
	vvsItlAttrList.append("NextInterlockSource8");
	QString vext_alarmAttr = "Source1";
	// Parsing pool
	foreach(Eqp *pEqp, pool.getOrderedEqpList()) {
		if (pEqp->getFunctionalType() == FunctionalType::VV) {
			foreach(QString attr, vvsItlAttrList) {
				QString attrValue = pEqp->getAttrValue(attr.toLatin1());
				if (attrValue == dpName) {
					targets.append((QString)pEqp->getDpName());
				}
			}
		}
		else if (pEqp->getFunctionalType() == FunctionalType::EXT_ALARM) {
			QString attrValue = pEqp->getAttrValue(vext_alarmAttr.toLatin1());
			if (attrValue == dpName) {
				targets.append((QString)pEqp->getDpName());
			}
		}
	}
}