#ifndef	IMAGINEITEMTEXT_H
#define	IMAGINEITEMTEXT_H

#include "ImageItem.h"

#include <QFont>

class VACCTLEWOUTIL_EXPORT ImageItemText : public ImageItem
{
public:
	ImageItemText();
	~ImageItemText();

	virtual void draw(QPainter & painter);
	virtual bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	// Access
	inline const QString &getText(void) const { return text; }
	inline void setText(QString value) { text = value; }
	inline const QFont &getFont(void) const { return font; }
	inline void setFont(const QFont &inFont) { font = inFont; }
	inline int getStartX(void) const { return startX; }
	inline void setStartX(int coord) { startX = coord; }
	inline int getStartY(void) const { return startY; }
	inline void setStartY(int coord) { startY = coord; }
	inline int getSizeX(void) const { return sizeX; }
	inline void setSizeX(int size) { sizeX = size; }
	inline int getSizeY(void) const { return sizeY; }
	inline void setSizeY(int size) { sizeY = size; }
	inline int getFlags(void) const { return flags; }
	inline void setFlags(int value) { flags = value; }

protected:
	// Text to draw
	QString	text;

	// Font used to draw text
	QFont	font;
	
	// Start/end coordinates
	int		startX;
	int		startY;

	// Sizes
	int		sizeX;
	int		sizeY;

	// Border width
	int		borderOffset;

	// Filled flag
	bool	filled;

	// Bordered flag
	bool	bordered;

	// Fit to text flag
	bool	fit;

	// Drawing flags - see QPainter::drawText()
	int 	flags;

	virtual void parseProp(QXmlStreamReader &reader);
	virtual void parseFont(QXmlStreamReader &reader);
	virtual void parseText(QXmlStreamReader &reader);

};

#endif	// IMAGINEITEMTEXT_H
