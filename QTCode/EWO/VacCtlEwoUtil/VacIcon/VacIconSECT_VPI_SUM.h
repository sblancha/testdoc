#ifndef VACICONSECT_VPI_SUM_H
#define	VACICONSECT_VPI_SUM_H

// Icon for summary of all ion pumps in one sector

#include "VacIcon.h"

class EqpSECT_VPI_SUM;

class VACCTLEWOUTIL_EXPORT VacIconSECT_VPI_SUM : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconSECT_VPI_SUM(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconSECT_VPI_SUM();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpSECT_VPI_SUM *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACICONSECT_VPI_SUM_H
