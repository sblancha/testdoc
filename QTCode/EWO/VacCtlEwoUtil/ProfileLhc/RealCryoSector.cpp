//	Implementation of RealCryoSector class
/////////////////////////////////////////////////////////////////////////////////

#include "RealCryoSector.h"

#include "DataPool.h"
#include "DebugCtl.h"

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

RealCryoSector::RealCryoSector(Sector *pSector, float viewStart, float viewEnd)
{
//	qDebug("RealCryoSector::RealCryoSector(%s, %f, %f) - start\n", pSector->getName(), viewStart, viewEnd);
	this->pSector = pSector;
	if(viewStart < viewEnd)		// if(start < end)
	{
		startInside = pSector->getStart() >= viewStart;
		endInside = pSector->getEnd() <= viewEnd;
		start = pSector->getStart();
		end = pSector->getEnd();
	}
	else
	{
		float lhcRingLength = DataPool::getInstance().getLhcRingLength();
		if(pSector->getStart() >= viewStart)
		{
			start = pSector->getStart();
		}
		else
		{
			start = pSector->getStart() + lhcRingLength;
		}
		if(pSector->getEnd() >= viewStart)
		{
			end = pSector->getEnd();
		}
		else
		{
			end = pSector->getEnd() + lhcRingLength;
		}

		if((pSector->getStart() > viewStart) || (pSector->getEnd() >= viewStart))
		{
			startInside = pSector->getStart() > viewStart;
			endInside = true;
		}
		else
		{
			startInside = true;
			endInside = pSector->getEnd() <= viewEnd;
		}
//		qDebug("      RealCryoSector::RealCryoSector(%s) : result: start = %f end = %f\n", pSector->getName(), start, end);
	}
	startBarIdx = endBarIdx = 0;
	leftX = rightX = 0;
	startPassed = false;
}

/*
**	FUNCTION
**		Dump content to output file - for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for write
**		idx		- Index of this sector in list
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void RealCryoSector::dump(FILE *pFile, int idx)
{
	if(!DebugCtl::isProfile())
	{
		return;
	}
	fprintf(pFile, "   %03d: %s from %f to %f (real from %f to %f) START %s END %s\n", idx,
		pSector->getName(), start, end, pSector->getStart(), pSector->getEnd(),
		(startInside ? "INSIDE" : "OUTSIDE"),
		(endInside ? "INSIDE" : "OUTSIDE") );
	fprintf(pFile, "     bars: %d %d, coord %d %d\n", startBarIdx, endBarIdx,
		leftX, rightX);
}

