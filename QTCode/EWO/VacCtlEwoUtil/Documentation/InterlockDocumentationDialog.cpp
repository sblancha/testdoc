#include "InterlockDocumentationDialog.h"

#include <qlayout.h>
#include <qpushbutton.h>
#include <qmenu.h>
#include <QScrollBar>
#include <qapplication.h>
#include <qdesktopwidget.h>
#include <qpainter.h>
#include <qprinter.h>
#include <qfiledialog.h>

#include "InterlockView.h"

InterlockDocumentationDialog::InterlockDocumentationDialog(QString valveName)
{
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle("Interlock Documentation");

	pView = NULL;
	this->valveName = valveName;
	buildLayout();
	//setTitle();

	buildView(true);
}

InterlockDocumentationDialog::~InterlockDocumentationDialog()
{

}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void InterlockDocumentationDialog::buildLayout(void)
{
	// 1) Main layout - menu, labels and combobox on top, scrolled view on bottom
	QVBoxLayout *mainBox = new QVBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);

	// 2) Horizontal layout with
	//	- buttons for activating popup menus
	//	- title label
	//	- ComboBox with main part selection
	QHBoxLayout *topBox = new QHBoxLayout();
	topBox->setSpacing(0);
	mainBox->addLayout(topBox);

	// 2.1 all popup menus
	pFilePb = new QPushButton("&File", this);
	pFilePb->setFlat(true);
	QSize size = pFilePb->fontMetrics().size(Qt::TextSingleLine, "File");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pFilePb->setFixedSize(size);
	buildFileMenu();
	pFilePb->setMenu(pFileMenu);
	topBox->addWidget(pFilePb);


	pHelpPb = new QPushButton("&Help", this);
	pHelpPb->setFlat(true);
	size = pHelpPb->fontMetrics().size(Qt::TextSingleLine, "Help");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pHelpPb->setFixedSize(size);
	buildHelpMenu();
	pHelpPb->setMenu(pHelpMenu);
	topBox->addWidget(pHelpPb);


	// 2.2 synoptic title
	pTitleLabel = new QLabel("Interlock Documentation - Valve " + valveName, this);
	topBox->addWidget(pTitleLabel, 10);	// The only resizable widget on top of dialog
	pTitleLabel->setAlignment(Qt::AlignHCenter);

	// 3 Scroll view where synoptic will lie
	pScroll = new QScrollArea(this);
	mainBox->addWidget(pScroll, 10);	// Scroll view will consume all free space
	connect(pScroll->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(viewMove(int)));

	/* Huge label for scroll testing
	QLabel *dummy = new QLabel("Some dummy content", pScroll->viewport());
	QSize bigSize(10000, 800);
	dummy->setFixedSize(bigSize);
	pScroll->addChild(dummy);
	*/
}

/*
**	FUNCTION
**		Build 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void InterlockDocumentationDialog::buildFileMenu(void)
{
	pFileMenu = new QMenu(this);
	pFileMenu->addAction("Print...", this, SLOT(filePrint()));
	pFileMenu->addSeparator();
	pFileMenu->addAction("Close", this, SLOT(deleteLater()));
}

/*
**	FUNCTION
**		Build 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void InterlockDocumentationDialog::buildHelpMenu(void)
{
	pHelpMenu = new QMenu(this);
	pHelpMenu->addAction("User manual...", this, SLOT(help()));
}


/*
**	FUNCTION
**		Build interlock documentation view according to current selection
**
**	ARGUMENTS
**		init	- Flag indicating if method is called from constructor
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void InterlockDocumentationDialog::buildView(bool init)
{
	if (pView)
	{
		delete pView;
		pView = NULL;
	}
	// Create instance of synoptic view

	pView = InterlockView::create(pScroll->viewport(), valveName);



	// Analyze equipment in rack to make QRects
	pView->showChain(valveName);

	// Add view to scroller
	pView->show();
	pScroll->setWidget(pView);

	setMyWidth(init);
	pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
}

void InterlockDocumentationDialog::setMyWidth(bool init)
{
	if (!pView)
	{
		return;
	}
	// Set own height according to size of synoptic view
	int scrollHeight = 900;
	/*	if (init)
	{
	scrollHeight = pView->minimumHeight() + pScroll->horizontalScrollBar()->height() / 2;
	scrollHeight += 2;
	pScroll->setMinimumHeight(scrollHeight);
	}
	else
	{
	int minHeight = pView->minimumHeight();
	int realHeight = pScroll->viewport()->height();
	scrollHeight = pView->minimumHeight() + pScroll->horizontalScrollBar()->height();
	scrollHeight += 2;
	pScroll->setMinimumHeight(scrollHeight);
	// resize(width(), height() + minHeight - realHeight);
	setMinimumHeight(height() + minHeight - realHeight);
	}*/

	// Adjust own width
	int viewWidth = pView->width();
	QDesktopWidget *pDesktop = QApplication::desktop();
	const QRect &screen = pDesktop->screenGeometry(pDesktop->screenNumber(this));
	// LIK 26.04.2009 if(init)
	{
		if (viewWidth > screen.width())
		{
			resize(screen.width() - 50, scrollHeight);
		}
		else
		{
			resize(viewWidth + 10, scrollHeight);
		}
	}
}

void InterlockDocumentationDialog::viewMove(int x)
{
	if (pView)
	{
		pView->setViewStart(x, pScroll->width(),
			pScroll->horizontalScrollBar()->isSliderDown());
	}
}

void InterlockDocumentationDialog::filePrint()
{
	QPrinter printer(QPrinter::HighResolution);
	printer.setOutputFormat(QPrinter::PdfFormat);

	QFileDialog dialog(this);
	dialog.setFileMode(QFileDialog::AnyFile);
	dialog.setNameFilter(tr("Pdf (*.pdf)"));
	dialog.setDefaultSuffix("pdf");
	dialog.setViewMode(QFileDialog::Detail);

	QStringList fileNames;
	if (dialog.exec())
		fileNames = dialog.selectedFiles();

	printer.setOutputFileName(fileNames.at(0));
	printer.setPageOrientation(QPageLayout::Landscape);
	//printer.setPageMargins(12, 16, 12, 20, QPrinter::Millimeter);
	printer.setFullPage(false);

	QPainter painter(&printer);
	
	QRect boundingRect = pView->getBoundingRect();
	double pageScaleY = printer.pageRect().height() / (double)boundingRect.bottom();
	double pageScaleX = printer.pageRect().width() / (double)boundingRect.right();

	double pageScale = pageScaleX < pageScaleY ? pageScaleX : pageScaleY;
	painter.scale(pageScale, pageScale);
	//painter.translate(printer.paperRect().x() + printer.pageRect().width() / 2,
		//printer.paperRect().y() + printer.pageRect().height() / 2);
	pView->setAutoFillBackground(false);
	pTitleLabel->render(&painter, QPoint(), QRegion(), QWidget::DrawChildren);
	pView->render(&painter, QPoint(), QRegion(), QWidget::DrawChildren);

	pView->setAutoFillBackground(true);


}