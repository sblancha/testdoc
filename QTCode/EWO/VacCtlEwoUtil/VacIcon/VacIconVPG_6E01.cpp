//	Implementation of VacIconVPG_6E01 class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPG_6E01.h"
#include "VacIconVPG_6A01.h"

#include "EqpProcess.h"

#include "DataPool.h"

#include <qpainter.h>
#include <QMouseEvent>


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
VacIconVPG_6E01::VacIconVPG_6E01(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	pIconVPG = pIconVVR1 = pIconVVR2 = pIconVGR = pIconVGP = pIconVGF = NULL;
	setFixedSize(47, 111);
	setAreaGeometry();
}

VacIconVPG_6E01::~VacIconVPG_6E01()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for symbol areas rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6E01::setAreaGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		vvr1.setRect(13, 2, 27, 21);
		vgf.setRect(6, 24, 35, 21);
		vvr2.setRect(13, 46, 27, 21);
		vgr.setRect(0, 68, 21, 21);
		vgp.setRect(26, 68, 21, 21);
		pump.setRect(13, 90, 21, 21);
		break;
	case VacIconContainer::Down:
		vvr1.setRect(7, 88, 27, 21);
		vgf.setRect(6, 66, 35, 21);
		vvr2.setRect(7, 44, 27, 21);
		vgr.setRect(0, 22, 21, 21);
		vgp.setRect(26, 22, 21, 21);
		pump.setRect(13, 0, 21, 21);
		break;
	case VacIconContainer::Left:
		vvr1.setRect(88, 13, 21, 27);
		vgf.setRect(66, 6, 21, 35);
		vvr2.setRect(44, 13, 21, 27);
		vgr.setRect(22, 0, 21, 21);
		vgp.setRect(22, 26, 21, 21);
		pump.setRect(0, 13, 21, 21);
		break;
	case VacIconContainer::Right:
		vvr1.setRect(2, 7, 21, 27);
		vgf.setRect(24, 6, 21, 35);
		vvr2.setRect(46, 7, 21, 27);
		vgr.setRect(68, 0, 21, 21);
		vgp.setRect(68, 26, 21, 21);
		pump.setRect(90, 13, 21, 21);
		break;
	}
	if(pIconVVR1)
	{
		pIconVVR1->move(vvr1.left(), vvr1.top());
	}
	if(pIconVGF)
	{
		pIconVGF->move(vgf.left(), vgf.top());
	}
	if(pIconVVR2)
	{
		pIconVVR2->move(vvr2.left(), vvr2.top());
	}
	if(pIconVPG)
	{
		pIconVPG->move(pump.left(), pump.top());
	}
	if(pIconVGR)
	{
		pIconVGR->move(vgr.left(), vgr.top());
	}
	if(pIconVGP)
	{
		pIconVGP->move(vgp.left(), vgp.top());
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPG_6E01::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		return VacIconGeometry(47, 111, 23, 112, -1);
	case VacIconContainer::Down:
		return VacIconGeometry(47, 111, 23, 112, -1);
	default:
		break;
	}
	return VacIconGeometry(111, 47, 112, 23, -1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6E01::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(47);
		size.setHeight(111);
		break;
	default:
		size.setWidth(111);
		size.setHeight(47);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreaGeometry();
	if(pIconVPG)
	{
		pIconVPG->setDirection(newDirection);
	}
	if(pIconVVR1)
	{
		pIconVVR1->setDirection(calculateVvrDirection());
	}
	if(pIconVVR2)
	{
		pIconVVR2->setDirection(calculateVvrDirection());
	}
	if(pIconVGR)
	{
		pIconVGR->setDirection(calculateVgrDirection());
	}
	if(pIconVGP)
	{
		pIconVGP->setDirection(calculateVgpDirection());
	}
	if(pIconVGF)
	{
		pIconVGF->setDirection(calculateVgrDirection());
	}
}

VacIconContainer::Direction VacIconVPG_6E01::calculateVvrDirection(void)
{
	VacIconContainer::Direction result = direction;
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		result = VacIconContainer::Left;
		break;
	default:
		result = VacIconContainer::Up;
		break;
	}
	return result;
}

VacIconContainer::Direction VacIconVPG_6E01::calculateVgrDirection(void)
{
	VacIconContainer::Direction result = VacIconContainer::Up;
	switch(direction)
	{
	case VacIconContainer::Up:
		result = VacIconContainer::Right;
		break;
	case VacIconContainer::Down:
		result = VacIconContainer::Left;
		break;
	case VacIconContainer::Left:
		result = VacIconContainer::Down;
		break;
	default:
		result = VacIconContainer::Up;
		break;
	}
	return result;
}

VacIconContainer::Direction VacIconVPG_6E01::calculateVgpDirection(void)
{
	VacIconContainer::Direction result = VacIconContainer::Up;
	switch(direction)
	{
	case VacIconContainer::Up:
		result = VacIconContainer::Left;
		break;
	case VacIconContainer::Down:
		result = VacIconContainer::Right;
		break;
	case VacIconContainer::Left:
		result = VacIconContainer::Up;
		break;
	default:
		result = VacIconContainer::Down;
		break;
	}
	return result;
}



/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6E01::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pIconVPG)
	{
		delete pIconVPG;
		pIconVPG = NULL;
	}
	if(pIconVVR1)
	{
		delete pIconVVR1;
		pIconVVR1 = NULL;
	}
	if(pIconVVR2)
	{
		delete pIconVVR2;
		pIconVVR2 = NULL;
	}
	if(pIconVGR)
	{
		delete pIconVGR;
		pIconVGR = NULL;
	}
	if(pIconVGP)
	{
		delete pIconVGP;
		pIconVGP = NULL;
	}
	if(pIconVGF)
	{
		delete pIconVGF;
		pIconVGF = NULL;
	}
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpProcess"), "VacIconVPG_6E01::setEqp", pEqp->getDpName());
		this->pEqp = (EqpProcess *)pEqp;
		pIconVPG = new VacIconVPG_6A01(this);
		pIconVPG->setEqp(pEqp);
		pIconVPG->move(pump.left(), pump.top());
		pIconVPG->setMode(mode);
		pIconVPG->setPipeColor(pipeColor);
		pIconVPG->setDirection(direction);
		pIconVPG->setDrawConnectionPipe(false);
		if(connected)
		{
			pIconVPG->connect();
		}

		QString attrName = pEqp->getAttrValue("VVR1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVVR1 = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVVR1)
		{
			pIconVVR1->move(vvr1.left(), vvr1.top());
			pIconVVR1->setMode(mode);
			pIconVVR1->setPipeColor(pipeColor);
			pIconVVR1->setDirection(calculateVvrDirection());
			if(connected)
			{
				pIconVVR1->connect();
			}
		}
		attrName = pEqp->getAttrValue("VVR2");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVVR2 = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVVR2)
		{
			pIconVVR2->move(vvr2.left(), vvr2.top());
			pIconVVR2->setMode(mode);
			pIconVVR2->setPipeColor(pipeColor);
			pIconVVR2->setDirection(calculateVvrDirection());
			if(connected)
			{
				pIconVVR2->connect();
			}
		}

		attrName = pEqp->getAttrValue("VGR1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGR = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGR)
		{
			pIconVGR->move(vgr.left(), vgr.top());
			pIconVGR->setMode(mode);
			pIconVGR->setPipeColor(pipeColor);
			pIconVGR->setDirection(calculateVgrDirection());
			if(connected)
			{
				pIconVGR->connect();
			}
		}

		attrName = pEqp->getAttrValue("VGP1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGP = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGP)
		{
			pIconVGP->move(vgp.left(), vgp.top());
			pIconVGP->setMode(mode);
			pIconVGP->setPipeColor(pipeColor);
			pIconVGP->setDirection(calculateVgpDirection());
			if(connected)
			{
				pIconVGP->connect();
			}
		}

		attrName = pEqp->getAttrValue("VGF");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGF = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGF)
		{
			pIconVGF->setDrawConnectionPipe(false);
			pIconVGF->move(vgf.left(), vgf.top());
			pIconVGF->setMode(mode);
			pIconVGF->setPipeColor(pipeColor);
			pIconVGF->setDirection(calculateVgrDirection());
			if(connected)
			{
				pIconVGF->connect();
			}
		}
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6E01::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	if(pIconVPG)
	{
		pIconVPG->setMode(newMode);
	}
	if(pIconVVR1)
	{
		pIconVVR1->setMode(newMode);
	}
	if(pIconVVR2)
	{
		pIconVVR2->setMode(newMode);
	}
	if(pIconVGR)
	{
		pIconVGR->setMode(newMode);
	}
	if(pIconVGP)
	{
		pIconVGP->setMode(newMode);
	}
	if(pIconVGF)
	{
		pIconVGF->setMode(newMode);
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set color used for pipe lines drawing
**
**	ARGUMENTS
**		newColor	- Color used for pipe drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6E01::setPipeColor(const QColor &newColor)
{
	if(pIconVPG)
	{
		pIconVPG->setPipeColor(newColor);
	}
	if(pIconVVR1)
	{
		pIconVVR1->setPipeColor(newColor);
	}
	if(pIconVVR2)
	{
		pIconVVR2->setPipeColor(newColor);
	}
	if(pIconVGR)
	{
		pIconVGR->setPipeColor(newColor);
	}
	if(pIconVGP)
	{
		pIconVGP->setPipeColor(newColor);
	}
	if(pIconVGF)
	{
		pIconVGF->setPipeColor(newColor);
	}
	VacIcon::setPipeColor(newColor);
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6E01::connect(void)
{
	if(pIconVPG)
	{
		pIconVPG->connect();
	}
	if(pIconVVR1)
	{
		pIconVVR1->connect();
	}
	if(pIconVVR2)
	{
		pIconVVR2->connect();
	}
	if(pIconVGR)
	{
		pIconVGR->connect();
	}
	if(pIconVGP)
	{
		pIconVGP->connect();
	}
	if(pIconVGF)
	{
		pIconVGF->connect();
	}
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6E01::disconnect(void)
{
	if(pIconVPG)
	{
		pIconVPG->disconnect();
	}
	if(pIconVVR1)
	{
		pIconVVR1->disconnect();
	}
	if(pIconVVR2)
	{
		pIconVVR2->disconnect();
	}
	if(pIconVGR)
	{
		pIconVGR->disconnect();
	}
	if(pIconVGP)
	{
		pIconVGP->disconnect();
	}
	if(pIconVGF)
	{
		pIconVGF->disconnect();
	}
	return false;
}

/*
**	FUNCTION
**		Override VacIcon's method in order to have different menus for different
**		parts of icon
**
**	ARGUMENTS
**		pEvent	- Pointer to mouse event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6E01::mousePressEvent(QMouseEvent * /* pEvent */)
{
	// Do nothing
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPG_6E01::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	painter.setPen(pipeColor);
//	painter.eraseRect(0, 0, width(), height());

	switch(direction)
	{
	case VacIconContainer::Up:
		// Line from VVR1 to outside
		painter.fillRect(pump.center().x() - 1, 0, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, vvr1.bottom() + 1,
			3, pump.top() - vvr1.bottom(), pipeColor);
		// connect VGR and VGP
		painter.fillRect(vgr.right(), vgr.center().y() - 1,
			vgp.left() - vgr.right(), 3, pipeColor);
		break;
	case VacIconContainer::Down:
		// Line from VVR to outside
		painter.fillRect(pump.center().x() - 1, vvr1.bottom(), 3, 4, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, pump.bottom(),
			3, vvr1.top() - pump.bottom(), pipeColor);
		// connect VGR and VGP
		painter.fillRect(vgr.right(), vgr.center().y() - 1,
			vgp.left() - vgr.right(), 3, pipeColor);
		break;
	case VacIconContainer::Left:
		// Line from VVR to outside
		painter.fillRect(vvr1.right(), pump.center().y() - 1, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.right(), pump.center().y() - 1,
			vvr1.left() - pump.right(), 3, pipeColor);
		// connect VGR and VGP
		painter.fillRect(vgr.center().x() - 1, vgr.bottom(),
			3, vgp.top() - vgr.bottom(), pipeColor);
		break;
	case VacIconContainer::Right:
		// Line from VVR to outside
		painter.fillRect(0, pump.center().y() - 1, 3, 3, pipeColor);
		// Connect pump to valves
		painter.fillRect(vvr1.right(), pump.center().y() - 1,
			pump.left() - vvr1.right() + 2, 3, pipeColor);
		// connect VGR and VGP
		painter.fillRect(vgr.center().x() - 1, vgr.bottom(),
			3, vgp.top() - vgr.bottom(), pipeColor);
		break;
	}

	if(!pEqp)
	{
		painter.setPen(Qt::black);
		painter.drawRect(pump.left(), pump.top(), pump.width() - 1, pump.height() - 1);
		painter.drawRect(vvr1.left(), vvr1.top(), vvr1.width() - 1, vvr1.height() - 1);
		painter.drawRect(vvr2.left(), vvr2.top(), vvr2.width() - 1, vvr2.height() - 1);
		painter.drawRect(vgf.left(), vgf.top(), vgf.width() - 1, vgf.height() - 1);
		painter.drawRect(vgr.left(), vgr.top(), vgr.width() - 1, vgr.height() - 1);
		painter.drawRect(vgp.left(), vgp.top(), vgp.width() - 1, vgp.height() - 1);
	}
}
