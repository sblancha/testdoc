#ifndef	VACICONVPR_VELO_H
#define	VACICONVPR_VELO_H

// Icon for VELO roughing pump

#include "VacIcon.h"

class EqpVPR_VELO;

class VACCTLEWOUTIL_EXPORT VacIconVPR_VELO : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPR_VELO(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPR_VELO();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);


protected:
	// Pointer to my device
	EqpVPR_VELO *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
	
	void setPainter(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
};

#endif	// VACICONVPR_VELO_H
