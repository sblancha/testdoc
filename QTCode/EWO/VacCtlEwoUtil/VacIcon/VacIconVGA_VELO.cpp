//	Implementation of VacIconVGA_VELO class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGA_VELO.h"

#include <qpainter.h>

// We only need to implement drawSpecific() method

VacIconVGA_VELO::VacIconVGA_VELO(QWidget *parent, Qt::WindowFlags f) :
	VacIconVG(parent, f)
{
	QFont mFont("Times", 8, QFont::Bold);
	setFont(mFont);
}

// Draw 'Ion' symbol in the center of icon
void VacIconVGA_VELO::drawSpecific(QPainter &painter)
{
	painter.setPen(Qt::black);
	painter.drawText(7, 12, "q");
}
