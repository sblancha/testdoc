//	Implementation of MainLhcEwo class
/////////////////////////////////////////////////////////////////////////////////


#include "MainLhcEwo.h"

#include "DpConnection.h"
#include "DpeHistoryQuery.h"
#include "DpPolling.h"
#include "MobileHistoryQuery.h"
#include "DpeHistoryDepthQuery.h"

EWO_PLUGIN(MainLhcEwo)


MainLhcEwo::MainLhcEwo(QWidget *parent) : VacMainViewEwo(parent)
{
	baseWidget = new MainLHC(parent);
	connectStdSignals();
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList MainLhcEwo::methodList(void) const
{
	QStringList list;
	list.append("int dataReady()");
	list.append("bool isSelected()");
	list.append("bool isContSelection()");
	list.append("dyn_string getSelectedSectors(bool allSectors)");
	list.append("uint getVacTypeMask()");
	list.append("int addLegendLabel(int x, int y, string text");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainLhcEwo::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "dataReady")
	{
		return invokeDataReady(name, values, error);
	}
	else if(name == "isSelected")
	{
		return invokeIsSelected(name, values, error);
	}
	else if(name == "isContSelection")
	{
		return invokeIsContSelection(name, values, error);
	}
	else if(name == "getSelectedSectors")
	{
		return invokeGetSelectedSectors(name, values, error);
	}
	else if(name == "getVacTypeMask")
	{
		return invokeGetVacTypeMask(name, values, error);
	}
	else if(name == "addLegendLabel")
	{
		return invokeAddLegendLabel(name, values, error);
	}
	else
	{
		error = "Uknown method MainLhcEwo.";
		error += name;
	}
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			int dataReady()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainLhcEwo::invokeDataReady(const QString & /* name */, QList<QVariant> & /* values */,
	QString & /* error */)
{
	// Execute and return result
printf("%s: invokeDataReady() calling dataReady()\n", QDateTime::currentDateTime().toString().toLatin1().constData());
	int coco = ((MainLHC *)baseWidget)->dataReady();
printf("%s: invokeDataReady() dataReady() finished\n", QDateTime::currentDateTime().toString().toLatin1().constData());
	return QVariant(coco);
}

/*
**	FUNCTION
**		Execute method
**			bool isSelected()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainLhcEwo::invokeIsSelected(const QString & /* name */, QList<QVariant> & /* values */,
	QString & /* error */)
{
	return QVariant(((MainLHC *)baseWidget)->isSelected());
}

/*
**	FUNCTION
**		Execute method
**			bool isContSelection()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainLhcEwo::invokeIsContSelection(const QString & /* name */, QList<QVariant> & /* values */,
	QString & /* error */)
{
	return QVariant(((MainLHC *)baseWidget)->isContSelection());
}

/*
**	FUNCTION
**		Execute method
**			dyn_string getSelectedSectors(bool allSectors)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainLhcEwo::invokeGetSelectedSectors(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Bool))
	{
		error = "argument 1 (allSectors) is not bool";
		return QVariant();
	}
	bool allSectors = values[0].toBool();
	return QVariant(((MainLHC *)baseWidget)->getSelectedSectors(allSectors));
}

/*
**	FUNCTION
**		Execute method
**			uint getVacTypeMask()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainLhcEwo::invokeGetVacTypeMask(const QString & /* name */, QList<QVariant> & /* values */,
	QString & /* error */)
{
	return QVariant(((MainLHC *)baseWidget)->getVacTypeMask());
}

/*
**	FUNCTION
**		Execute method
**			int addLegendLabel(int x, int y, string text)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainLhcEwo::invokeAddLegendLabel(const QString &name, QList<QVariant> &values,
	QString & error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (x) is not int";
		return QVariant();
	}
	int x = values[0].toInt();
	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (y) is not int";
		return QVariant();
	}
	int y = values[1].toInt();
	if(!values[2].canConvert(QVariant::String))
	{
		error = "argument 3 (text) is not string";
		return QVariant();
	}
	const QString text = values[2].toString();
	((MainLHC *)baseWidget)->addLegendLabel(x, y, text.toLatin1());
	return QVariant((int)0);
}
