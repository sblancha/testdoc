#ifndef	VACICONVGI_H
#define	VACICONVGI_H

//	Icon for VGI

#include "VacIconVG.h"

class VACCTLEWOUTIL_EXPORT VacIconVGI : public VacIconVG
{
public:
	VacIconVGI(QWidget *parent, Qt::WindowFlags f = 0);

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGI_H
