/** @defgroup gr_vacPanelConfigVPLC_PF admin/vacPanelConfigVPLC_PF.pnl
vacPanelConfigVPLC_PF.ctl is the scope library of the panel vacPanelConfigVPLC_PF.pnl in the directory admin/. \n
vacPanelConfigVPLC_PF.pnl is a panel to initialize configuration data of fixed pumping group PLC. \n
\n 
Vacuum PLC for Pumping group Fixed(VPLC_PF) is a PLC dedicated to the control of a single fixed pumping group, 
i.e. the process and sub-devices control. \n
PLCs of type VPLC_PF are not updated such as standard PLCs. This means no code files is generated 
from vacuum database exporter and so nothing is compiled neither downloaded. These PLCs are configured 
by this script setting write register dpe and getting read register dpe. \n
\n
The configuration parameters to be initialized are specified in the Vacuum Fixed Device Communication Register document: \n
https://svnvacplc.web.cern.ch/repo/Specifications/VacFixDevCommRegisters.xlsx sheet VPLC_PF \n
\n
PLCs of type VPLC_PF have 2 communication partners (other PLCs) to get the pressure values (1VGM/1VGR/1VGP per sector)   
of the previous and next sectors and display values on local touch panel. \n
The PLCs have so 3 IP Address to be defined in the PLC hardware configuration (is own IP and the IP of the 2 partner). \n
\n
The main item of the panel is a \b table called plcTable, it has the below columns: \n
    Column Name           | Title            |Description
    ----------------------|------------------|-----------
    nb                    | #                | Number of the PLC 
    plcName               | PLC Name         | The layout name of the PLC 
    systIntegrity         | Integrity        | PLC alive status from Unicos System integrity (updated with call back) 
    dbTS                  | TS DB            | Config Timestamp from Database  
    plcTS                 | TS PLC           | Config Timestamp from PLC 
    needUpdate            | UpD?             | Parameters need to be updated? (0:No or 1:Yes)
    txSt                  | Tx State         | The current config transmission state 
    pName                 | Message          | Message or parameter name currently processed 
    pVal                  | Param Value      | Value write or read of the current parameter
    pCode                 | P Code           | The parameter code currently processed 
    vpgName               | VPG Name         | Layout name of the associated Pumping group
    vg1Type               | vg1Type          | Control type of the first gauge
    vg2Type               | vg2Type          | Control type of the second gauge 
    isPrSwitch            | prSw             | VPG has a pressure switch? (0:No or 1:Yes)
    prePumpTime           | prePumpTim       | Parameter value prePumpTime [sec]
    ventCheckTime         | ventChecTim      | Parameter value ventCheckTime [sec]
    ventingTime           | ventTime         | Parameter value ventingTime [sec]
    vvtOpenDelay          | vvtOpDelay       | Parameter value vvtOpenDelay [sec]
    vvpOpenDelay          | vvpOpDelay       | Parameter value vvpOpenDelay [sec]
    errVptNOnTO           | errVptNOnTO      | Parameter value errVptNOnTO [sec]
    errAccelTO            | errAccelTO       | Parameter value errAccelTO [sec]
    errRecoverTO          | errRecovTO       | Parameter value errRecoverTO [sec]
    vvr1ForcedTO          | vvr1FoTO         | Parameter value vvr1ForcedTO [sec]
    vvr2ForcedTO          | vvr2FoTO         | Parameter value vvr2ForcedTO [sec]
    autoRestart           | auRest           | Parameter value autoRestart [0:no 1:yes]
    autoRestartMax        | auRestMax        | Parameter value autoRestartMax [number] 
    autoVent              | auVent           | Parameter value autoVent [0:no 1:yes]
    isVPPCurrentTransducer| Transducer       | Parameter value isVPPCurrentTransducer [0:no 1:yes]  
    vg2PrLimitVal         | vg2PrLimit       | Parameter value vg2PrLimitVal [mbar]
    vgmDbPrevSect         | vgmDbPrevSect    |
    vgrDbPrevSect         | vgrDbPrevSect    |
    vgpDbPrevSect         | vgpDbPrevSect    |
    vgmDbNextSect         | vgmDbNextSect    |
    vgrDbNextSect         | vgrDbNextSect    |
    vgpDbNextSect         | vgpDbNextSect    |
    timestampYMDH         | timestampYMDH    |
    timestampMSM          | timestampMSM     |
    ip                    | IP               | IP Address of the PLC 
*/
#uses "VacCtlEqpDataPvss"	      // C++ CTRL Library
#uses "VacCtlImportPvss"	      // C++ CTRL Library
#uses "vclGeneric.ctl"           // WCCOA CTRL Library
#uses "vclDeviceFileParser.ctl"	// WCCOA CTRL Library
#uses "vclDevCommon.ctl"	      // WCCOA CTRL Library
#uses "vclAddressConfig.ctl"     // WCCOA CTRL Library

/// Debug mode
bool debugFlag = false;

/// @brief COLOR used for table cells
string 
  COLOR_INVALID = "{ 63, 127, 255}", //BLUE
  COLOR_OK      = "{  0, 204,   0}"; //GREEN

/// @brief DP Type of PLC Pumping group Fixed
const string PLC_DP_TYPE = "VPLC_PF";
/// @brief DP Type of Pumping group
const string VPGU_DP_TYPE = "VP_GU";

/// @brief Parameter Code read back from PLC time out in second 
const int CODE_TIMEOUT = 5;
/// @brief Parameter Value read back from PLC time out in second 
const int VALUE_TIMEOUT = 10;

/// @brief Param type is string 
const int P_STRING = 1;
/// @brief Param type is integer
const int P_INTEGER = 2;
/// @brief Param type is float 
const int P_FLOAT = 3;

/// @brief DPE Write parameter code order
const string DPE_WR_CODE = ".parCoOrder";
/// @brief DPE Write the parameter value format integer
const string DPE_WR_INTEGER_VAL = ".uParValOrder";
/// @brief DPE Write the parameter value format float
const string DPE_WR_FLOAT_VAL = ".fParValOrder";
/// @brief DPE Write the parameter value format string
const string DPE_WR_STRING_VAL = ".wrName";
/// @brief DPE Read the timestamp format YMDH
const string DPE_RD_TS_YMDH = ".TS1.YMDH";
/// @brief DPE Read the timestamp format MSM
const string DPE_RD_TS_MSM = ".TS1.MSM";
/// @brief DPE Read parameter code 
const string DPE_RD_CODE = ".parCoSt";
/// @brief DPE Read the parameter value format integer
const string DPE_RD_INTEGER_VAL = ".uParValSt";
/// @brief DPE Read the parameter value format float
const string DPE_RD_FLOAT_VAL = ".fParValSt";
/// @brief DPE Read Hardware type
const string DPE_RD_HW_TYPE = ".hwTypeSt";
/// @brief DPE Read Hardware type
const string DPE_RD_OBJECTST = ".objectSt";

/// @brief Parameter code to Write Hardware Config VPG Name  
const int SET_vpgName_CODE = 1;
/// @brief Parameter code to Write Hardware Config VVR1 Name  
const int SET_vvr1Name_CODE = 2;                             
/// @brief Parameter code to Write Hardware Config VVR2 Name  
const int SET_vvr2Name_CODE = 3;  
/// @brief Parameter code to Set Hardware Config VG1 Type  
const int SET_vg1Type_CODE = 10;
/// @brief Parameter code to Set Hardware Config VG2 Type  
const int SET_vg2Type_CODE = 11;
/// @brief Parameter code to Set Hardware Config isPrSwitch  
const int SET_isPrSwitch_CODE = 12;

/// @brief Parameter code to Set Datablock Number of VGM in first partner PLC (previous sector) 
const int SET_vgmDbPrevSect_CODE = 13;
/// @brief Parameter code to Set Datablock Number of VGR in first partner PLC (previous sector) 
const int SET_vgrDbPrevSect_CODE = 14; 
/// @brief Parameter code to Set Datablock Number of VGP in first partner PLC (previous sector) 
const int SET_vgpDbPrevSect_CODE = 15; 

/// @brief Parameter code to Set Datablock Number of VGM in second partner PLC (next sector) 
const int SET_vgmDbNextSect_CODE = 16;
/// @brief Parameter code to Set Datablock Number of VGR in second partner PLC (next sector) 
const int SET_vgrDbNextSect_CODE = 17; 
/// @brief Parameter code to Set Datablock Number of VGP in second partner PLC (next sector) 
const int SET_vgpDbNextSect_CODE = 18;
/// @brief Parameter code to Set autoVent  NEED FEEDBACK  
const int SET_isVPPCurrentTransducer_CODE = 19;

/// @brief Parameter code to Set prePumpTime NEED FEEDBACK  
const int SET_prePumpTime_CODE = 100;
/// @brief Parameter code to Set ventCheckTime NEED FEEDBACK    
const int SET_ventCheckTime_CODE = 101;  
/// @brief Parameter code to Set ventingTime  NEED FEEDBACK  
const int SET_ventingTime_CODE = 102;  
/// @brief Parameter code to Set vvtOpenDelay  NEED FEEDBACK  
const int SET_vvtOpenDelay_CODE = 103;  
/// @brief Parameter code to Set vvpOpenDelay   NEED FEEDBACK  
const int SET_vvpOpenDelay_CODE = 104;  
/// @brief Parameter code to Set errVPTNOnTO  NEED FEEDBACK  
const int SET_errVPTNOnTO_CODE = 105;  
/// @brief Parameter code to Set errAccelTO  NEED FEEDBACK  
const int SET_errAccelTO_CODE = 106;  
/// @brief Parameter code to Set errRecoverTO  NEED FEEDBACK  
const int SET_errRecoverTO_CODE = 107;
/// @brief Parameter code to Set VVR1ForcedTO  NEED FEEDBACK  
const int SET_vvr1ForcedTO_CODE = 108;
/// @brief Parameter code to Set VVR2ForcedTO  NEED FEEDBACK  
const int SET_vvr2ForcedTO_CODE = 109;
/// @brief Parameter code to Set autoRestart  NEED FEEDBACK  
const int SET_autoRestart_CODE = 110;
/// @brief Parameter code to Set autoRestartMax NEED FEEDBACK   
const int SET_autoRestartMax_CODE = 111;
/// @brief Parameter code to Set autoVent  NEED FEEDBACK  
const int SET_autoVent_CODE = 112;
/// @brief Parameter code to Set Penning Gauge Pressure Interlock parameter code NEED FEEDBACK  
const int SET_vg2PrLimitVal_CODE = 113;
 
/// @brief Not Attribute Parameter code to Set Config file (.forDLL) Time stamp YMDH
const int SET_TS_YMDH_CODE = 150;
/// @brief Not Attribute Parameter code to Set Config file (.forDLL) Time stamp MSM
const int SET_TS_MSM_CODE = 151;

/// @brief Parameter code to Get prePumpTime  
const int GET_prePumpTime_CODE = 200;
/// @brief Parameter code to Get ventCheckTime  
const int GET_ventCheckTime_CODE = 201;  
/// @brief Parameter code to Get ventingTime  
const int GET_ventingTime_CODE = 202;  
/// @brief Parameter code to Get vvtOpenDelay  
const int GET_vvtOpenDelay_CODE = 203;  
/// @brief Parameter code to Get vvpOpenDelay   
const int GET_vvpOpenDelay_CODE = 204;  
/// @brief Parameter code to Get errVPTNOnTO  
const int GET_errVPTNOnTO_CODE = 205;  
/// @brief Parameter code to Get errAccelTO  
const int GET_errAccelTO_CODE = 206;  
/// @brief Parameter code to Get errRecoverTO  
const int GET_errRecoverTO_CODE = 207;
/// @brief Parameter code to Get VVR1ForcedTO  
const int GET_vvr1ForcedTO_CODE = 208;
/// @brief Parameter code to Get VVR2ForcedTO  
const int GET_vvr2ForcedTO_CODE = 209;
/// @brief Parameter code to Get autoRestart  
const int GET_autoRestart_CODE = 210;
/// @brief Parameter code to Get autoRestartMax  
const int GET_autoRestartMax_CODE = 211;
/// @brief Parameter code to Get autoVent  
const int GET_autoVent_CODE = 212;
/// @brief Parameter code to Get Penning Gauge Pressure Interlock  
const int GET_vg2PrLimitVal_CODE = 213; 

/// @brief Transmission status meaning GETTING
const string TX_START = "In Progress";
/// @brief Transmission status meaning ATTRIBUTE ERREUR 
const string TX_ATTR_ERR = "Err:AttrRead:";
/// @brief Transmission status meaning CHECKING CODE ERREUR 
const string TX_CHECK_CODE_ERR = "Err:StCodePLC:";
/// @brief Transmission status meaning CHECKING VALUE ERREUR 
const string TX_CHECK_VAL_ERR = "Err:StValPLC:";
/// @brief Transmission status meaning All VPG CONFIGURED 
const string TX_CONFIGURED = "Configured OK";

/// @brief Transmission status meaning GETTING
const string MESS_GETTING = "Get Val...";
/// @brief Transmission status meaning GETTING
const string MESS_NO_VAL = "No Val";
/// @brief Transmission status meaning SENDING 
const string MESS_SENDING = "Sending...";
/// @brief Transmission status meaning CHECKING 
const string MESS_CHECKING = "Checking...";
/// @brief Transmission status meaning OK parameter transmitted
const string MESS_OK = "OK";

/// @brief Code to send to FE through WR1 to notify eqp is activated in SCADA 
const unsigned SCADA_ACTIVATED_WR1 = 0x0400u;

/// @brief Flag to enable debugging output
const bool		  isDebugFlag = false;

/**
@brief Initialisation of the panel
@details Build the PLC table
@param[in,out] mainTable The PLC table of the panel
@param[out] plcDps List of VPG PLC Data Point name
@return 
*/
dyn_string PanelInit(shape mainTable, dyn_string plcDps, dyn_shape buttonsToEnable) {
  dyn_string exceptionInfo; 
  int ret; 
  //DebugTN("This is the init");
  dynClear(plcDps);
  mainTable.deleteAllLines();
  // Fill PLC DPs  
  plcDps = dpNames("*",PLC_DP_TYPE);
  //systemName = strrtrim(dpSubStr(plcDps[1], DPSUB_SYS), ":");
  if (dynlen(plcDps) == 0) {
    mainTable.appendLine("plcName", "NONE", "pName", "No PLC Found...");    
    return plcDps;
  }
  // Build the PLC table
  // Fill table with static data: names, ip addr...   
  for(int i = 1; i <= dynlen(plcDps); i++) {
    string layoutName, sIpAddr, vpgName, dbTS, plcTS, needUpdate;
    // Get PLC Names     
    LhcVacDisplayName(dpSubStr(plcDps[i], DPSUB_DP), layoutName, exceptionInfo);
    // Get PLC IP Addresses 
    LhcVacGetAttribute(dpSubStr(plcDps[i], DPSUB_DP), "ipAddress", sIpAddr);
    // Get VPG Name
    LhcVacGetAttribute(dpSubStr(plcDps[i], DPSUB_DP), "vpgName", vpgName);
    // Get DB Timestamp
    LhcVacGetAttribute(dpSubStr(plcDps[i], DPSUB_DP), "TimeStamp", dbTS);
    strreplace( dbTS, ".", "-");
    // Get PLC Timestamp
    BuildStringFromTS(plcDps[i], plcTS);
    // needUpdate? compare plcTS and dbTS
    if( ((string)plcTS) ==  ((string)dbTS) ) {
      needUpdate = "0";
    }
    else {
      needUpdate = "1";
    }
    // old method:    
    // LhcVacGetAttribute(dpSubStr(plcDps[i], DPSUB_DP), "needConfigUpdate", needUpdate);
    // Editing Table with 'static' data
    mainTable.appendLine("nb", i, "plcName", layoutName, "ip", sIpAddr, "vpgName", vpgName, "dbTS", dbTS, "plcTS", plcTS, "needUpdate", needUpdate);
    //    
    // System integrity need Call Back
    string systIntegDpName = dpSubStr(plcDps[i], DPSUB_DP);
    strchange(systIntegDpName, 0, 8, "");
    systIntegDpName = "_unSystemAlarm_DPE_" + systIntegDpName; 
    if(dpExists(systIntegDpName)) {    
      ret = dpConnectUserData("_updateSystIntegrityCB", makeDynAnytype(mainTable, "systIntegrity", i-1), systIntegDpName + ".alarm");
      if(ret != 0) {
        mainTable.cellValueRC(i-1, "systIntegrity", "Disable");
      }
    }
    else {
      mainTable.cellValueRC(i-1, "systIntegrity", "Disable");
    }
  }
  // Activate process buttons
  for(int i = 1; i <= dynlen(buttonsToEnable); i++) {
    shape pb = buttonsToEnable[i];
    pb.enabled(true);  
  }
  return plcDps;
}

/**
@brief Closing of the panel
*/
void PanelClose() {
	//DebugTN("This is the close function");
}

/**
@brief Configuration process of fixed pumping group PLCs
@param mainTable The PLC table of the panel
*/
int ConfigProcess(shape mainTable, dyn_string plcDps, bool dbUpOnly, dyn_shape buttonsToDisable) {
  int             ret = 0;             //return value of function (0 is OK)
    
  DebugTN("vacPanelConfigVPLC_PF.pnl: ----- Fixed Pumping group PLC configuration starting");

  // deactivate process buttons
  for(int i = 1; i <= dynlen(buttonsToDisable); i++) {
    shape pb = buttonsToDisable[i];
    pb.enabled(false);  
  }  
  
  // Build the array parameter code
  // ------------------------------
  // The difference between mandatory and optional: If missing mandatory will triggered an error and the overall config process will report an error
  // --- HARDWARE CONFIG PARAMS  
  // String Hardware config paramater codes and names (mandatory)
  dyn_int pCodes = makeDynInt( 
                                   SET_vpgName_CODE,
                                   SET_vvr1Name_CODE,
                                   SET_vvr2Name_CODE,
                                   SET_isPrSwitch_CODE,
                                   SET_vg1Type_CODE,
                                   SET_vg2Type_CODE,
                                   SET_vgmDbPrevSect_CODE,
                                   SET_vgrDbPrevSect_CODE,
                                   SET_vgpDbPrevSect_CODE,
                                   SET_vgmDbNextSect_CODE,
                                   SET_vgrDbNextSect_CODE,
                                   SET_vgpDbNextSect_CODE,
                                   SET_isVPPCurrentTransducer_CODE,
                                   
                                   SET_prePumpTime_CODE,
                                   SET_ventCheckTime_CODE,
                                   SET_ventingTime_CODE,
                                   SET_vvtOpenDelay_CODE,
                                   SET_vvpOpenDelay_CODE,
                                   SET_errVPTNOnTO_CODE,
                                   SET_errAccelTO_CODE,
                                   SET_errRecoverTO_CODE,
                                   SET_vvr1ForcedTO_CODE,
                                   SET_vvr2ForcedTO_CODE,
                                   SET_autoRestart_CODE,
                                   SET_autoRestartMax_CODE,
                                   SET_autoVent_CODE,
                                   SET_TS_YMDH_CODE,
                                   SET_TS_MSM_CODE,
                                   
                                   SET_vg2PrLimitVal_CODE);
                                   
  dyn_string pNames = makeDynString(
                                   "vpgName",
                                   "vvr1SectName",
                                   "vvr2SectName",
                                   "isPrSwitch",
                                   "vg1Type",
                                   "vg2Type",
                                   "vgmDbPrevSect",
                                   "vgrDbPrevSect",
                                   "vgpDbPrevSect",
                                   "vgmDbNextSect",
                                   "vgrDbNextSect",
                                   "vgpDbNextSect",
                                   "isVPPCurrentTransducer",

                                   "prePumpTime",
                                   "ventCheckTime",
                                   "ventingTime",
                                   "vvtOpenDelay",
                                   "vvpOpenDelay",
                                   "errVptNOnTO",
                                   "errAccelTO",
                                   "errRecoverTO",
                                   "vvr1ForcedTO",
                                   "vvr2ForcedTO",
                                   "autoRestart",
                                   "autoRestartMax",
                                   "autoVent",
                                   "timestampYMDH",
                                   "timestampMSM",
                                   
                                   "vg2PrLimitVal");
                                   
  dyn_int pTypes = makeDynInt(
                                   P_STRING,
                                   P_STRING,
                                   P_STRING,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   
                                   P_FLOAT);
  
  dyn_bool pIsMandatory = makeDynBool(
                                   TRUE,
                                   TRUE,
                                   FALSE,
                                   TRUE,
                                   FALSE,
                                   FALSE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   
                                   FALSE);
  
  dyn_bool pIsPlcValChecked = makeDynBool(
                                   FALSE,
                                   FALSE,
                                   FALSE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   TRUE,
                                   
                                   TRUE);
  // PROCESS
  // -------------
  bool err;  
  for(int i = 1; i <= dynlen(plcDps); i++) {
    string dbUp = mainTable.cellValueRC(i-1, "needUpdate");
    if( dbUpOnly && (dbUp != "1") ) {
      mainTable.cellValueRC(i-1, "txSt", "No Config");
      continue;
    }
    err = false;    
    // Init TX info
    mainTable.cellValueRC(i-1, "txSt", makeDynString(TX_START , COLOR_OFF));
    mainTable.cellValueRC(i-1, "pName", makeDynString(TX_START, COLOR_OFF));
    //    
    // Config main loop
    bool retry =false;
    for(int j = 1; j <= dynlen(pCodes); j++) { 
      //
      // Read attribute value
      string attrVal;
      mainTable.cellValueRC(i-1, "pName", MESS_GETTING);      
      LhcVacGetAttribute(dpSubStr(plcDps[i], DPSUB_DP), pNames[j], attrVal);
      if(debugFlag) {
        DebugTN("vacPanelConfigVPLC_PF.ctl:----- " + MESS_GETTING + " " + plcDps[i] + " " + pNames[j] + " " + attrVal);
      }
      mainTable.cellValueRC(i-1, "pCode", pCodes[j]);
      mainTable.cellValueRC(i-1, "pName", pNames[j]);
      mainTable.cellValueRC(i-1, "pVal", attrVal);
      if (attrVal == "") {
        if(pIsMandatory[j]) {        
          mainTable.cellValueRC(i-1, "txSt", makeDynString(TX_ATTR_ERR + pNames[j], COLOR_ERROR));
          err = true;
        }  
        continue;
      }
      mainTable.cellValueRC(i-1, "pName", MESS_SENDING);
      //      
      // Write parameter to PLC      
      // ALL TYPES
      // Clean com registers
      dpSetWait(plcDps[i] + DPE_WR_CODE, 0);
      //dpSetWait(plcDps[i] + DPE_RD_CODE, 0);
      // STRING PARAMETER
      if(pTypes[j] == P_STRING) {
        dpSetWait(plcDps[i] + DPE_WR_STRING_VAL, attrVal);
      }
      // INTEGER PARAMETER
      int iVal;      
      if(pTypes[j] == P_INTEGER) {
        iVal = (int)attrVal;     
        dpSetWait(plcDps[i] + DPE_WR_INTEGER_VAL, iVal);
      }
      // FLOAT PARAMETER
      float fVal;
      if(pTypes[j] == P_FLOAT) {
        fVal = (float)attrVal;
        dpSetWait(plcDps[i] + DPE_WR_FLOAT_VAL, fVal);
      }
      // ALL TYPES
      dpSetWait(plcDps[i] + DPE_WR_CODE, pCodes[j]);
      //      
      // Check Code feedback from PLC
      int feedback_code = -9999;
      int loopNb = 0;
      bool waitFeedback = true;  
      while (waitFeedback) {
        delay(1); // wait the PLC has time to answere
        dpGet(plcDps[i] + DPE_RD_CODE, feedback_code);
        //DebugTN("feedback_code ", feedback_code);
        if(pCodes[j] == feedback_code) {
          mainTable.cellValueRC(i-1, "pName", MESS_OK);          
          waitFeedback = false;          
        }         
        else if(loopNb > CODE_TIMEOUT) {
          mainTable.cellValueRC(i-1, "txSt", makeDynString(TX_CHECK_CODE_ERR + pNames[j], COLOR_ERROR));
          err = true;
          waitFeedback = false;
        }  
        loopNb ++;
      }
      // 
      // Check Value feedback from PLC
      if(pIsPlcValChecked[j]) {
        int iFeedback_val = -9999;
        float fFeedback_val = -9999.9;
        loopNb = 0;
        waitFeedback = true;  
        while (waitFeedback) {
          delay(1); // wait the PLC has time to answere
          if(pTypes[j] == P_INTEGER) {
            dpGet(plcDps[i] + DPE_RD_INTEGER_VAL, iFeedback_val);
            mainTable.cellValueRC(i-1,  pNames[j], iFeedback_val);
            //DebugTN("iFeedback_val ", iFeedback_val);
            if(iVal == iFeedback_val) {
              mainTable.cellValueRC(i-1, "pName", MESS_OK);          
              waitFeedback = false;
              retry = false;          
            }
          }
          if(pTypes[j] == P_FLOAT) {
            dpGet(plcDps[i] + DPE_RD_FLOAT_VAL, fFeedback_val);
            mainTable.cellValueRC(i-1,  pNames[j], fFeedback_val);
            //DebugTN("fFeedback_val ", fFeedback_val);
            if( ((fVal / fFeedback_val) < 1.1) && ((fVal / fFeedback_val) > 0.9) ) {
              mainTable.cellValueRC(i-1, "pName", MESS_OK);          
              waitFeedback = false;
              retry = false;            
            }
          }         
          if( (loopNb > VALUE_TIMEOUT) && (waitFeedback) ) {
            if(retry) { //Second try failed
              mainTable.cellValueRC(i-1, "txSt", makeDynString(TX_CHECK_VAL_ERR + pNames[j], COLOR_ERROR));
              err = true;
            }
            else { // let it another chance             
              mainTable.cellValueRC(i-1, "txSt", makeDynString(TX_CHECK_VAL_ERR + pNames[j], COLOR_WARNING));
              j --;
              retry =true;
            }
            waitFeedback = false;            
          }
          loopNb ++;
        }    
      }
         
    }
    if(err) {
      mainTable.cellValueRC(i-1, "pName", makeDynString("Finished Err", COLOR_ERROR));
    }    
    else {
      mainTable.cellValueRC(i-1, "txSt", makeDynString(TX_CONFIGURED, COLOR_OK));
      // Get PLC Timestamp
      string plcTS;
      BuildStringFromTS(plcDps[i], plcTS);
      mainTable.cellValueRC(i-1, "plcTS", plcTS, "needUpdate", "0");
    }
  }
  // activate process buttons
  for(int i = 1; i <= dynlen(buttonsToDisable); i++) {
    shape pb = buttonsToDisable[i];
    pb.enabled(true);  
  }      
}


/**
@brief Read Configuration from fixed pumping group PLCs
@param mainTable The PLC table of the panel
*/
int ConfigRead(shape mainTable, dyn_string plcDps, bool dbUpOnly, dyn_shape buttonsToDisable) {
  int             ret = 0;             //return value of function (0 is OK)
    
  DebugTN("vacPanelConfigVPLC_PF.pnl: ----- Fixed Pumping group PLC configuration reading");

  // deactivate process buttons
  for(int i = 1; i <= dynlen(buttonsToDisable); i++) {
    shape pb = buttonsToDisable[i];
    pb.enabled(false);  
  }  
  
  // Build the array parameter code
  // ------------------------------
  // The difference between mandatory and optional: If missing mandatory will triggered an error and the overall config process will report an error
  // --- HARDWARE CONFIG PARAMS  
  // String Hardware config paramater codes and names (mandatory)
  dyn_int pCodes = makeDynInt(     GET_prePumpTime_CODE,
                                   GET_ventCheckTime_CODE,
                                   GET_ventingTime_CODE,
                                   GET_vvtOpenDelay_CODE,
                                   GET_vvpOpenDelay_CODE,
                                   GET_errVPTNOnTO_CODE,
                                   GET_errAccelTO_CODE,
                                   GET_errRecoverTO_CODE,
                                   GET_vvr1ForcedTO_CODE,
                                   GET_vvr2ForcedTO_CODE,
                                   GET_autoRestart_CODE,
                                   GET_autoRestartMax_CODE,
                                   GET_autoVent_CODE,
                                   
                                   GET_vg2PrLimitVal_CODE);
                                   
  dyn_string pNames = makeDynString(
                                   "prePumpTime",
                                   "ventCheckTime",
                                   "ventingTime",
                                   "vvtOpenDelay",
                                   "vvpOpenDelay",
                                   "errVptNOnTO",
                                   "errAccelTO",
                                   "errRecoverTO",
                                   "vvr1ForcedTO",
                                   "vvr2ForcedTO",
                                   "autoRestart",
                                   "autoRestartMax",
                                   "autoVent",
                                   
                                   "vg2PrLimitVal");
                                   
  dyn_int pTypes = makeDynInt(     P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   P_INTEGER,
                                   
                                   P_FLOAT);
 

  // READ PROCESS
  // -------------
  bool err;  
  for(int i = 1; i <= dynlen(plcDps); i++) {
    string dbUp = mainTable.cellValueRC(i-1, "needUpdate");
    if( dbUpOnly && (dbUp != "1") ) {
      //mainTable.cellValueRC(i-1, "txSt", "No Config");
      continue;
    }
    err = false;    
    // Init TX info
    mainTable.cellValueRC(i-1, "txSt", makeDynString(TX_START , COLOR_OFF));
    mainTable.cellValueRC(i-1, "pName", makeDynString(TX_START, COLOR_OFF));
    //    
    // Config main loop
    bool retry =false;
    for(int j = 1; j <= dynlen(pCodes); j++) { 
      //
      
      mainTable.cellValueRC(i-1, "pName", "Wr Get Code");
      //      
      // Write Get Code to PLC      
      // Clean com registers
      dpSetWait(plcDps[i] + DPE_WR_CODE, 0);
      //dpSetWait(plcDps[i] + DPE_RD_CODE, 0);

      dpSetWait(plcDps[i] + DPE_WR_CODE, pCodes[j]);
      //      
      // Check Code feedback from PLC
      int feedback_code = -9999;
      int loopNb = 0;
      bool waitFeedback = true;  
      while (waitFeedback) {
        delay(1); // wait the PLC has time to answere
        dpGet(plcDps[i] + DPE_RD_CODE, feedback_code);
        DebugTN("feedback_code ", feedback_code);
        if(pCodes[j] == feedback_code) {
          mainTable.cellValueRC(i-1, "pName", MESS_OK);          
          waitFeedback = false;          
        }         
        else if(loopNb > CODE_TIMEOUT) {
          mainTable.cellValueRC(i-1, "txSt", makeDynString(TX_CHECK_CODE_ERR + pNames[j], COLOR_ERROR));
          err = true;
          waitFeedback = false;
        }  
        loopNb ++;
      }
      // 
      // Read Value feedback from PLC
      int iFeedback_val = -9999;
      float fFeedback_val = -9999.9;
      delay(2); // wait the PLC has time to answere
      if(pTypes[j] == P_INTEGER) {
        dpGet(plcDps[i] + DPE_RD_INTEGER_VAL, iFeedback_val);
        mainTable.cellValueRC(i-1,  pNames[j], iFeedback_val);
      }
      if(pTypes[j] == P_FLOAT) {
        dpGet(plcDps[i] + DPE_RD_INTEGER_VAL, fFeedback_val);
        mainTable.cellValueRC(i-1,  pNames[j], fFeedback_val);
      }         
    }
    if(err) {
      mainTable.cellValueRC(i-1, "pName", makeDynString("Read Params Err", COLOR_ERROR));
    }    
    else {
      mainTable.cellValueRC(i-1, "txSt", "Read Params finished");
    }  
  }
  // activate process buttons
  for(int i = 1; i <= dynlen(buttonsToDisable); i++) {
    shape pb = buttonsToDisable[i];
    pb.enabled(true);  
  }
  DebugTN("vacPanelConfigVPLC_PF.pnl: ----- Fixed Pumping group PLC configuration end");  
}
/*
@brief Private Get the row number from PLC number in the list (first column "nb")
*/
int _getTableRowNb(shape table, int nbPlc) {
  //DebugTN(" indexConnId ", sIndexConnId);  
  for(int i = 0; i < dynlen(plcDps); i++) {
    string sNbVal;
    getValue(table, "cellValueRC", i, "nb", sNbVal);
    //DebugTN(" connIdVal ", sConnIdVal, i);
    if( sNbVal == (string)nbPlc) {
      //DebugTN(" found i", i);      
      return i;    
    }     
  }
}
/*
@brief Private Call Back function check system integrity
@param[in,out] userData array of tableShape, columnName, rowNb  to be updated
@param[in] dp DP name called back
@param[in] alarm DP Value called back
*/
void _updateSystIntegrityCB(dyn_anytype userData, string dp, unsigned alarm) {
  shape tableShape  = userData[1];
  string columnName = userData[2];
  int rowNb         = userData[3]; 
  string integrity;
  if(alarm != 0) {
    integrity = "Bad";
  }
  else {
    integrity = "Ok";
  }
  tableShape.cellValueRC(rowNb, columnName, integrity);
}
/**
@brief Set 0 in column updat?
@param mainTable The PLC table of the panel
*/
void UnselectAllConfigVPLC_PF(shape mainTable) {
  int tableRowNb = mainTable.lineCount();  
  for (int i = 0; i < tableRowNb; i++) {
    mainTable.cellValueRC(i, "needUpdate", 0);
  }
}  
/**
@brief Set 1 in column updat?
@param mainTable The PLC table of the panel
*/
void SelectAllConfigVPLC_PF(shape mainTable) {
  int tableRowNb = mainTable.lineCount();  
  for (int i = 0; i < tableRowNb; i++) {
    mainTable.cellValueRC(i, "needUpdate", 1);
  }
}  
