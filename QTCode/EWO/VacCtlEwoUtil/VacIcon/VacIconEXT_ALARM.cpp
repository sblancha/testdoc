#include "VacIconEXT_ALARM.h"

#include "EqpEXT_ALARM.h"

#include <QPainter>

VacIconEXT_ALARM::VacIconEXT_ALARM(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(11, 11);
}

VacIconEXT_ALARM::~VacIconEXT_ALARM()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconEXT_ALARM::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Down:
	case VacIconContainer::Up:
		return VacIconGeometry(11, 11, 5, 5, 1);
	default:
		break;
	}
	return VacIconGeometry(11, 11, 5, 5, 1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconEXT_ALARM::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(11);
		size.setHeight(11);
		break;
	default:
		size.setWidth(11);
		size.setHeight(11);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconEXT_ALARM::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpEXT_ALARM"), "VacIconEXT_ALARM::setEqp", pEqp->getDpName());
		this->pEqp = (EqpEXT_ALARM *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconEXT_ALARM::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconEXT_ALARM::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconEXT_ALARM::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconEXT_ALARM::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
		painter.setPen(Qt::black);
	}
	else
	{
		painter.setPen(Qt::blue);
	}

	// Image is just small arrow
	switch(direction)
	{
	case VacIconContainer::Up:
		painter.drawLine(5, 1, 5, 9);
		painter.drawLine(1, 5, 5, 1);
		painter.drawLine(5, 1, 9, 5);
		break;
	case VacIconContainer::Down:
		painter.drawLine(5, 1, 5, 9);
		painter.drawLine(1, 5, 5, 9);
		painter.drawLine(5, 9, 9, 5);
		break;
	case VacIconContainer::Left:
		painter.drawLine(1, 5, 9, 5);
		painter.drawLine(1, 5, 5, 1);
		painter.drawLine(1, 5, 5, 9);
		break;
	case VacIconContainer::Right:
		painter.drawLine(1, 5, 9, 5);
		painter.drawLine(5, 1, 9, 5);
		painter.drawLine(5, 9, 9, 5);
		break;
	}
}
