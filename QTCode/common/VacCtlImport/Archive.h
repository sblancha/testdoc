#ifndef ARCHIVE_H
#define ARCHIVE_H

#include "VacCtlImportExport.h"

//	Class holding PVSS archive data

#include "FileParser.h"
#include "ImportConstants.h"

class ImportPool;

class VACCTLIMPORT_EXPORT Archive : public FileParser
{
public:
	~Archive() {}

	static void setImportPool(ImportPool *pImportPool);
	static Archive *createFromFile(FILE *pFile, int &lineNo, const char *name,
		QStringList &errList);

	void dump(FILE *pFile);

	// Access
	inline const char *getName(void) { return name; }
	inline const char *getDpName(void) { return dpName; }
	void setDpName(const char *name);
	inline int getSmoothType(void) { return smoothType; }
	inline int getTimeInterval(void) { return timeInterval; }
	inline int getDeadBand(void) { return deadBand; }
	inline int getDepth(void) { return depth; }
	inline int getRefCount(void) { return refCount; }
	inline void incrementRefCount(void) { refCount++; }
	inline int getLineNo(void) { return lineNo; }

protected:
	Archive(const char *name, int lineNo);

	// Archive name
	char					*name;

	// Archive DP name
	char					*dpName;

	// Smoothing type
	int						smoothType;

	// Time interval
	int						timeInterval;

	// Dead band
	int						deadBand;

	// Archive depth [days]
	int						depth;

	// Line number in input file where this archive was defined
	int						lineNo;

	// How many DPEs refer to this archive
	int						refCount;

	// Reference to ImportPool instance
	static ImportPool		*pPool;

	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);
};

#endif	// ARCHIVE_H
