//	Implementation of VacIconGeometry class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconGeometry.h"

VacIconGeometry::VacIconGeometry(int width, int height, int x, int y, int verticalPos)
{
	this->width = width;
	this->height = height;
	this->x = x;
	this->y = y;
	this->verticalPos = verticalPos;
}

VacIconGeometry::VacIconGeometry(const VacIconGeometry &source)
{
	width = source.width;
	height = source.height;
	x = source.x;
	y = source.y;
	verticalPos = source.verticalPos;
}

