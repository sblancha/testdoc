#ifndef	IMAGINEITEMPOLYGON_H
#define	IMAGINEITEMPOLYGON_H

#include "ImageItem.h"

#include <QPolygon>

class VACCTLEWOUTIL_EXPORT ImageItemPolygon : public ImageItem
{
public:
	ImageItemPolygon();
	~ImageItemPolygon();

	inline void clear(void) { points.clear(); }
	inline void addPoint(int x, int y) { points.append(QPoint(x, y)); }
	inline bool isEmpty(void) const { return points.isEmpty(); }
	virtual void draw(QPainter &painter);
	virtual bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	// Access
	inline QPolygon &getPoints(void) { return points; }
	inline bool isClosed(void) const { return closed; }
	inline void setClosed(bool flag) { closed = flag; }
	inline bool isFilled(void) const { return filled; }
	inline void setFilled(bool flag) { filled = flag; }

protected:
	// Points
	QPolygon	points;

	// If poygon is closed
	bool		closed;

	// If polygon is filled
	bool		filled;

	virtual void parseProp(QXmlStreamReader &reader);
	virtual void parsePoints(QXmlStreamReader &reader);
};

#endif	// IMAGINEITEMPOLYGON_H
