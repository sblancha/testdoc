#ifndef MAINPART_H
#define MAINPART_H

#include "VacCtlEqpDataExport.h"

// Class holding one main part data

#include "FileParser.h"

#include <qlist.h>

class DataPool;
class BeamLine;
class Eqp;

class MainPart : public FileParser
{
public:
	MainPart(const char *name);
	virtual ~MainPart();

	static MainPart *createFromFile(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		QStringList &errList);

	void addEqp(Eqp *pEqp) { eqpList.append(pEqp); }

protected:
	// List of all devices in this main part - to speed up 'eqp in main part' functionality
	QList<Eqp *>			eqpList;

	// Main part name
	char			*name;

	// Name of main part for PVSS (restricted character set)
	char			*pvssName;

	// Start coordinate of main part
	float			start;

	// End coordinate of main part
	float			end;

	// 'Main' beam line, i.e. beam line where the most of sectors are located
	BeamLine		*pMainLine;

	// OR'ed vacuum type of all sectors belonging to this main part (see class VacType)
	int				vacTypeMask;

public:
	VACCTLEQPDATA_EXPORT inline const char *getName(void) { return name; }
	VACCTLEQPDATA_EXPORT inline const char *getPvssName(void) { return pvssName; }
	VACCTLEQPDATA_EXPORT inline float getStart(void) { return start; }
	VACCTLEQPDATA_EXPORT inline float getEnd(void) { return end; }
	VACCTLEQPDATA_EXPORT inline BeamLine *getMainLine(void) { return pMainLine; }
	VACCTLEQPDATA_EXPORT inline void setMainLine(BeamLine *pMainLine) { this->pMainLine = pMainLine; }
	VACCTLEQPDATA_EXPORT inline int getVacTypeMask(void) { return vacTypeMask; }
	VACCTLEQPDATA_EXPORT inline void setVacTypeMask(int vacTypeMask) { this->vacTypeMask |= vacTypeMask; }
	VACCTLEQPDATA_EXPORT inline QList<Eqp *> getEqpList(void) { return eqpList; }

	// Implementation of FileParser abstract method
	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);
};

#endif	// MAINPART_H
