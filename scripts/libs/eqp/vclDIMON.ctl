
void LhcVacDeviceAction_DIMON(string dpName, string dpType, dyn_anytype action, dyn_string &exceptionInfo)
{
  mapping   mConnectDpes;
 
  unsigned act = action[1] & LVA_ACTION_MASK;  
  switch(act & LVA_ACTION_MASK)
  {
  case LVA_DEVICE_ACK:
    LhcVacSetActionDpeValue(dpName + ".WR1", (action[2] | (1 << 8)), exceptionInfo);
    return;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["TS0.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS0.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS1.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS1.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS3.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS3.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS4.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS4.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS5.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS5.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS6.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS6.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS7.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS7.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["TS0.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS0.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS1.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS1.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS3.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS3.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS4.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS4.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS5.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS5.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS6.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS6.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS7.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS7.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }  
}
