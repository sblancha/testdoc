//	Implementation of VacIconCRYO_TT_CUM class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconCRYO_TT_SUM.h"

#include "EqpCRYO_TT_SUM.h"

#include "FunctionalType.h"
#include "VacEqpTypeMask.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			153,153,255
#define COLOR_GREEN				153,255,153
#define COLOR_YELLOW			255,255,153
#define COLOR_RED				255,153,153

//	Main state bits to be analyzed
#define	RR1_INVALID		(0x1)

// Limits for temperatures
#define	CM_T_WARNING	((float)5)
#define	CM_T_BAD		((float)40)
#define	BS_T_WARNING	((float)5)
#define	BS_T_BAD		((float)40)

#define	max(a,b)	((a) > (b) ? (a) : (b))


/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconCRYO_TT_SUM::VacIconCRYO_TT_SUM(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(23, 11);
}

VacIconCRYO_TT_SUM::~VacIconCRYO_TT_SUM()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		It is supposed that this method will not be used for CRYO_TT
*/
VacIconGeometry VacIconCRYO_TT_SUM::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(23, 11, 11, 11, 0);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconCRYO_TT_SUM::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpCRYO_TT_SUM"), "VacIconCRYO_TT_SUM::setEqp", pEqp->getDpName());
		this->pEqp = (EqpCRYO_TT_SUM *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconCRYO_TT_SUM::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconCRYO_TT_SUM::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("CM.RR1");
	dpes.append("CM.T");
	dpes.append("BSIN.RR1");
	dpes.append("BSIN.T");
	dpes.append("BSOUT.RR1");
	dpes.append("BSOUT.T");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconCRYO_TT_SUM::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconCRYO_TT_SUM::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconCRYO_TT_SUM::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);

	// Selection border
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// CM part of icon
	unsigned rr1 = RR1_INVALID;
	float	t = 0;
	if(pEqp)
	{
		rr1 = pEqp->getCMRR1(mode);
		t = pEqp->getCMT(mode);
	}
	setPainter(painter, rr1, t, false);
	painter.drawRect(1, 1, 10, 9);

	// BS part of icon
	rr1 = RR1_INVALID;
	t = 0;
	if(pEqp)
	{
		rr1 = pEqp->getBSINRR1(mode) | pEqp->getBSOUTRR1(mode);
		t = pEqp->getBSINT(mode);
		float t2 = pEqp->getBSOUTT(mode);
		t = max(t, t2);
	}
	setPainter(painter, rr1, t, true);
	painter.drawRect(12, 1, 10, 9);

	// Separator between two parts
	painter.setPen(Qt::black);
	painter.drawLine(11, 1, 11, 9);

}

void VacIconCRYO_TT_SUM::setPainter(QPainter &painter, unsigned rr1, float t, bool isBs)
{
	QColor color(COLOR_UNDEFINED);
	if(!(rr1 & RR1_INVALID))
	{
		if(t < (isBs ? BS_T_WARNING : CM_T_WARNING))
		{
			color.setRgb(COLOR_GREEN);
		}
		else if(t < (isBs ? BS_T_BAD : CM_T_BAD))
		{
			color.setRgb(COLOR_YELLOW);
		}
		else
		{
			color.setRgb(COLOR_RED);
		}
	}
	painter.setPen(color);
	painter.setBrush(color);
}

