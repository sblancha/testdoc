#
# Project file for qmake utility to build Makefile for VacEqpData DLL
#
#	25.07.2008	L.Kopylov
#		Initial version
#
#	13.12.2008	L.Kopylov
#		Rename target library to VacCtlEqpData
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	10.01.2010	L.Kopylov
#		FunctionalType has been moved here from common/VacCtlUtil.
#		Change to dll on Windows
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#
#	08.07.2014	S. Blanchard
#   	Replace VGF_DP by VG_DP
#	30.07.2014	S. Blanchard
#   	Add VPC_HCCC
#	18.09.2014	S. Blanchard
#   	Add VV_PUL
#	06.10.2014	S. Blanchard
#   	Add ALARM_DO
#	11.12.2015	S. Blanchard
#		add VP_I0, VR_PI, VI_RI
#	07.02.2016	S. Blanchard
#		add VP_TP
#	14.10.2016	S. Blanchard
#		remove VP_I0, add VP_IP, VR_PI_CHAN
#	21.03.2018 S. Blanchard
#		add VP_GU
#	27.06.2018 S. Blanchard
#		add V_TCP
#	01.08.2018 S. Blanchard
#		add VR_ER, VR_EC, VR_EA
# 02.10.2018 S. Blanchard
#		add VA_RI
# 14.02.2019 S. Blanchard
#		add VG_PF VPGF_LHC
# 01.05.2019 S. BLanchard
#		add VG_U

TEMPLATE = lib

TARGET = VacCtlEqpData

unix:MAKEFILE = GNUmakefile

unix:VERSION = 1.0.1

# To avoid having version numbers in .so file name
unix:CONFIG += plugin

OBJECTS_DIR = obj

DESTDIR = ../../../bin

unix:CONFIG += qt thread release warn_on
win32:CONFIG += qt dll thread release

# To produce PDB file together with DLL
win32:QMAKE_CXXFLAGS+=/Zi
win32:QMAKE_LFLAGS+= /INCREMENTAL:NO /Debug

#DEFINES += _VACDEBUG

win32:DEFINES += BUILDING_VACCTLEQPDATA


INCLUDEPATH += \
	../VacCtlUtil \
	../Platform \
	../VacCtlHistoryData \
	./ \
	ResourcePool \
	FunctionalType \
	DataPool \
	Eqp \
	MobilePool \
	Rack \
	Interlock

HEADERS = ../Platform/PlatformDef.h \
	../VacCtlUtil/DataEnum.h \
	../VacCtlUtil/NamePool.h \
	../VacCtlUtil/FileParser.h \
	VacCtlEqpDataExport.h \
	DataPool/BeamLineConnType.h \
	DataPool/VacType.h \
	DataPool/BeamLinePart.h \
	DataPool/BeamLine.h \
	DataPool/SectorDrawPart.h \
	DataPool/MainPartMap.h \
	DataPool/Sector.h \
	DataPool/MainPart.h \
	DataPool/LhcRegion.h \
#	DataPool/LhcRegionList.h \
	DataPool/PlcConfig.h \
	DataPool/DataPool.h \
	Eqp/EqpType.h \
	Eqp/MobileType.h \
	Eqp/Eqp.h \
	Eqp/EqpPLC.h \
	Eqp/EqpV_TCP.h \
	Eqp/EqpVA_RI.h \
	Eqp/EqpVV.h \
	Eqp/EqpVG.h \
	Eqp/EqpVG_U.h \
	Eqp/EqpVG_STD.h \
	Eqp/EqpVRPI.h \
	Eqp/EqpVPI.h \
	Eqp/EqpVRCG.h \
	Eqp/EqpMP_ACCESS.h \
	Eqp/EqpVPG_EA.h \
	Eqp/EqpSECT_VPI_SUM.h \
	Eqp/EqpVCLDX.h \
	Eqp/EqpCOLDEX.h \
	Eqp/EqpVACOK.h \
	Eqp/EqpVPG_SA.h \
	Eqp/EqpVPG_BP.h \
	Eqp/EqpVPG_STD.h \
	Eqp/EqpVP_STDIO.h \
	Eqp/EqpVPC_HCCC.h \
	Eqp/EqpCRYO_TT.h \
	Eqp/EqpVGTR.h \
	Eqp/EqpEXP_AREA.h \
	Eqp/EqpEXT_ALARM.h \
	Eqp/EqpVPGM.h \
	Eqp/EqpVPGMPR.h \
	Eqp/EqpCRYO_TT_SUM.h \
	Eqp/EqpBEAM_Intens.h \
	Eqp/EqpVLV_ANA.h \
	Eqp/EqpVPR_VELO.h \
	Eqp/EqpVPT_VELO.h \
	Eqp/EqpVOPS_VELO.h \
	Eqp/EqpVINT_VELO.h \
	Eqp/EqpVPRO_VELO.h \
	Eqp/EqpVPT100.h \
	Eqp/EqpVBeamParamFloat.h \
	Eqp/EqpVBeamParamInt.h \
	Eqp/EqpVRPM.h \
	Eqp/EqpVIES.h \
	Eqp/EqpVV_PS_CMW.h \
	Eqp/EqpVP_VG_PS_CMW.h \
	Eqp/EqpVV_AO.h \
	Eqp/EqpVV_STD.h \
	Eqp/EqpVV_PUL.h \
	Eqp/EqpProcess.h \
	Eqp/EqpVBAKEOUT.h \
	Eqp/EqpINJ_6B02.h \
	Eqp/EqpVRJ_TC.h \
	Eqp/EqpVITE.h \
	Eqp/EqpV8DI_FE.h \
	Eqp/EqpALARM_DO.h \	
	Eqp/EqpSMSMachineMode.h \
	Eqp/EqpVPN.h \
	Eqp/EqpVP_NEG.h \
	Eqp/EqpVPNMUX.h \
	Eqp/EqpVG_DP.h \
	Eqp/EqpVPS.h \
	Eqp/EqpVPS_Process.h \
	Eqp/EqpITL_CMNT.h \
	Eqp/EqpITL_CMUX.h \
	Eqp/EqpVG_PT.h \
	Eqp/EqpVP_IP.h \
	Eqp/EqpVR_PI_CHAN.h \
	Eqp/EqpVR_PI.h \
	Eqp/EqpVP_TP.h \
	Eqp/EqpVR_GT.h \
	Eqp/EqpVPG_MBLK.h \
	Eqp/EqpVP_GU.h \
	Eqp/EqpVR_ER.h \
	Eqp/EqpVR_EC.h \
	Eqp/EqpVR_EA.h \
	Eqp/EqpVG_A_RO.h \
	Eqp/EqpVOPS_2PS.h \
	Eqp/EqpVPGF_LHC.h \
	Eqp/EqpVG_PF.h \
	Eqp/EqpVELO_P_Main.h \
	Eqp/EqpVELO_P_Interlocks.h \
	Eqp/EqpVELO_P_Bakeout.h \
	MobilePool/BakeoutWorkDp.h \
	MobilePool/MobilePool.h \
	FunctionalType/StdEqpMessage.h \
	FunctionalType/EqpMsgCriteria.h \
	FunctionalType/EqpMsgCriteria.h \
	FunctionalType/FunctionalTypeVA.h \
	FunctionalType/FunctionalTypeVV.h \
	FunctionalType/FunctionalTypeVG.h \
	FunctionalType/FunctionalTypeVPI.h \
	FunctionalType/FunctionalTypeVPG.h \
	FunctionalType/FunctionalTypeVPGF.h \
	FunctionalType/FunctionalTypeVP_STDIO.h \
	FunctionalType/FunctionalTypeVPC_HCCC.h \
	FunctionalType/FunctionalTypeVPGM.h \
	FunctionalType/FunctionalTypeVACOK.h \
	FunctionalType/FunctionalTypeCRYO_TT.h \
	FunctionalType/FunctionalTypeVV_ANA.h \
	FunctionalType/FunctionalTypePLC.h \
	FunctionalType/FunctionalTypePROCESS.h \
	FunctionalType/FunctionalTypePROCESS_VPG_6A01.h \
	FunctionalType/FunctionalTypePROCESS_BGI_6B01.h \
	FunctionalType/FunctionalTypePROCESS_VPG_6E01.h \
	FunctionalType/FunctionalTypeVBAKEOUT.h \
	FunctionalType/FunctionalTypeINJ_6B02.h \
	FunctionalType/FunctionalTypeVRJ_TC.h \
	FunctionalType/FunctionalTypeVITE.h \
	FunctionalType/FunctionalTypeVPS.h \
	FunctionalType/FunctionalTypeVPS_Process.h \
	FunctionalType/FunctionalTypeV8DI_FE.h \
	FunctionalType/FunctionalTypeALARM_DO.h \
	FunctionalType/FunctionalTypeVP_NEG.h \
	FunctionalType/FunctionalTypeVPNMUX.h \
	FunctionalType/FunctionalTypeITL_CMNT.h \
	FunctionalType/FunctionalTypeITL_CMUX.h \
	FunctionalType/FunctionalTypeSMS_MACHINE_MODE.h \
	FunctionalType/FunctionalTypeVRE.h \
	FunctionalType/VacEqpTypeMask.h \
	FunctionalType/FunctionalTypeVELO_Process.h \
	Rack/Rack.h \
	Rack/RackEqp.h \
	Interlock/InterlockEqp.h \
	Interlock/InterlockChain.h \
	ResourcePool/ResourceEqpMask.h \
	ResourcePool/ResourcePool.h

HEADERS += DataPool/SectorOrder.h \
	DataPool/LhcBeamSectorOrder.h


SOURCES = DataPool/BeamLineConnType.cpp \
	DataPool/VacType.cpp \
	DataPool/BeamLinePart.cpp \
	DataPool/BeamLine.cpp \
	DataPool/SectorDrawPart.cpp \
	DataPool/Sector.cpp \
	DataPool/MainPart.cpp \
	DataPool/LhcRegion.cpp \
#	DataPool/LhcRegionList.cpp \
	DataPool/PlcConfig.cpp \
	DataPool/DataPool.cpp \
	Eqp/EqpType.cpp \
	Eqp/MobileType.cpp \
	Eqp/Eqp.cpp \
	Eqp/EqpPLC.cpp \
	Eqp/EqpV_TCP.cpp \
	Eqp/EqpVA_RI.cpp \
	Eqp/EqpVV.cpp \
	Eqp/EqpVG.cpp \
	Eqp/EqpVG_U.cpp \
	Eqp/EqpVG_STD.cpp \
	Eqp/EqpVRPI.cpp \
	Eqp/EqpVPI.cpp \
	Eqp/EqpVRCG.cpp \
	Eqp/EqpMP_ACCESS.cpp \
	Eqp/EqpVPG_EA.cpp \
	Eqp/EqpSECT_VPI_SUM.cpp \
	Eqp/EqpVCLDX.cpp \
	Eqp/EqpCOLDEX.cpp \
	Eqp/EqpVACOK.cpp \
	Eqp/EqpVPG_SA.cpp \
	Eqp/EqpVPG_BP.cpp \
	Eqp/EqpVPG_STD.cpp \
	Eqp/EqpVP_STDIO.cpp \
	Eqp/EqpVPC_HCCC.cpp \
	Eqp/EqpCRYO_TT.cpp \
	Eqp/EqpVGTR.cpp \
	Eqp/EqpEXP_AREA.cpp \
	Eqp/EqpEXT_ALARM.cpp \
	Eqp/EqpVPGM.cpp \
	Eqp/EqpVPGMPR.cpp \
	Eqp/EqpCRYO_TT_SUM.cpp \
	Eqp/EqpBEAM_Intens.cpp \
	Eqp/EqpVLV_ANA.cpp \
	Eqp/EqpVPR_VELO.cpp \
	Eqp/EqpVPT_VELO.cpp \
	Eqp/EqpVOPS_VELO.cpp \
	Eqp/EqpVINT_VELO.cpp \
	Eqp/EqpVPRO_VELO.cpp \
	Eqp/EqpVPT100.cpp \
	Eqp/EqpVBeamParamFloat.cpp \
	Eqp/EqpVBeamParamInt.cpp \
	Eqp/EqpVRPM.cpp \
	Eqp/EqpVIES.cpp \
	Eqp/EqpVV_PS_CMW.cpp \
	Eqp/EqpVP_VG_PS_CMW.cpp \
	Eqp/EqpVV_AO.cpp \
	Eqp/EqpVV_STD.cpp \
	Eqp/EqpVV_PUL.cpp \
	Eqp/EqpProcess.cpp \
	Eqp/EqpVBAKEOUT.cpp \
	Eqp/EqpINJ_6B02.cpp \
	Eqp/EqpVRJ_TC.cpp \
	Eqp/EqpVITE.cpp	\
	Eqp/EqpV8DI_FE.cpp \
	Eqp/EqpALARM_DO.cpp \
	Eqp/EqpSMSMachineMode.cpp \
	Eqp/EqpVPN.cpp \
	Eqp/EqpVP_NEG.cpp \
	Eqp/EqpVPNMUX.cpp \
	Eqp/EqpVG_DP.cpp \
	Eqp/EqpVPS.cpp \
	Eqp/EqpVPS_Process.cpp \
	Eqp/EqpITL_CMNT.cpp \
	Eqp/EqpITL_CMUX.cpp \
	Eqp/EqpVG_PT.cpp \
	Eqp/EqpVP_IP.cpp \
	Eqp/EqpVR_PI_CHAN.cpp \
	Eqp/EqpVR_PI.cpp \
	Eqp/EqpVP_TP.cpp \
	Eqp/EqpVR_GT.cpp \
	Eqp/EqpVPG_MBLK.cpp \
	Eqp/EqpVP_GU.cpp \
	Eqp/EqpVR_ER.cpp \
	Eqp/EqpVR_EC.cpp \
	Eqp/EqpVR_EA.cpp \
	Eqp/EqpVG_A_RO.cpp \
	Eqp/EqpVOPS_2PS.cpp \
	Eqp/EqpVPGF_LHC.cpp \
	Eqp/EqpVG_PF.cpp \
	Eqp/EqpVELO_P_Main.cpp \
	Eqp/EqpVELO_P_Interlocks.cpp \		
	Eqp/EqpVELO_P_Bakeout.cpp \
	Rack/Rack.cpp \
	Rack/RackEqp.cpp \
	Interlock/InterlockEqp.cpp \
	Interlock/InterlockChain.cpp \
	MobilePool/BakeoutWorkDp.cpp \
	MobilePool/MobilePool.cpp \
	FunctionalType/FunctionalType.cpp \
	FunctionalType/StdEqpMessage.cpp \
	FunctionalType/EqpMsgCriteria.cpp \
	FunctionalType/FunctionalTypeVA.cpp \
	FunctionalType/FunctionalTypeVV.cpp \
	FunctionalType/FunctionalTypeVG.cpp \
	FunctionalType/FunctionalTypeVPI.cpp \
	FunctionalType/FunctionalTypeVPG.cpp \
	FunctionalType/FunctionalTypeVPGF.cpp \
	FunctionalType/FunctionalTypeVP_STDIO.cpp \
	FunctionalType/FunctionalTypeVPC_HCCC.cpp \
	FunctionalType/FunctionalTypeVPGM.cpp \
	FunctionalType/FunctionalTypeVACOK.cpp \
	FunctionalType/FunctionalTypeCRYO_TT.cpp \
	FunctionalType/FunctionalTypeVV_ANA.cpp \
	FunctionalType/FunctionalTypePLC.cpp \
	FunctionalType/FunctionalTypePROCESS.cpp \
	FunctionalType/FunctionalTypeVBAKEOUT.cpp \
	FunctionalType/FunctionalTypePROCESS_VPG_6A01.cpp \
	FunctionalType/FunctionalTypePROCESS_BGI_6B01.cpp \
	FunctionalType/FunctionalTypePROCESS_VPG_6E01.cpp \
	FunctionalType/FunctionalTypeINJ_6B02.cpp \
	FunctionalType/FunctionalTypeVRJ_TC.cpp \
	FunctionalType/FunctionalTypeVITE.cpp \
	FunctionalType/FunctionalTypeVPS.cpp \
	FunctionalType/FunctionalTypeVPS_Process.cpp \
	FunctionalType/FunctionalTypeV8DI_FE.cpp \
	FunctionalType/FunctionalTypeALARM_DO.cpp \
	FunctionalType/FunctionalTypeVP_NEG.cpp \
	FunctionalType/FunctionalTypeVPNMUX.cpp \
	FunctionalType/FunctionalTypeITL_CMNT.cpp \
	FunctionalType/FunctionalTypeITL_CMUX.cpp \
	FunctionalType/FunctionalTypeSMS_MACHINE_MODE.cpp \
	FunctionalType/FunctionalTypeVRE.cpp \
	FunctionalType/VacEqpTypeMask.cpp \
	FunctionalType/FunctionalTypeVELO_Process.cpp \
	ResourcePool/ResourcePool.cpp


SOURCES += DataPool/SectorOrder.cpp \
	DataPool/LhcBeamSectorOrder.cpp


LIBS += -L../../../bin \
	-lVacCtlHistoryData \
	-lVacCtlUtil 
