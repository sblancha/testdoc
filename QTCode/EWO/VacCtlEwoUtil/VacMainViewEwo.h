#ifndef VACMAINVIEWEWO_H
#define	VACMAINVIEWEWO_H

// Superclass for all MainViewXXX EWOs - provides basic EWO features (mainly - common signals),
// without taking into account particular main view features

#include <BaseExternWidget.hxx>

#include "VacMainView.h"

class VACCTLEWOUTIL_EXPORT VacMainViewEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	VacMainViewEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;

protected slots:
	void selectionChanged(void);
	void sectorMouseDown(int button, int x, int y, const char *sectorName);
	void dpMouseDown(int button, int mode, int x, int y, int extra, const char *dpName);
	void dpIconMouseDown(int button, int mode, int x, int y, int extra, const char *dpName);
	void rackIconMouseDown(int button, int mode, int x, int y, int extra, const char * dpName);
	void dpConnectChanged(void);
	void dpeHistoryQueryQueueChange(void);
	void dpPollingChanged(void);
	void dialogRequest(int type, unsigned vacType, const QStringList &sectorList,
		DataEnum::DataMode mode);
	void pollingPeriodChanged(int period);
	void mobileHistoryQueryChange(void);
	void dpeHistoryDepthQueryChange(void);
	void historySaveRequest(int id);
	void historySaveMultiRequest(int id);
	void historyLoadRequest(int id);
	void historyFillNumberRequest(int id);
	void sectorDoubleClicked(const QString &sector, int mode);
	void eqpDoubleClicked(const QString &dpName, int mode);
	void synSectorMouseDown(int button, int mode, int x, int y, const char *sectorName, bool selected); // [VACCO-929]

protected:
	VacMainView	*baseWidget;

	virtual void connectStdSignals(void);
};


#endif	// VACMAINVIEWEWO_H
