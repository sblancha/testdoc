// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Component Name: VacCtlEqpData librairy
Class: Equipment Vacuum Alarm - Relay of Ion pump controller (VA_RI)
Language: C++\Qt

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: This file contains the defined classes for the equipment acuum Alarm - Relay of Ion pump controller (VA_RI)

Limitations: To be used with vacuum framework.

Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------
#ifndef EQPVA_RI_H
#define	EQPVA_RI_H

#include "Eqp.h"
#include "EqpVR_PI.h"

// Color definitions are in Eqp.h
// Color used in this class:
//		COLOR_GP_NOEQP
//		COLOR_GP_INVALID
//		COLOR_GP_ON
//		COLOR_GP_ERROR

#define	RR1_FORCED				(0x0001) // 0x00 01

class VACCTLEQPDATA_EXPORT EqpVA_RI : public Eqp
{
	Q_OBJECT
public:
	EqpVA_RI(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVA_RI(const EqpVA_RI &src) : Eqp(src) {}
#endif
	~EqpVA_RI();

	virtual void postProcess(void);

	virtual void getMaster(QString &masterDp, int &masterChannel);
	virtual inline Eqp *getMaster(void) { return (Eqp *)pMaster; }
	virtual inline QString getMasterDp(void) { return (QString)masterDp; }
	virtual void getTargets(QStringList &targets); 

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);

	virtual bool isParamNotDef();

	virtual int			getChannelSourceDef() { return channelSourceDef; }
	virtual double		getThresholdDef() { return thresholdDef; }
	virtual int			getHysteresisPctDef() { return hysteresisPctDef; }
	virtual bool		isPositionMayChange() { return true; }

#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	
	inline int getchannelSourceSt(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? channelSourceStReplay : channelSourceStOnline; }
	inline double getthresholdSt(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? thresholdStReplay : thresholdStOnline; }
	inline int gethysteresisPctSt(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? hysteresisPctStReplay : hysteresisPctStOnline; }
	
	virtual DataEnum::AlarmArrowType getArrowType() { return arrowType;  }

	virtual void attachToSourceChange(bool isInit);

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:

	// Pointer to VRPI controlling this
	EqpVR_PI	*pMaster;
	QString		masterDp;
	int			relayNb;

	// Last known value of RR1 DPE 
	unsigned	rr1Online;
	unsigned	rr1Replay;
	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;
	bool		plcAlarmReplay;
	// Parameter Status
	int			channelSourceStOnline;
	int			channelSourceStReplay;
	double		thresholdStOnline;
	double		thresholdStReplay;
	int			hysteresisPctStOnline;
	int			hysteresisPctStReplay;
	
	// Parameter default value from DB
	int			channelSourceDef;
	double		thresholdDef;
	int			hysteresisPctDef;

	// for the icon
	DataEnum::AlarmArrowType arrowType;
	
#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, bool comAlarm, bool isNotDefaultParameters, QColor &color);
	virtual void getMainStateString(unsigned rr1, bool comAlarm, QString &string);
	virtual void getToolTipString(unsigned rr1, bool comAlarm, QString &string);

private:

#endif
};


#endif	// EQPVA_RI_H


