// Implementation of NullDateEdit class
/////////////////////////////////////////////////////////////////////////////////

#include "NullDateEdit.h"

#include <qdatetime.h>

void NullDateEdit::fix(void)
{
	if(!date().isNull())
	{
		QDateEdit::fix();
	}
}

