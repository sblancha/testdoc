#
# Project file for qmake utility to build ValueProfile EWO
#
#	28.07.2011	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = ValueProfile

INCLUDEPATH += ../../common/VacCtlUtil \
	../../common/Platform

HEADERS =	ValueProfileWidget.h \
	ValueProfile.h

SOURCES = ValueProfileWidget.cpp \
	ValueProfile.cpp

LIBS += -lVacCtlUtil

