#ifndef EQPPLC_H
#define EQPPLC_H

//	PLC health control

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpPLC : public Eqp
{
	Q_OBJECT

public:
	EqpPLC(const Eqp &source);
#ifdef Q_OS_WIN
	EqpPLC(const EqpPLC &src) : Eqp(src) {}
#endif
	~EqpPLC() {}

	virtual void getStateDpes(QStringList &stateDpes);
	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual bool isAlive(DataEnum::DataMode mode) const;

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

protected:
	// Last known value of the only DPE for online mode
	bool	alarmOnline;

	// Last known value of the only DPE for replay mode
	bool	alarmReplay;

	// Timestamp for alarm in online mode
	QDateTime	timeStampOnline;

	// Timestamp for alarm in replay mode
	QDateTime	timeStampReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(int alarm, QColor &color);
	virtual void getMainStateString(int alarm, QString &string);
#endif

};

#endif	// EQPPLC_H
