//	Implementation of SmsConfigParser class
/////////////////////////////////////////////////////////////////////////////////

#include "SmsConfigParser.h"
#include "SmsConfig.h"
#include "NotificationConfig.h"

#include "ResourcePool.h"
#include "FileParser.h"
#include "DebugCtl.h"

#include "DataPool.h"
#include "Eqp.h"

#include <QFile>
#include <QTextStream>

#include <stdio.h>

SmsConfigParser SmsConfigParser::instance;

SmsConfigParser &SmsConfigParser::getInstance(void)
{
	return instance;
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
SmsConfigParser::SmsConfigParser(void)
{
}

SmsConfigParser::~SmsConfigParser()
{
	clear();
}

void SmsConfigParser::clear(void)
{
	while(!list.isEmpty())
	{
		delete list.takeFirst();
	}
}

int SmsConfigParser::addConfig(const char *id)
{
	SmsConfig *pConfig = new SmsConfig(id);
	list.append(pConfig);
	return pConfig->getId();
}

int SmsConfigParser::setVisibleName(int configId, const char *name, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->setVisibleName(name);
	return 0;
}

int SmsConfigParser::setMode(int configId, unsigned mode, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->setMode(mode);
	return 0;
}

int SmsConfigParser::setMainType(int configId, int type, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->setFuncType(type);
	return 0;
}

int SmsConfigParser::addUsageFilter(int configId, int funcType, const QByteArray &attrName, bool reverse, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->addUsageFilter(funcType, attrName, reverse);
	return 0;
}


int SmsConfigParser::setScopeType(int configId, int type, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->setScopeType(type);
	return 0;
}

int SmsConfigParser::addScopeItem(int configId, const char *pName, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->addScopeItem(pName);
	return 0;
}

int SmsConfigParser::setCritJoinType(int configId, int type, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->setCritJoinType(type);
	return 0;
}

int SmsConfigParser::addCriteria(int configId, int funcType, int type, int subType, bool reverse, float lLimit, float uLimit, int minDuration, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->addCriteria(funcType, type, subType, reverse, lLimit, uLimit, minDuration);
	return 0;
}

int SmsConfigParser::setMessage(int configId, const char *message, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->setMessage(message);
	return 0;
}

int SmsConfigParser::setFiltering(int configId, int type, int time, int groupType, int groupTime, int indivLimit, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->setFilterType(type);
	pConfig->setFilterTime(time);
	pConfig->setGroupType(groupType);
	pConfig->setGroupTime(groupTime);
	pConfig->setIndivLimit(indivLimit);
	return 0;
}

int SmsConfigParser::setGroupMessage(int configId, const char *message, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->setGroupMessage(message);
	return 0;
}

int SmsConfigParser::addRecipient(int configId, const char *recipient, QString &errMsg)
{
	errMsg = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	pConfig->addRecipient(recipient);
	return 0;
}

int SmsConfigParser::writeToFile(const char *fileName, QString &errMsg)
{
	char *filePath = FileParser::pathToSystem(fileName);
#ifdef Q_OS_WIN
	FILE *pFile = NULL;
	if(fopen_s(&pFile, filePath, "w"))
	{
		pFile = NULL;
	}
#else
	FILE *pFile = fopen(filePath, "w");
#endif
	if(!pFile)
	{
		errMsg = "Failed to open file for writing <";
		errMsg += filePath;
		errMsg += ">";
		free((void *)filePath);
		return -1;
	}
	free((void *)filePath);
	initMachineModes();
	foreach(SmsConfig *pConfig, list)
	{
		pConfig->writeToFile(pFile, machineModes, allModesMask);
		fprintf(pFile, "\n");
	}
	fclose(pFile);
	list.clear();
	return 0;
}

void SmsConfigParser::initMachineModes(void)
{
	if(machineModes.count() > 0)
	{
		return;
	}
	allModesMask = 0;
	ResourcePool &pool = ResourcePool::getInstance();
	for(int n = 0 ; n < 32 ; n++)
	{
		char buf[32];
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "MachineMode%d", n);
#else
		sprintf(buf, "MachineMode%d", n);
#endif
		QString mode;
		if(pool.getStringValue(buf, mode) != ResourcePool::OK)
		{
			mode = "";
		}
		if(!mode.isEmpty())
		{
			allModesMask |= (1<<n);
		}
		machineModes.append(mode);
	}
}



int SmsConfigParser::parseFile(const char *fileName, QStringList &errList)
{
	errList.clear();
	char *filePath = FileParser::pathToSystem(fileName);
	QFile file(filePath);
	if(!file.open(QIODevice::ReadOnly))
	{
		QString errMsg = "Filed to open file for reading <";
		errMsg += filePath;
		errMsg += ">";
		errList.append(errMsg);
		free((void *)filePath);
		return  -1;
	}
	free((void *)filePath);
	list.clear();
	QTextStream stream(&file);
	int lineNo = 0, coco = 0;
	while(!stream.atEnd())
	{
		lineNo++;
		QString line = stream.readLine();
		if(line.isEmpty())
		{
			continue;
		}
		if(DebugCtl::isSms())
		{
#ifdef Q_OS_WIN
			qDebug("SmsConfigParser::parseFile(): read line %d, length %d\n", lineNo, line.length());
#else
			printf("SmsConfigParser::parseFile(): read line %d, length %d\n", lineNo, line.length());
			fflush(stdout);
#endif
		}
		if(parseConfigLine(line, lineNo, errList) < 0)
		{
			coco = -1;
		}
	}
	file.close();
	return coco < 0 ? coco : list.count();
}

int SmsConfigParser::parseConfigLine(QString &line, int lineNo, QStringList &errList)
{
	QString errMsg;
	QStringList lineItems = line.split(";", QString::KeepEmptyParts);
	if(lineItems.count() < 2)
	{
		errMsg = lineNoError(lineNo);
		errMsg += "not a string separated by ';' characters";
		errList.append(errMsg);
		return -1;
	}
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): split %d items\n", lineNo, lineItems.count());
#else
		printf("SmsConfigParser::parseConfigLine(%d): split %d items\n", lineNo, lineItems.count());
		fflush(stdout);
#endif
	}

	// Parse items one by one
	QStringList::Iterator iter = lineItems.begin();

	SmsConfig *pConfig = new SmsConfig();
	list.append(pConfig);
	int coco = 0;
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): new config\n", lineNo);
#else
		printf("SmsConfigParser::parseConfigLine(%d): new config\n", lineNo);
		fflush(stdout);
#endif
	}

	// Visible name
	if(pConfig->parseVisibleName(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): visible name\n", lineNo);
#else
		printf("SmsConfigParser::parseConfigLine(%d): visible name\n", lineNo);
		fflush(stdout);
#endif
	}

	// Machine mode
	initMachineModes();
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): initMachineModes() done\n", lineNo);
#else
		printf("SmsConfigParser::parseConfigLine(%d): initMachineModes() done\n", lineNo);
		fflush(stdout);
#endif
	}
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Main eqp type", errList);
	}
	if(pConfig->parseMachineMode(*iter, lineNo, machineModes, errList) < 0)
	{
		coco = -1;
	}
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): machine mode\n", lineNo);
#else
		printf("SmsConfigParser::parseConfigLine(%d): machine mode\n", lineNo);
		fflush(stdout);
#endif
	}

	// Main functional type
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Main eqp type", errList);
	}
	if(pConfig->parseMainType(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): main type\n", lineNo);
#else
		printf("SmsConfigParser::parseConfigLine(%d): main type\n", lineNo);
		fflush(stdout);
#endif
	}

	// Usage filter content
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "UsageFilter Eqp type(s)", errList);
	}
	if(pConfig->parseUsageFilterTypes(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}

	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Usage filter attribute name(s)", errList);
	}
	if(pConfig->parseUsageFilterAttrNames(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}

	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Usage filter reverse flag(s)", errList);
	}
	if(pConfig->parseUsageFilterReverseFlags(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}


	// Scope
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Scope type", errList);
	}
	if(pConfig->parseScopeType(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Scope items", errList);
	}
	if(pConfig->parseScopeItems(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): scope\n", lineNo);
#else
		printf("SmsConfigParser::parseConfigLine(%d): scope\n", lineNo);
		fflush(stdout);
#endif
	}

	// Criteria join type
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Scope items", errList);
	}
	if(pConfig->parseCritJoinType(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): join type\n", lineNo);
#else
		printf("SmsConfigParser::parseConfigLine(%d): join type\n", lineNo);
		fflush(stdout);
#endif
	}

	// Criteria content
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Criteria Eqp type", errList);
	}
	if(pConfig->parseCritEqpTypes(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}

	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Criteria type", errList);
	}
	if(pConfig->parseCritTypes(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}

	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Criteria", errList);
	}
	if(pConfig->parseCritSubTypes(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}

	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Criteria reverse flags", errList);
	}
	if(pConfig->parseCritReverseFlags(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}

	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Criteria upper limit", errList);
	}
	if(pConfig->parseCritUpperLimits(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}

	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Criteria upper limit", errList);
	}
	if(pConfig->parseCritLowerLimits(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Criteria min. duration", errList);
	}
	if(pConfig->parseCritMinDurations(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): criteria\n", lineNo);
#else
		printf("SmsConfigParser::parseConfigLine(%d): criteria\n", lineNo);
		fflush(stdout);
#endif
	}

	// Message filtering
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Message filtering type", errList);
	}
	if(pConfig->parseFilteringType(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Message filtering time", errList);
	}
	if(pConfig->parseFilteringTime(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Message grouping type", errList);
	}
	if(pConfig->parseGroupType(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Message grouping time", errList);
	}
	if(pConfig->parseGroupTime(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Message grouping individual limit", errList);
	}
	if(pConfig->parseGroupIndivLimit(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): filtering\n", lineNo);
#else
		printf("SmsConfigParser::parseConfigLine(%d): filtering\n", lineNo);
		fflush(stdout);
#endif
	}

	// Messages
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Message text", errList);
	}
	if(pConfig->parseMessage(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Group message text", errList);
	}
	if(pConfig->parseGroupMessage(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): messages\n", lineNo);
#else
		printf("SmsConfigParser::parseConfigLine(%d): messages\n", lineNo);
		fflush(stdout);
#endif
	}

	// Recipients
	if(++iter == lineItems.end())
	{
		return errorNotEnoughItems(lineNo, "Recipients", errList);
	}
	if(pConfig->parseRecipients(*iter, lineNo, errList) < 0)
	{
		coco = -1;
	}
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfigParser::parseConfigLine(%d): recipients = FINISH\n", lineNo);
#else
		printf("SmsConfigParser::parseConfigLine(%d): recipients = FINISH\n", lineNo);
		fflush(stdout);
#endif
	}

	return coco;
}

int SmsConfigParser::firstParsedConfig(void)
{
	if (!list.isEmpty()) {
		SmsConfig *pConfig = list.first();
		return pConfig->getId();
	}
	return 0;
}

int SmsConfigParser::nextParsedConfig(int configId)
{
	for(int idx = 0 ; idx < list.count() ; idx++)
	{
		SmsConfig *pConfig = list.at(idx);
		if(pConfig->getId() == configId)
		{
			idx++;
			if(idx >= list.count())
			{
				return 0;
			}
			pConfig = list.at(idx);
			return pConfig ? pConfig->getId() : 0;
		}
	}
	return 0;
}


int SmsConfigParser::getVisibleName(int configId, QString &name, QString &errMsg)
{
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	name = pConfig->getVisibleName();
	return 0;
}

int SmsConfigParser::getMode(int configId, unsigned &mode, QString &errMsg)
{
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	mode = pConfig->getMode();
	return 0;
}

int SmsConfigParser::getMainType(int configId, int &type, QString &errMsg)
{
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	type = pConfig->getFuncType();
	return 0;
}

int SmsConfigParser::getUsageFilter(int configId, QList<int> &funcTypes, QList<QByteArray> &attrNames,
				QList<bool> &reverseFlags, QString &errMsg)
{
	funcTypes.clear();
	attrNames.clear();
	reverseFlags.clear();
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	const QList<EqpUsageFilterItem *> &filterList = pConfig->getUsageFilterList();
	foreach(EqpUsageFilterItem *pItem, filterList)
	{
		funcTypes.append(pItem->getTypeId());
		attrNames.append(pItem->getAttrName());
		reverseFlags.append(pItem->isReverse());
	}
	return filterList.count();
}


int SmsConfigParser::getScopeType(int configId, int &type, QString &errMsg)
{
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	type = pConfig->getScopeType();
	return 0;
}

int SmsConfigParser::getScopeItems(int configId, QStringList &items, QString &errMsg)
{
	items.clear();
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	items = pConfig->getScopeItems();
	return 0;
}

int SmsConfigParser::getCritJoinType(int configId, int &type, QString &errMsg)
{
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	type = pConfig->getCritJoinType();
	return 0;
}

int SmsConfigParser::getCriteria(int configId, QList<int> &funcTypes, QList<int> &types,
				QList<int> &subTypes, QList<bool> &reverseFlags,
				QList<float> &lLimits, QList<float> &uLimits, QList<int> &minDurations,
				QString &errMsg)
{
	funcTypes.clear();
	types.clear();
	subTypes.clear();
	reverseFlags.clear();
	lLimits.clear();
	uLimits.clear();
	minDurations.clear();
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	const QList<EqpMsgCriteria *> &critList = pConfig->getCriteria();
	foreach(EqpMsgCriteria *pCrit, critList)
	{
		funcTypes.append(pCrit->getFunctionalType());
		types.append(pCrit->getType());
		subTypes.append(pCrit->getSubType());
		reverseFlags.append(pCrit->isReverse());
		lLimits.append(pCrit->getLowerLimit());
		uLimits.append(pCrit->getUpperLimit());
		minDurations.append(pCrit->getMinDuration());
	}
	return critList.count();
}

int SmsConfigParser::getMessage(int configId, QString &message, QString &errMsg)
{
	message = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	message = pConfig->getMessage();
	return 0;
}

int SmsConfigParser::getFiltering(int configId, int &type, int &time, int &groupType, int &groupTime, int &indivLimit, QString &errMsg)
{
	type = 0;
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	type = pConfig->getFilterType();
	time = pConfig->getFilterTime();
	groupType = pConfig->getGroupType();
	groupTime = pConfig->getGroupTime();
	indivLimit = pConfig->getIndivLimit();
	return 0;
}

int SmsConfigParser::getGroupMessage(int configId, QString &message, QString &errMsg)
{
	message = "";
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	message = pConfig->getGroupMessage();
	return 0;
}

int SmsConfigParser::getRecipients(int configId, QStringList &recipients, QString &errMsg)
{
	recipients.clear();
	SmsConfig *pConfig = findConfig(configId, errMsg);
	if(!pConfig)
	{
		return -1;
	}
	recipients = pConfig->getRecipients();
	return 0;
}


SmsConfig *SmsConfigParser::findConfig(int id, QString &errMsg)
{
	for(int idx = list.count() - 1 ; idx >= 0 ; idx--)
	{
		SmsConfig *pConfig = list.at(idx);
		if(pConfig->getId() == id)
		{
			return pConfig;
		}
	}
	errMsg = "SMS config with ID ";
	errMsg += QString::number(id);
	errMsg += " not found";
	return NULL;
}
			
QString SmsConfigParser::lineNoError(int lineNo)
{
	QString errMsg("Line ");
	errMsg += QString::number(lineNo);
	errMsg += ": ";
	return QString(errMsg);
}

int SmsConfigParser::errorNotEnoughItems(int lineNo, const char *msg, QStringList &errList)
{
	QString errMsg(lineNoError(lineNo));
	errMsg += "line is too short, no line item <";
	errMsg += msg;
	errMsg += ">";
	errList.append(errMsg);
	return -1;
}

