#!/bin/bash 
echo "---------------------------------------------------------------------"
echo "---------------Copy Linux Libraries to PVSS project (path=\$1=$1)"
echo "---mkdir"
mkdir -p $1/bin/widgets/linux-rhel-x86_64/
echo "---cp"
cp -v -f  ../bin/*.so $1/bin/
cp -v -f  ../bin/*.a $1/bin/
cp -v -f  ../bin/*.debug $1/bin/
cp -v -f  ../bin/widgets/linux-rhel-x86_64/*.debug $1/bin/widgets/linux-rhel-x86_64/
cp -v -f  ../bin/widgets/linux-rhel-x86_64/*.so $1/bin/widgets/linux-rhel-x86_64/
cd $1/bin/widgets/linux-rhel-x86_64/
echo "---------------------------------------------------------------------"
pwd
echo "---------------------------------------------------------------------"
echo "---------------Rename *.so to *.ewo"
echo "---------------------------------------------------------------------"
for f in *.so; do 
mv -v -- "$f" "${f%.so}.ewo"
done
exit
