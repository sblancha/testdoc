//	Implementation of MainSpsImageEqpConnect class
////////////////////////////////////////////////////////////////////////////////

#include "MainSpsImageEqpConnect.h"

#include "DataPool.h"
#include "Eqp.h"

#include <qdatetime.h>

MainSpsImageEqpConnect::~MainSpsImageEqpConnect()
{
	if(!connected)
	{
		return;
	}
	DataPool	&pool = DataPool::getInstance();
	QHashIterator<QByteArray, Eqp *> iter(pool.getEqpDict());
	while(iter.hasNext())
	{
		iter.next();
		Eqp *pEqp = iter.value();
		if(pEqp->isSpecColor())
		{
			pEqp->disconnect(this, DataEnum::Online);
		}
	}
}

/*
**	FUNCTION
**		Connect to all equipment that can affect colors of main SPS view
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSpsImageEqpConnect::connect(void)
{
	if(connected)
	{
		return;
	}
	// Check all equipment in data pool. Every device affecting main view
	// appearance must implement correctly method isSpecColor()
	DataPool	&pool = DataPool::getInstance();
	QHashIterator<QByteArray, Eqp *> iter(pool.getEqpDict());
	while(iter.hasNext())
	{
		iter.next();
		Eqp *pEqp = iter.value();
		if(pEqp->isSpecColor())
		{
			pEqp->connect(this, DataEnum::Online);
			connected = true;
		}
	}
}

/*
**	FUNCTION
**		Slot receiving notifications from equipment, see InterfaceEqp class
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSpsImageEqpConnect::dpeChange(Eqp * /* pSrc */, const char * /* dpeName */,
	DataEnum::Source /* source */, const QVariant & /* value */, DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	// Only online mode calls are of interest for main view
	if(mode != DataEnum::Replay)
	{
		emit stateChanged();
	}
}
