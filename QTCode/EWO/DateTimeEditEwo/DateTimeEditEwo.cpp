//	Implementation of DateTimeEditEwo class
/////////////////////////////////////////////////////////////////////////////////

#include "DateTimeEditEwo.h"

EWO_PLUGIN(DateTimeEditEwo)

DateTimeEditEwo::DateTimeEditEwo(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new QDateTimeEdit(QDate::currentDate(), parent);
	QFont fnt("Helvetica", 10);
	baseWidget->setFont(fnt);
	baseWidget->setDisplayFormat("dd-MM-yyyy hh:mm:ss");
	connect(baseWidget, SIGNAL(dateTimeChanged(const QDateTime &)),
		this, SLOT(dateTimeChange(const QDateTime &)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList DateTimeEditEwo::signalList(void) const
{
	QStringList list;
	list.append("valueChanged(time value)");
	return list;
}

/*
**	FUNCTION
**		Slot receiving valveChanged signal of QDateTimeEdit, emit
**		signal 'valueChanged' to PVSS
**
**	ARGUMENTS
**		value	- New date + time value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DateTimeEditEwo::dateTimeChange(const QDateTime &value)
{
	emit signal("valueChanged", QVariant(value));
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList DateTimeEditEwo::methodList(void) const
{
	QStringList list;
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant DateTimeEditEwo::invokeMethod(const QString & /* name */,
	QList<QVariant> & /* values */, QString &error)
{
	error = "No methods in this ewo";
	return QVariant();
}
