
// VTL #1753. Process scheduled TPG300 actions.

#uses "VacCtlEqpDataPvss"			// Load DLL or so
#uses "vclDevCommon.ctl"             // Load CTRL library

#uses "panel.ctl"      // for access control

// Calculated flag - beam presence in machine, set by callback
// For the moment realized for LHC only
private bool isBeamMode;

// Script name used in debugging messages
private const string MY_NAME = "vcsHotStartTPG300";

// Maximum number of attempts per gauge (in case action failed)
private const int MAX_ATTEMPTS = 1;

// Enable/disable debugging messages
private bool debug = true;

main()
{
  DebugTN(MY_NAME + ": starting");
  if(!InitStaticData())
  {
    return;
  }
  string machineModeDp;
  LhcVacGetMachineModeDp(machineModeDp);
  if(machineModeDp != "")
  {
    dpConnect("MachineModeCb",
              machineModeDp + ".Beam_B.Beam",
              machineModeDp + ".Beam_R.Beam");
  }

  // Infinite loop - wait for job to do, process the job
  while(true)
  {
    ProcessScheduledJob();
    delay(60);
  }
}

void ProcessScheduledJob()
{
  if(debug)
  {
    DebugTN(MY_NAME + ": ProcessScheduledJob() beam mode = " + isBeamMode);
  }
  if(isBeamMode)
  {
    return;
  }
  dyn_string allVgList = GetAllGauges();
  if(debug)
  {
    DebugTN(MY_NAME + ": start processing " + dynlen(allVgList) + " VGs");
  }
  time farInThePast = makeTime(2000, 1, 1);
  while(dynlen(allVgList) > 0)
  {
    if(isBeamMode)
    {
      break;
    }
    string dpName = allVgList[1];
    unsigned action;
    time plannedTime;
    unsigned userId, attemptCount;
    dpGet(dpName + ".TpgHotStart.PlannedAction", action,
          dpName + ".TpgHotStart.PlannedTime:_online.._user", userId,
          dpName + ".TpgHotStart.PlannedTime", plannedTime,
          dpName + ".TpgHotStart.AttemptCount", attemptCount);
    bool needProcessing = false;
    time now = getCurrentTime();
    if((plannedTime > farInThePast) && (plannedTime <= now))
    {
      needProcessing = (action == LVA_TPG300_HOT_START_DISABLE)
                       || (action == LVA_TPG300_HOT_START_ENABLE);
    }
    if(attemptCount >= MAX_ATTEMPTS)
    {
      needProcessing = false;
    }
    if(needProcessing)
    {
      ProcessVgAction(dpName, action, getUserName(userId), allVgList);
    }
    dynRemove(allVgList, 1);
  }
  if(debug)
  {
    DebugTN(MY_NAME + ": finish processing, " + dynlen(allVgList) + " VG(s) remain");
  }
}

void ProcessVgAction(string dpName, unsigned action, string userName, dyn_string &allVgList)
{
  string actName;
  if(action == LVA_TPG300_HOT_START_DISABLE)
  {
    actName = "Disable TPG300 Host Start";
  }
  else if(action == LVA_TPG300_HOT_START_ENABLE)
  {
    actName = "Enable TPG300 Host Start";
  }
  else
  {
    sprintf(actName, "Unknown %d", action);
    DebugTN(MY_NAME + ": " + actName + " for DP " + dpName);
    return;
  }
  if(debug)
  {
    DebugTN(MY_NAME + ": process action " + actName + " for DP " + dpName + " planned by " + userName);
  }

  // Write message to log
  dyn_string exceptionInfo;
  string devName = dpName;
  LhcVacDisplayName(dpName, devName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    dynClear(exceptionInfo);
    devName = dpName;
  }
  string systemName = getSystemName();
  string message = MY_NAME + ": Process " + actName + " for " + devName + ", planned by " + userName;

  unMessageText_send(systemName, "1", "CmdHandler", "user", "*", "INFO", message, exceptionInfo);

  // Use special function for execution - bypass privilege check forthis action
  string dpType;
  LhcVacExecuteAction(dpName, dpType, makeDynAnytype(action), exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN(MY_NAME + " action " + actName + " for DP " + dpName + " failed", exceptionInfo);
    message = MY_NAME + ": action " + actName + " for " + devName + " failed: ";
    for(int n = 0 ; n < dynlen(exceptionInfo) ; n+= 3)
    {
      message += "(" + exceptionInfo[n*3 + 1] + ": " + exceptionInfo[n*3 + 2];
    }
    unMessageText_send(systemName, "1", "CmdHandler", "user", "*", "INFO", message, exceptionInfo);
    // Increment attempt count
    unsigned attemptCount;
    dpGet(dpName + ".TpgHotStart.AttemptCount", attemptCount);
    attemptCount++;
    dpSet(dpName + ".TpgHotStart.AttemptCount", attemptCount);
    return;
  }

  // Done successfully - clean planned action for all slaves of this TPG300
  string masterDp;
  int masterChannel;
  LhcVacEqpMaster(dpName, masterDp, masterChannel);
  dyn_string slaveDpNames;
  if(masterDp == "")
  {
    dynAppend(slaveDpNames, dpName);
  }
  else
  {
    LhcVacEqpSlaves(masterDp, slaveDpNames);
  }
  time emptyTime;
  dyn_string dpes;
  dyn_anytype values;
  for(int n = dynlen(slaveDpNames) ; n > 0 ; n--)
  {
    dynAppend(dpes, slaveDpNames[n] + ".TpgHotStart.PlannedAction");
    dynAppend(values, (unsigned)0);
    dynAppend(dpes, slaveDpNames[n] + ".TpgHotStart.PlannedTime");
    dynAppend(values, emptyTime);
  }
  dpSet(dpes, values);
}

dyn_string GetAllGauges()
{
  dyn_string result;
  dyn_string dpList = dpNames("*", "VGP_T");
  for(int n = dynlen(dpList) ; n > 0 ; n--)
  {
    string dpName = dpSubStr(dpList[n], DPSUB_DP);
    dynAppend(result, dpName);
  }
  dpList = dpNames("*", "VGR_T");
  for(int n = dynlen(dpList) ; n > 0 ; n--)
  {
    string dpName = dpSubStr(dpList[n], DPSUB_DP);
    dynAppend(result, dpName);
  }
  return result;
}

void MachineModeCb(string dpe1, int modeB, string dpe2, int modeR)
{
  bool result = (modeB != 0) || (modeR != 0);
  if(result != isBeamMode)
  {
    isBeamMode = result;
    if(debug)
    {
      DebugTN(MY_NAME + ": beam mode became " + isBeamMode +
             " = B(" + modeB + ") + R(" + modeR + ")");
    }
  }
}

bool InitStaticData()
{
  dyn_string	exceptionInfo;

  // Set accelerator name to be used by other components
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN(MY_NAME + ": Failed to read machine name: ", exceptionInfo);
    return false;
  }
  bool isLhcData = glAccelerator == "LHC" ? true : false;
  // Initalize passive machine data
  int coco = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
  if(coco < 0)
  {
    DebugTN(MY_NAME + ": LhcVacEqpInitPassive() failed: ", exceptionInfo);
    return false;
  }
  DebugTN(MY_NAME + ": LhcVacEqpInitPassive() done");

  // Initialzie active equipment information
  coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
    exceptionInfo);
  if(coco < 0)
  {
    DebugTN(MY_NAME + ": LhcVacEqpInitActive() failed: ", exceptionInfo);
    return false;
  }
  DebugTN(MY_NAME + ": LhcVacEqpInitActive() done");
  LhcVacInitDevFunc();
  return true;	
}
