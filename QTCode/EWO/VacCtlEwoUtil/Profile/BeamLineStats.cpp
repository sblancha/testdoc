//	Implementation of BeamLineStats class
/////////////////////////////////////////////////////////////////////////////////

#include "BeamLineStats.h"

#include "DataPool.h"
#include "BeamLine.h"
#include "Eqp.h"
#include "EqpMsgCriteria.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

BeamLineStats::BeamLineStats()
{
	QList<BeamLine *> &beamLines = DataPool::getInstance().getLines();
	for(int idx = 0 ; idx < beamLines.count() ; idx++)
	{
		BeamLine *pLine = beamLines.at(idx);
		lines.append(new BeamLineWeight(pLine));
	}
}

BeamLineStats::~BeamLineStats()
{
	while(!lines.isEmpty())
	{
		delete lines.takeFirst();
	}
}

void BeamLineStats::increment(BeamLine *pBeamLine)
{
	for(int idx = lines.count() - 1 ; idx >= 0 ; idx--)
	{
		BeamLineWeight *pLine = lines.at(idx);
		if(pLine->getLine() == pBeamLine)
		{
			pLine->increment();
			break;
		}
	}
}

BeamLine *BeamLineStats::getBestLine(void)
{
	BeamLine *pBestLine = NULL;
	int	maxWeight = 0;
	for(int idx = lines.count() - 1 ; idx >= 0 ; idx--)
	{
		BeamLineWeight *pLine = lines.at(idx);
		if(pLine->getWeight() > maxWeight)
		{
			pBestLine = pLine->getLine();
			maxWeight = pLine->getWeight();
		}
	}
	return pBestLine;
}

