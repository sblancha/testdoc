#ifndef INTERFACEEQP_H
#define INTERFACEEQP_H

#include "VacCtlUtilExport.h"


// Auxilliary class holding connection information for single DPE

#include <DpConnection.h>
#include <DpeConnection.h>

// Superclass defining common signals and slots for both equipment
//	and equipment change listeneres (because equipment also can act as
//	equipment state listener)

#include <QObject>

#include <QVariant>

class Eqp;

class VACCTLUTIL_EXPORT InterfaceEqp : public QObject
{
	Q_OBJECT

signals:
	// Notify listeners that value of particular DPE has changed
	//	Arguments
	//		pSrc	- Pointer to object - source of signal (normally - this)
	//		dpeName	- DPE name
	//		source	- DPE change source (see enum above)
	//		value	- New value for DPE
	//		mode	- Data acquisition mode
	//		timeStamp	- Timsestamp of value
	void dpeChanged(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode,
		const QDateTime &timeStamp);

	// Notify listeners that mobile state of of this object (device)  has changed
	//	Arguments:
	//		pSrc	- Pointer to object - source of signal (normally - this)
	//		mode	- Data acquisition mode
	void mobileStateChanged(Eqp *pSrc, DataEnum::DataMode mode);

	// Notify listeners that selection state of this object (device) has changed
	//	Arguments:
	//		pSrc		- Pointer to object - source of signal (normally - this)
	void selectChanged(Eqp *pSrc);

public slots:
	// Slot to be signalled by dpeChanged() signal
	//	Arguments
	//		pSrc	- Pointer to object - source of signal
	//		dpeName	- DPE name
	//		source	- DPE change source (see enum above)
	//		value	- New value for DPE
	//		mode	- Data acquisition mode
	//
	virtual void dpeChange(Eqp * /* pSrc */, const char * /* dpeName */,
		DataEnum::Source /* source */, const QVariant & /* value */,
		DataEnum::DataMode /* mode */,
		const QDateTime & /* timeStamp */) {}

	// Slot to be signalled by mobileStateChanged() signal
	//	Arguments:
	//		pSrc	- Pointer to object - source of signal
	//		mode	- Data acquisition mode
	//
	virtual void mobileStateChange(Eqp * /* pSrc */, DataEnum::DataMode /* mode */) {}

	// Slot to be signalled by selectChanged() signal
	//	Arguments
	//		pSrc		- Pointer to object - source of signal
	virtual void selectChange(Eqp *pSrc);

public:
	InterfaceEqp();
	virtual ~InterfaceEqp() {}

	// Connect another object to important signals of this object. Which signals
	// are important is decided in subclass of this class. 
	//	Arguments:
	//		pDst	- Pointer to object that shall be notified in case of changes
	//		mode	- Data acquisition mode
	//
	virtual void connect(InterfaceEqp * /* pDst */, DataEnum::DataMode /* mode */) {}

	// Connect another object to given DPE of this object. Method is used when
	// object requesting connection knows exactly what DPE is needed, even if
	// this object can not know what to do with that DPE
	//	Arguments:
	//		pDst	- Pointer to object that shall be notified in case of changes
	//		dpeName	- Name of DPE which shall be connected
	//		mode	- Data acquisition mode
	//
	virtual void connectDpe(InterfaceEqp * /* pDst */, const char * /* dpeName */,
		DataEnum::DataMode /* mode */) {}
 
	// Disconnect another object from important signals of this object. Which signals
	// are important is decided in subclass of this class - these must be the same
	// signals which are connected in connect() method
	//	Arguments:
	//		pDst	- Pointer to object that requested disconnect
	//		mode	- Data acquisition mode
	//
	virtual void disconnect(InterfaceEqp * /* pDst */, DataEnum::DataMode /* mode */) {}

	// Disconnect another object from given DPE of this object. Method is used when
	// object requesting connection knows exactly what DPE is needed, even if
	// this object can not know what to do with that DPE
	//	Arguments:
	//		pDst		- Pointer to object that requested disconnect
	//		dpeName		- Name of DPE which shall be disconnected
	//		mode	- Data acquisition mode
	//
	virtual void disconnectDpe(InterfaceEqp * /* pDst */, const char * /* dpeName */,
		DataEnum::DataMode /* mode */) {}
};

#endif	// INTERFACEEQP_H
