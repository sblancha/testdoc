#uses "VacCtlEqpDataPvss"	// Load DLL
#uses "vclDevCommon.ctl"	// Load CTRL library
#uses "vclMachine.ctl"		// Load CTRL library
#uses "vclSectors.ctl"	// Load CTRL library
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "vclDIPConfig.ctl"	// Load CTRL library

//#uses "lhcVacEqpConfig.ctl"
//#uses "lhcVacSectors.ctl"
//#uses "VacEqpData"

/**@name SCRIPT: lhcVacGDPMSupport.ctl

@author: Leonid Kopylov (IHEP, Protvino, Russia)

Creation Date: 15/10/2005

Modification History: 
	14/06/2006: Kopylov
		- Add processing of QRL/CRYO main parts. Output is maximum pressure
			over all gauges in main part. Gauges on pumping groups are NOT used.
	25/07/2007: Kopylov
		- QLR/CRYO main parts: do not use VGM pressure if either VGR or VGP pressure
			are valid. If both VGR and VGP pressures are valid - use VGR pressure
	11.10.2007: Kopylov
		-	Publish integrated state of valves: open, closed, other, not connected
	22.02.2008: Kopylov
		- New function UpdateSectorPressure() - update DPs, containing 'integrated'
			pressures in isolation vacuum sectors to be published via CMW
	22.04.2008: Kopylov
		- Change criteria for gauge selection: VGR underrange is only used if there
			is no valid VGP in the same sector

version 1.5

Purpose: 
Subscribe to devices forming 'intergated' state of machine's main
part, form resulting 'interrated' state for main parts based on
state of single devices and write resulting 'intergated' state to
dedicated DPE. The values of these resulting DPEs shall be published
using DIP.
The script is also responsible for creating/deleting summary DPs and
for publishing their values using DIP.

Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. global variable: the following variables are global to the script
		. valves: List of all valves in main parts together with valve's state: dyn_dyn_anytype.
			Used for processing beam vacuum main parts. Two values are used for every valve:
				valves[mpIdx][2*n] - name of nth valve in main part with index mpIdx - string
				valves[mpIdx][2*n+1] - state (RR1) of nth valve in main part with index mpIdx - unsigned
		. vpcis: list of all sector names in main part together with VPCI summary state in sector: dyn_anytype
			Used for processing beam vacuum main parts. Two values are used for every sector:
				vpcis[2*n] - name of nth sector in system - string
				vpcis[2*n+1] - summary state of VPCIs in nth sector - unsigned
		. gauges: list of all gauges in main parts together with gauge's state and pressure: dyn_dyn_anytype.
			Used for processing isolation vacuum main parts. Five values are used for every gauge:
				gauges[mpIdx][5*n] - name of nth gauge in main part with index mpIdx - string
				gauges[mpIdx][5*n+1] - state (RR1) of nth gauge in main part with index mpIdx - unsigned
				gauges[mpIdx][5*n+2] - pressure (PR) of nth gauge in main part with index mpIdx - float
				gauges[mpIdx][5*n+3] - type (enum) of nth gauge in main part with index mpIdx - int
				gauges[mpIdx][5*n+4] - flag indicating if new value for gauge arrived - bool
		. lastUpdates: list of timestamps when resulting DPE for every main part was updated: dyn_time.
		. isInitialized: flag indicating if intialization is over: boolean
	. constant:
		. DEFAULT_DIP_CONFIG_DP: DIP config DP - fixed. only one DIP manager is used
		. STATE_SUMMARY_DP_TYPE: DP type for main part summary of type 'state', used for beam vacuum
		. PRESSURE_SUMMARY_DP_TYPE: DP type for main part summary of type 'pressure', used for isolation vacuum
		. MIN_UPDATE_PERIOD: minimum time between two updates of resulting DPE [sec]: float
	. data point type needed: _VacMpSummary, _VacMpSummaryPR
	. data point:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/

// Name of DIP config DP to use
const string DEFAULT_DIP_CONFIG_DP = "DIPConfig_12";

// Name of DP type for summary DP with INTERGER output
const string STATE_SUMMARY_DP_TYPE = "_VacMpSummary";

// Name of DP type for summary DP with FLOAT output
const string PRESSURE_SUMMARY_DP_TYPE = "_VacMpSummaryPR";

// Name of DP type for valve state to be published
const string VALVE_STATE_DP_TYPE = "_VacValveState";

// Name of DP type containing summary pressure for isolation vacuum sectors
const string ISOL_PR_EXPORT_DP_TYPE = "VG_IsolVacExport";

// Enumeration for gauge types
const int GAUGE_TYPE_VGP = 1;
const int GAUGE_TYPE_VGR = 2;
const int GAUGE_TYPE_VGM = 3;

// Names of DPs of summary DP type are formed by adding "_" and PVSS main part
// name to name of DP type, thus DP for TS1 will be called _VacMpSummary_TS1

// List of valves with their state. Every valve is represented by two
// members in dyn_anytype:
// 1) (string) DP name
// 2) (unsigned) RR1
dyn_dyn_anytype	valves;

// List of VPCI summary states in sector. Every sector is represented by
// two members in dyn_anytype:
// 1) (string) sector name
// 2) (unsigned) combination of RR1 of all ion pumps (taken from VSECT_VPI_SUM_xx.RRC)
dyn_anytype vpcis;

// List of gauge pressures. Every gauges is represented by five members in dyn_anytype:
// 1) (string) DP name
// 2) (unsigned) gauge state (usually - RR1)
// 3) (float) pressure. Switched OFF gauges are represented with value 10000.0
// 4) (unsigned) DP type mask
// 5) (bool) true if pending changes for gauge exist
dyn_dyn_anytype	gauges;

// Timestamp when value of output DP for given main part has been updated
// This is used to prevent too often updates of output values
dyn_time				lastUpdates;

// Flag indicating if changes for main part are not processed
dyn_bool				pendingChanges;

// Minimum result value update period
const float MIN_UPDATE_PERIOD = 10.0;

// Flag indicating if initialization has been finished. There is no reasons
// to make full-scale processing for long number of callbacks coming during
// initialization.
bool isInitialized;
//Like globals but used onli in this script

dyn_string glVacMainParts;
dyn_dyn_string glVacSectors, glSectDomains;


// main
/**
Purpose:
This is the main of the script.

Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. no temporary data point, no ActiveX
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
main()
{
  int	n;

  isInitialized = false;

  // Initialize machine equipment data
  if(!InitEquipmentData())
  {
    return;
  }

  // Initalize 'main' equipment data
  if(!InitMainEqpData())
  {
    return;
  }
  DebugTN("lhcVacGDPMSupport: InitMainEqpData() done");

  // Initialize result DPs
  if(!InitResultDpStates())
  {
    return;
  }
  DebugTN("lhcVacGDPMSupport: InitResultDpStates() done");

  if(!InitResultDpPressures())
  {
    return;
  }
  DebugTN("lhcVacGDPMSupport: InitResultDpPressures() done");

  if(!InitResultValveStates())
  {
    return;
  }
  DebugTN("lhcVacGDPMSupport: InitResultValveStates() done");

  // Connect to DPs
  ConnectToDps();
  DebugTN("lhcVacGDPMSupport: ConnectToDps() done");

  // Set initial values for all result DPEs
  isInitialized = true;
  for(n = dynlen(glVacMainParts) ; n > 0 ; n--)
  {
    ProcessMpState(n);
  }

  DebugTN("lhcVacGDPMSupport: Initialization completed");

  // Infinite loop - check if MP data shall be updated
  // Update if required - to make sure result DPEs are updated
  // even if no input updates are coming
  while(true)
  {
    delay(MIN_UPDATE_PERIOD);
    for(n = dynlen(pendingChanges) ; n > 0 ; n--)
    {
      if(pendingChanges[n])
      {
        ProcessMpState(n);
      }
    }
  }
}
// InitEquipmentData
/**
Purpose:
Initialize static machine data:
	. beam lines configuration and survey data
	. vacuum sectors and main parts
	. controllable equipment

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
bool InitEquipmentData()
{
  dyn_string	exceptionInfo;

  // Set accelerator name to be used by other components
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("lhcVacGDPMSupport: Failed to read machine name: " + exceptionInfo);
    return false;
  }

  bool isLhcData = glAccelerator == "LHC" ? true : false;
  // Initalize passive machine data
  int coco = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
  if(coco < 0)
  {
    DebugTN("lhcVacGDPMSupport: LhcVacEqpInitPassive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("lhcVacGDPMSupport: LhcVacEqpInitPassive() OK: " + coco);

  LhcVacInitDevFunc();

  // Initialzie active equipment information
  coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
    exceptionInfo);
  if(coco < 0)
  {
    DebugTN("lhcVacGDPMSupport: LhcVacEqpInitActive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("lhcVacGDPMSupport: LhcVacEqpInitActive() OK: " + coco);

  // Get sectors and main parts information
  if(LhcVacGetAllSectors(glVacMainParts, glVacSectors, glSectDomains, exceptionInfo) < 0)
  {
    DebugTN("lhcVacGDPMSupport: LhcVacGetAllSectors() failed: " + exceptionInfo);
    return false;
  }
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("lhcVacGDPMSupport: LhcVacGetAllSectors() exceptionInfo:", exceptionInfo);
  }
  DebugTN("lhcVacGDPMSupport: Init: " + dynlen(glVacMainParts) + " main parts");
  return true;
}
// InitMainEqpData
/**
Purpose:
Build lists of equipment used to produce summary results

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
bool InitMainEqpData()
{
  for(int n = dynlen(glVacMainParts) ; n > 0 ; n--)
  {
    dynAppend(lastUpdates, makeTime(1980, 01, 01));	// Far in the past
    dynAppend(pendingChanges, false);

    // DebugTN(n, glVacMainParts[n]);
    int vacType;
    LhcVacGetVacTypeOfMainPart(glVacMainParts[n], vacType);

    // There are two major types of main parts to deal with:
    // 1) Any beam vacuum	(main devices are valves)
    // 2) QRL/CRYO vacuum (main devices are gauges)
    if((vacType & (LVA_VACUUM_QRL | LVA_VACUUM_CRYO)) != 0)	// QRL/CRYO
    {
      AddEqpOfIsolationSector(n, glVacMainParts[n]);
    }
    else	// Beam
    {
      AddEqpOfBeamSector(n, glVacMainParts[n]);
    }
  }
  // DebugTN("Valves:", valves);
  // DebugTN("VPCIs:", vpcis);
  // DebugTN("gauges", gauges);
  return true;
}

void AddEqpOfIsolationSector(int mpIdx, string mpName)
{
// DebugTN("" + mpIdx + ": isolation " + mpName);
  dyn_string eqpList, exceptionInfo;
  dyn_anytype mainEqpList;
 
  // Build list of gauges
  LhcVacGetDevicesAtMainParts(makeDynString(mpName), eqpList, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("lhcVacGDPMSupport.AddEqpOfIsolationSector(): LhcVacGetDevicesAtMainParts(" + mpName +
      ") failed: " + exceptionInfo);
    return;
  }
  for(int i = dynlen(eqpList) ; i > 0 ; i--)
  {
    string dpType;
    LhcVacDpType(eqpList[i], dpType, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("lhcVacGDPMSupport.AddEqpOfIsolationSector(): LhcVacDpType(" + eqpList[i] +
        ") failed: " + exceptionInfo);
      return;
    }
    int dpTypeGroup = LhcVacDpTypeGroup(dpType);
    switch(dpTypeGroup)
    {
    case VAC_DP_TYPE_GROUP_VGM:
    case VAC_DP_TYPE_GROUP_VGR:
    case VAC_DP_TYPE_GROUP_VGP:
    case VAC_DP_TYPE_GROUP_VGI:
    case VAC_DP_TYPE_GROUP_VGF:
      {
        // Ignore gauges which are part of pumping group
        string ctlParent;
        LhcVacGetAttribute(eqpList[i], "CtlParent", ctlParent);
        if(ctlParent == "")
        {
          string eqpName = eqpList[i];
          dynAppend(mainEqpList, (string)eqpName);
          dynAppend(mainEqpList, (unsigned)0);
          dynAppend(mainEqpList, (float)10000.0);
          if(dpTypeGroup == VAC_DP_TYPE_GROUP_VGM)
          {
            dynAppend(mainEqpList, (int)GAUGE_TYPE_VGM);
          }
          else if(dpTypeGroup == VAC_DP_TYPE_GROUP_VGR)
          {
            dynAppend(mainEqpList, (int)GAUGE_TYPE_VGR);
          }
          else
          {
            dynAppend(mainEqpList, (int)GAUGE_TYPE_VGP);
          }
          dynAppend(mainEqpList, (bool)false);
        }
      }
      break;
    }
  }
  gauges[mpIdx] = mainEqpList;
}

void AddEqpOfBeamSector(int mpIdx, string mpName)
{
// DebugTN("" + mpIdx + ": beam " + mpName);

  dyn_string eqpList, exceptionInfo;
  dyn_anytype mainEqpList;

  // Build list of valves
  LhcVacGetDevicesAtMainParts(makeDynString(mpName), eqpList, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("lhcVacGDPMSupport.AddEqpOfBeamSector(): LhcVacGetDevicesAtMainParts(" + mpName +
      ") failed: " + exceptionInfo);
    return;
  }
  for(int i = dynlen(eqpList) ; i > 0 ; i--)
  {
    string dpType;
    LhcVacDpType(eqpList[i], dpType, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("lhcVacGDPMSupport.AddEqpOfBeamSector(): LhcVacDpType(" + eqpList[i] +
        ") failed: " + exceptionInfo);
      return;
    }
    int dpTypeGroup = LhcVacDpTypeGroup(dpType);
    // DebugTN(eqpList[i], dpType, dpTypeGroup);
    if(dpTypeGroup == VAC_DP_TYPE_GROUP_VV)
    {
      string eqpName = eqpList[i];
      dynAppend(mainEqpList, (string)eqpName);
      dynAppend(mainEqpList, (unsigned)0);
    }
  }
  // Build list of VPCI summaries per sectors
  for(int i = dynlen(glVacSectors[mpIdx]) ; i > 0 ; i--)
  {
    dynClear(exceptionInfo);
    string eqpName;
    LhcVacSectSummaryDpName(glVacSectors[mpIdx][i], eqpName, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      continue;
    }
    if(eqpName == "")
    {
      continue;
    }
    if(! dpExists(eqpName))
    {
      continue;
    }
    // Skip sectors without VPCI
    int nVpci;
    dpGet(eqpName + ".nVRPI:_original.._value", nVpci);
    if(nVpci < 1)
    {
      continue;
    }
    dynAppend(vpcis, (string)eqpName);
    dynAppend(vpcis, (unsigned)0);
  }
  valves[mpIdx] = mainEqpList;
}

// InitResultDpStates
/**
Purpose:
Create and publish via DIP resulting DPs for beam vacuum main parts

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
bool InitResultDpStates()
{
  int						n, i, dpIdx;
  dyn_string		exceptionInfo, existingDpNames, newDpNames, mpOfNewDps;
  string				mpName, dpName, itemName, dpeName, configDp;
  dyn_errClass	errList;
  bool					isConfigured;

  // Get names of all existing DPs of required type
  existingDpNames = dpNames("*", STATE_SUMMARY_DP_TYPE);
  for(n = dynlen(existingDpNames) ; n > 0 ; n--)
  {
    existingDpNames[n] = dpSubStr(existingDpNames[n], DPSUB_DP);
  }

  // Create new DPs if necessary
  for(n = dynlen(valves) ; n > 0 ; n--)
  {
    if(dynlen(valves[n]) == 0)
    {
      continue;	// Not valves MP
    }
    LhcVacPvssNameOfMainPart(glVacMainParts[n], mpName);
    dpName = STATE_SUMMARY_DP_TYPE + "_" + mpName;
    // DebugTN("dpName: ", dpName);
    // Check if DP with required name already exists
    if((dpIdx = dynContains(existingDpNames, dpName)) > 0)
    {
      // DebugTN("EXISTS");
      dynRemove(existingDpNames, dpIdx);
      dynAppend(newDpNames, dpName);
      dynAppend(mpOfNewDps, glVacMainParts[n]);
      continue;
    }
    // DebugTN("Create new ", dpName);
    // DP does not exist - create new DP
    dpCreate(dpName, STATE_SUMMARY_DP_TYPE);
    errList = getLastError();
    if(dynlen(errList) > 0)
    {
      DebugTN("lhcVacGDPMSupport: dpCreate(" + dpName + ") failed: " +
        StdErrorText(errList));
      continue;
    }
    if(!dpExists(dpName))
    {
      DebugTN("lhcVacGDPMSupport: failed to create DP <" + dpName + ">");
      continue;
    }
    dynAppend(newDpNames, dpName);
    dynAppend(mpOfNewDps, glVacMainParts[n]);
  }
  // DebugTN("Existing: " + existingDpNames);
  // DebugTN("New: " + newDpNames);
  // Set DIP adresses for new DPs
  for(n = dynlen(newDpNames) ; n > 0 ; n--)
  {
    dpeName = newDpNames[n] + ".STATE";
    itemName = "dip/VAC/" + glAccelerator + "/" + mpOfNewDps[n];
    ProcessDpePublishing(dpeName, itemName, exceptionInfo);    
    if(dynlen(exceptionInfo) > 0)    
    {
      DebugTN("lhcVacGDPMSupport.ctl: ProcessDpePublishing(" + dpeName + "," +
        itemName + ") failed", exceptionInfo);
    }
  }
  return true;
}
// InitResultDpPressures
/**
Purpose:
Create and publish via DIP resulting DPs for isolation vacuum main parts

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
bool InitResultDpPressures()
{
  int						n, i, dpIdx;
  dyn_string		exceptionInfo, existingDpNames, newDpNames, mpOfNewDps;
  string				mpName, dpName, itemName, dpeName, configDp;
  dyn_errClass	errList;
  bool					isConfigured;

  // Get names of all existing DPs of required type
  existingDpNames = dpNames("*", PRESSURE_SUMMARY_DP_TYPE);
  for(n = dynlen(existingDpNames) ; n > 0 ; n--)
  {
    existingDpNames[n] = dpSubStr(existingDpNames[n], DPSUB_DP);
  }

  // Create new DPs if necessary
  for(n = dynlen(gauges) ; n > 0 ; n--)
  {
    if(dynlen(gauges[n]) == 0)
    {
      continue;	// Not gauges MP
    }
    LhcVacPvssNameOfMainPart(glVacMainParts[n], mpName);
    dpName = PRESSURE_SUMMARY_DP_TYPE + "_" + mpName;
    // Check if DP with required name already exists
    if((dpIdx = dynContains(existingDpNames, dpName)) > 0)
    {
      dynRemove(existingDpNames, dpIdx);
      dynAppend(newDpNames, dpName);
      dynAppend(mpOfNewDps, glVacMainParts[n]);
      continue;
    }
    // DP does not exist - create new DP
    dpCreate(dpName, PRESSURE_SUMMARY_DP_TYPE);
    errList = getLastError();
    if(dynlen(errList) > 0)
    {
      DebugTN("lhcVacGDPMSupport: dpCreate(" + dpName + ") failed: " +
        StdErrorText(errList));
      continue;
    }
    if(! dpExists(dpName))
    {
      DebugTN("lhcVacGDPMSupport: failed to create DP <" + dpName + ">");
      continue;
    }
    dynAppend(newDpNames, dpName);
    dynAppend(mpOfNewDps, glVacMainParts[n]);
  }
  //DebugTN("Existing: " + existingDpNames);
  //DebugTN("New: " + newDpNames);
  // Set DIP adresses for new DPs
  for(n = dynlen(newDpNames) ; n > 0 ; n--)
  {
    dpeName = newDpNames[n] + ".STATE";
    itemName = "dip/VAC/" + glAccelerator + "/" + mpOfNewDps[n];
    ProcessDpePublishing(dpeName, itemName, exceptionInfo);    
    if(dynlen(exceptionInfo) > 0)    
    {
      DebugTN("lhcVacGDPMSupport.ctl: ProcessDpePublishing(" + dpeName + "," +
        itemName + ") failed", exceptionInfo);
    }
  }
  return true;
}
// InitResultValveStates
/**
Purpose:
Create and publish via DIP resulting DPs for state of single valves

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
bool InitResultValveStates()
{
  int						n, i, dpIdx;
  dyn_string		exceptionInfo, existingDpNames, newDpNames, valveDpNames;
  string				mpName, dpName, itemName, dpeName, configDp, eqpName;
  dyn_errClass	errList;
  bool					isConfigured;

  // Get names of all existing DPs of required type
  existingDpNames = dpNames("*", VALVE_STATE_DP_TYPE);
  for(n = dynlen(existingDpNames) ; n > 0 ; n--)
  {
    existingDpNames[n] = dpSubStr(existingDpNames[n], DPSUB_DP);
  }

  // Create new DPs if necessary
  for(n = dynlen(valves) ; n > 0 ; n--)
  {
    for(i = dynlen(valves[n]) ; i > 0 ; i -= 2)
    {
      dpName = VALVE_STATE_DP_TYPE + "_" + valves[n][i-1];
      // Check if DP with required name already exists
      if((dpIdx = dynContains(existingDpNames, dpName)) > 0)
      {
        dynRemove(existingDpNames, dpIdx);
        dynAppend(newDpNames, dpName);
        dynAppend(valveDpNames, valves[n][i-1]);
        continue;
      }
      // DP does not exist - create new DP
      dpCreate(dpName, VALVE_STATE_DP_TYPE);
      errList = getLastError();
      if(dynlen(errList) > 0)
      {
        DebugTN("lhcVacGDPMSupport: dpCreate(" + dpName + ") failed: " +
          StdErrorText(errList));
        continue;
      }
      if(!dpExists(dpName))
      {
        DebugTN("lhcVacGDPMSupport: failed to create DP <" + dpName + ">");
        continue;
      }
      dynAppend(newDpNames, dpName);
      dynAppend(valveDpNames, valves[n][i-1]);
    }
  }
//DebugTN("Existing: " + existingDpNames);
//DebugTN("New: " + newDpNames);

  // Set DIP adresses for all existing and new DPs
  for(n = dynlen(newDpNames) ; n > 0 ; n--)
  {
    LhcVacDisplayName(valveDpNames[n], eqpName, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("lhcVacGDPMSupport: ERROR: LhcVacDisplayName(" + valveDpNames[n] + ") failed:");
      DebugTN("lhcVacGDPMSupport:", exceptionInfo);
      dynClear(exceptionInfo);
      continue;
    }
    if(eqpName == "")
    {
      eqpName = newDpNames[n];
    }
    dpeName = newDpNames[n] + ".STATE";
    itemName = "dip/VAC/" + glAccelerator + "/Valves/" + eqpName;
    ProcessDpePublishing(dpeName, itemName, exceptionInfo);    
    if(dynlen(exceptionInfo) > 0)    
    {
      DebugTN("lhcVacGDPMSupport.ctl: ProcessDpePublishing(" + dpeName + "," +
        itemName + ") failed", exceptionInfo);
    }
  }
  return true;
}
// ConnectToDps
/**
Purpose:
Connect to all required DPs for state monitoring

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
ConnectToDps()
{
  int	n, i;

  // Valves
  for(n = dynlen(valves) ; n > 0 ; n--)
  {
    for(i = dynlen(valves[n]) ; i > 0 ; i -= 2)
    {
      dpConnect("ValveStateCb", valves[n][i-1] + ".RR1");
    }
  }
  // VPCI summaries
  for(n = dynlen(vpcis) ; n > 0 ; n -= 2)
  {
    dpConnect("VpciStateCb", vpcis[n-1] + ".RRC");
  }
  // Gauges
  for(n = dynlen(gauges) ; n > 0 ; n --)
  {
    for(i = dynlen(gauges[n]) ; i > 0 ; i -= 5)
    {
      dpConnect("GaugeStateCb", gauges[n][i-4] + ".RR1",
        gauges[n][i-4] + ".PR:");
    }
  }
}
// StdErrorText
/**
Purpose:
Replacement for errorText() function that has been removed from PVSS

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
string StdErrorText(dyn_errClass errList)
{
  int		errNbr, errCode;
  string	key, result;

  if (dynlen(errList) > 0)
  {
    errCode = getErrorCode(errList);
    sprintf(key, "%05d", errCode);
    result = getCatStr("_errors", key);
  }
  return result;
}
// ProcessMpState
/**
Purpose:
Process state of main part with given index

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void ProcessMpState(int mpIdx)
{
  int			vacType;
  string	mpName, dpName;
  time		now;
  float		deltaTime;

  if((mpIdx <= 0) || (mpIdx > dynlen(glVacMainParts)))
  {
    return;
  }
  now = getCurrentTime();
  deltaTime = now - lastUpdates[mpIdx];
  if(deltaTime < MIN_UPDATE_PERIOD)
  {
    pendingChanges[mpIdx] = true;
    return;
  }
  pendingChanges[mpIdx] = false;
  lastUpdates[mpIdx] = now;
  LhcVacPvssNameOfMainPart(glVacMainParts[mpIdx], mpName);
  LhcVacGetVacTypeOfMainPart(glVacMainParts[mpIdx], vacType);
  // There are two major types of main parts to deal with:
  // 1) Any beam vacuum	(main devices are valves)
  // 2) QRL/CRYO vacuum (main devices are gauges)
  if((vacType & (LVA_VACUUM_QRL | LVA_VACUUM_CRYO)) != 0)	// QRL/CRYO
  {
    dpName = PRESSURE_SUMMARY_DP_TYPE + "_" + mpName;
    ProcessQrlCryoMpState(mpIdx, dpName);
  }
  else
  {
    dpName = STATE_SUMMARY_DP_TYPE + "_" + mpName;
    ProcessBeamMpState(mpIdx, dpName);
  }
}
// ProcessQrlCryoMpState
/**
Purpose:
Process state of isolation vacuum main part with given index

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void ProcessQrlCryoMpState(int mpIdx, string dpName)
{
  string gaugeDpName;
  int				n, nValid, type;
  unsigned	state;
  float			pr, maxPr = 0.0;
  // Together with main part pressure this function calculates
  // pressures in individual sectors (to be piblished via CMW)
  dyn_string	vgSectorNames;
  dyn_float		vgSectorPr;
  dyn_bool		vgSectorPrValid;

  // DebugTN("ProcessQrlCryoMpState", mpIdx, dpName, lastUpdates[mpIdx]);
  if(dynlen(gauges[mpIdx]) == 0)
  {
    return;
  }
  // DebugTN(mpIdx + ": dynlen = " + dynlen(gauges[mpIdx]));
  for(n = dynlen(gauges[mpIdx]) ; n > 0 ; n -= 5)
  {
    gaugeDpName = gauges[mpIdx][n-4];
    state = gauges[mpIdx][n-3];
    pr = gauges[mpIdx][n-2];
    type = gauges[mpIdx][n-1];
    //DebugTN(gauges[mpIdx][n-2], state, pr, (state & VG_SPS_R_PRESS_VALID));

    // Get name of sector, add to sector list if needed
    string			sector1, sector2, mpName;
    int					vacType;
    bool				isBorder;
    dyn_string	exceptionInfo;
    int					sectorIdx;

    LhcVacDeviceVacLocation(gaugeDpName, vacType, sector1, sector2, mpName, isBorder, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("lhcVacGDPMSupport.ctl: LhcVacDeviceVacLocation(" + gaugeDpName + ") failed", exceptionInfo);
      sectorIdx = -1;
    }
    else
    {
      sectorIdx = dynContains(vgSectorNames, sector1);
      if(sectorIdx <= 0)
      {
        dynAppend(vgSectorNames, sector1);
        dynAppend(vgSectorPr, 0);
        dynAppend(vgSectorPrValid, false);
        sectorIdx = dynlen(vgSectorNames);
      }
    }

    // Process gauge data
    bool isValid = false;
    if(CanTrustToVG(type, state))
    {
      isValid = true;
      nValid++;
      if(type == GAUGE_TYPE_VGM)
      {
        if(ExistsValidOtherVg(gauges[mpIdx], gaugeDpName, GAUGE_TYPE_VGR, GAUGE_TYPE_VGP))
        {
          continue;
        }
      }
      /* L.Kopylov 22.04.2008 If VGP is valid - use VGP
      else if(type == GAUGE_TYPE_VGP)
      {
        if(ExistsValidOtherVg(gauges[mpIdx], gaugeDpName, GAUGE_TYPE_VGR, -1))
        {
          continue;
        }
      }
      */
      // L.Kopylov 22.04.2008 do not use VGR if there is valid VGP
      else if(type == GAUGE_TYPE_VGR)
      {
        if(ExistsValidOtherVg(gauges[mpIdx], gaugeDpName, GAUGE_TYPE_VGP, -1))
        {
          continue;
        }
      }
      if((pr < 10000.) && (pr > maxPr))
      {
        maxPr = pr;
      }
      // Sector pressure
      if(sectorIdx >= 0)
      {
        vgSectorPrValid[sectorIdx] = true;
        if((pr < 10000.) && (pr > vgSectorPr[sectorIdx]))
        {
          vgSectorPr[sectorIdx] = pr;
        }
      }
    }
    else
    {
      bool	skipGauge = false;
      switch(type)
      {
      case GAUGE_TYPE_VGM:
        if(ExistsValidOtherVg(gauges[mpIdx], gaugeDpName, GAUGE_TYPE_VGR, GAUGE_TYPE_VGP))
        {
          skipGauge = true;
        }
        break;
      case GAUGE_TYPE_VGP:
        if(ExistsValidOtherVg(gauges[mpIdx], gaugeDpName, GAUGE_TYPE_VGR, GAUGE_TYPE_VGM))
        {
          skipGauge = true;
        }
        break;
      case GAUGE_TYPE_VGR:
        if(ExistsValidOtherVg(gauges[mpIdx], gaugeDpName, GAUGE_TYPE_VGM, GAUGE_TYPE_VGP))
        {
          skipGauge = true;
        }
        break;
      }
      if(skipGauge)
      {
        continue;
      }
    }
  }
  if(nValid == 0)
  {
    maxPr = 10000.;
  }
  dpSet(dpName + ".STATE:_original.._value", maxPr,
    dpName + ".STATE:_original.._exp_inv", (nValid == 0 ? true : false));
  // DebugTN("Result for " + dpName + " is: " + maxPr, nValid);

  // Update pressures in individual sectors
  for(int sectorIdx = dynlen(vgSectorNames) ; sectorIdx > 0 ; sectorIdx--)
  {
    // Check if sector summary DP exists for this sector
    string sectorPvssName;
    LhcVacPvssNameOfSector(vgSectorNames[sectorIdx], sectorPvssName);
    if(sectorPvssName == "")
    {
      continue;
    }
    string prDpName = ISOL_PR_EXPORT_DP_TYPE + "_" + sectorPvssName;
    if(!dpExists(prDpName))
    {
      continue;
    }
    dpSet(prDpName + ".RR1", (vgSectorPrValid[sectorIdx] ? 1 : 0),
      prDpName + ".PR", (vgSectorPrValid[sectorIdx] ? vgSectorPr[sectorIdx] : 10000));
  }
}

bool ExistsValidOtherVg(dyn_string allGauges, string forVg, int type1, int type2)
{
  string	myTopParent, topParent;
  LhcVacGetAttribute(forVg, "TopParent", myTopParent);
  if(myTopParent == "")
  {
    return false;
  }
  for(int n = dynlen(allGauges) ; n > 0 ; n -= 5)
  {
    if((allGauges[n-1] != type1) && (allGauges[n-1] != type2))
    {
      continue;
    }
    LhcVacGetAttribute(allGauges[n-4], "TopParent", topParent);
    if(topParent != myTopParent)
    {
      continue;
    }
    if(CanTrustToVG(allGauges[n-1], allGauges[n-3]))
    {
      return true;
    }
  }
  return false;
}

bool CanTrustToVG(int gaugeType, unsigned state)
{
  if((state & VG_SPS_R_VALID) == 0)
  {
    return false;
  }
  if((state & VG_SPS_R_PRESS_VALID) != 0)
  {
    return true;
  }
  // We still can trust to VGR which is underrange
  if(gaugeType == GAUGE_TYPE_VGR)
  {
    if((state & (VG_SPS_R_OFF | VG_SPS_R_ON)) == VG_SPS_R_ON)	// ON
    {
      if((state & VG_SPS_R_UNDERRANGE) != 0)	// underrange
      {
        return true;
      }
    }
  }
  return false;
}

// ProcessBeamMpState
/**
Purpose:
Process state of beam vacuum main part with given index

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void ProcessBeamMpState(int mpIdx, string dpName)
{
  int					n, i, result = 1, vacType;
  string			sector1, sector2, mpName, sectDpName;
  bool				isBorder;
  dyn_string	exceptionInfo;
  unsigned 		statusOr;

  // DebugTN("ProcessBeamMpState", mpIdx, dpName, lastUpdates[mpIdx]);
  if(dynlen(valves[mpIdx]) == 0)
  {
    return;
  }

  for(n = dynlen(valves[mpIdx]) ; n > 0 ; n-= 2)
  {
    dynClear(exceptionInfo);
    if((valves[mpIdx][n] & SVCU_SPS_R_CLOSED) == 0)
    {
      continue;	// Valve is NOT closed
    }
    // Check if at least one VPCI is ON in one of neighbour sectors
    LhcVacDeviceVacLocation(valves[mpIdx][n-1], vacType, sector1, sector2, mpName, isBorder, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("lhcVacGDPMSupport: LhcVacDeviceVacLocation(" + valves[n-1] + ") failed: " +
        exceptionInfo);
      continue;
    }
    // Sector1
    LhcVacSectSummaryDpName(sector1, sectDpName, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      continue;
    }
    if(sectDpName == "")
    {
      continue;
    }
    if((i = dynContains(vpcis, sectDpName)) > 0)
    {
      statusOr = vpcis[i+1] & 0xFFFF0000u;
      if((statusOr & VRPI_W_R_ON) != 0)	// At least some VRPIs are ON
      {
        result = 2;
      }
      else
      {
        result = 3;
      }
    }
    if(result == 3)
    {
      break;
    }
    // Sector2
    if(sector2 == "")
    {
      continue;
    }
    if(sector2 == sector1)
    {
      continue;
    }
    LhcVacSectSummaryDpName(sector2, sectDpName, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      continue;
    }
    if(sectDpName == "")
    {
      continue;
    }
    if((i = dynContains(vpcis, sectDpName)) > 0)
    {
      statusOr = vpcis[i+1] & 0xFFFF0000u;
      if((statusOr & VRPI_W_R_ON) != 0)	// At least some VRPIs are ON
      {
        result = 2;
      }
      else
      {
        result = 3;
      }
    }
    if(result == 3)
    {
      break;
    }
  }
  // DebugTN(glVacMainParts[mpIdx], result);
  // Write resulting DP value
  dpSet(dpName + ".STATE:_original.._value", result);
}
// ProcessValveState
/**
Purpose:
Process state of single valve

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void ProcessValveState(string dpName, unsigned state)
{
  string stateDpName = VALVE_STATE_DP_TYPE + "_" + dpName;
  if(!dpExists(stateDpName))
  {
    return;
  }
  int result = 0;
  if(((state & SVCU_SPS_R_OPEN) != 0) && ((state & SVCU_SPS_R_CLOSED) != 0))
  {
    result = 3;
  }
  else if((state & SVCU_SPS_R_OPEN) != 0)
  {
    result = 1;
  }
  else if((state & SVCU_SPS_R_CLOSED) != 0)
  {
    result = 2;
  }
  else
  {
    result = 3;
  }
  dpSet(stateDpName + ".STATE:_original.._value", result,
    stateDpName + ".STATE:_original.._exp_inv", ((state & SVCU_SPS_R_VALID) == 0 ? true : false));
}
// ValveStateCb
/**
Purpose:
Callback for valve state change

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void ValveStateCb(string dpeName, unsigned state)
{
  string	dpName;
  int			n, i;

  //DebugTN("ValveStateCb", dpeName, state);
  dpName = dpSubStr(dpeName, DPSUB_DP);
  i = 0;
  for(n = dynlen(valves) ; n > 0 ; n--)
  {
    if((i = dynContains(valves[n], dpName)) > 0)
    {
      break;
    }
  }
  if(i <= 0)
  {
    DebugTN("lhcVacGDPMSupport: ValveStateCb(): Valve " + dpName + " is not found");
    return;
  }
  ProcessValveState(dpName, state);
  if(valves[n][i+1] == state)	// No changes
  {
    return;
  }
  valves[n][i+1] = state;
  // DebugTN(dpName, glVacMainParts[n], i);
  if(isInitialized)
  {
    ProcessMpState(n);
  }
}
// VpciStateCb
/**
Purpose:
Callback for VPCI summary state change

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void VpciStateCb(string dpeName, unsigned state)
{
  string	dpName;
  int			n;

  //DebugTN("VpciStateCb", dpeName, state);
  dpName = dpSubStr(dpeName, DPSUB_DP);
  if((n = dynContains(vpcis, dpName)) <= 0)
  {
    DebugTN("lhcVacGDPMSupport: VpciStateCb(): " + dpName + " is not found");
    return;
  }
  if(vpcis[n+1] == state)	// No changes
  {
    return;
  }
  vpcis[n+1] = state;
  if(!isInitialized)
  {
    return;
  }
  for(n = dynlen(glVacMainParts) ; n > 0 ; n--)
  {
    ProcessMpState(n);
  }
}
// VpciStateCb
/**
Purpose:
Callback for gauge state + pressure change

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void GaugeStateCb(string rrDpeName, unsigned rr, string prDpeName, float pr)
{
  string	dpName;
  int			n, i;

  dpName = dpSubStr(rrDpeName, DPSUB_DP);
  i = 0;
  for(n = dynlen(gauges) ; n > 0 ; n--)
  {
    if((i = dynContains(gauges[n], dpName)) > 0)
    {
      break;
    }
  }
  if(i <= 0)
  {
    DebugTN("lhcVacGDPMSupport: GaugeStateCb(): Gauge " + dpName + " is not found");
    return;
  }
  /*
  else
  {
    DebugTN(dpName, n, i);
  }
  */
  if((gauges[n][i+1] == rr) && (gauges[n][i+2] == pr))	// No changes
  {
    return;
  }
  gauges[n][i+1] = rr;
  gauges[n][i+2] = pr;
  gauges[n][i+4] = true;
  if(isInitialized)
  {
    ProcessMpState(n);
  }
}
