#ifndef MOBILETYPE_H
#define MOBILETYPE_H

#include "VacCtlEqpDataExport.h"

// Classification of equipment types according to mobility
// !!! Values of enum shall match corresponding values in master DB

#include <QString>

class VACCTLEQPDATA_EXPORT MobileType
{
public:
	enum
	{
		Fixed = 0,			// Fixed equipment
		Container = 1,		// Container for mobile equipment (VMOB devices)
		OnFlange = 2,		// Mobile equipment on flange (with physical location - VPGM)
		OnSector = 3,		// Mobile equipment on sector (without physical location - BAKEOUT)
		EqpList = 4,		// List of active mobile equipment on PVSS server
		IsolSectorPR = 5,	// Pressure summary in isolation vacuum sector (not related for mobile,
							// it is here just for historical reason - see master DB)
		PlcAlarm = 6, 		// PLC alarm device (not related for mobile, it is here just for historical reason - see master DB)
		PlcCtl = 7,			// PLC control device (not related for mobile, it is here just for historical reason - see master DB)
		Master = 8,			// Classification of equipment DP: Master Wireless Eqp for TCP communication
		Instance = 9,		// Classification of equipment DP: Position Instance Wireless Eqp for TCP communication
	};	

	static int decode(const char *token, int lineNo, QString &errMsg);

};
	
#endif // MOBILETYPE_H
