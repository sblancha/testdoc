#include "EqpType.h"

#include <string.h>
#include <stdio.h>

/*
**	FUNCTION
**		Return readable name of given type - used for debugging only
**
**	ARGUMENTS
**		type	- Type whose name is needed
**
**	RETURNS
**		Pointer to static string with type name
**
**	CAUTIONS
**		None
*/
const char *EqpType::getName(int type)
{
	switch(type)
	{
	case None:
		return "None";
	case Passive:
		return "Passive";
	case LineStart:
		return "LineStart";
	case LineEnd:
		return "LineEnd";
	case OtherLineStart:
		return "OtherLineStart";
	case OtherLineEnd:
		return "OtherLineEnd";
	case VacDevice:
		return "VacDevice";
	case SectorBefore:
		return "SectorBefore";
	case SectorAfter:
		return "SectorAfter";
	case Aux:
		return "Aux";
	default:
		break;
	}
	static char result[32];
#ifdef Q_OS_WIN
	sprintf_s(result, sizeof(result) / sizeof(result[0]), "Unknonw EqpType %d", type);
#else
	sprintf(result, "Unknonw EqpType %d", type);
#endif
	return result;
}
