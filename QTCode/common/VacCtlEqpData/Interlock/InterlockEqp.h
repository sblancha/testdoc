#ifndef INTERLOCKEQP_H
#define INTERLOCKEQP_H
#include "VacCtlEqpDataExport.h"
#include <qmap.h>

// Possible types
typedef enum
{
	VRVCL = 1,
	VRVCX,
	VRIVC,
	VRIVD,
	VRPI,
	VRGPT,
	UNKNOWN = 100
} TypeEnum;

class VACCTLEQPDATA_EXPORT InterlockEqp {

public:
	InterlockEqp(QString name);
	~InterlockEqp();

	inline QString getName(){ return name; };
	inline void addAttribute(QString attributeName, QString attributeValue){ attributes.insert(attributeName, attributeValue); };
	inline TypeEnum getType(){ return type; };
	inline QMap<QString, QString> &getAttributeMapRef(){ return attributes; };
private:
	QString name;
	TypeEnum type;

	void setTypeFromName();
	QMap<QString, QString> attributes;
};
#endif