#ifndef SYNEQPITEM_H
#define	SYNEQPITEM_H

// Class holding information for single equipment item in synoptic view,
// used for bot passive and active equipment
#include "ValveInterlockSource.h"

#include "DataEnum.h"

#include <qrect.h>

#include "Eqp.h"
class BeamLinePart;
class VacIcon;

// Special value for pointers to equipment indicating start/end for
// part of synoptic line to be drawn with alternative color.
// This value for start device of line part means: from start of line,
// for end device: to end of line
#define	SPEC_EQP_PTR	((Eqp *)1)

class SynEqpItem
{
public:
	SynEqpItem();
	~SynEqpItem();

	const char *getName(void) const;
	const char *getNameBefore(bool reverse) const;
	const char *getNameAfter(bool reverse) const;
	void moveIcon(void);
	void setPipeColor(QColor &color);
	void setBeamDirection(int direction);

	// Access
	inline QRect &getRect(void) { return rect; }
	void setRect(QRect &rect);
	inline QPoint &getConnPoint(void) { return connPoint; }
	inline void setConnPoint(QPoint point) { connPoint = point; }
	inline Eqp *getEqp(void) const { return pEqp; }
	inline void setEqp(Eqp *pEqp) { this->pEqp = pEqp; }
	inline VacIcon *getIcon(void) const { return pIcon; }
	inline void setIcon(VacIcon *pIcon) { this->pIcon = pIcon; }
	int getIconHeight(void);
	inline const BeamLinePart *getBeamLinePart(void) const { return pBeamLinePart; }
	inline void setBeamLinePart(const BeamLinePart *pPart) { pBeamLinePart = pPart; }
	inline int getType(void) const { return type; }
	inline void setType(int type) { this->type = type; }
	inline int getEqpIdx(void) const { return eqpIdx; }
	inline void setEqpIdx(int idx) { eqpIdx = idx; }
	inline int getGroupId(void) const { return groupId; }
	inline void setGroupId(int id) { groupId = id; }
	inline int getVerticalPos(void) const { return verticalPos; }
	inline void setVerticalPos(int pos) { verticalPos = pos; }
	inline bool isSectorBorder(void) const { return sectorBorder; }
	inline void setSectorBorder(bool border) { sectorBorder = border; }
	inline bool isStartFixed(void) const { return startFixed; }
	inline void setStartFixed(bool fixed) { startFixed = fixed; }
	inline bool isCommonReady(void) const { return commonReady; }
	inline void setCommonReady(bool ready) { commonReady = ready; }
	inline bool isPartReverse(void) const { return partReverse; }
	inline void setPartReverse(bool reverse) { partReverse = reverse; }

	inline bool isIntlSourceForward(void) { return intlSourceForward; }
	inline void setIntlSourceForward(bool flag) { intlSourceForward = flag; }
	inline bool	isIntlSourceBackward(void) { return intlSourceBackward; }
	inline void setIntlSourceBackward(bool flag) { intlSourceBackward = flag; }

	inline int getTextStart(void) const { return textStart; }
	inline void setTextStart(int coord) { textStart = coord; }
	inline int getTextEnd(void) const { return textEnd; }
	inline void setTextEnd(int coord) { textEnd = coord; }
	inline int getTextHeight(void) const { return textHeight; }
	inline void setTextHeight(int height) { textHeight = height; }

protected:
	// Geometry of this item
	QRect			rect;

	// Connection point
	QPoint			connPoint;

	// Pointer to equipment of this item.
	Eqp				*pEqp;

	// Pointer to icon of this item (active equipment only)
	VacIcon			*pIcon;

	// Pointer to beam line part where this equipment is taken
	const BeamLinePart	*pBeamLinePart;

	// Item type - see EqpType.h
	int				type;

	// Index of this equipment in beam line part
	int				eqpIdx;

	// ID of equipment group within synoptic line (unique for every group)
	int				groupId;

	// Vertical position of item: 0 = on line, 1 = above line, -1 = below line
	int				verticalPos;

	// Start of sector name text before element
	int				textStart;

	// End of sector name text after element
	int				textEnd;

	// Height of sector name text
	int				textHeight;

	// Flag indicating if this is visible sector border
	bool			sectorBorder;

	// Falg indicating if start of this item is fixed
	bool			startFixed;

	// Flag indicating if common lines of this item's group has been drawn
	bool			commonReady;

	// Flag indicating if part of line, including this item, is built in reverse order
	bool			partReverse;

	// L.Kopylov 28.01.2010 addition to identify interlock sources for valves on synoptic

	// Flag to identify if this device is interlock source for valve which is after this device on view
	bool		intlSourceForward;

	// Flag to identify if this device is interlock source for valve which is before this device on view
	bool		intlSourceBackward;
};

#endif	// SYNEQPITEM_H
