//	Implementation of EqpVPGMPR class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVPGMPR.h"

#include "FunctionalType.h"
#include "EqpMsgCriteria.h"
#include "DevListCriteria.h"

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

EqpVPGMPR::EqpVPGMPR(const Eqp &source) : EqpVG(source)
{
	activeOnline = activeReplay = false;
	functionalType = FunctionalType::VPGMPR;
	haveEqpStatus = false;
}

EqpVPGMPR::~EqpVPGMPR()
{
}

/*
**	FUNCTION
**		Notify that this device's activity has been changed for given mode
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**		active	- Activity flag for given modeo
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPGMPR::setActive(DataEnum::DataMode mode, bool active)
{
	switch(mode)
	{
	case DataEnum::Replay:
		if(active == activeReplay)
		{
			return;
		}
		else
		{
			activeReplay = active;
		}
		break;
	default:
		if(active == activeOnline)
		{
			return;
		}
		else
		{
			activeOnline = active;
		}
		break;
	}

	emit mobileStateChanged(this, mode);
}

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVPGMPR::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	DataEnum::DataMode mode = criteria.getMode();
	bool active = mode == DataEnum::Replay ? activeReplay : activeOnline;
	if(!active)
	{
		return 0;
	}
	return EqpVG::checkDevListCrit();
}
#endif
