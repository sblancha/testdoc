#ifndef	DATETIMEEDITEWO_H
#define	DATETIMEEDITEWO_H

// Simple EWO INTERFACE FOR DateEditWidget widget

#include <BaseExternWidget.hxx>

#include "DateEditWidget.h"

class EWO_EXPORT DateEditEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	DateEditEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void dateTimeChange(const QDateTime &value);
		
private:
	DateEditWidget	*baseWidget;
};

#endif	// DATETIMEEDITEWO_H
