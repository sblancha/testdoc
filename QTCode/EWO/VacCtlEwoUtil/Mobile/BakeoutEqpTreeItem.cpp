//	Implementation of BakeoutEqpTreeItem class
/////////////////////////////////////////////////////////////////////////////////

#include "BakeoutEqpTreeItem.h"

#include "BakeoutChannelTreeItem.h"
#include "MobileColorList.h"
#include "MobileHistoryImage.h"

#include "Eqp.h"

#include <qpixmap.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

BakeoutEqpTreeItem::BakeoutEqpTreeItem(MobileHistoryImage *pImage, QTreeWidgetItem *parent, Eqp *pEqp)
	: QTreeWidgetItem(parent, MobileTreeItemBakeoutRack)
{
	this->pEqp = pEqp;
	setText(0, pEqp->getVisibleName());
	setFlags(flags() | Qt::ItemIsUserCheckable);
	setCheckState(0, Qt::Unchecked);
	QPixmap *pm = MobileColorList::getVacIcon(pEqp->getVacType(), false);
	setIcon(0, QIcon(*pm));

	// Add channels in reverse order - they will appear in 'normal' order in tree
	for(int n = 1 ; n <= 32 ; n++)
	{
		addChannelItem(pImage, n, BakeoutWorkDp::ChannelValueTypeT);
		if (n <= 24) {
			addChannelItem(pImage, n, BakeoutWorkDp::ChannelValueTypeSetT);
		}
		addChannelItem(pImage, n, BakeoutWorkDp::ChannelValueTypeStatus);
	}
}

BakeoutEqpTreeItem::~BakeoutEqpTreeItem()
{
}

void BakeoutEqpTreeItem::addChannelItem(MobileHistoryImage *pImage, int channel, enum BakeoutWorkDp::ChannelValueType valueType)
{
	BakeoutChannelTreeItem *pItem = new BakeoutChannelTreeItem(this, channel, valueType);
	pItem->setFlags(pItem->flags() | Qt::ItemIsUserCheckable);
	int itemIndex = pImage->getEqpIndex(pEqp, channel, valueType);
	if (itemIndex >= 0)
	{
		pItem->setCheckState(0, Qt::Checked);
		pImage->setItemUsed(itemIndex);
	}
	else
	{
		pItem->setCheckState(0, Qt::Unchecked);
	}
}

/*
**	FUNCTION
**		Open/close this item
**
**	ARGUMENTS
**		open	- true if item shall be open
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutEqpTreeItem::setExpanded(bool open)
{
	QPixmap *pm = MobileColorList::getVacIcon(pEqp->getVacType(), open);
	setIcon(0, QIcon(*pm));
	QTreeWidgetItem::setExpanded(open);
}

