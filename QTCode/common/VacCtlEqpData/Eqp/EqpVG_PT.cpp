//	Implementation of EqpVG_PT class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVG_PT.h"

#include "DataPool.h"
#include "FunctionalTypeVG.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0
#define COLOR_WARNING			255,255,0
#define COLOR_UNDERRANGE		0,180,0
#define COLOR_FORCED			255,127,39

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define	RR1_ON			(0x02000000)
#define	RR1_OFF			(0x01000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define RR1_THRSH_HOLD	(0x00100000)
#define RR1_FORCE_ORDER	(0x00080000)
#define RR1_ON_ORDER	(0x00040000)
#define RR1_PROTECTED	(0x00020000)
#define RR1_FORCED		(0x00010000)
#define RR2_AL3			(0x8000)
#define RR2_AL2			(0x4000)
#define RR2_AL1			(0x2000)
#define RR1_ERROR_CODE_MASK	(0x000000FF)
#define RR1_WARN_CODE_MASK	(0x0000FF00)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVG_PT::EqpVG_PT(const Eqp &source) : Eqp(source)
{
	pSectorBeforeOnline = pSectorBeforeReplay = pSectorBefore;
	pSectorAfterOnline = pSectorAfterReplay = pSectorAfter;

	rr1Online = rr1Replay = rr2Online = rr2Replay = 0;
	prOnline = prReplay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	blockedOffOnline = blockedOffReplay = false;
	switch(ctrlSubType)
	{
	case 1:	// Penning gauges VGP (5E-3 to 5E-9)
	case 2:	// Penning gauges VGP (5E-3 to 1.1E-11)
		functionalType = FunctionalType::VGP;
		break;
	case 3:	// Pirani gauges VGR
		functionalType = FunctionalType::VGR;
		break;
	}
}

EqpVG_PT::~EqpVG_PT()
{
}

#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::setSectorBefore(DataEnum::DataMode mode, Sector *pSector)
{
	bool emitSignal = false;
	switch(mode)
	{
	case DataEnum::Replay:
		emitSignal = pSectorBeforeReplay != pSector;
		pSectorBeforeReplay = pSector;
		break;
	default:
		emitSignal = pSectorBeforeOnline != pSector;
		pSectorBeforeOnline = pSector;
		break;
	}
	if(emitSignal)
	{
		emit sectorsChanged(this);
	}
}
#endif	// PVSS_SERVER_VERSION

#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::setSectorAfter(DataEnum::DataMode mode, Sector *pSector)
{
	bool emitSignal = false;
	switch(mode)
	{
	case DataEnum::Replay:
		emitSignal = pSectorAfterReplay != pSector;
		pSectorAfterReplay = pSector;
		break;
	default:
		emitSignal = pSectorAfterOnline != pSector;
		pSectorAfterOnline = pSector;
		break;
	}
	if(emitSignal)
	{
		emit sectorsChanged(this);
	}
}
#endif	// PVSS_SERVER_VERSION

#ifndef PVSS_SERVER_VERSION
bool EqpVG_PT::isSectorBorder(DataEnum::DataMode mode) const
{
	bool result = false;
	switch(mode)
	{
	case DataEnum::Replay:
		result = pSectorBeforeReplay && pSectorAfterReplay && (pSectorBeforeReplay != pSectorAfterReplay);
		break;
	default:
		result = pSectorBeforeOnline && pSectorAfterOnline && (pSectorBeforeOnline != pSectorAfterOnline);
		break;
	}
	return result;
}
#endif	// PVSS_SERVER_VERSION

/*
**
**	FUNCTION
**		Set reference to new sectors after ordered sector list has been built
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_PT::setNewSectors(void)
{
	if(pSectorBefore)
	{
		pSectorBefore = pSectorBeforeOnline = pSectorBeforeReplay = pSectorBefore->getNewSector();
	}
	if(pSectorAfter)
	{
		pSectorAfter = pSectorAfterOnline = pSectorAfterReplay = pSectorAfter->getNewSector();
	}
}


/*
**	FUNCTION
**		Add reference to itself to VPG if this gauge is part of VPG
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_PT::postProcess(void)
{
	const QString vpgName = getAttrValue("CtlParent");
	if(!vpgName.isEmpty())
	{
		Eqp *pVPG = DataPool::getInstance().findEqpByDpName(vpgName.toLatin1());
		if(pVPG)
		{
			pVPG->addCtlChild((Eqp *)this);
		}
	}
}

/*
**	FUNCTION
**		Find master DP and master channel for this gauge.
**		In principle this makes sense only for TPG-controlled gauges,
**		but instead of checking type we'll just check if gauge has
**		corresponding attribute: compact gauges just have no such
**		attributes.
**
**	ARGUMENTS
**		masterDP		- Variable where master DP name will be written
**		masterChannel	- Variable where channel number in master will
**							be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_PT::getMaster(QString &masterDp, int &masterChannel)
{
	masterDp = getAttrValue("MasterName");
	if(masterDp.isEmpty())
	{
		masterChannel = 0;
		return;
	}
	masterChannel = getAttrValue("MasterChannel").toInt();
}

/*
**	FUNCTION
**		Find master config DP and master config channel for this gauge.
**		In principle this makes sense only for TPG-controlled gauges,
**		but instead of checking type we'll just check if gauge has
**		corresponding attribute: compact gauges just have no such
**		attributes.
**
**	ARGUMENTS
**		configDP		- Variable where config DP name will be written
**		configChannel	- Variable where channel number in config will
**							be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_PT::getConfig(QString &configDp, int &configChannel)
{
	configDp = getAttrValue("MasterConfig");
	if(configDp.isEmpty())
	{
		configChannel = 0;
		return;
	}
	configChannel = getAttrValue("MasterConfigRef").toInt();
}


/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_PT::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "RR2");
	addDpeNameToList(stateDpes, "BlockedOFF");
}

/*
**	FUNCTION
**		Return list of all DPEs, related to analog value of device. The list is used
**		to build value history
**
**	ARGUMENTS
**		valueDpes	- Variable where all value DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_PT::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	addDpeNameToList(valueDpes, "PR");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_PT::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "RR2", mode);
	connectDpe(pDst, "PR", mode);
	connectDpe(pDst, "BlockedOFF", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_PT::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "BlockedOFF", mode);
	disconnectDpe(pDst, "PR", mode);
	disconnectDpe(pDst, "RR2", mode);
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "EqpStatus", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_PT::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Online == newValue)
			{
				return;
			}
			rr2Online = newValue;
		}
	}
	else if(!strcmp(dpe, "PR"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(prOnline == newValue)
			{
				return;
			}
			prOnline = newValue;
		}
	}
	else if(!strcmp(dpe, "BlockedOFF"))
	{
		if(value.canConvert(QVariant::Bool))
		{
			bool newValue = value.toBool();
			if(blockedOffOnline == newValue)
			{
				return;
			}
			blockedOffOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Replay == newValue)
			{
				return;
			}
			rr2Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "PR"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(prReplay == newValue)
			{
				return;
			}
			prReplay = newValue;
		}
	}
	else if(!strcmp(dpe, "BlockedOFF"))
	{
		if(value.canConvert(QVariant::Bool))
		{
			bool newValue = value.toBool();
			if(blockedOffReplay == newValue)
			{
				return;
			}
			blockedOffReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_PT::dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, prReplay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, prOnline, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Return main value for gauge - pressure
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVG_PT::getMainValue(DataEnum::DataMode mode)
{
	return mode == DataEnum::Replay ? prReplay : prOnline;
}
#endif

/*
**	FUNCTION
**		Check if main value for gauge (pressure) is valid or not.
**		Latest known requirement: even invalid value shall be shown,
**		so the value is always considered to be valid
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
bool EqpVG_PT::isMainValueValid(DataEnum::DataMode /* mode */)
{
	return true;
}
#endif

/*
**	FUNCTION
**		Check if main value for gauge (pressure) can be negative
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if pressure can be negative;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
bool EqpVG_PT::isMainValueNegative(void) const
{
	return false;	// NEVER
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::getMainColor(unsigned rr1, float /* pr */, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))	// Not valid
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr1 & RR1_ERROR)	// Error
	{
		color.setRgb(COLOR_ERROR);
	}
	else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))	// Both ON and OFF == error
	{
		color.setRgb(COLOR_ERROR);
	}
	else if (!((rr1 & RR1_ON) || (rr1 & RR1_OFF))) {	// Neither ON nor OFF == undefined
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (rr1 & RR1_OFF) {	// Definitely OFF
		color.setRgb(COLOR_OFF);
	}
	else if (rr1 & RR1_ON) {	// ON
		if ((rr1 & RR1_FORCED) && ((ctrlSubType == 1) || (ctrlSubType == 2))) {	// Forced, only for VGP (subtypes 1 and 2)
			color.setRgb(COLOR_FORCED);
		}
		else if (rr1 & RR1_WARNING) {
			// Warning 'underrange' is shown in special color
			// when gauge is ON and no other warnings
			unsigned warningCode = (rr1 >> 8) & 0xFF;
			if(warningCode == 127) {
				color.setRgb(COLOR_UNDERRANGE);
			}
			else {
				color.setRgb(COLOR_WARNING);
			}
		}
		else {	// Not forced, no warning == just ON
			color.setRgb(COLOR_ON);
		}
	}
	else {	// What else? Most probably will never come here
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, prReplay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, prOnline, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::getMainValueString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainValueString(rr1Replay, prReplay, plcAlarmReplay, string);
	}
	else
	{
		getMainValueString(rr1Online, prOnline, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::getMainValueString(unsigned /* rr1 */, float pr, bool /* plcAlarm */, QString &string)
{
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%8.2E", pr);
#else
	sprintf(buf, "%8.2E", pr);
#endif
	string = buf;
}
#endif


/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::getMainStateString(unsigned rr1, float /* pr */, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr1 & RR1_ON)
	{
		if(rr1 & RR1_OFF)
		{
			string = "Error";
		}
		else
		{
			string = "ON";
		}
	}
	else if(rr1 & RR1_OFF)
	{
		string = "OFF";
	}
	else
	{
		string = "Undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainValueString(valueString, mode);
	string += valueString;
}
#endif

/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("RR1");
	//dpeNames.append("RR2");
	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, 0, false, state);
	getMainColor(rr1, 0, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpVG_PT::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	return rr1 & RR1_ERROR_CODE_MASK;
}





/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "EqpStatus");
	addDpeNameToList(dpes, "RR1");
	addDpeNameToList(dpes, "PR");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history)
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVG_PT::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode ==  DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if((rr1 & (RR1_ON | RR1_OFF)) == RR1_ON)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if((rr1 & (RR1_ON | RR1_OFF)) == RR1_OFF)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if(rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if(criteria.isWarnings())
	{
		if(rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}
	if(criteria.isAlerts())
	{
		unsigned rr2 = mode ==  DataEnum::Replay ? rr2Replay : rr2Online;
		if(rr2 & (RR2_AL1 | RR2_AL2 | RR2_AL3))
		{
			return true;
		}
	}

	// Check pressure if needed
	if(criteria.isUsePressureLimit())
	{
		float pr = mode == DataEnum::Replay ? prReplay : prOnline;
		if(pr > criteria.getPressureLimit())
		{
			return 1;
		}
	}

	// Last chance - criteria on pressure grow
	if(criteria.isUsePressureGrowLimit())
	{
		// Can not decide right now, more info is needed
		return 2;
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Return parameters for history query, required for device list
**
**	ARGUMENTS
**		dpe		- [out] DPE, for which history is required
**		start	- [out] start time of history query
**		end		- [out] end time of history query
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PT::getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUsePressureGrowLimit())
	{
		dpe = "";
		return;
	}
	dpe = dpName;
	dpe += ".PR";
	start = criteria.getPressureStartTime();
	if(criteria.isPressureEndTimeNow())
	{
		end = QDateTime::currentDateTime();
	}
	else
	{
		end = criteria.getPressureEndTime();
	}
}
#endif

/*
**	FUNCTION
**		Return grow factor for main value (pressure) to check if
**		device matches device list criteria
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Pressure grow factor; or
**		0 if criteria does not contain pressure grow
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVG_PT::getGrowFactorForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUsePressureGrowLimit())
	{
		return 0.0;
	}
	return criteria.getPressureGrowLimit();
}

int EqpVG_PT::getSpikeFilterForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUsePressureGrowLimit())
	{
		return 0.0;
	}
	return criteria.getPressureSpikeInterval();
}

#endif

/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVG_PT::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if(checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	// Without PLC connection does not match any criteria
	if(plcAlarmOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// In invalid state - only matches Error/warning criteria
	if(!(rr1Online & RR1_VALID))
	{
		switch(pCriteria->getType())
		{
		case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		default:
			pCriteria->setMatch(false, false, 0);
			return;
		}
	}

	// State is valid - decide based on criteria
	unsigned maskedRr1 = rr1Online & (RR1_ON | RR1_OFF);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		// L.Kopylov 29.10.2012 - ON/OFF state may be further clarified with value limit (i.e. if state is ON, but value is too high - gauge
		// is not really 'ON').
		if(pCriteria->getLowerLimit() > 0)	// OFF -> ON transition only if pressure if low enough
		{
			// Does criteria 'think' the recent state is OFF?
			bool recentStateOff = false;
			switch(pCriteria->getSubType())
			{
			case FunctionalTypeVG::Off:
				if(pCriteria->isReverse())
				{
					recentStateOff = ! pCriteria->isMatch();
				}
				else
				{
					recentStateOff = pCriteria->isMatch();
				}
				break;
			case FunctionalTypeVG::On:
				if(pCriteria->isReverse())
				{
					recentStateOff = pCriteria->isMatch();
				}
				else
				{
					recentStateOff = ! pCriteria->isMatch();
				}
				break;
			default:
				break;
			}
			// Make decision on new state (taking into account OFF -> ON transition)
			if(maskedRr1 == RR1_OFF)
			{
				if(pCriteria->getSubType() == FunctionalTypeVG::Off)
				{
					pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				}
				else if(pCriteria->getSubType() == FunctionalTypeVG::On)
				{
					pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				}
			}
			else if(maskedRr1 == RR1_ON)
			{
				if(recentStateOff)
				{
					if(prOnline < pCriteria->getLowerLimit())	// Became ON, even taking into account lower limit
					{
						if(pCriteria->getSubType() == FunctionalTypeVG::On)
						{
							pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
						}
						else if(pCriteria->getSubType() == FunctionalTypeVG::Off)
						{
							pCriteria->setMatch(pCriteria->isReverse(), false, 0);
						}
					}
					else	// Still OFF - taking into account lower limit
					{
						if(pCriteria->getSubType() == FunctionalTypeVG::Off)
						{
							pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
						}
						else if(pCriteria->getSubType() == FunctionalTypeVG::On)
						{
							pCriteria->setMatch(pCriteria->isReverse(), false, 0);
						}
					}
				}
				else
				{
					if(pCriteria->getSubType() == FunctionalTypeVG::On)
					{
						pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
					}
					else if(pCriteria->getSubType() == FunctionalTypeVG::Off)
					{
						pCriteria->setMatch(pCriteria->isReverse(), false, 0);
					}
				}
			}
			else	// State is neither ON nor OFF
			{
				if(pCriteria->getSubType() == FunctionalTypeVG::On)
				{
					pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				}
				else if(pCriteria->getSubType() == FunctionalTypeVG::Off)
				{
					pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				}
			}
			return;	// VERY IMPORTANT !!!
		}
		else	// 'old' style - based on RR1 only
		{
			switch(pCriteria->getSubType())
			{
			case FunctionalTypeVG::Off:
				if(maskedRr1 == RR1_OFF)
				{
					pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
					return;
				}
				else
				{
					pCriteria->setMatch(pCriteria->isReverse(), false, 0);
					return;
				}
				break;
			case FunctionalTypeVG::On:
				if(maskedRr1 == RR1_ON)
				{
					pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
					return;
				}
				else
				{
					pCriteria->setMatch(pCriteria->isReverse(), false, 0);
					return;
				}
				break;
			}
		}
		break;
	case EqpMsgCriteria::Error:
		if(rr1Online & RR1_ERROR)
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else
		{
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	case EqpMsgCriteria::Warning:
		if(rr1Online & (RR1_ERROR | RR1_WARNING))
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else
		{
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	case EqpMsgCriteria::MainValue:
		if(maskedRr1 == RR1_ON)
		{
			// It can be (and it was observed) that callback for PR=10000
			// arrives before callback for RR1 with state=OFF. Reverse
			// situation can also occur when switching ON.
			// To avoid sending wrong messages in such cases value 10000
			// is considered to be invalid in all cases and does not change
			// criteria matching
			if(prOnline > 9999)
			{
				return;
			}
			if(pCriteria->isReverse())
			{
				if(prOnline > pCriteria->getUpperLimit())
				{
					pCriteria->setMatch(false, true, prOnline);
					return;
				}
				else if(prOnline < pCriteria->getLowerLimit())
				{
					pCriteria->setMatch(true, true, prOnline);
					return;
				}
				// Between lower and upper limit - no change
				return;
			}
			else
			{
				if(prOnline > pCriteria->getUpperLimit())
				{
					pCriteria->setMatch(true, true, prOnline);
					return;
				}
				else if(prOnline < pCriteria->getLowerLimit())
				{
					pCriteria->setMatch(false, true, prOnline);
					return;
				}
				// Between lower and upper limit - no change
				return;
			}
		}
		break;
	default:
		break;
	}
	// L.Kopylov 30.10.2012 Keep matching unchanged pCriteria->setMatch(false, false, 0);
}

