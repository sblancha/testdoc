//	Implementation of VacIconVPG_6A01_2VVR_NO_VG class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPG_6A01_2VVR_NO_VG.h"
#include "VacIconVPG_6A01.h"

#include "EqpProcess.h"

#include "DataPool.h"

#include <qpainter.h>
#include <QMouseEvent>


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
VacIconVPG_6A01_2VVR_NO_VG::VacIconVPG_6A01_2VVR_NO_VG(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	pIconVPG = pIconVVR1 = pIconVVR2 = NULL;
	setFixedSize(55, 49);
	setAreaGeometry();
}

VacIconVPG_6A01_2VVR_NO_VG::~VacIconVPG_6A01_2VVR_NO_VG()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for symbol areas rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_NO_VG::setAreaGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		vvr1.setRect(0, 2, 27, 21);
		vvr2.setRect(28, 2, 27, 21);
		pump.setRect(14, 28, 21, 21);
		break;
	case VacIconContainer::Down:
		pump.setRect(14, 0, 21, 21);
		vvr1.setRect(28, 26, 27, 21);
		vvr2.setRect(0, 26, 27, 21);
		break;
	case VacIconContainer::Left:
		pump.setRect(0, 20, 21, 21);
		vvr1.setRect(26, 0, 21, 27);
		vvr2.setRect(26, 28, 21, 27);
		break;
	case VacIconContainer::Right:
		pump.setRect(28, 20, 21, 21);
		vvr1.setRect(2, 28, 21, 27);
		vvr2.setRect(2, 0, 21, 27);
		break;
	}
	if(pIconVVR1)
	{
		pIconVVR1->move(vvr1.left(), vvr1.top());
	}
	if(pIconVVR2)
	{
		pIconVVR2->move(vvr2.left(), vvr2.top());
	}
	if(pIconVPG)
	{
		pIconVPG->move(pump.left(), pump.top());
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPG_6A01_2VVR_NO_VG::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		return VacIconGeometry(55, 49, 27, 56, -1);	// 2rd value was 27
		break;
	case VacIconContainer::Down:
		return VacIconGeometry(55, 49, 22, 56, -1);	// 3rd value was 22
		break;
	default:
		break;
	}
	return VacIconGeometry(49, 55, 24, 56, -1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_NO_VG::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(55);
		size.setHeight(49);
		break;
	default:
		size.setWidth(49);
		size.setHeight(55);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreaGeometry();
	if(pIconVPG)
	{
		pIconVPG->setDirection(newDirection);
	}
	if(pIconVVR1)
	{
		pIconVVR1->setDirection(calculateVvrDirection());
	}
	if(pIconVVR2)
	{
		pIconVVR2->setDirection(calculateVvrDirection());
	}
}

VacIconContainer::Direction VacIconVPG_6A01_2VVR_NO_VG::calculateVvrDirection(void)
{
	VacIconContainer::Direction result = direction;
	/*
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		result = VacIconContainer::Left;
		break;
	default:
		result = VacIconContainer::Up;
		break;
	}
	*/
	return result;
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_NO_VG::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pIconVPG)
	{
		delete pIconVPG;
		pIconVPG = NULL;
	}
	if(pIconVVR1)
	{
		delete pIconVVR1;
		pIconVVR1 = NULL;
	}
	if(pIconVVR2)
	{
		delete pIconVVR2;
		pIconVVR2 = NULL;
	}
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpProcess"), "VacIconVPG_6A01_2VVR_NO_VG::setEqp", pEqp->getDpName());
		this->pEqp = (EqpProcess *)pEqp;
		pIconVPG = new VacIconVPG_6A01(this);
		pIconVPG->setEqp(pEqp);
		pIconVPG->move(pump.left(), pump.top());
		pIconVPG->setMode(mode);
		pIconVPG->setPipeColor(pipeColor);
		pIconVPG->setDirection(direction);
		if(connected)
		{
			pIconVPG->connect();
		}

		QString attrName = pEqp->getAttrValue("VVR1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVVR1 = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVVR1)
		{
			pIconVVR1->move(vvr1.left(), vvr1.top());
			pIconVVR1->setMode(mode);
			pIconVVR1->setPipeColor(pipeColor);
			pIconVVR1->setDirection(calculateVvrDirection());
			if(connected)
			{
				pIconVVR1->connect();
			}
		}

		attrName = pEqp->getAttrValue("VVR2");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVVR2 = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVVR2)
		{
			pIconVVR2->move(vvr2.left(), vvr2.top());
			pIconVVR2->setMode(mode);
			pIconVVR2->setPipeColor(pipeColor);
			pIconVVR2->setDirection(calculateVvrDirection());
			if(connected)
			{
				pIconVVR2->connect();
			}
		}
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_NO_VG::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	if(pIconVPG)
	{
		pIconVPG->setMode(newMode);
	}
	if(pIconVVR1)
	{
		pIconVVR1->setMode(newMode);
	}
	if(pIconVVR2)
	{
		pIconVVR2->setMode(newMode);
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set color used for pipe lines drawing
**
**	ARGUMENTS
**		newColor	- Color used for pipe drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_NO_VG::setPipeColor(const QColor &newColor)
{
	if(pIconVPG)
	{
		pIconVPG->setPipeColor(newColor);
	}
	if(pIconVVR1)
	{
		pIconVVR1->setPipeColor(newColor);
	}
	if(pIconVVR2)
	{
		pIconVVR2->setPipeColor(newColor);
	}
	VacIcon::setPipeColor(newColor);
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6A01_2VVR_NO_VG::connect(void)
{
	if(pIconVPG)
	{
		pIconVPG->connect();
	}
	if(pIconVVR1)
	{
		pIconVVR1->connect();
	}
	if(pIconVVR2)
	{
		pIconVVR2->connect();
	}
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6A01_2VVR_NO_VG::disconnect(void)
{
	if(pIconVPG)
	{
		pIconVPG->disconnect();
	}
	if(pIconVVR1)
	{
		pIconVVR1->disconnect();
	}
	if(pIconVVR2)
	{
		pIconVVR2->disconnect();
	}
	return false;
}

/*
**	FUNCTION
**		Override VacIcon's method in order to have different menus for different
**		parts of icon
**
**	ARGUMENTS
**		pEvent	- Pointer to mouse event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_NO_VG::mousePressEvent(QMouseEvent * /* pEvent */)
{
	// Do nothing
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPG_6A01_2VVR_NO_VG::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
//	painter.eraseRect(0, 0, width(), height());

	switch(direction)
	{
	case VacIconContainer::Up:
		// Line from VVR1 to outside
		painter.fillRect(9, 0, 3, vvr1.height() + 5, pipeColor);
		// Line from VVR2 to outside
		painter.fillRect(37, 0, 3, vvr2.height() + 5, pipeColor);
		// Line connecting 2 valves
		painter.fillRect(9, vvr1.bottom() + 2, vvr2.center().x() - vvr1.center().x() + 2, 3, pipeColor);
		// Connect pump to center of valve connection line
		painter.fillRect(pump.center().x() - 1, vvr1.bottom() + 1, 3, 6, pipeColor);
		break;
	case VacIconContainer::Down:
		// Line from VVR1 to outside
		painter.fillRect(9, vvr1.top() - 2, 3, vvr1.height() + 5, pipeColor);
		// Line from VVR2 to outside
		painter.fillRect(37, vvr2.top() - 2, 3, vvr2.height() + 5, pipeColor);
		// Line connecting 2 valves
		painter.fillRect(9, vvr1.top() - 4, vvr1.center().x() - vvr2.center().x() + 2, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, pump.bottom(), 3, 3, pipeColor);
		break;
	case VacIconContainer::Left:
		// Line from VVR1 to outside
		painter.fillRect(vvr1.left() - 4, 16, vvr1.width() + 5, 3, pipeColor);
		// Line from VVR2 to outside
		painter.fillRect(vvr2.left() - 4, 44, vvr2.width() + 5, 3, pipeColor);
		// Line connecting 2 valves
		painter.fillRect(vvr1.left() - 4, 16, 3, vvr2.center().y() - vvr1.center().y() + 2, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.right(), pump.center().y() - 1, 3, 3, pipeColor);
		break;
	case VacIconContainer::Right:
		// Line from VVR1 to outside
		painter.fillRect(0, 43, vvr1.width() + 5, 3, pipeColor);
		// Line from VVR2 to outside
		painter.fillRect(0, 15, vvr2.width() + 5, 3, pipeColor);
		// Line connecting 2 valves
		painter.fillRect(vvr1.right() + 2, 15, 3, vvr1.center().y() - vvr2.center().y() + 2, pipeColor);
		// Connect pump to valves
		painter.fillRect(pump.left() - 3, pump.center().y() - 1, 3, 3, pipeColor);
		break;
	}

	if(!pEqp)
	{
		painter.setPen(Qt::black);
		painter.drawRect(pump.left(), pump.top(), pump.width() - 1, pump.height() - 1);
		painter.drawRect(vvr1.left(), vvr1.top(), vvr1.width() - 1, vvr1.height() - 1);
		painter.drawRect(vvr2.left(), vvr2.top(), vvr2.width() - 1, vvr2.height() - 1);
	}
}
