//	Implementation of VacIconVP_TP class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVP_TP.h"
#include "VacMainView.h"
#include "EqpVP_TP.h"

#include <qpainter.h>

#include <QMouseEvent>


// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_NOMINAL_SPEED		0,255,0
#define COLOR_ON				127,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0
#define COLOR_WARNING			255,255,0
#define COLOR_UNDERRANGE		0,180,0

//	Main state bits to be analyzed
#define	RR1_VALID			(0x40000000)
#define RR1_NOMINAL_SPEED	(0x08000000)
#define	RR1_ON				(0x02000000)
#define	RR1_OFF				(0x01000000)
#define	RR2_OBJECTST_MASK	(0xFF00)
#define RR1_ERROR			(0x00800000)
#define	RR1_WARNING			(0x00400000)
#define RR1_ERROR_CODE_MASK	(0x000000FF)
#define RR1_WARNING_CODE_MASK	(0x0000FF00)

#define RR1_MANUAL			(0x04000000)
#define RR1_INTERLOCK		(0x00200000)
#define	RR1_ON_INTERLOCK	(0x00100000)
#define	RR1_AUTO_ON_ORDER	(0x00080000)
#define	RR1_MAN_ON_ORDER	(0x00040000)
#define RR1_FORCED			(0x00010000)

/////////////////////////////////////////////////////////////////////////////////
////////////////////	CONSTRUCTION/DESCTRUCTION
/////////////////////////////////////////////////////////////////////////////////
VacIconVP_TP::VacIconVP_TP(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 33);
}

VacIconVP_TP::~VacIconVP_TP()
{
	disconnect();
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////	FUNCTIONS
/////////////////////////////////////////////////////////////////////////////////
/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVP_TP::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(21, 33, 10, 35, -1); //(int width, int height, int x, int y, int verticalPos)
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVP_TP::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVP_TP"), "VacIconVP_TP::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVP_TP *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVP_TP::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVP_TP::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("ROTATION");
	dpes.append("RR1");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVP_TP::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVP_TP::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVP_TP::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0;
	float rotation = -999.0;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		rotation = pEqp->getROTATION(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);

	switch(direction)
	{
	case VacIconContainer::Up:
		painter.fillRect(10 - (pipeWidth >> 1), 0, pipeWidth, 9, pipeColor);
		break;
	case VacIconContainer::Down:
		painter.fillRect(10 - (pipeWidth >> 1), height() - 9, pipeWidth, 9, pipeColor);
		break;
	case VacIconContainer::Left:
		painter.fillRect(0, (height() >> 1) - (pipeWidth >> 1), 5, pipeWidth, pipeColor);
		break;
	case VacIconContainer::Right:
		painter.fillRect(width() - 4, (height() >> 1) - (pipeWidth >> 1), 5, pipeWidth, pipeColor);
		break;
	}


	painter.save();

	// Prepare painter's coordinate system for drawing
	// Coordinate system (0,0) is center of widget
	painter.translate(width() / 2, height() / 2);// x=0, y=0 is now the center of the widget

	// Coordinate system is ready - draw
	drawImage(painter, ctlStatus, rr1, rotation, plcAlarm);
	
	// Draw selection ON TOP of image
	painter.restore();	// Original coordinate system - see save() above x=0, y=0 is now is top-left of the widget

	drawSymbols(painter);

	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
}

void VacIconVP_TP::drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, float rotation, bool plcAlarm)
{
	QPen pen(pipeColor, pipeWidth);
	painter.setPen(pen);
	painter.drawLine(0, -10, 0, -7);

	// 1) Draw turbomoleculare symbol inside rectangle
	painter.setPen(Qt::black);
	painter.drawRect(-9,  -9, 18, 18);//(int x, int y, int width, int height) x=0, y=0 -> center; x axis goes to right, y axis goes to bottom
	
	drawAnimation(painter, ctlStatus, rr1, rotation, plcAlarm);
	
	painter.setPen(Qt::black);
	


	// inside symbol
	painter.drawArc(-8, -8, 17, 17, 0, 5760);
	painter.drawLine(-6, 0, 6, 0);
	painter.drawLine(-4, -2, -4, 2);
	painter.drawLine(0, -2, 0, 2);
	painter.drawLine(4, -2, 4, 2);
	painter.drawLine(0, -9, -2, -7);
	painter.drawLine(0, -9, 2, -7);

}


void VacIconVP_TP::drawAnimation(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, float rotation, bool plcAlarm)
{
	QColor	color(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr1 & RR1_ERROR)
				{
					color.setRgb(COLOR_ERROR);
				}
				else if(rr1 & RR1_WARNING)
				{
					color.setRgb(COLOR_WARNING);
				}
				else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))
				{
					color.setRgb(COLOR_ERROR);
				}
				//else if(rr1 & RR1_OFF)
				//{
				//	color.setRgb(COLOR_OFF);
				//}
				else if((rr1 & RR1_ON) && (rr1 & RR1_NOMINAL_SPEED))
				{
					color.setRgb(COLOR_NOMINAL_SPEED);
				}
				else if((rr1 & RR1_OFF)||(rr1 & RR1_ON))
				{
					color.setRgb(COLOR_OFF); // Background color is COLOR_OFF(white)
					// Rotation negative
					if (rotation < 0.0)
					{
						color.setRgb(COLOR_WARNING);
					}
					
					// Rotation < 5%
					else if (rotation < 5.0)
					{
						color.setRgb(COLOR_OFF);
					}
					// Rotation < 35%
					else if (rotation < 35.0)
					{
						painter.setBrush(QColor::fromRgb(COLOR_ON)); painter.setPen(QColor::fromRgb(COLOR_ON));
						painter.drawRect(-8,  4, 16, 4);//(int x, int y, int width, int height) x=0, y=0 -> middle ; x axis goes to right, y axis goes to bottom
					}
					// 35% < Rotation < 60% 
					else if (rotation < 60.0)
					{
						painter.setBrush(QColor::fromRgb(COLOR_ON)); painter.setPen(QColor::fromRgb(COLOR_ON));
						painter.drawRect(-8,  -1, 16, 9);//(int x, int y, int width, int height) x=0, y=0 -> middle ; x axis goes to right, y axis goes to bottom
					}
					// 60% < Rotation < 85% 
					else if (rotation < 85.0)
					{					
						painter.setBrush(QColor::fromRgb(COLOR_ON)); painter.setPen(QColor::fromRgb(COLOR_ON));
						painter.drawRect(-8,  -4, 16, 12);//(int x, int y, int width, int height) x=0, y=0 -> middle ; x axis goes to right, y axis goes to bottom
					}
					// Rotation > 85% 
					else
					{
						painter.setBrush(QColor::fromRgb(COLOR_ON)); painter.setPen(QColor::fromRgb(COLOR_ON));
						painter.drawRect(-8,  -6, 16, 14);//(int x, int y, int width, int height) x=0, y=0 -> middle ; x axis goes to right, y axis goes to bottom
					}
					break;
				}
				else
				{
					color.setRgb(COLOR_UNDEFINED);
				}

			}
		painter.fillRect(-8,  -8, 17, 17, color);// if eqp not RR1_ON only, fill the rect with the correct color
		}
	break;	
	}//end of switch
	painter.setBrush(color);
	painter.setPen(Qt::gray);
}


void VacIconVP_TP::drawSymbols(QPainter &painter)
{
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			return;
		default:
			if(pEqp->isPlcAlarm(mode))
			{
				return;	// Invalid
			}
			unsigned rr1 = pEqp->getRR1(mode);
			if(!(rr1 & RR1_VALID))
			{
				return;	// Invalid
			}

			if(rr1 & RR1_FORCED)
			{
				drawForced(painter);
			}
			else if(rr1 & RR1_MANUAL)
			{
				drawManual(painter);
			}

			if(rr1 & RR1_INTERLOCK)
			{
				drawFullInterlock(painter);
			}
			else if(rr1 & RR1_ON_INTERLOCK)
			{
				drawStartInterlock(painter);
			}

			if(rr1 & (RR1_MANUAL | RR1_FORCED))
			{
				if(rr1 & RR1_MAN_ON_ORDER)
				{
					drawCmdOn(painter);
				}
				else
				{
					drawCmdOff(painter);
				}
			}
			else
			{
				if(rr1 & RR1_AUTO_ON_ORDER)
				{
					drawCmdOn(painter);
				}
				else
				{
					drawCmdOff(painter);
				}
			}
			break;
		}
	}
	else
	{
		drawManual(painter);
		drawFullInterlock(painter);
		drawCmdOff(painter);
	}
}


void VacIconVP_TP::drawInterlockSrcArrow(QPainter &painter, bool before, bool after) //called externally 
{
	if(!(before || after))
	{
		return;
	}
	painter.resetTransform();
	painter.setPen(VacMainView::getSynPassiveLineColor());
	int start, end, pos;
	switch(direction)
	{
	case VacIconContainer::Up:
		start = x();
		end = start + width();
		pos = y() + height() + 5;
		painter.drawLine(start, pos, end, pos);
		if(before)
		{
			painter.drawLine(start, pos, start + 3, pos - 3);
			painter.drawLine(start, pos, start + 3, pos + 3);
		}
		if(after)
		{
			painter.drawLine(end, pos, end - 3, pos - 3);
			painter.drawLine(end, pos, end - 3, pos + 3);
		}
		break;
	case VacIconContainer::Down:	// 180 degrees
		start = x();
		end = start + width();
		pos = y() - 5;
		painter.drawLine(start, pos, end, pos);
		if(before)
		{
			painter.drawLine(start, pos, start + 3, pos - 3);
			painter.drawLine(start, pos, start + 3, pos + 3);
		}
		if(after)
		{
			painter.drawLine(end, pos, end - 3, pos - 3);
			painter.drawLine(end, pos, end - 3, pos + 3);
		}
		break;
	case VacIconContainer::Left:	// 90 degrees clockwise
		start = y();
		end = start + height();
		pos = x() + width() + 5;
		painter.drawLine(pos, start, pos, end);
		if(before)
		{
			painter.drawLine(pos, start, pos + 3, start + 3);
			painter.drawLine(pos, start, pos - 3, start + 3);
		}
		if(after)
		{
			painter.drawLine(pos, end, pos + 3, end - 3);
			painter.drawLine(pos, end, pos - 3, end - 3);
		}
		break;
	case VacIconContainer::Right:	// 90 degrees conuterclockwise
		start = y();
		end = start + height();
		pos = x() - 5;
		painter.drawLine(pos, start, pos, end);
		if(before)
		{
			painter.drawLine(pos, start, pos + 3, start + 3);
			painter.drawLine(pos, start, pos - 3, start + 3);
		}
		if(after)
		{
			painter.drawLine(pos, end, pos + 3, end - 3);
			painter.drawLine(pos, end, pos - 3, end - 3);
		}
		break;
	}
}


void VacIconVP_TP::drawForced(QPainter &painter)
{
	painter.setPen(Qt::red);
	painter.drawLine(1, 1, 1, 5);
	painter.drawLine(2, 1, 4, 1);
	painter.drawLine(2, 3, 3, 3);
}

void VacIconVP_TP::drawManual(QPainter &painter)
{
	painter.setPen(Qt::black);
	painter.drawLine(1, 1, 1, 5);
	painter.drawLine(5, 1, 5, 5);
	painter.drawLine(3, 3, 3, 4);
	painter.drawLine(2, 2, 2, 2);
	painter.drawLine(4, 2, 4, 2);
}

void VacIconVP_TP::drawFullInterlock(QPainter &painter)
{
	painter.setPen(Qt::red);
	// F
	painter.drawLine(1, 27, 1, 31);
	painter.drawLine(2, 27, 4, 27);
	painter.drawLine(2, 29, 3, 29);
	// I
	painter.drawLine(5, 27, 7, 27);
	painter.drawLine(6, 28, 6, 30);
	painter.drawLine(5, 31, 7, 31);
}

void VacIconVP_TP::drawStartInterlock(QPainter &painter)
{
	painter.setPen(Qt::darkYellow);
	// S
	painter.drawLine(1, 27, 3, 27);
	painter.drawLine(1, 27, 1, 28);
	painter.drawLine(2, 29, 3, 29);
	painter.drawLine(3, 30, 3, 31);
	painter.drawLine(1, 31, 2, 31);
	// I
	painter.drawLine(5, 27, 7, 27);
	painter.drawLine(6, 28, 6, 30);
	painter.drawLine(5, 31, 7, 31);
}

void VacIconVP_TP::drawCmdOff(QPainter &painter)
{
	QColor color = palette().color(backgroundRole());
	color.setAlpha(192);
	painter.fillRect(8, 0, 12, 7, color);
	painter.setPen(Qt::red);
	// O
	painter.drawLine(8, 2, 8, 4);
	painter.drawLine(9, 1, 10, 1);
	painter.drawLine(9, 5, 10, 5);
	painter.drawLine(11, 2, 11, 4);
	// F
	painter.drawLine(13, 1, 13, 5);
	painter.drawLine(14, 1, 15, 1);
	painter.drawLine(13, 3, 14, 3);
	// F
	painter.drawLine(17, 1, 17, 5);
	painter.drawLine(18, 1, 19, 1);
	painter.drawLine(17, 3, 18, 3);
}

void VacIconVP_TP::drawCmdOn(QPainter &painter)
{
	painter.setPen(Qt::darkGreen);
	// O
	painter.drawLine(11, 2, 11, 4);
	painter.drawLine(12, 1, 13, 1);
	painter.drawLine(12, 5, 13, 5);
	painter.drawLine(14, 2, 14, 4);
	// N
	painter.drawLine(16, 1, 16, 5);
	painter.drawLine(19, 1, 19, 5);
	painter.drawLine(16, 2, 19, 5);
}

