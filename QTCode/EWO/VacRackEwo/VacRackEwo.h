#ifndef	VACRACK_H
#define	VACRACK_H

#define RACK_WIDTH 0.483
#define RACK_HEIGHT_UNIT 0.0445
#define RACK_PIXEL_HEIGHT 675
#define RACK_PIXEL_WIDTH 200


#include <BaseExternWidget.hxx>
#include <QWidget>
#include <qStringList>
#include <QPen>
#include "VacMainViewEwo.h"
//	Icon for VGF

class VacRackEwo : public QWidget
{
	
	Q_OBJECT

	Q_PROPERTY( QPen pen READ getPen WRITE setPen DESIGNABLE false)
public:
	VacRackEwo(QWidget *parent);
	virtual ~VacRackEwo();

	const QPen &getPen() const { return pen; }
	void setPen(const QPen &p) { pen = p;  }
	void addEquip(QRect * rect, QString name, QString equipType) { equipment.append(rect); equipNames.append(name); equipTypes.append(equipType); }

protected:
	virtual void paintEvent(QPaintEvent *pEvent);


private:
	// TODO example of a string list property
	QStringList values;
	QPen pen;
	QList<QRect *> equipment;
	QList<QString> equipNames;
	QList<QString> equipTypes;
};


class EWO_EXPORT EwoInterface : public BaseExternWidget
{
	 Q_OBJECT

public:
	EwoInterface(QWidget *parent);

	virtual QWidget *widget() const;


	virtual QStringList methodList() const;
	public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private:
	VacRackEwo *baseWidget;
};



#endif