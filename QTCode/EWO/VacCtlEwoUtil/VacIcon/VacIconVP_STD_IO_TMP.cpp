//	Implementation of VacIconVP_STD_IO_TMP class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVP_STD_IO_TMP.h"

#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVP_STD_IO_TMP::VacIconVP_STD_IO_TMP(QWidget *parent, Qt::WindowFlags f) :
	VacIconVP_STD_IO(parent, f)
{
}

VacIconVP_STD_IO_TMP::~VacIconVP_STD_IO_TMP()
{
}

void VacIconVP_STD_IO_TMP::drawType(QPainter &painter)
{
	painter.setPen(Qt::black);
	painter.drawArc(-8, -8, 17, 17, 0, 5760);
	painter.drawLine(-6, 0, 6, 0);
	painter.drawLine(-4, -2, -4, 2);
	painter.drawLine(0, -2, 0, 2);
	painter.drawLine(4, -2, 4, 2);
	painter.drawLine(0, -9, -2, -7);
	painter.drawLine(0, -9, 2, -7);
}
