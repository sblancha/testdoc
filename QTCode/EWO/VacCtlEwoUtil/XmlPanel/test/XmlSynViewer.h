#ifndef	XMLSYNVIEWER_H
#define	XMLSYNVIEWER_H

#include "XmlSynoptic.h"

#include <QWidget>

class XmlSynViewer : public QWidget
{
public:
	XmlSynViewer(QWidget *parent, const QString &fileName);
	~XmlSynViewer();

protected:
	XmlSynoptic	*pSynoptic;

	virtual void paintEvent(QPaintEvent *pEvent);

};

#endif	// XMLSYNVIEWER_H
