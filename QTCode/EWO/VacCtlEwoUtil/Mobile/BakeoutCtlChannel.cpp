//	Implementation of BakeoutCtlChannel class
////////////////////////////////////////////////////////////////////////////////

#include "BakeoutCtlChannel.h"

#include "MobilePool.h"
#include "Eqp.h"
#include "ResourcePool.h"

// Color definitions. Every definition is arguments for QColor::setRgb()
#define	COLOR_OK			0,255,0
#define	COLOR_ERROR			255,0,0
#define	COLOR_WARNING		255,255,0
#define	COLOR_UNDEFINED		15,127,192
#define	COLOR_NOT_ACTIVE	0,127,127

/////////////////////////////////////////////////////////////////////////////////
////////////////// Construction/destruction

BakeoutCtlChannel::BakeoutCtlChannel(Eqp *pEqp, int channel)
{
	this->pEqp = pEqp;
	this->channel = channel;

	value = 0;
	status = 0;
	regChannel = 0;
	warningThreshold = 0;
	errorThreshold = 0;
	valueValid = true;

	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "CtlCH%02d.T", channel);
#else
	sprintf(buf, "CtlCH%02d.T", channel);
#endif
	tDpeName = buf;
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "CtlCH%02d.Status", channel);
#else
	sprintf(buf, "CtlCH%02d.Status", channel);
#endif
	statusDpeName = buf;
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "CtlCH%02d.IntlRegChannel", channel);
#else
	sprintf(buf, "CtlCH%02d.IntlRegChannel", channel);
#endif
	regChannelDpeName = buf;
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "CtlCH%02d.WarningThreshold", channel);
#else
	sprintf(buf, "CtlCH%02d.WarningThreshold", channel);
#endif
	warningThresholdDpeName = buf;
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "CtlCH%02d.ErrorThreshold", channel);
#else
	sprintf(buf, "CtlCH%02d.ErrorThreshold", channel);
#endif
	errorThresholdDpeName = buf;
	calculateColor(false);

	MobilePool::getInstance().connectCtlChannel(this, pEqp, channel);

	// Report last known values
	QDateTime timeStamp;
	value = MobilePool::getInstance().getDpeValue(pEqp, tDpeName.toLatin1(), timeStamp).toInt();
	status = MobilePool::getInstance().getDpeValue(pEqp, statusDpeName.toLatin1(), timeStamp).toInt();
	regChannel = MobilePool::getInstance().getDpeValue(pEqp, regChannelDpeName.toLatin1(), timeStamp).toInt();
	warningThreshold = MobilePool::getInstance().getDpeValue(pEqp, warningThresholdDpeName.toLatin1(), timeStamp).toInt();
	errorThreshold = MobilePool::getInstance().getDpeValue(pEqp, errorThresholdDpeName.toLatin1(), timeStamp).toInt();
	calculateColor(false);
	emit eqpDataChanged(this, true);
}

BakeoutCtlChannel::~BakeoutCtlChannel()
{
	MobilePool::getInstance().disconnectCtlChannel(this, pEqp, channel);
}

/*
**	FUNCTION
**		Slot activated when equipment data has been changed
**
**	PARAMETERS
**		pSrc	- Pointer to device - source of signal
**		dpeName	- Name of DPE whose value has been changed
**		source	- Type of source
**		dpeValue	- New DPE value
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void BakeoutCtlChannel::dpeChange(Eqp * /* pSrc */, const char *dpeName,
	DataEnum::Source source, const QVariant &dpeValue, DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	if(source != DataEnum::Own)
	{
		return;	// PLC alarm - processed differently
	}
	if(mode != DataEnum::Online)
	{
		return;
	}
/*
printf("BakeoutCtlChannel::dpeChange(): DPE <%s>\n", dpeName);
fflush(stdout);
*/
	int newValue;
	if(tDpeName == dpeName)
	{
/*
printf("BakeoutCtlChannel::dpeChange(): T DPE\n");
fflush(stdout);
*/
		newValue = dpeValue.toInt();
		if(value == newValue)
		{
			return;
		}
		value = newValue;
		emit eqpDataChanged(this, false);
	}
	else if(regChannelDpeName == dpeName)
	{
/*
printf("BakeoutCtlChannel::dpeChange(): IntlRegChannel DPE\n");
fflush(stdout);
*/
		int newValue = dpeValue.toInt();
		if(regChannel == newValue)
		{
			return;
		}
		regChannel = newValue;
		calculateColor(false);
		emit eqpDataChanged(this, false);
	}
	else if(statusDpeName == dpeName)
	{
/*
printf("BakeoutCtlChannel::dpeChange(): Status DPE\n");
fflush(stdout);
*/
		newValue = dpeValue.toInt();
		if(status == newValue)
		{
			return;
		}
		status = newValue;
		calculateColor(true);
	}
	else if(warningThresholdDpeName == dpeName)
	{
/*
printf("BakeoutCtlChannel::dpeChange(): WarningThreshold DPE\n");
fflush(stdout);
*/
		newValue = dpeValue.toInt();
		if(warningThreshold == newValue)
		{
			return;
		}
		warningThreshold = newValue;
		emit eqpDataChanged(this, false);
	}
	else if(errorThresholdDpeName == dpeName)
	{
/*
printf("BakeoutCtlChannel::dpeChange(): ErrorThreshold DPE\n");
fflush(stdout);
*/
		newValue = dpeValue.toInt();
		if(errorThreshold == newValue)
		{
			return;
		}
		errorThreshold = newValue;
		emit eqpDataChanged(this, false);
	}
}

void BakeoutCtlChannel::calculateColor(bool emitSignal)
{
	QColor newColor;
	if(!regChannel)
	{
		newColor.setRgb(COLOR_NOT_ACTIVE);
	}
	else
	{
		QString resourceName = "VBAKEOUT.CC.Status_";
		resourceName += QString::number(status & STATUS_MASK);
		resourceName += ".Kind";
		int kind;
		if(ResourcePool::getInstance().getIntValue(resourceName, kind) == ResourcePool::OK)
		{
			switch(kind)
			{
			case 0:	// OK
				newColor.setRgb(COLOR_OK);
				break;
			case 1:	// Warning
				newColor.setRgb(COLOR_WARNING);
				break;
			case 2:	// Error
				newColor.setRgb(COLOR_ERROR);
				break;
			default:	// Unknown
				newColor.setRgb(COLOR_UNDEFINED);
				break;
			}
		}
		else
		{
			newColor.setRgb(COLOR_UNDEFINED);
		}
	}
	color = newColor;
	if(emitSignal)
	{
		emit eqpDataChanged(this, true);
	}
}

const QString BakeoutCtlChannel::getStateString(void)
{
	char buf[512];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "CtlCH%02d\n",channel);
	strcat_s(buf, sizeof(buf) / sizeof(buf[0]), getShortStateString().toLatin1());
#else
	sprintf(buf, "CtlCH%02d\n",channel);
	strcat(buf, getShortStateString().toLatin1());
#endif
	char valueBuf[32];
#ifdef Q_OS_WIN
	sprintf_s(valueBuf, sizeof(valueBuf) / sizeof(valueBuf[0]), "\nRegChannel=%d\n", regChannel);
	strcat_s(buf, sizeof(buf) / sizeof(buf[0]), valueBuf);
	sprintf_s(valueBuf, sizeof(valueBuf) / sizeof(valueBuf[0]), "  WarningThreshold=%dC\n", warningThreshold);
	strcat_s(buf, sizeof(buf) / sizeof(buf[0]), valueBuf);
	sprintf_s(valueBuf, sizeof(valueBuf) / sizeof(valueBuf[0]), "  ErrorThreshold=%dC", errorThreshold);
	strcat_s(buf, sizeof(buf) / sizeof(buf[0]), valueBuf);
#else
	sprintf(valueBuf, "\nRegChannel=%d\n", regChannel);
	strcat(buf, valueBuf);
	sprintf(valueBuf, "  WarningThreshold=%dC\n", warningThreshold);
	strcat(buf, valueBuf);
	sprintf(valueBuf, "  ErrorThreshold=%dC", errorThreshold);
	strcat(buf, valueBuf);
#endif
	return QString(buf);
}

const QString BakeoutCtlChannel::getShortStateString(void)
{
	QString resourceName = "VBAKEOUT.CC.Status_";
	resourceName += QString::number(status & STATUS_MASK);
	QString result;
	if(ResourcePool::getInstance().getStringValue(resourceName, result) == ResourcePool::OK)
	{
		return QString(result);
	}
	return QString("Unknown status (%1)").arg(status & STATUS_MASK);
}
