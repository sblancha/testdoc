//	Implementation of SmsConfig class
/////////////////////////////////////////////////////////////////////////////////

#include "SmsConfig.h"

int SmsConfig::sortColumn = Owner;
Qt::SortOrder	SmsConfig::sortOrder = Qt::AscendingOrder;

//////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
//////////////////////////////////////////////////////////////////////////////////

SmsConfig::SmsConfig(const QString &configDpName)
	: dpName(configDpName), state("Inactive")
{
	id = 0;
	numMsg = 0;
	numEvents = 0;
	active = activated = initialized = deleted = connected = false;
	row = -1;
	color.setRgb(COLOR_INACTIVE);
}

SmsConfig::~SmsConfig()
{
}

bool SmsConfig::operator < (const SmsConfig &other) const
{
	bool result = false;
	switch(sortColumn)
	{
	case Id:
		result = id < other.id;
		break;
	case Owner:
		result = owner < other.owner;
		break;
	case Group:
		result = getNumChildren() < other.getNumChildren();
		break;
	case Config:
		result = visibleName < other.visibleName;
		break;
	case Error:
		result = error < other.error;
		break;
	case State:
		result = state < other.state;
		break;
	case Events:
		result = numEvents < other.numEvents;
		break;
	case Msg:
		result = numMsg < other.numMsg;
		break;
	}
	if(sortOrder == Qt::DescendingOrder)
	{
		result = ! result;
	}
	return result;
}

void SmsConfig::setFlags(bool activeFlag, bool activatedFlag, bool initializedFlag)
{
	if((activeFlag != active) || (activatedFlag != activated) || (initializedFlag != initialized))
	{
		active = activeFlag;
		activated = activatedFlag;
		initialized = initializedFlag;
		calculateState();
		if(!deleted)
		{
			emit changed(this);
		}
	}
}

void SmsConfig::setId(int value)
{
	if(id != value)
	{
		id = value;
		if(!deleted)
		{
			emit changed(this);
		}
	}
}

void SmsConfig::setOwner(const QString &name)
{
	if(owner != name)
	{
		owner = name;
		if(!deleted)
		{
			emit changed(this);
		}
	}
}

void SmsConfig::setVisibleName(const QString &name)
{
	if(visibleName != name)
	{
		visibleName = name;
		if(!deleted)
		{
			emit changed(this);
		}
	}
}

void SmsConfig::setError(const QString &msg)
{
	if(error != msg)
	{
		error = msg;
		if(!deleted)
		{
			emit changed(this);
		}
	}
}

void SmsConfig::setNumMsg(int num)
{
	int delta = num - numMsg;
	if(delta)
	{
		numMsg = num;
		if(!deleted)
		{
			emit numMsgChanged(this, delta);
		}
	}
}

void SmsConfig::setNumEvents(unsigned num)
{
	int delta = num - numEvents;
	if(delta)
	{
		numEvents = num;
		if(!deleted)
		{
			emit numEventsChanged(this, delta);
		}
	}
}

void SmsConfig::setActive(bool flag)
{
	if(active != flag)
	{
		active = flag;
		calculateState();
		if(!deleted)
		{
			emit changed(this);
		}
	}
}

void SmsConfig::setActivated(bool flag)
{
	if(activated != flag)
	{
		activated = flag;
		calculateState();
		if(!deleted)
		{
			emit changed(this);
		}
	}
}

void SmsConfig::setInitialized(bool flag)
{
	if(initialized != flag)
	{
		initialized = flag;
		calculateState();
		if(!deleted)
		{
			emit changed(this);
		}
	}
}

void SmsConfig::calculateState(void)
{
	if(!active)
	{
		state = "Inactive";
		color.setRgb(COLOR_INACTIVE);
	}
	else if(initialized)
	{
		state = "Running";
		color.setRgb(COLOR_RUNNING);
	}
	else
	{
		state = "Starting";
		color.setRgb(COLOR_STARTING);
	}
}
