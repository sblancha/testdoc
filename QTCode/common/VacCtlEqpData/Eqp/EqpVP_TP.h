#ifndef	EQPVP_TP_H
#define	EQPVP_TP_H

//	EqpVP_TP	- Turbo moleculare pump with profibus interface

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVP_TP : public Eqp
{
	Q_OBJECT

public:
	EqpVP_TP(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVP_TP(const EqpVP_TP &src) : Eqp(src) {}
#endif
	~EqpVP_TP();

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline float getROTATION(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rotationReplay : rotationOnline; }
	virtual int getErrorCode(DataEnum::DataMode mode);
	bool isStarting(unsigned rr1) const;	// Criteria is rather complex - use dedicated function
	inline bool isStarting(DataEnum::DataMode mode) const { return  mode == DataEnum::Replay ? isStarting(rr1Replay) : isStarting(rr1Online); }

#ifndef PVSS_SERVER_VERSION
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
#endif
	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE and ROTATION DPE for online mode
	unsigned	rr1Online;
	float		rotationOnline;
	// Last known value of 
	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR1 DPE and ROTATION DPE for replay mode
	unsigned	rr1Replay;
	float		rotationReplay;
	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, float rotation, bool plcAlarm, QString &string);
#endif
};

#endif	// EQPVP_TP_H
