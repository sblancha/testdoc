//	Implementation of MessageQueueItem class
/////////////////////////////////////////////////////////////////////////////////

#include "MessageQueueItem.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

MessageQueueItem::MessageQueueItem(const QString &cfgName, const QString &msgText) :
	configName(cfgName), text(msgText)
{
}

MessageQueueItem::MessageQueueItem(const QString &cfgName, const QString &msgText, const QString &msgBody) :
	configName(cfgName), text(msgText), body(msgBody)
{
}
