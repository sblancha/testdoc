#ifndef VACDPDATAEWO_H
#define	VACDPDATAEWO_H

//	EWO interface for VacDpData widget

#include <BaseExternWidget.hxx>

#include "VacDpData.h"

class EWO_EXPORT VacDpDataEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	VacDpDataEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void newDpeValue(const char *dpe, int source, const QVariant &value);
		
private:
	VacDpData	*baseWidget;
};


#endif	// VACDPDATAEWO_H
