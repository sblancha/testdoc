#ifndef	EQPVRPM_H
#define	EQPVRPM_H

//	VRPM - Power supply for solenoids (LHC)

#include "Eqp.h"


class VACCTLEQPDATA_EXPORT EqpVRPM : public Eqp
{
	Q_OBJECT

public:
	EqpVRPM(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVRPM(const EqpVRPM &src) : Eqp(src) {}
#endif
	~EqpVRPM();

	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getSlaves(QStringList &slaves);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual bool convertValue(const QVariant &value, float &result, EqpPart part = EqpPartNone);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::CurrentVRPM; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode mode);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
	virtual void getPartColor(int part, QColor &color, DataEnum::DataMode mode);
	virtual void getPartStateString(int part, QString &string, DataEnum::DataMode mode);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
//	virtual void getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end);
//	virtual float getGrowFactorForDevList(void);
#endif

//	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	}
	inline unsigned getRR1(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? rr1Replay : rr1Online;
	}
	inline unsigned getRR2(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? rr2Replay : rr2Online;
	}
	inline int getRmA(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? rmAReplay : rmAOnline;
	}
	inline int getRmV(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? rmVReplay : rmVOnline;
	}

	virtual int getErrorCode(DataEnum::DataMode mode);

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR2 DPE for online mode
	unsigned	rr2Online;

	// Last known value of RmA DPE for online mode
	int	rmAOnline;

	// Last known value of RmV DPE for online mode
	int	rmVOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for replay mode
	unsigned	rr2Replay;

	// Last known value of RmA DPE for replay mode
	int	rmAReplay;

	// Last known value of RmV DPE for replay mode
	int	rmVReplay;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned rr2, int rmA, int rmV, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, unsigned rr2, int rmA, int rmV, bool plcAlarm, QString &string);
	virtual void getMainValueString(unsigned rr1, unsigned rr2, int rmA, int rmV, bool plcAlarm, QString &string);
	virtual void getPartColorCB(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartStateStringCB(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
#endif

};

#endif	// EQPVRPM_H
