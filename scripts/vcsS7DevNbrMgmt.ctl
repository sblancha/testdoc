/** @defgroup gr_vcsS7DevNbrMgmt vcsS7DevNbrMgmt.ctl
Regroup S7 connection device number per Driver. \n
https://its.cern.ch/jira/browse/VACCO-3454 \n
@section igr_vcsS7DevNbrMgmt Introduction
First developed as an independant script, to be run once after LHC update (more than 256 S7 PLC Connections). \n
Then it will be integrated in the import process (end of the process) after checking that S7 PLC Connections 
exceed 256. \n
@section rgr_vcsS7DevNbrMgmt Requirement
The WCCOA project config shall be updated with the below statement: \n
```
[s7_4]
...
deviceOffset = <S7_4FirstDevNbr>
```
@section mgr_vcsS7DevNbrMgmt Implementation
The internal datapoints of wccoa that manage the S7 connections are:
| DP Type     | DP Name       | dpe Name        | dpe type     | Action
| :--         | :--           | :--             | :--          | :--
| _S7_Config  | _S7_Config    | IPAddress       | dyn_string   | Re-order
|             |               | Rack            | dyn_unsigned | Re-order
|             |               | Slot            | dyn_unsigned | Re-order
|             |               | Timeout         | dyn_unsigned | Re-order
|             |               | ConnectionType  | dyn_unsigned | Re-order
| _S7_Conn    | \<plcS7Name1\>| DevNr           | unsigned     | Change number
|             |               | DrvNumber       | int          | Read to check with driver nbr in DB
| _S7_Conn    | \<plcS7Name2\>| DevNr           | unsigned     | Change number
|             |               | DrvNumber       | int          | Read to check with driver nbr in DB
| ...         | ...           | ...             | ...          | ...
|             |               | ...             | ...          | ...


*/
#uses "VacCtlEqpDataPvss"	    // C++ CTRL Library
#uses "VacCtlImportPvss"	    // C++ CTRL Library
#uses "vclGeneric.ctl"         // WCCOA CTRL Library
#uses "vclDeviceFileParser.ctl"// WCCOA CTRL Library
#uses "vclDevCommon.ctl"	    // WCCOA CTRL Library
#uses "vclAddressConfig.ctl"   // WCCOA CTRL Library

/// @brief Const
const string S7_CONFIG_DP_NAME = "V_TCP";
/// @brief Code to send to FE through WR1 to notify eqp is activated in SCADA 
const unsigned SCADA_ACTIVATED_WR1 = 0x0400u;
/// @brief Global to calculate order ID -- NOT USED
int G_orderId = 0;

/**
@brief Main
@copydoc gr_vcsS7DevNbrMgmt
*/                              
main() {
  dyn_string		exceptionInfo;
  dyn_errClass	   err;
  string          errMessage; 
  int             ret; //return value of function (0 is OK)
  
  // From _S7_Config  
  dyn_string	ipAddresses, newIpAddresses;  
  dyn_int      racks, newRacks;  
  dyn_int      slots, newSlots;
  dyn_int      timeouts, newTimeouts; 
  dyn_int      connectionTypes, newConnectionTypes; 
  
  dyn_string   connectionDps;
  // From _S7_Conn
  dyn_int      devNbrs, newDevNbrs;
  dyn_int      driversWccoa; // not changed  
  
  dyn_anytype  driversFromDb;  
  
  int		connIdx, n, S7_4FirstDevNbr;

  dyn_bool	parsed;  
  
  DebugTN(
     "vcsS7DevNbrMgmt.ctl: --- Regroup S7 connect per Driver Script Starting V2020-04-07_1");
  
  /* Not needed
  // Init DLL/SO data pools
  ret = InitDynLibStaticDataPools();
  if(ret != 0) {
      errMessage = 
         "vcsS7DevNbrMgmt.ctl: +++ FATAL: InitDynLibStaticDataPools() failed: script stopped";
      err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return;
  }
  */
  // Parse IMPORT file
  DebugTN("vcsS7DevNbrMgmt.ctl: --- Parse import file");
  //DebugTN("vcsS7DevNbrMgmt.ctl: --- glAccelerator: ", glAccelerator);
  //DebugTN("vcsS7DevNbrMgmt.ctl: --- PROJ_PATH: ", PROJ_PATH);
  ret = VacImportParseFile( PROJ_PATH + "data/" + glAccelerator + ".for_Import", glAccelerator, false, exceptionInfo );
  //fwExceptionHandling_display( exceptionInfo );
  if( dynlen(exceptionInfo) > 0 ) {
    errMessage = "vcsS7DevNbrMgmt.ctl: +++ FATAL: Parse import file failed";
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    delay(5); // time to throwError
    return;
  }
  DebugTN("vcsS7DevNbrMgmt.ctl: --- Parse import file successfull");
  
  // --
  // Read S7_Config information
  dpGet( S7_PLC_INT_DPTYPE_CONF + ".IPAddress:_original.._value",      ipAddresses,
		   S7_PLC_INT_DPTYPE_CONF + ".Rack:_original.._value",           racks,
		   S7_PLC_INT_DPTYPE_CONF + ".Slot:_original.._value",           slots,
         S7_PLC_INT_DPTYPE_CONF + ".Timeout:_original.._value",        timeouts,
		   S7_PLC_INT_DPTYPE_CONF + ".ConnectionType:_original.._value", connectionTypes );
  err = getLastError();
  if(dynlen(err) > 0) {
    errMessage = "vcsS7DevNbrMgmt.ctl: +++++ FATAL: dpGet() " + S7_PLC_INT_DPTYPE_CONF + 
                 " failed";
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return;
  }
  //DebugTN("vcsS7DevNbrMgmt.ctl: --- ipAddresses:", ipAddresses);  
  // --
  // Read Connection DP names
  connectionDps = dpNames( "*", S7_PLC_INT_DPTYPE_CONN );
  err = getLastError();
  if( (dynlen(err) > 0) || (dynlen(connectionDps) == 0) ) {
    errMessage = "vcsS7DevNbrMgmt.ctl: +++++ FATAL: Get " + S7_PLC_INT_DPTYPE_CONN + " failed";
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return;
  }
  // Prepare dyn variables
  for( n = dynlen(connectionDps) ; n > 0 ; n-- ) {
    dynAppend(devNbrs, 0);
    dynAppend(newDevNbrs, 0);
    dynAppend(driversWccoa, 0);
    dynAppend(driversFromDb, 0);
  }
  // Read device numbers wccoa drivers
  for( n = dynlen(connectionDps) ; n > 0 ; n-- ) {
    dpGet( connectionDps[n] + ".DevNr:_original.._value", devNbrs[n],
           connectionDps[n] + ".DrvNumber:_original.._value", driversWccoa[n] );
    err = getLastError();
    if(dynlen(err) > 0) {
      errMessage = "vcsS7DevNbrMgmt.ctl: +++++ FATAL: dpGet() " + connectionDps[n] 
                   + ".DevNr:_original.._value failed";
      err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return;
    }
  }
  DebugTN("vcsS7DevNbrMgmt.ctl: --- devNbrs:", devNbrs); 
  // Read driver numbers 
  VacImportGetDriversS7Connection(drivers, exceptionInfo);
  if(dynlen(exceptionInfo) > 0) {
    errMessage = 
       "vcsS7DevNbrMgmt.ctl: +++FATAL: VacImportGetDriversS7Connection(() failed: " 
       + exceptionInfo;
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return;
  } 
  if(dynlen(drivers) < 1) {
    errMessage = 
       "vcsS7DevNbrMgmt.ctl: +++FATAL: Drivers number not found in import file" 
       + exceptionInfo;
    err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return;
  }   
  DebugTN("vcsS7DevNbrMgmt.ctl: --- drivers:", drivers); 
  for( n = dynlen(drivers) ; n > 0 ; n-- ) {
    DebugTN(" Drivers of S7 Connection " + n + " is: ", drivers[n]);		
  }
  
  
  
  
  
  
  
  DebugTN(
     "vcsS7DevNbrMgmt.ctl: --- End of script Regroup S7 connect per Driver Script");
}
