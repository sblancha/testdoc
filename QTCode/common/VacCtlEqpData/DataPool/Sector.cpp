#include "Sector.h"
#include "DataPool.h"
#include "MainPartMap.h"

#include "PlatformDef.h"
#include "DebugCtl.h"

#include "BeamLinePart.h"
#include "BeamLine.h"

#include "Eqp.h"

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

DataPool	*Sector::pDataPool = NULL;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Sector::Sector(const char *name)
{
	this->name = pDataPool->addMultiUseString(name);
	/*
	this->name = NULL;
	if(name)
	{
		if(*name)
		{
			this->name = (char *)malloc(strlen(name) + 1);
			strcpy(this->name, name);
		}
	}
	*/
	pvssName = NULL;
	vacType = VacType::decodeName(name);
	outer = false;
	/*
	specSynoptic = false;
	*/
	start = end = 0.0;
	beamOrder = 0;
	selected = false; // [VACCO-929]
	state = SECTOR_OPERATIONAL; // [VACCO-948] [VACCO-1645]
#ifndef PVSS_SERVER_VERSION
	pRedStartEqp = pRedEndEqp = pBlueStartEqp = pBlueEndEqp = pCrossStartEqp = pCrossEndEqp = NULL;
	onRedBeam = onBlueBeam = onCrossBeam = redOut = false;
#endif
}

Sector::~Sector()
{
	/*
	if(name)
	{
		free((void *)name);
	}
	*/
	while(!mpMaps.isEmpty())
	{
		delete mpMaps.takeFirst();
	}
#ifndef PVSS_SERVER_VERSION
	while(!drawParts.isEmpty())
	{
		delete drawParts.takeFirst();
	}
#endif
}

/*
**
** FUNCTION
**		Create new Sector instance from data read in file.
**
** PARAMETERS
**		pFile		- Pointer to file being read.
**		lineNo		- Line number in file.
**		tokens		- Pointer to array of tokens in file.
**		nTokens		- Number of tokens.
**		errList		- Variable where error message shall be added
**						in case of errors.
**
** RETURNS
**		Pointer to new Sector instance created from data in file;
**		NULL in case of error.
**
** CAUTIONS
**		It is responsability of caller to destroy returned instance using 'delete'
**		operator.
**
** CAUTIONS
**		None
*/
Sector *Sector::createFromFile(FILE *pFile, int &lineNo,
	char **tokens, int nTokens, QStringList &errList)
{
	if(nTokens != 2)
	{
		errList.append(QString("Line %1: bad format for <%2>").arg(lineNo).arg(tokens[0]));
		return NULL;
	}
	Sector	*pNew = new Sector(tokens[1]);
	switch(pNew->parseFile(pFile, lineNo, errList))
	{
	case 0:
		return pNew;
	case 1:
		errList.append(QString("Line %2: incomplete sector <%2>").arg(lineNo).arg(pNew->name));
		break;
	}
	delete pNew;
	return NULL;
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file.
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
** RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
** CAUTIONS
**		None
*/
int Sector::processLineTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	const QString & /* longValue */, QStringList &errList)
{
	// PVSS name
	if(!strcasecmp(tokens[0], "PvssName"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Sector::processLineTokens" );
		if(pvssName)
		{
			PARSING_ERR2("Line %d: multiple PVSS names for <%s>", lineNo, name );
		}
		pvssName = pDataPool->addString(tokens[1]);
		return 1;
	}
	// Flag for outer sector
	else if(!strcasecmp(tokens[0], "OUTER"))
	{
		outer = true;
		return 1;
	}
	// MainPart
	else if(!strcasecmp(tokens[0], "MainPart"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Sector::processLineTokens");
		MainPart *pMainPart = pDataPool->findMainPartData(tokens[1]);
		if(!pMainPart)
		{
			PARSING_ERR2("Line %d: unknown main part <%s>", lineNo, tokens[1]);
		}
		if(isInMainPart(pMainPart))
		{
			PARSING_ERR3("Line %d: duplicating main part <%s> for <%s>", lineNo, tokens[1], name);
		}
		mpMaps.append(new MainPartMap(pMainPart, NULL));
		pMainPart->setVacTypeMask(vacType);
		return 1;
	}
	// Domain
	else if(!strcasecmp(tokens[0], "Domain"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Sector::processLineTokens" );
		if(!mpMaps.count())
		{
			PARSING_ERR2("Line %d: <%s> must be after <MainPart>", lineNo, tokens[0]);
		}
		MainPartMap *pMap = mpMaps.last();
		if(!pMap->getDomain().isEmpty())
		{
			PARSING_ERR("Line %d: too many domains per main part mapping", lineNo);
		}
		QString pDomain = pDataPool->findDomain(tokens[1], true);
		pMap->setDomain(pDomain);
		return 1;
	}
	// Start location
	else if(!strcasecmp(tokens[0], "Start"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Sector::processLineTokens");
		if(sscanf(tokens[1], "%f", &start) != 1)
		{
			PARSING_ERR2("Line %d: bad start location format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	// End location
	else if(!strcasecmp(tokens[0], "End"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Sector::processLineTokens");
		if(sscanf(tokens[1], "%f", &end) != 1)
		{
			PARSING_ERR2("Line %d: bad end location format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	// End of main part mapping
	else if(!strcasecmp(tokens[0], "EndMainPart"))
	{
		CHECK_TOKEN_COUNT(nTokens, 1, lineNo, "Sector::processLineTokens");
		if(!mpMaps.count())
		{
			PARSING_ERR2("Line %d: <%s> must be after <MainPart>", lineNo, tokens[0]);
		}
		MainPartMap *pMap = mpMaps.last();
		if(pMap->getDomain().isEmpty())
		{
			PARSING_ERR2("Line %d: no domain mapping for main part <%s>", lineNo,
				pMap->getMainPart()->getName());
		}
		return 1;
	}
	// EndSector
	else if(!strcasecmp(tokens[0], "EndSector"))
	{
		if(!mpMaps.count())
		{
			PARSING_ERR2("Line %d: no main part mapping for <%s>", lineNo, name);
		}
		for(int mapIdx = 0 ; mapIdx < mpMaps.count() ; mapIdx++)
		{
			MainPartMap *pMap = mpMaps.at(mapIdx);
			if(pMap->getDomain().isEmpty())
			{
				PARSING_ERR3("Line %d: no PVSS domain for <%s> + <%s>", lineNo, name,
					pMap->getMainPart()->getName());
			}
		}
		return 0;
	}
	PARSING_ERR2("Line %d: unexpected <%s>", lineNo, tokens[0]);
}

/*
**
**	FUNCTION
**		Create new Sector instance = copy of this instance
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Pointer to new Sector instance.
**
**	CAUTIONS
**		It is responsability of caller to destroy returned instance using 'delete'
**		operator.
**		Note that drawing parts are NOT copied to new instance because this
**		method is intended for use BEFORE drawing parts are built.
**
**	CAUTIONS
**		None
*/
Sector *Sector::clone(void)
{
	Sector	*pNew = new Sector(name);

	pNew->pvssName = pvssName;
	pNew->start = start;
	pNew->end = end;
	pNew->vacType = vacType;
	pNew->outer = outer;
	pNew->beamOrder = beamOrder;
	pNew->selected = selected; // [VACCO - 929]
	pNew->state = state; // [VACCO-948] [VACCO-1645]
	int idx;
	for(idx = 0 ; idx < mpMaps.count() ; idx++)
	{
		pNew->mpMaps.append(mpMaps.at(idx)->clone());
	}
	for(idx = 0 ; idx < eqpList.count() ; idx++)
	{
		pNew->eqpList.append(eqpList.at(idx));
	}
	return pNew;
}

/*
**
**	FUNCTION
**		Set parameters for start of sector on red beam
**
**	PARAMETERS
**		pEqp	- Pointer to device at start of sector
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void Sector::setRedStartEqp(Eqp *pEqp)
{
	pRedStartEqp = pEqp;
	onRedBeam = true;
	redStart = pEqp->getStart();
}
#endif

/*
**
**	FUNCTION
**		Set parameters for end of sector on red beam
**
**	PARAMETERS
**		pEqp	- Pointer to device at end of sector
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void Sector::setRedEndEqp(Eqp *pEqp)
{
	pRedEndEqp = pEqp;
	onRedBeam = true;
	redEnd = pEqp->getStart();
}
#endif

/*
**
**	FUNCTION
**		Set parameters for start of sector on blue beam
**
**	PARAMETERS
**		pEqp	- Pointer to device at start of sector
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void Sector::setBlueStartEqp(Eqp *pEqp)
{
	pBlueStartEqp = pEqp;
	onBlueBeam = true;
	blueStart = pEqp->getStart();
}
#endif

/*
**
**	FUNCTION
**		Set parameters for end of sector on blue beam
**
**	PARAMETERS
**		pEqp	- Pointer to device at end of sector
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void Sector::setBlueEndEqp(Eqp *pEqp)
{
	pBlueEndEqp = pEqp;
	onBlueBeam = true;
	blueEnd = pEqp->getStart();
}
#endif

/*
**
**	FUNCTION
**		Set parameters for start of sector on cross beam
**
**	PARAMETERS
**		pEqp	- Pointer to device at start of sector
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void Sector::setCrossStartEqp(Eqp *pEqp)
{
	pCrossStartEqp = pEqp;
	onCrossBeam = true;
	crossStart = pEqp->getStart();
}
#endif

/*
**
**	FUNCTION
**		Set parameters for end of sector on cross beam
**
**	PARAMETERS
**		pEqp	- Pointer to device at end of sector
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void Sector::setCrossEndEqp(Eqp *pEqp)
{
	pCrossEndEqp = pEqp;
	onCrossBeam = true;
	crossEnd = pEqp->getStart();
}
#endif

/*
**
**	FUNCTION
**		If sector has at least one part of LHC ring vacuum:
**		1) Assign redOut flag if at least one part is located on either RED or BLUE beam
**			(the flag is not important for parts located on CROSS/COMMON beams)
**		2) Assign missing start or end coordinates. They can be missing if, for example,
**			part of sector on RED beam starts at valve and continues till end of RED beam
**			( i.e. when RED turns to CROSS)
**		Addition 19.10.2006 by LIK: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
**		Start/end coordinate can also be missing if sector borders data is not consistent,
**		for example, if valve at the start/end of sector is missing in database. In this
**		case corresponding coordinate shall be taken from closests valve in corresponding
**		direction
**
**	PARAMETERS
**		pLine	- Pointer to circular LHC line
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void Sector::finishBeamBorders(BeamLine *pLine)
{
	if(onRedBeam || onBlueBeam)	// Assign redOut flag
	{
		float	coord;
		if(onRedBeam)
		{
			coord = pRedStartEqp ? redStart : redEnd;
		}
		else
		{
			coord = pBlueStartEqp ? blueStart : blueEnd;
		}
		LhcRegion *pRegion = pDataPool->regionAtLocation(coord);
		if(pRegion->getType() == LhcRegion::RedOut)
		{
			redOut = true;
		}
	}
	bool debug = false;
	if(DebugCtl::isData())
	{
		if(!strcmp(name, "A4R2.C"))
		{
			debug = true;
#ifdef Q_OS_WIN
			qDebug("Finishing sector A4R2.C onRedBeam %d onBlueBeam %d onCrossBeam %d\n", onRedBeam, onBlueBeam, onCrossBeam);
#else
			printf("Finishing sector A4R2.C onRedBeam %d onBlueBeam %d onCrossBeam %d\n", onRedBeam, onBlueBeam, onCrossBeam);
			fflush(stdout);
#endif
		}
	}

	// Assign missing start/end coordinates
	LhcRegion *pRegion;
	if(onRedBeam)	// On RED beam
	{
		if(!pRedStartEqp)	// No start coordinate
		{
			pRegion = pDataPool->regionAtLocation(redEnd);
			if(pRedEndEqp && pLine)	// Try to find previous sector border within found region
			{
				pLine->findNeighbourSectorBorder(pRedEndEqp, pRegion, VacType::RedBeam,
					-1, &pRedStartEqp, redStart);
			}
			else
			{
				redStart = pRegion->getStart();
			}
		}
		if(!pRedEndEqp)	// No end coordinate
		{
			pRegion = pDataPool->regionAtLocation(redStart);
			if(pRedStartEqp && pLine)	// Try to find next sector border within found region
			{
				pLine->findNeighbourSectorBorder(pRedStartEqp, pRegion, VacType::RedBeam,
					1, &pRedEndEqp, redEnd);
			}
			else
			{
				redEnd = pRegion->getEnd();
			}
		}
	}
	if(onBlueBeam)	// On BLUE beam
	{
		if(!pBlueStartEqp)	// No start coordinate
		{
			pRegion = pDataPool->regionAtLocation(blueEnd);
			if(pBlueEndEqp && pLine)	// Try to find previous sector border within found region
			{
				pLine->findNeighbourSectorBorder(pBlueEndEqp, pRegion, VacType::BlueBeam,
					-1, &pBlueStartEqp, blueStart);
			}
			else
			{
				blueStart = pRegion->getStart();
			}
		}
		if(!pBlueEndEqp)	// No end coordinate
		{
			pRegion = pDataPool->regionAtLocation(blueStart);
			if(pBlueStartEqp && pLine)	// Try to find next sector border within found region
			{
				pLine->findNeighbourSectorBorder(pBlueStartEqp, pRegion, VacType::BlueBeam,
					1, &pBlueEndEqp, blueEnd);
			}
			else
			{
				blueEnd = pRegion->getEnd();
			}
		}
	}
	if(onCrossBeam)	// On CROSS beam
	{
		if(debug)
		{
#ifdef Q_OS_WIN
			qDebug("Initial: pCrossStartEqp %s pCrossEndEqp %s crossStart %f crossEnd %f\n",
				(pCrossStartEqp ? pCrossStartEqp->getName() : "null"),
				(pCrossEndEqp ? pCrossEndEqp->getName() : "null"),
				crossStart, crossEnd);
#else
			printf("Initial: pCrossStartEqp %s pCrossEndEqp %s crossStart %f crossEnd %f\n",
				(pCrossStartEqp ? pCrossStartEqp->getName() : "null"),
				(pCrossEndEqp ? pCrossEndEqp->getName() : "null"),
				crossStart, crossEnd);
			fflush(stdout);
#endif
		}
		if(!pCrossStartEqp)	// No start coordinate
		{
			pRegion = pDataPool->regionAtLocation(crossEnd);
			if(pCrossEndEqp && pLine)	// Try to find previous sector border within found region
			{
				pLine->findNeighbourSectorBorder(pCrossEndEqp, pRegion, VacType::CrossBeam,
					-1, &pCrossStartEqp, crossStart);
			}
			else
			{
				crossStart = pRegion->getStart();
			}
		}
		if(!pCrossEndEqp)	// No end coordinate
		{
			pRegion = pDataPool->regionAtLocation(crossStart);
			if(debug)
			{
#ifdef Q_OS_WIN
				qDebug("found region from %f to %f pLine %s\n", pRegion->getStart(), pRegion->getEnd(), (pLine ? pLine->getName() : "null"));
#else
				printf("found region from %f to %f pLine %s\n", pRegion->getStart(), pRegion->getEnd(), (pLine ? pLine->getName() : "null"));
				fflush(stdout);
#endif
			}
			if(pCrossStartEqp && pLine)	// Try to find next sector border within found region
			{
				pLine->findNeighbourSectorBorder(pCrossStartEqp, pRegion, VacType::CrossBeam,
					1, &pCrossEndEqp, crossEnd);
			}
			else
			{
				crossEnd = pRegion->getEnd();
			}
			if(debug)
			{
#ifdef Q_OS_WIN
				qDebug("RESULT: pCrossStartEqp %s pCrossEndEqp %s crossStart %f crossEnd %f\n",
					(pCrossStartEqp ? pCrossStartEqp->getName() : "null"),
					(pCrossEndEqp ? pCrossEndEqp->getName() : "null"),
					crossStart, crossEnd);
#else
				printf("RESULT: pCrossStartEqp %s pCrossEndEqp %s crossStart %f crossEnd %f\n",
					(pCrossStartEqp ? pCrossStartEqp->getName() : "null"),
					(pCrossEndEqp ? pCrossEndEqp->getName() : "null"),
					crossStart, crossEnd);
				fflush(stdout);
#endif
			}
		}
	}
}
#endif

/*
**
**	FUNCTION
**		Check if this sector belongs to given main part
**
**	PARAMETERS
**		pMainPart	- Pointer to main part
**
**	RETURNS
**		true if sector belongs to common main part;
**		false otherwise.
**
**	CAUTIONS
**		None
*/
bool Sector::isInMainPart(MainPart *pMainPart)
{
	for(int idx = 0 ; idx < mpMaps.count() ; idx++)
	{
		MainPartMap *pMap = mpMaps.at(idx);
		if(pMap->getMainPart() == pMainPart)
		{
			return true;
		}
	}
	return false;
}

/*
**
**	FUNCTION
**		Get domain name for this sector in given main part
**
**	PARAMETERS
**		pMainPart	- Pointer to main part
**
**	RETURNS
**		pointer to domain name;
**		NULL if sector is not in given main part
**
**	CAUTIONS
**		None
*/
const QString Sector::getDomainInMainPart(MainPart *pMainPart)
{
	for(int idx = 0 ; idx < mpMaps.count() ; idx++)
	{
		MainPartMap *pMap = mpMaps.at(idx);
		if(pMap->getMainPart() == pMainPart)
		{
			return pMap->getDomain();
		}
	}
	return QString();
}

/*
**
**	FUNCTION
**		Check if this sector and other sector have reference to common main part
**
**	PARAMETERS
**		pOtherSect	- Pointer to other sector, or NULL for dummy sector
**
**	RETURNS
**		true if two sectors have common main part;
**		false otherwise.
**
**	CAUTIONS
**		None
*/
bool Sector::hasCommonMainPart(Sector *pOtherSect)
{
	// If other sector is dummy sector - return true
	if(!pOtherSect)
	{
		return true;
	}
	if(mpMaps.isEmpty())
	{
		return true;
	}
	// Try to find common main part
	for(int mapIdx = 0 ; mapIdx < mpMaps.count() ; mapIdx++)
	{
		for(int otherMapIdx = 0 ; otherMapIdx < pOtherSect->mpMaps.count() ; otherMapIdx++)
		{
			if(mpMaps.at(mapIdx)->getMainPart() == pOtherSect->mpMaps.at(otherMapIdx)->getMainPart())
			{
				return true;
			}
		}
	}
	return false;
}

/*
**
**	FUNCTION
**		Return any (first) main part of this sector
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Pointer to first main part this sector belongs to
**
**	CAUTIONS
**		None
*/
MainPart *Sector::getFirstMainPart(void)
{
	if (!mpMaps.isEmpty()) {
		MainPartMap *pMap = mpMaps.first();
		return pMap->getMainPart();
	}
	return NULL;
}


/*
**
**	FUNCTION
**		Add new draw part to array of draw parts in this sector.
**
**	PARAMETERS
**		lineIdx			- Beam line index for new draw part
**		startPartIdx	- Index of start beam line part
**		startEqpIdx		- Index of start equipment in start beam line part
**		endPartIdx		- Index of end beam line part
**		endEqpIdx		- Index of last equipment in end beam line part
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void Sector::addDrawPart(BeamLine *pLine, BeamLinePart *pStartPart, int startEqpIdx,
	BeamLinePart *pEndPart, int endEqpIdx, bool isCircleClose)
{
	// To avoid draw parts duplication
	SectorDrawPart *pPart;
	for(int idx = 0 ; idx < drawParts.count() ; idx++)
	{
		pPart = drawParts.at(idx);
		if((pPart->getLine() == pLine) &&
			(pPart->getStartPart() == pStartPart) && (pPart->getStartEqpIdx ()== startEqpIdx) &&
			(pPart->getEndPart() == pEndPart) && (pPart->getEndEqpIdx() == endEqpIdx))
		{
			return;
		}
	}
	pPart = new SectorDrawPart();
	pPart->setLine(pLine);
	pPart->setStartPart(pStartPart);
	pPart->setStartEqpIdx(startEqpIdx);
	pPart->setEndPart(pEndPart);
	pPart->setEndEqpIdx(endEqpIdx);
	pPart->setCircleClose(isCircleClose);
	drawParts.append(pPart);
}
#endif

/*
**
**	FUNCTION
**		Dump sector parameters to file
**
**	PARAMETERS
**		idx		- Index of this sector in data pool
**		pFile	- Pointer to file opened for write.
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
**	CAUTIONS
**		None
*/
void Sector::dump(short idx, FILE *pFile)
{
	fprintf(pFile, "  SECT %d: <%s> ( PVSS <%s> ) type %d from %f to %f beamOrder %d\n", idx, name, pvssName, vacType, start, end, beamOrder);
	if(outer)
	{
		fprintf(pFile, "   OUTER SECTOR\n");
	}
	fprintf(pFile, "    %d MP mappings:\n", mpMaps.count());
	int n;
	MainPartMap *pMap;
	for(n = 0 ; n < mpMaps.count() ; n++)
	{
		pMap = mpMaps.at(n);
		fprintf(pFile, "      %d: MainPart %s Domain %s\n", n,
			pMap->getMainPart()->getName(),
			pMap->getDomain().toLatin1().constData());
	}
#ifndef PVSS_SERVER_VERSION
	fprintf(pFile, "    %d draw parts:\n", drawParts.count());
	SectorDrawPart *pPart;
	for(n = 0 ; n < drawParts.count() ; n++)
	{
		pPart = drawParts.at(n);
		fprintf(pFile, "      %d: %s from %s %d (%s) to %s %d (%s) %s\n", n,
			pPart->getLine()->getName(),
			pPart->getStartPart()->getName(), pPart->getStartEqpIdx(),
			pPart->getStartPart()->getEqpName(pPart->getStartEqpIdx()),
			pPart->getEndPart()->getName(), pPart->getEndEqpIdx(),
			pPart->getEndPart()->getEqpName(pPart->getEndEqpIdx()),
			(pPart->isCircleClose() ? "CIRCLE CLOSE" : ""));
	}
	if(onRedBeam)
	{
		fprintf(pFile, "      RED: start %f (%s) end %f (%s)\n",
			redStart, (pRedStartEqp ? pRedStartEqp->getName() : ""),
			redEnd, (pRedEndEqp ? pRedEndEqp->getName() : ""));
	}
	if(onBlueBeam)
	{
		fprintf(pFile, "     BLUE: start %f (%s) end %f (%s)\n",
			blueStart, (pBlueStartEqp ? pBlueStartEqp->getName() : ""),
			blueEnd, (pBlueEndEqp ? pBlueEndEqp->getName() : ""));
	}
	if(onCrossBeam)
	{
		fprintf(pFile, "    CROSS: start %f (%s) end %f (%s)\n",
			crossStart, (pCrossStartEqp ? pCrossStartEqp->getName() : ""),
			crossEnd, (pCrossEndEqp ? pCrossEndEqp->getName() : ""));
	}
#endif
}

/*
**	FUNCTION
**		[VACCO-929]
**		Set selected state of this sector.
**		Only allows one sector to be selected at each time.
**
**	PARAMETERS
**		selected	- New selection state
**		flag		- Flag indicating if this is original, or a recursive call to set the rest of the sector to select = false.
**						if flag = true then it is one recursive call (no signal emitted)
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void Sector::setSelected(bool selected, bool flag = true){
	if (this->selected == selected){
		return;
	}
	this->selected = selected;

	if (flag) return; // in case this method is already being called from a setSelected method

	QList<Sector *> &sectList = DataPool::getInstance().getSectors();
	for (int idx = 0; idx < sectList.count(); idx++){
		Sector *pSector;
		pSector = sectList.at(idx);
		if (pSector == this) continue;
		pSector->setSelected(false, true);
	}
	emit synSectSelectChanged(this);
}


/*
**	FUNCTION
**		[VACCO-1645]
**		Updates a Sector object state,
**		NOTE: states are defined on Sector.h
**		Current states: 	SECTOR_OPERATIONAL		(0x00000001)
**							SECTOR_ONWORK			(0x00000002)
**							SECTOR_VENTED			(0x00000004)
**
**	PARAMETERS
**		newState - new state flag to add to the Sector state
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void Sector::stateChanged(unsigned newState){
	switch (newState){
		case (SECTOR_OPERATIONAL) :
			this->state |= SECTOR_OPERATIONAL;
			this->state &= ~SECTOR_ONWORK;
			this->state &= ~SECTOR_VENTED;
			break;
		case (SECTOR_ONWORK) :
			this->state |= SECTOR_ONWORK;
			this->state &= ~SECTOR_OPERATIONAL;
			this->state &= ~SECTOR_VENTED;
			break;
		case (SECTOR_VENTED) :
			this->state |= SECTOR_VENTED;
			this->state &= ~SECTOR_OPERATIONAL;
			this->state &= ~SECTOR_ONWORK;
			break;
		default:
			break;
	}
	emit synSectStateChanged(this);
	return;
}


/*
**	FUNCTION
**		[VACCO-1645]
**		Checks if a Sector state has the VENTED flag on
**
**	PARAMETERS
**		None
**
**	RETURNS
**		True/False
**
** CAUTIONS
**		None
*/
QString Sector::getStateString(void){
	QString stateString;

	switch (this->state){
	case (SECTOR_OPERATIONAL) :
		stateString = "Operational";
		break;
	case (SECTOR_ONWORK) :
		stateString = "On Work";
		break;
	case (SECTOR_VENTED) :
		stateString = "Vented";
		break;
	default:
		stateString = "";
		break;
	}
	return stateString;
}


/*
**	FUNCTION
**		[VACCO-1645]
**		Checks if a Sector state has the OPERATIONAL flag on
**
**	PARAMETERS
**		None
**
**	RETURNS
**		True/False
**
** CAUTIONS
**		None
*/
bool Sector::isOperational(void){
	if (this->state & SECTOR_OPERATIONAL) return true;
	return false;
}


/*
**	FUNCTION
**		[VACCO-1645]
**		Checks if a Sector state has the ONWORK flag on
**
**	PARAMETERS
**		None
**
**	RETURNS
**		True/False
**
** CAUTIONS
**		None
*/
bool Sector::isOnWork(void){
	if (this->state & SECTOR_ONWORK) return true;
	return false;
}


/*
**	FUNCTION
**		[VACCO-1645]
**		Checks if a Sector state has the VENTED flag on
**
**	PARAMETERS
**		None
**
**	RETURNS
**		True/False
**
** CAUTIONS
**		None
*/
bool Sector::isVented(void){
	if (this->state & SECTOR_VENTED) return true;
	return false;
}



