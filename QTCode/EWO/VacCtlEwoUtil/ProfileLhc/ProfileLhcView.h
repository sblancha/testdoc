#ifndef	PROFILELHCVIEW_H
#define	PROFILELHCVIEW_H

#include "ProfileLhcLine.h"

#include "VacEqpTypeMask.h"

#include <QWidget>
#include <QList>
#include <QDateTime>

#include <stdio.h>

class ExtendedLabel;
#include <QSpinBox>
#include <QTimer>

class ProfileLhcView : public QWidget
{
	Q_OBJECT

public:
	ProfileLhcView(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	virtual ~ProfileLhcView();

	static ProfileLhcView *create(QWidget *parent, Sector *pFirstSector, Sector *pLastSector,
		DataEnum::DataMode mode);

	void init(const VacEqpTypeMask &cryoMask, const VacEqpTypeMask &beamMask);

	virtual int bestPositionForEqp(float realStart);	// !!!!!!!!!!!!!!!!!!!!!

	void analyzeTypes(unsigned &vacMask, VacEqpTypeMask &cryoMask, VacEqpTypeMask &beamMask);
	void applyMask(unsigned vacTypeMask, VacEqpTypeMask &cryoEqpMask, VacEqpTypeMask &beamEqpMask);
	void setCryoLimits(int newMin, int newMax);
	void setBeamLimits(int newMin, int newMax);
	void setLinLimits(int newMin, int newMax);

	bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	void dump(const char *fileName);

	// Access
	inline bool isLinearSelected(void) const { return linearSelected; }
	inline bool isHasLinearEqp(void) const { return hasLinearEqp; }
	inline bool isShowSectorNames(void) const { return showSectorNames; }
	void setShowSectorNames(bool flag);

signals:
	void mobileStateChanged(void);
	void eqpMaskChanged(void);
	void dpMouseDown(int button, int mode, int x, int y, const char *dpName);
	void cryoLimitsChanged(int min, int max);
	void beamLimitsChanged(int min, int max);
	void linLimitsChanged(int min, int max);

protected:
	// Pointer to start sector
	Sector					*pStartSector;

	// Pointer to end sector
	Sector					*pEndSector;

	// Start coordinate [m]
	float					start;

	// End coordinate [m]
	float					end;

	// List of synoptic lines
	QList<ProfileLhcLine *>	lines;

	// Label for displaying information on graph bars
	ExtendedLabel			*pLabel;

	// Timer to hide label after 'reasonable' delay
	QTimer					*pTimer;

	// Timestamp of last image drawing
	QTime					drawTime;

	// Timer to redraw image after online value arrival
	QTimer					*pRedrawTimer;

	// SpinBox for updating scale settings
	QSpinBox				*pSpinBox;

	// Bit mask for vacuum type selection
	unsigned 				vacTypeMask;

	// Equipment type mask - all types potentially allowed on isolation vacuum
	VacEqpTypeMask			allowedCryoEqpMask;

	// Equipment type mask - all types potentially allowed on beam vacuum
	VacEqpTypeMask			allowedBeamEqpMask;

	// Equipment type mask for isolation vacuum
	VacEqpTypeMask			cryoEqpMask;

	// Equipment type mask for beam vacuum
	VacEqpTypeMask			beamEqpMask;

	// Data acquisition mode for this view
	DataEnum::DataMode		mode;

	// Maximum bar index in all lines
	int				maxBarIdx;

	// Y-coordinate of CRYO vertical axis top
	int 			cryoAxisTop;

	// Y-coordinate of CRYO vertical axis bottom
	int				cryoAxisBottom;

	// Y-coordinate of BEAM vertical axis top
	int				beamAxisTop;

	// Y-coordinate of BEAM vertical axis bottom
	int				beamAxisBottom;

	// Y-coordinate of linear vertical axis top
	int				linAxisTop;

	// Y-coordinate of linear vertical axis bottom
	int				linAxisBottom;

	// X-coordinate of logarithmic axis location
	int				logAxisX;

	// X-coordinate of linear axis location
	int				linAxisX;

	// Which scale is being edited by spin box
	typedef enum
	{
		ScaleNone = 0,
		ScaleCryo = 1,
		ScaleBeam = 2,
		ScaleLin = 3
	} ScaleType;

	ScaleType			scaleType;

	// Flag indicating if minimum or maximum of scale is being edited by spin box
	bool				editScaleMin;

	// Flag indicating if value of spin box is changed as a result of settings by code
	bool				settingSpinBox;

	// Flag indicating if equipment type or vacuum masks have been changed
	bool				newMaskApplied;

	// Flag indicating if at least one line has equipment using linear vertical axis
	bool				hasLinearEqp;

	// Flag indicating if selected equipment uses linear axis
	bool				linearSelected;

	// Flag inidcating if sector names shall be shown beneath graph area for every line
	bool					showSectorNames;

	void build(void);
	void buildView(unsigned vacTypeMask, VacEqpTypeMask &cryoEqpMask, VacEqpTypeMask &beamEqpMask);
	void buildGeometry(void);
	void setLineVisibility(ProfileLhcLine *pLine, unsigned vacMask, VacEqpTypeMask &eqpMask, int &nGraphs);
	ProfileLhcLine *nextLine(ProfileLhcLine *pPrev);
	int compareCoordInView(float bestCoord, float newCoord);

	void draw(QPainter &painter, bool flashingOnly, bool useAltFlashColor);

	Eqp *findItemUnderMouse(int x, int y, QRect &rect);
	void showBarLabel(Eqp *pEqp, const QRect &rect, int y);
	void showLogScaleControl(int y);
	void showLinScaleControl(int y);
	void cryoSpinBoxChange(int value);
	void beamSpinBoxChange(int value);
	void linSpinBoxChange(int value);
	ProfileLhcLine *findLine(int type);

	virtual void resizeEvent(QResizeEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *pEvent);
	virtual void mousePressEvent(QMouseEvent *pEvent);
	virtual void mouseDoubleClickEvent(QMouseEvent *pEvent);
	virtual void leaveEvent(QEvent *pEvent);

protected slots:
	void eqpDataChange(void);
	void eqpSelectChange(bool linear);

	void spinBoxChange(int value);
	void redrawTimerDone(void);
	void rebuildImage(void);
	void mobileStateChange(bool redrawNeeded);
	void eqpVisibilityChange(void);
};

#endif	// PROFILELHCVIEW_H
