#ifndef MAINVIEWTABLED_H
#define MAINVIEWTABLED_H

// 'Invisible' main view widget, used in PVSS panels like DevMonitor

#include "MainViewTabledEqpConnect.h"

#include "VacMainView.h"

#include <qmutex.h>

class MainViewTabled : public VacMainView
{
	Q_OBJECT

	Q_PROPERTY(bool pvssEvents READ isPvssEvents WRITE setPvssEvents);

public:
	MainViewTabled(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~MainViewTabled();

	int connectDp(const char *dp, int tag);
	int disconnectDp(const char *dp);
	int disconnectAll(void);

	int getNextChangeTag(void);
	const char *getNextChangeDp(void);

	inline bool isPvssEvents(void) const { return VacMainView::isPvssEvents(); }
	inline void setPvssEvents(bool flag) { VacMainView::setPvssEvents(flag); }

	// Implement abstract methods of VacMainView
	virtual void dialogRequest(int type, unsigned vacTypes, const QStringList &sectorList, DataEnum::DataMode mode);

signals:
	void dialogRequested(int type, unsigned vacType, const QStringList &sectorList, DataEnum::DataMode mode);
	void dpDataChanged(void);

protected:
	// DP connection instance
	MainViewTabledEqpConnect	connection;

	// Queue of DP changes to be reported
	QList<OneDpConnection *>	dpChanges;

	// Mutex to protect dpChanges
	QMutex						mutex;


	virtual void paintEvent(QPaintEvent *event);

private slots:
	void dpDataChange(OneDpConnection *pDpData);
};

#endif	// MAINVIEWTABLED_H
