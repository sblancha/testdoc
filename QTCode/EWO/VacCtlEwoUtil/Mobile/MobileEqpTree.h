#ifndef	MOBILEEQPTREE_H
#define	MOBILEEQPTREE_H

// Subclass of QTreeWidget that emits signal when checkbox was clicked

#include <QTreeWidget>

class MobileEqpTree : public QTreeWidget
{
	Q_OBJECT
public:
	MobileEqpTree(QWidget *parent = 0);
	~MobileEqpTree();

signals:
	void checkBoxPressed(QTreeWidgetItem *pItem);

protected:
	virtual void mouseReleaseEvent(QMouseEvent *pEvent);
};

#endif	// MOBILEEQPTREE_H
