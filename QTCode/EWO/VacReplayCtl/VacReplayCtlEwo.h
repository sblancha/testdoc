#ifndef VACREPLAYCTLEWO_H
#define	VACREPLAYCTLEWO_H

//	EWO interface for VacReplayCtl class

#include <BaseExternWidget.hxx>

#include "VacReplayCtl.h"

class EWO_EXPORT VacReplayCtlEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	VacReplayCtlEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void dpeListChange(void);
		
private:
	VacReplayCtl	*baseWidget;
};


#endif	// VACREPLAYCTLEWO_H
