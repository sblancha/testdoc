//	Implementation of SynopticAllLhcWidget class
/////////////////////////////////////////////////////////////////////////////////

#include "SynopticAllLhcWidget.h"
#include "SynLhcView.h"

#include "VacMainView.h"

#include "DataPool.h"

#include <qlayout.h>
#include <qlabel.h>


/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

SynopticAllLhcWidget::SynopticAllLhcWidget(QWidget *parent, Qt::WindowFlags f, DataEnum::DataMode mode) :
	QWidget(parent, f)
{
	this->mode = mode;
	// images.setAutoDelete(false);
	pTitleLabel = NULL;

	// At creation time index of LHC part is not known, will be set later
	// (after initializang static data) by setLhcPartIndex()
	lhcPartIndex = -1;
}

SynopticAllLhcWidget::~SynopticAllLhcWidget()
{
}

/*
**	FUNCTION
**		Set index of LHC part shown in this dialog. Initialize
**		all contrls according to given index
**
**	ARGUMENTS
**		index	- LHC part index to set [0 or 1]
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Method shall be called after all static data are initialized
*/
void SynopticAllLhcWidget::setLhcPartIndex(int index)
{
	if(lhcPartIndex >= 0)
	{
		return;	// Already initialized, can not be changed at run time
	}
	if((index != 0) && (index != 1))
	{
		return;	// Wrong index
	}
	lhcPartIndex = index;

	if(lhcPartIndex)
	{
		setWindowTitle("All LHC Synoptic - part 2");
	}
	else
	{
		setWindowTitle("All LHC Synoptic");
	}

	buildLayout();
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticAllLhcWidget::buildLayout(void)
{
	// 1) Main layout - label on top, 4 synoptic views on bottom
	QVBoxLayout *mainBox = new QVBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);

	// Build equipment and vacuum type masks
	VacEqpTypeMask cryoEqpMask, beamEqpMask;
	cryoEqpMask.append(FunctionalType::VPGF);
	cryoEqpMask.append(FunctionalType::VV);
	cryoEqpMask.append(FunctionalType::VPGM);
	cryoEqpMask.append(FunctionalType::PROCESS_VPG_6A01);
	cryoEqpMask.append(FunctionalType::VPG);
	unsigned vacType = VacType::Qrl | VacType::Cryo;

	// 2) Label on top of - only required in replay mode
	if(mode == DataEnum::Replay)
	{
		pTitleLabel = new QLabel(" ", this);
		QPalette palette = pTitleLabel->palette();
		palette.setColor(QPalette::Window, Qt::cyan);
		pTitleLabel->setPalette(palette);
		pTitleLabel->setAutoFillBackground(true);
		mainBox->addWidget(pTitleLabel);
		pTitleLabel->show();
	}

	// 3) synoptic views for individual main parts
	QFont viewFont("Arial", 6);
	DataPool &pool = DataPool::getInstance();
	QList<MainPart *> mpList;
	QStringList sectorList;
	int startIp = lhcPartIndex ? 5 : 1;
	for(int n = 0 ; n < 4 ; n++)
	{
		int nextIp = startIp + 1;
		if(nextIp > 8)
		{
			nextIp = 1;
		}
		QString mpName = QString::number(startIp) + QString::number(nextIp) + ".Q";
		startIp++;
		MainPart *pMainPart = pool.findMainPartData(mpName.toLatin1());
		if(!pMainPart)
		{
			continue;
		}
		mpList.clear();
		mpList.append(pMainPart);
		vacMask = VacType::None;
		mainVacMask = VacType::Qrl;
		MainPart *pAdjucentMp = getAdjucentMainPart(pMainPart, VacType::Cryo);
		if(pAdjucentMp)
		{
			mpList.append(pAdjucentMp);
		}
		buildRangeFromMps(mpList, sectorList);

		Sector *pFirstSector = NULL, *pLastSector = NULL;
		// DSL sector shall not be used as start or end sector
		foreach(QString sectName, sectorList)
		{
			Sector *pSector = pool.findSectorData(sectName.toLatin1());
			if(pSector->getVacType() == VacType::DSL)
			{
				continue;
			}
			if(!pFirstSector)
			{
				pFirstSector = pSector;
			}
			pLastSector = pSector;
		}
		if((!pFirstSector) || (!pLastSector))
		{
			continue;
		}

		SynLhcView *pView = SynLhcView::create(this, pFirstSector, pLastSector, mode, true);

		if(!pView)
		{
			printf("SynopticAllLhc::buildLayout(): NULL pView\n");
			fflush(stdout);
			continue;
		}
		pView->setFont(viewFont);
		pView->setAlwaysIncludeSectorBorder(false);
		pView->applyMask(vacType, cryoEqpMask, beamEqpMask);
		mainBox->addWidget(pView, 10);
		pView->show();
		pView->setViewStart(0, pView->width(), false);
	}
	QSize minSize = mainBox->minimumSize();
	setMinimumSize(minSize);
	minSize = minimumSize();
}
