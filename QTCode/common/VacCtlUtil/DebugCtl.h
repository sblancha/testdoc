#ifndef DEBUGCTL_H
#define	DEBUGCTL_H

// Global control of debugging messages/output to files/etc.

#include "VacCtlUtilExport.h"

class VACCTLUTIL_EXPORT DebugCtl
{
public:
	static inline bool isData(void) { return data; }
	static inline void setData(bool flag) { data = flag; }
	static inline bool isMainView(void) { return mainView; }
	static inline void setMainView(bool flag) { mainView = flag; }
	static inline bool isAxis(void) { return axis; }
	static inline void setAxis(bool flag) { axis = flag; }
	static inline bool isHistory(void) { return history; }
	static inline void setHistory(bool flag) { history = flag; }
	static inline bool isSynoptic(void) { return synoptic; }
	static inline void setSynoptic(bool flag) { synoptic = flag; }
	static inline bool isProfile(void) { return profile; }
	static inline void setProfile(bool flag) { profile = flag; }
	static inline bool isSms(void) { return sms; }
	static inline void setSms(bool flag) { sms = flag; }

protected:
	// Debugging of basic operations with data
	static bool	data;

	// Debugging of main view
	static bool	mainView;

	// Debugging of operations with axies
	static bool	axis;

	// Debugging of history view
	static bool	history;

	// Debugging of synoptic view
	static bool	synoptic;

	// Debugging of profile view
	static bool	profile;

	// Debugging of SMS notifications
	static bool	sms;
};

#endif	// DEBUGCTL_H
