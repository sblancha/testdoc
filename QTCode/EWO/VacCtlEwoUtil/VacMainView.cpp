//	Implementation of VacMainView class
/////////////////////////////////////////////////////////////////////////////////

#include "VacMainView.h"

#include "DataPool.h"
#include "Sector.h"
#include "Eqp.h"
#include "Rack.h"
#include "EqpMsgCriteria.h"


#include "SectorView/SectorViewDialog.h"
#include "Synoptic/SynopticDialog.h"
#include "Profile/ProfileDialog.h"
#include "History/HistoryDialog.h"
#include "ProfileAllSPS/ProfileAllSPS.h"
#include "SynopticLhc/SynopticLhcDialog.h"
#include "ProfileLhc/ProfileLhcDialog.h"
#include "ProfileAllLHCInnerOuter/ProfileAllLHCInOut.h"
#include "ProfileAllLHCBeam/ProfileAllLHCBeam.h"
#include "SynopticAllLhc/SynopticAllLhc.h"
#include "SectorViewLhc/SectorViewLhcDialog.h"
#include "Mobile/MobileHistoryDialog.h"
#include "Mobile/BakeoutRackProfileDialog.h"
#include "Mobile/BakeoutRackDetailsDialog.h"
#include "Documentation/RackDocumentationDialog.h"
#include "Documentation/InterlockDocumentationDialog.h"

#include <QPrinter>
#include <QPainter>
#include <QPixmap>
#include <QPrintDialog>

#include <QApplication>
#include <stdio.h>
#include <stdlib.h>

VacMainView *VacMainView::pMainView = NULL;

QList<VacMainView *> VacMainView::instanceList;

#ifdef Q_OS_WIN
QString VacMainView::filePath("C:");
#else
QString VacMainView::filePath("/home");
#endif

QPrinter *VacMainView::pPrinter = NULL;


// List of dialog instances opened in Replay mode
QList<QWidget *> VacMainView::replayDialogs;

// Synoptic and sector view resources
QColor VacMainView::synPipeColor(0, 0, 255);
QColor VacMainView::synAltPipeColor(127, 127, 127);
QColor VacMainView::synPipePartColor(127, 127, 127);
QColor VacMainView::synPassiveLineColor(0, 0, 192);
QColor VacMainView::synPassiveFillColor(255, 0, 255);
QColor VacMainView::synSectorColor(192, 0, 192);
int VacMainView::synPipeWidth = 3;

// Profile resources
QColor VacMainView::graphAxisColor(0, 0, 0);
int VacMainView::profileBarWidth = 80;
QColor VacMainView::profileAltSectorColor(127, 127, 127);
QColor VacMainView::profileAltBarColor(127, 127, 127);
int VacMainView::profileRedrawDelay = 300;

// LHC profile resources
int VacMainView::profileSectorDrawMethod = 1;
QColor VacMainView::profileBackColorRedBeam(255, 183, 183);
QColor VacMainView::profileBackColorBlueBeam(202, 202, 255);
QColor VacMainView::profileBackColorCrossBeam(251, 255, 164);
QColor VacMainView::profileBackColorQrl(202, 255, 255);
QColor VacMainView::profileBackColorCryo(192, 192, 192);
QColor VacMainView::profileTempQrlBColor(255, 128, 128);
QColor VacMainView::profileTempQrlCColor(255, 128, 0);
QColor VacMainView::profileTempCmColor(242, 79, 0);
QColor VacMainView::profileTempRfColor(255, 128, 0);
QColor VacMainView::profileTempBsColor(255, 128, 128);


// History resources
QColor VacMainView::historyNegativeColor(255, 255, 255);
int VacMainView::historySelectWidth = 3;

// Polling period
int VacMainView::pollingPeriod = 30;


VacMainView *VacMainView::getInstance(void)
{
	return pMainView;
}



const QColor &VacMainView::getSynPipeColor(void)
{
	return synPipeColor;
}

void VacMainView::setSynPipeColor(const QColor &color)
{
	synPipeColor = color;
}


const QColor &VacMainView::getSynAltPipeColor(void)
{
	return synAltPipeColor;
}

void VacMainView::setSynAltPipeColor(const QColor &color)
{
	synAltPipeColor = color;
}


const QColor &VacMainView::getSynPipePartColor(void)
{
	return synPipePartColor;
}

void VacMainView::setSynPipePartColor(const QColor &color)
{
	synPipeColor = color;
}


const QColor &VacMainView::getSynPassiveLineColor(void)
{
	return synPassiveLineColor;
}

void VacMainView::setSynPassiveLineColor(const QColor &color)
{
	synPassiveLineColor = color;
}


const QColor &VacMainView::getSynPassiveFillColor(void)
{
	return synPassiveFillColor;
}

void VacMainView::setSynPassiveFillColor(const QColor &color)
{
	synPassiveFillColor = color;
}


const QColor &VacMainView::getSynSectorColor(void)
{
	return synSectorColor;
}

void VacMainView::setSynSectorColor(const QColor &color)
{
	synSectorColor = color;
}


int VacMainView::getSynPipeWidth(void)
{
	return synPipeWidth;
}

void VacMainView::setSynPipeWidth(int width)
{
	synPipeWidth = width;
}


QColor &VacMainView::getGraphAxisColor(void)
{
	return graphAxisColor;
}

void VacMainView::setGraphAxisColor(const QColor &color)
{
	graphAxisColor = color;
}


int VacMainView::getProfileBarWidth(void)
{
	return profileBarWidth;
}

void VacMainView::setProfileBarWidth(int width)
{
	profileBarWidth = (width < 0 ? 0 : (width > 100 ? 100 : width));
}


QColor &VacMainView::getProfileAltSectorColor(void)
{
	return profileAltSectorColor;
}

void VacMainView::setProfileAltSectorColor(const QColor &color)
{
	profileAltSectorColor = color;
}


QColor &VacMainView::getProfileAltBarColor(void)
{
	return profileAltBarColor;
}

void VacMainView::setProfileAltBarColor(const QColor &color)
{
	profileAltBarColor = color;
}


int VacMainView::getProfileRedrawDelay(void)
{
	return profileRedrawDelay;
}

void VacMainView::setProfileRedrawDelay(int delay)
{
	profileRedrawDelay = (delay < 200 ? 200 : delay);
}


QColor &VacMainView::getHistoryNegativeColor(void)
{
	return historyNegativeColor;
}

void VacMainView::setHistoryNegativeColor(const QColor &color)
{
	historyNegativeColor = color;
}


int VacMainView::getHistorySelectWidth(void)
{
	return historySelectWidth;
}

void VacMainView::setHistorySelectWidth(int width)
{
	historySelectWidth = (width < 2 ? 2 : width);
}


int VacMainView::getProfileSectorDrawMethod(void)
{
	return profileSectorDrawMethod;
}

void VacMainView::setProfileSectorDrawMethod(int method)
{
	profileSectorDrawMethod = method;
}


QColor &VacMainView::getProfileBackColorRedBeam(void)
{
	return profileBackColorRedBeam;
}

void VacMainView::setProfileBackColorRedBeam(const QColor &color)
{
	profileBackColorRedBeam = color;
}


QColor &VacMainView::getProfileBackColorBlueBeam(void)
{
	return profileBackColorBlueBeam;
}

void VacMainView::setProfileBackColorBlueBeam(const QColor &color)
{
	profileBackColorBlueBeam = color;
}


QColor &VacMainView::getProfileBackColorCrossBeam(void)
{
	return profileBackColorCrossBeam;
}

void VacMainView::setProfileBackColorCrossBeam(const QColor &color)
{
	profileBackColorCrossBeam = color;
}


QColor &VacMainView::getProfileBackColorQrl(void)
{
	return profileBackColorQrl;
}

void VacMainView::setProfileBackColorQrl(const QColor &color)
{
	profileBackColorQrl = color;
}


QColor &VacMainView::getProfileBackColorCryo(void)
{
	return profileBackColorCryo;
}

void VacMainView::setProfileBackColorCryo(const QColor &color)
{
	profileBackColorCryo = color;
}


QColor &VacMainView::getProfileTempQrlBColor(void)
{
	return profileTempQrlBColor;
}

void VacMainView::setProfileTempQrlBColor(const QColor &color)
{
	profileTempQrlBColor = color;
}


QColor &VacMainView::getProfileTempQrlCColor(void)
{
	return profileTempQrlCColor;
}

void VacMainView::setProfileTempQrlCColor(const QColor &color)
{
	profileTempQrlCColor = color;
}

QColor &VacMainView::getProfileTempCmColor(void)
{
	return profileTempCmColor;
}

void VacMainView::setProfileTempCmColor(const QColor &color)
{
	profileTempCmColor = color;
}

QColor &VacMainView::getProfileTempRfColor(void)
{
	return profileTempRfColor;
}

void VacMainView::setProfileTempRfColor(const QColor &color)
{
	profileTempRfColor = color;
}


QColor &VacMainView::getProfileTempBsColor(void)
{
	return profileTempRfColor;
}

void VacMainView::setProfileTempBsColor(const QColor &color)
{
	profileTempRfColor = color;
}


int VacMainView::getPollingPeriod(void)
{
	return pollingPeriod;
}

void VacMainView::setPollingPeriod(int period)
{
	if(period < 1)
	{
		period = 1;
	}
	pollingPeriod = period;
	if(pMainView)
	{
		pMainView->notifyPollingPeriod(period);
	}
}


/////////////////////////////////////////////////////////////////////////////////
/////////////// Constructor/destructor
/////////////////////////////////////////////////////////////////////////////////

void FatalMessageHandler(QtMsgType type, const char *msg)
{
	switch(type)
	{
	case QtDebugMsg:
		fprintf(stderr, "Debug: %s\n", msg);
		return;
	case QtInfoMsg:
		fprintf(stderr, "Info: %s\n", msg);
		return;
	case QtWarningMsg:
		fprintf(stderr, "Warning: %s\n", msg);
		break;
	case QtCriticalMsg:
		fprintf(stderr, "Critial: %s\n", msg);
		break;
	case QtFatalMsg:
		fprintf(stderr, "Fatal: %s\n", msg);
		break;
	}
	// to produce core file
	char *dummy = NULL;
	*dummy = '\0';
}

VacMainView::VacMainView(QWidget *parent, Qt::WindowFlags flags)
	: QWidget(parent, flags)
{
	pvssEvents = false;
	connectedToPrimary = false;
	if(pMainView)
	{
		secondary = true;
	}
	else
	{
		pMainView = this;
		secondary = false;
	}
	instanceList.append(this);
/* Commented out 23.08.2012 L.Kopylov.
#ifndef Q_OS_WIN
	qInstallMsgHandler(FatalMessageHandler);
#endif
*/
}

VacMainView::~VacMainView()
{
	if(!secondary)
	{
		pMainView = NULL;	// only ONE instance is expected!!!
	}
	instanceList.removeOne(this);

}

void VacMainView::setPvssEvents(bool flag)
{
	if(pvssEvents == flag)
	{
		return;
	}
	pvssEvents = flag;
	if(secondary)
	{
		if(pMainView && pvssEvents && (!connectedToPrimary))
		{
			connect(pMainView, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)),
				this, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
			connect(pMainView, SIGNAL(dpIconMouseDown(int, int, int, int, int, const char *)),
				this, SIGNAL(dpIconMouseDown(int, int, int, int, int, const char *)));
			connect(pMainView, SIGNAL(rackIconMouseDown(int, int, int, int, int, const char *)),
				this, SIGNAL(rackIconMouseDown(int, int, int, int, int, const char *)));
			connect(pMainView, SIGNAL(eqpDoubleClick(const QString &, int)),
				this, SIGNAL(eqpDoubleClick(const QString &, int)));
			connect(pMainView, SIGNAL(sectorDoubleClick(const QString &, int)),
				this, SIGNAL(sectorDoubleClick(const QString &, int)));
			connect(pMainView, SIGNAL(synSectorMouseDown(int, int, int, int, const char *, bool)),
				this, SIGNAL(synSectorMouseDown(int, int, int, int, const char *, bool))); // [VACCO-929]
			connectedToPrimary = true;
		}
		if(pMainView && (!pvssEvents) && connectedToPrimary)
		{
			disconnect(pMainView, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)),
				this, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
			disconnect(pMainView, SIGNAL(dpIconMouseDown(int, int, int, int, int, const char *)),
				this, SIGNAL(dpIconMouseDown(int, int, int, int, int, const char *)));
			disconnect(pMainView, SIGNAL(rackIconMouseDown(int, int, int, int, int, const char *)),
				this, SIGNAL(rackIconMouseDown(int, int, int, int, int, const char *)));
			disconnect(pMainView, SIGNAL(eqpDoubleClick(const QString &, int)),
				this, SIGNAL(eqpDoubleClick(const QString &, int)));
			disconnect(pMainView, SIGNAL(sectorDoubleClick(const QString &, int)),
				this, SIGNAL(sectorDoubleClick(const QString &, int)));
			disconnect(pMainView, SIGNAL(synSectorMouseDown(int, int, int, int, const char *, bool)),
				this, SIGNAL(synSectorMouseDown(int, int, int, int, const char *, bool))); // [VACCO-929]
			connectedToPrimary = false;
		}
	}

}

void VacMainView::notifyPollingPeriod(int period)
{
	emit pollingPeriodChanged(period);
}


/*
**	FUNCTION
**		Set color for drawing beam pipes on synoptic depending on
**		vacuum type
**
**	ARGUMENTS
**		vacType	- Vacuum type
**		color		- Color to set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainView::setSynLineColor(int vacType, QColor &color)
{
	switch(vacType)
	{
	case VacType::RedBeam:
		color.setRgb(255, 0, 0);
		break;
	case VacType::BlueBeam:
		color.setRgb(0, 0, 255);
		break;
	case VacType::CrossBeam:
	case VacType::CommonBeam:
	case VacType::DSL:
		color.setRgb(255, 255, 0);
		break;
	case VacType::Qrl:
		color.setRgb(128, 255, 255);
		break;
	case VacType::Cryo:
		color.setRgb(192, 192, 255);
		break;
	default:
		color = synPipeColor;
		break;
	}

}

/*
**	FUNCTION
**		Open given dialog on top of main view widget
**
**	ARGUMENTS
**		type		- Dialog type, must be one of enum values defined
**						in this class
**		vacTypes	- Vacuum type mask for equipment to be shown dialog
**		firstSector	- Name of first sector for equipment selection
**		lastSector	- Name of last sector for equipment selection
**		mode		- Data acquisition mode
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openDialog(int type, unsigned vacTypes, const char *firstSector,
	const char *lastSector, DataEnum::DataMode mode, QStringList &errList)
{
	errList.clear();
	if(!pMainView)
	{
		errList.append("No main view opened");
		return -1;
	}
	int coco;
	switch(type)
	{
	case DialogSector:
		coco = openSectorView(vacTypes, firstSector, lastSector, mode, errList);
		break;

	case DialogSynoptic:
		coco = openSynoptic(vacTypes, firstSector, lastSector, mode, errList);
		break;

	case DialogProfile:
		coco = openProfile(vacTypes, firstSector, lastSector, mode, errList);
		break;

	case DialogProfileAllSPS:
		coco = openProfileAllSPS(vacTypes, firstSector, lastSector, mode, errList);
		break;

	case DialogSynopticLHC:
		coco = openSynopticLhc(vacTypes, firstSector, lastSector, mode, errList);
		break;

	case DialogProfileLHC:
		coco = openProfileLhc(vacTypes, firstSector, lastSector, mode, errList);
		break;

	case DialogProfileAllLhcInOut:
		coco = openProfileAllLhcInOut(errList);
		break;

	case DialogProfileAllLhcBeam:
		coco = openProfileAllLhcBeam(errList);
		break;

	case DialogSynopticAllLhc:
		coco = openSynopticAllLhc(mode, errList);
		break;

	case DialogSectorLhc:
		coco = openSectorViewLhc(firstSector, lastSector, mode, errList);
		break;

	case DialogMobileHistory:
		coco = openMobileHistory(errList);
		break;

	case DialogBakeoutRackProfile:
		coco = openBakeoutRackProfile(errList);
		break;

	case DialogBakeoutRackDetails:
		coco = openBakeoutRackDetails(errList);
		break;
	default:
		errList.append(QString("Unsupported dialog type %1").arg(type));
		coco = -1;
		break;
	}
	return coco;
}

/*
**	FUNCTION
**		Open given dialog for documentation on top of main view widget
**
**	ARGUMENTS
**		type		- Dialog type, must be one of enum values defined
**						in this class
**		vacTypes	- Vacuum type mask for equipment to be shown dialog
**		equipmentName - equipment for which to make the diagram
**		mode		- Data acquisition mode
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openDocumentationDialog(int type, unsigned vacTypes, const char *equipmentName, DataEnum::DataMode mode, QStringList &errList)
{
	errList.clear();
	if (!pMainView)
	{
		errList.append("No main view opened");
		return -1;
	}
	int coco;
	switch (type)
	{
	case DialogRackDiagram:
		coco = openRackDiagram(equipmentName, errList);
		break;
	case DialogInterlockSchematic:
		coco = openInterlockSchematic(equipmentName, errList);
		break;
	default:
		errList.append(QString("Unsupported dialog type %1").arg(type));
		coco = -1;
		break;
	}
	return coco;
}

void VacMainView::makeDeviceVisible(Eqp *pEqp)
{
	SynopticLhcDialog::makeDeviceVisible(pEqp);
	SynopticDialog::makeDeviceVisible(pEqp);
}

/*
**	FUNCTION
**		Find IDs of all opened history instances.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::getAllInstances()
**
**	RETURNS
**		See HistoryDialog::getAllInstances()
**
**	CAUTIONS
**		None
*/
int VacMainView::getAllHistoryInstaces(QList<int> &idList)
{
	return HistoryDialog::getAllInstances(idList);
}

/*
**	FUNCTION
**		Set 
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::setConfigName()
**
**	RETURNS
**		See HistoryDialog::setConfigName()
**
**	CAUTIONS
**		None
*/
int VacMainView::setHistoryConfigName(int id, const QString &name)
{
	return HistoryDialog::setConfigName(id, name);
}

/*
**	FUNCTION
**		Find IDs of all opened history instances.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::getConfigName()
**
**	RETURNS
**		See HistoryDialog::getConfigName()
**
**	CAUTIONS
**		None
*/
const QString &VacMainView::getHistoryConfigName(int id)
{
	return HistoryDialog::getConfigName(id);
}

/*
**	FUNCTION
**		Create instance of history dialog.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::create()
**
**	RETURNS
**		See HistoryDialog::create()
**
**	CAUTIONS
**		None
*/
int VacMainView::createHistory(void)
{
	return HistoryDialog::create();
}

int VacMainView::getHistoryFilterType(int instanceId)
{
	return HistoryDialog::getFilterType(instanceId);
}

int VacMainView::setHistoryFilterType(int instanceId, int type)
{
	return HistoryDialog::setFilterType(instanceId, type);
}

/*
**	FUNCTION
**		Create instance of history dialog.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::clear()
**
**	RETURNS
**		See HistoryDialog::clear()
**
**	CAUTIONS
**		None
*/
void VacMainView::clearHistory(int instanceId)
{
	HistoryDialog::clear(instanceId);
}

/*
**	FUNCTION
**		Get all items shown in history dialog.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::getAllItems()
**
**	RETURNS
**		See HistoryDialog::getAllItems()
**
**	CAUTIONS
**		None
*/
void VacMainView::getAllHistoryItems(int instanceId, QStringList &dps, QStringList &dpes, int **bitNbrs, QStringList &colors, QStringList &axisTypeNames)
{
	HistoryDialog::getAllItems(instanceId, dps, dpes, bitNbrs, colors, axisTypeNames);
}

int VacMainView::getHistoryAllComments(int id, QStringList &comments)
{
	return HistoryDialog::getAllComments(id, comments);
}

int VacMainView::setHistoryAllComments(int id, QStringList comments)
{
	return HistoryDialog::setAllComments(id, comments);
}

/*
**	FUNCTION
**		Get parameters of time scale in history dialog.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::getTimeScaleParam()
**
**	RETURNS
**		See HistoryDialog::getTimeScaleParam()
**
**	CAUTIONS
**		None
*/
int VacMainView::getHistoryTimeScaleParam(int instanceId, QDateTime &start, QDateTime &end, bool &online, bool &logScale)
{
	return HistoryDialog::getTimeScaleParam(instanceId, start, end, online, logScale);
}

/*
**	FUNCTION
**		Set parameters of time scale in history dialog.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::setTimeScaleParam()
**
**	RETURNS
**		See HistoryDialog::setTimeScaleParam()
**
**	CAUTIONS
**		None
*/
int VacMainView::setHistoryTimeScaleParam(int instanceId, const QDateTime &start, const QDateTime &end, bool online, bool logScale)
{
	return HistoryDialog::setTimeScaleParam(instanceId, start, end, online, logScale);
}

/*
**	FUNCTION
**		Get parameters of all vertical scales in history dialog.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::getVerticalScalesParams()
**
**	RETURNS
**		See HistoryDialog::getVerticalScalesParams()
**
**	CAUTIONS
**		See HistoryDialog::getVerticalScalesParams()
*/
int VacMainView::getVerticalScalesParams(int id, int &nScales, int **ppTypes, bool **ppLogs, float **ppMins, float **ppMaxs)
{
	return HistoryDialog::getVerticalScalesParams(id, nScales, ppTypes, ppLogs, ppMins, ppMaxs);
}

/*
**	FUNCTION
**		Set parameters of vertical scale in history dialog.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::setVerticalScaleParams()
**
**	RETURNS
**		See HistoryDialog::setVerticalScaleParams()
**
**	CAUTIONS
**		None
*/
int VacMainView::setVerticalScaleParams(int id, int type, bool log, float min, float max)
{
	return HistoryDialog::setVerticalScaleParams(id, type, log, min, max);
}

/*
**	FUNCTION
**		Return title of history dialog.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::getTitle()
**
**	RETURNS
**		See HistoryDialog::getTitle()
**
**	CAUTIONS
**		None
*/
const QString &VacMainView::getHistoryTitle(int id)
{
	return HistoryDialog::getTitle(id);
}

/*
**	FUNCTION
**		Set title of history dialog.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::setTitle()
**
**	RETURNS
**		See HistoryDialog::setTitle()
**
**	CAUTIONS
**		None
*/
int VacMainView::setHistoryTitle(int id, const char *title)
{
	return HistoryDialog::setTitle(id, title);
}
	
/*
**	FUNCTION
**		Return geometry of history dialog.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::getWindowGeometry()
**
**	RETURNS
**		See HistoryDialog::getWindowGeometry()
**
**	CAUTIONS
**		None
*/
int VacMainView::getHistoryWindowGeometry(int id, int &x, int &y, int &w, int &h)
{
	return HistoryDialog::getWindowGeometry(id, x, y, w, h);
}

/*
**	FUNCTION
**		Set geometry of history dialog.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::setWindowGeometry()
**
**	RETURNS
**		See HistoryDialog::setWindowGeometry()
**
**	CAUTIONS
**		None
*/
int VacMainView::setHistoryWindowGeometry(int id, int x, int y, int w, int h)
{
	return HistoryDialog::setWindowGeometry(id, x, y, w, h);
}

/*
**	FUNCTION
**		Add device to analog value history, open value history dialog if
**		not open yet.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::addDevice()
**
**	RETURNS
**		See HistoryDialog::addDevice()
**
**	CAUTIONS
**		None
*/
int VacMainView::addToHistory(Eqp *pEqp, int historyDepth)
{
	return HistoryDialog::addDevice(pEqp, historyDepth);
}

/*
**	FUNCTION
**		Add device to analog value history dialog with given ID
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::addDevice()
**
**	RETURNS
**		See HistoryDialog::addDevice()
**
**	CAUTIONS
**		None
*/
int VacMainView::addToHistory(int instanceId, Eqp *pEqp, const char *colorString, int historyDepth)
{
	return HistoryDialog::addDevice(instanceId, pEqp, colorString, historyDepth);
}

/*
**	FUNCTION
**		Add Dpe of device to analog value history, open value history dialog if
**		not open yet.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::addDeviceDpe()
**
**	RETURNS
**		See HistoryDialog::addDeviceDpe()
**
**	CAUTIONS
**		None
*/
int VacMainView::addDpeToHistory(Eqp *pEqp, const char *dpeName, const char *yAxisName, int historyDepth)
{
	return HistoryDialog::addDeviceDpe(pEqp, dpeName, yAxisName, historyDepth);
}

/*
**	FUNCTION
**		Add Dpe of device to analog value history, open value history dialog dialog with given ID
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::addDeviceDpe()
**
**	RETURNS
**		See HistoryDialog::addDeviceDpe()
**
**	CAUTIONS
**		None
*/
int VacMainView::addDpeToHistory(int instanceId, Eqp *pEqp, const char *dpeName, const char *yAxisName, const char *colorString, int historyDepth)
{
	return HistoryDialog::addDeviceDpe(instanceId, pEqp, dpeName, yAxisName, colorString, historyDepth);
}

/*
**	FUNCTION
**		Add one bit of device state to analog value history, open value history dialog if
**		not open yet.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::addDeviceBit()
**
**	RETURNS
**		See HistoryDialog::addDeviceBit()
**
**	CAUTIONS
**		None
*/
int VacMainView::addToBitHistory(Eqp *pEqp, const char *dpe, int bitNbr, const char *bitName, int historyDepth)
{
	return HistoryDialog::addDeviceBit(pEqp, dpe, bitNbr, bitName, historyDepth);
}

/*
**	FUNCTION
**		Add one bit of device state to analog value history dialog with given ID
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::addDeviceBit()
**
**	RETURNS
**		See HistoryDialog::addDeviceBit()
**
**	CAUTIONS
**		None
*/
int VacMainView::addToBitHistory(int instanceId, Eqp *pEqp, const char *dpe, int bitNbr, const char *bitName, const char *colorString, int historyDepth)
{
	return HistoryDialog::addDeviceBit(instanceId, pEqp, dpe, bitNbr, bitName, colorString, historyDepth);
}

/*
**	FUNCTION
**		Add history value for one data range of one DPE in history.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::addHistoryValue()
**
**	RETURNS
**		See HistoryDialog::addHistoryValue()
**
**	CAUTIONS
**		None
*/
bool VacMainView::addHistoryValue(int id, const char *dpe, bool bitState, int bitNbr, const QDateTime &time, const QVariant &value)
{
	bool historyDialog = HistoryDialog::addHistoryValue(id, dpe, bitState, bitNbr, time, value);
	bool mobileHistoryDialog = MobileHistoryDialog::addHistoryValue(id, dpe, time, value);
	return historyDialog || mobileHistoryDialog;
}

/*
**	FUNCTION
**		Notify history dialog that all values for one data range of one DPE 
**		have been added.
**		In fact, this method just passes call to HistoryDialog
**
**	ARGUMENTS
**		See HistoryDialog::finishHistoryRange()
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainView::finishHistoryRange(int id, const char *dpe, bool bitState, int bitNbr)
{
	HistoryDialog::finishHistoryRange(id, dpe, bitState, bitNbr);
	MobileHistoryDialog::finishHistoryRange(id, dpe);
}

/*
**	FUNCTION
**		Report absolute start time of available history data for given DP
**
**	ARGUMENTS
**		dpName			- DP name
**		absStartTime	- Start time of 1st available history data
**
**	RETURNS
**		1	= Mobile history dialog is opened;
**		0	= mobile history dialog is not opened, data are not needed anymore
**
**	CAUTIONS
**		None
*/
int VacMainView::setDpHistoryDepth(const char *dpName, const QDateTime &absStartTime)
{
	return MobileHistoryDialog::setDpHistoryDepth(dpName, absStartTime);
}

/*
**	FUNCTION
**		Open sector view dialog on top of main view widget
**
**	ARGUMENTS
**		vacTypes	- Vacuum type mask for equipment to be shown dialog
**		firstSector	- Name of first sector for equipment selection
**		lastSector	- Name of last sector for equipment selection
**		mode		- Data aqcuisition mode
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openSectorView(unsigned /* vacTypes */, const char *firstSector,
	const char *lastSector, DataEnum::DataMode mode, QStringList &errList)
{
	DataPool &pool = DataPool::getInstance();
	Sector *pFirstSector = pool.findSectorData(firstSector);
	if(!pFirstSector)
	{
		errList.append(QString("Unknown first sector <%1>").arg(firstSector));
		return -1;
	}
	Sector *pLastSector = pool.findSectorData(lastSector);
	if(!pLastSector)
	{
		errList.append(QString("Unknown last sector <%1>").arg(lastSector));
		return -1;
	}
	SectorViewDialog *pDialog = new SectorViewDialog(pFirstSector, pLastSector, mode);
	pDialog->show();
	return 1;
}

/*
**	FUNCTION
**		Open synoptic dialog on top of main view widget
**
**	ARGUMENTS
**		vacTypes	- Vacuum type mask for equipment to be shown dialog
**		firstSector	- Name of first sector for equipment selection
**		lastSector	- Name of last sector for equipment selection
**		mode		- Data aqcuisition mode
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openSynoptic(unsigned /* vacTypes */, const char *firstSector,
	const char *lastSector, DataEnum::DataMode mode, QStringList &errList)
{
	DataPool &pool = DataPool::getInstance();
	Sector *pFirstSector = pool.findSectorData(firstSector);
	if(!pFirstSector)
	{
		errList.append(QString("Unknown first sector <%1>").arg(firstSector));
		return -1;
	}
	Sector *pLastSector = pool.findSectorData(lastSector);
	if(!pLastSector)
	{
		errList.append(QString("Unknown last sector <%1>").arg(lastSector));
		return -1;
	}
	SynopticDialog *pDialog = new SynopticDialog(pFirstSector, pLastSector, mode);
	pDialog->show();
	return 1;
}

/*
**	FUNCTION
**		Open pressure profile dialog on top of main view widget
**
**	ARGUMENTS
**		vacTypes	- Vacuum type mask for equipment to be shown dialog
**		firstSector	- Name of first sector for equipment selection
**		lastSector	- Name of last sector for equipment selection
**		mode		- Data acquisition mode
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openProfile(unsigned /* vacTypes */, const char *firstSector,
		const char *lastSector, DataEnum::DataMode mode, QStringList &errList)
{
	DataPool &pool = DataPool::getInstance();
	Sector *pFirstSector = pool.findSectorData(firstSector);
	if(!pFirstSector)
	{
		errList.append(QString("Unknown first sector <%1>").arg(firstSector));
		return -1;
	}
	Sector *pLastSector = pool.findSectorData(lastSector);
	if(!pLastSector)
	{
		errList.append(QString("Unknown last sector <%1>").arg(lastSector));
		return -1;
	}
	ProfileDialog *pDialog = new ProfileDialog(pFirstSector, pLastSector, mode);
	pDialog->show();
	return 1;
}

/*
**	FUNCTION
**		Open pressure profile dialog on top of main view widget
**
**	ARGUMENTS
**		vacTypes	- Vacuum type mask for equipment to be shown dialog
**		firstSector	- Name of first sector for equipment selection
**		lastSector	- Name of last sector for equipment selection
**		mode		- Data acquisition mode
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openProfileAllSPS(unsigned /* vacTypes */, const char * /* firstSector */,
		const char * /*lastSector */, DataEnum::DataMode /* mode */, QStringList & /*errList */)
{
	ProfileAllSPS *pDialog = new ProfileAllSPS();
	pDialog->show();
	return 1;
}

/*
**	FUNCTION
**		Open LHC synoptic dialog on top of main view widget
**
**	ARGUMENTS
**		vacTypes	- Vacuum type mask for equipment to be shown dialog
**		firstSector	- Name of first sector for equipment selection
**		lastSector	- Name of last sector for equipment selection
**		mode		- Data aqcuisition mode
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openSynopticLhc(unsigned vacTypes, const char *firstSector,
	const char *lastSector, DataEnum::DataMode mode, QStringList &errList)
{
	DataPool &pool = DataPool::getInstance();
	Sector *pFirstSector = pool.findSectorData(firstSector);
	if(!pFirstSector)
	{
		errList.append(QString("Unknown first sector <%1>").arg(firstSector));
		return -1;
	}
	Sector *pLastSector = pool.findSectorData(lastSector);
	if(!pLastSector)
	{
		errList.append(QString("Unknown last sector <%1>").arg(lastSector));
		return -1;
	}
	SynopticLhcDialog *pDialog = new SynopticLhcDialog(vacTypes, pFirstSector, pLastSector, mode);
	pDialog->show();
	return 1;
}

/*
**	FUNCTION
**		Open LHC profile dialog on top of main view widget
**
**	ARGUMENTS
**		vacTypes	- Vacuum type mask for equipment to be shown dialog
**		firstSector	- Name of first sector for equipment selection
**		lastSector	- Name of last sector for equipment selection
**		mode		- Data aqcuisition mode
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openProfileLhc(unsigned vacTypes, const char *firstSector,
	const char *lastSector, DataEnum::DataMode mode, QStringList &errList)
{
	DataPool &pool = DataPool::getInstance();
	Sector *pFirstSector = pool.findSectorData(firstSector);
	if(!pFirstSector)
	{
		errList.append(QString("Unknown first sector <%1>").arg(firstSector));
		return -1;
	}
	Sector *pLastSector = pool.findSectorData(lastSector);
	if(!pLastSector)
	{
		errList.append(QString("Unknown last sector <%1>").arg(lastSector));
		return -1;
	}
	ProfileLhcDialog *pDialog = new ProfileLhcDialog(vacTypes, pFirstSector, pLastSector, mode);
	pDialog->show();
	return 1;
}

/*
**	FUNCTION
**		Open all LHC profile dialog (inner/outer beam) on top of main view widget
**
**	ARGUMENTS
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openProfileAllLhcInOut(QStringList & /* errList */)
{
	if(ProfileAllLHCInOut::alreadyOpen())
	{
		return 1;
	}
	ProfileAllLHCInOut *pDialog = new ProfileAllLHCInOut();
	pDialog->show();
	return 1;
}

/*
**	FUNCTION
**		Open all LHC profile dialog (blue/red beam) on top of main view widget
**
**	ARGUMENTS
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openProfileAllLhcBeam(QStringList & /* errList */)
{
	if(ProfileAllLHCBeam::alreadyOpen())
	{
		return 1;
	}
	ProfileAllLHCBeam *pDialog = new ProfileAllLHCBeam();
	pDialog->show();
	return 1;
}

/*
**	FUNCTION
**		Open all LHC synoptic dialog (isolation vacuums) on top of main view widget
**
**	ARGUMENTS
**		mode		- Data acquisition mode
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openSynopticAllLhc(DataEnum::DataMode mode, QStringList & /* errList */ )
{
	if(SynopticAllLhc::alreadyOpen(mode))
	{
		return 1;
	}
	SynopticAllLhc *pDialog = new SynopticAllLhc(mode);
	pDialog->show();
	return 1;
}

/*
**	FUNCTION
**		Open all LHC synoptic dialog (isolation vacuums) on top of main view widget
**
**	ARGUMENTS
**		firstSector	- Name of first sector for equipment selection
**		lastSector	- Name of last sector for equipment selection
**		mode		- Data aqcuisition mode
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openSectorViewLhc(const char *firstSector, const char *lastSector,
	DataEnum::DataMode mode, QStringList &errList)
{
	DataPool &pool = DataPool::getInstance();
	Sector *pFirstSector = pool.findSectorData(firstSector);
	if(!pFirstSector)
	{
		errList.append(QString("Unknown first sector <%1>").arg(firstSector));
		return -1;
	}
	Sector *pLastSector = pool.findSectorData(lastSector);
	if(!pLastSector)
	{
		errList.append(QString("Unknown last sector <%1>").arg(lastSector));
		return -1;
	}
	SectorViewLhcDialog *pDialog = new SectorViewLhcDialog(pFirstSector, pLastSector, mode);
	pDialog->show();
	return 1;
}


/*
**	FUNCTION
**		Open mobile equipment history dialog on top of main view widget
**
**	ARGUMENTS
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openMobileHistory(QStringList & /* errList */)
{
	MobileHistoryDialog::create();
	return 1;
}

/*
**	FUNCTION
**		Open mobile bakeout rack profile dialog on top of main view widget
**
**	ARGUMENTS
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openBakeoutRackProfile(QStringList & /* errList */)
{
	BakeoutRackProfileDialog::create();
	return 1;
}


/*
**	FUNCTION
**		Open mobile bakeout rack details dialog on top of main view widget
**
**	ARGUMENTS
**		errList		- Variable where error message(s) will be added in case
**						of error.
**
**	RETURNS
**		1	= success;
**		-1	= fatal error
**
**	CAUTIONS
**		None
*/
int VacMainView::openBakeoutRackDetails(QStringList & /* errList */)
{
	BakeoutRackDetailsDialog::create();
	return 1;
}

int VacMainView::openRackDiagram(const char * equipmentName, QStringList & /* errList */)
{
	
	DataPool &pool = DataPool::getInstance();
	//Rack * rack = pool.getEquipmentParentRack(QString(equipmentName));
	//pool.fillBuildingRackList(rack->getRackName());
	QString buildingName = pool.getRackBuildingOfEquipment(equipmentName);
	RackDocumentationDialog *pRack = new RackDocumentationDialog(buildingName);
	pRack->showAllRacks();
	pRack->selectEquipment(equipmentName);
	pRack->reCalculateSize();
	pRack->show();
	return 1;
}

int VacMainView::openInterlockSchematic(const char * equipmentName, QStringList & /* errList */)
{
	InterlockDocumentationDialog *pInterlock = new InterlockDocumentationDialog(equipmentName);
	pInterlock->show();
	return 1;
}

/*
**	FUNCTION
**		Notification call that arrived when new dialog is opened
**		in replay mode.
**		Add dialog to list of dialogs opened in replay mode
**
**	ARGUMENTS
**		dialog	- Pointer to new dialog widget opened in replay mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainView::replayDialogOpened(QWidget *dialog)
{
	replayDialogs.append(dialog);
}

/*
**	FUNCTION
**		Notification call that arrived when new dialog, opened
**		in replay mode, is being deleted/
**		Remove dialog from list of dialogs opened in replay mode
**
**	ARGUMENTS
**		dialog	- Pointer to dialog widget being deleted
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainView::replayDialogDeleted(QWidget *dialog)
{
	replayDialogs.removeOne(dialog);
}

/*
**	FUNCTION
**		Delete all dialogs opened in Replay mode
**
**	ARGUMENTS
**		dialog	- Pointer to dialog widget being deleted
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainView::deleteReplayDialogs(void)
{
	foreach(QWidget *pWidget, replayDialogs)
	{
		pWidget->deleteLater();
	}
	replayDialogs.clear();
}

/*
**	FUNCTION
**		Print given widget. For the moment simple approach is used:
**		print pixmap taken from that widget
**
**	ARGUMENTS
**		pWidget	- Pointer to widget to pint
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainView::printWidget(QWidget *pWidget)
{
	if(!pPrinter)
	{
		pPrinter = new QPrinter(QPrinter::HighResolution);
	}
	QPrintDialog *pDialog = new QPrintDialog(pPrinter, pWidget);
	if(pDialog->exec() != QDialog::Accepted)
	{
		return;
	}
	QPixmap pix = QPixmap::grabWidget(pWidget);
	QPainter painter;
	painter.begin(pPrinter);

//qDebug("viewport [%d x %d] widget [%d x %d]\n",
//painter.viewport().width(), painter.viewport().height(), pWidget->width(), pWidget->height());
//fprintf(stderr, "viewport [%d x %d] widget [%d x %d]\n",
//painter.viewport().width(), painter.viewport().height(), pWidget->width(), pWidget->height());
	double xScale = painter.viewport().width() / (double)pWidget->width();
	double yScale = painter.viewport().height() / (double)pWidget->height();
	double scale = xScale < yScale ? xScale : yScale;
	painter.scale(scale, scale);
	painter.drawPixmap(0, 0, pix);
	painter.end(); 
}

