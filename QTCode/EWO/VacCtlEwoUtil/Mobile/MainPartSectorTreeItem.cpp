//	Implementation of MainPartSectorTreeItem class
/////////////////////////////////////////////////////////////////////////////////

#include "MainPartSectorTreeItem.h"

#include "MobileColorList.h"

#include <qpixmap.h>


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

MainPartSectorTreeItem::MainPartSectorTreeItem(QTreeWidget *parent, const QString &text, int vacType)
	: QTreeWidgetItem(parent, MobileTreeItemMpSector)
{
	setText(0, text);
	this->vacType = vacType;
	QPixmap *pm = MobileColorList::getVacIcon(vacType, false);
	setIcon(0, QIcon(*pm));
}

MainPartSectorTreeItem::MainPartSectorTreeItem(QTreeWidgetItem *parent, const QString &text, int vacType)
	: QTreeWidgetItem(parent, MobileTreeItemMpSector)
{
	setText(0, text);
	this->vacType = vacType;
	QPixmap *pm = MobileColorList::getVacIcon(vacType, false);
	setIcon(0, QIcon(*pm));
}

MainPartSectorTreeItem::~MainPartSectorTreeItem()
{
}

/*
**	FUNCTION
**		Open/close this item
**
**	ARGUMENTS
**		open	- true if item shall be open
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainPartSectorTreeItem::setExpanded(bool open)
{
	QPixmap *pm = MobileColorList::getVacIcon(vacType, open);
	setIcon(0, QIcon(*pm));
	QTreeWidgetItem::setExpanded(open);
}

