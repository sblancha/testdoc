//	Implementation of BakeoutTree class
/////////////////////////////////////////////////////////////////////////////////

#include "BakeoutTree.h"
#include "MainPartSectorTreeItem.h"
#include "EqpTreeItem.h"

#include "Eqp.h"
#include "MobilePool.h"
#include "MobileType.h"
#include "DataPool.h"

BakeoutTree::BakeoutTree(QWidget *parent) : QTreeWidget(parent)
{
	pSelectedEqp = NULL;

	setColumnCount(1);
	QTreeWidgetItem *pHead = new QTreeWidgetItem();
	pHead->setText(0, "Bakeout Racks");
	setHeaderItem(pHead);
	setRootIsDecorated(true);
	connect(this, SIGNAL(itemClicked(QTreeWidgetItem *, int)),
		this, SLOT(treeItemClicked(QTreeWidgetItem *, int)));

	// Set some minimum width
	QRect rect = fontMetrics().boundingRect("Bakeout BAKEOUT.ARC4-5.B");
	setMinimumSize(QSize(rect.width(), 100));

	// Connect to mobile states
	MobilePool &pool = MobilePool::getInstance();
	connect(&pool, SIGNAL(activeListChanged(DataEnum::DataMode)),
		this, SLOT(eqpListChanged(DataEnum::DataMode)));
	rebuildTree();
}

BakeoutTree::~BakeoutTree()
{
}

/*
**	FUNCTION
**		Slot activated when list of active mobile equipment has been changed
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutTree::eqpListChanged(DataEnum::DataMode mode)
{
	if(mode != DataEnum::Online)
	{
		return;
	}
	rebuildTree();
}

void BakeoutTree::rebuildTree(void)
{
	bool selectedExists = false;
	clear();

	const QList<Eqp *> &activeList = MobilePool::getInstance().getActive(DataEnum::Online);
	if(!activeList.count())
	{
		pSelectedEqp = NULL;
		emit selectChanged(pSelectedEqp);
		return;
	}

	// Build list of sectors used
	QList<Sector *> sectorList;
	int idx;
	for(idx = 0 ; idx < activeList.count() ; idx++)
	{
		Eqp *pEqp = activeList.at(idx);
		if(pEqp->getMobileType() != MobileType::OnSector)
		{
			continue;
		}
		Sector *pSector = pEqp->getSectorBefore();
		if(pSector)
		{
			if(pEqp == pSelectedEqp)
			{
				selectedExists = true;
			}
			if(sectorList.indexOf(pSector) < 0)
			{
				sectorList.append(pSector);
			}
		}
	}

	// Build content
	const QList<Sector *> &sectors = DataPool::getInstance().getSectors();
	QTreeWidgetItem *pMpItem = NULL;
	int eqpIndex = 0;
	for(idx = 0 ; idx < sectors.count() ; idx++)
	{
		Sector *pSector = sectors.at(idx);
		if(sectorList.indexOf(pSector) < 0)
		{
			continue;
		}

		QTreeWidgetItem *pSectItem = addSectorToTree(pSector, &pMpItem);
		addSectorEqp(pSectItem, pSector, activeList, eqpIndex);
	}
	if(!selectedExists)
	{
		pSelectedEqp = NULL;
	}
	emit selectChanged(pSelectedEqp);
}

/*
**	FUNCTION
**		Add sector item to tree
**
**	ARGUMENTS
**		pSector		- Pointer to sector to be added
**		ppMpItem	- Address of pointer to last used main part item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
QTreeWidgetItem *BakeoutTree::addSectorToTree(Sector *pSector, QTreeWidgetItem **ppMpItem)
{
	// Add (or find in tree) main part
	const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
	if(maps.count())
	{
		MainPartMap *pMap = maps.first();
		MainPart *pMainPart = pMap->getMainPart();
		QString mpName(pMainPart->getName());
		if(*ppMpItem)
		{
			if((*ppMpItem)->text(0) != mpName)
			{
				*ppMpItem = NULL;
			}
		}
		if(!(*ppMpItem))
		{
			for(int idx = 0 ; idx < topLevelItemCount() ; idx++)
			{
				QTreeWidgetItem *pMpItem = topLevelItem(idx);
				if(pMpItem->text(0) == mpName)
				{
					*ppMpItem = pMpItem;
					break;
				}
			}
		}
		if(!(*ppMpItem))
		{
			*ppMpItem = (QTreeWidgetItem *)(new MainPartSectorTreeItem(this, pMainPart->getName(), pMainPart->getVacTypeMask()));
		}
	}
	else
	{
		*ppMpItem = NULL;
	}

	// Add sector to either main part or to tree itself
	MainPartSectorTreeItem *pSectItem = NULL;
	if(*ppMpItem)
	{
		pSectItem = new MainPartSectorTreeItem(*ppMpItem, pSector->getName(), pSector->getVacType());
	}
	else
	{
		pSectItem = new MainPartSectorTreeItem(this, pSector->getName(), pSector->getVacType());
	}
	return pSectItem;
}

/*
**	FUNCTION
**		Add equipment of given sector to tree
**
**	ARGUMENTS
**		pSectItem		- Pointer to sector item in tree
**		pSector			- Pointer to sector
**		activeEqpIter	- Iterator for list of all active mobile equipment
**		eqpIndex		- Index of device in tree
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutTree::addSectorEqp(QTreeWidgetItem *pSectItem, Sector *pSector, const QList<Eqp *> &activeList,
	int &eqpIndex)
{
	for(int idx = 0 ; idx < activeList.count() ; idx++)
	{
		Eqp *pEqp = activeList.at(idx);
		if(pEqp->getSectorBefore() != pSector)
		{
			continue;
		}
		if(pEqp->getMobileType() != MobileType::OnSector)
		{
			continue;
		}
		new EqpTreeItem(pSectItem, pEqp, eqpIndex++);
	}
}

void BakeoutTree::treeItemClicked(QTreeWidgetItem *pItem, int /* column */)
{
	if(pItem->type() != MobileTreeItemEqp)
	{
		return;
	}

	// Checkbox setting could change as a result of click
	EqpTreeItem *pEqpItem = (EqpTreeItem *)pItem;
	Eqp *pNewEqp = NULL;
	if(pEqpItem->checkState(0) == Qt::Checked)
	{
		pNewEqp = pEqpItem->getEqp();
	}
	else
	{
		if(pEqpItem->getEqp() == pSelectedEqp)
		{
			pNewEqp = NULL;
		}
	}
	if(pSelectedEqp == pNewEqp)
	{
		return;
	}
	pSelectedEqp = pNewEqp;
	
	selectEqpInTree(NULL, pNewEqp);
	emit selectChanged(pSelectedEqp);
}

Eqp *BakeoutTree::findSelectedEqp(QTreeWidgetItem *pParent)
{
	if(pParent)
	{
		if(pParent->type() == MobileTreeItemEqp)
		{
			EqpTreeItem *pEqpItem = (EqpTreeItem *)pParent;
			return pEqpItem->checkState(0) == Qt::Checked ? pEqpItem->getEqp() : NULL;
		}
		else
		{
			for(int idx = 0 ; idx < pParent->childCount() ; idx++)
			{
				QTreeWidgetItem *pItem = pParent->child(idx);
				Eqp *pEqp = findSelectedEqp(pItem);
				if(pEqp)
				{
					return pEqp;
				}
			}
		}
	}
	else
	{
		for(int idx = 0 ; idx < topLevelItemCount() ; idx++)
		{
			QTreeWidgetItem *pItem = topLevelItem(idx);
			Eqp *pEqp = findSelectedEqp(pItem);
			if(pEqp)
			{
				return pEqp;
			}
		}
	}
	return NULL;
}

void BakeoutTree::selectEqpInTree(QTreeWidgetItem *pParent, Eqp *pEqp)
{
	if(pParent)
	{
		if(pParent->type() == MobileTreeItemEqp)
		{
			EqpTreeItem *pEqpItem = (EqpTreeItem *)pParent;
			Qt::CheckState state = pEqpItem->getEqp() == pEqp ? Qt::Checked : Qt::Unchecked;
			pEqpItem->setCheckState(0, state);
			if(state == Qt::Checked)
			{
				scrollToItem(pEqpItem);
			}
			return;
		}
		else
		{
			for(int idx = 0 ; idx < pParent->childCount() ; idx++)
			{
				QTreeWidgetItem *pItem = pParent->child(idx);
				selectEqpInTree(pItem, pEqp);
			}
		}
	}
	else
	{
		for(int idx = 0 ; idx < topLevelItemCount() ; idx++)
		{
			QTreeWidgetItem *pItem = topLevelItem(idx);
			selectEqpInTree(pItem, pEqp);
		}
	}
}

