#ifndef	VACICONCRYO_TT_SUM_H
#define	VACICONCRYO_TT_SUM_H

// Icon for CRYO_TT_SUM - summary of cold mass (CM) and beam screen (BS) CRYO thermometers

#include "VacIcon.h"

class EqpCRYO_TT_SUM;

class VACCTLEWOUTIL_EXPORT VacIconCRYO_TT_SUM : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconCRYO_TT_SUM(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconCRYO_TT_SUM();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpCRYO_TT_SUM *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	void setPainter(QPainter &painter, unsigned rr1, float t, bool isBs);
};


#endif	// VACICONCRYO_TT_SUM_H
