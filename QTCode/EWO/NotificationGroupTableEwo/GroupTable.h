#ifndef	GROUPTABLE_H
#define	GROUPTABLE_H

#include "GroupTableModel.h"
#include "SmsGroup.h"
#include <QTableView>
/**
@brief Table for Predefined group of configurations - base widget for corresponding EWO
*/
class GroupTable : public QTableView {
	Q_OBJECT
public:
	GroupTable(QWidget *parent = 0);
	~GroupTable();

	const QString getSelectedGroup(void);

	// Access
	GroupTableModel *getModel(void) { return &model; }

protected:
	// Model
	GroupTableModel	model;
	void resizeEvent(QResizeEvent *pEvent);
};

#endif	// GROUPTABLE_H
