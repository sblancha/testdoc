//	Implementation of VacIconEwo class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconEwo.h"

EWO_PLUGIN(VacIconEwo)


VacIconEwo::VacIconEwo(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new VacIconContainer(parent);
	connect(baseWidget, SIGNAL(iconMouseDown(int, int, int, int, int, const char *)),
		this, SLOT(iconMousePress(int, int, int, int, int, const char *)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList VacIconEwo::signalList(void) const
{
	QStringList list;
	list.append("IconMousePress(int button, int mode, int x, int y, int extra, string dpName)");
	return list;
}

/*
**	FUNCTION
**		Slot receiving iconMousePress signal of VacContainer, emit
**		signal 'IconMousePress' to PVSS
**
**	ARGUMENTS
**		button	- mouse button number
**		mode	- Data acquisition mode
**		x		- X coordinate of mouse pointer
**		y		- Y coordinate of mouse pointer
**		extra	- Extra parameter to identify PART of icon
**		dpName	- Name of DP where mouse press occured
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconEwo::iconMousePress(int button, int mode, int x, int y, int extra, const char *dpName)
{
	QList<QVariant> args;
	args.append(QVariant(button));
	args.append(QVariant(mode));
	args.append(QVariant(x));
	args.append(QVariant(y));
	args.append(QVariant(extra));
	args.append(QVariant(QString(dpName)));
	emit signal("IconMousePress", args);
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList VacIconEwo::methodList(void) const
{
	QStringList list;
	list.append("bool connect()");
	list.append("bool disconnect()");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant VacIconEwo::invokeMethod(const QString &name, QList<QVariant> & /* values */,
	QString &error)
{
	if(name == "connect")
	{
		bool coco = baseWidget->connect();
		return QVariant(coco);
	}
	else if(name == "disconnect")
	{
		bool coco = baseWidget->disconnect();
		return QVariant(coco);
	}
	else
	{
		error = "Uknown method VacIconEwo.";
		error += name;
	}
	return QVariant();
}
