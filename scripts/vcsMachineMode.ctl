#uses "VacCtlEqpDataPvss"	// Load DLL
#uses "vclEqpConfig.ctl"
//#uses "lhcVacSectors.ctl"
#uses "vclMachineMode.ctl"
#uses "vclBeamModeEmailConfig.ctl"
#uses "vclSectors.ctl"	      // Load CTRL library
#uses "email.ctl"	      // Load PVSS standard CTRL library
#uses "vclDevCommon.ctl"	// Load CTRL library
#uses "vclResources.ctl"       // Load CTRL library
#uses "vclMachine.ctl"		// Load CTRL library
//#uses "VacEqpData"
//#uses "lhcVacMachine.ctl"
/**@name SCRIPT: vcsMachineMode.ctl

@author: Leonid Kopylov (IHEP, Protvino, Russia)

Creation Date: 22/02/2008

Modification History:
  April 2008  S.Merker
    Two independent beam modes - for R and B beams

  15.05.2008  L.Kopylov
    Switch from 'PLC-based' to 'SVCU chassis-based' grouping of valves.
    Make problem reporting if beam modes is different in different chassis.

  04.05.2009  L.Kopylov
    Completely new approach:
    - output has 3 states:
      0 = no beam
      1 = beam
      2 = inconsistency
    - still one valve per SVCU chassis is checked. Valves on injection
      lines are not checked (for the moment it does not require any tricks: their
      names do not follow .R / .B convention).
    - valves with valid bit NOT set are not taken into account
    - beam is ON if all valves report 'close disable'
    - beam is OFF if all valves report 'close enable'
    - inconsitency: some valve(s) report 'close disable', some other valve(s) report 'close enable'

version 1.0

Purpose: 
Collect information, conrning machine mode, and write results to DPE
where other PVSS code can easily read this data.
First version realizes the only mode - 'beam' mode in LHC machine.
Value is obtained from 'Close enable' bit of LHC valves

Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
  . global variable: the following variables are global to the script
    . lhcChassisB: List of SVCU chassis in LHC where valves B are connected togeteher
        with DP names of valves in this chassis. First item in list is chassis name, next
			items are valve DP names: dyn_dyn_string
		. lhcChassisR: List of SVCU chassis in LHC where valves R are connected togeteher
			with DP names of valves in this chassis. First item in list is chassis name, next
			items are valve DP names: dyn_dyn_string
		. lhcChassisX: List of SVCU chassis in LHC where valves X are connected togeteher
			with DP names of valves in this chassis. First item in list is chassis name, next
			items are valve DP names: dyn_string
		. usedValvesBeamB: List valve parameters for every item from lhcChassisBeamB: dyn_dyn_anytype.
		. usedValvesBeamR: List valve parameters for every item from lhcChassisBeamR: dyn_dyn_anytype.
		. usedValvesBeamX: List valve parameters for every item from lhcChassisBeamX: dyn_dyn_anytype.
			Three lists above are used for calculation of beam mode. Three values are stored for
			every valve:
				usedValvesBeamN[chassisIdx][1] - DP name of valve in chassis with index cassisIdx - string
				usedValvesBeamN[chassisIdx][2] - state (RR1) of valve in chassis with index chassisIdx - unsigned
				usedValvesBeamN[chassisIdx][3] - PLC health for PLC where the valve is connected - int
				usedValvesBeamN[chassisIdx][4] - Beam mode: 0=unknown, 1=beam on, 2=beam off - int
	. constant:
	. data point type needed: _VacMachineInfo, VVS_LHC
	. data point:
	. PVSS version: 3.1
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/

// Index of valve name in usedValvesBeamN for one chassis
const int LHC_VALVE_NAME = 1;

// Index of valve state in usedValvesBeamN for one chassis
const int LHC_VALVE_STATE = 2;

// Index of PLC alarm in usedValvesBeamN for one chassis
const int LHC_PLC_ALARM = 3;

// Index of claculated beam mode in usedValvesBeamN for one chassis, values - see below
const int LHC_BEAM_MODE = 4;


const int VALVE_INVALID = 0;
const int VALVE_CLOSE_DISABLE = 1;
const int VALVE_CLOSE_ENABLE = 2;

// Name of DP where resulting machine mode shall be written
string	modeDpName;

// List of SVCU chassis names for B beam line
dyn_dyn_string lhcChassisBeamB;

// List of SVCU chassis names for R beam line
dyn_dyn_string lhcChassisBeamR;

// List of SVCU chassis names for X beam line
dyn_dyn_string lhcChassisBeamX;

// List of selected valve parameters in every chassis at B beam line
dyn_dyn_anytype usedValvesBeamB;

// List of selected valve parameters in every chassis at R beam line
dyn_dyn_anytype usedValvesBeamR;

// List of selected valve parameters in every chassis at X beam line
dyn_dyn_anytype usedValvesBeamX;





// Index of inconsistency vacuum type
const int INCONSISTENCY_VAC = 1;

// Index of active flag for inconsistency
const int INCONSISTENCY_ACTIVE = 2;

// Index of time when inconsistency was detected
const int INCONSISTENCY_TIME = 3;

// Index of time when message for this inconsistency was sent
const int INCONSISTENCY_MSG_TIME = 4;

// Index of number of messages sent for this inconsistency
const int INCONSISTENCY_MSG_NUMBER = 5;

// List of beam mode inconsistencies detected, see constants
// above for meaning of fields
dyn_dyn_anytype	inconsistencies;

// Names of chassis who reported beam mode ON for inconsistency
dyn_dyn_string	inconsistencyChassisOn;

// Names of chassis who reported beam mode OFF for inconsistency
dyn_dyn_string	inconsistencyChassisOff;

// Name of E-mail server for notification
string emailServer;

// Name of E-mail client (domain) for notification
string emailClient;

// Address of E-mail sender for notification
string emailSender;

// List of recipient addresses for notification
dyn_string emailRecipients;



// Flag to enable debugging messages
bool debug = false;

main()
{
  DebugTN("vcsMachineMode: initializing...");

  // Initialize machine equipment data
  if(!InitEquipmentData())
  {
    return;
  }

  InitLhcValves();
  if((dynlen(lhcChassisBeamB) + dynlen(lhcChassisBeamR) + dynlen(lhcChassisBeamX)) == 0)
  {
    DebugTN("vcsMachineMode: no valves in chassis, exiting...");
    return;
  }

  // Connect to E-mail notification config DP
  string      configDp;
  dyn_string  exceptionInfo;
  VacBeamModeEmailGetConfigDp(configDp, exceptionInfo);
  if(configDp == "")
  {
    DebugTN("vcsMachineMode: no configuration for E-mail, inconsistencies will not be reported");
  }
  else
  {
    dpConnect("EmailConfigCb",
      configDp + ".server",
      configDp + ".client",
      configDp + ".sender",
      configDp + ".recipients");
  }

  // Check from time to time if other valves shall be used
  while(1)
  {
    delay(20);
    CheckValveUsage();
    CheckIncosistencyMessages();
  }
}

// CheckIncosistencyMessages
/**
Purpose:
Check if message(s) for beam mode inconsistency shall be sent.
If required - prepare and send such messages

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void CheckIncosistencyMessages()
{
  if(debug)
  {
    DebugTN("vcsMachineMode.ctl: CheckIncosistencyMessages(): " + dynlen(emailRecipients) + " recipient(s)");
  }
  if(dynlen(emailRecipients) == 0)
  {
    return;
  }
  time now = getCurrentTime();
  for(int n = dynlen(inconsistencies) ; n > 0 ; n--)
  {
    if(debug)
    {
      DebugTN("vcsMachineMode.ctl: CheckIncosistencyMessages() type = " +
        inconsistencies[n][INCONSISTENCY_VAC]);
    }
    if(!inconsistencies[n][INCONSISTENCY_ACTIVE])
    {
      continue;
    }
    if(debug)
    {
      DebugTN("vcsMachineMode.ctl: CheckIncosistencyMessages() ACTIVE");
    }
    if((now - inconsistencies[n][INCONSISTENCY_TIME]) < 30)
    {
      continue;	// there can be transition time - chassis recive signal not simultaneosuly
    }
    if(debug)
    {
      DebugTN("vcsMachineMode.ctl: CheckIncosistencyMessages() old enough");
    }
    int hoursToWait = inconsistencies[n][INCONSISTENCY_MSG_NUMBER];
    if(hoursToWait > 8)
    {
      hoursToWait = 8;
    }
    if((now - inconsistencies[n][INCONSISTENCY_MSG_TIME]) < (hoursToWait * 1800))
    {
      continue;	// Do not report too often - every N hours is OK
    }
    if(debug)
    {
      DebugTN("vcsMachineMode.ctl: CheckIncosistencyMessages() message not sent");
    }
    // Prepare and send message
    string subject = "", body = "";
    BuildInconsistencyMessage(inconsistencies[n][INCONSISTENCY_VAC], inconsistencyChassisOn[n],
      inconsistencyChassisOff[n], subject, body);
    dyn_string	email = makeDynString("");
    for(int i = dynlen(emailRecipients) ; i > 0 ; i--)
    {
      if(email[1] != "")
      {
        email[1] += ";";
      }
      email[1] += emailRecipients[i];
    }
    email[2] = emailSender;
    email[3] = subject;
    email[4] = body;
    if(debug)
    {
      DebugTN("vcsMachineMode.ctl: message to send is '" + subject + "'", body);
    }
    int coco;
    emSendMail(emailServer, emailClient, email, coco);
    if(coco < 0)	// Failed, make pause then try again
    {
      if(debug)
      {
        DebugTN("vcsMachineMode.ctl: emSendMail() failed");
      }
      continue;
    }
    inconsistencies[n][INCONSISTENCY_MSG_TIME] = getCurrentTime();
    inconsistencies[n][INCONSISTENCY_MSG_NUMBER]++;
  }
}

// BuildInconsistencyMessage
/**
Purpose:
Build subject and body text to be sent in E-mail for beam mode inconsistency
detected.

Arguments:
	vacType	- [in] Vacuum type of beam mode inconsistency
	chassisOn - [in] List of SVCU chassis names which report beam mode ON
	chassisOff - [in] List of SVCU chassis names which report beam mode OFF
	subject - [out] String for message subject will be returned here
	body - [out] String for message body will be returned here
Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void BuildInconsistencyMessage(int vacType, dyn_string chassisOn, dyn_string chassisOff,
  string &subject, string &body)
{
  switch(vacType)
  {
  case LVA_VACUUM_BLUE_BEAM:
    subject = "Inconsistency BLUE beam mode";
    break;
  case LVA_VACUUM_RED_BEAM:
    subject = "Inconsistency RED beam mode";
    break;
  case LVA_VACUUM_CROSS_BEAM:
    subject = "Inconsistency X beam mode";
    break;
  }
  int nOn = dynlen(chassisOn), nOff = dynlen(chassisOff);
  bool needBody = false;
  if(nOn < nOff)
  {
    if(nOn == 1)
    {
      subject += " ON: chassis " + chassisOn[1];
    }
    else
    {
      needBody = true;
    }
  }
  else
  {
    if(nOff == 1)
    {
      subject += " OFF: chassis " + chassisOn[1];
    }
    else
    {
      needBody = true;
    }
  }
  if(!needBody)
  {
    return;
  }
  body = "Beam mode ON in chassis:\n";
  for(int n = dynlen(chassisOn) ; n > 0 ; n--)
  {
    body += "   " + chassisOn[n] + "\n";
  }
  body += "\nBeam mode OFF in chassis:\n";
  for(int n = dynlen(chassisOff) ; n > 0 ; n--)
  {
    body += "   " + chassisOff[n] + "\n";
  }
}

// ClearInconsistency
/**
Purpose:
Mark inconsistency of given type to disappear

Arguments:
	vacType - [in] Vacuum type of inconsistency

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void ClearInconsistency(int vacType)
{
  for(int n = dynlen(inconsistencies) ; n > 0 ; n--)
  {
    if(inconsistencies[n][INCONSISTENCY_VAC] == vacType)
    {
      inconsistencies[n][INCONSISTENCY_ACTIVE] = false;
      inconsistencies[n][INCONSISTENCY_MSG_NUMBER] = 0;
    }
  }
}

// ReportInconsistency
/**
Purpose:
Mark inconsistency of given type to appear

Arguments:
	vacType - [in] Vacuum type of inconsistency
	chassisOn - [in] List of SVCU chassis names which report beam mode ON
	chassisOff - [in] List of SVCU chassis names which report beam mode OFF

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void ReportInconsistency(int vacType, dyn_string chassisOn, dyn_string chassisOff)
{
  int n;
  if(debug)
  {
    DebugTN("vcsMachineMode.ctl: ReportInconsistency()", vacType, chassisOn, chassisOff);
  }
  for(n = dynlen(inconsistencies) ; n > 0 ; n--)
  {
    if(inconsistencies[n][INCONSISTENCY_VAC] == vacType)
    {
      break;
    }
  }
  int useIndex = n;
  if(n <= 0)	// New inconsistency
  {
    useIndex = dynlen(inconsistencies) + 1;
  }
  inconsistencyChassisOn[useIndex] = chassisOn;
  inconsistencyChassisOff[useIndex] = chassisOff;
  if(n <= 0)
  {
    inconsistencies[useIndex] = makeDynAnytype((int)vacType, (bool)true, (time)getCurrentTime(),
      (time)getCurrentTime(), (int)0);
  }
  else
  {
    if(!inconsistencies[useIndex][INCONSISTENCY_ACTIVE])
    {
      inconsistencies[useIndex][INCONSISTENCY_MSG_NUMBER] = 0;
      inconsistencies[useIndex][INCONSISTENCY_TIME] = getCurrentTime();
    }
    inconsistencies[useIndex][INCONSISTENCY_ACTIVE] = true;
  }
}

// InitLhcPlcValves
/**
Purpose:
Build list of SVCU chassis and list of valves in every chassis

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void InitLhcValves()
{
  dyn_string	exceptionInfo;

  if(glAccelerator != "LHC")
  {
    return;
  }
  LhcVacGetMachineModeDp(modeDpName);
  if(modeDpName == "")
  {
    return;
  }

  // Check all DPs of type VVS_LHC
  dyn_string allDpNames = dpNames("*", "VVS_LHC");
  for(int n = dynlen(allDpNames) ; n > 0 ; n--)
  {
    string dpName = dpSubStr(allDpNames[n], DPSUB_DP);

    // check type of vacuum the selected valves are belong to
    string   mainPart, sector1, sector2;
    bool    isBorder;
    int     devVacuum;
    LhcVacDeviceVacLocation(dpName, devVacuum, sector1, sector2, mainPart, isBorder, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("vcsMachineMode.ctl: LhcVacDeviceVacLocation(" + dpName + ") FAILED: ", exceptionInfo);
      dynClear(exceptionInfo);
      continue;
    }
    string chassisName = "";
    LhcVacGetAttribute(dpName, "SVCU_Chassis", chassisName);
    if(chassisName == "")
    {
      DebugTN("vcsMachineMode.ctl: No attribute SVCU_Chassis for <" + dpName + ">");
      continue;
    }
    switch(devVacuum)
    {
    case LVA_VACUUM_BLUE_BEAM:
      AppendDpsToLists(dpName, chassisName, lhcChassisBeamB, usedValvesBeamB, "B");
      break;
    case LVA_VACUUM_RED_BEAM:
      AppendDpsToLists(dpName, chassisName, lhcChassisBeamR, usedValvesBeamR, "R");
      break;
    case LVA_VACUUM_CROSS_BEAM:
      AppendDpsToLists(dpName, chassisName, lhcChassisBeamX, usedValvesBeamX, "X");
      break;
    default:
      DebugTN("vcsMachineMode.ctl: Unexpected vacuum type " + devVacuum + " for <" + dpName + ">");
      break;
    }
  }

  if(debug)
  {
    DebugTN("vcsMachineMode.ctl: lhcChassisBeamB", lhcChassisBeamB);
    DebugTN("vcsMachineMode.ctl: lhcChassisBeamR", lhcChassisBeamR);
    DebugTN("vcsMachineMode.ctl: lhcChassisBeamX", lhcChassisBeamX);
  }
}

// AppendDpsToLists
/**
Purpose:
Add name of chassis and DP name of valve in this chassis to list.
Connect to first valve in chassis.

Arguments:
	dpName - [in] Valve DP name
	chassisName - [in] Name of SVCU chassis where valve is connected
	chassisList - [in/out] List of all chassis together with their valves
									where new valve (and) chassis shall be added
	usedValves	- [in/out] List of all connected valves together with received
									valve values
	cbSuffix - [in] Suffix for callback name

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void AppendDpsToLists(string dpName, string chassisName, dyn_dyn_string &chassisList,
  dyn_dyn_anytype &usedValves, string cbSuffix)
{
  for(int n = dynlen(chassisList) ; n > 0 ; n--)
  {
    if(chassisList[n][1] == chassisName)
    {
      dynAppend(chassisList[n], dpName);
      return;
    }
  }

  // New chassis
  dynAppend(chassisList, makeDynString(chassisName, dpName));

  // Initially it does not matter to which valve connection is made, so let's connect
  // to first valve in this chassis
  dynAppend(usedValves, makeDynAnytype(dpName, (unsigned)0, (int)0, (int)0));
  ConnectToValve(dpName, cbSuffix);
  if(debug)
  {
    DebugTN("vcsMachineMode: initially use " + dpName + " in " + chassisName);
  }
}

// ConnectToValve
/**
Purpose:
Connect to valve parameters for beam mode decision

Arguments:
	dpName - [in] Valve DP name
	cbSuffix - [in] Suffix for callback name

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void ConnectToValve(string dpName, string cbSuffix)
{
  string   plcDp, alarmDp;
  dyn_string	exceptionInfo;
		
  LhcVacDevicePlc(dpName, plcDp, alarmDp, exceptionInfo);
  if(alarmDp != "")
  {
    dpConnect("LhcValvePlcCb" + cbSuffix, dpName + ".RR1", alarmDp + ".alarm");
  }
  else
  {
    dpConnect("LhcValveNoPlcCb" + cbSuffix, dpName + ".RR1");
  }
}

// DisconnectFromValve
/**
Purpose:
Disconnect from valve parameters for beam mode decision

Arguments:
	dpName - [in] Valve DP name
	cbSuffix - [in] Suffix for callback name

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void DisconnectFromValve(string dpName, string cbSuffix)
{
  string     plcDp, alarmDp;
  dyn_string  exceptionInfo;
		
  LhcVacDevicePlc(dpName, plcDp, alarmDp, exceptionInfo);
  if(alarmDp != "")
  {
    dpDisconnect("LhcValvePlcCb" + cbSuffix, dpName + ".RR1", alarmDp + ".alarm");
  }
  else
  {
    dpDisconnect("LhcValveNoPlcCb" + cbSuffix, dpName + ".RR1");
  }
}

// CheckValveUsage
/**
Purpose:
Select LHC valves to be used to find 'beam' mode of LHC, connect to selected valves.
This function is called from time to time in order to check if previously selected
valve's state is invalid, if so - select another valve in the same PLC
Contains two loops: first processes the lists concerning B - beam type, second R - beam type  
Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void CheckValveUsage()
{
  CheckBeamValveUsage(lhcChassisBeamB, usedValvesBeamB, "B");
  CheckBeamValveUsage(lhcChassisBeamR, usedValvesBeamR, "R");
  CheckBeamValveUsage(lhcChassisBeamX, usedValvesBeamX, "X");
}

void CheckBeamValveUsage(dyn_dyn_string chassisList, dyn_dyn_anytype &usedValves, string cbSuffix)
{
  dyn_string  exceptionInfo;

  // Check currently used valves in every PLC for B - beams
  for(int chassisIdx = dynlen(chassisList) ; chassisIdx > 0 ; chassisIdx--)
  {
    if(usedValves[chassisIdx][LHC_PLC_ALARM] != 0)
    {
      continue;	// PLC itself is dead - no reason to check individual valves in PLC
    }
    if((usedValves[chassisIdx][LHC_VALVE_STATE] & SVCU_SPS_R_VALID) == 0)
    {
      // Valve state is invalid, select another valve (if any)
      if(dynlen(chassisList[chassisIdx]) < 3)
      {
        continue;	// No choice
      }
      int valveIdx = 2 + (rand() % (dynlen(chassisList[chassisIdx]) - 1));
      if(chassisList[chassisIdx][valveIdx] == usedValves[chassisIdx][LHC_VALVE_NAME])
      {
        // Got the same valve number - take just one more or less
        if(valveIdx > 2)
        {
          valveIdx--;
        }
        else if(valveIdx < dynlen(chassisList[chassisIdx]))
        {
          valveIdx++;
        }
      }
      // Disconnect from old used valve, connect to new selected valve
      if(debug)
      {
        DebugTN("vcsMachineMode: trying to use " + chassisList[chassisIdx][valveIdx] +
          " instead of invalid " + usedValves[chassisIdx][LHC_VALVE_NAME]);
      }
      DisconnectFromValve(usedValves[chassisIdx][LHC_VALVE_NAME], cbSuffix);
        usedValves[chassisIdx][LHC_VALVE_NAME] = chassisList[chassisIdx][valveIdx];
      ConnectToValve(usedValves[chassisIdx][LHC_VALVE_NAME], cbSuffix);
    }
  }
}

// CalculateBeamMode
/**
Purpose:
Calculate BEAM mode of LHC machine according to beam type. The beam mode is considered to be set
if at least for one valve bit 'Close enable' is not set

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void CalculateBeamMode(string beamType, dyn_dyn_anytype &usedValves)
{
  int         nCloseDisable = 0, nCloseEnable = 0;
  string      sourceEqp, sourceMsg;
  dyn_string  exceptionInfo;

  CheckValves(usedValves, nCloseEnable, nCloseDisable, sourceMsg);

  if(beamType == "")  // Check was initiated by .X valve, calculate resulting mode for .R and .B
  {
    if((nCloseEnable == 0) || (nCloseDisable == 0))  // pure mode, further check is needed
    {
      CalculateBeamMode("Beam_B", usedValvesBeamB);
      CalculateBeamMode("Beam_R", usedValvesBeamR);
    }
    else  // Just report mode inconsistency for both R & B
    {
      int beamMode = 2;
      dpSet(modeDpName + ".Beam_B.Beam", beamMode,
        modeDpName + ".Beam_B.SourceEqp", sourceMsg);
      dpSet(modeDpName + ".Beam_R.Beam", beamMode,
        modeDpName + ".Beam_R.SourceEqp", sourceMsg);
    }
  }
  else  // Check also all .X valves
  {
    CheckValves(usedValvesBeamX, nCloseEnable, nCloseDisable, sourceMsg);
    int beamMode = 0;
    if((nCloseDisable > 0) && (nCloseEnable == 0))
    {
      beamMode = 1;
    }
    else if((nCloseDisable > 0) && (nCloseEnable > 0))
    {
      beamMode = 2;
    }
    dpSet(modeDpName + "." + beamType + ".Beam", beamMode,
      modeDpName + "." + beamType + ".SourceEqp", sourceMsg);
  }
}

void CheckValves(dyn_dyn_anytype usedValves, int &nCloseEnable, int &nCloseDisable, string &sourceMsg)
{
  dyn_string exceptionInfo;

  for(int n = dynlen(usedValves) ; n > 0 ; n--)
  {
    if(usedValves[n][LHC_PLC_ALARM] > 0)
    {
      usedValves[n][LHC_BEAM_MODE] = VALVE_INVALID;
      continue;
    }
    if((usedValves[n][LHC_VALVE_STATE] & SVCU_SPS_R_VALID) == 0)
    {
      usedValves[n][LHC_BEAM_MODE] = VALVE_INVALID;
      continue;
    }
    if((usedValves[n][LHC_VALVE_STATE] & SVCU_SPS_R_CLOSE_ENABLE) == 0)
    {
      if(nCloseDisable == 0)
      {
        string sourceEqp;
        LhcVacDisplayName(usedValves[n][LHC_VALVE_NAME], sourceEqp, exceptionInfo);
        if(sourceEqp == "" )
        {
          sourceEqp = usedValves[n][LHC_VALVE_NAME];
        }
        if(nCloseEnable == 0)
        {
          sourceMsg = sourceEqp;
        }
        else
        {
          sourceMsg += " and " + sourceEqp;
        }
        if(debug)
        {
          DebugTN("vcsMachineMode: close DISABLE " + usedValves[n][LHC_VALVE_NAME]);
        }
      }
      nCloseDisable++;
      usedValves[n][LHC_BEAM_MODE] = VALVE_CLOSE_DISABLE;
    }
    else
    {
      if(nCloseEnable == 0)
      {
        string sourceEqp;
        LhcVacDisplayName(usedValves[n][LHC_VALVE_NAME], sourceEqp, exceptionInfo);
        if(sourceEqp == "" )
        {
          sourceEqp = usedValves[n][LHC_VALVE_NAME];
        }
        if(nCloseDisable == 0)
        {
          sourceMsg = sourceEqp;
        }
        else
        {
          sourceMsg += " and " + sourceEqp;
        }
        if(debug)
        {
          DebugTN("vcsMachineMode: close DISABLE " + usedValves[n][LHC_VALVE_NAME]);
        }
      }
      nCloseEnable++;
      usedValves[n][LHC_BEAM_MODE] = VALVE_CLOSE_ENABLE;
    }
  }
}

// CheckModeConsistency
/**
Purpose:
Check consistency of beam mode reported by different SVCU chassis.
Beam mode must be the same in all chassis for the same beam type,
chassis for .X valves must have ORed beam condition of .B and .R valves

Parameters:
	vacType - [in] Vacuum type of valve who initiated this processing

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void CheckModeConsistency(int vacType)
{
  dyn_string  chassisOnB, chassisOffB, chassisOnR, chassisOffR, chassisOnX, chassisOffX;

  AnalyzeChassis(lhcChassisBeamB, usedValvesBeamB, chassisOnB, chassisOffB);
  if(debug)
  {
    DebugTN("Chassis on beam B:", chassisOnB, chassisOffB);
  }
  AnalyzeChassis(lhcChassisBeamR, usedValvesBeamR, chassisOnR, chassisOffR);
  if(debug)
  {
    DebugTN("Chassis on beam R:", chassisOnR, chassisOffR);
  }
  AnalyzeChassis(lhcChassisBeamX, usedValvesBeamX, chassisOnX, chassisOffX);
  if(debug)
  {
    DebugTN("Chassis on beam X:", chassisOnX, chassisOffX);
  }

  // Blue beam mode consistency
  if(dynlen(chassisOnB) > 0)
  {
    if((dynlen(chassisOffB) + dynlen(chassisOffX)) == 0)
    {
      ClearInconsistency(LVA_VACUUM_BLUE_BEAM);
    }
    else
    {
      ReportInconsistency(LVA_VACUUM_BLUE_BEAM, JoinLists(chassisOnB, chassisOnX), JoinLists(chassisOffB, chassisOffX));
    }
  }
  else
  {
    ClearInconsistency(LVA_VACUUM_BLUE_BEAM);
    if((dynlen(chassisOnX) > 0) && (dynlen(chassisOnR) == 0))
    {
      ReportInconsistency(LVA_VACUUM_CROSS_BEAM, chassisOnX, chassisOffX);
    }
    else
    {
      ClearInconsistency(LVA_VACUUM_CROSS_BEAM);
    }
  }

  // Red beam mode consistency
  if(dynlen(chassisOnR) > 0)
  {
    if((dynlen(chassisOffR) + dynlen(chassisOffX)) == 0)
    {
      ClearInconsistency(LVA_VACUUM_RED_BEAM);
    }
    else
    {
      ReportInconsistency(LVA_VACUUM_RED_BEAM, JoinLists(chassisOnR, chassisOnX), JoinLists(chassisOffR, chassisOffX));
    }
  }
  else
  {
    ClearInconsistency(LVA_VACUUM_RED_BEAM);
    if((dynlen(chassisOnX) > 0) && (dynlen(chassisOnB) == 0))
    {
      ReportInconsistency(LVA_VACUUM_CROSS_BEAM, chassisOnX, chassisOffX);
    }
    else
    {
      ClearInconsistency(LVA_VACUUM_CROSS_BEAM);
    }
  }
}

// JoinLists
/**
Purpose:
Build list of names containing all names from two lists

Parameters:
	list1 - [in] First list of names
	list2 - [in] Second list of names

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
dyn_string JoinLists(dyn_string list1, dyn_string list2)
{
  for(int n = dynlen(list2) ; n > 0 ; n--)
  {
    string name = list2[n];
    dynAppend(list1, name);
  }
  return list1;
}

// AnalyzeChassis
/**
Purpose:
Analyze beam mode conditions reported by all chassis in list, build
two lists of chassis names:
 - all chassis who report beam mode ON
 - all chassis who report beam mode OFF

Parameters:
	chassisList - [in] List of all chassis names together with valve names
										in every chassis
	usedValves - [in] List of all used valves in these chassis together with
										received valve parameters
	chassisOn - [out] List of chassis names who report beam mode ON will be returned here
	chassisOff - [out] List of chassis names who report beam mode OFF will be returned here

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void AnalyzeChassis(dyn_dyn_string chassisList, dyn_dyn_anytype usedValves,
  dyn_string &chassisOn, dyn_string &chassisOff)
{
  for(int chassisIdx = dynlen(usedValves) ; chassisIdx > 0 ; chassisIdx--)
  {
    switch(usedValves[chassisIdx][LHC_BEAM_MODE])
    {
    case VALVE_CLOSE_DISABLE:
      dynAppend(chassisOn, chassisList[chassisIdx][1]);
      break;
    case VALVE_CLOSE_ENABLE:
      dynAppend(chassisOff, chassisList[chassisIdx][1]);
      break;
    }
  }
}

// LhcValvePlcCbB
/**
Purpose:
Callback for valve at B beam line state together with PLC alarm state

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void LhcValvePlcCbB(string rr1Dpe, unsigned rr1, string alarmDpe, int alarm)
{
  synchronized(usedValvesBeamB)
  {
    int chassisIdx = FindLhcValve(rr1Dpe, usedValvesBeamB);
    if(chassisIdx < 1)
    {
      return;
    }
    usedValvesBeamB[chassisIdx][LHC_VALVE_STATE] = rr1;
    usedValvesBeamB[chassisIdx][LHC_PLC_ALARM] = alarm;
    CalculateBeamMode("Beam_B", usedValvesBeamB);
    CheckModeConsistency(LVA_VACUUM_BLUE_BEAM);
  }
}

// LhcValveNoPlcCbB
/**
Purpose:
Callback for valve at B beam line state without PLC alarm state

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void LhcValveNoPlcCbB(string rr1Dpe, unsigned rr1)
{
  synchronized(usedValvesBeamB)
  {
    int chassisIdx = FindLhcValve(rr1Dpe, usedValvesBeamB);
    if(chassisIdx < 1)
    {
      return;
    }
    usedValvesBeamB[chassisIdx][LHC_VALVE_STATE] = rr1;
    usedValvesBeamB[chassisIdx][LHC_PLC_ALARM] = 0;
    CalculateBeamMode("Beam_B", usedValvesBeamB);
    CheckModeConsistency(LVA_VACUUM_BLUE_BEAM);
  }
}

// LhcValvePlcCbR
/**
Purpose:
Callback for valve at R beam line state together with PLC alarm state

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void LhcValvePlcCbR(string rr1Dpe, unsigned rr1, string alarmDpe, int alarm)
{
  synchronized(usedValvesBeamR)
  {
    int chassisIdx = FindLhcValve(rr1Dpe, usedValvesBeamR);
    if(chassisIdx < 1)
    {
      return;
    }
    usedValvesBeamR[chassisIdx][LHC_VALVE_STATE] = rr1;
    usedValvesBeamR[chassisIdx][LHC_PLC_ALARM] = alarm;
    CalculateBeamMode("Beam_R", usedValvesBeamR);
    CheckModeConsistency(LVA_VACUUM_RED_BEAM);
  }
}

// LhcValveNoPlcCbR
/**
Purpose:
Callback for valve at R beam line state without PLC alarm state

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void LhcValveNoPlcCbR(string rr1Dpe, unsigned rr1)
{
  synchronized(usedValvesBeamR)
  {
    int chassisIdx = FindLhcValve(rr1Dpe, usedValvesBeamR);
    if(chassisIdx < 1)
    {
      return;
    }
    usedValvesBeamR[chassisIdx][LHC_VALVE_STATE] = rr1;
    usedValvesBeamR[chassisIdx][LHC_PLC_ALARM] = 0;
    CalculateBeamMode("Beam_R", usedValvesBeamR);
    CheckModeConsistency(LVA_VACUUM_RED_BEAM);
  }
}

// LhcValvePlcCbX
/**
Purpose:
Callback for valve at X beam line state together with PLC alarm state

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void LhcValvePlcCbX(string rr1Dpe, unsigned rr1, string alarmDpe, int alarm)
{
  synchronized(usedValvesBeamX)
  {
    int chassisIdx = FindLhcValve(rr1Dpe, usedValvesBeamX);
    if(chassisIdx < 1)
    {
      return;
    }
    usedValvesBeamX[chassisIdx][LHC_VALVE_STATE] = rr1;
    usedValvesBeamX[chassisIdx][LHC_PLC_ALARM] = alarm;
    CalculateBeamMode("", usedValvesBeamX);
    CheckModeConsistency(LVA_VACUUM_CROSS_BEAM);
  }
}

// LhcValveNoPlcCbX
/**
Purpose:
Callback for valve at R beam line state without PLC alarm state

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void LhcValveNoPlcCbX(string rr1Dpe, unsigned rr1)
{
  synchronized(usedValvesBeamX)
  {
    int chassisIdx = FindLhcValve(rr1Dpe, usedValvesBeamX);
    if(chassisIdx < 1)
    {
      return;
    }
    usedValvesBeamX[chassisIdx][LHC_VALVE_STATE] = rr1;
    usedValvesBeamX[chassisIdx][LHC_PLC_ALARM] = 0;
    CalculateBeamMode("", usedValvesBeamX);
    CheckModeConsistency(LVA_VACUUM_CROSS_BEAM);
  }
}

// FindLhcValve
/**
Purpose:
Find valve with given DP name in list usedValves, returns index of valve
found, or -1 if such valve is not found.

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
int FindLhcValve(string valveDpeName, dyn_dyn_anytype usedValves)
{
  string dpName = dpSubStr(valveDpeName, DPSUB_DP);
  for(int n = dynlen(usedValves) ; n > 0 ; n--)
  {
    if(usedValves[n][LHC_VALVE_NAME] == dpName)
    {
      return n;
    }
  }
  DebugTN("lhcMachineMode: ERROR: DP <" + dpName + "> is not found in usedValves");
  return -1;
}

// EmailConfigCb
/**
Purpose:
Callback for E-mail configuration DP

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void EmailConfigCb(string serverDpe, string server, string clientDpe, string client,
  string senderDpe, string sender, string recipientsDpe, dyn_string recipients)
{
  emailServer = server;
  emailClient = client;
  emailSender = sender;
  emailRecipients = recipients;
}

// InitEquipmentData
/**
Purpose:
Initialize static machine data:
	. beam lines configuration and survey data
	. vacuum sectors and main parts
	. controllable equipment

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
bool InitEquipmentData()
{
  dyn_string  exceptionInfo;

  // Set accelerator name to be used by other components
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsMachineMode: Failed to read machine name: " + exceptionInfo);
    return false;
  }

  bool isLhcData = glAccelerator == "LHC" ? true : false;

  // Initalize passive machine data
  int coco = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsMachineMode: LhcVacEqpInitPassive() failed: " + exceptionInfo);
    return false;
  }

  LhcVacInitDevFunc();

  // Initialzie active equipment information
  coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
			exceptionInfo );
  if(coco < 0)
  {
    DebugTN("vcsMachineMode: LhcVacEqpInitActive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsMachineMode: Init finished" );
  return true;
}
