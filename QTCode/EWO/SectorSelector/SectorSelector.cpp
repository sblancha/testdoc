//	Implementation of SectorSelector class
/////////////////////////////////////////////////////////////////////////////////

#include "SectorSelector.h"

EWO_PLUGIN(SectorSelector)


SectorSelector::SectorSelector(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new SectorSelectorControl(parent);

	// Connect to signals of base widget
	connect(baseWidget, SIGNAL(selectionChanged(void)), this, SLOT(selectionChange(void)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList SectorSelector::signalList(void) const
{
	QStringList list;
	list.append("selectionChanged()");
	return list;
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList SectorSelector::methodList(void) const
{
	QStringList list;
	list.append("void setSelectedSectors(dyn_string sectors)");
	list.append("dyn_string getSelectedSectors()");
	list.append("bool isEmpty()");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant SectorSelector::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "setSelectedSectors")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::StringList))
		{
			error = "argument 1 (sectorList) is not dyn_string";
			return QVariant();
		}
		baseWidget->setSelectedSectors(values[0].toStringList());
		return QVariant();
	}
	else if(name == "getSelectedSectors")
	{
		QStringList	sectorList;
		baseWidget->getSelectedSectors(sectorList);
		return QVariant(sectorList);
	}
	else if(name == "isEmpty")
	{
		return QVariant(baseWidget->isEmpty());
	}
	else
	{
		error = "Uknown method SectorSelector.";
		error += name;
	}
	return QVariant();
}

/*
**	FUNCTION
**		Slot receiving selectionChanged signal of SectorSelectorControl, emit
**		signal 'selectionChanged' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelector::selectionChange(void)
{
	emit signal("selectionChanged");
}

