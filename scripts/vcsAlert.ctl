// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Type: Script
Name: vcsAlert.ctl
Language: WCCOA CTRL

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: Process for WCCOA to activation alert system

Version : 1.0    
  
Limitations: WINCC OA Manager CTRL (WCCOA00CTRL)
 
Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------

#uses "VacCtlEqpDataPvss"	  //load dll or so file
#uses "VacCtlImportPvss"	  // load dll or so file
#uses "vclGeneric.ctl"       // Load control library 
#uses "vclMachine.ctl"	     // Load control library
#uses "vclDeviceFileParser.ctl"	// Load control library
#uses "vclSectors.ctl"	     // Load control library
#uses "vclDevCommon.ctl"	  // Load control library
#uses "vclAddressConfig.ctl" // Load control library

// @brief Global const 
const dyn_string alertDpTypes = makeDynString("VA_RI");

// @brief Global const 
const string TEMP = "TEMP";

// @brief Global to calculate order ID -- NOT USED
int G_integer = 0;

// ---------------------------------------------------------------------------------------------------------------------
main()
{
  dyn_string		exceptionInfo;
  dyn_errClass	   err;
  string          errMessage; 
  int             ret; //return value of function (0 is OK)

  DebugTN("vcsAlert.ctl: ----- Alert Script Starting");

  // Get list of dp that need to be configured to activate alert feature
  dyn_string dps;
  for(int i = 1; i <= dynlen(alertDpTypes); i++) {
    DebugTN("alertDpTypes : ",alertDpTypes[i]);
    dyn_string dpsOfType = dpNames("*.AL1", alertDpTypes[i]);
    DebugTN("dpsOfType: ",dpsOfType);
    dynAppend(dps, dpsOfType);  
  }
  DebugTN("dps: ",dps);
  
  // Set Alarm handling
  for(int i = 1; i < dynlen(dps); i++) {  
    ret = dpSetWait(dps[i] +":_alert_hdl.._type", DPCONFIG_ALERT_BINARYSIGNAL,
                    dps[i] +":_alert_hdl.._class", "alert.",
                    dps[i] +":_alert_hdl.._text0", "Ok",
                    dps[i] +":_alert_hdl.._text1", "Alert",
                    dps[i] +":_alert_hdl.._orig_hdl", FALSE);
    if(dynlen(err) > 0) {
      DebugTN("vcsAlert.ctl: set alarm handling error : " + dps[i]);   
      throw(err);    
    }
  }
}
    
