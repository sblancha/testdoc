//	Implementation of EqpVR_GT class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVR_GT.h"

#include "FunctionalType.h"
#include "EqpMsgCriteria.h"
#include "DataPool.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define	COLOR_ON_ERROR			204,255,0
#define	COLOR_ON_WARNING		153,255,0
#define COLOR_OFF				255,0,0
#define COLOR_OFF_ERROR			255,0,0
#define	COLOR_OFF_WARNING		255,0,0
#define	COLOR_ERROR				255,102,255
#define	COLOR_WARNING			255,255,0
//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_ON			(0x02000000)
#define	RR1_OFF			(0x01000000)
#define RR1_ERROR_CODE_MASK	(0x000000FF)
#define RR2_RELAY4_VALID	(0x00800000)
#define RR2_RELAY4_ON		(0x00400000)
#define RR2_RELAY3_VALID	(0x00200000)
#define RR2_RELAY3_ON		(0x00100000)
#define	RR2_RELAY2_VALID	(0x00080000)
#define	RR2_RELAY2_ON		(0x00040000)
#define	RR2_RELAY1_VALID	(0x00020000)
#define	RR2_RELAY1_ON		(0x00010000)
#define	RR2_RELAYB_VALID	(0x00000800)
#define	RR2_RELAYB_ON		(0x00000400)
#define	RR2_RELAYA_VALID	(0x00000200)
#define	RR2_RELAYA_ON		(0x00000100)


EqpVR_GT::EqpVR_GT(const Eqp &source) : Eqp(source)
{
	functionalType = FunctionalType::VR_GT;
}

void EqpVR_GT::postProcess(void)
{
//qDebug("postProcess(%s)\n", dpName);
	// Find reference to sector(s): sector(s) of connected gauges
	// Too slow - may be will be used later?
	return;
	QStringList slaves;
	getSlaves(slaves);
	DataPool &pool = DataPool::getInstance();
	for(int idx = 0 ; idx < slaves.count() ; idx++)
	{
		QString dpName = slaves.at(idx);
		Eqp *pEqp = pool.findEqpByDpName(dpName.toLatin1().constData());
		if(pEqp->getSectorBefore())
		{
			if(!pSectorBefore)
			{
				pSectorBefore = pEqp->getSectorBefore();
			}
			else if(!pSectorAfter)
			{
				pSectorAfter = pEqp->getSectorAfter();
			}
			else
			{
				break;
			}
		}
	}
}

/*
**	FUNCTION
**		Build list of slave DP names
**
**	ARGUMENTS
**		slaves	- Variable where slave DP names shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_GT::getSlaves(QStringList &slaves)
{
	slaves.clear();
	// Master of VR_GT is VRCG, and VRCG knows slaves
	const QString attrValue = getAttrValue("MasterName");
	if(!attrValue.isEmpty())
	{
		DataPool &pool = DataPool::getInstance();
		Eqp *pMasterEqp = pool.findEqpByDpName(attrValue.toLatin1().constData());
		if(pMasterEqp)
		{
			pMasterEqp->getSlaves(slaves);
		}
	}
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_GT::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "RR2");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_GT::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "RR2", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_GT::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "RR2", mode);
	disconnectDpe(pDst, "RR1", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_GT::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Online == newValue)
			{
				return;
			}
			rr2Online = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

#ifndef PVSS_SERVER_VERSION
void EqpVR_GT::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Replay == newValue)
			{
				return;
			}
			rr2Replay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_GT::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_GT::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, rr2Replay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, rr2Online, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_GT::getMainColor(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(rr1 & RR1_ERROR)	// Still different colors for ON/OFF
	{
		if(rr1 & RR1_ON)
		{
			if(rr1 & RR1_OFF)
			{
				color.setRgb(COLOR_ERROR);
			}
			else
			{
				color.setRgb(COLOR_ON_ERROR);
			}
		}
		else if(rr1 & RR1_OFF)
		{
			color.setRgb(COLOR_OFF_ERROR);
		}
		else
		{
			color.setRgb(COLOR_ERROR);
		}
	}
	// No errors - check ON/OFF state
	else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_WARNING)
	{
		if(rr1 & RR1_ON)
		{
			if(rr1 & RR1_OFF)
			{
				color.setRgb(COLOR_WARNING);
			}
			else
			{
				color.setRgb(COLOR_ON_WARNING);
			}
		}
		else if(rr1 & RR1_OFF)
		{
			color.setRgb(COLOR_OFF_WARNING);
		}
		else
		{
			color.setRgb(COLOR_WARNING);
		}
	}
	else if(rr1 & RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else if(rr1 & RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_GT::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, rr2Replay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, rr2Online, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_GT::getMainStateString(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
		return;
	}

	// Main state
	if(rr1 & RR1_ERROR)	// Still different strings for open/closed
	{
		if(rr1 & RR1_ON)
		{
			if(rr1 & RR1_OFF)
			{
				string = "Error";
			}
			else
			{
				string = "ON (error)";
			}
		}
		else if(rr1 & RR1_OFF)
		{
			string = "OFF (error)";
		}
		else
		{
			string = "Error";
		}
	}
	// No errors - check open/closed state
	else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		string = "Error";
	}
	else if(rr1 & RR1_WARNING)
	{
		if(rr1 & RR1_ON)
		{
			if(rr1 & RR1_OFF)
			{
				string = "Error";
			}
			else
			{
				string = "ON (warning)";
			}
		}
		else if(rr1 & RR1_OFF)
		{
			string = "OFF (warning)";
		}
		else
		{
			string = "Undefined (warning)";
		}
	}
	else if(rr1 & RR1_ON)
	{
		string = "ON";
	}
	else if(rr1 & RR1_OFF)
	{
		string = "OFF";
	}
	else
	{
		string = "Undefined";
	}

	// Add relays statistcs to string
	int nRelaysOn = 0, nRelaysOff = 0, nRelaysInvalid = 0;
	updateRelayStats(rr2 & RR2_RELAY4_VALID, rr2 & RR2_RELAY4_ON, nRelaysOn, nRelaysOff, nRelaysInvalid);
	updateRelayStats(rr2 & RR2_RELAY3_VALID, rr2 & RR2_RELAY3_ON, nRelaysOn, nRelaysOff, nRelaysInvalid);
	updateRelayStats(rr2 & RR2_RELAY2_VALID, rr2 & RR2_RELAY2_ON, nRelaysOn, nRelaysOff, nRelaysInvalid);
	updateRelayStats(rr2 & RR2_RELAY1_VALID, rr2 & RR2_RELAY1_ON, nRelaysOn, nRelaysOff, nRelaysInvalid);
	updateRelayStats(rr2 & RR2_RELAYB_VALID, rr2 & RR2_RELAYB_ON, nRelaysOn, nRelaysOff, nRelaysInvalid);
	updateRelayStats(rr2 & RR2_RELAYA_VALID, rr2 & RR2_RELAYA_ON, nRelaysOn, nRelaysOff, nRelaysInvalid);

	string += ", relays: ";
	string += QString::number(nRelaysOn);
	string += " ON,";
	string += QString::number(nRelaysOff);
	string += " OFF";
	if(nRelaysInvalid)
	{
		string += ", ";
		string += QString::number(nRelaysInvalid);
		string += " invalid";
	}
}
#endif

#ifndef PVSS_SERVER_VERSION
void EqpVR_GT::updateRelayStats(unsigned valid, unsigned on, int &nRelaysOn, int &nRelaysOff, int &nRelaysInvalid)
{
	if(valid)
	{
		if(on)
		{
			nRelaysOn++;
		}
		else
		{
			nRelaysOff++;
		}
	}
	else
	{
		nRelaysInvalid++;
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_GT::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("RR1");
	dpeNames.append("RR2");
	QList<QVariant *> values;
	if(!pPool->getDpValues(dpName, ts, dpeNames, values))
	{
		return;
	}
	if (values.isEmpty()) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	if (values.isEmpty()) {
		return;
	}
	pVariant = values.takeFirst();
	unsigned rr2 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, rr2, false, state);
	getMainColor(rr1, rr2, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpVR_GT::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	return rr1 & RR1_ERROR_CODE_MASK;
}
