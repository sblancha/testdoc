//	Implementation of FunctionalTypeVITE class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVITE.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include <stdio.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeVITE::FunctionalTypeVITE(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVITE::fillCriteria(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, 0, "Thermocouple value above limit");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: temperature {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: temperature {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Thermocouple value {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Thermocouple value {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Thermocouple value {value} in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Thermocouple value {value} in main part {mainPart}");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeVITE::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainValue:
		result = "Temperature";
		break;
	default:
		break;
	}
	return result;
}

int FunctionalTypeVITE::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainValue:
		if(!strcasecmp(name, "Temperature"))
		{
			return 0;
		}
		break;
	default:
		break;
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}

