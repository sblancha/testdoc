//	Implementation of DpeData class
///////////////////////////////////////////////////////////////////////////////////

#include "DpeData.h"
#include "ImportPool.h"

#include "PlatformDef.h"

#include <QStringList>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

ImportPool	*DpeData::pPool = NULL;

void DpeData::setImportPool(ImportPool *pImportPool)
{
	pPool = pImportPool;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


DpeData::DpeData(const char *name, const char *dpName, int lineNo)
{
	this->name = pPool->addString(name);
	this->dpName = dpName;
	addressType = NULL;
	drvNum = EMPTY_INT_VALUE;
	reference = NULL;
	direction = EMPTY_INT_VALUE;
	transType = NULL;
	oldNew = EMPTY_INT_VALUE;
	connect = NULL;
	group = NULL;
	archiveIndex = EMPTY_INT_VALUE;
	valueFormat = NULL;
	this->lineNo = lineNo;
	addressChange = false;
	archiveChange = 0;
	valueFormatChange = false;
	clearAll = false;
	updateCounter = 0;
	archSmoothType = archTimeInterval = archDeadBand = 0;
	exportToCMW = NULL;
	exportToDIP = NULL;
	dipConfigChange = false;
}

/*
**
**	FUNCTION
**		Create new DpeData instance from data read in file.
**
**	PARAMETERS
**		pFile		- Pointer to file being read.
**		lineNo		- Line number in file.
**		dpName		- Name of DP where this DPE appears
**		name		- name of new DPE
**		errList		- List where error message shall be added
**						in case of errors.
**
**	RETURNS
**		Pointer to new DpeData instance created from data in file;
**		NULL in case of error.
**
**	CAUTIONS
**		It is responsability of caller to destroy returned instance using 'delete'
**		operator.
*/
DpeData *DpeData::createFromFile(FILE *pFile, int &lineNo, const char *dpName,
	const char *name, QStringList &errList)
{
	DpeData	*pNew = new DpeData(name, dpName, lineNo);
	if(pNew->parseFile(pFile, lineNo, errList) < 0)
	{
		delete pNew;
		return NULL;
	}
	pNew->checkConsistency(errList);
	return pNew;
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int DpeData::processLineTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	const QString & /* longValue */, QStringList &errList)
{
	const char	*funcName = "DpeData::processLineTokens()";
	
	if(!strcasecmp(tokens[0], "AddressType"))	
	{
		
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		addressType = pPool->addMultiUseString(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "DrvNum"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &drvNum) != 1)
		{
			PARSING_WARNING2("Line %d: bad 'DrvNum' format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Reference"))	
	{
		
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		reference = pPool->addString(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Direction"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &direction) != 1)
		{
			PARSING_WARNING2("Line %d: bad 'Direction' format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "TransType"))	
	{
		
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		transType = pPool->addMultiUseString(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "OldNew"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &oldNew) != 1)
		{
			PARSING_WARNING2("Line %d: bad 'OldNew' format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Connect"))	
	{
		
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		connect = pPool->addMultiUseString(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Group"))	
	{
		
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		group = pPool->addMultiUseString(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Archive"))	
	{
		
		CHECK_TOKEN_COUNT( nTokens, 2, lineNo, funcName);
		archiveIndex = pPool->checkArchiveReference(tokens[1]);
		if(archiveIndex < 0)
		{
			PARSING_WARNING2("Line %d: archive <%s> doesn't exist", lineNo, tokens[1]);
		}
		// Added 03.12.2012 L.Kopylov: get archive's parameters as default archive settings
		Archive *pArchive = pPool->findArchive(tokens[1]);
		if(!pArchive)
		{
			PARSING_WARNING2("Line %d: archive <%s> reference doesn't exist", lineNo, tokens[1]);
		}
		else if((!archSmoothType) && (!archTimeInterval) && (!archDeadBand))
		{
			archSmoothType = pArchive->getSmoothType();
			archTimeInterval = pArchive->getTimeInterval();
			archDeadBand = pArchive->getDeadBand();
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "SmoothType"))	
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &archSmoothType) != 1)
		{
			PARSING_WARNING2("Line %d: bad 'SmoothType' format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "TimeInterval"))	
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &archTimeInterval) != 1)
		{
			PARSING_WARNING2("Line %d: bad 'TimeInterval' format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "DeadBand"))	
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &archDeadBand) != 1)
		{
			PARSING_WARNING2("Line %d: bad 'DeadBand' format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "ValueFormat"))	
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		valueFormat = pPool->addMultiUseString(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "ExportToCMW"))	
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		exportToCMW = pPool->addMultiUseString(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "ExportToDIP"))	
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		exportToDIP = pPool->addMultiUseString(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "EndDpe"))		// End of DPE
	{
		return 0;
	}
	PARSING_ERR2("Line %d: unexpected <%s>", lineNo, tokens[0]);
}

/*
**
**	FUNCTION
**		Check data consistency in this instance
**
**	PARAMETERS
**		errList		- List where error message shall be added
**						in case of errors.
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeData::checkConsistency(QStringList &errList)
{
	QString fullName(dpName);
	fullName += ".";
	fullName += name;

	// Check address consistency
	// If at least one component of address is filled -> all components must be presented

	if((addressType) || (drvNum != EMPTY_INT_VALUE ) || (reference) ||
		(direction != EMPTY_INT_VALUE ) || (transType) || (oldNew != EMPTY_INT_VALUE) ||
		(connect ) || (group))
	{
		if(!addressType)
		{
			PARSING_WARNING1("'AddressType' isn't specified for DPE <%s>", fullName.toLatin1().constData());
		}
		if(drvNum == EMPTY_INT_VALUE)
		{
			PARSING_WARNING1("'DrvNum' isn't specified for DPE <%s>", fullName.toLatin1().constData());
		}
		if(!reference)
		{
			PARSING_WARNING1("'Reference' isn't specified for DPE <%s>", fullName.toLatin1().constData());
		}
		if(direction == EMPTY_INT_VALUE)
		{
			PARSING_WARNING1("'Direction' isn't specified for DPE <%s>", fullName.toLatin1().constData());
		}
		if(!transType)
		{
			PARSING_WARNING1("'TransType' isn't specified for DPE <%s>", fullName.toLatin1().constData());
		}
		if(oldNew == EMPTY_INT_VALUE)
		{
			PARSING_WARNING1("'OldNew' isn't specified for DPE <%s>", fullName.toLatin1().constData());
		}
		if(addressType)
		{
			if(strcmp(addressType, CMW_ADDRESS_TYPE))	// CMW does not need connect and group
			{
				if(!connect)
				{
					PARSING_WARNING1("'Connect' isn't specified for DPE <%s>", fullName.toLatin1().constData());
				}
				if( !group )
				{
					PARSING_WARNING1("'Group' isn't specified for DPE <%s>", fullName.toLatin1().constData());
				}
			}
		}
	}
	if(addressType)
	{
		if(drvNum != EMPTY_INT_VALUE)
		{
			pPool->addDriverRef(drvNum, addressType);
		}
		if(!strcmp(addressType, DEFAULT_ADDRESS_TYPE))
		{
			if(connect)
			{
				if(!pPool->checkS7ConnectReference(connect))
				{
					PARSING_WARNING2("S7 Connection <%s> for DPE <%s> doesn't exist", connect, fullName.toLatin1().constData());
				}
			}
			if(group)
			{
				if(!pPool->checkPollGroupReference(group))
				{
					PARSING_WARNING2("Poll Group <%s>  for DPE <%s> doesn't exist", group, fullName.toLatin1().constData());
				}
			}
		}
	}
}

/*
**
**	FUNCTION
**		Dump instance content to output file for debugging
**
**	PARAMETERS
**		pFile		- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeData::dump(FILE *pFile)
{
	if(!ImportPool::isDebugMode())
	{
		return;
	}
	fprintf(pFile, "   ------------- DPE <%s>\n", name);
	fprintf(pFile, "      Address: type <%s> drv %d direction %d oldNew %d\n",
		addressType, drvNum, direction, oldNew);
	fprintf(pFile, "               transType <%s> reference <%s>\n", transType, reference);
	fprintf(pFile, "               connect <%s> group <%s>\n", connect, group);
	fprintf(pFile, "      Archive %d valueFormat <%s>\n", archiveIndex, valueFormat);
	fprintf(pFile, "      lineNo %d addressChange %d archiveChange %d valueFormatChange %d\n",
		lineNo, addressChange, archiveChange, valueFormatChange);
	fprintf(pFile, "      exportToCMW <%s> exportToDIP <%s> dipConfigChange %d\n",
		(exportToCMW == NULL ? "NULL" : exportToCMW), (exportToDIP == NULL ? "NULL" : exportToDIP),
		(int)dipConfigChange);
}
