#ifndef	FUNCTIONALTYPEALARM_DO_H
#define	FUNCTIONALTYPEALARM_DO_H

// Special functional type for CRYO alarms - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeALARM_DO : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		OK = 1
	};

	FunctionalTypeALARM_DO(int type, const char *name, const char *description);
	virtual ~FunctionalTypeALARM_DO() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEALARM_DO_H

