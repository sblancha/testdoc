#ifndef	PROFILELHCDIALOG_H
#define	PROFILELHCDIALOG_H

// Dialog for displaying LHC profile view

#include "DataEnum.h"
#include "VacEqpTypeMask.h"
#include "LhcRingSelection.h"

#include "ProfileLhcView.h"

#include <QDialog>

class Sector;

#include <QVBoxLayout>
#include <QPushButton>
#include <QMenu>
#include <QLabel>
#include <QComboBox>

class ProfileLhcDialog : public QDialog, public LhcRingSelection
{
	Q_OBJECT

public:
	ProfileLhcDialog(unsigned vacTypes, Sector *pFirstSector, Sector *pLastSector, DataEnum::DataMode mode);
	~ProfileLhcDialog();

signals:
	void profileDpDown(int button, int mode, int x, int y, int part, const char *dpName);

protected:
	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Layout providing geometry for all control elements. We need to keep it
	// because we need to insert view (when created) to it
	QVBoxLayout *mainBox;

	// Button for displaying 'File' menu
	QPushButton	*pFilePb;

	// Button for displaying 'View' menu
	QPushButton	*pViewPb;

	// Button for displaying 'Help' menu
	QPushButton	*pHelpPb;

	// 'File' menu
	QMenu	*pFileMenu;

	// 'View' menu
	QMenu	*pViewMenu;

	// 'Vacuum type' submenu of 'View' menu
	QMenu	*pVacSubMenu;

	// 'Equipment on isolation vacuum' submenu of 'View' menu
	QMenu	*pCryoEqpSubMenu;

	// 'Equipment on beam vacuum' submenu of 'View' menu
	QMenu	*pBeamEqpSubMenu;

	// 'Help' menu
	QMenu	*pHelpMenu;

	// Label with synoptic title
	QLabel		*pTitleLabel;

	// Combo box for main part selection
	QComboBox	*pCombo;

	// Profile view instance providing real profile drawing
	ProfileLhcView	*pView;

	// Actions of vacuum types menu
	QList<QAction *>	vacTypeActions;

	// Actions of CRYO equipment types menu
	QList<QAction *>	cryoTypeActions;

	// Actions of BEAM equipment types menu
	QList<QAction *>	beamTypeActions;

	QAction				*pActionSectorNames;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Data to be shown in dialog //////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Vacuum type mask
	unsigned		vacTypeMask;

	// Equipment type mask for all possible equipment types on isolation vacuum
	VacEqpTypeMask	allCryoEqpMask;

	// Equipment type mask for all possible equipment types on beam vacuum
	VacEqpTypeMask	allBeamEqpMask;

	// Equipment type mask on isolation vacuum
	VacEqpTypeMask	cryoEqpMask;

	// Equipment type mask on beam vacuum
	VacEqpTypeMask	beamEqpMask;

	// Pointer to first sector to be shown
	Sector			*pFirstSector;

	// Pointer to last sector to be shown
	Sector			*pLastSector;

	// Data acquisition mode
	DataEnum::DataMode		mode;

	// Limits for beam vacuum profile + flag indicating limits were changed
	int			beamVacMin;
	int			beamVacMax;
	bool		beamVacLimitsChanged;

	// Limits for cryo vacuum profile + flag indicating limits were changed
	int			cryoVacMin;
	int			cryoVacMax;
	bool		cryoVacLimitsChanged;

	// Limits for cryo temperature profile + flag indicating limits were changed
	int			cryoTempMin;
	int			cryoTempMax;
	bool		cryoTempLimitsChanged;

	void buildInitialEqpMask(void);

	void buildLayout(void);
	void buildFileMenu(void);
	void buildViewMenu(void);
	void buildHelpMenu(void);
	void buildMainPartCombo(void);
	void selectInitialMp(void);

	void setTitle(void);
	void buildView(bool init);

	void makeDialogRequest(int type);

private slots:
	void filePrint(void);

	void typeMenu(QAction *pAction);
	void vacTypeMenu(QAction *pAction);
	void cryoEqpTypeMenu(QAction *pAction);
	void beamEqpTypeMenu(QAction *pAction);
	void sectNamesToggled(bool flag);
	void viewSector(void);
	void viewSynoptic(void);
	void help(void);
	void comboActivated(const QString &name);

	void mobileStateChange(void);
	void dpMouseDown(int button, int mode, int x, int y, const char *dpName);
	void setTypesMenu(void);

	void cryoLimitsChange(int min, int max);
	void beamLimitsChange(int min, int max);
	void linLimitsChange(int min, int max);

};

#endif	// PROFILELHCDIALOG_H
