#ifndef	VACCTLUTILEXPORT_H
#define	VACCTLUTILEXPORT_H

#include <QtCore/QtGlobal>

#include "PlatformDef.h"

#if defined(BUILDING_VACCTLUTIL)
#	define VACCTLUTIL_EXPORT	Q_DECL_EXPORT
#else
#define	VACCTLUTIL_EXPORT		Q_DECL_IMPORT
#endif

#endif	// VACCTLUTILEXPORT_H
