#ifndef	HISTORYEQPVALUE_H
#define	HISTORYEQPVALUE_H

// Class holding one analog value at given moment in time

#include <QDateTime>

#include <stdlib.h>

#include "PlatformDef.h"

class HistoryEqpValue
{
public:
	HistoryEqpValue(const QDateTime &time, float value);
	~HistoryEqpValue();

	// Access
	inline const QDateTime & getTime(void) const { return time; }
	inline float getValue(void) const { return value; }

protected:
	// Timestamp
	QDateTime	time;

	// Value
	float		value;
};

#endif	// HISTORYEQPVALUE_H
