#include "RackIconVRPIT.h"
#include "EqpVRPI.h"
#include "EqpVPI.h"

#include "DataPool.h"
#include "qpainter.h"

RackIconVRPIT::RackIconVRPIT(QWidget *parent, Qt::WindowFlags f) :
RackIcon(parent, f)
{
	pEqp = NULL;
	//setFixedSize(21, 21); see about this later
	slaveConnected = false;
	QObject::connect(&slaveConnection, SIGNAL(stateChanged()), this, SLOT(forceRedraw()));
	QObject::connect(&slaveConnection, SIGNAL(selectChanged()), this, SLOT(forceRedraw()));
}

RackIconVRPIT::~RackIconVRPIT()
{
	disconnect();
	QObject::disconnect(&slaveConnection, SIGNAL(selectChanged()), this, SLOT(forceRedraw()));
	QObject::disconnect(&slaveConnection, SIGNAL(stateChanged()), this, SLOT(forceRedraw()));
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRPIT::disconnect(void)
{
	bool result = false;
	if (connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	if (slaveConnected && pSlaveEqp)
	{
		slaveConnection.disconnect(pSlaveEqp);
		result = result && true;
	}
	connected = false;
	slaveConnected = false;
	return result;
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRPIT::connect(void)
{
	if (connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	//dpes.append("PR");
	connection.connect(pEqp, dpes, DataEnum::Online);
	connected = true;
	if(connectSlave())
		return true;
	else
		return false;
}

/*
**	FUNCTION
**		Connect to slave device for pressure reading
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRPIT::connectSlave(void)
{
	if (slaveConnected || (!pSlaveEqp))
	{
		return false;
	}
	QStringList dpes;
	//dpes.append("EqpStatus");
	//dpes.append("RR1");
	dpes.append("PR");
	slaveConnection.connect(pSlaveEqp, dpes, DataEnum::Online);
	slaveConnected = true;
	return true;
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void RackIconVRPIT::setEqp(Eqp *pEqp)
{
	disconnect();
	DataPool &pool = DataPool::getInstance();
	if (pEqp != NULL)
	{
		//Q_ASSERT_X(pEqp->inherits("EqpVRPI"), "RackIconVRPIT::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVRPI *)pEqp;

		QStringList slaveList;
		pEqp->getSlaves(slaveList);

		if (slaveList.size() > 0)
		{
			this->pSlaveEqp = pool.findEqpByDpName(slaveList.at(0).toUtf8().data());
		}
	}
	forceRedraw();
}

void RackIconVRPIT::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.scale(scale, scale);
	QFont calibri("Calibri", 12, QFont::Bold);
	QFont cableFont("Calibri", 10, QFont::Normal);
	QFont calibriSmall("Calibri", 8);
	QString pressure(" -- ");
	QColor LEDColor;
	QPen transparentPen = QPen(Qt::transparent);
	QPen selectionPen = QPen(Qt::red);
	selectionPen.setWidth(3);
	int textFlags;

	if (pEqp)
	{
		QDateTime timeStamp;
		QVariant dpValue = pSlaveEqp->getDpeValue("PR", DataEnum::Online, timeStamp);
		double dPressure = dpValue.toDouble();
		
		pressure = QString("%1").arg(dPressure,0, 'e',3);

		pEqp->getMainColor(LEDColor, DataEnum::Online);
	}

	painter.setBrush(QColor(220, 220, 220));
	painter.setFont(calibri);
	textFlags = Qt::AlignHCenter | Qt::AlignTop | Qt::TextWrapAnywhere;


	if (isSelected)
	{
		painter.setPen(selectionPen);
	}
	else
	{
		painter.setPen(Qt::black);
	}
		
	QRect equipmentRect(0, 0, this->sizeX, this->sizeY);
	//painter.setPen(Qt::black);
	painter.drawRect(equipmentRect);
	painter.setPen(Qt::black);
	painter.drawText(equipmentRect, textFlags, this->equipmentName);

	//write cable number
	painter.setFont(cableFont);

	//detailText.adjust(RACK_PIXEL_WIDTH/2, 12, 0, 0);
	QString attributeValue, ET200Channel;

	attributeValue = attributes.value("CableVPI");
	painter.drawText(equipmentRect.left() + this->sizeX / 2, equipmentRect.top() + 14, this->sizeX / 2, 16, Qt::AlignCenter, "SVA3 " + attributeValue);

	//write channel + et200 rack
	attributeValue = attributes.value("RackET200");
	ET200Channel = attributes.value("ChannelET200");
	painter.drawText(equipmentRect.left() + this->sizeX / 2, equipmentRect.top() + 27, this->sizeX / 2, 16, Qt::AlignCenter, "CH " + ET200Channel + " " + attributeValue);

	//write cable to et200
	attributeValue = attributes.value("CableET200");
	painter.drawText(equipmentRect.left() + this->sizeX / 2, equipmentRect.top() + 40, this->sizeX / 2, 16, Qt::AlignCenter, "MCA8 " + attributeValue);

	//draw status

	painter.setBrush(LEDColor);
	painter.drawEllipse(equipmentRect.left() + 58, equipmentRect.top() + 17, 8, 8);
	painter.setFont(calibriSmall);
	painter.drawText(equipmentRect.left() + 10, equipmentRect.top() + 16, 40, 8, Qt::AlignVCenter | Qt::AlignLeft, "Status");

	//read current
	//outside rectangle
	painter.setBrush(Qt::white);
	painter.drawRect(equipmentRect.left() + 8, equipmentRect.top() + 30, 60, 24);
	painter.setBrush(Qt::gray);
	painter.drawRect(equipmentRect.left() + 8, equipmentRect.top() + 30, 60, 12);

	//write current
	painter.setFont(calibriSmall);
	painter.drawText(equipmentRect.left() + 8, equipmentRect.top() + 30, 60, 12, Qt::AlignCenter, "Current (A)");
	painter.drawText(equipmentRect.left() + 8, equipmentRect.top() + 42, 60, 12, Qt::AlignCenter, pressure);
}