//	Implementation of ExcelWriter class
/////////////////////////////////////////////////////////////////////////////////

#include "ExcelWriter.h"

#include <errno.h>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

ExcelWriter::ExcelWriter()
{
	pRowList = new QList<ExcelWriterRow *>();
	pColumnFormatList = new QList<ExcelWriterColumnFormat *>();
}

ExcelWriter::~ExcelWriter()
{
	clear();
	delete pColumnFormatList;
	delete pRowList;
}

void ExcelWriter::clear(void)
{
	while(!pRowList->isEmpty())
	{
		delete pRowList->takeFirst();
	}
	while(!pColumnFormatList->isEmpty())
	{
		delete pColumnFormatList->takeFirst();
	}
}

/*
**	FUNCTION
**		Add empty row
**
**	ARGUMENTS
**		None
**
**	RETURNS
*		Row index for new row
**
**	CAUTIONS
**		None
*/
int ExcelWriter::addRow(void)
{
	pRowList->append(new ExcelWriterRow(pRowList->count() + 1));
	return pRowList->count();
}

/*
**	FUNCTION
**		Set format for given cell
**
**	ARGUMENTS
**		row		- Row index
**		column	- column number
**		format	- format to set
**
**	RETURNS
*		None
**
**	CAUTIONS
**		None
*/
void ExcelWriter::setCellFormat(int row, int column, ExcelWriterCell::Format format)
{
	ExcelWriterRow *pRow = findRow(row);
	if(pRow)
	{
		pRow->setCellFormat(column, format);
	}
}

/*
**	FUNCTION
**		Set background color for given cell
**
**	ARGUMENTS
**		row		- Row index
**		column	- column number
**		color	- color to set
**
**	RETURNS
*		None
**
**	CAUTIONS
**		None
*/
void ExcelWriter::setCellColor(int row, int column, const QColor &color)
{
	ExcelWriterRow *pRow = findRow(row);
	if(pRow)
	{
		pRow->setCellColor(column, color);
	}
}

/*
**	FUNCTION
**		Set string to be shown in given cell
**
**	ARGUMENTS
**		row		- Row index
**		column	- column number
**		string	- string to set
**
**	RETURNS
*		None
**
**	CAUTIONS
**		None
*/
void ExcelWriter::setCellValue(int row, int column, const QString &string)
{
	ExcelWriterRow *pRow = findRow(row);
	if(pRow)
	{
		pRow->setCellValue(column, string);
	}
}

/*
**	FUNCTION
**		Set date+time to be shown in given cell
**
**	ARGUMENTS
**		row		- Row index
**		column	- column number
**		dateTime	- date+time to set
**		withMsec	- Flag indicating if milliseconds shall be shown
**
**	RETURNS
*		None
**
**	CAUTIONS
**		None
*/
void ExcelWriter::setCellValue(int row, int column, const QDateTime &dateTime, bool withMsec)
{
	ExcelWriterRow *pRow = findRow(row);
	if(pRow)
	{
		pRow->setCellValue(column, dateTime, withMsec);
	}
}

/*
**	FUNCTION
**		Set floating point value to be shown in given cell
**
**	ARGUMENTS
**		row		- Row index
**		column	- column number
**		value	- value to set
**
**	RETURNS
*		None
**
**	CAUTIONS
**		None
*/
void ExcelWriter::setCellValue(int row, int column, float value)
{
	ExcelWriterRow *pRow = findRow(row);
	if(pRow)
	{
		pRow->setCellValue(column, value);
	}
}

/*
**	FUNCTION
**		Set integer value to be shown in given cell
**
**	ARGUMENTS
**		row		- Row index
**		column	- column number
**		value	- value to set
**
**	RETURNS
*		None
**
**	CAUTIONS
**		None
*/
void ExcelWriter::setCellValue(int row, int column, int value)
{
	ExcelWriterRow *pRow = findRow(row);
	if(pRow)
	{
		pRow->setCellValue(column, value);
	}
}

/*
**	FUNCTION
**		Set comment to be shown in given cell
**
**	ARGUMENTS
**		row		- Row index
**		column	- column number
**		comment	- comment to set
**
**	RETURNS
*		None
**
**	CAUTIONS
**		None
*/
void ExcelWriter::setCellComment(int row, int column, const QString &comment)
{
	ExcelWriterRow *pRow = findRow(row);
	if(pRow)
	{
		pRow->setCellComment(column, comment);
	}
}

/*
**	FUNCTION
**		Set title for column with given number
**
**	ARGUMENTS
**		column	- column number
**		title	- title to set
**
**	RETURNS
*		None
**
**	CAUTIONS
**		None
*/
void ExcelWriter::setColumnTitle(int column, const QString &title)
{
	ExcelWriterColumnFormat *pCol = findColumnFormat(column);
	if(pCol)
	{
		pCol->setTitle(title);
	}
}

/*
**	FUNCTION
**		Set width [pixels?] for column with given number
**
**	ARGUMENTS
**		column	- column number
**		width	- width to set
**
**	RETURNS
*		None
**
**	CAUTIONS
**		None
*/
void ExcelWriter::setColumnWidth(int column, int width)
{
	if(width <= 0)
	{
		return;
	}
	ExcelWriterColumnFormat *pCol = findColumnFormat(column);
	if(pCol)
	{
		pCol->setWidth(width);
	}
}

/*
**	FUNCTION
**		Write content to output file
**
**	ARGUMENTS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriter::write(FILE *pFile)
{
	if(!pFile)
	{
		return QString("File not open");
	}
	if(pRowList->isEmpty())
	{
		return QString("Nothing to write");
	}

	// Build list of all background colors in all cells
	QList<ExcelCellFormat *>	allFormats;
	ExcelWriterRow *pRow;
	for(int row = 0 ; row < pRowList->count() ; row++)
	{
		pRow = pRowList->at(row);
		QList<ExcelCellFormat *> rowFormats;
		pRow->getCellFormats(rowFormats);
		for(int idx = 0 ; idx < rowFormats.count() ; idx++)
		{
			ExcelCellFormat *pNewFormat = rowFormats.at(idx);
			bool found = false;
			for(int oldIdx = 0 ; oldIdx < allFormats.count() ; oldIdx++)
			{
				ExcelCellFormat *pOldFormat = allFormats.at(oldIdx);
				if(*pOldFormat == *pNewFormat)
				{
					found = true;
					break;
				}
			}
			if(!found)
			{
				allFormats.append(pNewFormat);
			}
		}
		rowFormats.clear();
	}

	// Write file header
	QString error = writeHeader(pFile);
	if(!error.isEmpty())
	{
		return error;
	}

	// Write all styles (back colors)
	error = writeStyles(pFile, allFormats);
	if(!error.isEmpty())
	{
		return error;
	}

	// Write body - may be split to several sheets
	int sheetNumber = 1;
	int rowNumber = 0;
	for(int rowIdx = 0 ; rowIdx < pRowList->count() ; rowIdx++)
	{
		pRow = pRowList->at(rowIdx);
		if(rowNumber > 50000)
		{
			rowNumber = 0;
		}
		if(!rowNumber)	// Start new sheet
		{
			if(sheetNumber > 1)
			{
				error = finishSheet(pFile);
				if(!error.isEmpty())
				{
					return error;
				}
			}
			error = startSheet(pFile, sheetNumber++);
			if(!error.isEmpty())
			{
				return error;
			}
		}
		error = pRow->write(pFile);
		if(!error.isEmpty())
		{
			return error;
		}
		rowNumber++;
	}

	// Finally - finish last sheet and workbook
	error = finishSheet(pFile);
	if(!error.isEmpty())
	{
		return error;
	}
	int coco = fprintf(pFile, "</Workbook>\n");
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	while(!allFormats.isEmpty())
	{
		delete allFormats.takeFirst();
	}
	return QString();
}

/*
**	FUNCTION
**		Write content to output CSV file
**
**	ARGUMENTS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriter::writeCsv(FILE *pFile)
{
	if(!pFile)
	{
		return QString("File not open");
	}
	if(pRowList->isEmpty())
	{
		return QString("Nothing to write");
	}
	startSheetCsv(pFile);
	for(int rowIdx = 0 ; rowIdx < pRowList->count() ; rowIdx++)
	{
		ExcelWriterRow *pRow = pRowList->at(rowIdx);
		QString error = pRow->writeCsv(pFile);
		if(!error.isEmpty())
		{
			return error;
		}
	}
	return QString();
}

/*
**	FUNCTION
**		Find pointer to row with given row index
**
**	ARGUMENTS
**		row	- row index to find
**
**	RETURNS
*		Pointer to row with given index; or
**		NULL if such row is not found
**
**	CAUTIONS
**		None
*/
ExcelWriterRow *ExcelWriter::findRow(int row)
{
	for(int idx = pRowList->count() - 1 ; idx >= 0 ; idx--)
	{
		ExcelWriterRow *pRow = pRowList->at(idx);
		if(pRow->getRow() == row)
		{
			return pRow;
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Find pointer to column with given column index, create
**		new format if not found
**
**	ARGUMENTS
**		column	- column index to find
**
**	RETURNS
*		Pointer to column format with given index; or
**		NULL if column index is not valid (must be in range 1...255)
**
**	CAUTIONS
**		None
*/
ExcelWriterColumnFormat *ExcelWriter::findColumnFormat(int column)
{
	if((column < 1) || ( column > 255))
	{
		return NULL;
	}
	ExcelWriterColumnFormat *pCol;
	for(int idx = 0 ; idx < pColumnFormatList->count() ; idx++)
	{
		pCol = pColumnFormatList->at(idx);
		if(pCol->getColumn() == column)
		{
			return pCol;
		}
	}
	pCol = new ExcelWriterColumnFormat(column);
	pColumnFormatList->append(pCol);
	return pCol;
}

/*
**	FUNCTION
**		Write Excel file header to output file
**
**	ARGUMENTS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriter::writeHeader(FILE *pFile)
{
	int coco = fprintf(pFile, "<?xml version=\"1.0\"?>\n\
<?mso-application progid=\"Excel.Sheet\"?>\n\
<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\n\
 xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n\
 xmlns:x=\"urn:schemas-microsoft-com:office:excel\"\n\
 xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"\n\
 xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n\
 <DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">\n\
  <Author>VacPVSS</Author>\n\
  <LastAuthor>VacPVSS</LastAuthor>\n\
  <Created>%s</Created>\n\
  <Company>CERN</Company>\n\
  <Version>11.6568</Version>\n\
 </DocumentProperties>\n\
 <ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">\n\
  <WindowHeight>10000</WindowHeight>\n\
  <WindowWidth>10000</WindowWidth>\n\
  <WindowTopX>105</WindowTopX>\n\
  <WindowTopY>105</WindowTopY>\n\
  <ProtectStructure>False</ProtectStructure>\n\
  <ProtectWindows>False</ProtectWindows>\n\
 </ExcelWorkbook>\n\
 <Styles>\n\
  <Style ss:ID=\"Default\" ss:Name=\"Normal\">\n\
   <Alignment ss:Vertical=\"Bottom\"/>\n\
   <Borders/>\n\
   <Font/>\n\
   <Interior/>\n\
   <NumberFormat/>\n\
   <Protection/>\n\
  </Style>\n", QDateTime::currentDateTime().toString("yyyy-MM-ddThh:mm:ssZ").toLatin1().constData());

	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	return QString();
}

/*
**	FUNCTION
**		Write all styles (background colors + numeric format) to output file.
**		Set style indices to all cells using every format.
**
**	ARGUMENTS
**		pFile	- Pointer to file opened for writing
**		formats	- List of all non-defaults formats, used by cells
**
**	RETURNS
**		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		<Styles> section must be opened before calling this method
**		(see writeHeader()), this method closes <Styles> section
*/
QString ExcelWriter::writeStyles(FILE *pFile, QList<ExcelCellFormat *> &formats)
{
	int coco = 0;
	for(int n = 0 ; n < formats.count() ; n++)
	{
		ExcelCellFormat *pFormat = formats.at(n);
		if(pFormat->getColor() != Qt::white)
		{
			coco = writeColoredStyle(pFile, pFormat, n);
		}
		else
		{
			coco = writeColorlessStyle(pFile, pFormat, n);
		}
		if(coco < 0)
		{
			return QString("File write failed: errno %1").arg(errno);
		}
		for(int idx = 0 ; idx < pRowList->count() ; idx++)
		{
			ExcelWriterRow *pRow = pRowList->at(idx);
			pRow->setCellStyleIndex(*pFormat, n);
		}
	}
	// TODO:
	/*
	<Style ss:ID=\"pressure\">\n" +
			"   <NumberFormat ss:Format=\"Scientific\"/>\n" +
			"  </Style>\n"
		" </Styles>\n"
	*/
	coco = fprintf(pFile, " </Styles>\n");
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	return QString();
}

/*
**	FUNCTION
**		Write colorsed style to output file
**
**	ARGUMENTS
**		pFile		- Pointer to file opened for writing
**		pFormat		- Pointer to format being written
**		index		- style index
**
**	RETURNS
**		negative value	- in case of error;
**		other value - success
**
**	CAUTIONS
**		None
*/
int ExcelWriter::writeColoredStyle(FILE *pFile, ExcelCellFormat *pFormat, unsigned index)
{
	int coco = fprintf(pFile, "  <Style ss:ID=\"cellStyle%d\">\n\
   <Interior ss:Color=\"#%02X%02X%02X\" ss:Pattern=\"Solid\"/>\n", index,
		pFormat->getColor().red(), pFormat->getColor().green(), pFormat->getColor().blue());
	if(coco < 0)
	{
		return coco;
	}
	coco = addDataFormat(pFile, pFormat);
	if(coco < 0)
	{
		return coco;
	}
	return fprintf(pFile, "  </Style>\n");
}

/*
**	FUNCTION
**		Write colorsless style to output file
**
**	ARGUMENTS
**		pFile		- Pointer to file opened for writing
**		pFormat		- Pointer to format being written
**		index		- style index
**
**	RETURNS
**		negative value	- in case of error;
**		other value - success
**
**	CAUTIONS
**		None
*/
int ExcelWriter::writeColorlessStyle(FILE *pFile, ExcelCellFormat *pFormat, unsigned index)
{
	int coco = fprintf(pFile, "  <Style ss:ID=\"cellStyle%d\">\n", index);
	if(coco < 0)
	{
		return coco;
	}
	coco = addDataFormat(pFile, pFormat);
	if(coco < 0)
	{
		return coco;
	}
	return fprintf(pFile, "  </Style>\n");
}

/*
**	FUNCTION
**		Add data type dependent part of cell style to output file
**
**	ARGUMENTS
**		pFile		- Pointer to file opened for writing
**		pFormat		- Pointer to format being written
**
**	RETURNS
**		negative value	- in case of error;
**		other value - success
**
**	CAUTIONS
**		None
*/
int ExcelWriter::addDataFormat(FILE *pFile, ExcelCellFormat *pFormat)
{
	int coco = 0;
	switch(pFormat->getFormat())
	{
	case ExcelWriterCell::String:
		break;
	case ExcelWriterCell::DateTime:
		coco = fprintf(pFile, "   <NumberFormat ss:Format=\"yyyy-mm-dd hh:mm:ss\"/>\n");
		break;
	case ExcelWriterCell::DateTimeWithMsec:
		coco = fprintf(pFile, "   <NumberFormat ss:Format=\"yyyy-mm-dd hh:mm:ss.000\"/>\n");
		break;
	case ExcelWriterCell::Float:
		coco = fprintf(pFile, "   <NumberFormat ss:Format=\"Scientific\"/>\n");
		break;
	}
	return coco;
}

/*
**	FUNCTION
**		Write start of Excel sheet to output file.
**
**	ARGUMENTS
**		pFile		- Pointer to file opened for writing
**		sheetNumber	- Sheet number
**
**	RETURNS
**		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriter::startSheet(FILE *pFile, int sheetNumber)
{
	int coco = fprintf(pFile, " <Worksheet ss:Name=\"Page_%d\">\n\
  <Table x:FullColumns=\"1\" x:FullRows=\"1\">\n", sheetNumber);
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	bool haveTitles = false;
	ExcelWriterColumnFormat *pCol;
	int idx;
	for(idx = 0 ; idx < pColumnFormatList->count() ; idx++)
	{
		pCol = pColumnFormatList->at(idx);
		coco = fprintf(pFile, "   <Column ss:Index=\"%d\" ss:AutoFitWidth=\"1\" ss:Width=\"%d\"/>\n",
					pCol->getColumn(), pCol->getWidth() ? pCol->getWidth() : 50);
		if(coco < 0)
		{
			return QString("File write failed: errno %1").arg(errno);
		}
		if(!pCol->getTitle().isEmpty())
		{
			haveTitles = true;
		}
	}
	if(!haveTitles)
	{
		return QString();
	}

	// Write column titles row
	coco = fprintf(pFile, "   <Row>\n");
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	for(idx = 0 ; idx < pColumnFormatList->count() ; idx++)
	{
		pCol = pColumnFormatList->at(idx);
		if(!pCol->getTitle().isEmpty())
		{
			coco = fprintf(pFile, "    <Cell ssIndex=\"%d\"><Data ss:Type=\"String\">%s</Data></Cell>\n",
				pCol->getColumn(), pCol->getTitle().toLatin1().constData());
			if(coco < 0)
			{
				return QString("File write failed: errno %1").arg(errno);
			}
		}
	}
	coco = fprintf(pFile, "   </Row>\n");
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	return QString();
}

/*
**	FUNCTION
**		Write end of Excel sheet to output file.
**
**	ARGUMENTS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriter::finishSheet(FILE *pFile)
{
	int coco = fprintf(pFile, "  </Table>\n\
  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">\n\
   <PageSetup>\n\
    <PageMargins x:Bottom=\"0.5\" x:Left=\"0.5\"\n\
     x:Right=\"0.75\" x:Top=\"0.75\"/>\n\
   </PageSetup>\n\
   <Print>\n\
    <ValidPrinterInfo/>\n\
    <PaperSizeIndex>1</PaperSizeIndex>\n\
    <HorizontalResolution>600</HorizontalResolution>\n\
    <VerticalResolution>600</VerticalResolution>\n\
   </Print>\n\
   <Selected/>\n\
   <Panes>\n\
    <Pane>\n\
     <Number>3</Number>\n\
     <ActiveRow>1</ActiveRow>\n\
     <ActiveCol>1</ActiveCol>\n\
    </Pane>\n\
   </Panes>\n\
   <ProtectObjects>False</ProtectObjects>\n\
   <ProtectScenarios>False</ProtectScenarios>\n\
  </WorksheetOptions>\n\
 </Worksheet>\n");
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	return QString();
}

/*
**	FUNCTION
**		Write start of Excel sheet to output CSV file.
**
**	ARGUMENTS
**		pFile		- Pointer to file opened for writing
**		sheetNumber	- Sheet number
**
**	RETURNS
**		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriter::startSheetCsv(FILE *pFile)
{
	bool haveTitles = false;
	ExcelWriterColumnFormat *pCol;
	int idx, coco;
	for(idx = 0 ; idx < pColumnFormatList->count() ; idx++)
	{
		pCol = pColumnFormatList->at(idx);
		if(!pCol->getTitle().isEmpty())
		{
			haveTitles = true;
			break;
		}
	}
	if(!haveTitles)
	{
		return QString();
	}

	// Write column titles row
	for(idx = 0 ; idx < pColumnFormatList->count() ; idx++)
	{
		pCol = pColumnFormatList->at(idx);
		coco = fprintf(pFile, idx == 0 ? "%s" : "\t%s", pCol->getTitle().isEmpty() ? "" : pCol->getTitle().toLatin1().constData());
		if(coco < 0)
		{
			return QString("File write failed: errno %1").arg(errno);
		}
	}
	coco = fprintf(pFile, "\n");
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	return QString();
}

