#ifndef	EQPVV_PS_CMW_H
#define EQPVV_PS_CMW_H

//	VV_PS_CMW - valves in PSR, data from CMW

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVV_PS_CMW : public Eqp
{
	Q_OBJECT

public:
	EqpVV_PS_CMW(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVV_PS_CMW(const EqpVV_PS_CMW &src) : Eqp(src) {}
#endif
	~EqpVV_PS_CMW();

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual bool isSpecColor(void) { return true; }
	virtual inline bool isForSectorView(void) { return !skipOnSynoptic; }

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual int getDrawOrder(void);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline unsigned getRR1(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline bool isDriverOK(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? driverOkReplay : driverOkOnline;
	}
	
protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Flag indicating if this type has RR2 DPE
	bool		haveRr2;

	// Last known value of DriverOK DPE for online mode
	bool		driverOkOnline;

	// Last known value of DriverOK DPE for replay mode
	bool		driverOkReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(bool driverOk, unsigned rr1, QColor &color);
	virtual void getMainStateString(bool driverOk, unsigned rr1, QString &string);
	virtual void getMainStateStringForMenu(bool driverOk, unsigned rr1, QString &string);
	virtual void getToolTipString(bool driverOk, unsigned rr1, QString &string);
#endif
};

#endif	// EQPVV_PS_CMW_H
