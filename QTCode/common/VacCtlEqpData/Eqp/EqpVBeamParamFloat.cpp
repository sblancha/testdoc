//	Implementation of EqpVBeamParamFloat class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVBeamParamFloat.h"

#include "DataPool.h"
#include "FunctionalType.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"


#include <QColor>

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVBeamParamFloat::EqpVBeamParamFloat(const Eqp &source) : Eqp(source)
{
	valueOnline = valueReplay = 0;
	functionalType = FunctionalType::VBEAM_PARAM_FLOAT;

	// Decide for vacuum type based on last character in DP name
	if(dpName)
	{
		if(dpName[strlen(dpName) - 1] == 'B')
		{
			vacType = VacType::BlueBeam;
		}
		else
		{
			vacType = VacType::RedBeam;
		}
	}
}

EqpVBeamParamFloat::~EqpVBeamParamFloat()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to analog value of device. The list is used
**		to build value history
**
**	ARGUMENTS
**		valueDpes	- Variable where all value DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVBeamParamFloat::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	addDpeNameToList(valueDpes, "Value");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVBeamParamFloat::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "Value", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVBeamParamFloat::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "Value", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVBeamParamFloat::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "Value"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(valueOnline == newValue)
			{
				return;
			}
			valueOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamFloat::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "Value"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(valueReplay == newValue)
			{
				return;
			}
			valueReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

DataEnum::ValueType EqpVBeamParamFloat::getMainValueType(void) const
{
	DataEnum::ValueType result = DataEnum::ValueTypeNone;
	switch(ctrlSubType)
	{
	case 1:	// Beam current [mA]
		result = DataEnum::BeamCurrent;
		break;
	case 2:	// Beam energy
		result = DataEnum::Energy;
		break;
	case 3:	// Crit. energy
		result = DataEnum::CritEnergy;
		break;
	case 4:	// PhF
		result = DataEnum::PhF;
		break;
	case 5:	// PhDm
		result = DataEnum::PhDm;
		break;
	case 6:	// PhDmA
		result = DataEnum::PhDmA;
		break;
	default:
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Return main value for device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamFloat::getMainColor(QColor &color, DataEnum::DataMode /* mode */)
{
	color.setRgb(255, 255, 255);	// Neutral white
}

float EqpVBeamParamFloat::getMainValue(DataEnum::DataMode mode)
{
	return mode == DataEnum::Replay ? valueReplay : valueOnline;
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamFloat::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	getMainValueString(string, mode);
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamFloat::getMainValueString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainValueString(valueReplay, string);
	}
	else
	{
		getMainValueString(valueOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		intensity	- Value DPE
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamFloat::getMainValueString(float value, QString &string)
{
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%.2E", value);
#else
	sprintf(buf, "%.2E", value);
#endif
	string = buf;
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamFloat::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainStateStringForMenu(valueReplay, string);
	}
	else
	{
		getMainStateStringForMenu(valueOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		intensity	- Value DPE
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamFloat::getMainStateStringForMenu(float value, QString &string)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainValueString(value, valueString);
	string += valueString;
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamFloat::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	getMainStateStringForMenu(string, mode);
}
#endif





/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamFloat::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	addDpeNameToList(dpes, "Value");
}
#endif
/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history)
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVBeamParamFloat::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}
	return 0;
}
#endif


