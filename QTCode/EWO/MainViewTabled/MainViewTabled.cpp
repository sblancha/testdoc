//	Implementation of MainViewTabled class
////////////////////////////////////////////////////////////////////////////////////

#include "MainViewTabled.h"

#include "Eqp.h"

#include <qpainter.h>

MainViewTabled::MainViewTabled(QWidget *parent, Qt::WindowFlags flags)
	: VacMainView(parent, flags)
{
	setMinimumSize(3, 3);

	// Connect to signals
	connect(&connection, SIGNAL(dpDataChanged(OneDpConnection *)),
		this, SLOT(dpDataChange(OneDpConnection *)));
}

MainViewTabled::~MainViewTabled()
{
	disconnectAll();
}

void MainViewTabled::paintEvent(QPaintEvent * /* event */)
{
	QPainter painter(this);
	if(connection.getEqpCount())
	{
		painter.fillRect(rect(), Qt::green);
	}
	else
	{
		painter.fillRect(rect(), Qt::blue);
	}
}

////////////////////////////////////////////////////////////////////////////////////////
////////////////// Implementation of VacMainView abstract methods //////////////////////
////////////////////////////////////////////////////////////////////////////////////////
void MainViewTabled::dialogRequest(int type, unsigned vacTypes, const QStringList &sectorList, DataEnum::DataMode mode)
{
	emit dialogRequested(type, vacTypes, sectorList, mode);
}

/*
**	FUNCTION
**		Slot activate when one of DP values have been changed.
**		Add DP data to queue of pending changes, emit signal if
**		queue was empty before this event
**
**	ARGUMENTS
**		pDpData	- Pointer to DP data that has been changed
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainViewTabled::dpDataChange(OneDpConnection *pDpData)
{
	mutex.lock();
	bool needSignal = dpChanges.isEmpty();
	if(!needSignal)
	{
		if(dpChanges.indexOf(pDpData) < 0)
		{
			dpChanges.append(pDpData);
		}
	}
	else
	{
		dpChanges.append(pDpData);
	}
	mutex.unlock();
	if(needSignal)
	{
		emit dpDataChanged();
	}
}

/*
**	FUNCTION
**		Return tag of first DP change pending in the queue
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		tag of first DP change; or
**		-1 if queue is empty
**
**	CAUTIONS
**		None
*/
int MainViewTabled::getNextChangeTag(void)
{
	mutex.lock();
	int coco = -1;
	if(!dpChanges.isEmpty())
	{
		OneDpConnection *pDpData = dpChanges.takeFirst();
		coco = pDpData->getTag();
	}
	mutex.unlock();
	return coco;
}

/*
**	FUNCTION
**		Return DP name of first DP change pending in the queue
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		DP name of first DP change; or
**		NULL if queue is empty
**
**	CAUTIONS
**		None
*/
const char *MainViewTabled::getNextChangeDp(void)
{
	mutex.lock();
	const char *result = NULL;
	if(!dpChanges.isEmpty())
	{
		OneDpConnection *pDpData = dpChanges.takeFirst();
		result = pDpData->getEqp()->getDpName();
	}
	mutex.unlock();
	return result;
}

int MainViewTabled::connectDp(const char *dp, int tag)
{
	int coco = connection.connectDp(dp, tag);
	update();
	return coco;
}

/*
**	FUNCTION
**		Disconnect given DP, remove pending changes for this DP from queue
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Result of MainViewTabledEqpConnect.disconnectDp()
**
**	CAUTIONS
**		None
*/
int MainViewTabled::disconnectDp(const char *dp)
{
	mutex.lock();
	for(int idx = 0 ; idx < dpChanges.count() ; idx++)
	{
		OneDpConnection *pDpData = dpChanges.at(idx);
		if(!strcmp(dp, pDpData->getEqp()->getDpName()))
		{
			dpChanges.removeAt(idx);
			break;
		}
	}
	int coco = connection.disconnectDp(dp);
	mutex.unlock();
	update();
	return coco;
}

/*
**	FUNCTION
**		Disconnect all DPs, clear pending changes queue
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Result of MainViewTabledEqpConnect.disconnectAll()
**
**	CAUTIONS
**		None
*/
int MainViewTabled::disconnectAll(void)
{
	mutex.lock();
	int coco = connection.disconnectAll();
	dpChanges.clear();
	mutex.unlock();
	update();
	return coco;
}

