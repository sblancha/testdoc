#ifndef	PROFILEALLLHCBEAM_H
#define	PROFILEALLLHCBEAM_H

//	All LHC profile - for blue/red beams

#include "ProfileAllLHCBeamWidget.h"

#include <qdialog.h>

class ProfileAllLHCBeam : public QDialog
{
	Q_OBJECT

public:
	ProfileAllLHCBeam();
	~ProfileAllLHCBeam();

	static bool alreadyOpen(void);

protected:
	// Instances of 2 dialogs
	static ProfileAllLHCBeam	*dialogs[2];

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Widget with images
	ProfileAllLHCBeamWidget	*pWidget;

	// Index of this instance in array of dialogs
	int							myIndex;

	// ID of 'Period' item in popup menu
	QAction 						*pPeriodMenuAction;

	virtual void resizeEvent(QResizeEvent *e);

	inline void setBeamLimits(int min, int max) { if(pWidget) { pWidget->setBeamLimits(min, max); } }
	inline void setLinLimits(int min, int max) { if(pWidget) { pWidget->setLinLimits(min, max); } }

	void analyzeTypes(VacEqpTypeMask &allMpMask);
	void applyMask(VacEqpTypeMask &newEqpMask);

private slots:
//	void dpMouseDown(int button, int mode, int x, int y, const char *dpName);
	void beamLimitsChange(int min, int max);
	void linLimitsChange(int min, int max);
	void labelPress(QMouseEvent *e);
	void mpMenu(QAction *pAction);
};

#endif	// PROFILEALLLHCSINGLEBEAMDIALOG_H
