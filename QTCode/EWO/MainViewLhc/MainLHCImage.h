#ifndef MAINLHCIMAGE_H
#define MAINLHCIMAGE_H

// Drawing control for main view of LHC machine

#include "LhcSegment.h"
#include "LhcConnectLine.h"
//#include "MainLHCImageEqpConnect.h"


#include <qwidget.h>
#include <qlist.h>
#include <qlabel.h>
#include <qtimer.h>
#include <qdatetime.h>

#include <stdio.h>

class LegendLabel;
class ExtendedLabel;

class SectorDrawPart;
class Sector;

class MainLHCImage : public QWidget
{
	Q_OBJECT

public:
	MainLHCImage(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	virtual ~MainLHCImage();

	enum BeamTypeDrawMethod
	{
		None = 0,
		InnerOuterLines = 1,
		MiddleLine = 2
	};

	void reset(void);
	void renewImage(void);
	void setRingSelection(Sector *pFirstSector, Sector *pLastSector);
	void setLineSelection(QStringList &sectorList);
	void addLegendLabel(int x, int y, const char *text);

	// Access
	inline short getRingCenterX(void) { return ringCenterX; }
	inline short getRingCenterY(void) { return ringCenterY; }

	inline const QColor &getQrlColor(void) { return qrlColor; }
	inline void setQrlColor(QColor &color) { qrlColor = color; renewImage(); }
	inline const QColor &getCryoColor(void) { return cryoColor; }
	inline void setCryoColor(QColor &color) { cryoColor = color;  renewImage(); }
	inline const QColor &getRedBeamColor(void) { return redBeamColor; }
	inline void setRedBeamColor(QColor &color) { redBeamColor = color;  renewImage();}
	inline const QColor &getBlueBeamColor(void) { return blueBeamColor; }
	inline void setBlueBeamColor(QColor &color) { blueBeamColor = color;  renewImage(); }
	inline const QColor &getCrossBeamColor(void) { return crossBeamColor; }
	inline void setCrossBeamColor(QColor &color) { crossBeamColor = color; renewImage(); }
	inline const QColor &getSelectColor(void) { return selectColor; }
	inline void setSelectColor(QColor &color) { selectColor = color; renewImage(); }
	inline const QColor &getDefaultBeamColor(void) { return defaultBeamColor; }
	inline void setDefaultBeamColor(QColor &color) { defaultBeamColor = color; renewImage(); }
	inline float getLssScale(void) { return lssScale; }
	inline void setLssScale(float scale) { lssScale = scale < 1.0 ? 1.0 : scale; renewImage(); }
	inline int getQrlWidth(void) { return qrlWidth; }
	inline void setQrlWidth(int width) { qrlWidth = width < 1 ? 1 : (width > 99 ? 99 : width); renewImage(); }
	inline int getCryoWidth(void) { return cryoWidth; }
	inline void setCryoWidth(int width) { cryoWidth = width < 1 ? 1 : (width > 99 ? 99 : width); renewImage(); }
	inline int getConnLineWidth(void) { return connLineWidth; }
	inline void setConnLineWidth(int width) { connLineWidth = width < 1 ? 1 : width; renewImage(); }
	inline int getSelectLineWidth(void) { return selectLineWidth; }
	inline void setSelectLineWidth(int width) { selectLineWidth = width < 1 ? 1 : width; renewImage(); }
	inline BeamTypeDrawMethod getBeamTypeMethod(void) { return beamTypeMethod; }
	inline void setBeamTypeMethod(BeamTypeDrawMethod method) { beamTypeMethod = method; renewImage(); }
	inline int getBeamTypeWidth(void) { return beamTypeWidth; }
	inline void setBeamTypeWidth(int width) { beamTypeWidth = width < 0 ? 0 : width; renewImage(); }
	inline int getRedrawDelay(void) { return redrawDelay; }
	inline void setRedrawDelay(int delay) { redrawDelay = delay < 200 ? 200 : delay; }
	inline int getViewWidth(void) { return viewWidth; }
	inline int getViewHeight(void) { return viewHeight; }

signals:
	void mousePress(int button, int x, int y, const char *sectorName);

protected:
	// Color used to draw QRL vacuum
	QColor	qrlColor;

	// Color used to draw CRYO vacuum
	QColor	cryoColor;

	// Color used to draw RED beam vacuum
	QColor	redBeamColor;

	// Color used to draw BLUE beam vacuum
	QColor	blueBeamColor;

	// Color used to draw CROSS beam vacuum
	QColor	crossBeamColor;

	// Color used to highlight selected part
	QColor	selectColor;

	// Default foreground color
	QColor	foreColor;

	// Default color for beam vacuum
	QColor	defaultBeamColor;

	// Coefficient for LSS scale change
	float		lssScale;

	// Relative width of QRL vacuum line [%]
	int			qrlWidth;

	// Relative width of CRYO vacuum line [%]
	int			cryoWidth;

	// Width of line for drawing connected lines [Pixels]
	int			connLineWidth;

	// Width of line highlighutng selected part
	int			selectLineWidth;

	// Method for drawing vacuum beam type line (enum):
	BeamTypeDrawMethod	beamTypeMethod;

	// Width of line for vacuum beam type line, 0 = no line
	int			beamTypeWidth;

	// Delay between receiving of 'state changed' signal from equipment
	// and redrawing the image. Delay is introduced in order not to redraw
	// image too often, but instead redraw it after reasonable delay
	// during which more signals could come
	int			redrawDelay;

	// Time when image was drawn
	QTime		drawTime;

	// Timer providing redraw after redrawDelay
	QTimer		*pRedrawTimer;

	// Flag indicating if image has been drawn
	bool		isDrawn;

	// List of legend labels
	QList<LegendLabel *>	labels;

	// All segments used to draw LHC ring
	QList<LhcSegment *>	segments;

	// Segment for selection marker
	LhcSegment				*selectSegment;

	// All segments used to highlight ARCs on LHC ring
	QList<LhcSegment *>	arcSegments;

	// Geometry of IPs
	QList<SineCosine *>	ipGeoms;

	// Radius for IP markers
	short				ipMarkerRad;

	// Minimum radius of all segments
	short				minSegmentRad;

	// Flag indicating if segments have been built using sectors,
	// if yes - segments will be never rebuilt
	bool				areSegmentsFromSectors;

	// Flag indicating if reset() method was called at least once - only
	// after that we can expect some reasonable data
	bool				resetWasCalled;

	// List of all connected beam lines
	QList<LhcConnectLine *>	connectedLines;

	// Width of drawing area
	short				viewWidth;

	// Height of drawing area
	short				viewHeight;

	// X-coordinate of ring center
	short				ringCenterX;

	// Y-coordinate fo ring center
	short				ringCenterY;

	// List of sector selections on injection/dump lines
	QList<Sector *>	selSectors;

	// Label displaying name of sector under mouse pointer
	ExtendedLabel	*pLabel;

	// Timer to hide label
	QTimer			*pTimer;

	// Instance of class providing connections to equipment
//	MainLHCImageEqpConnect	connection;


	virtual void resizeEvent(QResizeEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseDoubleClickEvent(QMouseEvent *event);

	void makeImage(void);

	short draw(QPainter &painter);
	bool calculateGeometry(void);
	bool setNewGeometry(QPainter &painter);
	void calculateOtherLineGeometry(LhcSegment *pSegment);
	void searchForNeighborEqp(void);
	void buildIpAngles(void);
	bool buildSegments(void);
	void buildSectorBeamSegments(void);
	void buildEmptyBeamSegments(void);
	void buildSegmentPoints(void);

	void calculateOctantAngles(short ipIdx );
	LhcSegment *addSegment(int vacType, float start, float end, Sector *pSector);
	LhcSegment *addArcSegment(float start, float end, Sector *pSector);

	void drawNotReadyText(QPainter &painter);
	void drawLegendLabels(QPainter &painter);
	void drawIpMarkers(QPainter &painter);
	void drawArcMarkers(QPainter &painter);
	void drawLegend(QPainter &painter);
	void drawSelection(QPainter &painter);
	void drawSelPart(QPainter &painter, SectorDrawPart *pPart);
	void drawEqp(QPainter &painter, LhcConnectLine *pConnLine, Eqp *pEqp,
		bool &isFirst, int &prevX, int &prevY);
	void drawConnectedLinesTypes(QPainter &painter);
	void calculateConnLineGeometry(void);
	void drawConnLineType(LhcConnectLine *pLine, QPainter &painter);
	void drawConnectedLines(QPainter &painter);
	void drawConnectLine(QPainter &painter, LhcConnectLine *pConnLine, int drawOrder);

	const char *sectorAtLocation(int x, int y);
	const char *closestConnectLine(int x, int y);
	
	void dump(FILE *pFile);

private slots:
	void timerDone(void);
	void redrawTimerDone(void);
	void eqpStateChanged(void);
};

#endif	// MAINLHCIMAGE_H
