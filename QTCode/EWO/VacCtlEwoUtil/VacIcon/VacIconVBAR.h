#ifndef	VACICONVBAR_H
#define	VACICONVBAR_H

//	Icon for vacuum barrier (window)

#include "VacIcon.h"

class Eqp;

class VACCTLEWOUTIL_EXPORT VacIconVBAR : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVBAR(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVBAR();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask, bool forBeamVac);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	Eqp *pEqp;

	virtual inline bool isVirtualDevice(void) const { return true; }

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACICONVBAR_H
