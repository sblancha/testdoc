# Project file for qmake utility to build DateEditEwo EWO
#
#	15.01.2009	L.Kopylov
#		Initial version
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = DateEditEwo

HEADERS = DateEditWidget.h \
	DateEditEwo.h

SOURCES = DateEditWidget.cpp \
	DateEditEwo.cpp

