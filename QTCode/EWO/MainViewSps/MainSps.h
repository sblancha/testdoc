#ifndef MAINSPS_H
#define MAINSPS_H

// Main SPS view widget: image + list of sectors + buttons

#include "MainSpsImage.h"

#include "VacMainView.h"

#include <QComboBox>
#include <QTableWidget>
#include <QPushButton>
#include <QGroupBox>
#include <QEvent>


class MainSps : public VacMainView
{
	Q_OBJECT
		
	////////////////////////////////////////////////////////////////////////////
	///////////////////// Properties exposed to PVSS panel /////////////////////
	////////////////////////////////////////////////////////////////////////////
	//--------------------- Properties of VacMainView ---------------------------
	Q_PROPERTY(QColor synPipeColor READ getSynPipeColor WRITE setSynPipeColor);
	Q_PROPERTY(QColor synAltPipeColor READ getSynAltPipeColor WRITE setSynAltPipeColor);
	Q_PROPERTY(QColor synPipePartColor READ getSynPipePartColor WRITE setSynPipePartColor);
	Q_PROPERTY(QColor synPassiveLineColor READ getSynPassiveLineColor WRITE setSynPassiveLineColor);
	Q_PROPERTY(QColor synPassiveFillColor READ getSynPassiveFillColor WRITE setSynPassiveFillColor);
	Q_PROPERTY(QColor synSectorColor READ getSynSectorColor WRITE setSynSectorColor);
	Q_PROPERTY(int synPipeWidth READ getSynPipeWidth WRITE setSynPipeWidth);
	Q_PROPERTY(QColor graphAxisColor READ getGraphAxisColor WRITE setGraphAxisColor);
	Q_PROPERTY(int profileBarWidth READ getProfileBarWidth WRITE setProfileBarWidth);
	Q_PROPERTY(QColor profileAltSectorColor READ getProfileAltSectorColor WRITE setProfileAltSectorColor);
	Q_PROPERTY(QColor profileAltBarColor READ getProfileAltBarColor WRITE setProfileAltBarColor);
	Q_PROPERTY(int profileRedrawDelay READ getProfileRedrawDelay WRITE setProfileRedrawDelay);


	//--------------------- Properties specific to this class -------------------
	Q_PROPERTY(QColor mainDefaultColor READ getDefaultColor WRITE setDefaultColor);
	Q_PROPERTY(QColor mainSelectColor READ getSelectColor WRITE setSelectColor);
	Q_PROPERTY(int mainLineWidth READ getLineWidth WRITE setLineWidth);
	Q_PROPERTY(int mainSelectLineWidth READ getSelectLineWidth WRITE setSelectLineWidth);
	Q_PROPERTY(int mainRedrawDelay READ getRedrawDelay WRITE setRedrawDelay);
	Q_PROPERTY(int mainViewWidth READ getViewWidth WRITE setViewWidth);
	Q_PROPERTY(int mainViewHeight READ getViewHeight WRITE setViewHeight);
	Q_PROPERTY(bool readOnly READ isReadOnly WRITE setReadOnly);

public: // Accessors for properties above - specific properties
	inline const QColor &getDefaultColor(void) const { return pImage->getDefaultColor(); }
	inline void setDefaultColor(QColor &color) { pImage->setDefaultColor(color); }
	inline const QColor &getSelectColor(void) const { return pImage->getSelectColor(); }
	inline void setSelectColor(QColor &color) { pImage->setSelectColor(color); }
	inline int getLineWidth(void) const { return pImage->getLineWidth(); }
	inline void setLineWidth(int width) { pImage->setLineWidth(width); }
	inline int getSelectLineWidth(void) const { return pImage->getSelectLineWidth(); }
	inline void setSelectLineWidth(int width) { pImage->setSelectLineWidth(width); }
	inline int getRedrawDelay(void) const { return pImage->getRedrawDelay(); }
	inline void setRedrawDelay(int width) { pImage->setRedrawDelay(width); }
	inline int getViewWidth(void) const { return pImage->getViewWidth(); }
	inline void setViewWidth(int /* width */) {}	// Dummy method
	inline int getViewHeight(void) const { return pImage->getViewHeight(); }
	inline void setViewHeight(int /* width */) {}	// Dummy method
	inline bool isReadOnly(void) const { return readOnly; }
	void setReadOnly(bool flag);

	// Accessors for VacMainView properties - unfortunately, inherited methods
	// are not recognized by macro
	inline const QColor &getSynPipeColor(void) const { return VacMainView::getSynPipeColor(); }
	inline void setSynPipeColor(QColor &color) { VacMainView::setSynPipeColor(color); }
	inline const QColor &getSynAltPipeColor(void) const { return VacMainView::getSynAltPipeColor(); }
	inline void setSynAltPipeColor(QColor &color) { VacMainView::setSynAltPipeColor(color); }
	inline const QColor &getSynPipePartColor(void) const { return VacMainView::getSynPipePartColor(); }
	inline void setSynPipePartColor(QColor &color) { VacMainView::setSynPipePartColor(color); }
	inline const QColor &getSynPassiveLineColor(void) const { return VacMainView::getSynPassiveLineColor(); }
	inline void setSynPassiveLineColor(QColor &color) { VacMainView::setSynPassiveLineColor(color); }
	inline const QColor &getSynPassiveFillColor(void) const { return VacMainView::getSynPassiveFillColor(); }
	inline void setSynPassiveFillColor(QColor &color) { VacMainView::setSynPassiveFillColor(color); }
	inline const QColor &getSynSectorColor(void) const { return VacMainView::getSynSectorColor(); }
	inline void setSynSectorColor(QColor &color) { VacMainView::setSynSectorColor(color); }
	inline int getSynPipeWidth(void) const { return VacMainView::getSynPipeWidth(); }
	inline void setSynPipeWidth(int width) { VacMainView::setSynPipeWidth(width); }
	inline const QColor &getGraphAxisColor(void) const { return VacMainView::getGraphAxisColor(); }
	inline void setGraphAxisColor(QColor &color) { VacMainView::setGraphAxisColor(color); }
	inline int getProfileBarWidth(void) const { return VacMainView::getProfileBarWidth(); }
	inline void setProfileBarWidth(int width) { VacMainView::setProfileBarWidth(width); }
	inline const QColor &getProfileAltSectorColor(void) const { return VacMainView::getProfileAltSectorColor(); }
	inline void setProfileAltSectorColor(QColor &color) { VacMainView::setProfileAltSectorColor(color); }
	inline const QColor &getProfileAltBarColor(void) const { return VacMainView::getProfileAltBarColor(); }
	inline void setProfileAltBarColor(QColor &color) { VacMainView::setProfileAltBarColor(color); }
	inline int getProfileRedrawDelay(void) const { return VacMainView::getProfileRedrawDelay(); }
	inline void setProfileRedrawDelay(int width) { VacMainView::setProfileRedrawDelay(width); }


public:
	MainSps(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~MainSps();

	void addLegendLabel(int x, int y, const char *text)
	{
		pImage->addLegendLabel(x, y, text);
	}
	void addRotatedLegendLabel(int x, int y, int angle, const char *text)
	{
		pImage->addRotatedLegendLabel(x, y, angle, text);
	}
	int dataReady(void);
	inline bool isSelected(void) { return pTable->rowCount() > 0; }
	bool isContSelection(void);
	QStringList getSelectedSectors(void);

	// Implement abstract methods of VacMainView
	virtual void dialogRequest(int type, unsigned vacTypes, const QStringList &sectorList, DataEnum::DataMode mode);

	// Access
	//virtual void setFont(const QFont &font);

signals:
	void dialogRequested(int type, unsigned vacType, const QStringList &sectorList, DataEnum::DataMode mode);

protected:
	///////////////////////////////// Widgets /////////////////////////////////
	
	// Widget with image of LHC
	MainSpsImage	*pImage;

	// Border for main part combo box
	QGroupBox		*pMpBorder;

	// Combo box for main part selection
	QComboBox		*pCombo;

	// Border for sector list
	QGroupBox		*pSectorBorder;

	// List of selected sectors
	QTableWidget		*pTable;

	// Button to add sector at the beginning of sector list
	QPushButton		*pAddStartPb;

	// Button to remove sector at the beginning of sector list
	QPushButton		*pDelStartPb;

	// Button to add sector at the end of sector list
	QPushButton		*pAddEndPb;

	// Button to remove sector at the end of sector list
	QPushButton		*pDelEndPb;

	// Buttons for test
	QPushButton		*pTestPb;
	QPushButton		*pTestPb2;

	// Read-only flag (for overview panels)
	bool			readOnly;

	///////////////////////////////// Current selection /////////////////////////
	

	virtual void resizeEvent(QResizeEvent *event);

	void buildLayout(void);
	void buildMpCombo(void);

	void selectFromSector(Sector *pSector);
	void setSelection(QStringList &sectorList);
	void setSelectionFromTable(void);
	void setMainPartName(void);
	void setSectorButtonsSens(void);
	bool tableContains(const char *name);
	void setTableCellText(int row, int col, const QString &text);

	virtual void changeEvent(QEvent *pEvent );

private slots:
	void imageMousePress(int button, int x, int y, const char *sectorName);
	void comboActivated(const QString &string);
	void addAtStart(void);
	void delAtStart(void);
	void delAtEnd(void);
	void addAtEnd(void);

	void testAction();
	void testAction2();
};

#endif	// MAINSPS_H
