#ifndef	PROFILELINESECTOR_H
#define	PROFILELINESECTOR_H

// Parameters of one vacuum sector area in profile graph

#include "Sector.h"

#include "VacType.h"
#include "Eqp.h"
#include "EqpMsgCriteria.h"

#include <QRect>
#include <QPainter>
#include <QFontMetrics>

#include <stdio.h>

class ProfileLineSector
{
public:
	ProfileLineSector();
	~ProfileLineSector();

	void dump(FILE *pFile, int idx);
	bool needLabel(void);
	void calculateLabelRect(QFontMetrics &fontMetrics, int areaBottom);
	void moveLabelRectUp(void);
	void drawLabel(QPainter &painter);

	// Access
	inline Sector *getSector1(void) const { return pSector1; }
	inline void setSector1(Sector *pSector) { pSector1 = pSector; }
	inline Sector *getSector2(void) const { return pSector2; }
	inline void setSector2(Sector *pSector) { pSector2 = pSector; }
	inline int getLeftX(void) const { return leftX; }
	inline void setLeftX(int coord) { leftX = coord; }
	inline int getRightX(void) const { return rightX; }
	inline void setRightX(int coord) { rightX = coord; }
	inline int getLeftEqpIdx(void) const { return leftEqpIdx; }
	inline void setLeftEqpIdx(int idx) { leftEqpIdx = idx; }
	inline int getRightEqpIdx(void) const { return rightEqpIdx; }
	inline void setRightEqpIdx(int idx) { rightEqpIdx = idx; }
	inline bool isTransition(void) const { return transition; }
	inline void setTransition(bool flag) { transition = flag; }
	inline int getVacType(void) const { return pSector1 ? pSector1->getVacType() : VacType::None; }
	inline const QRect &getLabelRect(void) const { return labelRect; }

protected:
	//	Pointer to 1st sector
	Sector	*pSector1;

	// Pointer to 2nd sector
	Sector	*pSector2;

	// Rectangle = container for sector label
	QRect	labelRect;

	// Index of left device
	int		leftEqpIdx;

	// Pointer to right device
	int		rightEqpIdx;

	// X-coordinate of left area side
	int		leftX;

	// X-coordinate of right area side
	int		rightX;

	// Flag indicating if this area is transition between two sectors
	bool	transition;
};

#endif	// PROFILELINESECTOR_H
