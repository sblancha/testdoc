#ifndef	HISTORYIMAGE_H
#define	HISTORYIMAGE_H

// Class providing history image drawing

// Scale step for cryo temperature axis
#define	CRYO_TEMP_SCALE_STEP	(5)

#include "HistoryEqpData.h"
#include "TimeAxisWidget.h"
#include "VerticalAxisWidget.h"

#include <QWidget>

class HistoryProcessor;

#include <QLabel>
#include <QTimer>
//class QSpinBox;
// class QToolButton;
#include <QHBoxLayout>
#include <QThread>

class PlotImage;

class HistoryImage : public QWidget
{
	Q_OBJECT

public:
	HistoryImage(QWidget *parent);
	~HistoryImage();

	int addEqp(Eqp *pEqp, int historyDepth, QColor &color, bool useInputColor = false);
	int addDpe(Eqp *pEqp, const char *dpeName, const char *yAxisName, int historyDepth, QColor &color, bool useInputColor = false);
	int addEqpBit(Eqp *pEqp, const char *dpe, int bitNbr, const char *bitName, int historyDepth, QColor &color, bool useInputColor = false);
	int getMaximumDepth(void);
	void setTimeLimits(const QDateTime &start, const QDateTime &end);
	bool addHistoryValue(int id, const char *dpe, bool bitState, int bitNbr, const QDateTime &time, const QVariant &value);
	void finishHistoryRange(int id, const char *dpe, bool bitState, int bitNbr);

	void rebuildImage(int source);

	void showNextRange(const QDateTime &start, const QDateTime &end);
	void hideNextRange(void);

	const char *getDpName(int index);
	const Eqp *removeItem(int index);
	void setItemColor(int index, const QColor &color);
	int getEqpIndex(const Eqp *pEqp);
	int getDpeIndex(const Eqp *pEqp, const char *dpeName);

	void addExportValues(HistoryProcessor &processor);

	void getVerticalScalesParams(int &nScales, int **ppTypes, bool **ppLogs, float **ppMins, float **ppMaxs);
	void setVerticalScaleParams(int type, bool log, float min, float max);

	void draw(QPainter &painter);

	// Access
	inline unsigned getLogAxisUnit(void) const { return pTimeAxis->getLogAxisUnit(); }
	void setLogAxisUnit(unsigned unit);
	inline const QDateTime &getTimeMin(void) const { return pTimeAxis->getMin(); }
	inline const QDateTime &getTimeMax(void) const { return pTimeAxis->getMax(); }
	inline bool isOnline(void) const { return online; }
	inline bool isTimeLogScale(void) const { return pTimeAxis->isLogScale(); }
	inline void setTimeLogScale(bool flag) { pTimeAxis->setLogScale(flag); }
	void setOnline(bool flag);
	inline int getDepth(void) const { return depth; }
	void setDepth(int value);
	inline QList<HistoryEqpData *> &getEqps(void) { return eqps; }
	inline HistoryDataRange::FilterType getFilterType(void) const { return filterType; }
	void setFilterType(HistoryDataRange::FilterType newType);

signals:
	void imageUpdated(void);
	void valueAtPointerChanged(const QString &timeStr, const QString &valueStr);
	void scalesChanged(void);

public slots:
	void doDrawWork(void);

protected:
	// THread performing drawing
	QThread				*pDrawThread;

	// Time axis
	TimeAxisWidget		*pTimeAxis;

	// List of all vertical axies
	QList<VerticalAxisWidget *>	axisList;

	// Widget with image
	PlotImage		*pImage;

	// Layout containing image widget and all vertical axies
	QHBoxLayout	*pLayout;

	// List of all devices shown in history
	QList<HistoryEqpData *>	eqps;

	// Pointer to last found device with given DPE name, used to keep
	// reference to found DPE during massive offline data arrival
	HistoryEqpData				*pCurrentEqp;

	// List of colors used for devices
	QList<QColor *>			colors;

	// Label for displaying next time range
	QLabel						*pLabel;

	// Type of axis edited by SpinBox
//	DataEnum::ValueType			spinAxisType;

	// Timestamp of last image drawing
	QTime						drawTime;

	// Timer to redraw image after online value arrival
	QTimer						*pTimer;

	// Timer to keep online graph alive if no new values arrive for long time
	QTimer						*pAliveTimer;

	// Flag indicating if spin box is used to edit axis maximum
//	bool						spinEditsMax;

	// Type of last device selected in history
//	DataEnum::ValueType			lastSelected;

	// Type of last selected device with axis on right side
//	DataEnum::ValueType			lastSelectedRight;

	// Depth of history [min]
	int					depth;

	// Stored minimum value of pressure axis in logarithmic mode
	int					storedPressureMin;

	// Stored minimum value of intensity axis in logarithmic mode
	int					storedIntensityMin;

	// Filter type for all data in this graph
	HistoryDataRange::FilterType	filterType;

	// Flag indicating if history is in online mode
	bool				online;

	// Flag indicating the redrawing process is in progress - to avoid recursive redraw operations
	bool				redrawing;

	// Flag indicating if image was drawn at least once
	bool				drawnOnce;

	const QColor findNewColor(void);

	VerticalAxisWidget *findAxisForValueType(DataEnum::ValueType valueType);

	void calculateVerticalLimits(void);
	void calculateVerticalLimitsForAxis(VerticalAxisWidget *pAxis);
	void calculateAxisActivity(void);

//	void setVericalAxisGridLimits(void);

	void drawVerticalAxies(QPainter &painter);
	void drawTimeAxis(QPainter &painter);

//	void changeCryoTemperatureLimits(int value);

	void showValueAtPointer(void);

	void setZoomingTime(const QRect &zoomRect);
	void setZoomingVertical(const QRect &zoomRect);

	void rebuildWithDelay(void);

	virtual void resizeEvent(QResizeEvent *event);

	void dump(const char *fileName);

private slots:
	void onlineValueArrived(void);
	void redrawTimerDone(void);
	void aliveTimerDone(void);

	void imageResized(void);
	void imagePointerMoved(int location);
	void imageZoomFinished(const QRect &zoomRect);

	void scaleChanged(bool dragging);
};

#endif	// HISTORYIMAGE_H
