//	Implementation of TimeAxisWidget class
/////////////////////////////////////////////////////////////////////////////////

#include "TimeAxisWidget.h"

#include "VacMainView.h"
#include "DebugCtl.h"

#include <QPainter>
#include <QLabel>
#include <QMenu>
#include <QCursor>
#include <QMouseEvent>

#include <stdlib.h>
#include <math.h>

#include "PlatformDef.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

TimeAxisWidget::TimeAxisWidget(QWidget *parent, Qt::WindowFlags f)
	: QWidget(parent, f)
{
	if(VacMainView::getInstance())
	{
		setFont(VacMainView::getInstance()->font());
	}
	logScale = false;
	needPrepare = true;
	autoLogAxisUnit = true;
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("TimeAxisWidget::TimeAxisWidget(): font %s\n", font().toString().toLatin1().constData());
#else
		printf("TimeAxisWidget::TimeAxisWidget(): font %s\n", font().toString().toLatin1().constData());
		fflush(stdout);
#endif
	}
	int height = findLabelHeight(fontMetrics());
	setMinimumHeight(height + 2);
}

TimeAxisWidget::~TimeAxisWidget()
{
}

/*
**	FUNCTION
**		Set geometry of graph drawing area.
**
**	PARAMETERS
**		rect	- Graph area rectangle
**
** 	RETURNS
**		None
**
** 	CAUTIONS
**		None
*/
void TimeAxisWidget::setGraphArea(const QRect &rect)
{
	if(graphArea.width() != rect.width())
	{
		needPrepare = true;
	}
	else if(graphArea.left() != rect.left())
	{
		needPrepare = true;
	}
	graphArea = rect;
	update();
}

/*
**	FUNCTION
**		Set time limits for axis
**
**	PARAMETERS
**		newMin	- Start of time range
**		newMax	- End of time range
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::setLimits(const QDateTime &newMin, const QDateTime &newMax)
{
	QDateTime	useMin, useMax;

	if(newMin <= newMax)
	{
		useMin = newMin;
		useMax = newMax;
	}
	else
	{
		useMin = newMax;
		useMax = newMin;
	}
	if(useMin.secsTo(useMax) < 60)
	{
		useMin = useMax.addSecs(-60);
	}

	// set msec to zero - L.Kopylov 23.08.2010
	QTime time(useMin.time());
	if(time.msec() > 0)
	{
		time.setHMS(time.hour(), time.minute(), time.second());
		useMin.setTime(time);
		useMin = useMin.addSecs(1);

		time = useMax.time();
		time.setHMS(time.hour(), time.minute(), time.second());
		useMax.setTime(time);
		useMax = useMax.addSecs(1);
	}

	if(min != useMin)
	{
		needPrepare = true;
		minAsMsec = useMin.toMSecsSinceEpoch();
	}
	min = useMin;
	if(max != useMax)
	{
		needPrepare = true;
		maxAsMsec = useMax.toMSecsSinceEpoch();
	}
	max = useMax;
}

/*
**	FUNCTION
**		Set flag indicating if axis shall be linear or logarithmic
**
**	PARAMETERS
**		flag	- Flag indicating if logarithmic axis shall be used
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::setLogScale(bool flag)
{
	if(logScale == flag)
	{
		return;
	}
	needPrepare = true;
	logScale = flag;
	emit limitsChanged(false);
}

/*
**	FUNCTION
**		Set new unit for logarithmic axis
**
**	PARAMETERS
**		unit	- New value to set. Setting value < 1 switches to
**					automatic calculation mode.
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::setLogAxisUnit(unsigned unit)
{
	if(unit < 1)
	{
		autoLogAxisUnit = true;
		return;
	}
	logAxisUnit = unit;
	autoLogAxisUnit = false;
}

/*
**	FUNCTION
**		Convert time value to coordinate in window
**
**	PARAMETERS
**		value		- Time value to be converted
**		screenX		- Variable where resulting screen coordinate will be written
**
** RETURNS
**		Enumerated conversion success flag
**
** CAUTIONS
**		None
*/
TimeAxisWidget::TimeRange TimeAxisWidget::timeToScreen(const QDateTime &value, int &screenX) const
{
	TimeRange	result = WithinRange;

	if(value <= min)
	{
		screenX = 0;
		result = value < min ? BeforeMin : WithinRange;
	}
	else if(value >= max)
	{
		screenX = scaleRight - scaleLeft;
		result = value > max ? AfterMax : WithinRange;
	}
	else
	{
		if(logScale )
		{
			double val = log10((double)min.secsTo(value) / (double)logAxisUnit);
			if(val > 0)
			{
				screenX = (int)rint(val / logMax * (double)(scaleRight - scaleLeft));
			}
			else
			{
				screenX = 0;
			}
		}
		else
		{
			screenX =
				(int)rint((double)min.secsTo(value) / (double)min.secsTo(max) *
				(double)(scaleRight - scaleLeft));
		}
		result = WithinRange;
	}
	return result;
}

TimeAxisWidget::TimeRange TimeAxisWidget::timeAsMsecToScreen(qint64 value, int &screenX) const {
	TimeRange	result = WithinRange;

	if (value <= minAsMsec) {
		screenX = 0;
		result = value < minAsMsec ? BeforeMin : WithinRange;
	}
	else if (value >= maxAsMsec) {
		screenX = scaleRight - scaleLeft;
		result = value > maxAsMsec ? AfterMax : WithinRange;
	}
	else {
		if (logScale) {
			double val = log10((double)(value - minAsMsec) / (double)logAxisUnit / 1000.);	// 1000 - because work with msec here
			if (val > 0) {
				screenX = (int)rint(val / logMax * (double)(scaleRight - scaleLeft));
			}
			else {
				screenX = 0;
			}
		}
		else {
			screenX =
				(int)rint((double)(value - minAsMsec) / (double)(maxAsMsec - minAsMsec) *
					(double)(scaleRight - scaleLeft));
		}
		result = WithinRange;
	}
	return result;

}

/*
**	FUNCTION
**		Convert coordinate in window to time value
**
**	PARAMETERS
**		screenX	- X-coordinate in graph area
**		label	- Variable where string representation of resulting
**					time will be written.
**
**	RETURNS
**		Time corresponding to screen coordinate
**
**	CAUTIONS
**		None
*/
QDateTime TimeAxisWidget::screenToTime(int screenX, QString &label)
{
	QDateTime	result;

	screenX += scaleLeft;	// Location comes from PlotImage widget

	if(screenX <= scaleLeft)
	{
		result = min;
	}
	else if(screenX >= scaleRight)
	{
		result = max;
	}
	else
	{
		double scaleFactor, deltaTime;
		if(logScale)
		{
			scaleFactor = logMax / (double)(scaleRight - scaleLeft);
			deltaTime = logAxisUnit * pow(10., (double)(screenX - scaleLeft) * scaleFactor);
		}
		else
		{
			scaleFactor = (double)min.secsTo(max) / (double)(scaleRight - scaleLeft);
			deltaTime = (double)(screenX - scaleLeft) * scaleFactor;
		}
		int deltaSec = (int)deltaTime;
		result = min.addSecs(deltaSec);
		int msec = (int)(1000.0 * (deltaTime - deltaSec));
		if(msec > 0)
		{
			QTime time(result.time());
			time.setHMS(time.hour(), time.minute(), time.second(), msec);
			result.setTime(time);
		}
	}
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("TimeAxis::screenToTime(): result = %s\n", result.toString("yyyy-MM-dd hh:mm:ss.zzz").toLatin1().constData());
#else
		printf("TimeAxis::screenToTime(): result = %s\n", result.toString("yyyy-MM-dd hh:mm:ss.zzz").toLatin1().constData());
		fflush(stdout);
#endif
	}
	label = result.toString("dd-MM-yy hh:mm:ss.zzz");
	return result;
}

/*
**	FUNCTION
**		Calculate height which will be consumed by scale labels
**
**	PARAMETERS
**		fontMetrics	- Font metrics that will be used for drawing
**
** 	RETURNS
**		height for scale labels
**
**	CAUTIONS
**		Calculates height of fixed label string assuming real
**		labels will not be very different.
*/
int TimeAxisWidget::findLabelHeight(const QFontMetrics &fontMetrics)
{
	int	result = 0;

	if(!logScale)
	{
		QString label;
		makeLabel(label, QDateTime::currentDateTime());
		QSize size;
		labelExtents(fontMetrics, label, size);
		result = size.height() + 2;
	}
	else
	{
		QString label = "100";
		QSize size;
		labelExtents(fontMetrics, label, size);
		result = size.height() + 2;
	}
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("TimeAxisWidget::findLabelHeight(): result = %d\n", result);
#else
		printf("TimeAxisWidget::findLabelHeight(): result = %d\n", result);
		fflush(stdout);
#endif
	}
	return result;
}

/*
**	FUNCTION
**		Draw time axis
**
**	PARAMETERS
**		panter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::draw(QPainter &painter)
{
	QFontMetrics fm = painter.fontMetrics();
	if(needPrepare)
	{
		prepare(fm);
	}

	painter.setPen(VacMainView::getGraphAxisColor());
	if(logScale)
	{
		drawLog(painter, fm);
	}
	else
	{
		drawLin(painter, fm);
	}
}

/*
**	FUNCTION
**		Draw time axis lines on plot area
**
**	PARAMETERS
**		panter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::drawOnPlotArea(QPainter &painter)
{
	if(needPrepare)
	{
		prepare(painter.fontMetrics());
	}

	painter.setPen(VacMainView::getGraphAxisColor());
	if(logScale)
	{
		drawLogOnPlotArea(painter);
	}
	else
	{
		drawLinOnPlotArea(painter);
	}
}

/*
**	FUNCTION
**		Prepare all axis drawing parameters
**
**	PARAMETERS
**		fontMetrics	- Metrics of font that will be used for drawing
**
**	RETURNS
**		true if axis parameters have been changed,
**		false otherwise
**
**	CAUTIONS
**		None
*/
bool TimeAxisWidget::prepare(const QFontMetrics &fontMetrics)
{
	needPrepare = false;
	scaleLeft = graphArea.left();
	scaleRight = graphArea.right();

	if(logScale )
	{
		calculateLogAxis(fontMetrics);
	}
	else
	{
		calculateLinAxis(fontMetrics);
	}
	return true;
}

/*
**	FUNCTION
**		Calculate parameters of linear axis
**
**	PARAMETERS
**		fontMetrics	- Metrics of font that will be used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::calculateLinAxis(const QFontMetrics &fontMetrics)
{
	int labelHeight = findLabelHeight(fontMetrics);

	// Calculate label drawing parameters
	// Algorithm uses the following assumptions:
	// 1) Minumum displayed time range is 1 minute
	// 2) Reasonable steps between labels are:
	//		10, 20 sec,
	//		1,2,5,10 min,
	//		1,2,5,10 hours,
	//		1,2,5, days,
	//		10,20,50 days etc.
	// 3) First label shall be drawn at time which is multiple of step between labesl
	// 4) Labels shall not overlap
	// 5) The format used to represent time is fixed
	bool labelsOverlap = false;
	labelStep = 0;		// Let's start with minimum possible step
	do
	{
		labelStep = nextLabelStep(labelStep);
		// Find first label
		int mod = min.toTime_t() % labelStep;
		if(mod)
		{
			startLabelTime = min.addSecs(labelStep - mod);
		}
		else
		{
			startLabelTime = min;
		}
		if(startLabelTime > max) 	// No labels
		{
			labelsOverlap = true;
			break;
		}
		int prevLabelRight = 0;
		labelsOverlap = false;
		for(QDateTime value = startLabelTime ; value <= max ; value = value.addSecs(labelStep))
		{
			QString label;
			makeLabel(label, value);
			int labelX;
			timeToScreen(value, labelX);
			QSize size;
			labelExtents(fontMetrics, label, size);
			if(prevLabelRight && ((labelX - size.width() / 2 - size.width() / 8 ) < prevLabelRight))
			{
				labelsOverlap = true;
				break;
			}
			prevLabelRight = labelX + size.width() / 2 + size.width() / 8;
		}
	} while(labelsOverlap);
	/*
	if(labelsOverlap)		// No space for labels
	{
		axisLocation = graphArea.bottom() - 1;
		showLabels = false;
	}
	*/
	if(labelHeight > height())	// L.Kopylov 15.03.2012 - before comparison was !=
	{
		if(DebugCtl::isAxis())
		{
#ifdef Q_OS_WIN
			qDebug("TimeAxisWidget::calculateLinAxis(): set height %d\n", labelHeight);
#else
			printf("TimeAxisWidget::calculateLinAxis(): set height %d\n", labelHeight);
			fflush(stdout);
#endif
		}
		setFixedHeight(labelHeight);
		updateGeometry();
	}
}

/*
**	FUNCTION
**		Calculate parameters of logarithmic axis
**
**	PARAMETERS
**		fontMetrics	- Metrics of font that will be used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::calculateLogAxis(const QFontMetrics & /* fontMetrics */)
{
	double	scaleWidth = log10((double)min.secsTo(max));

	if(autoLogAxisUnit || (logAxisUnit < 1))
	{
		if(scaleWidth < 3)
		{
			logAxisUnit = 1;					// 1 second
		}
		else if(scaleWidth < 4)
		{
			logAxisUnit = 10;					// 10 seconds
		}
		else if(scaleWidth < 5)
		{
			logAxisUnit = 60;					// 1 minute
		}
		else if(scaleWidth < 6)
		{
			logAxisUnit = 600;					// 10 minutes
		}
		else if(scaleWidth < 7)
		{
			logAxisUnit = 60 * 60;				// 1 hour
		}
		else if(scaleWidth < 8)
		{
			logAxisUnit = 10 * 60 * 60;			// 10 hours
		}
		else if(scaleWidth < 9)
		{
			logAxisUnit = 24 * 60 * 60;			// 1 day
		}
		else
		{
			logAxisUnit = 10 * 24 * 60 * 60;	// 10 days
		}
		// !!!!!!!!!!!!!!!!!!!!!! Override the algorithm above
		// logAxisUnit = 1;
	}
	logMax = log10((double)min.secsTo(max) / (double)logAxisUnit);
//	axisLocation = graphArea.bottom() - findLabelHeight(fontMetrics);
}

/*
**	FUNCTION
**		Draw linear time axis
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Pen used for drawing shall be set in painter BEFORE calling this method
*/
void TimeAxisWidget::drawLin(QPainter &painter, const QFontMetrics &fm)
{
	for(QDateTime value = startLabelTime ; value <= max ; value = value.addSecs(labelStep))
	{
		int	x;
		timeToScreen(value, x);
		x += scaleLeft;
		painter.drawLine(x, 0, x, 2);
		QString label;
		makeLabel(label, value);
		drawLabel(painter, fm, label, x);
	}
}
void TimeAxisWidget::drawLinOnPlotArea(QPainter &painter)
{
	for(QDateTime value = startLabelTime ; value <= max ; value = value.addSecs(labelStep))
	{
		int	x;
		timeToScreen(value, x);
		painter.drawLine(x, 0, x, graphArea.height());
	}
}

/*
**	FUNCTION
**		Draw logarithmic time axis
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Pen used for drawing shall be set in painter BEFORE calling this method
*/
void TimeAxisWidget::drawLog(QPainter &painter, const QFontMetrics &fm)
{
	// Decide what will be the first label to draw
	int count = 1;
	switch(logAxisUnit)
	{
	case 1:			// Unit is 1 sec, 1st label at 10 sec
	case 60:		// Unit is 1 min, 1st label at 10 min
	case 3600:		// Unit is 1 hour, 1st label at 10 hours
	case 86400:		// Unit is 1 day, 1st label at 10 days
		count = 10;
		break;
	case 10:		// Unit is 10 sec, 1st label at 100 sec
	case 600:		// Unit is 10 min, 1st label at 100 min
	case 36000:		// Unit is 10 hours, 1st label at 100 hours
	case 864000:	// Unit is 10 days, 1st label at 100 days
		count = 100;
		break;
	}

	// Draw axis grid and labels if necessary
	bool	isFirstLabel = true, isFinished = false;
	for(unsigned step = logAxisUnit ; ! isFinished ; step *= 10 )
	{
		int x = 0;
		QString label;
		for(unsigned n = 2 ; n <= 10 ; n++)
		{
			QDateTime value = min.addSecs(n * step);
			if(timeToScreen(value, x) == AfterMax)
			{
				isFinished = true;
				break;
			}
			x += scaleLeft;
			painter.drawLine(x, 0, x, 2);
			if(n < 10)
			{
				continue;
			}
			if(isFirstLabel)
			{
				label.setNum(count);
				switch(logAxisUnit)
				{
				case 1:
				case 10:
					label += "s";
					break;
				case 60:
				case 600:
					label += "m";
					break;
				case 3600:
				case 36000:
					label += "h";
					break;
				case 86400:
				case 864000:
					label += "d" ;
					break;
				}
				isFirstLabel = false;
			}
			else
			{
				label.setNum(count);
			}
			count *= 10;
			drawLabel(painter, fm, label, x);
		}
	}
}

void TimeAxisWidget::drawLogOnPlotArea(QPainter &painter)
{
	// Decide what will be the first label to draw
	int count = 1;
	switch(logAxisUnit)
	{
	case 1:			// Unit is 1 sec, 1st label at 10 sec
	case 60:		// Unit is 1 min, 1st label at 10 min
	case 3600:		// Unit is 1 hour, 1st label at 10 hours
	case 86400:		// Unit is 1 day, 1st label at 10 days
		count = 10;
		break;
	case 10:		// Unit is 10 sec, 1st label at 100 sec
	case 600:		// Unit is 10 min, 1st label at 100 min
	case 36000:		// Unit is 10 hours, 1st label at 100 hours
	case 864000:	// Unit is 10 days, 1st label at 100 days
		count = 100;
		break;
	}

	// Draw axis grid and labels if necessary
	bool	isFinished = false;
	for(unsigned step = logAxisUnit ; ! isFinished ; step *= 10 )
	{
		int x = 0;
		for(unsigned n = 2 ; n <= 10 ; n++)
		{
			QDateTime value = min.addSecs(n * step);
			if(timeToScreen(value, x) == AfterMax)
			{
				isFinished = true;
				break;
			}
			painter.drawLine(x, 0, x, 2);
			painter.drawLine(x, graphArea.bottom() - 2, x, graphArea.bottom());
			if(n < 10)
			{
				continue;
			}
			count *= 10;
			painter.drawLine(x, 0, x, graphArea.height());
		}
	}
}

/*
**	FUNCTION
**		Draw label consistinig of more than one line of text
**
**	PARAMETERS
**		painter	- Painter used for drawing
**		label	- Label to draw
**		x		- X-coordinate of label center
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::drawLabel(QPainter &painter, const QFontMetrics &fm, QString &label, int x)
{
	char	*asciiLabel = (char *)malloc(label.length() + 1);
#ifdef Q_OS_WIN
	strcpy_s(asciiLabel, label.length() + 1, label.toLatin1().constData());
#else
	strcpy(asciiLabel, label.toLatin1().constData());
#endif
	int	y = 2;

#ifdef Q_OS_WIN
	char *nextToken = NULL;
	char *token = strtok_s(asciiLabel, "\n", &nextToken);
#else
	char *token = strtok(asciiLabel, "\n");
#endif
//	QFontMetrics fm = painter.fontMetrics();
	while(token)
	{
		QRect size = fm.boundingRect(token);
		y += size.height();
		if(DebugCtl::isAxis())
		{
#ifdef Q_OS_WIN
			qDebug("TimeAxisWidget::drawLabel(): token <%s> %d x %d, draw at %d %d (descent %d)\n", token, size.width(), size.height(), x - size.width() / 2, y - fm.descent(), fm.descent());
#else
			printf("TimeAxisWidget::drawLabel(): token <%s> %d x %d, draw at %d %d (descent %d)\n", token, size.width(), size.height(), x - size.width() / 2, y - fm.descent(), fm.descent());
			fflush(stdout);
#endif
		}
		painter.drawText(x - size.width() / 2, y - fm.descent(), token);
		y += 2;
#ifdef Q_OS_WIN
		token = strtok_s(NULL, "\n", &nextToken);
#else
		token = strtok(NULL, "\n");
#endif
	}
	free((void *)asciiLabel);
}

/*
**	FUNCTION
**		Calculate overall size of label consisting of more then one line of text
**
**	PARAMETERS
**		fontMetrics	- Metrics of font used for drawing
**		label		- Label whose size shall be calculated
**		size		- Variable where result will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::labelExtents(const QFontMetrics &fontMetrics, QString &label, QSize &size)
{
	char	*asciiLabel = (char *)malloc(label.length() + 1);
#ifdef Q_OS_WIN
	strcpy_s(asciiLabel, label.length() + 1, label.toLatin1().constData());
#else
	strcpy(asciiLabel, label.toLatin1().constData());
#endif

	size.setWidth(0);
	size.setHeight(0);

#ifdef Q_OS_WIN
	char *nextToken = NULL;
	char *token = strtok_s(asciiLabel, "\n", &nextToken);
#else
	char *token = strtok(asciiLabel, "\n");
#endif
	while(token)
	{
		QRect tokenSize = fontMetrics.boundingRect(token);
		if(size.height())
		{
			size.setHeight(size.height() + 2);
		}
		size.setHeight(size.height() + tokenSize.height());
		if(tokenSize.width() > size.width())
		{
			size.setWidth(tokenSize.width());
		}
		if(DebugCtl::isAxis())
		{
#ifdef Q_OS_WIN
			qDebug("TimeAxisWidget::labelExtents(): token <%s> %d x %d, result %d\n", token, tokenSize.width(), tokenSize.height(), size.height());
#else
			printf("TimeAxisWidget::labelExtents(): token <%s> %d x %d, result %d\n", token, tokenSize.width(), tokenSize.height(), size.height());
#endif
		}
#ifdef Q_OS_WIN
		token = strtok_s(NULL, "\n", &nextToken);
#else
		token = strtok(NULL, "\n");
#endif
	}
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("TimeAxisWidget::labelExtents(): final result %d\n", size.height());
#else
		printf("TimeAxisWidget::labelExtents(): final result %d\n", size.height());
		fflush(stdout);
#endif
	}
	free((void *)asciiLabel);
}

/*
**	FUNCTION
**		Prepare string representation of given time value.
**
**	PARAMETERS
**		label	- Variable where this method will put result
**		value	- Value to be shown on axis
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::makeLabel(QString &label, const QDateTime &value)
{
	label = value.toString("dd-MM-yyyy\nhh:mm:ss");
}

/*
**	FUNCTION
**		Calculate next step between time labels to use. The following
**		steps are used:
**			10, 20 sec,
**			1,2,5,10 min,
**			1,2,5,10 hours,
**			1,2,5, days,
**			10,20,50 days etc.
**
**	PARAMETERS
**		step	- previously used step between labels
**
**	RETURNS
**		New step between labels - always greater than previous one
**
**	CAUTIONS
**		None
*/
int TimeAxisWidget::nextLabelStep(int step)
{
	// If previous step is 0 - start with 10 seconds step
	if(!step)
	{
		return 10;
	}

	// Do we already work with days?
	int nUnits = step / (60 * 60 * 24);
	if(nUnits)
	{
		nUnits = nextScaleStep(nUnits, false);
		return nUnits * 60 * 60 * 24;
	}

	// Not yet - do we work with hours?
	nUnits = step / (60 * 60);
	if(nUnits)
	{
		nUnits = nextScaleStep(nUnits, false);
		// Maximum hours is 10, then switch to days
		if(nUnits > 10)
		{
			return 1 * 24 * 60 * 60;	// 1 day
		}
		return nUnits * 60 * 60;
	}

	// Not yet = do we work with minutes?
	nUnits = step / 60;
	if(nUnits)
	{
		nUnits = nextScaleStep(nUnits, true);
		// Maximum minutes is 30, then switch to hours
		if(nUnits > 30)
		{
			return 1 * 60 * 60;	// 1 hour
		}
		return nUnits * 60;
	}

	// Still working with seconds
	nUnits = nextScaleStep(step, true);
	// Maximum seconds is 30, then switch to minutes
	if(nUnits > 30)
	{
		return 1 * 60;	// 1 min
	}
	return nUnits;
}

/*
**	FUNCTION
**		Calculate next number of units (sec,min,hors, days) between labels.
**		Number of units is taken from the sequence: 1,2,5,10,20,50.
**		If units are 60-based (seconds and minutes) then also use 30.
**
**	PARAMETERS
**		step		- previously used step between labels
**		use60Base	- true if units are 60-based
**
**	RETURNS
**		New step between labels - always greater than previous one
**
** CAUTIONS
**		None
*/
int TimeAxisWidget::nextScaleStep(int step, bool use60Base)
{
	int	result = step, multiple = 1;

	if(step >= 100)
	{
		multiple = step / 100;
	}
	switch(step )
	{
	case 1:
		result = 2;
		break;
	case 2:
		result = 5;
		break;
	case 5:
		result = 10;
		break;
	case 10:
		result = 20;
		break;
	case 20:
		result = use60Base ? 30 : 50;
		break;
	case 30:
		result = 50;
		break;
	case 50:
		result = 100;
		break;
	}
	return result * multiple;
}

void TimeAxisWidget::paintEvent(QPaintEvent * /* pEvent */)
{
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("TimeAxisWidget::paintEvent(): font %s\n", font().toString().toLatin1().constData());
#else
		printf("TimeAxisWidget::paintEvent(): font %s\n", font().toString().toLatin1().constData());
		fflush(stdout);
#endif
	}

	QPainter painter(this);

	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("TimeAxisWidget::paintEvent(): painter font %s\n", painter.font().toString().toLatin1().constData());
#else
		printf("TimeAxisWidget::paintEvent(): painter font %s\n", painter.font().toString().toLatin1().constData());
		fflush(stdout);
#endif
	}

	draw(painter);
}

/*
**	FUNCTION
**		Mouse press event - show popup menu
**
**	PARAMETERS
**		pEvent	- Pointer to event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::mousePressEvent(QMouseEvent *pEvent)
{
	switch(pEvent->button() & Qt::MouseButtonMask)
	{
	case Qt::RightButton:
		showTimeScaleMenu();
		break;
	default:
		break;
	}
}

/*
**	FUNCTION
**		Show menu for selecting units for logarithmic time scale
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void TimeAxisWidget::showTimeScaleMenu(void)
{
	// Build menu to be shown
	QMenu *pMenu = new QMenu(this);

	pMenu->addAction("Time Scale");
	pMenu->addSeparator();

	QAction *pAction = pMenu->addAction("Linear");
	pAction->setData(1000000);
	pAction->setCheckable(true);
	pAction->setChecked(!logScale);
	pAction = pMenu->addAction("Logarithmic");
	pAction->setData(1000001);
	pAction->setCheckable(true);
	pAction->setChecked(logScale);

	if(logScale)
	{
		pMenu->addSeparator();
		pMenu->addAction("Interval Unit");
		pMenu->addSeparator();

		pAction = pMenu->addAction("Auto");
		pAction->setData(0);
		pAction->setCheckable(true);
		pAction->setChecked(autoLogAxisUnit == 0);
		pMenu->addSeparator();

		pAction = pMenu->addAction("1 sec");
		pAction->setData(1);
		pAction->setCheckable(true);
		pAction->setChecked(logAxisUnit == 1);
		pAction = pMenu->addAction("10 sec");
		pAction->setData(10);
		pAction->setCheckable(true);
		pAction->setChecked(logAxisUnit == 10);
		pAction = pMenu->addAction("1 min");
		pAction->setData(60);
		pAction->setCheckable(true);
		pAction->setChecked(logAxisUnit == 60);
		pAction = pMenu->addAction("10 min");
		pAction->setData(600);
		pAction->setCheckable(true);
		pAction->setChecked(logAxisUnit == 600);
		pAction = pMenu->addAction("1 hour");
		pAction->setData(3600);
		pAction->setCheckable(true);
		pAction->setChecked(logAxisUnit == 3600);
		pAction = pMenu->addAction("10 hours");
		pAction->setData(36000);
		pAction->setCheckable(true);
		pAction->setChecked(logAxisUnit == 36000);
		pAction = pMenu->addAction("1 day");
		pAction->setData(86400);
		pAction->setCheckable(true);
		pAction->setChecked(logAxisUnit == 86400);
		pAction = pMenu->addAction("10 days");
		pAction->setData(864000);
		pAction->setCheckable(true);
		pAction->setChecked(logAxisUnit == 864000);
	}
	connect(pMenu, SIGNAL(triggered(QAction *)), this, SLOT(axisMenu(QAction *)));
	pMenu->exec(QCursor::pos());
	delete pMenu;
}

void TimeAxisWidget::axisMenu(QAction *pAction)
{
	if(!pAction->data().isValid())
	{
		return;
	}
	int unit = pAction->data().toInt();
	if(unit >= 1000000)	// switch log/lin scale
	{
		setLogScale(unit - 1000000);
	}
	else	// set units for logarithmic scale
	{
		setLogAxisUnit(unit);
		update();
		emit limitsChanged(false);
	}
}

/*
**	FUNCTION
**		Dump axis parameters to text file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxisWidget::dump(FILE *pFile)
{
	if(!DebugCtl::isAxis())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	fprintf(pFile, "###################### TIME AXIS\n");
	fprintf(pFile, "  left %d top %d width %d height %d\n",
		graphArea.left(), graphArea.top(), graphArea.width(), graphArea.height());
	fprintf(pFile, "  min %s max %s (range = %ld) %s\n",
		min.toString("dd-MM-yyyy hh:mm:ss.zzz").toLatin1().constData(),
		max.toString("dd-MM-yyyy hh:mm:ss.zzz").toLatin1().constData(),
		(long)min.secsTo(max), (logScale ? "LOGARITHMIC" : "LINEAR"));
	fprintf(pFile, "  scale: left %d right %d\n", scaleLeft, scaleRight);
	if(logScale )
	{
		fprintf(pFile, "  logAxisUnit %d, logMax %f\n", logAxisUnit, logMax);
	}
	else
	{
		fprintf(pFile, "  labelStep %d startLabelTime %s\n",
			labelStep, startLabelTime.toString("dd-MM-yyyy hh:mm:ss.zzz").toLatin1().constData());
	}
	fprintf(pFile, "\n");
}
