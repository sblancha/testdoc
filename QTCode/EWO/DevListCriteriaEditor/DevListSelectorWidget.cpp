//	Implementation of DevListSelectorWiget class
/////////////////////////////////////////////////////////////////////////////////

#include "DevListSelectorWidget.h"

DevListSelectorWidget::DevListSelectorWidget(QWidget *parent, Qt::WindowFlags /* flags */) :
	QTabWidget(parent)
{
	mode = 0;

	pScopeSelector = new EqpTypeSelectorWidget(this);
	addTab(pScopeSelector, "Scope");
	connect(pScopeSelector, SIGNAL(selectionChanged(void)),
		this, SIGNAL(scopeChanged(void)));

	pCriteriaEditor = new DevListCriteriaEditorWidget(this);
	addTab(pCriteriaEditor, "Criteria");
	connect(pCriteriaEditor, SIGNAL(changed(void)),
		this, SIGNAL(criteriaChanged(void)));

	pEqpSelector = new SingleEqpSelectorWidget(this);
	addTab(pEqpSelector, "Manual");

	pDataModeEditor = new DataModeEditorWidget(this);
	DevListCriteria &criteria = DevListCriteria::getInstance();
	const char *label = criteria.getMode() == DataEnum::Replay ? "Archive" : "Online";
	addTab(pDataModeEditor, label);

	
	connect(pEqpSelector, SIGNAL(addEqpRequest(void)),
		this, SIGNAL(addEqpRequest(void)));
	connect(pEqpSelector, SIGNAL(removeSelected(void)),
		this, SIGNAL(removeSelected(void)));
	connect(pEqpSelector, SIGNAL(removeAll(void)),
		this, SIGNAL(removeAll(void)));

	connect(this, SIGNAL(currentChanged(int)),
		this, SLOT(tabCurrentChanged(int)));

	connect(pDataModeEditor, SIGNAL(modeChanged(void)),
		this, SLOT(dataModeChange(void)));
}

DevListSelectorWidget::~DevListSelectorWidget()
{
}

void DevListSelectorWidget::tabCurrentChanged(int tabIndex)
{
	int newMode = mode;
	QWidget *pWidget = widget(tabIndex);
	if(pWidget == pScopeSelector)
	{
		newMode = 0;
	}
	else if(pWidget == pCriteriaEditor)
	{
		newMode = 0;
	}
	else if(pWidget == pEqpSelector)
	{
		newMode = 1;
	}
	if(newMode == mode)
	{
		return;
	}
	mode = newMode;
	emit modeChanged(mode);
}

void DevListSelectorWidget::setMode(int mode)
{
	if(mode == 1)
	{
		if(currentWidget() != pEqpSelector)
		{
			setCurrentWidget(pEqpSelector);
		}
	}
	else
	{
		if(currentWidget() == pEqpSelector)
		{
			setCurrentWidget(pScopeSelector);
		}
	}
}

void DevListSelectorWidget::dataModeChange(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	setTabText(indexOf(pDataModeEditor), criteria.getMode() == DataEnum::Replay ? "Archive" : "Online");
	emit dataModeChanged();
}

int DevListSelectorWidget::getDataMode(void) const
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	return (int)criteria.getMode();
}

const QDateTime DevListSelectorWidget::getDataTime(void) const
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	return criteria.getTime();
}

