
#uses "VacCtlOnlineNotificationPvss"	// Load DLL
#uses "vclResources.ctl"

// Definitions, constants and commonly used functions for SMS notifications configuration

// Name of DP type holding common SMS configuration
const string SMS_CONFIG_COMMON_DP_TYPE = "_VacSmsConfigCommon";

// Name of DP type holding specific SMS configurations
const string SMS_CONFIG_DP_TYPE = "_VacSmsConfig";

// Name of DP type holding GROPU SMS configurations
const string SMS_CONFIG_GROUP_DP_TYPE = "_VacSmsConfigGroup";

// Name of DP type holding filter for the SMS task list
const string SMS_CONFIG_FILTER_DP_TYPE = "_VacSmsConfigFilter";

// Types of criteria used by functional (equipment) types
const int CRIT_TYPE_NONE = 0;
const int CRIT_TYPE_MAIN_STATE = 1;
const int CRIT_TYPE_MAIN_VALUE = 2;
const int CRIT_TYPE_ACTIVITY = 3;	// For mobile equipment
const int CRIT_TYPE_ERROR = 4;
const int CRIT_TYPE_WARNING = 5;

// How single criteria are joined
const int CRIT_JOIN_TYPE_AND = 1;
const int CRIT_JOIN_TYPE_OR = 2;


// How names are interpereted in SMS configuration scope
const int SCOPE_TYPE_SECTOR = 1;		// Names of vacuum sectors
const int SCOPE_TYPE_MAIN_PART = 2;	// Names of vacuum main parts
const int SCOPE_TYPE_ARC = 3;				// Names of LHC arcs - not implemented yet
const int SCOPE_TYPE_EQP = 4;				// Names of individual devices
const int SCOPE_TYPE_GLOBAL = 5;  // Equipment not related to sectors - machine name


// How resulting messages are filtered
const int MSG_FILTER_TYPE_NONE = 0;				// No filtering - all messages are sent
const int MSG_FILTER_TYPE_DEAD_TIME = 1;	// Messages are not sent too often
const int MSG_FILTER_TYPE_GROUPING = 2;		// Number of messages is grouped to produce single message

// In case of MSG_FILTER_TYPE_GROUPING - how messages are grouped
const int MSG_GROUPING_NONE = 0;

const int MSG_GROUPING_MACHINE = 1;				// All messages in PVSS system
const int MSG_GROUPING_MAIN_PART = 2;			// All messages within main part
const int MSG_GROUPING_SECTOR = 3;				// All messages within vacuum sector
const int MSG_GROUPING_ARC = 4;						// All messages within LHC arc - not implemented yet

// LhcSmsGetCommonConfigDp
/**
Purpose:
Return name of DP containing common configuration for SMS notifications.

Arguments:
		dpName	-	string, [out] Name of DP will be returned here. In case
										of error returned string is empty
Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.6 
	. operating system: WXP, NT and Linux, but tested only under Linux.
	. distributed system: yes.
*/
void LhcSmsGetCommonConfigDp(string &dpName)
{
  dpName = SMS_CONFIG_COMMON_DP_TYPE;
  if(dpExists(dpName))
  {
    return;
  }

  dpCreate(dpName, SMS_CONFIG_COMMON_DP_TYPE);
  if(!dpExists(dpName))
  {
    DebugTN("LhcSmsGetCommonConfigDp(): failed to create DP <" + dpName + ">");
    dpName = "";
    return;
  }
  dpSetWait(dpName + ".server", "cernmx.cern.ch");
  dpSetWait(dpName + ".client", "cern.ch");
  dpSetWait(dpName + ".sender", "vacin@cern.ch");
}

// LhcSmsCreateConfigDp
/**
Purpose:
Create new DP for specific SMS configuration.

Arguments:
    dpName         -  string, [out] Name of DP will be returned here. In case
                      of error returned string is empty
    exceptionInfo  - dyn_string, [out] Details of error are returned here

Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.6 
	. operating system: WXP and Linux
	. distributed system: yes.
*/
void LhcSmsCreateConfigDp(string &dpName, dyn_string &exceptionInfo)
{
  string text = "SELECT 'MAX(_original.._value)' FROM '*.DpNumber' WHERE (_DPT=\"" +
    SMS_CONFIG_DP_TYPE + "\")";
  dyn_dyn_anytype tab;
  dpQuery(text, tab);
  int nReturned = dynlen(tab);
  int dpNumber = 1;
  if(nReturned > 1)
  {
    dpNumber = tab[2][2] + 1;
  }
  for( ; dpNumber <= 2000000000 ; dpNumber++)
  {
    sprintf(dpName, "%s_%d", SMS_CONFIG_DP_TYPE, dpNumber);
    if(dpExists(dpName))
    {
      continue;
    }
    dpCreate(dpName, SMS_CONFIG_DP_TYPE);
    if(!dpExists(dpName))
    {
      fwException_raise(exceptionInfo, "ERROR", "LhcSmsCreateConfigDp(): failed to create DP <" + dpName + ">", "");
      dpName = "";
    }
    dpSetWait(dpName + ".DpNumber", dpNumber);
    return;
  }
  fwException_raise(exceptionInfo, "LhcSmsCreateConfigDp(): too many DPs have been created", "");
  dpName = "";
}

// LhcSmsCreateConfigGroupDp
/**
Purpose:
Create new DP for specific SMS configuration group

Arguments:
    dpName         -  string, [out] Name of DP will be returned here. In case
                      of error returned string is empty
    exceptionInfo  - dyn_string, [out] Details of error are returned here

Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.6 
	. operating system: WXP and Linux
	. distributed system: yes.
*/
void LhcSmsCreateConfigGroupDp(string &dpName, dyn_string &exceptionInfo)
{
  int	dpNumber = 1;
  for(int dpNumber = 1 ; dpNumber <= 999999 ; dpNumber++)
  {
    sprintf(dpName, "%s_%06d", SMS_CONFIG_GROUP_DP_TYPE, dpNumber);
    if(dpExists(dpName))
    {
      continue;
    }
    dpCreate(dpName, SMS_CONFIG_GROUP_DP_TYPE);
    if(!dpExists(dpName))
    {
      fwException_raise(exceptionInfo, "ERROR", "LhcSmsCreateConfigGroupDp(): failed to create DP <" + dpName + ">", "");
      dpName = "";
    }
    return;
  }
  fwException_raise(exceptionInfo, "LhcSmsCreateConfigGroupDp(): too many DPs have been created", "");
  dpName = "";
}

// LhcSmsCreateConfigGroupDp
/**
Purpose:
Create new DP for specific SMS configuration group

Arguments:
    dpName         -  string, [out] Name of DP will be returned here. In case
                      of error returned string is empty
    exceptionInfo  - dyn_string, [out] Details of error are returned here

Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.6 
	. operating system: WXP and Linux
	. distributed system: yes.
*/
void LhcSmsGetConfigFilterDp(string &dpName, dyn_string &exceptionInfo)
{
  dpName = SMS_CONFIG_FILTER_DP_TYPE + "_" + myManNum();
  if(dpExists(dpName))
  {
    return;
  }
  dpCreate(dpName, SMS_CONFIG_FILTER_DP_TYPE);
  if(!dpExists(dpName))
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcSmsGetConfigFilterDp(): failed to create DP <" + dpName + ">", "");
    dpName = "";
  }
}

// VacSmsBuildGroupContent
/**
Purpose:
Find all configuration DPs which are part of given group

Arguments:
    groupDpName  -  string, [in] Name of group DP

Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.6 
	. operating system: WXP and Linux
	. distributed system: yes.
*/
dyn_string VacSmsBuildGroupContent(string groupDpName)
{
  dyn_string result;

  string groupName;
  if(groupDpName != "")
  {
    groupName = dpSubStr(groupDpName, DPSUB_DP);
  }
  string text = "SELECT '_original.._value' FROM '_VacSmsConfig*.Group' WHERE (_DPT=\"" +
    SMS_CONFIG_DP_TYPE + "\") AND ('_original.._value'=\"" +
    groupName + "\")";
  dyn_dyn_anytype tab;
  dpQuery(text, tab);
  int nReturned = dynlen(tab);
  for(int n = 2 ; n <= nReturned ; n++)
  {
    string dpName = dpSubStr(tab[n][1], DPSUB_DP);
    dynAppend(result, dpName);
  }
//DebugTN("" + dynlen(result) + " for " + groupName);
  /*
  dyn_string dpList = dpNames("*", SMS_CONFIG_DP_TYPE);
  for(int n = dynlen(dpList) ; n > 0 ; n--)
  {
    string group = "";
    dpGet(dpList[n] + ".Group", group);
    if(group == groupDpName)
    {
      string dpName = dpSubStr(dpList[n], DPSUB_DP);
      dynAppend(result, dpName);
    }
  }
  */
  return result;
}

dyn_string VacSmsGetSimpleConfigDps()
{
  return VacSmsBuildGroupContent("");
}

// VacSmsWriteToFile
/**
Purpose:
Write SMS configuration in given DP to CSV file

Arguments:
    fileName   - string, [in] Name of output file
    dpName     - string, [in] Name of SMS configuration DP
    isGroup    - bool, [in] true if group configuration shall be written

Usage: Public

PVSS manager usage: UI (PVSS00UI)

Constraints:
	. PVSS version: 3.6 
	. operating system: WXP and Linux
	. distributed system: yes.
*/
void VacSmsWriteToFile(string fileName, string dpName, bool isGroup)
{
  VacSmsConfigPrepareForWrite();
  if(isGroup)
  {
    VacSmsPrepareGroupConfigForWrite(dpName);
  }
  else
  {
    VacSmsPrepareConfigForWrite(dpName);
  }
  dyn_string exceptionInfo;
  VacSmsConfigWrite(fileName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

void VacSmsPrepareGroupConfigForWrite(string dpName)
{
  dyn_string groupDps = VacSmsBuildGroupContent(dpName);
  int nDps = dynlen(groupDps);
  for(int n = 1 ; n <= nDps ; n++)
  {
    VacSmsPrepareConfigForWrite(groupDps[n]);
  }
}

void VacSmsPrepareConfigForWrite(string dpName)
{
  int id = VacSmsConfigAdd(dpName);

  string visibleName;
  dpGet(dpName + ".VisibleName", visibleName);
  VacSmsConfigSetVisibleName(id, visibleName);

  int value;

  dpGet(dpName + ".Mode", value);
  VacSmsConfigSetMode(id, value);

  dyn_int funcTypes;
  dpGet(dpName + ".Criteria.FunctionalTypes", funcTypes);
  if(dynlen(funcTypes) > 0)
  {
    VacSmsConfigSetMainType(id, funcTypes[1]);
  }

  dyn_int usageTypes;
  dyn_string attrNames;
  dyn_bool reverseFlags;
  dpGet(dpName + ".Scope.UsageFilter.FunctionalTypes", usageTypes,
        dpName + ".Scope.UsageFilter.AttrNames", attrNames,
        dpName + ".Scope.UsageFilter.ReverseFlags", reverseFlags);
  VacSmsConfigSetUsageFilter(id, usageTypes, attrNames, reverseFlags);
 
  dyn_string names;
  dpGet(dpName + ".Scope.Type", value,
    dpName + ".Scope.Names", names);
  VacSmsConfigSetScopeType(id, value);
  VacSmsConfigSetScopeItems(id, names);

  dpGet(dpName + ".Criteria.JoinType", value);
  VacSmsConfigSetCritJoinType(id, value);

  dyn_int types, subTypes, minDurations;
  dyn_float lowerLimits, upperLimits;
  dpGet(dpName + ".Criteria.Types", types,
    dpName + ".Criteria.Subtypes", subTypes,
    dpName + ".Criteria.LowerLimits", lowerLimits,
    dpName + ".Criteria.UpperLimits", upperLimits,
    dpName + ".Criteria.ReverseFlags", reverseFlags,
    dpName + ".Criteria.MinDurations", minDurations);
  int critLen = dynlen(funcTypes);
  if(dynlen(types) < critLen)
  {
    critLen = dynlen(types);
  }
  if(dynlen(subTypes) < critLen)
  {
    critLen = dynlen(subTypes);
  }
  if(dynlen(lowerLimits) < critLen)
  {
    critLen = dynlen(lowerLimits);
  }
  if(dynlen(upperLimits) < critLen)
  {
    critLen = dynlen(upperLimits);
  }
  if(dynlen(reverseFlags) < critLen)
  {
    critLen = dynlen(reverseFlags);
  }
  if(dynlen(minDurations) < critLen)
  {
    critLen = dynlen(minDurations);
  }
  for(int n = 1 ; n <= critLen ; n++)
  {
    VacSmsConfigAddCriteria(id, funcTypes[n], types[n], subTypes[n], reverseFlags[n], lowerLimits[n], upperLimits[n], minDurations[n]);
  }

  string message;
  dpGet(dpName + ".Message", message);
  VacSmsConfigSetMessage(id, message);

  int filterType, filterTime, groupType, groupTime, indivLimit;
  dpGet(dpName + ".MsgFiltering.Type", filterType,
    dpName + ".MsgFiltering.DeadTime", filterTime,
    dpName + ".MsgFiltering.Grouping.Type", groupType,
    dpName + ".MsgFiltering.Grouping.Interval", groupTime,
    dpName + ".MsgFiltering.Grouping.Count", indivLimit);
  VacSmsConfigSetFiltering(id, filterType, filterTime, groupType, groupTime, indivLimit);

  dpGet(dpName + ".MsgFiltering.Grouping.Message", message);
  VacSmsConfigSetGroupMessage(id, message);

  dpGet(dpName + ".Recipients", names);
  VacSmsConfigSetRecipients(id, names);
}

void VacSmsNewFromFile(string fileName, string &dpName, bool &isGroup, dyn_string &exceptionInfo)
{
  int nConfigs = VacSmsConfigParse(fileName, exceptionInfo);
  DebugTN("Parse = " + nConfigs + ", exceptionInfo:", exceptionInfo);
  if(nConfigs <= 0)
  {
    return;
  }
  if(nConfigs > 1)  // Group config
  {
    LhcSmsCreateConfigGroupDp(dpName, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      fwExceptionHandling_display(exceptionInfo);
      return;
    }
    dpSet(dpName + ".Owner", LhcVacGetUserName(),
      dpName + ".VisibleName", "New from file");
    isGroup = true;
  }
  else
  {
    isGroup = false;
  }

  string owner = LhcVacGetUserName();
  int nAdded = 0;
  for(int configId = VacSmsConfigFirstParsed() ; configId != 0 ; configId = VacSmsConfigNextParsed(configId))
  {
    VacSmsCreateConfig(dpName, configId, owner, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      VacSmsDeleteConfig(dpName);
      break;
    }
    nAdded++;
    if(!isProgressBarOpen())
    {
      fwException_raise(exceptionInfo, "ERROR", "VacSmsNewFromFile(): create procedure cancelled", "");
      break;
    }
    else if((nAdded % 50) == 0)
    {
      float percent = nAdded;
      percent /= nConfigs;
      percent *= 100;
      showProgressBar("", "Creating configuration " + nAdded + "/" + nConfigs,
        "", percent);
    }
  }
DebugTN("Total created " + nAdded);
}

void VacSmsCreateConfig(string &dpName, int configId, string owner, dyn_string &exceptionInfo)
{
  string configDp;
  LhcSmsCreateConfigDp(configDp, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
   return;
  }
  dpSet(configDp + ".Owner", owner);
  if(dpName != "")  // Group, create new DP for group item
  {
    dpSet(configDp + ".Group", dpName);
  }
  else
  {
    dpName = configDp;
  }

  // Set all config parameters
  string visibleName;
  VacSmsConfigGetVisibleName(configId, visibleName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSet(configDp + ".VisibleName", visibleName);

  int mode;
  VacSmsConfigGetMode(configId, mode, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSet(configDp + ".Mode", mode);

  int type;
  VacSmsConfigGetMainType(configId, type, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }

  dyn_int usedByTypes;
  dyn_string attrNames;
  dyn_bool reverseFlags;
  VacSmsConfigGetUsageFilter(configId, usedByTypes, attrNames, reverseFlags, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSet(configDp + ".Scope.UsageFilter.FunctionalTypes", usedByTypes,
        configDp + ".Scope.UsageFilter.AttrNames", attrNames,
        configDp + ".Scope.UsageFilter.ReverseFlags", reverseFlags);
  
  VacSmsConfigGetScopeType(configId, type, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dyn_string names;
  VacSmsConfigGetScopeItems(configId, names, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSet(configDp + ".Scope.Type", type,
    configDp + ".Scope.Names", names);

  VacSmsConfigGetCritJoinType(configId, type, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }

  dyn_int funcTypes, types, subTypes, minDurations;
  dyn_float lLimits, uLimits;
  VacSmsConfigGetCriteria(configId, funcTypes, types, subTypes, reverseFlags, lLimits, uLimits, minDurations, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSet(configDp + ".Criteria.JoinType", type,
    configDp + ".Criteria.FunctionalTypes", funcTypes,
    configDp + ".Criteria.Types", types,
    configDp + ".Criteria.Subtypes", subTypes,
    configDp + ".Criteria.LowerLimits", lLimits,
    configDp + ".Criteria.UpperLimits", uLimits,
    configDp + ".Criteria.ReverseFlags", reverseFlags,
    configDp + ".Criteria.MinDurations", minDurations);

  string message;
  VacSmsConfigGetMessage(configId, message, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSet(configDp + ".Message", message);

  int filterTime, groupType, groupTime, indivLimit;
  VacSmsConfigGetFiltering(configId, type, filterTime, groupType, groupTime, indivLimit, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  VacSmsConfigGetGroupMessage(configId, message, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSet(configDp + ".MsgFiltering.Type", type,
    configDp + ".MsgFiltering.DeadTime", filterTime,
    configDp + ".MsgFiltering.Grouping.Type", groupType,
    configDp + ".MsgFiltering.Grouping.Interval", groupTime,
    configDp + ".MsgFiltering.Grouping.Count", indivLimit,
    configDp + ".MsgFiltering.Grouping.Message", message);
  
  VacSmsConfigGetRecipients(configId, names, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dpSet(configDp + ".Recipients", names);
}

void VacSmsDeleteConfig(string dpName)
{
  dyn_string dpList = VacSmsBuildGroupContent(dpName);
  for(int n = dynlen(dpList) ; n > 0 ; n--)
  {
    dpSet(dpList[n] + ".ToBeDeleted", true);
  }
  dpSet(dpName + ".ToBeDeleted", true);
}

bool VacSmsIsMyConfiguration(string dpName)
{
  string owner;
  dpGet(dpName + ".Owner", owner);
  string userName = LhcVacGetUserName();
  bool result = owner == userName;
  if(!result)
  {
    result = LhcVacUserHasMaxPrivilege();
  }
  return result;
}

string VacSmsGetConnectionMonitor(dyn_string &exceptionInfo)
{
  string dpName = "_VacSmsConnectMonitor_" + myManNum();
  if(dpExists(dpName))
  {
    return dpName;
  }
  dpCreate(dpName, "_VacDpConnectionMonitor");
  if(!dpExists(dpName))
  {
    fwException_raise(exceptionInfo, "ERROR", "VacSmsGetConnectionMonitor(): failed to create DP <" + dpName + ">", "");
    return "";
  }
  return dpName;
}

// Calculate machine mode for SMS from Amode+Bmode values
int VacSmsMachineModeAuto(int amode, int bmode)
{
  int result = 0;

  // Modes for SMS definition:
  // 1 = PROTON
  // 2 = MACHINE DEVELOP
  // 4 = CRYO/ACCESS
  // 8 = SHUTDOWN
  // 16 = STEADY
  // 32 = BEAM SETUP
  // 64 = ION
  switch(amode)
  {
  case 1:  // NOMODE No valid machine mode 
    break;
  case 2:  // ACCESS Access or about to access machine 
    result = 4;
    break;
  case 3:  // SHUTDOWN Machine shutdown 
    result = 8;
    break;
  case 4:  // COOLDOWN Cryo related before start up 
    result = 4;
    break;
  case 5:  // CHECKOUT Checking subsystems before startup 
    result = 16;
    break;
  case 6:  // MACHTEST Ad-hoc tests with no beam 
    result = 16;
    break;
  case 7:  // CALIB Calibrating power converters 
    result = 16;
    break;
  case 8:  // BMSETUP Machine setup with beam 
    result = 32;
    break;
  case 9:  // PROTPHYS Proton physics 
    result = 1;
    break;
  case 10:  // IONPHYS Ion physics 
    result = 64;
    break;
  case 11:  // SPOPTPHY Special Optics Physics 
    result = 1;
    break;
  case 12:  // MD Machine development with beam 
    result = 2;
    break;
  case 13:  // WARMUP One ore more sectors warming up for repair 
    result = 4;
    break;
  case 14:  // RECOVERY Quench or cryo recovery 
    result = 4;
    break;
  case 15:  // SECTDEP Status dependent on sector modes
    break; 
  case 16:  // PAPHYS Proton-nucleus physics 
    result = 1;
    break;
  }
  return result;
}

string VacSmsAModeName(int mode)
{
  string resourceName = "AMode" + mode;
  string result = VacResourcesGetValue(resourceName, VAC_RESOURCE_STRING, "");
  if(result == "")
  {
    sprintf(result, "??? %d", mode);
  }
  return result;
}

string VacSmsMachineModeName(int mode)
{
  string result;
  for(int bit = 0 ; bit < 32 ; bit++)
  {
    if(mode == (1 << bit))
    {
      string resourceName = "MachineMode" + bit;
      result = VacResourcesGetValue(resourceName, VAC_RESOURCE_STRING, "");
      if(result != "")
      {
        return result;
      }
      else
      {
        break;
      }
    }
  }
  sprintf(result, "??? %d", mode);
  return result;
}

bool VacIsValidMachineMode(int mode)
{
  for(int bit = 0 ; bit < 32 ; bit++)
  {
    if(mode == (1 << bit))
    {
      string resourceName = "MachineMode" + bit;
      string result = VacResourcesGetValue(resourceName, VAC_RESOURCE_STRING, "");
      return result != "";
    }
  }
  return false;
}
