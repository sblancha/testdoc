#ifndef RESOURCEEQPMASK_H
#define	RESOURCEEQPMASK_H

//	Class holding equipment type mask definition read from file

#include <qstring.h>

class ResourceEqpMask
{
public:
	ResourceEqpMask(const char *name, int value)
	{
		this->name = name;
		this->value = value;
	}
	~ResourceEqpMask() {}

	// Access
	inline const QString &getName(void) const { return name; }
	inline int getValue(void) const { return value; }
 
protected:
	// Mask name given in resource file
	QString	name;

	// Mask value given in resource file
	int		value;
};

#endif	// RESOURCEEQPMASK_H
