#ifndef	SYNLHCVIEW_H
#define	SYNLHCVIEW_H

//	LHC synoptic image

#include "SynLhcLine.h"
#include "SynopticWidget.h"

#include "VacEqpTypeMask.h"
#include "Eqp.h"
#include "EqpMsgCriteria.h"

#include <qlist.h>

#include <stdio.h>

class SynopticToolTip;


class SynLhcView : public SynopticWidget
{
	Q_OBJECT

public:
	SynLhcView(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	virtual ~SynLhcView();

	static SynLhcView *create(QWidget *parent, Sector *pFirstSector, Sector *pLastSector,
		DataEnum::DataMode mode, bool hideMagnets = false);

	void analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allCryoEqpMask,
		VacEqpTypeMask &allBeamEqpMask);
	void applyMask(unsigned vacTypeMask, VacEqpTypeMask &cryoEqpMask, VacEqpTypeMask &beamEqpMask);

	void setViewStart(int viewStart, int viewWidth, bool dragging);

	bool findDevicePosition(Eqp *pEqp, int &position, bool &visible);

	void setAlwaysIncludeSectorBorder(bool value);

	// Implementa abstract method of SynopticWidget
	virtual bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	void dump(const char *fileName);

	void connectSectorSelection(SynLhcLine * pLine); // [VACCO-929]
	void disconnectSectorSelection(SynLhcLine * pLine); // [VACCO-929]
	SynLhcSectorLabel * getSectorFromXY(int mouseX, int mouseY); // [VACCO-929]

	virtual void showMobilesNotConnected(void);
	virtual void hideMobilesNotConnected(void);

signals:
	void mobileStateChanged(void);
	void mouseDown(int button, int mode, int localX, int localY, int globalX, int globalY, int extra); // [VACCO-929] [VACCO-948] [VACCO-1645]
	void synSectorMouseDown(int button, int mode, int x, int y, const char *sectorName, bool selected); // [VACCO-929] [VACCO-948] [VACCO-1645]

	
public slots:
	bool synSectorClicked(int button, int mode, int localX, int localY, int globalX, int globalY, int extra); // [VACCO-929] [VACCO-948] [VACCO-1645]
	void forceRedraw(); // [VACCO-929]

protected:
	// Pointer to start sector
	Sector					*pStartSector;

	// Pointer to end sector
	Sector					*pEndSector;

	// Start coordinate [m]
	float					start;

	// End coordinate [m]
	float					end;

	// List of synoptic lines
	QList<SynLhcLine *>	lines;

	// Tooltip for synoptic view
	SynopticToolTip			*pToolTip;

	// Width of image on unlimited drawing area
	int						totalWidth;

	// Height of image on unlimited drawing area
	int						totalHeight;

	// Start X-coordinate of visible area
	int						viewStart;

	// Bit mask for vacuum type selection
	unsigned 				vacTypeMask;

	// Equipment type mask for isolation vacuum
	VacEqpTypeMask			cryoEqpMask;

	// Equipment type mask for beam vacuum
	VacEqpTypeMask			beamEqpMask;

	// Data acquisition mode for this view
	DataEnum::DataMode		mode;

	// Flag indicating if redrawing is required after mobile equipment state change
	bool					redrawRequiredForMobile;


	virtual void build(bool hideMagnets);
	virtual void buildView(unsigned vacTypeMask, VacEqpTypeMask &cryoEqpMask,
		VacEqpTypeMask &beamEqpMask);
	virtual void buildGeometry(void);
	SynLhcLine *findLineOfType(int type);
	virtual void buildLinesGeometry(void);
	virtual void finishLineParts(void);
	void calcVerticalPos(void);
	SynLhcLine *nextLine(SynLhcLine *pPrevLine);
	int compareCoordInView(float bestCoord, float newCoord);

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void mousePressEvent(QMouseEvent *pEvent); // [VACCO-929] React on mouse press
};

#endif	// SYNLHCVIEW_H
