//	Implementation of FunctionalTypeVA class
////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVA.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

FunctionalTypeVA::FunctionalTypeVA(int type, const char *name, const char *description) :
FunctionalType(type, name, description) {
}
/**
@brief FUNCTION Add possible criteria fro this functional type to this (or another).
@param[in]   pType		pointer to type where criteria shall be added.
*/
void FunctionalTypeVA::fillCriteria(FunctionalType *pType)
{
	// Own criteria
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Ok, "Hardware Alarm is OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} alarm is OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} alarm is NOT OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Hardware Alarm in sector {sector} is OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Hardware Alarm in sector {sector} is NOT OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Hardware Alarm in main part {mainPart} is OK");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Hardware Alarm in main part {mainPart} is NOT OK");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Bad, "Hardware Alarm is BAD");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} alarm is BAD");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} alarm is NOT BAD");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Hardware Alarm in sector {sector} is BAD");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Hardware Alarm in sector {sector} is NOT BAD");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Hardware Alarm in main part {mainPart} is BAD");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Hardware Alarm in main part {mainPart} is NOT BAD");
}
/**
@brief FUNCTION Return key for searching default message for criteria in resources
@param pCriteria	pointer to criteria that needs a key
*/
const char *FunctionalTypeVA::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch (pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch (pCriteria->getSubType())
		{
		case Ok:
			result = "OK";
			break;
		case Bad:
			result = "BAD";
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
	return result;
}
/**
@brief FUNCTION Return key for searching default message for criteria in resources
@param critType	type of criteria here Ok or Bad
@param name		key name of the criteria type "OK" or "BAD"
@param errMsg	error message link with this criteria type
*/
int FunctionalTypeVA::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch (critType)
	{
	case EqpMsgCriteria::MainState:
		if (!strcasecmp(name, "OK")) {
			result = Ok;
		}
		else if (!strcasecmp(name, "BAD")) {
			result = Bad;
		}
		break;
	default:
		break;
	}
	if (result)	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}
