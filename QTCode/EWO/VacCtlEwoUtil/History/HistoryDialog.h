#ifndef HISTORYDIALOG_H
#define	HISTORYDIALOG_H

// Value history dialog

#include <QDialog>

#include "HistoryImage.h"

class Eqp;

#include <QScrollBar>
#include <QTableWidget>
#include <QLabel>
#include <QCheckBox>
#include <QSpinBox>
#include <QPushButton>
#include <QMenu>
#include <QDateTime>
#include <QVariant>
#include <QGroupBox>
#include <QMouseEvent>
#include <QComboBox>

class HistoryDialog : public QDialog
{
	Q_OBJECT

public:
	static int getAllInstances(QList<int> &idList);
	static int create(void);
	static int getFilterType(int instanceId);
	static int setFilterType(int instanceId, int type);
	static int clear(int instanceId);
	static int addDevice(Eqp *pEqp, int historyDepth);
	static int addDevice(int instanceId, Eqp *pEqp, const char *colorString, int historyDepth);
	static int addDeviceDpe(Eqp *pEqp, const char *dpeName, const char *yAxisName, int historyDepth);
	static int addDeviceDpe(int instanceId, Eqp *pEqp, const char *dpeName, const char *yAxisName, const char *colorString, int historyDepth);
	static int addDeviceBit(Eqp *pEqp, const char *dpe, int bitNbr, const char *name, int historyDepth);
	static int addDeviceBit(int instanceId, Eqp *pEqp, const char *dpe, int bitNbr, const char *name, const char *colorString, int historyDepth);
	static bool addHistoryValue(int id, const char *dpe, bool bitState, int bitNbr, const QDateTime &time, const QVariant &value);
	static void finishHistoryRange(int id, const char *dpe, bool bitState, int bitNbr);

	static void getAllItems(int id, QStringList &dps, QStringList &dpes, int **bitNbrs, QStringList &colors, QStringList &axisTypeNames);
	static int getTimeScaleParam(int id, QDateTime &start, QDateTime &end, bool &online, bool &logScale);
	static int setTimeScaleParam(int id, const QDateTime &start, const QDateTime &end, bool online, bool logScale);
	static int getVerticalScalesParams(int id, int &nScales, int **ppTypes, bool **ppLogs, float **ppMins, float **ppMaxs);
	static int setVerticalScaleParams(int id, int type, bool log, float min, float max);
	static int getAllComments(int id, QStringList &comments);
	static int setAllComments(int id, QStringList comments);

	static const QString &getTitle(int id);
	static int setTitle(int id, const char *title);
	static int getWindowGeometry(int id, int &x, int &y, int &w, int &h);
	static int setWindowGeometry(int id, int x, int y, int w, int h);

	static const QString &getConfigName(int id);
	static int setConfigName(int id, const QString &name);
	static void instanceActivated(HistoryDialog *instance);
	static HistoryDialog *findActive(void);

	// Destructor is made public to make QPtrList happy
	~HistoryDialog();

	// Access
	inline int getId(void) const { return id; }
	inline const QString &getTitle(void) const { return title; }
	void setTitle(const QString &newTitle);
	inline const QString &getConfigName(void) const { return configName; }
	inline void setConfigName(const QString &name) { configName = name; }

signals:
	void dpMouseDown(int button, int mode, int x, int y, int extra, const char *dpName);
	void saveRequest(int id);
	void saveMultiRequest(int id);
	void loadRequest(int id);
	void fillNumberRangeRequest(int id);

protected:
	// List of dialog instances
	static QList<HistoryDialog *>	instanceList;

	// Variable where ID of next instance is taken
	static int lastInstanceId;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// History image
	HistoryImage	*pImage;

	// Horizontal scroll bar below image to scroll time range
	QScrollBar		*pScrollBar;

	// Table with list of devices shown in history
	QTableWidget			*pTable;

	// Label where time of mouse pointer is shown
	QLabel			*pTimeLabel;

	// Label where value of selected device at selected time is shown
	QLabel			*pValueLabel;

	// Checkbox to switch ON/OFF online mode of history
	QCheckBox		*pOnlineCheckBox;

	// ComboBox for filtering type selections
	QComboBox		*pFilterTypeCombo;

	/*
	// Checkbox to switch ON/OFF logarithmic time scale
	QCheckBox		*pLogTimeCheckBox;

	// Checkbox to switch temporarily pressure axis to linear
	QCheckBox		*pRpLinearCheckBox;

	// Checkbox to switch temporarily intensity axis to linear
	QCheckBox		*pIntensLinearCheckBox;
	*/

	// Spin box to change number of minutes in displayed time range
	QSpinBox		*pMinSpinBox;

	// Spin box to change number of hours in displayed time range
	QSpinBox		*pHourSpinBox;

	// Spin box to change number of days in displayed time range
	QSpinBox		*pDaySpinBox;

	// GroupBox containing control for editing time range
	QGroupBox 		*pRangeBox;

	// Button to open dialog for direct editing of displayed time range
	QPushButton		*pRangePb;

	// Button to select fill number to be displayed (LHC only)
	QPushButton		*pFillNumberPb;

	// Button for displaying 'File' menu
	QPushButton		*pFilePb;

	// 'File' menu
	QMenu			*pFileMenu;

	// Button to close this dialog
	QPushButton		*pClosePb;

	// Button to show help
	QPushButton		*pHelpPb;

	// Dialog title
	QString			title;

	// Name of configuration that created this instance, may be empty
	QString			configName;

	// ID of this dialog instance
	int				id;

	// 'Activate' state of this instance: 0 for last activated instance, the higher number - the
	// 'deeper' this instance in activation queue
	int				activated;

	// Row number of item where popup menu is displayed
	int				menuRow;

	// Row number from where selection is initiated
	int				selectionRow;

	// Flag indicating if SpinBox value is set from code, so range cnange is not needed
	bool			settingSpinBoxes;

	// Flag indicating if ScrollBar value is set from code, so range cnange is not needed
	bool			settingScrollBar;

	// Flag indicating if Online check box is set from code
	bool			settingOnline;

	static HistoryDialog *findInstance(int id = 0);

	HistoryDialog();


	static bool stringToColor(const char *string, QColor &color);
	static void colorToString(const QColor &color, QString &string);

	void clear(void);
	void buildLayout(void);
	inline int getFilterType(void) const { return pImage->getFilterType(); }
	void setFilterType(int type);
	int addEqp(Eqp *pEqp, int historyDepth);
	int addEqp(Eqp *pEqp, const QColor &useColor, int historyDepth);
	int addDpe(Eqp *pEqp, const char *dpeName, const char *yAxisName, int historyDepth);
	int addDpe(Eqp *pEqp, const char *dpeName, const char *yAxisName, const QColor &useColor, int historyDepth);
	int addEqpBit(Eqp *pEqp, const char *dpe, int bitNbr, const char *name, int historyDepth);
	int addEqpBit(Eqp *pEqp, const char *dpe, int bitNbr, const char *name, const QColor &color, int historyDepth);
	void addToTable(Eqp *pEqp, const QColor &color, const char *bitName);

	void getAllItems(QStringList &dps, QStringList &dpes, int **bitNbrs, QStringList &colors, QStringList &axisTypeNames);
	void getTimeScaleParam(QDateTime &start, QDateTime &end, bool &online, bool &logScale);
	void setTimeScaleParam(const QDateTime &start, const QDateTime &end, bool online, bool logScale);
	void setWindowGeometry(int x, int y, int w, int h);
	void getAllComments(QStringList &comments);
	void setAllComments(QStringList comments);

	void showTimeRange(void);
	void setRangeFromSpinBoxes(void);

	void setSelectionInTable(int row, Eqp *pEqp);
	void setRowSelectionInTable(int row, bool isSelected);

	virtual void resizeEvent(QResizeEvent *e);
	virtual bool event(QEvent *e);

	void writeToFile(bool writeCsv);
	bool saveImageToPng(const QString &pathName);

private slots:
	void updateScrollBar(void);
	void scrollBarValueChange(int value);
	void scrollBarSliderRelease(void);

	void onlineChange(bool on);
	void filterTypeComboActivated(int index);

	/*
	void logTimeChange(bool on);
	void linearPrChange(bool on);
	void linearIntensChange(bool on);
	*/

	void minChange(int value);
	void hourChange(int value);
	void dayChange(int value);

	void tableCellChanged(int row, int column);

	void fileTitle(void);
	void filePrint(void);
	void fileExport(void);
	void fileExportCsv(void);
	void fileSavePng(void);
	void fileSaveAllPng(void);
	void fileSave(void);
	void fileSaveMulti(void);
	void fileLoad(void);
	void closePress(void);
	void helpPress(void);

	void eqpSelectChanged(Eqp *pEqp);
	void valueAtPointerChanged(const QString &timeStr, const QString &valueStr);
	void imageZoomed(void);

	void tableMousePress(int row, int col);
	void tableDoubleClick(int row, int col);
	void tableContextMenuRequest(const QPoint &point);
	void removeItem(void);

	void rangeButtonClicked(void);
	void fillNumberButtonClicked(void);
	void rangeChange(const QDateTime &start, const QDateTime &end);
};

#endif	// HISTORYDIALOG_H
