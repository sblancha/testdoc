#ifndef _VACCTLIMPORTPVSS_H_
#define _VACCTLIMPORTPVSS_H_

#include <VacCtlPvssUtil.h>

# ifdef _WIN32
#   define  PVSS_EXPORT  __declspec(dllexport)
# else
#   define  PVSS_EXPORT
# endif


class DpData;
class DpeData;

class VacCtlImportPvss : public VacCtlPvssUtil
{
public:
    VacCtlImportPvss(BaseExternHdl *nextHdl, PVSSulong funcCount, FunctionListRec fnList[])
      : VacCtlPvssUtil(nextHdl, funcCount, fnList) {}

    virtual const Variable *execute(ExecuteParamRec &param);

protected:
	int executeParseFile(ExecuteParamRec &param);
	int executeGetTimeStamp(ExecuteParamRec &param);
	int executeGetDeviceDataByName(ExecuteParamRec &param);
	int executeGetDeviceDataByIndex(ExecuteParamRec &param);
	int executeGetAllArchives(ExecuteParamRec &param);
	int executeGetAllPollGroups(ExecuteParamRec &param);
	int executeGetAllS7Connection(ExecuteParamRec &param);
	int executeSetDpeAddressChange(ExecuteParamRec &param);
	int executeSetDpeArchiveChange(ExecuteParamRec &param);
	int executeSetDpeValueFormatChange(ExecuteParamRec &param);
	int executeAddDpe(ExecuteParamRec &param);
	int executeSetArchiveDp(ExecuteParamRec &param);
	int executeGetDrivers(ExecuteParamRec &param);
	int executeResetUpdateCounter(ExecuteParamRec &param);
	int executeIncrementUpdateCounter(ExecuteParamRec &param);
	int executePendingDpeCount(ExecuteParamRec &param);
	int executeSetDipConfigChange(ExecuteParamRec &param);

	int fillDpData(DpData *pDpData, DynVar *dpInfo, DynVar *dpeInfo);
	DpeData *findDpe(const char *dpName, const char *dpeName, ExecuteParamRec &param,
		DynVar *exceptionInfo);
};

#endif	// _VACCTLIMPORTPVSS_H_
