
//*********************************************************************************
// *************** List of valve DP names
global dyn_string valveDps;

global dyn_bool pendingDataChange;

// Current test step for every valve
global dyn_int valveTestStep;

// Current sub-test step for every valve
global dyn_int valveTestSubStep;

// Flag indicating if test for valve is completed
global dyn_bool valveTestCompleted;

// How many times action was executed for current step (if any)
global dyn_int valveTestStepRetryCount;

// Maxumum number of times action can be executed for current step
global dyn_int valveTextStepMaxRetry;

// Time when timeout will expire for current step
global dyn_time stepTimeoutTime;

// Name of function, processing current step
global dyn_string stepProcessFunction;

// Test state for every valve
global dyn_int valveTestResult;

// Possible values for every valveTestResult
const int TEST_NOT_STARTED = 0,
  TEST_IN_PROGRESS = 1,
  TEST_OK = 2,
  TEST_FAILURE = 3,
  TEST_CANCELLED = 4,
  TEST_PAUSE = 5;

// History of test for every valve: description of what's happening, result, errors etc.
global dyn_dyn_string valveTestHistory;

// Timestamp for every message in valveTestHistory
global dyn_dyn_time valveTestHistoryTs;

// Flag for every message in history: see below possible values
global dyn_dyn_int valveTestHistorySeverity;

// History message severity
const int
  HISTORY_INFO = 0,
  HISTORY_WARN = 1,
  HISTORY_ERROR = 2;


// Data to be queried from LASER log DB after test is finished
global dyn_dyn_string  laserEqpNames;  // DP names of equipments with LASER alarms
global dyn_dyn_string  laserStateNames;  // Textual descritpion of LASER alarm
global dyn_dyn_string  laserAlarmIds;  // Alarm IDs for corresponding alarms
global dyn_dyn_time    laserPvssTimes;  // Timestamps from PVSS
global dyn_dyn_time    laserLogTimes;  // Timestamps from LASER log DB


// List of valve DP states together with timestamps
global dyn_dyn_anytype valveStates;

//  Meaning of fields in valveStates[index]:
const int EQP_STATE = 1;        // Calculated state of device - see constants firther
const int EQP_STATE_TIME = 2;    // Timestamp of device state change
const int EQP_CLOSED_TIME = 3;
const int EQP_START_CLOSING_TIME = 4;  // Time from PLC
const int EQP_INTERLOCK_TIME = 5;  // Time from PLC
const int EQP_LASER_ALARM = 6;  // Bool, ALn flag - bit in RR1
const int EQP_LASER_ALARM_REAL = 7;  // Bool, value of AL1_LASER DPE
const int EQP_TOTAL = 7;  // Total number of fields

// Calculated states for valves
const int VALVE_STATE_NONE = 0,
  VALVE_OPEN = 1,
  VALVE_CLOSED = 2,
  VALVE_UNDEFINED = 3,
  VALVE_INVALID = 4,
  VALVE_ERROR = 5;

// Calculated states for gauges and pumps
const int EQP_STATE_NONE = 0,
  EQP_STATE_ON = 1,
  EQP_STATE_OFF = 2,
  EQP_STATE_UNDEFINED = 3,
  EQP_STATE_INVALID = 4,
  EQP_STATE_ERROR = 5;

// List of all valves, related to BICs of valve being tested
global dyn_string valvesOfBicsDps;
global dyn_dyn_anytype valvesOfBicsStates;

//*********************************************************************************
// *************** List of 'gauge before' DP names
global dyn_string gaugeBeforeDps;

// List of 'gauge before' states together with timestamps, meaning of fields
// is the same as for valves
global dyn_dyn_anytype gaugeBeforeStates;

//*********************************************************************************
// *************** List of 'pump before' DP names
global dyn_string pumpBeforeDps;

//for SPS
global dyn_string pumpBefore1Dps;
global dyn_string pumpBefore2Dps;
global dyn_string pumpBefore3Dps;
global dyn_string pumpBefore4Dps;
global dyn_string pumpBefore5Dps;
global dyn_string pumpBefore6Dps;

// List of 'pump before' states together with timestamps, meaning of fields
// is the same as for valves
global dyn_dyn_anytype pumpBeforeStates;

//for SPS
global dyn_dyn_anytype pumpBefore1States;
global dyn_dyn_anytype pumpBefore2States;
global dyn_dyn_anytype pumpBefore3States;
global dyn_dyn_anytype pumpBefore4States;
global dyn_dyn_anytype pumpBefore5States;
global dyn_dyn_anytype pumpBefore6States;

//*********************************************************************************
// *************** List of 'gauge after' DP names
global dyn_string gaugeAfterDps;

// List of 'gauge after' states together with timestamps, meaning of fields
// is the same as for valves
global dyn_dyn_anytype gaugeAfterStates;

//*********************************************************************************
// *************** List of 'pump after' DP names
global dyn_string pumpAfterDps;

//for SPS
global dyn_string pumpAfter1Dps;
global dyn_string pumpAfter2Dps;
global dyn_string pumpAfter3Dps;
global dyn_string pumpAfter4Dps;
global dyn_string pumpAfter5Dps;
global dyn_string pumpAfter6Dps;
// List of 'pump after' states together with timestamps, meaning of fields
// is the same as for valves
global dyn_dyn_anytype pumpAfterStates;

// for SPS
global dyn_dyn_anytype pumpAfter1States;
global dyn_dyn_anytype pumpAfter2States;
global dyn_dyn_anytype pumpAfter3States;
global dyn_dyn_anytype pumpAfter4States;
global dyn_dyn_anytype pumpAfter5States;
global dyn_dyn_anytype pumpAfter6States;

//*********************************************************************************
// *************** List of 'gauge #3' DP names
global dyn_string gaugeThreeDps;

// List of 'gauge #3' states together with timestamps, meaning of fields
// is the same as for valves
global dyn_dyn_anytype gaugeThreeStates;

//*********************************************************************************
// *************** List of 'gauge #4' DP names
global dyn_string gaugeFourDps;

// List of 'gauge #4' states together with timestamps, meaning of fields
// is the same as for valves
global dyn_dyn_anytype gaugeFourStates;

//*********************************************************************************
// *************** List of 'related valves' DP names
global dyn_dyn_string relatedValvesDps;

//SPS style calculation for next valve
global dyn_string nextValveDps;
global dyn_dyn_anytype nextValveStates;

// List of 'related valves' states together with timestamps, meaning of fields
// is the same as for valves
global dyn_dyn_anytype relatedValvesStates;

//*********************************************************************************
//*************** List of first BIC DP names
global dyn_string firstBicDps;

// List of channel numbers of this valve in 1st BIC
global dyn_dyn_int firstBicChannels;

// List of first BIC states together with timestamps
global dyn_dyn_anytype firstBicAStates;
global dyn_dyn_anytype firstBicBStates;
global dyn_dyn_anytype firstBicAStates2;
global dyn_dyn_anytype firstBicBStates2;

//*********************************************************************************
// *************** List of second BIC DP names
global dyn_string secondBicDps;

// List of channel numbers of this valve in 2nd BIC
global dyn_dyn_int secondBicChannels;

// List of first BIC states together with timestamps
global dyn_dyn_anytype secondBicAStates;
global dyn_dyn_anytype secondBicBStates;
global dyn_dyn_anytype secondBicAStates2;
global dyn_dyn_anytype secondBicBStates2;

global bool history_buffer;
global bool interlockSent;
global bool skipWaitingForValve;
// Each xxxBicStates contains the following fields
const int BIC_DATA_ARRIVED = 1,  // bool
  BIC_VACUUM_PERMIT = 2,  // bool
  BIC_BEAM_PERMIT = 3,  // bool
  BIC_BEAM_SIMULATION = 4,  // bool
  BIC_BEAM_INFO = 5,  // bool
  BIC_TIME = 6,  // time
  BIC_VACUUM_PERMIT_FALSE_TIME = 7,  // Time
  BIC_BEAM_PERMIT_FALSE_TIME = 8;  // Time

// Generalization of constants for pumps and gauges - bits in state RR1
const unsigned
  DEVICE_R_VALID = 0x40000000u,
  DEVICE_R_ERRORS = 0x00800000u,
  DEVICE_R_OFF = 0x00010000u,
  DEVICE_R_ON = 0x00020000u;

// Check flags
const unsigned
  CHECK_BEAM_PERMIT = 1,
  CHECK_VACUUM_PERMIT = 2,
  CHECK_LASER_ALARMS = 4;
 
  
// Comment for all actions being executed during test
const string actComment = "Valve interlock test";

dyn_string testSteps;

dyn_string testStepNames;

// If test step N fails and corresponding string in this list is not
// empty - go to step AFTER failed one == testExceptions[N]
dyn_string testExceptions;

global bool checkBeamInfo;
global bool checkBeamPermit;
global bool checkVacuumPermit;
global bool checkLaser;


string g_sDriverDp, g_sDevProp, g_sSetException;  // To use unCMWClient_setUpExceptionHandling()

global unsigned expectedInterlocks = 0;
global unsigned expectedInterlocksNext = 0;

// Commands for BIC
const unsigned CMD_BEAM_INFO_FORCED_TRUE = 0xC0000003u;
const unsigned CMD_BEAM_INFO_FORCED_FALSE = 0xC0000002u;
const unsigned CMD_BEAM_INFO_UNFORCED = 0xC0000000u;


// Flag indicating if intialization for test is finished, after that LASER alarms
// are related to our tests
global bool intlTestInitializedCb;

/**********************************************************************************
************************** Build initial content of all lists *********************
**********************************************************************************/
void IntlTestSetValveList(dyn_string valveDpNames, dyn_string &exceptionInfo)
{
  // Clean old content
  dynClear(valveDps);
  dynClear(pendingDataChange);
  dynClear(valveTestStep);
  dynClear(valveTestSubStep);
  dynClear(valveTestCompleted);
  dynClear(valveTestStepRetryCount);
  dynClear(valveTextStepMaxRetry);
  dynClear(stepTimeoutTime);
  dynClear(stepProcessFunction);
  dynClear(valveTestResult);
  dynClear(laserEqpNames);
  dynClear(laserStateNames);
  dynClear(laserAlarmIds);
  dynClear(laserPvssTimes);
  dynClear(laserLogTimes);
  dynClear(valveTestHistory);
  dynClear(valveTestHistoryTs);
  dynClear(valveTestHistorySeverity);
  dynClear(valveStates);
  dynClear(gaugeBeforeDps);
  dynClear(gaugeBeforeStates);
  dynClear(pumpBeforeDps);
  dynClear(pumpBefore1Dps);
  dynClear(pumpBefore2Dps);
  dynClear(pumpBefore3Dps);
  dynClear(pumpBefore4Dps);
  dynClear(pumpBefore5Dps);
  dynClear(pumpBefore6Dps);
  dynClear(pumpBeforeStates);
  dynClear(pumpBefore1States);
  dynClear(pumpBefore2States);
  dynClear(pumpBefore3States);
  dynClear(pumpBefore4States);
  dynClear(pumpBefore5States);
  dynClear(pumpBefore6States);  
  dynClear(gaugeAfterDps);
  dynClear(gaugeAfterStates);
  dynClear(pumpAfterDps);
  dynClear(pumpAfter1Dps);
  dynClear(pumpAfter2Dps);
  dynClear(pumpAfter3Dps);
  dynClear(pumpAfter4Dps);
  dynClear(pumpAfter5Dps);
  dynClear(pumpAfter6Dps);  
  dynClear(pumpAfterStates);
  dynClear(pumpAfter1States);
  dynClear(pumpAfter2States);
  dynClear(pumpAfter3States);
  dynClear(pumpAfter4States);
  dynClear(pumpAfter5States);
  dynClear(pumpAfter6States);
  dynClear(gaugeThreeDps);
  dynClear(gaugeThreeStates);
  dynClear(gaugeFourDps);
  dynClear(gaugeFourStates);
  dynClear(relatedValvesDps);
  dynClear(relatedValvesStates);
  dynClear(firstBicDps);
  dynClear(firstBicChannels);
  dynClear(firstBicAStates);
  dynClear(firstBicBStates);
  dynClear(firstBicAStates2);
  dynClear(firstBicBStates2);
  dynClear(secondBicDps);
  dynClear(secondBicChannels);
  dynClear(secondBicAStates);
  dynClear(secondBicBStates);
  dynClear(secondBicAStates2);
  dynClear(secondBicBStates2);
  history_buffer = true;

  // Build new lists
  int nValves = dynlen(valveDpNames);
  
  for(int n = 1 ; n <= nValves ; n++)
  {
    string dpName = valveDpNames[n];
    dynAppend(valveDps, dpName);

    // Related lists
    dynAppend(pendingDataChange, false);
    dynAppend(valveTestStep, 0);
    dynAppend(valveTestSubStep, 0);
    dynAppend(valveTestCompleted, false);
    dynAppend(valveTestStepRetryCount, 0);
    dynAppend(valveTextStepMaxRetry, 0);
    dynAppend(stepTimeoutTime, makeTime(1970, 01, 01));
    dynAppend(stepProcessFunction, "");
    dynAppend(valveTestResult, TEST_NOT_STARTED);
    laserEqpNames[n] = makeDynString();
    laserStateNames[n] = makeDynString();
    laserAlarmIds[n] = makeDynString();
    laserPvssTimes[n] = makeDynTime();
    laserLogTimes[n] = makeDynTime();
    valveTestHistory[n] = makeDynString();
    valveTestHistoryTs[n] = makeDynTime();
    valveTestHistorySeverity[n] = makeDynInt();
    valveStates[n] = IntlTestMakeEmptyValveState();

    string gaugeBefore = "", gaugeAfter = "", pumpBefore = "", pumpAfter = "", gaugeThree = "", gaugeFour = "";
    IntlTestFindInterlockSources(dpName, gaugeBefore, gaugeAfter, pumpBefore, pumpAfter, gaugeThree, gaugeFour);
	string pumpBefore1 = "", pumpBefore2 = "", pumpBefore3 = "", pumpAfter1 = "", pumpAfter2 = "", pumpAfter3 = "";
	string pumpBefore4 = "", pumpBefore5 = "", pumpBefore6 = "", pumpAfter4 = "", pumpAfter5 = "", pumpAfter6 = "";
	IntlTestFindSPSInterlockSources(dpName, gaugeBefore, gaugeAfter, pumpBefore1, pumpBefore2, pumpBefore3, pumpBefore4, pumpBefore5, pumpBefore6, 
										pumpAfter1, pumpAfter2, pumpAfter3, pumpAfter4, pumpAfter5, pumpAfter6);	
	
	
    dynAppend(gaugeBeforeDps, gaugeBefore);
    gaugeBeforeStates[n] = IntlTestMakeEmptyValveState();
    dynAppend(pumpBeforeDps, pumpBefore);
    pumpBeforeStates[n] = IntlTestMakeEmptyValveState();
    dynAppend(gaugeAfterDps, gaugeAfter);
    gaugeAfterStates[n] = IntlTestMakeEmptyValveState();
    dynAppend(pumpAfterDps, pumpAfter);
    pumpAfterStates[n] = IntlTestMakeEmptyValveState();
    dynAppend(gaugeThreeDps, gaugeThree);
    gaugeThreeStates[n] = IntlTestMakeEmptyValveState();
    dynAppend(gaugeFourDps, gaugeFour);
    gaugeFourStates[n] = IntlTestMakeEmptyValveState();
	
	
	dynAppend(pumpBefore1Dps, pumpBefore1);
	dynAppend(pumpBefore2Dps, pumpBefore2);
	dynAppend(pumpBefore3Dps, pumpBefore3);
	dynAppend(pumpBefore4Dps, pumpBefore4);
	dynAppend(pumpBefore5Dps, pumpBefore5);
	dynAppend(pumpBefore6Dps, pumpBefore6);
	
	pumpBefore1States[n] = IntlTestMakeEmptyValveState();
	pumpBefore2States[n] = IntlTestMakeEmptyValveState();
	pumpBefore3States[n] = IntlTestMakeEmptyValveState();
	pumpBefore4States[n] = IntlTestMakeEmptyValveState();
	pumpBefore5States[n] = IntlTestMakeEmptyValveState();
	pumpBefore6States[n] = IntlTestMakeEmptyValveState();
	
	dynAppend(pumpAfter1Dps, pumpAfter1);
	dynAppend(pumpAfter2Dps, pumpAfter2);
	dynAppend(pumpAfter3Dps, pumpAfter3);
	dynAppend(pumpAfter4Dps, pumpAfter4);
	dynAppend(pumpAfter5Dps, pumpAfter5);
	dynAppend(pumpAfter6Dps, pumpAfter6);
	
	pumpAfter1States[n] = IntlTestMakeEmptyValveState();
	pumpAfter2States[n] = IntlTestMakeEmptyValveState();
	pumpAfter3States[n] = IntlTestMakeEmptyValveState();
	pumpAfter4States[n] = IntlTestMakeEmptyValveState();
	pumpAfter5States[n] = IntlTestMakeEmptyValveState();
	pumpAfter6States[n] = IntlTestMakeEmptyValveState();
	
    relatedValvesDps[n] = makeDynString();
    dyn_string intlFor = makeDynString(dpName);
    dyn_string queue = makeDynString(dpName);
    string attr;
    bool first = true;
    while(dynlen(queue) > 0)
    {
      string dp = queue[1];
      dynRemove(queue, 1);
      
      if(first)
      {
        LhcVacGetAttribute(dp, "PRInterlockFor1", attr);
        if((attr != "") && !dynContains(intlFor, attr))
        {
          if(!dynContains(relatedValvesDps[n], attr))
          {
            dynAppend(relatedValvesDps[n], attr);
            dynAppend(relatedValvesStates[n], IntlTestMakeEmptyValveState());
          }
          dynAppend(intlFor, attr);
          dynAppend(queue, attr);
        }
        LhcVacGetAttribute(dp, "PRInterlockFor2", attr);
        if((attr != "") && !dynContains(intlFor, attr))
        {
          if(!dynContains(relatedValvesDps[n], attr))
          {
            dynAppend(relatedValvesDps[n], attr);
            dynAppend(relatedValvesStates[n], IntlTestMakeEmptyValveState());
          }
          dynAppend(intlFor, attr);
          dynAppend(queue, attr);
        }
        first = false;
      }
      LhcVacGetAttribute(dp, "ValveBefore1", attr);
      if((attr != "") && !dynContains(relatedValvesDps[n], attr))
      {
        dynAppend(relatedValvesDps[n], attr);
        dynAppend(relatedValvesStates[n], IntlTestMakeEmptyValveState());
      }
      LhcVacGetAttribute(dp, "ValveBefore2", attr);
      if((attr != "") && !dynContains(relatedValvesDps[n], attr))
      {
        dynAppend(relatedValvesDps[n], attr);
        dynAppend(relatedValvesStates[n], IntlTestMakeEmptyValveState());
      }
      LhcVacGetAttribute(dp, "ValveAfter1", attr);
      if((attr != "") && !dynContains(relatedValvesDps[n], attr))
      {
        dynAppend(relatedValvesDps[n], attr);
        dynAppend(relatedValvesStates[n], IntlTestMakeEmptyValveState());
      }
      LhcVacGetAttribute(dp, "ValveAfter2", attr);
      if((attr != "") && !dynContains(relatedValvesDps[n], attr))
      {
        dynAppend(relatedValvesDps[n], attr);
        dynAppend(relatedValvesStates[n], IntlTestMakeEmptyValveState());
      }
    }
    
    int vacType;
    string sector1, sector2, mainPart;
    bool isBorder;
    dyn_uint values;
    LhcVacDeviceVacLocation(dpName, vacType, sector1, sector2, mainPart, isBorder, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return;
    }
    LhcVacGetAttribute(dpName, "BIC1", attr);
    if(attr == "")
    {
      LhcVacGetAttribute(dpName, "BIC", attr);
    }
    dynAppend(firstBicDps, attr);
    firstBicChannels[n] = IntlTestGetBicChannels(attr, vacType);
    firstBicAStates[n] = IntlTestMakeEmptyBicState();
    firstBicBStates[n] = IntlTestMakeEmptyBicState();
    firstBicAStates2[n] = IntlTestMakeEmptyBicState();
    firstBicBStates2[n] = IntlTestMakeEmptyBicState();
//     if(attr != "" && dpGet(attr + ".BoardRegisters", values) == 0)
//     {
//       IntlTestCbBicBoardRegistersProcess(n, firstBicChannels[n], firstBicAStates[n], firstBicBStates[n], values);
//     }

    LhcVacGetAttribute(dpName, "BIC2", attr);
    dynAppend(secondBicDps, attr);
    secondBicChannels[n] = IntlTestGetBicChannels(attr, vacType);
    secondBicAStates[n] = IntlTestMakeEmptyBicState();
    secondBicBStates[n] = IntlTestMakeEmptyBicState();
    secondBicAStates2[n] = IntlTestMakeEmptyBicState();
    secondBicBStates2[n] = IntlTestMakeEmptyBicState();
//     if(attr != "" && dpGet(attr + ".BoardRegisters", values) == 0)
//     {
//       IntlTestCbBicBoardRegistersProcess(n, secondBicChannels[n], secondBicAStates[n], secondBicBStates[n], values);
//     }
    IntlTestUpdateBic(n);
  }
}

private void IntlTestFindInterlockSources(string dpName, string &gaugeBefore, string &gaugeAfter, string &pumpBefore, string &pumpAfter,
                                          string &gaugeThree, string &gaugeFour)
{
  string attr, attrName, realDp;

  // Old-style attributes
  LhcVacGetAttribute(dpName, "GaugeBefore", attr);
  gaugeBefore = IntlTestCheckDpExist(attr);

  LhcVacGetAttribute(dpName, "GaugeAfter", attr);
  gaugeAfter = IntlTestCheckDpExist(attr);

  LhcVacGetAttribute(dpName, "PumpBefore", attr);
  pumpBefore = IntlTestCheckDpExist(attr);

  LhcVacGetAttribute(dpName, "PumpAfter", attr);
  pumpAfter = IntlTestCheckDpExist(attr);

  if((gaugeBefore != "") && (gaugeAfter != "") && (pumpBefore != "") && (pumpAfter != ""))
  {
    return;  // Got all interlock sources
  }

  // New-style attributes
  for(int idx = 1 ; idx <= 4 ; idx++)
  {
    // Previous interlock sources
    attrName = "PrevInterlockSource" + idx;
    LhcVacGetAttribute(dpName, attrName, attr);
    realDp = IntlTestCheckDpExist(attr);
    if(realDp != "")
    {
      if(idx == 3)
      {
        if(gaugeThree == "")
        {
          gaugeThree = realDp;
        }
      }
      else if(idx == 4)
      {
        if(gaugeFour == "")
        {
          gaugeFour = realDp;
        }
      }
      else
      {
        int family = 0, type = 0, subType = 0;
        LhcVacGetCtlAttributes(realDp, family, type, subType);
        if(family == 202)  // VRPI
        {
          if(pumpBefore == "")
          {
            pumpBefore = realDp;
          }
        }
        else  // Gauge
        {
          if(gaugeBefore == "")
          {
            gaugeBefore = realDp;
          }
        }
      }
    }

    // Next interlock sources
    attrName = "NextInterlockSource" + idx;
    LhcVacGetAttribute(dpName, attrName, attr);
    realDp = IntlTestCheckDpExist(attr);
    if(realDp != "")
    {
      if(idx == 3)
      {
        if(gaugeThree == "")
        {
          gaugeThree = realDp;
        }
      }
      else if(idx == 4)
      {
        if(gaugeFour == "")
        {
          gaugeFour = realDp;
        }
      }
      else
      {
        int family = 0, type = 0, subType = 0;
        LhcVacGetCtlAttributes(realDp, family, type, subType);
        if(family == 202)  // VRPI
        {
          if(pumpAfter == "")
          {
            pumpAfter = realDp;
          }
        }
        else  // Gauge
        {
          if(gaugeAfter == "")
          {
            gaugeAfter = realDp;
          }
        }
      }
    }
  }
}

// only to be called after valvesOfBics is filled
private void findNextValveBySectors(string dpName, string &nextValveName)
{
	string attr, attrName ="Sector2";
	string sectorAttrValue1, sectorAttrValue2;
	int valveCount;
	  int vac;
  string sector1, sector2, mainPart;
  bool isBorder;
  dyn_string exceptionInfo;
  LhcVacDeviceVacLocation(dpName, vac, sector1, sector2, mainPart, isBorder, exceptionInfo);
	
	if(sector2 != "")
	{
		sectorAttrValue1 = sector2;
		valveCount = dynlen(valvesOfBicsDps);
		for(int i = 1; i<=valveCount;i++)
		{
			 LhcVacDeviceVacLocation(valvesOfBicsDps[i], vac, sector1, sector2, mainPart, isBorder, exceptionInfo);

			if(sectorAttrValue1 == sector1)
			{
				nextValveName = valvesOfBicsDps[i];
				return;
			}
		}
	}
	
}
private void IntlTestFindSPSInterlockSources(string dpName, string &gaugeBefore, string &gaugeAfter, string &pumpBefore1, string &pumpBefore2, string &pumpBefore3, string &pumpBefore4, string &pumpBefore5, string &pumpBefore6,
												string &pumpAfter1, string &pumpAfter2, string &pumpAfter3, string &pumpAfter4, string &pumpAfter5, string &pumpAfter6)
{
	string attr, attrName;
	dyn_string dpsBefore, dpsAfter;
	for(int i= 1; i<=6;i++)
	{
		attrName = "PrevInterlockSource" + i;
		LhcVacGetAttribute(dpName, attrName, attr);
		dynAppend(dpsBefore, IntlTestCheckDpExist(attr));
		
		attrName = "NextInterlockSource" + i;
		LhcVacGetAttribute(dpName, attrName, attr);
		dynAppend(dpsAfter, IntlTestCheckDpExist(attr));
		
	}
	
	pumpBefore1 = dpsBefore[1];
	pumpBefore2 = dpsBefore[2];
	pumpBefore3 = dpsBefore[3];
	pumpBefore4 = dpsBefore[4];
	pumpBefore5 = dpsBefore[5];
	pumpBefore6 = dpsBefore[6];
	
	pumpAfter1 = dpsAfter[1];
	pumpAfter2 = dpsAfter[2];
	pumpAfter3 = dpsAfter[3];
	pumpAfter4 = dpsAfter[4];
	pumpAfter5 = dpsAfter[5];
	pumpAfter6 = dpsAfter[6];
	
	  // Old-style attributes
	LhcVacGetAttribute(dpName, "GaugeBefore", attr);
	if(IntlTestCheckDpExist(attr) != "")
		gaugeBefore = IntlTestCheckDpExist(attr);

	LhcVacGetAttribute(dpName, "GaugeAfter", attr);
	if(IntlTestCheckDpExist(attr) != "")
		gaugeAfter = IntlTestCheckDpExist(attr);
}
/**********************************************************************************
************************** Start test procedure ***********************************
**********************************************************************************/
void IntlTestStart(int valveIdx, bool stepByStep, unsigned checkFlags, int testNumber, dyn_string &exceptionInfo)
{
  IntlTestBuildValvesOfBics(valveIdx, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  
  if(dynlen(relatedValvesDps[valveIdx]) == 0)
  {
	  string nextValve="";
	  findNextValveBySectors(valveDps[valveIdx], nextValve);
	  if(nextValve != "")
	  {
		  dynAppend(relatedValvesDps[valveIdx], nextValve);
		  dynAppend(relatedValvesStates[valveIdx], IntlTestMakeEmptyValveState());
		  DebugTN(nextValve, " IS NEXT VALVE");
	  }
		
  }
  
  skipWaitingForValve = false;
  checkBeamInfo     = false;
  checkBeamPermit   = (checkFlags & CHECK_BEAM_PERMIT) != 0;
  checkVacuumPermit = (checkFlags & CHECK_VACUUM_PERMIT) != 0;
  checkLaser        = (checkFlags & CHECK_LASER_ALARMS) != 0;
  switch(testNumber)
  {
  case 1:
    IntlTestSetProgram1();
    break;
  case 2:
    IntlTestSetProgram2();
    break;
  case 3:
	IntlTestSetProgramSPS();
	break;
  case 4:
	IntlTestSetProgramSPS2();
	break;
  default:
    fwException_raise(exceptionInfo, "ERROR", "IntlTestStart(): unknown test # " + testNumber, "");
    return;
  }

// DebugTN("IntlTestStart(" + valveIdx + ")", testSteps);
  // Check if index is correct
  if((valveIdx <= 0) || (valveIdx > dynlen(valveDps)))
  {
    fwException_raise(exceptionInfo, "ERROR", "IntlTestStart(): invalid valve index " + valveIdx +
                      ", must be in range 1..." + dynlen(valveDps), "");
    return;
  }

  // Check if test is already running on this (or another) valve
  for(int n = dynlen(valveDps) ; n > 0 ; n--)
  {
    if(valveTestResult[n] == TEST_IN_PROGRESS)
    {
      fwException_raise(exceptionInfo, "ERROR", "IntlTestStart(): test is in progress for valve " + n +
                      " (" + IntlTestGetEqpName(valveDps[n]) + ")", "");
      return;
    }
  }
  dynClear(valveTestHistory[valveIdx]);
  dynClear(valveTestHistoryTs[valveIdx]);
  dynClear(valveTestHistorySeverity[valveIdx]);
  dynClear(laserEqpNames[valveIdx]);
  dynClear(laserStateNames[valveIdx]);
  dynClear(laserAlarmIds[valveIdx]);
  dynClear(laserPvssTimes[valveIdx]);
  dynClear(laserLogTimes[valveIdx]);
  intlTestInitializedCb = false;

  valveTestCompleted[valveIdx] = false;
  valveTestResult[valveIdx] = TEST_IN_PROGRESS;
  valveTestStep[valveIdx] = 0;
  IntlTestAddInfo(valveIdx, IntlTestGetEqpName(valveDps[valveIdx]) + ": Test start");
  startThread("IntlTestMainLoop", valveIdx, stepByStep);
}

void IntlTestMainLoop(int valveIdx, bool stepByStep)
{
DebugTN("IntlTestMainLoop(" + valveIdx + ")", testSteps);

  // Infinite cycle - while test in in progress
  int stepStatus;
  valveTestStep[valveIdx] = 1;
  stepProcessFunction[valveIdx] = "";
  valveTestResult[valveIdx] = TEST_IN_PROGRESS;
  while((valveTestResult[valveIdx] == TEST_IN_PROGRESS) || (valveTestResult[valveIdx] == TEST_PAUSE))
  {
    if(valveTestResult[valveIdx] == TEST_IN_PROGRESS)
    {
      if((stepStatus = IntlStepExecuteStep(valveIdx, testSteps)) > 0)
      {
        if(stepStatus != 2 && stepByStep)
        {
          string msg = testStepNames[valveTestStep[valveIdx]];
          dyn_float fReturn;
          dyn_string strReturn;
          ChildPanelOnModalReturn ("/vision/interlockTest/testPrompter.pnl", "Continue with test", 
            makeDynString("$text1:"+ msg), 0, 0, fReturn, strReturn);
          if(dynlen(fReturn) == 0)
          {
            IntlTestAddInfo(valveIdx, "TEST CANCELLED BY OPERATOR");
            valveTestResult[valveIdx] = TEST_CANCELLED;
            break;
          }
        }
        if(valveTestStep[valveIdx] == dynlen(testSteps))
        {
          if(valveTestResult[valveIdx] == TEST_IN_PROGRESS)
          {
            valveTestResult[valveIdx] = TEST_OK;
          }
          break;
        }
        valveTestStep[valveIdx]++;
      }
    }
    delay(0, 100);
  }
  //DebugTN("history", valveTestHistory);
  //IntlTestStepCleanUpValve(valveIdx, stepByStep);
  expectedInterlocks = 0;
  expectedInterlocksNext = 0;
  // Clean up all flags on status availability
  valveStates[valveIdx][EQP_STATE] = VALVE_STATE_NONE;
  gaugeBeforeStates[valveIdx][EQP_STATE] = EQP_STATE_NONE;
  pumpBeforeStates[valveIdx][EQP_STATE] = EQP_STATE_NONE;
  gaugeAfterStates[valveIdx][EQP_STATE] = EQP_STATE_NONE;
  pumpAfterStates[valveIdx][EQP_STATE] = EQP_STATE_NONE;
  gaugeThreeStates[valveIdx][EQP_STATE] = EQP_STATE_NONE;
  gaugeFourStates[valveIdx][EQP_STATE] = EQP_STATE_NONE;
  for(int n = dynlen(relatedValvesDps[valveIdx]) ; n > 0 ; n--)
  {
    relatedValvesStates[valveIdx][(n-1)*EQP_TOTAL + EQP_STATE] = VALVE_STATE_NONE;
  }
  firstBicAStates[valveIdx][BIC_DATA_ARRIVED] = false;
  firstBicBStates[valveIdx][BIC_DATA_ARRIVED] = false;
  secondBicAStates[valveIdx][BIC_DATA_ARRIVED] = false;
  secondBicBStates[valveIdx][BIC_DATA_ARRIVED] = false;
  firstBicAStates2[valveIdx][BIC_DATA_ARRIVED] = false;
  firstBicBStates2[valveIdx][BIC_DATA_ARRIVED] = false;
  secondBicAStates2[valveIdx][BIC_DATA_ARRIVED] = false;
  secondBicBStates2[valveIdx][BIC_DATA_ARRIVED] = false;
  IntlTestAddInfo(valveIdx,
     "========= Testing " + IntlTestGetEqpName(valveDps[valveIdx]) + " completed, result is " +
     + IntlTestResultName(valveTestResult[valveIdx]));
  valveTestCompleted[valveIdx] = true;
}

int IntlStepExecuteStep(int valveIdx, dyn_string testSteps)
{
  if(stepProcessFunction[valveIdx] != testSteps[valveTestStep[valveIdx]])
  {
    IntlTestExecuteFunction(testSteps[valveTestStep[valveIdx]], valveIdx);
  }
  stepProcessFunction[valveIdx] = testSteps[valveTestStep[valveIdx]];
  return IntlTestExecuteFunction(stepProcessFunction[valveIdx], valveIdx);
}

int IntlTestExecuteFunction(string funcName, int valveIdx)
{
  int coco;
  evalScript(coco, "int main(int valveIdx, string funcName) " +
              "{ " +
              "return " + funcName + "(valveIdx, funcName); " +
              "} ",
              makeDynString(),
              valveIdx, funcName);
  return coco;
}

/*************************************************************************************
**** Initial step: wait until all devices involved got 1st data ***********************
*************************************************************************************/
int IntlTestStepInitial(int valveIdx, string funcName)
{
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
DebugTN("IntlTestStepInitial(" + valveIdx + ")");

    valveTestStepRetryCount[valveIdx] = 0;
    stepTimeoutTime[valveIdx] = getCurrentTime() + 5;
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  if(valveStates[valveIdx][EQP_STATE] == VALVE_STATE_NONE)
  {
    dynAppend(errMsg, "    " + IntlTestGetEqpName(valveDps[valveIdx]));
  }
  if((gaugeBeforeDps[valveIdx] != "") && (gaugeBeforeStates[valveIdx][EQP_STATE] == EQP_STATE_NONE))
  {
    dynAppend(errMsg, "    " + IntlTestGetEqpName(gaugeBeforeDps[valveIdx]));
  }
  if((pumpBeforeDps[valveIdx] != "") && (pumpBeforeStates[valveIdx][EQP_STATE] == EQP_STATE_NONE))
  {
    dynAppend(errMsg, "    " + IntlTestGetEqpName(pumpBeforeDps[valveIdx]));
  }
  if((gaugeAfterDps[valveIdx] != "") && (gaugeAfterStates[valveIdx][EQP_STATE] == EQP_STATE_NONE))
  {
    dynAppend(errMsg, "    " + IntlTestGetEqpName(gaugeAfterDps[valveIdx]));
  }
  if((pumpAfterDps[valveIdx] != "") && (pumpAfterStates[valveIdx][EQP_STATE] == EQP_STATE_NONE))
  {
    dynAppend(errMsg, "    " + IntlTestGetEqpName(pumpAfterDps[valveIdx]));
  }
  if((gaugeThreeDps[valveIdx] != "") && (gaugeThreeStates[valveIdx][EQP_STATE] == EQP_STATE_NONE))
  {
    dynAppend(errMsg, "    " + IntlTestGetEqpName(gaugeThreeDps[valveIdx]));
  }
  if((gaugeFourDps[valveIdx] != "") && (gaugeFourStates[valveIdx][EQP_STATE] == EQP_STATE_NONE))
  {
    dynAppend(errMsg, "    " + IntlTestGetEqpName(gaugeFourDps[valveIdx]));
  }
  for(int n = dynlen(relatedValvesDps[valveIdx]) ; n > 0 ; n--)
  {
    if(relatedValvesStates[valveIdx][(n-1)*EQP_TOTAL + EQP_STATE] == VALVE_STATE_NONE)
    {
      dynAppend(errMsg, "    " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]));
    }
  }
  if(checkVacuumPermit)
  {
    if((firstBicDps[valveIdx] != "") && (!firstBicAStates2[valveIdx][BIC_DATA_ARRIVED] || !firstBicBStates2[valveIdx][BIC_DATA_ARRIVED]))
    {
      dynAppend(errMsg, "    " + IntlTestGetEqpName(firstBicDps[valveIdx]));
    }
    if((secondBicDps[valveIdx] != "") && (!secondBicAStates2[valveIdx][BIC_DATA_ARRIVED] || !secondBicBStates2[valveIdx][BIC_DATA_ARRIVED]))
    {
      dynAppend(errMsg, "    " + IntlTestGetEqpName(secondBicDps[valveIdx]));
    }
  }

  // If not success - check if timeout expired
  if(dynlen(errMsg) > 0)
  {
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    dynAppend(errMsg, IntlTestGetEqpName(valveDps[valveIdx]) + ": Initial step FAILED - no callback data");
    IntlTestStopOnException(valveIdx, errMsg);
DebugTN("IntlTestStepInitial(" + valveIdx + ") = FAILED");
    return 0;
  }

DebugTN("IntlTestStepInitial(" + valveIdx + ") = OK");
  // Success - next step possible
  intlTestInitializedCb = true;
  return 1;
}

/*************************************************************************************
**** Step: set Beam info signal to FALSE on all affected BICs ************************
*************************************************************************************/
int IntlTestStepBeamInfoToFalse(int valveIdx, string funcName)
{
  if(!checkBeamInfo)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
DebugTN("IntlTestStepBeamInfoToFalse(" + valveIdx + ")");
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Start step set BeamInfo to FALSE");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  if((firstBicDps[valveIdx] != "") && (firstBicAStates[valveIdx][BIC_BEAM_INFO] || firstBicBStates[valveIdx][BIC_BEAM_INFO]))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(firstBicDps[valveIdx]) + ": BeamInfo is TRUE");
  }
  if((secondBicDps[valveIdx] != "") && (secondBicAStates[valveIdx][BIC_BEAM_INFO] || secondBicBStates[valveIdx][BIC_BEAM_INFO]))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(secondBicDps[valveIdx]) + ": BeamInfo is TRUE");
  }

  if(dynlen(errMsg) > 0)
  {
    if(valveTestStepRetryCount[valveIdx] == 0)
    {
      IntlTestWriteBicCommand(valveIdx, CMD_BEAM_INFO_FORCED_FALSE);
      valveTestStepRetryCount[valveIdx]++;
      return 0;
    }

    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
    {
      IntlTestWriteBicCommand(valveIdx, CMD_BEAM_INFO_FORCED_FALSE);
      valveTestStepRetryCount[valveIdx]++;
      stepTimeoutTime[valveIdx] = now + 10;
      return 0;
    }
    dynAppend(errMsg, IntlTestGetEqpName(valveDps[valveIdx]) +
                   ": NOT POSSIBLE to set BEAM INFO FALSE ");
    IntlTestStopOnException(valveIdx, errMsg);
DebugTN("IntlTestStepFinishedBeamInfoToFalse(" + valveIdx + ") = FAILED");
    return 0;
  }
DebugTN("IntlTestStepFinishedBeamInfoToFalse(" + valveIdx + ") = OK");

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Open the valve and related valves ***********************************************
*************************************************************************************/

int IntlTestStepValveOpenRelated(int valveIdx, string funcName)
{
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Start step OPEN the valve and related valves");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 300;
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  if(valveStates[valveIdx][EQP_STATE] != VALVE_OPEN)
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(valveDps[valveIdx]) + " IS NOT OPEN");
  }
  for(int n = dynlen(relatedValvesDps[valveIdx]) ; n > 0 ; n--)
  {
    if(relatedValvesStates[valveIdx][(n-1)*EQP_TOTAL + EQP_STATE] != VALVE_OPEN)
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) + " IS NOT OPEN");
    }
  }
  
  if(dynlen(errMsg) > 0)
  {
    if(valveTestStepRetryCount[valveIdx] == 0)
    {
      IntlTestSwitchValve(valveIdx, valveDps[valveIdx], LVA_DEVICE_OPEN, valveStates[valveIdx][EQP_STATE]);
      for(int n = dynlen(relatedValvesDps[valveIdx]) ; n > 0 ; n--)
      {
        IntlTestSwitchValve(valveIdx, relatedValvesDps[valveIdx][n], LVA_DEVICE_OPEN, relatedValvesStates[valveIdx][(n-1)*EQP_TOTAL + EQP_STATE]);
      }
      valveTestStepRetryCount[valveIdx]++;
      return 0;
    }
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
    {
      IntlTestSwitchValve(valveIdx, valveDps[valveIdx], LVA_DEVICE_OPEN, valveStates[valveIdx][EQP_STATE]);
      for(int n = dynlen(relatedValvesDps[valveIdx]) ; n > 0 ; n--)
      {
        IntlTestSwitchValve(valveIdx, relatedValvesDps[valveIdx][n], LVA_DEVICE_OPEN, relatedValvesStates[valveIdx][(n-1)*EQP_TOTAL + EQP_STATE]);
      }
      valveTestStepRetryCount[valveIdx]++;
      stepTimeoutTime[valveIdx] = now + 300;
      return 0;
    }
    dynAppend(errMsg, "UNABLE TO OPEN VACUUM VALVES");
    for(int i = 1; i <= dynlen(errMsg); i++)
    {
      IntlTestAddWarn(valveIdx, errMsg[i]);
    }
    //IntlTestStopOnException(valveIdx, errMsg);
    return 1;
  }

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Open all valves in affected BIC(s) **********************************************
*************************************************************************************/

int IntlTestStepValveOpenAllOnBics(int valveIdx, string funcName)
{
  if(!checkVacuumPermit)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Start step OPEN all valves of affected BICs");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  for(int n = dynlen(valvesOfBicsDps) ; n > 0 ; n--)
  {
    if(valvesOfBicsStates[n][EQP_STATE] != VALVE_OPEN)
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(valvesOfBicsDps[n]) + " IS NOT OPEN");
    }
  }
  
  if(dynlen(errMsg) > 0)
  {
    if(valveTestStepRetryCount[valveIdx] == 0)
    {
      for(int n = dynlen(valvesOfBicsDps) ; n > 0 ; n--)
      {
        IntlTestSwitchValve(valveIdx, valvesOfBicsDps[n], LVA_DEVICE_OPEN, valvesOfBicsStates[n][EQP_STATE]);
      }
      valveTestStepRetryCount[valveIdx]++;
      return 0;
    }
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
    {
      for(int n = dynlen(valvesOfBicsDps) ; n > 0 ; n--)
      {
        IntlTestSwitchValve(valveIdx, valvesOfBicsDps[n], LVA_DEVICE_OPEN, valvesOfBicsStates[n][EQP_STATE]);
      }
      valveTestStepRetryCount[valveIdx]++;
      stepTimeoutTime[valveIdx] = now + 10;
      return 0;
    }
    dynAppend(errMsg, "UNABLE TO OPEN VACUUM VALVES");
    for(int i = 1; i <= dynlen(errMsg); i++)
    {
      IntlTestAddWarn(valveIdx, errMsg[i]);
    }
    //IntlTestStopOnException(valveIdx, errMsg);
    return 1;
  }

  // Success - next step possible
  return 1;
}
/*************************************************************************************
**** Open all valves in affected BIC(s) **********************************************
*************************************************************************************/

int IntlTestStepValveCloseAllOnBics(int valveIdx, string funcName)
{

  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Start step CLOSE all valves of affected BICs");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 300;
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  for(int n = dynlen(valvesOfBicsDps) ; n > 0 ; n--)
  {
    if(valvesOfBicsStates[n][EQP_STATE] != VALVE_CLOSED)
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(valvesOfBicsDps[n]) + " IS NOT CLOSED");
    }
  }
  
  if(dynlen(errMsg) > 0)
  {
    if(valveTestStepRetryCount[valveIdx] == 0)
    {
      for(int n = dynlen(valvesOfBicsDps) ; n > 0 ; n--)
      {
        IntlTestSwitchValve(valveIdx, valvesOfBicsDps[n], LVA_DEVICE_CLOSE, valvesOfBicsStates[n][EQP_STATE]);
      }
      valveTestStepRetryCount[valveIdx]++;
      return 0;
    }
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
    {
      for(int n = dynlen(valvesOfBicsDps) ; n > 0 ; n--)
      {
        IntlTestSwitchValve(valveIdx, valvesOfBicsDps[n], LVA_DEVICE_CLOSE, valvesOfBicsStates[n][EQP_STATE]);
      }
      valveTestStepRetryCount[valveIdx]++;
      stepTimeoutTime[valveIdx] = now + 300;
      return 0;
    }
    dynAppend(errMsg, "UNABLE TO CLOSE VACUUM VALVES");
    for(int i = 1; i <= dynlen(errMsg); i++)
    {
      IntlTestAddWarn(valveIdx, errMsg[i]);
    }
    //IntlTestStopOnException(valveIdx, errMsg);
    return 1;
  }

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Close the valve, wait until valve is closed **************************************
*************************************************************************************/

int IntlTestStepValveClose(int valveIdx, string funcName)
{
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Start step CLOSE valve");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 300;
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  if(valveStates[valveIdx][EQP_STATE] != VALVE_CLOSED)
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(valveDps[valveIdx]) + " IS NOT CLOSED");
  }
  if(dynlen(errMsg) > 0)
  {
//DebugTN("IntlTestStepValveOpenAllOnBics(" + valveIdx + "): " + errMsg + ", count = " + valveTestStepRetryCount[valveIdx]);
    if(valveTestStepRetryCount[valveIdx] == 0)
    {
      IntlTestSwitchValve(valveIdx, valveDps[valveIdx], LVA_DEVICE_CLOSE, valveStates[valveIdx][EQP_STATE]);
      valveTestStepRetryCount[valveIdx]++;
      return 0;
    }
    time now = getCurrentTime();
//DebugTN(now, stepTimeoutTime[valveIdx]);
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
    {
      IntlTestSwitchValve(valveIdx, valveDps[valveIdx], LVA_DEVICE_CLOSE, valveStates[valveIdx][EQP_STATE]);
      valveTestStepRetryCount[valveIdx]++;
      stepTimeoutTime[valveIdx] = now + 300;
      return 0;
    }
    IntlTestStopOnException(valveIdx, errMsg);
    return 0;
  }

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Open the valve, wait until valve is opened **************************************
*************************************************************************************/

int IntlTestStepValveOpen(int valveIdx, string funcName)
{
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Open the valve");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 300;
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  if(valveStates[valveIdx][EQP_STATE] != VALVE_OPEN)
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(valveDps[valveIdx]) + " IS NOT OPEN");
  }
  if(dynlen(errMsg) > 0)
  {
//DebugTN("IntlTestStepValveOpenAllOnBics(" + valveIdx + "): " + errMsg + ", count = " + valveTestStepRetryCount[valveIdx]);
    if(valveTestStepRetryCount[valveIdx] == 0)
    {
      IntlTestSwitchValve(valveIdx, valveDps[valveIdx], LVA_DEVICE_OPEN, valveStates[valveIdx][EQP_STATE]);
      valveTestStepRetryCount[valveIdx]++;
      return 0;
    }
    time now = getCurrentTime();
//DebugTN(now, stepTimeoutTime[valveIdx]);
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
    {
      IntlTestSwitchValve(valveIdx, valveDps[valveIdx], LVA_DEVICE_OPEN, valveStates[valveIdx][EQP_STATE]);
      valveTestStepRetryCount[valveIdx]++;
      stepTimeoutTime[valveIdx] = now + 300;
      return 0;
    }
    IntlTestStopOnException(valveIdx, errMsg);
    return 0;
  }

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Close the valve, but valve must remain OPEN *************************************
*************************************************************************************/

int IntlTestStepValveCloseMustStayOpen(int valveIdx, string funcName)
{
  if(!checkBeamInfo)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Start step CLOSE valve must remain OPEN");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }

  // Send command to CLOSE - anyway, only once
  if(valveTestStepRetryCount[valveIdx] == 0)
  {
    IntlTestSwitchValve(valveIdx, valveDps[valveIdx], LVA_DEVICE_CLOSE, valveStates[valveIdx][EQP_STATE]);
    valveTestStepRetryCount[valveIdx]++;
    return 0;
  }

  // Wait for 10 sec - ANYWAY
  time now = getCurrentTime();
  if(now <= stepTimeoutTime[valveIdx])
  {
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  if(valveStates[valveIdx][EQP_STATE] != VALVE_OPEN)
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(valveDps[valveIdx]) + " IS NOT OPEN");
  }
  if(dynlen(errMsg) > 0)
  {
    dynAppend(errMsg, "OPERATOR CLOSURE OF VALVE " + IntlTestGetEqpName(valveDps[valveIdx]) +
             " WHEN BEAM INFO = TRUE");
    IntlTestStopOnException(valveIdx, errMsg);
    return 0;
  }

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Open the valve, but valve must remain CLOSED *************************************
*************************************************************************************/

int IntlTestStepOpenValveMustStayClosed(int valveIdx, string funcName)
{
    if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Start step OPEN valve must remain CLOSED");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }
  
  // Send command to OPEN - anyway, only once
  if(valveTestStepRetryCount[valveIdx] == 0)
  {
    
	
	IntlTestSwitchValve(valveIdx, valveDps[valveIdx], LVA_DEVICE_OPEN, valveStates[valveIdx][EQP_STATE]);
    valveTestStepRetryCount[valveIdx]++;
    return 0;
  }

  // Wait for 10 sec - ANYWAY
  time now = getCurrentTime();
  if(now <= stepTimeoutTime[valveIdx])
  {
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  if(valveStates[valveIdx][EQP_STATE] != VALVE_CLOSED)
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(valveDps[valveIdx]) + " IS NOT CLOSED");
  }
  if(dynlen(errMsg) > 0)
  {
    dynAppend(errMsg, "OPERATOR OPENING OF VALVE " + IntlTestGetEqpName(valveDps[valveIdx]) +
             " WHEN INTERLOCK SHOULD BE ACTIVE");
    IntlTestStopOnException(valveIdx, errMsg);
    return 0;
  }

  // Success - next step possible
  return 1;
}


/*************************************************************************************
**** Wait for vacuum permit after valve opening **************************************
*************************************************************************************/

int IntlTestWaitVacPermitTrue(int valveIdx, string funcName)
{
  if(!checkVacuumPermit)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 0;  // Does not matter
    IntlTestAddInfo(valveIdx, "Start step wait VacPermit = TRUE");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }

  // Check execution
  IntlTestUpdateBic(valveIdx);
  dyn_string errMsg, warnMsg;
  history_buffer = true;
//DebugTN("BICs:", firstBicDps[valveIdx], secondBicDps[valveIdx]);
  if(firstBicDps[valveIdx] != "")
  {
    if(!firstBicAStates[valveIdx][BIC_VACUUM_PERMIT] || !firstBicBStates[valveIdx][BIC_VACUUM_PERMIT])
    {
      if(!firstBicAStates2[valveIdx][BIC_VACUUM_PERMIT] || !firstBicBStates2[valveIdx][BIC_VACUUM_PERMIT])
      {
        dynAppend(errMsg, "   " + IntlTestGetEqpName(firstBicDps[valveIdx]) + " VacuumPermit = FALSE");
      }
      else
      {
        dynAppend(warnMsg, "   " + IntlTestGetEqpName(firstBicDps[valveIdx]) + " No transition VacuumPermit F->T in HistoryBuffer");
        history_buffer = false;
      }
    }
  }
  if(secondBicDps[valveIdx] != "")
  {
    if(!secondBicAStates[valveIdx][BIC_VACUUM_PERMIT] || !secondBicBStates[valveIdx][BIC_VACUUM_PERMIT])
    {
      if(!secondBicAStates2[valveIdx][BIC_VACUUM_PERMIT] || !secondBicBStates2[valveIdx][BIC_VACUUM_PERMIT])
      {
        dynAppend(errMsg, "   " + IntlTestGetEqpName(secondBicDps[valveIdx]) + " VacuumPermit = FALSE");
      }
      else
      {
        dynAppend(warnMsg, "   " + IntlTestGetEqpName(secondBicDps[valveIdx]) + " No transition VacuumPermit F->T in HistoryBuffer");
        history_buffer = false;
      }
    }
  }
  if(dynlen(errMsg) > 0)
  {
//DebugTN("IntlTestWaitVacPermitTrue(" + valveIdx + "): " + errMsg + ", count = " + valveTestStepRetryCount[valveIdx]);
    time now = getCurrentTime();
    if(now > stepTimeoutTime[valveIdx])
    {
      // Timeout
      dynAppend(errMsg, "VACUUM USER PERMIT NOT TRUE WITH VACUUM VALVES OPEN");
      IntlTestStopOnException(valveIdx, errMsg);
    }
    return 0;
  }
  if(dynlen(warnMsg) > 0)
  {
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    for(int i = 1; i <= dynlen(warnMsg); i++)
    {
      IntlTestAddWarn(valveIdx, warnMsg[i]);
    }
  }

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Wait for vacuum permit after valve closing **************************************
*************************************************************************************/

int IntlTestWaitVacPermitFalse(int valveIdx, string funcName)
{
  if(!checkVacuumPermit)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 0;  // Does not matter
    IntlTestAddInfo(valveIdx, "Start step wait VacPermit = FALSE");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }

  // Check execution
  IntlTestUpdateBic(valveIdx);
  dyn_string errMsg, warnMsg;
  history_buffer = true;
//DebugTN("BICs:", firstBicDps[valveIdx], secondBicDps[valveIdx]);
  if(firstBicDps[valveIdx] != "")
  {
    if(firstBicAStates[valveIdx][BIC_VACUUM_PERMIT] || firstBicBStates[valveIdx][BIC_VACUUM_PERMIT])
    {
      if(firstBicAStates2[valveIdx][BIC_VACUUM_PERMIT] || firstBicBStates2[valveIdx][BIC_VACUUM_PERMIT])
      {
        dynAppend(errMsg, "   " + IntlTestGetEqpName(firstBicDps[valveIdx]) + " VacuumPermit = TRUE");
      }
      else
      {
        dynAppend(warnMsg, "   " + IntlTestGetEqpName(firstBicDps[valveIdx]) + " No transition VacuumPermit T->F in HistoryBuffer");
        history_buffer = false;
      }
    }
  }
  if(secondBicDps[valveIdx] != "")
  {
    if(secondBicAStates[valveIdx][BIC_VACUUM_PERMIT] || secondBicBStates[valveIdx][BIC_VACUUM_PERMIT])
    {
      if(secondBicAStates2[valveIdx][BIC_VACUUM_PERMIT] || secondBicBStates2[valveIdx][BIC_VACUUM_PERMIT])
      {
        dynAppend(errMsg, "   " + IntlTestGetEqpName(secondBicDps[valveIdx]) + " VacuumPermit = TRUE");
      }
      else
      {
        dynAppend(warnMsg, "   " + IntlTestGetEqpName(secondBicDps[valveIdx]) + " No transition VacuumPermit T->F in HistoryBuffer");
        history_buffer = false;
      }
    }
  }
  if(dynlen(errMsg) > 0)
  {
//DebugTN("IntlTestWaitVacPermitTrue(" + valveIdx + "): " + errMsg + ", count = " + valveTestStepRetryCount[valveIdx]);
    time now = getCurrentTime();
    if(now > stepTimeoutTime[valveIdx])
    {
      // Timeout
      dynAppend(errMsg, "NO VACUUM INTERLOCK for VACUUM VALVE " + IntlTestGetEqpName(valveDps[valveIdx]));
      IntlTestStopOnException(valveIdx, errMsg);
    }
    return 0;
  }
  if(dynlen(warnMsg) > 0)
  {
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    for(int i = 1; i <= dynlen(warnMsg); i++)
    {
      IntlTestAddWarn(valveIdx, warnMsg[i]);
    }
  }

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Unforce BEAM INFO ***************************************************************
*************************************************************************************/

int IntlTestUnforceBeamInfo(int valveIdx, string funcName)
{
  if(!checkBeamInfo)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Start step Unforce Beam Info");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }

  // Check execution
  dyn_string errMsg;//DebugTN("BICs:", firstBicDps[valveIdx], secondBicDps[valveIdx]);
  if((firstBicDps[valveIdx] != "") && (firstBicAStates[valveIdx][BIC_BEAM_SIMULATION] || firstBicBStates[valveIdx][BIC_BEAM_SIMULATION]))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(firstBicDps[valveIdx]) + " Beam Simulation = TRUE");
  }
  if((secondBicDps[valveIdx] != "") && (secondBicAStates[valveIdx][BIC_BEAM_SIMULATION] || secondBicBStates[valveIdx][BIC_BEAM_SIMULATION]))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(secondBicDps[valveIdx]) + " Beam Simulation = TRUE");
  }
  if(dynlen(errMsg) > 0)
  {
    if(valveTestStepRetryCount[valveIdx] == 0)
    {
      IntlTestWriteBicCommand(valveIdx, CMD_BEAM_INFO_UNFORCED);
      valveTestStepRetryCount[valveIdx]++;
      return 0;
    }
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
    {
      IntlTestWriteBicCommand(valveIdx, CMD_BEAM_INFO_UNFORCED);
      valveTestStepRetryCount[valveIdx]++;
      stepTimeoutTime[valveIdx] = now + 10;
      return 0;
    }
    // Timeout
    dynAppend(errMsg, "UNABLE TO UNFORCE BEAM INFO");
    IntlTestStopOnException(valveIdx, errMsg);
    return 0;
  }

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Force BEAM INFO = TRUE **********************************************************
*************************************************************************************/

int IntlTestForceBeamInfoTrue(int valveIdx, string funcName)
{
  if(!checkBeamInfo)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Start step Force Beam Info = TRUE");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  if((firstBicDps[valveIdx] != "") && (!firstBicAStates[valveIdx][BIC_BEAM_INFO] || !firstBicBStates[valveIdx][BIC_BEAM_INFO]))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(firstBicDps[valveIdx]) + " Beam Info = FALSE");
  }
  if((secondBicDps[valveIdx] != "") && (!secondBicAStates[valveIdx][BIC_BEAM_INFO] || !secondBicBStates[valveIdx][BIC_BEAM_INFO]))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(secondBicDps[valveIdx]) + " Beam Info = FALSE");
  }
  if(dynlen(errMsg) > 0)
  {
    if(valveTestStepRetryCount[valveIdx] == 0)
    {
      IntlTestWriteBicCommand(valveIdx, CMD_BEAM_INFO_FORCED_TRUE);
      valveTestStepRetryCount[valveIdx]++;
      return 0;
    }
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
    {
      IntlTestWriteBicCommand(valveIdx, CMD_BEAM_INFO_FORCED_TRUE);
      valveTestStepRetryCount[valveIdx]++;
      stepTimeoutTime[valveIdx] = now + 10;
      return 0;
    }
    // Timeout
    dynAppend(errMsg, "NOT POSSIBLE to set BEAM INFO TRUE");
    IntlTestStopOnException(valveIdx, errMsg);
    return 0;
  }

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Switch OFF devices and check PR Sources *****************************************
*************************************************************************************/

int IntlTestDevicesOffPRSources(int valveIdx, string funcName)
{
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
DebugTN("IntlTestDevicesOffPRSources() - start");
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    valveTestSubStep[valveIdx] = 0;
    IntlTestAddInfo(valveIdx, "Switch OFF interlocking devices and check PR Sources");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }

  
  // Check execution
  dyn_string errMsg;
  string dp = "";
  int state;
  bool isGauge = false;
  int totalSources = IntlTestTotalSources(valveIdx, true);
  int src = 0;
  int srcStep = valveTestSubStep[valveIdx] / 2;
  bool deviceOff = (valveTestSubStep[valveIdx] - srcStep * 2) == 0;
  if(dp == "" && gaugeBeforeDps[valveIdx] != "")
  {
    if(src == srcStep)
    {
      dp = gaugeBeforeDps[valveIdx];
      state = gaugeBeforeStates[valveIdx][EQP_STATE];
      isGauge = true;
      if(deviceOff)
      {
        if(gaugeBeforeStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeBeforeDps[valveIdx]) + " is not OFF");
        }
        if(checkLaser && !gaugeBeforeStates[valveIdx][EQP_LASER_ALARM_REAL])
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeBeforeDps[valveIdx]) + " no LASER alarm");
        }
      }
      else
      {
        if(gaugeBeforeStates[valveIdx][EQP_STATE] != EQP_STATE_ON)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeBeforeDps[valveIdx]) + " is not ON");
        }
      }
    }
    src++;
  }
  if(dp == "" && gaugeAfterDps[valveIdx] != "")
  {
    if(src == srcStep)
    {
      dp = gaugeAfterDps[valveIdx];
      state = gaugeAfterStates[valveIdx][EQP_STATE];
      isGauge = true;
      if(deviceOff)
      {
        if(gaugeAfterStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeAfterDps[valveIdx]) + " is not OFF");
        }
        if(checkLaser && !gaugeAfterStates[valveIdx][EQP_LASER_ALARM_REAL])
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeAfterDps[valveIdx]) + " no LASER alarm");
        }
      }
      else
      {
        if(gaugeAfterStates[valveIdx][EQP_STATE] != EQP_STATE_ON)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeAfterDps[valveIdx]) + " is not ON");
        }
      }
    }
    src++;
  }
  if(dp == "" && pumpBeforeDps[valveIdx] != "")
  {
    if(src == srcStep)
    {
      dp = pumpBeforeDps[valveIdx];
      state = pumpBeforeStates[valveIdx][EQP_STATE];
      if(deviceOff)
      {
        if(pumpBeforeStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpBeforeDps[valveIdx]) + " is not OFF");
        }
        if(checkLaser && !pumpBeforeStates[valveIdx][EQP_LASER_ALARM_REAL])
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpBeforeDps[valveIdx]) + " no LASER alarm");
        }
      }
      else
      {
        if(pumpBeforeStates[valveIdx][EQP_STATE] != EQP_STATE_ON)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpBeforeDps[valveIdx]) + " is not ON");
        }
      }
    }
    src++;
  }
  if(dp == "" && pumpAfterDps[valveIdx] != "")
  {
    if(src == srcStep)
    {
      dp = pumpAfterDps[valveIdx];
      state = pumpAfterStates[valveIdx][EQP_STATE];
      if(deviceOff)
      {
        if(pumpAfterStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpAfterDps[valveIdx]) + " is not OFF");
        }
        if(checkLaser && !pumpAfterStates[valveIdx][EQP_LASER_ALARM_REAL])
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpAfterDps[valveIdx]) + " no LASER alarm");
        }
      }
      else
      {
        if(pumpAfterStates[valveIdx][EQP_STATE] != EQP_STATE_ON)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpAfterDps[valveIdx]) + " is not ON");
        }
      }
    }
    src++;
  }
  if(dp == "" && gaugeThreeDps[valveIdx] != "")
  {
    if(src == srcStep)
    {
      dp = gaugeThreeDps[valveIdx];
      state = gaugeThreeStates[valveIdx][EQP_STATE];
      isGauge = true;
      if(deviceOff)
      {
        if(gaugeThreeStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeThreeDps[valveIdx]) + " is not OFF");
        }
        if(checkLaser && !gaugeThreeStates[valveIdx][EQP_LASER_ALARM_REAL])
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeThreeDps[valveIdx]) + " no LASER alarm");
        }
      }
      else
      {
        if(gaugeThreeStates[valveIdx][EQP_STATE] != EQP_STATE_ON)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeThreeDps[valveIdx]) + " is not ON");
        }
      }
    }
    src++;
  }
  if(dp == "" && gaugeFourDps[valveIdx] != "")
  {
    if(src == srcStep)
    {
      dp = gaugeFourDps[valveIdx];
      state = gaugeFourStates[valveIdx][EQP_STATE];
      isGauge = true;
      if(deviceOff)
      {
        if(gaugeFourStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeFourDps[valveIdx]) + " is not OFF");
        }
        if(checkLaser && !gaugeFourStates[valveIdx][EQP_LASER_ALARM_REAL])
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeFourDps[valveIdx]) + " no LASER alarm");
        }
      }
      else
      {
        if(gaugeFourStates[valveIdx][EQP_STATE] != EQP_STATE_ON)
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeFourDps[valveIdx]) + " is not ON");
        }
      }
    }
    src++;
  }
  //DebugTN("IntlTestDevicesOffPRSources", valveTestSubStep[valveIdx], srcStep, deviceOff, totalSources, errMsg);
  if(dynlen(errMsg) > 0)
  {
    if(valveTestStepRetryCount[valveIdx] == 0)
    {
      if(deviceOff)
      {
        IntlTestSwitchDevice(valveIdx, dp, LVA_DEVICE_OFF, state);
      }
      else
      {
        IntlTestSwitchDevice(valveIdx, dp, isGauge ? LVA_DEVICE_ON_AUTO : LVA_DEVICE_ON, state);
      }
      valveTestStepRetryCount[valveIdx]++;
      return 0;
    }
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
    {
      if(deviceOff)
      {
        IntlTestSwitchDevice(valveIdx, dp, LVA_DEVICE_OFF, state);
      }
      else
      {
        IntlTestSwitchDevice(valveIdx, dp, isGauge ? LVA_DEVICE_ON_AUTO : LVA_DEVICE_ON, state);
      }
      valveTestStepRetryCount[valveIdx]++;
      stepTimeoutTime[valveIdx] = now + 10;
      return 0;
    }
    // Timeout
    if(deviceOff)
    {
      dynAppend(errMsg, "UNABLE TO SWITCH OFF DEVICE" + errMsg);
    }
    else
    {
      dynAppend(errMsg, "UNABLE TO SWITCH ON DEVICE" + errMsg);
    }
    IntlTestStopOnException(valveIdx, errMsg);
    return 0;
  }
  else
  {
    if(deviceOff)
    {
      if(IntlTestPRSources(valveIdx))
      {
        time now = getCurrentTime();
        if(now > stepTimeoutTime[valveIdx])
        {
          dynAppend(errMsg, "PR SOURCES OF VALVE " + IntlTestGetEqpName(valveDps[valveIdx]) + " NOT OFF WHEN DEVICE " + IntlTestGetEqpName(dp) + " IS OFF");
          IntlTestStopOnException(valveIdx, errMsg);
        }
        return 0;
      }
    }
    else
    {
      if(!IntlTestPRSources(valveIdx))
      {
        time now = getCurrentTime();
        if(now > stepTimeoutTime[valveIdx])
        {
          dynAppend(errMsg, "PR SOURCES OF VALVE " + IntlTestGetEqpName(valveDps[valveIdx]) + " NOT ON WHEN DEVICE " + IntlTestGetEqpName(dp) + " IS ON");
          IntlTestStopOnException(valveIdx, errMsg);
        }
        return 0;
      }
    }
    if(valveTestSubStep[valveIdx] < totalSources * 2 - 1)
    {
      valveTestStepRetryCount[valveIdx] = 0;
      valveTestSubStep[valveIdx]++;
      stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
      return 0;
    }
  }

  // Success - next step possible
  return 1;
}

bool IntlTestPRSources(int valveIdx)
{
  unsigned rr1;
  dpGet(valveDps[valveIdx] + ".RR1", rr1);
  if((rr1 & SVCU_SPS_R_OPEN_ENABLE) != 0)
  {
    return true;
  }
  return false;
}

/*************************************************************************************
**** Switch OFF 'previous' gauge *******************************************
*************************************************************************************/
int IntlTestStepTurnOffGaugePrevious(int valveIdx, string funcName)
{
	if(stepProcessFunction[valveIdx] != funcName)  // First call
	{
		DebugTN("IntlTestStepTurnOffGaugePrevious() - start");
		valveTestStepRetryCount[valveIdx] = 0;
		valveTextStepMaxRetry[valveIdx] = 1;
		valveTestSubStep[valveIdx] = 0;
		IntlTestAddInfo(valveIdx, "Switch OFF previous gauge");
		stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
		return 0;
	}
	
	dyn_string errMsg, dp, state;
	if(gaugeBeforeDps[valveIdx] != "")
	{
		if(gaugeBeforeStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
        {
          dp = gaugeBeforeDps[valveIdx];
		  state = gaugeBeforeStates[valveIdx][EQP_STATE];
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeBeforeDps[valveIdx]) + " is not OFF");
        }
        if(checkLaser && !gaugeBeforeStates[valveIdx][EQP_LASER_ALARM_REAL])
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeBeforeDps[valveIdx]) + " no LASER alarm");
		  dp = gaugeBeforeDps[valveIdx];
		  state = gaugeBeforeStates[valveIdx][EQP_STATE];
        }
	}
	else
	{
		dynAppend(errMsg, "NO GAUGE PREVIOUS AVAILABLE ");
    
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
	}
	
	if(dynlen(errMsg) > 0)
    {
		if(valveTestStepRetryCount[valveIdx] == 0)
		{
		 
			IntlTestSwitchDevice(valveIdx, dp, LVA_DEVICE_OFF, state);
		  
			valveTestStepRetryCount[valveIdx]++;
			return 0;
		}
		time now = getCurrentTime();
		if(now <= stepTimeoutTime[valveIdx])
		{
		  return 0;
		}
		// Timeout
		if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
		{
		 
			IntlTestSwitchDevice(valveIdx, dp, LVA_DEVICE_OFF, state);
		  
		    valveTestStepRetryCount[valveIdx]++;
		    stepTimeoutTime[valveIdx] = now + 10;
		    return 0;
		}
    // Timeout
   
		dynAppend(errMsg, "UNABLE TO SWITCH OFF DEVICE" + errMsg);
    
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
    }
    
	expectedInterlocks = SVCU_SPS_R_CIR_DP_BEFORE;
	return 1;
}

/*************************************************************************************
**** Switch ON 'previous' gauge *******************************************
*************************************************************************************/
int IntlTestStepTurnOnGaugePrevious(int valveIdx, string funcName)
{
	if(stepProcessFunction[valveIdx] != funcName)  // First call
	{
		DebugTN("IntlTestStepTurnOnGaugePrevious() - start");
		valveTestStepRetryCount[valveIdx] = 0;
		valveTextStepMaxRetry[valveIdx] = 1;
		valveTestSubStep[valveIdx] = 0;
		IntlTestAddInfo(valveIdx, "Switch ON previous gauge");
		stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
		return 0;
	}
	
	dyn_string errMsg, dp, state;
	if(gaugeBeforeDps[valveIdx] != "")
	{
		if(gaugeBeforeStates[valveIdx][EQP_STATE] != EQP_STATE_ON)
        {
          dp = gaugeBeforeDps[valveIdx];
		  state = gaugeBeforeStates[valveIdx][EQP_STATE];
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeBeforeDps[valveIdx]) + " is not ON");
        }
	}
	else
	{
		dynAppend(errMsg, "NO GAUGE PREVIOUS AVAILABLE ");
    
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
	}
	
	if(dynlen(errMsg) > 0)
    {
		if(valveTestStepRetryCount[valveIdx] == 0)
		{
		 
			IntlTestSwitchDevice(valveIdx, dp, LVA_DEVICE_ON_AUTO, state);
		  
			valveTestStepRetryCount[valveIdx]++;
			return 0;
		}
		time now = getCurrentTime();
		if(now <= stepTimeoutTime[valveIdx])
		{
		  return 0;
		}
		// Timeout
		if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
		{
		 
			IntlTestSwitchDevice(valveIdx, dp, LVA_DEVICE_ON_AUTO, state);
		  
		    valveTestStepRetryCount[valveIdx]++;
		    stepTimeoutTime[valveIdx] = now + 10;
		    return 0;
		}
    // Timeout
   
		dynAppend(errMsg, "UNABLE TO SWITCH ON DEVICE" + errMsg);
    
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
    }
	
    expectedInterlocks = 0;
	expectedInterlocksNext = 0;
	return 1;
}

/*************************************************************************************
**** Switch OFF 'previous' gauge *******************************************
*************************************************************************************/
int IntlTestStepTurnOffGaugeAfter(int valveIdx, string funcName)
{
	if(stepProcessFunction[valveIdx] != funcName)  // First call
	{
		DebugTN("IntlTestStepTurnOffGaugeAfter() - start");
		valveTestStepRetryCount[valveIdx] = 0;
		valveTextStepMaxRetry[valveIdx] = 1;
		valveTestSubStep[valveIdx] = 0;
		IntlTestAddInfo(valveIdx, "Switch OFF next gauge");
		stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
		return 0;
	}
	
	dyn_string errMsg, dp, state;
	if(gaugeAfterDps[valveIdx] != "")
	{
		if(gaugeAfterStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
        {
          dp = gaugeAfterDps[valveIdx];
		  state = gaugeAfterStates[valveIdx][EQP_STATE];
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeAfterDps[valveIdx]) + " is not OFF");
        }
        if(checkLaser && !gaugeAfterStates[valveIdx][EQP_LASER_ALARM_REAL])
        {
          dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeAfterDps[valveIdx]) + " no LASER alarm");
		  dp = gaugeAfterDps[valveIdx];
		  state = gaugeAfterStates[valveIdx][EQP_STATE];
        }
	}
	else
	{
		dynAppend(errMsg, "NO GAUGE NEXT AVAILABLE ");
    
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
	}
	
	if(dynlen(errMsg) > 0)
    {
		if(valveTestStepRetryCount[valveIdx] == 0)
		{
		 
			IntlTestSwitchDevice(valveIdx, dp, LVA_DEVICE_OFF, state);
		  
			valveTestStepRetryCount[valveIdx]++;
			return 0;
		}
		time now = getCurrentTime();
		if(now <= stepTimeoutTime[valveIdx])
		{
		  return 0;
		}
		// Timeout
		if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
		{
		 
			IntlTestSwitchDevice(valveIdx, dp, LVA_DEVICE_OFF, state);
		  
		    valveTestStepRetryCount[valveIdx]++;
		    stepTimeoutTime[valveIdx] = now + 10;
		    return 0;
		}
    // Timeout
   
		dynAppend(errMsg, "UNABLE TO SWITCH OFF DEVICE" + errMsg);
    
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
    }
    expectedInterlocks = SVCU_SPS_R_CIR_DP_AFTER;
	return 1;
}

/*************************************************************************************
**** Switch ON 'next' gauge *******************************************
*************************************************************************************/
int IntlTestStepTurnOnGaugeAfter(int valveIdx, string funcName)
{
	if(stepProcessFunction[valveIdx] != funcName)  // First call
	{
		DebugTN("IntlTestStepTurnOnGaugeAfter() - start");
		valveTestStepRetryCount[valveIdx] = 0;
		valveTextStepMaxRetry[valveIdx] = 1;
		valveTestSubStep[valveIdx] = 0;
		IntlTestAddInfo(valveIdx, "Switch ON next gauge");
		stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
		return 0;
	}
	
	dyn_string errMsg, dp, state;
	if(gaugeAfterDps[valveIdx] != "")
	{
		if(gaugeAfterStates[valveIdx][EQP_STATE] != EQP_STATE_ON)
        {
          dp = gaugeAfterDps[valveIdx];
		  state = gaugeAfterStates[valveIdx][EQP_STATE];
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeAfterDps[valveIdx]) + " is not ON");
        }
	}
	else
	{
		dynAppend(errMsg, "NO GAUGE NEXT AVAILABLE ");
    
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
	}
	
	if(dynlen(errMsg) > 0)
    {
		if(valveTestStepRetryCount[valveIdx] == 0)
		{
		 
			IntlTestSwitchDevice(valveIdx, dp, LVA_DEVICE_ON_AUTO, state);
		  
			valveTestStepRetryCount[valveIdx]++;
			return 0;
		}
		time now = getCurrentTime();
		if(now <= stepTimeoutTime[valveIdx])
		{
		  return 0;
		}
		// Timeout
		if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
		{
		 
			IntlTestSwitchDevice(valveIdx, dp, LVA_DEVICE_ON_AUTO, state);
		  
		    valveTestStepRetryCount[valveIdx]++;
		    stepTimeoutTime[valveIdx] = now + 10;
		    return 0;
		}
    // Timeout
   
		dynAppend(errMsg, "UNABLE TO SWITCH ON DEVICE" + errMsg);
    
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
    }
	expectedInterlocksNext = 0;
    expectedInterlocks = 0;
	return 1;
}

/*************************************************************************************
**** Switch OFF pumps 1 to 3 in next sector ******************************************
*************************************************************************************/
int IntlTestStepTurnOffFirstThreePumps(int valveIdx, string funcName)
{
	if(stepProcessFunction[valveIdx] != funcName)  // First call
	{
		DebugTN("IntlTestStepTurnOffFirstThreePumps() - start");
		valveTestStepRetryCount[valveIdx] = 0;
		valveTextStepMaxRetry[valveIdx] = 1;
		IntlTestAddInfo(valveIdx, "Switch OFF first three pumps to interlock valve");
		stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
		return 0;
	}
	
	string pumpDp1 = pumpAfter1Dps[valveIdx];
	string pumpDp2 = pumpAfter2Dps[valveIdx];
	string pumpDp3 = pumpAfter3Dps[valveIdx];
	
	dyn_string errMsg;
	if(pumpDp1 != "")
	{
		if(pumpAfter1States[valveIdx][EQP_STATE] != EQP_STATE_OFF)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp1) + " is not OFF");
		}
		if(checkLaser && !pumpAfter1States[valveIdx][EQP_LASER_ALARM_REAL])
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp1) + " no LASER alarm");
		}
	}
	if(pumpDp2 != "")
	{
		if(pumpAfter2States[valveIdx][EQP_STATE] != EQP_STATE_OFF)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp2) + " is not OFF");
		}
		if(checkLaser && !pumpAfter2States[valveIdx][EQP_LASER_ALARM_REAL])
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp2) + " no LASER alarm");
		}
	}
	if(pumpDp3 != "")
	{
		if(pumpAfter3States[valveIdx][EQP_STATE] != EQP_STATE_OFF)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp3) + " is not OFF");
		}
		if(checkLaser && !pumpAfter3States[valveIdx][EQP_LASER_ALARM_REAL])
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp3) + " no LASER alarm");
		}
	}
	
	if(dynlen(errMsg) > 0)
	{
		if(valveTestStepRetryCount[valveIdx] == 0)
		{
		  IntlTestCmdPumpsOff(valveIdx, pumpDp1, pumpDp2, pumpDp3,
										pumpAfter1States[valveIdx][EQP_STATE], pumpAfter2States[valveIdx][EQP_STATE], pumpAfter3States[valveIdx][EQP_STATE]);
		  valveTestStepRetryCount[valveIdx]++;
		  return 0;
		}
		time now = getCurrentTime();
		if(now <= stepTimeoutTime[valveIdx])
		{
		  return 0;
		}
		// Timeout
		if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
		{
		  IntlTestCmdPumpsOff(valveIdx, pumpDp1, pumpDp2, pumpDp3,
								pumpAfter1States[valveIdx][EQP_STATE], pumpAfter2States[valveIdx][EQP_STATE], pumpAfter3States[valveIdx][EQP_STATE]);
		  valveTestStepRetryCount[valveIdx]++;
		  stepTimeoutTime[valveIdx] = now + 10;
		  return 0;
		}
		// Timeout
		dynAppend(errMsg, "UNABLE TO SWITCH OFF DEVICES" + errMsg);
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
	}
	interlockSent = true;
	expectedInterlocks = SVCU_SPS_R_CIR_LCL_AFTER;
	expectedInterlocksNext = SVCU_SPS_R_CIR_LCL_BEFORE;
	return 1;
}

/*************************************************************************************
**** Switch OFF pumps 4 to 6 in next sector ******************************************
*************************************************************************************/
int IntlTestStepTurnOffLastThreePumps(int valveIdx, string funcName)
{
	if(stepProcessFunction[valveIdx] != funcName)  // First call
	{
		DebugTN("IntlTestStepTurnOffLastThreePumps() - start");
		valveTestStepRetryCount[valveIdx] = 0;
		valveTextStepMaxRetry[valveIdx] = 1;
		IntlTestAddInfo(valveIdx, "Switch OFF last three pumps to interlock valve");
		stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
		return 0;
	}
	
	string pumpDp4 = pumpAfter4Dps[valveIdx];
	string pumpDp5 = pumpAfter5Dps[valveIdx];
	string pumpDp6 = pumpAfter6Dps[valveIdx];
	if(pumpDp4 == "" && pumpDp5 == "" && pumpDp6 == "")
	{
		interlockSent = false;
		expectedInterlocks = 0;
		expectedInterlocksNext = 0;
		return 2;
	}
	
	dyn_string errMsg;
	if(pumpDp4 != "")
	{
		if(pumpAfter4States[valveIdx][EQP_STATE] != EQP_STATE_OFF)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp4) + " is not OFF");
		}
		if(checkLaser && !pumpAfter4States[valveIdx][EQP_LASER_ALARM_REAL])
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp4) + " no LASER alarm");
		}
	}
	if(pumpDp5 != "")
	{
		if(pumpAfter5States[valveIdx][EQP_STATE] != EQP_STATE_OFF)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp5) + " is not OFF");
		}
		if(checkLaser && !pumpAfter5States[valveIdx][EQP_LASER_ALARM_REAL])
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp5) + " no LASER alarm");
		}
	}
	if(pumpDp6 != "")
	{
		if(pumpAfter6States[valveIdx][EQP_STATE] != EQP_STATE_OFF)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp6) + " is not OFF");
		}
		if(checkLaser && !pumpAfter6States[valveIdx][EQP_LASER_ALARM_REAL])
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp6) + " no LASER alarm");
		}
	}
	
	if(dynlen(errMsg) > 0)
	{
		if(valveTestStepRetryCount[valveIdx] == 0)
		{
		  IntlTestCmdPumpsOff(valveIdx, pumpDp4, pumpDp5, pumpDp6,
										pumpAfter4States[valveIdx][EQP_STATE], pumpAfter5States[valveIdx][EQP_STATE], pumpAfter6States[valveIdx][EQP_STATE]);
		  valveTestStepRetryCount[valveIdx]++;
		  return 0;
		}
		time now = getCurrentTime();
		if(now <= stepTimeoutTime[valveIdx])
		{
		  return 0;
		}
		// Timeout
		if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
		{
		  IntlTestCmdPumpsOff(valveIdx, pumpDp4, pumpDp5, pumpDp6,
										pumpAfter4States[valveIdx][EQP_STATE], pumpAfter5States[valveIdx][EQP_STATE], pumpAfter6States[valveIdx][EQP_STATE]);
		  valveTestStepRetryCount[valveIdx]++;
		  stepTimeoutTime[valveIdx] = now + 10;
		  return 0;
		}
		// Timeout
		dynAppend(errMsg, "UNABLE TO SWITCH OFF DEVICES" + errMsg);
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
	}
	interlockSent = true;
	expectedInterlocks = SVCU_SPS_R_CIR_LCL_AFTER;
	expectedInterlocksNext = SVCU_SPS_R_CIR_LCL_BEFORE;
	return 1;
}

void IntlTestCmdPumpsOff(int valveIdx, string pumpDp1, string pumpDp2, string pumpDp3, int state1, int state2, int state3)
{
	//IntlTestSwitchDevice(valveIdx, dp, isGauge ? LVA_DEVICE_ON_AUTO : LVA_DEVICE_ON, state);
	if(pumpDp1 != "")
	{
		IntlTestSwitchDevice(valveIdx, pumpDp1, LVA_DEVICE_OFF, state1);
		if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
		{
		  return;
		}
	}
	if(pumpDp2 != "")
	{
		IntlTestSwitchDevice(valveIdx, pumpDp2, LVA_DEVICE_OFF, state2);
		if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
		{
		  return;
		}
	}
	if(pumpDp3 != "")
	{
		IntlTestSwitchDevice(valveIdx, pumpDp3, LVA_DEVICE_OFF, state3);
		if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
		{
		  return;
		}
	}
	
}

/*************************************************************************************
**** Switch ON pumps 1 to 3 in next sector ******************************************
*************************************************************************************/
int IntlTestStepTurnOnFirstThreePumps(int valveIdx, string funcName)
{
	if(stepProcessFunction[valveIdx] != funcName)  // First call
	{
		DebugTN("IntlTestStepTurnOnFirstThreePumps() - start");
		valveTestStepRetryCount[valveIdx] = 0;
		valveTextStepMaxRetry[valveIdx] = 1;
		IntlTestAddInfo(valveIdx, "Switch ON first three pumps to interlock valve");
		stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
		return 0;
	}
	
	string pumpDp1 = pumpAfter1Dps[valveIdx];
	string pumpDp2 = pumpAfter2Dps[valveIdx];
	string pumpDp3 = pumpAfter3Dps[valveIdx];
	
	dyn_string errMsg;
	if(pumpDp1 != "")
	{
		if(pumpAfter1States[valveIdx][EQP_STATE] != EQP_STATE_ON)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp1) + " is not ON");
		}
	}
	if(pumpDp2 != "")
	{
		if(pumpAfter2States[valveIdx][EQP_STATE] != EQP_STATE_ON)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp2) + " is not ON");
		}
	}
	if(pumpDp3 != "")
	{
		if(pumpAfter3States[valveIdx][EQP_STATE] != EQP_STATE_ON)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp3) + " is not ON");
		}
	}
	
	if(dynlen(errMsg) > 0)
	{
		if(valveTestStepRetryCount[valveIdx] == 0)
		{
		  IntlTestCmdPumpsOn(valveIdx, pumpDp1, pumpDp2, pumpDp3,
										pumpAfter1States[valveIdx][EQP_STATE], pumpAfter2States[valveIdx][EQP_STATE], pumpAfter3States[valveIdx][EQP_STATE]);
		  valveTestStepRetryCount[valveIdx]++;
		  return 0;
		}
		time now = getCurrentTime();
		if(now <= stepTimeoutTime[valveIdx])
		{
		  return 0;
		}
		// Timeout
		if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
		{
		  IntlTestCmdPumpsOn(valveIdx, pumpDp1, pumpDp2, pumpDp3,
								pumpAfter1States[valveIdx][EQP_STATE], pumpAfter2States[valveIdx][EQP_STATE], pumpAfter3States[valveIdx][EQP_STATE]);
		  valveTestStepRetryCount[valveIdx]++;
		  stepTimeoutTime[valveIdx] = now + 10;
		  return 0;
		}
		// Timeout
		dynAppend(errMsg, "UNABLE TO SWITCH ON DEVICES" + errMsg);
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
	}
	expectedInterlocks = 0;
	expectedInterlocksNext = 0;
	return 1;
}

/*************************************************************************************
**** Switch ON pumps 4 to 6 in next sector ******************************************
*************************************************************************************/
int IntlTestStepTurnOnLastThreePumps(int valveIdx, string funcName)
{
	if(stepProcessFunction[valveIdx] != funcName)  // First call
	{
		DebugTN("IntlTestStepTurnOnLastThreePumps() - start");
		valveTestStepRetryCount[valveIdx] = 0;
		valveTextStepMaxRetry[valveIdx] = 1;
		IntlTestAddInfo(valveIdx, "Switch ON last three pumps to interlock valve");
		stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
		return 0;
	}
	
	string pumpDp4 = pumpAfter4Dps[valveIdx];
	string pumpDp5 = pumpAfter5Dps[valveIdx];
	string pumpDp6 = pumpAfter6Dps[valveIdx];
	
	dyn_string errMsg;
	if(pumpDp4 != "")
	{
		if(pumpAfter4States[valveIdx][EQP_STATE] != EQP_STATE_ON)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp4) + " is not ON");
		}
	}
	if(pumpDp5 != "")
	{
		if(pumpAfter5States[valveIdx][EQP_STATE] != EQP_STATE_ON)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp5) + " is not ON");
		}
	}
	if(pumpDp6 != "")
	{
		if(pumpAfter6States[valveIdx][EQP_STATE] != EQP_STATE_ON)
		{
		  dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpDp6) + " is not ON");
		}
	}
	
	if(dynlen(errMsg) > 0)
	{
		if(valveTestStepRetryCount[valveIdx] == 0)
		{
		  IntlTestCmdPumpsOn(valveIdx, pumpDp4, pumpDp5, pumpDp6,
										pumpAfter4States[valveIdx][EQP_STATE], pumpAfter5States[valveIdx][EQP_STATE], pumpAfter6States[valveIdx][EQP_STATE]);
		  valveTestStepRetryCount[valveIdx]++;
		  return 0;
		}
		time now = getCurrentTime();
		if(now <= stepTimeoutTime[valveIdx])
		{
		  return 0;
		}
		// Timeout
		if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
		{
		  IntlTestCmdPumpsOn(valveIdx, pumpDp4, pumpDp5, pumpDp6,
										pumpAfter4States[valveIdx][EQP_STATE], pumpAfter5States[valveIdx][EQP_STATE], pumpAfter6States[valveIdx][EQP_STATE]);
		  valveTestStepRetryCount[valveIdx]++;
		  stepTimeoutTime[valveIdx] = now + 10;
		  return 0;
		}
		// Timeout
		dynAppend(errMsg, "UNABLE TO SWITCH ON DEVICES" + errMsg);
		IntlTestStopOnException(valveIdx, errMsg);
		return 0;
	}
	expectedInterlocks = 0;
	expectedInterlocksNext = 0;
	return 1;
}

void IntlTestCmdPumpsOn(int valveIdx, string pumpDp1, string pumpDp2, string pumpDp3, int state1, int state2, int state3)
{
	string eqpType;
	bool isGauge;
	if(pumpDp1 != "")
	{
		eqpType = dpTypeName(pumpDp1);
		if(eqpType == "VGP_T")
			isGauge = 1;
		else
			isGauge = 0;
		
		IntlTestSwitchDevice(valveIdx, pumpDp1, isGauge ? LVA_DEVICE_ON_AUTO : LVA_DEVICE_ON, state1);
		if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
		{
		  return;
		}
	}
	if(pumpDp2 != "")
	{
		eqpType = dpTypeName(pumpDp2);
		if(eqpType == "VGP_T")
			isGauge = 1;
		else
			isGauge = 0;
		
		IntlTestSwitchDevice(valveIdx, pumpDp2, isGauge ? LVA_DEVICE_ON_AUTO : LVA_DEVICE_ON, state2);
		if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
		{
		  return;
		}
	}
	if(pumpDp3 != "")
	{
		eqpType = dpTypeName(pumpDp3);
		if(eqpType == "VGP_T")
			isGauge = 1;
		else
			isGauge = 0;
		
		
		IntlTestSwitchDevice(valveIdx, pumpDp3,  isGauge ? LVA_DEVICE_ON_AUTO : LVA_DEVICE_ON, state3);
		if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
		{
		  return;
		}
	}
	
}

/*************************************************************************************
**** Switch OFF devices to interlock valve *******************************************
*************************************************************************************/

int IntlTestDevicesOffInterlock(int valveIdx, string funcName)
{
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
DebugTN("IntlTestDevicesOffInterlock() - start");
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Switch OFF devices to interlock valve");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  int totalSources = IntlTestTotalSources(valveIdx, false);
  int srcToOff = totalSources - 1;
// DebugTN("IntlTestGaugePumpOff(): total " + totalSources + ", to OFF " + srcToOff);
  if((srcToOff > 0) && (gaugeBeforeDps[valveIdx] != ""))
  {
    if(gaugeBeforeStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeBeforeDps[valveIdx]) + " is not OFF");
    }
    if(checkLaser && !gaugeBeforeStates[valveIdx][EQP_LASER_ALARM_REAL])
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeBeforeDps[valveIdx]) + " no LASER alarm");
    }
    srcToOff--;
  }
  if((srcToOff > 0) && (gaugeAfterDps[valveIdx] != ""))
  {
    if(gaugeAfterStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeAfterDps[valveIdx]) + " is not OFF");
    }
    if(checkLaser && !gaugeAfterStates[valveIdx][EQP_LASER_ALARM_REAL])
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeAfterDps[valveIdx]) + " no LASER alarm");
    }
    srcToOff--;
  }
  if((srcToOff > 0) && (pumpBeforeDps[valveIdx] != ""))
  {
    if(pumpBeforeStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpBeforeDps[valveIdx]) + " is not OFF");
    }
    if(checkLaser && !pumpBeforeStates[valveIdx][EQP_LASER_ALARM_REAL])
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpBeforeDps[valveIdx]) + " no LASER alarm");
    }
    srcToOff--;
  }
  if((srcToOff > 0) && (pumpAfterDps[valveIdx] != ""))
  {
    if(pumpAfterStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpAfterDps[valveIdx]) + " is not OFF");
    }
    if(checkLaser && !pumpAfterStates[valveIdx][EQP_LASER_ALARM_REAL])
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpAfterDps[valveIdx]) + " no LASER alarm");
    }
    srcToOff--;
  }
  if(dynlen(errMsg) > 0)
  {
    if(valveTestStepRetryCount[valveIdx] == 0)
    {
      IntlTestCmdSourcesOff(valveIdx, totalSources);
      valveTestStepRetryCount[valveIdx]++;
      return 0;
    }
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
    {
      IntlTestCmdSourcesOff(valveIdx, totalSources);
      valveTestStepRetryCount[valveIdx]++;
      stepTimeoutTime[valveIdx] = now + 10;
      return 0;
    }
    // Timeout
    dynAppend(errMsg, "UNABLE TO SWITCH OFF DEVICES" + errMsg);
    IntlTestStopOnException(valveIdx, errMsg);
    return 0;
  }

  // Success - next step possible
  interlockSent = true;
  return 1;
}

void IntlTestCmdSourcesOff(int valveIdx, int totalSources)
{
  int srcToOff = totalSources - 1;
  if((srcToOff > 0) && (gaugeBeforeDps[valveIdx] != ""))
  {
    IntlTestSwitchDevice(valveIdx, gaugeBeforeDps[valveIdx], LVA_DEVICE_OFF, gaugeBeforeStates[valveIdx][EQP_STATE]);
    if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
    {
      return;
    }
    srcToOff--;
  }
  if((srcToOff > 0) && (gaugeAfterDps[valveIdx] != ""))
  {
    IntlTestSwitchDevice(valveIdx, gaugeAfterDps[valveIdx], LVA_DEVICE_OFF, gaugeAfterStates[valveIdx][EQP_STATE]);
    if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
    {
      return;
    }
    srcToOff--;
  }
  if((srcToOff > 0) && (pumpBeforeDps[valveIdx] != ""))
  {
    IntlTestSwitchDevice(valveIdx, pumpBeforeDps[valveIdx], LVA_DEVICE_OFF, pumpBeforeStates[valveIdx][EQP_STATE]);
    if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
    {
      return;
    }
    srcToOff--;
  }
  if((srcToOff > 0) && (pumpAfterDps[valveIdx] != ""))
  {
    IntlTestSwitchDevice(valveIdx, pumpAfterDps[valveIdx], LVA_DEVICE_OFF, pumpAfterStates[valveIdx][EQP_STATE]);
    if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
    {
      return;
    }
    srcToOff--;
  }
}

/*************************************************************************************
**** Switch OFF devices #3 and #4 to interlock valve *********************************
*************************************************************************************/

int IntlTestNewDevicesOffInterlock(int valveIdx, string funcName)
{
  if(gaugeThreeDps[valveIdx] == "" || gaugeFourDps[valveIdx] == "")
  {
    interlockSent = false;
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
DebugTN("IntlTestNewDevicesOffInterlock() - start");
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Switch OFF devices #3 and #4 to interlock valve");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  int totalSources = IntlTestTotalSources(valveIdx, false);
  if(gaugeThreeStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeThreeDps[valveIdx]) + " is not OFF");
  }
  if(checkLaser && !gaugeThreeStates[valveIdx][EQP_LASER_ALARM_REAL])
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeThreeDps[valveIdx]) + " no LASER alarm");
  }
  if(gaugeFourStates[valveIdx][EQP_STATE] != EQP_STATE_OFF)
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeFourDps[valveIdx]) + " is not OFF");
  }
  if(checkLaser && !gaugeFourStates[valveIdx][EQP_LASER_ALARM_REAL])
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeFourDps[valveIdx]) + " no LASER alarm");
  }
  if(dynlen(errMsg) > 0)
  {
    if(valveTestStepRetryCount[valveIdx] == 0)
    {
      IntlTestCmdNewSourcesOff(valveIdx);
      valveTestStepRetryCount[valveIdx]++;
      return 0;
    }
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    if(valveTestStepRetryCount[valveIdx] <= valveTextStepMaxRetry[valveIdx])
    {
      IntlTestCmdNewSourcesOff(valveIdx);
      valveTestStepRetryCount[valveIdx]++;
      stepTimeoutTime[valveIdx] = now + 10;
      return 0;
    }
    // Timeout
    dynAppend(errMsg, "UNABLE TO SWITCH OFF DEVICES #3 AND #4" + errMsg);
    IntlTestStopOnException(valveIdx, errMsg);
    return 0;
  }

  // Success - next step possible
  interlockSent = true;
  return 1;
}

void IntlTestCmdNewSourcesOff(int valveIdx)
{
  IntlTestSwitchDevice(valveIdx, gaugeThreeDps[valveIdx], LVA_DEVICE_OFF, gaugeThreeStates[valveIdx][EQP_STATE]);
  if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
  {
    return;
  }
  IntlTestSwitchDevice(valveIdx, gaugeFourDps[valveIdx], LVA_DEVICE_OFF, gaugeFourStates[valveIdx][EQP_STATE]);
  if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)  // Error in action execution
  {
    return;
  }
}

/*************************************************************************************
**** Wait for BEAM PERMIT = FALSE ****************************************************
*************************************************************************************/

int IntlTestWaitForBeamPermitFalse(int valveIdx, string funcName)
{
  if(!checkVacuumPermit || !interlockSent)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
DebugTN("IntlTestWaitForBeamPermitFalse() - start");
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Waiting for VacuumPermit = FALSE");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 10;
    return 0;
  }

  // Check execution
  IntlTestUpdateBic(valveIdx);
  dyn_string errMsg, warnMsg;
  history_buffer = true;
  if(firstBicDps[valveIdx] != "")
  {
    if(firstBicAStates[valveIdx][BIC_VACUUM_PERMIT] || firstBicBStates[valveIdx][BIC_VACUUM_PERMIT])
    {
      if(firstBicAStates2[valveIdx][BIC_VACUUM_PERMIT] || firstBicBStates2[valveIdx][BIC_VACUUM_PERMIT])
      {
        dynAppend(errMsg, "   " + IntlTestGetEqpName(firstBicDps[valveIdx]) + " VacuumPermit is TRUE");
      }
      else
      {
        dynAppend(warnMsg, "   " + IntlTestGetEqpName(firstBicDps[valveIdx]) + " No transition VacuumPermit T->F in HistoryBuffer");
        history_buffer = false;
      }
    }
  }
  if(secondBicDps[valveIdx] != "")
  {
    if(secondBicAStates[valveIdx][BIC_VACUUM_PERMIT] || secondBicBStates[valveIdx][BIC_VACUUM_PERMIT])
    {
      if(secondBicAStates2[valveIdx][BIC_VACUUM_PERMIT] || secondBicAStates2[valveIdx][BIC_VACUUM_PERMIT])
      {
        dynAppend(errMsg, "   " + IntlTestGetEqpName(secondBicDps[valveIdx]) + " VacuumPermit is TRUE");
      }
      else
      {
        dynAppend(warnMsg, "   " + IntlTestGetEqpName(secondBicDps[valveIdx]) + " No transition VacuumPermit T->F in HistoryBuffer");
        history_buffer = false;
      }
    }
  }
  if(dynlen(errMsg) > 0)
  {
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    dynAppend(errMsg, "NO VACUUM INTERLOCK ON VALVE " + IntlTestGetEqpName(valveDps[valveIdx]));
    IntlTestStopOnException(valveIdx, errMsg);
    return 0;
  }
  if(dynlen(warnMsg) > 0)
  {
    time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    for(int i = 1; i <= dynlen(warnMsg); i++)
    {
      IntlTestAddWarn(valveIdx, warnMsg[i]);
    }
  }

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Wait for valve closed ***********************************************************
*************************************************************************************/

int IntlTestWaitForValveClosed(int valveIdx, string funcName)
{
  if(!interlockSent)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
DebugTN("IntlTestWaitForValveClosed() - start");
    valveTestStepRetryCount[valveIdx] = 0;
    valveTextStepMaxRetry[valveIdx] = 1;
    IntlTestAddInfo(valveIdx, "Waiting for valve closed");
    stepTimeoutTime[valveIdx] = getCurrentTime() + 300;
	skipWaitingForValve = false; //user must press button while in this step
    return 0;
  }

  // Check execution
  dyn_string errMsg;
  if(valveStates[valveIdx][EQP_STATE] != VALVE_CLOSED)
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(valveDps[valveIdx]) + " is not CLOSED");
  }
  if(checkLaser && !valveStates[valveIdx][EQP_LASER_ALARM_REAL])
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(valveDps[valveIdx]) + " no LASER alarm");
  }
  
  for(int n = dynlen(relatedValvesDps[valveIdx]) ; n > 0 ; n--)
  {
    if(relatedValvesStates[valveIdx][(n-1)*EQP_TOTAL + EQP_STATE] != VALVE_CLOSED)
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) + " is not CLOSED");
    }
    if(checkLaser && !relatedValvesStates[valveIdx][(n-1)*EQP_TOTAL + EQP_LASER_ALARM_REAL])
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) + " no LASER alarm");
    }
  }
  
  if(dynlen(errMsg) > 0)
  {
    if(skipWaitingForValve)
	{
		IntlTestAddWarn(valveIdx, "USER SKIPPED WAITING FOR VALVE TO CLOSE");
		skipWaitingForValve = false;
		return 1;
	}
	
	time now = getCurrentTime();
    if(now <= stepTimeoutTime[valveIdx])
    {
      return 0;
    }
    // Timeout
    dynAppend(errMsg, "VALVE " + IntlTestGetEqpName(valveDps[valveIdx]) +
             " DOES NOT CLOSE WITH DEVICES OFF");
    IntlTestStopOnException(valveIdx, errMsg);
    return 0;
  }

  // Success - next step possible
  return 1;
}

/*************************************************************************************
**** Compare times of: devices OFF, VVS not open, VVS closed **********************
*************************************************************************************/

int IntlTestCompareReationTimes(int valveIdx, string funcName)
{
  if(!interlockSent)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    IntlTestAddInfo(valveIdx, "Checking event sequence");
    return 0;
  }
DebugTN("IntlTestCompareReationTimes() - start");

  // Check execution
  time t1 = valveStates[valveIdx][EQP_INTERLOCK_TIME];
  time t2 = valveStates[valveIdx][EQP_START_CLOSING_TIME];
  time t3 = valveStates[valveIdx][EQP_CLOSED_TIME];

  string timeStr = t1;
  IntlTestAddInfo(valveIdx, " T1: " + timeStr);
  timeStr = t2;
  IntlTestAddInfo(valveIdx, " T2: " + timeStr);
  timeStr = t3;
  IntlTestAddInfo(valveIdx, " T3: " + timeStr);
  float tmov = floor(((float)t3 - (float)t2) * 1000) / 1000;
  IntlTestAddInfo(valveIdx, "  Moving time of valve " + IntlTestGetEqpName(valveDps[valveIdx]) + ": " + tmov + " s");
  
  if(t1 > t2 || t2 >= t3)
  {
    dyn_string errMsg;
    dynAppend(errMsg, "VACUUM VALVE REACTION NOT CORRECT FOR VALVE " + IntlTestGetEqpName(valveDps[valveIdx]) + ": ");
    IntlTestStopOnException(valveIdx, errMsg);
  }
  
  return 1;
}

int IntlTestLogSPSInterlocks(int valveIdx, string funcName)
{

  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    IntlTestAddInfo(valveIdx, "Log state of valve interlocks");
    return 0;
  }
  
  delay(10);
  dyn_string errMsg;
  unsigned rr2;
  bool noInterlocks = true;
  dpGet(valveDps[valveIdx] + ".RR2", rr2);
  if((rr2 & SVCU_SPS_R_CIR_DP_AFTER) != 0)
  {
    noInterlocks = false;
	IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(valveDps[valveIdx]) +  " current interlock ON: DP after");
  }
  if((rr2 & SVCU_SPS_R_CIR_DP_BEFORE) != 0)
  {
    noInterlocks = false;
	IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(valveDps[valveIdx]) +  " current interlock ON: DP before");
  }
    if((rr2 & SVCU_SPS_R_CIR_GBL_AFTER) != 0)
  {
    noInterlocks = false;
	IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(valveDps[valveIdx]) +  " current interlock ON: Global after");
  }
  if((rr2 & SVCU_SPS_R_CIR_GBL_BEFORE) != 0)
  {
    noInterlocks = false;
	IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(valveDps[valveIdx]) +  " current interlock ON: Global before");
  }
    if((rr2 & SVCU_SPS_R_CIR_LCL_AFTER) != 0)
  {
    noInterlocks = false;
	IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(valveDps[valveIdx]) +  " current interlock ON: Local after");
  }
    if((rr2 & SVCU_SPS_R_CIR_LCL_BEFORE) != 0)
  {
    noInterlocks = false;
	IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(valveDps[valveIdx]) +  " current interlock ON: Local before");
  }
  
  if(noInterlocks)
  {
	IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(valveDps[valveIdx]) +  " current interlocks OFF: no existing interlocks");
  }
  
  if(((expectedInterlocks & (rr2 & 0xFFFF)) == 0) && expectedInterlocks)
  {
	DebugTN(expectedInterlocks, rr2);
    IntlTestAddWarn(valveIdx,IntlTestGetEqpName(valveDps[valveIdx]) + ": interlocks expected but not found!");
	DebugTN("IntlTestLogSPSInterlocks(" + valveIdx + ") = FAILED");   
  }
  
  for(int n = dynlen(relatedValvesDps[valveIdx]) ; n > 0 ; n--)
  {
	  noInterlocks = true; //IntlTestGetEqpName(
	  dpGet(relatedValvesDps[valveIdx][n] + ".RR2", rr2);
	  if((rr2 & SVCU_SPS_R_CIR_DP_AFTER) != 0)
	  {
		noInterlocks = false;
		IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) +  " current interlock ON: DP after");
	  }
	  if((rr2 & SVCU_SPS_R_CIR_DP_BEFORE) != 0)
	  {
		noInterlocks = false;
		IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) +  " current interlock ON: DP before");
	  }
		if((rr2 & SVCU_SPS_R_CIR_GBL_AFTER) != 0)
	  {
		noInterlocks = false;
		IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) +  " current interlock ON: Global after");
	  }
	  if((rr2 & SVCU_SPS_R_CIR_GBL_BEFORE) != 0)
	  {
		noInterlocks = false;
		IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) +  " current interlock ON: Global before");
	  }
		if((rr2 & SVCU_SPS_R_CIR_LCL_AFTER) != 0)
	  {
		noInterlocks = false;
		IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) +  " current interlock ON: Local after");
	  }
		if((rr2 & SVCU_SPS_R_CIR_LCL_BEFORE) != 0)
	  {
		noInterlocks = false;
		IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) +  " current interlock ON: Local before");
	  }
	  
	  if(noInterlocks)
	  {
		IntlTestAddInfo(valveIdx, "Valve " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) +  " current interlocks OFF: no existing interlocks");
	  }
	  
	  if(((expectedInterlocksNext & (rr2 & 0xFFFF)) == 0) && expectedInterlocksNext)
	  {
		DebugTN(expectedInterlocksNext, rr2);
		IntlTestAddWarn(valveIdx,IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) + ": interlocks expected but not found!");
		DebugTN("IntlTestLogSPSInterlocks(" + valveIdx + ") = FAILED");   
	  }
  }
  
  return 1;
}

int IntlTestLogTimes(int valveIdx, string funcName)
{
  if((!checkVacuumPermit && !checkBeamPermit) || !interlockSent)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    IntlTestAddInfo(valveIdx, "Log other useful timestamps");
    return 0;
  }
DebugTN("IntlTestLogTimes() - start");

  bool notime;
  string timeStr;
  time beamPermitFalseTimeA, beamPermitFalseTimeB, defaultTime;
  
  // Vacuum permit FALSE time
  if(checkVacuumPermit)
  {
    notime = true;
    if(firstBicDps[valveIdx] != "")
    {
      if(history_buffer)
      {
        if(notime || beamPermitFalseTimeA < firstBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeA = firstBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
        }
        if(notime || beamPermitFalseTimeB < firstBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeB = firstBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
        }
      }
      else
      {
        if(notime || beamPermitFalseTimeA < firstBicAStates2[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeA = firstBicAStates2[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
        }
        if(notime || beamPermitFalseTimeB < firstBicBStates2[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeB = firstBicBStates2[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
        }
      }
      notime = false;
    }
    if(secondBicDps[valveIdx] != "")
    {
      if(history_buffer)
      {
        if(notime || beamPermitFalseTimeA < secondBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeA = secondBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
        }
        if(notime || beamPermitFalseTimeB < secondBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeB = secondBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
        }
      }
      else
      {
        if(notime || beamPermitFalseTimeA < secondBicAStates2[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeA = secondBicAStates2[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
        }
        if(notime || beamPermitFalseTimeB < secondBicBStates2[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeB = secondBicBStates2[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
        }
      }
      notime = false;
    }
  
    timeStr = beamPermitFalseTimeA;
    IntlTestAddInfo(valveIdx, " TVacA: " + timeStr);
    timeStr = beamPermitFalseTimeB;
    IntlTestAddInfo(valveIdx, " TVacB: " + timeStr);
  }
  
  // Beam permit FALSE time
  if(checkBeamPermit)
  {
    notime = true;
    if(firstBicDps[valveIdx] != "")
    {
      if(history_buffer)
      {
        if(notime || beamPermitFalseTimeA < firstBicAStates[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeA = firstBicAStates[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME];
        }
        if(notime || beamPermitFalseTimeB < firstBicBStates[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeB = firstBicBStates[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME];
        }
      }
      else
      {
        if(notime || beamPermitFalseTimeA < firstBicAStates2[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeA = firstBicAStates2[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME];
        }
        if(notime || beamPermitFalseTimeB < firstBicBStates2[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeB = firstBicBStates2[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME];
        }
      }
      notime = false;
    }
    if(secondBicDps[valveIdx] != "")
    {
      if(history_buffer)
      {
        if(notime || beamPermitFalseTimeA < secondBicAStates[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeA = secondBicAStates[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME];
        }
        if(notime || beamPermitFalseTimeB < secondBicBStates[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeB = secondBicBStates[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME];
        }
      }
      else
      {
        if(notime || beamPermitFalseTimeA < secondBicAStates2[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeA = secondBicAStates2[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME];
        }
        if(notime || beamPermitFalseTimeB < secondBicBStates2[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME])
        {
          beamPermitFalseTimeB = secondBicBStates2[valveIdx][BIC_BEAM_PERMIT_FALSE_TIME];
        }
      }
      notime = false;
    }
    if(beamPermitFalseTimeA == defaultTime || beamPermitFalseTimeB == defaultTime)
    {
      IntlTestAddWarn(valveIdx, "   No transition BeamPermit T->F");
    }
    else
    {
      string timeStr = beamPermitFalseTimeA;
      IntlTestAddInfo(valveIdx, " TBeamA: " + timeStr);
      timeStr = beamPermitFalseTimeB;
      IntlTestAddInfo(valveIdx, " TBeamB: " + timeStr);
    }
  }
  
  return 1;
}

/*************************************************************************************
**** Compare times of: VacuumPermit FALSE, VV closed *********************************
*************************************************************************************/

int IntlTestCompareClosePermitTimes(int valveIdx, string funcName)
{
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    IntlTestAddInfo(valveIdx, "Checking event sequence - valve closed");
    return 0;
  }
DebugTN("IntlTestCompareClosePermitTimes() - start");

  // Check execution

  // Vacuum permit FALSE time
  bool notime = true;
  time beamPermitFalseTime;
  if(firstBicDps[valveIdx] != "")
  {
    if(notime || beamPermitFalseTime < firstBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
    {
      beamPermitFalseTime = firstBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
    }
    if(beamPermitFalseTime < firstBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
    {
      beamPermitFalseTime = firstBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
    }
    notime = false;
  }
  if(secondBicDps[valveIdx] != "")
  {
    if(notime || beamPermitFalseTime < secondBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
    {
      beamPermitFalseTime = secondBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
    }
    if(beamPermitFalseTime < secondBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
    {
      beamPermitFalseTime = secondBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
    }
    notime = false;
  }

  // Valve closed time
  time valveClosedTime = valveStates[valveIdx][EQP_CLOSED_TIME];

  // Comparing...
  if(fabs((float)beamPermitFalseTime - (float)valveClosedTime) < 2.0)
  {
    IntlTestAddBeamClosedSequenceInfo(valveIdx, true, beamPermitFalseTime, valveClosedTime);
    return 1;  // Sequence correct
  }

  IntlTestAddBeamClosedSequenceInfo(valveIdx, false, beamPermitFalseTime, valveClosedTime);
  return 1;
}

void IntlTestAddBeamClosedSequenceInfo(int valveIdx, bool success, time beamPermitFalseTime, time valveClosedTime)
{
  dyn_string msg;
  if(!success)
  {
    dynAppend(msg, "VACUUM VALVE CLOSURE REACTION NOT CORRECT for VACUUM VALVE " + IntlTestGetEqpName(valveDps[valveIdx]) + ": ");
  }
  string timeStr = beamPermitFalseTime;
  dynAppend(msg, " VacuumPermit FALSE time: " + timeStr);
  timeStr = valveClosedTime;
  dynAppend(msg, " Valve not OPEN time:     " + timeStr);
  float diff1 = (float)beamPermitFalseTime - (float)valveClosedTime;
  dynAppend(msg, "  Tvalve " + diff1 + " Tpermit");
  if(success)
  {
    time now = getCurrentTime();
    for(int n = 1 ; n <= dynlen(msg) ; n++)
    {
      IntlTestAddInfo(valveIdx, msg[n], now);
    }
  }
  else
  {
    IntlTestStopOnException(valveIdx, msg);
  }
}

/*************************************************************************************
**** Compare times of: VacuumPermit FALSE, VV not open *******************************
*************************************************************************************/

int IntlTestCompareNotOpenPermitTimes(int valveIdx, string funcName)
{
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    IntlTestAddInfo(valveIdx, "Checking event sequence - valve not open");
    return 0;
  }
DebugTN("IntlTestCompareNotOpenPermitTimes() - start");

  // Check execution

  // Vacuum permit FALSE time
  bool notime = true;
  time beamPermitFalseTime;
  if(firstBicDps[valveIdx] != "")
  {
    if(notime || beamPermitFalseTime < firstBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
    {
      beamPermitFalseTime = firstBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
    }
    if(beamPermitFalseTime < firstBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
    {
      beamPermitFalseTime = firstBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
    }
    notime = false;
  }
  if(secondBicDps[valveIdx] != "")
  {
    if(notime || beamPermitFalseTime < secondBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
    {
      beamPermitFalseTime = secondBicAStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
    }
    if(beamPermitFalseTime < secondBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME])
    {
      beamPermitFalseTime = secondBicBStates[valveIdx][BIC_VACUUM_PERMIT_FALSE_TIME];
    }
    notime = false;
  }

  // Valve not open time
  time valveNotOpenTime = valveStates[valveIdx][EQP_START_CLOSING_TIME];

  // Comparing...
  if(fabs((float)beamPermitFalseTime - (float)valveNotOpenTime) < 2.0)
  {
    IntlTestAddBeamValveSequenceInfo(valveIdx, true, beamPermitFalseTime, valveNotOpenTime);
    return 1;  // Sequence correct
  }

  IntlTestAddBeamNotOpenSequenceInfo(valveIdx, false, beamPermitFalseTime, valveNotOpenTime);
  return 1;
}

void IntlTestAddBeamNotOpenSequenceInfo(int valveIdx, bool success, time beamPermitFalseTime, time valveNotOpenTime)
{
  dyn_string msg;
  if(!success)
  {
    dynAppend(msg, "BEAM INTERLOCK REACTION TIME TOO LONG FOR VACUUM VALVE " + IntlTestGetEqpName(valveDps[valveIdx]) + ": ");
  }
  string timeStr = beamPermitFalseTime;
  dynAppend(msg, " VacuumPermit FALSE time: " + timeStr);
  timeStr = valveNotOpenTime;
  dynAppend(msg, " Valve not OPEN time:     " + timeStr);
  float diff1 = (float)beamPermitFalseTime - (float)valveNotOpenTime;
  dynAppend(msg, "  Tvalve " + diff1 + " Tpermit");
  if(success)
  {
    time now = getCurrentTime();
    for(int n = 1 ; n <= dynlen(msg) ; n++)
    {
      IntlTestAddInfo(valveIdx, msg[n], now);
    }
  }
  else
  {
    IntlTestStopOnException(valveIdx, msg);
  }
}

/*************************************************************************************
**** Final step: clean up, does not matter if test succeeded or not ******************
*************************************************************************************/
int IntlTestStepCleanUpValveNew(int valveIdx, string funcName)
{
  if(!interlockSent)
  {
    return 2;
  }
  if(stepProcessFunction[valveIdx] != funcName)  // First call
  {
    IntlTestAddInfo(valveIdx, "Clean up: switch on gauges #3 and #4");
    return 0;
  }
DebugTN("IntlTestStepCleanUpValveNew(" + valveIdx + ") start");
  // Switch ON gauges
  IntlTestSwitchDevice(valveIdx, gaugeThreeDps[valveIdx], LVA_DEVICE_ON_AUTO, gaugeThreeStates[valveIdx][EQP_STATE]);
  IntlTestSwitchDevice(valveIdx, gaugeFourDps[valveIdx], LVA_DEVICE_ON_AUTO, gaugeFourStates[valveIdx][EQP_STATE]);

  // Wait for 20 seconds to allow to switch ON
  time stepEndTime = getCurrentTime() + 20;
  while(IntlTestStepCleanUpValveFinishedOn(valveIdx, stepEndTime) == 0)
  {
    delay(0, 100);
  }
/*
  // Open the valve
  IntlTestSwitchValve(valveIdx, valveDps[valveIdx], LVA_DEVICE_OPEN, valveStates[valveIdx][EQP_STATE]);
  for(int n = dynlen(relatedValvesDps[valveIdx]) ; n > 0 ; n--)
  {
    IntlTestSwitchValve(valveIdx, relatedValvesDps[valveIdx][n], LVA_DEVICE_OPEN, relatedValvesStates[valveIdx][(n-1)*EQP_TOTAL + EQP_STATE]);
  }
  if(checkVacuumPermit)
  {
    for(int n = dynlen(valvesOfBicsDps) ; n > 0 ; n--)
    {
      IntlTestSwitchValve(valveIdx, valvesOfBicsDps[n], LVA_DEVICE_OPEN, valvesOfBicsStates[n][EQP_STATE]);
    }
  }
  
  // Wait for 20 seconds to allow to OPEN
  stepEndTime = getCurrentTime() + 20;
  while(IntlTestStepCleanUpValveFinishedOpen(valveIdx, stepEndTime) == 0)
  {
    delay(0, 100);
  }
*/
//DebugTN("IntlTestStepCleanUpValve(" + valveIdx + ") finish");
  return 1;
}

int IntlTestStepCleanUpValveFinishedOn(int valveIdx, time &stepEndTime)
{
  dyn_string errMsg;
// DebugTN("IntlTestStepCleanUpValveFinished(): valveIdx = " + valveIdx);
  // All gauges/pumps before/after must be ON
  if((gaugeThreeDps[valveIdx] != "") && (gaugeThreeStates[valveIdx][EQP_STATE] != EQP_STATE_ON))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeThreeDps[valveIdx]) + " is not ON");
  }
  if((gaugeFourDps[valveIdx] != "") && (gaugeFourStates[valveIdx][EQP_STATE] != EQP_STATE_ON))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeFourDps[valveIdx]) + " is not ON");
  }
  if(!IntlTestPRSources(valveIdx))
  {
    dynAppend(errMsg, "   PR SOURCES OF VALVE " + IntlTestGetEqpName(valveDps[valveIdx]) + " is not OFF");
  }

  if(dynlen(errMsg) > 0)
  {
    time now = getCurrentTime();
    if(now <= stepEndTime)
    {
      return 0;
    }
    // Timeout
    dynAppend(errMsg, IntlTestGetEqpName(valveDps[valveIdx]) + ": Clean up step FAILED:");
    IntlTestStopOnException(valveIdx, errMsg);
    return -1;
  }

  return 1;
}

int IntlTestStepCleanUpValveFinishedOpen(int valveIdx, time &stepEndTime)
{
  dyn_string errMsg;
  if(valveStates[valveIdx][EQP_STATE] != VALVE_OPEN)
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(valveDps[valveIdx]) + " is not OPEN");
  }
  
  for(int n = dynlen(relatedValvesDps[valveIdx]) ; n > 0 ; n--)
  {
    if(relatedValvesStates[valveIdx][(n-1)*EQP_TOTAL + EQP_STATE] != VALVE_OPEN)
    {
      dynAppend(errMsg, "   " + IntlTestGetEqpName(relatedValvesDps[valveIdx][n]) + " is not OPEN");
    }
  }
  
  if(dynlen(errMsg) > 0)
  {
    time now = getCurrentTime();
    if(now <= stepEndTime)
    {
      return 0;
    }
    // Timeout
    dynAppend(errMsg, IntlTestGetEqpName(valveDps[valveIdx]) + ": Clean up step FAILED:");
    IntlTestStopOnException(valveIdx, errMsg);
    return -1;
  }

  return 1;
}

void IntlTestStepCleanUpValve(int valveIdx, bool stepByStep)
{
DebugTN("IntlTestStepCleanUpValve(" + valveIdx + ") start");
  // Switch ON pumps & gauges
  dyn_string exceptionInfo;
  IntlTestAddInfo(valveIdx, "Clean up after test finish: switch on pumps and gauges");

  IntlTestSwitchDevice(valveIdx, pumpBeforeDps[valveIdx], LVA_DEVICE_ON, pumpBeforeStates[valveIdx][EQP_STATE]);
  IntlTestSwitchDevice(valveIdx, pumpAfterDps[valveIdx], LVA_DEVICE_ON, pumpAfterStates[valveIdx][EQP_STATE]);
  IntlTestSwitchDevice(valveIdx, gaugeBeforeDps[valveIdx], LVA_DEVICE_ON_AUTO, gaugeBeforeStates[valveIdx][EQP_STATE]);
  IntlTestSwitchDevice(valveIdx, gaugeAfterDps[valveIdx], LVA_DEVICE_ON_AUTO, gaugeAfterStates[valveIdx][EQP_STATE]);
  IntlTestSwitchDevice(valveIdx, gaugeThreeDps[valveIdx], LVA_DEVICE_ON_AUTO, gaugeThreeStates[valveIdx][EQP_STATE]);
  IntlTestSwitchDevice(valveIdx, gaugeFourDps[valveIdx], LVA_DEVICE_ON_AUTO, gaugeFourStates[valveIdx][EQP_STATE]);

  // Wait for 10 seconds to allow to switch ON
  time stepEndTime = getCurrentTime() + 15;
  valveTestStep[valveIdx] = 100;
  while(valveTestStep[valveIdx] == 100)
  {
    IntlTestStepCleanUpValveFinished(valveIdx, stepEndTime);
    delay(0, 100);
  }

  // Open the valve
  IntlTestSwitchValve(valveIdx, valveDps[valveIdx], LVA_DEVICE_OPEN, valveStates[valveIdx][EQP_STATE]);
  for(int n = dynlen(relatedValvesDps[valveIdx]) ; n > 0 ; n--)
  {
    IntlTestSwitchValve(valveIdx, relatedValvesDps[valveIdx][n], LVA_DEVICE_OPEN, relatedValvesStates[valveIdx][(n-1)*EQP_TOTAL + EQP_STATE]);
  }
  if(checkVacuumPermit)
  {
    for(int n = dynlen(valvesOfBicsDps) ; n > 0 ; n--)
    {
      IntlTestSwitchValve(valveIdx, valvesOfBicsDps[n], LVA_DEVICE_OPEN, valvesOfBicsStates[n][EQP_STATE]);
    }
  }

  // Don't need to wait for valve to open???
DebugTN("IntlTestStepCleanUpValve(" + valveIdx + ") finish");
}

void IntlTestStepCleanUpValveFinished(int valveIdx, time &stepEndTime)
{
  dyn_string errMsg;
// DebugTN("IntlTestStepCleanUpValveFinished(): valveIdx = " + valveIdx);
  // All gauges/pumps before/after must be ON
  if((gaugeBeforeDps[valveIdx] != "") && (gaugeBeforeStates[valveIdx][EQP_STATE] != EQP_STATE_ON))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeBeforeDps[valveIdx]) + " is not ON");
  }
  if((pumpBeforeDps[valveIdx] != "") && (pumpBeforeStates[valveIdx][EQP_STATE] != EQP_STATE_ON))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpBeforeDps[valveIdx]) + " is not ON");
  }
  if((gaugeAfterDps[valveIdx] != "") && (gaugeAfterStates[valveIdx][EQP_STATE] != EQP_STATE_ON))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(gaugeAfterDps[valveIdx]) + " is not ON");
  }
  if((pumpAfterDps[valveIdx] != "") && (pumpAfterStates[valveIdx][EQP_STATE] != EQP_STATE_ON))
  {
    dynAppend(errMsg, "   " + IntlTestGetEqpName(pumpAfterDps[valveIdx]) + " is not ON");
  }
  if(!IntlTestPRSources(valveIdx))
  {
    dynAppend(errMsg, "   PR SOURCES OF VALVE " + IntlTestGetEqpName(valveDps[valveIdx]) + " is not OFF");
  }

  if(dynlen(errMsg) > 0)
  {
    time now = getCurrentTime();
    if(now <= stepEndTime)
    {
      return;
    }
    // Timeout
    dynAppend(errMsg, IntlTestGetEqpName(valveDps[valveIdx]) + ": Clean up step FAILED:");
    IntlTestStopOnException(valveIdx, errMsg);
    valveTestStep[valveIdx]++;
    return;
  }

  // No next step after this one
  valveTestStep[valveIdx]++;
}

/*************************************************************************************
*********************** Callbacks for data from equipment ****************************
*************************************************************************************/
int IntlTestCbValve(int idx, unsigned rr1, int plcAlarm, time ts)
{
  /*
  int idx = dynContains(valveDps, dpName);
  if(idx <= 0)
  {
    return idx;
  }
  */
  int newState = IntlTestCalculateValveState(rr1, plcAlarm);
  if(valveStates[idx][EQP_STATE] == newState)
  {
    return idx;
  }
  unsigned ymdh, msm;
  dpGet(valveDps[idx] + ".TS1.YMDH", ymdh,
        valveDps[idx] + ".TS1.MSM", msm);
  valveStates[idx][EQP_START_CLOSING_TIME] = IntlTestDecodePlcTime(ymdh, msm);
  dpGet(valveDps[idx] + ".TS2.YMDH", ymdh,
        valveDps[idx] + ".TS2.MSM", msm);
  valveStates[idx][EQP_INTERLOCK_TIME] = IntlTestDecodePlcTime(ymdh, msm);
  if(newState == VALVE_CLOSED)
  {
    valveStates[idx][EQP_CLOSED_TIME] = ts;
  }
  valveStates[idx][EQP_STATE] = newState;
  valveStates[idx][EQP_STATE_TIME] = ts;
  /* See another callback below
  if(newState == VALVE_CLOSED)
  {
    IntlTestAddVvLaserClosedEvent(valveIdx, valveDps[valveIdx], ts);
  }
  */
  pendingDataChange[idx] = true;
  return idx;
}

int IntlTestCbValveAl1Laser(int idx, bool al1, time ts)
{
  if(valveStates[idx][EQP_LASER_ALARM_REAL] == al1)
  {
    return al1;
  }
  if(al1)
  {
    IntlTestAddVvLaserClosedEvent(idx, valveDps[idx], ts);
  }
  valveStates[idx][EQP_LASER_ALARM_REAL] = al1;
}

int IntlTestCbValveOfBic(string dpName, unsigned rr1, int plcAlarm, time ts)
{
  int idx = dynContains(valvesOfBicsDps, dpName);
  if(idx <= 0)
  {
DebugTN("IntlTestCbValveOfBic(" + dpName + "):  NOT FOUND !!!!!!");
    return idx;
  }
  int newState = IntlTestCalculateValveState(rr1, plcAlarm);
  if(valvesOfBicsStates[idx][EQP_STATE] == VALVE_OPEN)
  {
    // Changed to something from OPEN
    valvesOfBicsStates[idx][EQP_START_CLOSING_TIME] = ts;
  }
  if(newState == VALVE_CLOSED)
  {
    valvesOfBicsStates[idx][EQP_CLOSED_TIME] = ts;
  }
  valvesOfBicsStates[idx][EQP_STATE] = newState;
  valvesOfBicsStates[idx][EQP_STATE_TIME] = ts;
  pendingDataChange[idx] = true;
  return idx;
}

int IntlTestCalculateValveState(unsigned rr1, int plcAlarm)
{
  int newState = VALVE_STATE_NONE;
  if(plcAlarm)
  {
    newState = VALVE_INVALID;
  }
  else if((rr1 & SVCU_SPS_R_VALID) == 0)
  {
    newState = VALVE_INVALID;
  }
  else if((rr1 & SVCU_SPS_R_ERRORS) != 0)
  {
    newState = VALVE_ERROR;
  }
  else if(((rr1 & SVCU_SPS_R_OPEN) != 0) && ((rr1 & SVCU_SPS_R_CLOSED) != 0))
  {
    newState = VALVE_ERROR;
  }
  else if((rr1 & SVCU_SPS_R_OPEN) != 0)
  {
    newState = VALVE_OPEN;
  }
  else if((rr1 & SVCU_SPS_R_CLOSED) != 0)
  {
    newState = VALVE_CLOSED;
  }
  else
  {
    newState = VALVE_UNDEFINED;
  }
  return newState;
}

void IntlTestAddVvLaserClosedEvent(int valveIdx, string dpName, time ts)
{
//  DebugTN("IntlTestAddVvLaserClosedEvent(" + valveIdx + "," + dpName + ") intlTestInitializedCb = " + intlTestInitializedCb);
  if(!intlTestInitializedCb)
  {
    return;
  }
  dynAppend(laserEqpNames[valveIdx], dpName);  // DP names of equipments with LASER alarms
  dynAppend(laserStateNames[valveIdx], "CLOSED");  // Textual descritpion of LASER alarm
  int vac;
  string sector1, sector2, mainPart;
  bool isBorder;
  dyn_string exceptionInfo;
  LhcVacDeviceVacLocation(dpName, vac, sector1, sector2, mainPart, isBorder, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    IntlTestStopOnException(valveIdx, exceptionInfo);
    return;
  }
  string alarmId = "SECTOR_VALVES_LHC:" + mainPart + "_" + IntlTestGetEqpName(dpName) + ":1";
  dynAppend(laserAlarmIds[valveIdx], alarmId);  // Alarm IDs for corresponding alarms
  dynAppend(laserPvssTimes[valveIdx], ts);  // Timestamps from PVSS
  dynAppend(laserLogTimes[valveIdx], makeTime(1970, 01, 01));  // Timestamps from LASER log DB
}

int IntlTestCbGaugeBefore(int idx, unsigned rr1, int plcAlarm, time ts)
{
  /*
  int idx = dynContains(gaugeBeforeDps, dpName);
  if(idx <= 0)
  {
    return idx;
  }
  */
  bool laserAlarm;
  int newState = IntlTestCalculateGaugePumpState(rr1, plcAlarm, laserAlarm);
  if((newState == gaugeBeforeStates[idx][EQP_STATE]) && (gaugeBeforeStates[idx][EQP_LASER_ALARM] == laserAlarm))
  {
    return idx;
  }
  if(gaugeBeforeStates[idx][EQP_STATE] == EQP_STATE_ON)
  {
    gaugeBeforeStates[idx][EQP_START_CLOSING_TIME] = ts;
  }
  if(newState == VALVE_CLOSED)
  {
    gaugeBeforeStates[idx][EQP_CLOSED_TIME] = ts;
  }
  gaugeBeforeStates[idx][EQP_STATE] = newState;
  gaugeBeforeStates[idx][EQP_STATE_TIME] = ts;
  /* See another callback below
  if(newState == EQP_STATE_OFF)
  {
    if(laserAlarm && (!gaugeBeforeStates[idx][EQP_LASER_ALARM]))
    {
      IntlTestAddVgVpLaserOffEvent(idx, gaugeBeforeDps[idx], ts);
    }
  }
  */
  gaugeBeforeStates[idx][EQP_LASER_ALARM] = laserAlarm;
  pendingDataChange[idx] = true;
  return idx;
}

int IntlTestCbGaugeBeforeAl1Laser(int idx, bool al1, time ts)
{
  if(gaugeBeforeStates[idx][EQP_LASER_ALARM_REAL] == al1)
  {
    return al1;
  }
  if(al1)
  {
    IntlTestAddVgVpLaserOffEvent(idx, gaugeBeforeDps[idx], ts);
  }
  gaugeBeforeStates[idx][EQP_LASER_ALARM_REAL] = al1;
}

int IntlTestCbPumpBefore(int idx, unsigned rr1, int plcAlarm, time ts)
{
  /*
  int idx = dynContains(pumpBeforeDps, dpName);
  if(idx <= 0)
  {
    return idx;
  }
  */
  bool laserAlarm;
  int newState = IntlTestCalculateGaugePumpState(rr1, plcAlarm, laserAlarm);
  if((newState == pumpBeforeStates[idx][EQP_STATE]) && (pumpBeforeStates[idx][EQP_LASER_ALARM] == laserAlarm))
  {
    return idx;
  }
  if(pumpBeforeStates[idx][EQP_STATE] == EQP_STATE_ON)
  {
    pumpBeforeStates[idx][EQP_START_CLOSING_TIME] = ts;
  }
  if(newState == EQP_STATE_OFF)
  {
    pumpBeforeStates[idx][EQP_CLOSED_TIME] = ts;
  }
  pumpBeforeStates[idx][EQP_STATE] = newState;
  pumpBeforeStates[idx][EQP_STATE_TIME] = ts;
  /* See another callback below
  if(newState == EQP_STATE_OFF)
  {
    if(laserAlarm && (!pumpBeforeStates[idx][EQP_LASER_ALARM]))
    {
      IntlTestAddVgVpLaserOffEvent(idx, pumpBeforeDps[idx], ts);
    }
  }
  */
  pumpBeforeStates[idx][EQP_LASER_ALARM] = laserAlarm;
  pendingDataChange[idx] = true;
  return idx;
}

int IntlTestCbPumpBeforeAl1Laser(int idx, bool al1, time ts)
{
  if(pumpBeforeStates[idx][EQP_LASER_ALARM_REAL] == al1)
  {
    return al1;
  }
  if(al1)
  {
    IntlTestAddVgVpLaserOffEvent(idx, pumpBeforeDps[idx], ts);
  }
  pumpBeforeStates[idx][EQP_LASER_ALARM_REAL] = al1;
}

int IntlTestCbGaugeAfter(int idx, unsigned rr1, int plcAlarm, time ts)
{
  /*
  int idx = dynContains(gaugeAfterDps, dpName);
  if(idx <= 0)
  {
    return idx;
  }
  */
  bool laserAlarm;
  int newState = IntlTestCalculateGaugePumpState(rr1, plcAlarm, laserAlarm);
  if((newState == gaugeAfterStates[idx][EQP_STATE]) && (gaugeAfterStates[idx][EQP_LASER_ALARM] == laserAlarm))
  {
    return idx;
  }
  if(gaugeAfterStates[idx][EQP_STATE] == EQP_STATE_ON)
  {
    gaugeAfterStates[idx][EQP_START_CLOSING_TIME] = ts;
  }
  if(newState == EQP_STATE_OFF)
  {
    gaugeAfterStates[idx][EQP_CLOSED_TIME] = ts;
  }
  gaugeAfterStates[idx][EQP_STATE] = newState;
  gaugeAfterStates[idx][EQP_STATE_TIME] = ts;
  /* See another callback below
  if(newState == EQP_STATE_OFF)
  {
    if(laserAlarm && (!gaugeAfterStates[idx][EQP_LASER_ALARM]))
    {
      IntlTestAddVgVpLaserOffEvent(idx, gaugeAfterDps[idx], ts);
    }
  }
  */
  gaugeAfterStates[idx][EQP_LASER_ALARM] = laserAlarm;
  pendingDataChange[idx] = true;
  return idx;
}

int IntlTestCbGaugeAfterAl1Laser(int idx, bool al1, time ts)
{
  if(gaugeAfterStates[idx][EQP_LASER_ALARM_REAL] == al1)
  {
    return al1;
  }
  if(al1)
  {
    IntlTestAddVgVpLaserOffEvent(idx, gaugeAfterDps[idx], ts);
  }
  gaugeAfterStates[idx][EQP_LASER_ALARM_REAL] = al1;
}

int IntlTestCbPumpAfter(int idx, unsigned rr1, int plcAlarm, time ts)
{
  /*
  int idx = dynContains(pumpAfterDps, dpName);
  if(idx <= 0)
  {
    return idx;
  }
  */
  bool laserAlarm;
  int newState = IntlTestCalculateGaugePumpState(rr1, plcAlarm, laserAlarm);
DebugTN("IntlTestCbPumpAfter()", pumpAfterStates[idx][EQP_STATE], newState);
  if((newState == pumpAfterStates[idx][EQP_STATE]) && (pumpAfterStates[idx][EQP_LASER_ALARM] == laserAlarm))
  {
    return idx;
  }
  if(pumpAfterStates[idx][EQP_STATE] == EQP_STATE_ON)
  {
    pumpAfterStates[idx][EQP_START_CLOSING_TIME] = ts;
  }
  if(newState == EQP_STATE_OFF)
  {
    pumpAfterStates[idx][EQP_CLOSED_TIME] = ts;
  }
  pumpAfterStates[idx][EQP_STATE] = newState;
  pumpAfterStates[idx][EQP_STATE_TIME] = ts;
  /* See another callback below
  if(newState == EQP_STATE_OFF)
  {
    if(laserAlarm && (!pumpAfterStates[idx][EQP_LASER_ALARM]))
    {
      IntlTestAddVgVpLaserOffEvent(idx, pumpAfterDps[idx], ts);
    }
  }
  */
  pumpAfterStates[idx][EQP_LASER_ALARM] = laserAlarm;
  pendingDataChange[idx] = true;
  return idx;
}

int IntlTestCbPumpAfterX(int idx, unsigned rr1, int plcAlarm, time ts, int pumpNumber)
{
  bool laserAlarm;
  int newState = IntlTestCalculateGaugePumpState(rr1, plcAlarm, laserAlarm);
  
  if(pumpNumber == 1)
  {
	  DebugTN("IntlTestCbPumpAfterX()",pumpNumber, pumpAfter1States[idx][EQP_STATE], newState);
	  if((newState == pumpAfter1States[idx][EQP_STATE]) && (pumpAfter1States[idx][EQP_LASER_ALARM] == laserAlarm))
	  {
		return idx;
	  }
	  if(pumpAfter1States[idx][EQP_STATE] == EQP_STATE_ON)
	  {
		pumpAfter1States[idx][EQP_START_CLOSING_TIME] = ts;
	  }
	  if(newState == EQP_STATE_OFF)
	  {
		pumpAfter1States[idx][EQP_CLOSED_TIME] = ts;
	  }
	  pumpAfter1States[idx][EQP_STATE] = newState;
	  pumpAfter1States[idx][EQP_STATE_TIME] = ts;

	  pumpAfter1States[idx][EQP_LASER_ALARM] = laserAlarm;
  }
  if(pumpNumber == 2)
  {
	  DebugTN("IntlTestCbPumpAfterX()",pumpNumber, pumpAfter2States[idx][EQP_STATE], newState);
	  if((newState == pumpAfter2States[idx][EQP_STATE]) && (pumpAfter2States[idx][EQP_LASER_ALARM] == laserAlarm))
	  {
		return idx;
	  }
	  if(pumpAfter2States[idx][EQP_STATE] == EQP_STATE_ON)
	  {
		pumpAfter2States[idx][EQP_START_CLOSING_TIME] = ts;
	  }
	  if(newState == EQP_STATE_OFF)
	  {
		pumpAfter2States[idx][EQP_CLOSED_TIME] = ts;
	  }
	  pumpAfter2States[idx][EQP_STATE] = newState;
	  pumpAfter2States[idx][EQP_STATE_TIME] = ts;

	  pumpAfter2States[idx][EQP_LASER_ALARM] = laserAlarm;
  }
  if(pumpNumber == 3)
  {
	  DebugTN("IntlTestCbPumpAfterX()",pumpNumber, pumpAfter3States[idx][EQP_STATE], newState);
	  if((newState == pumpAfter3States[idx][EQP_STATE]) && (pumpAfter3States[idx][EQP_LASER_ALARM] == laserAlarm))
	  {
		return idx;
	  }
	  if(pumpAfter3States[idx][EQP_STATE] == EQP_STATE_ON)
	  {
		pumpAfter3States[idx][EQP_START_CLOSING_TIME] = ts;
	  }
	  if(newState == EQP_STATE_OFF)
	  {
		pumpAfter3States[idx][EQP_CLOSED_TIME] = ts;
	  }
	  pumpAfter3States[idx][EQP_STATE] = newState;
	  pumpAfter3States[idx][EQP_STATE_TIME] = ts;

	  pumpAfter3States[idx][EQP_LASER_ALARM] = laserAlarm;
  }
  if(pumpNumber == 4)
  {
	  DebugTN("IntlTestCbPumpAfterX()",pumpNumber, pumpAfter4States[idx][EQP_STATE], newState);
	  if((newState == pumpAfter4States[idx][EQP_STATE]) && (pumpAfter4States[idx][EQP_LASER_ALARM] == laserAlarm))
	  {
		return idx;
	  }
	  if(pumpAfter4States[idx][EQP_STATE] == EQP_STATE_ON)
	  {
		pumpAfter4States[idx][EQP_START_CLOSING_TIME] = ts;
	  }
	  if(newState == EQP_STATE_OFF)
	  {
		pumpAfter4States[idx][EQP_CLOSED_TIME] = ts;
	  }
	  pumpAfter4States[idx][EQP_STATE] = newState;
	  pumpAfter4States[idx][EQP_STATE_TIME] = ts;

	  pumpAfter4States[idx][EQP_LASER_ALARM] = laserAlarm;
  }
  if(pumpNumber == 5)
  {
	  DebugTN("IntlTestCbPumpAfterX()",pumpNumber, pumpAfter5States[idx][EQP_STATE], newState);
	  if((newState == pumpAfter5States[idx][EQP_STATE]) && (pumpAfter5States[idx][EQP_LASER_ALARM] == laserAlarm))
	  {
		return idx;
	  }
	  if(pumpAfter5States[idx][EQP_STATE] == EQP_STATE_ON)
	  {
		pumpAfter5States[idx][EQP_START_CLOSING_TIME] = ts;
	  }
	  if(newState == EQP_STATE_OFF)
	  {
		pumpAfter5States[idx][EQP_CLOSED_TIME] = ts;
	  }
	  pumpAfter5States[idx][EQP_STATE] = newState;
	  pumpAfter5States[idx][EQP_STATE_TIME] = ts;

	  pumpAfter5States[idx][EQP_LASER_ALARM] = laserAlarm;
  }
  if(pumpNumber == 6)
  {
	  DebugTN("IntlTestCbPumpAfterX()",pumpNumber, pumpAfter6States[idx][EQP_STATE], newState);
	  if((newState == pumpAfter6States[idx][EQP_STATE]) && (pumpAfter6States[idx][EQP_LASER_ALARM] == laserAlarm))
	  {
		return idx;
	  }
	  if(pumpAfter6States[idx][EQP_STATE] == EQP_STATE_ON)
	  {
		pumpAfter6States[idx][EQP_START_CLOSING_TIME] = ts;
	  }
	  if(newState == EQP_STATE_OFF)
	  {
		pumpAfter6States[idx][EQP_CLOSED_TIME] = ts;
	  }
	  pumpAfter6States[idx][EQP_STATE] = newState;
	  pumpAfter6States[idx][EQP_STATE_TIME] = ts;

	  pumpAfter6States[idx][EQP_LASER_ALARM] = laserAlarm;
  }  
  pendingDataChange[idx] = true;
 
  return idx;
}

int IntlTestCbPumpAfterAl1Laser(int idx, bool al1, time ts)
{
  if(pumpAfterStates[idx][EQP_LASER_ALARM_REAL] == al1)
  {
    return al1;
  }
  if(al1)
  {
    IntlTestAddVgVpLaserOffEvent(idx, pumpAfterDps[idx], ts);
  }
  pumpAfterStates[idx][EQP_LASER_ALARM_REAL] = al1;
}

int IntlTestCbPumpAfterXAl1Laser(int idx, bool al1, time ts, int pumpNumber)
{
  if(pumpNumber==1)
  {
	  if(pumpAfter1States[idx][EQP_LASER_ALARM_REAL] == al1)
	  {
		return al1;
	  }
	  if(al1)
	  {
		IntlTestAddVgVpLaserOffEvent(idx, pumpAfter1Dps[idx], ts);
	  }
	  pumpAfter1States[idx][EQP_LASER_ALARM_REAL] = al1; 
  }
    if(pumpNumber==2)
  {
	  if(pumpAfter2States[idx][EQP_LASER_ALARM_REAL] == al1)
	  {
		return al1;
	  }
	  if(al1)
	  {
		IntlTestAddVgVpLaserOffEvent(idx, pumpAfter2Dps[idx], ts);
	  }
	  pumpAfter2States[idx][EQP_LASER_ALARM_REAL] = al1; 
  }
    if(pumpNumber==3)
  {
	  if(pumpAfter3States[idx][EQP_LASER_ALARM_REAL] == al1)
	  {
		return al1;
	  }
	  if(al1)
	  {
		IntlTestAddVgVpLaserOffEvent(idx, pumpAfter3Dps[idx], ts);
	  }
	  pumpAfter3States[idx][EQP_LASER_ALARM_REAL] = al1; 
  }
    if(pumpNumber==4)
  {
	  if(pumpAfter4States[idx][EQP_LASER_ALARM_REAL] == al1)
	  {
		return al1;
	  }
	  if(al1)
	  {
		IntlTestAddVgVpLaserOffEvent(idx, pumpAfter4Dps[idx], ts);
	  }
	  pumpAfter4States[idx][EQP_LASER_ALARM_REAL] = al1; 
  }
    if(pumpNumber==5)
  {
	  if(pumpAfter5States[idx][EQP_LASER_ALARM_REAL] == al1)
	  {
		return al1;
	  }
	  if(al1)
	  {
		IntlTestAddVgVpLaserOffEvent(idx, pumpAfter5Dps[idx], ts);
	  }
	  pumpAfter5States[idx][EQP_LASER_ALARM_REAL] = al1; 
  }
    if(pumpNumber==6)
  {
	  if(pumpAfter6States[idx][EQP_LASER_ALARM_REAL] == al1)
	  {
		return al1;
	  }
	  if(al1)
	  {
		IntlTestAddVgVpLaserOffEvent(idx, pumpAfter6Dps[idx], ts);
	  }
	  pumpAfter6States[idx][EQP_LASER_ALARM_REAL] = al1; 
  }

}

int IntlTestCbGaugeThree(int idx, unsigned rr1, int plcAlarm, time ts)
{
  /*
  int idx = dynContains(gaugeThreeDps, dpName);
  if(idx <= 0)
  {
    return idx;
  }
  */
  bool laserAlarm;
  int newState = IntlTestCalculateGaugePumpState(rr1, plcAlarm, laserAlarm);
  if((newState == gaugeThreeStates[idx][EQP_STATE]) && (gaugeThreeStates[idx][EQP_LASER_ALARM] == laserAlarm))
  {
    return idx;
  }
  if(gaugeThreeStates[idx][EQP_STATE] == EQP_STATE_ON)
  {
    gaugeThreeStates[idx][EQP_START_CLOSING_TIME] = ts;
  }
  if(newState == EQP_STATE_OFF)
  {
    gaugeThreeStates[idx][EQP_CLOSED_TIME] = ts;
  }
  gaugeThreeStates[idx][EQP_STATE] = newState;
  gaugeThreeStates[idx][EQP_STATE_TIME] = ts;
  /* See another callback below
  if(newState == EQP_STATE_OFF)
  {
    if(laserAlarm && (!gaugeThreeStates[idx][EQP_LASER_ALARM]))
    {
      IntlTestAddVgVpLaserOffEvent(idx, gaugeThreeDps[idx], ts);
    }
  }
  */
  gaugeThreeStates[idx][EQP_LASER_ALARM] = laserAlarm;
  pendingDataChange[idx] = true;
  return idx;
}

int IntlTestCbGaugeThreeAl1Laser(int idx, bool al1, time ts)
{
  if(gaugeThreeStates[idx][EQP_LASER_ALARM_REAL] == al1)
  {
    return al1;
  }
  if(al1)
  {
    IntlTestAddVgVpLaserOffEvent(idx, gaugeThreeDps[idx], ts);
  }
  gaugeThreeStates[idx][EQP_LASER_ALARM_REAL] = al1;
}


int IntlTestCbGaugeFour(int idx, unsigned rr1, int plcAlarm, time ts)
{
  /*
  int idx = dynContains(gaugeFourDps, dpName);
  if(idx <= 0)
  {
    return idx;
  }
  */
  bool laserAlarm;
  int newState = IntlTestCalculateGaugePumpState(rr1, plcAlarm, laserAlarm);
  if((newState == gaugeFourStates[idx][EQP_STATE]) && (gaugeFourStates[idx][EQP_LASER_ALARM] == laserAlarm))
  {
    return idx;
  }
  if(gaugeFourStates[idx][EQP_STATE] == EQP_STATE_ON)
  {
    gaugeFourStates[idx][EQP_START_CLOSING_TIME] = ts;
  }
  if(newState == EQP_STATE_OFF)
  {
    gaugeFourStates[idx][EQP_CLOSED_TIME] = ts;
  }
  gaugeFourStates[idx][EQP_STATE] = newState;
  gaugeFourStates[idx][EQP_STATE_TIME] = ts;
  /* See another callback below
  if(newState == EQP_STATE_OFF)
  {
    if(laserAlarm && (!gaugeFourStates[idx][EQP_LASER_ALARM]))
    {
      IntlTestAddVgVpLaserOffEvent(idx, gaugeFourDps[idx], ts);
    }
  }
  */
  gaugeFourStates[idx][EQP_LASER_ALARM] = laserAlarm;
  pendingDataChange[idx] = true;
  return idx;
}

int IntlTestCbGaugeFourAl1Laser(int idx, bool al1, time ts)
{
  if(gaugeFourStates[idx][EQP_LASER_ALARM_REAL] == al1)
  {
    return al1;
  }
  if(al1)
  {
    IntlTestAddVgVpLaserOffEvent(idx, gaugeFourDps[idx], ts);
  }
  gaugeFourStates[idx][EQP_LASER_ALARM_REAL] = al1;
}

int IntlTestCbRelatedValve(int idx, string dpName, unsigned rr1, int plcAlarm, time ts)
{
  DebugTN("IntlTestCbRelatedValve",dpName);
  int n = dynContains(relatedValvesDps[idx], dpName);
  if(n <= 0)
  {
    return n;
  }
  int newState = IntlTestCalculateValveState(rr1, plcAlarm);
  if(relatedValvesStates[idx][(n-1)*EQP_TOTAL + EQP_STATE] == newState)
  {
    return idx;
  }
  if(relatedValvesStates[idx][(n-1)*EQP_TOTAL + EQP_STATE] == VALVE_OPEN)  // Changed from OPEN to something else
  {
    // Changed to something from OPN
    unsigned ymdh, msm;
    dpGet(relatedValvesDps[idx][n] + ".TS1.YMDH", ymdh,
          relatedValvesDps[idx][n] + ".TS1.MSM", msm);
    relatedValvesStates[idx][(n-1)*EQP_TOTAL + EQP_START_CLOSING_TIME] = IntlTestDecodePlcTime(ymdh, msm);
    dpGet(relatedValvesDps[idx][n] + ".TS2.YMDH", ymdh,
          relatedValvesDps[idx][n] + ".TS2.MSM", msm);
    relatedValvesStates[idx][(n-1)*EQP_TOTAL + EQP_INTERLOCK_TIME] = IntlTestDecodePlcTime(ymdh, msm);
  }
  if(newState == VALVE_CLOSED)
  {
    relatedValvesStates[idx][(n-1)*EQP_TOTAL + EQP_CLOSED_TIME] = ts;
  }
  relatedValvesStates[idx][(n-1)*EQP_TOTAL + EQP_STATE] = newState;
  relatedValvesStates[idx][(n-1)*EQP_TOTAL + EQP_STATE_TIME] = ts;
  /* See another callback below
  if(newState == VALVE_CLOSED)
  {
    IntlTestAddVvLaserClosedEvent(valveIdx, valveDps[valveIdx], ts);
  }
  */
  pendingDataChange[idx] = true;
  return idx;
}

int IntlTestCbRelatedValveAl1Laser(int idx, string dpName, bool al1, time ts)
{
  int n = dynContains(relatedValvesDps[idx], dpName);
  if(n <= 0)
  {
    return n;
  }
// DebugTN("IntlTestCbValveAl1Laser(" + idx + ", " + al1 + ")");
  if(relatedValvesStates[idx][(n-1)*EQP_TOTAL + EQP_LASER_ALARM_REAL] == al1)
  {
    return al1;
  }
  relatedValvesStates[idx][(n-1)*EQP_TOTAL + EQP_LASER_ALARM_REAL] = al1;
}

void IntlTestAddVgVpLaserOffEvent(int valveIdx, string dpName, time ts)
{
  if(!intlTestInitializedCb)
  {
    return;
  }
  dynAppend(laserEqpNames[valveIdx], dpName);  // DP names of equipments with LASER alarms
  dynAppend(laserStateNames[valveIdx], "OFF");  // Textual descritpion of LASER alarm
  int vac;
  string sector1, sector2, mainPart;
  bool isBorder;
  dyn_string exceptionInfo;
  LhcVacDeviceVacLocation(dpName, vac, sector1, sector2, mainPart, isBorder, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    IntlTestStopOnException(valveIdx, exceptionInfo);
    return;
  }
  if(sector1 == "")
  {
    dyn_string slaveDps;
    LhcVacEqpSlaves(dpName, slaveDps);
    if(dynlen(slaveDps) == 0)
    {
      fwException_raise(exceptionInfo, "ERROR", "IntlTestAddVgVpLaserOffEvent() No sector information for " +
        IntlTestGetEqpName(dpName), "");
      IntlTestStopOnException(valveIdx, exceptionInfo);
      return;
    }
    LhcVacDeviceVacLocation(slaveDps[1], vac, sector1, sector2, mainPart, isBorder, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      IntlTestStopOnException(valveIdx, exceptionInfo);
      return;
    }
  }
  string alarmId = "VAC_PRESSURE_LHC:" + mainPart + "_" + sector1 + "_" + IntlTestGetEqpName(dpName) + ":1";
  dynAppend(laserAlarmIds[valveIdx], alarmId);  // Alarm IDs for corresponding alarms
  dynAppend(laserPvssTimes[valveIdx], ts);  // Timestamps from PVSS
  dynAppend(laserLogTimes[valveIdx], makeTime(1970, 01, 01));  // Timestamps from LASER log DB
}

int IntlTestCalculateGaugePumpState(unsigned rr1, int plcAlarm, bool &laserAlarm)
{
  int newState = EQP_STATE_NONE;
  if(plcAlarm)
  {
    newState = EQP_STATE_INVALID;
  }
  else if((rr1 & DEVICE_R_VALID) == 0)
  {
    newState = EQP_STATE_INVALID;
  }
  else if((rr1 & DEVICE_R_ERRORS) != 0)
  {
    newState = EQP_STATE_ERROR;
  }
  else if(((rr1 & DEVICE_R_OFF) != 0) && ((rr1 & DEVICE_R_ON) != 0))
  {
    newState = EQP_STATE_ERROR;
  }
  else if((rr1 & DEVICE_R_ON) != 0)
  {
    newState = EQP_STATE_ON;
  }
  else if((rr1 & DEVICE_R_OFF) != 0)
  {
    newState = EQP_STATE_OFF;
  }
  else
  {
    newState = EQP_STATE_UNDEFINED;
  }
  laserAlarm = (rr1 & 0x2000) != 0;
  return newState;
}

// int IntlTestCbFirstBic(int idx, bool driverOk, unsigned inputReg, dyn_uint cibuStatuses, time ts)
// {
//   IntlTestCbBicProcess(idx, firstBicChannels[idx], firstBicAStates[idx], firstBicBStates[idx], driverOk, inputReg, cibuStatuses, ts);
//   return idx;
// }

int IntlTestCbFirstBicExpertHistory(int idx, dyn_uint type, dyn_uint subtype, dyn_uint utcSec, dyn_uint utcuSec)
{
  IntlTestCbBicExpertHistoryProcess(idx, firstBicChannels[idx], firstBicAStates[idx], firstBicBStates[idx], type, subtype, utcSec, utcuSec);
  return idx;
}

int IntlTestCbFirstBicBoardRegisters(int idx, dyn_uint values)
{
  IntlTestCbBicBoardRegistersProcess(idx, firstBicChannels[idx], firstBicAStates2[idx], firstBicBStates2[idx], values);
  return idx;
}

// int IntlTestCbSecondBic(int idx, bool driverOk, unsigned inputReg, dyn_uint cibuStatuses, time ts)
// {
//   IntlTestCbBicProcess(idx, secondBicChannels[idx], secondBicAStates[idx], secondBicBStates[idx], driverOk, inputReg, cibuStatuses, ts);
//   return idx;
// }

int IntlTestCbSecondBicExpertHistory(int idx, dyn_uint type, dyn_uint subtype, dyn_uint utcSec, dyn_uint utcuSec)
{
  IntlTestCbBicExpertHistoryProcess(idx, secondBicChannels[idx], secondBicAStates[idx], secondBicBStates[idx], type, subtype, utcSec, utcuSec);
  return idx;
}

int IntlTestCbSecondBicBoardRegisters(int idx, dyn_uint values)
{
  IntlTestCbBicBoardRegistersProcess(idx, secondBicChannels[idx], secondBicAStates2[idx], secondBicBStates2[idx], values);
  return idx;
}

void IntlTestUpdateBic(int idx)
{
  string dpev1, dpev2, dpev3, dpev4, dpet1, dpet2;
  dyn_uint types, subtypes, utcSecs, utcuSecs;
  
  if(firstBicDps[idx] != "") {
    dpev1 = firstBicDps[idx] + ".ExpertHistoryBuffer.type";
    dpev2 = firstBicDps[idx] + ".ExpertHistoryBuffer.subtype";
    dpev3 = firstBicDps[idx] + ".ExpertHistoryBuffer.utcSec";
    dpev4 = firstBicDps[idx] + ".ExpertHistoryBuffer.utcuSec";
    dpGet(dpev1, types, dpev2, subtypes, dpev3, utcSecs, dpev4, utcuSecs);
    IntlTestCbBicProcess(idx, firstBicChannels[idx], firstBicAStates[idx], firstBicBStates[idx],
                            types, subtypes, utcSecs, utcuSecs);
  }
  
  if(secondBicDps[idx] != "") {
    dpev1 = secondBicDps[idx] + ".ExpertHistoryBuffer.type";
    dpev2 = secondBicDps[idx] + ".ExpertHistoryBuffer.subtype";
    dpev3 = secondBicDps[idx] + ".ExpertHistoryBuffer.utcSec";
    dpev4 = secondBicDps[idx] + ".ExpertHistoryBuffer.utcuSec";
    dpGet(dpev1, types, dpev2, subtypes, dpev3, utcSecs, dpev4, utcuSecs);
    IntlTestCbBicProcess(idx, secondBicChannels[idx], secondBicAStates[idx], secondBicBStates[idx],
                            types, subtypes, utcSecs, utcuSecs);
  }
}

void IntlTestCbBicProcess(int idx, dyn_int channels, dyn_anytype &statesA, dyn_anytype &statesB,
                    dyn_uint types, dyn_uint subtypes, dyn_uint utcSecs, dyn_uint utcuSecs)
{
  time ts;
  int ch;
  dyn_int subtypesAFT, subtypesBFT, subtypesATF, subtypesBTF;
  
  subtypesAFT = makeDynInt();
  subtypesBFT = makeDynInt();
  subtypesATF = makeDynInt();
  subtypesBTF = makeDynInt();
  for(int n = dynlen(channels) ; n > 0 ; n--)
  {
    ch = channels[n];
    dynAppend(subtypesAFT, ch*2-1);
    dynAppend(subtypesBFT, ch*2+27);
    dynAppend(subtypesATF, ch*2);
    dynAppend(subtypesBTF, ch*2+28);
  }
  
  int n = dynlen(types);
  bool vacuumA = false, vacuumB = false, beamA = false, beamB = false;
  while(n > 0)
  {
    //setPeriod(ts, utcSecs[n], utcuSecs[n] / 1000);
    //DebugTN(types[n], subtypes[n], ts, utcuSecs[n]);
    switch(types[n])
    {
    case 4: // USER_PERMIT
    case 9: // DISABLED_PERMIT
      if(!vacuumA && dynContains(subtypesAFT, subtypes[n])) // A F-T
      {
        statesA[BIC_VACUUM_PERMIT] = true;
        statesA[BIC_DATA_ARRIVED] = true;
        vacuumA = true;
      }
      else if(!vacuumB && dynContains(subtypesBFT, subtypes[n])) // B F-T
      {
        statesB[BIC_VACUUM_PERMIT] = true;
        statesB[BIC_DATA_ARRIVED] = true;
        vacuumB = true;
      }
      else if(!vacuumA && dynContains(subtypesATF, subtypes[n])) // A T-F
      {
        setPeriod(ts, utcSecs[n], utcuSecs[n] / 1000);
        statesA[BIC_VACUUM_PERMIT_FALSE_TIME] = ts;
        statesA[BIC_VACUUM_PERMIT] = false;
        statesA[BIC_DATA_ARRIVED] = true;
        vacuumA = true;
      }
      else if(!vacuumB && dynContains(subtypesBTF, subtypes[n])) // B T-F
      {
        setPeriod(ts, utcSecs[n], utcuSecs[n] / 1000);
        statesB[BIC_VACUUM_PERMIT_FALSE_TIME] = ts;
        statesB[BIC_VACUUM_PERMIT] = false;
        statesB[BIC_DATA_ARRIVED] = true;
        vacuumB = true;
      }
      break;
    case 2: // BEAM_PERMIT
      if(!beamA && subtypes[n] == 1) // A F-T
      {
        statesA[BIC_BEAM_PERMIT] = true;
        statesA[BIC_DATA_ARRIVED] = true;
        beamA = true;
      }
      else if(!beamB && subtypes[n] == 3) // B F-T
      {
        statesB[BIC_BEAM_PERMIT] = true;
        statesB[BIC_DATA_ARRIVED] = true;
        beamB = true;
      }
      else if(!beamA && subtypes[n] == 2) // A T-F
      {
        setPeriod(ts, utcSecs[n], utcuSecs[n] / 1000);
        statesA[BIC_BEAM_PERMIT_FALSE_TIME] = ts;
        statesA[BIC_BEAM_PERMIT] = false;
        statesA[BIC_DATA_ARRIVED] = true;
        beamA = true;
      }
      else if(!beamB && subtypes[n] == 4) // B T-F
      {
        setPeriod(ts, utcSecs[n], utcuSecs[n] / 1000);
        statesB[BIC_BEAM_PERMIT_FALSE_TIME] = ts;
        statesB[BIC_BEAM_PERMIT] = false;
        statesB[BIC_DATA_ARRIVED] = true;
        beamB = true;
      }
      break;
    }
    if(((checkVacuumPermit && vacuumA && vacuumB) || !checkVacuumPermit) &&
       ((checkBeamPermit && beamA && beamB) || !checkBeamPermit))
    {
      break;
    }
    n--;
  }
}

void IntlTestCbBicExpertHistoryProcess(int idx, dyn_int channels, dyn_anytype &statesA, dyn_anytype &statesB,
                    dyn_uint types, dyn_uint subtypes, dyn_uint utcSecs, dyn_uint utcuSecs)
{
  time ts;
  int ch;
  dyn_int subtypesAFT, subtypesBFT, subtypesATF, subtypesBTF;
  
  subtypesAFT = makeDynInt();
  subtypesBFT = makeDynInt();
  subtypesATF = makeDynInt();
  subtypesBTF = makeDynInt();
  for(int n = dynlen(channels) ; n > 0 ; n--)
  {
    ch = channels[n];
    dynAppend(subtypesAFT, ch*2-1);
    dynAppend(subtypesBFT, ch*2+27);
    dynAppend(subtypesATF, ch*2);
    dynAppend(subtypesBTF, ch*2+28);
  }
  // What if arrays don't have same size?
  // Keep the remaining values?
  int listLen = dynlen(types);
  for(int n = 1; n <= listLen; n++)
  {
    if(types[n] == 9) // DISABLED_PERMIT
    {
      if(dynContains(subtypesAFT, subtypes[n]))
      {
        statesA[BIC_VACUUM_PERMIT] = true;
        statesA[BIC_DATA_ARRIVED] = true;
      }
      else if(dynContains(subtypesBFT, subtypes[n]))
      {
        statesB[BIC_VACUUM_PERMIT] = true;
        statesB[BIC_DATA_ARRIVED] = true;
      }
      else if(dynContains(subtypesATF, subtypes[n]))
      {
        setPeriod(ts, utcSecs[n], utcuSecs[n] / 1000);
        statesA[BIC_VACUUM_PERMIT_FALSE_TIME] = ts;
        statesA[BIC_VACUUM_PERMIT] = false;
        statesA[BIC_DATA_ARRIVED] = true;
      }
      else if(dynContains(subtypesBTF, subtypes[n]))
      {
        setPeriod(ts, utcSecs[n], utcuSecs[n] / 1000);
        statesB[BIC_VACUUM_PERMIT_FALSE_TIME] = ts;
        statesB[BIC_VACUUM_PERMIT] = false;
        statesB[BIC_DATA_ARRIVED] = true;
      }
    }
  }
}

void IntlTestCbBicBoardRegistersProcess(int idx, dyn_int channels, dyn_anytype &statesA, dyn_anytype &statesB, dyn_uint values)
{
  if(dynlen(values) < 13)
  {
    DebugTN("IntlTestCbBicBoardRegistersProcess: No BIC data for " + IntlTestGetEqpName(valveDps[idx]));
    return;
  }
  
  bool vacPermit, beamPermit, beamSimulation, beamInfo;
  unsigned mask;
  
  time ts;
  uint utcSec = values[12]; // UTC_seconds
  uint utcuSec = values[13]; // UTC_microseconds
  setPeriod(ts, utcSec, utcuSec / 1000);
  unsigned inputReg = values[4]; // INPUTS
  
  // A
  mask = IntlTestCalculateMask(channels);
  vacPermit = (inputReg & mask) == mask;
  
  if(statesA[BIC_DATA_ARRIVED] && statesA[BIC_VACUUM_PERMIT] && (!vacPermit))
  {
    statesA[BIC_VACUUM_PERMIT_FALSE_TIME] = ts;
  }
  if(statesA[BIC_DATA_ARRIVED] && statesA[BIC_BEAM_PERMIT] && (!beamPermit))
  {
    statesA[BIC_BEAM_PERMIT_FALSE_TIME] = ts;
  }
  
  statesA[BIC_DATA_ARRIVED] = true;
  statesA[BIC_VACUUM_PERMIT] = vacPermit;
  statesA[BIC_BEAM_PERMIT] = beamPermit;
  statesA[BIC_BEAM_SIMULATION] = beamSimulation;
  statesA[BIC_BEAM_INFO] = beamInfo;
  statesA[BIC_TIME] = ts;
  
  // B
  mask = mask << 16;
  vacPermit = (inputReg & mask) == mask;
  if(statesB[BIC_DATA_ARRIVED] && statesB[BIC_VACUUM_PERMIT] && (!vacPermit))
  {
    statesB[BIC_VACUUM_PERMIT_FALSE_TIME] = ts;
  }
  if(statesB[BIC_DATA_ARRIVED] && statesB[BIC_BEAM_PERMIT] && (!beamPermit))
  {
    statesB[BIC_BEAM_PERMIT_FALSE_TIME] = ts;
  }
  statesB[BIC_DATA_ARRIVED] = true;
  statesB[BIC_VACUUM_PERMIT] = vacPermit;
  statesB[BIC_BEAM_PERMIT] = beamPermit;
  statesB[BIC_BEAM_SIMULATION] = beamSimulation;
  statesB[BIC_BEAM_INFO] = beamInfo;
  statesB[BIC_TIME] = ts;
  
  pendingDataChange[idx] = true;
}
  
unsigned IntlTestCalculateMask(dyn_int channels)
{
  unsigned mask = 0x0u;
  for(int n = dynlen(channels) ; n > 0 ; n--)
  {
    unsigned channelMask = 0x1 << channels[n];
    mask |= channelMask;
  }
  return mask;
}

void IntlTestCalculateBeamPermit(dyn_int channels, dyn_uint cibuStatuses,
                                 bool &beamPermit, bool &beamSimulation,
                                 bool &beamInfo)
{
  beamPermit = beamSimulation = beamInfo = true;
  for(int n = dynlen(channels) ; n > 0 ; n--)
  {
    if(dynlen(cibuStatuses) < channels[n])
    {
      DebugTN("Too short cibuStatuses " + dynlen(cibuStatuses));
      beamPermit = false;
      beamSimulation = false;
      beamInfo = false;
      break;
    }
    if((cibuStatuses[channels[n]] & 0x00002000u) == 0)
    {
      beamPermit = false;
    }
    if((cibuStatuses[channels[n]] & 0x00000080u) == 0)
    {
      beamSimulation = false;
    }
    // TODO
    if((cibuStatuses[channels[n]] & 0x00000004u) == 0)
    {
      beamInfo = false;
    }
  }
}

/*************************************************************************************
*********************** Auxillary functions ******************************************
*************************************************************************************/

string IntlTestGetEqpName(string dpName)
{
  string name;
  dyn_string exceptionInfo;
  LhcVacDisplayName(dpName, name, exceptionInfo);
  if(name == "")
  {
    name = dpName;
  }
  return name;
}

bool IntlTestSwitchDevice(int valveIdx, string dpName, unsigned actionRaw, int state)
{
  if(dpName == "")
  {
    return true;
  }
  
  unsigned action = actionRaw;
  string dpType;
  dyn_string exceptionInfo;
  LhcVacDpType(dpName, dpType,exceptionInfo);
 
  if(dpType=="VG_PT")
  {
	  if(action ==LVA_DEVICE_OFF)
	  {
		  action = LVA_DEVICE_VG_PT_OFF;
	  }		  
	  if(action == LVA_DEVICE_ON_AUTO)
	  {
		  action = LVA_DEVICE_VG_PT_ON_PROTECT;
	  }
  }
  
  bool on = (action == LVA_DEVICE_ON) || (action == LVA_DEVICE_ON_AUTO) || (action == LVA_DEVICE_VG_PT_ON_PROTECT);
  int expectedState = on ? EQP_STATE_ON : EQP_STATE_OFF;
  if(expectedState == state)
  {
    return true;  // Action is not needed, device is already in expected state
  }
  IntlTestAddInfo(valveIdx, "Switching " + (on ? "ON" : "OFF") +
            " " + IntlTestGetEqpName(dpName));
  
  dyn_string exceptionInfo;
  LhcVacDeviceAction(makeDynAnytype(action), makeDynString(), makeDynString(),
     dpName, 0, exceptionInfo, actComment);
  if(dynlen(exceptionInfo) == 0)
  {
    return true;
  }
  IntlTestStopOnException(valveIdx, exceptionInfo);
  return false;
}

bool IntlTestSwitchValve(int valveIdx, string dpName, unsigned action, int state)
{
  int expectedState = action == LVA_DEVICE_OPEN ? VALVE_OPEN : VALVE_CLOSED;

  if(expectedState == state)
  {
    return true;  // Action is not needed, device is already in expected state
  }
  IntlTestAddInfo(valveIdx,
      (action == LVA_DEVICE_OPEN ? "Opening" : "Closing") +
      " " + IntlTestGetEqpName(dpName));
  
  dyn_string exceptionInfo;
  LhcVacDeviceAction(makeDynAnytype(action), makeDynString(), makeDynString(),
     dpName, 0, exceptionInfo, actComment);
  if(dynlen(exceptionInfo) == 0)
  {
    return true;
  }
  IntlTestStopOnException(valveIdx, exceptionInfo);
  return false;
}
dyn_int IntlTestGetBicChannels(string bicDp, int vacType)
{
  dyn_int result = makeDynInt();
  string attr;
  int channel = 0;
  switch(vacType)
  {
  case LVA_VACUUM_BLUE_BEAM:
    LhcVacGetAttribute(bicDp, "ChannelVacuumB1", attr);
    if(attr != "")
    {
      channel = attr;
      dynAppend(result, channel);
    }
    break;
  case LVA_VACUUM_RED_BEAM:
    LhcVacGetAttribute(bicDp, "ChannelVacuumB2", attr);
    if(attr != "")
    {
      channel = attr;
      dynAppend(result, channel);
    }
    break;
  default:
    LhcVacGetAttribute(bicDp, "ChannelVacuumB1", attr);
    if(attr != "")
    {
      channel = attr;
      dynAppend(result, channel);
    }
    channel = 0;
    LhcVacGetAttribute(bicDp, "ChannelVacuumB2", attr);
    if(attr != "")
    {
      channel = attr;
      dynAppend(result, channel);
    }
    channel = 0;
    LhcVacGetAttribute(bicDp, "ChannelVacuumBX", attr);
    if(attr != "")
    {
      channel = attr;
      dynAppend(result, channel);
    }
    break;
  }
  return result;
}
string IntlTestCheckDpExist(string dpName)
{
  if(dpName == "")
  {
    return "";
  }
  if(dpExists(dpName))
  {
    return dpName;
  }
  dyn_string dpList = dpNames(dpName + "*;");
  if(dynlen(dpList) > 0)
  {
    return(dpSubStr(dpList[1], DPSUB_DP));
  }
  return "";
}

void IntlTestWriteBicCommand(int valveIdx, unsigned cmd)
{
  if(firstBicDps[valveIdx] != "")
  {
    IntlTestWriteBicDpe(valveIdx, firstBicDps[valveIdx], cmd);
    if(valveTestResult[valveIdx] != TEST_IN_PROGRESS)
    {
      return;
    }
  }
  if(secondBicDps[valveIdx] != "")
  {
    IntlTestWriteBicDpe(valveIdx, secondBicDps[valveIdx], cmd);
  }
}

void IntlTestWriteBicDpe(int valveIdx, string bicDp, unsigned cmd)
{
  int   iTimeout=5;  // wait 5 sec max to get the exception
  dyn_string exceptionInfo;

  // Write to Test1
  string dpe = bicDp + ".Test1";
  unCMWClient_setUpExceptionHandling(dpe, exceptionInfo); // set up an exception handler
  dpSet(dpe, cmd); // do here the dpSet
  unCMWClient_getException(exceptionInfo, iTimeout); // get the exception if any.

  if(dynlen(exceptionInfo) > 0)        // error Set
  {
    // handle the errors here
    IntlTestStopOnException(valveIdx, exceptionInfo);
    return;
  }

  // Write to Test2
  string dpe = bicDp + ".Test2";
  unCMWClient_setUpExceptionHandling(dpe, exceptionInfo); // set up an exception handler
  dpSet(dpe, cmd); // do here the dpSet
  unCMWClient_getException(exceptionInfo, iTimeout); // get the exception if any.

  if(dynlen(exceptionInfo) > 0)        // error Set
  {
    // handle the errors here
    IntlTestStopOnException(valveIdx, exceptionInfo);
    return;
  }
}

void IntlTestAddInfo(int valveIdx, string info)
{
  dynAppend(valveTestHistory[valveIdx], info);
  dynAppend(valveTestHistoryTs[valveIdx], getCurrentTime());
  dynAppend(valveTestHistorySeverity[valveIdx], HISTORY_INFO);
}

void IntlTestAddWarn(int valveIdx, string warn)
{
  dynAppend(valveTestHistory[valveIdx], warn);
  dynAppend(valveTestHistoryTs[valveIdx], getCurrentTime());
  dynAppend(valveTestHistorySeverity[valveIdx], HISTORY_WARN);
}

void IntlTestStopOnException(int valveIdx, dyn_string exceptionInfo)
{
DebugTN("IntlTestStopOnException(" + valveIdx + ")", exceptionInfo);
  time now = getCurrentTime();
  int nExceptions = dynlen(exceptionInfo);
  for(int n = 1 ; n <= nExceptions ; n++)
  {
    if(exceptionInfo[n] != "")
    {
      dynAppend(valveTestHistory[valveIdx], exceptionInfo[n]);
      dynAppend(valveTestHistoryTs[valveIdx], now);
      dynAppend(valveTestHistorySeverity[valveIdx], HISTORY_ERROR);
    }
  }
  valveTestResult[valveIdx] = TEST_FAILURE;
}

string IntlTestResultName(int coco)
{
  switch(coco)
  {
  case TEST_NOT_STARTED:
    return "NOT STARTED";
  case TEST_IN_PROGRESS:
    return "IN PROGRESS";
  case TEST_OK:
    return "SUCCESS";
  case TEST_FAILURE:
    return "FAILURE";
  case TEST_CANCELLED:
    return "CANCELLED";
  case TEST_PAUSE:
    return "PAUSED";
  }
  return "Unknown result " + coco;
}

void IntlTestBuildValvesOfBics(int valveIdx, dyn_string &exceptionInfo)
{
  dynClear(valvesOfBicsStates);
  dynClear(valvesOfBicsDps);
  IntlTestBuildValvesOfBic(valveIdx, firstBicDps[valveIdx], exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  IntlTestBuildValvesOfBic(valveIdx, secondBicDps[valveIdx], exceptionInfo);
  if(dynlen(valvesOfBicsDps) == 0)
  {
    fwException_raise(exceptionInfo, "ERROR",
      "IntlTestBuildValvesOfBics(): no BIC(s) for valve " + IntlTestGetEqpName(valveDps[valveIdx]),
      "");
  }
  //DebugTN("Valves for BIC total: " + dynlen(valvesOfBicsDps), valvesOfBicsDps);
}

void IntlTestBuildValvesOfBic(int valveIdx, string bicDp, dyn_string &exceptionInfo)
{
DebugTN("IntlTestBuildValvesOfBic(" + bicDp + ")");
  if(bicDp == "")
  {
    return;
  }
  dyn_string   allValves = dpNames("*", "VVS_S");
  for(int i = dynlen(allValves); i > 0; i--)
  {
    string valveDp = dpSubStr(allValves[i], DPSUB_DP);
    bool useDp = false;
    string attr;
    LhcVacGetAttribute(valveDp, "BIC", attr);
    if(attr == bicDp)
    {
      useDp = true;
	  DebugTN("Found valve");
    }
    else
    {
      LhcVacGetAttribute(valveDp, "BIC1", attr); 
      if(attr == bicDp)
      {
        useDp = true;
      }
    }
    if(!useDp)
    {
      LhcVacGetAttribute(valveDp, "BIC2", attr); 
      if(attr == bicDp)
      {
        useDp = true;
      }
    }
    if(!useDp)
    {
      continue;
    }
    if(dynContains(valvesOfBicsDps, valveDp) > 0)
    {
      continue;
    }
    dynAppend(valvesOfBicsDps, valveDp);
    valvesOfBicsStates[dynlen(valvesOfBicsDps)] = IntlTestMakeEmptyValveState();
  }
  DebugTN("Valves for BIC " + bicDp + ": " + dynlen(valvesOfBicsDps), valvesOfBicsDps);
}

int IntlTestTotalSources(int valveIdx, bool newGauges)
{
  int totalSources = 0;
  if(gaugeBeforeDps[valveIdx] != "")
  {
    totalSources++;
  }
  if(gaugeAfterDps[valveIdx] != "")
  {
    totalSources++;
  }
  if(pumpBeforeDps[valveIdx] != "")
  {
    totalSources++;
  }
  if(pumpAfterDps[valveIdx] != "")
  {
    totalSources++;
  }
  if(newGauges && gaugeThreeDps[valveIdx] != "")
  {
    totalSources++;
  }
  if(newGauges && gaugeFourDps[valveIdx] != "")
  {
    totalSources++;
  }
  return totalSources;
}

dyn_anytype IntlTestMakeEmptyValveState()
{
  return makeDynAnytype((int)VALVE_STATE_NONE,  // STATE
                        (time)makeTime(1970, 01, 01), // STATE_TIME
                        (time)makeTime(1970, 01, 01), // EQP_CLOSED_TIME   -- added 11.12.2013 L.Kopylov after answer by H.Pereira
                        (time)makeTime(1970, 01, 01),  // START_CLOSING_TIME
                        (time)makeTime(1970, 01, 01),  // INTERLOCK_TIME
                        (bool)false,  // LASER_ALARM
                        (bool)false);  // LASER_ALARM_REAL
}

dyn_anytype IntlTestMakeEmptyBicState()
{
  return makeDynAnytype((bool)false, (bool)false, (bool)false,
                        (bool)false, (bool)false,
                        (time)makeTime(1970, 01, 01),
                        (time)makeTime(1970, 01, 01),
                        (time)makeTime(1970, 01, 01));
}

time IntlTestDecodePlcTime(unsigned ymdh, unsigned msm)
{
  unsigned yearValue = 2000 + (( ymdh & 0xFF000000u ) >> 24);
  unsigned monthValue = ( ymdh & 0x00FF0000u ) >> 16;
  unsigned dayValue = ( ymdh & 0x0000FF00u ) >> 8;
  unsigned hourValue = ( ymdh & 0x000000FFu );
  unsigned minValue = ( msm & 0xFF000000u ) >> 24;
  unsigned secValue = ( msm & 0x00FF0000u ) >> 16;
  unsigned msecValue = ( msm & 0x0000FFFFu );
  return makeTime(yearValue, monthValue, dayValue, hourValue, minValue, secValue, msecValue, false) + timeFromGMT();
}

void IntlTestSetProgram1()
{
  testSteps = makeDynString(
    "IntlTestStepInitial",
    "IntlTestStepBeamInfoToFalse",
    "IntlTestStepValveOpenRelated",
    "IntlTestStepValveOpenAllOnBics",
    "IntlTestWaitVacPermitTrue",
    "IntlTestUnforceBeamInfo",
    "IntlTestDevicesOffPRSources",
    "IntlTestNewDevicesOffInterlock",
    "IntlTestWaitForBeamPermitFalse",
    "IntlTestWaitForValveClosed",
    "IntlTestCompareReationTimes",
    "IntlTestLogTimes",
    "IntlTestStepCleanUpValveNew",
    "IntlTestStepValveOpenRelated",
    "IntlTestDevicesOffInterlock",
    "IntlTestWaitForBeamPermitFalse",
    "IntlTestWaitForValveClosed",
    "IntlTestCompareReationTimes",
    "IntlTestLogTimes"
    );

  testExceptions = makeDynString(
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    ""
    );

  testStepNames = makeDynString(
    "Initial - wait for data",
    "Set BEAM INFO to FALSE",
    "Open the valve and related valves",
    "Open all valves in affected BIC(s)",
    "Wait for VACUUM PERMIT = TRUE",
    "Unforce Beam Info",
    "Switch OFF devices and check PR Sources",
    "Switch OFF devices #3 and #4 to interlock valve",
    "Wait for VACUUM BEAM PERMIT = FALSE",
    "Wait for valve to close",
    "Check events sequence",
    "Log other useful timestamps",
    "Cleaning up devices #3 and #4",
    "Open the valve and related valves",
    "Switch OFF devices to interlock valve",
    "Wait for VACUUM BEAM PERMIT = FALSE",
    "Wait for valve to close",
    "Check events sequence",
    "Log other useful timestamps"
    );
}

void IntlTestSetProgram2()
{
  testSteps = makeDynString(
    "IntlTestStepInitial",
    "IntlTestStepBeamInfoToFalse",
    "IntlTestStepValveClose",
    "IntlTestWaitVacPermitFalse",
    "IntlTestCompareClosePermitTimes",
    "IntlTestCompareNotOpenPermitTimes",
    "IntlTestStepValveOpenAllOnBics",
    "IntlTestWaitVacPermitTrue",
    "IntlTestForceBeamInfoTrue",
    "IntlTestStepValveCloseMustStayOpen",
    "IntlTestWaitVacPermitTrue"
    );

  testExceptions = makeDynString(
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    ""
    );

  testStepNames = makeDynString(
    "Initial - wait for data",
    "Set BEAM INFO to FALSE",
    "Close valve",
    "Wait for VACUUM BEAM PERMIT = FALSE",
    "Check events sequence - valve closed",
    "Check events sequence - valve not open",
    "Open all valves in affected BIC(s)",
    "Wait for VACUUM PERMIT = TRUE",
    "Force Beam Info to TRUE",
    "CLOSE valve must remain OPEN",
    "Wait for VACUUM PERMIT = TRUE"
    );
}

void IntlTestSetProgramSPS()
{
	testSteps = makeDynString(
    "IntlTestStepInitial",
    "IntlTestStepBeamInfoToFalse",
//	"IntlTestStepValveOpenRelated",
 //   "IntlTestStepValveOpenAllOnBics",
//	"IntlTestWaitVacPermitTrue",
    "IntlTestStepValveCloseAllOnBics",
	"IntlTestStepTurnOffGaugePrevious",
	"IntlTestLogSPSInterlocks",
	"IntlTestStepOpenValveMustStayClosed",
	"IntlTestStepTurnOnGaugePrevious",
	"IntlTestStepTurnOffGaugeAfter",
	"IntlTestLogSPSInterlocks",
	"IntlTestStepOpenValveMustStayClosed",
	"IntlTestStepTurnOnGaugeAfter",
	"IntlTestStepValveOpenRelated",
	"IntlTestStepTurnOffFirstThreePumps",
	"IntlTestWaitForBeamPermitFalse",
    "IntlTestWaitForValveClosed",
    "IntlTestCompareReationTimes",
    "IntlTestLogTimes",
	"IntlTestLogSPSInterlocks",
	"IntlTestStepTurnOnFirstThreePumps",
	"IntlTestStepValveOpenRelated",
	"IntlTestStepTurnOffLastThreePumps",
	"IntlTestWaitForBeamPermitFalse",
    "IntlTestWaitForValveClosed",
    "IntlTestCompareReationTimes",
    "IntlTestLogTimes",
	"IntlTestLogSPSInterlocks",
	"IntlTestStepTurnOnLastThreePumps"
    );

  testExceptions = makeDynString(
    "",
    "",
    ""
    );

  testStepNames = makeDynString(
    "Initial - wait for data",
    "Set BEAM INFO to FALSE",
//	"Open the valve and related valves",
 //   "Open all valves in affected BIC(s)",
   // "Wait for VACUUM BEAM PERMIT = TRUE",
    "Close all valves on BIC",
	"Switch off previous gauge",
	"Log interlock states",
	"Try to open valve - must remain closed",
	"Switch on previous gauge",
	"Switch off next gauge",
	"Log interlock states",
	"Try to open valve - must remain closed",
	"Switch on next gauge",
	"Open valve",
	"Turn off first three pumps on next sector",
	"Wait for VACUUM BEAM PERMIT = FALSE",
    "Wait for valve to close",
    "Check events sequence",
    "Log other useful timestamps",
	"Log interlock states",
	"Turn on first three pumps on next sector",
	"Open valve",
	"Turn off last three pumps on next sector",
	"Wait for VACUUM BEAM PERMIT = FALSE",
    "Wait for valve to close",
    "Check events sequence",
    "Log other useful timestamps",
	"Log interlock states",
	"Turn on last three pumps on next sector"	
    );
}

void IntlTestSetProgramSPS2()
{
	testSteps = makeDynString(
    "IntlTestStepInitial",
    "IntlTestStepValveOpenAllOnBics",
	"IntlTestWaitVacPermitTrue"
    );

  testExceptions = makeDynString(
    "",
    "",
    ""
    );

  testStepNames = makeDynString(
    "Initial - wait for data",
    "Open all valves in affected BIC(s)",
    "Wait for VACUUM BEAM PERMIT = TRUE"
    );
}