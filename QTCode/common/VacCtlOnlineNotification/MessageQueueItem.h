#ifndef	MESSAGEQUEUEITEM_H
#define	MESSAGEQUEUEITEM_H

#include "VacCtlOnlineNotificationExport.h"

// Item if message queue

#include <qstring.h>

class VACCTLONLINENOTIFICATION_EXPORT MessageQueueItem
{
public:
	MessageQueueItem(const QString &cfgName, const QString &msgText);
	MessageQueueItem(const QString &cfgName, const QString &msgText, const QString &msgBody);
	~MessageQueueItem() {}

	// Access
	inline const QString &getConfigName(void) const { return configName; }
	inline const QString &getText(void) const { return text; }
	inline const QString &getBody(void) const { return body; }

protected:
	// Name of configuration that produced the message
	QString	configName;

	// Message text - goes to E-mail subject
	QString	text;

	// Body - goes to message body
	QString	body;
};

#endif	// MESSAGEQUEUEITEM_H
