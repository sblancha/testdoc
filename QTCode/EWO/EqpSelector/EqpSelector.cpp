//	Implementation of EqpSelector class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpSelector.h"

EWO_PLUGIN(EqpSelector)


EqpSelector::EqpSelector(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new EqpSelectorControl(parent);

	// Connect to signals of base widget
	connect(baseWidget, SIGNAL(selectionChanged(void)), this, SLOT(selectionChange(void)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList EqpSelector::signalList(void) const
{
	QStringList list;
	list.append("selectionChanged()");
	return list;
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList EqpSelector::methodList(void) const
{
	QStringList list;
	list.append("void setSelectedDps(dyn_string dpList)");
	list.append("dyn_string getSelectedDps()");
	list.append("bool isEmpty()");
	list.append("int initContent()");
	list.append("void setUsageFilter(dyn_int funcTypes, dyn_string attrNames, dyn_bool reverseFlags)");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant EqpSelector::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "setSelectedDps")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::StringList))
		{
			error = "argument 1 (dpList) is not dyn_string";
			return QVariant();
		}
		baseWidget->setSelectedEqp(values[0].toStringList());
		return QVariant();
	}
	else if(name == "getSelectedDps")
	{
		QStringList	sectorList;
		baseWidget->getSelectedEqp(sectorList);
		return QVariant(sectorList);
	}
	else if(name == "isEmpty")
	{
		return QVariant(baseWidget->isEmpty());
	}
	else if(name == "initContent")
	{
		return QVariant(baseWidget->initContent());
	}
	else if(name == "setUsageFilter")
	{
		if(!hasNumArgs(name, values, 3, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::List))
		{
			error = "argument 1 (funcTypes) is not dyn_int";
			return QVariant();
		}
		QList<QVariant> funcTypeList = values[0].toList();
		QList<int> funcTypes;
		for(int idx = 0 ; idx < funcTypeList.count() ; idx++)
		{
			if(!funcTypeList[idx].canConvert(QVariant::Int))
			{
				error = "Element ";
				error += QString::number(idx);
				error += " of argument 1 (funcTypes) is not int";
				return QVariant();
			}
			funcTypes.append(funcTypeList[idx].toInt());
		}

		if(!values[1].canConvert(QVariant::StringList))
		{
			error = "argument 2 (attrNames) is not dyn_string";
			return QVariant();
		}
		QStringList attrNameList = values[1].toStringList();
		QList<QByteArray> attrNames;
		foreach(QString attrName, attrNameList)
		{
			attrNames.append(attrName.toLatin1());
		}

		if(!values[2].canConvert(QVariant::List))
		{
			error = "argument 3 (reverseFlags) is not dyn_bool";
			return QVariant();
		}
		QList<QVariant> revervseFlagList = values[2].toList();
		QList<bool> reverseFlags;
		for(int idx = 0 ; idx < revervseFlagList.count() ; idx++)
		{
			if(!revervseFlagList[idx].canConvert(QVariant::Bool))
			{
				error = "Element ";
				error += QString::number(idx);
				error += " of argument 3 (reverseFlags) is not bool";
				return QVariant();
			}
			reverseFlags.append(revervseFlagList[idx].toBool());
		}

		if((funcTypes.count() != attrNames.count()) || (funcTypes.count() != reverseFlags.count()))
		{
			error = "List length mismatch: funcTypes ";
			error += QString::number(funcTypes.count());
			error += ", attrNames ";
			error += QString::number(attrNames.count());
			error += ", reverseFlags ";
			error += QString::number(reverseFlags.count());
			return QVariant();
		}
		baseWidget->setEqpUsageFilter(funcTypes, attrNames, reverseFlags);
	}
	else
	{
		error = "Uknown method EqpSelector.";
		error += name;
	}
	return QVariant();
}

/*
**	FUNCTION
**		Slot receiving selectionChanged signal of EqpSelectorControl, emit
**		signal 'selectionChanged' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelector::selectionChange(void)
{
	emit signal("selectionChanged");
}

