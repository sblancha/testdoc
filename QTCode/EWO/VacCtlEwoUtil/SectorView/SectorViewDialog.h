#ifndef	SECTORVIEWDIALOG_H
#define	SECTORVIEWDIALOG_H

// Dialog for displaying sector view
// TODO: can it be just a subclass of synoptic view dialog???

#include "DataEnum.h"

#include <QDialog>

class Sector;
class SectView;

#include <QPushButton>
#include <QMenu>
#include <QLabel>
#include <QComboBox>
#include <QScrollArea>

class SectorViewDialog : public QDialog
{
	Q_OBJECT

public:
	SectorViewDialog(Sector *pFirstSector, Sector *pLastSector,
		DataEnum::DataMode mode);
	~SectorViewDialog();

protected:
	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Button for displaying 'File' menu
	QPushButton	*pFilePb;

	// Button for displaying 'View' menu
	QPushButton	*pViewPb;

	// Button for displaying 'Help' menu
	QPushButton	*pHelpPb;

	// 'File' menu
	QMenu	*pFileMenu;

	// 'View' menu
	QMenu	*pViewMenu;

	// 'Help' menu
	QMenu	*pHelpMenu;

	// Label with synoptic title
	QLabel		*pTitleLabel;

	// Combo box for main part selection
	QComboBox	*pCombo;

	// Scroll view holding synoptic image and icons
	QScrollArea	*pScroll;

	// Sector view instance providing real sector view drawing
	SectView		*pView;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Data to be shown in dialog //////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Pointer to first sector to be shown
	Sector			*pFirstSector;

	// Pointer to last sector to be shown
	Sector			*pLastSector;

	// Data acquisition mode
	DataEnum::DataMode		mode;

	void buildLayout(void);
	void buildFileMenu(void);
	void buildViewMenu(void);
	void buildHelpMenu(void);
	void buildMainPartCombo(void);
	void selectInitialMp(void);

	void setTitle(void);
	void buildView(bool init);
	void setMyWidth(bool init);

	void makeDialogRequest(int type);

	virtual void resizeEvent(QResizeEvent *pEvent);

private slots:
	void filePrint(void);

	void viewSynoptic(void);
	void viewProfile(void);
	void help(void);
	void comboActivated(const QString &name);

	void viewMove(int x);
};

#endif	// SECTORVIEWDIALOG_H
