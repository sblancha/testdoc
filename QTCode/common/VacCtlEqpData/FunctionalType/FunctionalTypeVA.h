#ifndef FUNCTIONALTYPEVA_H
#define FUNCTIONALTYPEVA_H

// Special functional type for vacuum hardware alarm - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVA : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum {
		Ok = 1,
		Bad = 2
	};

	FunctionalTypeVA(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVA() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVA_H


