#ifndef	EQPEXP_AREA_H
#define	EQPEXP_AREA_H

//	Experimental area - 'passive' device in a sense that there are no any data behind it

// Without Q_OBJECT inherits() does not work

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpEXP_AREA : public Eqp
{
	Q_OBJECT

public:
	EqpEXP_AREA(const Eqp &source);
#ifdef Q_OS_WIN
	EqpEXP_AREA(const EqpEXP_AREA &src) : Eqp(src) {}
#endif
	~EqpEXP_AREA();

	// Override Eqp's methods
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);

protected:
	// Visible name for menu etc. - comes from resource file
	QString	description;
};


#endif	// EQPEXP_AREA_H
