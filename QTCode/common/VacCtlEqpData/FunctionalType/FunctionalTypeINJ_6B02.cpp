//	Implementation of FunctionalTypeINJ_6B02 class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeINJ_6B02.h"


#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeINJ_6B02::FunctionalTypeINJ_6B02(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeINJ_6B02::fillCriteria(FunctionalType *pType)
{
	// Own criteria
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Nominal, "Gas injection is nominal");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is NOMINAL");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT NOMINAL");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Gas injection in sector {sector} is NOMINAL");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Gas injection in sector {sector} is NOT NOMINAL");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Gas injection in main part {mainPart} is NOMINAL");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Gas injection in main part {mainPart} is NOT NOMINAL");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, VvgFullOpen, "VVG is full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} VVG is full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} iVVG is NOT full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Gas injection in sector {sector} VVG is full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Gas injection in sector {sector} VVG is NOT full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Gas injection in main part {mainPart} VVG is full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Gas injection in main part {mainPart} VVG is NOT full OPEN");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeINJ_6B02::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case Nominal:
			result = "Nominal";
			break;
		case VvgFullOpen:
			result = "VvgFullOpen";
			break;
		}
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeINJ_6B02::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "Nominal"))
		{
			result = Nominal;
		}
		else if(!strcasecmp(name, "VvgFullOpen"))
		{
			result = VvgFullOpen;
		}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}

