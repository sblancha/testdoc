#ifndef EXTENDEDLABEL_H
#define	EXTENDEDLABEL_H

#include "VacCtlEwoUtilExport.h"

// Subclass QLabel to provide extra functionality.
// Depending on labelType the following functionality is provided:
//	Default	- usual QLabel
//	Active	- this label can emit mousePressed() signal
//	Transparent	- the label does not process mouse events, but rather passes them to parent
//
// In addition, this class provides conveniency method setBackColor. Setting invalid color
// make this widget transparent (default for QLabel)

#include <QLabel>

class VACCTLEWOUTIL_EXPORT ExtendedLabel : public QLabel
{
	Q_OBJECT

public:

	enum
	{
		Default = 0,
		Active = 1,
		Transparent = 2
	};

	ExtendedLabel(QWidget *parent, Qt::WindowFlags flags = 0) :
		QLabel(parent, flags) { labelType = Default; }
	ExtendedLabel(const QString &text, QWidget *parent, Qt::WindowFlags flags = 0) :
		QLabel(text, parent, flags) { labelType = Default; }
	~ExtendedLabel() { labelType = Default; }

	// Access
	virtual inline int getLabelType(void) const { return labelType; }
	virtual void setLabelType(int type);
	virtual void setBackColor(const QColor &color);
	
signals:
	void mousePressed(QMouseEvent *e);

protected:
	int	labelType;

	virtual void mouseDoubleClickEvent(QMouseEvent *e);
	virtual void mouseMoveEvent(QMouseEvent *e);
	virtual void mousePressEvent(QMouseEvent *e);
	virtual void mouseReleaseEvent(QMouseEvent *e);
};

#endif	// EXTENDEDLABEL_H
