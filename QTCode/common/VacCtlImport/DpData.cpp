//	Implementation of DpData class
//////////////////////////////////////////////////////////////////////////////////

#include "DpData.h"
#include "ImportPool.h"

#include "PlatformDef.h"

#include <QStringList>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

ImportPool *DpData::pPool = NULL;

void DpData::setImportPool(ImportPool *pImportPool)
{
	pPool = pImportPool;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DpData::DpData(const char *name, int lineNo)
{
	this->name= pPool->addString( name );
	this->lineNo = lineNo;
	dpType = NULL;
	mobileType = DEFAULT_MOBILE_TYPE;
	exportToCMW = false;
	pDpes = new QList<DpeData *>();
}

DpData::~DpData()
{
	while(!pDpes->isEmpty())
	{
		delete pDpes->takeFirst();
	}
	delete pDpes;
}

/*
**
**	FUNCTION
**		Create new DpData instance from data read in file.
**
**	PARAMETERS
**		pFile		- Pointer to file being read.
**		lineNo		- Line number in file.
**		name		- name of new DP
**		errList		- List where error message shall be added
**						in case of errors.
**
**	RETURNS
**		Pointer to new DpData instance created from data in file;
**		NULL in case of error.
**
**	CAUTIONS
**		It is responsability of caller to destroy returned instance using 'delete'
**		operator.
*/
DpData *DpData::createFromFile(FILE *pFile, int &lineNo, const char *name,
	QStringList &errList)
{
	DpData	*pNew = new DpData(name, lineNo);
	if(pNew->parseFile(pFile, lineNo, errList) < 0)
	{
		delete pNew;
		return NULL;
	}

	if(!pNew->getDpType())
	{
		PARSING_WARNING1("DpType for DP <%s> isn't specified", pNew->getName());
	}
	return(pNew);
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int DpData::processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
	const QString & /* longValue */, QStringList &errList)
{
	const char *funcName = "DpData::processLineTokens()";
	if(!strcasecmp(tokens[0], "DpType"))	
	{
		
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		dpType = pPool->addMultiUseString(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "MobileType"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &mobileType) != 1)
		{
			PARSING_WARNING2("Line %d: bad mobileType format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "ExportToCMW"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		int value;
		if(sscanf(tokens[1], "%d", &value) != 1)
		{
			PARSING_WARNING2("Line %d: bad ExportToCMW format <%s>", lineNo, tokens[1]);
		}
		exportToCMW = value != 0;
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Dpe"))	
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		DpeData *pDpe;
		foreach(pDpe, *pDpes)
		{
			if(!strcmp(tokens[1], pDpe->getName()))
			{
				PARSING_WARNING2("Line %d: duplicate DPE <%s>", lineNo, tokens[1]);
				return 1;
			}	
		}
		pDpe = DpeData::createFromFile(pFile, lineNo, name, tokens[1], errList);
		if(!pDpe)
		{
			return -1;
		}
		pDpes->append(pDpe);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "EndDevice"))		// End of device
	{
		return 0;
	}
	PARSING_ERR2("Line %d: unexpected <%s>", lineNo, tokens[0]);
}

/*
**
**	FUNCTION
**		Dump instance content to output file for debugging
**
**	PARAMETERS
**		pFile		- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpData::dump(FILE *pFile)
{
	if(!ImportPool::isDebugMode())
	{
		return;
	}
	fprintf(pFile, "============= DP <%s> of type <%s> mobile %d lineNo %d\n",
		name, dpType, mobileType, lineNo);
	for(int idx = 0 ; idx < pDpes->count() ; idx++)
	{
		DpeData *pDpe = pDpes->at(idx);
		pDpe->dump(pFile);
	}
}
