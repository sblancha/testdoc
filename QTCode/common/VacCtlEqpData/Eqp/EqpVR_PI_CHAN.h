#ifndef	EQPVR_PI_CHAN_H
#define EQPVR_PI_CHAN_H

//	VR_PI_CHAN - Ion Pump controller channel

#include "Eqp.h"
#include "EqpVR_PI.h"
// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Generic purpose color defined in eqp.h

//	Main state bits to be analyzed
#define	RR1_STEPMODE_VRPICHAN		(0x00020000) // (0x00 02  00 00) 

class VACCTLEQPDATA_EXPORT EqpVR_PI_CHAN : public Eqp
{
	Q_OBJECT

public:
	EqpVR_PI_CHAN(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVR_PI_CHAN(const EqpVR_PI_CHAN &src) : Eqp(src) {}
#endif
	~EqpVR_PI_CHAN();
	virtual inline QString getMasterDp(void) { return (QString)masterDp; }
	virtual void getMaster(QString &master, int &masterChannel);
	virtual void getSlaves(QStringList &slaves);
	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);
	virtual int getChannelNb() { return channelNb; }

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::Pressure; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode mode);
	virtual bool isMainValueNegative(void) const;
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
	virtual void getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end);
	virtual float getGrowFactorForDevList(void);
	virtual int getSpikeFilterForDevList(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	}
	inline unsigned getRR1(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? rr1Replay : rr1Online;
	}
	inline unsigned getRR2(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? rr2Replay : rr2Online;
	}
	inline float getPR(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? prReplay : prOnline;
	}
	inline float getCURRENT(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? currentReplay : currentOnline;
	}
	inline float getVOLTAGE(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? voltageReplay : voltageOnline;
	}
	inline bool isBlockedOff(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? blockedOffReplay : blockedOffOnline;
	}

	virtual int getErrorCode(DataEnum::DataMode mode);

	bool isValueValid(DataEnum::DataMode mode);

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:

	QString masterDp;

	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR2 DPE for online mode
	unsigned	rr2Online;

	// Last known value of PR DPE for online mode
	float	prOnline;
	
	// Last known value of CURRENT DPE for online mode
	float	currentOnline;
	
	// Last known value of CURRENT DPE for online mode
	float	voltageOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for replay mode
	unsigned	rr2Replay;

	// Last known value of PR DPE for replay mode
	float	prReplay;
	
	// Last known value of CURRENT DPE for replay mode
	float	currentReplay;
	
	// Last known value of CURRENT DPE for replay mode
	float	voltageReplay;

	// Last knwon value of BlockedOFF DPE for online mode
	bool		blockedOffOnline;

	// Last known value of BlockedOFF DPE for replay mode
	bool		blockedOffReplay;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

	// Flag indicating if this device have BlockedOff DPE (VELO VRPI has no one)
	bool		haveBlockedOff;

	// The channel number on the controller
	int			channelNb;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, float pr, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, float pr, bool plcAlarm, QString &string);
	virtual void getMainValueString(unsigned rr1, float pr, bool plcAlarm, QString &string);
#endif

	virtual bool isValueValid(bool plcAlarm, unsigned rr1);

};

#endif	// EQPVR_PI_CHAN_H
