//	Implementation of VacIconVGP class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGP.h"

#include "EqpVG.h"

#include <qpainter.h>

#define	RR1_VALID		(0x40000000)
#define RR1_REMOTE		(0x20000000)
#define RR1_AUTO		(0x08000000)
#define RR1_PROTECTED	(0x00080000)
#define	RR1_ON			(0x00020000)
#define	RR1_OFF			(0x00010000)

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_FORCED_ON		255,0,0
#define COLOR_AUTO			0,204,0


// We only need to implement drawSpecific() method

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVGP::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("PR");
	if(pEqp->inherits("EqpVG"))
	{
		if(((EqpVG *)pEqp)->isHaveBlockedOff())
		{
			dpes.append("BlockedOFF");
		}
	}
	else if(pEqp->inherits("EqpVG_PT"))
	{
		dpes.append("BlockedOFF");
	}
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

// Draw 'Penning' symbol in the center of icon
void VacIconVGP::drawSpecific(QPainter &painter)
{
	QPen blackPen(Qt::black, 2);
	painter.setPen(blackPen);
	/*
	painter.drawArc(9, 8, 3, 3, 0, 360 * 16);
	*/
	painter.drawLine(10, 8, 12, 8);

	bool blockedOff = false;
	bool forcedOn = false;
	bool localOnOrder = false;

	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			break;
		default:
			{
				// Special painting for AUTO and ForcedON modes
				bool plcAlarm = pEqp->isPlcAlarm(mode);
				if(!plcAlarm)
				{
					unsigned rr1 = 0;
					if(pEqp->inherits("EqpVG"))
					{
						rr1 = ((EqpVG *)pEqp)->getRR1(mode);
					}
					if(rr1 & RR1_VALID)
					{
						if(rr1 & RR1_AUTO)
						{
							if(!(rr1 & RR1_ON))
							{
								/*
								QColor color(COLOR_AUTO);
								QPen pen(color, 2);
								painter.setPen(pen);
								*/
							}
						}
						else if((rr1 & RR1_ON) && (!(rr1 & RR1_OFF)))	// Assume this is ForcedON - because AUTO is not there
						{
							/*
							QColor color(COLOR_FORCED_ON);
							QPen pen(color, 2);
							painter.setPen(pen);
							*/
							forcedOn = true;
							if(!(rr1 & RR1_REMOTE))
							{
								localOnOrder = true;
							}
						}
					}
				}
				if(pEqp->inherits("EqpVG"))
				{
					blockedOff = ((EqpVG *)pEqp)->isBlockedOff(mode);
				}
			}
			break;
		}
	}

	painter.setPen(Qt::black);
	QPolygon points(4);
	points.setPoint(0, 5, 8);
	points.setPoint(1, 5, 13);
	points.setPoint(2, 15, 13);
	points.setPoint(3, 15, 8);
	painter.drawPolyline(points);
	if(forcedOn)
	{
		painter.drawLine(5, 11, 15, 11);
		if(localOnOrder)
		{
			painter.drawLine(5, 14, 5, 15);
			painter.drawLine(15, 14, 15, 15);
			painter.drawLine(5, 15, 15, 15);
		}
	}

	if(blockedOff)
	{
		painter.setPen(blackPen);
		painter.drawLine(0, 13, 20, 20);
		painter.drawLine(0, 20, 20, 13);
	}
}
