#ifndef	BEAMLINESTATS_H
#define	BEAMLINESTATS_H

//	Class to collect and analyze statistics of beam lines in orde to find beam
//	line with the highest 'weight'. What is 'weight' is decided by user of this
//	class

#include "BeamLineWeight.h"

#include <qlist.h>

class BeamLineStats
{
public:
	BeamLineStats();
	~BeamLineStats();

	void increment(BeamLine *pBeamLine);
	BeamLine *getBestLine(void);

protected:
	QList<BeamLineWeight *>	lines;
};
#endif	// BEAMLINESTATS_H
