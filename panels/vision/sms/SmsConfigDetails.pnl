V 14
1
LANG:1 21 Configuration Details
PANEL,-1 -1 525 560 N "_3DFace" 1
"$dpName"
"main()
{
  dpName = $dpName;
  if(dpName == \"\")
  {
    PanelOff();
    return;
  }
  if(!dpExists(dpName))
  {
    PanelOff();
    return;
  }

  BuildMachineModeTable();

  // Is this my configuration or not?
  isMyConfig = VacSmsIsMyConfiguration(dpName);

  // Connect to DPEs to show configuration content/state
  dpConnect(\"VisibleNameCb\",
    dpName + \".VisibleName\");
  dpConnect(\"MachineModeCb\",
    dpName + \".Mode\");

  dpConnect(\"StateCb\",
    dpName + \".State.Activate\",
    dpName + \".State.Activated\",
    dpName + \".State.Initialized\",
    dpName + \".State.MessagesSent\",
    dpName + \".State.ErrorMessage\",
    dpName + \".State.EventCount\");

  dpConnect(\"CriteriaCb\",
    dpName + \".Criteria.JoinType\",
    dpName + \".Criteria.FunctionalTypes\",
    dpName + \".Criteria.Types\",
    dpName + \".Criteria.Subtypes\",
    dpName + \".Criteria.LowerLimits\",
    dpName + \".Criteria.UpperLimits\",
    dpName + \".Criteria.ReverseFlags\");

  dpConnect(\"ScopeCb\",
    dpName + \".Scope.Type\",
    dpName + \".Scope.Names\");

  dpConnect(\"FilteringCb\",
    dpName + \".MsgFiltering.Type\",
    dpName + \".MsgFiltering.DeadTime\",
    dpName + \".MsgFiltering.Grouping.Type\",
    dpName + \".MsgFiltering.Grouping.Interval\",
    dpName + \".MsgFiltering.Grouping.Count\",
    dpName + \".MsgFiltering.Grouping.Message\");

  dpConnect(\"RecipientsCb\",
    dpName + \".Recipients\");

  dpConnect(\"MessageCb\",
    dpName + \".Message\");

  ResetCounterPb.enabled = isMyConfig;
  initialized = true;
}" 0
 E E E E 1 0 0 0  10 10
""0  1
E "
// DP name of configuration being edited
string dpName;

// Flag indicating if this is my configuration, i.e. I'm allowed to edit it
bool    isMyConfig;

// Flag indicating if criteria is empty
bool	emptyCriteria;

// Flag indicating if scope is empty
bool	emptyScope;

// Flag indicating if message filtering is bad
bool	emptyFiltering;

// Flag indicating if list of recipients is empty
bool	emptyRecipients;

// Flag indicating if message isempty
bool	emptyMessage;

// Flag inidcating if panel initialization is finished
bool initialized = false;

void BuildMachineModeTable()
{
  for(int mode = 0 ; mode < 32 ; mode++)
  {
    string resourceName = \"MachineMode\" + mode;
    string modeName = VacResourcesGetValue(resourceName, VAC_RESOURCE_STRING, \"\");
    if(modeName == \"\")
    {
      continue;
    }
    TabMachineMode.appendLine(\"mode\", mode, \"use\", false, \"modeName\", modeName);
  }
}


void UpdateSensitivity()
{
  DescriptionText.enabled = isMyConfig;
  TabMachineMode.enabled = isMyConfig;
  MessageText.enabled = isMyConfig;
  ActivatePb.enabled = isMyConfig;
  DeactivatePb.enabled = isMyConfig;
  if(!isMyConfig)
  {
    return;
  }
  
  bool ready = true;
  if(emptyCriteria || emptyScope || emptyFiltering || emptyRecipients || emptyMessage)
  {
    ready = false;
  }
  if(strltrim(strrtrim(MessageText.text)) == \"\")
  {
    ready = false;
  }
  ActivatePb.enabled = ready;

  /*
  bool mayEdit = ! ActivateText.visible;
  CriteriaPb.enabled = mayEdit;
  ScopePb.enabled = mayEdit;
  FilteringPb.enabled = mayEdit;
  */
}


bool MayEditConfiguration()
{
  if(ActivatedText.visible)
  {
    dyn_string exceptionInfo;
    fwException_raise(exceptionInfo, \"ERROR\", \"MayEditConfiguration(): editing activated configuration is not allowed\", \"\");
    fwExceptionHandling_display(exceptionInfo);
    return false;
  }
  return true;
}

void VisibleNameCb(string dpe, string description)
{
  DescriptionText.text = description;
}

void SetNewDescription()
{
  if(!isMyConfig)
  {
    return;
  }
  string oldDescription;
  dpGet(dpName + \".VisibleName\", oldDescription);
  string newDescription = strltrim(strrtrim(DescriptionText.text));
  if(oldDescription == newDescription)
  {
    return;
  }
  if(newDescription == \"\")
  {
    return;
  }
  dpSet(dpName + \".VisibleName\", newDescription);
}

void MachineModeCb(string dpe, unsigned mode)
{
  for(int row = 0 ; row < TabMachineMode.lineCount ; row++)
  {
    int tabMode;
    getValue(\"TabMachineMode\", \"cellValueRC\", row, \"mode\", tabMode);
    bool flag = (mode & (1 << tabMode)) != 0;
    TabMachineMode.cellValueRC(row, \"use\", flag);
  }
}

void SetNewMode()
{
  if(!isMyConfig)
  {
    return;
  }
  unsigned mode = 0;
  if(ModeRunCb.state(0))
  {
    mode |= 0x1;
  }
  if(ModeMdCb.state(0))
  {
    mode |= 0x2;
  }
  if(ModeScCb.state(0))
  {
    mode |= 0x4;
  }
  if(ModeSwCb.state(0))
  {
    mode |= 0x8;
  }

  unsigned oldMode;
  dpGet(dpName + \".Mode\", oldMode);
  if(mode == oldMode)
  {
    return;
  }
  dpSet(dpName + \".Mode\", mode);
}

void StateCb(string dpe1, bool activate, string dpe2, bool activated,
  string dpe3, bool initialized, string dpe4, int nMessages,
  string dpe5, string errMsg, string dpe6, uint nEvents)
{
  ActivateText.visible = activate;
  ActivatedText.visible = activated;
  InitializedText.visible = initialized;
  MsgCountText.text = \"Events: \" + nEvents + \", messages sent: \" + nMessages;
  ErrorText.text = errMsg;
  UpdateSensitivity();
}


void CriteriaCb(string dpe1, int joinType, string dpe2, dyn_int funcTypes,
  string dpe3, dyn_int types, string dpe4, dyn_int subTypes,
  string dpe5, dyn_float lowerLimits, string dpe6, dyn_float upperLimits,
  string dpe7, dyn_bool reverseFlags)
{
  if(dynlen(funcTypes) == 0)
  {
    emptyCriteria = true;
    CriteriaText.text = \"No criteria\";
  }
  else if(dynlen(funcTypes) != dynlen(types))
  {
    emptyCriteria = true;
    CriteriaText.text = \"Invalid criteria\";
  }
  else if(dynlen(funcTypes) != dynlen(subTypes))
  {
    emptyCriteria = true;
    CriteriaText.text = \"Invalid criteria\";
  }
  else if(dynlen(funcTypes) != dynlen(lowerLimits))
  {
    emptyCriteria = true;
    CriteriaText.text = \"Invalid criteria\";
  }
  else if(dynlen(funcTypes) != dynlen(upperLimits))
  {
    emptyCriteria = true;
    CriteriaText.text = \"Invalid criteria\";
  }
  else if(dynlen(funcTypes) != dynlen(reverseFlags))
  {
    emptyCriteria = true;
    CriteriaText.text = \"Invalid criteria\";
  }
  else
  {
    int nCriteria = 0;
    for(int n = dynlen(types) ; n > 0 ; n--)
    {
      if(types[n] != CRIT_TYPE_NONE)
      {
        nCriteria++;
      }
    }
    string text = \"\" + nCriteria + \" criteria\";
    if(nCriteria > 1)
    {
      text += \" joined by \";
      if(joinType == CRIT_JOIN_TYPE_AND)
      {
        text += \"AND\";
        emptyCriteria = false;
      }
      else if(joinType == CRIT_JOIN_TYPE_OR)
      {
        text += \"OR\";
        emptyCriteria = false;
      }
      else
      {
        text += \"no logic\";
        emptyCriteria = true;
      }
    }
    else
    {
      emptyCriteria = false;
    }
    CriteriaText.text = text;
  }
  UpdateSensitivity();
  SetResetPbNeeded();
}

void ScopeCb(string dpe1, int type, string dpe2, dyn_string names)
{
  if(dynlen(names) == 0)
  {
    emptyScope = true;
    ScopeText.text = \"Not defined\";
  }
  else
  {
    string text = dynlen(names) + \" \";
    switch(type)
    {
    case SCOPE_TYPE_SECTOR:
      text += \"sector(s)\";
      emptyScope = false;
      break;
    case SCOPE_TYPE_MAIN_PART:
      text += \"main part(s)\";
      emptyScope = false;
      break;
    case SCOPE_TYPE_ARC:
      text += \"LHC arc(s)\";
      emptyScope = false;
      break;
    case SCOPE_TYPE_EQP:
      text += \"device(s)\";
      emptyScope = false;
      break;
    case SCOPE_TYPE_GLOBAL:
      text = \"Global scope\";
      break;
    default:
      text += \"unknwon name(s)\";
      emptyScope = true;
      break;
    }
    ScopeText.text = text;
  }
  UpdateSensitivity();
  SetResetPbNeeded();
}


void FilteringCb(string dpe1, int type, string dpe2, int deadTime,
  string dpe3, int groupType, string dpe4, int groupInterval,
  string dpe5, int groupCount, string dpe6, string groupMsg)
{
  string text;
  bool groupTypeOk = true;
  switch(type)
  {
  case MSG_FILTER_TYPE_NONE:
    FilteringText.text = \"No filtering\";
    emptyFiltering = false;
    break;
  case MSG_FILTER_TYPE_DEAD_TIME:
    FilteringText.text = \"Dead time \" + deadTime + \" [sec]\";
    emptyFiltering = false;
    break;
  case MSG_FILTER_TYPE_GROUPING:
    text = \"Group \" + groupCount + \" msg(s) in \" + groupInterval + \" sec by \";
    switch(groupType)
    {
    case MSG_GROUPING_MACHINE:
      text += \"machine\";
      break;
    case MSG_GROUPING_MAIN_PART:
      text += \"main part\";
      break;
    case MSG_GROUPING_SECTOR:
      text += \"sector\";
      break;
    case MSG_GROUPING_ARC:
      text += \"LHC arc\";
      break;
    default:
      text += \"unknown (\" + groupType + \")\";
      groupTypeOk = false;
      break;
    }
    FilteringText.text = text;
    if(groupTypeOk)
    {
      if((groupInterval > 0) && (groupCount > 0) && (strltrim(strrtrim(groupMsg)) != \"\"))
      {
        emptyFiltering = false;
      }
      else
      {
        emptyFiltering = true;
      }
    }
    else
    {
      emptyFiltering = true;
    }
    break;
  }
  UpdateSensitivity();
  SetResetPbNeeded();
}


void RecipientsCb(string dpe, dyn_string recipients)
{
  if(dynlen(recipients) == 0)
  {
    RecipientsText.text = \"No recipients\";
    emptyRecipients = true;
  }
  else
  {
    RecipientsText.text = dynlen(recipients) + \" recipient(s)\";
    emptyRecipients = false;
  }
  UpdateSensitivity();
}

void MessageCb(string dpe, string message)
{
  MessageText.text = message;
  UpdateSensitivity();
}

void SetNewMessage()
{
  if(!isMyConfig)
  {
    return;
  }
  string oldMessage;
  dpGet(dpName + \".Message\", oldMessage);
  string newMessage = strltrim(strrtrim(MessageText.text));
  if(oldMessage == newMessage)
  {
    return;
  }
  dpSet(dpName + \".Message\", newMessage);
}

void SetResetPbNeeded()
{
  if(!initialized)
  {
    return;
  }
  if(!isMyConfig)
  {
    return;
  }
  ResetCounterPb.backCol = \"yellow\";
}
" 0
 3
"CBRef" "1"
"EClose" E
"dpi" "120"
0 0 0
""
DISPLAY_LAYER, 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0
LAYER, 0 
1
LANG:1 6 Layer1
2 0
"Text1"
""
1 10 4 E E E 1 E 1 E N "_WindowText" E N "_Transparent" E E
 E E
1 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 12 6 78 22
0 2 0 "0s" 0 0 0 64 0 0  12 6 1
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 1
LANG:1 11 Description
30 2
"FrameCriteria"
""
1 10 90 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
3 0 0 0 0 0
E E E
1
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E 1 0 1 -5 185 0 E 10 40 260 90
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 1
LANG:1 8 Criteria
2 4
"CriteriaText"
""
1 15 245 E E E 1 E 1 E N "_WindowText" E N "_Transparent" E E
 E "main()
{
  /*
  if(!MayEditConfiguration())
  {
    return;
  }
  */
  ChildPanelOnCentralModal(\"vision/sms/SmsConfigCriteria.pnl\", \"Configuration Criteria\",
    makeDynString(\"$dpName:\" + dpName));
}" 0

5 0 0 0 0 0
E E E
0
1
LANG:1 29 Double-click to edit criteria

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 17 247 243 263
0 2 0 "0s" 0 0 0 64 0 0  17 247 1
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0 1
LANG:1 8 criteria
30 5
"FrameScope"
""
1 270 90 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
6 0 0 0 0 0
E E E
1
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E 1 0 1 -5 185 0 E 270 40 520 90
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 1
LANG:1 5 Scope
2 6
"ScopeText"
""
1 275 245 E E E 1 E 1 E N "_WindowText" E N "_Transparent" E E
 E "main()
{
  /*
  if(!MayEditConfiguration())
  {
    return;
  }
  */
  ChildPanelOnCentralModal(\"vision/sms/SmsConfigScope.pnl\", \"Configuration Scope\",
    makeDynString(\"$dpName:\" + dpName));
}" 0

7 0 0 0 0 0
E E E
0
1
LANG:1 26 Double-click to edit scope

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 277 247 503 263
0 2 0 "0s" 0 0 0 64 0 0  277 247 1
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0 1
LANG:1 5 scope
30 7
"FrameFiltering"
""
1 10 150 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
8 0 0 0 0 0
E E E
1
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E 1 0 1 -5 185 0 E 10 100 260 150
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 1
LANG:1 9 Filtering
2 8
"FilteringText"
""
1 15 305 E E E 1 E 1 E N "_WindowText" E N "_Transparent" E E
 E "main()
{
  /*
  if(!MayEditConfiguration())
  {
    return;
  }
  */
  ChildPanelOnCentralModal(\"vision/sms/SmsConfigFiltering.pnl\", \"Message Filtering\",
    makeDynString(\"$dpName:\" + dpName));
}" 0

9 0 0 0 0 0
E E E
0
1
LANG:1 30 Double-click to edit filtering

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 17 307 243 323
0 2 0 "0s" 0 0 0 64 0 0  17 307 1
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0 1
LANG:1 9 filtering
30 9
"FrameRecipients"
""
1 270 150 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
10 0 0 0 0 0
E E E
1
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E 1 0 1 -5 185 0 E 270 100 520 150
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 1
LANG:1 10 Recipients
2 10
"RecipientsText"
""
1 273 303 E E E 1 E 1 E N "_WindowText" E N "_Transparent" E E
 E "main()
{
  ChildPanelOnCentralModal(\"vision/sms/SmsConfigUsers.pnl\", \"Configuration Scope\",
    makeDynString(\"$dpName:\" + dpName));
}" 0

11 0 0 0 0 0
E E E
0
1
LANG:1 35 Double-click to edit recipient list

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 275 305 501 321
0 2 0 "0s" 0 0 0 64 0 0  275 305 1
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0 1
LANG:1 10 recipients
30 11
"FrameMessage"
""
1 10 210 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
12 0 0 0 0 0
E E E
1
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E 1 0 1 -5 185 0 E 10 160 520 210
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0 1
LANG:1 7 Message
30 13
"FrameState"
""
1 10 330 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
14 0 0 0 0 0
E E E
1
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E 1 0 1 -5 185 0 E 10 220 520 330
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 1
LANG:1 14 State/Commands
2 16
"ActivateText"
""
1 15 425 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
17 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 17 427 173 443
1 2 0 "0s" 0 0 0 64 0 0  17 427 1
1
LANG:1 26 Arial,-1,13,5,69,0,0,0,0,0
0 1
LANG:1 21 Activate command sent
2 17
"ActivatedText"
""
1 355 425 E E E 1 E 1 E N {0,255,0} E N "_Transparent" E E
 E E
18 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 357 427 423 443
1 2 0 "0s" 0 0 0 65 0 0  357 427 1
1
LANG:1 26 Arial,-1,13,5,69,0,0,0,0,0
0 1
LANG:1 9 Activated
2 18
"InitializedText"
""
1 435 425 E E E 1 E 1 E N {0,255,0} E N "_Transparent" E E
 E E
19 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 437 427 503 443
1 2 0 "0s" 0 0 0 65 0 0  437 427 1
1
LANG:1 26 Arial,-1,13,5,69,0,0,0,0,0
0 1
LANG:1 11 Initialized
2 21
"MsgCountText"
""
1 15 485 E E E 1 E 1 E N "_WindowText" E N "_Transparent" E E
 E E
22 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 17 487 283 503
0 2 0 "0s" 0 0 0 64 0 0  17 487 1
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 1
LANG:1 16 Messages sent: 0
14 1
"DescriptionText"
""
1 86 4 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
2 0 0 0 0 0
E "main()
{
  SetNewDescription();
}" 0
 E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  84 2 522 29
3 "0s" 0 0 0 0 0 -1  E E "main()
{
  SetNewDescription();
}" 0

14 12
"MessageText"
""
1 15 365 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
13 0 0 0 0 0
E "main()
{
  SetNewMessage();
}" 0
 E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  13 363 507 390
3 "0s" 0 0 0 0 0 -1  E E "main()
{
  SetNewMessage();
}" 0

14 15
"ErrorText"
""
1 15 455 E E E 1 E 1 E N "_WindowText" E N "_3DFace" E E
 E E
16 0 0 0 0 0
E E E
0
1
LANG:1 13 Error message

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  13 453 507 480
2 "0s" 0 0 0 0 0 -1  E E E
13 19
"ActivatePb"
""
1 185 425 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
20 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  183 423 257 447

T 
1
LANG:1 8 Activate
"main()
{
  if(!isMyConfig)
  {
    return;
  }
  dpSet(dpName + \".State.Activate\", true);
}" 0
 E E E
13 20
"DeactivatePb"
""
1 265 425 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
21 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  263 423 337 447

T 
1
LANG:1 10 Deactivate
"main()
{
  if(!isMyConfig)
  {
    return;
  }
  dpSet(dpName + \".State.Activate\", false);
}" 0
 E E E
13 22
"ClosePb"
""
1 325 525 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
23 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  323 523 377 548

T 
1
LANG:1 5 Close
"main()
{
  PanelOff();
}" 0
 E E E
13 23
"CriteriaPb"
""
1 15 225 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
24 0 0 0 0 0
E E E
0
1
LANG:1 20 Edit device criteria

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0  13 223 91 247

T 
1
LANG:1 8 Criteria
"main()
{
  /*
  if(!MayEditConfiguration())
  {
    return;
  }
  */
  ChildPanelOnCentralModal(\"vision/sms/SmsConfigCriteria.pnl\", \"Configuration Criteria\",
    makeDynString(\"$dpName:\" + dpName));
}" 0
 E E E
13 24
"ScopePb"
""
1 275 225 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
25 0 0 0 0 0
E E E
0
1
LANG:1 31 Edit scope for device selection

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0  273 223 351 247

T 
1
LANG:1 5 Scope
"main()
{
  /*
  if(!MayEditConfiguration())
  {
    return;
  }
  */
  ChildPanelOnCentralModal(\"vision/sms/SmsConfigScope.pnl\", \"Configuration Scope\",
    makeDynString(\"$dpName:\" + dpName));

}" 0
 E E E
13 25
"FilteringPb"
""
1 15 285 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
26 0 0 0 0 0
E E E
0
1
LANG:1 43 Edit rules for filtering resulting messages

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0  13 283 91 307

T 
1
LANG:1 9 Filtering
"main()
{
  /*
  if(!MayEditConfiguration())
  {
    return;
  }
  */
  ChildPanelOnCentralModal(\"vision/sms/SmsConfigFiltering.pnl\", \"Message Filtering\",
    makeDynString(\"$dpName:\" + dpName));
}" 0
 E E E
13 26
"RecipientsPb"
""
1 275 285 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
27 0 0 0 0 0
E E E
0
1
LANG:1 46 Edit list of persons who will receive messages

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0  273 283 351 307

T 
1
LANG:1 10 Recipients
"main()
{
  ChildPanelOnCentralModal(\"vision/sms/SmsConfigUsers.pnl\", \"Configuration Recipients\",
    makeDynString(\"$dpName:\" + dpName));
}" 0
 E E E
30 27
"FrameModes"
""
1 4 25 E E E 1 E 1 E N "_WindowText" E N {0,0,0} E E
 E E
28 0 0 0 0 0
E E E
1
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 0 1 0 1 E 1 0 2.53246753246753 0 -38.31168831168825 0 E 4 25 521 103
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0 1
LANG:1 12 Machine Mode
13 32
"ResetCounterPb"
""
1 298 485 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
36 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0  296 483 437 509

T 
1
LANG:1 18 Reset msg. counter
"main()
{
  if(isMyConfig)
  {
    dpSet(dpName + \".State.MessagesSent\", 0,
          dpName + \".State.EventCount\", 0);
  }
  ResetCounterPb.backCol = \"_Button\";
}" 0
 E E E
25 34
"TabMachineMode"
""
1 11 43 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
38 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0  9 41 512 213
E"main(int row, string column, string value)
{
  if(!isMyConfig)
  {
    return;
  }
  unsigned mode = 0;
  for(int row = 0 ; row < TabMachineMode.lineCount ; row++)
  {
    bool use;
    getValue(\"TabMachineMode\", \"cellValueRC\", row, \"use\", use);
    if(!use)
    {
      continue;
    }
    int idx;
    getValue(\"TabMachineMode\", \"cellValueRC\", row, \"mode\", idx);
    mode |= (1 << idx);
  }

  unsigned oldMode;
  dpGet(dpName + \".Mode\", oldMode);
  if(mode == oldMode)
  {
    return;
  }
  dpSet(dpName + \".Mode\", mode);
}" 0
 1 0 1 3 0 "use" 2 1 1 "s" 1
LANG:1 3 Use
E
1
LANG:1 26 Send message for this mode

32 "modeName" 31 1 0 "s" 1
LANG:1 12 Machine mode
E
1
LANG:1 12 Machine mode

350 "mode" 4 0 0 "s" 1
LANG:1 4 Mode
E
1
LANG:1 0 

60 
18 18 10 10
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0 0 1 1 1 7
1 0
13 35
"PUSH_BUTTON1"
""
1 115 523.5 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
39 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  113 521.5 187 549.5

T 
1
LANG:1 4 Help
"main()
{
	ShowHelpTopic(\"notification\");

}" 0
 E E E
0
LAYER, 1 
1
LANG:1 6 Layer2
0
LAYER, 2 
1
LANG:1 6 Layer3
0
LAYER, 3 
1
LANG:1 6 Layer4
0
LAYER, 4 
1
LANG:1 6 Layer5
0
LAYER, 5 
1
LANG:1 6 Layer6
0
LAYER, 6 
1
LANG:1 6 Layer7
0
LAYER, 7 
1
LANG:1 6 Layer8
0
0
