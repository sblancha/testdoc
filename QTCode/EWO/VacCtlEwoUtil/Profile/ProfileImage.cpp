//	Implementation of ProfileImage class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileImage.h"
#include "BeamLineStats.h"

#include "ExtendedLabel.h"

#include "DataPool.h"
#include "BeamLine.h"
#include "BeamLinePart.h"
#include "Sector.h"
#include "SectorDrawPart.h"
#include "Eqp.h"
#include "EqpType.h"

#include "VacMainView.h"

#include <QPixmap>
#include <QTimer>
#include <QSpinBox>
#include <QMouseEvent>
#include <QApplication>

#include <math.h>

#include "PlatformDef.h"
#include "DebugCtl.h"

/*
**	FUNCTION
**		Create new instance of profile image.
**
**	PARAMETERS
**		parent			- Parent widget for new profile image
**		pStartSector	- Pointer to start sector of profile
**		pEndSector		- Pointer to end sector of profile
**		mode			- Data acquisition mode for this view
**		allEqpMask		- Equipment type mask containig all allowed types
**		errMsg			- Variable where error message shall be written
**							in case of error
**
**	RETURNS
**		Pointer to new instance, or
**		NULL in case of error
**
**	CAUTIONS
**		None
*/
ProfileImage *ProfileImage::create(QWidget *parent,
	Sector *pStartSector, Sector *pEndSector, DataEnum::DataMode mode,
	VacEqpTypeMask &allEqpMask, QString &errMsg)
{
	ProfileImage *pView = new ProfileImage(parent);
	pView->mode = mode;
	pView->pStartSector = pStartSector;
	pView->pEndSector = pEndSector;
	pView->allowedEqpMask = allEqpMask;
	if(!pView->build())
	{
		errMsg = "No equipment found between <";
		errMsg += pStartSector->getName();
		errMsg += "> and <";
		errMsg += pEndSector->getName();
		errMsg += ">";
		delete pView;
		pView = NULL;
	}
	return pView;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

ProfileImage::ProfileImage(QWidget *parent, Qt::WindowFlags f) :
		QWidget(parent, f)
{
	setAutoFillBackground(true);
	pAxis = new VerticalAxis(true, true);
	pStartSector = pEndSector = NULL;
	nBarsToDraw = 0;
	viewWidth = viewHeight = 0;
	eqpMaskApplied = newEqpMaskApplied = redrawRequired = false;
	flashBarsOnly = useAltColor = false;
	showSectorNames = false;
	setMinimumSize(QSize(400, 300));

	pLabel = new ExtendedLabel(this);
	pLabel->setLabelType(ExtendedLabel::Transparent);
	pLabel->setBackColor(QColor(255, 255, 204, 103));
	pLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	pLabel->hide();

	pTimer = new QTimer(this);
	connect(pTimer, SIGNAL(timeout()), pLabel, SLOT(hide()));

	pRedrawTimer = new QTimer(this);
	pRedrawTimer->setSingleShot(true);
	connect(pRedrawTimer, SIGNAL(timeout()), this, SLOT(redrawTimerDone()));

	pSpinBox = new QSpinBox(this);
	pSpinBox->setRange(-18, 4);
	pSpinBox->setSingleStep(1);
	QString prefix("1E");
	pSpinBox->setPrefix(prefix);
	pSpinBox->hide();
	QObject::connect(pSpinBox, SIGNAL(valueChanged(int)), this, SLOT(spinBoxChange(int)));

	setMouseTracking(true);

	// Finally...
	rebuildImage();
}

ProfileImage::~ProfileImage()
{
	clear();
	if(pAxis)
	{
		delete pAxis;
		pAxis = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////////
// Event processing
/////////////////////////////////////////////////////////////////////////////////
void ProfileImage::resizeEvent(QResizeEvent *event)
{
	rebuildImage();
	QWidget::resizeEvent(event);
}

void ProfileImage::mouseMoveEvent(QMouseEvent *pEvent)
{
	ProfileItem *pItem = findItemUnderMouse(pEvent->x(), pEvent->y());
	if(pItem)
	{
		showBarLabel(pItem, pEvent->y());
	}
	else
	{
		pLabel->hide();
		if(pEvent->x() < pAxis->getAxisLocation())
		{
			showScaleControl(pEvent->y());
		}
		else
		{
			pSpinBox->hide();
		}
	}
}

void ProfileImage::mousePressEvent(QMouseEvent *pEvent)
{
	ProfileItem *pItem = findItemUnderMouse(pEvent->x(), pEvent->y());
	if(pItem)
	{
		int buttonNbr = 0;
		switch(pEvent->button())
		{
		case Qt::LeftButton:
			buttonNbr = 1;
			break;
		case Qt::RightButton:
			buttonNbr = 2;
			break;
		default:	// Just to make Linux compiler happy
			break;
		}
		if(!buttonNbr)
		{
			return;
		}
		QPoint point = mapToGlobal(pEvent->pos());
		emit dpMouseDown(buttonNbr, mode, point.x(), point.y(), pItem->getEqp()->getDpName());
	}
}

void ProfileImage::mouseDoubleClickEvent(QMouseEvent *pEvent)
{
	ProfileItem *pItem = findItemUnderMouse(pEvent->x(), pEvent->y());
	if(pItem)
	{
		VacMainView *pMainView = VacMainView::getInstance();
		if(pMainView)
		{
			pMainView->emitEqpDoubleClick(pItem->getEqp()->getDpName(), mode);
		}
	}
}


/*
**	FUNCTION
**		Clear content of view
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileImage::clear(void)
{
	while(!sectors.isEmpty())
	{
		delete sectors.takeFirst();
	}
	while(!eqps.isEmpty())
	{
		delete eqps.takeFirst();
	}
	sectorUsage.clear();
	sectorPartPass.clear();
}

/*
**	FUNCTION
**		Analyze which equipment types may be shown in profile
**
**	PARAMETERS
**		vacTypeMask	- Variable where resulting bit mask for all vacuum types will be written
**		beamEqpMask	- Variable where resulting functional type mask for all equipment
**						on beam vacuum will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileImage::analyzeTypes(unsigned &vacTypeMask, VacEqpTypeMask &beamEqpMask)
{
	vacTypeMask = 0;
	beamEqpMask.clear();

	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		ProfileItem *pItem = eqps.at(idx);
		if(pItem->getEqp()->isActive(mode))
		{
			vacTypeMask |= pItem->getEqp()->getVacType();
			beamEqpMask.append(pItem->getEqp()->getFunctionalType());
		}
	}
}

/*
**	FUNCTION
**		Build list of equipment to be shown in profile for given sector range.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Number of devices added to profile
**
**	CAUTIONS
**		None
*/
int ProfileImage::build(void)
{
	BeamLine *pMainLine = findMainLine();

	clear();

	// Pass all draw parts of sectors, first add equipment of draw parts
	// located on main line, then add equipment on other draw parts
	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pStartSector;
	do
	{
		sectorUsage.append(pSector);
		QList<SectorDrawPart *> &drawParts = pSector->getDrawParts();
		SectorDrawPart *pDrawPart;
		int idx;
		for(idx = 0 ; idx < drawParts.count() ; idx++)
		{
			pDrawPart = drawParts.at(idx);
			if(pDrawPart->getLine() == pMainLine)
			{
				buildEqpOnLine(pSector, pDrawPart);
			}
		}
		for(idx = drawParts.count() - 1 ; idx >= 0 ; idx--)
		{
			pDrawPart = drawParts.at(idx);
			if(!sectorPartPass.contains(pDrawPart))
			{
				buildEqpOnLine(pSector, pDrawPart);
			}
		}
		if(pSector == pEndSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);

	return eqps.count();
}

/*
**	FUNCTION
**		Set new limits for vertical scale
**
**	PARAMETERS
**		newMin	- New scale minimum
**		newMax	- New scale maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileImage::setLimits(int newMin, int newMax)
{
	pAxis->setLimits(newMin, newMax);
	rebuildImage();
}

/*
**	FUNCTION
**		Calculate hide/show parameters for all devices using
**		new equipment type masks
**
**	PARAMETERS
**		newMask		- Mask for equipment functional types to show
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileImage::applyMask(const VacEqpTypeMask &newMask)
{
	newEqpMaskApplied = ! eqpMask.equals(newMask);
	eqpMask = newMask;
	eqpMaskApplied = true;
	nBarsToDraw = 0;
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		ProfileItem *pItem = eqps.at(idx);
		Eqp *pEqp = pItem->getEqp();
		if(eqpMask.contains(pEqp->getFunctionalType()))	// To be shown
		{
			if(pItem->isHidden())
			{
				pItem->show();
			}
			if(pItem->shallBeVisible())
			{
				nBarsToDraw++;
			}
		}
		else
		{
			if(!pItem->isHidden())
			{
				pItem->hide();
			}
		}
	}
	rebuildImage();
}

/*
**	FUNCTION
**		Slot activated when redraw timeout expired
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileImage::redrawTimerDone(void)
{
	rebuildImage();
}

/*
**	FUNCTION
**		Build new profile image, set image as background for this widget
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileImage::rebuildImage(void)
{
	pRedrawTimer->stop();
	QPixmap bgPixmap(size());
	QPalette pal = QApplication::palette();
	bgPixmap.fill(pal.color(QPalette::Window));
	QPainter painter(&bgPixmap);
	if(prepare(painter))
	{
		if(nBarsToDraw)
		{
			drawBars(painter);
		}
	}
	pal.setBrush(QPalette::Window, bgPixmap);
	setPalette(pal);
	drawTime = QTime::currentTime();
}


/*
**	FUNCTION
**		Find 'main' line of profile: beam line containing the most of sectors
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Pointer to the best candidate for 'main' line
**
**	CAUTIONS
**		None
**
*/
BeamLine *ProfileImage::findMainLine(void)
{
	sectorPartPass.clear();	// useful 'side effect'
	BeamLineStats stats;

	// Pass all draw parts of sectors, increment weight of corresponding line
	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pStartSector;
	do
	{
		const QList<SectorDrawPart *> &drawParts = pSector->getDrawParts();
		for(int idx = drawParts.count() - 1 ; idx >= 0 ; idx--)
		{
			SectorDrawPart *pPart = drawParts.at(idx);
			stats.increment(pPart->getLine());
		}
		if(pSector == pEndSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);

	return stats.getBestLine();
}

/*
**	FUNCTION
**		Pass all equipment on given draw part of sector, add relevant
**		equipment to profile
**
**	PARAMETERS
**		pSector		- Pointer to sector being processed
**		pDrawPart	- Pointer to draw part being processed
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileImage::buildEqpOnLine(Sector *pSector, SectorDrawPart *pDrawPart)
{
	sectorPartPass.append(pDrawPart);

	BeamLine		*pLine = pDrawPart->getLine();
	BeamLinePart	*pLinePart = pDrawPart->getStartPart();
	int				eqpIdx = pDrawPart->getStartEqpIdx();
	BeamLinePart	*pEndPart = pDrawPart->getEndPart();
	int				endEqpIdx = pDrawPart->getEndEqpIdx();

	const QList<SectorDrawPart *> &drawParts = pSector->getDrawParts();
	SectorDrawPart *pOtherPart;
	while(true)
	{
		Eqp **eqpPtrs = pLinePart->getEqpPtrs();
		int nEqpPtrs = pLinePart->getNEqpPtrs();

		do
		{
			Eqp *pEqp = eqpPtrs[eqpIdx];
			switch(pEqp->getType())
			{
			case EqpType::OtherLineStart:
			case EqpType::OtherLineEnd:
				for(int idx = 0 ; idx < drawParts.count() ; idx++)
				{
					pOtherPart = drawParts.at(idx);
					if(sectorPartPass.contains(pOtherPart))
					{
						continue;
					}
					BeamLine	*pOtherLine = pOtherPart->getLine();
					if(!strcmp(pOtherLine->getName(), pEqp->getName()))
					{
						buildEqpOnLine(pSector, pOtherPart);
					}
				}
				break;
			case EqpType::VacDevice:
				if(allowedEqpMask.contains(pEqp->getFunctionalType()))
				{
					addDevice(pEqp);
				}
				break;
			}
			if((pLinePart == pEndPart) && (eqpIdx == endEqpIdx))
			{
				break;
			}
		} while(++eqpIdx <nEqpPtrs);

		if((pLinePart == pEndPart) && (eqpIdx == endEqpIdx))
		{
			break;
		}
		pLinePart = pLine->nextPart(pLinePart);
		if(!pLinePart)
		{
			break;
		}
		eqpIdx = 0;
	}
}

/*
**	FUNCTION
**		Add new device to profile
**
**	PARAMETERS
**		pEqp		- Pointer to device to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileImage::addDevice(Eqp *pEqp)
{
	ProfileItem *pItem;

	// Check if device has been added already
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		pItem = eqps.at(idx);
		if(pItem->getEqp() == pEqp)
		{
			return;
		}
	}
	pItem = new ProfileItem(pEqp, mode);
	QObject::connect(pItem, SIGNAL(eqpDataChanged(ProfileItem *, bool)),
		this, SLOT(eqpDataChange(ProfileItem *, bool)));
	QObject::connect(pItem, SIGNAL(eqpSelectChanged(ProfileItem *)),
		this, SLOT(eqpSelectChange(ProfileItem *)));
	QObject::connect(pItem, SIGNAL(eqpSetChanged()), this, SLOT(mobileVisibilityChanged()));
	QObject::connect(pItem, SIGNAL(visibilityChanged(ProfileItem *)), this, SLOT(itemVisibilityChanged(ProfileItem *)));
	QObject::connect(pItem, SIGNAL(eqpMaskChanged()), this, SIGNAL(eqpMaskChanged()));
	eqps.append(pItem);
}

/*
**
**	FUNCTION
**		Prepare all data for drawing, draw axis and sectors background if necessary
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		true in case of success
**		false in case of error
**
**	CAUTIONS
**		None
**
*/
bool ProfileImage::prepare(QPainter &painter)
{
	if(flashBarsOnly)
	{
		return true;
	}

	// Prepare vertical axis
	QRect viewRect = rect();
	viewRect.setRight(viewRect.right() - 1);
	pAxis->setGraphArea(viewRect);
	pAxis->fixMinAt(calculateSectorLabels(painter, viewRect));
	if(pAxis->prepare(painter.fontMetrics()) || newEqpMaskApplied || redrawRequired ||
		(width() != viewWidth) || (height() != viewHeight))
	{
		// Build all geometries (bars, sector areas)
		if(redrawRequired)
		{
			redrawRequired = false;
			applyMask(eqpMask);
		}
		viewWidth = width();
		viewHeight = height();
		buildBarGeom();
		buildSectors(painter, viewRect);
		if(DebugCtl::isProfile())
		{
			const char *fileName;
			#ifdef Q_OS_WIN
				fileName = "C:\\ProfileImage.txt";
			#else
				fileName = "/user/vacin/ProfileImage.txt";
			#endif
			dump(fileName);
		}
	}

	// Draw sector areas
	if(sectors.count())
	{
		drawSectors(painter);
	}

	// Draw rectangle, axis and Y-axis labels
	painter.setPen(VacMainView::getGraphAxisColor());
	pAxis->setGridLineEnd(viewWidth);
	pAxis->draw(painter);
	painter.drawLine(pAxis->getAxisLocation(), pAxis->getScaleTop(),
		pAxis->getAxisLocation(), pAxis->getScaleBottom());
	painter.drawLine(viewRect.right(), pAxis->getScaleTop(),
		viewRect.right(), pAxis->getScaleBottom());
	return true;
}

/*
**	FUNCTION
**		Build geometry of all bars to be shown in profile
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Vertical axis must be prepared before calling this method.
**		Equipment mask shall also be prepared before calling this method
*/
void ProfileImage::buildBarGeom(void)
{
	if(!nBarsToDraw)
	{
		return;
	}

	// Calculate graph area limits
	int leftX = pAxis->getAxisLocation();
	int rightX = viewWidth - 2;
	
	// Calculate vertical scale coefficients
	int	useMin, useMax;
	pAxis->getLimits(useMin, useMax);
	double scaleHeight = useMax - useMin;
	double scaleMax = pow(10.0, useMax);
	double scaleMin = pow(10.0, useMin);

	// Calculate one bar width and horizontal scale coefficients
	double xScale = (double)(rightX - leftX) / (double)nBarsToDraw;
	double oneBarWidth = xScale * ((double)VacMainView::getProfileBarWidth() / 100.0);
	bool useNarrowBar = oneBarWidth < 2.0;

	// Calculate geometry of all bars
	double barNumber = 0.0;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		ProfileItem *pItem = eqps.at(idx);
		if(pItem->isHidden())
		{
			continue;
		}
		if(!pItem->shallBeVisible())
		{
			continue;
		}
		if(useNarrowBar)
		{
			pItem->setNarrow((int)rint(leftX + (barNumber + 0.5) * xScale));
		}
		else
		{
			int left = (int)rint(leftX + (barNumber + 0.5) * xScale - oneBarWidth / 2.);
			pItem->setWide(left, (int)rint(left + oneBarWidth));
		}
		barVerticalGeom(pItem, scaleMin, scaleMax, scaleHeight, useMin);
		barNumber += 1.0;
	}
}

/*
**	FUNCTION
**		Calculate vertical bar geometry for single profile item
**
**	PARAMETERS
**		pItem		- Pointer to profile item
**		scaleMin	- Minimum of vertical scale
**		scaleMax	- Maximum of vertical scale
**		scaleHeight	- Height of vertical scale in LOGARITHMIC units
**		useMin		- Logarithm of vertical scale minimum
**
**	RETURNS
**		true	- if bar top or bootm coordinate is changed;
**		false	- otherwise
**
**	CAUTIONS
**		None
**
*/
bool ProfileImage::barVerticalGeom(ProfileItem *pItem, double scaleMin, double scaleMax,
	double scaleHeight, int useMin)
{
	int barTop, barBottom;
	if(pItem->isValueValid())
	{
		double value = pItem->getValue();
		if(value > scaleMin)
		{
			barBottom = pAxis->getScaleBottom() - 1;
		}
		else
		{
			barBottom = pAxis->getScaleBottom() + 2;
		}
		if(value <= scaleMax)
		{
			if(value > scaleMin)
			{
				barTop = (int)rint(pAxis->getScaleTop() +
					(pAxis->getScaleBottom() - pAxis->getScaleTop()) *
					(1.0 - (log10(value) - useMin) / scaleHeight));
			}
			else
			{
				barTop = pAxis->getScaleBottom();
			}
		}
		else
		{
			barTop = pAxis->getScaleTop() - 4;
		}
	}
	else
	{
		barBottom = pAxis->getScaleBottom() + 1;
		barTop = pAxis->getScaleTop() + 2;
	}

	if((barTop != pItem->getTop()) || (barBottom != pItem->getBottom()))
	{
		pItem->setTop(barTop);
		pItem->setBottom(barBottom);
		return true;
	}
	return false;
}

/*
**	FUNCTION
**		Build geometry of all sector areas be shown in profile
**
**	PARAMETERS
**		None
**
** 	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileImage::buildSectors(QPainter &painter, const QRect &viewRect)
{
	sectors.clear();
	if(!nBarsToDraw)
	{
		return;
	}
	ProfileSector sector(NULL);
	int lastBar = -1, idx;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		ProfileItem *pItem = eqps.at(idx);
		if(pItem->isHidden())
		{
			continue;
		}
		if(!pItem->shallBeVisible())
		{
			continue;
		}
		Eqp *pEqp = pItem->getEqp();
		if(sectorUsage.contains(pEqp->getSectorBefore(mode)))
		{
			if(sector.getLeft() < 0)
			{
				sector.setLeft(pItem->getLeft());
			}
			if(sector.getSector() != pEqp->getSectorBefore(mode))
			{
				sector.setRight(lastBar);
				addSector(sector, false, pEqp->getSectorBefore(mode));
			}
			else if(pEqp->isSectorBorder(mode))
			{
				if(sectorUsage.contains(pEqp->getSectorAfter(mode)))
				{
					sector.setRight((pItem->getLeft() + pItem->getRight()) / 2);
					addSector(sector, true, pEqp->getSectorAfter(mode));
				}
			}
		}
		else if(sectorUsage.contains(pEqp->getSectorAfter(mode)))
		{
			if(sector.getSector() != pEqp->getSectorAfter(mode))
			{
				sector.setRight(lastBar);
				addSector(sector, false, pEqp->getSectorAfter(mode));
			}
		}
		lastBar = pEqp->isSectorBorder(mode) ?
			(pItem->getLeft() + pItem->getRight()) / 2 : pItem->getRight();
		if(sector.getLeft() < 0)
		{
			sector.setLeft(pItem->getLeft());
		}
	}
	// qDebug("End of cycle: left = %d\n", sector.getLeft());
	addSector(sector, false, sector.getSector());
	// qDebug("total sectors %d\n", sectors.count());

	if(!sectors.count())
	{
		return;
	}

	// Set borders for first and last sectors
	sectors.first()->setLeft(pAxis->getAxisLocation());
	sectors.last()->setRight(width() - 1);

	// Correct (if necessary) borders of intermediate sectors for
	// exact border match
	for(idx = 0 ; idx < sectors.count() ; )
	{
		ProfileSector *pSector = sectors.at(idx++);
		if(idx >= sectors.count())
		{
			break;
		}
		ProfileSector *pNextSector = sectors.at(idx);
		if(!pNextSector)
		{
			break;
		}
		if(pSector->isRightWithinDevice())
		{
			pNextSector->setLeft(pSector->getRight());
		}
		else if(pNextSector->isLeftWithinDevice())
		{
			pSector->setRight(pNextSector->getLeft());
		}
		else
		{
			pSector->setRight((pSector->getRight() + pNextSector->getLeft()) / 2);
			pNextSector->setLeft(pSector->getRight());
		}
	}
	pAxis->fixMinAt(calculateSectorLabels(painter, viewRect));
	if(pAxis->prepare(painter.fontMetrics()))
	{
		buildBarGeom();
	}
}

/*
**	FUNCTION
**		Add one sector area to list of sector areas in profile
**
**	PARAMETERS
**		source			- Partially filled sector area parameters to be used as source
**		withinDevice	- Flag indicating if sector border area is within device
**		pSector			- Pointer to vacuum sector to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileImage::addSector(ProfileSector &source, bool withinDevice, Sector *pSector)
{
	source.setRightWithinDevice(withinDevice);
	if((source.getLeft() >= 0) && source.getSector())
	{
		ProfileSector *pNewSector = new ProfileSector(source);
		// pNewSector->setSector(pSector);
		pNewSector->setSector(source.getSector());
		sectors.append(pNewSector);
	}

	source.setLeft(-1);
	source.setLeftWithinDevice(source.isRightWithinDevice());
	source.setRight(-1);
	source.setRightWithinDevice(false);
	source.setSector(pSector);
}

/*
**	FUNCTION
**		Calculate geometry for drawing sector labels
**
**	PARAMETERS
**		painter		- Painter which will be used for drawing
**		viewRect	- Rectangle used for profile drawing
**
**	RETURNS
**		Bottom position of vertical axis minimum, area below this position will be
**		used to draw sector labels
**
**	CAUTIONS
**		None
**
*/
int ProfileImage::calculateSectorLabels(QPainter &painter, const QRect &viewRect)
{
	if((!showSectorNames) || (!sectors.count()))
	{
		return -1;
	}

	// Calculate label rectangle for all sectors, check if they overlap
	int maxHeight = 0;
	bool overlap = false;
	QRect lastRect;
	for(int idx = 0 ; idx < sectors.count() ; idx++)
	{
		ProfileSector *pSector = sectors.at(idx);
		pSector->calculateLabelRect(painter, viewRect.bottom());
		const QRect &labelRect = pSector->getLabelRect();
		if(maxHeight < labelRect.height())
		{
			maxHeight = labelRect.height();
		}
		if(!overlap)
		{
			if(idx)
			{
				overlap = lastRect.intersects(pSector->getLabelRect());
			}
			lastRect = pSector->getLabelRect();
		}
	}

	// If labels would overlap - move every 2nd label
	if(overlap)
	{
		for(int idx = 1 ; idx < sectors.count() ; idx += 2)
		{
			ProfileSector *pSector = sectors.at(idx);
			pSector->moveLabelRectUp();
		}
		maxHeight *= 2;
	}
	return viewRect.bottom() - maxHeight;
}

/*
**	FUNCTION
**		Draw background of sector areas
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Vertical axis must be prepared before calling this method
**
*/
void ProfileImage::drawSectors(QPainter &painter)
{
	QRect rect = pAxis->getGraphArea();
	if(!showSectorNames)
	{
		rect.setBottom(pAxis->getScaleBottom());
	}
	//qDebug("drawing %d sectors\n", sectors.count());
	for(int n = 0 ; n < sectors.count() ; n++)
	{
		ProfileSector *pSector = sectors.at(n);
		if(!pSector)
		{
			qDebug("NULL sector at index %d\n", n);
			continue;
		}
		// qDebug("Sector index %d, pointer %X\n", n, (unsigned)pSector->getSector());
		rect.setLeft(pSector->getLeft());
		rect.setRight(pSector->getRight());
		if(n & 0x1)
		{
			// Draw odd areas only, so even areas will have default background color
			rect.setTop(pAxis->getScaleTop());
			painter.fillRect(rect, VacMainView::getProfileAltSectorColor());
		}
		if(showSectorNames)
		{
			pSector->drawLabel(painter);
			//QString label = pSector->getSector()->getName();
			//rect.setTop(pAxis->getScaleBottom());
			///*
			//QRect boundRect = painter.boundingRect(rect, Qt::AlighCenter, label);
			//*/
			//painter.setPen(Qt::black);
			//painter.drawText(rect, Qt::AlignCenter, label);
		}
	}
}

/*
**	FUNCTION
**		Draw bars
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileImage::drawBars(QPainter &painter)
{
	QColor useColor;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		ProfileItem *pItem = eqps.at(idx);
		if(pItem->isHidden()) 
		{
			continue;
		}
		if(!pItem->shallBeVisible())
		{
			continue;
		}
		if(flashBarsOnly && (!pItem->isFlashColor()))
		{
			continue;
		}
		useColor = useAltColor ? VacMainView::getProfileAltBarColor() : pItem->getColor();
		if((!pItem->isValueValid()) && (useColor == Qt::black))
		{
			useColor = palette().color(QPalette::Window);
		}
		if(pItem->getLeft() == pItem->getRight())
		{
			painter.setPen(useColor);
			painter.drawLine(pItem->getLeft(), pItem->getTop(), pItem->getLeft(), pItem->getBottom());
		}
		else
		{
			painter.fillRect(pItem->getRect(), useColor);
		}
		// Highlight selected bar
		if((!pItem->isSelected()) || useAltColor)
		{
			continue;
		}
		useColor.setRgb(useColor.red() >> 1, useColor.green() >> 1, useColor.blue() >> 1);
		painter.setPen(useColor);
		int stepValue = (pItem->getRight() - pItem->getLeft()) > 16 ? 5 : 2;
		int i;
		for(i = pItem->getLeft() + 1 ; i < pItem->getRight() ; i += stepValue )
		{
			painter.drawLine(i, pItem->getTop(), i, pItem->getBottom());
		}
		for(i = pItem->getTop() + 1 ; i < pItem->getBottom() ; i += stepValue)
		{
			painter.drawLine(pItem->getLeft(), i, pItem->getRight(), i);
		}
	}
}

/*
**	FUNCTION
**		Slot activated when data for one of devices have been changed
**
**	PARAMETERS
**		pSrc			- Pointer to signal source
**		colorChanged	- Flag indicating if color of item has been changed,
**							hence, redraw is necessary
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileImage::eqpDataChange(ProfileItem *pSrc, bool colorChanged)
{
	int useMin, useMax;
	pAxis->getLimits(useMin, useMax);
	double scaleHeight = useMax - useMin;
	double scaleMax = pow(10.0, useMax);
	double scaleMin = pow(10.0, useMin);
	bool valueChanged = barVerticalGeom(pSrc, scaleMin, scaleMax, scaleHeight, useMin);

	if(colorChanged || valueChanged)
	{
		initiateRedraw();
	}
}

void ProfileImage::mobileVisibilityChanged(void)
{
	redrawRequired = true;
	initiateRedraw();
}

void ProfileImage::itemVisibilityChanged(ProfileItem * /* pSrc */)
{
	redrawRequired = true;
	initiateRedraw();
}

void ProfileImage::initiateRedraw(void)
{
	pRedrawTimer->stop();
	int elapsed = drawTime.msecsTo(QTime::currentTime());
	if(elapsed < 0)	// wrap around 24 hours
	{
		rebuildImage();
	}
	else if(elapsed > 1000)
	{
		rebuildImage();
	}
	else
	{
		pRedrawTimer->start(300);
	}
}

/*
**	FUNCTION
**		Slot activated when selection state for one of devices have been changed
**
**	PARAMETERS
**		pSrc	- Pointer to signal source
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileImage::eqpSelectChange(ProfileItem * /* pSrc */)
{
	rebuildImage();
}

/*
**	FUNCTION
**		Show label for bar under mouse pointer
**
**	PARAMETERS
**		pItem	- Pointer to profile item under mouse pointer
**		y		- Y-coordinate of mouse pointer
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileImage::showBarLabel(ProfileItem *pItem, int y)
{
	pTimer->stop();
	pSpinBox->hide();
	Eqp *pEqp = pItem->getEqp();
	QString text = pEqp->getSectorBefore()->getName();
	if(pEqp->isSectorBorder())
	{
		text += " / ";
		text += pEqp->getSectorAfter()->getName();
	}
	text += "\n";
	QString state;
	pEqp->getToolTipString(state, mode);
	text += state;
	pLabel->setText(text);
	pLabel->adjustSize();
	int	x = (pItem->getLeft() + pItem->getRight()) / 2;
	if((x + pLabel->width()) > width())
	{
		x -= pLabel->width();
	}
	if((y + pLabel->height()) > height())
	{
		y -= pLabel->height();
	}
	pLabel->move(x, y);
	pLabel->show();
	pTimer->start(5000);
}

/*
**	FUNCTION
**		It has been found that mouse pointer is within vertical scale
**		area. Decide if spin box for scale control shall be shown,
**		if yes - configure and show it
**
**	PARAMETERS
**		y		- Y-coordinate of mouse pointer
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileImage::showScaleControl(int y)
{
	// Are we close enough to min or max of scale
	bool closeToMin = false;
	if(y < (viewHeight >> 2))
	{
		closeToMin = false;
	}
	else if(y > (viewHeight - (viewHeight >> 2)))
	{
		closeToMin = true;
	}
	else	// Far enough from both min and max
	{
		pSpinBox->hide();
		return;
	}
	if(!pSpinBox->isHidden())
	{
		if(editScaleMin == closeToMin)
		{
			return;
		}
		else	// We have to switch to editing another limit
		{
			pSpinBox->hide();
		}
	}

	int useMin, useMax;
	pAxis->getLimits(useMin, useMax);
	editScaleMin = closeToMin;
	settingSpinBox = true;
	if(closeToMin)
	{
		pSpinBox->setValue(useMin);
		pSpinBox->setRange(-18, 3);
		pSpinBox->adjustSize();
		pSpinBox->move(0, viewHeight - pSpinBox->height() - 2);
	}
	else
	{
		pSpinBox->setValue(useMax);
		pSpinBox->setRange(-17, 4);
		pSpinBox->adjustSize();
		pSpinBox->move(0, 2);
	}
	pSpinBox->show();
	settingSpinBox = false;
}

/*
**	FUNCTION
**		Slot activated when axis limit has been changed in SpinBox
**
**	PARAMETERS
**		value	- New SpinBox value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileImage::spinBoxChange(int value)
{
	if(settingSpinBox)
	{
		return;
	}
	int useMin, useMax;
	pAxis->getLimits(useMin, useMax);
	if(editScaleMin)
	{
		useMin = value;
		if(useMax <= useMin)
		{
			useMax = useMin + 1;
		}
	}
	else
	{
		useMax = value;
		if(useMax <= useMin)
		{
			useMin = useMax - 1;
		}
	}
	setLimits(useMin, useMax);
	emit limitsChanged(useMin, useMax);
}

/*
**	FUNCTION
**		Find profile item located under mouse pointer
**
**	PARAMETERS
**		x	- mouse X coordinate
**		y	- mouse Y coordinate
**
**	RETURNS
**		Pointer to profile item under mouse pointer; or
**		NULL if there is no profile item under mouse pointer
**
**	CAUTIONS
**		None
*/
ProfileItem *ProfileImage::findItemUnderMouse(int x, int y)
{
	if(x < pAxis->getAxisLocation())
	{
		return NULL;
	}
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		ProfileItem *pItem = eqps.at(idx);
		if(!pItem->isHidden())
		{
			if(pItem->shallBeVisible())
			{
				if(pItem->getRect().contains(x, y))
				{
					return pItem;
				}
			}
		}
	}
	return NULL;
}

void ProfileImage::leaveEvent(QEvent * /* pEvent */)
{
	pTimer->stop();
	pLabel->hide();
	pSpinBox->hide();
}

/*
**	FUNCTION
**		Dump all profile geometry to file for debugging purpose
**
**	PARAMETERS
**		fileName	- Name of file where parameters shall be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileImage::dump(const char *fileName)
{
	if(!DebugCtl::isProfile())
	{
		return;
	}
	FILE	*pFile;
#ifdef Q_OS_WIN
	if(fopen_s(&pFile, fileName, "w"))
	{
		return;
	}
#else
	if(!(pFile = fopen(fileName, "w")))
	{
		return;
	}
#endif

	fprintf(pFile, "SECTORS IN PROFILE:\n");
	int n = 0;
	for(n = 0 ; n < sectorUsage.count() ; n++)
	{
		Sector *pSector = sectorUsage.at(n);
		fprintf(pFile, "  %s\n", pSector->getName());
	}

	fprintf(pFile, "\n%d DEVICES:\n", eqps.count());
	for(n = 0 ; n < eqps.count() ; n++)
	{
		ProfileItem *pItem = eqps.at(n);
		fprintf(pFile, "  %04d: %s @ %f: S1 %s S2 %s (%d, %d) (%d, %d) %s\n",
			n, pItem->getEqp()->getName(), pItem->getEqp()->getStart(),
			(pItem->getEqp()->getSectorBefore() ? pItem->getEqp()->getSectorBefore()->getName() : "NONE"),
			(pItem->getEqp()->getSectorAfter() ? pItem->getEqp()->getSectorAfter()->getName() : "NONE"),
			pItem->getLeft(), pItem->getTop(), pItem->getRight(), pItem->getBottom(),
			(pItem->isHidden() ? "HIDDEN" : ""));
		n++;
	}

	fprintf(pFile, "\n%d SECTORS:\n", sectors.count());
	for(n = 0 ; n < sectors.count() ; n++)
	{
		ProfileSector *pProfSector = sectors.at(n);
		fprintf(pFile, "  %02d: %s: from %d %s to %d %s\n",
			n, pProfSector->getSector() ? pProfSector->getSector()->getName() : "NONE",
			pProfSector->getLeft(), (pProfSector->isLeftWithinDevice() ? "WITHIN" : ""),
			pProfSector->getRight(), (pProfSector->isRightWithinDevice() ? "WITHIN" : ""));
	}
	fclose(pFile);
}
