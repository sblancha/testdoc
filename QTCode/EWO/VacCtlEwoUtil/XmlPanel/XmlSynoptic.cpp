//	Implementation of XmlSynoptic class
/////////////////////////////////////////////////////////////////////////////////

#include "XmlSynoptic.h"
#include "ImageItemLine.h"
#include "ImageItemRectangle.h"
#include "ImageItemPolygon.h"
#include "ImageItemText.h"
#include "ImageItemEllipse.h"
#include "ImageItemFrame.h"
#include "ImageItemGeneric.h"

#include "Eqp.h"
#include "MobileType.h"
#include "FunctionalType.h"

#include <QFile>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

XmlSynoptic *XmlSynoptic::createFromFile(QWidget *pParent, const QString &fileName,
	DataEnum::DataMode newMode, QString &errMsg)
{
	if(fileName.isEmpty())
	{
		errMsg = "Empty file name";
		return NULL;
	}
	QFile file(fileName);
	if(!file.exists())
	{
		errMsg = "File does not exist <";
		errMsg += fileName;
		errMsg += ">";
		return NULL;
	}
	if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		errMsg = "Failed to open file <";
		errMsg += fileName;
		errMsg += ">, error ";
		errMsg += QString::number(file.error());
		return NULL;
	}
	XmlSynoptic *pSynoptic = new XmlSynoptic(pParent);
	pSynoptic->mode = newMode;
	QXmlStreamReader reader(&file);
	if(!pSynoptic->parse(reader, errMsg))
	{
		delete pSynoptic;
		pSynoptic = NULL;
	}
	return pSynoptic;
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
/////////////////////////////////////////////////////////////////////////////////
XmlSynoptic::XmlSynoptic(QWidget *parent)
{
	pParent = parent;
	width = height = 0;
	mode = DataEnum::Online;
}

XmlSynoptic::~XmlSynoptic()
{
	while(!items.isEmpty())
	{
		delete items.takeFirst();
	}

	// Icons will be destroyed when parent is destroyed?
}

void XmlSynoptic::draw(QPainter &painter)
{
	painter.setRenderHint(QPainter::Antialiasing);
	for(int idx = 0 ; idx < items.count() ; idx++)
	{
		ImageItem *pItem = items.at(idx);
		if(pItem->isVisible())
		{
			pItem->draw(painter);
		}
	}
}

bool XmlSynoptic::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	for(int idx = 0 ; idx < items.count() ; idx++)
	{
		ImageItem *pItem = items.at(idx);
		if(pItem->isVisible())
		{
			if(pItem->getToolTip(point, text, rect))
			{
				return true;
			}
		}
	}
	return false;
}


bool XmlSynoptic::parse(QXmlStreamReader &reader, QString &errMsg)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		/*
		if(reader.isStartElement())
		{
			printf("start %s\n", reader.name().toString().toLatin1().constData());
		}
		else if(reader.isEndElement())
		{
			printf("end %s\n", reader.name().toString().toLatin1().constData());
		}
		*/
		if(reader.isStartElement())
		{
#ifdef XML_PANEL_PARSE
			printf("%d GLOBAL: start of %s\n", (int)reader.lineNumber(), reader.name().toString().toLatin1().constData());
#endif
			if(!reader.name().compare("panel", Qt::CaseInsensitive))
			{
				parsePanel(reader);
			}
			else
			{
				parseUnknown(reader);
			}
		}
	}
	if(reader.hasError())
	{
		errMsg = "Parse error at ";
		errMsg += QString::number(reader.lineNumber());
		errMsg += ": ";
		errMsg += reader.errorString();
#ifdef XML_PANEL_PARSE
	printf("parse error at %d: %s\n", (int)reader.lineNumber(), errMsg.toLatin1().constData());
#endif
		return false;
	}
	if((!width) || (!height))
	{
		errMsg = "Panel has zero width and/or height";
		return false;
	}
	if(items.isEmpty() && icons.isEmpty())
	{
		errMsg = "No items/icons in panel";
		return false;
	}
#ifdef XML_PANEL_PARSE
	printf("%d items %d icons\n", items.count(), icons.count());
#endif

	assignInterlockSources();
	return true;
}

void XmlSynoptic::assignInterlockSources(void)
{
	for(int itemIdx = 0 ; itemIdx < items.count() ; itemIdx++)
	{
		ImageItem *pItem = items.at(itemIdx);
		Eqp *pEqp = pItem->getEqp();
		if(!pEqp)
		{
			continue;
		}
		pItem->checkForInterlockSource();
		pItem->addAlarmSources(items, icons);
	}
}

void XmlSynoptic::parsePanel(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement() && (!reader.name().compare("panel", Qt::CaseInsensitive)))
		{
			break;
		}
		if(reader.isStartElement())
		{
#ifdef XML_PANEL_PARSE
printf("%d: start of %s\n", (int)reader.lineNumber(), reader.name().toString().toLatin1().constData());
#endif
			if(!reader.name().compare("shapes", Qt::CaseInsensitive))
			{
				parseShapes(reader);
			}
			else if(!reader.name().compare("properties", Qt::CaseInsensitive))
			{
				parsePanelProperties(reader);
			}
			else
			{
				parseUnknown(reader);
			}
		}
	}
}

void XmlSynoptic::parsePanelProperties(QXmlStreamReader &reader)
{
#ifdef XML_PANEL_PARSE
	printf("parsePanelProperties() - START at %d\n", (int)reader.lineNumber());
#endif
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement() && (!reader.name().compare("properties", Qt::CaseInsensitive)))
		{
#ifdef XML_PANEL_PARSE
			printf("parsePanelProperties() - FINISH at %d\n", (int)reader.lineNumber());
#endif
			break;
		}
		if(reader.isStartElement())
		{
#ifdef XML_PANEL_PARSE
			printf("parsePanelProperties() - start element %s\n", reader.name().toString().toLatin1().constData());
#endif
			if(!reader.name().compare("prop", Qt::CaseInsensitive))
			{
				QStringRef propName = reader.attributes().value("name");
#ifdef XML_PANEL_PARSE
				printf("PANEL prop %s\n", propName.toString().toLatin1().constData());
#endif
				if(!propName.compare("Size", Qt::CaseInsensitive))
				{
					sscanf(reader.readElementText().toLatin1().constData(), "%d %d", &width, &height);
#ifdef XML_PANEL_PARSE
					printf("Size: <%s> -> %d %d\n", reader.readElementText().toLatin1().constData(), width, height);
#endif
				}
			}
		}
	}
}

void XmlSynoptic::parseShapes(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement() && (!reader.name().compare("shapes", Qt::CaseInsensitive)))
		{
			break;
		}
		if(reader.isStartElement())
		{
#ifdef XML_PANEL_PARSE
			printf("%d shape: start of %s\n", (int)reader.lineNumber(), reader.name().toString().toLatin1().constData());
#endif
			if(!reader.name().compare("shape", Qt::CaseInsensitive))
			{
				parseShape(reader);
			}
			else
			{
				parseUnknown(reader);
			}
		}
	}
}

void XmlSynoptic::parseShape(QXmlStreamReader &reader)
{
	QStringRef shapeType = reader.attributes().value("shapeType");
#ifdef XML_PANEL_PARSE
	printf("parseShape() start %s type %s\n", reader.name().toString().toLatin1().constData(),
		shapeType.toString().toLatin1().constData());
#endif
	if(!shapeType.compare("LINE", Qt::CaseInsensitive))
	{
		parseLine(reader);
	}
	else if(!shapeType.compare("RECTANGLE", Qt::CaseInsensitive))
	{
		parseRectangle(reader);
	}
	else if(!shapeType.compare("POLYGON", Qt::CaseInsensitive))
	{
		parsePolygon(reader);
	}
	else if(!shapeType.compare("PRIMITIVE_TEXT", Qt::CaseInsensitive))
	{
		parsePrimitiveText(reader);
	}
	else if(!shapeType.compare("ELLIPSE", Qt::CaseInsensitive))
	{
		parseEllipse(reader);
	}
	else if(!shapeType.compare("FRAME", Qt::CaseInsensitive))
	{
		parseFrame(reader);
	}
	else if(!shapeType.compare("GENERIC", Qt::CaseInsensitive))
	{
		parseGeneric(reader);
	}
	else
	{
		parseUnknown(reader);
	}
}

void XmlSynoptic::parseLine(QXmlStreamReader &reader)
{
	ImageItemLine *pItem = new ImageItemLine();
	pItem->parse(reader);
	items.append(pItem);
#ifdef XML_PANEL_PARSE
	printf("LINE at %d: from (%d, %d) to (%d %d), ", (int)reader.lineNumber(),
		pItem->getStartX(), pItem->getStartY(), pItem->getEndX(), pItem->getEndY());
	printf("Fore: (%d,%d,%d,%d) Back: (%d,%d,%d,%d)\n",
		pItem->getForeColor().red(), pItem->getForeColor().green(), pItem->getForeColor().blue(), pItem->getForeColor().alpha(),
		pItem->getBackColor().red(), pItem->getBackColor().green(), pItem->getBackColor().blue(), pItem->getBackColor().alpha());
#endif
		
}

void XmlSynoptic::parseRectangle(QXmlStreamReader &reader)
{
	ImageItemRectangle *pItem = new ImageItemRectangle();
	pItem->parse(reader);
	items.append(pItem);
#ifdef XML_PANEL_PARSE
	printf("RECTANGLE at %d: at (%d, %d) size (%d %d), filled %d, ", (int)reader.lineNumber(),
		pItem->getStartX(), pItem->getStartY(), pItem->getSizeX(), pItem->getSizeY(),
		(int)pItem->isFilled());
	printf("Fore: (%d,%d,%d,%d) Back: (%d,%d,%d,%d)\n",
		pItem->getForeColor().red(), pItem->getForeColor().green(), pItem->getForeColor().blue(), pItem->getForeColor().alpha(),
		pItem->getBackColor().red(), pItem->getBackColor().green(), pItem->getBackColor().blue(), pItem->getBackColor().alpha());
#endif
}

void XmlSynoptic::parsePolygon(QXmlStreamReader &reader)
{
	ImageItemPolygon *pItem = new ImageItemPolygon();
	pItem->parse(reader);
	items.append(pItem);
#ifdef XML_PANEL_PARSE
	printf("POLYGON at %d: closed %d, filled %d, ", (int)reader.lineNumber(),
		(int)pItem->isClosed(), (int)pItem->isFilled());
	printf("Fore: (%d,%d,%d,%d) Back: (%d,%d,%d,%d)\n",
		pItem->getForeColor().red(), pItem->getForeColor().green(), pItem->getForeColor().blue(), pItem->getForeColor().alpha(),
		pItem->getBackColor().red(), pItem->getBackColor().green(), pItem->getBackColor().blue(), pItem->getBackColor().alpha());
	const QPolygon &points = pItem->getPoints();
	for(int idx = 0 ; idx < points.count() ; idx++)
	{
		printf("   point %d: (%d, %d)\n", idx, points.at(idx).x(), points.at(idx).y());
	}
#endif
}

void XmlSynoptic::parsePrimitiveText(QXmlStreamReader &reader)
{
	ImageItemText *pItem = new ImageItemText();
	pItem->parse(reader);
	items.append(pItem);
#ifdef XML_PANEL_PARSE
	printf("PRIMITIVE_TEXT at %d: pos (%d %d) size (%d %d) font <%s> text <%s> flags %d\n",
		(int)reader.lineNumber(),
		pItem->getStartX(), pItem->getStartY(), pItem->getSizeX(), pItem->getSizeY(),
		pItem->getFont().toString().toLatin1().constData(),
		pItem->getText().toLatin1().constData(),
		pItem->getFlags());
#endif
}

void XmlSynoptic::parseFrame(QXmlStreamReader &reader)
{
	ImageItemFrame *pItem = new ImageItemFrame();
	pItem->parse(reader);
	items.append(pItem);
#ifdef XML_PANEL_PARSE
	printf("FRAME at %d: pos (%d %d) size (%d %d) font <%s> text <%s>\n",
		(int)reader.lineNumber(),
		pItem->getStartX(), pItem->getStartY(), pItem->getSizeX(), pItem->getSizeY(),
		pItem->getFont().toString().toLatin1().constData(),
		pItem->getText().toLatin1().constData());
#endif
}

void XmlSynoptic::parseEllipse(QXmlStreamReader &reader)
{
	ImageItemEllipse *pItem = new ImageItemEllipse();
	pItem->parse(reader);
	items.append(pItem);
#ifdef XML_PANEL_PARSE
	printf("ELLIPSE at %d: center (%f %f) radii (%d %d)\n",
		(int)reader.lineNumber(),
		pItem->getCenterX(), pItem->getCenterY(), pItem->getRadiusX(), pItem->getRadiusY());
#endif
}

void XmlSynoptic::parseGeneric(QXmlStreamReader &reader)
{
	ImageItemGeneric *pItem = new ImageItemGeneric();
	pItem->parse(reader);
	items.append(pItem);
	VacIcon *pIcon = pItem->getIcon(pParent, mode);
#ifdef XML_PANEL_PARSE
	printf("XmlSynoptic::parseGeneric(): pIcon %X at %d\n", (unsigned)pIcon, (int)reader.lineNumber());
	fflush(stdout);
#endif
	if(pIcon)
	{
		icons.append(pIcon);
		Eqp *pEqp = pIcon->getEqp();
		if(pEqp)
		{
			if(pEqp->getMobileType() == MobileType::OnFlange)
			{
				if(!pEqp->isActive(mode))
				{
					pIcon->hide();
				}
				connect(pEqp, SIGNAL(mobileStateChanged(Eqp *, DataEnum::DataMode)),
					this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
			}
		}
	}
}

void XmlSynoptic::parseUnknown(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement())
		{
			break;
		}
		if(reader.isStartElement())
		{
			parseUnknown(reader);
		}
	}
}

void XmlSynoptic::mobileStateChange(Eqp *pSrc, DataEnum::DataMode eqpMode)
{
	if(eqpMode != mode)
	{
		return;
	}
	for(int idx = 0 ; idx < icons.count() ; idx++)
	{
		VacIcon *pIcon = icons.at(idx);
		if(pIcon->getEqp() == pSrc)
		{
			if(pSrc->isActive(mode))
			{
				pIcon->show();
			}
			else
			{
				pIcon->hide();
			}
			break;
		}
	}
}

