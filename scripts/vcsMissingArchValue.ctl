//#uses "VacEqpData"					// Load DLL or so
//#uses "VacResources"				
//#uses "lhcVacSectors.ctl"		// Load CTRL library
//#uses "lhcVacEqpConfig.ctl"	// Load CTRL library
//#uses "lhcVacArchiveConfig.ctl"
#uses "VacCtlEqpDataPvss"	// Load DLL
#uses "vclSectors.ctl"	// Load CTRL library
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "vclResources.ctl"   // Load CTRL library
#uses "vclArchiveConfig.ctl"   // Load CTRL library
#uses "vclOscEqp.ctl" // S.Merker 21.05.2013
#uses "vclDevCommon.ctl"	// Load CTRL library

dyn_string	dpeToSurvey;
dyn_time		dpeLastUpdate;
dyn_time		dpeUpdPeriod;
dyn_int			dpeHasUpdated;
const int DP_OLD = 0;
const int DP_UPDATED = 1;
const int DP_RESET = 2;
const int DP_MANUAL = 3;

// List of all dyn_* element types
dyn_int  dynTypesList;

bool			debugFlag = false;

main()
{
  DebugTN("vcsMissingArchValue.ctl::StartPreparation:");
  PrepareDynTypesList();
  LhcVacInitDevFunc();
  PrepareListToCheck();
  DebugTN("vcsMissingArchValue.ctl::PrepareListToCheck COMPLETED, found:", dynlen(dpeToSurvey));
  while(1)
  {
    if(debugFlag)
    {
      DebugTN("vcsMissingArchValue.ctl::NewCycle START for :", dynlen(dpeToSurvey));
    }
    for(int i=1;i<dynlen(dpeToSurvey); i++)
    {
      if(dpeHasUpdated[i] == DP_RESET)
      {
        continue;
      }
      string timeDp = dpeToSurvey[i] + ":_online.._stime";
      if((getCurrentTime() - dpeLastUpdate[i]) > dpeUpdPeriod[i])
      {
        if(debugFlag)
        {
          DebugTN("vcsMissingArchValue.ctl::update DPE " + dpeToSurvey[i]);
        }
        dpeHasUpdated[i] = DP_MANUAL;
        int type = dpElementType(dpeToSurvey[i]);
        if(dynContains(dynTypesList, type) > 0)
        {
          dyn_anytype dynValue;
          dpGet(dpeToSurvey[i], dynValue);
          dpSet(dpeToSurvey[i], dynValue);
        }
        else
        {
          anytype value;
          dpGet(dpeToSurvey[i],value);
          dpSet(dpeToSurvey[i],value);
        }
      }
    }
    if(debugFlag)
    {
      DebugTN("vcsMissingArchValue.ctl::NewCycle END");
    }
    delay(10);
  }
}

void PrepareDynTypesList()
{
  dynClear(dynTypesList);
  dynAppend(dynTypesList, DPEL_DYN_INT);
  dynAppend(dynTypesList, DPEL_DYN_INT_STRUCT);
  dynAppend(dynTypesList, DPEL_DYN_LANGSTRING);
  dynAppend(dynTypesList, DPEL_DYN_LANGSTRING_STRUCT);
  dynAppend(dynTypesList, DPEL_DYN_STRING);
  dynAppend(dynTypesList, DPEL_DYN_STRING_STRUCT);
  dynAppend(dynTypesList, DPEL_DYN_TIME);
  dynAppend(dynTypesList, DPEL_DYN_TIME_STRUCT);
  dynAppend(dynTypesList, DPEL_DYN_UINT);
  dynAppend(dynTypesList, DPEL_DYN_UINT_STRUCT);
  dynAppend(dynTypesList, DPEL_DYN_BIT32);
  dynAppend(dynTypesList, DPEL_DYN_BIT32_STRUCT);
  dynAppend(dynTypesList, DPEL_DYN_BLOB);
  dynAppend(dynTypesList, DPEL_DYN_BLOB_STRUCT);
  dynAppend(dynTypesList, DPEL_DYN_BOOL);
  dynAppend(dynTypesList, DPEL_DYN_BOOL_STRUCT);
  dynAppend(dynTypesList, DPEL_DYN_CHAR);
  dynAppend(dynTypesList, DPEL_DYN_CHAR_STRUCT);
  dynAppend(dynTypesList, DPEL_DYN_DPID);
  dynAppend(dynTypesList, DPEL_DYN_DPID_STRUCT);
  dynAppend(dynTypesList, DPEL_DYN_FLOAT);
  dynAppend(dynTypesList, DPEL_DYN_FLOAT_STRUCT);
}

/** PrepareListToCheck
*	Purpose:
Goes through dps and build a list of those which has time smoothing archive type

Parameters:
	- None

**/
PrepareListToCheck()
{
  dyn_string	exceptionInfo;

  // Found all dpTypes
  int nTypes = dynlen(glLhcVacDpTypes);
  if(debugFlag)
  {
    DebugTN("PrepareListToCheck:: nTypes", nTypes);
  }
  for(int k = 1; k <= nTypes; k++)
  {
    dyn_string 	allDpeNames, dpNs;
    dynClear(allDpeNames);
    dynClear(dpNs);
//For debug purpose it is possible to limit the search for desirable device types
//		if((glLhcVacDpTypes[k] != "VRPI_L") &&
//			(glLhcVacDpTypes[k] != "VGP_VELO") &&
//		 	(glLhcVacDpTypes[k] != "VGP_T"))
//		if((glLhcVacDpTypes[k] != "VGR_T"))
//			continue;

    if(glLhcVacDpTypes[k] == "_VacMobileList")
    {
      continue;
    }
    if(glLhcVacDpTypes[k] == "VMOB")
    {
      continue;
    }
    dyn_string exceptionInfo;
    BuildDpeListForDpType(glLhcVacDpTypes[k], allDpeNames, exceptionInfo);
    dpNs = dpNames("*", glLhcVacDpTypes[k]);

    if(dynlen(dpNs) == 0)
    {
      continue;
    }
    if(dynlen(allDpeNames) == 0)
    {
      continue;
    }
    if(debugFlag)
    {
      DebugTN("vcsMissingArchValue.ctl::PrepareListToCheck dpes for: " + k + "/" + nTypes, glLhcVacDpTypes[k], dynlen(allDpeNames), dynlen(dpNs));
    }
    for(int i = 1; i<= dynlen(dpNs); i++)
    {
      dyn_string	localExceptionInfo;

      if(dynlen(exceptionInfo) > 0)
      {
        return;
      }
      delay(0, 20);			
      for(int n = dynlen(allDpeNames) ; n > 0 ; n--)
      {
        // Get archiving  parameters for DPE
        string archiveClass;
        int archiveType, smoothProcedure, isActive;
        float deadband, timeInterval;
        bool		isArchived, configExists;
        fwArchive_get(dpNs[i] + "." + allDpeNames[n], configExists, archiveClass, archiveType, smoothProcedure,
          deadband, timeInterval, isActive, exceptionInfo); 
        if(dynlen(exceptionInfo) > 0)
        {
          isArchived = false;
        }
        else if(archiveClass == "")
        {
          isArchived = false;
        }
        else
        {
          isArchived = configExists && isActive;
        }
        if(!isArchived)
        {
          continue;
        }

        if((smoothProcedure == DPATTR_TIME_SMOOTH) // time dependent
          || (smoothProcedure == DPATTR_TIME_AND_VALUE_SMOOTH)  // value AND time dependent,
          || (smoothProcedure == DPATTR_TIME_AND_VALUE_REL_SMOOTH)  // relative value AND time dependent,
          || (smoothProcedure == DPATTR_TIME_OR_VALUE_SMOOTH)  // value OR time dependent,
          || (smoothProcedure == DPATTR_TIME_OR_VALUE_REL_SMOOTH)  // relative value OR time dependent,
          || (smoothProcedure == DPATTR_COMPARE_OLD_NEW)  // old-new comparison,
          || (smoothProcedure == DPATTR_OLD_NEW_AND_TIME_SMOOTH)  // old-new comparison AND time,
          || (smoothProcedure == DPATTR_OLD_NEW_OR_TIME_SMOOTH))  // old-new comparison OR time          string survDpe = dpNs[i] + "." + allDpeNames[n];
        {
          string survDpe = dpNs[i] + "." + allDpeNames[n];
          dynAppend(dpeToSurvey, survDpe);
          dynAppend(dpeLastUpdate, makeTime(1970,1,1));
          dynAppend(dpeUpdPeriod, timeInterval);
          dynAppend(dpeHasUpdated, DP_OLD);
          string survDpeTime = survDpe + ":_online.._stime";
          dpConnect("ValueChangeCb", true, survDpeTime);
        }
      }
    }
  }	
}
/*
Callback functiom for dpConnect
when value is changed in equipment the flag is set to DP_UPDATED
then later the script will reset value to be sure it is catched by archiver
*/
ValueChangeCb(string dpet, time tChange)
{
  int id; 
  id = dynContains(dpeToSurvey, dpSubStr(dpet, DPSUB_SYS_DP_EL));
  if(id > 0)
  {
    dpeLastUpdate[id] = tChange;
    if(dpeHasUpdated[id] == DP_MANUAL)
    {
      dpeHasUpdated[id] = DP_RESET;
    }
    else
    {
      dpeHasUpdated[id] = DP_UPDATED;
    }
//		DebugN("vcsMissingArchValue.ctl::ValueChangeCb:",  id, dpeToSurvey[id], dpeHasUpdated[id]);
  }
}


// BuildDpeListForDpType
/**
Purpose:
Build full names of all DPEs of given DP. Every resulting DPE names contains the whole
path WITHOUT input DP name

Parameters:
	- dpName, string, input, Name of DP for which all DPE names are required
	- allDpeNames, dyn_string, output, List of all DPE names for given DP will be returned here
	- exceptionInfo, dyn_string, output, details of exception will be written here

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void BuildDpeListForDpType(string dpType, dyn_string &allDpeNames, dyn_string &exceptionInfo)
{
  dyn_dyn_string	names;
  dyn_dyn_int			types;
  dyn_string			dpeTree, localExceptionInfo;

  dynClear(allDpeNames);
  dynClear(exceptionInfo);

  // Get DP type structure for working DP
  if(dpTypeGet(dpType, names, types) < 0)
  {
    dyn_errClass err = getLastError();
    DebugTN("cvsMissingArchValue.ctl: ERROR: dpTypeGet(" + dpType + ") failed: " + StdErrorText(err));
    DebugTN(err);
    fwException_raise(exceptionInfo, "ERROR", "BuildDpeListForDpType(): dpTypeGet(" + dpType + ") failed", "");
    return;
  }
  int dpeIdx, typeIdx, type, nDpes = dynlen(names);
  for(dpeIdx = 1 ; dpeIdx <= nDpes ; dpeIdx++)
  {
    typeIdx = dynlen(types[dpeIdx]);
    type = types[dpeIdx][typeIdx];
    if((type == DPEL_STRUCT) ||	// Structure
      (type == DPEL_BIT32_STRUCT) || (type == DPEL_BLOB_STRUCT) || (type == DPEL_BOOL_STRUCT) ||
      (type == DPEL_CHAR_STRUCT) || (type == DPEL_DPID_STRUCT) || (type == DPEL_DYN_BIT32_STRUCT) ||
      (type == DPEL_DYN_BLOB_STRUCT) || (type == DPEL_DYN_BOOL_STRUCT) || (type == DPEL_DYN_CHAR_STRUCT) ||
      (type == DPEL_DYN_DPID_STRUCT) || (type == DPEL_DYN_FLOAT_STRUCT) || (type == DPEL_DYN_INT_STRUCT) ||
      (type == DPEL_DYN_LANGSTRING_STRUCT) || (type == DPEL_DYN_STRING_STRUCT) || (type == DPEL_DYN_TIME_STRUCT) ||
      (type == DPEL_DYN_UINT_STRUCT) || (type == DPEL_FLOAT_STRUCT) || (type == DPEL_INT_STRUCT) ||
      (type == DPEL_LANGSTRING_STRUCT) || (type == DPEL_STRING_STRUCT) || (type == DPEL_TIME_STRUCT) ||
      (type == DPEL_UINT_STRUCT))
    {
      if(dynlen(dpeTree) >= typeIdx)
      {
        dpeTree[typeIdx] = names[dpeIdx][typeIdx];
      }
      else
      {
        dynAppend(dpeTree, names[dpeIdx][typeIdx]);
      }
      continue; 
    }
    if(dynlen(dpeTree) == 0)
    {
      continue;	// 'root' DPE
    }
    string dpeName = "";
    for(int n = 2 ; n < typeIdx ; n++)	// Start from 2 because 1 is DP type name
    {
      if(dpeName != "")
      {
        dpeName += ".";
      }
      dpeName += dpeTree[n];
    }
    if(dpeName != "")
    {
      dpeName += ".";
    }
    dpeName += names[dpeIdx][typeIdx];
    dynAppend(allDpeNames, dpeName);
  }
}
