#ifndef	FUNCTIONALTYPEVPS_PROCESS_H
#define	FUNCTIONALTYPEVPS_PROCESS_H

// Special functional type for VPS_Process - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVPS_Process : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Off = 1,
		On = 2
	};

	FunctionalTypeVPS_Process(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVPS_Process() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVPS_PROCESS_H
