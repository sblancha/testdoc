//	Implementation of SectView class
/////////////////////////////////////////////////////////////////////////////////

#include "SectView.h"

#include "VacMainView.h"

#include "DataPool.h"
#include "DebugCtl.h"

#include <qpainter.h>

/*
**	FUNCTION
**		Create new instance of sector view
**
**	PARAMETERS
**		parent			- Pointer to parent widget
**		pFirstSector	- Pointer to first sector
**		pLastSector		- Pointer to last sector
**		mode			- Data acquisition mode for sector view
**
**	RETURNS
**		Pointer to new instance of sector view,
**		NULL in case of error.
**
**	CAUTIONS
**		None
*/
SectView *SectView::create(QWidget *parent, Sector *pFirstSector, Sector *pLastSector,
	DataEnum::DataMode mode)
{
	// LEIR BTL specific: if one of sectors has draw part on survey line E01 - 
	// result produced by 'generic' algorithm is not good. In this case use special
	// view designed especially for this area
	DataPool &pool = DataPool::getInstance();
	bool isSpecialLeirBtl = false;
	Sector *pSector = pFirstSector;
	do
	{
		const QList<SectorDrawPart *> &drawParts = pSector->getDrawParts();
		for(int drawPartIdx = 0 ; drawPartIdx < drawParts.count() ; drawPartIdx++)
		{
			SectorDrawPart *pPart = drawParts.at(drawPartIdx);
			const QList<BeamLinePart *> &lineParts = pPart->getLine()->getParts();
			for(int linePartIdx = 0 ; linePartIdx < lineParts.count() ; linePartIdx++)
			{
				BeamLinePart *pLinePart = lineParts.at(linePartIdx);
				if(!strcmp(pLinePart->getName(), "E01"))
				{
					isSpecialLeirBtl = true;
					break;
				}
			}
			if(isSpecialLeirBtl)
			{
				break;
			}
		}
		if(isSpecialLeirBtl)
		{
			break;
		}
		if(pSector == pLastSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);

	// Create view
	SectView *pView = NULL;
	/* TODO ... or not TODO?
	if(isSpecialLeirBtl)
	{
		pView = SynBtlView::Create( startSectIdx, endSectIdx );
	}
	else
	TODO */
	{
		pView = new SectView(parent);
		pView->mode = mode;
		pView->pStartSector = pFirstSector;
		pView->pEndSector = pLastSector;
		const QList<BeamLine *> &lines = pool.getLines();
		for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			BeamLine *pLine = lines.at(lineIdx);
			SectLine *pSectLine = SectLine::create(pView, pLine, pFirstSector, pLastSector);
			if(pSectLine)
			{
				pSectLine->setMode(mode);
				pView->lines.append(pSectLine);
				connect(pSectLine, SIGNAL(coldexStateChanged(bool, bool)),
					pView, SLOT(coldexStateChange(bool, bool)));
			}
		}
	}
	return pView;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SectView::SectView(QWidget *parent, Qt::WindowFlags f) :
	SynView(parent, f)
{
	pStartSector = pEndSector = NULL;
}

SectView::~SectView()
{
}

/*
** FUNCTION
**		Rebuild geometries of all individual lines if necessary,
**		then build overall geometry of the whole view.
**		This method overrides CSynView's method.
**
** PARAMETERS
**		mask	- Equipment type mask, not used here
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void SectView::buildView(VacEqpTypeMask & /* mask */)
{
	if(totalWidth && totalHeight)
	{
		return;	// The view is built once and then never rebuilt
	}

	// Prepare list of sectorUsage: add there all sectors except for those which
	// SHALL appear in this view
	DataPool &pool = DataPool::getInstance();
	QList<Sector *> sectorUsage = pool.getSectors();
	Sector *pSector = pStartSector;
	while(pSector)
	{
		sectorUsage.removeOne(pSector);
		if(pSector == pEndSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	}

	// Build equipment list for all beam lines. Start with parent lines - sectors
	// common for parent and child lines shall appear in parent line only
	int prevToProcess = lines.count(), newToProcess = 0;
	do
	{
		newToProcess = 0;
		for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			SynLine *pLine = lines.at(lineIdx);
			if(pLine->getEqps().count())
			{
				continue;	// L.Kopylov 23.05.2013 - do not build line twice - sectorUSage will prevent adding sectors
			}
			if(pLine->getStartEqp())
			{
				int parentEqpIdx;
				SynLine *pParent = getParentLine(pLine, pLine->getStartEqp(), parentEqpIdx);
				if(pParent)
				{
					if(!pParent->getEqps().count())
					{
						newToProcess++;
						continue;
					}
				}
			}
			if(pLine->getEndEqp())
			{
				int parentEqpIdx;
				SynLine *pParent = getParentLine(pLine, pLine->getEndEqp(), parentEqpIdx);
				if(pParent)
				{
					if(!pParent->getEqps().count())
					{
						newToProcess++;
						continue;
					}
				}
			}
			pLine->build(sectorUsage);
			if(!pLine->getEqps().count())
			{
				newToProcess++;
			}
			else
			{
				if(DebugCtl::isSynoptic())
				{
					QString fileName;
					#ifdef Q_OS_WIN
						fileName = "C:\\SvLineInit";
					#else
						fileName = "/home/kopylov/SvLineInit";
					#endif
					fileName += pLine->getBeamLine()->getName();
					fileName += ".txt";
					dump(fileName.toLatin1().constData());
				}
			}

		}
		if(newToProcess == prevToProcess)
		{
			break;	// No more progress
		}
	} while((prevToProcess = newToProcess));

	if(DebugCtl::isSynoptic())
	{
		#ifdef Q_OS_WIN
			dump("C:\\SvInit.txt");
		#else
			dump("/home/kopylov/SvInit.txt");
		#endif
	}

	if(!totalWidth)
	{
		buildGeometry();
		/* TODO
		for(n = 0 ; n < nLines ; n++)
		{
			pLine = (CSectLine *)lines[n];
			pLine->yDescent += SECTOR_ICON_OFFSET;
		}
		totalHeight += SECTOR_ICON_OFFSET;
		AdjustViewStart( pParam );
		*/
	}
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		SynLine *pLine = lines.at(lineIdx);
		pLine->finishGeometry();
	}
	if(DebugCtl::isSynoptic())
	{
		#ifdef Q_OS_WIN
			dump("C:\\SvConnect.txt");
		#else
			dump("/home/kopylov/SvConnect.txt");
		#endif
	}
}
