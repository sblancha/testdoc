#
# Project file for qmake utility to build Makefile for VacStateArchive DLL
#
#	09.01.2000	L.Kopylov
#		Initial version
#
#	13.01.2008	L.Kopylov
#		Rename target library to VacCtlHistoryData
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	10.01.2010	L.Kopylov
#		Changes for DLL on Windows
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

TEMPLATE = lib

TARGET = VacCtlHistoryData

# To avoid having version numbers in .so file name
unix:CONFIG += plugin
# no_plugin_name_prefix

DESTDIR = ../../../bin

OBJECTS_DIR = obj

unix:CONFIG += qt thread release warn_on
win32:CONFIG += qt dll thread release

# To produce PDB file together with DLL
win32:QMAKE_CXXFLAGS+=/Zi
win32:QMAKE_LFLAGS+= /INCREMENTAL:NO /Debug


win32:DEFINES += BUILDING_VACCTLHISTORYDATA

#DEFINES += _VACDEBUG

INCLUDEPATH += ../Platform \
	../VacCtlUtil

HEADERS = DpeValue.h \
	DpeValuesSet.h \
	DpValuesSet.h \
	VacCtlStateArchive.h \
	ReplayPool.h


SOURCES = DpeValue.cpp \
	DpeValuesSet.cpp \
	DpValuesSet.cpp \
	VacCtlStateArchive.cpp \
	ReplayPool.cpp
	

LIBS += -L../../../bin \
	-lVacCtlUtil

