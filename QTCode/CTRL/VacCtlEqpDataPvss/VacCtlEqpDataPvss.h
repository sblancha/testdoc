// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Component Name: VacCtlEqpDataPvss librairy
Language: C++\Qt

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: C++ Libraries API for WCCOA CTRL

Limitations: To be used with vacuum framework.

Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------
#ifndef _VACCTLEQPDATAPVSS_H_
#define _VACCTLEQPDATAPVSS_H_

# ifdef _WIN32
#   define  PVSS_EXPORT  __declspec(dllexport)
# else
#   define  PVSS_EXPORT
# endif

#include "VacCtlPvssUtil.h"

#include "DataEnum.h"

class Eqp;
class Sector;
class PlcConfig;
#include <QVariant>

class AnyTypeVar;

class VacCtlEqpDataPvss : public VacCtlPvssUtil
{
public:
	VacCtlEqpDataPvss(BaseExternHdl *nextHdl, PVSSulong funcCount, FunctionListRec fnList[])
		: VacCtlPvssUtil(nextHdl, funcCount, fnList) {}

	virtual const Variable *execute(ExecuteParamRec &param);

protected:
	int processSetDataPart(ExecuteParamRec &param);
	int processInitPassive(ExecuteParamRec &param);
	int processInitActive(ExecuteParamRec &param);
	int processInitRackData(ExecuteParamRec &param);
	int processInitInterlockData(ExecuteParamRec &param);
	int processAddGlobalEqp(ExecuteParamRec &param);
	int processSetMinSectorLength(ExecuteParamRec &param);
	int processGetTimeStamp(ExecuteParamRec &param);
	int processLinuxUserName(ExecuteParamRec &param);
	int processSetPicturePath(ExecuteParamRec &param);


	int processGetAllMainParts(ExecuteParamRec &param);
	int processGetSectorsOfMainPart(ExecuteParamRec &param);
	int processGetAllSectors(ExecuteParamRec &param);
	int processGetAllDomains(ExecuteParamRec &param);
	int processPvssNameOfSector(ExecuteParamRec &param);
	int processPvssNameOfMainPart(ExecuteParamRec &param);
	int processVacTypeOfSector(ExecuteParamRec &param);
	int processVacTypeOfMainPart(ExecuteParamRec &param);
	int processDomainsOfMainPart(ExecuteParamRec &param);
	int processDomainsOfSector(ExecuteParamRec &param);
	/* L.Kopylov 25.05.2012
	int processSetSectorSpecSynoptic(ExecuteParamRec &param);
	*/
	int processSetSectorSpecSynPanel(ExecuteParamRec &param);
	int processSetSectorSpecSectPanel(ExecuteParamRec &param);
	int processIsSectorInMainPart(ExecuteParamRec &param);
	int processGetSectorsOfDomains(ExecuteParamRec &param);

	int processSetPvssFunctionalType(ExecuteParamRec &param);
	int processDpType(ExecuteParamRec &param);
	int processMobileProp(ExecuteParamRec &param);
	int processDisplayName(ExecuteParamRec &param);
	int processDpName(ExecuteParamRec &param);
	int processDeviceLocation(ExecuteParamRec &param);
	int processDeviceAccessPointDistance(ExecuteParamRec &param);
	int processDeviceVacLocation(ExecuteParamRec &param);
	int processDeviceDomain(ExecuteParamRec &param);
	int processGetAllAttributes(ExecuteParamRec &param);
	int processGetAttribute(ExecuteParamRec &param);
	int processGetCtlAttributes(ExecuteParamRec &param);
	int processGetByAttrValue(ExecuteParamRec &param);
	int processEqpCtlStatus(ExecuteParamRec &param);
	int processEqpMaster(ExecuteParamRec &param);
	int processEqpConfig(ExecuteParamRec &param);
	int processEqpSlaves(ExecuteParamRec &param);
	int processEqpTargets(ExecuteParamRec &param);
	int processEqpMasterProcess(ExecuteParamRec &param);
	int processEqpStateDpes(ExecuteParamRec &param);
	int processEqpValueDpes(ExecuteParamRec &param);
	int processSelectEqp(ExecuteParamRec &param);
	int processGetDevicesAtSectors(ExecuteParamRec &param);
	int processGetDevicesOfDpTypesAtSectors(ExecuteParamRec &param);
	int processGetDevicesAtMainParts(ExecuteParamRec &param);
	int processGetDevicesMatchingFilter(ExecuteParamRec &param);
	int processGetThermometersForValve(ExecuteParamRec &param);
	int processGetNeighbourValves(ExecuteParamRec &param);
	int processAddBakeoutWorkDP(ExecuteParamRec &param);
	int processSetMobileEqp(ExecuteParamRec &param);
	int processSetWireLessEqp(ExecuteParamRec &param);
	int processAllEqpWithSameVisName(ExecuteParamRec &param);
	int processNextMobileHistoryQuery(ExecuteParamRec &param);
	int processSetMobileEqpHistory(ExecuteParamRec &param);
	int processNextDpeHistoryDepthQuery(ExecuteParamRec &param);
	int processSetDpeHistoryDepth(ExecuteParamRec &param);
	int processEqpSetActive(ExecuteParamRec &param);
	int processGetEqpIconType(ExecuteParamRec &param);
	int processGetEqpByMasterAndChannelNb(ExecuteParamRec &param);

	int processEqpMainStateString(ExecuteParamRec &param);
	int processEqpPartStateString(ExecuteParamRec &param);
	int processEqpMainValueString(ExecuteParamRec &param);
	int processEqpMainStateStringForMenu(ExecuteParamRec &param);
	int processEqpMainColor(ExecuteParamRec &param);
	int processEqpPartColor(ExecuteParamRec &param);
	int processSubEqpColor(ExecuteParamRec &param);
	int processEqpDpeValue(ExecuteParamRec &param);
	int processEqpPlcAlarm(ExecuteParamRec &param);
	int processEqpGetHistoryState(ExecuteParamRec &param);

	int processNextDpConnect(ExecuteParamRec &param);
	int processNextDpPolling(ExecuteParamRec &param);
	int processNewOnlineValue(ExecuteParamRec &param);
	int processNewReplayValue(ExecuteParamRec &param);

	int processNextHistoryQuery(ExecuteParamRec &param);
	int processAddHistoryValues(ExecuteParamRec &param);

	int processDevListGetDpes(ExecuteParamRec &param);
	int processDevListCheckCrit(ExecuteParamRec &param);
	int processDevListGetHistoryParam(ExecuteParamRec &param);
	int processDevListCheckHistory(ExecuteParamRec &param);
	int processDevListCheckValueGrow(ExecuteParamRec &param);

	int processOpenDialog(ExecuteParamRec &param);
	int processOpenDocumentationDialog(ExecuteParamRec &param);
	int processMakeDeviceVisible(ExecuteParamRec &param);
	int processConfirmEvConnLost(ExecuteParamRec &param);

	int processAddToHistoryDialog(ExecuteParamRec &param);
	int processAddToHistoryInstance(ExecuteParamRec &param);
	int processAddDpeToHistoryDialog(ExecuteParamRec &param);
	int processAddDpeToHistoryInstance(ExecuteParamRec &param);
	int processAddBitToHistoryDialog(ExecuteParamRec &param);
	int processAddBitToHistoryInstance(ExecuteParamRec &param);
	int processCreateHistoryInstance(ExecuteParamRec &param);
	int processClearHistoryInstance(ExecuteParamRec &param);
	int processGetHistoryFilterType(ExecuteParamRec &param);
	int processSetHistoryFilterType(ExecuteParamRec &param);
	int processGetAllHistoryItems(ExecuteParamRec &param);
	int processGetHistoryTimeScale(ExecuteParamRec &param);
	int processSetHistoryTimeScale(ExecuteParamRec &param);
	int processGetHistoryVerticalScales(ExecuteParamRec &param);
	int processSetHistoryVerticalScale(ExecuteParamRec &param);
	int processGetHistoryTitle(ExecuteParamRec &param);
	int processSetHistoryTitle(ExecuteParamRec &param);
	int processGetHistoryGeometry(ExecuteParamRec &param);
	int processSetHistoryGeometry(ExecuteParamRec &param);
	int processGetAllHistoryInstances(ExecuteParamRec &param);
	int processSetHistoryConfigName(ExecuteParamRec &param);
	int processGetHistoryConfigName(ExecuteParamRec &param);
	int progessGetHistoryAllComments(ExecuteParamRec &param);
	int progessSetHistoryAllComments(ExecuteParamRec &param);

	int processDpForMobileOnFlange(ExecuteParamRec &param);
	int processDpForMobileOnSector(ExecuteParamRec &param);

	int processSetDebugMode(ExecuteParamRec &param);

	int processChangeSectorState(ExecuteParamRec &param); // [VACCO-948] [VACCO-1645]
	int processDisplayNameOfSector(ExecuteParamRec &param); // [VACCO-948] [VACCO-1645]


	Eqp *findDp(const char *dpName, ExecuteParamRec &param, DynVar *exceptionInfo);
	Sector *findSectorDp(const char *dpName, ExecuteParamRec &param, DynVar *exceptionInfo); // [VACCO-948] [VACCO-1645]

	bool checkMode(ExecuteParamRec &param, int mode, QString &errMsg);
	void clearAllActiveMobile(DataEnum::DataMode mode);

	int findSourcePlc(const char *sourceDp, PlcConfig **ppPlc, QString &errMsg);
	Eqp *findMobileOnFlange(int mobileType, const char *flangeName, const char *dpType);
	Eqp *findMobileOnSector(int mobileType, Sector *pSector, int indexInSector, const char *dpType);

};

#endif	// _VACCTLEQPDATAPVSS_H_
