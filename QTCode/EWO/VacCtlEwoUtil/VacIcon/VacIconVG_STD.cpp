//	Implementation of VacIconVG_STD_STD class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVG_STD.h"

#include "EqpVG_STD.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVG_STD::VacIconVG_STD(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 21);
}

VacIconVG_STD::~VacIconVG_STD()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVG_STD::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(21, 21, 10, 24, 1);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVG_STD::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVG_STD"), "VacIconVG_STD::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVG_STD *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVG_STD::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVG_STD::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("PR");
	dpes.append("BlockedOFF");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVG_STD::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVG_STD::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVG_STD::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Gauge image is NOT rotated (does not depend on direction), but
	// connecting pipe IS rotated
	if(drawConnectionPipe)
	{
		switch(direction)
		{
		case VacIconContainer::Up:	// No rotation
			painter.fillRect(10 - pipeWidth / 2, 18, pipeWidth ? pipeWidth : 1, 4, pipeColor);
			break;
		case VacIconContainer::Down:	// 180 degrees
			painter.fillRect(10 - pipeWidth / 2, 0, pipeWidth ? pipeWidth : 1, 4, pipeColor);
			break;
		case VacIconContainer::Left:	// 90 degrees clockwise
			painter.fillRect(0, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
			break;
		case VacIconContainer::Right:	// 90 degrees conuterclockwise
			painter.fillRect(18, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
			break;
		}
	}

	// Pipe is ready - draw gauge image
	drawImage(painter);
}

void VacIconVG_STD::drawImage(QPainter &painter)
{
	setPainterForMainImage(painter);

	// Filled circle
	QPen pen = painter.pen();
	painter.setPen(Qt::NoPen);
	painter.drawPie(1, 1, 19, 19, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawEllipse(1, 1, 19, 19);

	// type-specific part of image
	drawSpecific(painter);

	// Draw blocked OFF symbol
	if(pEqp)
	{
		if(pEqp->isBlockedOff(mode))
		{
			QPen blackPen(Qt::black, 2);
			painter.setPen(blackPen);
			painter.drawLine(0, 13, 20, 20);
			painter.drawLine(0, 20, 20, 13);
		}
	}
}

void VacIconVG_STD::setPainterForMainImage(QPainter &painter)
{
	QColor fillColor(COLOR_UNDEFINED);
	QColor lineColor = Qt::black;
	if(pEqp)
	{
		pEqp->getMainColor(fillColor, mode);
	}
	painter.setBrush(fillColor);
	QPen pen(lineColor, lineColor == Qt::black ? 1 : 2);
	painter.setPen(pen);
}

