#include "RackIconVRGPT300.h"

#include "DataPool.h"
#include "Eqp/EqpVR_GT.h"

#include "qpainter.h"

RackIconVRGPT300::RackIconVRGPT300(QWidget *parent, Qt::WindowFlags f) :
RackIcon(parent, f)
{
	pEqp = NULL;
	//setFixedSize(21, 21); see about this later
}

RackIconVRGPT300::~RackIconVRGPT300()
{
	disconnect();
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRGPT300::disconnect(void)
{
	bool result = false;
	if (connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	
	connected = false;
	
	return result;
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRGPT300::connect(void)
{
	if (connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	//dpes.append("PR");
	connection.connect(pEqp, dpes, DataEnum::Online);
	connected = true;
	return true;


}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void RackIconVRGPT300::setEqp(Eqp *pEqp)
{
	disconnect();
	DataPool &pool = DataPool::getInstance();
	if (pEqp != NULL)
	{
		//Q_ASSERT_X(pEqp->inherits("EqpVRPI"), "RackIconVRPIT::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVR_GT *)pEqp;
	}
	forceRedraw();
}

void RackIconVRGPT300::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.scale(scale, scale);
	QPen selectionPen = QPen(Qt::red);
	selectionPen.setWidth(3);
	QFont calibriExtraSmallBold("Calibri", 6, QFont::Bold);
	QFont calibriExtraSmall("Calibri", 6);
	int textFlags;

	if (isSelected)
	{
		painter.setPen(selectionPen);
	}
	else
	{
		painter.setPen(Qt::black);
	}

	int lineSplitIndex = this->equipmentName.indexOf("\.", 7);
	QString firstLine = this->equipmentName.left(lineSplitIndex + 1);
	QString secondLine = this->equipmentName.right(this->equipmentName.length() - (lineSplitIndex + 1));

	painter.setBrush(QColor(220, 220, 220));
	painter.setFont(calibriExtraSmallBold);
	textFlags = Qt::AlignHCenter | Qt::AlignTop | Qt::TextWrapAnywhere;

	QRect equipmentRect(0, 0, this->sizeX, this->sizeY);
	//painter.setPen(Qt::black);
	painter.drawRect(equipmentRect);
	painter.setPen(Qt::black);
	painter.drawText(equipmentRect, textFlags, firstLine);
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 7, equipmentRect.width(), equipmentRect.height() - 7, textFlags, secondLine);

	painter.setFont(calibriExtraSmall);
	textFlags = Qt::AlignCenter;

	QString attributeValue;
	attributeValue = attributes.value("CableGaugeA1");
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 16, equipmentRect.width(), 7, textFlags, attributeValue + " (A1)");
	attributeValue = attributes.value("CableGaugeA2");
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 23, equipmentRect.width(), 7, textFlags, attributeValue + " (A2)");
	attributeValue = attributes.value("CableGaugeB1");
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 30, equipmentRect.width(), 7, textFlags, attributeValue + " (B1)");
	attributeValue = attributes.value("CableGaugeB2");
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 37, equipmentRect.width(), 7, textFlags, attributeValue + " (B2)");

}


