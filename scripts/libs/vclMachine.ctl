
// Functions and variables for machine identification

// Controlled accelerator name
global string				glAccelerator;

// Name of DP holding machine ID and timestamp of imported data
const string MACHINE_CTL_DP = "_VacMachineInfo";

// Bit masks for vacuum selection - the same constants
// are used by VacEqpData.dll
const unsigned LVA_VACUUM_ANY_BEAM =     0x00000001u;
const unsigned LVA_VACUUM_BLUE_BEAM =     0x00000002u;
const unsigned LVA_VACUUM_RED_BEAM =    0x00000004u;
const unsigned LVA_VACUUM_CROSS_BEAM =   0x00000008u;
const unsigned LVA_VACUUM_BEAMS_COMMON = 0x00000010u;
const unsigned LVA_VACUUM_QRL =          0x00000020u;
const unsigned LVA_VACUUM_CRYO =         0x00000040u;
const unsigned LVA_VACUUM_ANY =          0x0000007Fu;


//LhcVacGetMachineId:
/**
  Read machine identification, it must be written in special DPE.
  Resulting machine ID will be written to global variable glAccelerator

  @param exceptionInfo: out, details of errors detected will be written here

  @return None
  @author LIK
*/
void LhcVacGetMachineId(dyn_string &exceptionInfo)
{
  dynClear(exceptionInfo);
  if(!dpExists(MACHINE_CTL_DP))
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetMachineId(): DP <" + MACHINE_CTL_DP +
      "> does not exist - giving up...", "");
    return;
  }
  if(dpGet(MACHINE_CTL_DP + ".MACHINE", glAccelerator) < 0)
  {
    fwException_raise(exceptionInfo, "ERROR",
      "LhcVacGetMachineId(): Failed to read machine name - giving up...", "" );
    return;
  }
  if(glAccelerator == "")
  {
    fwException_raise(exceptionInfo, "ERROR",
      "LhcVacGetMachineId(): Empty machine name - giving up...", "");
    return;
  }
}
//LhcVacGetDataTimeStamp:
/**
  Read timestamp of data imported to PVSS from dedicated DP

  @param exceptionInfo: out, details of errors detected will be written here

  @return None
  @author LIK
*/
void LhcVacGetDataTimeStamp(string &timeStamp, dyn_string &exceptionInfo)
{
  timeStamp = "";
  dynClear(exceptionInfo);
  if(!dpExists(MACHINE_CTL_DP))
  {
    fwException_raise(exceptionInfo, "ERROR", "LhcVacGetDataTimeStamp(): DP <" + dpName +
      "> does not exist - giving up...", "");
    return;
  }
  if(dpGet(MACHINE_CTL_DP + ".TIMESTAMP", timeStamp) < 0)
  {
    fwException_raise(exceptionInfo, "ERROR",
      "LhcVacGetDataTimeStamp(): Failed to read data timestamp - giving up...", "");
  return;
  }
}
