//	Implementation of SynLhcSectorLabel class
/////////////////////////////////////////////////////////////////////////////////

#include "SynLhcSectorLabel.h"

#include "Sector.h"
#include "Eqp.h"
#include "DebugCtl.h"

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

SynLhcSectorLabel::SynLhcSectorLabel(Sector *pSector)
{
	this->pSector = pSector;
	start = end = 0;
	endPos = 0;
	byEqp = drawStart = drawEnd = false;

	startOrder = endOrder = corrDelta = 0;
	sectorGraph.clear(); // [VACCO-929]
}

SynLhcSectorLabel::~SynLhcSectorLabel()
{
}

void SynLhcSectorLabel::clearGraph(void) {
	while (!sectorGraph.isEmpty()) {
		delete sectorGraph.takeFirst();
	}
}

int SynLhcSectorLabel::addToGraph(int x1, int y1, int x2, int y2) {
	sectorGraph.append(new QLine(x1, y1, x2, y2));
	return sectorGraph.count();
}

int SynLhcSectorLabel::addToGraph(const QPoint &p1, const QPoint &p2) {
	sectorGraph.append(new QLine(p1, p2));
	return sectorGraph.count();
}

bool SynLhcSectorLabel::isPointOnSector(int x, int y, int tolerance) const {
	for (int idx3 = 0; idx3 < sectorGraph.count(); idx3++) {
		QLine *pLine = sectorGraph.at(idx3);

		// It is expected that y1 == y2
		if ((x >= pLine->x1()) && (x <= pLine->x2())
			&& (y <= (pLine->y1() + tolerance)) && (y >= (pLine->y1() - tolerance))) {

			return true;
		}
	}
	return false;
}

bool SynLhcSectorLabel::isPointOnSector(const QPoint &p, int tolerance) const {
	return isPointOnSector(p.x(), p.y(), tolerance);
}


/*
**	FUNCTION
**		Write all parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**		idx		- Index of this instance in the list
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcSectorLabel::dump(FILE *pFile, int idx)
{
	if(!DebugCtl::isSynoptic())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	fprintf(pFile, "  %02d: <%s> %s (endPos %f) from %d [%d] to %d [%d] drawStart %d drawEnd %d (corr %d)\n",
		idx, pSector->getName(), (byEqp ? "EQP" : "PART"),
		endPos, start, startOrder, end, endOrder, drawStart, drawEnd, corrDelta);
}
