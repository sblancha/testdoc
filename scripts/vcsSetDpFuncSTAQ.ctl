

void main()
{
  DebugTN("vcsSetDpFuncSTAQ.ctl: start");
  ProcessDpType("VGF_C");
  ProcessDpType("VGF_C0");
  ProcessDpType("VGFH_C");
  ProcessDpType("VGI");
  ProcessDpType("VGM_C");
  ProcessDpType("VGM_C0");
  ProcessDpType("VGP_C");
  ProcessDpType("VGP_C0");
  ProcessDpType("VGP_T");
  ProcessDpType("VGR_C");
  ProcessDpType("VGR_C0");
  ProcessDpType("VGR_T");
  ProcessDpType("VGR_T0");
  ProcessDpType("VGTR_C0");
  ProcessDpType("VLV");
  ProcessDpType("VLV_C0");
  ProcessDpType("VP_STDIO");
  ProcessDpType("VPG_6A01");
  ProcessDpType("VPI");
  ProcessDpType("VPT100_C0");
  ProcessDpType("VV_AO");
  ProcessDpType("VV_PULSED");
  ProcessDpType("VV_STD_IO");
  ProcessDpType("VV_STD_6E01");
  ProcessDpType("VVF_S");
  ProcessDpType("VVS_LHC");
  ProcessDpType("VVS_PS");
  ProcessDpType("VVS_PSB_SR");
  ProcessDpType("VVS_PSB_SUM");
  ProcessDpType("VVS_S");
  ProcessDpType("VVS_SV");
  DebugTN("vcsSetDpFuncSTAQ.ctl: finish");
}

void ProcessDpType(string dpType)
{
  dyn_string allDpNames = dpNames("*", dpType);
  DebugTN("vcsSetDpFuncSTAQ.ctl: ProcessDpType(" + dpType + "): " + dynlen(allDpNames) + " DPs");
  for(int n = dynlen(allDpNames) ; n > 0 ; n--)
  {
    string dpName = dpSubStr(allDpNames[n], DPSUB_DP);
    if(!dpExists(dpName + ".State"))
    {
      DebugTN("vcsSetDpFuncSTAQ.ctl: no State DPE in " + dpType);
      return;
    }
    if(!dpExists(dpName + ".STAQ"))
    {
      DebugTN("vcsSetDpFuncSTAQ.ctl: no STAQ DPE in " + dpType);
      return;
    }
    int funcType;
    dpGet(dpName + ".State:_dp_fct.._type", funcType);
    if(funcType != DPCONFIG_DP_FUNCTION)
    {
      continue;
    }
    DebugTN("vcsSetDpFuncSTAQ.ctl: found DP function in " + dpName);
    dyn_string exceptionInfo;
    SetDpFunctionSTAQ(dpName, exceptionInfo);
  }
}

void SetDpFunctionSTAQ(string dpTarget, dyn_string &exceptionInfo)
{
  dyn_string params = makeDynString(dpSubStr(dpTarget, DPSUB_DP) + ".State:_online.._value");
  // Function is different for valves and gauges+pumps
  string function;
  string dpType = dpTypeName(dpTarget);
  switch(dpType)
  {
  case "VV_AO":
  case "VV_PULSED":
  case "VV_STD_IO":
  case "VV_STD_6E01":
  case "VVF_S":
  case "VVS_LHC":
  case "VVS_PS":
  case "VVS_PSB_SR":
  case "VVS_PSB_SUM":
  case "VVS_S":
  case "VVS_SV":
    function = "(p1 & 0x1) ? ((p1 & 0x6) == 0x2 ? 3 : ((p1 & 0x6) == 0x4 ? 2 : 1)) : 1";
    break;
  default:
    function = "(p1 & 0x1) ? ((p1 & 0x6) == 0x2 ? 10 : ((p1 & 0x6) == 0x4 ? 11 : 1)) : 1";
    break;
  }
  dpSetWait(dpTarget + ".STAQ:_dp_fct.._type", DPCONFIG_DP_FUNCTION );
  dyn_errClass err = getLastError(); 
  if(dynlen(err) > 0)
  { 
    throwError(err);
    fwException_raise(exceptionInfo, "ERROR",
      "SetDpFunctionSTAQ():  Could not create dp function config for <" +
      dpTarget + ">", "");
    DebugN(exceptionInfo);
    return;
  }
  dpSetWait(dpTarget + ".STAQ:_dp_fct.._param", params,
    dpTarget + ".STAQ:_dp_fct.._fct", function);
  err = getLastError(); 
  if(dynlen(err) > 0)
  {
    DebugN("FAILURE dpSetWait for <", dpTarget + ".STAQ:_dp_fct.._param> <", dpTarget + ".STAQ:_dp_fct.._fct>");
    DebugN("PARAMS: ", params);
    DebugN( "FUNCTION: ", function );
    throwError(err);
    fwException_raise(exceptionInfo, "ERROR",
      "SetDpFunctionSTAQ():  Could not configure dp function for <" + dpTarget + ">", "");
    DebugN(exceptionInfo);
  }
}
