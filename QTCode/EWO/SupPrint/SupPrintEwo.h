#ifndef SUPPRINTEWO_H
#define	SUPPRINTEWO_H

//	EWO interface for SupPrint widget

#include <BaseExternWidget.hxx>
#include <SupPrint.h>


class EWO_EXPORT SupPrintEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	SupPrintEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;


public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

		
private:
	SupPrint *baseWidget;


private slots:
	void printRequest(void);

};


#endif	// SUPPRINTEWO_H
