//	Implementation of ImageItemEllipse class
/////////////////////////////////////////////////////////////////////////////////

#include "ImageItemEllipse.h"

#include <QLocale>

//////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
//////////////////////////////////////////////////////////////////////////////////

ImageItemEllipse::ImageItemEllipse() : ImageItem()
{
	centerX = centerY = 0;
	radiusX = radiusY = 0;
}

ImageItemEllipse::~ImageItemEllipse()
{
}


void ImageItemEllipse::draw(QPainter & painter)
{
	painter.setTransform(transform);
	if(backColor.alpha() > 0)
	{
		painter.setBrush(backColor);
	}
	else
	{
		painter.setBrush(Qt::NoBrush);
	}
	setForePen(painter);


	QRectF rect(centerX - radiusX, centerY - radiusY, radiusX * 2 + 1, radiusY * 2 + 1);

	painter.drawEllipse(rect);
	/*
	if(backColor.alpha() > 0)
	{
		painter.drawPie(rect, 0, 5760);
	}
	else
	{
		painter.drawEllipse(rect);
	}
	*/
}

bool ImageItemEllipse::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	if(!visible)
	{
		return false;
	}
	if(toolTipText.isEmpty())
	{
		return false;
	}
	if((radiusX <= 0) || (radiusY <= 0))
	{
		return false;
	}
	float bound = ((float)(point.x() - centerX) * (float)(point.x() - centerX)) /
		((float)radiusX * (float)radiusX) +
		((float)(point.y() - centerY) * (float)(point.y() - centerY)) /
		((float)radiusY * (float)radiusY);
	if(bound > 1)
	{
		return false;
	}
	text = toolTipText;
	rect.setRect(point.x() - 1, point.y() - 1, 3, 3);
	return true;
}

void ImageItemEllipse::parseProp(QXmlStreamReader &reader)
{
	QStringRef propName = reader.attributes().value("name");
	if(!propName.compare("Center", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		centerX = (int)rint(c.toDouble(str.section(' ', 0, 0)));
		centerY = (int)rint(c.toDouble(str.section(' ', 1, 1)));
	}
	else if(!propName.compare("X-Radius", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		radiusX = (int)rint(c.toDouble(str));
	}
	else if(!propName.compare("Y-Radius", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		radiusY = (int)rint(c.toDouble(str));
	}
	else
	{
		ImageItem::parseProp(reader);
	}
}

