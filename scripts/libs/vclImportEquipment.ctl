// CTRL Library
// created from lhcVacImportEquipment.ctl for QT
// @SAA 16may10

// Load CTRL libraries

#uses "vclSectors.ctl"	// Load CTRL library

//#uses "lhcVacProgress.ctl" //BEFORE_QT

//#uses "lhcVacArchiveConfig.ctl" //BEFORE_QT

//#uses "lhcVacDeviceFileParser.ctl" //BEFORE_QT

//#uses "lhcVacEqpConfig.ctl" //BEFORE_QT

// as enums ...
const int
	FLAG_PUT  = 0,
	FLAG_GET  = 1;

//
// accelerator name;
// its initialized when accelerator panel started
// to compare by import equipment panel after "start" button pressed
//
global string	acceleratorForImport;

// Timestamp of file for import
global string	glImportTimeStamp;

// **********************************************************
// QT *******************************************************
// new functions, variables sections


// new function sections *************************************
// ***********************************************************


//LhcVacSetDataTimeStamp:
/**
	Set timestamp of data imported to PVSS - write to dedicated DPE

	@param exceptionInfo: out, details of errors detected will be written here

	@return None
	@author LIK
*/
void LhcVacSetDataTimeStamp( string timeStamp, dyn_string &exceptionInfo )
{
	dynClear( exceptionInfo );
	if( ! dpExists( MACHINE_CTL_DP ) )
	{
		fwException_raise( exceptionInfo, "ERROR",
			"LhcVacSetDataTimeStamp(): DP <" + dpName + "> does not exist - giving up...", "" );
		return;
	}
	if( dpSetWait( MACHINE_CTL_DP + ".TIMESTAMP:_original.._value", timeStamp ) < 0 )
	{
		fwException_raise( exceptionInfo, "ERROR",
			"LhcVacSetDataTimeStamp(): Failed to set data timestamp DPE - giving up...", "" );
		return;
	}
}

//ChangeImportInProgressState:
/**
	Set _VacMachineInfo.ImportInProgress dpe into TRUE or FALSE
	if TRUE then IMPORT processing
	if FALSE then IMPORT dialogue is closed
	
	@return None
	@author ASA
	@date 07apr08
*/
void ChangeImportInProgressState( bool state )
{
dyn_string exceptionInfo;
	
	//dynClear( exceptionInfo );
	
	if( ! dpExists( MACHINE_CTL_DP ) )
	{
		fwException_raise( exceptionInfo, "ERROR",
			"ChangeImportInProgressState(): DP <" + dpName + "> does not exist.", "" );
		fwExceptionHandling_display( exceptionInfo );
		return;
	}
	if( dpSetWait( MACHINE_CTL_DP + ".ImportInProgress:_original.._value", state ) < 0 )
	{
		fwException_raise( exceptionInfo, "ERROR",
			"ChangeImportInProgressState(): Failed to set ImportInProgress flag.", "" );
		fwExceptionHandling_display( exceptionInfo );
		return;
	}
}


//LhcVacDelSectorSummaryDpImport (modified from)<-LhcVacDelSectorSummaryDp
/** Delete all DPs used as a summary of VPI states in sector

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param progressBarLabel: in, shape of progress bar label
	@param exceptionInfo: out, details of any exceptions are returned here

	@return none
	@author L.Kopylov
	@modify ASA - 3apr06
	@modify ASA - 21sep11
*/
void LhcVacDelSectorSummaryDpImport( shape progressBarLabel, dyn_string &exceptionInfo )
{
  dyn_string    allSectDps;
  int           listLen, n;
  dyn_errClass  err;

  allSectDps = dpNames( "*", LHC_VAC_SECT_VPI_DP_TYPE );
  listLen = dynlen( allSectDps );
  //ProgressLabelStart( progressBarLabel );        
  for( n = 1 ; n <= listLen ; n++ )
  {
    //LhcVacReportLongAct( n, listLen );
    //ProgressLabelSet( progressBarLabel, n, listLen );
		
    dpDelete( allSectDps[n] );
    err = getLastError( ); 
    if( dynlen( err ) > 0 )
    { 
      throwError( err );
      fwException_raise( exceptionInfo, "ERROR",
        "LhcVacDelSectorSummaryDpImport(): delete DP <" + allSectDps[n] + "> failed:" + err, "");
    }
  }
  //ProgressLabelFinish( progressBarLabel, "" );        
}

//LhcVacInitSectorSummaryDpImport (modified from)<-LhcVacInitSectorSummaryDp
/** Create and configure all DPs used as a summary of VPI states in sector

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param progressBarLabel: in, shape of progress bar label
	@param exceptionInfo: out, details of any exceptions are returned here

	@return none
	@author L.Kopylov
	@modify ASA - 3apr06
	@modify ASA - 21sep11
*/
void LhcVacInitSectorSummaryDpImport( shape progressBarLabel, dyn_string &exceptionInfo )
{
  dynClear( exceptionInfo );

  // Delete existing auxilliary DPs
  LhcVacDelSectorSummaryDpImport( progressBarLabel, exceptionInfo );
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }

  // Init sector data
  dyn_string allMainParts;
  dyn_dyn_string  allMpSectors, allMpDomains;
  if(LhcVacGetAllSectors(allMainParts, allMpSectors, allMpDomains, exceptionInfo) < 0)
  {
    DebugTN( "LhcVacInitSectorSummaryDpImport(): ", exceptionInfo );
    return;
  }

  // Count total number of DPs to be processed
  int nMp = dynlen(allMpSectors);
  /*
  float totalSteps = 0;
  for(int n = 1 ; n <= nMp ; n++)
  {
    totalSteps += dynlen( glVacSectors[n] );
  }
  float oneSectPart = 79.9 / ( totalSteps + 1 );
  */
	
  //Process all sectors
  //ProgressLabelStart( progressBarLabel );        
  for( int n = 1 ; n <= nMp ; n++ )
  {
    //ProgressLabelSet( progressBarLabel, n, nMp );

    int nSect = dynlen(allMpSectors[n]);
    dyn_string localExceptionInfo;
    dynClear(localExceptionInfo);
    for(int i = 1 ; i <= nSect ; i++)
    {
      // skip cases when sector is presented more than once in list of sectors
      bool sectorFound = false;
      for(int k = n; k > 0  ; k--)
      {
        for(int l = (k == n ? i - 1 : dynlen(allMpSectors[k])) ; l > 0 ; l--)
        {
          if(l <= 0)
          {
            break;
          }
          if(allMpSectors[n][i] == allMpSectors[k][l])
          {
            sectorFound = true;
            break;
          }
        }
        if(sectorFound)
        {
          break;
        }
      }
      if(sectorFound)
      {
        //DebugTN("skip duplicating sector", n, i,allMpSectors[n][i]);
        continue;
      }
      // Process sector

      //LhcVacStartLongActPart( oneSectPart );

      // Find or create DP
      string sectDpName;
      LhcVacSectSummaryDpName(allMpSectors[n][i], sectDpName, localExceptionInfo);
      if(dynlen(localExceptionInfo) > 0)
      {
        dynAppend(exceptionInfo, localExceptionInfo);
        //LhcVacFinishLongActPart();
        continue;
      }
      dpCreate(sectDpName, LHC_VAC_SECT_VPI_DP_TYPE);
      dyn_errClass err = getLastError(); 
      if(dynlen(err) > 0)
      { 
        throwError(err);
        fwException_raise(exceptionInfo, "ERROR",
          "LhcVacInitSectorSummaryDpImport(): Create DP <" + sectDpName + "> failed:" + err, "");
        //LhcVacFinishLongActPart( );
        continue;
      }
      LhcVacInitSummaryForSector(allMpSectors[n][i], sectDpName, exceptionInfo);

    }
  }
  //ProgressLabelFinish( progressBarLabel, "" );        
}
//
// LhcVacInitSummaryForSector
//
void LhcVacInitSummaryForSector(string sectorName, string sectDpName, dyn_string &exceptionInfo)
{
  dyn_string localExceptionInfo;

  // Fetch all devices in sector, process all VPIs
  dyn_string allDpNames;
  LhcVacGetDevicesOfDpTypesAtSectors(makeDynString(sectorName), makeDynString("VPI"), allDpNames, localExceptionInfo);
  if(dynlen(localExceptionInfo) > 0)
  {
    dynAppend(exceptionInfo, localExceptionInfo);
    return;
  }

  // Build list of VRPIs
  dyn_string vrpiNames;
  int nVpi = 0;
  for(int n = dynlen(allDpNames) ; n > 0 ; n--)
  {
    nVpi++;
    string master;
    int channel;
    LhcVacEqpMaster(allDpNames[n], master, channel);
    if(master == "")
    {
      fwException_raise(exceptionInfo, "ERROR",
        "LhcVacInitSummaryForSector(): No master for <" + allDpNames[n] + ">", "");
          //LhcVacFinishLongActPart();
      continue;
    }

    if(dynContains(vrpiNames, master) < 1)
    {
      dynAppend(vrpiNames, master);
    }
  }

  // Build parameters and functions
  int parNum = 1;
  dyn_string params;
  string functionOr = "";
  string functionAnd = "";
  for(int n = dynlen(vrpiNames) ; n > 0 ; n--)
  {
    dynAppend(params, vrpiNames[n] + ".RR1:_online.._value");
    if(parNum > 1)
    {
      functionOr += "|p" + parNum;
      functionAnd += "&p" + parNum;
    }
    else
    {
      functionOr = "p1";
      functionAnd = "p1";
    }
    parNum++;
  }

  // Set parameters for sector DP
  dpSetWait(
    sectDpName + ".nVPI:_original.._value", nVpi,
    sectDpName + ".nVRPI:_original.._value", dynlen(vrpiNames));
  dyn_errClass err = getLastError(); 
  if(dynlen(err) > 0)
  { 
    throwError(err);
    fwException_raise(exceptionInfo, "ERROR",
      "LhcVacInitSummaryForSector(): set nVPI = " + parNum - 1 + " for <" + sectDpName + "> failed:" +
      err, "");
    return;
  }
  if(parNum > 1)
  {
    dpSetWait(
      sectDpName + ".RRC:_dp_fct.._type", DPCONFIG_DP_FUNCTION );
    err = getLastError(); 
    if(dynlen(err) > 0)
    { 
      throwError(err);
      fwException_raise(exceptionInfo, "ERROR",
        "LhcVacInitSummaryForSector():  Could not create dp function config for <" +
        sectDpName + ">", "");
      return;
    }

    // Bits 31...16 - OR'ed bits 31...16 of RR1, bits 15...0 - AND'ed bits 31...16 of RR1
    string function = "((" + functionOr + ") & 0xFFFF0000u ) | ((" + functionAnd + ") >> 16 )";

    //if(sectDpName == "VSECT_VPI_SUM_A4L2_C")
    //{
    //DebugTN( "params", params );
    //DebugTN( "function", function );
    //}
								
    for(int n = dynlen(params) ; n > 0 ; n--)
    {
      if(!dpExists(params[n]))
      {
        fwException_raise(localExceptionInfo, "ERROR",
          "LhcVacInitSummaryForSector():  Could not configure dp function for <" + sectDpName +
          ">, dp <" + params[n] + "> not exist;", "");
        return;
      }
    }
    if(dynlen(localExceptionInfo) > 0)
    {
      dynAppend(exceptionInfo, localExceptionInfo);
      return;
    }

    dpSetWait(
      sectDpName + ".RRC:_dp_fct.._param", params,
      sectDpName + ".RRC:_dp_fct.._fct", function );
    err = getLastError(); 
    if(dynlen(err) > 0)
    {
      DebugN("FAILURE dpSetWait for <", sectDpName + ".RRC:_dp_fct.._param> <", sectDpName + ".RRC:_dp_fct.._fct>");
      DebugN("PARAMS: ", params);
      DebugN("FUNCTION: ", function);
      throwError(err);
      fwException_raise(exceptionInfo, "ERROR",
        "LhcVacInitSummaryForSector():  Could not configure dp function for <" + sectDpName + ">",
            "");
    }
				
  }
}



//LhcVacInitCryoSumDp
/** Set DP functions (analog of address) for all VCRYO_TT_SUM DPs in system

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param progressBarLabel: in, shape of progress bar label
	@param exceptionInfo: out, details of any exceptions are returned here

	@return none
	@author L.Kopylov
	@modify ASA - 21sep11
*/
void LhcVacInitCryoSumDp( shape progressBarLabel, dyn_string &exceptionInfo )
{
	dyn_string dpList = dpNames("*", "VCRYO_TT_SUM");
	int processed = 0, total = dynlen(dpList);
	for(int n = dynlen(dpList) ; n > 0 ; n--)
	{
		//ProgressLabelSet( progressBarLabel, processed++, total );
		string sumDpName = dpSubStr(dpList[n], DPSUB_DP);
		// Find all CM and BS temperature DPS
		dyn_string cmDps, bsInDps, bsOutDps;
		for(int n = 1 ; n <= 4 ; n++)
		{
			string dpName = "";
			LhcVacGetAttribute(sumDpName, "CM_Thermometer" + n, dpName);
			if(strlen(dpName) > 0)
			{
				if(!dpExists(dpName))
				{
					fwException_raise(exceptionInfo, "ERROR",
						"LhcVacInitCryoSumDp(): CM thermometer DP <" + dpName +
						"> for <" + sumDpName + "> does not exist", "");
				}
				else
				{
					dynAppend(cmDps, dpName);
				}
			}
		}
		for(int n = 1 ; n <= 5 ; n++)
		{
			string dpName = "";
			LhcVacGetAttribute(sumDpName, "BS_IN_Thermometer" + n, dpName);
			if(strlen(dpName) > 0)
			{
				if(!dpExists(dpName))
				{
					fwException_raise(exceptionInfo, "ERROR",
						"LhcVacInitCryoSumDp(): BS IN thermometer DP <" + dpName +
						"> for <" + sumDpName + "> does not exist", "");
				}
				else
				{
					dynAppend(bsInDps, dpName);
				}
			}
			dpName = "";
			LhcVacGetAttribute(sumDpName, "BS_OUT_Thermometer" + n, dpName);
			if(strlen(dpName) > 0)
			{
				if(!dpExists(dpName))
				{
					fwException_raise(exceptionInfo, "ERROR",
						"LhcVacInitCryoSumDp(): BS OUT thermometer DP <" + dpName +
						"> for <" + sumDpName + "> does not exist", "");
				}
				else
				{
					dynAppend(bsOutDps, dpName);
				}
			}
			dpName = "";
			LhcVacGetAttribute(sumDpName, "BS_OUT_Thermometer" + n + "_2", dpName);
			if(strlen(dpName) > 0)
			{
				if(!dpExists(dpName))
				{
					fwException_raise(exceptionInfo, "ERROR",
						"LhcVacInitCryoSumDp(): BS OUT 2 thermometer DP <" + dpName +
						"> for <" + sumDpName + "> does not exist", "");
				}
				else
				{
					dynAppend(bsOutDps, dpName);
				}
			}
		}
		// Build and set DP functions
		LhcVacSetCryoSumDpFunc(sumDpName + ".CM", cmDps, exceptionInfo);
		LhcVacSetCryoSumDpFunc(sumDpName + ".BSIN", bsInDps, exceptionInfo);
		LhcVacSetCryoSumDpFunc(sumDpName + ".BSOUT", bsOutDps, exceptionInfo);
	}
}


//LhcVacSetCryoSumDpFunc
/** Set DP functions for one part (CM or BS) of VCRYO_TT_SUM DP

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, name of VCRYO_TT_SUM DP to be configured PLUS part
									to be configured (CM or BS). For example, C1109_11R3.CM.
	@param dpList:	in, list of source DP names for this part, can be empty
	@param exceptionInfo: out, details of any exceptions are returned here

	@return none
	@author L.Kopylov
*/
bool LhcVacSetCryoSumDpFunc(string dpName, dyn_string dpList, dyn_string &exceptionInfo)
{
	if(dynlen(dpList) > 0)
	{
		dyn_string paramsRR, paramsT;
		string functionRR, functionT;
		int parNum = 1;
		for(int n = dynlen(dpList) ; n > 0 ; n--)
		{
			dynAppend(paramsRR, dpList[n] + ".RR1:_online.._value");
			dynAppend(paramsT, dpList[n] + ".T:_online.._value");
			if(parNum > 1)
			{
				functionRR += "|p" + parNum;
				functionT += ",p" + parNum;
			}
			else
			{
				functionRR = "p1";
				functionT = "dynMax(makeDynFloat(p1";
			}
			parNum++;
		}
		functionT += "))";
		dpSetWait(
			dpName + ".RR1:_dp_fct.._type", DPCONFIG_DP_FUNCTION,
			dpName + ".T:_dp_fct.._type", DPCONFIG_DP_FUNCTION);
		dpSetWait(
			dpName + ".RR1:_dp_fct.._param", paramsRR,
			dpName + ".RR1:_dp_fct.._fct", functionRR,
			dpName + ".T:_dp_fct.._param", paramsT,
			dpName + ".T:_dp_fct.._fct", functionT);
		dyn_errClass err = getLastError(); 
		if(dynlen(err) > 0)
		{
			DebugTN("dpSetWait() failed for DP function of <" +dpName + ">");
			DebugN("PARAMS RR: ", paramsRR);
			DebugN("FUNCTION RR: ", functionRR);
			DebugN("PARAMS T: ", paramsT);
			DebugN("FUNCTION T: ", functionT);
			throwError(err);
			fwException_raise(exceptionInfo, "ERROR",
				"LhcVacSetCryoSumDpFunc(): Could not configure DP function for <" + dpName +
				">", "");
			return false;
		}
	}
	else
	{
		dpSetWait(
			dpName + ".RR1:_dp_fct.._type", DPCONFIG_NONE,
			dpName + ".T:_dp_fct.._type", DPCONFIG_NONE);
		dyn_errClass err = getLastError(); 
		if(dynlen(err) > 0)
		{
			DebugTN("dpSetWait() failed for NO DP function of <" +dpName + ">");
			throwError(err);
			fwException_raise(exceptionInfo, "ERROR",
				"LhcVacSetCryoSumDpFunc(): Could not clear DP function for <" + dpName +
				">", "");
			return false;
		}
	}
	return true;
}

//LhcVacInitVvsPsbSumDp
/** Set DP functions for all VVS_PSB_SUM DPs in system
  Set also DP functions for all real valves (VVS_PSB_SR) of these DPs

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param progressBarLabel: in, shape of progress bar label
	@param exceptionInfo: out, details of any exceptions are returned here

	@return none
	@author L.Kopylov
	@modify ASA - 21sep11
*/
void LhcVacInitVvsPsbSumDp( shape progressBarLabel, dyn_string &exceptionInfo )
{
	dyn_errClass err;
	dyn_string dpList = dpNames("*", "VVS_PSB_SUM");
	int processed = 0, total = dynlen(dpList);

	for(int dpIdx = dynlen(dpList) ; dpIdx > 0 ; dpIdx--)
	{
		//ProgressLabelSet( progressBarLabel, processed++, total );
		string sumDpName = dpSubStr(dpList[dpIdx], DPSUB_DP);
		// Find all real valves
		dyn_string vvsDps;
		for(int n = 1 ; n <= 4 ; n++)
		{
			string dpName = "";
			LhcVacGetAttribute(sumDpName, "RealValve" + n, dpName);
			if(strlen(dpName) > 0)
			{
				if(!dpExists(dpName))
				{
					fwException_raise(exceptionInfo, "ERROR",
						"LhcVacInitVvsPsbSumDp(): Real Valve # " + n + " DP <" + dpName +
						"> for <" + sumDpName + "> does not exist", "");
				}
				else
				{
					if(dynlen(vvsDps) != (n - 1))
					{
						fwException_raise(exceptionInfo, "ERROR",
							"LhcVacInitVvsPsbSumDp(): Real Valve # " + n + " for <" + sumDpName +
							"> is not in order", "");
					}
					dynAppend(vvsDps, dpName);
				}
			}
		}
		// There must be at least 2 real valves
		string	functionRR1 = "", functionRR2;
		switch(dynlen(vvsDps))
		{
		case 2:
			functionRR1 = "(p1|p2) | " +
                    "(((p1&p2) & 0x40000000u) == 0 ? 0x00030000u : " +
                    "((((p1^p2) & 0x00030000u) == 0 ? 0 : 0x00030000u))";
			functionRR1 = "(p1|p2) & (0xBFFCFFFFu | (p1&p2&0xFFFCFFFFu) |" +
				"((((p1^p2) & 0x00030000u) == 0 ? (p1|p2) : 0x0) & 0x00030000u))";
			functionRR2 = "p1|p2";
			break;
		case 4:
			functionRR1 = "(p1|p2|p3|p4) | " +
                    "(((p1&p2&p3&p4) & 0x40000000u) == 0 ? 0x00030000u : " +
                    "(((((p1^p2)|(p1^p3)|(p1^p4)|(p2^p4)|(p2^p3))|(p3^p4)) & 0x00030000u) == 0 ? 0 : 0x00030000u))";
			functionRR2 = "p1|p2|p3|p4";
			break;
		default:
			fwException_raise(exceptionInfo, "ERROR",
				"LhcVacInitVvsPsbSumDp(): number real valves is (" + dynlen(vvsDps) +
				") for <" + sumDpName + ">, expected 2 or 4", "");
			break;
		}
		if(functionRR1 == "")
		{
			continue;
		}
		// Build and set DP functions
		dyn_string paramsRR1, paramsRR2;
		dynClear(paramsRR1);
		dynClear(paramsRR2);
		for(int n = dynlen(vvsDps) ; n > 0 ; n--)
		{
			// Set DP function for WR1 of real valve
			dpSetWait(
				vvsDps[n] + ".WR1:_dp_fct.._type", DPCONFIG_DP_FUNCTION);
			dpSetWait(
				vvsDps[n] + ".WR1:_dp_fct.._param", makeDynString(sumDpName + ".WR1:_online.._value"),
				vvsDps[n] + ".WR1:_dp_fct.._fct", "p1");
			err = getLastError(); 
			if(dynlen(err) > 0)
			{
				DebugTN("dpSetWait() failed for DP function of WR1 of <" + vvsDps[n] + ">");
				throwError(err);
				fwException_raise(exceptionInfo, "ERROR",
					"LhcVacInitVvsPsbSumDp(): Could not configure DP function for WR1 of <" + vvsDps[n] +
					">", "");
				return;
			}
			// Prepare functions for summary valve
			dynAppend(paramsRR1, vvsDps[n] + ".RR1:_online.._value");
			dynAppend(paramsRR2, vvsDps[n] + ".RR2:_online.._value");
		}
		// Setup function for summary valve
		dpSetWait(
			sumDpName + ".RR1:_dp_fct.._type", DPCONFIG_DP_FUNCTION,
			sumDpName + ".RR2:_dp_fct.._type", DPCONFIG_DP_FUNCTION);
		dpSetWait(
			sumDpName + ".RR1:_dp_fct.._param", paramsRR1,
			sumDpName + ".RR1:_dp_fct.._fct", functionRR1,
			sumDpName + ".RR2:_dp_fct.._param", paramsRR2,
			sumDpName + ".RR2:_dp_fct.._fct", functionRR2);
		err = getLastError(); 
		if(dynlen(err) > 0)
		{
			DebugTN("dpSetWait() failed for DP function of <" + sumDpName + ">");
			throwError(err);
			fwException_raise(exceptionInfo, "ERROR",
				"LhcVacInitVvsPsbSumDp(): Could not configure DP function for <" + sumDpName +
				">", "");
			return;
		}
	}
	return;
}

//LhcVacInitVbeamPathDp
/** Set DP functions for all VBEAM_PATH DPs in system

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param progressBarLabel: in, shape of progress bar label
	@param exceptionInfo: out, details of any exceptions are returned here

	@return none
	@author L.Kopylov
	@modify ASA - 21sep11
*/
void LhcVacInitVbeamPathDp( shape progressBarLabel, dyn_string &exceptionInfo )
{
  dyn_errClass err;
  dyn_string dpList = dpNames("*", "VBEAM_PATH");
  int processed = 0, total = dynlen(dpList);

  for(int dpIdx = dynlen(dpList) ; dpIdx > 0 ; dpIdx--)
  {
    //ProgressLabelSet( progressBarLabel, processed++, total );
    string sumDpName = dpSubStr(dpList[dpIdx], DPSUB_DP);

    // Find all real valves and PLC DPs for all real valves
    dyn_string vvsDps, plcDps;
    for(int n = 1 ; n <= 12 ; n++)
    {
      string dpName = "";
      LhcVacGetAttribute(sumDpName, "SourceValve" + n, dpName);
      if(strlen(dpName) > 0)
      {
        if(!dpExists(dpName))
        {
          fwException_raise(exceptionInfo, "ERROR",
            "ConfigureVbeamPath(): Source Valve # " + n + " DP <" + dpName +
            "> for <" + sumDpName + "> does not exist", "");
        }
        else
        {
          dynAppend(vvsDps, dpName);
          string plcName = "", alarmDp;
	  LhcVacDevicePlc(dpName, plcName, alarmDp, exceptionInfo);
          if(strlen(plcName) == 0)
          {
            continue;
          }
          string plcDp = "VPLC_A_" + plcName + ".AL1";
          if(!dpExists(plcDp))
          {
            plcDp = "VPLC_A0_" + plcName + ".AL1";
            if(!dpExists(plcDp))
            {
              continue;
            }
          }
          if(dynContains(plcDps, plcDp) <= 0)
          {
            dynAppend(plcDps, plcDp);
          }
        }
      }
    }

    // There must be at least 1 source valve
    if(dynlen(vvsDps) == 0)
    {
      fwException_raise(exceptionInfo, "ERROR",
        "ConfigureVbeamPath(): No Source Valves for <" + sumDpName + ">", "");
      continue;
    }
    // Building functions and arguments - function for AL1 of PLC
    int argNbr = 1;
    string functionPlc = "";
    dyn_string params;
    dynClear(params);
    for(int n = dynlen(plcDps) ; n > 0 ; n--)
    {
      dynAppend(params, plcDps[n] + ":_online.._value");
      if(functionPlc != "")
      {
        functionPlc += "|";
      }
      functionPlc += "p" + argNbr;
      argNbr++;
    }

    // Building functions and arguments - function for RR1 of valves
    string functionAND = "";
    string functionOR = "";
    for(int n = dynlen(vvsDps) ; n > 0 ; n--)
    {
      dynAppend(params, vvsDps[n] + ".RR1:_online.._value");
      if(functionAND != "")
      {
        functionAND += "&";
      }
      functionAND += "p" + argNbr;
      if(functionOR != "")
      {
        functionOR += "|";
      }
      functionOR += "p" + argNbr;
      argNbr++;
    }

    // Building resulting function
    string function = (functionPlc == "" ? "" : "(" + functionPlc + ") | ") +
      "(((" + functionAND + ")&0x40020000u)!=0x40020000u) | " +
      "(((" + functionOR + ")&0x00010000u)!=0)";

    // Setting DP function and arguments
    dpSetWait(
      sumDpName + ".AL1:_dp_fct.._type", DPCONFIG_DP_FUNCTION);
    dpSetWait(
      sumDpName + ".AL1:_dp_fct.._param", params,
      sumDpName + ".AL1:_dp_fct.._fct", function);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      DebugTN("dpSetWait() failed for DP function of <" + sumDpName + ">");
      throwError(err);
      fwException_raise(exceptionInfo, "ERROR",
        "ConfigureVbeamPath(): Could not configure DP function for <" + sumDpName +
        ">", "");
      return;
    }
  }
}

// -----------------------------------------------------
//  Transfer errors into import error dialogue section
// -----------------------------------------------------
//TransferImportErrors:
/**
	Transfer errors from Import dialogue into error dialogue for display its. 

	@param importErrors: in, array contained errors 
	@param put_get: in,
		if FLAG_PUT then put data here 
		if FLAG_GET then get data from here

	@return none
	@author ASA
*/
global dyn_string importErrorsL;
void TransferImportErrors( dyn_string &importErrors, int put_get )
{
	// GET
	if( put_get == FLAG_GET )
	{
		importErrors = importErrorsL;
		return;
	}
	// PUT
	dynClear( importErrorsL );	// clear library
	importErrorsL = importErrors;
	return;
}

// ------------------------------------
//  Get devices from PVSS DB section
// ------------------------------------
//GetDpPVSS:
/**
	Get 2 arrays contained datapoints and corresponded datapoint types

	@param devicesExisted: in, array contained existed devices
	@param typesOfDevicesExisted: in, array contained device types
	@param put_get: in,
		if put_get == FLAG_PUT then put data here
		if put_get == FLAG_GET then get data from here

	@return number of devices in devicesExisted array
	@author ASA
        
        modify @SAA 7june2010
        delete from list LHC_VAC_SECT_VPI_DP_TYPE dp types
        so that this type added into LhcVacGetAllEqpDpTypes Func
        
*/
//
global dyn_string 	devicesExistedL;
global dyn_string	typesOfDevicesExistedL;
global int		numberOfDevicesExistedL;
//
int GetDpPVSS( dyn_string &devicesExisted, dyn_string &typesOfDevicesExisted, int put_get )
{
	dyn_string	allDpTypes;
	int		n, len;

	// GET
	if( put_get == FLAG_GET ) // get prepared devices in this case
	{
		devicesExisted = devicesExistedL;
		typesOfDevicesExisted = typesOfDevicesExistedL;
		return( numberOfDevicesExistedL );	
	}
	// PUT
	dynClear( devicesExisted );
	dynClear( typesOfDevicesExisted );
	allDpTypes = LhcVacGetAllEqpDpTypes( );
	n = dynlen( allDpTypes );
        len = dynUnique( allDpTypes );
	if( n != len ) DebugN( "GetDpPVSS: INTERNAL WARNING  LhcVacGetAllEqpDpTypes return redundant elements!!!" );

        //DebugN( "vclImportEquipment.ctl:GetDpPVSS: dpTypes nums", len );
        //DebugN( "vclImportEquipment.ctl:GetDpPVSS: dpTypes", allDpTypes );
	
        // Patch from 7jun2010 remove LHC_VAC_SECT_VPI_DP_TYPE dp type
        n = dynContains( allDpTypes, LHC_VAC_SECT_VPI_DP_TYPE );
        if( n == -1 ) DebugN( "GetDpPVSS: dynContains INTERNAL ERROR" );
        if( n != 0 )
        {
          n = dynRemove( allDpTypes, n );
          if( n == -1 ) DebugN( "GetDpPVSS: dynRemove INTERNAL ERROR" );
        }
	// PatchEnd
	
	len = dynlen( allDpTypes );
        //DebugN( "GetDpPVSS: dpTypes, dpTypes numbers", allDpTypes, len );        
	for( n = 1; n <= len; n++ )
	{
		GetDevicesExistedByPattern( devicesExisted, typesOfDevicesExisted, allDpTypes[n] );
	}
	dynClear( allDpTypes );
	dynClear( devicesExistedL );		// clear library
	dynClear( typesOfDevicesExistedL );	// clear library
	devicesExistedL = devicesExisted;
	typesOfDevicesExistedL = typesOfDevicesExisted;
	numberOfDevicesExistedL = dynlen( devicesExisted );	
 	return( numberOfDevicesExistedL );
}
//GetDevicesExistedByPattern:
/**
	Filled 2 arrays contained datapoints and corresponded datapoint types
	(remember! clear arrays before first call by cqalling dynClear function)

	@param devicesExisted: in/out, array contained existed devices
	@param typesOfDevicesExisted: in/out, array contained device types
	@param dpType: in, device type

	@return none
	@author ASA
*/
void GetDevicesExistedByPattern(
	dyn_string &devicesExisted, dyn_string &typesOfDevicesExisted, string dpType )
{
	int         n, nDevices;
	dyn_string  devices;
	
	devices = dpNames( "*" , dpType );
 	nDevices = dynlen( devices ); 
 	for( n = 1; n <= nDevices; n++ )
 	{
		dynAppend( devicesExisted, dpSubStr( devices[n], DPSUB_DP ) );
		dynAppend( typesOfDevicesExisted, dpType );
 	}
}

/*
** FUNCTION CheckTransTypeSetDataType
**	check correct transaction
** function transfered into lhcVacAddressConfig.ctl
*
int CheckTransTypeSetDataType( string addressType, string transType, int &ret )
{
	int	result;
	ret = OK;
	switch( addressType )
	{
	case fwPeriphAddress_TYPE_OPCCLIENT:
		switch( transType )
		{
		case "boolean":
			result = 486;
			break;
		case "integer16":
			result = 481;
			break;
		case "integer32":
	 		result = 482;
			break;
		case "float32":
			result = 484;
			break;
		case "float64":
			result = 485;
			break;
		case "string":
			result = 487;
			break;
		case "uchar":
			result = 483;
			break;
		case "uinteger16":
			result = 488;
			break;
		case "uinteger32":
			result = 489;
			break;
		default:
			ret = FAILURE;
			AddErrorMessageSeverity( "ERROR: Wrong TransType <" + transType + ">", ERR_ERROR );
		}
		break;
	case fwPeriphAddress_TYPE_S7:
		switch( transType )
		{
		case "boolean":
			result = 706;
			break;
		case "integer16":
			result = 701;
			break;
		case "integer32":
	 		result = 702;
			break;
		case "float32":
			result = 705;
			break;
		case "float64":
			result = 705;
			break;
		case "string":
			result = 707;
			break;
		case "uchar":
			result = 704;
			break;
		case "uinteger16":
			result = 703;
			break;
		case "uinteger32":
			result = 708;
			break;
		default:
			ret = FAILURE;
			AddErrorMessageSeverity( "ERROR: Wrong TransType <" + transType + ">", ERR_ERROR );
		}
		break;
	default:
		ret = FAILURE;
		AddErrorMessageSeverity( "ERROR: Unsupported address type <" + addressType + ">", ERR_ERROR );
	}
	return result;
}
*/
/*
** Check address config consistency for one DPE
*/
void CheckAddressConsistency( string dpeName, dyn_anytype &addressConfig, string opcGroup, int &ret )
{
	ret = OK;
	addressConfig[FW_PARAMETER_FIELD_NUMBER] = FW_PARAMETER_FIELD_NUMBER;
	switch( addressConfig[FW_PARAMETER_FIELD_COMMUNICATION] )
	{
	case fwPeriphAddress_TYPE_OPCCLIENT:
		CheckAddressConsistencyOPC( dpeName, addressConfig, opcGroup, ret );
		break;
	case fwPeriphAddress_TYPE_S7:
		CheckAddressConsistencyS7( dpeName, addressConfig, ret );
		break;
	default:
		AddErrorMessageSeverity(
			"ERROR: CheckAddressConsistency(" + dpeName + "): unsupported address type <" +
			addressConfig[FW_PARAMETER_FIELD_COMMUNICATION], ERR_ERROR );
		ret = FAILURE;
		break;
	}
	if( ret == FAILURE )
		AddErrorMessage( "   File line number: " + fileLinesG );
}
/*
** Check address config consistency for one DPE - fwPeriphAddress_TYPE_OPCCLIENT address mode
*/
void CheckAddressConsistencyOPC( string dpeName, dyn_anytype addressConfig, string opcGroup, int &ret )
{
	// Not implemented yet LIK 041.10.2005
}
/*
** Check address config consistency for one DPE - fwPeriphAddress_TYPE_S7 address mode
*/
void CheckAddressConsistencyS7( string dpeName, dyn_anytype addressConfig, int &ret )
{
	// Not implemented yet LIK 041.10.2005
}

//PrepareSortIndexForImportTable:
//
//	Prepare indexes for table sort:
//	Depends from flags:
//		NONE
//		CREATE
//		DELETE
//		ADDRESS
//		ARCHIVE
//		VALUEFORMAT
//		PROBLEM
//	AND device types (dpTypes)
//
// PrepareSortIndexForImportTable old used function
//	Now used 2 function for set indexes into table:
// PrepareSortIndexForImportTableByDpType
// PrepareSortIndexForImportTableByModify
/*
int PrepareSortIndexForImportTable( dyn_bool modifyFlag, string dpType )
{
	int sort, add, beSortedAs;


	// sort order: changed 20dec07 by SAA
	// PROBLEM
	// DELETE
	// CREATE
	// MODIFY
	if( modifyFlag[PROBLEM] ) sort = -100000000;    // (-100) 10**6 for problem devices
	else if( modifyFlag[DELETE] ) sort = -10000000; // (-10)  10**6 for devices be deleted
	else if( modifyFlag[CREATE] ) sort = -1000000;  // (-1)   10**6 for devices be created
	else sort = 1000000;                            // (+1)   10**6 for modify and other devices   

	add = LhcVacDpTypeGroupMask( dpType );
	beSortedAs = sort + add;

	return beSortedAs;
}
*/
unsigned PrepareSortIndexForImportTableByDpType( string dpType )
{
// For sorting 
//		unsigned LhcVacDpTypeGroupMask( string dpType )
// function 
// from file
//		lhcVacEqpConfig.ctl
// be used.
	unsigned beSortedAs;
	//beSortedAs = LhcVacDpTypeGroupMask( dpType ); //BEFORE_QT
	beSortedAs = LhcVacDpTypeGroup( dpType ); // QT
	return beSortedAs;
}
int PrepareSortIndexForImportTableByModify( dyn_bool modifyFlag )
{
	// sort order: changed 4apr08 by SAA
	// PROBLEM
	// DELETE
	// CREATE
	// MODIFY
	int beSortedAs;
	if( modifyFlag[PROBLEM] ) beSortedAs = -100000000;    // (-100) 10**6 for problem devices
	else if( modifyFlag[DELETE] ) beSortedAs = -10000000; // (-10)  10**6 for devices be deleted
	else if( modifyFlag[CREATE] ) beSortedAs = -1000000;  // (-1)   10**6 for devices be created
	else beSortedAs = 1000000;                            // (+1)   10**6 for modify and other devices   
	return beSortedAs;
}

/**
	@brief Return the dpe name, no dpe name in case of alive counter
	@param[in]	dpType	dp type
	@param[in]	deviceName device name
	@param[in]	partDpeName part of dpe name which be added to device name after point (.)
*/
string UseDpeName( string dpType, string deviceName, string partDpeName )
{
	switch( dpType )
	{
	case "VPLC":
   case "V_FrontEnd_aliveCounter":  
		return( deviceName + "." );
	default:
		return( deviceName + "." + partDpeName );
	}
}
//QuestionBox:
/**
	Call Question dialogue to ask operator about any action

	@param message: in, question message displayed in dialogue
	@param okCancel: out,
		TRUE then OK button was pressed
		FALSE then CANCEL button was pressed

	@return none

	@author ASA
*/
void QuestionBox( string message, bool &okCancel )
{
	dyn_string 	msg;
	dyn_float   df;
	dyn_string  ds;

	msg = makeDynString( message, "YES", "NO" );
	ChildPanelOnCentralModalReturn( "vision/MessageInfo", "Question", msg, df, ds );
	if( ds[1] == "true" ) okCancel = true;
	else okCancel = false;
}

