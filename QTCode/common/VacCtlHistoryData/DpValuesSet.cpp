//	Implementation of DpValuesSet class
/////////////////////////////////////////////////////////////////////////////////

#include "DpValuesSet.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

DpValuesSet::DpValuesSet(const char *dpName) : name(dpName)
{
	pDpes = new QList<DpeValuesSet *>();
}

DpValuesSet::~DpValuesSet()
{
	while(!pDpes->isEmpty())
	{
		delete pDpes->takeFirst();
	}
	delete pDpes;
}

/*
**	FUNCTION
**		Add value at given timestamp to DPE data
**
**	ARGUMENTS
**		dpeName	- Name of DPE
**		ts		- Timestamp
**		value	- Value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpValuesSet::append(const char *dpeName, const QDateTime &ts, const QVariant &value)
{
	DpeValuesSet *pDpe = findDpeData(dpeName);
	if(!pDpe)
	{
		pDpe = new DpeValuesSet(dpeName);
		pDpes->append(pDpe);
	}
	pDpe->append(ts, value);
}

/*
**	FUNCTION
**		All data have been added - sort all DPE's data for further fast search
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpValuesSet::sort(void)
{
	for(int idx = 0 ; idx < pDpes->count() ; idx++)
	{
		pDpes->at(idx)->sort();
	}
}

/*
**	FUNCTION
**		Find data for DPE with given name
**
**	ARGUMENTS
**		dpeName	- Name of DPE
**
**	RETURNS
**		Pointer to data for given DPE; or
**		NULL if data for such DPE are not found
**
**	CAUTIONS
**		None
*/
DpeValuesSet *DpValuesSet::findDpeData(const char *dpeName)
{
	for(int idx = 0 ; idx < pDpes->count() ; idx++)
	{
		DpeValuesSet *pDpe = pDpes->at(idx);
		if(pDpe->getName() == dpeName)
		{
			return pDpe;
		}
	}
	return NULL;
}

