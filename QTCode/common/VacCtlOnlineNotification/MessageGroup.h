#ifndef	MESSAGEGROUP_H
#define	MESSAGEGROUP_H

#include "VacCtlOnlineNotificationExport.h"

#include "Sector.h"
#include "MainPart.h"

// Class collecting messages of one group before decsion is taken on how
// messages shall be sent: one by one, or single message for the whole group

#include <qstring.h>
#include <qstringlist.h>
#include <qdatetime.h>

class VACCTLONLINENOTIFICATION_EXPORT MessageGroup
{
public:
	MessageGroup(void *id, const QString cfgName, int lifeTime);
	~MessageGroup() {}

	int addMessage(QString &message);
	bool isTimeExpired(const QDateTime &now);

	// Access
	inline void *getGroupId(void) const { return groupId; }
	inline const QString &getConfigName(void) const { return configName; }
	inline const QDateTime &getCreateTime(void) const { return createTime; }
	inline QStringList &getMessages(void) { return messages; }
	inline Sector *getSector(void) { return pSector; }
	inline void setSector(Sector *pNewSector) { pSector = pNewSector; }
	inline MainPart *getMainPart(void) { return pMainPart; }
	inline void setMainPart(MainPart *pNewMainPart) { pMainPart = pNewMainPart; }

protected:
	// Group identifier - pointer to 'something'
	void	*groupId;

	// Name of configuration that owns the group
	QString	configName;

	// List of messages
	QStringList	messages;

	// Time when group was created
	QDateTime	createTime;

	// Life time of group [sec]
	int lifeTime;

	// Sector of first device in group
	Sector		*pSector;

	// Main part of first device in group
	MainPart	*pMainPart;
};

#endif	// MESSAGEGROUP_H
