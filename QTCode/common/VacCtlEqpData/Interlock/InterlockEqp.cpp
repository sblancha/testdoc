#include "InterlockEqp.h"

InterlockEqp::InterlockEqp(QString name)
{
	this->name = name;
	setTypeFromName();
}

InterlockEqp::~InterlockEqp()
{

}

void InterlockEqp::setTypeFromName()
{
	if (name.startsWith("VRVCL"))
	{
		type = VRVCL;
	}
	else if (name.startsWith("VRVCX"))
	{
		type = VRVCX;
	}
	else if (name.startsWith("VRIVC"))
	{
		type = VRIVC;
	}
	else if (name.startsWith("VRIVD"))
	{
		type = VRIVD;
	}
	else if (name.startsWith("VRPI"))
	{
		type = VRPI;
	}
	else if (name.startsWith("VRGPT"))
	{
		type = VRGPT;
	}
	else
		type = UNKNOWN;
}