//	Implementation of BakeoutPlotImage class
/////////////////////////////////////////////////////////////////////////////////

#include "BakeoutPlotImage.h"

#include "VacMainView.h"

BakeoutPlotImage::BakeoutPlotImage(QWidget *parent) :
	QWidget(parent)
{
	if(VacMainView::getInstance())
	{
		setFont(VacMainView::getInstance()->font());
	}
	setMouseTracking(true);
	setAutoFillBackground(true);
}

BakeoutPlotImage::~BakeoutPlotImage()
{
}

void BakeoutPlotImage::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);

	emit resized();
}

void BakeoutPlotImage::mouseMoveEvent(QMouseEvent *pEvent)
{
	emit pointerMoved(pEvent);
}

void BakeoutPlotImage::leaveEvent(QEvent *pEvent)
{
	QWidget::leaveEvent(pEvent);
	emit mouseLeave();
}

