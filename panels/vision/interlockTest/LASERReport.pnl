V 11
1
LANG:1 26 ValveInterlockTest:AlarmDb
PANEL,-1 -1 758 440 N "_3DFace" 0
"main()
{
  UpdateTable();
}" 0
 E E E E 1 -1 -1 0  20 10
""0  1
E "
const time farInThePast = makeTime(2000, 01, 01);

// Name of DB connection we use
string connectName = \"LaserLogDB\";


void UpdateTable()
{
  // Fill in table
  int nValves = dynlen(valveDps);
  for(int valveIdx = 1 ; valveIdx <= nValves ; valveIdx++)
  {
    int nAlarms = dynlen(laserPvssTimes[valveIdx]);
    for(int alarmIdx = 1 ; alarmIdx <= nAlarms ; alarmIdx++)
    {
      string valveName = alarmIdx == 1 ? IntlTestGetEqpName(valveDps[valveIdx]) : \"\";
      string timePvss = formatTime(\"%d-%m-%Y %H:%M:%S\", laserPvssTimes[valveIdx][alarmIdx], \".%03d\");
      string timeLaser;
      if(laserLogTimes[valveIdx][alarmIdx] > farInThePast)
      {
        timeLaser = formatTime(\"%d-%m-%Y %H:%M:%S\", laserLogTimes[valveIdx][alarmIdx], \".%03d\");
      }
      else
      {
        timeLaser = \"\";
      }
      LaserTable.appendLine(\"valve\", valveName,
                            \"device\", IntlTestGetEqpName(laserEqpNames[valveIdx][alarmIdx]),
                            \"state\", laserStateNames[valveIdx][alarmIdx],
                            \"timePVSS\", timePvss,
                            \"timeLASER\", timeLaser);
      UpdateTableRow(valveIdx, alarmIdx, LaserTable.lineCount - 1);
    }
  }
}

void UpdateTableRow(int valveIdx, int alarmIdx, int row)
{
  string timeLaser;
  if(laserLogTimes[valveIdx][alarmIdx] > farInThePast)
  {
    timeLaser = formatTime(\"%d-%m-%Y %H:%M:%S\", laserLogTimes[valveIdx][alarmIdx], \".%03d\");
  }
  else
  {
    timeLaser = \"\";
  }
  LaserTable.cellValueRC(row, \"timeLASER\", timeLaser);
  string color;
  if(timeLaser == \"\")
  {
    color = \"{255, 127, 127}\";
  }
  else if(laserLogTimes[valveIdx][alarmIdx] < laserPvssTimes[valveIdx][alarmIdx])
  {
    color = \"{255,255,193}\";
  }
  else
  {
    color = \"{193,255,193}\";
  }
  LaserTable.cellBackColRC(row, \"timeLASER\", color);
  string diffString;
  if(timeLaser != \"\")
  {
    float timeDiff;
    if(laserLogTimes[valveIdx][alarmIdx] < laserPvssTimes[valveIdx][alarmIdx])
    {
      timeDiff = -((float)(laserPvssTimes[valveIdx][alarmIdx] - laserLogTimes[valveIdx][alarmIdx]));
    }
    else
    {
      timeDiff = laserLogTimes[valveIdx][alarmIdx] - laserPvssTimes[valveIdx][alarmIdx];
    }
    diffString = timeDiff;
  }
  LaserTable.cellValueRC(row, \"diff\", diffString); 
}

void QueryData(dyn_string &exceptionInfo)
{
  time startTs = farInThePast,
       endTs = farInThePast;
  dyn_string alarmIds;
  CollectAlarmIds(startTs, endTs, alarmIds);
  if(dynlen(alarmIds) == 0)
  {
    return;
  }
  ConnectToDb(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    ExecStatusText.text = \"Connect to DB failed\";
    return;
  }
  QueryAllData(startTs, endTs, alarmIds, exceptionInfo);
  if(dynlen(exceptionInfo) == 0)
  {
    ExecStatusText.text = \"FINISHED\";
  }
}

void CollectAlarmIds(time &startTs, time &endTs, dyn_string &alarmIds)
{
  ExecStatusText.text = \"Analyzing parameters for query...\";
  int nValves = dynlen(valveDps);
  for(int valveIdx = 1 ; valveIdx <= nValves ; valveIdx++)
  {
    int nAlarms = dynlen(laserPvssTimes[valveIdx]);
    for(int alarmIdx = 1 ; alarmIdx <= nAlarms ; alarmIdx++)
    {
      if(laserLogTimes[valveIdx][alarmIdx] > farInThePast)
      {
        continue;
      }
      if(dynContains(alarmIds, laserAlarmIds[valveIdx][alarmIdx]) <= 0)
      {
        dynAppend(alarmIds, laserAlarmIds[valveIdx][alarmIdx]);
      }
      if(startTs == farInThePast)
      {
        startTs = laserPvssTimes[valveIdx][alarmIdx];
        endTs = startTs;
      }
      else
      {
        if(startTs > laserPvssTimes[valveIdx][alarmIdx])
        {
          startTs = laserPvssTimes[valveIdx][alarmIdx];
        }
        if(endTs < laserPvssTimes[valveIdx][alarmIdx])
        {
          endTs = laserPvssTimes[valveIdx][alarmIdx];
        }
      }
    }
  }
  if(dynlen(alarmIds) == 0)
  {
    ExecStatusText.text = \"All alarms are already FOUND!!!\";
  }
  startTs -= 10;
  endTs += 10;
}

void ConnectToDb(dyn_string &exceptionInfo)
{
  ExecStatusText.text = \"Connecting to database...\";

  if (!isFunctionDefined(\"rdbOption\"))
  {
    fwException_raise(exceptionInfo, \"ERROR\", \"ConnectToDb(): CtrlRDBAccess library is not available. Check your installation and config file\", \"\");
    return;
  }


  dbConnection dbCon;
  string errTxt;
  int rc = rdbGetConnection(connectName, dbCon);

  DebugTN(\"rdbGetConnection(): rc = \" + rc);
  if(rc == -1)
  {
    rdbCheckError(errTxt, dbCon);
    fwExceptionRaise(exceptionInfo, \"ERROR\", \"ERROR Getting the connection: \" + errTxt, \"\");
    return;
  }
  else if(rc == 1)
  {
    // connection with such name does not exist yet. Create it
    string connectString = \"Driver=QOCI8;Database=laserdb;User=laser_public;Password=read_laser\";
    rdbOpenConnection(connectString, dbCon, connectName);
    if(rdbCheckError(errTxt, dbCon))
    {
      fwExceptionRaise(exceptionInfo, \"ERROR\", \"ERROR WHILE OPENING DATABASE: \" + errTxt, \"\");
      return;
    }
  }
}

void QueryAllData(time startTs, time endTs, dyn_string &alarmIds, dyn_string &exceptionInfo)
{
  // Query in batches of 10 alarm IDs in each
  dyn_string queryIds;
  int queryNbr = 1;
  while(dynlen(alarmIds) > 0)
  {
    string id = alarmIds[1];
    dynAppend(queryIds, id);
    dynRemove(alarmIds, 1);
    if(dynlen(queryIds) >= 10)
    {
      QueryAlarmData(queryNbr++, startTs, endTs, queryIds, exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        return;
      }
      dynClear(queryIds);
    }
  }
  if(dynlen(queryIds) > 0)
  {
    QueryAlarmData(queryNbr++, startTs, endTs, queryIds, exceptionInfo);
  }
}

void QueryAlarmData(int queryNbr, time startTs, time endTs, dyn_string queryIds, dyn_string &exceptionInfo)
{
  int nIds = dynlen(queryIds);
  ExecStatusText.text = \"Query #\" + queryNbr + \" for \" + nIds + \" Alarm IDs...\";

  string queryText =
    \"select distinct \" +
    \"	alarm_id \" +
    \"	,to_char(source_ts, 'YYYY=MM=DD=HH24=MI=SS=FF3') as ts \" +
    \"from alarm_events \" +
    \"where alarm_id in (\";
  for(int n = 1 ; n <= nIds ; n++)
  {
    queryText += (n == 1 ? \"\" : \",\") + \"'\" + queryIds[n] + \"'\";
  }
  string startTime = formatTime(\"%d-%b-%Y %H:%M:%S\", startTs);
  string endTime = formatTime(\"%d-%b-%Y %H:%M:%S\", endTs);

  queryText += \") \" +
    \"  and source_ts between to_date('\" + startTime + \"','dd-mon-yyyy HH24:mi:ss') \" +
    \"  and to_date('\" + endTime + \"', 'dd-mon-yyyy HH24:mi:ss') \" +
    \"  and active = 'Y' \" +
    \"  order by alarm_id, ts\";
DebugTN(queryText);

  dbCommand cmd;
  string errTxt;
  int rc;

  rdbStartCommand(connectName, queryText, cmd);
  if(rdbCheckError(errTxt, cmd))
  {
    fwException_raise(exceptionInfo, \"ERROR\", \"rdbStartCommand() ERROR: \" + errTxt, \"\");
    return;
  };

  rdbExecuteCommand(cmd);
  if(rdbCheckError(errTxt, cmd))
  {
    fwException_raise(exceptionInfo, \"ERROR\", \"rdbExecuteCommand() ERROR: \" + errTxt, \"\");
    return;
  };

  // extract the data, 5 rows at a time, each record in separate row (row-wise)
  do // loop until there are rows to extract (rc==1)
  {
    dyn_dyn_mixed data;
    rc = rdbGetData(cmd, data, FALSE, 5);
    if(rdbCheckError(errTxt, cmd))
    {
      fwException_raise(exceptionInfo, \"ERROR\", \"rdbGetData() ERROR: \" + errTxt, \"\");
      return;
    };

    ExecStatusText.text = \"Processing query #\" + queryNbr + \" result...\";
    for(int i=1 ; i <= dynlen(data) ; i++) 
    {
      time ts = DecodeOracleTimestamp(data[i][2]);
      DebugTN(\"DecodeOracleTimestamp(\" + data[i][2] + \")\", ts);
      ProcessDataFromDb(data[i][1], ts);
    }

  } while (rc==1);

  rdbFinishCommand(cmd);
  // note that we do not put the second parameter here!
  if(rdbCheckError(errTxt))
  {
    fwException_raise(exceptionInfo, \"ERROR\", \"rdbFinishCommand() ERROR: \" + errTxt, \"\");
    return;
  }
}

time DecodeOracleTimestamp(string oracleTs)
{
  dyn_uint comp = makeDynUInt(0, 0, 0, 0, 0, 0, 0);
  dyn_string splitted = strsplit(oracleTs, \"=\");
  for(int n = dynlen(splitted) ; n > 0 ; n--)
  {
    comp[n] = splitted[n];
  }
  return makeTime(comp[1], comp[2], comp[3], comp[4], comp[5], comp[6], comp[7]);
}

void ProcessDataFromDb(string alarmId, time ts)
{
DebugTN(\"Processing\", alarmId, ts);
  int row = 0;
  int nValves = dynlen(valveDps);
  for(int valveIdx = 1 ; valveIdx <= nValves ; valveIdx++)
  {
    int nAlarms = dynlen(laserPvssTimes[valveIdx]);
    for(int alarmIdx = 1 ; alarmIdx <= nAlarms ; alarmIdx++)
    {
      if(laserAlarmIds[valveIdx][alarmIdx] == alarmId)
      {
DebugTN(\"alarmId FOUND\", alarmId, laserPvssTimes[valveIdx][alarmIdx], ts);
        if(laserPvssTimes[valveIdx][alarmIdx] == ts)
        {
          laserLogTimes[valveIdx][alarmIdx] = ts;
          UpdateTableRow(valveIdx, alarmIdx, row);
        }
        else if(laserPvssTimes[valveIdx][alarmIdx] < ts)
        {
          float timeDiff = ts - laserPvssTimes[valveIdx][alarmIdx];
          if(timeDiff < 3)
          {
            laserLogTimes[valveIdx][alarmIdx] = ts;
            UpdateTableRow(valveIdx, alarmIdx, row);
          }
        }
        else
        {
          float timeDiff = laserPvssTimes[valveIdx][alarmIdx] - ts;
          if(timeDiff < 2)
          {
            laserLogTimes[valveIdx][alarmIdx] = ts;
            UpdateTableRow(valveIdx, alarmIdx, row);
          }
        }
      }
      row++;
    }
  }
/*
global dyn_dyn_string  laserEqpNames;  // DP names of equipments with LASER alarms
global dyn_dyn_string  laserStateNames;  // Textual descritpion of LASER alarm
global dyn_dyn_string  laserAlarmIds;  // Alarm IDs for corresponding alarms
global dyn_dyn_time    laserPvssTimes;  // Timestamps from PVSS
global dyn_dyn_time    laserLogTimes;  // Timestamps from LASER log DB
*/
}
" 0
 2
"CBRef" "1"
"EClose" E
""
DISPLAY_LAYER, 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0
LAYER, 0 
1
LANG:1 0 
25 0
"LaserTable"
""
1 10 10 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
0 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 25 Arial,8,-1,5,50,0,0,0,0,0
0  8 8 752 402
EE 1 0 1 6 0 "valve" 14 1 0 "s" 1
LANG:1 5 Valve
E
1
LANG:1 0 

150 "device" 14 1 0 "s" 1
LANG:1 6 Device
E
1
LANG:1 0 

150 "state" 5 1 0 "s" 1
LANG:1 5 State
E
1
LANG:1 0 

60 "timePVSS" 14 1 0 "s" 1
LANG:1 9 Time PVSS
E
1
LANG:1 0 

150 "timeLASER" 14 1 0 "s" 1
LANG:1 10 Time LASER
E
1
LANG:1 0 

150 "diff" 6 1 0 "s" 1
LANG:1 4 Diff
E
1
LANG:1 0 

60 
14 14 10 10
1
LANG:1 25 Arial,8,-1,5,50,0,0,0,0,0
0 0 1 1 1 7
1 0
13 1
"QueryPb_"
""
1 20 410 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
1 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  18 408 82 432

T 
1
LANG:1 5 Query
"main()
{
  dyn_string exceptionInfo;
  QueryData(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}" 0
 E E E
13 2
"ClosePb"
""
1 690 410 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
2 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  688 408 752 432

T 
1
LANG:1 5 Close
"main()
{
  PanelOff();
}" 0
 E E E
14 3
"ExecStatusText"
""
1 100 410 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
3 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  98 408 682 432
3 "0s" 0 0 0 0 0 -1  E E E
0
LAYER, 1 
1
LANG:1 0 
0
LAYER, 2 
1
LANG:1 0 
0
LAYER, 3 
1
LANG:1 0 
0
LAYER, 4 
1
LANG:1 0 
0
LAYER, 5 
1
LANG:1 0 
0
LAYER, 6 
1
LANG:1 0 
0
LAYER, 7 
1
LANG:1 0 
0
0
