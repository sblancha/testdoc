#ifndef VACICONGEOMETRY_H
#define VACICONGEOMETRY_H

#include "VacCtlEwoUtilExport.h"

// Class holding geometry of icon

class VACCTLEWOUTIL_EXPORT VacIconGeometry
{
public:
	VacIconGeometry(int width, int height, int x, int y, int verticalPos);
	VacIconGeometry(const VacIconGeometry &source);

	// Access
	inline int getWidth(void) { return width; }
	inline void setWidth(int width) { this->width = width; }
	inline int getHeight(void) { return height; }
	inline void setHeight(int height) { this->height = height; }
	inline int getX(void) { return x; }
	inline void setX(int x) { this->x = x; }
	inline int getY(void) { return y; }
	inline void setY(int y) { this->y = y; }
	inline int getVerticalPos(void) { return verticalPos; }
	inline void setVerticalPos(int verticalPos) { this->verticalPos = verticalPos; }

protected:
	// Width of icon rectangle
	int width;

	// Height of icon rectangle
	int height;

	// Offset of icon's upper left corner from icon connection point in X direction
	int	x;

	// Offset of icon's upper left corner from icon connection point in Y direction
	int	y;

	// Vertical position for 'normal' drawing icon:
	//	-1 = icon is drawn below vacuum pipe line
	//	0 = icon is drawn on vacuum pipe line
	// 1 = icon is drawn above vacuum pipe line
	int verticalPos;
};

#endif	// VACICONGEOMETRY_H
