#ifndef FUNCTIONALTYPE_H
#define FUNCTIONALTYPE_H

#include "VacCtlEqpDataExport.h"

//	Equipment is classified by equipment types, controllable
//	equipment is classified by Family+Type+Subtype.
//	However, in many cases another classification is needed,
//	for example, all VGRs shall be considered together does not
//	matter what is Family+Type+SubType.
//
//	This class provides basis for such 'functional' equipment
//	classification. It includes specific 'functional types for
//	both controllable and non-controllable equipment

#include <qstring.h>
#include <qlist.h>
#include <qmutex.h>

// Special class holding wildcards for parsing functionl type
// for passing equipment

class PassiveEqpTypeWc
{
public:
	PassiveEqpTypeWc(const char *wc, int type)
	{
		this->wc = wc;
		this->type = type;
	}

	// Wildcard specification
	QString	wc;

	// Passive equipment type for given wildcard
	int		type;
};

#include "EqpMsgCriteria.h"
class Eqp;

class VACCTLEQPDATA_EXPORT FunctionalType
{
public:
	enum
	{
		None = 0,				// No special interpretation
		ColdDipole = 1,		// Passive equipment - Cold dipole
		ColdQuadrupole = 2,	// Passive equipment - Cold quadrupole
		WarmDipole = 3,		// Passive equipment - Warm dipole
		WarmQuadrupole = 4,	// Passive equipment - Warm quadrupole
		TDICollimator = 5,
		
		VG = 7,				// Active equipment - Vacuum Gauge
		VA = 8,				// Active equipment - Vacuum Alarm
		VPG = 9,			// Active equipment - Pumping Group
		VV = 10,			// Active equipment - Valves
		VGM = 11,			// Active equipment - Membrane gauges
		VGR = 12,			// Active equipment - Pirani gauges
		VGP = 13,			// Active equipment - Penning gauges
		VGI = 14,			// Active equipment - Ion gauges
		VGF = 15,			// Active equipment - Full range gauges
		VPI = 16,			// Active equipment - Ion pumps
		VRPI = 17,			// Active equipment - Ion pump power supply
		VPGF = 18,			// Active equipment - Fixed pumping group
		VPGM = 19,			// Active equipment - Mobile pumping group
		VPGMPR = 20,		// Active equipment - Gauge of mobile pumping group
		VACOK = 21,			// Active equipment - CRYO vacuum alarms
		EXP_AREA = 22,		// Active equipment - Experimental area
		ACCESS_MODE = 23,	// Active equipment - Access mode for main part
		AUX_EQP = 24,		// Active equipment - Auxilliary, not controllable by human
		CRYO_TT = 25,		// Active equipment - Main CRYO thermometer
		CRYO_TT_EXT = 26,	// Active equipment - Extra CRYO thermometer
		VV_ANA = 27,		// Active equipment - Analog valve
		VGTR = 28,			// Active equipment - Pressure transmitter
		BEAM_INT = 29,		// Active equipment - Beam intensity measurement
		CRYO_TT_SUM = 30,	// Active equipment - Beam intensity measurement
		COLDEX = 31,		// Active equipment - COLDEX
		VPT100 = 32,		// Active equipment - PT100 thermometer in LHC
		VRPM = 33,			// Active equipment - Power supply for solenoids
		VBEAM_PARAM_FLOAT = 34,	// Generic - beam parameters of type float
		VV_PLUS_PP = 35,	// Speicifc eqp in LHC: VV + PP, but only VV is controllable.
								// Furthermore, for PLC it is the same device as bypass valve
								// on LHC isolation vacuum (family+type+subtype). Only presentation
								// in PVSS shall be different from 
		VBEAM_PARAM_INT = 36,	// Generic - beam parameters of type integer
		VIES = 37,				// Solenoids
		VP_STDIO = 38,			// Pumping group in TDC2 of SPS
		VBAKEOUT = 39,			// Bakeout rack
		PROCESS_VPG_6A01 = 40,	// Processs device, driven by sequencer - VPG_6A01
		PROCESS_BGI_6B01 = 41,	// Processs device, driven by sequencer - BGI_6B01
		INJ_6B02 = 42,			// Gas injection 6B02
		PROCESS_VPG_6E01 = 43,	// Processs device, driven by sequencer - VPG_6E01
		VRJ_TC = 50,			// Thermocouples remote IO
		VITE = 51,				// Thermocouple temperature measurement
		V8DI_FE = 52,			// Digital Input monitor for interlock signals

		VPN = 53,				// Single NEG pump
		VP_NEG = 54,			// Control of NEG pump chain
		VPNMUX = 55,			// Multiplexor for VP_NEG
		ALARM_DO = 56,			// DO alarm generator
		
		VPS = 60,				// Sublimation pump
		VPS_Process = 61,		// Schedule master for VPS

		VPC_HCCC = 62,			// Cryo pump controller HRS-HCC

		ITL_CMNT = 63,			// CryoMaintain interlock source
		ITL_CMUX = 64,			// CryoMaintain interlock multiplexor

		VRE = 65,				// Bakeout Rack	
		VG_A_RO = 66,			// Active Equipment - Generic Analog Gauge, Read only 
		VOPS_2PS = 67,

		VV_VELO = 100,		// Active equipment - Valve in VELO
		VPT_VELO = 101,		// Active equipment - Turbo pump in VELO
		VPR_VELO = 102,		// Active equipment - Rouphing pump in VELO
		VGA_VELO = 103,		// Active equipment - Absolute gauge in VELO
		VGD_VELO = 104,		// Active equipment - Differential gauge in VELO
		VINT_VELO = 105,	// Active equipment - Interlock in VELO
		VPRO_VELO = 106,	// Active equipment - Process status in VELO
		VOPS_VELO = 107,	// Active equipment - Over pressure switch in VELO
		VELO_PROCESS = 108,	// VELO Main Process

		PLC = 200,			// PLC health control

		VRCG = 300,			// TPG-300 device, non-controllable directly
		TPG_CONFIG = 301,	// TPG configurator in PLC
		VR_GT = 302,		// TPG-300, accessed in a different (new) way
		VR = 303,			// Generic controller (no analog values)
		BAKEOUT = 304,		// Dedicated to Bakeout channels, e.g. regulation channels, alarm/control channels

		MP_ACCESS = 400,	// Main part access control
		SECT_VPI_SUM = 401,	// VRPI summary in sector

		EXT_ALARM = 402,	// External target for alarms

		// Global equipment - not related to sectors etc.
		SMS_MACHINE_MODE = 1000
	};

	static void clear(void);
	static void addType(int type, const char *wildCard);
	static int findPassiveType(const char *eqpName);
	static FunctionalType *findTypeData(int type);

	virtual ~FunctionalType() { delete pCriteria; }

	virtual void fillCriteria(FunctionalType * /* pType */) {}

	EqpMsgCriteria *addCriteria(int funcType, int type, int subType, const char *name);

	virtual void findRelatedEqp(Eqp *pEqp, int otherType, QList<Eqp *> &relatedList);

	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

	// Introduced for processes - to interpret sequencer step as main value
	virtual void getCriteriaValues(int /* critType */, int /* critSubType */, QList<int> &values, QList<QByteArray> &labels)
	{
		values.clear();
		labels.clear();
	}

	// Access
	static QList<FunctionalType *> &getTypes(void);
	inline int getType(void) { return type; }
	inline const char *getName(void) { return name; }
	inline const char *getDescription(void) { return description; }
	inline const QList<EqpMsgCriteria *> &getCriteria(void) const { return *pCriteria; }

protected:
	// List of wildcards for passive equipment names
	static QList<PassiveEqpTypeWc *>	*pWcs;

	// List of known functional types
	static QList<FunctionalType *>		*pTypes;

	//////////////////////////////// To prevent initialization by multiple threads
	static QMutex			mutex;


	static bool nameMatchesWildCard(const char *eqpName, const char *wc);
	static void buildTypes(void);

	FunctionalType(int type, const char *name, const char *description)
	{
		this->type = type;
		this->name = name;
		this->description = description;
		pCriteria = new QList<EqpMsgCriteria *>();
	}

	// Enumeration of functional type - see enum above
	int		type;

	// Short name of functional type
	const char	*name;

	// Long name (description) of functional type
	const char	*description;

	// List of different criteria for SMS notification, supported by this type
	QList<EqpMsgCriteria *>	*pCriteria;

};

#endif	// FUNCTIONALTYPE_H
