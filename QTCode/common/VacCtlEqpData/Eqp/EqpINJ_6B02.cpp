//	Implementation of EqpINJ_6B02 class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpINJ_6B02.h"

#include "DataPool.h"
#include "FunctionalTypeINJ_6B02.h"
#include "EqpMsgCriteria.h"
#include "ResourcePool.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define	COLOR_ERROR				255,102,102
#define	COLOR_WARNING			255,255,0
#define	COLOR_VVG_OPEN			0,255,0
#define	COLOR_VVG_NOT_OPEN		255,255,0
#define	COLOR_VVG_CLOSED		255,0,0

//	Main state bits to be analyzed
#define	RR1_VALID			(0x40000000)
#define RR1_ERROR			(0x00800000)
#define	RR1_WARNING			(0x00400000)
#define	RR1_OFF				(0x01000000)
#define	RR1_ON				(0x02000000)
#define	RR1_VVG_OPEN		(0x00020000)
#define	RR1_VVG_CLOSE		(0x00010000)

#define RR1_ERROR_CODE_MASK	(0x000000FF)
#define RR1_WARNING_CODE_MASK	(0x0000FF00)

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

EqpINJ_6B02::EqpINJ_6B02(const Eqp &source) : Eqp(source)
{
	rr1Online = rr1Replay = 0;
	setPointOnline = setPointReplay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::INJ_6B02;
}

EqpINJ_6B02::~EqpINJ_6B02()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpINJ_6B02::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "RR2");
}

void EqpINJ_6B02::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	addDpeNameToList(valueDpes, "SetPointR_mbar");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpINJ_6B02::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "RR2", mode);
	connectDpe(pDst, "SetPointR_mbar", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpINJ_6B02::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "SetPointR_mbar", mode);
	disconnectDpe(pDst, "RR2", mode);
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "EqpStatus", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpINJ_6B02::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Online == newValue)
			{
				return;
			}
			rr2Online = newValue;
		}
	}
	else if(!strcmp(dpe, "SetPointR_mbar"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(setPointOnline == newValue)
			{
				return;
			}
			setPointOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpINJ_6B02::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Replay == newValue)
			{
				return;
			}
			rr2Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "SetPointR_mbar"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(setPointReplay == newValue)
			{
				return;
			}
			setPointReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpINJ_6B02::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpINJ_6B02::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, rr2Replay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, rr2Online, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpINJ_6B02::getMainColor(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(rr1 & RR1_ERROR)
	{
		color.setRgb(COLOR_ERROR);
	}
	// No errors - check ON/OFF state
	else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if(rr1 & RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else if(rr1 & RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpINJ_6B02::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, rr2Replay, setPointReplay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, rr2Online, setPointOnline, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpINJ_6B02::getMainStateString(unsigned rr1, unsigned rr2, float /* setPoint */, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
		return;
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
		return;
	}
	else if(rr1 & RR1_ERROR)	// Still different strings for ON/OFF
	{
		string = "Error";
	}
	else
	{
		unsigned mode = (rr2 & 0x00FF0000) >> 16;
		getModeName(mode, string);
		/*
		QString vvgString;
		getPartStateStringVVG(rr1, rr2, setPoint, plcAlarm, vvgString);
		string += ", ";
		string += vvgString;
		*/
	}
}
void EqpINJ_6B02::getModeName(int mode, QString &result)
{
	char resourceName[256];
	if(dpType)
	{
#ifdef Q_OS_WIN
		sprintf_s(resourceName, sizeof(resourceName) / sizeof(resourceName[0]), "%s.Mode_%d.ShortName", dpType, mode);
#else
		sprintf(resourceName, "%s.Mode_%d.ShortName", dpType, mode);
#endif
		if(ResourcePool::getInstance().getStringValue(resourceName, result) == ResourcePool::OK)
		{
//qDebug("OK: Resource <%s> -> %s\n", resourceName, result.toAscii().constData());
			return;
		}
	}
//qDebug("FAILURE: Resource <%s> -> %s\n", resourceName, result.toAscii().constData());
#ifdef Q_OS_WIN
	sprintf_s(resourceName, sizeof(resourceName) / sizeof(resourceName[0]), "Mode %d", mode);
#else
	sprintf(resourceName, "Mode %d", mode);
#endif
	result = resourceName;
}

#endif

/*
**	FUNCTION
**		Calculate color for given part of device
**
**	ARGUMENTS
**		part	- which part of device is requested (see EqpPart enum in Eqp.h)
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpINJ_6B02::getPartColor(int part, QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	unsigned useRr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	unsigned useRr2 = mode == DataEnum::Replay ? rr2Replay : rr2Online;
	bool useAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	switch(part)
	{
	case EqpPartPump:
		getPartColorPump(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVVG:
		getPartColorVVG(useRr1, useRr2, useAlarm, color);
		break;
	}
}
void EqpINJ_6B02::getPartColorPump(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if((rr1 & (RR1_ON | RR1_OFF)) == (RR1_ON | RR1_OFF))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else if(rr1 & RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}

void EqpINJ_6B02::getPartColorVVG(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr1 & RR1_ERROR)
	{
		color.setRgb(COLOR_ERROR);
	}
	else if((rr1 & (RR1_VVG_OPEN | RR1_VVG_CLOSE)) == (RR1_VVG_OPEN | RR1_VVG_CLOSE))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_VVG_OPEN)
	{
		color.setRgb(COLOR_VVG_OPEN);
	}
	else if(rr1 & RR1_VVG_CLOSE)
	{
		color.setRgb(COLOR_VVG_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_VVG_NOT_OPEN);
	}
}

void EqpINJ_6B02::getPartStateString(int part, QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	unsigned useRr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	unsigned useRr2 = mode == DataEnum::Replay ? rr2Replay : rr2Online;
	float setPoint = mode == DataEnum::Replay ? setPointReplay : setPointOnline;
	bool useAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	switch(part)
	{
	case EqpPartPump:
		getPartStateStringPump(useRr1, useRr2, useAlarm, string);
		break;
	case EqpPartVVG:
		getPartStateStringVVG(useRr1, useRr2, setPoint, useAlarm, string);
		break;
	}
}

void EqpINJ_6B02::getPartStateStringPump(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else
	{
		unsigned mode = (rr2 & 0x00FF0000) >> 16;
		getModeName(mode, string);
	}
}

void EqpINJ_6B02::getPartStateStringVVG(unsigned rr1, unsigned /* rr2 */, float setPoint, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if((rr1 & (RR1_VVG_OPEN | RR1_VVG_CLOSE)) == (RR1_VVG_OPEN | RR1_VVG_CLOSE))
	{
		string = "VVG Error";
	}
	else if(rr1 & RR1_VVG_OPEN)
	{
		string = "VVG OPEN";
	}
	else if(rr1 & RR1_VVG_CLOSE)
	{
		string = "VVG CLOSED";
	}
	else
	{
		string = "VVG ";
		string += QString::number(setPoint, 'f', 2);
	}
}

#endif

/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpINJ_6B02::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("RR1");
	dpeNames.append("RR2");
	dpeNames.append("SetPointR_mbar");
	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	if (values.isEmpty()) {
		return;
	}
	pVariant = values.takeFirst();
	unsigned rr2 = pVariant->toUInt();
	delete pVariant;
	if (values.isEmpty()) {
		return;
	}
	pVariant = values.takeFirst();
	float setPoint = (float)pVariant->toDouble();
	delete pVariant;
	getMainStateString(rr1, rr2, false, setPoint, state);
	getMainColor(rr1, rr2, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpINJ_6B02::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "EqpStatus");
	addDpeNameToList(dpes, "RR1");
	addDpeNameToList(dpes, "RR2");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpINJ_6B02::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if(rr1 & RR1_ON)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if(rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if(criteria.isWarnings())
	{
		if(rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpINJ_6B02::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	return rr1 & RR1_ERROR_CODE_MASK;
}

/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpINJ_6B02::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if(checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	// Without PLC connection does not match any criteria
	if(plcAlarmOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// In invalid state - only matches Error/warning criteria
	if(!(rr1Online & RR1_VALID))
	{
		switch(pCriteria->getType())
		{
		case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		default:
			pCriteria->setMatch(false, false, 0);
			return;
		}
	}

	// State is valid - decide based on criteria
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case FunctionalTypeINJ_6B02::Nominal:
			if(rr1Online & RR1_ON)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeINJ_6B02::VvgFullOpen:
			if(rr1Online & RR1_VVG_OPEN)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}

