#ifndef VACDPDATA_H
#define VACDPDATA_H

#include "VacDpDataConnection.h"

#include <qwidget.h>
#include <qstring.h>

class Eqp;

class VacDpData : public QWidget
{
	Q_OBJECT

	////////////////////////////////////////////////////////////////////////////
	///////////////////// Properties exposed to PVSS panel /////////////////////
	////////////////////////////////////////////////////////////////////////////
	Q_PROPERTY(QString dpName READ getDpName WRITE setDpName);
	Q_PROPERTY(DataEnumMode mode READ getMode WRITE setMode);

	Q_ENUMS(DataEnumMode);

public:
	// Mode of data acquistion - copy of enum from DataEnum
	enum DataEnumMode
	{
		Online = 0,
		Replay = 1,
		Polling = 2
	};
	 // Accessors for properties above
	inline const QString &getDpName(void) const { return dpName; }
	void setDpName(QString &name) { dpName = name; }
	inline DataEnumMode getMode(void) const { return (DataEnumMode)connection.getMode(); }
	void setMode(DataEnumMode mode) { connection.setMode((DataEnum::DataMode)mode); }

	// Construction/destruction
	VacDpData(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~VacDpData();

	int	connectDpe(const char *dpe);
	const QVariant &getDpeValue(const char *dpe);
	bool isPlcAlarm(void);

signals:
	void newDpeValue(const char *dpe, int source, const QVariant &value);

protected:
	VacDpDataConnection	connection;

	// DP name for connection
	QString			dpName;

	void findEqp(void);
	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACDPDATA_H
