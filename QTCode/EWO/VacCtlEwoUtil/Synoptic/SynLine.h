#ifndef	SYNLINE_H
#define	SYNLINE_H

// One beam line of synoptic drawing. All properties and methods in this
// line are enough for drawing line itself.

#include "SynEqpItem.h"
#include "ChildSynLine.h"
#include "SynLhcSectorLabel.h"

#include "EqpType.h"
#include "InterfaceEqp.h"

#include <QList>
#include <QPainter>
#include <QColor>

#include <stdio.h>

class VacEqpTypeMask;

class BeamLine;
class BeamLinePart;
class Sector;
class Eqp;
class EqpCOLDEX;

#include <QWidget>

// Line itself shall react on COLDEX state change, that's why it is
// subclass of InterfaceEqp

class SynLine : public InterfaceEqp
{
	Q_OBJECT

public:
	virtual ~SynLine();

	static SynLine *create(QWidget *parent, BeamLine *pLine, Sector *pStartSector, Sector *pEndSector);

	void clear(void);
	void analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allCryoEqpTypes,
			VacEqpTypeMask &allBeamEqpTypes);
	int build(const VacEqpTypeMask &eqpTypes, bool hidePassive);
	virtual bool findDevicePosition(Eqp *pEqp, int &eqpPosition, bool &eqpVisible);

	// The following methods shall be implemented by sector line class
	virtual int build(QList<Sector *> & /* sectorUsage */) { Q_ASSERT(0); return -1; }
	virtual void buildSectorIcons(void) { }
	void addSectorNameSpace(bool labelsUpAndDown);

	QList<ChildSynLine *> *moveHorizontally(int delta, bool touchConnect);
	void moveVertically(int delta, bool touchConnect);
	void increaseRow(int baseRowIdx);
	QList<ChildSynLine *> *moveLinePart(int startItemIdx, int delta);
	void movePartAfter(int coord, int delta);
	int findConnectEqp(const char *forLine, int type);
	const char *havePartAway(int &connStartX, int &connEndX);
	int getEqpX(int eqpIdx);

	void finishGeometry(void);
	void addAlarmIcons(void);

	void setViewStart(int viewStart, int viewWidth, bool dragging);

	void draw(QPainter &painter, int viewStart, int viewEnd);

	void fixStart(int viewStart, const VacEqpTypeMask &newMask);
	void findRecommendedStart(int &startMin, int &startMax);
	bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	// Access
	virtual inline void setMode(DataEnum::DataMode mode) { this->mode = mode; }
	virtual inline QList<SynEqpItem *> &getEqps(void) { return eqps; }
	virtual inline float getMetersToScreen(void) const { return metersToScreen; }
	virtual inline const BeamLine *getBeamLine(void) const { return pBeamLine; }
	virtual inline const BeamLinePart *getStartPart(void) const { return pStartPart; }
	virtual inline int getStartEqpIdx(void) const { return startEqpIdx; }
	virtual inline const BeamLinePart *getEndPart(void) const { return pEndPart; }
	virtual inline int getEndEqpIdx(void) const { return endEqpIdx; }
	virtual inline bool isReverse(void) const { return reverse; }
	virtual inline bool isAnalyzed(void) const { return analyzed; }
	virtual inline bool isVertReady(void) const { return vertReady; }
	virtual inline void setVertReady(bool ready) { vertReady = ready; }
	virtual inline bool isHorReady(void) const { return horReady; }
	virtual inline void setHorReady(bool ready) { horReady = ready; }
	virtual inline int getStartX(void) const { return startX; }
	virtual inline int getEndX(void) const { return endX; }
	virtual inline void setEndX(int x) { endX = x; }
	virtual inline int getY(void) const { return y; }
	virtual inline int getAscent(void) const { return ascent; }
	virtual inline int getDescent(void) const { return descent; }
	virtual inline int getStartXBeamLine(void) const { return startXBeamLine; } // [VACCO-527]
	virtual inline int getEndXBeamLine(void) const { return endXBeamLine; } // [VACCO-527]

	virtual inline const Eqp *getStartEqp(void) const { return pStartEqp; }
	virtual inline int getStartConnectX(void) const { return startConnectX; }
	virtual inline void setStartConnectX(int x) { startConnectX = x; }
	virtual inline int getStartConnectY(void) const { return startConnectY; }
	virtual inline void setStartConnectY(int y) { startConnectY = y; }
	virtual inline const Eqp *getEndEqp(void) const { return pEndEqp; }
	virtual inline int getEndConnectX(void) const { return endConnectX; }
	virtual inline void setEndConnectX(int x) { endConnectX = x; }
	virtual inline int getEndConnectY(void) const { return endConnectY; }
	virtual inline void setEndConnectY(int y) { endConnectY = y; }

	virtual inline Eqp *getColdex(void) const { return (Eqp *)pColdex; }
	virtual inline void setColdexState(bool isIn, bool isOut) { coldexIsIn = isIn; coldexIsOut = isOut; }
	virtual inline void setColdexLine(const SynLine *line) { coldexLine = line; }

	virtual inline bool isAlwaysIncludeSectorBorder(void) { return alwaysIncludeSectorBorder; }
	virtual inline void setAlwaysIncludeSectorBorder(bool flag) { alwaysIncludeSectorBorder = flag; }

	virtual inline void showMobilesNotConnectedInLine(void) { isShowMobiles = true; }
	virtual inline void hideMobilesNotConnectedInLine(void) { isShowMobiles = false; }
	
	virtual void setPipeColor(QColor &color, bool force = false);

	void dump(FILE *pFile);

	// [VACCO-527] To define the start/end tag for a BeamLine
	virtual QString findMainPartNameBeforeFirstEqp(BeamLine *pLine, BeamLinePart *pStartPart, int startEqpIdx);
	virtual QString findMainPartNameAfterLastEqp(BeamLine *pLine, BeamLinePart *pEndPart, int endEqpIdx);

	inline void setChildStart(bool flag) { childStart = flag; } // [VACCO-527]
	inline void setChildEnd(bool flag) { childEnd = flag; } // [VACCO-527]
	inline bool isChildStart(void) { return childStart; } // [VACCO-527]
	inline bool isChildEnd(void) { return childEnd; } // [VACCO-527]
	virtual void setTagStart(QString text) { tagStart = text; } // [VACCO-527]
	virtual void setTagEnd(QString text) { tagEnd = text; } // [VACCO-527]
	inline QString getTagStart(void) { return tagStart; } // [VACCO-527]
	inline QString getTagEnd(void) { return tagEnd; } // [VACCO-527]

	virtual void setSectorList(void);							// [VACCO-929]
	virtual void setSectorLabelList(void);						// [VACCO-929]
	virtual void setSectorGeometry(void);						// [VACCO-929]
	virtual QList<SynLhcSectorLabel *> getSectorLabelList(void) { return pSectorLabelList; } // [VACCO-929]
	virtual SynLhcSectorLabel* getSectorFromXY(int sectx, int secty); // [VACCO-929]
	virtual void drawSectSelection(QPainter &painter);			// [VACCO-929]
	virtual QList<Eqp *> getListEqpsActiveMayChange(void) { return listEqpsActiveMayChange; }
	virtual QList<Eqp *> getListEqpsPositionMayChange(void) { return listEqpsPositionMayChange; }

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value,
		DataEnum::DataMode mode, const QDateTime &timeStamp);

signals:
	void coldexStateChanged(bool isIn, bool isOut);

protected:
	// Widget where line will be drawn
	QWidget					*parent;

	// List of devices to be drawn
	QList<SynEqpItem *>	eqps;


	// Color to draw beam pipe, eqp. connectrions etc.
	QColor					pipeColor;

	// Coefficient to convert non-vacuum equipment length to width on screen
	float					metersToScreen;


	// Pointer to beam line - primary source of this synoptics line
	BeamLine			*pBeamLine; // [VACCO-527] before the tag feature *pBeamLine was a constant

	// Pointer to beam line part where 1st equipment for this synoptic line is located
	const BeamLinePart		*pStartPart;

	// Index of 1st device for this synoptic line in first beam line part
	int					startEqpIdx;

	// Pointer to beam line part where last equipment for this synoptic line is located
	const BeamLinePart		*pEndPart;

	// Index of 1st device for this synoptic line in last beam line part
	int					endEqpIdx;


	// Equipment at 'parent' line where this line starts, NULL if this line does not
	// start at another line
	Eqp					*pStartEqp;

	// X coordinate of this line's start at parent line
	int					startConnectX;

	// Y coordinate of this line's start at parent line
	int					startConnectY;


	// Equipment at 'parent' line where this line ends, NULL if this line does not
	// end at another line
	Eqp					*pEndEqp;

	// X coordinate of this line's end at parent line
	int					endConnectX;

	// Y coordinate of this line's end at parent line
	int					endConnectY;

	// [VACCO-929] List of sectors in the SynLine
	QList<Sector *>		pSectorList;

	// [VACCO-929] List of sector labels in the SynLine
	QList<SynLhcSectorLabel *>	pSectorLabelList;

	// Positive index of 'row' which detrmines vertical position of this line relative
	// to other lines
	int					row;

	// Start coordinate of this line's image on unlimited drawing area
	int					startX;

	// End coordinate of this line's image on unlimited drawing area
	int					endX;

	// [VACCO-527] X Start coordinate of the beamline on unlimited drawing area
	int					startXBeamLine;

	// [VACCO-527] X End coordinate of the beamline on unlimited drawing area
	int					endXBeamLine;

	// Y-coordinate of this line's image on unlimited drawing area - base line
	// Line is drwan horizontally, so there is just one coordinate
	int					y;

	// Ascent of this line's image above base line
	int					ascent;

	// Descent of this line's image below base line
	int					descent;


	// Pointer to COLDEX device if COLDEX is in this line
	EqpCOLDEX			*pColdex;

	// Pointer to COLDEX line if COLDEX is in a child line
	const SynLine	*coldexLine;

	// Flag indicating if COLDEX is IN
	bool					coldexIsIn;

	// Flag indicating if COLDEX is OUT
	bool					coldexIsOut;

	// Flag indicating if sector border devices shall always be included
	bool					alwaysIncludeSectorBorder;

	// Pipe color is forced, shall not calculate from devices
	bool					pipeColorForced;

	// Minimum space between two neighbour elements [Pixels]
	int					minSpacing;

	// Flag indicating if this line contains interlock sources
	bool					haveInterlockSources;

	// [VACCO-527] Size of startTag - text to be added before the start of the beamLine
	QSize sizeStartTag;

	// [VACCO-527] Size of endTag - text to be added after the end of the beamLine
	QSize sizeEndTag;


	// When applying new equipment type mask - it is required to keep scroll position
	// at position it was before applying new mask (== 'as close as possible to old
	// position). In order to achieve this - line shall fix last invisible device
	// and first visible device BEFORE applying new mask, making sure that these
	// devices will also be in line AFTER applying the mask

	// Last invisible device before applying new mask
	Eqp					*pLastInvisible;

	// First visible device before applying new mask
	Eqp					*pFirstVisible;

	// Data acquisition mode for this line
	DataEnum::DataMode	mode;

	// Flag indicating if global line analysis is complete.
	// In principle this shall be done by method AnalyzeTypes() which must be called
	// before drawing. However, sometimes drawing is done before AnalyzeTypes()
	// have been called, for example, from Paint event of control.
	bool				analyzed;

	// Flag indicating if this line is drawn in reverse order
	bool				reverse;

	// Flag indicating if final horizontal position for this line is ready
	bool				horReady;

	// Flag indicating if final vertical position for this line is ready
	bool				vertReady;

	// List of equipments used to rebuilt qdialog when active state of Eqp changed
	QList<Eqp*> listEqpsActiveMayChange;

	// List of equipments used to rebuilt qdialog when position changed
	QList<Eqp*> listEqpsPositionMayChange;

	// Flag to show mobiles not connected on synoptic
	bool				isShowMobiles;

	// Line is only created via create() static method
	SynLine();

	virtual void checkForReverse(void);
	void addDevice(const BeamLinePart *pPart, int eqpIdx, Eqp *pEqp, int type);
	void reverseEqp(void);
	void buildGroups(void);
	void buildGeometry(void);
	int findMinPos(SynEqpItem *pItem, bool checkPassive, int &minRectPos);

	void drawEqp(QPainter &painter, int viewStart, int viewEnd);
	void drawBeamPipe(QPainter &painter, int viewStart, int viewEnd);
	void drawCommonConn(QPainter &painter, SynEqpItem *pStartItem);
	virtual void drawSectorBorder(QPainter &painter, SynEqpItem *pItem);
	virtual void markInterlockSources(void);

	void dump(const char *fileName);

	// [VACCO-527] Flag to sign if the line is connected (at the start or end) to another (parent) line
	bool				childStart;
	bool				childEnd;

	// [VACCO-527] Start of line text TAG, for both automatic and manual tag - For synoptic display 
	QString				tagStart;

	// [VACCO-527] Start of line text TAG, for both automatic and manual tag - For synoptic display
	QString				tagEnd;


};

#endif	// SYNLINE_H
