#ifndef	FUNCTIONALTYPEVP_NEG_H
#define	FUNCTIONALTYPEVP_NEG_H

// Special functional type for VP_NEG - chain of NEG pumps

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVP_NEG : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Off = 1,
		On = 2
	};

	FunctionalTypeVP_NEG(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVP_NEG() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVP_NEG_H
