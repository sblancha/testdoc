#ifndef VACICONVVS_PS_H
#define	VACICONVVS_PS_H

// Icon for VVS_PS

#include "VacIcon.h"

class EqpVV;

class VACCTLEWOUTIL_EXPORT VacIconVVS_PS : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVVS_PS(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVVS_PS();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

	// Override method of VacIcon 
	virtual void setDirection(VacIconContainer::Direction newDirection);
	void setBeamDirection(VacIconContainer::BeamDirection newDirection);

protected:
	// Pointer to my device
	EqpVV *pEqp;

	// Rectangles for interlock symbols. Drawing can be easy using QPainter
	// coordinate transformations, but finding areas for tooltips is not so
	// evident. May be it is more convenient just to keep in meory coordinates
	// of all interlock rectangles

	QRect	intlBefore;
	QRect	intlAfter;

	void setInterlockGeometry(void);

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
	virtual void drawInterlocks(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
	
	void setPainterForControl(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
	void setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
	QColor colorForInterlock(Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm, unsigned bit);
};

#endif	// VACICONVVS_PS_H
