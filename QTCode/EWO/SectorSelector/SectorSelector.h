#ifndef SECTORSELECTOR_H
#define	SECTORSELECTOR_H

// EWO interface for SectorSelectorControl widget

#include <BaseExternWidget.hxx>

#include "SectorSelectorControl.h"

class EWO_EXPORT SectorSelector : public BaseExternWidget
{
	Q_OBJECT

public:
	SectorSelector(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void selectionChange(void);

private:
	SectorSelectorControl	*baseWidget;
};

#endif	// SECTORSELECTOR_H
