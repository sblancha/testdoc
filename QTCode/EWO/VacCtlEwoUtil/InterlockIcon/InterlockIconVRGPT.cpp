#include "InterlockIconVRGPT.h"

#include "qpainter.h"

InterlockIconVRGPT::InterlockIconVRGPT(QWidget * parent) :
InterlockIcon(parent)
{
	

}

InterlockIconVRGPT::~InterlockIconVRGPT()
{

}

void InterlockIconVRGPT::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	QFont calibri("Calibri", 10, QFont::Bold);
	QFont calibriBig("Calibri", 12);
	//draw title
	QPen whitePen(Qt::white);
	QBrush blackBrush(Qt::black);
	
	

	painter.setBrush(blackBrush);
	painter.setPen(whitePen);
	painter.setFont(calibri);
	painter.translate(80, 0);
	painter.drawRect(0, 0, 200, 20);
	painter.drawText(0, 0, 200, 20, Qt::AlignCenter | Qt::TextWrapAnywhere, this->name);
	
	
	painter.setPen(Qt::black);
	painter.setBrush(Qt::white);

	painter.drawRect(0, 20, 200, 300);
	
	painter.drawLine(0, 170, 200, 170);
	painter.drawLine(0, 80, 200, 80);
	painter.drawLine(0, 140, 200, 140);
	painter.drawLine(0, 200, 200, 200);
	painter.drawLine(0, 230, 200, 230);
	painter.drawLine(0, 260, 200, 260);
	painter.drawLine(0, 290, 200, 290);

	painter.drawLine(30, 20, 30, 140);
	painter.drawLine(30, 170, 30, 290);

	painter.drawLine(170, 20, 170, 140);
	painter.drawLine(170, 170, 170, 290);

	painter.drawLine(0, 50, 30, 50);
	painter.drawLine(0, 110, 30, 110);

	painter.drawText(0, 140, 200, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "Levels");
	painter.drawText(0, 20, 30, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "A1");
	painter.drawText(0, 50, 30, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "A2");
	painter.drawText(0, 80, 30, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "B1");
	painter.drawText(0, 110, 30, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "B2");

	painter.translate(-80, 0);
	painter.drawText(0, 20, 80, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, attributes.value("A1Cable", ""));
	painter.drawText(0, 50, 80, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, attributes.value("A2Cable", ""));
	painter.drawText(0, 80, 80, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, attributes.value("B1Cable", ""));
	painter.drawText(0, 110, 80, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, attributes.value("B2Cable", ""));
	painter.translate(80, 0);

	painter.drawText(30, 20, 140, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, attributes.value("A1Gauge", "-"));
	painter.drawText(30, 50, 140, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, attributes.value("A2Gauge", "-"));
	painter.drawText(30, 80, 140, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, attributes.value("B1Gauge", "-"));
	painter.drawText(30, 110, 140, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, attributes.value("B2Gauge", "-"));


	painter.drawText(0, 170, 30, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, mapGaugeNumberToSlot(attributes.value("LevelR1Source", "No")));
	painter.drawText(0, 200, 30, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, mapGaugeNumberToSlot(attributes.value("LevelR2Source", "No")));
	painter.drawText(0, 230, 30, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, mapGaugeNumberToSlot(attributes.value("LevelR3Source", "No")));
	painter.drawText(0, 260, 30, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, mapGaugeNumberToSlot(attributes.value("LevelR4Source", "No")));

	painter.drawText(40, 170, 50, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, convertToExp(attributes.value("LevelR1Max", "-")));
	painter.drawText(40, 200, 50, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, convertToExp(attributes.value("LevelR2Max", "-")));
	painter.drawText(40, 230, 50, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, convertToExp(attributes.value("LevelR3Max", "-")));
	painter.drawText(40, 260, 50, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, convertToExp(attributes.value("LevelR4Max", "-")));

	painter.drawText(110, 170, 50, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, convertToExp(attributes.value("LevelR1Min", "-")));
	painter.drawText(110, 200, 50, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, convertToExp(attributes.value("LevelR2Min", "-")));
	painter.drawText(110, 230, 50, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, convertToExp(attributes.value("LevelR3Min", "-")));
	painter.drawText(110, 260, 50, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, convertToExp(attributes.value("LevelR4Min", "-")));

	painter.drawText(15, 290, 185, 30, Qt::AlignVCenter | Qt::AlignLeft, "DP adr : " + attributes.value("DP", "-"));

	QPainterPath path;
	path.moveTo(70, 160);
	path.lineTo(75, 152);
	path.lineTo(80, 160);
	path.lineTo(70, 160);
	
	path.moveTo(130, 152);
	path.lineTo(125, 160);
	path.lineTo(120, 152);
	path.lineTo(130, 152);
	painter.fillPath(path, blackBrush);




}

void InterlockIconVRGPT::moveIcon(int x, int y)
{
	this->move(x - 280 / 2, y);
}

QPoint InterlockIconVRGPT::getConnectionPoint(QString connectorName)
{
	int baseX = this->x() + 280;
	int baseY = this->y();

	
	if (connectorName.contains("Relay1"))
	{
		baseY += 185;
	}
	else if (connectorName.contains("Relay2"))
	{
		baseY += 215;
	}
	else if (connectorName.contains("Relay3"))
	{
		baseY += 245;
	}
	else if (connectorName.contains("Relay4"))
	{
		baseY += 275;
	}

	return QPoint(baseX, baseY);
}

void InterlockIconVRGPT::getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber)
{
	direction = InterlockIconConnection::Right;
	offset = 25;
	writeCableNumber = false;
}

QRect InterlockIconVRGPT::getBoundingRect()
{
	QPoint topLeft = QPoint(this->x(), this->y());
	QRect boundingRect(topLeft, topLeft + QPoint(280, 320));

	return boundingRect;
}

QString InterlockIconVRGPT::convertToExp(QString floatString)
{
	float floatValue;
	bool ok;
	floatValue = floatString.toFloat(&ok);
	QString expString = floatString;
	if (ok)
	{
		expString = QString("%1").arg(floatValue, 0, 'E', 2);
	}

	return expString;
}

QString InterlockIconVRGPT::mapGaugeNumberToSlot(QString gaugeNumber)
{
	if (!gaugeNumber.compare("1"))
	{
		return "A1";
	}
	else if (!gaugeNumber.compare("2"))
	{
		return "A2";
	}
	else if (!gaugeNumber.compare("3"))
	{
		return "B1";
	}
	else if (!gaugeNumber.compare("4"))
	{
		return "B2";
	}
	else
	{
		return "No";
	}
}