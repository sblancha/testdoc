#include "InterlockIconConnection.h"

#include "qpainter.h"

InterlockIconConnection::InterlockIconConnection(QPoint startPoint, QPoint endPoint, QWidget * parent) :
QWidget(parent)
{
	orderedPointList.append(startPoint);
	orderedPointList.append(endPoint);
	QSize mySize(1920, 1720);
	setFixedSize(mySize);
	startPointDirection = InterlockIconConnection::Undefined;
	endPointDirection = InterlockIconConnection::Undefined;
	startPointOffset = endPointOffset = 0;
	isOpenEnded = false;
	writeCableEnd = writeCableStart = false;
}

InterlockIconConnection::InterlockIconConnection(QPoint startPoint, InterlockIconConnection::Direction direction, QString displayString, QWidget * parent) :
QWidget(parent)
{
	orderedPointList.append(startPoint);
	QSize mySize(1920, 1720);
	setFixedSize(mySize);
	startPointDirection = direction;
	endPointDirection = InterlockIconConnection::Undefined;
	startPointOffset = endPointOffset = 0;
	isOpenEnded = true;
	this->displayString = displayString;
	writeCableEnd = writeCableStart = false;
}

InterlockIconConnection::~InterlockIconConnection()
{

}


void InterlockIconConnection::calculateGeometry()
{
	if (orderedPointList.length() < 2)
		return;

	QPoint startPoint = orderedPointList.at(0);
	QPoint endPoint = orderedPointList.at(1);
	int insertIndex = 1;

	QPoint firstMidPoint;
	if (startPointOffset > 0)
	{
		switch (startPointDirection)
		{
		case InterlockIconConnection::Up:
			firstMidPoint = startPoint + QPoint(0, -startPointOffset);
			break;
		case InterlockIconConnection::Down:
			firstMidPoint = startPoint + QPoint(0, startPointOffset);
			break;
		case InterlockIconConnection::Right:
			firstMidPoint = startPoint + QPoint(startPointOffset, 0);
			break;
		case InterlockIconConnection::Left:
			firstMidPoint = startPoint + QPoint(-startPointOffset, 0);
			break;
		default:
			firstMidPoint = startPoint;
		}
	}
	else
	{
		firstMidPoint = startPoint;
	}

	QPoint lastMidPoint;

	if (endPointOffset > 0)
	{
		switch (endPointDirection)
		{
		case InterlockIconConnection::Up:
			lastMidPoint = endPoint + QPoint(0, -endPointOffset);
			break;
		case InterlockIconConnection::Down:
			lastMidPoint = endPoint + QPoint(0, endPointOffset);
			break;
		case InterlockIconConnection::Right:
			lastMidPoint = endPoint + QPoint(endPointOffset, 0);
			break;
		case InterlockIconConnection::Left:
			lastMidPoint = endPoint + QPoint(-endPointOffset, 0);
			break;
		default:
			lastMidPoint = endPoint;
		}
	}
	else
	{
		lastMidPoint = endPoint;
	}

	orderedPointList.insert(insertIndex,firstMidPoint);
	insertIndex++;


	QPoint secondMidPoint;
	if (!areLinesParalel(startPointDirection, endPointDirection))
	{
		if (startPointDirection == InterlockIconConnection::Left || startPointDirection == InterlockIconConnection::Right)
		{
			secondMidPoint = QPoint(firstMidPoint.x(), lastMidPoint.y());
		}
		else
		{
			secondMidPoint = QPoint(lastMidPoint.x(), firstMidPoint.y());
		}

		orderedPointList.insert(insertIndex,secondMidPoint);
		insertIndex++;
	}
	else
	{
		QPoint thirdMidPoint;

		if (startPointDirection == InterlockIconConnection::Up || startPointDirection == InterlockIconConnection::Down)
		{
			int halfDistance = (firstMidPoint.y() + lastMidPoint.y()) / 2;
			secondMidPoint = QPoint(firstMidPoint.x(), halfDistance);
			thirdMidPoint = QPoint(lastMidPoint.x(), halfDistance);
		}
		else
		{
			int halfDistance = (firstMidPoint.x() + lastMidPoint.x()) / 2;
			secondMidPoint = QPoint(halfDistance, firstMidPoint.y());
			thirdMidPoint = QPoint(halfDistance, lastMidPoint.y());
		}

		orderedPointList.insert(insertIndex, secondMidPoint);
		orderedPointList.insert(insertIndex + 1, thirdMidPoint);
		insertIndex += 2;
	}

	orderedPointList.insert(insertIndex, lastMidPoint);
}

void InterlockIconConnection::paintEvent(QPaintEvent * pEvent)
{
	QPainter painter(this);
	painter.setPen(Qt::black);
	QFont calibri("Calibri", 10, QFont::Bold);
	painter.setFont(calibri);
	if (!isOpenEnded)
	{
		for (int i = 0; i < orderedPointList.length() - 1; i++)
		{
			painter.drawLine(orderedPointList.at(i), orderedPointList.at(i + 1));
		}

		if (writeCableEnd)
		{
			QPoint lastPoint = orderedPointList.last();
			QPoint stringPoint;
			if (endPointDirection == InterlockIconConnection::Left)
			{
				stringPoint = lastPoint;
				painter.drawText(stringPoint.x(), stringPoint.y(), 80, 50, Qt::AlignCenter, cableNumber);
			}
			if (endPointDirection == InterlockIconConnection::Right)
			{
				stringPoint = lastPoint - QPoint(80,0);
				painter.drawText(stringPoint.x(), stringPoint.y(), 80, 50, Qt::AlignCenter, cableNumber);
			}
			if (endPointDirection == InterlockIconConnection::Up)
			{
				stringPoint = lastPoint + QPoint(-40, -50);
				painter.drawText(stringPoint.x(), stringPoint.y(), 80, 50, Qt::AlignCenter, cableNumber);
			}
			if (endPointDirection == InterlockIconConnection::Down)
			{
				stringPoint = lastPoint + QPoint(-40, 0);
				painter.drawText(stringPoint.x(), stringPoint.y(), 80, 50, Qt::AlignCenter, cableNumber);
			}
		}
		if (writeCableStart)
		{
			QPoint firstPoint = orderedPointList.first();
			QPoint stringPoint;
			if (startPointDirection == InterlockIconConnection::Left)
			{
				stringPoint = firstPoint - QPoint(80, 0);
				painter.drawText(stringPoint.x(), stringPoint.y()- 25, 80, 25, Qt::AlignCenter, cableNumber);
			}
			if (startPointDirection == InterlockIconConnection::Right)
			{
				stringPoint = firstPoint;
				painter.drawText(stringPoint.x(), stringPoint.y() - 25, 80, 50, Qt::AlignCenter, cableNumber);
			}
			if (startPointDirection == InterlockIconConnection::Up)
			{
				stringPoint = firstPoint + QPoint(-40, -50);
				painter.drawText(stringPoint.x(), stringPoint.y() - 25, 80, 50, Qt::AlignCenter, cableNumber);
			}
			if (startPointDirection == InterlockIconConnection::Down)
			{
				stringPoint = firstPoint + QPoint(-40, 0);
				painter.drawText(stringPoint.x(), stringPoint.y() - 25, 80, 50, Qt::AlignCenter, cableNumber);
			}
		}
	}
	else
	{
		QPoint lineEnd;
		QRect textRect;
		if (startPointDirection == InterlockIconConnection::Up)
		{
			lineEnd = orderedPointList.at(0) + QPoint(0, -75);
			textRect = QRect(lineEnd + QPoint(-60, -55), lineEnd + QPoint(60, -5));
		}
		else if (startPointDirection == InterlockIconConnection::Down)
		{
			lineEnd = orderedPointList.at(0) + QPoint(0, 75);
			textRect = QRect(lineEnd + QPoint(-60, 5), lineEnd + QPoint(60, 55));
		}
		else if (startPointDirection == InterlockIconConnection::Right)
		{
			lineEnd = orderedPointList.at(0) + QPoint(75, 0);
			textRect = QRect(lineEnd + QPoint(5, -25), lineEnd + QPoint(125, 25));
		}
		else if (startPointDirection == InterlockIconConnection::Left)
		{
			lineEnd = orderedPointList.at(0) + QPoint(-75, 0);
			textRect = QRect(lineEnd + QPoint(-125, -25), lineEnd + QPoint(-5, 25));
		}
		else
		{
			lineEnd = orderedPointList.at(0);
		}

		painter.drawLine(orderedPointList.at(0), lineEnd);
		painter.drawText(textRect, Qt::AlignCenter, displayString);
	}
	

	
}

void InterlockIconConnection::setStartOffset(InterlockIconConnection::Direction direction, int offset)
{
	startPointDirection = direction;
	startPointOffset = offset;
}

void InterlockIconConnection::setEndOffset(InterlockIconConnection::Direction direction, int offset)
{
	endPointDirection = direction;
	endPointOffset = offset;
}

bool InterlockIconConnection::areLinesParalel(const InterlockIconConnection::Direction direction1, const InterlockIconConnection::Direction direction2)
{
	if (direction1 == InterlockIconConnection::Up || direction1 == InterlockIconConnection::Down)
	{
		if (direction2 == InterlockIconConnection::Up || direction2 == InterlockIconConnection::Down)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (direction1 == InterlockIconConnection::Left || direction1 == InterlockIconConnection::Right)
	{
		if (direction2 == InterlockIconConnection::Left || direction2 == InterlockIconConnection::Right)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

void InterlockIconConnection::fixOverlapping(InterlockIconConnection &otherConnection)
{
	
	for (int i = 0; i < orderedPointList.length() - 1; i++)
	{
		QPoint p1 = orderedPointList.at(i);
		QPoint p2 = orderedPointList.at(i + 1);
		for (int j = 0; j < otherConnection.orderedPointList.length() - 1; j++)
		{
			
			QPoint p3 = otherConnection.orderedPointList.at(j);
			QPoint p4 = otherConnection.orderedPointList.at(j + 1);
			//lines only have horizontal or vertical components
			if (p1.x() == p2.x())
			{
				if (p3.x() == p4.x() && p3.x() == p1.x())
				{
					if (!((p3.y() > p2.y() && p3.y() > p1.y() && p4.y() > p2.y() && p4.y() > p1.y()) || (p3.y() < p2.y() && p3.y() < p1.y() && p4.y() < p2.y() && p4.y() < p1.y())))
					{
						otherConnection.orderedPointList.replace(j, otherConnection.orderedPointList.at(j) + QPoint(10, 0));
						otherConnection.orderedPointList.replace(j + 1, otherConnection.orderedPointList.at(j + 1) + QPoint(10, 0));
					}

				}
			}

			if (p1.y() == p2.y())
			{
				if (p3.y() == p4.y() && p3.y() == p1.y())
				{
					if (!((p3.x() > p2.x() && p3.x() > p1.x() && p4.x() > p2.x() && p4.x() > p1.x()) || (p3.x() < p2.x() && p3.x() < p1.x() && p4.x() < p2.x() && p4.x() < p1.x())))
					{
						otherConnection.orderedPointList.replace(j, otherConnection.orderedPointList.at(j) + QPoint(0, 10));
						otherConnection.orderedPointList.replace(j + 1, otherConnection.orderedPointList.at(j + 1) + QPoint(0, 10));
					}
				}
			}
		}
			
	}

}

void InterlockIconConnection::setWriteCables(bool writeCableStart, bool writeCableEnd)
{
	this->writeCableStart = writeCableStart;
	this->writeCableEnd = writeCableEnd;
}
