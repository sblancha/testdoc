# Project file for small test application for SPS main view EWO
#
#	23.12.2008	L.Kopylov
#		Initial version
#

TEMPLATE = app

TARGET = SpsainViewer

INCLUDEPATH += .. ../../../VacCtlUtil

unix:LIBS += -L../../../../bin/widgets/linux -L../ -lMainViewSps

CONFIG += qt thread release warn_on

DEFINES += _VACDEBUG


unix:HEADERS = SpsMainViewer.h

unix:SOURCES = SpsMainViewer.cpp\
				main.cpp
