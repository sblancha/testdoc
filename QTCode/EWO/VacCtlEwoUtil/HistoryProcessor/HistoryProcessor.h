#ifndef HISTORYPROCESSOR_H
#define	HISTORYPROCESSOR_H

#include "VacCtlEwoUtilExport.h"

#include "PlatformDef.h"

// Class that collects analog history values from one (or more) devices
// and then writes to Excel file with optional time reduction.
// This class uses instance of ExcelWriter to produce Excel file

#include "HistoryEqpValuesSet.h"

#include <QString>
#include <QDateTime>

#include <stdio.h>

class ExcelWriter;

class VACCTLEWOUTIL_EXPORT HistoryProcessor
{
public:
	HistoryProcessor();
	~HistoryProcessor();

	void clear(void);
	void addValue(Eqp *pEqp, const QString &bitDpe, int bitNbr, const QString &bitName, const QDateTime &time, float value);
	void setPreviousValue(Eqp *pEqp, const QString &bitDpe, int bitNbr, const QString &bitName, const QDateTime &minTime, float value);
	void addValue(Eqp *pEqp, const QString &dpe, const QDateTime &time, float value);
	void setPreviousValue(Eqp *pEqp, const QString &dpe, const QDateTime &minTime, float value);
	QString write(FILE *pFile, bool writeCsv);
	QString write(FILE *pFile, ExcelWriter *pWriter, bool writeCsv);

	// Access
	inline int getInterval(void) const { return interval; }
	inline void setInterval(int interval) { this->interval = interval; }

protected:
	// List of devices with their history
	QList<HistoryEqpValuesSet *>	*pEqpList;

	// Pointer to last used values set, used to speed up search when adding value
	HistoryEqpValuesSet				*pLastSet;

	// Time reduction interval, 0 = write to output all values
	int								interval;

	int buildAllTimes(QList<QDateTime> &alLTimesList);

};

#endif	// HISTORYPROCESSOR_H
