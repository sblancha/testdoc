#ifndef VACICONEWO_H
#define VACICONEWO_H

// EWO interface for VacIconContainer widget

#include <BaseExternWidget.hxx>

#include "VacIconContainer.h"

class EWO_EXPORT VacIconEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	VacIconEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void iconMousePress(int button, int mode, int x, int y, int extra, const char *dpName);
		
private:
	VacIconContainer	*baseWidget;
};

#endif	// VACICONEWO_H
