#ifndef	VACICONVINT_VELO_H
#define	VACICONVINT_VELO_H

//	Icon for VELO interlock

#include "VacIcon.h"

class EqpVINT_VELO;

class VACCTLEWOUTIL_EXPORT VacIconVINT_VELO : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVINT_VELO(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVINT_VELO();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVINT_VELO *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACICONVINT_VELO_H
