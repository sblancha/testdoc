#ifndef RACKVIEW_H
#define RACKVIEW_H


#include <qwidget.h>
//#include "SynopticWidget.h"

class Rack;
class RackWidget;
class RackIcon;

class RackView : public QWidget
{
	Q_OBJECT

public:
	RackView(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	~RackView();

	static RackView *create(QWidget *parent, QString buildingName);
	virtual void setViewStart(int viewStart, int viewWidth, bool dragging);
	void addWidget(Rack * rack);
	void showAllRacks();
	inline int getWidgetCount(){ return rackWidgets.count(); };
	int renderWidget(QPainter &painter, int index);
	void selectEquipment(QString equipmentName);
	int getWidgetHeight();

public slots:
void selectionChanged(RackIcon * rackIcon);

protected:
	QString buildingName;
	qreal scale;
	QList<RackWidget *> rackWidgets;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void wheelEvent(QWheelEvent *pEvent);
};



#endif