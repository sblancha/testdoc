//	Implementation of class DataPool
///////////////////////////////////////////////////////////

#include "DataPool.h"
#include "SectorOrder.h"

#include "PlatformDef.h"
#include "DebugCtl.h"

#include "FunctionalType.h"
#include "EqpMsgCriteria.h"
#include "Eqp.h"
#include "EqpType.h"
#include "EqpMP_ACCESS.h"
#include "EqpSECT_VPI_SUM.h"
#include "EqpSMSMachineMode.h"
#include "Rack.h"
#include "RackEqp.h"
#include "InterlockChain.h"
#include "InterlockEqp.h"


#ifdef Q_OS_WIN
#include <io.h>		// for _access()
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif

#include <math.h>	// for fabs()

#include <QDateTime>
#include <QStringList>

#include <QtDebug>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

DataPool DataPool::pool;

DataPool &DataPool::getInstance(void) { return pool; }

// File parsing type
typedef enum
{
	ParseLineConfig = 1,
	ParsePassive = 2,
	ParseActive = 3,
	ParseLhcGlobal = 4,
	ParseRackData = 5,
	ParseInterlockData = 6
} ParseEnum;

// Auxilliary data type to determine if sector list is continuous
typedef struct
{
	BeamLine	*pLine;		// Pointer to beam line
	short		nParts;		// Number of sector draw parts in this line
	short		nConnects;	// Number of connections (via sector draw part) to other lines
} SSectorConnectInfo, *SSectorConnectInfoPtr;



// Functions to be called by qsort()
static int compareIpCoord(const void *p1, const void *p2);

bool regionLessThan(LhcRegion *pReg1, LhcRegion *pReg2)
{
	if(pReg1->getStart() > pReg1->getEnd())
	{
		return false;
	}
	return pReg1->getStart() < pReg2->getStart();
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DataPool::DataPool()
{
	status = StatusClean;
	rackDataLoaded = false;
	interlockDataLoaded = false;
	fileNameBuffer = NULL;
	pureSurvey = true;
	timeStamp = NULL;
	machine = NULL;
	dataPart = NULL;
	currentLineIdx = 0;
	lhcIps = NULL;
	nLhcIps = 0;
	minPassiveEqpVisibleWidth = 0.0;
	// Set static reference in other classes
	Sector::setDataPool(this);
#ifndef PVSS_SERVER_VERSION
	SectorOrder::setDataPool(this);
#endif
}

DataPool::~DataPool()
{
	clearData(true);
	if(fileNameBuffer)
	{
		free((void *)fileNameBuffer);
	}
	if(dataPart)
	{
		free((void *)dataPart);
	}

	qDeleteAll(rackList);
	rackList.clear();
}

///////////////////////////////////////////////////////////////////////////////////////
/////////////////// Data initialization/cleanup ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

/*
**
** FUNCTION
**		Reset content of data pool to state before any file reads
**
** PARAMETERS
**		None
**
** RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void DataPool::reset(void)
{
	mutex.lock();
	clearData(true);
	status = StatusClean;
	mutex.unlock();
}

/*
**	FUNCTION
**		Initialize non-vacuum equipment data from files, build beam lines and all
**		non-controllable equipment in lines. Note that names of input files are
**		hardcoded: they are built from machine name accoding to predefined rules.
**
**	PARAMETERS
**		isLhcFormat		- true if input file has been generated from LHC DB,
**							false if file has been generated from SURVEY DB
**		pathName		- Name of path where all required files shall be located
**		machineName		- Name of machine for which data are read.
**		errList			- Variable where error message shall be added
**							in case of errors.
**
**	RETURNS
**		Number of resulting beam lines
**		-1 - in case of error
**
**	CAUTIONS
**		None
**
*/
int DataPool::initPassive(bool isLhcFormat, bool withoutConfig, const char *pathName, const char *machineName,
	QStringList &errList)
{
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::initPassive() start\n");
#else
		printf("DataPool::initPassive() start\n");
		fflush(stdout);
#endif
	}
	int	coco;
	mutex.lock();
	if(canInitPassive(coco, errList))
	{
		coco = parsePassive(isLhcFormat, withoutConfig, pathName, machineName, errList);
		status = coco > 0 ? StatusPassiveOK : StatusPassiveFailed;
	}
	mutex.unlock();
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::initPassive() finish, coco %d\n", coco);
#else
		printf("DataPool::initPassive() finish, coco %d\n", coco);
		fflush(stdout);
#endif
	}
	return coco;
}

/*
**
**	FUNCTION
**		Check if data pool is in a state when passive equipment can be
**		read from input file(s)
**
**	PARAMETERS
**		coco	- Variable where completion code will be pur if passive
**					equipment file(s) can not be parsed now
**		errList	- Variable where error message shall be added in case of error
**
**	RETURNS
**		true	- if passive equipment can be read from file(s);
**		false	- otherwise
**
**	CAUTIONS
**		None
**
*/
bool DataPool::canInitPassive(int &coco, QStringList &errList)
{
	// Check if data pool content was already initialized
	switch(status)
	{
	case StatusPassiveOK:
	case StatusActiveOK:
		coco = lines.count();
		return false;
	case StatusPassiveFailed:
		errList.append("DataPool::initPassive() failed before this call()");
		coco = -1;
		return false;
	case StatusActiveFailed:
		errList.append("DataPool::initActive() failed before this call()");
		coco = -1;
		return false;
	default:	// To make Linux compiler happy
		break;
	}
	return true;
}

/*
**	FUNCTION
**		Read non-vacuum equipment data from files, build beam lines and all
**		non-controllable equipment in lines. Note that names of input files are
**		hardcoded: they are built from machine name accoding to predefined rules.
**
**	PARAMETERS
**		isLhcFormat		- true if input file has been generated from LHC DB,
**							false if file has been generated from SURVEY DB
**		pathName		- Name of path where all required files shall be located
**		machineName		- Name of machine for which data are read.
**		errList			- Variable where error message shall be added
**							in case of errors.
**
**	RETURNS
**		Number of resulting beam lines
**		-1 - in case of error
**
**	CAUTIONS
**		It must be checked BEFORE calling this method that data pool is in a state
**		when this can be done - see canInitPassive()
**
*/
int DataPool::parsePassive(bool isLhcFormat, bool withoutConfig, const char *pathName, const char *machineName,
	QStringList &errList)
{
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::parsePassive() start\n");
#else
		printf("DataPool::parsePassive() start\n");
		fflush(stdout);
#endif
	}
	// Process files
	char	*fileName;
	#ifndef Q_OS_WIN
	struct stat fileStat;
	#endif

	clearData(true);

	this->lhcFormat = isLhcFormat;

	// Configuration of beam lines
	fileName = buildFileName(pathName, machineName, ".config");
	#ifdef Q_OS_WIN
	if(_access(fileName, 0) != -1)	// File exists, if not - 'pure survey' mode
	#else
	if(stat(fileName, &fileStat) == 0)
	#endif
	{
		pureSurvey = false;
		parseType = ParseLineConfig;
		parseCoco = 0;
		parseFile(fileName, errList);
		switch(parseCoco)
		{
		case 1:
			errList.append("Incomplete file");
			/* NOBREAK */
		case -1:
			addFileNameToMsg(errList, fileName);
			status = StatusPassiveFailed;
			return -1;
		}
		if(lines.isEmpty())
		{
			QString errMsg = "No beam lines in ";
			errMsg += fileName;
			errList.append(errMsg);
			return -1;
		}
	}
	else if(!withoutConfig)
	{
		QString errMsg = "File not found ";
		errMsg += fileName;
		errList.append(errMsg);
		status = StatusPassiveFailed;
		return -1;
	}
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::parsePassive() .config\n");
#else
		printf("DataPool::parsePassive() .config\n");
		fflush(stdout);
#endif
	}
	// Main file with passive elements
	fileName = buildFileName(pathName, machineName, "_list.ora");
	parseType = ParsePassive;
	if(parseFile(fileName, errList) < 0)
	{
		clearData(true);
		addFileNameToMsg(errList, fileName);
		return -1;
	}
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::parsePassive() .ora\n");
#else
		printf("DataPool::parsePassive() .ora\n");
		fflush(stdout);
#endif
	}
	// Second file with passive elements
	fileName = buildFileName(pathName, machineName, "_extra.txt");
	#ifdef Q_OS_WIN
	if(_access(fileName, 0) != -1)	// File exists
	#else
	if(stat(fileName, &fileStat) == 0)
	#endif
	{
		if(parseFile(fileName, errList) < 0)
		{
			clearData(true);
			addFileNameToMsg(errList, fileName);
			return -1;
		}
	}
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::parsePassive() .extra\n");
#else
		printf("DataPool::parsePassive() .extra\n");
		fflush(stdout);
#endif
	}

	// For LHC - read also extra file with global LHC parameters (total length, IP locations etc.)
	if(isLhcFormat)
	{
		fileName = buildFileName(pathName, machineName, "_VacConfig.txt");
		parseType = ParseLhcGlobal;
		if(parseFile(fileName, errList) < 0)
		{
			clearData(true);
			addFileNameToMsg(errList, fileName);
			return -1;
		}
	}
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::parsePassive() before pasPostProcess()\n");
#else
		printf("DataPool::parsePassive() before pasPostProcess()\n");
		fflush(stdout);
#endif
	}

	// Post-processing after reading all passive elements
	if(!pasPostProcess(errList))
	{
		clearData(true);
		return -1;
	}
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::parsePassive() finish\n");
#else
		printf("DataPool::parsePassive() finish\n");
		fflush(stdout);
#endif
	}
	return lines.count();
}

/*
**
**	FUNCTION
**		Initialize controllable equipment data from file, build main parts and sectors;.
**
**	PARAMETERS
**		machine		- Name of machine being processed
**		fileName	- Name of file with controllable equipment data (typically content
**						of this file is extracted from ORACLE).
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**    0  - in case of success
**    -1 - in case of error
**
**	CAUTIONS
**    None
**
*/
int DataPool::initActive(const char *machine, const char *fileName, QStringList &errList)
{
	if(DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": initActive() start";
	}
	int	coco;
	mutex.lock();
	if(canInitActive(coco, errList))
	{
		coco = parseActive(machine, fileName, errList);
		status = coco < 0 ? StatusActiveFailed : StatusActiveOK;
	}
	mutex.unlock();
	if(DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": initActive() finish";
	}
	return coco;
}


/*
**
**	FUNCTION
**		Initialize equipment data for racks, etc from file and build list
**
**	PARAMETERS
**		machine		- Name of machine being processed
**		fileName	- Name of file with documentation equipment data (typically content
**						of this file is extracted from ORACLE).
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**    0  - in case of success
**    -1 - in case of error
**
**	CAUTIONS
**    None
**
*/
int DataPool::initRackData(const char *machine, const char *fileName, QStringList &errList)
{
	if (DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": initActive() start";
	}
	int	coco=-1;
	mutex.lock();
	if (!rackDataLoaded)
	{
		coco = parseRackData(machine, fileName, errList); 
		if (coco == 0)
		{
			rackDataLoaded = true;
		}
		
	}
	mutex.unlock();
	return coco;

}

/*
**
**	FUNCTION
**		Initialize equipment data for interlock diagrams, etc from file and build list
**
**	PARAMETERS
**		machine		- Name of machine being processed
**		fileName	- Name of file with documentation equipment data (typically content
**						of this file is extracted from ORACLE).
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**    0  - in case of success
**    -1 - in case of error
**
**	CAUTIONS
**    None
**
*/
int DataPool::initInterlockData(const char *machine, const char *fileName, QStringList &errList)
{
	if (DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": initActive() start";
	}
	int	coco = -1;
	mutex.lock();
	if (!interlockDataLoaded)
	{
		coco = parseInterlockData(machine, fileName, errList);
		if (coco == 0)
		{
			interlockDataLoaded = true;
		}

	}
	mutex.unlock();
	return coco;

}


/*
**
**	FUNCTION
**		Check if data pool is in a state when active equipment can be
**		read from input file(s)
**
**	PARAMETERS
**		coco	- Variable where completion code will be pur if passive
**					equipment file(s) can not be parsed now
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**		true	- if passive equipment can be read from file(s);
**		false	- otherwise
**
**	CAUTIONS
**		None
**
*/
bool DataPool::canInitActive(int &coco, QStringList &errList)
{
	// Check if data pool content was already initialized
	switch(status)
	{
	case StatusActiveOK:
		coco = lines.count();
		return false;
	case StatusPassiveFailed:
		errList.append("DataPool::initPassive() failed before this call()");
		coco = -1;
		return false;
	case StatusActiveFailed:
		errList.append("DataPool::initActive() failed before this call()");
		coco = -1;
		return false;
	default:	// To make Linux compiler happy
		break;
	}
	return true;
}

/*
**
**	FUNCTION
**		Read controllable equipment data from file, build main parts and sectors;.
**
**	PARAMETERS
**		machine		- Name of machine being processed
**		fileName	- Name of file with controllable equipment data (typically content
**						of this file is extracted from ORACLE).
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**    0  - in case of success
**    -1 - in case of error
**
**	CAUTIONS
**		It must be checked BEFORE calling this method that data pool is in a state
**		when this can be done - see canInitActive()
**
*/
int DataPool::parseActive(const char *machine, const char *fileName, QStringList &errList)
{
	if(DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": parseActive(): start";
	}
	if(!lines.count())
	{
		errList.append("Beam lines data are not initialized");
		return -1;
	}
	clearData(false);
	parseType = ParseActive;
	parseCoco = 0;
	this->machine = addString(machine);
	parseFile(fileName, errList);
	switch(parseCoco)
	{
	case 1:
		errList.append("Incomplete file");
		/* NOBREAK */
	case -1:
		addFileNameToMsg(errList, fileName);
		return -1;
	}

	if(DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": parseActive(): parsing done";
	}

	#ifndef PVSS_SERVER_VERSION
	// For LHC - make 'minor' corrections to locations of equipment
	// at sector borders - data from LHC DB are not perfect...
	if(lhcFormat)
	{
		correctSectorBorders();
	}
	#endif
	if(DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": parseActive(): correctSectorBorders done";
	}

	// Post-processing after reading all active elements
	if(!actPostProcess(errList))
	{
		clearData(false);
		return -1;
	}
	if(DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": parseActive(): actPostProcess done";
	}

	// Build auxilliary equipment - it does not exist in file, but exists in PVSS
	buildAuxEquipment();
	if(DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": parseActive(): buildAuxEquipment done";
	}

	// Build dictionary for fast equipment search
	buildDpNameDictionary();
	if(DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": parseActive(): buildDpNameDictionary done";
	}

	// Make post-processing for all active equipment - L.Kopylov 14.05.2014: move before building ordered eqp list
	foreach(Eqp *pEqp, eqpDict)
	{
		pEqp->postProcess();
	}

	// Build ordered list of all equipment
	buildOrderedEqpList();
	if(DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": parseActive(): buildOrderedEqpList done:" << orderedEqpList.count();
	}

	/* L.Kopylov 14.05.2014 - moved above
	// Make post-processing for all active equipment
	foreach(Eqp *pEqp, eqpDict)
	{
		pEqp->postProcess();
	}
	*/

	if(lhcFormat)
	{
		calculateAccessPointDistanceLhc();
	}
	else
	{
		calculateAccessPointDistance();
	}

	if(DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": parseActive(): finish";
	}

	// For debugging
	if(DebugCtl::isData())
	{
		char	fileName[256];
#ifdef Q_OS_WIN
		sprintf_s(fileName, sizeof(fileName) / sizeof(fileName[0]), "D:\\%s_InitActive.txt", machine);
		FILE *pFile = NULL;
		if(fopen_s(&pFile, fileName, "w"))
		{
			pFile = NULL;
		}
#else
		sprintf(fileName, "/user/vacin/%s_InitActive.txt", machine);
		FILE *pFile = fopen(fileName, "w");
#endif
		if(pFile)
		{
			qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": start dump to " << fileName;
			dump(pFile);
			fclose(pFile);
		}
		else
		{
			qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": failed to open dump file " << fileName;
		}
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": parseActive(): dump finish";
	}

	return 0;
}

/*
**
**	FUNCTION
**		Read documentation data from file
**
**	PARAMETERS
**		machine		- Name of machine being processed
**		fileName	- Name of file with controllable equipment data (typically content
**						of this file is extracted from ORACLE).
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**    0  - in case of success
**    -1 - in case of error
**
**	CAUTIONS
**		It must be checked BEFORE calling this method that data pool is in a state
**		when this can be done - check rackDataLoaded variable
**
*/
int DataPool::parseRackData(const char *machine, const char *fileName, QStringList &errList)
{
	if (DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": parseDocData(): start";
	}
	parseType = ParseRackData;
	parseCoco = 0;

	//this->machine = addString(machine); Is this needed? Will need edit to addString if so
	parseCoco=parseFile(fileName, errList);

	if (parseCoco < 0)
	{
		errList.append("DataPool::parseDocData - parseFile method not successful");
		return -1;
	}

	sortRackList();
	
	return 0;
}

/*
**
**	FUNCTION
**		Read documentation data from file for interlocks
**
**	PARAMETERS
**		machine		- Name of machine being processed
**		fileName	- Name of file with controllable equipment data (typically content
**						of this file is extracted from ORACLE).
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**    0  - in case of success
**    -1 - in case of error
**
**	CAUTIONS
**		It must be checked BEFORE calling this method that data pool is in a state
**		when this can be done - check interlockDataLoaded variable
**
*/
int DataPool::parseInterlockData(const char *machine, const char *fileName, QStringList &errList)
{
	if (DebugCtl::isData())
	{
		qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << ": parseDocData(): start";
	}
	parseType = ParseInterlockData;
	parseCoco = 0;

	//this->machine = addString(machine); Is this needed? Will need edit to addString if so
	parseCoco = parseFile(fileName, errList);

	if (parseCoco < 0)
	{
		errList.append("DataPool::parseDocData - parseFile method not successful");
		return -1;
	}

	return 0;
}

int DataPool::addGlobalDevice(const char *dpName, const char *dpeName, const char *visibleName, int funcType, QString &errMsg)
{
	if(findEqpByDpName(dpName))
	{
		errMsg = "DP <";
		errMsg += dpName;
		errMsg += "> already exists";
		return -1;
	}
	Eqp *pEqp = NULL;
	switch(funcType)
	{
	case FunctionalType::SMS_MACHINE_MODE:
		pEqp = new EqpSMSMachineMode(dpName, visibleName, dpeName);
		break;
	default:
		errMsg = "Unsupported functional type ";
		errMsg += QString::number(funcType);
		errMsg += " for addGlobalDevice";
		return 0;
	}
	ctlList.append(pEqp);
	eqpDict.insert(dpName, pEqp);
	return 1;
}

/*
**
**	FUNCTION
**		Build name fo file from pathName, machineName and suffix according to the
**		following rule:
**		fileName = pathName + machineName + suffix
**
**	PARAMETERS
**		pathName		- Name of path where all required files shall be located
**		machineName		- Name of machine for which data are read.
**		suffix			- Suffix to be added to file name.
**
**	RETURNS
**		Pointer to resulting file name
**
** CAUTIONS
**		Method returns pointer to static buffer that will be overwritten by next call.
**		Caller shall NOT free memory allocated for result.
*/
char *DataPool::buildFileName(const char *pathName, const char *machineName, const char *suffix)
{
	QString newName(pathName);
	if(!(newName.endsWith('\\') || newName.endsWith('/')))
	{
		newName += "\\";
	}
	newName += machineName;
	newName += suffix;
	return pathToSystem(newName.toLatin1().constData());
}

/*
**
**	FUNCTION
**		Add file name where error was detected to error message
**
**	PARAMETERS
**		errList		- Variable where error message is added.
**		pFileName	- Pointer to name of file where error was detected
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DataPool::addFileNameToMsg(QStringList &errList, const char *pFileName)
{
	QString	extra("Error(s) in file ");
	extra += pFileName;
	extra += ":\n";
	errList.prepend(extra);
}


/*
**
**	FUNCTION
**		Clear data from pool
**
**	PARAMETERS
**		clearAll	- true if all data shall be deleted;
**						false if only active data shall be deleted
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
void DataPool::clearData(bool clearAll)
{
	eqpDict.clear();
	sectorDict.clear();
	orderedEqpList.clear();

	domains.clear();
	while(!mainParts.isEmpty())
	{
		delete mainParts.takeFirst();
	}
	while(!sectors.isEmpty())
	{
		delete sectors.takeFirst();
	}
	if(clearAll)
	{
		while(!lines.isEmpty())
		{
			delete lines.takeFirst();
		}
	}
	else
	{
		for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			lines.at(lineIdx)->clearActive();
		}
	}
	actNamePool.clear();
	if(clearAll)
	{
		pasNamePool.clear();
	}
	while(!ctlList.isEmpty())
	{
		delete ctlList.takeFirst();
	}
	FunctionalType::clear();
	PlcConfig::clear();
	if(clearAll)	// Special LHC parameters
	{
		if(lhcIps)
		{
			free((void *)lhcIps);
		}
		lhcIps = NULL;
		nLhcIps = 0;
		while(!lhcArcs.isEmpty())
		{
			delete lhcArcs.takeFirst();
		}
		while(!lhcBeamLocs.isEmpty())
		{
			delete lhcBeamLocs.takeFirst();
		}
	}
}

/*
**
**	FUNCTION
**		Set name of data part to be used - data with other names will be just ignored
**		(DATA_PART attribute of beam lines in configuration file)
**
**	PARAMETERS
**		newName	- Name of data part to be used, can be NULL or empty string to reset
**
**	RETURNS
**		None
**
**	CAUTIONS
**		It is expected that method is called BEFORE InitPassive() and InitActive()
**
*/
void DataPool::setDataPart(const char *newName)
{
	if(dataPart)
	{
		free((void *)dataPart);
	}
	dataPart = NULL;
	if(!newName)
	{
		return;
	}
	if(!*newName)
	{
		return;
	}
	dataPart = (char *)malloc(strlen(newName) + 1);
#ifdef Q_OS_WIN
	strcpy_s(dataPart, strlen(newName) + 1, newName);
#else
	strcpy(dataPart, newName);
#endif
}

/*
**
**	FUNCTION
**		Make post-processing after all beam lines and passive equipment data
**		have been read from files.
**
**	PARAMETERS
**		errList	- Variable where error message shall be added in case of errors.
**
** RETURNS
**		true - in case of success;
**		false - in case of error
**
** CAUTIONS
**		None
**
*/
bool DataPool::pasPostProcess(QStringList &errList)
{
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::pasPostProcess() start\n");
#else
		printf("DataPool::pasPostProcess() start\n");
		fflush(stdout);
#endif
	}
	int lineIdx;
	if(lhcFormat)
	{
		// Order all regios by coordinate
		if(lhcIps)
		{
			qsort((void *)lhcIps, nLhcIps, sizeof(float), compareIpCoord);
		}
		qSort(lhcArcs.begin(), lhcArcs.end(), regionLessThan);
		qSort(lhcBeamLocs.begin(), lhcBeamLocs.end(), regionLessThan);
		if(lhcBeamLocs.count())
		{
			if(DebugCtl::isData())
			{
				for(int n = 0 ; n < lhcBeamLocs.count() ; n++)
				{
					LhcRegion *pReg = lhcBeamLocs.at(n);
#ifdef Q_OS_WIN
					qDebug("%d after qSort(): from %f to %f\n", n, pReg->getStart(), pReg->getEnd());
#else
					printf("%d after qSort(): from %f to %f\n", n, pReg->getStart(), pReg->getEnd());
					fflush(stdout);
#endif
				}
			}
			LhcRegion *pReg, *pPrevReg;
			for(int locIdx = 1 ; locIdx < lhcBeamLocs.count() ; locIdx++)
			{
				LhcRegion *pReg = lhcBeamLocs.at(locIdx),
						*pPrevReg = lhcBeamLocs.at(locIdx - 1);
				float coord = (float)((pPrevReg->getEnd() + pReg->getStart()) / 2.0);
				pPrevReg->setEnd(coord);
				pReg->setStart(coord);
				pPrevReg = pReg;
			}
			// The very first is a neighbour for the very last
			pPrevReg = lhcBeamLocs.last();
			pReg = lhcBeamLocs.first();
			float coord = (float)((pPrevReg->getEnd() + pReg->getStart()) / 2.0);
			pPrevReg->setEnd(coord);
			pReg->setStart(coord);
		}
#ifndef PVSS_SERVER_VERSION
		// In case of LHC - calculate (X,Y) coordinates for all equipment on circle
		// because LHC DB does not contain (X,Y) coordinates.
		for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			BeamLine *pLine = lines.at(lineIdx);
			if(pLine->isCircle())
			{
				pLine->makeXY();
				break;
			}
		}
#endif
	}
	else	// VERY specific post-processing for other machine(s)
	{
		pasPostProcessLinac2();
	}
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::pasPostProcess() 1\n");
#else
		printf("DataPool::pasPostProcess() 1\n");
		fflush(stdout);
#endif
	}

	// Finalize all beam lines
	BeamLine *pLine;
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		pLine = lines.at(lineIdx);
		if(!pLine->finish(errList))
		{
			return false;
		}
	}
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::pasPostProcess() lines finish\n");
#else
		printf("DataPool::pasPostProcess() lines finish\n");
		fflush(stdout);
#endif
	}

	// Turn and resize beam lines if necessary
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		pLine = lines.at(lineIdx);
		pLine->turn();
	}
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::pasPostProcess() turn\n");
#else
		printf("DataPool::pasPostProcess() turn\n");
		fflush(stdout);
#endif
	}
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		pLine = lines.at(lineIdx);
		pLine->resize();
	}
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::pasPostProcess() resize\n");
#else
		printf("DataPool::pasPostProcess() resize\n");
		fflush(stdout);
#endif
	}

	// Find geo limits, build ordered arrays of equipment
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		pLine = lines.at(lineIdx);
		pLine->findGeoLimits();
		pLine->buildOrderedEqp();
	}
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::pasPostProcess() geoLimits\n");
#else
		printf("DataPool::pasPostProcess() geoLimits\n");
		fflush(stdout);
#endif
	}

	// For debugging
	if(DebugCtl::isData())
	{
		#ifdef Q_OS_WIN
		FILE *pFile = NULL;
		if(fopen_s(&pFile, "D:\\InitPassive.txt", "w"))
		{
			pFile = NULL;
		}
		#else
		FILE *pFile = fopen("/user/vacin/InitPassive.txt", "w");
		#endif
		if(pFile)
		{
			dump(pFile);
			fclose(pFile);
		}
#ifdef Q_OS_WINM
		qDebug("DataPool::pasPostProcess() finish\n");
#else
		printf("DataPool::pasPostProcess() finish\n");
		fflush(stdout);
#endif
	}
	return true;
}

/*
**
**	FUNCTION
**		Make post-processing after all beam lines and passive equipment data:
**		specific for LINAC2: join parts of 3 tanks to 3 single tanks.
**		The task is so specific that everything is hardcoded
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void DataPool::pasPostProcessLinac2(void)
{
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		QList<BeamLinePart *> &partList = pLine->getParts();
		for(int partIdx = 0 ; partIdx < partList.count() ; partIdx++)
		{
			BeamLinePart *pPart = partList.at(partIdx);
			if(!strcmp(pPart->getName(), "LINAC2"))
			{
				pPart->processLinac2();
			}
		}
	}
}

/*
**	FUNCTION
**		Correct location of equipment on sector borders on QRL and CYRO lines
**		(LHC only). Central LHC DB contains coordinates for sector start/end,
**		and in some cases coordinates of equipment located at sector borders
**		is slightly different from coordinates of sector borders themselves.
**		This minor difference may produce major problems in further processing,
**		so it's easier to make a minor correction to coordinates.
**		There is no such problem on beam vacuums because sector border coordinates
**		on beam vacuum are taken from coordinates of valves.
**
**	EXTERNAL
**
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None.
**
**	CAUTIONS
**		None.
*/
#ifndef PVSS_SERVER_VERSION
void DataPool::correctSectorBorders(void)
{
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		if(!pLine->isCircle())
		{
			continue;
		}
		QList<BeamLinePart *> &partList = pLine->getParts();
		for(int partIdx = 0 ; partIdx < partList.count() ; partIdx++)
		{
			BeamLinePart *pPart = partList.at(partIdx);
			QList<Eqp *> &eqpList = pPart->getEqpList();
			for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
			{
				Eqp *pEqp = eqpList.at(eqpIdx);
				if(pEqp->getType() != EqpType::VacDevice)
				{
					continue;
				}
				if((pEqp->getVacType() != VacType::Qrl) && (pEqp->getVacType() != VacType::Cryo))
				{
					continue;
				}
				if(!pEqp->isSectorBorder())
				{
					continue;	// Not sector border
				}
				float coord = (float)((pEqp->getStart() +
					pEqp->getSectorBefore()->getEnd() +
					pEqp->getSectorAfter()->getStart()) / 3.0);
				pEqp->setStart(coord);
				pEqp->getSectorBefore()->setEnd(coord);
				pEqp->getSectorAfter()->setStart(coord);
			}
		}
	}
}
#endif

/*
**
**	FUNCTION
**		Make post-processing after all controllable equipment and sectors
**		have been read from files.
**
**	PARAMETERS
**		errList	- Variable where error message shall be added in case of errors.
**
**	RETURNS
**		true - in case of success;
**		false - in case of error
**
**	CAUTIONS
**		None
**
*/
bool DataPool::actPostProcess(QStringList &errList)
{
	int lineIdx;
	BeamLine *pLine;
	// Build ordered arrays of equipment
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		lines.at(lineIdx)->buildOrderedEqp();
	}

#ifndef PVSS_SERVER_VERSION
	// Build ordered sectors
	if(SectorOrder::orderSectors(errList) < 0)
	{
		return false;
	}

	// For LHC: assign start/end coordinates and equipment for beam vacuum sectors
	if(lhcFormat)
	{
		buildBeamSectorBorders();
	}
#endif

	setExtraSectors();	// L.Kopylov 11.03.2013

	// Build list of devices in main parts and sectors
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		pLine = lines.at(lineIdx);
		QList<BeamLinePart *> &partList = pLine->getParts();
		for(int partIdx = 0 ; partIdx < partList.count() ; partIdx++)
		{
			BeamLinePart *pPart = partList.at(partIdx);
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			for(int n = 0 ; n < nEqpPtrs ; n++)
			{
				Eqp *pEqp =eqpPtrs[n];
				if(pEqp->getSectorBefore())
				{
					pEqp->getSectorBefore()->addEqp(pEqp);
				}
				if(pEqp->getSectorAfter())
				{
					pEqp->getSectorAfter()->addEqp(pEqp);
				}
				if(pEqp->getMainPart())
				{
					pEqp->getMainPart()->addEqp(pEqp);
				}
				// 11.03.2013 L.Kopylov: use extra sectors of CRYO thermometers
				const QList<Sector *> &extraSectors = pEqp->getExtraSectors();
				for(int extraIdx = 0 ; extraIdx < extraSectors.count() ; extraIdx++)
				{
					Sector *pExtraSector = extraSectors.at(extraIdx);
					pExtraSector->addEqp(pEqp);
					// qDebug("Add %s to sector %s\n", pEqp->getName(), pExtraSector->getName());
				}
			}
		}
	}
	for(int ctlIdx = 0 ; ctlIdx < ctlList.count() ; ctlIdx++)
	{
		Eqp *pEqp = ctlList.at(ctlIdx);
		if(pEqp->getSectorBefore())
		{
			pEqp->getSectorBefore()->addEqp(pEqp);
		}
		if(pEqp->getSectorAfter())
		{
			pEqp->getSectorAfter()->addEqp(pEqp);
		}
		if(pEqp->getMainPart())
		{
			pEqp->getMainPart()->addEqp(pEqp);
		}
	}

	return true;
}

// L.Kopylov 11.03.2013
// Set extra reference to beam sectors for CRYO thermometers - only works for LHC
void DataPool::setExtraSectors(void)
{
	if(lhcFormat)
	{
		setExtraSectorsLHC();
	}
	else
	{
		setExtraSectorsNonLHC();
	}
}

void DataPool::setExtraSectorsNonLHC(void)
{
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		QList<SectorDrawPart *> &drawParts = pSector->getDrawParts();
		for(int drawPartIdx = 0 ; drawPartIdx < drawParts.count() ; drawPartIdx++)
		{
			SectorDrawPart *pDrawPart = drawParts.at(drawPartIdx);
			BeamLine *pLine = pDrawPart->getLine();
			QList<BeamLinePart *> &lineParts = pLine->getParts();
			int partIdx = 0;
			BeamLinePart *pPart = NULL;
			for(partIdx = 0 ; partIdx < lineParts.count() ; partIdx++)
			{
				pPart = lineParts.at(partIdx);
				if(pPart == pDrawPart->getStartPart())
				{
					break;
				}
			}
			int eqpIdx = pDrawPart->getStartEqpIdx();
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			while(true)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				switch(pEqp->getFunctionalType())
				{
				case FunctionalType::VRJ_TC:
				case FunctionalType::VITE:
					pEqp->addExtraSector(pSector);
					//qDebug("add extra sector %s for %s\n", pSector->getName(), pEqp->getDpName());
					break;
				default:
					break;
				}

				if((pPart == pDrawPart->getEndPart()) && (eqpIdx == pDrawPart->getEndEqpIdx()))
				{
					break;
				}
				eqpIdx++;
				if(eqpIdx >= nEqpPtrs)
				{
					eqpIdx = 0;
					partIdx++;
					if(partIdx >= lineParts.count())
					{
						if(pLine->isCircle())
						{
							partIdx = 0;
						}
						else
						{
							break;
						}
					}
					pPart = lineParts.at(partIdx);
					eqpPtrs = pPart->getEqpPtrs();
					nEqpPtrs = pPart->getNEqpPtrs();
				}
			}
		}
	}
}

void DataPool::setExtraSectorsLHC(void)
{
	BeamLine *pLine = getCircularLine();
	if(!pLine)
	{
		return;
	}
	BeamLinePart *pPart = pLine->getParts().at(0);
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();

	// Analyze all beam sectors
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		switch(pSector->getVacType())
		{
		case VacType::BlueBeam:
		case VacType::RedBeam:
		case VacType::CommonBeam:
		case VacType::CrossBeam:
			break;
		default:
			continue;
		}
		Eqp *pStartEqp = NULL, *pEndEqp = NULL;

		// Select start device of sector
		if(pSector->getCrossStartEqp())
		{
			pStartEqp = pSector->getCrossStartEqp();
		}
		else
		{
			if(pSector->getRedStartEqp())
			{
				pStartEqp = pSector->getRedStartEqp();
			}
			if(pSector->getBlueStartEqp())
			{
				if(pStartEqp)
				{
					if(pSector->getBlueStartEqp()->getStart() < pStartEqp->getStart())
					{
						pStartEqp = pSector->getBlueStartEqp();
					}
				}
				else
				{
					pStartEqp = pSector->getBlueStartEqp();
				}
			}
		}
		if(!pStartEqp)
		{
			//qDebug("No start eqp for %s\n", pSector->getName());
			continue;
		}

		// Select end device of sector
		if(pSector->getCrossEndEqp())
		{
			pEndEqp = pSector->getCrossEndEqp();
		}
		else
		{
			if(pSector->getRedEndEqp())
			{
				pEndEqp = pSector->getRedEndEqp();
			}
			if(pSector->getBlueEndEqp())
			{
				if(pEndEqp)
				{
					if(pSector->getBlueEndEqp()->getStart() < pEndEqp->getStart())
					{
						pEndEqp = pSector->getBlueEndEqp();
					}
				}
				else
				{
					pEndEqp = pSector->getBlueEndEqp();
				}
			}
		}
		if(!pEndEqp)
		{
			//qDebug("No end eqp for %s, start is %s\n", pSector->getName(), pStartEqp->getName());
			continue;
		}
		//qDebug("%s from %s to %s\n", pSector->getName(), pStartEqp->getName(), pEndEqp->getName());

		// Process thermometers in sector
		int startEqpIdx = 0;
		for(startEqpIdx = 0 ; startEqpIdx < nEqpPtrs ; startEqpIdx++)
		{
			if(eqpPtrs[startEqpIdx] == pStartEqp)
			{
				break;
			}
		}
		if(startEqpIdx >= nEqpPtrs)
		{
			break;
		}
		int eqpIdx = startEqpIdx;
		while(true)
		{
			eqpIdx++;
			if(eqpIdx >= nEqpPtrs)
			{
				eqpIdx = 0;
			}
			if(eqpIdx == startEqpIdx)
			{
				break;
			}
			Eqp *pEqp = eqpPtrs[eqpIdx];
			if(pEqp == pEndEqp)
			{
				break;
			}
			if(pEqp->isCryoThermometer())
			{
				pEqp->addExtraSector(pSector);
			}
			// Add also for VRJ_TC and VITE
			switch(pEqp->getFunctionalType())
			{
			case FunctionalType::VRJ_TC:
			case FunctionalType::VITE:
				pEqp->addExtraSector(pSector);
				break;
			}
		}
	}
	/*
	// 'Current' beam sectors, maximum - 2 sectors
	Sector	*pSector1 = NULL,
			*pSector2 = NULL;

	// Start from position 0 - moddle of IP1, middle of some *.X sector, search in backward direction
	for(eqpIdx = nEqpPtrs - 1 ; eqpIdx >= 0 ; eqpIdx--)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		switch(pEqp->getVacType())
		{
		case VacType::BlueBeam:
		case VacType::RedBeam:
		case VacType::CommonBeam:
		case VacType::CrossBeam:
			if(pEqp->getSectorAfter())
			{
				pSector1 = pEqp->getSectorAfter();
				startEqpIdx = eqpIdx;
			}
			break;
		default:
			break;
		}
		if(pSector1)
		{
			break;
		}
	}
	if(!pSector1)
	{
		return;
	}

	// Pass all devices
	while(true)
	{
		eqpIdx++;
		if(eqpIdx >= nEqpPtrs)
		{
			eqpIdx = 0;
		}
		if(eqpIdx == startEqpIdx)
		{
			break;
		}
		Eqp *pEqp = eqpPtrs[eqpIdx];
		switch(pEqp->getVacType())
		{
		case VacType::BlueBeam:
		case VacType::RedBeam:
		case VacType::CommonBeam:
		case VacType::CrossBeam:
			if(pEqp->isSectorBorder())
			{
				if(pEqp->getSectorBefore() == pSector1)
				{
					pSector1 = pEqp->getSectorAfter();
				}
				else if(pEqp->getSectorBefore() == pSector2)
				{
					pSector2 = pEqp->getSectorAfter();
				}
				else
				{
					if(pSector1 == pEqp->getSectorAfter())
					{
						pSector2 = pEqp->getSectorAfter();
					}
					else if(pSector2 == pEqp->getSectorAfter())
					{
						pSector1 = pEqp->getSectorAfter();
					}
					else
					{
						pSector1 = pEqp->getSectorAfter();
					}
				}
				qDebug("after %s: sector1 %s sector2 %s\n", pEqp->getName(), (pSector1 ? pSector1->getName() : "null"), (pSector2 ? pSector2->getName() : "null"));
			}
			break;
		default:
			if(pEqp->isCryoThermometer())
			{
				if(pSector1)
				{
					pEqp->addExtraSector(pSector1);
					//qDebug("Add extra sector 1 %s for %s\n", pSector1->getName(), pEqp->getName());
				}
				if(pSector2 && (pSector2 != pSector1))
				{
					pEqp->addExtraSector(pSector2);
					//qDebug("Add extra sector 2 %s for %s\n", pSector2->getName(), pEqp->getName());
				}
			}
			break;
		}
	}
	*/
}

/*
**	FUNCTION
**		Build locations for start(s) and end(s) of every beam vacuum
**		sector on 3 lines: RED, BLUE, CROSS. Only sectors on main LHC
**		ring are processed, sectors on injection/dump lines are processed
**		in another way.
**
**	EXTERNAL
**
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None.
**
**	CAUTIONS
**		None.
*/
#ifndef PVSS_SERVER_VERSION
void DataPool::buildBeamSectorBorders(void)
{
	// Step 1 - assign coordinates from devices at sector borders
	BeamLine *pLine = getCircularLine();
	if(pLine)
	{
		pLine->buildBeamSectorBorders();
	}

	// Pass 2: for all sectors having at least one part of LHC ring vacuum:
	// 1) Assign isRedOut flag if at least one part is located on either RED or BLUE beam
	//	(the flag is not important for parts located on CROSS/COMMON beams)
	// 2) Assign missing start or end coordinates. They can be missing if, for example,
	//	  part of sector on RED beam starts at valve and continues till end of RED beam
	//	  ( i.e. when RED turns to CROSS)
	for(int idx = 0 ; idx < sectors.count() ; idx++)
	{
		sectors.at(idx)->finishBeamBorders(pLine);
	}
}
#endif

/*
**	FUNCTION
**		Build auxilliary equipment which does no exist in input file:
**		main part access control, sector VRPI summary etc.
**		
**	PARAMETERS
**		None
**
**	RETURNS
**		None.
**
**	CAUTIONS
**		None.
*/
void DataPool::buildAuxEquipment(void)
{
	// Main part access modes
	for(int mpIdx = 0 ; mpIdx < mainParts.count() ; mpIdx++)
	{
		ctlList.append(new EqpMP_ACCESS(mainParts.at(mpIdx)));
	}

	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::buildAuxEquipment(): EqpMP_ACCESS done\n");
#else
		printf("DataPool::buildAuxEquipment(): EqpMP_ACCESS done\n");
		fflush(stdout);
#endif
	}

	// Summary of VPIs in one sector
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		QString dpName = EqpSECT_VPI_SUM::getDpNameForSector(pSector);
		if(!dpName.isEmpty())
		{
			ctlList.append(new EqpSECT_VPI_SUM(pSector));
		}
	}
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("DataPool::buildAuxEquipment(): EqpSECT_VPI_SUM done\n");
#else
		printf("DataPool::buildAuxEquipment(): EqpSECT_VPI_SUM done\n");
		fflush(stdout);
#endif
	}
}

/*
**	FUNCTION
**		Build dictionary for fast search of equipment by DP name.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None.
**
**	CAUTIONS
**		None.
*/
void DataPool::buildDpNameDictionary(void)
{
	eqpDict.clear();

	// Fill in dictionary
	for(int ctlIdx = 0 ; ctlIdx < ctlList.count() ; ctlIdx++)
	{
		Eqp *pEqp = ctlList.at(ctlIdx);
		eqpDict.insert(pEqp->getDpName(), pEqp);
	}
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		QList<BeamLinePart *> &partList = pLine->getParts();
		for(int partIdx = 0 ; partIdx < partList.count() ; partIdx++)
		{
			BeamLinePart *pPart = partList.at(partIdx);
			QList<Eqp *> eqpList = pPart->getEqpList();
			for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
			{
				Eqp *pEqp = eqpList.at(eqpIdx);
				if(pEqp->getDpName())
				{
					eqpDict.insert(pEqp->getDpName(), pEqp);
				}
			}
		}
	}
}

/*
**	FUNCTION
**		Build list of equipment ordered according to geography
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None.
**
**	CAUTIONS
**		None.
*/
void DataPool::buildOrderedEqpList(void)
{
	orderedEqpList.clear();

	// Equipment is ordered according to order of sectors
	QList<BeamLine *>::ConstIterator lineIter;

	// Clean up equipment with physical location
	for(lineIter = lines.constBegin() ; lineIter != lines.constEnd() ; ++lineIter)
	{
		BeamLine *pLine = *lineIter;
		QList<BeamLinePart *> &parts = pLine->getParts();
		QList<BeamLinePart *>::ConstIterator partIter;
		for(partIter = parts.constBegin() ; partIter != parts.constEnd() ; ++partIter)
		{
			BeamLinePart *pPart = *partIter;
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if(!pEqp->getFunctionalType())
				{
					pEqp->setOrdered(true);
					continue;
				}
				if(pEqp->getSectorBefore() || pEqp->getSectorAfter())
				{
				}
				else
				{
					pEqp->setOrdered(true);
				}
			}
		}
	}

	// Clean up controllers
	QList<Eqp *>::ConstIterator ctlIter;
	for(ctlIter = ctlList.constBegin() ; ctlIter != ctlList.constEnd() ; ++ctlIter)
	{
		Eqp *pEqp = *ctlIter;
		if(pEqp->getSectorBefore() || pEqp->getSectorAfter() || pEqp->getMainPart())
		{
		}
		else
		{
			pEqp->setOrdered(true);
		}
	}

	// Build the list
	QList<Sector *>::ConstIterator sectIter;
	for(sectIter = sectors.constBegin() ; sectIter != sectors.constEnd() ; ++ sectIter)
	{
		Sector *pSector = *sectIter;
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();

		// Equipment with physical location
		for(lineIter = lines.constBegin() ; lineIter != lines.constEnd() ; ++lineIter)
		{
			BeamLine *pLine = *lineIter;
			QList<BeamLinePart *> &parts = pLine->getParts();
			QList<BeamLinePart *>::ConstIterator partIter;
			for(partIter = parts.constBegin() ; partIter != parts.constEnd() ; ++partIter)
			{
				BeamLinePart *pPart = *partIter;
				Eqp **eqpPtrs = pPart->getEqpPtrs();
				int nEqpPtrs = pPart->getNEqpPtrs();
				for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(pEqp->isOrdered())
					{
						continue;
					}
					if((pEqp->getSectorBefore() == pSector) ||
						(pEqp->getSectorAfter() == pSector))
					{
						orderedEqpList.append(pEqp);
						pEqp->setOrdered(true);
					}
				}
			}
		}

		// Controllers
		for(ctlIter = ctlList.constBegin() ; ctlIter != ctlList.constEnd() ; ++ctlIter)
		{
			Eqp *pEqp = *ctlIter;
			if(pEqp->isOrdered())
			{
				continue;
			}
			if(pEqp->getSectorBefore() || pEqp->getSectorAfter())
			{
				if((pEqp->getSectorBefore() == pSector) ||
					(pEqp->getSectorAfter() == pSector))
				{
					orderedEqpList.append(pEqp);
					pEqp->setOrdered(true);
				}
			}
			else if(pEqp->getMainPart())
			{
				QList<MainPartMap *>::ConstIterator mapIter;
				for(mapIter = maps.constBegin() ; mapIter != maps.constEnd() ; ++mapIter)
				{
					MainPartMap *pMap = *mapIter;
					if(pMap->getMainPart() == pEqp->getMainPart())
					{
						orderedEqpList.append(pEqp);
						pEqp->setOrdered(true);
					}
				}
			}
		}
	}
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int DataPool::processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
	const QString & /* longValue */, QStringList &errList)
{
	int	coco = 0;

	switch(parseType)
	{
	case ParseLineConfig:
		coco = processConfigTokens(pFile, lineNo, tokens, nTokens, errList);
		break;
	case ParsePassive:
		coco = processEqpTokens(pFile, lineNo, tokens, nTokens, errList);
		break;
	case ParseActive:
		coco = processActTokens(pFile, lineNo, tokens, nTokens, errList);
		break;
	case ParseLhcGlobal:
		coco = processLhcGlobalTokens(pFile, lineNo, tokens, nTokens, errList);
		break;
	case ParseRackData:
		coco = processRackDataTokens(pFile, lineNo, tokens, nTokens, errList);
		// for documentation data, missing data for one equipment should not cancel the entire parsing
		return 1;
	case ParseInterlockData:
		coco = processInterlockDataTokens(pFile, lineNo, tokens, nTokens, errList);
		// for documentation data, missing data for one equipment should not cancel the entire parsing
		return 1;
	}
	return coco;
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file - beam lines configuration
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int DataPool::processConfigTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
	QStringList &errList)
{
	if(!strcasecmp(tokens[0], "ColdDipole"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processConfigTokens");
		FunctionalType::addType(FunctionalType::ColdDipole, tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "ColdQuadrupole"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processConfigTokens");
		FunctionalType::addType(FunctionalType::ColdQuadrupole, tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "WarmDipole"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processConfigTokens");
		FunctionalType::addType(FunctionalType::WarmDipole, tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "WarmQuadrupole"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processConfigTokens");
		FunctionalType::addType(FunctionalType::WarmQuadrupole, tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "TDICollimator"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processConfigTokens");
		FunctionalType::addType(FunctionalType::TDICollimator, tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "BEAM_LINE"))	// Beam lines configuration
	{
		CHECK_MIN_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processConfigTokens");
		bool isCircle = false;
		if(nTokens > 2)
		{
			for(int n = 2 ; n < nTokens ; n++)
			{
				if(!strcasecmp(tokens[2], "circle"))
				{
					isCircle = true;
					break;
				}
			}
		}
		BeamLine *pLine = new BeamLine(tokens[1], isCircle);
		if(nTokens > 2)
		{
			for(int n = 2 ; n < nTokens ; n++)
			{
				if(!strcasecmp(tokens[2], "virtual"))
				{
					pLine->setVirtualLine(true);
					pLine->setHidden(true);
					break;
				}
			}
		}
		lines.append(pLine);
		if((parseCoco = pLine->parseFile(pFile, lineNo, errList)) < 0)
		{
			return parseCoco;
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "MIN_VISIBLE_WIDTH"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processConfigTokens");
		if(sscanf(tokens[1], "%f", &minPassiveEqpVisibleWidth) != 1)
		{
			PARSING_ERR2("Line %d: bad format for MIN_VISIBLE_WIDTH <%s>", lineNo, tokens[1]);
		}
		if(minPassiveEqpVisibleWidth <= 0)
		{
			PARSING_ERR2("Line %d: bad value for MIN_VISIBLE_WIDTH %f - must be > 0", lineNo, minPassiveEqpVisibleWidth);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "KEEP_MANUAL_SECTOR_ORDER"))
	{
		SectorOrder::setKeepManualOrderOnCircularLines(true);
		return 1;
	}
	PARSING_ERR2("Line %d: Unexpected <%s>", lineNo, tokens[0]);
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file - passive equipment
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added in case of errors.
**
** RETURNS
**		1	- tokens have been processed successfully, more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
** CAUTIONS
**		None
*/
int DataPool::processEqpTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
	QStringList &errList)
{
	Eqp *pEqp = Eqp::createFromFile(EqpType::Passive, pFile, lineNo, tokens, nTokens, errList);
	if(!pEqp)
	{
		return -1;
	}

	// Unfortunately, it can be that the same equipment appears in more than one line.
	// Example - DSL in LHC contains part of LHC ring
	bool lineFound = false;
	for(int lineIdx = 0 ; lineIdx < lines.size() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		if(pLine->addEqp(pEqp, pureSurvey))
		{
			lineFound = true;
			pEqp = pEqp->clone();
		}
	}
	if(lineFound)
	{
		delete pEqp;	// DELETE UNUSED CLONE!!!!
		return 1;
	}
	if(pureSurvey)	// No beam lines configuration, pure survey data
	{
		BeamLine *pLine = new BeamLine(tokens[0], false);
		lines.append(pLine);
		if(pLine->addEqp(pEqp, true))
		{
			return 1;
		}
	}
	delete pEqp;
	return 1;
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file - active equipment and sectors
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added in case of errors.
**
**	RETURNS
**		1	- tokens have been processed successfully, more lines are
**				expected.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int DataPool::processActTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
	QStringList &errList)
{
	if(!strcasecmp( tokens[0], "Accelerator"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processActTokens");
		if(strcmp(machine, tokens[1]))
		{
			errList.append(QString("Line %1: Input file is generated for <%2>, expected <%3>").arg(lineNo).arg(tokens[1]).arg(machine));
			return(parseCoco = -1);
		}
		return 1;
	}
	if(!strcasecmp(tokens[0], "TimeStamp"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processActTokens");
		timeStamp = addString(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "MainPart"))
	{
		MainPart *pMainPart = MainPart::createFromFile(pFile, lineNo, tokens, nTokens, errList);
		if(!pMainPart)
		{
			return(parseCoco = -1);
		}
		if(findMainPartData(pMainPart->getName()))
		{
			errList.append(QString("Line %1: duplicating main part <%2>").arg(lineNo).arg(pMainPart->getName()));
			delete pMainPart;
			return(parseCoco = -1);
		}
		mainParts.append(pMainPart);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Sector"))
	{
		Sector *pSect = Sector::createFromFile(pFile, lineNo, tokens, nTokens, errList);
		if(!pSect)
		{
			return(parseCoco = -1);
		}
		if(findSectorData(pSect->getName()))
		{
			errList.append(QString("Line %1: duplicating sector <%2>").arg(lineNo).arg(pSect->getName()));
			delete pSect;
			return(parseCoco = -1);
		}
		addSector(pSect);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Device"))
	{
		Eqp *pEqp = Eqp::createFromFile(EqpType::VacDevice, pFile, lineNo, tokens, nTokens, errList);
		if(!pEqp)
		{
			return(parseCoco = -1);
		}
		if(pEqp->getSurveyPart())	// Device with physical location
		{
			// Processing lines in reverse order shall prevent entering DSL equipment
			// into LHC ring line: it is expected that DSL line is AFTER LHC ring
			// line in the list of lines
			for(int lineIdx = 0 ; lineIdx < lines.size() ; lineIdx++)
			{
				BeamLine *pLine = lines.at(lineIdx);
				if(pLine->addEqp(pEqp, false))
				{
					return 1;
				}
			}
			errList.append(QString("Location for <%1> not found, device is thrown away").arg(pEqp->getName()));
			delete pEqp;
		}
		else	// Device without physical location - controller
		{
			ctlList.append(pEqp);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "PlcConfig"))
	{
		if(PlcConfig::createFromFile(pFile, lineNo, tokens, nTokens, errList) < 0)
		{
			return -1;
		}
		return 1;
	}
	parseCoco = -1;
	PARSING_ERR2("Line %d: unexpected <%s>", lineNo, tokens[0]);
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file - LHC global configuration
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int DataPool::processLhcGlobalTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	QStringList &errList)
{
	int	regionType = LhcRegion::None;

	if(!strcasecmp(tokens[0], "LENGTH"))		// Length of LHC ring
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processLhcGlobalTokens");
		if(sscanf(tokens[1], "%f", &lhcRingLength) != 1)
		{
			PARSING_ERR2("Line %d: bad format for LENGTH <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "IP"))	// Location of IP
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processLhcGlobalTokens");
		float	start;
		if(sscanf(tokens[1], "%f", &start) != 1)
		{
			PARSING_ERR2("Line %d: bad format for IP location <%s>", lineNo, tokens[1]);
		}
		if(start < 0.0)
		{
			PARSING_ERR2("Line %d: bad value for IP %f - must be >= 0", lineNo, start);
		}
		lhcIps = (float *)realloc((void *)lhcIps, (nLhcIps + 1) * sizeof(float));
		lhcIps[nLhcIps++] = start;
		return 1;
	}
	else if(!strcasecmp(tokens[0], "ARC"))	// Limits for ARC
	{
		CHECK_TOKEN_COUNT(nTokens, 3, lineNo, "DataPool::processLhcGlobalTokens");
		float start;
		if(sscanf(tokens[1], "%f", &start) != 1)
		{
			PARSING_ERR2("Line %d: bad format for ARC start <%s>", lineNo, tokens[1]);
		}
		if(start < 0.0)
		{
			PARSING_ERR2("Line %d: bad value for ARC start %f - must be >= 0", lineNo, start);
		}
		float end;
		if(sscanf(tokens[2], "%f", &end) != 1)
		{
			PARSING_ERR2("Line %d: bad format for ARC end <%s>", lineNo, tokens[2]);
		}
		if(end < 0.0)
		{
			PARSING_ERR2("Line %d: bad value for ARC end %f - must be >= 0", lineNo, end);
		}
		LhcRegion *pRegion = new LhcRegion(start, end, LhcRegion::None);
		lhcArcs.append(pRegion);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "BLUE_OUT"))	// Limits for region with BLUE beam outer
	{
		regionType = LhcRegion::BlueOut;
	}
	else if(!strcasecmp(tokens[0], "RED_OUT"))	// Limits for region with RED beam outer
	{
		regionType = LhcRegion::RedOut;
	}
	else if(!strcasecmp(tokens[0], "CROSS"))		// Limits for region with CROSS beam
	{
		regionType = LhcRegion::Cross;
	}
	if(regionType != LhcRegion::None)
	{
		CHECK_TOKEN_COUNT(nTokens, 3, lineNo, "DataPool::processLhcGlobalTokens");
		float start;
		if(sscanf(tokens[1], "%f", &start) != 1)
		{
			PARSING_ERR3("Line %d: bad format for %s start <%s>", lineNo, tokens[0], tokens[1]);
		}
		if(start < 0.0)
		{
			PARSING_ERR3("Line %d: bad value for %s start %f - must be >= 0", lineNo, tokens[0], start);
		}
		float end;
		if(sscanf(tokens[2], "%f", &end) != 1)
		{
			PARSING_ERR3("Line %d: bad format for %s end <%s>", lineNo, tokens[0], tokens[2]);
		}
		if(end < 0.0)
		{
			PARSING_ERR3("Line %d: bad value for %s end %f - must be >= 0", lineNo, tokens[0], end);
		}
		// Check if new region is immediate neighbour of existing region - situation
		// may occur due to errors/incompleteness of LHCLAYOUT DB
		bool found = false;
		for(int regIdx = lhcBeamLocs.size() - 1 ; regIdx >= 0 ; regIdx--)
		{
			LhcRegion *pRegion = lhcBeamLocs.at(regIdx);
			if(pRegion->getType() != regionType)
			{
				continue;
			}
			if(fabs(pRegion->getStart() - end) < 1.0)
			{
				pRegion->setStart(start);
				found = true;
				break;
			}
			if(fabs(pRegion->getEnd() - start) < 1.0)
			{
				pRegion->setEnd(end);
				found = true;
				break;
			}
		}
		if(!found)
		{
			if(DebugCtl::isData())
			{
#ifdef Q_OS_WIN
				qDebug("Adding region of type %d from %f to %f\n", regionType, start, end);
#else
				printf("Adding region of type %d from %f to %f\n", regionType, start, end);
				fflush(stdout);
#endif
			}
			lhcBeamLocs.append(new LhcRegion(start, end, regionType));
		}
		return 1;
	}
	PARSING_ERR2("Line %d: Unexpected <%s>", lineNo, tokens[0]);
}

int DataPool::processRackDataTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
	QStringList &errList)
{
	if (!strcasecmp(tokens[0], "EqpName"))
	{
		RackEqp * newEqp = new RackEqp();
		Rack * rack = NULL; //rack where equipment will be added
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processDocDataTokens");

		newEqp->setName(QString(tokens[1]));

		while (nextFileLine(pFile, lineNo))
		{
			int nTokensLoop = getNumberOfTokens();
			
			QString identifier = getCurrentToken(0);
			if (QString::compare(identifier, "Rack") == 0)
			{
				if (nTokensLoop == 3)
				{
					QString value = getCurrentToken(1) + "=" + getCurrentToken(2);
					int rackIndex = findRackByName(value);

					if (rackIndex >= 0)
						rack = rackList.at(rackIndex);
					else
					{
						rack = new Rack();
						rack->setName(value);
						rackList.append(rack);
					}
				}
				else
				{
					QString error = QString("Error parsing data doc at line ") + lineNo + " with token " + identifier;
					errList.append(error);
				}
			}
			else if (QString::compare(identifier, "EqpType") == 0)
			{
				if (nTokensLoop == 2)
				{
					QString value = getCurrentToken(1);
				
					newEqp->setType(value);
				}
				else
				{
					QString error = QString("Error parsing data doc at line ") + lineNo + " with token " + identifier;
					errList.append(error);
				}
			}
			else if (QString::compare(identifier, "PositionX")==0)
			{
				if (nTokensLoop == 2)
				{
					QString value = getCurrentToken(1);
					bool ok;
					int positionX = value.toInt(&ok);

					if (!ok)
					{
						PARSING_ERR3("Line %d: bad format for %s start <%s>", lineNo, identifier.toUtf8().constData(), getCurrentToken(1).toUtf8().constData());
					}
					newEqp->setPositionX(positionX);
				}
				else
				{
					QString error = QString("Error parsing data doc at line ") + lineNo + " with token " + identifier;
					errList.append(error);
				}
			}
			else if (QString::compare(identifier, "PositionY") == 0)
			{
				if (nTokensLoop == 2)
				{
					QString value = getCurrentToken(1);
					bool ok;
					int positionY = value.toInt(&ok);

					if (!ok)
					{
						PARSING_ERR("Format error at line %d while parsing argument PositionY", lineNo);
					}
					newEqp->setPositionY(positionY);
				}
				else
				{
					QString error = QString("Error parsing data doc at line ") + lineNo + " with token " + identifier;
					errList.append(error);
				}
			}
			else if (QString::compare(identifier, "SizeX") == 0)
			{
				if (nTokensLoop == 2)
				{
					QString value = getCurrentToken(1);
					bool ok;
					float sizeX = value.toFloat(&ok);

					if (!ok)
					{
						errList.append(value);
						PARSING_ERR("Format error at line %d while parsing argument sizeX", lineNo);
					}
					newEqp->setSizeX(sizeX);
				}
				else
				{
					QString error = QString("Error parsing data doc at line ") + lineNo + " with token " + identifier;
					errList.append(error);
				}
			}
			else if (QString::compare(identifier, "SizeY") == 0)
			{
				if (nTokensLoop == 2)
				{

					QString value = getCurrentToken(1);
					bool ok;
					float sizeY = value.toFloat(&ok);

					if (!ok)
					{
						PARSING_ERR("Format error at line %d while parsing argument sizeY", lineNo);
					}
					newEqp->setSizeY(sizeY);
				}
				else
				{
					QString error = QString("Error parsing data doc at line ") + lineNo + " with token " + identifier;
					errList.append(error);
				}
			}
			else if (QString::compare(identifier, "EndEqp") == 0)
			{
				break;
			}
			else
			{
				if (nTokensLoop == 2)
				{

					QString value = getCurrentToken(1);

					newEqp->addAttribute(identifier, value);
				}
			}
		}

		if (rack == NULL)
		{
			errList.append("No Rack found for equipment " + newEqp->getName());
			return -1;
		}

		if (newEqp->isMinimumInformationSet())
		{
			Eqp * pEqp = findEqpByVisibleName(newEqp->getName().toUtf8().data());
			newEqp->setEqpPointer(pEqp);
			
			rack->addEquipment(newEqp);
			return 1;
		}
		else
		{
			errList.append("Equipment does not have all necessary attributes. Parsing at line " + lineNo);
			return -1;
		}
	}
	QString error = QString("Error parsing data doc at line ") + lineNo + ". Expected start of eqp, found " + tokens[0];
	errList.append(error);
}


int DataPool::processInterlockDataTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
	QStringList &errList)
{

	if (!strcasecmp(tokens[0], "StartChain"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "DataPool::processDocDataTokens");

		InterlockChain * pChain = new InterlockChain(QString(tokens[1]));
		QList<InterlockConnection> listOfConnections;
		//Loop inside one chain
		while (nextFileLine(pFile, lineNo))
		{
			int nTokensLoop = getNumberOfTokens();

			QString identifier = getCurrentToken(0);
			
			if (QString::compare(identifier, "EqpName") == 0)
			{
				if (nTokensLoop == 2)
				{
					listOfConnections.clear();
					InterlockEqp * pEqp = new InterlockEqp(QString(getCurrentToken(1)));

					// Loop inside one equipment
					while (nextFileLine(pFile, lineNo))
					{
						int nTokensLoopEqp = getNumberOfTokens();

						QString identifierEqp = getCurrentToken(0);
						if (QString::compare(identifierEqp, "Parent") == 0)
						{
							InterlockConnection connection;
							connection.childEquipment = pEqp;
							if (nTokensLoopEqp == 2)
							{
								QString parentName = getCurrentToken(1);
								InterlockEqp *  pParent = pChain->getEquipmentPtr(parentName);
								connection.parentEquipment = pParent;
							}
							else
							{
								QString error = QString("Error parsing interlock doc at line ") + lineNo + " with token " + identifierEqp;
								errList.append(error);
								break;
							}

							//get rest of details
							nextFileLine(pFile, lineNo);
							int nTokensLoopCon = getNumberOfTokens();
							QString identifierCon = getCurrentToken(0);

							if (QString::compare(identifierCon, "ConnectedOn") == 0)
							{
								if (nTokensLoopCon == 2)
								{
									connection.childConnector = getCurrentToken(1);
								}
								else
								{
									QString error = QString("Error parsing interlock doc at line ") + lineNo + " with token " + identifierCon;
									errList.append(error);
									break;
								}
							}
							else
							{
								QString error = QString("Error parsing interlocks at line ") + lineNo + ". Expected 'ConnectedOn but found" + identifierCon;
								errList.append(error);
								break;
							}

							nextFileLine(pFile, lineNo);
							nTokensLoopCon = getNumberOfTokens();
							identifierCon = getCurrentToken(0);

							if (QString::compare(identifierCon, "ConnectedTo") == 0)
							{
								if (nTokensLoopCon == 2)
								{
									connection.parentConnector = getCurrentToken(1);
								}
								else
								{
									QString error = QString("Error parsing interlock doc at line ") + lineNo + " with token " + identifierCon;
									errList.append(error);
									break;
								}
							}
							else
							{
								QString error = QString("Error parsing interlocks at line ") + lineNo + ". Expected 'ConnectedTo but found" + identifierCon;
								errList.append(error);
								break;
							}

							nextFileLine(pFile, lineNo);
							nTokensLoopCon = getNumberOfTokens();
							identifierCon = getCurrentToken(0);

							if (QString::compare(identifierCon, "Cable") == 0)
							{
								if (nTokensLoopCon == 2)
								{
									connection.cable = getCurrentToken(1);
								}
								else
								{
									connection.cable = "";
								}
							}
							else
							{
								QString error = QString("Error parsing interlocks at line ") + lineNo + ". Expected 'Cable but found" + identifierCon;
								errList.append(error);
								break;
							}

							if (connection.parentEquipment != NULL && connection.childEquipment != NULL)
							{
								listOfConnections.append(connection);
							}
							else
							{
								QString error = QString("Error parsing interlocks at line ") + lineNo + ". Connection with non-existant equipment attempted";
								errList.append(error);
								break;
							}
						}
						else if (QString::compare(identifierEqp, "EndEqp") == 0)
						{
							pChain->addEquipemnt(pEqp);
							for (int i = 0; i < listOfConnections.length(); i++)
							{
								pChain->addConnection(listOfConnections.at(i));
							}
							listOfConnections.clear();
							break;
						}
						else
						{
							if (nTokensLoopEqp == 2)
							{
								pEqp->addAttribute(getCurrentToken(0), getCurrentToken(1));
							}
							else if (nTokensLoopEqp == 1)
							{
								pEqp->addAttribute(getCurrentToken(0), "");
							}
							else
							{
								QString error = QString("Error parsing interlocks at line ") + lineNo + ". No value found.";
								errList.append(error);
								break;
							}
						}
					}
				}
				else
				{
					QString error = QString("Error parsing interlocks at line ") + lineNo + ". No value found for token.";
					errList.append(error);
				}
			}
			else if (QString::compare(identifier, "EndChain") == 0)
			{
				pChain->setVisibilityMasks();
				interlockChainList.append(pChain);
				break;
			}
		}
	}

}

///////////////////////////////////////////////////////////////////////////////////////
/////////////////// Helper methods  //////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

/*
**
**	FUNCTION
**		Add 'single-use' string to either passive or active name pool
**		depending on what is being parsed now
**
**	PARAMETERS
**		pString	- Pointer to string to be added to name pool
**
**	RETURNS
**		value returned by NamePool's addString() method.
**
**	CAUTIONS
**		None
*/
char *DataPool::addString(const char *pString)
{
	if(parseType == ParseActive)
	{
		return actNamePool.addString(pString);
	}
	return pasNamePool.addString(pString);
}

/*
**
**	FUNCTION
**		Add 'multi-use' string to either passive or active name pool
**		depending on what is being parsed now
**
**	PARAMETERS
**		pString	- Pointer to string to be added to name pool
**
**	RETURNS
**		value returned by NamePool's addMulriUseString() method.
**
** CAUTIONS
**		None
*/
char *DataPool::addMultiUseString(const char *pString)
{
	if(parseType == ParseActive)
	{
		return actNamePool.addMultiUseString(pString);
	}
	return pasNamePool.addMultiUseString(pString);
}



///////////////////////////////////////////////////////////////////////////////////////
/////////////////// Search for required data from pool ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

/*
**	FUNCTION
**		Add sector to both list and dictionary of sectors
**
**	PARAMETERS
**		pSector	- Pointer to sector to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DataPool::addSector(Sector *pSector)
{
	sectors.append(pSector);
	sectorDict.insert(pSector->getName(), pSector);
}

/*
**	FUNCTION
**		Replace existing sectors with new list of sectors
**
**	PARAMETERS
**		newSectors	- New list of sectors
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DataPool::replaceSectors(QList<Sector *> &newSectors)
{
	sectors.clear();
	sectorDict.clear();
	for(int sectIdx = 0 ; sectIdx < newSectors.count() ; sectIdx++)
	{
		Sector *pSector = newSectors.at(sectIdx);
		addSector(pSector);
	}
}

/*
**	FUNCTION
**		Find vacuum region at given location on LHC ring
**
**	PARAMETERS
**		location	- Location where region is required
**
**	RETURNS
**		Pointer to region data at this location
**
**	CAUTIONS
**		In principle NULL can be returned, though I can not imagine such situation
*/
LhcRegion *DataPool::regionAtLocation(float location)
{
	for(int idx = 0 ; idx < lhcBeamLocs.size() ; idx++)
	{
		LhcRegion *pRegion = lhcBeamLocs.at(idx);
		if(pRegion->isCoordInside(location))
		{
			return pRegion;
		}
	}
	return NULL;
}

/**
@brief Return Front End equipment (PLC, TCP communication, OPC server...)
@details If Front End is PLC the problem is: active devices have reference to PLC like
	PLC=PLCM_RE12 and their is device named PLCM_RE12, but that device has NO 
	family/type/subtype. Hence, such device remain instance of 'generic' 
	Eqp class and as such it does not have connect() and other related 
	methods implemented. On the other hand, for every PLC there is another DP named 
	VPLC_A_PLCM_RE12 or VPLC_A0_PLCM_RE12 (depending on DP type of PLC - VPLC_A or VPLC_A0 or VPLC_PF)
	This method shall find for given 'pure' PLC name (like PLCM_RE12) 
	one of DPs which exists - it is not known in advance which of VPLC_A or VPLC_A0 or VPLC_PF exist.
@param[in]	dpName	'Pure' Front dp name
**/
Eqp *DataPool::findPlc(const QString &dpName)
{
	if(dpName.isEmpty())
		return NULL;
	
	// Front End (PLC) is type VPLC_A_ see details for explanation 
	QByteArray plcDpName("VPLC_A_");
	plcDpName += dpName;
	Eqp *pEqp = eqpDict.value(plcDpName);
	if(pEqp)
		return pEqp;

	// Front End (PLC) is type VPLC_A0_ see details for explanation 
	plcDpName = "VPLC_A0_";
	plcDpName += dpName;
	pEqp = eqpDict.value(plcDpName);
	if (pEqp)
		return pEqp;

	// Front End (PLC) is type VPLC_PF_ see details for explanation 
	plcDpName = "VPLC_PF_";
	plcDpName += dpName;
	pEqp = eqpDict.value(plcDpName);
	if (pEqp)
		return pEqp;

	// Other Front End type
	QByteArray feDpName("");
	feDpName += dpName;
	return eqpDict.value(feDpName);
}


/*
**
**	FUNCTION
**		Find building of the equipment or parent of equipment given as argument
**
**	PARAMETERS
**		eqpDisplayName - name as found in database
**	RETURNS
**		string - name of building
**
**	CAUTIONS
**		None
*/
QString DataPool::getRackBuildingOfEquipment(const QString &eqpDisplayName)
{
	QString buildingName;
	for (int i = 0; i < rackList.length(); i++)
	{
		if (rackList.at(i)->containsEqp(eqpDisplayName))
		{
			
			QString rackName = rackList.at(i)->getRackName();
			buildingName = rackName.right(rackName.length() - rackName.indexOf("=") - 1);
			return buildingName;
			
		}
	}
	return "";
}



/*
**
**	FUNCTION
**		Find (X,Y) coordinates of given position in raw line data by
**		interpolating coordinates of neighbour elements. Used to find
**		coordinates of line start/end in another line.
**
**	PARAMETERS
**		pName			- Name of beam line where coordinates are needed.
**		pForLine		- Name of beam line for which coordinates are needed.
**		connectVacType	- Vacuum type in parent line
**		coord			- Position of start/end in raw line.
**		isStart			- true if equipment is start of another line, false if it is finish of another line
**		resultX			- Resulting X coordinate is returned here.
**		resultY			- Resulting Y coordinate is returned here.
**
**	RETURNS
**		true if location has been found;
**		false otherwise.
**
**	CAUTIONS
**		None
*/
bool DataPool::coordInLine(char *pName, char *pForLine, int connectVacType, float coord, bool isStart,
	float &resultX, float &resultY)
{
	for(int idx = 0 ; idx < lines.size() ; idx++)
	{
		BeamLine *pLine = lines.at(idx);
		// In case of LHC other lines only start/end on LHC ring
		if(pForLine && lhcFormat)
		{
			if(!pLine->isCircle())
			{
				continue;
			}
		}
		if(pLine->coordInLine(pName, pForLine, connectVacType, coord, isStart, resultX, resultY))
		{
			return true;
		}
	}
	return false;
}

/*
**
**	FUNCTION
**		Find beam line with given name
**
**	PARAMETERS
**		pName	- Pointer to required beam line name
**
**	RETURNS
**		Pointer to BeamLine with required name;
**		NULL if such beam line is not found.
**
** CAUTIONS
**		None
*/
BeamLine *DataPool::getLine(const char *pName)
{
	for(int idx = 0 ; idx < lines.size() ; idx++)
	{
		BeamLine *pLine = lines.at(idx);
		if(!strcmp(pLine->getName(), pName))
		{
			return pLine;
		}
	}
	return NULL;
}

/*
**
**	FUNCTION
**		Find circular line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Pointer to circular BeamLine
**		NULL if such beam line is not found.
**
**	CAUTIONS
**		The method is mainly used for LHC machine where only one circular
**		line is expected
*/
BeamLine *DataPool::getCircularLine(void)
{
	for(int idx = 0 ; idx < lines.size() ; idx++)
	{
		BeamLine *pLine = lines.at(idx);
		if(pLine->isCircle())
		{
			return pLine;
		}
	}
	return NULL;
}

/*
**
**	FUNCTION
**		Find sector with given name
**
**	PARAMETERS
**		sectorName	- Name of sector to find
**
**	RETURNS
**		Pointer to Sector with given name;
**		NULL if such sector is not found.
**
**	CAUTIONS
**		None
*/
Sector *DataPool::findSectorData(const char *sectorName)
{
	return sectorDict.value(sectorName);
	/*
	QPtrListIterator<Sector> iter(sectors);
	for(Sector *pSect = iter.toFirst() ; pSect ; pSect = ++iter)
	{
		if(!strcmp(pSect->getName(), sectorName))
		{
			return pSect;
		}
	}
	return NULL;
	*/
}

/*
**
**	FUNCTION
**		Find main part with given name
**
**	PARAMETERS
**		mpName	- Name of main part to find
**
**	RETURNS
**		Pointer to MainPart with given name;
**		NULL if such sector is not found.
**
**	CAUTIONS
**		None
*/
MainPart *DataPool::findMainPartData(const char *mpName)
{
	for(int idx = 0 ; idx < mainParts.count() ; idx++)
	{
		MainPart *pMainPart = mainParts.at(idx);
		if(!strcmp(pMainPart->getName(), mpName))
		{
			return pMainPart;
		}
	}
	return NULL;
}

/*
**
**	FUNCTION
**		Find PVSS domain with given name,
**		add new domain name if not found 
**
**	PARAMETERS
**		name	- Pointer to required domain name
**		adIfNew	- Add new domain name if it does exists yet
**
**	RETURNS
**		Pointer to domain;
**		NULL if domain is not found
**
** CAUTIONS
**		None
*/
const QString DataPool::findDomain(const char *name, bool addIfNew)
{
	int idx = domains.indexOf(name);
	if(idx >= 0)
	{
		return domains.at(idx);
	}
	if(!addIfNew)
	{
		return QString();
	}
	domains.append(name);
	idx = domains.indexOf(name);
	return domains.at(idx);
}

/*
**
**	FUNCTION
**		Build list of all main part names in 'reasonable' order 
**
**	PARAMETERS
**		result		- list where all names will be put
**		visibleOnly	- true if only visible main part are needed. Main part
**						is visible if there is at least one sector in it with
**						at least one draw part located on visible beam line
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void DataPool::findMainPartNames(QStringList &result, bool visibleOnly)
{
	result.clear();
	// Main part list is built based on sector list because sectors are ordered
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		if(pSector->isOuter())
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			if(result.contains(pMap->getMainPart()->getName()))
			{
				continue;
			}
			if(visibleOnly)
			{
				const QList<SectorDrawPart *> &parts = pSector->getDrawParts();
				// Sectors on LHC ring have no draw parts
				if(parts.isEmpty() && lhcFormat)
				{
					result.append(pMap->getMainPart()->getName());
					continue;
				}
				bool found = false;
				for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
				{
					SectorDrawPart *pPart = parts.at(partIdx);
					if(!pPart->getLine()->isHidden())
					{
						result.append(pMap->getMainPart()->getName());
						found = true;
						break;
					}
				}
				// Added 28.05.2012 L.Kopylov
				if(!found)
				{
					if(!pSector->getSpecSynPanel().isEmpty())
					{
						result.append(pMap->getMainPart()->getName());
					}
				}
			}
			else
			{
				result.append(pMap->getMainPart()->getName());
			}
		}
	}
}

/*
**
**	FUNCTION
**		Build list of all sector names in given main part in 'reasonable' order 
**
**	PARAMETERS
**		mpName		- Name of main part
**		result		- list where all names will be put
**		visibleOnly	- true if only visible sectors are needed. Sector
**						is visible if there is at least one draw part
**						located on visible beam line
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void DataPool::findSectorsInMainPart(const char *mpName, QStringList &result, bool visibleOnly)
{
	result.clear();

	MainPart *pMainPart = findMainPartData(mpName);
	if(!pMainPart)
	{
		return;
	}
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		if(pSector->isOuter())
		{
			continue;
		}
		if(!pSector->isInMainPart(pMainPart))
		{
			continue;
		}
		if(visibleOnly)
		{
			const QList<SectorDrawPart *> &parts = pSector->getDrawParts();
			// Sectors on LHC ring have no draw parts
			if(parts.isEmpty() && lhcFormat)
			{
				result.append(pSector->getName());
				continue;
			}
			for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
			{
				SectorDrawPart *pPart = parts.at(partIdx);
				if(!pPart->getLine()->isHidden())
				{
					result.append(pSector->getName());
					break;
				}
			}
		}
		else
		{
			result.append(pSector->getName());
		}
	}
}

/*
**
**	FUNCTION
**		Check if sectors in list form continuous sequence of sectors,
**		i.e. it is possible to pass all sectors in list without passing
**		other sectors
**
**	PARAMETERS
**		sectorList	- List of sectors to check
**
**	RETURNS
**		true	- sectors in list form continuous sequence;
**		false	- otherwise
**
** CAUTIONS
**		None
*/
bool DataPool::isSectorListContinuous(QList<Sector *> &sectorList)
{
	if(!sectorList.count())
	{
		return false;
	}

	// Build array of sector connections
	SSectorConnectInfoPtr	connInfo = (SSectorConnectInfoPtr)alloca(
		lines.size() * sizeof(SSectorConnectInfo));
	memset((void *)connInfo, 0, lines.count() * sizeof(SSectorConnectInfo));
	int lineIdx;
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		connInfo[lineIdx].pLine = pLine;
	}

	for(int sectIdx = 0 ; sectIdx < sectorList.count() ; sectIdx++)
	{
		Sector *pSector = sectorList.at(sectIdx);
		const QList<SectorDrawPart *> &parts = pSector->getDrawParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			SectorDrawPart *pPart = parts.at(partIdx);
			for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
			{
				if(connInfo[lineIdx].pLine == pPart->getLine())
				{
					connInfo[lineIdx].nParts++;
					if(pSector->getDrawParts().size() > 1)
					{
						connInfo[lineIdx].nConnects++;
					}
					break;
				}
			}
		}
	}
	// Count total number of lines used
	int nLinesUsed = 0;
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		if(connInfo[lineIdx].nParts > 0)
		{
			nLinesUsed++;
		}
	}
	if(nLinesUsed == 1)
	{
		return true;
	}
	for(lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		if((connInfo[lineIdx].nParts > 0) && (connInfo[lineIdx].nConnects == 0))
		{
			return false;
		}
	}
	return true;
}

/*
**	FUNCTION
**		Check if all sectors of main part form continuous sequence of sectors,
**		i.e. it is possible to pass all sectors in list without passing
**		other sectors
**
**	PARAMETERS
**		sectorList	- List of sectors to check
**
**	RETURNS
**		true	- sectors in list form continuous sequence;
**		false	- otherwise
**
** CAUTIONS
**		None
*/
bool DataPool::areSectorsOfMainPartContinuous(MainPart *pMainPart)
{
	QList<Sector *> sectorList;
	for(int idx = 0 ; idx < sectors.count() ; idx++)
	{
		Sector *pSector = sectors.at(idx);
		if(pSector->isInMainPart(pMainPart))
		{
			sectorList.append(pSector);
		}
	}
	return isSectorListContinuous(sectorList);
}

/*
**	FUNCTION
**		Check if device is completely within one of LHC arcs
**
**	PARAMETERS
**		pEqp	- Pointer to device to be checked
**
**	RETURNS
**		true	- If device is entirely within one of arcs;
**		false	- otherwise
**
**	CAUTIONS
**		It is assumed that none of arcs crosses IP1
*/
bool DataPool::isEqpInTheArc(Eqp *pEqp)
{
	for(int idx = 0 ; idx < lhcArcs.count() ; idx++)
	{
		LhcRegion *pArc = lhcArcs.at(idx);
		if((pArc->getStart() <= pEqp->getStart()) &&
			((pEqp->getStart() + pEqp->getLength()) <= pArc->getEnd()))
		{
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Find for given vacuum sector neighbour vacuum sector in given direction
**		(backward/forward). Search may be or may be not limited by main part name.
**
**	PARAMETERS
**		sectorName	- Name of vacuum sector to start search from.
**		pMainPart	- Pointer to main part to limit search to,
**						NULL shall not be limited to one main part.
**		direction	- direction for search (-1 = backward, 1 = forward)
**
**	RETURNS
**		Pointer to neighbour vacuum sector;
**		NULL if there is no neighbour in given direction.
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
Sector *DataPool::findNeighbourSector(const char *sectorName, MainPart *pMainPart, int direction)
{
	Sector *pSector = findSectorData(sectorName);
	Q_ASSERT(pSector);
	return findNeighbourSector(pSector, pMainPart, direction);
}
#endif

/*
**
**	FUNCTION
**		Find for given vacuum sector neighbour vacuum sector in given direction
**		(backward/forward). Search may be or may be not limited by main part name.
**
**	PARAMETERS
**		pStartSector- Pointer to vacuum sector to start search from.
**		pMainPart	- Pointer to main part to limit search to,
**						NULL shall not be limited to one main part.
**		direction	- direction for search (-1 = backward, 1 = forward)
**
**	RETURNS
**		Pointer to neighbour vacuum sector;
**		NULL if there is no neighbour in given direction.
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
Sector *DataPool::findNeighbourSector(Sector *pStartSector, MainPart *pMainPart, int direction)
{
	if(pMainPart)
	{
		return neighbourSectorInMainPart(pStartSector, pMainPart, direction);
	}
	return neighbourSectorAnyMainPart(pStartSector, direction);
}	
#endif

/*
**
**	FUNCTION
**		Find for given sector neighbour sector in given direction.
**		Search is not limited to particular main part (though 'unlimited'
**		search only works on circular beam line).
**
**	PARAMETERS
**		pStartSector	- Pointer to sector for which neighbour is needed.
**		direction		- Direction for search (-1 = backward, 1 = forward).
**
**	RETURNS
**		Pointer neighbour sector,
**		NULL if no neighbour sector found
**
**	CAUTIONS
**    None
*/
#ifndef PVSS_SERVER_VERSION
Sector *DataPool::neighbourSectorAnyMainPart(Sector *pStartSector, int direction)
{
	// Check every main part of this sector
	int bestNbrIdx = direction > 0 ? (int)sectors.count() : -1,
		mapIdx;
	const QList<MainPartMap *> maps = pStartSector->getMainPartMaps();
	for(mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
	{
		MainPartMap *pMap = maps.at(mapIdx);
		Sector *pSector = neighbourSectorInMainPart(pStartSector, pMap->getMainPart(), direction);
		if(!pSector)
		{
			continue;
		}
		int idx = sectors.indexOf(pSector);
		if(direction > 0)
		{
			if(idx < bestNbrIdx)
			{
				bestNbrIdx = idx;
			}
		}
		else
		{
			if(idx > bestNbrIdx)
			{
				bestNbrIdx = idx;
			}
		}
	}
	if((bestNbrIdx >= 0) && (bestNbrIdx < (int)sectors.count()))
	{
		return sectors.at(bestNbrIdx);
	}

	// Search in another main parts - check main parts which are located on the same
	// beam line as main parts of start sector
	for(int mpIdx = 0 ; mpIdx < mainParts.count() ; mpIdx++)
	{
		MainPart *pOtherMp = mainParts.at(mpIdx);
		if(pStartSector->isInMainPart(pOtherMp))
		{
			continue;	// Already checked
		}
		for(mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			if(pOtherMp->getMainLine() != pMap->getMainPart()->getMainLine())
			{
				continue;
			}
			Sector *pSector = neighbourSectorInMainPart(pStartSector, pOtherMp, direction);
			if(pSector)
			{
				return pSector;
			}
		}
	}
	return NULL;
}
#endif

/*
**
**	FUNCTION
**		Find for given sector index neighbour sector in given direction.
**		Search is limited to particular main part.
**
**	PARAMETERS
**		pStartSector	- Pointer to sector for which neighbour is needed.
**		pMainPart		- Pointer to main part where search is limited.
**		direction		- Direction for search (-1 = backward, 1 = forward).
**
**	RETURNS
**		Pointer to neighbour sector;
**		NULL if no neughbour.
**
**	CAUTIONS
**		The method does NOT work for sectors on LHC main ring
*/
#ifndef PVSS_SERVER_VERSION
Sector *DataPool::neighbourSectorInMainPart(Sector *pStartSector, MainPart *pMainPart, int direction)
{
	BeamLine *pBeamLine = pMainPart->getMainLine();

	int sectIdx;
	Sector	*pSector = NULL;
	for(sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSect = sectors.at(sectIdx);
		if(pSect == pStartSector)
		{
			pSector = pSect;
			break;
		}
	}
	Q_ASSERT(pSector);

	if(pBeamLine->isCircle())
	{
		while(true)
		{
			if(direction < 0)
			{
				sectIdx--;
				if(sectIdx < 0)
				{
					sectIdx = sectors.count() - 1;
				}
			}
			else
			{
				sectIdx++;
				if(sectIdx >= sectors.count())
				{
					sectIdx = 0;
				}
			}
			pSector = sectors.at(sectIdx);
			if(pSector == pStartSector)
			{
				return NULL;
			}
			if(pSector->isOuter())
			{
				continue;
			}
			// Check if at least one main part of neighbour sector has the same main line index
			const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
			bool mainLineMatches = false;
			for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
			{
				MainPartMap *pMap = maps.at(mapIdx);
				if(pMap->getMainPart()->getMainLine() == pBeamLine)
				{
					mainLineMatches = true;
					break;
				}
			}
			if(mainLineMatches)
			{
				break;
			}
		}
		// Only come here if pSector is on main part with the same main beam line as
		// requested main part
		return(pSector->isInMainPart(pMainPart) ? pSector : NULL);
	}
	else
	{
		while(true)
		{
			if(direction < 0)
			{
				sectIdx--;
				if(sectIdx < 0)
				{
					return NULL;
				}
			}
			else
			{
				sectIdx++;
				if(sectIdx >= sectors.count())
				{
					return NULL;
				}
			}
			pSector = sectors.at(sectIdx);
			if(!pSector->isOuter())
			{
				if(pSector->isInMainPart(pMainPart))
				{
					return pSector;
				}
			}
		}
	}
	return NULL;
}
#endif

/*
**	FUNCTION
**		Find thermometer(s) providing opening interlock for valve
**
**	PARAMETERS
**		valveDpName	- DP name of valve
**		result		- Variable where thermometer DP name(s) will be put
**		errMsg		- Variable where error message will be put in case of error
**
**	RETURNS
**		Number of thermometer(s) found, or
**		-1 if not found
**
**	CAUTIONS
**		None
*/
int DataPool::findThermometersForValve(const char *valveDp, QStringList &result, QString &errMsg)
{
	int	coco = 0;

	errMsg = "";
	result.clear();

	if(!lhcFormat)
	{
		return 0;	// LHC only
	}

	Eqp *pValve = findEqpByDpName(valveDp);
	if(!pValve)
	{
		errMsg = valveDp;
		errMsg += " not found";
		return -1;
	}

	// Find coordinates range to search: from this valve to either
	// previous or next valve on THE SAME vacuum type
	BeamLine *pLhc = getCircularLine();
	if(!pLhc)
	{
		errMsg = "LHC ring data not found";
		return -1;
	}

	// Find valve itself
	int valveIdx = 0;
	QList<BeamLinePart *> &parts = pLhc->getParts();
	BeamLinePart *pPart = parts.first();	// LHC consists of one part only
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();

	for(valveIdx = nEqpPtrs - 1 ; valveIdx >= 0 ; valveIdx--)
	{
		if(eqpPtrs[valveIdx] == pValve)
		{
			break;
		}
	}

	if(valveIdx < 0)	// Valve not on LHC ring
	{
		return 0;
	}

	// Search for thermometers in both directions
	coco += findThermometersNearValve(pPart, pValve, valveIdx, 1, result);
	coco += findThermometersNearValve(pPart, pValve, valveIdx, -1, result);
	return coco;
}

/*
**	FUNCTION
**		Find CRYO thermometers, closest to the valve, when travelling from valve
**		in given direction
**
**	PARAMETERS
**		pPart		- Pointer to beam line part, where valve is located (LHC ring)
**		pValve		- Pointer to valve for which thermometers shall be found
**		valveIdx	- Index of valve device in equipment on beam line part
**		direction	- Direction for search:
**						-1 = search backward
**						 1 = search forward
**		result		- Variable where DP names of thermometers shall be added,
**
**	RETURNS
**		Number of thermometers found
**
**	CAUTIONS
**		None
*/
int DataPool::findThermometersNearValve(BeamLinePart *pPart, Eqp *pValve, int valveIdx, int direction,
		QStringList &result)
{
	int		coco = 0;
	float	thermoCoord = 2 * lhcRingLength;
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	int eqpIdx = valveIdx + direction;
	while(true)
	{
		if(eqpIdx < 0)
		{
			eqpIdx = nEqpPtrs - 1;
		}
		else if(eqpIdx >= nEqpPtrs)
		{
			eqpIdx = 0;
		}
		if(eqpIdx == valveIdx)
		{
			break;
		}
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(pEqp->isCryoThermometer())
		{
			if(!(pEqp->getVacType() & VacType::Qrl))	// Not interesting on QRL
			{
				if(pEqp->getCtrlSubType() == 5)	// Max thermometer
				{
					if(thermoCoord <= lhcRingLength)	// Already have thermometer(s)
					{
						if(pEqp->getStart() != thermoCoord)
						{
							break;
						}
					}
					pEqp->setValveInterlock(true);
					coco++;
					result.append(pEqp->getDpName());
				}
			}

		}
		// Stop search on another valve on the same (or compatible) vacuum type
		if(pEqp->getFunctionalType() == FunctionalType::VV)
		{
			if(pEqp->getVacType() == pValve->getVacType())
			{
				break;
			}
			bool	neighbourFound = false;
			switch(pValve->getVacType())
			{
			case VacType::RedBeam:
			case VacType::BlueBeam:
				if(pEqp->getVacType() == VacType::CrossBeam)
				{
					neighbourFound = true;
				}
				break;
			case VacType::CrossBeam:
				if((pEqp->getVacType() == VacType::RedBeam) || (pEqp->getVacType() == VacType::BlueBeam))
				{
					neighbourFound = true;
				}
				break;
			}
			if(neighbourFound)
			{
				break;
			}
		}
		eqpIdx += direction;
	}
	return coco;
}

/*
**	FUNCTION
**		Find valves before and after given valve
**
**	PARAMETERS
**		valveDpName		- DP name of valve
**		valvesBefore	- Variable where previous valves DP name(s) will be put
**		valvesAfter		- Variable where next valves DP name(s) will be put
**		errMsg			- Variable where error message will be put in case of error
**
**	RETURNS
**		Number of valve(s) found, or
**		-1 if not found
**
**	CAUTIONS
**		None
*/
int DataPool::findNeighbourValves(const char *valveDpName, QStringList &valvesBefore, QStringList &valvesAfter, QString &errMsg)
{
	int	coco = 0;

	errMsg = "";
	valvesBefore.clear();
	valvesAfter.clear();

	Eqp *pValve = findEqpByDpName(valveDpName);
	if(!pValve)
	{
		errMsg = valveDpName;
		errMsg += " not found";
		return -1;
	}

	for(QHash<QByteArray, Eqp *>::ConstIterator iter = eqpDict.constBegin() ; iter != eqpDict.constEnd() ; ++ iter)
	{
		Eqp *pEqp = iter.value();
		if(pEqp->getFunctionalType() != FunctionalType::VV)
		{
			continue;
		}
		if(pEqp == pValve)
		{
			continue;
		}
		if(pEqp->getSectorBefore() == pValve->getSectorAfter())
		{
			valvesAfter.append(pEqp->getDpName());
		}
		if(pEqp->getSectorAfter() == pValve->getSectorBefore())
		{
			valvesBefore.append(pEqp->getDpName());
		}
	}

	coco = valvesBefore.count() + valvesAfter.count();
	return coco;

	/* New approach - just search for valves with appropriate SectorBefore/sectorAfter.
	** Old approach (geometry) commented out below
	*/
	
	/* L.Kopylov 19.10.2010
	if(lhcFormat)
	{
		coco = findNeigourValvesLhc(pValve, valvesBefore, valvesAfter, errMsg);
	}
	else
	{
		coco = findNeigourValvesNonLhc(pValve, valvesBefore, valvesAfter, errMsg);
	}

	return coco;
	*/
}

/*
**	FUNCTION
**		Find valves before and after given valve - version for LHC data
**
**	PARAMETERS
**		pValve			- Pointer to valve for which neighbours are required
**		valvesBefore	- Variable where previous valves DP name(s) will be put
**		valvesAfter		- Variable where next valves DP name(s) will be put
**		errMsg			- Variable where error message will be put in case of error
**
**	RETURNS
**		Number of valve(s) found, or
**		-1 if not found
**
**	CAUTIONS
**		None
*/
int DataPool::findNeigourValvesLhc(Eqp *pValve, QStringList &valvesBefore, QStringList &valvesAfter, QString &errMsg)
{
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		// Valve is start of sector
		if(pSector->getRedStartEqp() == pValve)
		{
			if(pSector->getRedEndEqp())
			{
				valvesAfter.append(pSector->getRedEndEqp()->getDpName());
			}
			if(pSector->getBlueEndEqp())
			{
				valvesAfter.append(pSector->getBlueEndEqp()->getDpName());
			}
			if(pSector->getCrossEndEqp())
			{
				valvesAfter.append(pSector->getCrossEndEqp()->getDpName());
			}
		}
		if(pSector->getBlueStartEqp() == pValve)
		{
			if(pSector->getRedEndEqp())
			{
				valvesAfter.append(pSector->getRedEndEqp()->getDpName());
			}
			if(pSector->getBlueEndEqp())
			{
				valvesAfter.append(pSector->getBlueEndEqp()->getDpName());
			}
			if(pSector->getCrossEndEqp())
			{
				valvesAfter.append(pSector->getCrossEndEqp()->getDpName());
			}
		}
		if(pSector->getCrossStartEqp() == pValve)
		{
			if(pSector->getRedEndEqp())
			{
				valvesAfter.append(pSector->getRedEndEqp()->getDpName());
			}
			if(pSector->getBlueEndEqp())
			{
				valvesAfter.append(pSector->getBlueEndEqp()->getDpName());
			}
			if(pSector->getCrossEndEqp())
			{
				valvesAfter.append(pSector->getCrossEndEqp()->getDpName());
			}
		}

		// Valve is end of sector
		if(pSector->getRedEndEqp() == pValve)
		{
			if(pSector->getRedStartEqp())
			{
				valvesBefore.append(pSector->getRedStartEqp()->getDpName());
			}
			if(pSector->getBlueStartEqp())
			{
				valvesBefore.append(pSector->getBlueStartEqp()->getDpName());
			}
			if(pSector->getCrossStartEqp())
			{
				valvesBefore.append(pSector->getCrossStartEqp()->getDpName());
			}
		}
		if(pSector->getBlueEndEqp() == pValve)
		{
			if(pSector->getRedStartEqp())
			{
				valvesBefore.append(pSector->getRedStartEqp()->getDpName());
			}
			if(pSector->getBlueStartEqp())
			{
				valvesBefore.append(pSector->getBlueStartEqp()->getDpName());
			}
			if(pSector->getCrossStartEqp())
			{
				valvesBefore.append(pSector->getCrossStartEqp()->getDpName());
			}
		}
		if(pSector->getCrossEndEqp() == pValve)
		{
			if(pSector->getRedStartEqp())
			{
				valvesBefore.append(pSector->getRedStartEqp()->getDpName());
			}
			if(pSector->getBlueStartEqp())
			{
				valvesBefore.append(pSector->getBlueStartEqp()->getDpName());
			}
			if(pSector->getCrossStartEqp())
			{
				valvesBefore.append(pSector->getCrossStartEqp()->getDpName());
			}
		}	
	}
	// In addition valves can be on draw parts
	findNeigourValvesNonLhc(pValve, valvesBefore, valvesAfter, errMsg);

	return valvesBefore.count() + valvesAfter.count();
}

/*
**	FUNCTION
**		Find valves before and after given valve - version for non-LHC data
**
**	PARAMETERS
**		pValve			- Pointer to valve for which neighbours are required
**		valvesBefore	- Variable where previous valves DP name(s) will be put
**		valvesAfter		- Variable where next valves DP name(s) will be put
**		errMsg			- Variable where error message will be put in case of error
**
**	RETURNS
**		Number of valve(s) found, or
**		-1 if not found
**
**	CAUTIONS
**		None
*/
int DataPool::findNeigourValvesNonLhc(Eqp *pValve, QStringList &valvesBefore, QStringList &valvesAfter, QString & /* errMsg */)
{
	bool valveFound = false;
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		if(lhcFormat && pLine->isCircle())
		{
			continue;	// skip LHC ring
		}
		if(pLine->isVirtualLine())
		{
			continue;
		}
		if(pLine->isHidden())
		{
			continue;
		}
		const QList<BeamLinePart *> &parts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			for(int n = 0 ; n < nEqpPtrs ; n++)
			{
				Eqp *pEqp = eqpPtrs[n];
				if(pEqp == pValve)
				{
					findValvesBefore(pLine, pPart, n, valvesBefore);
					findValvesAfter(pLine, pPart, n, valvesAfter);
					valveFound = true;
					break;
				}
			}
			if(valveFound)
			{
				break;
			}
		}
		if(valveFound)
		{
			break;
		}
	}
	
	return valvesBefore.count() + valvesAfter.count();
}

void DataPool::findValvesAfter(BeamLine *pStartLine, BeamLinePart *pStartPart, int startEqpIdx, QStringList &valves)
{
	QList<BeamLine *>::ConstIterator lineIter;
	BeamLine *pLine = findStartLine(lineIter, pStartLine);
	if(!pLine)
	{
		return;	// Can not happen
	}
	QList<BeamLinePart *>::ConstIterator partIter;
	const QList<BeamLinePart *> &parts = pLine->getParts();
	BeamLinePart *pPart = findStartLinePart(parts, partIter, pStartPart);
	if(!pPart)
	{
		return;	// Can not happen
	}

	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	int eqpIdx = startEqpIdx;
	bool valveFound = false;

	while(true)
	{
		eqpIdx++;
		if(eqpIdx >= nEqpPtrs)
		{
			if(pLine->isCircle())
			{
				partIter = parts.constBegin();
				pPart = *partIter;
				eqpPtrs = pPart->getEqpPtrs();
				nEqpPtrs = pPart->getNEqpPtrs();
				eqpIdx = 0;
			}
			else
			{
				break;
			}
		}
		if((pLine == pStartLine) && (pPart == pStartPart) && (eqpIdx == startEqpIdx))
		{
			break;
		}
		Eqp *pEqp = eqpPtrs[eqpIdx];
		switch(pEqp->getType())
		{
		case EqpType::VacDevice:
			if(pEqp->getFunctionalType() == FunctionalType::VV)
			{
				valves.append(pEqp->getDpName());
				valveFound = true;
			}
			break;
		case EqpType::OtherLineStart:
			{
				BeamLine *pOtherLine = getLine(pEqp->getName());
				if(pOtherLine)
				{
					const QList<BeamLinePart *> otherParts = pOtherLine->getParts();
					for(int otherIdx = 0 ; otherIdx < otherParts.count() ; otherIdx++)
					{
						BeamLinePart *pOtherPart = otherParts.at(otherIdx);
						if(pOtherPart->getNEqpPtrs())
						{
							findValvesAfter(pOtherLine, pOtherPart, -1, valves);
						}
					}
				}
			}
			break;
		case EqpType::OtherLineEnd:
			{
				BeamLine *pOtherLine = getLine(pEqp->getName());
				if(pOtherLine)
				{
					const QList<BeamLinePart *> otherParts = pOtherLine->getParts();
					for(int otherIdx = 0 ; otherIdx < otherParts.count() ; otherIdx++)
					{
						BeamLinePart *pOtherPart = otherParts.at(otherIdx);
						if(pOtherPart->getNEqpPtrs())
						{
							findValvesBefore(pOtherLine, pOtherPart, pOtherPart->getNEqpPtrs(), valves);
						}
					}
				}
			}
			break;
		}
		if(valveFound)
		{
			break;
		}
	}
}

void DataPool::findValvesBefore(BeamLine *pStartLine, BeamLinePart *pStartPart, int startEqpIdx, QStringList &valves)
{
	QList<BeamLine *>::ConstIterator lineIter;
	BeamLine *pLine = findStartLine(lineIter, pStartLine);
	if(!pLine)
	{
		return;	// Can not happen
	}
	QList<BeamLinePart *>::ConstIterator partIter;
	const QList<BeamLinePart *> &parts = pLine->getParts();
	BeamLinePart *pPart = findStartLinePart(parts, partIter, pStartPart);
	if(!pPart)
	{
		return;	// Can not happen
	}

	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	int eqpIdx = startEqpIdx;
	bool valveFound = false;

	while(true)
	{
		eqpIdx--;
		if(eqpIdx < 0)
		{
			if(pLine->isCircle())
			{
				partIter = parts.constEnd();
				--partIter;
				pPart = *partIter;
				eqpPtrs = pPart->getEqpPtrs();
				nEqpPtrs = pPart->getNEqpPtrs();
				eqpIdx = nEqpPtrs - 1;
			}
			else
			{
				break;
			}
		}
		if((pLine == pStartLine) && (pPart == pStartPart) && (eqpIdx == startEqpIdx))
		{
			break;
		}
		Eqp *pEqp = eqpPtrs[eqpIdx];
		switch(pEqp->getType())
		{
		case EqpType::VacDevice:
			if(pEqp->getFunctionalType() == FunctionalType::VV)
			{
				valves.append(pEqp->getDpName());
				valveFound = true;
			}
			break;
		case EqpType::OtherLineStart:
			{
				BeamLine *pOtherLine = getLine(pEqp->getName());
				if(pOtherLine)
				{
					const QList<BeamLinePart *> otherParts = pOtherLine->getParts();
					for(int otherIdx = 0 ; otherIdx < otherParts.count() ; otherIdx++)
					{
						BeamLinePart *pOtherPart = otherParts.at(otherIdx);
						{
							findValvesAfter(pOtherLine, pOtherPart, -1, valves);
						}
					}
				}
			}
			break;
		case EqpType::OtherLineEnd:
			{
				BeamLine *pOtherLine = getLine(pEqp->getName());
				if(pOtherLine)
				{
					const QList<BeamLinePart *> otherParts = pOtherLine->getParts();
					for(int otherIdx = 0 ; otherIdx < otherParts.count() ; otherIdx++)
					{
						BeamLinePart *pOtherPart = otherParts.at(otherIdx);
						if(pOtherPart->getNEqpPtrs())
						{
							findValvesBefore(pOtherLine, pOtherPart, pOtherPart->getNEqpPtrs(), valves);
						}
					}
				}
			}
			break;
		}
		if(valveFound)
		{
			break;
		}
	}
}

BeamLine *DataPool::findStartLine(QList<BeamLine *>::ConstIterator &iter, BeamLine *pStartLine)
{
	for(iter = lines.constBegin() ; iter != lines.constEnd() ; ++iter)
	{
		BeamLine *pLine = *iter;
		if(pLine == pStartLine)
		{
			return pLine;
		}
	}
	return NULL;
}

BeamLinePart *DataPool::findStartLinePart(const QList<BeamLinePart *> &parts,
	QList<BeamLinePart *>::ConstIterator &iter, BeamLinePart *pStartPart)
{
	for(iter = parts.constBegin() ; iter != parts.constEnd() ; ++iter)
	{
		BeamLinePart *pPart = *iter;
		if(pPart == pStartPart)
		{
			return pPart;
		}
	}
	return NULL;
}

static bool CompareVisibleNames(Eqp *pEqp1, Eqp *pEqp2)
{
	return strcmp(pEqp1->getVisibleName(), pEqp2->getVisibleName()) < 0;
}

void DataPool::findEqpMatchingMask(const QString &filter, QStringList &dpNames, QStringList &eqpNames)
{
	dpNames.clear();
	eqpNames.clear();

	// Traverse all devices, add devices mathcing filter to table.
	// Check only devices in sectors
	// L.Kopylov 28.08.2012: check also devices NOT in sectors, they shall appear before devices in sectors
	QStringList filters = filter.split(";", QString::SkipEmptyParts);

	// Use QList in order to sort by visible name
	QList<Eqp *> eqpList;
	foreach(Eqp *pEqp, eqpDict)
	{
		if(!pEqp->getFunctionalType())
		{
			continue;
		}
		if((!pEqp->getSectorBefore()) && (!pEqp->getSectorAfter()) && (!pEqp->getMainPart()))
		{
			bool matches = false;
			switch(pEqp->getFunctionalType())	// Some types are ignored
			{
			case FunctionalType::VRPI:
				break;
			default:
				{
					QStringList devListDpeNames;
					pEqp->getDpesForDevList(devListDpeNames);
					matches = devListDpeNames.count() > 0;
				}
				break;
			}
			if(matches)
			{
				foreach(QString filter, filters)
				{
					if(nameMatchesFilter(pEqp->getVisibleName(), filter.toLatin1()))
					{
						eqpList.append(pEqp);
						break;
					}
				}
			}
		}
	}

	// Sort and add to name list
	if(eqpList.count())
	{
		qSort(eqpList.begin(), eqpList.end(), CompareVisibleNames);
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			Eqp *pEqp = eqpList.at(eqpIdx);
			eqpNames.append(pEqp->getVisibleName());
			dpNames.append(pEqp->getDpName());
		}
		eqpList.clear();
	}

	const QList<Sector *> &sectors = pool.getSectors();
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		const QList<Eqp *> &eqpList = pSector->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			Eqp *pEqp = eqpList.at(eqpIdx);
			if(!pEqp->getDpName())
			{
				continue;
			}
			// Device can be already in list if device appears in more than one sector. L.Kopylov 10.10.2014
			if(dpNames.contains(pEqp->getDpName()))
			{
				continue;
			}
			foreach(QString filter, filters)
			{
				if(nameMatchesFilter(pEqp->getVisibleName(), filter.toLatin1()))
				{
					eqpNames.append(pEqp->getVisibleName());
					dpNames.append(pEqp->getDpName());
					break;
				}
			}
		}
	}
}

/*
**
**	FUNCTION
**		Check if given name matches wildcard. Wildcard includes:
**		* - matches 0 or more of any characters
**		? - mathes any one character
**
**	PARAMETERS
**		name		- Name of passive equipment
**		filter	- Wildcard to check name against
**
**	RETURNS
**		Type of equipment
**
**	CAUTIONS
**		None
*/
bool DataPool::nameMatchesFilter(const char *name, const char *filter)
{
	while(*name && *filter)
	{
/*
printf("DataPool::nameMatchesFilter(): <%s> filter <%s>\n", name, filter);
fflush(stdout);
*/
		if(*filter == '*')
		{
			filter++;
			if(!(*filter))
			{
				return true;
			}
			while(*name)
			{
				if(nameMatchesFilter(name, filter))
				{
					return true;
				}
				name++;
			}
			return false;
		}
		else if(*filter == '?')
		{
			/*
			name++;
			filter++;
			*/
		}
		else if(*name != *filter)
		{
			return false;
		}
		name++;
		filter++;
	}
/*
printf("DataPool::nameMatchesFilter(): cycle exit %s %s\n",
(*name ? name : "null"), (*filter ? filter : "null"));
fflush(stdout);
*/
	if(*name || *filter)
	{
		return false;
	}
	return true;
}

// L.Kopylov 29.01.2010 addition to extend SPS sectors for main view
/*
**	FUNCTION
**		Set minimum sector length by extending short sectors (if possible)
**		 on beam line with given name
**
**	PARAMETERS
**		lineName		- Beam line name
**		minLength	- required minimum sector length
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DataPool::setMinSectorLength(const char *lineName, int minLength)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		BeamLine *pLine = lines.at(idx);
		if(!strcmp(pLine->getName(), lineName))
		{
			pLine->setMinSectorLength(minLength);
			break;
		}
	}
	if(DebugCtl::isData())
	{
		char	fileName[256];
#ifdef Q_OS_WIN
		sprintf_s(fileName, sizeof(fileName) / sizeof(fileName[0]), "D:\\%s_SetMinSectorLength.txt", machine);
		FILE *pFile = NULL;
		if(fopen_s(&pFile, fileName, "w"))
		{
			pFile = NULL;
		}
#else
		sprintf(fileName, "/opt/vacin/debug/%s_SetMinSectorLength.txt", machine);
		FILE *pFile = fopen(fileName, "w");
#endif
		if(pFile)
		{
			dump(pFile);
			fclose(pFile);
		}
	}
}

/*
**	FUNCTION
**		[VACCO-1645]
**		Finds the Sector object corresponding to a specific DP (pvss) name
**
**	PARAMETERS
**		dpName - sector DP (pvss) name
**
**	RETURNS
**		Sector - pointer for the sector object, if it exists
**
**	CAUTIONS
**		None
*/
Sector *DataPool::findSectorByDpName(const char *dpName)
{
	if (!dpName)
	{
		return NULL;
	}
	if (!(*dpName))
	{
		return NULL;
	}
	const QList<Sector *> &allSectors = DataPool::getSectors();
	Sector *pSector = NULL;
	for (int sectIdx = 0; sectIdx < allSectors.count(); sectIdx++){
		if (strcmp(dpName, allSectors.at(sectIdx)->getPvssName()) == 0){
			pSector = allSectors.at(sectIdx);
			break;
		}
	}
	if (pSector) return pSector;
	else return NULL;
}



Eqp *DataPool::findEqpByVisibleName(const char *name)
{
	if(!name)
	{
		return NULL;
	}
	if(!(*name))
	{
		return NULL;
	}
	foreach(Eqp *pEqp, eqpDict)
	{
		if(!strcmp(name, pEqp->getVisibleName()))
		{
			return pEqp;
		}
	}
	return NULL;
}

QList<Eqp *> DataPool::findEqpsByMasterDp(QString masterDp) {
	QList<Eqp *> eqps;
	eqps.clear();
	if (masterDp == NULL) {
		return eqps;
	}
	foreach(Eqp *pEqp, eqpDict) {
		if (pEqp->getMasterDp() == masterDp) {
			eqps.append(pEqp);
		}
	}
	return eqps;
}

void DataPool::calculateAccessPointDistance(void)
{
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		QList<BeamLinePart *> &parts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			QList<Eqp *> &eqpList = pPart->getEqpList();
			for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
			{
				Eqp *pEqp = eqpList.at(eqpIdx);
				if(pEqp->getType() != EqpType::VacDevice)
				{
					continue;	// Does not matter for others
				}
				pEqp->setAccessPointDistance(pEqp->getStart());
			}
		}
	}
}

void DataPool::calculateAccessPointDistanceLhc(void)
{
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		if(pLine->isCircle())
		{
			calculateAccessPointDistanceLhcCircleLine(pLine);
		}
		else
		{
			calculateAccessPointDistanceLhcLine(pLine);
		}
	}
}

void DataPool::calculateAccessPointDistanceLhcCircleLine(BeamLine *pLine)
{
	QList<BeamLinePart *> &parts = pLine->getParts();
	float halfRingLength = lhcRingLength / 2;
	for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			Eqp *pEqp = eqpList.at(eqpIdx);
			if(pEqp->getType() != EqpType::VacDevice)
			{
				continue;	// Does not matter for others
			}
			float bestDistance = lhcRingLength;
			for(int ipIdx = 0 ; ipIdx < nLhcIps ; ipIdx++)
			{
				float distance = (float)fabs(lhcIps[ipIdx] - pEqp->getStart());
				if(distance < bestDistance)
				{
					bestDistance = distance;
				}
				if(pEqp->getSurveyPos() > halfRingLength)
				{
					distance = (float)fabs(lhcIps[ipIdx] + lhcRingLength - pEqp->getStart());
					if(distance < bestDistance)
					{
						bestDistance = distance;
					}
				}
			}
			pEqp->setAccessPointDistance(bestDistance);
		}
	}
}

void DataPool::calculateAccessPointDistanceLhcLine(BeamLine *pLine)
{
	QList<BeamLinePart *> &parts = pLine->getParts();
	for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			Eqp *pEqp = eqpList.at(eqpIdx);
			if(pEqp->getType() != EqpType::VacDevice)
			{
				continue;	// Does not matter for others
			}
			pEqp->setAccessPointDistance(pEqp->getStart());
		}
	}
}

/*
**
**	FUNCTION
**		Dump general parameters for output file for debugging purpose
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DataPool::dump(FILE *pFile)
{
	if(!DebugCtl::isData())
	{
		return;
	}
	int		n;

	fprintf(pFile, "minPassiveEqpVisibleWidth = %f\n\n", minPassiveEqpVisibleWidth);
	if(lhcFormat)
	{
		fprintf(pFile, "LHC ring length = %f\n\n", lhcRingLength);

		fprintf(pFile, "%d LHC IPs:\n", nLhcIps);
		for(n = 0 ; n < nLhcIps ; n++)
		{
			fprintf(pFile, "  %d :  %f\n", n, lhcIps[n]);
		}

		fprintf(pFile, "\n%d LHC ARCs:\n", lhcArcs.count());
		LhcRegion *pReg;
		for(n = 0 ; n < lhcArcs.count() ; n++)
		{
			pReg = lhcArcs.at(n);
			pReg->dump(pFile, n);
		}

		fprintf(pFile, "\n%d LHC beam locations:\n", lhcBeamLocs.count());
		for(n = 0 ; n < lhcBeamLocs.count() ; n++)
		{
			pReg = lhcBeamLocs.at(n);
			pReg->dump(pFile, n);
		}
	}

	fprintf(pFile, "\n======== %d DOMAINS:\n", domains.count());
	n = 0;
	foreach(QString domain, domains)
	{
		fprintf(pFile, "  %d: <%s>\n", n++, domain.toLatin1().constData());
	}
	
	fprintf(pFile, "\n======== %d MAIN PARTS:\n", mainParts.count());
	for(n = 0 ; n < mainParts.count() ; n++)
	{
		MainPart *pMp = mainParts.at(n);
		fprintf(pFile, "  %d: <%s>\n", n, pMp->getName());
	}
	
	fprintf(pFile, "\n======== %d SECTORS:\n", sectors.count());
	for(n = 0 ; n < sectors.count() ; n++)
	{
		Sector *pSect = sectors.at(n);
		pSect->dump(n, pFile);
	}

	fprintf(pFile, "\n#################### %d BEAM LINES ######################\n", lines.count());
	for(n = 0 ; n < lines.count() ; n++)
	{
		BeamLine *pLine = lines.at(n);
		pLine->dump(pFile, n);
	}
	fprintf(pFile, "\n#################### %d CONTROLLERS ######################\n", ctlList.count());
	for(n = 0 ; n < ctlList.count() ; n++)
	{
		Eqp *pEqp = ctlList.at(n);
		pEqp->dump(pFile, n);
	}
}

// Function to be called by qsort() to order array lhcIps
static int compareIpCoord(const void *p1, const void *p2)
{
	float	pos1 = *((float *)p1), pos2 = *((float *)p2);

	if(pos1 < pos2)
	{
		return -1;
	}
	else if(pos1 > pos2)
	{
		return 1;
	}
	return 0;
}

//comparison function to be used in rack list sorting
bool DataPool::rackNameLessThan(Rack * firstRack, Rack * secondRack)
{
	if (firstRack && secondRack)
	{
		QString firstRackName = firstRack->getRackName();
		QString secondRackName = secondRack->getRackName();
		QString firstBuildingName = firstRackName.right(firstRackName.length() - firstRackName.indexOf("=")- 1);
		QString secondBuildingName = secondRackName.right(secondRackName.length() - secondRackName.indexOf("=") - 1);

		int compareBuilding = firstBuildingName.compare(secondBuildingName);
		int compareRack = firstRackName.compare(secondRackName);

		if (compareBuilding < 0)
		{
			return true;
		}
		else if (compareBuilding > 0)
		{
			return false;
		}
		else if (compareBuilding == 0)
		{
			return (compareRack < 0);
		}
	}
	else
		return false;
}


void DataPool::sortRackList(void)
{
	if (rackList.length()>0)
	{
		qSort(rackList.begin(), rackList.end(), rackNameLessThan);
	}
}

/*
** FUNCTION
**		find rack in rack list, if it exists
**
** PARAMETERS
**		QString - name of rack
**
** RETURNS
**		int - index of rack if found, -1 if not
**
** CAUTIONS
**		None
*/
int DataPool::findRackByName(QString rackName)
{
	for (int i = 0; i < rackList.size(); i++)
	{
		if (QString::compare(rackName, rackList.at(i)->getRackName()) == 0)
		{
			return i;
		}
	}
	
	return -1;
}

/**
@brief Return the list of Eqp with a given mobyle type
@param[in]	mType	Mobile type used as filter
*/
QList<Eqp *> DataPool::getMobileTypeEqps(int mType) {
	QList<Eqp *> mTypeEqps;
	mTypeEqps.clear();
	//QHash<QByteArray, Eqp *>	eqpDict;
	foreach(Eqp *pEqp, eqpDict) {
		if ( pEqp->getMobileType() == mType) {
			mTypeEqps.append(pEqp);
		}
	}
	return mTypeEqps;
}

InterlockChain * DataPool::getInterlockChain(QString valveName)
{
	for (int i = 0; i < interlockChainList.length(); i++)
	{
		if (QString::compare(interlockChainList.at(i)->getValveName(), valveName) == 0)
		{
			return interlockChainList.at(i);
		}
	}

	return NULL;
}