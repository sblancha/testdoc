const string HISTORY_PROFILE_DP_TYPE = "_VacHistoryUserProfile";
const string HISTORY_MULTIPLE_PROFILE_DP_TYPE = "_VacHistoryUserProfileMultiple";

//@author: Serguey Merker (IHEP, Protvino, Russia)

//Creation Date: 09/01/2007

//Modification History:
//    08/01/2009  L.Kopylov
//      Redesigned for Qt-based application
			

//version 1.0

//Purpose: 
//	Get history (state and(or) analog values) for  any DPE
//	at specified time interval
//	Usage: Public


#uses "vclMobile.ctl"	// Load control library
#uses "vclActions.ctl"

// For bakeout rack first time intervals for archive selection are built
// as dyn_dyn_anytype. Every interval contains 3 values, constants below
// are used to identify these 3 values in dyn_anytype:
const int RANGE_WORK_DP = 1;  // Name of work DP for bakeout rack [string]
const int RANGE_START = 2;    // Start time of range [time]
const int RANGE_END = 3;      // End time of range [time]

void VacEqpFindFirstArchiveTime(string dpeName, int mobileType, time startTime)
{
  string dpName, dpe;
  time firstTime = startTime;

  switch(mobileType)
  {
  case VAC_MOBILE_TYPE_FIXED:
  case VAC_MOBILE_TYPE_ON_FLANGE:
    dpName = dpSubStr(dpeName, DPSUB_DP);
    VacEqpFirstArchiveTime(dpeName, firstTime);
    break;
  case VAC_MOBILE_TYPE_ON_SECTOR:
    if(ParseDpString(dpeName, dpName, dpe) != LVA_OK) {
      break;
    }
    VacEqpFirstRackArchiveTime(dpName, dpe, startTime, firstTime);
    break;
	default:
    dpName = dpSubStr(dpeName, DPSUB_DP);
    VacEqpFirstArchiveTime(dpeName, firstTime);
    break;
  }
// DebugTN("VacEqpFindFirstArchiveTime(): result <" + dpName + ">", firstTime);
  LhcVacSetDpeHistoryDepth(dpName, firstTime);
}

void VacEqpFirstRackArchiveTime(string dpName, string dpeName, time startTime, time &firstTime)
{
  // First find when this racj was active. Search back from startTime for one month
  dyn_dyn_string  mobDpNames, mobWorkDpNames;
  dyn_dyn_bool    mobStates;
  dyn_time        nameTimes, stateTimes, workNameTimes;

  time monthAgo = startTime - 31 * 24 * 60 * 60;
// DebugTN("Time interval:", monthAgo, startTime);
  dpGetPeriod(monthAgo, startTime, 1, VAC_MOBILE_LIST_DP + ".DP_Names", mobDpNames, nameTimes);
  if(dynlen(nameTimes) <= 0)
  {
    return;
  }
  dpGetPeriod(monthAgo, startTime, 1, VAC_MOBILE_LIST_DP + ".State", mobStates, stateTimes);
  dpGetPeriod(monthAgo, startTime, 1, VAC_MOBILE_LIST_DP + ".WorkDPNames", mobWorkDpNames, workNameTimes);
// DebugTN(dpName, mobDpNames);
  int nHistoryNames = dynlen(mobDpNames);
  if(nHistoryNames > dynlen(mobStates))
  {
    nHistoryNames = dynlen(mobStates);
  }
  if(nHistoryNames > dynlen(mobWorkDpNames))
  {
    nHistoryNames = dynlen(mobWorkDpNames);
  }
  for(int histIdx = 1 ; histIdx <= nHistoryNames ; histIdx++)
  {
    int dpIdx = dynContains(mobDpNames[histIdx], dpName);
    if(dpIdx <= 0)
    {
      continue;
    }
    if(dpIdx > dynlen(mobStates[histIdx]))
    {
      continue;
    }
    if(!mobStates[histIdx][dpIdx])
    {
      continue;
    }
    firstTime = nameTimes[histIdx];
    return;
  }
}

//VacEqpFirstArchiveTime:
/** Find the very first archiving time for given DPE

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
  @param dpeName: in, name of DPE for which first archiving time shall be found
  @param firstTime: out, first archiving time will be returned here
*/
void VacEqpFirstArchiveTime(string dpeName, time &firstTime)
{
  // Retention period for vacuum values in RDB is 3 years, so let's assume we have history for 3 years
  float retentionPeriod = 3 * 365 * 24 * 60 * 60;
  firstTime = getCurrentTime() - retentionPeriod;
  // However, the retention period became 3 years after July 2017; it was 1 year before
  // Hence, we can't have archive before July 2016
  time firstBeforeExtension = makeTime(2016, 7, 1);
  if(firstTime < firstBeforeExtension)
  {
    firstTime = firstBeforeExtension;
  }

  /* This algorithm below is executed too long for RDB archiving
  //DebugTN("Start search first time for " + dpeName);
  time now = getCurrentTime() - 100;
  firstTime = now;
  time startTime = now;
  int delta = 200 * 24 * 60 * 60;
  while(true)
  {
    dyn_time times;
    dyn_float values;
    dynClear(times);
    dpGetPeriod(startTime, startTime + 1, 1, dpeName, values, times);
    //DebugTN("Query for ", startTime, dynlen(times), delta, times);
    if(dynlen(times) == 0)
    {
      delta /= 2;
      if(delta < 3600)
      {
        break;
      }
      startTime = firstTime - delta;
    }
    else
    {
      firstTime = times[1];
      if(firstTime > startTime)
      {
        break;  // No more times before start time
      }
      startTime -= delta;
    }
  }
  //DebugTN("First time for " + dpeName, firstTime);
  if(firstTime < now)
  {
    firstTime -= 7200;  // Search was stopped at this precision
  }
  */

  /*
  ** Method dpGet(dpe + ":_start.._online", firstTime) works well for file archving,
  ** but useless for RDB archiving.
  ** Method dpGetPeriod(farInThePast) works somtimes, not always, the logic is not clear to me.
  */
}

/*
**  Process history request for DPE at specified time interval
**	IN:
**        inDpe - name of dpe
**        startTime - begin  of time interval
**        endTime - end  of time interval
**        id  - Range id for Qt code processing
**
**	OUT:
**        exceptionInfo - error message
**
**	RETURN - None
*/
void VacProcessHistoryRequest(string inDpe, bool bitState, int bitNbr,
                              time startTime, time endTime,
                              int id, dyn_string &exceptionInfo)
{
  dyn_time  times;
  dyn_anytype  values;
  if(dpExists(inDpe))  // Not bakeout rack
  {
    // First check if online value has timestamp not after startTime. If yes - use online value and TS
    anytype onlineValue;
    time onlineTs;
    dpGet(inDpe, onlineValue, inDpe + ":_original.._stime", onlineTs);
    if(onlineTs <= startTime)
    {
      dynAppend(values, onlineValue);
      dynAppend(times, onlineTs);
    }
    else  // We really need a query
    {
      dpGetPeriod(startTime, endTime, 1, inDpe, values, times);
    }
//DebugN(inDpe, startTime, endTime, dynlen(values));
    LhcVacAddHistoryValues(id, inDpe, bitState, bitNbr, times, values, exceptionInfo);
    return;
  }

  // Bakeout rack - more complex processing
  string dpName, dpeName;
  int coco = ParseDpString(inDpe, dpName, dpeName);
  if(coco!= LVA_OK)
  {			
    fwException_raise(exceptionInfo,
      "ERROR", "VacProcessHistoryRequest(): invalid name of DPE '" + inDpe +  "'", "");
    LhcVacAddHistoryValues(id, inDpe, times, values, exceptionInfo);
    return;
  }

  // DebugTN("Total period: ", startTime, endTime);
  dyn_dyn_anytype			criteriaList;
  BuildCriteria(dpName, startTime, endTime, criteriaList, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    LhcVacAddHistoryValues(id, inDpe, bitState, bitNbr, times, values, exceptionInfo);
    return;
  }
  int criteriaLength = dynlen(criteriaList);
  if(criteriaLength <= 0)
  {
    LhcVacAddHistoryValues(id, inDpe, bitState, bitNbr, times, values, exceptionInfo);
    return;
  }
  for(int n = 1; n <= criteriaLength; n++)
  {
    string workDpeName = criteriaList[n][RANGE_WORK_DP] + "." + dpeName;
    dyn_time tsList;
    dyn_anytype valueList;
// DebugTN("Select: ", criteriaList[n][2], criteriaList[n][3], workDpeName);
    dpGetPeriod(criteriaList[n][RANGE_START],
      criteriaList[n][RANGE_END] < endTime ? criteriaList[n][RANGE_END] : endTime,
      1, workDpeName,
      valueList, tsList);
    dyn_errClass err = getLastError();
    if(dynlen(err) > 0)
    {
      DebugTN("dpGetPeriod(" + workDpeName + ") failed:", err);
    }
    int resultLen = dynlen(tsList);
// DebugTN("Got " + resultLen + " values");
    if(resultLen > 0)
    {
      // DebugTN("Returned " + resultLen);
      // Remove the very last value from list if it is out of required time interval
      if(tsList[resultLen] > criteriaList[n][3])
      {
        // DebugTN("Remove last value " + values[resultLen] + " at:", tsList[resultLen]);
        dynRemove(tsList, resultLen);
        dynRemove(valueList, resultLen);
      }
      if(dynlen(tsList) > 0)
      {
        // DebugTN("Added " + dynlen(tsList));
        dynAppend(times, tsList);
        dynAppend(values, valueList);
      }
    }
  }
// DebugTN("Total " + dynlen(times));
  LhcVacAddHistoryValues(id, inDpe, bitState, bitNbr, times, values, exceptionInfo);
}

/*
** Check if device matches device list criteria of type 'value grow'
**
**  Return values:
**    1 = device matches criteria
**    0 = device does not match criteria
**    -1 = error
*/
int VacEqpCheckValueGrowForDevList(string dpName)
{
  string dpeName;
  time startTime, endTime;
  LhcVacDevListGetHistoryParam(dpName, dpeName, startTime, endTime);
  if(dpeName == "")
  {
    return 0;
  }
  DebugTN("HISTORY for device list:", dpeName, startTime, endTime);
  float minValue = 1E+10;  // Something very large
  time firstLargeValueTime;  // Time when value first exceeded threshold, originally - empty
  while(startTime < endTime)
  {
    time queryEndTime = startTime + 8 * 60 * 60;  // Query in 8 hours interval
    if(queryEndTime > endTime)
    {
      queryEndTime = endTime;
    }
    dyn_anytype values;
    dyn_time times;
    dpGetPeriod(startTime, queryEndTime, 1, dpeName, values, times);
    DebugTN("History query for " + dpeName, startTime, queryEndTime, dynlen(values));
    if(dynlen(values) > 0)
    {
      if(times[dynlen(times)] > startTime)  // Can happen that only time before interval exists
      {
        int coco = LhcVacDevListCheckValueGrow(dpName, values, times, minValue, firstLargeValueTime);
        if(coco != 0)
        {
          return coco;  // either 1 or -1
        }
        string buf;
        sprintf(buf, "%8.2e", minValue);
        DebugTN(dpeName + ": min " + buf, firstLargeValueTime);
        queryEndTime = times[dynlen(times)] + 1;  // Will be start time for next query
      }
    }
    startTime = queryEndTime;
  }
  return 0;                
}

/*
**  Build criteria for dpGetPeriod() of bakeout rack
**	at specified time interval
**  IN:
**    dp - name of bakeout rack DP
**    startTime - begin  of time interval
**    endTime - end  of time interval
**  OUT:		
**    criteriaList - list of qriteria, criteriaList[n][dpName, tsStart, tsEnd]
**    exceptionInfo - error message
**
**  RETURN - None
*/
void BuildCriteria(string dpName, time startTime, time endTime,
	dyn_dyn_anytype &criteriaList, dyn_string &exceptionInfo)
{
  dyn_dyn_string  mobDpNames;
  dyn_dyn_string  mobWorkDpNames;
  dyn_dyn_bool    mobStates;
  dyn_time        nameTimes, stateTimes, workNameTimes;
  dyn_int         nameEvTypes, stateEvTypes, workEvTypes;

  dpGetPeriod(startTime, endTime, 1, VAC_MOBILE_LIST_DP + ".DP_Names", mobDpNames, nameTimes, nameEvTypes);
  int tsLength = dynlen(nameTimes);
  if(tsLength <= 0)
  {
    return;
  }
  dpGetPeriod(startTime, endTime, 1, VAC_MOBILE_LIST_DP + ".State", mobStates, stateTimes, stateEvTypes);
  dpGetPeriod(startTime, endTime, 1, VAC_MOBILE_LIST_DP + ".WorkDPNames", mobWorkDpNames, workNameTimes, workEvTypes);

  //  for(int n = 1; n <= tsLength; n++)
  //  {
  //    DebugTN("nameTimes: ", nameTimes[n], "mobDpNames: ", mobDpNames[n], "mobWorkDpNames: ", mobWorkDpNames[n], "mobStates: ", mobStates[n]); 
  //  }


  // All timestamp collections (nameTimes,stateTimes,workNameTimes)  should be absolutely identical
  // because single dpSet() has been performed to modify value in all list (.DP_Names, .State, .WorkDPNames)

  // Build lists of query intervals and workNames associated with dpNames
  // Start of the very first interval can not be before requested startTime, even if mobile
  // device became active before that time
  // If for the very last interval 'mobile disconnect' event not found - the end of the very
  // last interval will be == endTime, otherwise the end of the very last interval will be
  // set to the time of 'mobile disconnect' event, which can be before or after endTime.
  bool startFound = false;
  int intervalIdx = 1;
  if(tsLength > dynlen(stateTimes))
  {
    tsLength = dynlen(stateTimes);
  }
  if(tsLength > dynlen(workNameTimes))
  {
    tsLength = dynlen(workNameTimes);
  }
  
  for(int n = 1; n <= tsLength; n++)
  {
    int k = dynContains(mobDpNames[n], dpName);
    bool dpFound = k > 0 ? dpFound = mobStates[n][k] : false;

    if(dpFound)	// Mobile device is active at given moment
    {
      if(!startFound)	// This is the start of new activity interval
      {
        //assign name and startTs, set end of interval to requested endTime
        startFound = true;
        criteriaList[intervalIdx] = makeDynAnytype((string)mobWorkDpNames[n][k],
          (time)(nameTimes[n] < startTime  ? startTime : nameTimes[n]),
          (time)endTime);
      }
      else	// Start of activity for this device was already found, correct end of activity interval
      {
        if(nameTimes[n] > criteriaList[intervalIdx][3])
        {
          criteriaList[intervalIdx][3] = nameTimes[n];
        }
      }
    }
    else	// Mobile device is not active
    {
      if(startFound)	// It was active before, assign end time from archive event
      {
        startFound = false;
        criteriaList[intervalIdx][3] = nameTimes[n];
        intervalIdx++;
      }
    }
  }
  //  DebugTN("criteriaList: ", criteriaList);
}

int ParseDpString(string name, string &dp, string &dpe)
{
  dyn_string tokens = strsplit(name, ".");
  if(dynlen(tokens) < 2)
  {
    return LVA_FAILURE;
  }
  dp = tokens[1];
  dpe= tokens[2];
  for(int n = 3; n <= dynlen(tokens); n++)
  {
    dpe += "." + tokens[n];
  }
  return LVA_OK;
}
