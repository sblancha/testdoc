//	Implementation of MainViewTabledEwo class
/////////////////////////////////////////////////////////////////////////////////

#include "MainViewTabledEwo.h"

#include "DpConnection.h"
#include "DpeHistoryQuery.h"

EWO_PLUGIN(MainViewTabledEwo)


MainViewTabledEwo::MainViewTabledEwo(QWidget *parent) : VacMainViewEwo(parent)
{
	baseWidget = new MainViewTabled(parent);

	// Connect to signals of base widget
	connectStdSignals();
	connect(baseWidget, SIGNAL(dpDataChanged(void)), this, SLOT(dpDataChange(void)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList MainViewTabledEwo::signalList(void) const
{
	QStringList list = VacMainViewEwo::signalList();
	// One more signal specific for this EWO
	list.append("dpDataChanged()");
	return list;
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList MainViewTabledEwo::methodList(void) const
{
	QStringList list;
	list.append("int connectDp(string dp, int tag)");
	list.append("int disconnectDp(string dp)");
	list.append("int disconnectAll()");
	list.append("int getNextChangeTag()");
	list.append("string getNextChangeDp()");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewTabledEwo::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "connectDp")
	{
		return invokeConnectDp(name, values, error);
	}
	else if(name == "disconnectDp")
	{
		return invokeDisconnectDp(name, values, error);
	}
	else if(name == "disconnectAll")
	{
		return invokeDisconnectAll(name, values, error);
	}
	else if(name == "getNextChangeTag")
	{
		return invokeGetNextChangeTag(name, values, error);
	}
	else if(name == "getNextChangeDp")
	{
		return invokeGetNextChangeDp(name, values, error);
	}
	else
	{
		error = "Uknown method MainViewTabledEwo.";
		error += name;
	}
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			int connectDp(string dp, int tag)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewTabledEwo::invokeConnectDp(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dp) is not string";
		return QVariant(-1);
	}
	const QString dp = values[0].toString();
	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (tag) is not int";
		return QVariant(-1);
	}
	int tag = values[1].toInt();
	int coco = ((MainViewTabled *)baseWidget)->connectDp(dp.toLatin1(), tag);
	return QVariant(coco);
}

/*
**	FUNCTION
**		Execute method
**			int disconnectDp(string dp)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewTabledEwo::invokeDisconnectDp(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dp) is not string";
		return QVariant(-1);
	}
	const QString dp = values[0].toString();
	int coco = ((MainViewTabled *)baseWidget)->disconnectDp(dp.toLatin1());
	return QVariant(coco);
}

/*
**	FUNCTION
**		Execute method
**			int disconnectAll()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewTabledEwo::invokeDisconnectAll(const QString & /* name */, QList<QVariant> & /* values */,
	QString & /* error */)
{
	return QVariant(((MainViewTabled *)baseWidget)->disconnectAll());
}

/*
**	FUNCTION
**		Execute method
**			int getNextChangeTag()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewTabledEwo::invokeGetNextChangeTag(const QString & /* name */,
	QList<QVariant> & /* values */, QString & /* error */)
{
	int coco = ((MainViewTabled *)baseWidget)->getNextChangeTag();
	return QVariant(coco);
}

/*
**	FUNCTION
**		Execute method
**			string getNextChangeDp()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewTabledEwo::invokeGetNextChangeDp(const QString & /* name */,
	QList<QVariant> & /* values */, QString & /* error */)
{
	QString result;
	const char *dpName = ((MainViewTabled *)baseWidget)->getNextChangeDp();
	if(dpName)
	{
		result = dpName;
	}
	return QVariant(result);
}

/*
**	FUNCTION
**		Slot receiving dpDataChanged() signal of base widget instance, emit
**		signal 'dpDataChanged' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainViewTabledEwo::dpDataChange(void)
{
	emit signal("dpDataChanged");
}
