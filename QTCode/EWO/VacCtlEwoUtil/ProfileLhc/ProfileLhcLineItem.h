#ifndef	PROFILELHCLINEITEM_H
#define	PROFILELHCLINEITEM_H

//	Information for one device in profile LHC line

#include "InterfaceEqp.h"
#include "Eqp.h"

#include <qpainter.h>
#include <qcolor.h>
#include <qrect.h>

#include <stdio.h>

class ProfileLhcLineItem : public InterfaceEqp
{
	Q_OBJECT

public:
	ProfileLhcLineItem(Eqp *pEqp, DataEnum::DataMode mode);
	~ProfileLhcLineItem();

	void show(void);
	void hide(void);
	bool shallBeVisible(void);
	void draw(QPainter &painter, const QColor &backColor, bool flashBarsOnly, bool useAltFlashColor);

	void dump(FILE *pFile, int idx);

	// Access
	inline Eqp *getEqp(void) const { return pEqp; }
	inline float getStart(void) const { return start; }
	inline void setStart(float coord) { start = coord; }
	inline float getRealStart(void) const { return realStart; }
	inline int getBarIdx(void) const { return barIdx; }
	inline void setBarIdx(int idx) { barIdx = idx; }
	inline QRect &getRect(void) { return rect; }
	inline bool isAtSectorBorder(void) const { return atSectorBorder; }
	inline bool isHidden(void) const { return hidden; }
	inline void setHidden(bool flag) { hidden = flag; }
	inline bool isFlashing(void) const { return flashing; }
	inline void setFlashing(bool flag) { flashing = flag; }
	inline float getValue(void) const { return value; }

	inline int getLeftX(void) const { return rect.left(); }
	inline int getRightX(void) const { return rect.right(); }

public slots:	// Implement slots of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);
	virtual void mobileStateChange(Eqp *pSrc, DataEnum::DataMode mode);
	virtual void selectChange(Eqp *pSrc);

signals:	// Define own signals
	void eqpDataChanged(ProfileLhcLineItem *pSrc, bool colorChanged);
	void visibilityChanged(ProfileLhcLineItem *pSrc);
	void eqpSelectChanged(bool linear);
	void mobileStateChanged(ProfileLhcLineItem *pSrc);

protected:
	// Pointer to device
	Eqp		*pEqp;

	// Last known device status
	Eqp::CtlStatus	ctlStatus;

	// Coordinate to be used for calculations
	float	start;

	// Real coordinate in DCUM coordinate system
	float	realStart;

	// Value of device to shown in graph
	float	value;

	// Used bar index
	int		barIdx;

	// Bar rectangle
	QRect	rect;

	// Color used for bar drawing
	QColor		color;

	// Data acquisition mode
	// (in fact, it is the same value for all items in one graph, but
	// this item needs to know the mode
	DataEnum::DataMode		mode;

	// True if devices is located on sector border
	bool	atSectorBorder;

	// true if device is hidden
	bool	hidden;

	// Flag indicating if device is selected
	bool	selected;

	// Flag indicating if value is valid
	bool	valid;

	// Flag indicating if device's color has been changed
	bool	colorChanged;

	// true if bar shall be flashing
	bool	flashing;

	void analyzeCtlStatusChange(void);

private slots:
	void sectorsChange(Eqp *pSrc);
};

#endif	// PROFILELHCLINEITEM_H
