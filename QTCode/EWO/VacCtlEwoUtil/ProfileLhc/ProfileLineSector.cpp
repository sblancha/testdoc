//	Implementation of ProfileLineSector class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileLineSector.h"

#include "Sector.h"
#include "DebugCtl.h"

/////////////////////////////////////////////////////////////////////////////////
////////////////// Construction/destruction

ProfileLineSector::ProfileLineSector()
{
	pSector1 = pSector2 = NULL;
	leftEqpIdx = rightEqpIdx = -1;
	leftX = rightX = 0;
	transition = false;
}

ProfileLineSector::~ProfileLineSector()
{
}

bool ProfileLineSector::needLabel(void)
{
	return pSector1 && (!pSector2);
}

void ProfileLineSector::calculateLabelRect(QFontMetrics &fontMetrics, int areaBottom)
{

	if(!needLabel())
	{
		labelRect.setWidth(1);
		labelRect.setHeight(1);
		labelRect.setBottom(areaBottom);
		labelRect.setTop(areaBottom - 1);
		return;
	}
	labelRect = fontMetrics.boundingRect(pSector1->getName());
	// qDebug("Bounding rect for %s is (%d %d) (%d x %d)\n", pSector1->getName(), labelRect.left(), labelRect.top(), labelRect.width(), labelRect.height());
	labelRect.setWidth(labelRect.width() + 6);
	//labelRect.setHeight(labelRect.height() + 2);
	labelRect.moveBottom(areaBottom);
	int center = (leftX + rightX) >> 1;
	labelRect.moveLeft(center - (labelRect.width() >> 1));
	// qDebug("Final rect for %s is (%d %d) (%d x %d)\n", pSector1->getName(), labelRect.left(), labelRect.top(), labelRect.width(), labelRect.height());
}

void ProfileLineSector::moveLabelRectUp(void)
{
	labelRect.moveTop(labelRect.top() - labelRect.height());
}

void ProfileLineSector::drawLabel(QPainter &painter)
{
	if(needLabel())
	{
		painter.setPen(Qt::black);
		// qDebug("Draw sector label %s at (%d %d) (%d x %d)\n", pSector1->getName(), labelRect.left(), labelRect.top(), labelRect.width(), labelRect.height());
		painter.drawText(labelRect, Qt::AlignCenter, pSector1->getName());
	}
}


/*
**	FUNCTION
**		Dump content to output file - for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for write
**		idx		- Index of this sector in list
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLineSector::dump(FILE *pFile, int idx)
{
	if(!DebugCtl::isProfile())
	{
		return;
	}
	fprintf(pFile, "  %02d: <%s> <%s> from %d to %d eqp1 %d eqp2 %d vacType %d %s\n", idx,
		(pSector1 ? pSector1->getName() : "none"),
		(pSector2 ? pSector2->getName() : "none"),
		leftX, rightX, leftEqpIdx, rightEqpIdx,
		(pSector1 ? pSector1->getVacType() : 0),
		(transition ? " TRANSITION" : ""));
}
