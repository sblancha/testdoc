#
# Project file for qmake utility to build MainViewTabled EWO
#
#	17.03.2009	L.Kopylov
#		Initial version
#
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = MainViewTabledEwo

QT += printsupport

INCLUDEPATH += ../../common/VacCtlUtil \
	../../common/Platform \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp \
	../VacCtlEwoUtil

HEADERS = MainViewTabled.h \
	MainViewTabledEqpConnect.h \
	MainViewTabledEwo.h

SOURCES = MainViewTabled.cpp \
	MainViewTabledEqpConnect.cpp \
	MainViewTabledEwo.cpp

LIBS += -L../../../bin \
	-lVacCtlEwoUtil \
	-lVacCtlHistoryData \
	-lVacCtlEqpData \
	-lVacCtlUtil

