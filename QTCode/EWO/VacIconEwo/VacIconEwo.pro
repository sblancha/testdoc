#
# Project file for qmake utility to build VacIconEwo EWO
#
#	14.12.2008	L.Kopylov
#		Initial version
#
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = VacIconEwo

INCLUDEPATH += ../../common/VacCtlUtil \
		../../common/Platform \
		../../common/VacCtlEqpData/DataPool \
		../../common/VacCtlEqpData/Eqp \
		../VacCtlEwoUtil/VacIcon \
		../VacCtlEwoUtil

HEADERS =	VacIconEwo.h

SOURCES = VacIconEwo.cpp

LIBS += -L../../../bin \
	-lVacCtlEwoUtil \
	-lVacCtlEqpData \
	-lVacCtlHistoryData \
	-lVacCtlUtil

