#ifndef	SECTLHCLINE_H
#define	SECTLHCLINE_H

// One line in LHC sector view

#include "SynLhcLineBeam.h"
#include "SectLhcInjDumpLine.h"

class SectLhcLine : public SynLhcLineBeam
{
	Q_OBJECT

public:
	// Types of individual lines in view
	typedef enum
	{
		OuterBeam = 0,
		CrossBeam = 1,
		InnerBeam = 2,
		Passive = 3
	} SectLhcLineType;

	SectLhcLine(QWidget *parent, int type, float start, float end);
	virtual ~SectLhcLine();

	virtual void clear(void);
	virtual int build(unsigned vacTypeMask, VacEqpTypeMask &eqpMask);
	virtual void calcVerticalLimits(void);
	virtual void moveVertically(int &delta);
	virtual int draw(QPainter &painter, int viewStart, int viewEnd);

	virtual void finishGeometry(void);

	// Implementation of SynLhcLine's dummy methods
	virtual void finalSectorIconsGeometry(void);

	virtual void dump(FILE *pFile);

protected:
	// List of child (injection/dump) line
	QList<SectLhcInjDumpLine *>	childLineList;

	virtual void decideForEqpLocation(void);
	virtual void placeEqp(float &coord, int &connPos, float &endCoord, int &endConnPos);

	void placeSector(float &coord, int &connPos);
	virtual void findSectorSummaryLimits(SynLhcEqpItem *pItem, int &iconStart, int &iconEnd);
	virtual void addEqp(Eqp *pEqp, int partIdx);
	virtual bool isSectorForThisLine(Eqp *pEqp);
	virtual void addSectorDevice(Eqp *pEqp, int partIdx, int type);
	virtual void addChildLine(Eqp *pConnEqp, int partIdx);
	virtual bool isMyEqp(Eqp *pEqp, int &partIdx);
	virtual bool vacTypeMatchesMask(unsigned vacType);
	virtual int vacTypeOfRegion(int regionType);
	virtual const char *getLineName(void);
};

#endif	// SECTLHCLINE_H
