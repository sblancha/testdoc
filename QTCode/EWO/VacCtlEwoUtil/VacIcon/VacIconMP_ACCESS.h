#ifndef	VACICONMP_ACCESS_H
#define	VACICONMP_ACCESS_H

//	Icon for main part access mode

#include "VacIcon.h"

class EqpMP_ACCESS;

class VACCTLEWOUTIL_EXPORT VacIconMP_ACCESS : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconMP_ACCESS(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconMP_ACCESS();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

public slots:
	// Override slot of VacIcon
	virtual void forceRedraw(void);

protected:
	// Pointer to my device
	EqpMP_ACCESS *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACICONMP_ACCESS_H
