/**
* @brief Class implementation for widget Gauge Pirani
* @see VacIcon
*/
#ifndef	VACICONGAUGEPIRANI_H
#define	VACICONGAUGEPIRANI_H

#include "VacIconVG_U.h"

class VACCTLEWOUTIL_EXPORT VacIconGaugePirani : public VacIconVG_U {
public:
	VacIconGaugePirani(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG_U(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONAUGEPIRANI_H
