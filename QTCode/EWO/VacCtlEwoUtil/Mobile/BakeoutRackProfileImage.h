#ifndef BAKEOUTRACKPROFILEIMAGE_H
#define	BAKEOUTRACKPROFILEIMAGE_H

//	Class performing bakeout rack profile drawing on itself

#include "BakeoutRackChannel.h"
#include "BakeoutStateConnection.h"

#include "VerticalAxisWidget.h"

#include <QWidget>
#include <QList>
#include <QDateTime>

#include <stdio.h>

class Eqp;

class ExtendedLabel;
#include <QTimer>
class BakeoutPlotImage;

class BakeoutRackProfileImage : public QWidget
{
	Q_OBJECT

public:
	BakeoutRackProfileImage(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	~BakeoutRackProfileImage();

	void setEqp(Eqp *pEqp);
	inline Eqp *getEqp(void) const { return pEqp; }
	inline unsigned getRr1(void) const { return state.getRr1(); }
	inline bool isPlcAlarm(void) const { return state.isPlcAlarm(); }

private slots:
	void eqpDataChange(BakeoutRackChannel *pSrc, bool colorChanged);
	void stateChange(void);

	void redrawTimerDone(void);

signals:
	void stateChanged(void);

protected:

	// Pointer to device shown in profile
	Eqp			*pEqp;

	// Connection to bakeout rack state (RR1 and PLC)
	BakeoutStateConnection		state;

	// List of all channels in profile
	QList<BakeoutRackChannel *>	channels;

	// Vertical axis
	VerticalAxisWidget		*pAxis;

	// Label for displaying information on graph bars
	ExtendedLabel					*pLabel;

	// Plot image
	BakeoutPlotImage		*pImage;

	// Timer to hide label after 'reasonable' delay
	QTimer					*pTimer;

	// Timestamp of last image drawing
	QTime					drawTime;

	// Timer to redraw image after online value arrival
	QTimer					*pRedrawTimer;

	// Widget width used for last drawing
	int						viewWidth;

	// Widget height used for last drawing
	int						viewHeight;

	// Flag indicating if reference to new device was set
	bool					newEqpSet;

	void renewImage(void);
	void rebuildImage(void);
	bool prepare(QPainter &painter);
	void buildBarGeom(const QRect &plotRect);
	bool barVerticalGeom(BakeoutRackChannel *pItem);
	void drawBars(QPainter &painter);
	

	virtual void resizeEvent(QResizeEvent *event);

	BakeoutRackChannel *findItemUnderMouse(int x, int y);
	void showBarLabel(BakeoutRackChannel *pItem, int y);

	void dump(const char *fileName);

private slots:
	void imageResized(void);
	void imageMouseMove(QMouseEvent *pEvent);
	void imageMouseLeave(void);
	void axisLimitsChanged(bool scrolling);
};
	
#endif	// BAKEOUTRACKPROFILEIMAGE_H
