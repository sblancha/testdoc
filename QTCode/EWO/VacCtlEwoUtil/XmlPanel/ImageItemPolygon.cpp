//	Implementation of ImageItemPolygon class
/////////////////////////////////////////////////////////////////////////////////

#include "ImageItemPolygon.h"

#include <QLocale>

//////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
//////////////////////////////////////////////////////////////////////////////////

ImageItemPolygon::ImageItemPolygon() : ImageItem()
{
	closed = filled = false;
}

ImageItemPolygon::~ImageItemPolygon()
{
}


void ImageItemPolygon::draw(QPainter & painter)
{
	if(points.isEmpty())
	{
		return;
	}
	painter.setTransform(transform);
	if(backColor.alpha() == 0)
	{
		painter.setBrush(Qt::NoBrush);
	}
	else if(filled)
	{
		painter.setBrush(backColor);
	}
	else
	{
		painter.setBrush(Qt::NoBrush);
	}
	setForePen(painter);
	/*
	if(filled)
	{
		QRect rect = points.boundingRect();
		painter.fillRect(rect, backColor);
	}
	*/
	if(closed)
	{
//printf("drawPolygon() %d points\n", points.count());
		painter.drawPolygon(points);	// , Qt::OddEvenFill);	// Qt::WindingFill);
	}
	else
	{
//printf("drawPolyline() %d points\n", points.count());
		painter.drawPolyline(points);
	}
}

bool ImageItemPolygon::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	if(!visible)
	{
		return false;
	}
	if(toolTipText.isEmpty())
	{
		return false;
	}
	if(!points.containsPoint(transform.map(point), Qt::OddEvenFill))	// Qt::FillRule::WindingFill))
	{
		return false;
	}
	text = toolTipText;
	rect = points.boundingRect();
	return true;

}

void ImageItemPolygon::parseProp(QXmlStreamReader &reader)
{
	QStringRef propName = reader.attributes().value("name");
	if(!propName.compare("Closed", Qt::CaseInsensitive))
	{
		closed = reader.readElementText().contains("True", Qt::CaseInsensitive);
	}
	else if(!propName.compare("FillType", Qt::CaseInsensitive))
	{
		filled = !reader.readElementText().contains("outline", Qt::CaseInsensitive);
	}
	else if(!propName.compare("Points", Qt::CaseInsensitive))
	{
		parsePoints(reader);
	}
	else
	{
		ImageItem::parseProp(reader);
	}
}

void ImageItemPolygon::parsePoints(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement())
		{
			break;
		}
		if(reader.isStartElement())
		{
			if(!reader.name().compare("prop", Qt::CaseInsensitive))
			{
				QStringRef propName = reader.attributes().value("name");
				if(!propName.compare("Location", Qt::CaseInsensitive))
				{
					QString str(reader.readElementText());
					QLocale c(QLocale::C);
					int x = (int)rint(c.toDouble(str.section(' ', 0, 0)));
					int y = (int)rint(c.toDouble(str.section(' ', 1, 1)));
					points.append(QPoint(x, y));
					continue;
				}
			}
			parseUnknown(reader);
		}
	}
}

