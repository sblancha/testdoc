//	Implementation of BakeoutRackTable class
/////////////////////////////////////////////////////////////////////////////////

#include "BakeoutRackTable.h"

#include "VacMainView.h"

#include "PlatformDef.h"
#include "ResourcePool.h"

#include <QHeaderView>

float	BakeoutRackTable::colWidthCoeff[6];
bool	BakeoutRackTable::colWidthReady = false;

// Column indices
#define	COL_CHANNEL	(0)
#define	COL_T		(1)
#define	COL_SET		(2)
#define	COL_PCS		(3)
#define	COL_SAF		(4)
#define	COL_STATE	(5)

BakeoutRackTable::BakeoutRackTable(QWidget *parent) : QTableWidget(parent)
{
	if(VacMainView::getInstance())
	{
		setFont(VacMainView::getInstance()->font());
	}
	connect(&state, SIGNAL(changed(void)), this, SLOT(stateChange(void)));

	// Table initialization
	setColumnCount(6);
	QString colTitle;
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.RC.Table.ColCH.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "Channel";
	}
	setHorizontalHeaderItem(COL_CHANNEL, new QTableWidgetItem(colTitle));
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.RC.Table.ColTset.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "T set";
	}
	setHorizontalHeaderItem(COL_SET, new QTableWidgetItem(colTitle));
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.RC.Table.ColT.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "T";
	}
	setHorizontalHeaderItem(COL_T, new QTableWidgetItem(colTitle));
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.RC.Table.ColPCS.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "PCS";
	}
	setHorizontalHeaderItem(COL_PCS, new QTableWidgetItem(colTitle));
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.RC.Table.ColSAF.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "SAF";
	}
	setHorizontalHeaderItem(COL_SAF, new QTableWidgetItem(colTitle));
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.RC.Table.ColState.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "State";
	}
	setHorizontalHeaderItem(COL_STATE, new QTableWidgetItem(colTitle));
	horizontalHeader()->show();
	verticalHeader()->hide();

	// Some reasonable default height
	QRect rect = fontMetrics().boundingRect("Channel");
	//setMinimumSize(QSize(rect.width() * 8, rect.height() * 42));
	recommendedSize.setWidth(rect.width() * 8);
	recommendedSize.setHeight(rect.height() * 42);
}

BakeoutRackTable::~BakeoutRackTable()
{
	while(!channels.isEmpty())
	{
		delete channels.takeFirst();
	}
}

void BakeoutRackTable::resizeEvent(QResizeEvent *pEvent)
{
	QTableWidget::resizeEvent(pEvent);

	if(!colWidthReady)
	{
		int colWidth[6];
		int wd;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.RC.Table.ColCH.Width", wd) != ResourcePool::OK)
		{
			wd = 130;
		}
		colWidth[COL_CHANNEL] = wd > 0 ? wd : 130;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.RC.Table.ColTset.Width", wd) != ResourcePool::OK)
		{
			wd = 109;
		}
		colWidth[COL_SET] = wd > 0 ? wd : 109;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.RC.Table.ColT.Width", wd) != ResourcePool::OK)
		{
			wd = 109;
		}
		colWidth[COL_T] = wd > 0 ? wd : 109;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.RC.Table.ColPCS.Width", wd) != ResourcePool::OK)
		{
			wd = 90;
		}
		colWidth[COL_PCS] = wd > 0 ? wd : 90;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.RC.Table.ColSAF.Width", wd) != ResourcePool::OK)
		{
			wd = 90;
		}
		colWidth[COL_SAF] = wd > 0 ? wd : 90;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.RC.Table.ColState.Width", wd) != ResourcePool::OK)
		{
			wd = 652;
		}
		colWidth[COL_STATE] = wd > 0 ? wd : 652;
		float total = 0;
		for(int col = 0 ; col < 6 ; col++)
		{
			total += colWidth[col];
		}
		for(int col = 0 ; col < 6 ; col++)
		{
			colWidthCoeff[col] = (float)colWidth[col] / total;
		}
		colWidthReady = true;
	}
	int totalWidth = width();
	int wd = (int)rint(totalWidth * colWidthCoeff[COL_CHANNEL]);
	setColumnWidth(COL_CHANNEL, wd);
	wd = (int)rint(totalWidth * colWidthCoeff[COL_SET]);
	setColumnWidth(COL_SET, wd);
	wd = (int)rint(totalWidth * colWidthCoeff[COL_T]);
	setColumnWidth(COL_T, wd);
	wd = (int)rint(totalWidth * colWidthCoeff[COL_PCS]);
	setColumnWidth(COL_PCS, wd);
	wd = (int)rint(totalWidth * colWidthCoeff[COL_SAF]);
	setColumnWidth(COL_SAF, wd);
	wd = (int)rint(totalWidth * colWidthCoeff[COL_STATE]);
	setColumnWidth(COL_STATE, wd);
}

/*
**	FUNCTION
**		Set device to be shown in view
**
**	PARAMETERS
**		pEqp	- Pointer to device to be shown
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutRackTable::setEqp(Eqp *pEqp)
{
	if(pEqp == this->pEqp)
	{
		return;
	}
	while(!channels.isEmpty())
	{
		delete channels.takeFirst();
	}
	setRowCount(0);
	this->pEqp = pEqp;
	if(pEqp)
	{
		setRowCount(24);
		for(int n = 1 ; n <= 24 ; n++)
		{
			setItem(n - 1, COL_CHANNEL, new QTableWidgetItem(QString::number(n)));
			item(n - 1, COL_CHANNEL)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			setItem(n - 1, COL_SET, new QTableWidgetItem());
			item(n - 1, COL_SET)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			setItem(n - 1, COL_T, new QTableWidgetItem());
			item(n - 1, COL_T)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			setItem(n - 1, COL_PCS, new QTableWidgetItem());
			item(n - 1, COL_PCS)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			setItem(n - 1, COL_SAF, new QTableWidgetItem());
			item(n - 1, COL_SAF)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			setItem(n - 1, COL_STATE, new QTableWidgetItem());
			item(n - 1, COL_STATE)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
	
			BakeoutRackChannel *pChannel = new BakeoutRackChannel(pEqp, n);
			channels.append(pChannel);
			connect(pChannel, SIGNAL(eqpDataChanged(BakeoutRackChannel *, bool)),
				this, SLOT(eqpDataChange(BakeoutRackChannel *, bool)));
		}
		resizeRowsToContents();
	}
	state.setEqp(pEqp);
}

void BakeoutRackTable::eqpDataChange(BakeoutRackChannel *pSrc, bool /* colorChanged */)
{
	int row = pSrc->getChannel() - 1;
	QTableWidgetItem *pItem = item(row, COL_SET);
	QColor bgColor;
	if(pSrc->isActive())
	{
		bgColor = Qt::white;
	}
	else
	{
		bgColor.setRgb(196, 196, 196);	// Grey
	}
	pItem->setBackground(bgColor);
	pItem->setText(QString::number(pSrc->getValueSet()));
	QModelIndex startIdx = indexFromItem(pItem);

	pItem = item(row, COL_T);
	pItem->setBackground(bgColor);
	pItem->setText(QString::number(pSrc->getValue()));

	int	status = pSrc->getStatus();
	pItem = item(row, COL_PCS);
	pItem->setBackground(bgColor);
	pItem->setText(status & PCS_BIT ? "ON" : "OFF");
	
	pItem = item(row, COL_SAF);
	pItem->setBackground(bgColor);
	pItem->setText(status & SAF_BIT ? "ON" : "OFF");
	
	pItem = item(row, COL_STATE);
	pItem->setBackground(pSrc->getColor());
	pItem->setText(pSrc->getShortStatusString());

	QModelIndex endIdx = indexFromItem(pItem);
	dataChanged(startIdx, endIdx);	// Force table update
}

void BakeoutRackTable::stateChange(void)
{
	unsigned rr1 = state.getRr1();
	unsigned mask = 1;
	for(int idx = 0 ; idx < channels.count() ; idx++)
	{
		BakeoutRackChannel *pChannel = channels.at(idx);
		pChannel->setActive((rr1 & mask) != 0);
		mask = mask << 1;
		eqpDataChange(pChannel, false);
	}
	emit stateChanged();
}
