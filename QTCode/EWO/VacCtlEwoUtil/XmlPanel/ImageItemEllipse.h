#ifndef	IMAGINEITEMELLIPSE_H
#define	IMAGINEITEMELLIPSE_H

#include "ImageItem.h"

class VACCTLEWOUTIL_EXPORT ImageItemEllipse : public ImageItem
{
public:
	ImageItemEllipse();
	~ImageItemEllipse();

	virtual void draw(QPainter & painter);
	virtual bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	// Access
	inline float getCenterX(void) const { return centerX; }
	inline void setCenterX(float coord) { centerX = coord; }
	inline float getCenterY(void) const { return centerY; }
	inline void setCenterY(float coord) { centerY = coord; }
	inline int getRadiusX(void) const { return radiusX; }
	inline void setRadiusX(int value) { radiusX = value >= 0 ? value : 0; }
	inline int getRadiusY(void) const { return radiusY; }
	inline void setRadiusY(int value) { radiusY = value >= 0 ? value : 0; }

protected:
	// Center
	float	centerX;
	float	centerY;

	// Two radii
	int		radiusX;
	int		radiusY;

	virtual void parseProp(QXmlStreamReader &reader);
};

#endif	// IMAGINEITEMELLIPSE_H
