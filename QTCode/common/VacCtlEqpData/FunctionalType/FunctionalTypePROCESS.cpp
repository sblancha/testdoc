//	Implementation of FunctionalTypePROCESS class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypePROCESS.h"


#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

#include "ResourcePool.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypePROCESS::FunctionalTypePROCESS(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypePROCESS::fillCriteria(FunctionalType *pType)
{
	// Own criteria
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "Process is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Process in sector {sector} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Process in sector {sector} is NOT ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Process in main part {mainPart} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Process in main part {mainPart} is NOT ON");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "Process is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Process in sector {sector} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Process in sector {sector} is NOT OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Process in main part {mainPart} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Process in main part {mainPart} is NOT OFF");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, SequencerStep, "Process step between");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: step is {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: step is {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Process step {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Process step is {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Process step {value} in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Process step is {value} in main part {mainPart}");
}

void FunctionalTypePROCESS::getCriteriaValues(int critType, int critSubType, QList<int> &values, QList<QByteArray> &labels)
{
	FunctionalType::getCriteriaValues(critType, critSubType, values, labels);	// Clear lists
	if((critType != EqpMsgCriteria::MainValue))
	{
		return;
	}
	QString resourceBase(getStepResourceBase());
	if(resourceBase.isEmpty())
	{
		return;
	}
	ResourcePool &pool = ResourcePool::getInstance();
	for(int step = 1 ; step < 256 ; step++)
	{
		QString resourceName = resourceBase;
		resourceName += QString::number(step);
		QString stepName;
		if(pool.getStringValue(resourceName, stepName) == ResourcePool::OK)
		{
			values.append(step);
			labels.append(stepName.toLatin1());
		}
	}
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypePROCESS::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case Off:
			result = "OFF";
			break;
		case On:
			result = "ON";
			break;
		}
		break;
	case EqpMsgCriteria::MainValue:
		if(pCriteria->getSubType() == SequencerStep)
		{
			result = "STEP";
		}
		break;
	default:
		break;
		
	}
	return result;
}

int FunctionalTypePROCESS::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "OFF"))
		{
			result = Off;
		}
		else if(!strcasecmp(name, "ON"))
		{
			result = On;
		}
		break;
	case EqpMsgCriteria::MainValue:
		if(!strcasecmp(name, "STEP"))
		{
			result = SequencerStep;
		}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}

