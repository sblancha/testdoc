#ifndef	MOBILEHISTORYEQPDATA_H
#define	MOBILEHISTORYEQPDATA_H

// Class holding history data for one fixed device or one channel value of BAKEOUT

#include "HistoryEqpData.h"
#include "BakeoutWorkDp.h"

class MobileHistoryEqpData : public HistoryEqpData
{
	Q_OBJECT

public:
	MobileHistoryEqpData(Eqp *pEqp, QDateTime &absStartTime);
	MobileHistoryEqpData(Eqp *pEqp, QDateTime &absStartTime, int channel, enum BakeoutWorkDp::ChannelValueType type);
	virtual ~MobileHistoryEqpData();

	void addExportValues(const QDateTime &minTime, const QDateTime &maxTime, HistoryProcessor &processor);
	virtual bool isSelected(void) const { return channel ? selected : pEqp->isSelected(); }
	// method is defined in superclass virtual void setSelected(bool flag) { selected = flag; }

	// Access
	inline int getChannel(void) const { return channel; }
	inline enum BakeoutWorkDp::ChannelValueType getChannelValueType(void) const { return channelValueType; }
	inline bool isUsed(void) const { return used; }
	inline void setUsed(bool flag) { used = flag; }

protected:
	// Channel number
	int		channel;

	// Type of value for channel in this item
	enum BakeoutWorkDp::ChannelValueType channelValueType;

	// Flag indicating if this item is selected
	// The flag is defined in superclass bool	selected;

	// Flag indicating if this item is used after content renew
	bool	used;
};

#endif	// MOBILEHISTORYEQPDATA_H
