#ifndef	INTERLOCKICONVRIVD_H
#define	INTERLOCKICONVRIVD_H

#include "qwidget.h"
#include "InterlockIcon.h"

class InterlockIconVRIVD : public InterlockIcon
{
	Q_OBJECT
public:
	InterlockIconVRIVD(QWidget * parent = NULL);
	inline InterlockIconClass getIconClass() { return TypeDInterlock; };
	void moveIcon(int x, int y);
	QPoint getConnectionPoint(QString connectorName);
	void getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber);
	QRect getBoundingRect();
	virtual ~InterlockIconVRIVD();
private:
	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif