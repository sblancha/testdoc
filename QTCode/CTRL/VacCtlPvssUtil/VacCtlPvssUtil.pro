#
# 	14.11.2016	L.Kopylov
#		Switch from Makefile-based to qmake-based compilation - see https://wikis.web.cern.ch/wikis/display/EN/WinCC+OA+3.14+Validation+Development
#

include($$(API_ROOT)/WinCCOA-API.pri)

TEMPLATE = lib

TARGET = VacCtlPvssUtil

DESTDIR = ../../../bin

OBJECTS_DIR = obj

# To produce PDB file together with DLL
win32 {
	CONFIG += qt static thread release
    DEFINES += WIN32
    QMAKE_LFLAGS += /nodefaultlib:libc
    QMAKE_LFLAGS += /nodefaultlib:libcp
    # This is to have PDB files also on Release target                                                                                                                                                                         	QMAKE_CXXFLAGS+=/Zi
	QMAKE_LFLAGS+= /INCREMENTAL:NO /Debug
}

unix {
	CONFIG += qt static thread release warn_off separate_debug_info
    # This is to separate debug info
    CONFIG += separate_debug_info
    QMAKE_CLEAN += ${TARGET} ${TARGET}.debug ${QMAKE_MOC_SRC}
}

INCLUDEPATH += ./ \
	../../common/Platform

HEADERS = VacCtlPvssUtil.h

SOURCES = VacCtlPvssUtil.cpp

