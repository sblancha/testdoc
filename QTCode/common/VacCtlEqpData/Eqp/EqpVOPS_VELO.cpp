//	Implementation of EqpVOPS_VELO class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVOPS_VELO.h"

#include "DataPool.h"
#include "FunctionalType.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OK				0,255,0
#define COLOR_BAD				255,0,0
#define	COLOR_ERROR				255,102,102
#define	COLOR_WARNING			255,255,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_A			(0x00080000)
#define	RR1_A_CABLE		(0x00040000)
#define	RR1_B			(0x00020000)
#define	RR1_B_CABLE		(0x00010000)


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

EqpVOPS_VELO::EqpVOPS_VELO(const Eqp &source) : Eqp(source)
{
	rr1Online = rr1Replay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::VOPS_VELO;
}

EqpVOPS_VELO::~EqpVOPS_VELO()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVOPS_VELO::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	addDpeNameToList(stateDpes, "RR1");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVOPS_VELO::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "RR1", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVOPS_VELO::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "EqpStatus", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVOPS_VELO::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVOPS_VELO::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getMainColor(unsigned rr1, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(rr1 & RR1_ERROR)
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	// No errors or warnings - check detail state
	else if((rr1 & RR1_A_CABLE) && (rr1 & RR1_B_CABLE))
	{
		if((rr1 & RR1_A) && (!(rr1 & RR1_B)))
		{
			color.setRgb(COLOR_OK);
		}
		else
		{
			color.setRgb(COLOR_BAD);
		}
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getMainStateString(unsigned rr1, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
		return;
	}
	if(rr1 & RR1_ERROR)
	{
		string = "Error";
	}
	else if(rr1 & RR1_WARNING)
	{
		string = "Warning";
	}
	else
	{
		if(rr1 & RR1_A_CABLE)
		{
			if(rr1 & RR1_A)
			{
				string = "A+: OK";
			}
			else
			{
				string = "A+: bad";
			}
		}
		else
		{
			string = "A+: undef";
		}
		if(rr1 & RR1_B_CABLE)
		{
			if(!(rr1 & RR1_B))
			{
				string += ", B-: OK";
			}
			else
			{
				string += ", B-: bad";
			}
		}
		else
		{
			string += ", B-: undef";
		}
	}
}
#endif


/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("RR1");
	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, false, state);
	getMainColor(rr1, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif


/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "EqpStatus");
	addDpeNameToList(dpes, "RR1");
}
#endif


/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVOPS_VELO::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if((rr1 & RR1_A_CABLE) && (rr1 & RR1_B_CABLE))
	{
		if((rr1 & RR1_A) && (!(rr1 & RR1_B)))
		{
			state = DevListCriteria::EqpStateNormal;
		}
		else
		{
			state = DevListCriteria::EqpStateAbnormal;
		}
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if(rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if(criteria.isWarnings())
	{
		if(rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Calculate color for given part of device
**
**	ARGUMENTS
**		part	- which part of device is requested (see EqpPart enum in Eqp.h)
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getPartColor(int part, QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	unsigned useRr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	bool useAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	switch(part)
	{
	case EqpPartVOPS_A:
		getPartColorA(useRr1, useAlarm, color);
		break;
	case EqpPartVOPS_B:
		getPartColorB(useRr1, useAlarm, color);
		break;
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for A+ part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getPartColorA(unsigned rr1, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr1 & RR1_A_CABLE)
	{
		if(rr1 & RR1_A)
		{
			color.setRgb(COLOR_OK);
		}
		else
		{
			color.setRgb(COLOR_BAD);
		}
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for B- part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getPartColorB(unsigned rr1, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr1 & RR1_B_CABLE)
	{
		if(!(rr1 & RR1_B))
		{
			color.setRgb(COLOR_OK);
		}
		else
		{
			color.setRgb(COLOR_BAD);
		}
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif



/*
**	FUNCTION
**		Calculate state string for given part of device
**
**	ARGUMENTS
**		part	- which part of device is requested (see EqpPart enum in Eqp.h)
**		string	- Variable where resulting string will be put
**		mode	- Required data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getPartStateString(int part, QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	unsigned useRr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	bool useAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	switch(part)
	{
	case EqpPartVOPS_A:
		getPartStateStringA(useRr1, useAlarm, string);
		break;
	case EqpPartVOPS_B:
		getPartStateStringB(useRr1, useAlarm, string);
		break;
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for A+ part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getPartStateStringA(unsigned rr1, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr1 & RR1_A_CABLE)
	{
		if(rr1 & RR1_A)
		{
			string = "A+: OK";
		}
		else
		{
			string = "A+: overpressure";
		}
	}
	else
	{
		string = "A+: undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for B- part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVOPS_VELO::getPartStateStringB(unsigned rr1, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr1 & RR1_B_CABLE)
	{
		if(!(rr1 & RR1_B))
		{
			string = "B-: OK";
		}
		else
		{
			string = "B-: overpressure";
		}
	}
	else
	{
		string = "B-: undefined";
	}
}
#endif

