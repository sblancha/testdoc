#ifndef	FINALEQPMESSAGE_H
#define	FINALEQPMESSAGE_H

#include "VacCtlOnlineNotificationExport.h"

// Final readable message built from equipment state change

#include <qstring.h>
#include <qdatetime.h>

class VACCTLONLINENOTIFICATION_EXPORT FinalEqpMessage
{
public:
	FinalEqpMessage(const QString &inText);
	~FinalEqpMessage() {}
		
	// Access
	inline const QString &getText(void) const { return text; }
	inline QDateTime &getSendTime(void) { return sendTime; }
	inline void setSendTimeNow(void) { sendTime.setTime_t(QDateTime::currentDateTime().toTime_t()); }

protected:
	// Readable message text
	QString		text;

	// Time when message was sent
	QDateTime	sendTime;
};

#endif	// FINALEQPMESSAGE_H
