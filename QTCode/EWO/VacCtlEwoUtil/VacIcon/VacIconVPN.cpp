//	Implementation of VacIconVPN class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPN.h"
#include "VacMainView.h"

#include "EqpVPN.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255

QFont VacIconVPN::font("Arial", 5);	// , QFont::Normal);

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVPN::VacIconVPN(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 21);
}

VacIconVPN::~VacIconVPN()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPN::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(21, 21, 10, 26, -1);
}

VacIconContainer::Direction VacIconVPN::getSlaveDirection(void) const
{
	VacIconContainer::Direction result = VacIconContainer::Up;
	switch(direction)
	{
	case VacIconContainer::Up:
		result = VacIconContainer::Down;
		break;
	case VacIconContainer::Down:
		result = VacIconContainer::Up;
		break;
	case VacIconContainer::Left:
		result = VacIconContainer::Right;
		break;
	case VacIconContainer::Right:
		result = VacIconContainer::Left;
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPN::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVPN"), "VacIconVPN::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVPN *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPN::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPN::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	/*
	dpes.append("RR1");
	dpes.append("RR2");
	dpes.append("STATE.I_Value");
	*/
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPN::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPN::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPN::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		painter.fillRect(10 - pipeWidth / 2, 0, pipeWidth ? pipeWidth : 1, 4, pipeColor);
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.fillRect(10 - pipeWidth / 2, 18, pipeWidth ? pipeWidth : 1, 4, pipeColor);
		break;
	case VacIconContainer::Left:	// 90 degrees clockwise
		painter.fillRect(0, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		break;
	case VacIconContainer::Right:	// 90 degrees conuterclockwise
		painter.fillRect(18, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		break;
	}

	//painter.save();

	// Prepare painter's coordinate system for drawing
	// Coordinate system (0,0) is center of widget
	//painter.translate(10, 10); // width() / 2, height() / 2);

	// fillRect() produces not perfect result after rotation,
	// so draw rectangles before painter rotation
	// Draw pump image
	setPainterForImage(painter);

	// 1) Filled rectangle
	painter.drawRect(1, 1, 18, 18);

	/*
	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	// Coordinate system is ready - draw
	*/
	drawImage(painter);

	// Draw selection ON TOP of image
	//painter.restore();	// Original coordinate system - see save() above
	/*
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
	*/
}

void VacIconVPN::drawImage(QPainter &painter)
{
	// 1) Two slope lines emanating from connection point
	// pen.setWidth(2);
	painter.setPen(Qt::black);
	/*
	painter.drawLine(0, -9, -9, 9);
	painter.drawLine(0, -9, 9, 9);
	*/
	/*
	// 'N'
	painter.drawLine(2, 7, 2, 13);
	painter.drawLine(3, 7, 3, 13);
	painter.drawLine(4, 8, 4, 10);
	painter.drawLine(5, 11, 5, 12);
	painter.drawLine(6, 7, 6, 13);

	// 'E'
	painter.drawLine(8, 7, 8, 13);
	painter.drawLine(9, 7, 9, 13);
	painter.drawLine(10, 7, 12, 7);
	painter.drawLine(10, 9, 11, 9);
	painter.drawLine(10, 13, 12, 13);

	// 'G'
	painter.drawLine(13, 8, 13, 12);
	painter.drawLine(14, 7, 14, 13);
	painter.drawLine(15, 7, 17, 7);
	painter.drawLine(18, 7, 18, 8);
	painter.drawLine(15, 13, 17, 13);
	painter.drawLine(18, 10, 18, 13);
	painter.drawLine(17, 10, 18, 10);
	*/
	painter.setFont(font);
	painter.drawText(2, 13, "NEG");
}


void VacIconVPN::setPainterForImage(QPainter &painter)
{
	QColor	color(COLOR_UNDEFINED);
	if(pEqp)
	{
		pEqp->getMainColor(color, mode);
	}
	painter.setBrush(color);
	// L.Kopylov 04.02.2014 painter.setPen(color);
	painter.setPen(Qt::gray);
}

