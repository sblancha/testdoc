#ifndef INTERLOCKVIEW_H
#define INTERLOCKVIEW_H

#include <qwidget.h>
#include "InterlockIcon.h"
#include "InterlockIconConnection.h"

class InterlockChain;

class InterlockView : public QWidget
{
	Q_OBJECT

public:
	InterlockView(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	~InterlockView();

	static InterlockView *create(QWidget *parent, QString valveName);
	virtual void setViewStart(int viewStart, int viewWidth, bool dragging);
	void showChain(QString valveName);
	void setVisibleIcons();
	QRect getBoundingRect();
	InterlockIcon * getIcon(QString iconName);

protected:
	QString valveName;
	InterlockChain * pChain;
	QList<InterlockIcon *> iconList;
	QList<InterlockIconConnection *> iconConnectionList;

	int setIconPositions();
	void drawConnectionLines();
	void fixConnectionConflicts();
	bool iconExists(QString name);
	virtual void paintEvent(QPaintEvent *pEvent);

};
#endif