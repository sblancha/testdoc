#ifndef	EQPVBEAMPARAMFLOAT_H
#define	EQPVBEAMPARAMFLOAT_H

//	VBeamParamFloat - different beam parameters

#include "Eqp.h"


class VACCTLEQPDATA_EXPORT EqpVBeamParamFloat : public Eqp
{
	Q_OBJECT

public:
	EqpVBeamParamFloat(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVBeamParamFloat(const EqpVBeamParamFloat &src) : Eqp(src) {}
#endif
	~EqpVBeamParamFloat();

	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const;
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode /* mode */) { return true; }
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);

	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
//	virtual void getDpesForDevList(QStringList &dpes);
//	virtual int checkDevListCrit(void);
//	virtual void getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end);
//	virtual float getGrowFactorForDevList(void);
#endif

//	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline float getPR(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? valueReplay : valueOnline;
	}

protected:
	// Last known value of Value DPE for online mode
	float	valueOnline;

	// Last known value of Value DPE for replay mode
	float	valueReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainValueString(float value, QString &string);
	virtual void getMainStateStringForMenu(float value, QString &string);
#endif

};

#endif	// EQPVBEAMPARAMFLOAT_H
