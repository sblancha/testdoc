//	Implementation of LhcConnectLine class
///////////////////////////////////////////////////////////////////////

#include "LhcConnectLine.h"
#include "MainLHCImage.h"

#include "BeamLine.h"
#include "Sector.h"
#include "Eqp.h"
#include "DataPool.h"
#include "DebugCtl.h"

#include <math.h>	// for fabs()

LhcConnectLine::LhcConnectLine(BeamLine *pLine, int vacType)
{
	this->pLine = pLine;
	this->vacType = vacType;
	connected = false;
}

/*
** FUNCTION
**		Connect to all equipment on this beam line that may affect
**		image of main view.
**
** PARAMETERS
**		pImage	- Pointer to image that shall be notified in case of equipment
**					state change
**
** RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void LhcConnectLine::connectEqp(MainLHCImage *pImage)
{
	if(connected)
	{
		return;
	}
	connected = true;
	bool haveEqp = false;
	const QList<BeamLinePart *> &parts = pLine->getParts();
	for(int idx = 0 ; idx < parts.count() ; idx++)
	{
		BeamLinePart *pPart = parts.at(idx);
		Eqp **eqpPtrs = pPart->getEqpPtrs();
		int nEqpPtrs = pPart->getNEqpPtrs();
		for(int n = 0 ; n < nEqpPtrs ; n++)
		{
			Eqp *pEqp = eqpPtrs[n];
			if(pEqp->isSpecColor())
			{
				pEqp->connect(this, DataEnum::Online);
				haveEqp = true;
			}
		}
	}
	if(haveEqp)
	{
		QObject::connect(this, SIGNAL(stateChanged(void)), pImage, SLOT(eqpStateChanged(void)));
	}
}

/*
**	FUNCTION
**		Slot receiving notifications from equipment, see InterfaceEqp class
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcConnectLine::dpeChange(Eqp * /* pSrc */, const char * /* dpeName */,
	DataEnum::Source /* source */, const QVariant & /* value */, DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	// Only online mode calls are of interest for main view
	if(mode != DataEnum::Replay)
	{
		emit stateChanged();
	}
}

/*
** FUNCTION
**		Check if mouse pointer coordinates are within injection/dump line
**		and correspond to one of sectors in this line. If yes - return name
**		of closest sector in this line.
**
** PARAMETERS
**		pConnLine	- Pointer to injection/dump line to check
**		x			- Screen X coordinate
**		y			- Screen Y coordinate
**
** RETURNS
**		Name of closest sector;
**		NULL if screen point is too far from all sectors
**
** CAUTIONS
**		None
**
*/
const char *LhcConnectLine::sectorAtLocation(int x, int y, int connLineWidth)
{
	if((!coeffX) && (!coeffY))
	{
		return NULL;
	}

	// All connected lines are either horizontal or vertical
	if(!coeffX)	// vertical line
	{
		if(abs(x - startX) >= connLineWidth)
		{
			return NULL;
		}
		if(startY < endY)
		{
			if((y < startY) || (y > endY))
			{
				return NULL;
			}
		}
		else
		{
			if((y < endY) || (y > startY))
			{
				return NULL;
			}
		}
	}
	else	// horizontal line
	{
		if(abs(y - startY) >= connLineWidth)
		{
			return NULL;
		}
		if(startX < endX)
		{
			if((x < startX) || (x > endX))
			{
				return NULL;
			}
		}
		else
		{
			if((x < endX) || (x > startX))
			{
				return NULL;
			}
		}
	}

	// We are within line - find the closest device
	float bestDistance = 100000;	// Assiming no screen with such resolution
	int bestEqpIdx = -1;
	BeamLinePart	*pBestPart = NULL;
	const QList<BeamLinePart *> &parts = pLine->getParts();
	for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);
		int	nEqpPtrs = pPart->getNEqpPtrs();
		Eqp **eqpPtrs = pPart->getEqpPtrs();
		
		for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
		{
			Eqp *pEqp = eqpPtrs[eqpIdx];
			if(pEqp->getLength() < 0.0)
			{
				continue;
			}
			float distance;
			if(!coeffX)	// Vertical line
			{
				distance = y - (startY + (pEqp->getStart() - startPos) * coeffY);
			}
			else
			{
				distance = x - (startX + (pEqp->getStart() - startPos) * coeffX);
			}
			if(fabs(distance) > bestDistance)
			{
				break;
			}
			else if(fabs(distance) < bestDistance)
			{
				bestDistance = fabs(distance);
				pBestPart = pPart;
				bestEqpIdx = eqpIdx;
			}
		}
	}
	if(!pBestPart)
	{
		return NULL;
	}

	// Device has been found, find sector of device using draw parts of sectors
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		if(pSector->isOuter())
		{
			continue;
		}
		const QList<SectorDrawPart *> &drawParts = pSector->getDrawParts();
		for(int drawPartIdx = 0 ; drawPartIdx < drawParts.count() ; drawPartIdx++)
		{
			SectorDrawPart *pDrawPart = drawParts.at(drawPartIdx);
			if(pDrawPart->getLine() == pLine)
			{
				if(pDrawPart->getStartPart() == pBestPart)
				{
					if(pDrawPart->getEndPart() == pBestPart)
					{
						if((pDrawPart->getStartEqpIdx() <= bestEqpIdx) &&
							(bestEqpIdx <= pDrawPart->getEndEqpIdx()))
						{
							return pSector->getName();
						}
					}
					else if(pDrawPart->getStartEqpIdx() <= bestEqpIdx)
					{
						return pSector->getName();
					}
				}
				else if(pDrawPart->getEndPart() == pBestPart)
				{
					if(pDrawPart->getEndEqpIdx() >= bestEqpIdx)
					{
						return pSector->getName();
					}
				}
			}
		}
	}
	return NULL;
}

void LhcConnectLine::dump(FILE *pFile)
{
	if(!DebugCtl::isMainView())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	fprintf(pFile, "    <%s> : start (%d,%d) end (%d,%d)\n", pLine->getName(),
		startX, startY, endX, endY);
	fprintf(pFile, "         startPos %f coeffX %f coeffY %f\n", startPos,
		coeffX, coeffY);
}
