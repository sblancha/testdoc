// Script to manage Server time fasility
//Should be running to get curent time on the main views
//
dyn_string  exceptionInfo;
string SERVER_TIMED_DP_TYPE = "_TimedFunc";
string SERVER_TIMED_DP = "_ServerTimer";
string SERVER_CLOCK_DP_TYPE = "_ServerClock";
string SERVER_CLOCK_DP = "_ServerClock";

main()
{
DebugTN("vcsServerTime:Start");
 if( !dpExists( SERVER_TIMED_DP ) )
	{
	  if( dpCreate( SERVER_TIMED_DP, SERVER_TIMED_DP_TYPE ) < 0 )
	  {
		  fwException_raise(exceptionInfo, "vcsServerTime.ctl::main() ERROR", " Time config DP dpCreate() failed", "" );
		  return;
	  }
	  if( ! dpExists( SERVER_TIMED_DP ) )
	  {
		  fwException_raise(exceptionInfo, "vcsServerTime.ctl::main() ERROR", "Time config DP not exist", "" );
		  return;
	  }
	}
 if( !dpExists( SERVER_CLOCK_DP ) )
	{
	  if( dpCreate( SERVER_CLOCK_DP, SERVER_CLOCK_DP_TYPE ) < 0 )
	  {
		  fwException_raise(exceptionInfo, "vcsServerTime.ctl::main() ERROR", " Server clock config DP dpCreate() failed", "" );
		  return;
	  }
	  if( ! dpExists( SERVER_CLOCK_DP ) )
	  {
		  fwException_raise(exceptionInfo, "vcsServerTime.ctl::main() ERROR", "TServer clock  DP not exist", "" );
		  return;
	  }
	}
 int processingInterval = 1; 	//Interval in sec
 string timedDpName = SERVER_TIMED_DP;
//Configure  timed dp to perform calculation with processingInterval interval
  dpSet( timedDpName + ".validFrom", makeTime(2006,6,1),
      	timedDpName + ".validUntil", makeTime(2020,12,30),
        timedDpName + ".time", makeDynInt(), 				
        timedDpName + ".monthDay", makeDynInt(), 				
        timedDpName + ".month", makeDynInt(), 				
        timedDpName + ".weekDay", makeDynInt(), 			
        timedDpName + ".month", makeDynInt(), 				
     	  timedDpName + ".delay", 0,
      	timedDpName + ".interval", processingInterval,		
     		timedDpName + ".syncDay", -1,
     		timedDpName + ".syncWeekDay", -1,
    		 timedDpName + ".syncTime", -1, 
     		timedDpName + ".syncMonth", -1);
        
 	timedFunc ("ProcessValues", timedDpName);
  DebugTN("timedFunc Started");

}
void ProcessValues()
{
  string serverTimeclockDpe = "_ServerClock.Value";
  dpSet(serverTimeclockDpe, 1);
}
