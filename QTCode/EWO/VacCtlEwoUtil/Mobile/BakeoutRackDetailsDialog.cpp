//	Implementation of BakeoutRackDetailsDialog class
/////////////////////////////////////////////////////////////////////////////////

#include "BakeoutRackDetailsDialog.h"

#include "BakeoutRackTable.h"
#include "BakeoutCtlTable.h"

#include "BakeoutTree.h"

#include "ExtendedLabel.h"
#include "VacMainView.h"

#include "MobilePool.h"
#include "DataPool.h"
#include "Eqp.h"
#include "Sector.h"
#include "MobileType.h"

#include <qlayout.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qmenu.h>
#include <qsplitter.h>

BakeoutRackDetailsDialog *BakeoutRackDetailsDialog::pInstance = NULL;

/*
**	FUNCTION
**		Create and show new instance of dialog
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutRackDetailsDialog::create(void)
{
	if(pInstance)
	{
		pInstance->raise();
		return;
	}
	pInstance = new BakeoutRackDetailsDialog();
	pInstance->show();
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

BakeoutRackDetailsDialog::BakeoutRackDetailsDialog() :
	QDialog(NULL /* VacMainView::getInstance() */, Qt::Window)
{
	// Initialize itself
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle("Bakeout Rack Details");
	buildLayout();

	/*
	const QList<VacMainView *> &mainList = VacMainView::getInstanceList();
	for(int idx = 0 ; idx < mainList.count() ; idx++)
	{
		VacMainView *pInstance = mainList.at(idx);
		if(pInstance->isPvssEvents())
		{
			QObject::connect(this, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)),
				pInstance, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
		}
	}
	*/
	VacMainView *pMainView = VacMainView::getInstance();
	if(pMainView)
	{
		QObject::connect(this, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)),
			pMainView, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
	}
}

BakeoutRackDetailsDialog::~BakeoutRackDetailsDialog()
{
	pInstance = NULL;
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void BakeoutRackDetailsDialog::buildLayout(void)
{
	// 1) Main layout:
	//  - menu, eqp name and safety label on top
	// - tree and image on bottom
	QVBoxLayout *pMainBox = new QVBoxLayout(this);
	pMainBox->setContentsMargins(0, 0, 0, 0);
	pMainBox->setSpacing(0);

	// 2) Horizontal layout with menu, deviceName and safety label
	QHBoxLayout *pTopLayout = new QHBoxLayout();
	pTopLayout->setContentsMargins(0, 0, 0, 0);
	pTopLayout->setSpacing(0);
	pMainBox->addLayout(pTopLayout);

	// 2.1) All popup menu
	QPushButton *pFilePb = new QPushButton("&File", this);
	pFilePb->setFlat(true);
	QSize size = pFilePb->fontMetrics().size(Qt::TextSingleLine, "File");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pFilePb->setFixedSize(size);
	buildFileMenu();
	pFilePb->setMenu(pFileMenu);
	pTopLayout->addWidget(pFilePb);
	
	QPushButton *pHelpPb = new QPushButton("&Help", this);
	pHelpPb->setFlat(true);
	size = pHelpPb->fontMetrics().size(Qt::TextSingleLine, "Help");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pHelpPb->setFixedSize(size);
	buildHelpMenu();
	pHelpPb->setMenu(pHelpMenu);
	pTopLayout->addWidget(pHelpPb);

	pTopLayout->addStretch(10);

	// 2.2) Label for device name
	pEqpNameLabel = new ExtendedLabel(this);
	pTopLayout->addWidget(pEqpNameLabel);
	pEqpNameLabel->show();

	pTopLayout->addStretch(10);

	// 2.3) PCS (==Power Cut System) state label
	pPcsLabel = new QLabel("PCS", this);
	QPalette pal = pPcsLabel->palette();
	pal.setColor(QPalette::WindowText, Qt::red);
	pPcsLabel->setPalette(pal);
	pTopLayout->addWidget(pPcsLabel);
	pPcsLabel->hide();

	pTopLayout->addStretch(4);

	// 2.4) safety state label
	pSafetyLabel = new QLabel("SAFETY", this);
	pal = pSafetyLabel->palette();
	pal.setColor(QPalette::WindowText, Qt::red);
	pSafetyLabel->setPalette(pal);
	pTopLayout->addWidget(pSafetyLabel);
	pSafetyLabel->hide();

	pTopLayout->addStretch(4);

	// 3) Splitter
	QSplitter *pSplitter = new QSplitter(this);
	pMainBox->addWidget(pSplitter, 10);

	// 3.1) Tree - main parts, sectors, racks
	pTree = new BakeoutTree(pSplitter);
	connect(pTree, SIGNAL(selectChanged(Eqp *)),
		this, SLOT(eqpSelectChanged(Eqp *)));

	
	// 3.2) Table (regulation channels)
	pTable = new BakeoutRackTable(pSplitter);
	connect(pTable, SIGNAL(stateChanged()), this, SLOT(eqpStateChange()));

	// 3.3) Table (control channels)
	pCtlTable = new BakeoutCtlTable(pSplitter);
}

/*
**	FUNCTION
**		Build 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void BakeoutRackDetailsDialog::buildFileMenu(void)
{
	pFileMenu = new QMenu(this);
	pFileMenu->addAction("Print...", this, SLOT(filePrint()));
	pFileMenu->addSeparator();
	pFileMenu->addAction("Close", this, SLOT(deleteLater()));
}

/*
**	FUNCTION
**		Build 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void BakeoutRackDetailsDialog::buildHelpMenu(void)
{
	pHelpMenu = new QMenu(this);
	pHelpMenu->addAction("User manual...", this, SLOT(help()));
}

void BakeoutRackDetailsDialog::eqpSelectChanged(Eqp *pEqp)
{
	pTable->setEqp(NULL);
	pCtlTable->setEqp(NULL);
	pEqpNameLabel->setText("");
	pPcsLabel->setText("");
	pSafetyLabel->setText("");
	if(pEqp)
	{
		pTable->setEqp(pEqp);
		pCtlTable->setEqp(pEqp);
	}
}

void BakeoutRackDetailsDialog::eqpStateChange(void)
{
	if(!pTable->getEqp())
	{
		return;
	}
	unsigned rr1 = pTable->getRr1();
	bool plcAlarm = pTable->isPlcAlarm();
	QColor color;
	QString newLabel(pTable->getEqp()->getVisibleName());
	if(plcAlarm)
	{
		color.setRgb(127, 127, 255);
		newLabel += " (PLC error)";
	}
	else if((rr1 & 0x40000000) == 0)
	{
		color.setRgb(127, 127, 255);
		newLabel += " (not valid)";
	}
	pEqpNameLabel->setBackColor(color);
	pEqpNameLabel->setText(newLabel);
	pEqpNameLabel->adjustSize();

	// PCS state
	bool pcs = false;
	if(!plcAlarm)
	{
		if(rr1 & 0x40000000)
		{
			if(rr1 & 0x02000000)
			{
				pcs = true;
			}
		}
	}
	pPcsLabel->setText(pcs ? "PCS ON" : "PCS OFF");
	pPcsLabel->adjustSize();
	pPcsLabel->show();

	// Safety state
	bool safety = false;
	if(!plcAlarm)
	{
		if(rr1 & 0x40000000)
		{
			if(rr1 & 0x01000000)
			{
				safety = true;
			}
		}
	}
	pSafetyLabel->setText(safety ? "SAFETY ON" : "SAFETY OFF");
	pSafetyLabel->adjustSize();
	pSafetyLabel->show();
}

/*
**	FUNCTION
**		Slot activated when 'Print' item is selected in 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutRackDetailsDialog::filePrint(void)
{
	VacMainView::printWidget(this);
}

/*
**	FUNCTION
**		Slot activated when 'User manual' item is selected in 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutRackDetailsDialog::help(void)
{
}
