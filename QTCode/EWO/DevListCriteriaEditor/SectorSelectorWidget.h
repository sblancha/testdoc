#ifndef	SECTORSELECTORWIDGET_H
#define	SECTORSELECTORWIDGET_H

// Class providing functionality for selection sector(s) from lists

#include <QWidget>

#include "Eqp.h"
#include "EqpMsgCriteria.h"

class Sector;

#include <QListWidget>
#include <QListWidgetItem>
#include <QCheckBox>

class SectorSelectorWidget : public QWidget
{
	Q_OBJECT

public:
	SectorSelectorWidget(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~SectorSelectorWidget();

	int initContent(void);
	void setSelectedSectors(const QStringList &sectorList);
	void getSelectedSectors(QStringList &result) const;
	bool isEmpty(void);
	bool isNonVacSelected(void) const;
	void setNonVacSelected(bool flag);

signals:
	void selectionChanged(void);

protected:

	// CheckBox for selection QRL vacuum (LHC only)
	QCheckBox	*pQrlVacCb;

	// CheckBox for selection CRYO vacuum (LHC only)
	QCheckBox	*pCryoVacCb;

	// CheckBox for selection BLUE BEAM vacuum (LHC only)
	QCheckBox	*pBlueBeamVacCb;

	// CheckBox for selection RED BEAM vacuum (LHC only)
	QCheckBox	*pRedBeamVacCb;

	// List of main parts
	QListWidget	*pMainPartList;

	// List of sectors
	QListWidget	*pSectorList;

	// Flag inidcating if selection is being set by code - so don't react on it
	bool		setting;

	void updateMpContent(bool force = false);

	void selectInList(QListWidget *pList, const QString &name);
	bool isListItemSelected(QListWidget *pList, const QString &name);
	bool vacTypeMatches(int vacType);

private slots:

	void vacSelectChanged(bool flag);

	void mpSelectionChanged(void);
	void sectorSelectionChanged(void);

	void mpContextMenuRequest(const QPoint &point);
	void mpSelectAll(void);
	void mpUnselectAll(void);
	void mpInvertSelection(void);

	void sectorContextMenuRequest(const QPoint &point);
	void sectorSelectAll(void);
	void sectorUnselectAll(void);
	void sectorInvertSelection(void);

};

#endif	// SECTORSELECTORWIDGET_H
