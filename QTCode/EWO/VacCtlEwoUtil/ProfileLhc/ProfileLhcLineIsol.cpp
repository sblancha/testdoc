//	Implementation of ProfileLhcLineIsol class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileLhcLineIsol.h"

#include "VacLinePart.h"

#include "DataPool.h"
#include "Eqp.h"
#include "EqpType.h"

#include "VacMainView.h"

#include <qpainter.h>

#include <math.h>

#include "PlatformDef.h"
#include "DebugCtl.h"

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

ProfileLhcLineIsol::ProfileLhcLineIsol(ProfileLhcView *parent, int type, float start, float end) :
	ProfileLhcLine(parent, type, start, end)
{
}

ProfileLhcLineIsol::~ProfileLhcLineIsol()
{
	clear();
}

/*
**	FUNCTION
**		Clear content
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineIsol::clear(void)
{
	ProfileLhcLine::clear();
	realSectors.clear();
	while(!vpgs.isEmpty())
	{
		delete vpgs.takeFirst();
	}
}

/*
**	FUNCTION
**		Build all equipment that potentially can be shown in this line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineIsol::buildEquipment(void)
{
	// All parts are pure vacuum sectors
	DataPool &pool = DataPool::getInstance();
	for(int linePartIdx = 0 ; linePartIdx < pPartList->count() ; linePartIdx++)
	{
		VacLinePart *pPart = pPartList->at(linePartIdx);
		Sector *pSector = pPart->getSector();
		// .Q and .M sectors - LHC ring, DSL - other lines
		BeamLine *pLine = NULL;
		bool	useDSL = false;
		if((pSector->getVacType() == VacType::Qrl) || (pSector->getVacType() == VacType::Cryo))
		{
			pLine = pool.getCircularLine();
		}
		else	// other vac. types - DSL, it is expected that line only contains one sector
		{
			QList<BeamLine *> &lines = pool.getLines();
			for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
			{
				BeamLine *pBeamLine = lines.at(lineIdx);
				if(pBeamLine->isCircle())
				{
					continue;
				}
				QList<BeamLinePart *> &parts = pBeamLine->getParts();
				for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
				{
					BeamLinePart *pLinePart = parts.at(partIdx);
					QList<Eqp *> &eqpList = pLinePart->getEqpList();
					for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
					{
						Eqp *pEqp = eqpList.at(eqpIdx);
						if(pEqp->getSectorBefore() == pSector)
						{
							pLine = pBeamLine;
							break;
						}
					}
					if(pLine)
					{
						break;
					}
				}
				if(pLine)
				{
					break;
				}
			}
			useDSL = true;
		}
		if(!pLine)
		{
			continue;
		}

		// Coordinates for equipment on DSL line will be changed to be within
		// coordinate range of DSL sector.
		// It is also important that equipment with the same 'real' coordinates
		// will have the same 'synoptic' coordinates in order to join such
		// equipment to groups
		float	dslEqpCoord = pPart->getRealStart(),
				lastRealEqpCoord = -pool.getLhcRingLength();
		QList<BeamLinePart *> &parts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pLinePart = parts.at(partIdx);
			Eqp **eqpPtrs = pLinePart->getEqpPtrs();
			int nEqpPtrs = pLinePart->getNEqpPtrs();
			for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if(isMyEqp(pEqp, linePartIdx))
				{
					// For DSL line replace real coordinate with some coordinate lying
					// within VERY short coordinate range for sector
					if(useDSL)
					{
						float eqpCoord = pEqp->getStart();
						if(lastRealEqpCoord != eqpCoord)
						{
							dslEqpCoord += (float)0.01;
							lastRealEqpCoord = eqpCoord;
						}
						pEqp->setStart(dslEqpCoord);
						addEqp(pEqp, linePartIdx);
						pEqp->setStart(eqpCoord);
					}
					else
					{
						addEqp(pEqp, linePartIdx);
					}
				}
			}
		}
		// L.Kopylov 13.12.2012 Add equipment of DSL which is located on LHC ring
		if(useDSL)
		{
			pLine = DataPool::getInstance().getCircularLine();
			if(!pLine)
			{
				continue;
			}
			QList<BeamLinePart *> &parts = pLine->getParts();
			for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
			{
				BeamLinePart *pLinePart = parts.at(partIdx);
				Eqp **eqpPtrs = pLinePart->getEqpPtrs();
				int nEqpPtrs = pLinePart->getNEqpPtrs();
				for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(isMyEqp(pEqp, linePartIdx))
					{
						// For DSL line replace real coordinate with some coordinate lying
						// within VERY short coordinate range for sector
						if(useDSL)
						{
							float eqpCoord = pEqp->getStart();
							if(lastRealEqpCoord != eqpCoord)
							{
								dslEqpCoord += (float)0.01;
								lastRealEqpCoord = eqpCoord;
							}
							pEqp->setStart(dslEqpCoord);
							addEqp(pEqp, linePartIdx);
							pEqp->setStart(eqpCoord);
						}
						else
						{
							addEqp(pEqp, linePartIdx);
						}
					}
				}
			}
		}

	}
	if(eqps.isEmpty())
	{
		return;
	}

	// Build all real sectors included in line

	// Find first sector to add - only coordinate is considered for decision
	QList<Sector *> &sectList = pool.getSectors();
	Sector *pSector;
	int sectIdx;
	for(sectIdx = 0 ; sectIdx < sectList.count() ; sectIdx++)
	{
		pSector = sectList.at(sectIdx);
		if(!isMySector(pSector))
		{
			continue;
		}
		if(pSector->getEnd() >= start)
		{
			break;
		}
	}

	// Add first portion of sectors
	for( ; sectIdx < sectList.count() ; sectIdx++)
	{
		pSector = sectList.at(sectIdx);
		if(start < end)
		{
			if(pSector->getStart() >= end)
			{
				break;
			}
		}
		if(pSector->getEnd() <= start)
		{
			continue;
		}
		if(isMySector(pSector))
		{
			addRealSector(pSector);
		}
	}
// qDebug("start %f end %f added %d sectors\n", start, end, realSectors.count());
	if(start < end)
	{
		return;
	}

	// Add sectors after IP1
	for(sectIdx = 0 ; sectIdx < sectList.count() ; sectIdx++)
	{
		pSector = sectList.at(sectIdx);
		if(!isMySector(pSector))
		{
			continue;
		}
		if(pSector->getStart() >= end)
		{
//			qDebug("Stop at sector %s: start %f end %f\n", pSector->getName(), pSector->getStart(), pSector->getEnd());
			break;
		}
		if(isMySector(pSector))
		{
			addRealSector(pSector);
		}
	}
}

/*
**	FUNCTION
**		Add one real sector area to array of real sectors in profile
**
**	PARAMETERS
**		pSector	- Pointer to sector to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineIsol::addRealSector(Sector *pSector)
{
	RealCryoSector *pNewItem = new RealCryoSector(pSector, start, end);
	realSectors.append(pNewItem);
}

/*
**	FUNCTION
**		Calculate hide/show parameters for all devices using
**		new equipment type masks
**
**	PARAMETERS
**		eqpMask		- Equipment type mask for device types to show
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcLineIsol::applyMask(VacEqpTypeMask &eqpMask)
{
	ProfileLhcLine::applyMask(eqpMask);
	curSectorIdx = 0;
	for(int idx = 0 ; idx < realSectors.count() ; idx++)
	{
		RealCryoSector *pSect = realSectors.at(idx);
		pSect->setStartPassed(false);
	}
}

/*
**	FUNCTION
**		Find next position of element to be processed when
**		calculating bar geometry
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Position of next element to process, or
**		huge value (twice LHC ring length) if no more elements
**		shall be processed
**
**	CAUTIONS
**		None
*/
float ProfileLhcLineIsol::nextPosition(void)
{
	float	result = HUGE_COORDINATE;

	result = ProfileLhcLine::nextPosition();
	//qDebug("ProfileLhcLineIsol::nextPosition(): parent result %f curSectorIdx %d\n", result, curSectorIdx);
	placeSector = false;
	if((0 <= curSectorIdx) && (curSectorIdx < realSectors.count()))
	{
		RealCryoSector *pSect = realSectors.at(curSectorIdx);
		float	position;
		if(pSect->isStartInside() && (!pSect->isStartPassed()))
		{
			position = pSect->getStart();
		}
		else
		{
			position = pSect->getEnd();
		}
		//qDebug("     ProfileLhcLineIsol::nextPosition(): parent result %f position for sector %f\n", result, position);
		if(position < result)
		{
			result = position;
			placeSector = true;
		}
	}
	return result;
}

/*
**	FUNCTION
**		Assign bar index to current device (or sector) in profile given
**		by curEqpIdx (or curSectorIdx)
**
**	PARAMETERS
**		coord	- Last processed physical location, value can be
**					changed by this method
**		barIdx	- Last used bar index, value can be changed by this
**					method.
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void ProfileLhcLineIsol::placeItem(float &coord, int &barIdx)
{
	if(placeSector)
	{
		if((0 <= curSectorIdx) && (curSectorIdx < realSectors.count()))
		{
			RealCryoSector *pSect = realSectors.at(curSectorIdx);
			if(pSect->isStartInside() && (!pSect->isStartPassed()))
			{
				// CheckCoordAndBar( coord, barIdx, pPool->sectors[pSector->sectorIdx]->start );
				pSect->setStartPassed(true);
				pSect->setStartBarIdx(barIdx);
			}
			else
			{
				// CheckCoordAndBar( coord, barIdx, pPool->sectors[pSector->sectorIdx]->end );
				pSect->setStartPassed(true);
				pSect->setEndBarIdx(barIdx);
				curSectorIdx++;
			}
		}
	}
	else
	{
		if((0 <= curEqpIdx) && (curEqpIdx < eqps.count()))
		{
			ProfileLhcLineItem *pItem = eqps.at(curEqpIdx);
			if(pItem->shallBeVisible() && (!pItem->isHidden()))
			{
				checkCoordAndBar(coord, barIdx, pItem->getStart());
			}
			// Assign bar number, move to next equipment index
			pItem->setBarIdx(barIdx);
			curEqpIdx++;
		}
	}
}

/*
**	FUNCTION
**		Check if bar index for new coordinate shall increase
**
**	PARAMETERS
**		coord		- Last processed physical location, value can be
**						changed by this method
**		barIdx		- Last used bar index, value can be changed by this
**						method
**		newCoord	- Physical location being processed
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void ProfileLhcLineIsol::checkCoordAndBar(float &coord, int &barIdx, float newCoord)
{
	if(newCoord > coord)
	{
		if(coord >= 0.0)
		{
			barIdx++;
		}
		coord = newCoord;
		if(barIdx < 0)
		{
			barIdx = 0;
		}
	}
	for(int idx = curEqpIdx - 1 ; idx >= 0 ; idx--)
	{
		ProfileLhcLineItem *pPrev = eqps.at(idx);
		if(pPrev->isHidden() || (!pPrev->shallBeVisible()))
		{
			continue;
		}
		if(barIdx <= pPrev->getBarIdx())
		{
			barIdx = pPrev->getBarIdx() + 1;
		}
		break;
	}
}

/*
**	FUNCTION
**		Calculate geometry for all bars in graph.
**
**	PARAMETERS
**		maxBarIdx		- Maximum bar index
**		hasLinearEqp	- Flag indicating if graph contains equipment using linear
**							vertical scale
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void ProfileLhcLineIsol::calcBarGeometry(int &maxBarIdx)
{
	ProfileLhcLine::calcBarGeometry(maxBarIdx);

	// Calculate one bar width and horizontal scale coefficients
	double xScale = (double)(rightX - leftX) / (double)(maxBarIdx + 1 );

	// Calculate geometry of all real sectors
	for(int idx = 0 ; idx < realSectors.count() ; idx++)
	{
		RealCryoSector *pSect = realSectors.at(idx);
		if(pSect->isStartInside())
		{
			pSect->setLeftX((int)rint(leftX + ((double)pSect->getStartBarIdx() + 1.0) * xScale));
		}
		else
		{
			pSect->setLeftX(leftX);
		}
		if(pSect->isEndInside())
		{
			pSect->setRightX((int)rint(leftX + ((double)pSect->getEndBarIdx() + 1.0) * xScale));
		}
		else
		{
			pSect->setRightX(rightX);
		}
	}
}

/*
**	FUNCTION
**		Find best screen position for given world coordinate. The method is called
**		after all pressure measurement devices and sectors have been plced to find
**		screen position for thermometer areas.
**
**	PARAMETERS
**		coord		- Required coordinate (LHCLAYOUT DCUM)
**		bestDelta	- The best difference of world coordinate from found coordinate,
**						calculated by another line, can be updated by this line
**		screenCoord	- Calculated screen coordinate is returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcLineIsol::bestPositionForEqp(float coord, float &bestDelta, int &screenCoord)
{
	ProfileLhcLine::bestPositionForEqp(coord, bestDelta, screenCoord);
	if(bestDelta == 0)
	{
		return;
	}
	float	myBestDelta = bestDelta;
	int	myScreenCoord = 0;

	// Check sectors
	for(int idx = 0 ; idx < sectors.count() ; idx++)
	{
		ProfileLineSector *pSect = sectors.at(idx);
		if(pSect->getSector1())
		{
			float delta = (float)fabs(pSect->getSector1()->getStart() - coord);
			if(delta < myBestDelta)
			{
				myBestDelta = delta;
				myScreenCoord = pSect->getLeftX();
			}
			delta = (float)fabs(pSect->getSector1()->getEnd() - coord);
			if(delta < myBestDelta)
			{
				myBestDelta = delta;
				myScreenCoord = pSect->getRightX();
			}
		}
	}
	if(myBestDelta < bestDelta)
	{
		bestDelta = myBestDelta;
		screenCoord = myScreenCoord;
	}
}

/*
**	FUNCTION
**		Build geometry of all sector areas be shown in profile
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcLineIsol::buildSectors(QFontMetrics &fontMetrics)
{
	int	startEqpIdx, endEqpIdx;

	sectors.clear();
	if(!nBarsToDraw)
	{
		axisMinLocation = -1;
		return;
	}
	ProfileLineSector	sector;
	int eqpIdx = 0;

	for(int sectIdx = 0 ; sectIdx < realSectors.count() ; sectIdx++)
	{
		RealCryoSector *pRealSector = realSectors.at(sectIdx);
		sector.setSector1(pRealSector->getSector());
		sector.setSector2(NULL);
		sector.setLeftX(pRealSector->getLeftX());
		sector.setRightX(pRealSector->getRightX());
		int areaNumberInSector = 1;

		// The following possibilities exist:
		// 1) For first sector only - new area with eqp of another sector can be added
		//    before the sector
		// 2) Start of sector can move from 'real' sector border
		// 3) End of sector can move from 'real' sector border
		// 4) New transition area with eqp of another sector can be added after the sector

		// Check at the start of real sector
		float sectorBorder = pRealSector->getStart();
		ProfileLhcLineItem *pItem;
		for( ; eqpIdx < eqps.count() ; eqpIdx++)
		{
			pItem = eqps.at(eqpIdx);
			if(pItem->isHidden())
			{
				continue;
			}
			int compareResult = compareToSectorBorder(pItem->getStart(), sectorBorder);
			if(compareResult == 0)	// Equipment at sector start
			{
				if(!isEqpOfThisSector(pRealSector->getSector(), pItem->getEqp()))
				{
					endEqpIdx = startEqpIdx = eqpIdx++;
					ProfileLhcLineItem *pLast = pItem;
					for(int nextIdx = eqpIdx + 1 ; nextIdx < eqps.count() ; nextIdx++)
					{
						ProfileLhcLineItem *pNext = eqps.at(nextIdx);
						if(pNext->isHidden())
						{
							continue;
						}
						if((compareToSectorBorder(pNext->getStart(), sectorBorder) == 0) &&
							(!isEqpOfThisSector(pRealSector->getSector(), pNext->getEqp())))
						{
							endEqpIdx = eqpIdx;
							pLast = pNext;
						}
						else
						{
							break;
						}
					}
					sector.setSector1(pItem->getEqp()->getSectorBefore(mode));
					sector.setSector2(pItem->getEqp()->getSectorAfter(mode));
					sector.setLeftEqpIdx(startEqpIdx);
					sector.setRightEqpIdx(endEqpIdx);
					sector.setRightX(pLast->getRightX() + 1);
					addSector(&sector, areaNumberInSector++);
					sector.setLeftX(sector.getRightX());
					eqpIdx = startEqpIdx - 1;
//					eqps.at(eqpIdx);
					break;
				}
				else if(pItem->getLeftX() < pRealSector->getLeftX())
				{
					sector.setLeftX(pItem->getLeftX() - 1);
					break;
				}
				break;
			}
			else if(compareResult > 0)	// Equipment after sector border
			{
				break;
			}
		}

		// Check at the end of real sector
		sectorBorder = pRealSector->getEnd();
		sector.setRightX(pRealSector->getRightX());
		// Two lines below added by L.Kopylov 08.05.2008
		sector.setSector1(pRealSector->getSector());
		sector.setSector2(NULL);
		if((eqpIdx < 0) || (eqpIdx >= eqps.count()))
		{
			eqpIdx = 0;
		}
		for( ; eqpIdx < eqps.count() ; eqpIdx++)
		{
			pItem = eqps.at(eqpIdx);
			if(pItem->isHidden())
			{
				continue;
			}
			int compareResult = compareToSectorBorder(pItem->getStart(), sectorBorder);
			if(compareResult == 0)	// Equipment at sector end
			{
				if(!isEqpOfThisSector(pRealSector->getSector(), pItem->getEqp()))
				{
					sector.setRightX(pItem->getLeftX() - 1);
					sector.setRightEqpIdx(eqpIdx);
					addSector(&sector, areaNumberInSector++);	// Add sector itself
					endEqpIdx = startEqpIdx = eqpIdx++;
					ProfileLhcLineItem *pLast = pItem;
					for(int nextIdx = eqpIdx + 1 ; nextIdx < eqps.count() ; nextIdx++)
					{
						ProfileLhcLineItem *pNext = eqps.at(nextIdx);
						if(pNext->isHidden())
						{
							continue;
						}
						if((compareToSectorBorder(pNext->getStart(), sectorBorder) == 0) &&
							(!isEqpOfThisSector(pRealSector->getSector(), pNext->getEqp())))
						{
							endEqpIdx = eqpIdx;
							pLast = pNext;
						}
						else
						{
							sector.setLeftX(sector.getRightX());
							sector.setSector1(pItem->getEqp()->getSectorBefore(mode));
							sector.setSector2(pItem->getEqp()->getSectorAfter(mode));
							sector.setLeftEqpIdx(startEqpIdx);
							sector.setRightEqpIdx(endEqpIdx);
							sector.setRightX(pLast->getRightX());
							break;
						}
					}
				}
				else
				{
					// Run out of sector
					for(++eqpIdx ; eqpIdx < eqps.count() ; eqpIdx++)
					{
						pItem = eqps.at(eqpIdx);
						if(isEqpOfThisSector(pRealSector->getSector(), pItem->getEqp()))
						{
							sector.setRightX(pItem->getRightX());
						}
						else
						{
							break;
						}
					}
					if(sector.getRightX() < pRealSector->getRightX())
					{
						sector.setRightX(pRealSector->getRightX());
					}
				}
				break;
			}
			else if(compareResult > 0)	// Equipment after sector border
			{
				break;
			}
		}
		addSector(&sector, areaNumberInSector++);
	}
	axisMinLocation = calculateSectorLabels(fontMetrics);
}

/*
**	FUNCTION
**		Check if given coordinate is at, before or after coordinate of sector border
**
**	PARAMETERS
**		coord			- Coordinate to check
**		sectorBorder	- Coordinate of sector border
**
**	RETURNS
**		-1	- coordinate is before sector border
**		0	- coordinate is at sector border
**		1	- coordinate is after sector border
**
**	CAUTIONS
**		None
**
*/
int ProfileLhcLineIsol::compareToSectorBorder(float coord, float sectorBorder)
{
	// For QRL and CRYO distance criteria can be used
	if(fabs(sectorBorder - coord) < 1.0)
	{
		return 0;
	}
	return coord < sectorBorder ? -1 : 1;
}

/*
**	FUNCTION
**		Check if given device is related to given sector
**
**	PARAMETERS
**		pSector	- Pointer to sector
**		pEqp	- Pointer to device to check
**
**	RETURNS
**		true	- if device is related to sector;
**		false	- otherwise
**
**	CAUTIONS
**		None
**
*/
bool ProfileLhcLineIsol::isEqpOfThisSector(Sector *pSector, Eqp *pEqp)
{
	return ((pEqp->getSectorBefore(mode) == pSector) && (!pEqp->getSectorAfter(mode))) ||
			((pEqp->getSectorAfter(mode) == pSector) && (!pEqp->getSectorBefore(mode)));
}

/*
**	FUNCTION
**		Add one sector area to list of sector areas in profile line
**		In addition make sure there is no holes between sector areas
**		where they shall not be.
**
**	PARAMETERS
**		pSector				- Pointer to filled sector area parameters
**		areaNumberInSector	- Number of sector area within real sector.
**							  Up to 3 areas may be added for every sector
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineIsol::addSector(ProfileLineSector *pSector, int areaNumberInSector)
{
	if(sectors.count())
	{
		if(areaNumberInSector == 1)
		{
			RealCryoSector *pSect = NULL;
			if((0 <= curSectorIdx) && (curSectorIdx < realSectors.count()))
			{
				pSect = realSectors.at(curSectorIdx);
			}
			
			RealCryoSector *pPrev = NULL;
			if((0 < curSectorIdx) && (curSectorIdx <= realSectors.count()))
			{
				pPrev = realSectors.at(curSectorIdx - 1);
			}
			if(pSect && pPrev)
			{
				if(pSect->getLeftX() == pPrev->getRightX())
				{
					pSector->setLeftX(sectors.last()->getRightX());
				}
			}
		}
		else
		{
			pSector->setLeftX(sectors.last()->getRightX());
		}
	}
	ProfileLhcLine::addSector(pSector);
}

/*
**	FUNCTION
**		Draw background of isolation vacuum sector areas
**		Default drawing method using hatch brush to draw sector
**		background. Color of brush is determined by vacuum type,
**		type of hatch is changed between sectors
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineIsol::drawSectorsHatchFill(QPainter &painter)
{
	bool isAltColor = false;

	QRect rect;
	rect.setTop(topLogY);
	rect.setBottom(area.bottom());
	QColor color;
	switch(type)
	{
	case Qrl:
		color = sectorBackColor(VacType::Qrl);
		break;
	default:
		color = sectorBackColor(VacType::Cryo);
		break;
	}

	// Fill the whole graph area using main filling method
	QBrush brushB(color, Qt::BDiagPattern);
	rect.setLeft(sectors.first()->getLeftX());
	rect.setRight(sectors.last()->getRightX());
	painter.fillRect(rect, brushB);

	QBrush brushF(color, Qt::FDiagPattern);
	QBrush brushC(color, Qt::CrossPattern);

	for(int idx = 0 ; idx < sectors.count() ; idx++)
	{
		ProfileLineSector *pSect = sectors.at(idx);
		rect.setLeft(pSect->getLeftX());
		rect.setRight(pSect->getRightX());

		// Transition area between two sectors is drawn using special filling method
		if(pSect->getSector1() && pSect->getSector2() &&
			(pSect->getSector1() != pSect->getSector2()))
		{
			painter.fillRect(rect, brushC);
		}
		else	// One sector area is drawn as rectangle
		{
			// Draw background rectangle - only when alternative color is used
			if(isAltColor)
			{
				painter.fillRect(rect, brushF);
			}
			isAltColor = ! isAltColor;
		}
	}
}

/*
**	FUNCTION
**		Draw background of beam vacuum sector areas for isolation vacuums
**		Method using solid brush to draw sector
**		background and vertical line to draw border between sectors.
**		Color of brush is determined by vacuum type.
**		Border between sector may be not vertical, but rather zigzag
**		line - in case of equipment measuring pressure in two adjusent
**		sectors (gauges on by-pass pumping groups)
**
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcLineIsol::drawSectorsWithBorders(QPainter &painter)
{
//qDebug("ProfileLhcLineIsol::drawSectorsWithBorders(): start, %d sectors, %d realSectors\n", sectors.count(), realSectors.count());
	QRect rect;
	rect.setTop(topLogY);
	rect.setBottom(area.bottom());

	// Prepare brush for background filling
	QColor	color;
	switch(type)
	{
	case Qrl:
		color = sectorBackColor(VacType::Qrl);
		break;
	default:
		color = sectorBackColor(VacType::Cryo);
		break;
	}

//qDebug("ProfileLhcLineIsol::drawSectorsWithBorders(): 1\n");
	// Pass 1 - fill in sector areas
	ProfileLineSector *pSect;
	int sectIdx;
	for(sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		pSect = sectors.at(sectIdx);
		rect.setLeft(pSect->getLeftX() + 1);	// +1 = do not overdraw vertical line of previous sector
		rect.setRight(pSect->getRightX() + 1);	// +1 because of FillRect behaviour
		painter.fillRect(rect, color);
	}

//qDebug("ProfileLhcLineIsol::drawSectorsWithBorders(): 2\n");
	// Pass 2 - draw borders bewteen two sectors
	QPen pen(VacMainView::getGraphAxisColor(), 0, Qt::DotLine);
	painter.setPen(pen);
	for(sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		pSect = sectors.at(sectIdx);
		int startX = pSect->getRightX();
		int startY = topLogY;
//qDebug("ProfileLhcLineIsol::drawSectorsWithBorders(): sectIdx %d, transition %d\n", sectIdx, (int)pSect->isTransition());
		// Transition area between two sectors is drawn using zigzag line
		if(pSect->isTransition())
		{
			int	width = (pSect->getRightX() - pSect->getLeftX()) >> 1;
			bool useLeftSide = true;
			int y = 0;
//qDebug("ProfileLhcLineIsol::drawSectorsWithBorders(): topLogY %d, bottomLogY %d, width %d\n", topLogY, bottomLogY, width);
			if(width == 0)
			{
				width = 2;	// L.Kopylov 02.07.2012
			}
			for(int i = topLogY ; i < rect.bottom() ; i += width)
			{
				if((i + width) <= rect.bottom())
				{
					y = i + width;
				}
				else
				{
					y = rect.bottom();
				}
				int x = useLeftSide ? pSect->getLeftX() : pSect->getRightX();
				painter.drawLine(startX, startY, x, y);
				startX = x;
				startY = y;
				useLeftSide = ! useLeftSide;
			}
		}
		else	// Border between two sectors is drawn as vertical line
		{
			// Do not draw line if next area is transition between two sectors
			if((0 <= (sectIdx + 1)) && ((sectIdx + 1) < sectors.count()))
			{
				ProfileLineSector *pNext = sectors.at(sectIdx + 1);
				if(!pNext->isTransition())
				{
					painter.drawLine(startX, startY, startX, rect.bottom());
				}
			}
		}
//qDebug("ProfileLhcLineIsol::drawSectorsWithBorders(): sectIdx %d - DONE\n", sectIdx);
	}
//qDebug("ProfileLhcLineIsol::drawSectorsWithBorders(): FINISH\n");
}

/*
**	FUNCTION
**		Check if given device shall appear in this profile line. Coordinate
**		of device is NOT checked.
**
**	PARAMETERS
**		pEqp	- Pointer to device in question
**		partIdx	- Index of line part, where device is located
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
bool ProfileLhcLineIsol::isMyEqp(Eqp *pEqp, int &partIdx)
{
	if(pEqp->getType() != EqpType::VacDevice)
	{
		return false;
	}
	// Skip NotControl + NotConnected - NO, NotConnected can change on the fly
	switch(pEqp->getCtlStatus(mode))
	{
	case Eqp::NotControl:
		return false;
	default:
		break;
	}
	/* Add all equipment - even not active at the moment
	if(!pEqp->isActive(mode))
	{
		return false;	// Mobile equipment - not activated
	}
	*/

	if(pEqp->isCryoThermometer())
	{
		if(pEqp->isSkipOnSynoptic())
		{
			return false;
		}
	}

	// Controllable vacuum equipment is checked here
	if(!allowedEqpMask.contains(pEqp->getFunctionalType()))
	{
		return false;
	}

	// Check sector
	VacLinePart *pPart = pPartList->at(partIdx);
	if((pEqp->getSectorBefore() == pPart->getSector()) ||
		(pEqp->getSectorAfter() == pPart->getSector()))
	{
		// It can be that the same device (sitting on sector border) has been added already
		for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
		{
			ProfileLhcLineItem *pItem = eqps.at(idx);
			if(pItem->getEqp() == pEqp)
			{
				return false;
			}
		}
		return true;
	}
	return false;
}

/*
**	FUNCTION
**		In addition to just adding device to this line - check if this is VG
**		of pumping group, if so - add VPGF to list of VPGFs which shall be
**		connected in order to update sector reference of gauge
**
**	PARAMETERS
**		pEqp	- Pointer to device in question
**		partIdx	- Index of line part, where device is located
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineIsol::addEqp(Eqp *pEqp, int partIdx)
{
	ProfileLhcLine::addEqp(pEqp, partIdx);
	switch(pEqp->getFunctionalType())
	{
	case FunctionalType::VGR:
	case FunctionalType::VGP:
		break;
	default:
		return;
	}
	const QString vpgfName = pEqp->getAttrValue("CtlParent");
	if(vpgfName.isEmpty())
	{
		return;
	}
	Eqp *pVpgf = DataPool::getInstance().findEqpByDpName(vpgfName.toLatin1());
	Q_ASSERT(pVpgf);
	for(int idx = vpgs.count() - 1 ; idx >= 0 ; idx--)
	{
		ProfileLhcVpgConnector *pItem = vpgs.at(idx);
		if(pItem->getEqp() == pVpgf)
		{
			return;
		}
	}
	vpgs.append(new ProfileLhcVpgConnector(pVpgf, mode));
}

/*
**	FUNCTION
**		Check if sector shall be used for drawing line parts of this line
**
**	PARAMETERS
**		pSector	- Pointer to sector in question
**
**	RETURNS
**		true	- if sector can be used for this profile line
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool ProfileLhcLineIsol::isMySector(Sector *pSector)
{
	switch(pSector->getVacType())
	{
	case VacType::Qrl:
		if(type == Qrl)
		{
			return true;
		}
		break;
	case VacType::Cryo:
		if(type == Cryo)
		{
			return true;
		}
		break;
	}
	return false;
}

/*
**	FUNCTION
**		Dump content of line to output file - for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for write
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineIsol::dump(FILE *pFile)
{
	if(!DebugCtl::isProfile())
	{
		return;
	}
	ProfileLhcLine::dump(pFile);

	fprintf(pFile, "  %d REAL SECTORS:\n", realSectors.count());
	int idx;
	for(idx = 0 ; idx < realSectors.count() ; idx++)
	{
		RealCryoSector *pSect = realSectors.at(idx);
		pSect->dump(pFile, idx);
	}

	fprintf(pFile, "   %d VPGs:\n", vpgs.count());
	for(idx = 0 ; idx < vpgs.count() ; idx++)
	{
		ProfileLhcVpgConnector *pItem = vpgs.at(idx);
		fprintf(pFile, "     %03d: %s\n", idx, pItem->getEqp()->getName());
	}
}

// TODO
void ProfileLhcLineIsol::sectorChange(Eqp * /* pSrc */)
{
}

