#ifndef	EQPMSGCRITERIA_H
#define	EQPMSGCRITERIA_H

#include "VacCtlEqpDataExport.h"

// Superclass defining common members/methods for different equipment criteria

#include <qstring.h>
#include <qlist.h>
#include <qdatetime.h>

class StdEqpMessage;

class VACCTLEQPDATA_EXPORT EqpMsgCriteria
{
public:

	// Major types of critera
	typedef enum
	{
		None = 0,
		MainState = 1,
		MainValue = 2,
		ActiveState = 3,	// Mobile connect/disconnect
		Error = 4,
		Warning = 5
	} CriteriaType;

	static const char *critTypeToString(int type);
	static int critTypeParse(const char *string, QString &errMsg);

	EqpMsgCriteria(int functionalType, int type, int subType, const char *name);
	EqpMsgCriteria(const EqpMsgCriteria &source);
	~EqpMsgCriteria();
	EqpMsgCriteria & operator = (const EqpMsgCriteria &other);

	void addStdMessage(int type, bool reverse, const char *text);

	const char *getStdMessage(int type, bool reverse);

	void resetPendingMatch(void);
	bool checkPendingMatch(const QDateTime &now);

	// Access
	inline int getFunctionalType(void) const { return functionalType; }
	inline int getType(void) const { return type; }
	inline void setType(int value) { type = value; }
	inline int getSubType(void) const { return subType; }
	inline void setSubType(int value) { subType = value; }
	inline float getUpperLimit(void) const { return upperLimit; }
	inline void setUpperLimit(float value) { upperLimit = value; }
	inline float getLowerLimit(void) const { return lowerLimit; }
	inline void setLowerLimit(float value) { lowerLimit = value; }
	inline int getErrCode(void) const { return errCode; }
	inline void setErrCode(int code) { errCode = code; }
	inline int getWarningCode(void) const { return warningCode; }
	inline void setWarningCode(int code) { warningCode = code; }
	inline const QString &getName(void) const {return name; }
	inline void setName(const QString &newName) { name = newName; }
	inline bool isReverse(void) const { return reverse; }
	inline void setReverse(bool flag) { reverse = flag; }
	inline bool isMatch(void) const { return match; }
	void setMatch(bool flag, bool isValue, float value);

	inline int getMinDuration(void) const { return minDuration; }
	inline void setMinDuration(int value) { minDuration = value; }

	inline const QDateTime &getEventTime(void) const { return eventTime; }
	inline bool isMatchByValue(void) const { return matchByValue; }
	inline float getEventValue(void) const { return eventValue; }

protected:
	// Functional type that uses the criteria
	int		functionalType;

	// Criteria type - one of enum values above
	int		type;

	// Criteria subtype - may be required by some subclasses
	int		subType;

	// Value limit - only used for MainValue type
	float	upperLimit;

	// Value limit - only used for MainValue type
	float	lowerLimit;

	// Error/warning code that shall produce message
	int		errCode;

	// Warning code that shall produce message
	int		warningCode;

	// Minimum 'bad' condition duration that shall produce message
	int		minDuration;

	// Flag indicating if REVERSE criteria shall be used
	bool	reverse;

	// Flag indicating if crtirea is fulfilled (used at criteria check time)
	bool	match;

	// Flag indicating that criteria macthes, but we have to wait minDuration
	// in order to set matches flag
	bool	pendingMatch;

	// Flag indicating if criteria match was caused by equipment value
	bool	matchByValue;

	// Equipment value that triggred criteria match
	float	eventValue;

	// Visible name for this criteria
	QString	name;

	// Timestamp - when 'bad' condition was detected, only used when minDuration > 0
	QDateTime	eventTime;

	// List of default messages for this criteria
	QList<StdEqpMessage *>	*pMsgList;
};

#endif	// EQPMSGCRITERIA_H
