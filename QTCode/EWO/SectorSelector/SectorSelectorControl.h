#ifndef	SECTORSELECTORCONTROL_H
#define	SECTORSELECTORCONTROL_H

// Class providing functionality for selection sector(s) from lists

#include <QWidget>

class Sector;

#include <QListWidget>
#include <QTableWidget>
#include <QTableWidgetSelectionRange>
#include <QPushButton>

class SectorSelectorControl : public QWidget
{
	Q_OBJECT

	////////////////////////////////////////////////////////////////////////////
	///////////////////// Properties exposed to PVSS panel /////////////////////
	////////////////////////////////////////////////////////////////////////////
	Q_PROPERTY(bool showDateColumn READ isShowDate WRITE setShowDate);

public:
	SectorSelectorControl(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~SectorSelectorControl();

	int initContent(void);
	void setSelectedSectors(const QStringList &sectorList);
	void getSelectedSectors(QStringList &result) const;
	bool isEmpty(void);

	// Access
	bool isShowDate(void) const { return showDate; }
	void setShowDate(bool flag);

signals:
	void selectionChanged(void);

protected:
	// List of main parts
	QListWidget	*pMainPartList;

	// List of all sectors
	QListWidget	*pAllSectorList;

	// List of selected sectors - table to have 2nd column = Date
	QTableWidget	*pSectorTable;

	// Button to move selected sectors from left to right list
	QPushButton	*pRightPb;

	// Button to move selected sectors from right to left list
	QPushButton	*pLeftPb;

	// Flag inidcating if selection is being set by code - so don't react on it
	bool		setting;

	// Flag indicating if Date column shall be shown in sector table or not
	bool		showDate;

	void selectInList(QListWidget *pList, const QString &name);
	bool isListItemSelected(QListWidget *pList, const QString &name);
	void addToSelection(int row, int &firstSelectedRow, int &lastSelectedRow,
		QList<QTableWidgetSelectionRange *> &newSelections);
	void setNewSelection(int firstSelectedRow, int lastSelectedRow,
		QList<QTableWidgetSelectionRange *> &newSelections);

private slots:
	void mpSelectionChanged(void);
	void allSectorSelectionChanged(void);
	void sectorSelectionChanged(void);

	void mpListContextMenuRequest(const QPoint &point);
	void mpSelectAll(void);
	void mpUnselectAll(void);
	void mpInvertSelection(void);

	void allSectorsListContextMenuRequest(const QPoint &point);
	void allSectorSelectAll(void);
	void allSectorUnselectAll(void);
	void allSectorInvertSelection(void);

	void sectorsListContextMenuRequest(const QPoint &point);
	void sectorSelectAll(void);
	void sectorUnselectAll(void);
	void sectorInvertSelection(void);

	void rightAllPbClicked(void);
	void rightPbClicked(void);
	void leftPbClicked(void);
	void leftAllPbClicked(void);
};


#endif	// SECTORSELECTORCONTROL_H
