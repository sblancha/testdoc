#ifndef DPECONNECTION_H
#define	DPECONNECTION_H

#include "VacCtlUtilExport.h"

// Class holding connection statistics for one DPE

#include "DataEnum.h"

#include <QVariant>
#include <QDateTime>

class VACCTLUTIL_EXPORT DpeConnection
{
public:

	DpeConnection(const char *dpeName);
	~DpeConnection() {}

	// Access
	inline const char *getName(void) const { return (const char *)name; }

	int getCount(DataEnum::DataMode mode) const;
	void plusCount(DataEnum::DataMode mode);
	void minusCount(DataEnum::DataMode mode);

	const QVariant &getValue(DataEnum::DataMode mode, QDateTime &timeStamp) const;
	void setValue(DataEnum::DataMode mode, const QVariant &value, const QDateTime &timeStamp);

protected:
	//	DPE name
	QByteArray	name;

	// Dummy value - to be returned for unknwon mode
	static QVariant	dummy;

	// Last known online value
	QVariant	onlineValue;

	// Last known replay value
	QVariant	replayValue;

	// Last known polling value
	QVariant	pollingValue;

	// Timestamp for online value
	QDateTime	onlineTimeStamp;

	// Timestamp for replay value
	QDateTime	replayTimeStamp;

	// Timestamp for polling value
	QDateTime	pollingTimeStamp;

	// Number of online connections to this DPE
	int			online;

	// Number of replay connections to this DPE
	int			replay;

	// Number of polling connections for this DPE
	int			polling;
};

#endif	// DPECONNECTION_H
