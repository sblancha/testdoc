//	Implementation of EqpVBeamParamInt class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVBeamParamInt.h"

#include "DataPool.h"
#include "FunctionalType.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVBeamParamInt::EqpVBeamParamInt(const Eqp &source) : Eqp(source)
{
	valueOnline = valueReplay = 0;
	functionalType = FunctionalType::VBEAM_PARAM_INT;

	// Decide for vacuum type based on last character in DP name
	if(dpName)
	{
		if(dpName[strlen(dpName) - 1] == 'B')
		{
			vacType = VacType::BlueBeam;
		}
		else
		{
			vacType = VacType::RedBeam;
		}
	}
}

EqpVBeamParamInt::~EqpVBeamParamInt()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to analog value of device. The list is used
**		to build value history
**
**	ARGUMENTS
**		valueDpes	- Variable where all value DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVBeamParamInt::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	addDpeNameToList(valueDpes, "Value");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVBeamParamInt::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "Value", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVBeamParamInt::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "Value", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVBeamParamInt::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "Value"))
	{
		if(value.canConvert(QVariant::Int))
		{
			int newValue = value.toInt();
			if(valueOnline == newValue)
			{
				return;
			}
			valueOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamInt::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "Value"))
	{
		if(value.canConvert(QVariant::Int))
		{
			int newValue = value.toInt();
			if(valueReplay == newValue)
			{
				return;
			}
			valueReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

DataEnum::ValueType EqpVBeamParamInt::getMainValueType(void) const
{
	DataEnum::ValueType result = DataEnum::ValueTypeNone;
	switch(ctrlSubType)
	{
	case 10:	// LHC fill number
		result = DataEnum::FillNumber;
		break;
	default:
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Return main value for device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVBeamParamInt::getMainValue(DataEnum::DataMode mode)
{
	return mode == DataEnum::Replay ? valueReplay : valueOnline;
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamInt::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	getMainValueString(string, mode);
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamInt::getMainValueString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainValueString(valueReplay, string);
	}
	else
	{
		getMainValueString(valueOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		intensity	- Value DPE
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamInt::getMainValueString(int value, QString &string)
{
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d", value);
#else
	sprintf(buf, "%d", value);
#endif
	string = buf;
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamInt::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainStateStringForMenu(valueReplay, string);
	}
	else
	{
		getMainStateStringForMenu(valueOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		intensity	- Value DPE
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamInt::getMainStateStringForMenu(int value, QString &string)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainValueString(value, valueString);
	string += valueString;
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVBeamParamInt::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	getMainStateStringForMenu(string, mode);
}
#endif

