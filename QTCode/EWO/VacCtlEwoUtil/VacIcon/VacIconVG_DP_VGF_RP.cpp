//	Implementation of VacIconVG_DP_VGF_RP class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVG_DP_VGF_RP.h"

#include "EqpVG_DP.h"

#include <QPainter>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVG_DP_VGF_RP::VacIconVG_DP_VGF_RP(QWidget *parent, Qt::WindowFlags f) :
	VacIconVG_STD(parent, f)
{
	setFixedSize(21, 35);
}

VacIconVG_DP_VGF_RP::~VacIconVG_DP_VGF_RP()
{
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVG_DP_VGF_RP::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		return VacIconGeometry(21, 35, 10, 38, 1);
		break;
	default:
		break;
	}
	return VacIconGeometry(35, 21, 38, 10, 1);
}

/*
**	FUNCTION
**		Override method of VacIconVG: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVG_DP_VGF_RP::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(21);
		size.setHeight(35);
		break;
	default:
		size.setWidth(35);
		size.setHeight(21);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
}

void VacIconVG_DP_VGF_RP::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVG_DP"), "VacIconVG_DP_VGF_RP::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVG_DP *)pEqp;
	}
	forceRedraw();
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVG_DP_VGF_RP::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
// L.Kopylov 08.10.2013	painter.eraseRect(0, 0, width(), height());
//	painter.fillRect(rect(), Qt::green);	// For testing
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Draw connecting pipe
	if(drawConnectionPipe)
	{
		switch(direction)
		{
		case VacIconContainer::Up:	// No rotation
			painter.fillRect((width() >> 1) - (pipeWidth >> 1), height() - 4, pipeWidth ? pipeWidth : 1, 5, pipeColor);
			break;
		case VacIconContainer::Down:
			painter.fillRect((width() >> 1) - (pipeWidth >> 1), 0, pipeWidth ? pipeWidth : 1, 5, pipeColor);
			break;
		case VacIconContainer::Left:
			painter.fillRect(width() - 4, (height() >> 1) - (pipeWidth >> 1), 5, pipeWidth ? pipeWidth : 1, pipeColor);
			break;
		case VacIconContainer::Right:	// 90 degrees clockwise
			painter.fillRect(0, (height() >> 1) - (pipeWidth >> 1), 5, pipeWidth ? pipeWidth : 1, pipeColor);
			break;
		}
	}

	// Pipe is ready - draw gauge image
	drawImage(painter);

	// Draw BlockedOFF state (if any)
	if(pEqp)
	{
		if(pEqp->isBlockedOff(mode))
		{
			QPen blackPen(Qt::black, 2);
			painter.setPen(blackPen);
			painter.drawLine((width() >> 1) - 10, (height() >> 1) - 10, (width() >> 1) + 10, (height() >> 1) + 10);
			painter.drawLine((width() >> 1) - 10, (height() >> 1) + 10, (width() >> 1) + 10, (height() >> 1) - 10);
		}
	}
}

void VacIconVG_DP_VGF_RP::drawImage(QPainter &painter)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		drawImageVertical(painter, 0);
		break;
	case VacIconContainer::Down:
		drawImageVertical(painter, 1);
		break;
	case VacIconContainer::Left:
		drawImageHorizontal(painter, 0);
		break;
	case VacIconContainer::Right:
		drawImageHorizontal(painter, 1);
		break;
	}
}

void VacIconVG_DP_VGF_RP::drawImageVertical(QPainter &painter, int offset)
{
	setPainterForMainImage(painter);

	// Filled circle for Pirani
	painter.setClipping(true);
	painter.setClipRect(0, 0 + offset, 21, 17);
	QPen pen = painter.pen();
	painter.setPen(Qt::NoPen);
	painter.drawPie(1, 1 + offset, 18, 18, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(1, 1 + offset, 18, 18, 0, 360 * 16);

	// Filled circle for Penning
	painter.setClipRect(0, (height() >> 1) + offset, 21, 17);
	QBrush brush(painter.brush());
	painter.setBrush(Qt::gray);
	painter.setPen(Qt::NoPen);
	painter.drawPie(1, (height() >> 1) - 3 + offset, 18, 18, 0, 360 * 16);
	setPainterForPenning(painter, brush);
	painter.drawPie(1, (height() >> 1) - 3 + offset, 18, 18, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(1, (height() >> 1) - 3 + offset, 18, 18, 0, 360 * 16);

	// Pirani symbol
	painter.setClipping(false);
	painter.setPen(Qt::black);
	QPolygon points(10);
	points.setPoint(0, 3, 9 + offset);
	points.setPoint(1, 5, 9 + offset);
	points.setPoint(2, 5, 13 + offset);
	points.setPoint(3, 8, 13 + offset);
	points.setPoint(4, 8, 7 + offset);
	points.setPoint(5, 12, 7 + offset);
	points.setPoint(6, 12, 13 + offset);
	points.setPoint(7, 15, 13 + offset);
	points.setPoint(8, 15, 9 + offset);
	points.setPoint(9, 17, 9 + offset);
	painter.drawPolyline(points);

	// Penning symbol
	QPen blackWide(Qt::black, 2);
	painter.setPen(blackWide);
	points.remove(4, 6);
	points.setPoint(0, 6, 20 + offset);
	points.setPoint(1, 6, 26 + offset);
	points.setPoint(2, 15, 26 + offset);
	points.setPoint(3, 15, 20 + offset);
	painter.drawPolyline(points);

	painter.drawArc(9, 20 + offset, 3, 3, 0, 360 * 16);
}

void VacIconVG_DP_VGF_RP::drawImageHorizontal(QPainter &painter, int offset)
{
	setPainterForMainImage(painter);

	// Filled circle for Pirani
	painter.setClipping(true);
	painter.setClipRect(0 + offset, 0, 17, 21);
	QPen pen = painter.pen();
	painter.setPen(Qt::NoPen);
	painter.drawPie(1 + offset, 1, 18, 18, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(1 + offset, 1, 18, 18, 0, 360 * 16);

	// Filled circle for Penning (close to connection point)
	painter.setClipRect((width() >> 1) + offset, 1, 17, 21);
	QBrush brush(painter.brush());
	painter.setBrush(Qt::gray);
	int baseVGP = (width() >> 1) - 3 + offset;
	painter.drawPie(baseVGP, 1, 18, 18, 0, 360 * 16);
	setPainterForPenning(painter, brush);
	painter.drawPie(baseVGP, 1, 18, 18, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(baseVGP, 1, 18, 18, 0, 360 * 16);

	// Pirani symbol
	painter.setClipping(false);
	painter.setPen(Qt::black);
	QPolygon points(10);
	points.setPoint(0, 3 + offset, 9);
	points.setPoint(1, 5 + offset, 9);
	points.setPoint(2, 5 + offset, 13);
	points.setPoint(3, 8 + offset, 13);
	points.setPoint(4, 8 + offset, 7);
	points.setPoint(5, 12 + offset, 7);
	points.setPoint(6, 12 + offset, 13);
	points.setPoint(7, 15 + offset, 13);
	points.setPoint(8, 15 + offset, 9);
	points.setPoint(9, 16 + offset, 9);
	painter.drawPolyline(points);

	// Penning symbol
	QPen blackWide(Qt::black, 2);
	painter.setPen(blackWide);
	points.remove(4, 6);
	points.setPoint(0, baseVGP + 6, 8);
	points.setPoint(1, baseVGP + 6, 14);
	points.setPoint(2, baseVGP + 14, 14);
	points.setPoint(3, baseVGP + 14, 8);
	painter.drawPolyline(points);

	painter.drawArc(baseVGP + 8, 8, 3, 3, 0, 360 * 16);
}

void VacIconVG_DP_VGF_RP::setPainterForPenning(QPainter &painter, QBrush &brush)
{
	// Set another brush style if Penning is disabled
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			break;
		default:
			if(!pEqp->isPlcAlarm(mode))
			{
				unsigned rr1 = pEqp->getRR1(mode);
				if(rr1 & 0x00100000)	// Penning Disabled bit
				{
					brush.setStyle(Qt::Dense5Pattern);
				}
			}
			break;
		}
	}
	painter.setBrush(brush);
}

