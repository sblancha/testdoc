#ifndef	PROFILEALLLHCINOUT_H
#define	PROFILEALLLHCINOUT_H

//	All LHC profile - for inner/outer beams

#include "ProfileAllLHCInOutWidget.h"
#include <qdialog.h>

class MainPart;
#include "Eqp.h"
#include "EqpMsgCriteria.h"

class ProfileAllLHCInOut : public QDialog
{
	Q_OBJECT

public:
	ProfileAllLHCInOut();
	~ProfileAllLHCInOut();

	static bool alreadyOpen(void);

protected:
	// Instances of 2 dialogs
	static ProfileAllLHCInOut	*dialogs[2];

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Widget with images
	ProfileAllLHCInOutWidget	*pWidget;

	// Index of this instance in array of dialogs
	int							myIndex;

	// ID of 'Synoptic' item in popup menu
	QAction 						*pSynopticMenuAction;

	// ID of 'Period' item in popup menu
	QAction 						*pPeriodMenuAction;

	// Main part from where menu was activated
	MainPart					*pMenuMainPart;

	virtual void resizeEvent(QResizeEvent *e);

	void analyzeTypes(VacEqpTypeMask &allMpMask);
	void applyMask(VacEqpTypeMask &newEqpMask);
	inline void setBeamLimits(int min, int max) { if(pWidget) { pWidget->setBeamLimits(min, max); } }
	inline void setLinLimits(int min, int max) { if(pWidget) { pWidget->setLinLimits(min, max); } }


private slots:
	void beamLimitsChange(int min, int max);
	void linLimitsChange(int min, int max);
	void mpLabelPress(QMouseEvent *e, MainPart *pMainPart);
	void mpMenu(QAction *pAction);
};

#endif	// PROFILEALLLHCINOUT_H
