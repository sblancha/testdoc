//	Implementation of DriverRef class
//////////////////////////////////////////////////////////////////////////////

#include "DriverRef.h"
#include "ImportPool.h"

#include <stdlib.h>
#include <string.h>

QList<DriverRef *>	*DriverRef::pInstances = new QList<DriverRef *>();

QList<DriverRef *> &DriverRef::getInstances(void)
{
	return *pInstances;
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DriverRef::DriverRef(int drvNum, const char *type)
{
	this->drvNum = drvNum;
	this->type = (char *)malloc(strlen(type ) + 1);
#ifdef Q_OS_WIN
	strcpy_s(this->type, strlen(type ) + 1, type);
#else
	strcpy(this->type, type);
#endif
	refCount = 1;
}

DriverRef::~DriverRef()
{
	if(type)
	{
		free((void *)type);
	}
}


/*
**	FUNCTION
**		Clear list of instances
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DriverRef::clear(void)
{
	while(!pInstances->isEmpty())
	{
		delete pInstances->takeFirst();
	}
}

/*
**	FUNCTION
**		Add reference to driver number
**
**	ARGUMENTS
**		drvNum	- driver number
**		type	- name of driver type
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DriverRef::addRef(int drvNum, const char *type)
{
	foreach(DriverRef *pItem, *pInstances)
	{
		if(pItem->drvNum == drvNum)
		{
			pItem->refCount++;
			return;
		}
	}
	pInstances->append(new DriverRef(drvNum, type));
}


/*
**
**	FUNCTION
**		Dump instance content to output file for debugging
**
**	PARAMETERS
**		pFile		- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DriverRef::dump(FILE *pFile)
{
	if(!ImportPool::isDebugMode())
	{
		return;
	}
	for(int idx = 0 ; idx < pInstances->count() ; idx++)
	{
		DriverRef *pRef = pInstances->at(idx);
		fprintf(pFile, "================ Driver ref: type <%s> DRV %d references %ld\n",
			pRef->type, pRef->drvNum, pRef->refCount);
	}
}
