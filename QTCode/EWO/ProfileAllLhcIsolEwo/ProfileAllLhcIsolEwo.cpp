//	Implementation of ProfileAllLhcIsolEwo class
/////////////////////////////////////////////////////////////////////////////////


#include "ProfileAllLhcIsolEwo.h"

#include "DpConnection.h"
#include "DpeHistoryQuery.h"
#include "DpPolling.h"
#include "MobileHistoryQuery.h"
#include "DpeHistoryDepthQuery.h"

EWO_PLUGIN(ProfileAllLhcIsolEwo)


ProfileAllLhcIsolEwo::ProfileAllLhcIsolEwo(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new ProfileAllLhcIsolWidget(parent);

	// Connect to signals of MainSps
	connect(baseWidget, SIGNAL(profileDpDown(int, int, int, int, int, const char *)),
		this, SLOT(dpMouseDown(int, int, int, int, int, const char *)));
	/* Not used anyway LIK 27.09.2010
	connect(baseWidget, SIGNAL(pollingPeriodChanged(int)),
		this, SLOT(pollingPeriodChange(int)));
	*/


	// Connect to signals of DpConnection instance
	DpConnection &connection = DpConnection::getInstance();
	connect(&connection, SIGNAL(changed(void)), this, SLOT(dpConnectChanged(void)));

	// Connect to signals of DpeHistoryQuery instance
	DpeHistoryQuery &historyQuery = DpeHistoryQuery::getInstance();
	connect(&historyQuery, SIGNAL(changed(void)), this, SLOT(dpeHistoryQueryQueueChange(void)));

	// Connect to signals of DpPolling instance
	DpPolling &polling = DpPolling::getInstance();
	connect(&polling, SIGNAL(changed(void)), this, SLOT(dpPollingChanged(void)));

	// Connect to signals of MobileHistoryQuery instance
	MobileHistoryQuery &mobileHist = MobileHistoryQuery::getInstance();
	connect(&mobileHist, SIGNAL(changed(void)), this, SLOT(mobileHistoryQueryChange(void)));

	// Connect to signals of DpeHistoryDepthQuery instance
	DpeHistoryDepthQuery &dpeHistDepth = DpeHistoryDepthQuery::getInstance();
	connect(&dpeHistDepth, SIGNAL(changed(void)), this, SLOT(dpeHistoryDepthQueryChange(void)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList ProfileAllLhcIsolEwo::signalList(void) const
{
	QStringList list;
	list.append("dpMouseDown(int button, int mode, int x, int y, int extra, string dpName)");
	list.append("dpConnectRequest()");
	list.append("dpeHistoryQueryQueueChange()");
	list.append("dpPollingRequest()");
	list.append("pollingPeriodChanged(int period)");
	list.append("mobileHistoryRequest()");
	list.append("dpeHistoryDepthRequest()");
	return list;
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList ProfileAllLhcIsolEwo::methodList(void) const
{
	QStringList list;
	list.append("int dataReady(int lhcPartIndex)");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ProfileAllLhcIsolEwo::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "dataReady")
	{
		return invokeDataReady(name, values, error);
	}
	else
	{
		error = "Uknown method ProfileAllLhcIsolEwo.";
		error += name;
	}
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			int dataReady()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ProfileAllLhcIsolEwo::invokeDataReady(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (lhcPartIndex) is not int";
		return QVariant();
	}
	int lhcPartIndex = values[0].toInt();
	baseWidget->setLhcPartIndex(lhcPartIndex);
	return QVariant();
}

/*
**	FUNCTION
**		Slot receiving dpMouseDown signal of MainSps, emit
**		signal 'dpMouseDown' to PVSS
**
**	ARGUMENTS
**		button	- mouse button number
**		mode	- Data acquisition mode
**		x		- X coordinate of mouse pointer
**		y		- Y coordinate of mouse pointer
**		extra	- Extra parameter to identify PART of icon
**		dpName	- Name of sector where mouse press occured
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLhcIsolEwo::dpMouseDown(int button, int mode, int x, int y, int extra, const char *dpName)
{
	QList<QVariant> args;
	args.append(QVariant(button));
	args.append(QVariant(mode));
	args.append(QVariant(x));
	args.append(QVariant(y));
	args.append(QVariant(extra));
	args.append(QVariant(QString(dpName)));
	emit signal("dpMouseDown", args);
}

/*
**	FUNCTION
**		Slot receiving changed() signal of DpConnection instance, emit
**		signal 'dpConnectRequest' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLhcIsolEwo::dpConnectChanged(void)
{
	emit signal("dpConnectRequest");
}

/*
**	FUNCTION
**		Slot receiving changed() signal of DpeHistoryQuery instance, emit
**		signal 'dpeHistoryQueryQueueChange' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLhcIsolEwo::dpeHistoryQueryQueueChange(void)
{
	emit signal("dpeHistoryQueryQueueChange");
}

/*
**	FUNCTION
**		Slot receiving changed() signal of DpPolling instance, emit
**		signal 'dpPollingRequest' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLhcIsolEwo::dpPollingChanged(void)
{
	emit signal("dpPollingRequest");
}

/*
**	FUNCTION
**		Slot receiving pollingPeriodChanged signal of MainLhc, emit
**		signal 'pollingPeriodChanged' to PVSS
**
**	ARGUMENTS
**		period	- New polling period
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLhcIsolEwo::pollingPeriodChanged(int period)
{
	emit signal("pollingPeriodChanged", QVariant(period));
}

/*
**	FUNCTION
**		Slot receiving changed() signal of MobileHistoryQuery instance, emit
**		signal 'mobileHistoryRequest' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLhcIsolEwo::mobileHistoryQueryChange(void)
{
	emit signal("mobileHistoryRequest");
}

/*
**	FUNCTION
**		Slot receiving changed() signal of DpeHistoryDepthQuery instance, emit
**		signal 'dpeHistoryDepthRequest' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLhcIsolEwo::dpeHistoryDepthQueryChange(void)
{
	emit signal("dpeHistoryDepthRequest");
}

