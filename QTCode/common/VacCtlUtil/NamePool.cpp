// NamePool.cpp: implementation of the NamePool class.
//
//////////////////////////////////////////////////////////////////////

#include "NamePool.h"
#include <stdlib.h>
#include <string.h>

#define	SU_BUFFER_SIZE	(4096)
#define	MU_BUFFER_SIZE	(1024)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

NamePool::NamePool()
{

}

NamePool::~NamePool()
{
	clear();
}

/*
**
** FUNCTION
**		Add string which most probably will not appear more than once
**		('single-use' string) to pool.
**
** PARAMETERS
**		pString	- Pointer to string to be added.
**
** RETURNS
**		Pointer to copy of string,
**		NULL if source string is empty.
**
** CAUTIONS
**		None
*/
char *NamePool::addString(const char *pString)
{
	// What is the length of name to be added?
	if(!pString)
	{
		return NULL;
	}
	if(!*pString)
	{
		return NULL;
	}
	int len = (int)strlen(pString) + 1;

	// Check if new memory block shall be allocated or previous one
	// still has enough space to add new name
	if((!nSuBuffers) || ((suCharIdx + len) > SU_BUFFER_SIZE))
	{
		suBuffers = (char **)realloc((void *)suBuffers, (nSuBuffers + 1) * sizeof(char *));
		suBuffers[nSuBuffers++] = (char *)malloc(len > SU_BUFFER_SIZE ? len : SU_BUFFER_SIZE);
		suCharIdx = 0;
	}

	// Add new name to pool
	char *result = &(suBuffers[nSuBuffers-1][suCharIdx]);
#ifdef Q_OS_WIN
	strcpy_s(result, len, pString);
#else
	strcpy(result, pString);
#endif
	suCharIdx += len;
	return result;
}

/*
**
** FUNCTION
**		Add string which most probably will appear many times
**		('multi-use' string) to pool.
**
** PARAMETERS
**		pString	- Pointer to string to be added.
**
** RETURNS
**		Pointer to copy of string,
**		NULL if source string is empty.
**
** CAUTIONS
**		None
*/
char *NamePool::addMultiUseString(const char *pString)
{
	char	*result;

	if(!pString)
	{
		return NULL;
	}
	if(!*pString)
	{
		return NULL;
	}

	// Find if string already appears in buffers
	for(int n = 0 ; n < nMuBuffers ; n++)
	{
		for(int i = 0 ; i < MU_BUFFER_SIZE ; )
		{
			result = &(muBuffers[n][i]);
			if(!*result)
			{
				break;	// No more used string in buffers
			}
			if(!strcmp(result, pString))
			{
				return result;
			}
			i += (int)strlen(result) + 1;
		}
	}

	// New string - add to buffers
	int len = (int)strlen(pString) + 1;
	if((!nMuBuffers) || ((muCharIdx + len) > MU_BUFFER_SIZE))
	{
		muBuffers = (char **)realloc((void *)muBuffers, (nMuBuffers + 1) * sizeof(char *));
		muBuffers[nMuBuffers++] = (char *)calloc(len > MU_BUFFER_SIZE ? len : MU_BUFFER_SIZE,
			sizeof(char));
		muCharIdx = 0;
	}
	result = &(muBuffers[nMuBuffers-1][muCharIdx]);
#ifdef Q_OS_WIN
	strcpy_s(result, len, pString);
#else
	strcpy(result, pString);
#endif
	muCharIdx += len;
	return result;
}
/*
**
** FUNCTION
**		Free all memory allocated for string buffers.
**
** PARAMETERS
**		None
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void NamePool::clear( void )
{
	int	n;

	if(suBuffers)
	{
		for(n = 0 ; n < nSuBuffers ; n++)
		{
			free((void *)suBuffers[n]);
		}
		free( (void *)suBuffers );
	}
	suBuffers = NULL;
	nSuBuffers = 0;

	if(muBuffers)
	{
		for(n = 0 ; n < nMuBuffers ; n++)
		{
			free((void *)muBuffers[n]);
		}
		free((void *)muBuffers);
	}
	muBuffers = NULL;
	nMuBuffers = 0;

	suCharIdx = muCharIdx = 0;
}
