#ifndef	BAKEOUTCHANNELTREEITEM_H
#define	BAKEOUTCHANNELTREEITEM_H

// Item of tree view representing one channel of bakeout rack

#include <QTreeWidget>

#include "BakeoutWorkDp.h"

#include "BakeoutEqpTreeItem.h"

#include "MobileTreeItemTypes.h"

class BakeoutChannelTreeItem : public QTreeWidgetItem
{
public:
	BakeoutChannelTreeItem(QTreeWidgetItem *parent, int channel, enum BakeoutWorkDp::ChannelValueType valueType);
	~BakeoutChannelTreeItem();

	// Access
	inline int getChannel(void) const { return channel; }
	inline enum BakeoutWorkDp::ChannelValueType getValueType(void) const { return valueType; }
	inline Eqp *getEqp(void) { return ((BakeoutEqpTreeItem *)parent())->getEqp(); }

protected:
	// Channel number
	int	channel;

	// Value type
	enum BakeoutWorkDp::ChannelValueType valueType;
};

#endif	// BAKEOUTCHANNELTREEITEM_H
