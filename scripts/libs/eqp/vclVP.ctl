
/***********************************************************************************
****************** Constants for vacuum pump control **************************
***********************************************************************************/

// Bitmasks for VRPI_W status bits (RR1 - 32 bits)
const unsigned
	VRPI_W_R_LOCKED = 0x80000000u,
	VRPI_W_R_VALID = 0x40000000u,
	VRPI_W_R_REMOTE = 0x20000000u,
	VRPI_W_R_AUTO = 0x08000000u,
	VRPI_W_R_ON_CMD = 0x02000000u,
	VRPI_W_R_ERRORS = 0x00800000u,
	VRPI_W_R_WARNINGS = 0x00400000u,
	VRPI_W_R_PR_VALID = 0x00040000u,
	VRPI_W_R_ON = 0x00020000u,
	VRPI_W_R_OFF = 0x00010000u,
	VRPI_W_R_UNDERRANGE = 0x200u,
	VRPI_W_R_REST_WARN = 0x1100u,
// VP_STDIO specific
 	VP_STDIO_MANUAL = 0x04000000u,
 	VP_STDIO_NOMINAL_SPEED = 0x08000000u,
	VP_STDIO_LOCAL = 0x10000000u,
	VP_STDIO_HW_INTERLOCK= 0x20000000u,
	VP_STDIO_INTERLOCK= 0x00200000u,
	VP_STDIO_ON_INTERLOCK= 0x00100000u,
	VP_STDIO_AUTO_ON_ORDER= 0x00080000u,
	VP_STDIO_MAN_ON_ORDER= 0x00040000u,
	VP_STDIO_FORCED = 0x00010000u;
// VPS & VPS_Process specific
const unsigned
	VPS_R_F2_COMMAND = 0x80000000u,
	VPS_R_F2_USED = 0x20000000u,
	VPS_R_LOCAL = 0x10000000u,
	VPS_R_SUBLIMATION = 0x8000000u,
	VPS_R_MANUAL = 0x4000000u,
	VPS_R_ON = 0x2000000u,
	VPS_R_DEGASSING = 0x1000000u,
	VPS_R_CYCLE_ON = 0x00200000u,
	VPS_R_CYCLE_SET = 0x00100000u,
	VPS_R_FILAMENT1 = 0x00010000u,
	VPS_R_FILAMENT2 = 0x00020000u;
// VP common
// VP rr1
const unsigned
	VP_LOCKED = 		0x80000000u,
	VP_BAKEOUT = 		0x80000000u,
	VP_VALID = 			0x40000000u,
	VP_TMP_NOMINAL = 	0x20000000u,
	VP_LOCAL = 			0x10000000u,
	VP_TMP_ACCEL = 		0x08000000u,
	VP_MANUAL = 		0x04000000u,
	VP_ON = 			0x02000000u,
	VP_OFF = 			0x01000000u,
	VP_ERROR = 			0x00800000u,
	VP_WARNING = 		0x00400000u,
	VP_INTERLOCK = 		0x00200000u,
	VP_TMP_ON = 		0x00200000u,
	VP_ON_INTERLOCK	= 	0x00100000u,
	VP_PP_THERM_OK = 	0x00100000u, 
	VP_AUTO_ON_ORDER = 	0x00080000u,
	VP_PP_PH_OK = 		0x00080000u,
	VP_MAN_ON_ORDER	= 	0x00040000u,
	VP_PP_CABLE_OK =	0x00040000u,
	VP_ON_DO = 			0x00020000u,
	VP_PP_3PH =			0x00020000u,	
	VP_FORCED = 		0x00010000u,
	VP_PP_ON = 			0x00010000u,
	VP_VVD_CL = 		0x00008000u,
	VP_VVD_OP = 		0x00004000u,
	VP_VVP_CL = 		0x00002000u,
	VP_VVP_OP = 		0x00001000u,
	VP_VVI_CL = 		0x00000800u,
	VP_VVI_OP = 		0x00000400u,
	VP_VVT_CL = 		0x00000200u,
	VP_VVT_OP = 		0x00000100u,
	VP_VVR2_ITL = 		0x00000080u,
	VP_VVR2_FO = 		0x00000040u,
	VP_VVR2_CL = 		0x00000020u,
	VP_VVR2_OP = 		0x00000010u,
	VP_VVR1_ITL =		0x00000008u,
	VP_VVR1_FO = 		0x00000004u,
	VP_VVR1_CL = 		0x00000002u,
	VP_VVR1_OP = 		0x00000001u;
// VP rr2
const unsigned
	VP_VVD_ERROR = 		0x80000000u,	
	VP_VVP_ERROR = 		0x40000000u,	
	VP_VVI_ERROR = 		0x20000000u,
	VP_VVT_ERROR = 		0x10000000u,
	VP_VVR2_ERROR = 	0x08000000u,
	VP_VVR1_ERROR = 	0x04000000u,
	VP_PP_ERROR = 		0x02000000u,
	VP_TMP_ERROR = 		0x01000000u,
	VP_VVD_WARNING =	0x00800000u,	
	VP_VVP_WARNING =	0x00400000u,	
	VP_VVI_WARNING =	0x00200000u,
	VP_VVT_WARNING = 	0x00100000u,
	VP_VVR2_WARNING = 	0x00080000u,
	VP_VVR1_WARNING = 	0x00040000u,
	VP_PP_WARNING = 	0x00020000u,
	VP_TMP_WARNING = 	0x00010000u,
	VP_VG2_ON = 		0x00000800u,
	VP_VG1_ON = 		0x00000400u,
	VP_VG2_ERROR = 		0x00000200u,
	VP_VG1_ERROR = 		0x00000100u,
	VP_VG1_OR = 		0x00000002u,
	VP_VG1_UR = 		0x00000001u;
// VP bitmask
const unsigned
	VP_MODE_MASK =		0x0000F000u, // + SHIFT RIGHT 12BITS
	VP_MODE_SHIFTR =	12;
// VP wr1
const unsigned
	VP_WR_RESET = 		0x80000000u,
	//VP_WR_
	VP_WR_REMOTE = 		0x20000000u,
	VP_WR_LOCAL = 		0x10000000u,
	//VP_WR_
	VP_WR_MANUAL = 		0x04000000u,
	VP_WR_ON = 			0x02000000u,
	VP_WR_OFF = 		0x01000000u,
	VP_WR_VVR2_FO = 	0x00800000u,
	VP_WR_VVR2_UNFO = 	0x00400000u,
	VP_WR_VVR2_OP = 	0x00200000u,
	VP_WR_VVR2_CL = 	0x00100000u,
	VP_WR_VVR1_FO = 	0x00080000u,
	VP_WR_VVR1_UNFO = 	0x00040000u,
	VP_WR_VVR1_OP = 	0x00020000u,
	VP_WR_VVR1_CL = 	0x00010000u,
	//VP_WR_
	//VP_WR_
	VP_WR_VG1_ON =		0x00002000u,
	VP_WR_VG1_OFF =		0x00001000u,
	VP_WR_SET_MODE = 	0x00000800u,
	//VP_WR_
	//VP_WR_
	//VP_WR_
	//VP_WR_
	VP_WR_PP_THERM_RST =0x00000080u,
	VP_WR_TMP_ERROR_RST =0x00000040u,
	VP_WR_BAKE_ON = 	0x00000020u,
	VP_WR_BAKE_OFF =	0x00000010u;
// VP wr1 bitmask
const unsigned
	VP_WR_MODE_MASK =		0x00000F00u, // + SHIFT RIGHT 8BITS
	VP_WR_MODE_SHIFTR =	8;
	
	
// VR_PI_CHAN
const unsigned
	VR_PI_CHAN_PROTECT = 0x08000000u,
	VR_PI_CHAN_STEP = 0x00020000u;
// Bitmasks for VR PI command bits (WR1 - 16 bits)
const unsigned
	VRPI_W_W_REMOTE = 0x2000u,
	VRPI_W_W_LOCAL = 0x1000u,
	VRPI_W_W_AUTO = 0x0800u,
	VRPI_W_W_MANUAL = 0x0400u,
	VRPI_W_W_ON = 0x0200u,
	VRPI_W_W_OFF = 0x0100u,
	VRPI_W_W_FORCE = 0x4000u,
	VRPI_W_W_RESET = 0x8000u,
// VR_PI_CHAN specific	
	VR_PI_CHAN_W_STEP = 0x1000u,
	VR_PI_CHAN_W_FIXED = 0x2000u,
	VR_PI_CHAN_W_AUTO = 0x0800u,
	VR_PI_CHAN_W_MANUAL = 0x0400u,
	VR_PI_CHAN_W_ON = 0x0200u,
	VR_PI_CHAN_W_OFF = 0x0100u,
	VR_PI_CHAN_W_FORCE = 0x4000u,
  VR_PI_CHAN_W_FIXED_PARAM_CODE = 0x12u; //B#16#12 :	Switch to Fixed mode Parameter code

	
// VPS & VPS_Process specific
const unsigned
  VPS_W_APPLY = 0x40u,
  VPS_W_SWITCH_FIL = 0x20u,
  VPS_W_RESET_COUNT = 0x10u,
  VPS_W_FIL2 = 0x8u,
  VPS_W_FIL1 = 0x4u,
  VPS_W_DEGAS = 0x2u,
  VPS_W_SUBLIM = 0x1u,
	VPS_W_START_NOW = 0x1000u;

// PS_CMW specific
const int VPI_PS_CMW_W_STOP = 6,
  VPI_PS_CMW_W_START = 7;
const int VPI_PS_CMW_R_ON = 10,
  VPI_PS_CMW_R_OFF = 11,
  VPI_PS_CMW_R_VALID = 1;

	
//LhcVacActionPossible_VP:
/** Check if pump is in state that allows executing actions for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, action to be executed
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_VP(string dpName, string dpType, const dyn_anytype act, bool &possible,
	dyn_string &exceptionInfo )
{
  dyn_errClass 	err;
  unsigned	state;
  string	dpNameAct, dpTypeAct;
		
  LhcVacGetDpForAct(dpName, dpType, dpNameAct, dpTypeAct, exceptionInfo);
  if(dpTypeAct == "VRPI_VELO")
  {
// all actions are impossible for VRPI at VELO area
// ONLY Set connected/Not connected available SME
    possible = true;
    return;
  }
  if(dpTypeAct == "VPI_PS_CMW")
  {
    possible = true;
    return;
  }
  if((dpTypeAct == "VPS") || (dpTypeAct == "VPS_Process"))
  {
    possible = true;
    return;
  }
  dpGet(dpNameAct + ".RR1", state);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacActionPossible_VP: Read RR1 for <" + dpName + "> failed: " + err, "" );
    possible = false;
    return;
  }
  if((state & VRPI_W_R_LOCKED) != 0)
  {
    possible = FALSE;
  }
  else
  {
    possible = TRUE;
  }
}
//LhcVacIsShowMenuItem_Vp:
/** Check if menu item has to be shown

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param dpType: in, DP type
	@param showItem: out,  (show/hide) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacIsShowMenuItem_VP(string dpName, string dpType, unsigned action, bool &showItem)
{
  string masterDpName, usageInMaster;
  switch(action & LVA_ACTION_MASK)
  {
  case LVA_DEVICE_ON:
  case LVA_DEVICE_OFF:
  case LVA_DEVICE_RESET:
    showItem = true;
    break;
  case LVA_DEVICE_MANUAL:
  case LVA_DEVICE_AUTO:
  case LVA_DEVICE_FORCE:  
   LhcVacEqpMasterProcess(dpName, masterDpName, usageInMaster);
   if(masterDpName == "")
   {
    // stand alone device    
      showItem = true;
      return;
   }
   string masterDpType = dpTypeName(masterDpName);
   switch(masterDpType)
   {
   case "VPG_6E01":
      showItem = false;
      break;
   default:
      showItem = true;
      break;      
   }
   break;
  default:
    return;
  }  
}
/**
@brief Return privelege required when dynamic conditions shall be checked
@param[in]		dpName			Device dp name
@param[in]		action			Action requested
@param[out]		privRequired	Level of privilege required (0:Not possible, 1:Monitor, 2:Operator, 3:Expert, 4:Admin)
@param[out]		exceptionInfo	Exception information
*/
void LhcVacGetRequiredActPriv_VP(string dpName, const dyn_anytype action, int &privRequired, dyn_string &exceptionInfo)
{
  string dpType = dpTypeName(dpName);
  switch(dpType)
  {
  case "VP_STDIO":  
    CheckCommandForManualMode_VP(dpName, action, privRequired, exceptionInfo);
    break;
  case "VP_GU":
	LhcVacGetRequiredActPriv_VP_GU(dpName, action, privRequired, exceptionInfo);
	break;
  default: //all VRPI + VPI
    LhcVacGetRequiredActPriv_VRPI(dpName, action, privRequired, exceptionInfo);
    return;
  }
}

/**
@brief Return privelege required for VP_GU when dynamic conditions shall be checked
@param[in]		dpName			Device dp name
@param[in]		action			Action requested
@param[out]		privRequired	Level of privilege required (0:Not possible, 1:Monitor, 2:Operator, 3:Expert, 4:Admin)
@param[out]		exceptionInfo	Exception information
*/
void LhcVacGetRequiredActPriv_VP_GU(string dpName, const dyn_anytype action, int &privRequired, dyn_string &exceptionInfo)
{
		// To be done if dynamic conditions are needed
		// if required add case in LhcVacCheckDevActPrivileges()
		return;
}	

void LhcVacGetRequiredActPriv_VRPI(string dpName, const dyn_anytype action, int &privRequired, dyn_string &exceptionInfo)
{
  // L.Kopylov after discussion with F.Antoniotti - 2 changes:
  // 1) not only OFF, but also ON commands require check
  // 2) expert privilege is enough for both actions
  switch(action[1] & LVA_ACTION_MASK)
  {
    case LVA_DEVICE_ON:
    case LVA_DEVICE_OFF:
    case LVA_DEVICE_AUTO:
    case LVA_DEVICE_ON_AUTO:
	case LVA_DEVICE_FORCED_ON:
      break;
    default:
      return;
  }
 
  string modeDpName;
  LhcVacGetMachineModeDp(modeDpName);
  if(modeDpName == "")
  {
    return;
  }

  string	mainPart, sector1, sector2;
  bool		isBorder;
  int		devVacuum;
  LhcVacDeviceVacLocation(dpName, devVacuum, sector1, sector2, mainPart, isBorder, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacGetRequiredActPriv_VRPI: LhcVacDeviceVacLocation(" + dpName + ") failed", "");
    return;
  }
//DebugTN(dpName + ": sector1 <" + sector1 + ">, sector2 <" + sector2 + "> vacType " + devVacuum);

  int		mode;
  dyn_errClass 	err;
  if((devVacuum == LVA_VACUUM_BLUE_BEAM) || (devVacuum == LVA_VACUUM_CROSS_BEAM) || (devVacuum == LVA_VACUUM_BEAMS_COMMON))
  {	
    dpGet(modeDpName + ".Beam_B.Beam", mode);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacGetRequiredActPriv_VRPI: Read <" + modeDpName + ".Beam_B.Beam> failed: " + err, "");
      return;
    }
    if(mode == 1)  // expert privillege is required
    {
      dynAppend(glActSpecialMessages, "BLUE BEAM MODE ON");
      if(privRequired < 3)
      {
        privRequired = 3;
      }
      return;
    }
  }
  if((devVacuum == LVA_VACUUM_RED_BEAM) || (devVacuum == LVA_VACUUM_CROSS_BEAM) || (devVacuum == LVA_VACUUM_BEAMS_COMMON))
  {	
    dpGet(modeDpName + ".Beam_R.Beam", mode);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacGetRequiredActPriv_VRPI: Read <" + modeDpName + ".Beam_R.Beam> failed: " + err, "");
      return;
    }
    if(mode == 1)  // expert privillege is required
    {
      dynAppend(glActSpecialMessages, "RED BEAM MODE ON");
      if(privRequired < 3)
      {
        privRequired = 3;
      }
      return;
    }
  }
 }
// make compound confirm message for ALL std objects. S. Blanchard request 29.08.2013
void CheckCommandForManualMode_VP(string dpName, const dyn_anytype action, int &privRequired, dyn_string &exceptionInfo)
{
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
  case LVA_DEVICE_OFF:
    break;
  case LVA_DEVICE_FORCED_ON:
  case LVA_DEVICE_RESET:
    if(!VacIsDeviceUsedInProcess(dpName, "VPG_6E01", makeDynString()))
    {
      if(privRequired < 3)
      {
        privRequired = 3;
      }
    }
    return;
  default:
    return;
  }
  dynAppend(glActSpecialMessages, "ATTENTION: MANUAL MODE COMMAND ");
}

//LhcVacDeviceAction_VRPI:
/** Execute action for ion pump. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
int LhcVacDeviceAction_VRPI(string dpName, string dpType, dyn_anytype  action,
  dyn_string &exceptionInfo)
{
  unsigned newWr1, act;
  long newWr2;
  bool isWr2 = FALSE;
  bool possible;
  dyn_errClass err;
  int newWr1PS;
  mapping mConnectDpes;
  dyn_float dfReturn;
  dyn_string dsReturn;
  string sValidRange;
  long val;  
  
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
    if( dpType == "VPI_PS_CMW")
    {
      newWr1PS = VVS_PS_CMW_W_START;
    }
    else if( (dpType == "VP_STDIO") || (dpType == "VR_PI_CHAN") )
    {
       newWr1 = VRPI_W_W_ON | VRPI_W_W_MANUAL;
    }
    else
    {    
      newWr1 = VRPI_W_W_REMOTE | VRPI_W_W_ON | VRPI_W_W_MANUAL;
    }
    break;
  case LVA_DEVICE_OFF:
    if( dpType == "VPI_PS_CMW")
    {
      newWr1PS = VVS_PS_CMW_W_STOP;
    }
    else if( (dpType == "VP_STDIO") || (dpType == "VR_PI_CHAN") )
    {
       newWr1 = VRPI_W_W_OFF | VRPI_W_W_MANUAL;
    }
    else
    {    
      newWr1 = VRPI_W_W_REMOTE | VRPI_W_W_OFF | VRPI_W_W_MANUAL;
    }
    break;
  case LVA_DEVICE_AUTO:
    newWr1 = VRPI_W_W_REMOTE | VRPI_W_W_AUTO;
    break;
  case LVA_DEVICE_MANUAL:
    newWr1 = 0x2400u;
    break;
  case LVA_DEVICE_SET_BLOCKED_OFF:
  case LVA_DEVICE_CLEAR_BLOCKED_OFF:
    return ProcessBlockedOFFVRPI(dpName, dpType, act, exceptionInfo);
    break;
 case LVA_DEVICE_RESET:
    newWr1 = VRPI_W_W_REMOTE | VRPI_W_W_RESET;
    break;
  case LVA_DEVICE_FORCE:  	newWr1 = 0x800u; 
  case LVA_ACTION_VPG_SERVICE_DATE:
    SetServiceDate(dpName, exceptionInfo);
    return LVA_OK;
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["PR"] = PR_VALUE_OVERRANGE;
    mConnectDpes["RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["StartCounter"] = NO_VALUE_CHANGE;    
    mConnectDpes["AL1"] = false;
    mConnectDpes["AL2"] = false;
    mConnectDpes["AL3"] = false;
    mConnectDpes["SPEED"] = NO_VALUE_CHANGE;
    mConnectDpes["T"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["PR"] = NO_VALUE_CHANGE;
    mConnectDpes["RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["StartCounter"] = NO_VALUE_CHANGE;    
    mConnectDpes["AL1"] = false;
    mConnectDpes["AL2"] = false;
    mConnectDpes["AL3"] = false;
    mConnectDpes["SPEED"] = NO_VALUE_CHANGE;
    mConnectDpes["T"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  case LVA_DEVICE_FORCED_ON:
	newWr1 = VR_PI_CHAN_W_ON | VR_PI_CHAN_W_FORCE;
	break;
  case LVA_DEVICE_STEP:
    newWr1 = VR_PI_CHAN_W_STEP | VRPI_W_W_MANUAL;
    break;
  case LVA_DEVICE_3KV_FIXED:
    newWr1 = VR_PI_CHAN_W_FIXED_PARAM_CODE; 
    newWr2 = 1; //Parameter Value = 1 for 3kV
    isWr2 = TRUE;
    break;
  case LVA_DEVICE_5KV_FIXED:
    newWr1 = VR_PI_CHAN_W_FIXED_PARAM_CODE; 
    newWr2 = 2; //Parameter Value = 2 for 5kV
    isWr2 = TRUE;
    break;
  case LVA_DEVICE_SPD_TO_NOMINAL:
    newWr1 = VR_PI_CHAN_W_ON | 3; // Parameter Code = 3 back to nominal 
    break;
  case LVA_DEVICE_SPD_TO_ROTATION_SP:
    sValidRange = "[1, 100]";
    ChildPanelOnCentralModalReturn("vision/dialogs/NewValue.pnl", "Set New Value" , 
                                   makeDynString("$value:1", "$description:Rotation Set Point(%) " + sValidRange, 
                                                 "$isFloat:0"),
                                   dfReturn, dsReturn);
    val = (long) dsReturn[1];
    if ((val < 1) || (val > 100)) {
      fwException_raise( exceptionInfo, "ERROR", "Value out of valid range " +sValidRange, "" );
      fwExceptionHandling_display( exceptionInfo );
      return LVA_FAILURE;
    }
    newWr1 = VR_PI_CHAN_W_ON | 2; // Parameter Code = 2 speed to rotation set point 
    newWr2 = val;
    isWr2 = TRUE;
    break;      
  default:
    fwException_raise(exceptionInfo, 
                      "ERROR", "LhcVacDeviceAction_VRPI: unknown action " + act, "");
    return LVA_FAILURE;
  }

  // Check if action is possible
  LhcVacActionPossible_VP(dpName, dpType, act, possible, exceptionInfo);
  if(dynlen(exceptionInfo) != 0)
  {
    return LVA_FAILURE;
  }
  if(!possible)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: Action forbidden for  <" + dpName + ">", "");
    return LVA_FAILURE;
  }	
  // Execute
  if( dpType == "VPI_PS_CMW")
  {
    dpSetWait(dpName + ".WR1", newWr1PS);
  }
  else
  {
    dpSetWait(dpName + ".WR1", newWr1);
    if (isWr2)
    {
      dpSetWait(dpName + ".WR2", newWr2);
    }  
  }
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return LVA_FAILURE;
  }
  return LVA_OK;
}
//LhcVacDeviceAction_VPS:
/** Execute action for sublimation pump.
  It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
int LhcVacDeviceAction_VPS(string dpName, string dpType, dyn_anytype  action,
  dyn_string &exceptionInfo)
{
  mapping       valuesToWrite;
  bool		      possible;  
  unsigned      processType, act = action[1] & LVA_ACTION_MASK;
  mapping       mConnectDpes;
  
  switch(act)
  {
  case LVA_DEVICE_ON:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VRPI_W_W_ON | VRPI_W_W_MANUAL);
    break;
  case LVA_DEVICE_VPS_APPLY_SUBLIMATION:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VRPI_W_W_MANUAL | VPS_W_SUBLIM | VPS_W_APPLY );
    valuesToWrite[".SublimRate_W"] = (int)action[2];
    valuesToWrite[".SublimTime_W"] = (int)action[3];
    break;
  case LVA_DEVICE_VPS_APPLY_DEGASSING:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VRPI_W_W_MANUAL | VPS_W_DEGAS | VPS_W_APPLY);
    valuesToWrite[".DegasCurrent_W"] = (int)action[2];
    break;
  case LVA_DEVICE_VPS_SET_SUBLIMATION_MODE:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VRPI_W_W_MANUAL | VPS_W_SUBLIM );
    break;
  case LVA_DEVICE_VPS_SET_DEGASSING_MODE:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VRPI_W_W_MANUAL | VPS_W_DEGAS);
    break;
  case LVA_DEVICE_ON:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VRPI_W_W_ON | VRPI_W_W_MANUAL);
    break;
  case LVA_DEVICE_OFF:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VRPI_W_W_OFF | VRPI_W_W_MANUAL);
    break;
  case LVA_DEVICE_AUTO:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VRPI_W_W_AUTO);
    break;
  case LVA_DEVICE_RESET:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VRPI_W_W_RESET);
    break;
  case LVA_DEVICE_VPS_SWITCH_FILAMENT:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VPS_W_SWITCH_FIL);
    break;
  case LVA_DEVICE_VPS_RESET_COUNTERS:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VPS_W_RESET_COUNT);
    break;
  case LVA_DEVICE_VPS_FILAMENT1:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VPS_W_FIL1);
    break;
  case LVA_DEVICE_VPS_FILAMENT2:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VPS_W_FIL2);
    break;
  case LVA_DEVICE_MANUAL:
    valuesToWrite[".WR1"] = (unsigned)(VRPI_W_W_REMOTE | VRPI_W_W_MANUAL);
    break;   
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;        
    mConnectDpes["SublimTime"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimRate"] = NO_VALUE_CHANGE;                     
    mConnectDpes["Current"] = NO_VALUE_CHANGE;
    mConnectDpes["Filam1_StartCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["Filam1_RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["Filam2_StartCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["Filam2_RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimTime_W"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimRate_W"] = NO_VALUE_CHANGE;
    mConnectDpes["DegasCurrent_W"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;    
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;        
    mConnectDpes["SublimTime"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimRate"] = NO_VALUE_CHANGE;                     
    mConnectDpes["Current"] = NO_VALUE_CHANGE;
    mConnectDpes["Filam1_StartCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["Filam1_RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["Filam2_StartCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["Filam2_RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimTime_W"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimRate_W"] = NO_VALUE_CHANGE;
    mConnectDpes["DegasCurrent_W"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;    
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VPS: unknown action " + act, "");
    return LVA_FAILURE;
  }

  // Check if action is possible
  LhcVacActionPossible_VP(dpName, dpType, act, possible, exceptionInfo);
  if(dynlen(exceptionInfo) != 0)
  {
    return LVA_FAILURE;
  }
  if(!possible)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: Action forbidden for  <" + dpName + ">", "");
    return LVA_FAILURE;
  }	
  // Execute
  return LhcVacSetActionDpeValues(dpName, valuesToWrite, exceptionInfo);

}
//LhcVacDeviceAction_VPS:
/** Execute action for sublimation process.
  It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
int LhcVacDeviceAction_VPS_Process(string dpName, string dpType, dyn_anytype  action,
  dyn_string &exceptionInfo)
{
  mapping valuesToWrite;
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_VPS_PROCESS_APPLY_SET:
    valuesToWrite[".WR1"] = (unsigned)0x2C00;  // REMOTE + APPLY_SETTINGS + MANUAL
    valuesToWrite[".SublimTime_W"] = (unsigned)action[2];
    valuesToWrite[".SublimRate_W"] = (unsigned)action[3];
    {
      time cycleStartTime = (time)action[4];
      float fromStartOfEpoch = cycleStartTime - makeTime(1990, 01, 01);  // PLC start of epoch
      valuesToWrite[".CycleStartDate_W"] = (int)(fromStartOfEpoch / 86400);  // number of days since 01.01.1990
      float fromStartOfDay = cycleStartTime - makeTime(year(cycleStartTime), month(cycleStartTime), day(cycleStartTime));
      valuesToWrite[".CycleStartTime_W"] = (int)(fromStartOfDay / 60);  // number of minutes since midnight
    }      
    valuesToWrite[".SublimAmount_W"] = (unsigned)action[5];
    valuesToWrite[".TimeBetweenCycles_W"] = (unsigned)action[6];
    break;
  case LVA_DEVICE_VPS_PROCESS_APPLY_START:
    valuesToWrite[".WR1"] = (unsigned)0x3C00;  // REMOTE + START_NOW + APPLY_SETTINGS + MANUAL
    valuesToWrite[".SublimTime_W"] = (unsigned)action[2];
    valuesToWrite[".SublimRate_W"] = (unsigned)action[3];
    {
      time cycleStartTime = (time)action[4];
      float fromStartOfEpoch = cycleStartTime - makeTime(1990, 01, 01);  // PLC start of epoch
      valuesToWrite[".CycleStartDate_W"] = (int)(fromStartOfEpoch / 86400);  // number of days since 01.01.1990
      float fromStartOfDay = cycleStartTime - makeTime(year(cycleStartTime), month(cycleStartTime), day(cycleStartTime));
      valuesToWrite[".CycleStartTime_W"] = (int)(fromStartOfDay / 60);  // number of minutes since midnight
    }      
    valuesToWrite[".SublimAmount_W"] = (unsigned)action[5];
    valuesToWrite[".TimeBetweenCycles_W"] = (unsigned)action[6];
    break;
  case LVA_DEVICE_ON:
    valuesToWrite[".WR1"] = (unsigned)0x2600;  // REMOTE + MANUAL + ON
    break;
  case LVA_DEVICE_OFF:
    valuesToWrite[".WR1"] = (unsigned)0x2500;  // REMOTE + MANUAL + OFF
    break;
  case LVA_DEVICE_RESET:
    valuesToWrite[".WR1"] = (unsigned)0xA000;  // RESET + REMOTE
    break;
  case LVA_DEVICE_VPS_PROCESS_START_NOW:
    valuesToWrite[".WR1"] = (unsigned)0x3400;  // REMOTE + START_NOW + MANUAL
    break;
  case LVA_DEVICE_VPS_PROCESS_START_ONCE:
    valuesToWrite[".WR1"] = (unsigned)0x7400;  // FORCE + REMOTE + START_NOW + MANUAL
    valuesToWrite[".SublimTime_W"] = (unsigned)action[2];
    valuesToWrite[".SublimRate_W"] = (unsigned)action[3];
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["SublimTime"] = NO_VALUE_CHANGE;        
    mConnectDpes["SublimRate"] = NO_VALUE_CHANGE;                     
    mConnectDpes["Current"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimAmount"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["TimeBetweenCycles"] = NO_VALUE_CHANGE;
    mConnectDpes["TimeCountDown"] = NO_VALUE_CHANGE;
    
    mConnectDpes["TimeYMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TimeMSM"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimTime_W"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimRate_W"] = NO_VALUE_CHANGE;
    mConnectDpes["CycleStartDate_W"] = NO_VALUE_CHANGE;
    mConnectDpes["CycleStartTime_W"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimAmount_W"] = NO_VALUE_CHANGE;
    mConnectDpes["TimeBetweenCycles_W"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;    
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimTime"] = NO_VALUE_CHANGE;        
    mConnectDpes["SublimRate"] = NO_VALUE_CHANGE;                     
    mConnectDpes["Current"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimAmount"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["TimeBetweenCycles"] = NO_VALUE_CHANGE;
    mConnectDpes["TimeCountDown"] = NO_VALUE_CHANGE;
    
    mConnectDpes["TimeYMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TimeMSM"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimTime_W"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimRate_W"] = NO_VALUE_CHANGE;
    mConnectDpes["CycleStartDate_W"] = NO_VALUE_CHANGE;
    mConnectDpes["CycleStartTime_W"] = NO_VALUE_CHANGE;
    mConnectDpes["SublimAmount_W"] = NO_VALUE_CHANGE;
    mConnectDpes["TimeBetweenCycles_W"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;    
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VPS_Process: unknown action " + act, "");
    return LVA_FAILURE;
  }

  // Check if action is possible
  bool		      possible;
  LhcVacActionPossible_VP(dpName, dpType, act, possible, exceptionInfo);
  if(dynlen(exceptionInfo) != 0)
  {
    return LVA_FAILURE;
  }
  if(!possible)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: Action forbidden for  <" + dpName + ">", "");
    return LVA_FAILURE;
  }	
  // Execute
  return LhcVacSetActionDpeValues(dpName, valuesToWrite, exceptionInfo);
}
int ProcessBlockedOFFVRPI(string dpName, string dpType, unsigned act,
  dyn_string &exceptionInfo)
{
  bool	        newValue;

  switch(act)
  {
  case LVA_DEVICE_SET_BLOCKED_OFF:
    newValue = true;
    break;
  case LVA_DEVICE_CLEAR_BLOCKED_OFF:
    newValue = false;
    break;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "ProcessBlockedOFFVRPI: unknown action " + act, "");
    return LVA_FAILURE;
  }

  // VRPI can be 'virtual' VRPI created to support different VPI types connected to single VRPI.
  // In such case action shall be executed for all virtual VRPIs which are part of one real VRPI
  dyn_string vrpiList;
  LhcVacAllEqpWithSameVisName(dpName, vrpiList);
  for(int n = dynlen(vrpiList) ; n > 0 ; n--)
  {
    dpSetWait(vrpiList[n] + ".BlockedOFF", newValue);
    dyn_errClass  err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, "ERROR", "Action Execution: execution failed:" + err , "");
      return LVA_FAILURE;
    }
  }
  return LVA_OK;
}
//LhcVacCalcCurrentFromPressure:
/*
  Performe reverce ion pump current calculation from pressure reported from PLC
Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name of VRPI
	@param pressure: in, pressure

	@return: calculated current
  
 */ 
float  LhcVacCalcCurrentFromPressure(string dpName, float pressure)
{
  dyn_string 	slaveDpNames;
  string	master;
  int		channel;
  float         a;
  LhcVacEqpMaster(dpName, master, channel);
  LhcVacEqpSlaves(master, slaveDpNames);
  int nSlaves = dynlen( slaveDpNames );
  int family, type, subtype;
  LhcVacGetCtlAttributes(dpName, family, type, subtype);
//DebugTN("DP " + dpName + ", type " + type);
  a = WimKoCoeficients(type);
//DebugN("CurConv:", type, nSlaves, a, nSlaves/a);
  return pressure*nSlaves/a;
}

// H.Pereira 20.06.2014 - new K coefficients calculated by Agnieszka CHMIELINSKA
float WimKoCoeficients(int type)
{
  float  co;
  switch(type)
  {
  case 1:
    co = 3.170000e-003; //["VPIA_SPS" - VPI_SPS_30] 30l/s VPIA SPS
    break;
  case 2:
    co = 1.000000e-004; //["VPIB_SPS" - VPI_SPS_400] 400l/s VPIB SPS
    break;
  case 3:
    co = 1.945000e-003; //["VPIA_LHC" - VPI_LHC_30] starcell 30 l/s
    break;
  case 4:
    co = 9.882000e-004; //["VPIB_LHC" - VPI_LHC_60] starcell 60 l/s
    break;
  case 5:
    co = 2.000000e-004; //[VPIC_LHC-VPIZ_LHC - VPI_LHC_400] 400l/s     
    break;
  case 6:
    co = 1.520000e-003; //VPX Flako 20 l/s
    break;
  case 7:
    co = 9.882000e-004; //["VPIA_CPS" - VPI_CPS_75] starcell 60 l/s
    break;
  case 8:
    co = 2.000000e-004; //["VPIB_CPS" - VPI_CPS_500] starcell 400 l/s
    break;
  case 9:
    co = 7.501000e-005; //["VPIC_CPS" - VPI_CPS_400] demi 1000 l/s tank Linac2
    break;
  case 10:
    co = 4.000000e-004; //["VPID_CPS" - VPI_CPS_300] 230 l/s
    break;
  case 11:
    co = 3.302000e-004; //["VPIE_CPS" - VPI_CPS_150] 120 l/s
    break;
  case 12:
    co = 2.600000e-004; //["VPIF_CPS" - VPI_CPS_270] 200 l/s
    break;
  case 13:
    co = 2.500000e-003; //["VPID_LHC" - VPI_LHC_20] 20 l/s
    break;
  case 14:
    co = 6.433000e-003; //["VPIX_LHC" - VPI_LHC_8] 8 l/s
    break;
  case 15:
    co = 1.000000e-003; //["VPIA_CTF" - VPI_CTF_55] 55 l/s
    break;
  case 16:
    co = 5.000000e-004; //["VPIB_CTF" - VPI_CTF_75] 75 l/s
    break;
  case 17:
    co = 2.600000e-004; //["VPIC_CTF" - VPI_CTF_270] 270 l/s
    break;
  case 18:
    co = 6.018000e-004; //["VPIG_CPS" - VPI_LEIR_60] 60 l/s Triode VacIon LEIR
    break;
  case 19:
    co = 1.500000e-004; //["VPIH_CPS" - VPI_CPS_220] 220 l/s Triode VacIon PS
    break;
  case 20:
    co = 3.340000e-003; //["VPIE_SPS" - VPI_SPS_20] 27 l/s Diode VacIon SPS  v5.2
    break;
  case 21:
    co = 1.400000e-002; //Ion Pump type NEXTorr 5 l/s
    break;
  default:
    co = 1.000000e-003;
    break;
  }
  return co;
}
