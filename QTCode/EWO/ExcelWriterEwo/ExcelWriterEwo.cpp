//	Implementation of ExcelWriterEwo class
/////////////////////////////////////////////////////////////////////////////////

#include "ExcelWriterEwo.h"

#include "DpConnection.h"
#include "DpeHistoryQuery.h"

EWO_PLUGIN(ExcelWriterEwo)

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

ExcelWriterEwo::ExcelWriterEwo(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new ExcelWriterWidget(parent);

	// Connect to signals of base widget
	connect(baseWidget, SIGNAL(contentRequested(void)),
		this, SLOT(contentRequest(void)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList ExcelWriterEwo::signalList(void) const
{
	QStringList list;
	list.append("contentRequest()");
	return list;
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList ExcelWriterEwo::methodList(void) const
{
	QStringList list;
	list.append("void clear()");
	list.append("int addRow()");
	list.append("void setCellFormat(int row, int col, int format)");
	list.append("void setCellColor(int row, int col, string color)");
	list.append("void setCellText(int row, int col, string text)");
	list.append("void setCellDateTime(int row, int col, time dateTime)");
	list.append("void setCellDateTimeWithMsec(int row, int col, time dateTime)");
	list.append("void setCellValue(int row, int col, float value)");
	list.append("void setCellComment(int row, int col, string comment)");
	list.append("void setColumnTitle(int col, string title)");
	list.append("void setColumnWidth(int col, int width)");
	list.append("string write()");
	list.append("void addHistoryValue(string dp, time ts, float value)");
	list.append("string writeHistory()");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "clear")
	{
		return invokeClear(name, values, error);
	}
	else if(name == "addRow")
	{
		return invokeAddRow(name, values, error);
	}
	else if(name == "setCellFormat")
	{
		return invokeSetCellFormat(name, values, error);
	}
	else if(name == "setCellColor")
	{
		return invokeSetCellColor(name, values, error);
	}
	else if(name == "setCellText")
	{
		return invokeSetCellText(name, values, error);
	}
	else if(name == "setCellDateTime")
	{
		return invokeSetCellDateTime(name, values, error);
	}
	else if(name == "setCellDateTimeWithMsec")
	{
		return invokeSetCellDateTimeWithMsec(name, values, error);
	}
	else if(name == "setCellValue")
	{
		return invokeSetCellValue(name, values, error);
	}
	else if(name == "setCellComment")
	{
		return invokeSetCellComment(name, values, error);
	}
	else if(name == "setColumnTitle")
	{
		return invokeSetColumnTitle(name, values, error);
	}
	else if(name == "setColumnWidth")
	{
		return invokeSetColumnWidth(name, values, error);
	}
	else if(name == "write")
	{
		return invokeWrite(name, values, error);
	}
	else if(name == "addHistoryValue")
	{
		return invokeAddHistoryValue(name, values, error);
	}
	else if(name == "writeHistory")
	{
		return invokeWriteHistory(name, values, error);
	}
	else
	{
		error = "Uknown method ExcelWriterEwo.";
		error += name;
	}
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			void clear()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeClear(const QString & /* name */,
	QList<QVariant> & /* values */, QString & /* error */)
{
	baseWidget->clear();
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			int addRow()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeAddRow(const QString & /* name */,
	QList<QVariant> & /* values */, QString & /* error */)
{
	int coco = baseWidget->addRow();
	return QVariant(coco);
}

/*
**	FUNCTION
**		Execute method
**			void setCellFormat(int row, int col, int format)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeSetCellFormat(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not integer";
		return QVariant(-1);
	}
	int row = values[0].toInt();

	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (col) is not integer";
		return QVariant(-1);
	}
	int col = values[1].toInt();

	if(!values[2].canConvert(QVariant::Int))
	{
		error = "argument 3 (format) is not integer";
		return QVariant(-1);
	}
	int format = values[2].toInt();
	baseWidget->setCellFormat(row, col, format);
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			void setCellColor(int row, int col, string color)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeSetCellColor(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not integer";
		return QVariant(-1);
	}
	int row = values[0].toInt();

	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (col) is not integer";
		return QVariant(-1);
	}
	int col = values[1].toInt();

	if(!values[2].canConvert(QVariant::String))
	{
		error = "argument 3 (color) is not string";
		return QVariant(-1);
	}
	const QString colorString = values[2].toString();
	int red, green, blue;
	if(sscanf(colorString.toLatin1().constData(), "{%d,%d,%d}", &red, &green, &blue) != 3)
	{
		if(sscanf(colorString.toLatin1().constData(), "[%d,%d,%d]", &red, &green, &blue) != 3)
		{
			printf("ExcelWriterEwo::invokeSetCellColor(): row %d col %d: failed to extract RGB from <%s>\n",
				row, col, colorString.toLatin1().constData());
			return QVariant();
		}
		red = (int)(red * 2.55);
		if(red > 255)
		{
			red = 255;
		}
		green = (int)(green * 2.55);
		if(green > 255)
		{
			green = 255;
		}
		blue = (int)(blue * 2.55);
		if(blue > 255)
		{
			blue = 255;
		}
	}
	QColor color(red, green, blue);
	baseWidget->setCellColor(row, col, color);
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			void setCellText(int row, int col, string text)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeSetCellText(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not integer";
		return QVariant(-1);
	}
	int row = values[0].toInt();

	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (col) is not integer";
		return QVariant(-1);
	}
	int col = values[1].toInt();

	if(!values[2].canConvert(QVariant::String))
	{
		error = "argument 3 (text) is not string";
		return QVariant(-1);
	}
	const QString text = values[2].toString();
	baseWidget->setCellValue(row, col, text);
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			void setCellDateTime(int row, int col, time dateTime)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeSetCellDateTime(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not integer";
		return QVariant(-1);
	}
	int row = values[0].toInt();

	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (col) is not integer";
		return QVariant(-1);
	}
	int col = values[1].toInt();

	if(!values[2].canConvert(QVariant::DateTime))
	{
		error = "argument 3 (dateTime) is not time";
		return QVariant(-1);
	}
	const QDateTime dateTime = values[2].toDateTime();
	baseWidget->setCellValue(row, col, dateTime, false);
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			void setCellDateTimeWithMsec(int row, int col, time dateTime)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeSetCellDateTimeWithMsec(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not integer";
		return QVariant(-1);
	}
	int row = values[0].toInt();

	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (col) is not integer";
		return QVariant(-1);
	}
	int col = values[1].toInt();

	if(!values[2].canConvert(QVariant::DateTime))
	{
		error = "argument 3 (dateTime) is not time";
		return QVariant(-1);
	}
	const QDateTime dateTime = values[2].toDateTime();
	baseWidget->setCellValue(row, col, dateTime, true);
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			void setCellValue(int row, int col, float value)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeSetCellValue(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not integer";
		return QVariant(-1);
	}
	int row = values[0].toInt();

	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (col) is not integer";
		return QVariant(-1);
	}
	int col = values[1].toInt();

	if(!values[2].canConvert(QVariant::Double))
	{
		error = "argument 3 (value) is not float";
		return QVariant(-1);
	}
	float value = (float)values[2].toDouble();
	baseWidget->setCellValue(row, col, value);
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			void setCellText(int row, int col, string text)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeSetCellComment(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not integer";
		return QVariant(-1);
	}
	int row = values[0].toInt();

	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (col) is not integer";
		return QVariant(-1);
	}
	int col = values[1].toInt();

	if(!values[2].canConvert(QVariant::String))
	{
		error = "argument 3 (comment) is not string";
		return QVariant(-1);
	}
	const QString comment = values[2].toString();
	baseWidget->setCellComment(row, col, comment);
	return QVariant();
}


/*
**	FUNCTION
**		Execute method
**			void setColumnTitle(int col, string title)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeSetColumnTitle(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (col) is not integer";
		return QVariant(-1);
	}
	int col = values[0].toInt();

	if(!values[1].canConvert(QVariant::String))
	{
		error = "argument 2 (title) is not string";
		return QVariant(-1);
	}
	const QString title = values[1].toString();

	baseWidget->setColumnTitle(col, title);
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			void setColumnWidth(int col, int width)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeSetColumnWidth(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (col) is not integer";
		return QVariant(-1);
	}
	int col = values[0].toInt();

	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (width) is not integer";
		return QVariant(-1);
	}
	int width = values[1].toInt();

	baseWidget->setColumnWidth(col, width);
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			string write()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeWrite(const QString & /* name */,
	QList<QVariant> & /* values */, QString & /* error */)
{
	return QVariant(baseWidget->write());
}

/*
**	FUNCTION
**		Execute method
**			void addHistoryValue(string dp, time ts, float value)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeAddHistoryValue(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dp) is not string";
		return QVariant(-1);
	}
	QString dp = values[0].toString();

	if(!values[1].canConvert(QVariant::DateTime))
	{
		error = "argument 2 (ts) is not time";
		return QVariant(-1);
	}
	QDateTime ts = values[1].toDateTime();

	if(!values[2].canConvert(QVariant::Double))
	{
		error = "argument 3 (value) is not float";
		return QVariant(-1);
	}
	float value = (float)values[2].toDouble();
	baseWidget->addHistoryValue(dp.toLatin1(), ts, value);
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			string writeHistory()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant ExcelWriterEwo::invokeWriteHistory(const QString & /* name */,
	QList<QVariant> & /* values */, QString & /* error */)
{
	return QVariant(baseWidget->writeHistory());
}


/*
**	FUNCTION
**		Slot receiving contentRequested signal of ExcelWriterEwo, emit
**		signal 'contentRequest' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterEwo::contentRequest(void)
{
printf("ExcelWriterEwo::contentRequest(): start\n");
	emit signal("contentRequest");
}
