#ifndef	IMAGEITEM_H
#define	IMAGEITEM_H

#include "VacCtlEwoUtilExport.h"

#include "Eqp.h"

// Supreclass for all 'passive' items of synoptic

#include <QXmlStreamReader>

#include <QPainter>

class VacIcon;

class VACCTLEWOUTIL_EXPORT ImageItem
{
public:
	ImageItem();
	virtual ~ImageItem();

	virtual void parse(QXmlStreamReader &reader);

	// To be implemented by subclasses
	virtual void draw(QPainter & /* painter */) {}
	virtual bool getToolTip(const QPoint & /* point */, QString & /* text */, QRect & /* rect */) { return false; }
	virtual Eqp *getEqp(void) { return NULL; }
	virtual bool checkForInterlockSource(void) { return false; }
	virtual bool addAlarmSources(QList<ImageItem *> & /* allItems */, QList<VacIcon *> & /* allIcons */) { return false; }

	// Access
	inline const QColor &getBackColor(void) const { return backColor; }
	inline void setBackColor(const QColor &color) { backColor = color; }
	inline const QColor &getForeColor(void) const { return foreColor; }
	inline void setForeColor(const QColor &color) { foreColor = color; }
	inline const QTransform &getTransform(void) const { return transform; }
	inline void setTransform(const QTransform &newTransform) { transform = newTransform; }
	inline int getLineWidth(void) const { return lineWidth; }
	inline void setLineWidth(int value) { lineWidth = value; }
	inline Qt::PenStyle getPenStyle(void) const { return penStyle; }
	inline void setPenStyle(Qt::PenStyle style) { penStyle = style; }
	inline Qt::PenCapStyle getCapStyle(void) const { return capStyle; }
	inline void setCapStyle(Qt::PenCapStyle style) { capStyle = style; }
	inline Qt::PenJoinStyle getJoinStyle(void) const { return joinStyle; }
	inline void setJoinStyle(Qt::PenJoinStyle style) { joinStyle = style; }
	inline bool isVisible(void) const { return visible; }
	inline void setVisible(bool flag) { visible = flag; }
	inline const QString &getToolTipText(void) const { return toolTipText; }
	inline void setToolTipText(const QString &text) { toolTipText = text; }

protected:
	// Background color
	QColor				backColor;

	// Foreground color
	QColor				foreColor;

	// Transfromation
	QTransform			transform;

	// Tooltip text
	QString				toolTipText;

	// Line width
	int					lineWidth;

	// Pen style
	Qt::PenStyle		penStyle;

	// Cap stype
	Qt::PenCapStyle		capStyle;

	// Join style
	Qt::PenJoinStyle	joinStyle;

	// Visibility of this item
	bool				visible;

	virtual void setForePen(QPainter &painter);

	virtual void parseProp(QXmlStreamReader &reader);
	virtual void parseProperties(QXmlStreamReader &reader);
	virtual void parseToolTipText(QXmlStreamReader &reader);
	virtual void parseUnknown(QXmlStreamReader &reader);
	virtual QColor parseColor(const QString &string, const QColor &defaultColor);

};

#endif	// IMAGEITEM_H
