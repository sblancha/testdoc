#ifndef RACKICONVRVCX_H
#define RACKICONVRVCX_H

#include "RackIcon.h"

class Eqp;

class VACCTLEWOUTIL_EXPORT RackIconVRVCX : public RackIcon
{
	Q_OBJECT
public:
	RackIconVRVCX(QWidget * parent, Qt::WindowFlags f = 0);
	virtual ~RackIconVRVCX();

	virtual void setEqp(Eqp * pEqp);
	virtual bool connect(void);
	virtual bool disconnect(void);
protected:
	Eqp * pEqp; //pointer to equipment, if it exists
	virtual void paintEvent(QPaintEvent *pEvent);

};

#endif
