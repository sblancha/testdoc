#ifndef SECTORDRAWPART_H
#define SECTORDRAWPART_H

#include "VacCtlEqpDataExport.h"

// Class holding information for one drawing part of sector


#include "BeamLine.h"

class BeamLinePart;
#include <QColor>

class VACCTLEQPDATA_EXPORT SectorDrawPart
{
public:
	SectorDrawPart();
	virtual ~SectorDrawPart();

	inline int getStartEqpIdx(void) { return startEqpIdx; }
	inline void setStartEqpIdx(int startEqpIdx) { this->startEqpIdx = startEqpIdx; }
	inline int getEndEqpIdx(void) { return endEqpIdx; }
	inline void setEndEqpIdx(int endEqpIdx) { this->endEqpIdx = endEqpIdx; }
	inline BeamLine *getLine(void) { return pLine; }
	inline void setLine(BeamLine *pLine) { this->pLine = pLine; }
	inline BeamLinePart *getStartPart(void) { return pStartPart; }
	inline void setStartPart(BeamLinePart *pStartPart) { this->pStartPart = pStartPart; }
	inline BeamLinePart *getEndPart(void) { return pEndPart; }
	inline void setEndPart(BeamLinePart *pEndPart) { this->pEndPart = pEndPart; }
	inline short getDrawOrder(void) { return drawOrder; }
	inline QColor *getColor(void) { return pColor; }
	inline bool isCircleClose(void) { return circleClose; }
	inline void setCircleClose(bool circleClose) { this->circleClose = circleClose; }
	inline bool isPassed(void) { return passed; }

protected:
	// Index of device at start of draw part
	int				startEqpIdx;

	// Index of device at end of draw part
	int				endEqpIdx;

	// Pointer to beam line for draw part
	BeamLine		*pLine;

	// Pointer to beam line part at start of draw part
	BeamLinePart	*pStartPart;

	// Pointer to beam line part at end of draw part
	BeamLinePart	*pEndPart;

	// Order of part drawing - to make sure parts are drawn in requested order
	short			drawOrder;

	// Pointer to color used for drawing this part of sector
	QColor			*pColor;

	// Flag indicating if this part closes the beam circle line
	bool			circleClose;

	// Flag indicating if this draw part has been passed by algorithm
	bool			passed;		
};

#endif	// SECTORDRAWPART_H
