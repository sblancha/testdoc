#ifndef DEVLISTCRITERIAEDITORWIDGET_H
#define DEVLISTCRITERIAEDITORWIDGET_H

// Widget for editing device list criteria

#include "DevListCriteria.h"

#include <QWidget>
#include <QBitArray>

#include <QCheckBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QDateTimeEdit>

#include <QGroupBox>
#include <QBoxLayout>

class DevListCriteriaEditorWidget : public QWidget
{
	Q_OBJECT

public:
	DevListCriteriaEditorWidget(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~DevListCriteriaEditorWidget() {}

	bool isCriteriaEmpty(void) const { return DevListCriteria::getInstance().isEmpty(); }
	bool isCriteriaValid(void) const { return valid; }

	QList<QVariant> getCriteriaFlags(void) const;
	void setCriteriaFlags(const QList<QVariant> &flags);

	QList<QVariant> getCriteriaValues(void) const;
	void setCriteriaValues(const QList<QVariant> &values);

	QList<QVariant> getCriteriaTimes(void) const;
	void setCriteriaTimes(const QList<QVariant> &times);

signals:
	void changed(void);

protected:

	// Checkbox for 'normal' flag
	QCheckBox	*pNormalCb;

	// Checkbox for 'not normal' flag
	QCheckBox	*pNotNormalCb;

	// Checkbox for 'abnormal' flag
	QCheckBox	*pAbnormalCb;

	// Checkbox for 'not abnormal' flag
	QCheckBox	*pNotAbnormalCb;

	// Checkbox for 'errors' flag
	QCheckBox	*pErrorsCb;

	// Checkbox for 'warnings' flag
	QCheckBox	*pWarningsCb;

	// Checkbox for 'alerts' flag
	QCheckBox	*pAlertsCb;

	// Checkbox for 'access errors' flag
	QCheckBox	*pAccessCb;

	// Checkbox for 'not connected' flag
	QCheckBox	*pNotConnectedCb;

	// Checkbox for 'not active' flag
	QCheckBox	*pNotActiveCb;

	// Checkbox for 'use pressure limit' flag
	QCheckBox	*pUsePressureLimitCb;

	// Editor for pressure limit value
	QLineEdit	*pPressureLimitText;

	// Checkbox for 'use pressure grow limit' flag
	QCheckBox	*pUsePressureGrowLimitCb;

	// Editor for pressure grow limit value
	QLineEdit	*pPressureGrowLimitText;

	// Editor for start of pressure grow interval
	QDateTimeEdit	*pPressureStartTimeEdit;

	// Editor for end of pressure grow interval
	QDateTimeEdit	*pPressureEndTimeEdit;

	// Check box for end of pressure grow interval = NOW
	QCheckBox	*pPressureEndTimeNowCb;

	// Spin box for pressure spike filter interval - sec
	QSpinBox	*pPressureSpikeIntervalSb;


	// Checkbox for 'use temperature limit' flag
	QCheckBox	*pUseTemperatureLimitCb;

	// Editor for temperature limit value
	QLineEdit	*pTemperatureLimitText;

	// Checkbox for 'use temperature grow limit' flag
	QCheckBox	*pUseTemperatureGrowLimitCb;

	// Editor for temperature grow limit value
	QLineEdit	*pTemperatureGrowLimitText;

	// Editor for start of temperature grow interval
	QDateTimeEdit	*pTemperatureStartTimeEdit;

	// Editor for end of temperature grow interval
	QDateTimeEdit	*pTemperatureEndTimeEdit;

	// Check box for end of temperature grow interval = NOW
	QCheckBox	*pTemperatureEndTimeNowCb;

	// Spin box for temperature spike filter interval - sec
	QSpinBox	*pTemperatureSpikeIntervalSb;

	// Flag indicating if value for control widget is set by code
	bool	setting;

	// Flag indicating if current criteria settings are valid
	bool	valid;

	QWidget *buildPressureLimitsGroup(QWidget *pParent, DevListCriteria &criteria);
	QWidget *buildTemperatureLimitsGroup(QWidget *pParent, DevListCriteria &criteria);

	void showPrLimit(void);
	void adjustPressureTimes(void);
	void showPrGrowLimit(void);
	void adjustTemperatureTimes(void);
	void showTempLimit(void);
	void showTempGrowLimit(void);
	void setWidgetBackground(QWidget *pWidget, const QColor &color);

private slots:
	void normalChanged(bool use);
	void notNormalChanged(bool use);
	void abnormalChanged(bool use);
	void notAbnormalChanged(bool use);
	void errorsChanged(bool use);
	void warningsChanged(bool use);
	void alertsChanged(bool use);
	void accessChanged(bool use);
	void notConnectedChanged(bool use);
	void notActiveChanged(bool use);
	void usePressureLimitChanged(bool use);
	void usePressureGrowLimitChanged(bool use);
	void useTemperatureLimitChanged(bool use);
	void useTemperatureGrowLimitChanged(bool use);

	void pressureLimitChanged(const QString &text);
	void pressureGrowLimitChanged(const QString &text);
	void temperatureLimitChanged(const QString &text);
	void temperatureGrowLimitChanged(const QString &text);

	void pressureStartTimeChanged(const QDateTime &value);
	void pressureEndTimeChanged(const QDateTime &value);
	void pressureEndTimeNowChanged(bool value);
	void pressureSpikeIntervalChanged(int value);

	void temperatureStartTimeChanged(const QDateTime &value);
	void temperatureEndTimeChanged(const QDateTime &value);
	void temperatureEndTimeNowChanged(bool value);
	void temperatureSpikeIntervalChanged(int value);
};

#endif	// DEVLISTCRITERIAEDITORWIDGET_H
