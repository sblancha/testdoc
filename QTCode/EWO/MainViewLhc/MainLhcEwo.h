#ifndef MAINLHCEWO_H
#define	MAINLHCEWO_H

// EWO interface for MainLHC widget

#include "VacMainViewEwo.h"

#include "MainLHC.h"

class EWO_EXPORT MainLhcEwo : public VacMainViewEwo
{
	Q_OBJECT

public:
	MainLhcEwo(QWidget *parent);
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private:

	QVariant invokeDataReady(const QString &name, QList<QVariant> &values,
		QString &error);
	QVariant invokeIsSelected(const QString &name, QList<QVariant> &values,
		QString &error);
	QVariant invokeIsContSelection(const QString &name, QList<QVariant> &values,
		QString &error);
	QVariant invokeGetSelectedSectors(const QString &name, QList<QVariant> &values,
		QString &error);
	QVariant invokeGetVacTypeMask(const QString &name, QList<QVariant> &values,
		QString &error);
	QVariant invokeAddLegendLabel(const QString &name, QList<QVariant> &values,
		QString &error);
};


#endif	// MAINLHCEWO_H
