//	Implementation  of SmsConfig class
/////////////////////////////////////////////////////////////////////////////////

#include "SmsConfig.h"

#include "NotificationConfig.h"
#include "ComplexCriteria.h"
#include "MessageFilteringRule.h"

#include "FunctionalType.h"
#include "DataPool.h"
#include "Eqp.h"
#include "DebugCtl.h"

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

int SmsConfig::lastId = 0;

SmsConfig::SmsConfig(void)
{
	init();
}

SmsConfig::SmsConfig(const char *externalId) : extId(externalId)
{
	init();
}

SmsConfig::~SmsConfig()
{
	while(!critList.isEmpty())
	{
		delete critList.takeFirst();
	}
	while(!usageFilterList.isEmpty())
	{
		delete usageFilterList.takeFirst();
	}
}

void SmsConfig::addScopeItem(const QString &name)
{
	scopeItems.append(name);
}

void SmsConfig::addUsageFilter(int funcType, const QByteArray &attrName, int reverse)
{
	EqpUsageFilterItem *pItem = new EqpUsageFilterItem(funcType, attrName, reverse);
	usageFilterList.append(pItem);
}

void SmsConfig::addCriteria(int funcType, int type, int subType, bool reverse, float lowerLimit, float upperLimit, int minDuration)
{
	EqpMsgCriteria *pCriteria = new EqpMsgCriteria(funcType, type, subType, "any name");
	critList.append(pCriteria);
	pCriteria->setReverse(reverse);
	pCriteria->setLowerLimit(lowerLimit);
	pCriteria->setUpperLimit(upperLimit);
	pCriteria->setMinDuration(minDuration);
}

void SmsConfig::addRecipient(const char *pName)
{
	recipients.append(pName);
}


void SmsConfig::init(void)
{
	id = ++lastId;
	mode = 0;
	funcType = 0;
	scopeType = 0;
	critJoinType = 0;
	filterType = 0;
	filterTime = 0;
	groupType = 0;
	groupTime = 0;
	indivLimit = 0;
}


void SmsConfig::writeToFile(FILE *pFile, QStringList &machineModes, unsigned allModesMask)
{
	if(!pFile)
	{
		return;
	}

	// Visible name
	fprintf(pFile, "%s;", visibleName.toLatin1().constData());

	// Machine mode
	writeMachineMode(pFile, machineModes, allModesMask);

	// Main equipment type
	FunctionalType *pFuncType = FunctionalType::findTypeData(funcType);
	fprintf(pFile, "%s;", pFuncType ? pFuncType->getName() : "NOTYPE");

	// Usage filter
	writeUsageFilterFuncTypes(pFile);
	writeUsageFilterAttrNames(pFile);
	writeUsageFilterReverse(pFile);

	// Scope type
	fprintf(pFile, "%s;", NotificationConfig::scopeTypeToString(scopeType));
	writeScopeItems(pFile);

	// Criteria
	fprintf(pFile, "%s;", ComplexCriteria::joinTypeToString(critJoinType));
	writeCriteriaFuncTypes(pFile);
	writeCriteriaTypes(pFile);
	writeCriteriaSubTypes(pFile);
	writeCriteriaReverse(pFile);
	writeCriteriaUpperLimits(pFile);
	writeCriteriaLowerLimits(pFile);
	writeCriteriaMinDurations(pFile);

	// Filter
	fprintf(pFile, "%s;", MessageFilteringRule::filterTypeToString(filterType));
	fprintf(pFile, "%d;", filterTime);
	fprintf(pFile, "%s;", MessageFilteringRule::groupTypeToString(groupType));
	fprintf(pFile, "%d;", groupTime);
	fprintf(pFile, "%d;", indivLimit);

	// Message(s)
	fprintf(pFile, "%s;", message.isEmpty() ? "" : message.toLatin1().constData());
	fprintf(pFile, "%s;", groupMessage.isEmpty() ? "" : groupMessage.toLatin1().constData());

	// Recipients
	bool firstItem = true;
	foreach(QString recipient, recipients)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		fprintf(pFile, recipient.toLatin1().constData());
	}
}

void SmsConfig::writeMachineMode(FILE *pFile, QStringList &machineModes, unsigned allModesMask)
{
	if(!pFile)
	{
		return;
	}
	if((mode & allModesMask) == allModesMask)
	{
		fprintf(pFile, "ALL;");
		return;
	}
	if(!mode)
	{
		fprintf(pFile, "NONE;");
		return;
	}
	bool firstMode = true;
	int bitNbr = 0;
	foreach(QString name,  machineModes)
	{
		if(!name.isEmpty())
		{
			if(mode & (1<<bitNbr))
			{
				if(!firstMode)
				{
					fprintf(pFile, "|");
				}
				else
				{
					firstMode = false;
				}
				fprintf(pFile, name.toLatin1().constData());
			}
		}
		bitNbr++;
	}
	fprintf(pFile, ";");
}


void SmsConfig::writeScopeItems(FILE *pFile)
{
	if(!pFile)
	{
		return;
	}

	bool firstItem = true;
	foreach(QString name, scopeItems)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		fprintf(pFile, getScopeItemName(name.toLatin1().constData()));
	}
	fprintf(pFile, ";");
}

const char *SmsConfig::getScopeItemName(const char *pName)
{
	const char *result = pName;
	if(scopeType == NotificationConfig::DeviceScope)
	{
		DataPool &pool = DataPool::getInstance();
		Eqp *pEqp = pool.findEqpByDpName(pName);
		if(pEqp)
		{
			result = pEqp->getVisibleName();
		}
	}
	return result;
}

void SmsConfig::writeUsageFilterFuncTypes(FILE *pFile)
{
	bool firstItem = true;
	foreach(EqpUsageFilterItem *pItem, usageFilterList)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		FunctionalType *pFuncType = FunctionalType::findTypeData(pItem->getTypeId());
		fprintf(pFile, "%s", pFuncType ? pFuncType->getName() : "NOTYPE");
	}
	fprintf(pFile, ";");
}

void SmsConfig::writeUsageFilterAttrNames(FILE *pFile)
{
	bool firstItem = true;
	foreach(EqpUsageFilterItem *pItem, usageFilterList)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		fprintf(pFile, "%s", pItem->getAttrName().constData());
	}
	fprintf(pFile, ";");
}

void SmsConfig::writeUsageFilterReverse(FILE *pFile)
{
	bool firstItem = true;
	foreach(EqpUsageFilterItem *pItem, usageFilterList)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		fprintf(pFile, "%s", pItem->isReverse() ? "True" : "False");
	}
	fprintf(pFile, ";");
}


void SmsConfig::writeCriteriaFuncTypes(FILE *pFile)
{
	bool firstItem = true;
	foreach(EqpMsgCriteria *pCriteria, critList)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		FunctionalType *pFuncType = FunctionalType::findTypeData(pCriteria->getFunctionalType());
		fprintf(pFile, "%s", pFuncType ? pFuncType->getName() : "NOTYPE");
	}
	fprintf(pFile, ";");
}

void SmsConfig::writeCriteriaTypes(FILE *pFile)
{
	bool firstItem = true;
	foreach(EqpMsgCriteria *pCriteria, critList)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		fprintf(pFile, "%s", EqpMsgCriteria::critTypeToString(pCriteria->getType()));
	}
	fprintf(pFile, ";");
}

void SmsConfig::writeCriteriaReverse(FILE *pFile)
{
	bool firstItem = true;
	foreach(EqpMsgCriteria *pCriteria, critList)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		fprintf(pFile, "%s", pCriteria->isReverse() ? "True" : "False");
	}
	fprintf(pFile, ";");
}

void SmsConfig::writeCriteriaSubTypes(FILE *pFile)
{
	bool firstItem = true;
	foreach(EqpMsgCriteria *pCriteria, critList)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		FunctionalType *pFuncType = FunctionalType::findTypeData(pCriteria->getFunctionalType());
		fprintf(pFile, "%s", pFuncType ? pFuncType->getCriteriaKey(pCriteria) : "NONE");
	}
	fprintf(pFile, ";");
}

void SmsConfig::writeCriteriaUpperLimits(FILE *pFile)
{
	bool firstItem = true;
	foreach(EqpMsgCriteria *pCriteria, critList)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		fprintf(pFile, "%8.2E", pCriteria->getUpperLimit());
	}
	fprintf(pFile, ";");
}

void SmsConfig::writeCriteriaLowerLimits(FILE *pFile)
{
	bool firstItem = true;
	foreach(EqpMsgCriteria *pCriteria, critList)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		fprintf(pFile, "%8.2E", pCriteria->getLowerLimit());
	}
	fprintf(pFile, ";");
}

void SmsConfig::writeCriteriaMinDurations(FILE *pFile)
{
	bool firstItem = true;
	foreach(EqpMsgCriteria *pCriteria, critList)
	{
		if(!firstItem)
		{
			fprintf(pFile, "|");
		}
		else
		{
			firstItem = false;
		}
		fprintf(pFile, "%d", pCriteria->getMinDuration());
	}
	fprintf(pFile, ";");
}

/********************************************************************************
********************	Parsing *************************************************
********************************************************************************/

int SmsConfig::parseVisibleName(QString &string, int /* lineNo */, QStringList & /* errList */)
{
	visibleName = string.toLatin1().trimmed();
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfig::parseVisibleName(%s)\n", visibleName.toLatin1().constData());
#else
		printf("SmsConfig::parseVisibleName(%s)\n", visibleName.toLatin1().constData());
		fflush(stdout);
#endif
	}
	return 0;
}

int SmsConfig::parseMachineMode(QString &string, int lineNo, QStringList &machineModes, QStringList &errList)
{
	int coco = 0;
	mode = 0;
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfig::parseMachineMode(): split = %d items\n", itemList.count());
#else
		printf("SmsConfig::parseMachineMode(): split = %d items\n", itemList.count());
		fflush(stdout);
#endif
	}

	int bitNbr;
	foreach(QString item, itemList)
	{
		QString name(item.toLower().toLatin1().trimmed());
		if(DebugCtl::isSms())
		{
#ifdef Q_OS_WIN
			qDebug("SmsConfig::parseMachineMode(): next item = %s\n", name.toLatin1().constData());
#else
			printf("SmsConfig::parseMachineMode(): next item = %s\n", name.toLatin1().constData());
			fflush(stdout);
#endif
		}
		if(name == "none")
		{
		}
		else if(name == "all")
		{
			bitNbr = 0;
			foreach(QString machineMode, machineModes)
			{
				if(DebugCtl::isSms())
				{
#ifdef Q_OS_WIN
					qDebug("SmsConfig::parseMachineMode(): next mode for ALL = %s\n", machineMode.toLatin1().constData());
#else
					printf("SmsConfig::parseMachineMode(): next mode for ALL = %s\n", machineMode.toLatin1().constData());
					fflush(stdout);
#endif
				}
				if(!machineMode.isEmpty())
				{
					mode |= (1<<bitNbr);
				}
				bitNbr++;
			}
			break;	// No more modes can be found
		}
		else
		{
			bitNbr = 0;
			bool found = false;
			foreach(QString machineMode, machineModes)
			{
				if(DebugCtl::isSms())
				{
#ifdef Q_OS_WIN
					qDebug("SmsConfig::parseMachineMode(): next mode = %s\n", machineMode.toLatin1().constData());
#else
					printf("SmsConfig::parseMachineMode(): next mode = %s\n", machineMode.toLatin1().constData());
					fflush(stdout);
#endif
				}
				if(!machineMode.isEmpty())
				{
					if(machineMode.toLower() == name)
					{
						mode |= (1<<bitNbr);
						found = true;
						break;
					}
				}
				bitNbr++;
			}
			if(!found)
			{
				QString errMsg(lineNoError(lineNo));
				errMsg += "unknown machine mode <";
				errMsg += item.toLatin1().trimmed();
				errMsg += ">";
				errList.append(errMsg);
				coco = -1; 
			}
		}
	}
	return coco;
}

int SmsConfig::parseMainType(QString &string, int lineNo, QStringList &errList)
{
	QString name(string.toLower().toLatin1().trimmed());
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfig::parseMainType(%s)\n", name.toLatin1().constData());
#else
		printf("SmsConfig::parseMainType(%s)\n", name.toLatin1().constData());
		fflush(stdout);
#endif
	}
	QList<FunctionalType *> &types = FunctionalType::getTypes();
	foreach(FunctionalType *pType, types)
	{
		if(name == QString(pType->getName()).toLower())
		{
			funcType = pType->getType();
			return 0;
		}
	}
	QString errMsg(lineNoError(lineNo));
	errMsg += "unknown main type <";
	errMsg += string;
	errMsg += ">";
	errList.append(errMsg);
	return -1;
}

int SmsConfig::parseUsageFilterTypes(QString &string, int lineNo, QStringList &errList)
{
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	QString errMsg;
	QList<FunctionalType *> &types = FunctionalType::getTypes();
	foreach(QString item, itemList)
	{
		int type = 0;
		QString name = item.toLatin1().trimmed();
		if(name.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "empty equipment type for usage filter";
			errList.append(errMsg);
			return -1;
		}
		foreach(FunctionalType *pType, types)
		{
			if(name == pType->getName())
			{
				type = pType->getType();
				break;
			}
		}
		if(!type)
		{
			errMsg = lineNoError(lineNo);
			errMsg += "unknown equipment type <";
			errMsg += name;
			errMsg += "> for usage filter";
			errList.append(errMsg);
			return -1;
		}
		else
		{
			EqpUsageFilterItem *pItem = new EqpUsageFilterItem(type, "", false);
			usageFilterList.append(pItem);
		}
	}
	return usageFilterList.count();
}

int SmsConfig::parseUsageFilterAttrNames(QString &string, int lineNo, QStringList &errList)
{
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	QString errMsg;
	int critIdx = 0;
	foreach(QString item, itemList)
	{
		QString name = item.toLatin1().trimmed();
		if(name.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "empty usage filter attribute name";
			errList.append(errMsg);
			return -1;
		}
		QString err;
		if(critIdx >= usageFilterList.count())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "number of attribute names exceeds number of equipment types";
			errList.append(errMsg);
			return -1;
		}
		EqpUsageFilterItem *pItem = usageFilterList.at(critIdx++);
		pItem->setAttrName(name.toLatin1());
	}
	return checkUsageFilterLength(lineNo, critIdx, "attribute names", errList);
}

int SmsConfig::parseUsageFilterReverseFlags(QString &string, int lineNo, QStringList &errList)
{
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	QString errMsg;
	int critIdx = 0;
	foreach(QString item, itemList)
	{
		QString name = item.toUpper().toLatin1().trimmed();
		if(name.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "empty usage filter reverse flag";
			errList.append(errMsg);
			return -1;
		}
		if(critIdx >= usageFilterList.count())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "number of usage filter reverse flags exceeds number of equipment types";
			errList.append(errMsg);
			return -1;
		}
		EqpUsageFilterItem *pItem = usageFilterList.at(critIdx++);
		if(name == "TRUE")
		{
			pItem->setReverse(true);
		}
		else if(name == "FALSE")
		{
			pItem->setReverse(false);
		}
		else
		{
			errMsg = lineNoError(lineNo);
			errMsg += "unknown value for usage filter reverse flag <";
			errMsg += name;
			errMsg += "> (expected TRUE or FALSE)";
			errList.append(errMsg);
			return -1;
		}
	}
	return checkUsageFilterLength(lineNo, critIdx, "usage filter reverse flags", errList);
}

int SmsConfig::parseScopeType(QString &string, int lineNo, QStringList &errList)
{
	QString errMsg;
	QString name(string.toLower().toLatin1().trimmed());
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfig::parseScopeType(%s)\n", name.toLatin1().constData());
#else
		printf("SmsConfig::parseScopeType(%s)\n", name.toLatin1().constData());
		fflush(stdout);
#endif
	}
	scopeType = NotificationConfig::scopeTypeParse(name.toLatin1(), errMsg);
	if(!errMsg.isEmpty())
	{
		QString err(lineNoError(lineNo));
		err += errMsg;
		errList.append(err);
		return -1;
	}
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfig::parseScopeType(%s): resulting scopeType = %d\n", name.toLatin1().constData(), scopeType);
#else
		printf("SmsConfig::parseScopeType(%s): resulting scopeType = %d\n", name.toLatin1().constData(), scopeType);
		fflush(stdout);
#endif
	}
	return 0;
}

int SmsConfig::parseScopeItems(QString &string, int lineNo, QStringList &errList)
{
	QString errMsg;
	if(DebugCtl::isSms())
	{
#ifdef Q_OS_WIN
		qDebug("SmsConfig::parseScopeType(%s) type %d\n", string.toLatin1().constData(), scopeType);
#else
		printf("SmsConfig::parseScopeType(%s) type %d\n", string.toLatin1().constData(), scopeType);
		fflush(stdout);
#endif
	}
	switch(scopeType)
	{
	case NotificationConfig::SectorScope:
	case NotificationConfig::MainPartScope:
	case NotificationConfig::DeviceScope:
		break;
	default:
		errMsg = lineNoError(lineNo);
		errMsg += "unknown scope type ";
		errMsg += QString::number(scopeType);
		errMsg += ", not clear how to interpret scope items";
		errList.append(errMsg);
		return -1;
	}
	DataPool &pool = DataPool::getInstance();
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	int coco = 0;
	Eqp *pEqp = NULL;
	foreach(QString item, itemList)
	{
		QString name = item.toLatin1().trimmed();
		switch(scopeType)
		{
		case NotificationConfig::SectorScope:
			if(!pool.findSectorData(name.toLatin1()))
			{
				errMsg = lineNoError(lineNo);
				errMsg += "unknown sector <";
				errMsg += name;
				errMsg += ">";
				errList.append(errMsg);
				coco = -1;
			}
			else
			{
				addScopeItem(name);
			}
			break;
		case NotificationConfig::MainPartScope:
			if(!pool.findMainPartData(name.toLatin1()))
			{
				errMsg = lineNoError(lineNo);
				errMsg += "unknown main part <";
				errMsg += name;
				errMsg += ">";
				errList.append(errMsg);
				coco = -1;
			}
			else
			{
				addScopeItem(name);
			}
			break;
		case NotificationConfig::DeviceScope:
			if(!(pEqp = pool.findEqpByVisibleName(name.toLatin1())))
			{
				errMsg = lineNoError(lineNo);
				errMsg += "unknown device <";
				errMsg += name;
				errMsg += ">";
				errList.append(errMsg);
				coco = -1;
			}
			else
			{
				addScopeItem(pEqp->getDpName());
			}
			break;
		}
	}
	return coco;
}

int SmsConfig::parseCritJoinType(QString &string, int lineNo, QStringList &errList)
{
	QString err;
	QString name = string.toLatin1().trimmed();
	critJoinType = ComplexCriteria::joinTypeParse(name.toLatin1(), err);
	if(!err.isEmpty())
	{
		QString errMsg(lineNoError(lineNo));
		errMsg += err;
		errList.append(errMsg);
		return -1;
	}
	return 0;
}

int SmsConfig::parseCritEqpTypes(QString &string, int lineNo, QStringList &errList)
{
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	QString errMsg;
	QList<FunctionalType *> &types = FunctionalType::getTypes();
	foreach(QString item, itemList)
	{
		int type = 0;
		QString name = item.toLatin1().trimmed();
		if(name.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "empty equipment type for criteria";
			errList.append(errMsg);
			return -1;
		}
		foreach(FunctionalType *pType, types)
		{
			if(name == pType->getName())
			{
				type = pType->getType();
				break;
			}
		}
		if(!type)
		{
			errMsg = lineNoError(lineNo);
			errMsg += "unknown equipment type <";
			errMsg += name;
			errMsg += "> for criteria";
			errList.append(errMsg);
			return -1;
		}
		else
		{
			EqpMsgCriteria *pCriteria = new EqpMsgCriteria(type, 0, 0, "Some name");
			critList.append(pCriteria);
		}
	}
	return critList.count();
}

int SmsConfig::parseCritTypes(QString &string, int lineNo, QStringList &errList)
{
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	QString errMsg;
	int critIdx = 0;
	foreach(QString item, itemList)
	{
		QString name = item.toLatin1().trimmed();
		if(name.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "empty criteria type";
			errList.append(errMsg);
			return -1;
		}
		QString err;
		int type = EqpMsgCriteria::critTypeParse(name.toLatin1(), err);
		if(!err.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += err;
			errList.append(errMsg);
			return -1;
		}
		if(critIdx >= critList.count())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "number of criteria types exceeds number of equipment types";
			errList.append(errMsg);
			return -1;
		}
		EqpMsgCriteria *pCriteria = critList.at(critIdx++);
		pCriteria->setType(type);
	}
	return checkCriteraLength(lineNo, critIdx, "criteria types", errList);
}

int SmsConfig::parseCritSubTypes(QString &string, int lineNo, QStringList &errList)
{
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	QString errMsg;
	int critIdx = 0;
	foreach(QString item, itemList)
	{
		QString name = item.toLatin1().trimmed();
		if(name.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "empty criteria subtype";
			errList.append(errMsg);
			return -1;
		}
		if(critIdx >= critList.count())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "number of criteria types exceeds number of equipment types";
			errList.append(errMsg);
			return -1;
		}
		EqpMsgCriteria *pCriteria = critList.at(critIdx++);
		FunctionalType *pFuncType = FunctionalType::findTypeData(pCriteria->getFunctionalType());
		if(!pFuncType)
		{
			errMsg = lineNoError(lineNo);
			errMsg += "cannot parse subtype because equipment type is unknown";
			errList.append(errMsg);
			return -1;
		}
		QString err;
		int type = pFuncType->parseCriteriaKey(pCriteria->getType(), name.toLatin1(), err);
		if(!err.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += err;
			errList.append(errMsg);
			return -1;
		}
		pCriteria->setSubType(type);
	}
	return checkCriteraLength(lineNo, critIdx, "criteria subtypes", errList);
}

int SmsConfig::parseCritReverseFlags(QString &string, int lineNo, QStringList &errList)
{
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	QString errMsg;
	int critIdx = 0;
	foreach(QString item, itemList)
	{
		QString name = item.toUpper().toLatin1().trimmed();
		if(name.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "empty criteria reverse flag";
			errList.append(errMsg);
			return -1;
		}
		if(critIdx >= critList.count())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "number of criteria reverse flags exceeds number of equipment types";
			errList.append(errMsg);
			return -1;
		}
		EqpMsgCriteria *pCriteria = critList.at(critIdx++);
		if(name == "TRUE")
		{
			pCriteria->setReverse(true);
		}
		else if(name == "FALSE")
		{
			pCriteria->setReverse(false);
		}
		else
		{
			errMsg = lineNoError(lineNo);
			errMsg += "unknown value for criteria reverse flag <";
			errMsg += name;
			errMsg += "> (expected TRUE or FALSE)";
			errList.append(errMsg);
			return -1;
		}
	}
	return checkCriteraLength(lineNo, critIdx, "criteria reverse flags", errList);
}

int SmsConfig::parseCritUpperLimits(QString &string, int lineNo, QStringList &errList)
{
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	QString errMsg;
	int critIdx = 0;
	foreach(QString item, itemList)
	{
		QString name = item.toLatin1().trimmed();
		// Limit is only important for criteria type = MainValue
		if(critIdx >= critList.count())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "number of criteria upper limits exceeds number of equipment types";
			errList.append(errMsg);
			return -1;
		}
		EqpMsgCriteria *pCriteria = critList.at(critIdx++);
		if(pCriteria->getType() != EqpMsgCriteria::MainValue)
		{
			continue;
		}
		if(name.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "empty upper limit";
			errList.append(errMsg);
			return -1;
		}
		float value;
		if(sscanf(name.toLatin1(), "%g", &value) != 1)
		{
			errMsg = lineNoError(lineNo);
			errMsg += "failed to parse criteria upper limit <";
			errMsg += name;
			errMsg += ">";
			errList.append(errMsg);
			return -1;
		}
		pCriteria->setUpperLimit(value);
	}
	return checkCriteraLength(lineNo, critIdx, "criteria upper limits", errList);
}

int SmsConfig::parseCritLowerLimits(QString &string, int lineNo, QStringList &errList)
{
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	QString errMsg;
	int critIdx = 0;
	foreach(QString item, itemList)
	{
		QString name = item.toLatin1().trimmed();
		// Limit is only important for criteria type = MainValue
		if(critIdx >= critList.count())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "number of criteria lower limits exceeds number of equipment types";
			errList.append(errMsg);
			return -1;
		}
		EqpMsgCriteria *pCriteria = critList.at(critIdx++);
		if(pCriteria->getType() != EqpMsgCriteria::MainValue)
		{
			continue;
		}
		if(name.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "empty lower limit";
			errList.append(errMsg);
			return -1;
		}
		float value;
		if(sscanf(name.toLatin1(), "%g", &value) != 1)
		{
			errMsg = lineNoError(lineNo);
			errMsg += "failed to parse criteria lower limit <";
			errMsg += name;
			errMsg += ">";
			errList.append(errMsg);
			return -1;
		}
		pCriteria->setLowerLimit(value);
	}
	return checkCriteraLength(lineNo, critIdx, "criteria lower limits", errList);
}

int SmsConfig::parseCritMinDurations(QString &string, int lineNo, QStringList &errList)
{
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	QString errMsg;
	int critIdx = 0;
	foreach(QString item, itemList)
	{
		QString name = item.toLatin1().trimmed();
		// Limit is only important for criteria type = MainValue
		if(critIdx >= critList.count())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "number of criteria min durations exceeds number of equipment types";
			errList.append(errMsg);
			return -1;
		}
		EqpMsgCriteria *pCriteria = critList.at(critIdx++);
		if(name.isEmpty())
		{
			errMsg = lineNoError(lineNo);
			errMsg += "empty min. duration";
			errList.append(errMsg);
			return -1;
		}
		int value;
		if(sscanf(name.toLatin1(), "%d", &value) != 1)
		{
			errMsg = lineNoError(lineNo);
			errMsg += "failed to parse criteria min. duration <";
			errMsg += name;
			errMsg += ">";
			errList.append(errMsg);
			return -1;
		}
		pCriteria->setMinDuration(value);
	}
	return checkCriteraLength(lineNo, critIdx, "criteria min. durations", errList);
}

int SmsConfig::parseFilteringType(QString &string, int lineNo, QStringList &errList)
{
	QString name = string.toLatin1().trimmed();
	if(name.isEmpty())
	{
		filterType = MessageFilteringRule::None;
		return 0;
	}
	QString errMsg;
	filterType = MessageFilteringRule::filterTypeParse(name.toLatin1(), errMsg);
	if(!errMsg.isEmpty())
	{
		QString err(lineNoError(lineNo));
		err += errMsg;
		errList.append(err);
		return -1;
	}
	return 0;
}

int SmsConfig::parseFilteringTime(QString &string, int lineNo, QStringList &errList)
{
	QString name = string.toLatin1().trimmed();
	if(name.isEmpty())
	{
		filterTime = 0;
		return 0;
	}
	if(sscanf(name.toLatin1(), "%d", &filterTime) != 1)
	{
		QString err(lineNoError(lineNo));
		err += "error parsing filter time <";
		err += name;
		err += ">";
		errList.append(err);
		return -1;
	}
	return 0;
}

int SmsConfig::parseGroupType(QString &string, int lineNo, QStringList &errList)
{
	QString name = string.toLatin1().trimmed();
	if(name.isEmpty())
	{
		groupType = MessageFilteringRule::GroupNone;
		return 0;
	}
	QString errMsg;
	groupType = MessageFilteringRule::groupTypeParse(name.toLatin1(), errMsg);
	if(!errMsg.isEmpty())
	{
		QString err(lineNoError(lineNo));
		err += errMsg;
		errList.append(err);
		return -1;
	}
	return 0;
}

int SmsConfig::parseGroupTime(QString &string, int lineNo, QStringList &errList)
{
	QString name = string.toLatin1().trimmed();
	if(name.isEmpty())
	{
		groupTime = 0;
		return 0;
	}
	if(sscanf(name.toLatin1(), "%d", &groupTime) != 1)
	{
		QString err(lineNoError(lineNo));
		err += "error parsing grouping time <";
		err += name;
		err += ">";
		errList.append(err);
		return -1;
	}
	return 0;
}

int SmsConfig::parseGroupIndivLimit(QString &string, int lineNo, QStringList &errList)
{
	QString name = string.toLatin1().trimmed();
	if(name.isEmpty())
	{
		indivLimit = 0;
		return 0;
	}
	if(sscanf(name.toLatin1(), "%d", &indivLimit) != 1)
	{
		QString err(lineNoError(lineNo));
		err += "error parsing group individual limit <";
		err += name;
		err += ">";
		errList.append(err);
		return -1;
	}
	return 0;
}

int SmsConfig::parseMessage(QString &string, int lineNo, QStringList &errList)
{
	message = string.toLatin1().trimmed();
	if(message.isEmpty())
	{
		QString err(lineNoError(lineNo));
		err += "empty message text";
		errList.append(err);
		return -1;
	}
	return 0;
}

int SmsConfig::parseGroupMessage(QString &string, int lineNo, QStringList &errList)
{
	groupMessage = string.toLatin1().trimmed();
	if(groupMessage.isEmpty() && (filterType == MessageFilteringRule::Grouping))
	{
		QString err(lineNoError(lineNo));
		err += "empty group message text for filter type = Grouping";
		errList.append(err);
		return -1;
	}
	return 0;
}

int SmsConfig::parseRecipients(QString &string, int lineNo, QStringList &errList)
{
	QStringList itemList = string.split("|", QString::SkipEmptyParts);
	foreach(QString item, itemList)
	{
		QString name = item.toLatin1().trimmed();
		if(name.isEmpty())
		{
			continue;
		}
		recipients.append(name);
	}
	if(recipients.count() == 0)
	{
		QString err(lineNoError(lineNo));
		err += "empty list of recipients";
		errList.append(err);
		return -1;
	}
	return recipients.count();
}


QString SmsConfig::lineNoError(int lineNo)
{
	QString errMsg("Line ");
	errMsg += QString::number(lineNo);
	errMsg += ": ";
	return QString(errMsg);
}

int SmsConfig::checkCriteraLength(int lineNo, int critIdx, const char *msg, QStringList &errList)
{
	if(critIdx == critList.count())
	{
		return critIdx;
	}
	QString errMsg(lineNoError(lineNo));
	errMsg += "number of ";
	errMsg += msg;
	errMsg += " is less than number of criteria eqp. types";
	errList.append(errMsg);
	return -1;
}

int SmsConfig::checkUsageFilterLength(int lineNo, int critIdx, const char *msg, QStringList &errList)
{
	if(critIdx == usageFilterList.count())
	{
		return critIdx;
	}
	QString errMsg(lineNoError(lineNo));
	errMsg += "number of ";
	errMsg += msg;
	errMsg += " is less than number of usage filter eqp. types";
	errList.append(errMsg);
	return -1;
}
