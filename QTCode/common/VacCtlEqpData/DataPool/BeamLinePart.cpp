#include "BeamLinePart.h"
#include "DataPool.h"
#include "DebugCtl.h"

#include "VacType.h"
#include "Sector.h"
#include "Eqp.h"
#include "EqpType.h"

#include <QDebug>
#include <stdlib.h>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

// Function to be called by qsort() to compare coordinates of two
// devices for oredring devices according to their coordinates
static int compareEqpCoord(const void *p1, const void *p2);

// Variable for comparing function above
static bool reverseOrder;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BeamLinePart::BeamLinePart(const char *name, float start, float end)
{
	this->name = name;
	pEqpList = new QList<Eqp *>();
	pFirstEqp = NULL;
	eqpPtrs = NULL;
	nEqpPtrs = 0;
	this->start = start;
	this->end = end;
	minX = minY = maxX = maxY = 0.0;
	if(start > end)
	{
		reverse = true;
		this->start = end;
		this->end = start;
	}
	else
	{
		reverse = false;
	}
	reverseBeamDirection = false;
}

BeamLinePart::~BeamLinePart()
{
	if(eqpPtrs)
	{
		free((void *)eqpPtrs);
	}
	delete pEqpList;
}

/*
**	FUNCTION
**		Create beam line part from text line read in file
**
**	ARGUMENTS
**		name	- Name of survey partition forming this beam line part
**		start	- String containing start coordinate in survey partition
**		end		- String containing end coordinate in survey partition
**		lineNo	- Line number in inpur file, used for error message
**		errList	- Variable where error message shall be added in case of error
**
**	RETURNS
**		Pointer to new beam line part, or
**		NULL in case of error
**
**	CAUTIONS
**		It is responsability of caller to free memory allocated for new beam line part
**		by deleting it when no more needed
*/
BeamLinePart *BeamLinePart::create(char *name, char *start, char *end, int lineNo, QStringList &errList)
{
	float	startCoord, endCoord;
	if(sscanf(start, "%f", &startCoord) != 1)
	{
		errList.append(QString("Line %1: bad start coordinate format <%2>").arg(lineNo).arg(start));
		return NULL;
	}
	if(sscanf(end, "%f", &endCoord) != 1)
	{
		errList.append(QString("Line %1: bad end coordinate format <%2>").arg(lineNo).arg(end));
		return NULL;
	}
	if(endCoord == startCoord)
	{
		errList.append(QString("Line %1:  start position must be != end position").arg(lineNo));
		return NULL;
	}
	return new BeamLinePart(name, startCoord, endCoord);
}

/*
**	FUNCTION
**		Find (X,Y) coordinates for given coordinate along beam line part
**
**	EXTERNAL
**
**
**	PARAMETERS
**		coord	- Coordinate along beam line part
**		resultX	- Resulting X coordinate will be returned here
**		resultY	- Resulting Y coordinate will be returned here
**		ppEqp	- Pointer to the very first device will be returned here
**					if coordinate is before the very first device;
**					pointer to the very last device will be returned here
**					if coordinate is after the very last device.
**
**	RETURNS
**		-1	- coordinate is before the very first device, in this case
**				pointer to the very first device will be returned in ppEqp
**		0	- coordinate was found
**		1	- coordinate is after the very last device, in this case
**				pointer to the very lasts device will be returned in ppEqp
**
**	CAUTIONS
**		None
*/
int BeamLinePart::coordInPart(float coord, float &resultX, float &resultY, Eqp **ppEqp, bool testEqp)
{
	// Note list of equipment in line part is built in coordinate
	// increasing order, may be with exception of lines start/end
	// which are not necessarily placed at right position in list
	// The coordinate increasing order is provided by SQL statement
	// selecting data from DB, so SQL must use coordinate in ORDER BY clause

	// Special case: required coordinate is before the very first passive device in part
	if(!pFirstEqp)
	{
		/*
#ifdef Q_OS_WIN
		qDebug("BeamLinePart::coordInPart(): no equipment in line part %s\n", name);
#else
		printf("BeamLinePart::coordInPart(): no equipment in line part %s\n", name);
		fflush(stdout);
#endif
		*/
		return 10;	// ???? No eqp at all???
	}
	if(coord < pFirstEqp->getStart())
	{
		*ppEqp = pFirstEqp;
		return -1;
	}

	// Another special case - required coordinate is after the very last device
	Eqp *pLastEqp = pEqpList->last();
	if(coord > (pLastEqp->getStart() + pLastEqp->getLength()))
	{
		*ppEqp = pLastEqp;
		return 1;
	}

	// Required coordinate is inside - search is done differently depending on
	// readiness of eqpPtrs array
	if(eqpPtrs)
	{
/*
if(testEqp)
{
	printf("BeamLinePart::coordInPart(): use array\n");
	fflush(stdout);
}
*/
		coordInArray(coord, resultX, resultY, testEqp);
	}
	else
	{
/*
if(testEqp)
{
	printf("BeamLinePart::coordInPart(): use list\n");
	fflush(stdout);
}
*/
		coordInList(coord, resultX, resultY);
	}
	return 0;
}

/*
**	FUNCTION
**		Find (X,Y) coordinates for given coordinate along beam line part,
**		eqpPtrs array is used for search
**
**	EXTERNAL
**
**
**	PARAMETERS
**		coord	- Coordinate along beam line part
**		resultX	- Resulting X coordinate will be returned here
**		resultY	- Resulting Y coordinate will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall only be called when eqpPtrs array is ready.
*/
void BeamLinePart::coordInArray(float coord, float &resultX, float &resultY, bool /* testEqp */)
{
	int	high = nEqpPtrs,
		low = - 1,
		probe;
	Eqp *pPrevEqp;
/*
if(testEqp)
{
printf("Searching for %f: low %d high %d\n", coord, low, high);
fflush(stdout);
}
*/

	if(reverse)
	{
		while((high - low) > 1)
		{
			probe = (low + high) >> 1;
			Eqp *pEqp = eqpPtrs[probe];
/*
if(testEqp)
{
printf("reverse probe %d: %s %f\n", probe, pEqp->getName(), pEqp->getStart());
fflush(stdout);
}
*/
			// Is coordinate within device ?
			if(pEqp->getStart() == coord)
			{
				resultX = pEqp->getStartX();
				resultY = pEqp->getStartY();
				return;
			}
			else if((pEqp->getStart() + pEqp->getLength()) == coord)
			{
				resultX = pEqp->getEndX();
				resultY = pEqp->getEndY();
				return;
			}
			else if((pEqp->getStart() < coord) && (coord < (pEqp->getStart() + pEqp->getLength())))
			{
				resultX = pEqp->getStartX() + (pEqp->getEndX() - pEqp->getStartX()) /
					pEqp->getLength() * (coord - pEqp->getStart());
				resultY = pEqp->getStartY() + (pEqp->getEndY() - pEqp->getStartY()) /
					pEqp->getLength() * (coord - pEqp->getStart());
				return;
			}
			// Not within device - check neighbour	`
			if(pEqp->getStart() < coord)	// Check previous device
			{
				pPrevEqp = eqpPtrs[probe - 1];
				high = probe;
			}
			else	// Check next device
			{
				pPrevEqp = pEqp;
				pEqp = eqpPtrs[probe + 1];
				low = probe;
			}
			if(((pEqp->getStart() + pEqp->getLength()) < coord) &&  (coord < pPrevEqp->getStart()))
			{
/*
if(testEqp)
{
printf("forward prev %s @ %f (%f %f) next %s @ %f (%f %f)\n",
	pPrevEqp->getName(), pPrevEqp->getStart(), pPrevEqp->getEndX(), pPrevEqp->getEndY(),
	pEqp->getName(), pEqp->getStart(), pEqp->getStartX(), pEqp->getStartY());
fflush(stdout);
}
*/
				resultX = pEqp->getEndX() + (pPrevEqp->getStartX() - pEqp->getEndX()) /
					(pPrevEqp->getStart() - (pEqp->getStart() + pEqp->getLength())) *
					(coord - (pEqp->getStart() + pEqp->getLength()));
				resultY = pEqp->getEndY() + (pPrevEqp->getStartY() - pEqp->getEndY()) /
					(pPrevEqp->getStart() - (pEqp->getStart() + pEqp->getLength())) *
					(coord - (pEqp->getStart() + pEqp->getLength()));
				return;
			}
		}
	}
	else
	{
		while((high - low) > 1)
		{
			probe = (low + high) >> 1;
			Eqp *pEqp = eqpPtrs[probe];
/*
if(testEqp)
{
printf("forward probe %d: %s %f\n", probe, pEqp->getName(), pEqp->getStart());
fflush(stdout);
}
*/
			// Is coordinate within device ?
			if(pEqp->getStart() == coord)
			{
				resultX = pEqp->getStartX();
				resultY = pEqp->getStartY();
				return;
			}
			else if((pEqp->getStart() + pEqp->getLength()) == coord)
			{
				resultX = pEqp->getEndX();
				resultY = pEqp->getEndY();
				return;
			}
			else if((pEqp->getStart() < coord) && (coord < (pEqp->getStart() + pEqp->getLength())))
			{
				resultX = pEqp->getStartX() + (pEqp->getEndX() - pEqp->getStartX()) /
					pEqp->getLength() * (coord - pEqp->getStart());
				resultY = pEqp->getStartY() + (pEqp->getEndY() - pEqp->getStartY()) /
					pEqp->getLength() * (coord - pEqp->getStart());
				return;
			}
			// Not within device - check neighbour	`
			if(pEqp->getStart() > coord)	// Check previous device
			{
				pPrevEqp = eqpPtrs[probe - 1];
				high = probe;
			}
			else	// Check next device
			{
				pPrevEqp = pEqp;
				pEqp = eqpPtrs[probe + 1];
				low = probe;
			}
			if(((pPrevEqp->getStart() + pPrevEqp->getLength()) < coord) &&  (coord < pEqp->getStart()))
			{
/*
if(testEqp)
{
printf("forward prev %s @ %f (%f %f) next %s @ %f (%f %f)\n",
	pPrevEqp->getName(), pPrevEqp->getStart(), pPrevEqp->getEndX(), pPrevEqp->getEndY(),
	pEqp->getName(), pEqp->getStart(), pEqp->getStartX(), pEqp->getStartY());
fflush(stdout);
}
*/
				resultX = pPrevEqp->getEndX() + (pEqp->getStartX() - pPrevEqp->getEndX()) /
					(pEqp->getStart() - (pPrevEqp->getStart() + pPrevEqp->getLength())) *
					(coord - (pPrevEqp->getStart() + pPrevEqp->getLength()));
				resultY = pPrevEqp->getEndY() + (pEqp->getStartY() - pPrevEqp->getEndY()) /
					(pEqp->getStart() - (pPrevEqp->getStart() + pPrevEqp->getLength())) *
					(coord - (pPrevEqp->getStart() + pPrevEqp->getLength()));
				return;
			}
		}
	}
}

/*
**	FUNCTION
**		Find (X,Y) coordinates for given coordinate along beam line part,
**		eqpList list is used for search
**
**	EXTERNAL
**
**
**	PARAMETERS
**		coord	- Coordinate along beam line part
**		resultX	- Resulting X coordinate will be returned here
**		resultY	- Resulting Y coordinate will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall only be called when eqpPtrs array is NOT ready.
*/
void BeamLinePart::coordInList(float coord, float &resultX, float &resultY)
{
	int	high = pEqpList->count(),
		low = pEqpList->indexOf(pFirstEqp) - 1,
		probe;
	Eqp *pPrevEqp;
// printf("Searching for %f: low %d high %d\n", coord, low, high);
	while((high - low) > 1)
	{
		probe = (low + high) >> 1;
		Eqp *pEqp = pEqpList->at(probe);
// printf("probe %d: %s %f\n", probe, pEqp->getName(), pEqp->getStart());
		// Is coordinate within device ?
		if(pEqp->getStart() == coord)
		{
			resultX = pEqp->getStartX();
			resultY = pEqp->getStartY();
			return;
		}
		else if((pEqp->getStart() + pEqp->getLength()) == coord)
		{
			resultX = pEqp->getEndX();
			resultY = pEqp->getEndY();
			return;
		}
		else if((pEqp->getStart() < coord) && (coord < (pEqp->getStart() + pEqp->getLength())))
		{
			resultX = pEqp->getStartX() + (pEqp->getEndX() - pEqp->getStartX()) /
				pEqp->getLength() * (coord - pEqp->getStart());
			resultY = pEqp->getStartY() + (pEqp->getEndY() - pEqp->getStartY()) /
				pEqp->getLength() * (coord - pEqp->getStart());
			return;
		}
		// Not within device - check neighbour
		if(pEqp->getStart() > coord)	// Check previous device
		{
			pPrevEqp = pEqpList->at(probe - 1);
			high = probe;
		}
		else	// Check next device
		{
			pPrevEqp = pEqp;
			pEqp = pEqpList->at(probe + 1);
			low = probe;
		}
		if(((pPrevEqp->getStart() + pPrevEqp->getLength()) < coord) &&  (coord < pEqp->getStart()))
		{
			resultX = pPrevEqp->getEndX() + (pEqp->getStartX() - pPrevEqp->getEndX()) /
				(pEqp->getStart() - (pPrevEqp->getStart() + pPrevEqp->getLength())) *
				(coord - (pPrevEqp->getStart() + pPrevEqp->getLength()));
			resultY = pPrevEqp->getEndY() + (pEqp->getStartY() - pPrevEqp->getEndY()) /
				(pEqp->getStart() - (pPrevEqp->getStart() + pPrevEqp->getLength())) *
				(coord - (pPrevEqp->getStart() + pPrevEqp->getLength()));
			return;
		}
	}
}

/*
**	FUNCTION
**		Build locations for start(s) and end(s) of every beam vacuum
**		sector on 3 lines: RED, BLUE, CROSS. Only sectors on main LHC
**		ring are processed, sectors on injection/dump lines are processed
**		in another way.
**
**	EXTERNAL
**
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None.
**
**	CAUTIONS
**		The method shall only be called for LHC machine and ring line.
*/
#ifndef PVSS_SERVER_VERSION
void BeamLinePart::buildBeamSectorBorders(void)
{
	for(int idx = 0 ; idx < pEqpList->count() ; idx++)
	{
		Eqp *pEqp = pEqpList->at(idx);
		if(pEqp->getType() != EqpType::VacDevice)
		{
			continue;
		}
		if(!pEqp->isSectorBorder())
		{
			continue;	// Not a sector border
		}
		Sector *pSectBefore = pEqp->getSectorBefore();
		Sector *pSectAfter = pEqp->getSectorAfter();
		switch(pEqp->getVacType())
		{
		case VacType::RedBeam:
			pSectBefore->setRedEndEqp(pEqp);
			pSectAfter->setRedStartEqp(pEqp);
			break;
		case VacType::BlueBeam:
			pSectBefore->setBlueEndEqp(pEqp);
			pSectAfter->setBlueStartEqp(pEqp);
			break;
		case VacType::CrossBeam:
		case VacType::CommonBeam:
			pSectBefore->setCrossEndEqp(pEqp);
			pSectAfter->setCrossStartEqp(pEqp);
		}
	}	
}
#endif

/*
**
**	FUNCTION
**		Make post-processing after all beam lines and passive equipment data:
**		specific for LINAC2: join parts of 3 tanks to 3 single tanks.
**		The task is so specific that everything is hardcoded
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		It was checked BEFORE calling this method that part contains data for LINAC2
*/
void BeamLinePart::processLinac2(void)
{
	Eqp *pLongTank = NULL;
	int	tankNbr = 0;
	for(int idx = 0 ; idx > pEqpList->count() ; idx++)
	{
		Eqp *pEqp = pEqpList->at(idx);
		if(strncmp(pEqp->getName(), "LIBT_", 5))
		{
			continue;
		}
		int newTankNbr = 0;
		switch(pEqp->getName()[5])
		{
		case '1':
			newTankNbr = 1;
			break;
		case '2':
			newTankNbr = 2;
			break;
		case '3':
			newTankNbr = 3;
			break;
		}
		if(!newTankNbr)
		{
			continue;
		}
		if(newTankNbr != tankNbr)
		{
			pLongTank = pEqp;
			char *buf = pEqp->getName();
#ifdef Q_OS_WIN
			sprintf_s(buf, 6, "LA%d", newTankNbr);
#else
			sprintf(buf, "LA%d", newTankNbr);
#endif
			pEqp->setName(buf);
			tankNbr = newTankNbr;
		}
		else
		{
			pLongTank->setLength(pLongTank->getLength() + pEqp->getLength() +
				(pEqp->getStart() - (pLongTank->getStart() + pLongTank->getLength())));
			pEqpList->removeOne(pEqp);
		}
	}
}

/*
**	FUNCTION
**		Create 'pseudo' beam line part from equipment data
**
**	ARGUMENTS
**		name	- Name of survey partition forming this beam line part
**		start	- Start coordinate in survey partition
**		end		- End coordinate in survey partition
**
**	RETURNS
**		Pointer to new beam line part, or
**		NULL in case of error
**
**	CAUTIONS
**		It is responsability of caller to free memory allocated for new beam line part
**		by deleting it when no more needed
*/
BeamLinePart *BeamLinePart::create(char *name, float start, float end)
{
	return new BeamLinePart(name, start, end);
}

/*
**
**	FUNCTION
**		Add passive equipment to beam line part
**
**	PARAMETERS
**		pEqp	- Pointer to equipment to be added
**
**	RETURNS
**		true	- if equipment has been added to this part;
**		false	- otherwise
**
**	CAUTIONS
**		None.
*/
bool BeamLinePart::addPassiveEqp(Eqp *pEqp)
{
	if(strcmp(name, pEqp->getSurveyPart()))
	{
		return false;
	}
	// Check if EQP coordinates are within coordinates range of this part
	if(start < end)
	{
		if((pEqp->getStart() + pEqp->getLength()) < start)
		{
			return false;
		}
		if(pEqp->getStart() > end)
		{
			return false;
		}
	}
	else
	{
		if((pEqp->getStart() + pEqp->getLength()) < end)
		{
			return false;
		}
		if(pEqp->getStart() > start)
		{
			return false;
		}
	}
	pEqpList->append(pEqp);
	if(!pFirstEqp)
	{
		pFirstEqp = pEqp;
	}
	return true;
}

/*
**
**	FUNCTION
**		Add passive equipment for 'pure survey' mode
**
**	PARAMETERS
**		pEqp	- Pointer to equipment to be added
**
**	RETURNS
**		None.
**
**	CAUTIONS
**		None.
*/
void BeamLinePart::addPureSurveyEqp(Eqp *pEqp)
{
	pEqpList->append(pEqp);
	if(start > pEqp->getStart())
	{
		start = pEqp->getStart();
	}
	if(end < (pEqp->getStart() + pEqp->getLength()))
	{
		end = pEqp->getStart() + pEqp->getLength();
	}
}

/*
**
**	FUNCTION
**		Calculate (X,Y) limits for all equipment in this part of beam line.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None.
**
**	CAUTIONS
**		None.
*/
void BeamLinePart::calculateGeoLimits(void)
{
	Eqp	*pEqp = pEqpList->at(0);

	if(!pEqp)
	{
		return;
	}

	minX = pEqp->getStartX() < pEqp->getEndX() ? pEqp->getStartX() : pEqp->getEndX();
	maxX = pEqp->getStartX() > pEqp->getEndX() ? pEqp->getStartX() : pEqp->getEndX();
	minY = pEqp->getStartY() < pEqp->getEndY() ? pEqp->getStartY() : pEqp->getEndY();
	maxY = pEqp->getStartY() > pEqp->getEndY() ? pEqp->getStartY() : pEqp->getEndY();

	for(int idx = 1 ; idx < pEqpList->count() ; idx++)
	{
		pEqp = pEqpList->at(idx);
		if(pEqp->getStartX() < minX)
		{
			minX = pEqp->getStartX();
		}
		else if(pEqp->getStartX() > maxX)
		{
			maxX = pEqp->getStartX();
		}
		if(pEqp->getEndX() < minX)
		{
			minX = pEqp->getEndX();
		}
		else if(pEqp->getEndX() > maxX)
		{
			maxX = pEqp->getEndX();
		}
		if(pEqp->getStartY() < minY)
		{
			minY = pEqp->getStartY();
		}
		else if(pEqp->getStartY() > maxY)
		{
			maxY = pEqp->getStartY();
		}
		if(pEqp->getEndY() < minY)
		{
			minY = pEqp->getEndY();
		}
		else if(pEqp->getEndY() > maxY)
		{
			maxY = pEqp->getEndY();
		}
	}
}

/*
**
**	FUNCTION
**		Remove all active equipment from beam line part
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BeamLinePart::clearActive(void)
{
	Eqp	*pEqp;
	while(!pEqpList->isEmpty())
	{
		pEqp = pEqpList->first();
		if(pEqp->getType() != EqpType::VacDevice)
		{
			break;
		}
		delete pEqpList->takeFirst();
	}
	buildOrderedEqp();
}

/*
**
** FUNCTION
**		Build in beam part line array of equipment pointers ordered according to
**		positions of equipment.
**
** PARAMETERS
**		None
**
** RETURNS
**		None.
**
** CAUTIONS
**		None.
*/
void BeamLinePart::buildOrderedEqp(void)
{
	int	n, i;
	Eqp	*pEqp;

	nEqpPtrs = 0;
	if(eqpPtrs)
	{
		free((void *)eqpPtrs);
	}
	eqpPtrs = NULL;
	nEqpPtrs = pEqpList->count();
	if(!nEqpPtrs)
	{
		return;
	}
	eqpPtrs = (Eqp **)malloc(nEqpPtrs * sizeof(Eqp *));
	for(n = 0 ; n < pEqpList->count() ; n++)
	{
		eqpPtrs[n] = pEqpList->at(n);
	}
	reverseOrder = reverse;
	qsort((void *)eqpPtrs, nEqpPtrs, sizeof(Eqp *), compareEqpCoord);
/*
printf("BeamLinePart::buildOrderedEqp(%s) after qsort:\n", name);
for(n = 0 ; n < nEqpPtrs ; n++)
{
	printf("  %d : %s @ %8.3f\n", n, eqpPtrs[n]->getName(), eqpPtrs[n]->getStart());
}
fflush(stdout);
*/

	// Fine tuning after qsort(): some items still can be ordered incorrectly
	// because qsort() does not compare ALL pairs in array
	for(n = 0 ; n < nEqpPtrs ; n++)
	{
		if(n == (nEqpPtrs - 1))
		{
			break;
		}
		pEqp = eqpPtrs[n];
		if(pEqp->getType() != EqpType::VacDevice)
		{
			continue;
		}
		for(i = n + 1 ; i < nEqpPtrs ; i++)
		{
			Eqp *pNextEqp = eqpPtrs[i];
			if(pNextEqp->getStart() != pEqp->getStart())
			{
				break;
			}
			if(pNextEqp->getType() != EqpType::VacDevice)
			{
				continue;
			}
			if(pEqp->getSectorBefore() == pNextEqp->getSectorAfter())
			{
				continue;
			}
			if(reverse)
			{
				if(pNextEqp->getSectorAfter() &&
					(pNextEqp->getSectorBefore() != pNextEqp->getSectorAfter()) &&
					(pNextEqp->getSectorBefore() == pEqp->getSectorBefore()) &&
					(!pNextEqp->getSectorAfter()))
				{
					Eqp *pTempEqp = eqpPtrs[i];
					eqpPtrs[i] = eqpPtrs[n];
					eqpPtrs[n] = pTempEqp;
					n--;
					break;
				}
				if(pEqp->getSectorAfter() &&
					(pEqp->getSectorBefore() != pEqp->getSectorAfter()) &&
					(pNextEqp->getSectorBefore() == pEqp->getSectorAfter()) &&
					(!pEqp->getSectorAfter()))
				{
					Eqp *pTempEqp = eqpPtrs[i];
					eqpPtrs[i] = eqpPtrs[n];
					eqpPtrs[n] = pTempEqp;
					n--;
					break;
				}
			}
			else
			{
				if(pEqp->getSectorAfter() &&
					(pEqp->getSectorBefore() != pEqp->getSectorAfter()) &&
					(pEqp->getSectorBefore() == pNextEqp->getSectorBefore()) &&
					(!pNextEqp->getSectorAfter()))
				{
					Eqp *pTempEqp = eqpPtrs[i];
					eqpPtrs[i] = eqpPtrs[n];
					eqpPtrs[n] = pTempEqp;
					n--;
					break;
				}
				if(pNextEqp->getSectorAfter() &&
					(pNextEqp->getSectorBefore() != pNextEqp->getSectorAfter()) &&
					(pEqp->getSectorBefore() == pNextEqp->getSectorAfter()) &&
					(!pEqp->getSectorAfter()))
				{
					Eqp *pTempEqp = eqpPtrs[i];
					eqpPtrs[i] = eqpPtrs[n];
					eqpPtrs[n] = pTempEqp;
					n--;
					break;
				}
			}
		}
	}
/*
printf("BeamLinePart::buildOrderedEqp(%s) FINAL:\n", name);
for(n = 0 ; n < nEqpPtrs ; n++)
{
	printf("  %d : %s @ %8.3f\n", n, eqpPtrs[n]->getName(), eqpPtrs[n]->getStart());
}
fflush(stdout);
*/
}

/*
**
**	FUNCTION
**		Find index of device in array of ordered devices
**
**	PARAMETERS
**		pEqp	- Pointer to device to find
**
**	RETURNS
**		Index of device in array, or
**		-1 if device was not found
**
**	CAUTIONS
**		None
*/
int BeamLinePart::getEqpIdx(Eqp *pEqp)
{
	for(int n = nEqpPtrs - 1 ; n >= 0 ; n--)
	{
		if(eqpPtrs[n] == pEqp)
		{
			return n;
		}
	}
	return -1;
}

/*
**	FUNCTION
**		Find name of device with given index
**
**	PARAMETERS
**		eqpIx	- device index
**
**	RETURNS
**		name of device, or
**		NULL if device was not found
**
**	CAUTIONS
**		None
*/
const char *BeamLinePart::getEqpName(int eqpIdx)
{
	return (eqpIdx >= 0) && (eqpIdx < nEqpPtrs) ? eqpPtrs[eqpIdx]->getName() : NULL;
}

/*
**	FUNCTION
**		Make all devices in this part as short as possible in order to get
**		space required to extend sectors
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BeamLinePart::shortenEqp(void)
{
	float minVisibleWidth = DataPool::getInstance().getMinPassiveEqpVisibleWidth();

	for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(pEqp->getLength() > 0)
		{
			int visible = (int)(pEqp->getLength() / minVisibleWidth);
			if(!visible)
			{
				pEqp->setLength(0);
				pEqp->setEndX(pEqp->getStartX());
				pEqp->setEndY(pEqp->getStartY());
			}
			else
			{
				// Setting invisible to zero is not enouhg, let's try to get further space:
				// length is only important when there are active devices WITHIN long passive
				// device. So let's make them just long enough to still keep all 'active inside'
				float lastInside = pEqp->getStart();
				for(int nextEqpIdx = eqpIdx + 1 ; nextEqpIdx < nEqpPtrs ; nextEqpIdx++)
				{
					Eqp *pNextEqp = eqpPtrs[nextEqpIdx];
					if(pNextEqp->getStart() > (pEqp->getStart() + pEqp->getLength()))
					{
						break;
					}
					lastInside = pNextEqp->getStart();
				}
				float newLength = lastInside - pEqp->getStart() + MIN_HOLE;
				if(newLength < (minVisibleWidth + MIN_HOLE))
				{
					newLength = minVisibleWidth + MIN_HOLE;
				}
				if(newLength < pEqp->getLength())
				{
					pEqp->setEndX(pEqp->getStartX() +
						(pEqp->getEndX() - pEqp->getStartX()) /
						pEqp->getLength() * newLength);
					pEqp->setEndY(pEqp->getStartY() +
						(pEqp->getEndY() - pEqp->getStartY()) /
						pEqp->getLength() * newLength);
					pEqp->setLength(newLength);
				}
			}
		}
	}
}

/*
**
**	FUNCTION
**		Write beam line part content to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**		index	- Index of this part in list of parts
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BeamLinePart::dump(FILE *pFile, int index)
{
	if(!DebugCtl::isData())
	{
		return;
	}
	fprintf(pFile, "  ======= Part %d <%s> from %f to %f %s\n", index, name,
		start, end, (reverse ? "REVERSE" : ""));
	for(int i = 0 ; i < nEqpPtrs ; i++)
	{
		eqpPtrs[i]->dump(pFile, i);
	}
	if(pEqpList)
	{
		fprintf(pFile, "    ------------------ Equipment list in the same part:\n");
		for(int i = 0 ; i < pEqpList->count() ; i++)
		{
			Eqp *pEqp = pEqpList->at(i);
			pEqp->dump(pFile, i);
		}
	}
}

/*
** Compare two devices for ordering, to be called by qsort() only
*/
static int compareEqpCoord(const void *p1, const void *p2)
{
	Eqp	*pEqp1 = *((Eqp **)p1), *pEqp2 = *((Eqp **)p2);

	return pEqp1->compare(pEqp2, reverseOrder);
}
