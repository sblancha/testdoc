#ifndef	EQPVG_STD_H
#define EQPVG_STD_H

//	VG_STD - standard gauges, very similar to VG, but some bits are different

#include "Eqp.h"


class VACCTLEQPDATA_EXPORT EqpVG_STD : public Eqp
{
	Q_OBJECT

public:
	EqpVG_STD(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVG_STD(const EqpVG_STD &src) : Eqp(src) {}
#endif
	~EqpVG_STD();

	virtual void setNewSectors(void);

	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::Pressure; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode mode);
	virtual bool isMainValueNegative(void) const;
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
	virtual void getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end);
	virtual float getGrowFactorForDevList(void);
	virtual int getSpikeFilterForDevList(void);
#endif

#ifndef PVSS_SERVER_VERSION
	// Sector before device for given mode
	virtual inline Sector *getSectorBefore(DataEnum::DataMode mode) const
		{ return mode == DataEnum::Replay ? pSectorBeforeReplay : pSectorBeforeOnline; }

	virtual void setSectorBefore(DataEnum::DataMode mode, Sector *pSector);
	
	// Sector after device for given mode
	virtual inline Sector *getSectorAfter(DataEnum::DataMode mode) const
		{ return mode == DataEnum::Replay ? pSectorAfterReplay : pSectorAfterOnline; }

	virtual void setSectorAfter(DataEnum::DataMode mode, Sector *pSector);

	bool isSectorBorder(DataEnum::DataMode mode) const;
#endif	// PVSS_SERVER_VERSION

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	}
	inline unsigned getRR1(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? rr1Replay : rr1Online;
	}
	inline float getPR(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? prReplay : prOnline;
	}
	inline bool isBlockedOff(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? blockedOffReplay : blockedOffOnline;
	}

	inline bool isHaveBlockedOff(void) { return haveBlockedOff; }
	virtual int getErrorCode(DataEnum::DataMode mode);

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Sector reference for gauge on VPGF may change depending on valve's state in VPGF
#ifndef PVSS_SERVER_VERSION
	// Reference to sector before in online mode
	Sector		*pSectorBeforeOnline;

	// Reference to sector before in replay mode
	Sector		*pSectorBeforeReplay;

	// Reference to sector after in online mode
	Sector		*pSectorAfterOnline;

	// Reference to sector after in replay mode
	Sector		*pSectorAfterReplay;
#endif

	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of PR DPE for online mode
	float	prOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of PR DPE for replay mode
	float	prReplay;

	// Last knwon value of BlockedOFF DPE for online mode
	bool		blockedOffOnline;

	// Last known value of BlockedOFF DPE for replay mode
	bool		blockedOffReplay;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

	// Flag indicating if this device has BlockedOFF DPE
	bool		haveBlockedOff;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, float pr, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, float pr, bool plcAlarm, QString &string);
	virtual void getMainValueString(unsigned rr1, float pr, bool plcAlarm, QString &string);
#endif

};

#endif	// EQPVG_STD_H
