#   08.12.2015	S.Blanchard
#		Ewo SectorViewEwo development in progress
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#

TEMPLATE = subdirs

SUBDIRS = VacCtlEwoUtil \
	VacIconEwo \
	MainViewSps \
	MainViewLhc \
	MainViewTabled \
	VacDpDataEwo \
	VacReplayCtl \
	DateTimeEditEwo \
	DateEditEwo \
	TimeEditEwo \
	SpinBoxEwo \
	DevListCriteriaEditor \
	SectorSelector \
#	SectorViewEwo \      DEVELOPMENT IN PROGRESS
	EqpSelector \
	ExcelWriterEwo \
	ProfileEwo \
	ProfileAllLhcBeamEwo \
	ProfileAllLhcInOutEwo \
	ProfileAllLhcIsolEwo \
	ProfileAllSpsEwo \
	SynopticAllLhcEwo \
	SupPrint \
	ValueProfile \
	SmsConfigTable \
  NotificationGroupTableEwo \
	FlexTable

