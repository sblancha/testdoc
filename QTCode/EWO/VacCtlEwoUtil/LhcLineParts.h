#ifndef LHCLINEPARTS_H
#define	LHCLINEPARTS_H

#include "VacCtlEwoUtilExport.h"

// Class holding information for beam line parts of LHC.
// This class has notsence on it's own, but is used by other classes
// like synoptic, sector and profile view for LHC

#include <qlist.h>

#include "VacLinePart.h"

class Sector;
class LhcRegion;

#define	HUGE_COORDINATE	((float)(1.0E20))

class VACCTLEWOUTIL_EXPORT LhcLineParts  
{
public:
	// Enumeration - how line parts shall be built. Decision shall
	// be taken by descendant of this class by implementing abstract
	// method linePartMethod()
	typedef enum
	{
		TypeNone = 0,		// No line parts
		TypeSector = 1,		// Line parts are sector based (QRL & CRYO)
		TypeBeam = 2		// Line parts are beam based (all beams + passive equipment)
	} BuildType;
		
public:
	LhcLineParts(float start, float end);
	virtual ~LhcLineParts();

	void clear(void) { while(!pPartList->isEmpty()) { delete pPartList->takeFirst(); } }
	void buildLineParts(void);

protected:
	// Start coordinate
	float					start;

	// End coordinate
	float					end;

	// List of parts
	QList<VacLinePart *>	*pPartList;

	// Flag indicating if coordinate range includes IP1 (i.e. crosses 0 coordinate)
	bool					includesIP1;

	// Asbtract method - decide this vacuum line parts shall be built
	virtual BuildType linePartMethod(void) = 0;

	// Abstract method - decide if given sector is of interest or not
	virtual bool isMySector(Sector *pSector) = 0;

	// Asbtract method - decide for type of line to be added for given region type
	virtual int vacTypeOfRegion(int regionType) = 0;

	virtual bool isLocationInRange(LhcRegion *pRegion);
	void buildSectorBasedParts(void);
	void buildBeamBasedParts(void);
	void addLinePart(Sector *pSector, bool afterIp1);
	void addLinePart(int vacType, LhcRegion *pRegion, bool afterIp1);
	void addLinePart(int partVacType, float partStart, float partEnd, bool afterIp1 );
};

#endif	// LHCLINEPARTS_H
