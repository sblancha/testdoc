#ifndef	EQPMESSAGEPART_H
#define	EQPMESSAGEPART_H

#include "VacCtlOnlineNotificationExport.h"

// Primitiv part of the whole message that shall be built for device

#include <qstring.h>

class VACCTLONLINENOTIFICATION_EXPORT EqpMessagePart
{
public:

	typedef enum
	{
		None = 0,
		MessagePartString = 1,
		MessagePartEqpName = 2,
		MessagePartSectorName = 3,
		MessagePartMainPartName = 4,
		MessagePartValue = 5,
		MessagePartGroupCount = 6,
		MessagePartErrorText = 7,
		MessagePartLowLimit = 8,
		MessagePartHighLimit = 9,
		MessagePartEventTime = 10,
		MessagePartSendTime = 11,
		MessagePartEventValue = 12,
		MessageTypeMinDuration = 13,
		MessagePartSlaves = 14,
		MessagePartWarningText = 15
	} MessagePartType;

	EqpMessagePart(MessagePartType type);
	~EqpMessagePart();

	static MessagePartType decodeType(const QString &key, QString &errMsg);

	// Access
	inline MessagePartType getType(void) const { return type; }
	inline const QString &getString(void) const { return string; }
	inline void setString(const QString &string) { this->string = string; }

protected:
	// Type of this message part
	MessagePartType	type;

	// "Pure string" content (only for MessagePartString)
	QString			string;
};

#endif	// EQPMESSAGEPART_H
