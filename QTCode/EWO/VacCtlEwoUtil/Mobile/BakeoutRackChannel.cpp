//	Implementation of BakeoutRackChannel class
/////////////////////////////////////////////////////////////////////////////////

#include "BakeoutRackChannel.h"

#include "MobilePool.h"
#include "Eqp.h"
#include "ResourcePool.h"

// Color definitions. Every definition is arguments for QColor::setRgb()
#define	COLOR_OK			0,255,0
#define	COLOR_ERROR			255,0,0
#define	COLOR_WARNING		255,255,0
#define	COLOR_UNDEFINED		15,127,192
#define	COLOR_NOT_ACTIVE	0,127,127


/////////////////////////////////////////////////////////////////////////////////
////////////////// Construction/destruction

BakeoutRackChannel::BakeoutRackChannel(Eqp *pEqp, int channel)
{
	this->pEqp = pEqp;
	this->channel = channel;

	value = valueSet = 0;
	status = 0;
	valueValid = true;
	active = false;

	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "CH%02d.T", channel);
#else
	sprintf(buf, "CH%02d.T", channel);
#endif
	tDpeName = buf;
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "CH%02d.SET", channel);
#else
	sprintf(buf, "CH%02d.SET", channel);
#endif
	setDpeName = buf;
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "CH%02d.STATE", channel);
#else
	sprintf(buf, "CH%02d.STATE", channel);
#endif
	statusDpeName = buf;
	calculateColor(false);

	MobilePool::getInstance().connectChannel(this, pEqp, channel);

	// Report last known values
	QDateTime timeStamp;
	value = MobilePool::getInstance().getDpeValue(pEqp, tDpeName.toLatin1(), timeStamp).toInt();
	valueSet = MobilePool::getInstance().getDpeValue(pEqp, setDpeName.toLatin1(), timeStamp).toInt();
	status = MobilePool::getInstance().getDpeValue(pEqp, statusDpeName.toLatin1(), timeStamp).toInt();
	calculateColor(false);
	emit eqpDataChanged(this, true);
}

BakeoutRackChannel::~BakeoutRackChannel()
{
	MobilePool::getInstance().disconnectChannel(this, pEqp, channel);
}

/*
**	FUNCTION
**		Slot activated when equipment data has been changed
**
**	PARAMETERS
**		pSrc	- Pointer to device - source of signal
**		dpeName	- Name of DPE whose value has been changed
**		source	- Type of source
**		dpeValue	- New DPE value
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void BakeoutRackChannel::dpeChange(Eqp * /* pSrc */, const char *dpeName,
	DataEnum::Source source, const QVariant &dpeValue, DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	if(source != DataEnum::Own)
	{
		return;	// PLC alarm - processed differently
	}
	if(mode != DataEnum::Online)
	{
		return;
	}
/*
printf("BakeoutRackChannel::dpeChange(): DPE <%s>\n", dpeName);
fflush(stdout);
*/
	int newValue;
	if(tDpeName == dpeName)
	{
/*
printf("BakeoutRackChannel::dpeChange(): T DPE\n");
fflush(stdout);
*/
		newValue = dpeValue.toInt();
		if(value == newValue)
		{
			return;
		}
		value = newValue;
		emit eqpDataChanged(this, false);
	}
	else if(setDpeName == dpeName)
	{
/*
printf("BakeoutRackChannel::dpeChange(): SET DPE\n");
fflush(stdout);
*/
		int newValue = dpeValue.toInt();
		if(valueSet == newValue)
		{
			return;
		}
		valueSet = newValue;
		emit eqpDataChanged(this, false);
	}
	else if(statusDpeName == dpeName)
	{
/*
printf("BakeoutRackChannel::dpeChange(): STATE DPE\n");
fflush(stdout);
*/
		newValue = dpeValue.toInt();
		if(status == newValue)
		{
			return;
		}
		status = newValue;
		calculateColor(true);
	}
}

void BakeoutRackChannel::setActive(bool flag)
{
	if(active == flag)
	{
		return;
	}
	active = flag;
	calculateColor(true);
}

void BakeoutRackChannel::calculateColor(bool emitSignal)
{
	QColor newColor;
	if(!active)
	{
		newColor.setRgb(COLOR_NOT_ACTIVE);
	}
	else
	{
		QString resourceName = "VBAKEOUT.RC.State_";
		resourceName += QString::number(status & STATUS_MASK);
		resourceName += ".Kind";
		if(status & SAF_BIT)
		{
			resourceName += ".SAF";
		}
		int kind;
		if(ResourcePool::getInstance().getIntValue(resourceName, kind) == ResourcePool::OK)
		{
			switch(kind)
			{
			case 0:	// OK
				newColor.setRgb(COLOR_OK);
				break;
			case 1:	// Warning
				newColor.setRgb(COLOR_WARNING);
				break;
			case 2:	// Error
				newColor.setRgb(COLOR_ERROR);
				break;
			default:	// Unknown
				newColor.setRgb(COLOR_UNDEFINED);
				break;
			}
		}
		else
		{
			newColor.setRgb(COLOR_UNDEFINED);
		}
	}
	color = newColor;
	if(emitSignal)
	{
		emit eqpDataChanged(this, true);
	}
}

const QString BakeoutRackChannel::getStatusString(void)
{
	char buf[512];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "CH%02d%s\n",channel, (active ? "" : " : NOT ACTIVE"));
	strcat_s(buf, sizeof(buf) / sizeof(buf[0]), getShortStatusString().toLatin1());
#else
	sprintf(buf, "CH%02d%s\n",channel, (active ? "" : " : NOT ACTIVE"));
	strcat(buf, getShortStatusString().toLatin1());
#endif
	char valueBuf[32];
#ifdef Q_OS_WIN
	sprintf_s(valueBuf, sizeof(valueBuf) / sizeof(valueBuf[0]), "  PCS=%s, SAF=%s\n", (status & PCS_BIT ? "ON" : "OFF"), (status & SAF_BIT ? "ON" : "OFF"));
	strcat_s(buf, sizeof(buf) / sizeof(buf[0]), valueBuf);
	sprintf_s(valueBuf, sizeof(valueBuf) / sizeof(valueBuf[0]), "\nSET=%dC\n", valueSet);
	strcat_s(buf, sizeof(buf) / sizeof(buf[0]), valueBuf);
	sprintf_s(valueBuf, sizeof(valueBuf) / sizeof(valueBuf[0]), "  T=%dC", value);
	strcat_s(buf, sizeof(buf) / sizeof(buf[0]), valueBuf);
#else
	sprintf(valueBuf, "  PCS=%s, SAF=%s\n", (status & PCS_BIT ? "ON" : "OFF"), (status & SAF_BIT ? "ON" : "OFF"));
	strcat(buf, valueBuf);
	sprintf(valueBuf, "\nSET=%dC\n", valueSet);
	strcat(buf, valueBuf);
	sprintf(valueBuf, "  T=%dC", value);
	strcat(buf, valueBuf);
#endif
	return QString(buf);
}

const QString BakeoutRackChannel::getShortStatusString(void)
{
	QString resourceName = "VBAKEOUT.RC.State_";
	resourceName += QString::number(status & STATUS_MASK);
	QString result;
	if(ResourcePool::getInstance().getStringValue(resourceName, result) == ResourcePool::OK)
	{
		return QString(result);
	}
	return QString("Unknown state (%1)").arg(status & STATUS_MASK);
}

