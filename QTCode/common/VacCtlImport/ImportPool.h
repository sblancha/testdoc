#ifndef IMPORTPOOL_H
#define	IMPORTPOOL_H

#include "VacCtlImportExport.h"

//	Data pool containing all data from parsed file

#include "FileParser.h"
#include "NamePool.h"
#include "Archive.h"
#include "S7Connection.h"
#include "PollGroup.h"
#include "DpData.h"
#include "DriverRef.h"

#include <qlist.h>
#include <qhash.h>

class VACCTLIMPORT_EXPORT ImportPool : public FileParser
{
public:
	~ImportPool();

	static ImportPool &getInstance(void);
	static bool isDebugMode(void) { return debugMode; }
	static void setDebugMode(bool flag) { debugMode = flag; }

	int processFile(const char *fileName, const char *machineName, QStringList &errList);

	char *addString(const char *string) { return namePool.addString(string); }
	char *addMultiUseString(const char *string) { return namePool.addMultiUseString(string); }
	int checkArchiveReference(const char *name);
	int checkS7ConnectReference(const char *name);
	int checkPollGroupReference(const char *name);
	void addDriverRef(const int drvNum, const char *drvType) { DriverRef::addRef(drvNum, drvType); }

	Archive *findArchive(const char *name);
	PollGroup *findPollGroup(const char *name);
	S7Connection *findS7Connection(const char *name);
	DpData *findDp(const char *name);
	DpData *findDp(const char *name, const char *dpType);

	void dump(FILE *pFile);

	// Access
	inline bool isMobile(void) { return mobile; }
	void setMobile(bool mobile);
	inline const char *getTimestamp(void) { return timestamp; }
	inline const char *getMachine(void) { return machine; }
	inline QHash<QByteArray, DpData *> &getDps(void) { return *pDps; }
	inline QList<Archive *> &getArchives(void) { return *pArchives; }
	inline QList<PollGroup *> &getPollGroups(void) { return *pPollGroups; }
	inline QList<S7Connection *> &getS7Connections(void) { return *pS7Connections; }

protected:
	// The only available instance of pool
	static ImportPool	instance;
	static bool debugMode;

	ImportPool();

	// Flag indicating if file was read for mobile equipment addresses
	bool					mobile;

	// Timestamp read from file
	char					*timestamp;

	// Machine name read from file
	char					*machine;

	// List of all DPs read from file
	// For fixed equipment key is pure DP name
	// For mobile equipment key id DP name followed by '|' followed by DP type name,
	//	for example, "VGR.123.2R5.B|VGR_T"
	QHash<QByteArray, DpData *>		*pDps;

	// List of all archives read from file
	QList<Archive *>		*pArchives;

	// List of all poll groups read from file
	QList<PollGroup *>		*pPollGroups;

	// List of all S7 connections read from file
	QList<S7Connection *>	*pS7Connections;

	// Name pool
	NamePool				namePool;

	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);

};

#endif	// IMPORTPOOL_H
