#ifndef	VACICONVP_STD_IO_TMP_H
#define	VACICONVP_STD_IO_TMP_H

//	Icon for VP_STDIO - TMP

#include "VacIconVP_STD_IO.h"

class VACCTLEWOUTIL_EXPORT VacIconVP_STD_IO_TMP : public VacIconVP_STD_IO
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVP_STD_IO_TMP(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVP_STD_IO_TMP();

protected:
	// Implement VacIconVP_STD_IO abstract method
	virtual void drawType(QPainter &painter);
};

#endif	// VACICONVP_STD_IO_TMP_H
