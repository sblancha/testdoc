//	Implementation of VacReplayCtlEwo class
/////////////////////////////////////////////////////////////////////////////////

#include "VacReplayCtlEwo.h"

EWO_PLUGIN(VacReplayCtlEwo)


VacReplayCtlEwo::VacReplayCtlEwo(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new VacReplayCtl(parent);
	connect(baseWidget, SIGNAL(dpeListChanged()), this, SLOT(dpeListChange()));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList VacReplayCtlEwo::signalList(void) const
{
	QStringList list;
	list.append("DpeListChanged()");
	return list;
}

/*
**	FUNCTION
**		Slot receiving dpeListChanged signal of VacReplayCtl, emit
**		signal 'DpeListChanged' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacReplayCtlEwo::dpeListChange(void)
{
	emit signal("DpeListChanged");
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList VacReplayCtlEwo::methodList(void) const
{
	QStringList list;
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant VacReplayCtlEwo::invokeMethod(const QString & /* name */, QList<QVariant> & /* values */, QString &error)
{
	error = "VacReplayCtlEwo does not have methods";
	return QVariant();
}

