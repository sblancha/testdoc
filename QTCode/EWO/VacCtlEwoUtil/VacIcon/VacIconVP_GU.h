// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Component Name: VacCtlEwoUtil librairy
Class: Widget Vacuum Pump - Group Unified (VP_GU)
Language: Qt/C++

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: This file contains the defined classes for the icon and widget Vacuum Pump - Group Unified (VP_GU)

Limitations: To be used with vacuum framework.

Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------
#ifndef	VACICONVP_GU_H
#define	VACICONVP_GU_H

#include "VacIcon.h"

class EqpVP_GU;

class VACCTLEWOUTIL_EXPORT VacIconVP_GU : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVP_GU(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVP_GU();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

	// Override method of VacIcon 
	virtual void setDirection(VacIconContainer::Direction newDirection);
	virtual void setPipeColor(const QColor &newColor);

protected:
	// Pointer to my device
	EqpVP_GU *pEqp;

	// Areas for pump and valve symbols. Drawing can be easy using QPainter
	// coordinate transformations, but finding areas for tooltips is not so
	// evident. May be it is more convenient just to keep in meory coordinates
	// of all distinct areas

	QRect	body;
	QRect	vvr1;
	QRect	vvr2;

	void setAreaGeometry(void);

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawPipes(QPainter &painter, bool isVvr2);
	virtual void drawVvr(QPainter &painter, QRect vvr, QString subEqpName);
	virtual void drawBody(QPainter &painter);
	virtual void drawPressureValue(QPainter &painter, QString &sPr);
	// Override VacIcon's reaction on mouse press
	virtual void mousePressEvent(QMouseEvent *pEvent);

	// has vvr2 valve
	bool isVvr2;
};

#endif	// VACICONVP_GU_H
