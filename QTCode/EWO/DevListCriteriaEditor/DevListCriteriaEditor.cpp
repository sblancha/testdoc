//	Implementation of DevListCriteriaEditor class
/////////////////////////////////////////////////////////////////////////////////

#include "DevListCriteriaEditor.h"

EWO_PLUGIN(DevListCriteriaEditor)


DevListCriteriaEditor::DevListCriteriaEditor(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new DevListSelectorWidget(parent);

	// Connect to signals of base widget
	connect(baseWidget, SIGNAL(scopeChanged(void)), this, SLOT(scopeChange(void)));
	connect(baseWidget, SIGNAL(criteriaChanged(void)), this, SLOT(criteriaChange(void)));
	connect(baseWidget, SIGNAL(addEqpRequest(void)), this, SLOT(addEqpRequest(void)));
	connect(baseWidget, SIGNAL(modeChanged(int)), this, SLOT(modeChanged(int)));
	connect(baseWidget, SIGNAL(removeSelected(void)), this, SLOT(removeSelected(void)));
	connect(baseWidget, SIGNAL(removeAll(void)), this, SLOT(removeAll(void)));
	connect(baseWidget, SIGNAL(dataModeChanged(void)), this, SLOT(dataModeChanged(void)));
}

DevListCriteriaEditor::~DevListCriteriaEditor()
{
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList DevListCriteriaEditor::signalList(void) const
{
	QStringList list;
	list.append("scopeChanged()");
	list.append("criteriaChanged()");
	list.append("addEqpRequest()");
	list.append("modeChanged(int mode)");
	list.append("removeSelected()");
	list.append("removeAll()");
	list.append("dataModeChanged()");
	return list;
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList DevListCriteriaEditor::methodList(void) const
{
	QStringList list;
	list.append("void initSectorContent()");
	list.append("void setSelectedSectors(dyn_string sectors)");
	list.append("dyn_string getSelectedSectors()");
	list.append("dyn_string getDpNames()");
	list.append("dyn_bool getCriteriaFlags()");
	list.append("void setCriteriaFlags(dyn_bool flags)");
	list.append("dyn_float getCriteriaValues()");
	list.append("void setCriteriaValues(dyn_float values)");
	list.append("dyn_time getCriteriaTimes()");
	list.append("void setCriteriaTimes(dyn_time times)");
	list.append("dyn_int getSelectedEqpTypes()");
	list.append("void setSelectedEqpTypes(dyn_int types)");
	list.append("bool isNonVacSelected()");
	list.append("void setNonVacSelected(bool selected)");
	list.append("bool isScopeEmpty()");
	list.append("bool isCriteriaEmpty()");
	list.append("bool isCriteriaValid()");
	list.append("string getNextSelectedDp()");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant DevListCriteriaEditor::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "initSectorContent")
	{
		baseWidget->initSectorContent();
		return QVariant();
	}
	else if(name == "setSelectedSectors")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::StringList))
		{
			error = "argument 1 (sectorList) is not dyn_string";
			return QVariant();
		}
		baseWidget->setSelectedSectors(values[0].toStringList());
		return QVariant();
	}
	else if(name == "getSelectedSectors")
	{
		QStringList	sectorList;
		baseWidget->getSelectedSectors(sectorList);
		return QVariant(sectorList);
	}
	else if(name == "getDpNames")
	{
		QStringList	dpList;
		baseWidget->getDpNames(dpList);
		return QVariant(dpList);
	}
	else if(name == "getCriteriaFlags")
	{
		QList<QVariant> flags = baseWidget->getCriteriaFlags();
		return QVariant(flags);
	}
	else if(name == "setCriteriaFlags")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::List))
		{
			error = "argument 1 (flags) is not dyn_bool";
			return QVariant();
		}
		QList<QVariant> listFlags = values[0].toList();
		baseWidget->setCriteriaFlags(listFlags);
		return QVariant();
	}
	else if(name == "getCriteriaValues")
	{
		QList<QVariant> critValues = baseWidget->getCriteriaValues();
		for(int idx = 0 ; idx < critValues.count() ; idx++)
		{
			qDebug("%d: %f\n", idx, critValues.at(idx).toDouble());
		}
		return QVariant(critValues);
	}
	else if(name == "setCriteriaValues")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::List))
		{
			error = "argument 1 (values) is not dyn_float";
			return QVariant();
		}
		QList<QVariant> listValues = values[0].toList();
		baseWidget->setCriteriaValues(listValues);
		return QVariant();
	}
	else if(name == "getCriteriaTimes")
	{
		QList<QVariant> critValues = baseWidget->getCriteriaTimes();
		return QVariant(critValues);
	}
	else if(name == "setCriteriaTimes")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::List))
		{
			error = "argument 1 (times) is not dyn_time";
			return QVariant();
		}
		QList<QVariant> listTimes = values[0].toList();
		baseWidget->setCriteriaTimes(listTimes);
		return QVariant();
	}
	else if(name == "getSelectedEqpTypes")
	{
		QList<QVariant> critValues = baseWidget->getSelectedEqpTypes();
		return QVariant(critValues);
	}
	else if(name == "setSelectedEqpTypes")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::List))
		{
			error = "argument 1 (flags) is not dyn_int";
			return QVariant();
		}
		QList<QVariant> types = values[0].toList();
		baseWidget->setSelectedEqpTypes(types);
		return QVariant();
	}
	else if(name == "isNonVacSelected")
	{
		return QVariant(baseWidget->isNonVacSelected());
	}
	else if(name == "setNonVacSelected")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::Bool))
		{
			error = "argument 1 (selected) is not bool";
			return QVariant();
		}
		bool flag = values[0].toBool();
		baseWidget->setNonVacSelected(flag);
		return QVariant();
	}
	else if(name == "isScopeEmpty")
	{
		return QVariant(baseWidget->isScopeEmpty());
	}
	else if(name == "isCriteriaEmpty")
	{
		return QVariant(baseWidget->isCriteriaEmpty());
	}
	else if(name == "isCriteriaValid")
	{
		return QVariant(baseWidget->isCriteriaValid());
	}
	else if(name == "getNextSelectedDp")
	{
		return QVariant(QString(baseWidget->getNextSelectedDp()));
	}
	else
	{
		error = "Uknown method MainViewTabledEwo.";
		error += name;
	}
	return QVariant();
}

/*
**	FUNCTION
**		Slot receiving changed signal of DevListCriteriaEditor, emit
**		signal 'criteriaChange' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DevListCriteriaEditor::scopeChange(void)
{
	emit signal("scopeChanged");
}

/*
**	FUNCTION
**		Slot receiving changed signal of DevListCriteriaEditor, emit
**		signal 'criteriaChange' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DevListCriteriaEditor::criteriaChange(void)
{
	emit signal("criteriaChanged");
}

void DevListCriteriaEditor::addEqpRequest(void)
{
	emit signal("addEqpRequest");
}

void DevListCriteriaEditor::modeChanged(int mode)
{
	emit signal("modeChanged", QVariant(mode));
}

void DevListCriteriaEditor::removeSelected(void)
{
	emit signal("removeSelected");
}

void DevListCriteriaEditor::removeAll(void)
{
	emit signal("removeAll");
}

void DevListCriteriaEditor::dataModeChanged(void)
{
	emit signal("dataModeChanged");
}

