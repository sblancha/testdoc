//	Implementation of EqpSelectorControl class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpSelectorControl.h"
#include "QListBoxEqp.h"

#include "DataPool.h"
#include "Eqp.h"


#include <qlayout.h>
#include <qgroupbox.h>
#include <qlistwidget.h>
#include <qpushbutton.h>
#include <qmenu.h>



EqpSelectorControl::EqpSelectorControl(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	setting = false;

	// Build layout. Layout is:
	// - four GroupBoxes, each containing one ListBox:
	//	 leftmost one for main parts,
	//	 next one for all sectors in selected main parts
	//	 next one for all devices in selected sectors
	//	 rightmost one for selected sectors
	// - group of 4 buttons between 3rd and 4th lists
	QHBoxLayout *mainBox = new QHBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);

	QGroupBox *mpBox = new QGroupBox("Main Parts", this);
	QVBoxLayout *mpBoxLayout = new QVBoxLayout(mpBox);
	pMainPartList = new QListWidget(mpBox);
	mpBoxLayout->addWidget(pMainPartList, 10);
	pMainPartList->setSelectionMode(QAbstractItemView::ExtendedSelection);
	connect(pMainPartList, SIGNAL(itemSelectionChanged(void)),
		this, SLOT(mpSelectionChanged(void)));
	pMainPartList->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(pMainPartList, SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(mpListContextMenuRequest(const QPoint &)));
	mainBox->addWidget(mpBox);
	mainBox->setStretchFactor(mpBox, 10);

	QGroupBox *sectorBox = new QGroupBox("Sectors", this);
	QVBoxLayout *sectorBoxLayout = new QVBoxLayout(sectorBox);
	pSectorList = new QListWidget(sectorBox);
	sectorBoxLayout->addWidget(pSectorList, 10);
	pSectorList->setSelectionMode(QAbstractItemView::ExtendedSelection);
	connect(pSectorList, SIGNAL(itemSelectionChanged(void)),
		this, SLOT(sectorSelectionChanged(void)));
	pSectorList->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(pSectorList, SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(sectorListContextMenuRequest(const QPoint &)));
	mainBox->addWidget(sectorBox);
	mainBox->setStretchFactor(sectorBox, 10);

	QGroupBox *allEqpBox = new QGroupBox("All Eqp", this);
	QVBoxLayout *allEqpBoxLayout = new QVBoxLayout(allEqpBox);
	pAllEqpList = new QListWidget(allEqpBox);
	allEqpBoxLayout->addWidget(pAllEqpList, 10);
	pAllEqpList->setSelectionMode(QAbstractItemView::ExtendedSelection);
	connect(pAllEqpList, SIGNAL(itemSelectionChanged(void)),
		this, SLOT(allEqpSelectionChanged(void)));
	pAllEqpList->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(pAllEqpList, SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(allEqpListContextMenuRequest(const QPoint &)));
	mainBox->addWidget(allEqpBox);
	mainBox->setStretchFactor(allEqpBox, 20);

	QVBoxLayout *buttonBox = new QVBoxLayout();
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);
	mainBox->addLayout(buttonBox);
	buttonBox->addStretch();
	QPushButton *pRightAllPb = new QPushButton(">>", this);
	QSize size = pRightAllPb->fontMetrics().size(Qt::TextSingleLine, ">>");
	size.setWidth(size.width() + 8);
	size.setHeight(size.height() + 4);
	pRightAllPb->setFixedSize(size);
	connect(pRightAllPb, SIGNAL(clicked(void)), this, SLOT(rightAllPbClicked(void)));
	buttonBox->addWidget(pRightAllPb);

	pRightPb = new QPushButton(">", this);
	pRightPb->setFixedSize(size);
	pRightPb->setEnabled(false);
	connect(pRightPb, SIGNAL(clicked(void)), this, SLOT(rightPbClicked(void)));
	buttonBox->addWidget(pRightPb);

	pLeftPb = new QPushButton("<", this);
	pLeftPb->setFixedSize(size);
	pLeftPb->setEnabled(false);
	connect(pLeftPb, SIGNAL(clicked(void)), this, SLOT(leftPbClicked(void)));
	buttonBox->addWidget(pLeftPb);

	QPushButton *pLeftAllPb = new QPushButton("<<", this);
	pLeftAllPb->setFixedSize(size);
	connect(pLeftAllPb, SIGNAL(clicked(void)), this, SLOT(leftAllPbClicked(void)));
	buttonBox->addWidget(pLeftAllPb);
	buttonBox->addStretch();

	mainBox->setStretchFactor(buttonBox, 0);

	QGroupBox *eqpBox = new QGroupBox("Eqp", this);
	QVBoxLayout *eqpBoxLayout = new QVBoxLayout(eqpBox);
	pEqpList = new QListWidget(eqpBox);
	eqpBoxLayout->addWidget(pEqpList, 10);
	pEqpList->setSelectionMode(QAbstractItemView::ExtendedSelection);
	connect(pEqpList, SIGNAL(itemSelectionChanged(void)),
		this, SLOT(eqpSelectionChanged(void)));
	pEqpList->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(pEqpList, SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(eqpListContextMenuRequest(const QPoint &)));
	mainBox->addWidget(eqpBox);
	mainBox->setStretchFactor(eqpBox, 20);

	// Add all main part names to main part list
	initContent();
}

EqpSelectorControl::~EqpSelectorControl()
{
}

/*
**	FUNCTION
**		Initialize editor content to empty: add and unselect all main parts
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Number of main parts in list
**
**	CAUTIONS
**		None
*/
int EqpSelectorControl::initContent(void)
{
	pEqpList->clear();
	pAllEqpList->clear();
	pSectorList->clear();
	pMainPartList->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	QStringList mpNames;
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		// Ignore DSL sector(s)
		if(pSector->getVacType() == VacType::DSL)
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			bool alreadyInList = false;
			foreach(QString mpName, mpNames)
			{
				if(mpName == pMap->getMainPart()->getName())
				{
					alreadyInList = true;
					break;
				}
			}
			if(!alreadyInList)
			{
				mpNames.append(pMap->getMainPart()->getName());
			}
		}
	}
	if(mpNames.count())
	{
		pMainPartList->insertItems(0, mpNames);
	}
	return mpNames.count();
}

/*
**	FUNCTION
**		Set functional type of equipment to be selected
**
**	ARGUMENTS
**		type	- New functional type to set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::setFunctionalType(int type)
{
	if(type == functionalType)
	{
		return;
	}
	functionalType = type;
	usageFilter.setMainType(type);
	pEqpList->clear();
	sectorSelectionChanged();
}

void EqpSelectorControl::setEqpUsageFilter(QList<int> &funcTypes, QList<QByteArray> &attrNames, QList<bool> &reverseFlags)
{
	if((funcTypes.count() != attrNames.count()) || (funcTypes.count() != reverseFlags.count()))
	{
		return;
	}
	usageFilter.clear();
	for(int idx = 0 ; idx < funcTypes.count() ; idx++)
	{
		usageFilter.addFilterItem(funcTypes.at(idx), attrNames.at(idx), reverseFlags.at(idx));
	}
	pEqpList->clear();
	sectorSelectionChanged();
}


/*
**	FUNCTION
**		Set initial selection in main part, sector and equipment lists
**
**	ARGUMENTS
**		dpList	- List of DP names that must become selected
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::setSelectedEqp(const QStringList &dpList)
{
	DataPool &pool = DataPool::getInstance();
	
	// Find all main parts which must be selected in order to show all required
	// sectors in sector list
	setting = true;
	QStringList listCopy(dpList);
	pMainPartList->clearSelection();
	selectedSectors.clear();
	pEqpList->clear();
	foreach(QString dpName, listCopy)
	{
		Eqp *pEqp = pool.findEqpByDpName(dpName.toLatin1());
		if(!pEqp)
		{
			continue;
		}
		Sector *pSector = pEqp->getSectorBefore();
		if(pSector)
		{
			if(selectedSectors.indexOf(pSector) < 0)
			{
				selectedSectors.append(pSector);
			}
			const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
			for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
			{
				MainPartMap *pMap = maps.at(mapIdx);
				selectInList(pMainPartList, pMap->getMainPart()->getName());
			}

			pSector = pEqp->getSectorAfter();
			if(pSector)
			{
				if(selectedSectors.indexOf(pSector) < 0)
				{
					selectedSectors.append(pSector);
				}
				const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
				for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
				{
					MainPartMap *pMap = maps.at(mapIdx);
					selectInList(pMainPartList, pMap->getMainPart()->getName());
				}
			}
		}
		else if(pEqp->getMainPart())
		{
			selectInList(pMainPartList, pEqp->getMainPart()->getName());
		}
		new QListBoxEqp(pEqpList, pEqp);
	}
	setting = false;

	mpSelectionChanged();
	emit selectionChanged();
}

/*
**	FUNCTION
**		Return names of selected DPs
**
**	ARGUMENTS
**		result	- Variable where names of selected DPs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::getSelectedEqp(QStringList &result) const
{
	result.clear();
	for(int n = 0 ; n < pEqpList->count() ; n++)
	{
		result.append(((QListBoxEqp *)pEqpList->item(n))->getEqp()->getDpName());
	}
}

/*
**	FUNCTION
**		Build list of equipment selected in given QListBox. Selected lines
**		are also removed from list.
**
**	ARGUMENTS
**		pList		- Pointer to QListBox where item shall be checked
**		result	- List where all selected devices shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Only pAllEqpList and pEqpList can be used as pList argument
**		because only these lists contain QListBoxEqp items
*/
void EqpSelectorControl::getSelectedEqp(QListWidget *pList, QList<Eqp *> &result) const
{
	for(int row = pList->count() - 1 ; row >= 0 ; row--)
	{
		if(pList->item(row)->isSelected())
		{
			result.append(((QListBoxEqp *)pList->item(row))->getEqp());
			QListWidgetItem *pItem = pList->takeItem(row);
			if(pItem)
			{
				delete pItem;
			}
		}
	}
}

/*
**	FUNCTION
**		Build list of equipment in given QListBox
**
**	ARGUMENTS
**		pList		- Pointer to QListBox where item shall be checked
**		result	- List where all selected devices shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Only pAllEqpList and pEqpList can be used as pList argument
**		because only these lists contain QListBoxEqp items
*/
void EqpSelectorControl::getAllEqp(QListWidget *pList, QList<Eqp *> &result) const
{
	for(int row = 0 ; row < pList->count() ; row++)
	{
		result.append(((QListBoxEqp *)pList->item(row))->getEqp());
	}
}

/*
**	FUNCTION
**		Check if at least one device is selected
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- No device(s) selected;
**		false	- at least one device is selected
**
**	CAUTIONS
**		None
*/
bool EqpSelectorControl::isEmpty(void)
{
	return pEqpList->count() > 0;
}

/*
**	FUNCTION
**		Select item with given name in given list
**
**	ARGUMENTS
**		pList	- Pointer to QListBox where item shall be selected
**		pName	- Pointer to name to be selected in list
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::selectInList(QListWidget *pList, const QString &name)
{
	for(int n = pList->count() - 1 ; n >= 0 ; n--)
	{
		if(pList->item(n)->text() == name)
		{
			pList->item(n)->setSelected(true);
			break;
		}
	}
}

/*
**	FUNCTION
**		Check if item with given name is selected in given list
**
**	ARGUMENTS
**		pList	- Pointer to QListBox where item shall be checked
**		pName	- Pointer to name to be checked
**
**	RETURNS
**		true	- Item is selected in list;
**		false	- otherwise
**
**	CAUTIONS
**		It is expected that list conatins at most one item with given name
*/
bool EqpSelectorControl::isListItemSelected(QListWidget *pList, const QString &name)
{
	for(int n = pList->count() - 1 ; n >= 0 ; n--)
	{
		if(pList->item(n)->text() == name)
		{
			return pList->item(n)->isSelected();
		}
	}
	return false;
}

/*
**	FUNCTION
**		Find item with given Eqp in given list
**
**	ARGUMENTS
**		pList	- Pointer to QListBox where item shall be checked
**		pName	- Pointer to device to be checked
**
**	RETURNS
**		Index of item with given Eqp in list; or
**		-1 if item was not found
**
**	CAUTIONS
**		Only pAllEqpList and pEqpList can be used as pList argument
**		because only these lists contain QListBoxEqp items
*/
int EqpSelectorControl::findEqpInList(QListWidget *pList, Eqp *pEqp)
{
	for(int n = pList->count() - 1 ; n >= 0 ; n--)
	{
		if(((QListBoxEqp *)pList->item(n))->getEqp() == pEqp)
		{
			return n;
		}
	}
	return -1;
}

/*
**	FUNCTION
**		Slot activated when selection in main part list has been changed.
**		Display all sectors of selected main parts in sector list.
**		Keep current sector selection whenever possible (i.e. if some sector
**		was selected in sector list - it must remain selected in 'new' sector
**		list if it will be there)
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::mpSelectionChanged(void)
{
	if(setting)
	{
		return;
	}
	setting = true;

	selectedMps.clear();
	pSectorList->clear();

	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			if(isListItemSelected(pMainPartList, pMap->getMainPart()->getName()))
			{
				selectedMps.append(pMap->getMainPart());
				pSectorList->addItem(pSector->getName());
				if(selectedSectors.indexOf(pSector) >= 0)
				{
					pSectorList->item(pSectorList->count() - 1)->setSelected(true);
				}
				break;
			}
		}
	}
	setting = false;
	sectorSelectionChanged();
}

/*
**	FUNCTION
**		Add given device to list of all devices if it matches sector
**		selection, functional type and if it is not in list of all
**		devices yet.
**
**	ARGUMENTS
**		pEqp		- Pointer to device to be checked and added if necessary
**
**	RETURNS
**		true	- device has been added to list of all devices;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool EqpSelectorControl::addEqpIfMatches(Eqp *pEqp)
{
	if(!functionalType)
	{
		return false;
	}

	if(pEqp->getFunctionalType() != functionalType)
	{
		return false;
	}
	if(!usageFilter.eqpMatches(pEqp))
	{
		return false;
	}
	Sector *pSector = pEqp->getSectorBefore();
	if(pSector)
	{
		if(selectedSectors.indexOf(pSector) >= 0)
		{
			new QListBoxEqp(pAllEqpList, pEqp);
			return true;
		}
		pSector = pEqp->getSectorAfter();
		if(pSector && (selectedSectors.indexOf(pSector) >= 0))
		{
			new QListBoxEqp(pAllEqpList, pEqp);
			return true;
		}
	}
	else
	{
		MainPart *pMainPart = pEqp->getMainPart();
		if(pMainPart && (selectedMps.indexOf(pMainPart) >= 0))
		{
			new QListBoxEqp(pAllEqpList, pEqp);
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Slot activated when selection in sector list has been changed.
**		Display all equipment of selected sectors in all equipment list.
**		Keep current equipment selection whenever possible (i.e. if some eqp
**		was selected in eqp list - it must remain selected in 'new' eqp
**		list if it will be there)
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::sectorSelectionChanged(void)
{
	if(setting)
	{
		return;
	}
	DataPool &pool = DataPool::getInstance();

	// Renew content of selectedSectors list
	selectedSectors.clear();
	for(int row = 0 ; row < pSectorList->count() ; row++)
	{
		if(pSectorList->item(row)->isSelected())
		{
			Sector *pSector = pool.findSectorData(pSectorList->item(row)->text().toLatin1());
			if(pSector)
			{
				selectedSectors.append(pSector);
			}
		}
	}

	QList<Eqp *> selectedEqp;
	getSelectedEqp(pAllEqpList, selectedEqp);
	pAllEqpList->clear();

	QList<Eqp *> rightEqp;
	getAllEqp(pEqpList, rightEqp);

	QList<Eqp *> &allEqp = pool.getOrderedEqpList();
	for(int idx = 0 ; idx < allEqp.count() ; idx++)
	{
		Eqp *pEqp = allEqp.at(idx);
		int listIdx = rightEqp.indexOf(pEqp);
		if(listIdx >= 0)
		{
			rightEqp.removeAt(listIdx);
			continue;
		}
		if(addEqpIfMatches(pEqp))
		{
			int selIdx = selectedEqp.indexOf(pEqp);
			if(selIdx >= 0)
			{
				pAllEqpList->item(pAllEqpList->count() - 1)->setSelected(true);
				selectedEqp.removeAt(selIdx);
			}
		}
	}

	allEqpSelectionChanged();
}

/*
**	FUNCTION
**		Slot activated when selection in all eqp list has been changed.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::allEqpSelectionChanged(void)
{
	bool somethingSelected = false;
	for(int n = pAllEqpList->count() - 1 ; n >= 0 ; n--)
	{
		if(pAllEqpList->item(n)->isSelected())
		{
			somethingSelected = true;
			break;
		}
	}
	pRightPb->setEnabled(somethingSelected);
}

/*
**	FUNCTION
**		Slot activated when selection in eqp list has been changed.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::eqpSelectionChanged(void)
{
	bool somethingSelected = false;
	for(int n = pEqpList->count() - 1 ; n >= 0 ; n--)
	{
		if(pEqpList->item(n)->isSelected())
		{
			somethingSelected = true;
			break;
		}
	}
	pLeftPb->setEnabled(somethingSelected);
}

/*
**	FUNCTION
**		Slot activated when right mouse button is pressed in main part list.
**		Show popup menu for fast selection change.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::mpListContextMenuRequest(const QPoint & /* point */) {
	QMenu *pMenu = new QMenu();
	pMenu->addAction("Select All", this, SLOT(mpSelectAll()));
	pMenu->addAction("Unselect All", this, SLOT(mpUnselectAll()));
	pMenu->addAction("Invert Selection", this, SLOT(mpInvertSelection()));
	pMenu->exec(QCursor::pos());
}

void EqpSelectorControl::mpSelectAll(void)
{
	pMainPartList->selectAll();
}

void EqpSelectorControl::mpUnselectAll(void)
{
	pMainPartList->clearSelection();
}

void EqpSelectorControl::mpInvertSelection(void)
{
	for(int n = pMainPartList->count() - 1 ; n >= 0 ; n--)
	{
		pMainPartList->item(n)->setSelected(!pMainPartList->item(n)->isSelected());
	}
}

/*
**	FUNCTION
**		Slot activated when right mouse button is pressed in sectors list.
**		Show popup menu for fast selection change.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::sectorListContextMenuRequest(const QPoint & /* point */) {
	QMenu *pMenu = new QMenu();
	pMenu->addAction("Select All", this, SLOT(sectorSelectAll()));
	pMenu->addAction("Unselect All", this, SLOT(sectorUnselectAll()));
	pMenu->addAction("Invert Selection", this, SLOT(sectorInvertSelection()));
	pMenu->exec(QCursor::pos());
}

void EqpSelectorControl::sectorSelectAll(void)
{
	pSectorList->selectAll();
}

void EqpSelectorControl::sectorUnselectAll(void)
{
	pSectorList->clearSelection();
}

void EqpSelectorControl::sectorInvertSelection(void)
{
	for(int n = pSectorList->count() - 1 ; n >= 0 ; n--)
	{
		pSectorList->item(n)->setSelected(!pSectorList->item(n)->isSelected());
	}
}

/*
**	FUNCTION
**		Slot activated when right mouse button is pressed in left eqp list.
**		Show popup menu for fast selection change.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::allEqpListContextMenuRequest(const QPoint & /* point */) {
	QMenu *pMenu = new QMenu();
	pMenu->addAction("Select All", this, SLOT(allEqpSelectAll()));
	pMenu->addAction("Unselect All", this, SLOT(allEqpUnselectAll()));
	pMenu->addAction("Invert Selection", this, SLOT(allEqpInvertSelection()));
	pMenu->exec(QCursor::pos());
}

void EqpSelectorControl::allEqpSelectAll(void)
{
	pAllEqpList->selectAll();
}

void EqpSelectorControl::allEqpUnselectAll(void)
{
	pAllEqpList->clearSelection();
}

void EqpSelectorControl::allEqpInvertSelection(void)
{
	for(int n = pAllEqpList->count() - 1 ; n >= 0 ; n--)
	{
		pAllEqpList->item(n)->setSelected(!pAllEqpList->item(n)->isSelected());
	}
}

/*
**	FUNCTION
**		Slot activated when right mouse button is pressed in right eqp list.
**		Show popup menu for fast selection change.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::eqpListContextMenuRequest(const QPoint & /* point */) {
	QMenu *pMenu = new QMenu();
	pMenu->addAction("Select All", this, SLOT(eqpSelectAll()));
	pMenu->addAction("Unselect All", this, SLOT(eqpUnselectAll()));
	pMenu->addAction("Invert Selection", this, SLOT(eqpInvertSelection()));
	pMenu->exec(QCursor::pos());
}

void EqpSelectorControl::eqpSelectAll(void)
{
	pEqpList->selectAll();
}

void EqpSelectorControl::eqpUnselectAll(void)
{
	pEqpList->clearSelection();
}

void EqpSelectorControl::eqpInvertSelection(void)
{
	for(int n = pEqpList->count() - 1 ; n >= 0 ; n--)
	{
		pEqpList->item(n)->setSelected(!pEqpList->item(n)->isSelected());
	}
}

/*
**	FUNCTION
**		Slot activated when ">>" button was cliecked. Move everything from all eqp
**		list to eqp list.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::rightAllPbClicked(void)
{
	if(!pAllEqpList->count())
	{
		return;
	}

	QList<Eqp *> leftEqp;
	getAllEqp(pAllEqpList, leftEqp);
	if(!leftEqp.count())
	{
		return;
	}

	QList<Eqp *> rightEqp;
	getAllEqp(pEqpList, rightEqp);

	pEqpList->clear();
	pAllEqpList->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<Eqp *> &allEqp = pool.getOrderedEqpList();
	int firstIndex = -1, listIdx;
	bool gotNew = false;
	for(int eqpIdx = 0 ; eqpIdx < allEqp.count() ; eqpIdx++)
	{
		Eqp *pEqp = allEqp.at(eqpIdx);
		if((listIdx = leftEqp.indexOf(pEqp)) >= 0)
		{
			new QListBoxEqp(pEqpList, pEqp);
			gotNew = true;
			pEqpList->item(pEqpList->count() - 1)->setSelected(true);
			if(firstIndex < 0)
			{
				firstIndex = pEqpList->count() - 1;
			}
			leftEqp.removeAt(listIdx);
		}
		else if((listIdx = rightEqp.indexOf(pEqp)) >= 0)
		{
			new QListBoxEqp(pEqpList, pEqp);
			rightEqp.removeAt(listIdx);
		}
		if(!(leftEqp.count() + rightEqp.count()))
		{
			break;
		}
	}
	if(firstIndex >= 0)
	{
		pEqpList->setCurrentItem(pEqpList->item(firstIndex));
//		pEqpList->ensureCurrentVisible();
	}
	pRightPb->setEnabled(false);
	pLeftPb->setEnabled(true);
	if(gotNew)
	{
		emit selectionChanged();
	}
}

/*
**	FUNCTION
**		Slot activated when ">" button was cliecked. Move selected lines from all sectors
**		eqp to eqp list.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::rightPbClicked(void)
{
	QList<Eqp *> leftEqp;
	getSelectedEqp(pAllEqpList, leftEqp);
	if(leftEqp.isEmpty())
	{
		pRightPb->setEnabled(false);
		return;
	}

	QList<Eqp *> rightEqp;
	getAllEqp(pEqpList, rightEqp);
	pEqpList->clear();

	DataPool &pool = DataPool::getInstance();
	const QList<Eqp *> &allEqp = pool.getOrderedEqpList();
	int firstIndex = -1, listIdx;
	bool gotNew = false;
	for(int eqpIdx = 0 ; eqpIdx < allEqp.count() ; eqpIdx++)
	{
		Eqp *pEqp = allEqp.at(eqpIdx);
		if((listIdx = leftEqp.indexOf(pEqp)) >= 0)
		{
			new QListBoxEqp(pEqpList, pEqp);
			gotNew = true;
			pEqpList->item(pEqpList->count() - 1)->setSelected(true);
			if(firstIndex < 0)
			{
				firstIndex = pEqpList->count() - 1;
			}
			leftEqp.removeAt(listIdx);
		}
		else if((listIdx = rightEqp.indexOf(pEqp)) >= 0)
		{
			new QListBoxEqp(pEqpList, pEqp);
			rightEqp.removeAt(listIdx);
		}
	}
	if(firstIndex >= 0)
	{
		pEqpList->setCurrentItem(pEqpList->item(firstIndex));
// TODO		pEqpList->ensureCurrentVisible();
	}
	pRightPb->setEnabled(false);
	pLeftPb->setEnabled(true);
	if(gotNew)
	{
		emit selectionChanged();
	}
}


/*
**	FUNCTION
**		Slot activated when "<" button was cliecked. Move selected lines from eqp
**		list to all eqp list.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::leftPbClicked(void)
{
	QList<Eqp *> rightEqp;
	getSelectedEqp(pEqpList, rightEqp);
	if(rightEqp.isEmpty())
	{
		pLeftPb->setEnabled(false);
		return;
	}

	QList<Eqp *> remainEqp;
	getAllEqp(pEqpList, remainEqp);
	
	pAllEqpList->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<Eqp *> &allEqp = pool.getOrderedEqpList();
	int firstIndex = -1, listIdx;
	setting = true;
	for(int eqpIdx = 0 ; eqpIdx < allEqp.count() ; eqpIdx++)
	{
		Eqp *pEqp = allEqp.at(eqpIdx);
		if((listIdx = remainEqp.indexOf(pEqp)) >= 0)
		{
			remainEqp.removeAt(listIdx);
			continue;
		}
		if(addEqpIfMatches(pEqp))
		{
			int listIdx = rightEqp.indexOf(pEqp);
			if(listIdx >= 0)
			{
				if(firstIndex < 0)
				{
					firstIndex = pAllEqpList->count() - 1;
				}
				pAllEqpList->item(pAllEqpList->count() - 1)->setSelected(true);
				rightEqp.removeAt(listIdx);
			}
		}
	}
	if(firstIndex >= 0)
	{
		pAllEqpList->setCurrentItem(pAllEqpList->item(firstIndex));
// TODO		pAllEqpList->ensureCurrentVisible();
	}
	setting = false;
	pRightPb->setEnabled(firstIndex >= 0);
	pLeftPb->setEnabled(false);
	emit selectionChanged();
}

/*
**	FUNCTION
**		Slot activated when "<<" button was cliecked. Remove all from eqp list,
**		and show (if possible) in all eqp list
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSelectorControl::leftAllPbClicked(void)
{
	QList<Eqp *> rightEqp;
	getAllEqp(pEqpList, rightEqp);
	if(rightEqp.isEmpty())
	{
		return;
	}
	pEqpList->clear();
	pAllEqpList->clear();

	DataPool &pool = DataPool::getInstance();
	const QList<Eqp *> &allEqp = pool.getOrderedEqpList();
	int firstIndex = -1;
	for(int eqpIdx = 0 ; eqpIdx < allEqp.count() ; eqpIdx++)
	{
		Eqp *pEqp = allEqp.at(eqpIdx);
		if(addEqpIfMatches(pEqp))
		{
			int idx = rightEqp.indexOf(pEqp);
			if(idx >= 0)
			{
				pAllEqpList->item(pAllEqpList->count() - 1)->setSelected(true);
				rightEqp.removeAt(idx);
				if(firstIndex < 0)
				{
					firstIndex = pAllEqpList->count() - 1;
				}
			}
		}
	}
	pRightPb->setEnabled(pAllEqpList->count() > 0);
	pLeftPb->setEnabled(false);
	emit selectionChanged();
}
