#
# Project file for qmake utility to build Profile all of LHC - Inner/Outer EWO
#
#	01.10.2010	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = ProfileAllLhcInOutEwo

INCLUDEPATH += ../VacCtlEwoUtil \
	../VacCtlEwoUtil/ProfileLhc \
	../VacCtlEwoUtil/Axis \
	../VacCtlEwoUtil/ProfileAllLHCInnerOuter \
	../../common/VacCtlUtil \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/Platform \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp

HEADERS = ProfileAllLhcInOutEwo.h

SOURCES = ProfileAllLhcInOutEwo.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlHistoryData \
	-lVacCtlEqpData \
	-lVacCtlUtil

