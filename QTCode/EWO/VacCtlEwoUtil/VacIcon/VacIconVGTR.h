#ifndef	VACICONVGTR_H
#define	VACICONVGTR_H

//	Icon for pressure transmitter (alsmot like gauge, with minor differences)

#include "VacIcon.h"

class EqpVGTR;

class VACCTLEWOUTIL_EXPORT VacIconVGTR : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVGTR(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVGTR();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVGTR *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, int pr, bool plcAlarm);

	void setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, int pr, bool plcAlarm);
};


#endif	// VACICONVGTR_H
