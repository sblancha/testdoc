const unsigned VG_SPS_W_PLC_INIT = 0x1u;

//LhcVacActionPossible_PLC:
/** Check that allows executing actions for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, action to be executed
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_PLC(string dpName, string dpType, dyn_anytype act, bool &possible,
  dyn_string &exceptionInfo)
{
  possible = TRUE;
}
//LhcVacDeviceAction_PLC:
/** Execute action for gauge. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_PLC(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  mapping   mConnectDpes;
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_PLC_PING:
    SendPingCmd(dpName, action, exceptionInfo);
    break;
  case LVA_PLC_RESET:
    SendResetCmd(dpName, action, exceptionInfo);
    break;
  case LVA_PLC_INIT:
    SendInitCmd(dpName, exceptionInfo);
    break;	
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["TS1.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS1.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = false;
    mConnectDpes["BATT_STATE"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["TS1.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS1.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = false;
    mConnectDpes["BATT_STATE"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacDeviceAction_PLC: unknown action " + act, "");
  }
}


//LhcVacGetRequiredActPriv_VGR_P:
/** Check if gauge is in a domain that need special privelege to execute action for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param defaultPriv: in/out default privilige for action/necessary privilege for action.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacGetRequiredActPriv_PLC(string dpName, const dyn_anytype action, int &defaultPriv, dyn_string &exceptionInfo)
{
  
  switch(action[1] & LVA_ACTION_MASK)
  {
  case LVA_PLC_PING:
  case LVA_PLC_RESET:
  case LVA_PLC_INIT:
    if(defaultPriv < 4)
    {
      defaultPriv = 4;
    }
    break;
  }
  return;
}

void SendPingCmd(string dpName, dyn_anytype data, dyn_string &exceptionInfo)
{
  string command = "start \\\\cern.ch\\dfs\\Applications\\Putty\\PuTTY-0.60b\\PLINK.EXE " + data[2] + " -l vacin -pw " + data[3] + " ping -c20 ";
 //  ping = "start \\\\cern.ch\\dfs\\Applications\\Putty\\PuTTY-0.60b\\PLINK.EXE cs-ccr-ps1 -l cryolhc -pw ***** ping -c20 "+device+".cern.ch";  
  SendCommandForPlc(command, dpName, exceptionInfo);
}
void SendResetCmd(string dpName, dyn_anytype data, dyn_string &exceptionInfo)
{
  string command = "start \\\\cern.ch\\dfs\\Applications\\Putty\\PuTTY-0.60b\\PLINK.EXE " + data[2] + " -l vacin -pw " + data[3] + " /mcr/reset/rem_reset -f ";
  SendCommandForPlc(command, dpName, exceptionInfo);
}
void SendInitCmd(string dpName, dyn_string &exceptionInfo)
{
  dpSet(dpName + ".WR1", VG_SPS_W_PLC_INIT);
  dyn_errClass 	err;
  dyn_string    exceptionInfo;
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "SendInitCmd for PLC  <" + dpName + "> failed: " + err, "");
    return;
  }
}
void GetPasswordAndServerName(dyn_string &data)
{
  dyn_float   dfReturn;
  ChildPanelOnCentralModalReturn("vision/dialogs/PlcStatePassword.pnl", "Password",
    makeDynString(""), dfReturn, data);
}


void SendCommandForPlc(string command, string plcName, dyn_string &exceptionInfo)  
{  
  string plcOfficialName;
  LhcVacGetAttribute(plcName, "OfficialName" , plcOfficialName);  
  if(plcOfficialName == "")
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "SendCommandForPlc for PLC  <" + plcName + "> failed (no official name)", "");
    return;    
  }
  command += (plcOfficialName + ".cern.ch");
//  DebugTN(" command " , command);
  int result = system(command);
  
//  DebugTN("result ", result);
}

// Get and show timestamp of PLC data
void BuildStringFromTS(string dpName, string &result)
{
  unsigned ymdh, msm;

  result = "unknown";
  // next check is needed for the following reason
  // for example PLCM_VELO has no correspondant dp in VPLC_A and VPLC_A0
  // So, dpName will be empty
  if( dpName == "")
  {
    return;
  }
  dpGet(dpName + ".TS1.YMDH", ymdh,
    dpName + ".TS1.MSM", msm);  
  
  if(ymdh != 0)
  {
    sprintf(result, "20%02d-%02d-%02d_%02d:%02d:%02d",
      (ymdh >> 24), ((ymdh >> 16) & 0xFF), ((ymdh >> 8) & 0xFF), (ymdh & 0xFF),
      (msm >> 24), ((msm >> 16) & 0xFF));
  }
  
}  
