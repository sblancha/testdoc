//	Implementation of SectorSelectorWidget class
/////////////////////////////////////////////////////////////////////////////////

#include "SectorSelectorWidget.h"

#include "DataPool.h"
#include "ResourcePool.h"

#include <qlayout.h>
#include <qgroupbox.h>
#include <QListWidget>
#include <qmenu.h>
#include <qcheckbox.h>

SectorSelectorWidget::SectorSelectorWidget(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	setting = false;

	// Build layout. Layout is very simple: two GroupBoxes, each containing
	// one ListBox: left one for main parts, right one for sectors
	QHBoxLayout *mainBox = new QHBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);

	QGroupBox *mpBox = new QGroupBox("Main Parts", this);
	QVBoxLayout *pMpBoxLayout = new QVBoxLayout(mpBox);
	pMpBoxLayout->setContentsMargins(0, 0, 0, 0);
	pMpBoxLayout->setSpacing(0);

	pMainPartList = new QListWidget(mpBox);
	pMainPartList->setSelectionMode(QAbstractItemView::ExtendedSelection);
	connect(pMainPartList, SIGNAL(itemSelectionChanged(void)),
		this, SLOT(mpSelectionChanged(void)));
	pMainPartList->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(pMainPartList, SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(mpContextMenuRequest(const QPoint &)));
	pMpBoxLayout->addWidget(pMainPartList, 10);

	if(DataPool::getInstance().isLhcFormat())
	{
		// Add checkboxes for BEAM/ISOL filtering
		QHBoxLayout *vacTypeBox = new QHBoxLayout();
		vacTypeBox->setContentsMargins(0, 0, 0, 0);
		vacTypeBox->setSpacing(0);
		pMpBoxLayout->addLayout(vacTypeBox);

		pRedBeamVacCb = new QCheckBox("R", this);
		pRedBeamVacCb->setChecked(true);
		vacTypeBox->addWidget(pRedBeamVacCb, 1);
		connect(pRedBeamVacCb, SIGNAL(toggled(bool)), this, SLOT(vacSelectChanged(bool)));

		pBlueBeamVacCb = new QCheckBox("B", this);
		pBlueBeamVacCb->setChecked(true);
		vacTypeBox->addWidget(pBlueBeamVacCb, 1);
		connect(pBlueBeamVacCb, SIGNAL(toggled(bool)), this, SLOT(vacSelectChanged(bool)));

		pCryoVacCb = new QCheckBox("M", this);
		pCryoVacCb->setChecked(true);
		vacTypeBox->addWidget(pCryoVacCb, 1);
		connect(pCryoVacCb, SIGNAL(toggled(bool)), this, SLOT(vacSelectChanged(bool)));

		pQrlVacCb = new QCheckBox("Q", this);
		pQrlVacCb->setChecked(true);
		vacTypeBox->addWidget(pQrlVacCb, 1);
		connect(pQrlVacCb, SIGNAL(toggled(bool)), this, SLOT(vacSelectChanged(bool)));
	}
	else
	{
		pRedBeamVacCb = pBlueBeamVacCb = pCryoVacCb = pQrlVacCb = NULL;
	}
	mainBox->addWidget(mpBox);

	QGroupBox *sectorBox = new QGroupBox("Sectors", this);
	QVBoxLayout *pSectorBoxLayout = new QVBoxLayout(sectorBox);
	pSectorBoxLayout->setContentsMargins(0, 0, 0, 0);
	pSectorBoxLayout->setSpacing(0);

	pSectorList = new QListWidget(sectorBox);
	pSectorList->setSelectionMode(QAbstractItemView::ExtendedSelection);
	// In order to avoid too narrow setor list
	int maxWidth = 64;
	const QList<Sector *> &sectList = DataPool::getInstance().getSectors();
	QFontMetrics fm = pSectorList->fontMetrics();
	for(int idx = 0 ; idx < sectList.count() ; idx++)
	{
		Sector *pSector = sectList.at(idx);
		QRect textRect = fm.boundingRect(pSector->getName());
		if(textRect.width() > maxWidth)
		{
			maxWidth = textRect.width();
		}
	}
	pSectorList->setMinimumWidth(maxWidth + 16);
	connect(pSectorList, SIGNAL(itemSelectionChanged(void)),
		this, SLOT(sectorSelectionChanged(void)));
	pSectorList->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(pSectorList, SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(sectorContextMenuRequest(const QPoint &)));
	pSectorBoxLayout->addWidget(pSectorList, 10);
	mainBox->addWidget(sectorBox);

	// Add all main part names to main part list
	initContent();
}

SectorSelectorWidget::~SectorSelectorWidget()
{
}

/*
**	FUNCTION
**		Initialize editor content to empty: add and unselect all main parts
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Number of main parts in list
**
**	CAUTIONS
**		None
*/
int SectorSelectorWidget::initContent(void)
{
	pSectorList->clear();
	pMainPartList->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	QStringList mpNames;
	for(int idx = 0 ; idx < sectors.count() ; idx++)
	{
		Sector *pSector = sectors.at(idx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		// Ignore DSL sector(s)
		/* L.Kopylov 14.11.2012 why are DSL sectors ignored???
		if(pSector->getVacType() == VacType::DSL)
		{
			continue;
		}
		*/
		if(!vacTypeMatches(pSector->getVacType()))
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			bool alreadyInList = false;
			foreach(QString mpName, mpNames)
			{
				if(mpName == pMap->getMainPart()->getName())
				{
					alreadyInList = true;
					break;
				}
			}
			if(!alreadyInList)
			{
				mpNames.append(pMap->getMainPart()->getName());
			}
		}
	}
	// Special item - 'non-vacuum devices'
	QString specName;
	if(ResourcePool::getInstance().getStringValue("DeviceList.NoMainPartItem", specName) != ResourcePool::OK)
	{
		specName = "NO MAIN PART";
	}
	pMainPartList->insertItem(0, specName);
	if(mpNames.count())
	{
		pMainPartList->insertItems(1, mpNames);
	}
	return mpNames.count();
}

/*
**	FUNCTION
**		Set initial selection in both main part and sector lists
**
**	ARGUMENTS
**		sectorList	- List of sector names that must become selected
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorWidget::setSelectedSectors(const QStringList &sectorList)
{
	DataPool &pool = DataPool::getInstance();
	
	// Find all main parts which must be selected in order to show all required
	// sectors in sector list
	setting = true;
	if(pQrlVacCb)
	{
		pQrlVacCb->setChecked(true);
		pCryoVacCb->setChecked(true);
		pBlueBeamVacCb->setChecked(true);
		pRedBeamVacCb->setChecked(true);
		updateMpContent(true);
	}
	QStringList listCopy(sectorList);
	pMainPartList->clearSelection();
	foreach(QString sectName, listCopy)
	{
		Sector *pSector = pool.findSectorData(sectName.toLatin1());
		if(!pSector)
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			selectInList(pMainPartList, pMap->getMainPart()->getName());
		}
	}
	setting = false;
	mpSelectionChanged();

	// Select all given sectors in list of sectors
	setting = true;
	pSectorList->clearSelection();
	foreach(QString sectName, sectorList)
	{
		selectInList(pSectorList, sectName);
	}
	setting = false;
	emit selectionChanged();
}

/*
**	FUNCTION
**		Return names of selected sectors
**
**	ARGUMENTS
**		result	- Variable where names of selected sectors will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorWidget::getSelectedSectors(QStringList &result) const
{
	result.clear();
	QList<QListWidgetItem *> selItems = pSectorList->selectedItems();
	for(int n = 0 ; n < selItems.count() ; n++)
	{
		result.append(selItems.at(n)->text());
	}
}

/*
**	FUNCTION
**		Check if at least one sector is selected
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- No sector(s) selected;
**		false	- at least one sector is selected
**
**	CAUTIONS
**		None
*/
bool SectorSelectorWidget::isEmpty(void)
{
	QList<QListWidgetItem *> selItems = pSectorList->selectedItems();
	return selItems.isEmpty();
}

bool SectorSelectorWidget::isNonVacSelected(void) const
{
	// 'No main part' is always 1st item in list
	QListWidgetItem *pItem = pMainPartList->item(0);
	if(pItem)
	{
		return pItem->isSelected();
	}
	return false;
}

void SectorSelectorWidget::setNonVacSelected(bool flag)
{
	// 'No main part' is always 1st item in list
	if(!pMainPartList->count())
	{
		return;
	}
	QListWidgetItem *pItem = pMainPartList->item(0);
	if(pItem)
	{
		pItem->setSelected(flag);
	}
}

/*
**	FUNCTION
**		Select item with given name in given list
**
**	ARGUMENTS
**		pList	- Pointer to QListBox where item shall be selected
**		pName	- Pointer to name to be selected in list
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorWidget::selectInList(QListWidget *pList, const QString &name)
{
	for(int n = pList->count() - 1 ; n >= 0 ; n--)
	{
		if(pList->item(n)->text() == name)
		{
			pList->item(n)->setSelected(true);
			break;
		}
	}
}

/*
**	FUNCTION
**		Check if item with given name is selected in given list
**
**	ARGUMENTS
**		pList	- Pointer to QListBox where item shall be checked
**		pName	- Pointer to name to be checked
**
**	RETURNS
**		true	- Item is selected in list;
**		false	- otherwise
**
**	CAUTIONS
**		It is expected that list conatins at most one item with given name
*/
bool SectorSelectorWidget::isListItemSelected(QListWidget *pList, const QString &name)
{
	for(int n = pList->count() - 1 ; n >= 0 ; n--)
	{
		if(pList->item(n)->text() == name)
		{
			return pList->item(n)->isSelected();
		}
	}
	return false;
}

/*
**	FUNCTION
**		Slot activated when selection in main part list has been changed.
**		Display all sectors of selected main parts in sector list.
**		Keep current sector selection whenever possible (i.e. if some sector
**		was selected in sector list - it must remain selected in 'new' sector
**		list if it will be there)
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorWidget::mpSelectionChanged(void)
{
	if(setting)
	{
		return;
	}
	QStringList selectedSectors;
	getSelectedSectors(selectedSectors);
	pSectorList->clear();

	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
/*
printf("SectorSelectorWidget::mpSelectionChanged(): check %s (%d)\n",
	pSector->getName(), pSector->getBeamOrder());
fflush(stdout);
*/
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		// Ignore DSL sector(s)
		/* L.Kopylov 14.11.2012 why are DSL sectors ignored???
		if(pSector->getVacType() == VacType::DSL)
		{
			continue;
		}
		*/
		if(!vacTypeMatches(pSector->getVacType()))
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			if(isListItemSelected(pMainPartList, pMap->getMainPart()->getName()))
			{
				pSectorList->addItem(pSector->getName());
				break;
			}
		}
	}
	setting = true;
	foreach(QString sectName, selectedSectors)
	{
		selectInList(pSectorList, sectName);
	}
	setting = false;
	emit selectionChanged();
}

/*
**	FUNCTION
**		Check if vacuum type matches recent checkbox selection
**
**	ARGUMENTS
**		vacType	- Vacuum type to check
**
**	RETURNS
**		true	- if vacuum typematches checkbox selection
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SectorSelectorWidget::vacTypeMatches(int vacType)
{
	if(!pQrlVacCb)
	{
		return true;	// No checkboxes
	}
	if(vacType & (VacType::None | VacType::Beam | VacType::CrossBeam | VacType::CommonBeam | VacType::RedBeam))
	{
		if(pRedBeamVacCb->isChecked())
		{
			return true;
		}
	}
	if(vacType & (VacType::None | VacType::Beam | VacType::CrossBeam | VacType::CommonBeam | VacType::BlueBeam))
	{
		if(pBlueBeamVacCb->isChecked())
		{
			return true;
		}
	}
	if(vacType & (VacType::None | VacType::Cryo | VacType::DSL))
	{
		if(pCryoVacCb->isChecked())
		{
			return true;
		}
	}
	if(vacType & (VacType::None | VacType::Qrl))
	{
		if(pQrlVacCb->isChecked())
		{
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Slot activated when selection in sector list has been changed.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorWidget::sectorSelectionChanged(void)
{
	if(setting)
	{
		return;
	}
	emit selectionChanged();
}
	
/*
**	FUNCTION
**		Slot activated when one of 'vacuum' checkboxe's state was changed.
**		Update list of main parts and sectors according to new checkbox setting.
**
**	ARGUMENTS
**		flag	- New state of checkbox
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorWidget::vacSelectChanged(bool /* flag */)
{
	updateMpContent();
}

/*
**	FUNCTION
**		
**		Update list of main parts and sectors according setting of checboxes
**		BEAM and ISOL
**
**	ARGUMENTS
**		force - update content even if setting
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorWidget::updateMpContent(bool force)
{
	if(setting && (!force))
	{
		return;
	}

	// Preserve selected main parts
	QStringList selectedMps;
	if(!force)
	{
		for(int n = 0 ; n < pMainPartList->count() ; n++)
		{
			if(pMainPartList->item(n)->isSelected())
			{
				selectedMps.append(pMainPartList->item(n)->text());
			}
		}
	}

	// Build new list of main parts
	setting = true;
	pMainPartList->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	QStringList mpNames;
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		// Ignore DSL sector(s), check setting of checkboxes
		if(!vacTypeMatches(pSector->getVacType()))
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			bool alreadyInList = false;
			foreach(QString mpName, mpNames)
			{
				if(mpName == pMap->getMainPart()->getName())
				{
					alreadyInList = true;
					break;
				}
			}
			if(!alreadyInList)
			{
				mpNames.append(pMap->getMainPart()->getName());
			}
		}
	}
	// Special item - 'non-vacuum devices'
	QString specName;
	if(ResourcePool::getInstance().getStringValue("DeviceList.NoMainPartItem", specName) != ResourcePool::OK)
	{
		specName = "NO MAIN PART";
	}
	pMainPartList->insertItem(0, specName);
	if(mpNames.count())
	{
		pMainPartList->insertItems(1, mpNames);
	}

	// Restore selection whenever possible
	foreach(QString mp, selectedMps)
	{
		selectInList(pMainPartList, mp);
	}
	setting = false;
	if(!force)
	{
		mpSelectionChanged();
	}
}


void SectorSelectorWidget::mpContextMenuRequest(const QPoint &point)
{
	QMenu *pMenu = new QMenu();
	pMenu->addAction("Select All", this, SLOT(mpSelectAll()));
	pMenu->addAction("Unselect All", this, SLOT(mpUnselectAll()));
	pMenu->addAction("Invert Selection", this, SLOT(mpInvertSelection()));
	pMenu->exec(pMainPartList->mapToGlobal(point));
}


void SectorSelectorWidget::mpSelectAll(void)
{
	pMainPartList->selectAll();
}

void SectorSelectorWidget::mpUnselectAll(void)
{
	pMainPartList->clearSelection();
}

void SectorSelectorWidget::mpInvertSelection(void)
{
	for(int n = pMainPartList->count() - 1 ; n >= 0 ; n--)
	{
		pMainPartList->item(n)->setSelected(!pMainPartList->item(n)->isSelected());
	}
}

void SectorSelectorWidget::sectorContextMenuRequest(const QPoint &point)
{
	QMenu *pMenu = new QMenu();
	pMenu->addAction("Select All", this, SLOT(sectorSelectAll()));
	pMenu->addAction("Unselect All", this, SLOT(sectorUnselectAll()));
	pMenu->addAction("Invert Selection", this, SLOT(sectorInvertSelection()));
	pMenu->exec(pSectorList->mapToGlobal(point));
}

void SectorSelectorWidget::sectorSelectAll(void)
{
	pSectorList->selectAll();
}

void SectorSelectorWidget::sectorUnselectAll(void)
{
	pSectorList->clearSelection();
}

void SectorSelectorWidget::sectorInvertSelection(void)
{
	for (int n = pSectorList->count() - 1; n >= 0; n--)
	{
		pSectorList->item(n)->setSelected(!pSectorList->item(n)->isSelected());
	}
}

