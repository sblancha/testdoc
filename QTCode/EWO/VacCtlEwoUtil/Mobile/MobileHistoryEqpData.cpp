//	Implementation of MobileHistoryEqpData class
/////////////////////////////////////////////////////////////////////////////////

#include "MobileHistoryEqpData.h"

#include "MobilePool.h"

#include "DpeHistoryQuery.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

MobileHistoryEqpData::MobileHistoryEqpData(Eqp *pEqp, QDateTime &absStartTime) :
	HistoryEqpData(pEqp, absStartTime)
{
	channel = 0;
	valueType = DataEnum::Pressure;
	selected = false;
	used = true;
}

MobileHistoryEqpData::MobileHistoryEqpData(Eqp *pEqp, QDateTime &absStartTime, int channel, enum BakeoutWorkDp::ChannelValueType channelValueType)
{
	this->pEqp = pEqp;
	this->absStartTime = absStartTime;
	this->channel = channel;
	this->channelValueType = channelValueType;
	switch (channelValueType) {
	case BakeoutWorkDp::ChannelValueTypeStatus:
		valueType = DataEnum::Status;
		break;
	default:
		valueType = DataEnum::Temperature;
		break;
	}
	selected = false;
	used = true;

	// Calculate DPE name and connect to device
	dpeName = MobilePool::findBakeoutDpeName(pEqp, channel, channelValueType).toLatin1();

	if(MobilePool::getInstance().connectValue(this, pEqp, channel, channelValueType))
	{
		// Get and apply immediately last known value ???
		const QVariant &value = MobilePool::getInstance().getDpeValue(pEqp, channel, channelValueType);
		QDateTime now = QDateTime::currentDateTime();
		addOnlineValue(value, now);
	}
}

MobileHistoryEqpData::~MobileHistoryEqpData()
{
	MobilePool::getInstance().disconnectValue(this, pEqp, channel, channelValueType);

	// Remove any pending queries for history data
	DpeHistoryQuery &pQueryPool = DpeHistoryQuery::getInstance();
	QDateTime startTime(QDateTime::currentDateTime());
	QDateTime endTime(startTime);
	for(int idx = 0 ; idx < pOfflineList->count() ; idx++)
	{
		HistoryDataRange *pRange = pOfflineList->at(idx);
		startTime.setMSecsSinceEpoch(pRange->getStart());
		endTime.setMSecsSinceEpoch(pRange->getEnd());
		pQueryPool.remove(pRange->getId(), (const char *)dpeName,
			0, 0, startTime, endTime);
	}
}

/*
**	FUNCTION
**		Add all values in given time range to class that will export them
**		to Excel file
**
**	PARAMETERS
**		minTime		- Start time of interval to be exported
**		maxTime		- End time of interval to be exported
**		processor	- Processor where we shall add values
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryEqpData::addExportValues(const QDateTime &minTime, const QDateTime &maxTime, HistoryProcessor &processor)
{
	QString dpe;

	if(channel) {
		dpe = MobilePool::findBakeoutChannelDpeName(channel, channelValueType);
	}
	int idx;
	HistoryDataRange *pRange;
	qint64 minTimeAsMsec = minTime.toMSecsSinceEpoch();
	qint64 maxTimeAsMsec = maxTime.toMSecsSinceEpoch();
	for(idx = 0 ; idx < pOfflineList->count() ; idx++)
	{
		pRange = pOfflineList->at(idx);
		if(pRange->getEnd() < minTimeAsMsec)
		{
			if (dpe.isEmpty()) {
				pRange->setLastAsPreviousExportValue(pEqp, "", 0, "", minTime, processor);
			}
			else {
				pRange->setLastAsPreviousExportValue(pEqp, dpe, minTime, processor);
			}
			continue;
		}
		if(pRange->getStart() > maxTimeAsMsec)
		{
			return;
		}
		if(dpe.isEmpty())
		{
			pRange->addExportValues(pEqp, "", 0, "", minTime, maxTime, processor);
		}
		else
		{
			pRange->addExportValues(pEqp, dpe, minTime, maxTime, processor);
		}
	}
	for(idx = 0 ; idx < pOnlineList->count() ; idx++)
	{
		pRange = pOnlineList->at(idx);
		if(pRange->getStart() > maxTimeAsMsec)
		{
			return;
		}
		if(dpe.isEmpty())
		{
			pRange->addExportValues(pEqp, "", 0, "", minTime, maxTime, processor);
		}
		else
		{
			pRange->addExportValues(pEqp, dpe, minTime, maxTime, processor);
		}
	}
}

