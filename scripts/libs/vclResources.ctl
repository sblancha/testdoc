
#uses "VacCtlResourcesPvss"	// Load DLL or so file

/*
** The resources for vacuum application are handled by dedicated DLL (see #uses).
** This file declares constants used for resource processing and
** prototypes for functions in DLL.
*/

/*
** First resource file shall be read:
**
** int VacResourcesParseFile(string fileName, string machineName, dyn_string &exceptionInfo);
*/

/*
** Values for required resources can be retrieved using:
**
** anytype VacResourcesGetValue(string resourceName, int dataType, anytype defaultValue);
**
** Where dataType is one of enum values declared below:
*/

// Resource of type int
const int VAC_RESOURCE_INT = 0;

// Resource of type unsigned
const int VAC_RESOURCE_UNSIGNED = 1;

// Resource of type float
const int VAC_RESOURCE_FLOAT = 2;

// Resource of type string
const int VAC_RESOURCE_STRING = 3;

// Resource of type bool
const int VAC_RESOURCE_BOOL = 4;

// Resource of type 'equipment type mask' (result is dyn_int)
const int VAC_RESOURCE_EQP_TYPE_MASK = 5;
