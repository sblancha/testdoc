#ifndef	PROFILELHCSINGLEBEAMVIEW_H
#define	PROFILELHCSINGLEBEAMVIEW_H

// Similar to ProfileLhcView, but for special profile: by beam
#include "ProfileLhcView.h"

#include "VacEqpTypeMask.h"

class ProfileLhcSingleBeamView : public ProfileLhcView
{
	Q_OBJECT

public:
	ProfileLhcSingleBeamView(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	virtual ~ProfileLhcSingleBeamView();

	static ProfileLhcSingleBeamView *create(QWidget *parent, int vacType, MainPart *pMainPart, Sector *pFirstSector, Sector *pLastSector);

	virtual void init(const VacEqpTypeMask &beamMask);

protected:
	// Main part of this view
	MainPart *pMainPart;
};

#endif	// PROFILELHCSINGLEBEAMVIEW_H
