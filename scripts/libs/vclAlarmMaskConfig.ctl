
// Configuration and auxilliary functions for alarm mask for LASER


// Name of DP type containing alarm mask configuration
const string VAC_ALARM_CONFIG_DP_TYPE = "_VacLaserMask";

// Name of DP containing alarm mask configuration
const string VAC_ALARM_CONFIG_DP = "_VacLaserMask";

// Name of DP type containing time configuration
const string VAC_ALARM_MASK_REPORT_DP_TYPE = "_TimedFunc";

// Name of DP containing time configuration
const string VAC_ALARM_MASK_REPORT_DP = "_VacLaserMaskReport";
// Name of DP containing time configuration for releasalarm blocking
const string VAC_ALARM_MASK_RELEAS_DP = "_VacLaserMaskReleas";
// Check if DP with alarm mask config exists, if not - create such DP
void VacAlarmMaskConfigDp( string &configDpName, dyn_string &exceptionInfo )
{
	configDpName = "";
	if( dpExists( VAC_ALARM_CONFIG_DP ) )
	{
		configDpName = VAC_ALARM_CONFIG_DP;
		return;
	}
	if( dpCreate( VAC_ALARM_CONFIG_DP, VAC_ALARM_CONFIG_DP_TYPE ) < 0 )
	{
		fwException_raise(exceptionInfo, "lhcAlarmMaskConfig.ctl ERROR", "VacAlarmMaskConfigDp(): dpCreate() failed", "" );
		return;
	}
	if( ! dpExists( VAC_ALARM_CONFIG_DP ) )
	{
		fwException_raise(exceptionInfo, "lhcAlarmMaskConfig.ctl ERROR", "VacEAlarmMaskConfigDp(): config DP not created", "" );
		return;
	}
	configDpName = VAC_ALARM_CONFIG_DP;
}
void VacAlarmMaskReportDp( string &reportDpName, dyn_string &exceptionInfo )
{
	reportDpName = "";
	if( dpExists( VAC_ALARM_MASK_REPORT_DP ) )
	{
		reportDpName = VAC_ALARM_MASK_REPORT_DP;
		return;
	}
	if( dpCreate( VAC_ALARM_MASK_REPORT_DP, VAC_ALARM_MASK_REPORT_DP_TYPE ) < 0 )
	{
		fwException_raise(exceptionInfo, "lhcAlarmMaskConfig.ctl ERROR", "VacAlarmMaskReportDp(): dpCreate() failed", "" );
		return;
	}
	if( ! dpExists( VAC_ALARM_MASK_REPORT_DP ) )
	{
		fwException_raise(exceptionInfo, "lhcAlarmMaskConfig.ctl ERROR", "VacAlarmMaskReportDp(): config DP not created", "" );
		return;
	}
	reportDpName = VAC_ALARM_MASK_REPORT_DP;
}
void VacAlarmMaskReleasDp( string &releasDpName, dyn_string &exceptionInfo )
{
	releasDpName = "";
	if( dpExists( VAC_ALARM_MASK_RELEAS_DP ) )
	{
		releasDpName = VAC_ALARM_MASK_RELEAS_DP;
		return;
	}
	if( dpCreate( VAC_ALARM_MASK_RELEAS_DP, VAC_ALARM_MASK_REPORT_DP_TYPE ) < 0 )
	{
		fwException_raise(exceptionInfo, "lhcAlarmMaskConfig.ctl ERROR", "VacAlarmMaskReleasDp(): dpCreate() failed", "" );
		return;
	}
	if( ! dpExists( VAC_ALARM_MASK_RELEAS_DP ) )
	{
		fwException_raise(exceptionInfo, "lhcAlarmMaskConfig.ctl ERROR", "VacAlarmMaskReleasDp(): config DP not created", "" );
		return;
	}
	releasDpName = VAC_ALARM_MASK_RELEAS_DP;
}

