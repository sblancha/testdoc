//	Implementation of EqpMP_ACCESS class
/////////////////////////////////////////////////////////////////////////////////


#include "EqpMP_ACCESS.h"

#include "FunctionalType.h"
#include "EqpMsgCriteria.h"

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_RED			255,0,0
#define COLOR_GREEN			0,255,0

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpMP_ACCESS::EqpMP_ACCESS(MainPart *pMainPart) : Eqp()
{
	this->pMainPart = pMainPart;
	dpName = (char *)malloc(strlen(pMainPart->getPvssName()) + 1);
#ifdef Q_OS_WIN
	strcpy_s(dpName, strlen(pMainPart->getPvssName()) + 1, pMainPart->getPvssName());
#else
	strcpy(dpName, pMainPart->getPvssName());
#endif
	name = (char *)malloc(strlen(pMainPart->getName()) + 1);
#ifdef Q_OS_WIN
	strcpy_s(name, strlen(pMainPart->getName()) + 1, pMainPart->getName());
#else
	strcpy(name, pMainPart->getName());
#endif
	functionalType = FunctionalType::MP_ACCESS;
	modeOnline = modeReplay = 0;
}

EqpMP_ACCESS::~EqpMP_ACCESS()
{
	free((void *)name);
	free((void *)dpName);
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpMP_ACCESS::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "MODE");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpMP_ACCESS::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "MODE", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpMP_ACCESS::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "MODE", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpMP_ACCESS::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "MODE"))
	{
		if(value.canConvert(QVariant::Int))
		{
			int newValue = value.toInt();
			if(modeOnline == newValue)
			{
				return;
			}
			modeOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpMP_ACCESS::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "MODE"))
	{
		if(value.canConvert(QVariant::Int))
		{
			int newValue = value.toInt();
			if(modeReplay == newValue)
			{
				return;
			}
			modeReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquistion mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpMP_ACCESS::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	int myMode = mode == DataEnum::Replay ? modeReplay : modeOnline;
	if(myMode)
	{
		color.setRgb(COLOR_RED);
	}
	else
	{
		color.setRgb(COLOR_GREEN);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpMP_ACCESS::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	int myMode = mode == DataEnum::Replay ? modeReplay : modeOnline;
	if(myMode)
	{
		string = "ACCESS";
	}
	else
	{
		string = "NO Access";
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpMP_ACCESS::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	QString state;
	getMainStateString(state, mode);
	string = "Main part ";
	string += getVisibleName();
	string += ": ";
	string += state;
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in toltip
**
**	ARGUMENTS
**		string			- Variable where resulting string will be put
**		isReplayMode	- Flag indicating if state string is required for replay mode (true)
**							or for online mode (false)
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpMP_ACCESS::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	getMainStateStringForMenu(string, mode);
}
#endif

