#ifndef	VACICONVGF_STD_RI_H
#define	VACICONVGF_STD_RI_H

//	Icon for VG_STD - VGF(R+I)

#include "VacIconVG_STD.h"

class VACCTLEWOUTIL_EXPORT VacIconVGF_STD_RI : public VacIconVG_STD
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVGF_STD_RI(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVGF_STD_RI();

	// Override VacIconVG's methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setDirection(VacIconContainer::Direction newDirection);

protected:
	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter);
	virtual void drawImageVertical(QPainter &painter, int offset);
	virtual void drawImageHorizontal(QPainter &painter, int offset);

	// Dummy implementation of abstract method - never used
	virtual void drawSpecific(QPainter & /* painter */) {}
};

#endif	// VACICONVGF_STD_RI_H
