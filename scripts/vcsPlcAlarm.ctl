
main()
{
  int	count = 0;

  // Get all DPs of type VPLC
  DebugTN("vcsPlcAlarm.ctl: InitData for PLC");
  dyn_string plcDpNames = dpNames("*", "VPLC");
  for(int n = dynlen( plcDpNames ) ; n > 0 ; n--) {
    string alarmDp = c_unSystemAlarm_dpPattern + DPE_pattern + dpSubStr(plcDpNames[n], DPSUB_DP);
    if(dpExists(alarmDp)) {
      dpConnect("AlarmStateCb", alarmDp + ".alarm");
      count++;
    }
  }
  DebugTN("vcsPlcAlarm.ctl: initialization finished: " + count + " PLC(s)");
  
  count = 0;
  // Get all DPs of type V_TCP
  DebugTN("vcsPlcAlarm.ctl: InitData for SIMCARD PLC V_TCP");
  dyn_string simCardDpNames = dpNames("*", "V_FrontEnd_aliveCounter");
  for(int n = dynlen( simCardDpNames ) ; n > 0 ; n--) {
    string alarmDp = c_unSystemAlarm_dpPattern + DPE_pattern + dpSubStr(simCardDpNames[n], DPSUB_DP);
    if(dpExists(alarmDp)) {
      dpConnect("V_TCPAlarmStateCb", alarmDp + ".alarm");
      count++;
    }
  }
  DebugTN("vcsPlcAlarm.ctl: initialization finished: " + count + " SIMCARD(s)");
}
void AlarmStateCb(string alarmDpe, int state)
{
  // Find PLC device name
  string plcName = substr(dpSubStr(alarmDpe, DPSUB_DP), strlen(c_unSystemAlarm_dpPattern + DPE_pattern));
  // DebugTN("plcName:", plcName);
  string dpName = "VPLC_A_" + plcName;
  if(!dpExists(dpName)) {
    dpName = "VPLC_A0_" + plcName;
    if(!dpExists(dpName)) {
      dpName = "VPLC_PF_" + plcName;
      if(!dpExists(dpName))
      {
        DebugTN("vcsPlcAlarm.ctl: callback for bad configured PLC <" + plcName + ">");
        return;
      }
    }
  }
  dpSet(dpName + ".AL1", state != 0);
}
void V_TCPAlarmStateCb(string alarmDpe, int state){
  // Find SIMCARD device name
  string simCardCounter = substr(dpSubStr(alarmDpe, DPSUB_DP), strlen(c_unSystemAlarm_dpPattern + DPE_pattern));
  // DebugTN("simCardCounter:", simCardCounter);
  // Remove '_ALIVE_COUNTER' 14chars at the end of string
  string dpName = substr(simCardCounter, 0, strlen(simCardCounter)-14);
  if(!dpExists(dpName)) {
    DebugTN("vcsPlcAlarm.ctl: callback for bad configured SIMCARD <" + simCardCounter + ">");
    return;
  }
  dpSet(dpName + ".AL1", state != 0);
}
