//	Implementation of FunctionalTypeCRYO_TT class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeCRYO_TT.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include <stdio.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeCRYO_TT::FunctionalTypeCRYO_TT(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeCRYO_TT::fillCriteria(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Valid, "CRYO Thermometer is Valid");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is Valid");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not Valid");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"CRYO thermometer is Valid in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"CRYO thermometer is not Valid in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"CRYO thermometer is Valid in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"CRYO thermometer is not Valid in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, NominalTemp_2, "CRYO Temperature above limit (for normal T=2K)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: T = {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: T = {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"CRYO thermometer T = {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"CRYO thermometer T = {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"CRYO thermometer T = {value} in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"CRYO thermometer T = {value} in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, NominalTemp_4, "CRYO Temperature above limit (for normal T=4K)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: T = {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: T = {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"CRYO thermometer T = {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"CRYO thermometer T = {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"CRYO thermometer T = {value} in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"CRYO thermometer T = {value} in main part {mainPart}");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeCRYO_TT::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		result = "Valid";
		break;
	case EqpMsgCriteria::MainValue:
		switch(pCriteria->getSubType())
		{
		case NominalTemp_2:
			result = "NominalTemp2";
			break;
		case NominalTemp_4:
			result = "NominalTemp4";
			break;
		}
		break;
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeCRYO_TT::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "valid"))
		{
			result = Valid;
		}
		break;
	case EqpMsgCriteria::MainValue:
		if(!strcasecmp(name, "NominalTemp2"))
		{
			result = NominalTemp_2;
		}
		else if(!strcasecmp(name, "NominalTemp4"))
		{
			result = NominalTemp_4;
		}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}


