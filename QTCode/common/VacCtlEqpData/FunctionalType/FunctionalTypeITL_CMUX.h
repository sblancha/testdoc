#ifndef FUNCTIONALTYPEITL_CMUX_H
#define	FUNCTIONALTYPEITL_CMUX_H

// Special functional type for CryoMaintain interlock multiplexor - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeITL_CMUX : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Interlock = 1
	};

	FunctionalTypeITL_CMUX(int type, const char *name, const char *description);
	virtual ~FunctionalTypeITL_CMUX() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);
};

#endif	// FUNCTIONALTYPEITL_CMUX_H
