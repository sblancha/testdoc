//	Implementation of EqpMessage class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpMessage.h"
#include "ComplexCriteria.h"

#include "EqpMsgCriteria.h"
#include "EqpState.h"
#include "MessageGroup.h"

#include "Eqp.h"
#include "EqpVPI.h"
#include "Sector.h"
#include "MainPart.h"

#include "FunctionalType.h"

/*
**	FUNCTION
**		Parse given string, build instance from parsing result
**
**	ARGUMENTS
**		string	- Pointer to string to be parsed
**		errMsg	- variable where error message will be written in case of parsing error
**
**	RETURNS
**		Pointer to new instance; or
**		NULL if parsing failed
**
**	CAUTIONS
**		None
*/
EqpMessage *EqpMessage::create(const char *string, QString &errMsg)
{
	errMsg = "";
	if(!string)
	{
		errMsg = "Empty string to parse";
		return NULL;
	}
	if(!(*string))
	{
		errMsg = "Empty string to parse";
		return NULL;
	}

	QString stringPart, keyPart;
	QList<EqpMessagePart *> partList;
	bool error = false, inKey = false;
	for(const char *c = string ; *c ; c++)
	{
		switch(*c)
		{
		case '{':	// Start of key
			if(inKey)
			{
				errMsg = "'{' without closing '}'";
				error = true;
				break;
			}
			if(!stringPart.isEmpty())
			{
				EqpMessagePart *pPart = new EqpMessagePart(EqpMessagePart::MessagePartString);
				pPart->setString(stringPart);
				partList.append(pPart);
				stringPart = "";
			}
			inKey = true;
			break;
		case '}':	// end of key
			if(!inKey)
			{
				errMsg = "'}' without opening '{'";
				error = true;
				break;
			}
			{
				EqpMessagePart::MessagePartType type = EqpMessagePart::decodeType(keyPart, errMsg);
				if(type == EqpMessagePart::None)
				{
					error = true;
					break;
				}
				EqpMessagePart *pPart = new EqpMessagePart(type);
				partList.append(pPart);
				keyPart = "";
			}
			inKey = false;
			break;
		default:	// Content of string or key
			if(inKey)
			{
				keyPart += *c;
			}
			else
			{
				stringPart += *c;
			}
			break;
		}
		if(error)
		{
			break;	// Stop on 1st error
		}
	}

	// The rest of string
	if(!error)
	{
		if(inKey)
		{
			errMsg = "Unmatched '{' in string";
			error = true;
		}
		else if(!stringPart.isEmpty())
		{
			EqpMessagePart *pPart = new EqpMessagePart(EqpMessagePart::MessagePartString);
			pPart->setString(stringPart);
			partList.append(pPart);
		}
	}
	if(error)
	{
		while(!partList.isEmpty())
		{
			delete partList.takeFirst();
		}
		return NULL;
	}

	if(partList.isEmpty())
	{
		errMsg = "Empty error message";
		return NULL;
	}
	return new EqpMessage(partList);
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

EqpMessage::EqpMessage(const QList<EqpMessagePart *> &partList)
{
	pPartList = new QList<EqpMessagePart *>(partList);
}

EqpMessage::~EqpMessage()
{
	while(!pPartList->isEmpty())
	{
		delete pPartList->takeFirst();
	}
	delete pPartList;
}

/*
**	FUNCTION
**		Build message for given equipment
**
**	ARGUMENTS
**		pState				- Pointer to state of equipment
**		constantPart	- Constant part of message that does not depend
**								on device value
**		pCriteria		- Pointer to criteria of configuration
**
**	RETURNS
**		Resulting message string
**
**	CAUTIONS
**		None
*/
QString EqpMessage::buildMessage(EqpState *pState, QString &constantPart, ComplexCriteria *pCriteria)
{
	QString	result;

	// Message is built for 1st device of source
	Eqp *pEqp = pState->getFirstEqp();
	Eqp *pOtherEqp;
	constantPart = "";
	EqpMsgCriteria *pEqpCriteria;
	QDateTime now;
	foreach(EqpMessagePart *pPart, *pPartList)
	{
		switch(pPart->getType())
		{
		case EqpMessagePart::MessagePartString:
			result += pPart->getString();
			constantPart += pPart->getString();
			break;
		case EqpMessagePart::MessagePartEqpName:
			switch(pEqp->getFunctionalType())
			{
			case FunctionalType::VPI:
				pOtherEqp = ((EqpVPI *)pEqp)->getMaster();
				if(pOtherEqp)
				{
					result += pOtherEqp->getVisibleName();
					constantPart += pOtherEqp->getVisibleName();
				}
				else
				{
					result += pEqp->getVisibleName();
					constantPart += pEqp->getVisibleName();
				}
				break;
			default:
				result += pEqp->getVisibleName();
				constantPart += pEqp->getVisibleName();
			}
			break;
		case EqpMessagePart::MessagePartSectorName:
			if(pEqp->getSectorBefore())
			{
				result += pEqp->getSectorBefore()->getName();
				constantPart += pEqp->getSectorBefore()->getName();
			}
			else if(pEqp->getSectorAfter())
			{
				result += pEqp->getSectorAfter()->getName();
				constantPart += pEqp->getSectorAfter()->getName();
			}
			else
			{
				result += pEqp->getVisibleName();
				result += " in unknown sector";
				constantPart += pEqp->getVisibleName();
				constantPart += " in unknown sector";
			}
			break;
		case EqpMessagePart::MessagePartMainPartName:
			if(pEqp->getMainPart())
			{
				result += pEqp->getMainPart()->getName();
				constantPart += pEqp->getMainPart()->getName();
			}
			else
			{
				result += pEqp->getVisibleName();
				result += " in unknown main part";
				constantPart += pEqp->getVisibleName();
				constantPart += " in unknown main part";
			}
			break;
		case EqpMessagePart::MessagePartValue:
			{
				QString valueString;
				pEqp->convertValueToString(pEqp->getMainValue(DataEnum::Online), valueString);
				result += valueString;
			}
			break;
		case EqpMessagePart::MessagePartErrorText:
			{
				QString errText;
				pEqp->getErrorString(DataEnum::Online, errText);
				result += errText;
				constantPart += errText;
			}
			break;
		case EqpMessagePart::MessagePartLowLimit:
			pEqpCriteria = pCriteria->getValueCriteria();
			if(pEqpCriteria)
			{
				QString valueString;
				pEqp->convertValueToString(pEqpCriteria->getLowerLimit(), valueString);
				result += valueString;
				constantPart += valueString;
			}
			break;
		case EqpMessagePart::MessagePartHighLimit:
			pEqpCriteria = pCriteria->getValueCriteria();
			if(pEqpCriteria)
			{
				QString valueString;
				pEqp->convertValueToString(pEqpCriteria->getUpperLimit(), valueString);
				result += valueString;
				constantPart += valueString;
			}
			break;
		case EqpMessagePart::MessagePartSlaves:
			if (pEqp->getFunctionalType() == FunctionalType::VPI)
			{
				QStringList slaves;
				pOtherEqp = ((EqpVPI *)pEqp)->getMaster();
				if (pOtherEqp)
				{
					pOtherEqp->getSlaves(slaves);
					for (int i = 0; i < slaves.size() - 1; i++)
					{
						result += slaves.at(i) + ", ";
						constantPart += slaves.at(i) + ", ";
					}
					result += slaves.last();
					constantPart += slaves.last();
				}
				else
				{
					result += "";
					constantPart += "";
				}

			}
			else
			{
				result += "type does not have slaves";
				constantPart += "type does not have slaves";
			}
			break;
		case EqpMessagePart::MessagePartEventTime:
			result += pCriteria->getEventTime().toString("dd-MM-yyyy hh:mm:ss");
			// constantPart += pState->getEventTime().toString("dd-MM-yyyy hh:mm:ss");
			break;
		case EqpMessagePart::MessagePartSendTime:
			now = QDateTime::currentDateTime();
			result += now.toString("dd-MM-yyyy hh:mm:ss");
			// constantPart += now.toString("dd-MM-yyyy hh:mm:ss");
			break;
		case EqpMessagePart::MessagePartEventValue:
			{
				QString valueString;
				pEqp->convertValueToString(pCriteria->getEventValue(), valueString);
				result += valueString;
			}
			break;
		case EqpMessagePart::MessageTypeMinDuration:
			{
				result += QString::number(pCriteria->getMinDuration());
			}
			break;
		default:
			break;
		}
	}
	return result;
}

/*
**	FUNCTION
**		Build message to replace given group of messages
**
**	ARGUMENTS
**		pGroup	- Pointer to group for which message is built
**
**	RETURNS
**		Resulting message string
**
**	CAUTIONS
**		None
*/
QString EqpMessage::buildMessage(MessageGroup *pGroup)
{
	QString	result;
	foreach(EqpMessagePart *pPart, *pPartList)
	{
		switch(pPart->getType())
		{
		case EqpMessagePart::MessagePartString:
			result += pPart->getString();
			break;
		case EqpMessagePart::MessagePartGroupCount:
			{
				result += QString::number(pGroup->getMessages().count());
			}
			break;
		case EqpMessagePart::MessagePartSectorName:
			if(pGroup->getSector())
			{
				result += pGroup->getSector()->getName();
			}
			else
			{
				result += "unknown sector";
			}
			break;
		case EqpMessagePart::MessagePartMainPartName:
			if(pGroup->getMainPart())
			{
				result += pGroup->getMainPart()->getName();
			}
			else
			{
				result += "unknown main part";
			}
			break;
		case EqpMessagePart::MessagePartEventTime:
			result += pGroup->getCreateTime().toString("dd-MM-yyyy hh:mm:ss");
			break;
		default:
			break;
		}
	}
	return result;
}

