#ifndef	VACICONVGP_H
#define	VACICONVGP_H

//	Icon for VGP

#include "VacIconVG.h"

class VACCTLEWOUTIL_EXPORT VacIconVGP : public VacIconVG
{
public:
	VacIconVGP(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG(parent, f) {}

	virtual bool connect(void);

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGP_H
