#
# Project file for qmake utility to build Makefile for simple VacImport test
#
#	03.10.2008	L.Kopylov
#		Initial version

TEMPLATE = app

TARGET = testImport

INCLUDEPATH += .. \
				../../VacAux


unix:LIBS += -L../../../bin -lVacImport -l VacAux

CONFIG += qt thread release warn_on

DEFINES += _VACDEBUG

unix:SOURCES = testImport.cpp




