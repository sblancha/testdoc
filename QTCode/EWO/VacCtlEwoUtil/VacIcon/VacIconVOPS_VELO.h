#ifndef	VACICONVOPS_VELO_H
#define	VACICONVOPS_VELO_H

// Icon for VOPS_VELO - VELO overpressure switch

#include "VacIcon.h"

class EqpVOPS_VELO;

class VACCTLEWOUTIL_EXPORT VacIconVOPS_VELO : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVOPS_VELO(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVOPS_VELO();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

	// Override method of VacIcon 
	virtual void setDirection(VacIconContainer::Direction newDirection);

protected:
	// Pointer to my device
	EqpVOPS_VELO *pEqp;

	// Areas for pump and valve symbols. Drawing can be easy using QPainter
	// coordinate transformations, but finding areas for tooltips is not so
	// evident. May be it is more convenient just to keep in meory coordinates
	// of all distinct areas

	QRect	a;
	QRect	b;

	void setAreaGeometry(void);

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawA(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
	virtual void drawB(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
	
	void setPainterForA(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
	void setPainterForB(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
};

#endif	// VACICONVOPS_VELO_H
