// Access mode control per main part

const int ACCESS_MODE_ON = 1;
const int ACCESS_MODE_OFF = 0;

/** Get attributes for access mode
Usage: LHC Vacuum control internal
PVSS manager usage: VISION

  @param mainPart: in, main part
  @param dpName: out, readable dpName
  @param dpeName: out, readable dpeName
  @return None.
  @author SME
*/
void LhcVacGetDpeForAccessMode(string mainPart, string &dpName, string &dpeName)
{
  LhcVacPvssNameOfMainPart(mainPart, dpName);
  dpeName = dpName + ".MODE";
}

// LhcVacGetMainPartForAccessDp
/** Find name of main part controlled by given access mode DP
Usage: LHC Vacuum control internal
PVSS manager usage: VISION

  @param dpName: in, DP name for access control
  @param mainPart: out, Name of main part
  @param exceptionInfo: out, eroor message
  @return None.
  @author LIK
*/
void LhcVacGetMainPartForAccessDp(const string dpName, string &mainPart, dyn_string &exceptionInfo)
{
  mainPart = "";
  dyn_string mainParts;
  LhcVacGetAllMainParts(mainParts);
  for(int n = dynlen(mainParts) ; n > 0 ; n--)
  {
    string accessDpName, accessDpeName;
    LhcVacGetDpeForAccessMode(mainParts[n], accessDpName, accessDpeName);
    if(accessDpName == dpName)
    {
      mainPart = mainParts[n];
      return;
    }
  }
  fwException_raise(exceptionInfo, 
    "ERROR", "LhcVacGetMainPartForAccessDp(): no main part for <" + dpName +
    ">" , "");
}

// LhcVacGetDeviceForAccessMode
/** Get devices affected by access mode
Usage: LHC Vacuum control internal
PVSS manager usage: VISION

  @param dpName: in, DP name for access control
  @param dpList: out, dpNames of valves
  @param exceptionInfo: out, eroor message
  @return None.
  @author SME
*/
void LhcVacGetDeviceForAccessMode(string dpName, dyn_string &dpList, dyn_string &exceptionInfo)
{
  // First find main part name corresponding to DP name
  string mainPart;
  LhcVacGetMainPartForAccessDp(dpName, mainPart, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }

  // Next find all valves in this main part
  dyn_string devDpNames;
  LhcVacGetDevicesAtMainParts(makeDynString(mainPart), devDpNames, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("LhcVacGetDeviceForAccessMode: LhcVacGetDevicesAtMainParts(" +
      mainPart + ") failed: " + exceptionInfo);
    return;
  }
  int nDpNames = dynlen(devDpNames);
  for(int n = 1 ; n <= nDpNames ; n++)
  {
    string dpType = dpTypeName(devDpNames[n]);
    switch(dpType)
    {
    case "":
    case "VLV_C0":
      break;
    default:
      {
        int group = LhcVacDpTypeGroup(dpType);
        if(group == VAC_DP_TYPE_GROUP_VV)
        {
          string eqpName = devDpNames[n];
          dynAppend(dpList, eqpName);
        }
      }
      break;
    }
  }
}

/** Performs actions for access mode
Usage: LHC Vacuum control internal
PVSS manager usage: VISION

  @param dpName: in, dpName for action
  @param action: in, action to be performed
  @param exceptionInfo: out, eroor message
  @return None.
  @author SME
*/
void LhcVacDeviceAction_ACCESS_MODE(string dpName, dyn_anytype action, dyn_string &exceptionInfo)
{
  unsigned       newWr1;

  switch(action[1] & LVA_ACTION_MASK)
  {
  case LVA_ACCESS_MODE_ON:
    newWr1 = ACCESS_MODE_ON;
    break;
  case LVA_ACCESS_MODE_OFF:
    newWr1 = ACCESS_MODE_OFF;
    break;
  }

  // Write for access mode itself
  dpSetWait(dpName + ".MODE", newWr1);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_ACCESS_MODE(): dpSetWait() failed:" + err , "" );
    return;
  }

  // That's all for removing access mode
  if((action[1] & LVA_ACTION_MASK) == LVA_ACCESS_MODE_OFF)
  {
    return;
  }

  // For setting access mode we shall close all valves in main part
  dyn_string valveDpList;
  LhcVacGetDeviceForAccessMode(dpName, valveDpList, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }

  dyn_anytype valveAction = makeDynAnytype(LVA_DEVICE_MASK | LVA_DEVICE_CLOSE);
  for(int n = dynlen(valveDpList); n > 0; n--)
  {
    string dpType;
    LhcVacDpType(valveDpList[n], dpType, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("LhcVacDeviceAction_ACCESS_MODE: LhcVacDpType(" + valveDpList[n] +
        ") failed: " + exceptionInfo);
      continue;
    }
    LhcVacExecuteAction(valveDpList[n], dpType, valveAction, exceptionInfo);
  }
}

/**
  Check if access mode is set for main part where device is located
Usage: LHC Vacuum control internal
PVSS manager usage: VISION

  @param dpName: in, dpName of device
  @param accessMode: out, access mode for device
  @param exceptionInfo: out, eroor message
  @return None.
  @author SME
*/
void LhcVacCheckAccessMode(string dpName, int &accessMode, dyn_string &exceptionInfo)
{
  string   mainPart, sector1, sector2;
  bool	   isBorder;
  int	   devVacuum;

  accessMode = false;	
  LhcVacDeviceVacLocation(dpName, devVacuum, sector1, sector2, mainPart, isBorder, exceptionInfo);
  if(mainPart == "")
  {
    return;
  }
  string dpName, dpeName;
  LhcVacGetDpeForAccessMode(mainPart, dpName, dpeName);
  if(!dpExists(dpeName))
  {
    return;
  }
  dpGet(dpeName, accessMode);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacCheckAccessMode(): Read MODE for <" + mainPart + "> failed: " + err, "" );
  }
}
