//	Implementation of VacDpData class
/////////////////////////////////////////////////////////////////////////////////

#include "SupPrint.h"


#include <qpainter.h>
#include <qprinter.h>
#include <qprintdialog.h>
#include <qstatusbar.h>
// LIK #include <qstylesheet.h>
#include <qfontmetrics.h>
#include <qfile.h>
#include <qrect.h>
#include <QTextStream>


/////////////////////////////////////////////////////////////////////////////////
/////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

SupPrint::SupPrint(QWidget *parent):
	QPushButton(parent), printer(QPrinter::ScreenResolution), fontName("Courier")
{
	fontSize = 8;
	connect(this, SIGNAL(clicked(void)), this, SLOT(buttonClick(void)));
}

SupPrint::~SupPrint()
{
}

/*
**	FUNCTION
**		Print text
**
**	ARGUMENTS
**		textToPrint	- plain text
**
**	RETURNS
**		0	= success,
**		-1	= failure
**
**	CAUTIONS
**		None
*/
int SupPrint::printText(const char *strToPrint)
{
	// printer.setOutputFileName("print.ps");
	QPrintDialog dialog(&printer, this);
	if(dialog.exec() != QDialog::Accepted)
	{
		return 0;
	}
	QPainter painter;
	QFont font(fontName, fontSize);
	QFontMetrics fm(font);
	// LIK 13.05.2011 - not used int fontHight = fm.height();
	painter.begin(&printer);
	QPaintDevice *pDevice = painter.device();
	int dpiy = pDevice->logicalDpiY();
	int margin =(int)((2/2.54)*dpiy); // 2 cm margins
	// LIK 13.05.2011 - not used int nRow =(metrics.height()- 2*margin)/fontHight;
	// printf("NumberOfRowPerPage: %d\n", nRow);
	QRect view(margin, margin, pDevice->width()- 2*margin, pDevice->height()- 2*margin);
	QTextDocument richText;
	richText.setHtml(strToPrint);
	richText.print(&printer);
//   painter.end();
	return 1;
}
/*
**	FUNCTION
**		Slot receiving 'clicked' signal of button. Open dialog for
**		selecting print options. 
**
**	ARGUMENTS
**		None
**
**	RETURNS
*		None
**
**	CAUTIONS
**		None
*/
void SupPrint::buttonClick(void)
{
   // printer.setOutputFileName("VacDevStateHist.ps");
   printer.setFullPage(true);

   /*
   QPrintDialog *dialog = new QPrintDialog(&printer, this);
   if(dialog->exec()== QDialog::Accepted)
		emit printRequest();
	*/
	QPrintDialog dialog(&printer, this);
	if(dialog.exec() == QDialog::Accepted)
	{
	   emit printRequest();
   }
}

/*
**	FUNCTION
**		Print text file
**
**	ARGUMENTS
**		fileName	- file name
**
**	RETURNS
**		0	= success,
**		-1	= failure
**
**	CAUTIONS
**		None
*/
int SupPrint::printFile(const char *fileName)
{
printf("SupPrint::printFile(%s)\n", fileName);
fflush(stdout);
	QFile file(fileName);
	QStringList lines;
	QStringList combLines;
	QString templ("<< Page Break >>");
	if(file.open(QIODevice::ReadOnly))
	{
printf("SupPrint::printFile(): file.open() - OK\n");
fflush(stdout);
		QTextStream stream(&file);
		QString line;
		while(!stream.atEnd())
		{
			line = stream.readLine(); // line of text excluding '\n'
			int res = line.compare(templ);
			if(res != 0)
			{
				lines += line;
			}
			else	
			{
				/*
				QString tmpS = lines.join(QString("\n"));
				combLines += tmpS;
				lines.clear();
				*/
			}
		}
		file.close();
	}
printf("SupPrint::printFile(): file read: %d lines\n", lines.count());
fflush(stdout);
	QTextDocument textDoc;
	textDoc.setHtml(lines.join(QString("\n")));
	textDoc.print(&printer);
	return 1;
}
const int LargeGap = 48; 

/*
**	FUNCTION
**		Print text file
**
**	ARGUMENTS
**		fileName	- file name
**
**	RETURNS
**		0	= success,
**		-1	= failure
**
**	CAUTIONS
**		None
*/
int SupPrint::printDialog(void)
{
	if(printer.orientation()== QPrinter::Portrait)
	{
		prOrient = 0;
	}
	else
	{
		prOrient = 1;
	}
	return prOrient;
}
