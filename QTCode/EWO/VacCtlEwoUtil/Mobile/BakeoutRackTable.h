#ifndef BAKEOUTRACKTABLE_H
#define	BAKEOUTRACKTABLE_H

// Table for displaying details of bakeout rack

#include "BakeoutRackChannel.h"
#include "BakeoutStateConnection.h"

#include <qtablewidget.h>
#include <qlist.h>

class BakeoutRackTable : public QTableWidget
{
	Q_OBJECT

public:
	BakeoutRackTable(QWidget *parent);
	~BakeoutRackTable();

	void setEqp(Eqp *pEqp);
	inline Eqp *getEqp(void) const { return pEqp; }
	inline unsigned getRr1(void) const { return state.getRr1(); }
	inline bool isPlcAlarm(void) const { return state.isPlcAlarm(); }

	virtual QSize sizeHint(void) const { return recommendedSize; }

private slots:
	void eqpDataChange(BakeoutRackChannel *pSrc, bool colorChanged);
	void stateChange(void);

signals:
	void stateChanged(void);

protected:
	// Relative widths for all columns
	static float	colWidthCoeff[6];

	// Flag indicating if relative coulmn widths have been initialized from resources
	static bool	colWidthReady;

	// Pointer to device shown in profile
	Eqp			*pEqp;

	// Connection to bakeout rack state (RR1 and PLC)
	BakeoutStateConnection		state;

	// List of all channels in profile
	QList<BakeoutRackChannel *>	channels;

	virtual void resizeEvent(QResizeEvent *event);

	// Size hint
	QSize	recommendedSize;
};

#endif	// BAKEOUTRACKTABLE_H
