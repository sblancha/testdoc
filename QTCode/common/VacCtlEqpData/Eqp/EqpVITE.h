#ifndef	EQPVITE_H
#define	EQPVITE_H

//	VITE - temperature of thermocouple

#include "Eqp.h"
#include "EqpVRJ_TC.h"	// strongly depends on VRJ_TC

class VACCTLEQPDATA_EXPORT EqpVITE : public Eqp
{
	Q_OBJECT

public:
	EqpVITE(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVITE(const EqpVITE &src) : Eqp(src) {}
#endif
	~EqpVITE();

	virtual void postProcess(void);

	virtual void getMaster(QString &masterDp, int &masterChannel);
	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void setSelected(bool selected, bool isFromPvss);
	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::Temperature; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode mode);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
	virtual void getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end);
	virtual float getGrowFactorForDevList(void);
	virtual int getSpikeFilterForDevList(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);


	// Access for essential values
	inline Eqp *getMaster(void) { return (Eqp *)pMaster; }
	bool isPlcAlarm(DataEnum::DataMode mode) const;
	inline float getTemperature(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? temperatureReplay : temperatureOnline; }

	// In fact, these values come from master
	unsigned getRR1(DataEnum::DataMode mode) const;

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);
	virtual void selectChange(Eqp * /* pScr */) {}

	// VITE shall react to selection (via it's master - VRJ_TC). Normally connection
	// to signal of VRJ_TC is done in connectDpe() method (of VRJ_TC in this case).
	// However, disconnect causes also disconnect from selection signal.
	// Even if second ('permanent') connection to selection signal is done -
	// disconnect() disconnects both (including 'permanent' connection).
	// This was discovered in pressure history of VPI where PR is connected, but
	// RR1 is not.
	// In order to solve this problem two major changes were introduced:
	// 1) VITE has it's own NEW slot for processing selection signals from VRJ_TC
	//		(see below)
	// 2) New virtual method postProcess() has been added to Eqp class, the
	//		purpose is to do what particular device needs AFTER all devices
	//		have been read from file. This allows to open VITE history without
	//		first opening VITE 'details', including VRJ_TC state

	virtual void masterSelectChange(Eqp *pSrc);

protected:
	// Pointer to VRPM controlling this VIES
	EqpVRJ_TC	*pMaster;

	// Number of connect requests to this Eqp - in order to decide if
	// we need to connect to/disconnect from master (VRJ_TC)
	int		nOnlineConnects;
	int		nReplayConnects;
	int		nPollingConnects;
	
	// Last known value of RmA DPE for online mode
	float	temperatureOnline;

	// Last known value of RmA DPE for replay mode
	float	temperatureReplay;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool	plcAlarmOnline;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool	plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainValueString(unsigned rr1, float temperature, bool plcAlarm, QString &string);
	virtual void getMainStateStringForMenu(unsigned rr1, float temperature, bool plcAlarm, QString &string);
	virtual void getMainValueStringForMenu(unsigned rr1, float temperature, bool plcAlarm, QString &string);
#endif
};

#endif	// EQPVITE_H
