//	Implementation of SectLhcIbjDumpLine class
/////////////////////////////////////////////////////////////////////////////////

#include "SectLhcInjDumpLine.h"

#include "DataPool.h"

#include "VacEqpTypeMask.h"

/*
** FUNCTION
**		Create new instance of SectLhcInjDumpLine for beam line and return it.
**		Note that new instance has only limits equipment selection, NOT equipment
**		itself - equipment and geometry will be built by method Build().
**
** PARAMETERS
**		pLine			- Pointer to beam line for which synoptic line shall be built
**		parentVacType	- Vacuum type of parent line
**
** RETURNS
**		Pointer to new instance of CSynLhcInjDumpLine if given beam line shall be at least
**			partially presented in synoptic;
**		NULL if given beam line shall not be presented in synoptic
**
** CAUTIONS
**		None
*/
SectLhcInjDumpLine *SectLhcInjDumpLine::create(QWidget *parent, BeamLine *pLine, int parentVacType)
{
	bool	found = false;

	SectLhcInjDumpLine *pSectLine = new SectLhcInjDumpLine();
	pSectLine->pBeamLine = pLine;
	QList<BeamLinePart *> &partList = pLine->getParts();
	BeamLinePart *pPart;
	int idx;
	for(idx = 0 ; idx < partList.count() ; idx++)
	{
		pPart = partList.at(idx);
		if(pPart->getNEqpPtrs())
		{
			found = true;
			pSectLine->pStartPart = pPart;
			pSectLine->startEqpIdx = 0;
			break;
		}
	}
	for(idx = partList.count() - 1 ; idx >= 0 ; idx--)
	{
		pPart = partList.at(idx);
		if(pPart->getNEqpPtrs())
		{
			pSectLine->pEndPart = pPart;
			pSectLine->endEqpIdx = pPart->getNEqpPtrs() - 1;
			break;
		}
	}
	if(!found)
	{
		delete pSectLine;
		return NULL;
	}
	DataPool &pool = DataPool::getInstance();
	pSectLine->metersToScreen = 1.0 / pool.getMinPassiveEqpVisibleWidth();
	pSectLine->vacType = parentVacType;
	pSectLine->parent = parent;
	unsigned allVacTypeMask;
	VacEqpTypeMask cryoEqpMask, beamEqpMask;
	pSectLine->analyzeTypes(allVacTypeMask, cryoEqpMask, beamEqpMask);
	return pSectLine;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SectLhcInjDumpLine::SectLhcInjDumpLine()
{
	reverse = false;
}

SectLhcInjDumpLine::~SectLhcInjDumpLine()
{
}

void SectLhcInjDumpLine::checkForReverse(void)
{
	reverse = false;
	if(pBeamLine->getStartLine ())
	{
		switch(pBeamLine->getStartType())
		{
		case BeamLineConnType::LeftIn:
		case BeamLineConnType::RightIn:
			reverse = true;
			break;
		}
	}
	if(pBeamLine->getEndLine())
	{
		switch(pBeamLine->getEndType())
		{
		case BeamLineConnType::LeftOut:
		case BeamLineConnType::RightOut:
			reverse = true;
			break;
		}
	}
}
