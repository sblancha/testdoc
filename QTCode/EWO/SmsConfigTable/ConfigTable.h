#ifndef	CONFIGTABLE_H
#define	CONFIGTABLE_H

// Table for SMS configurations - base widget for corresponding EWO

#include "ConfigTableModel.h"
#include "GroupConfig.h"

#include <QTableView>

class ConfigTable : public QTableView
{
	Q_OBJECT

public:
	ConfigTable(QWidget *parent = 0);
	~ConfigTable();

	const QString getSelectedConfig(void);

	// Access
	ConfigTableModel *getModel(void) { return &model; }

protected:
	// Model
	ConfigTableModel	model;
	void resizeEvent(QResizeEvent *pEvent);
};

#endif	// CONFIGTABLE_H
