#
# Project file for qmake utility to build SmsConfigTable EWO
#
#	29.09.2011	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = SmsConfigTable

INCLUDEPATH += ./ \
	../../common/VacCtlUtil \
	../../common/Platform \
	../VacCtlEwoUtil

HEADERS =	\
	SmsConfig.h \
	GroupConfig.h \
	ConfigTableModel.h \
	ConfigTable.h \
	SmsConfigTable.h

SOURCES =	\
	SmsConfig.cpp \
	GroupConfig.cpp \
	ConfigTableModel.cpp \
	ConfigTable.cpp \
	SmsConfigTable.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlUtil

