#
# Project file for qmake utility to build MainLHC EWO
#
#	24.09.2010	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = ProfileAllLhcBeamEwo

INCLUDEPATH += ../VacCtlEwoUtil \
	../VacCtlEwoUtil/ProfileAllLHCBeam \
	../VacCtlEwoUtil/ProfileLhc \
	../VacCtlEwoUtil/Axis \
	../../common/VacCtlUtil \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/Platform \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp

HEADERS = ProfileAllLhcBeamEwo.h

SOURCES = ProfileAllLhcBeamEwo.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlHistoryData \
	-lVacCtlEqpData \
	-lVacCtlUtil

