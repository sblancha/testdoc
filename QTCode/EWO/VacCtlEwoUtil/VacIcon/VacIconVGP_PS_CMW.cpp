//	Implementation of VacIconVGP_PS_CMW class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGP_PS_CMW.h"

#include <qpainter.h>

// We only need to implement drawSpecific() method

// Draw 'Penning' symbol in the center of icon
void VacIconVGP_PS_CMW::drawSpecific(QPainter &painter)
{
	QPen blackPen(Qt::black, 2);
	painter.setPen(blackPen);
	painter.drawLine(10, 8, 12, 8);
	painter.setPen(Qt::black);
	QPolygon points(4);
	points.setPoint(0, 5, 8);
	points.setPoint(1, 5, 13);
	points.setPoint(2, 15, 13);
	points.setPoint(3, 15, 8);
	painter.drawPolyline(points);
}
