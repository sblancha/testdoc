# Project file for small test application for LHC main view EWO
#
#	08.09.2008	L.Kopylov
#		Initial version
#

TEMPLATE = app

TARGET = LhcMainViewer

INCLUDEPATH += .. ../../../VacAux

unix:LIBS += -L../../../../bin -L../ -lMainLHC

CONFIG += qt thread release warn_on

DEFINES += _VACDEBUG


unix:HEADERS = LhcMainViewer.h

unix:SOURCES = LhcMainViewer.cpp\
				main.cpp
