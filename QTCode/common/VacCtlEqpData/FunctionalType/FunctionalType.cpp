//	Implementation of FunctionalType class
///////////////////////////////////////////////////////////////////////////////////////

#include "FunctionalType.h"
#include "StdEqpMessage.h"

#include "Eqp.h"

// Specific functional types with their own criteria
#include "EqpMsgCriteria.h"
#include "FunctionalTypeVA.h"
#include "FunctionalTypeVV.h"
#include "FunctionalTypeVG.h"
#include "FunctionalTypeVPI.h"
#include "FunctionalTypeVPG.h"
#include "FunctionalTypeVRE.h"
#include "FunctionalTypeVPGF.h"
#include "FunctionalTypeVP_STDIO.h"
#include "FunctionalTypeVPC_HCCC.h"
#include "FunctionalTypeVPGM.h"
#include "FunctionalTypeVACOK.h"
#include "FunctionalTypeCRYO_TT.h"
#include "FunctionalTypeVV_ANA.h"
#include "FunctionalTypePLC.h"
#include "FunctionalTypePROCESS_VPG_6A01.h"
#include "FunctionalTypePROCESS_BGI_6B01.h"
#include "FunctionalTypePROCESS_VPG_6E01.h"
#include "FunctionalTypeVRJ_TC.h"
#include "FunctionalTypeVITE.h"
#include "FunctionalTypeV8DI_FE.h"
#include "FunctionalTypeALARM_DO.h"
#include "FunctionalTypeVBAKEOUT.h"
#include "FunctionalTypeINJ_6B02.h"
#include "FunctionalTypeVP_NEG.h"
#include "FunctionalTypeVPNMUX.h"
#include "FunctionalTypeSMS_MACHINE_MODE.h"
#include "FunctionalTypeVPS.h"
#include "FunctionalTypeVPS_Process.h"
#include "FunctionalTypeITL_CMNT.h"
#include "FunctionalTypeITL_CMUX.h"
#include "FunctionalTypeVELO_Process.h"
#include <stdio.h>

QList<PassiveEqpTypeWc *>	*FunctionalType::pWcs = new QList<PassiveEqpTypeWc *>();
QList<FunctionalType *>		*FunctionalType::pTypes = new QList<FunctionalType *>();
QMutex						FunctionalType::mutex;

void FunctionalType::clear(void)
{
	while(!pWcs->isEmpty())
	{
		delete pWcs->takeFirst();
	}
}

void FunctionalType::addType(int type, const char *wildCard)
{
	pWcs->append(new PassiveEqpTypeWc(wildCard, type));
}

/*
**	FUNCTION
**		Find type of passive equipment from it's name
**
**	PARAMETERS
**		eqpName	- Name of passive equipment
**
**	RETURNS
**		Type of equipment
**
**	CAUTIONS
**		None
*/
int FunctionalType::findPassiveType(const char *eqpName)
{
	for(int idx = 0 ; idx < pWcs->count() ; idx++)
	{
		PassiveEqpTypeWc *pWc = pWcs->at(idx);
		if(nameMatchesWildCard(eqpName, pWc->wc.toLatin1()))
		{
			return pWc->type;
		}
	}
	return None;
}

/*
**	FUNCTION
**		Build (if empty) and return list of functional types
**
**	PARAMETERS
**		None
**
**	RETURNS
**		List of functional types
**
**	CAUTIONS
**		None
*/
QList<FunctionalType *> &FunctionalType::getTypes(void)
{
	mutex.lock();
	if(pTypes->isEmpty())
	{
		buildTypes();
	}
	mutex.unlock();
	return *pTypes;
}

/*
**	FUNCTION
**		Find reference to functional type with given ID
**
**	PARAMETERS
**		type	- ID of type to find
**
**	RETURNS
**		Pointer to functional type with given ID
**
**	CAUTIONS
**		In principle NULL shall be rturned, but this shall not happen
*/
FunctionalType *FunctionalType::findTypeData(int type)
{
	if(pTypes->isEmpty())
	{
		getTypes();
	}
	if(type == None)
	{
		return NULL;
	}
	for(int idx = 0 ; idx < pTypes->count() ; idx++)
	{
		FunctionalType *pType = pTypes->at(idx);
		if(pType->type == type)
		{
			return pType;
		}
	}
	printf("Failed to find functional type %d\n", type);
	fflush(stdout);
	Q_ASSERT(false);
	return NULL;
}

/*
**	FUNCTION
**		Find list of devices with given functional type, 'related' to given
**		device of this functional type. In generic class -just dummy function
**		that returns nothing
**
**	PARAMETERS
**		pEqp		- Pointer to device of this functional type
**		otherType	- ID of other functional type - related devices of that type
**						are required
**		relatedList	- List where all found related devices shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalType::findRelatedEqp(Eqp * /* pEqp */, int /* otherType */, QList<Eqp *> &relatedList)
{
	relatedList.clear();
}

/*
**	FUNCTION
**		Fill in static list of functional types
**
**	PARAMETERS
**		None
**
**	RETURNS
**		List of functional types
**
**	CAUTIONS
**		None
*/
void FunctionalType::buildTypes(void)
{
	while(!pTypes->isEmpty())
	{
		delete pTypes->takeFirst();
	}

	// Passive equipment
	pTypes->append(new FunctionalType(ColdDipole, "ColdDipole", "Cold dipole"));
	pTypes->append(new FunctionalType(ColdQuadrupole, "ColdQuadrupole", "Cold quadrupole"));
	pTypes->append(new FunctionalType(WarmDipole, "WarmDipole", "Warm dipole"));
	pTypes->append(new FunctionalType(WarmQuadrupole, "WarmQuadrupole", "Warm quadrupole"));
	pTypes->append(new FunctionalType(TDICollimator, "TDICollimator", "TDI collimator"));

	// Regular active equipment
	pTypes->append(new FunctionalTypeVPG(VA, "VA", "Vacuum Alarms"));
	pTypes->append(new FunctionalTypeVPG(VPG, "VPG", "Pumping Groups Unified(mobile and fixe)"));
	pTypes->append(new FunctionalTypeVV(VV, "VV", "Valves"));
	pTypes->append(new FunctionalTypeVG(VGM, "VGM", "Membrane gauges"));
	pTypes->append(new FunctionalTypeVG(VGR, "VGR", "Pirani gauges"));
	pTypes->append(new FunctionalTypeVG(VGP, "VGP", "Penning gauges"));
	pTypes->append(new FunctionalTypeVG(VGI, "VGI", "Ionization gauges"));
	pTypes->append(new FunctionalTypeVG(VGF, "VGF", "Full range gauges"));
	pTypes->append(new FunctionalTypeVPI(VPI, "VPI", "Ion pumps(Wimko)"));
	pTypes->append(new FunctionalType(VRPI, "VRPI", "Ion pump controllers(WimKo)"));
	pTypes->append(new FunctionalTypeVPGF(VPGF, "VPGF", "Legacy pumping groups(GIS)"));
	pTypes->append(new FunctionalTypeVP_STDIO(VP_STDIO, "VP_STDIO", "Pump standard IO"));
	pTypes->append(new FunctionalTypeVPC_HCCC(VPC_HCCC, "VPC_HCCC", "Cryo pump HRS HCC Controller"));
	pTypes->append(new FunctionalTypeVPGM(VPGM, "VPGM", "Mobile pumping groups(Profibus mobile)"));
	pTypes->append(new FunctionalTypeVG(VPGMPR, "VPGMPR", "Gauges of pumping groups(Profibus mobile)"));
	pTypes->append(new FunctionalTypeVBAKEOUT(VBAKEOUT, "VBAKEOUT", "Bake-out racks(Profibus mobile)"));
	pTypes->append(new FunctionalTypeVACOK(VACOK, "VACOK", "Vacuum CRYO alarms"));
	pTypes->append(new FunctionalType(EXP_AREA, "EXP_AREA", "Experimental areas"));
	pTypes->append(new FunctionalType(COLDEX, "COLDEX", "COLDEX"));
	pTypes->append(new FunctionalType(ACCESS_MODE, "ACCESS_MODE", "Access modes of main partss"));
	pTypes->append(new FunctionalType(AUX_EQP, "AUX_EQP", "Auxilliary equipment"));
	pTypes->append(new FunctionalTypeCRYO_TT(CRYO_TT, "CRYO_TT", "Main CRYO thermometers"));
	pTypes->append(new FunctionalTypeCRYO_TT(CRYO_TT_EXT, "CRYO_TT_EXT", "Extra CRYO thermometers"));
	pTypes->append(new FunctionalTypeVV_ANA(VV_ANA, "VV_ANA", "Analog valves"));
	pTypes->append(new FunctionalTypeVG(VGTR, "VGTR", "Pressure transmitters"));
	pTypes->append(new FunctionalType(BEAM_INT, "BEAM_INT", "Beam intensity"));
	pTypes->append(new FunctionalType(CRYO_TT_SUM, "CRYO_TT_SUM", "Summary CRYO thermometers"));
	pTypes->append(new FunctionalType(VPT100, "VPT100", "PT100 thermometers"));
	pTypes->append(new FunctionalType(VRPM, "VRPM", "Power Supply for Solenoids"));
	pTypes->append(new FunctionalType(VBEAM_PARAM_FLOAT, "VBEAM_PARAM_FLOAT", "Beam Parameters of type float"));
	pTypes->append(new FunctionalTypeVV(VV_PLUS_PP, "VV+PP", "Valve with PP"));
	pTypes->append(new FunctionalType(VBEAM_PARAM_INT, "VBEAM_PARAM_INT", "Beam Parameters of type int"));
	pTypes->append(new FunctionalType(VIES, "VIES", "Solenoids"));
	pTypes->append(new FunctionalTypePROCESS_VPG_6A01(PROCESS_VPG_6A01, "PROCESS_VPG_6A01", "Pumping groups Type 6A01(Standard LHC)"));
	pTypes->append(new FunctionalTypePROCESS_BGI_6B01(PROCESS_BGI_6B01, "PROCESS_BGI_6B01", "Gaz injection Type 6B01(BGI, BGV...)"));
	pTypes->append(new FunctionalTypePROCESS_VPG_6E01(PROCESS_VPG_6E01, "PROCESS_VPG_6E01", "Pumping groups Type 6E01(BGI, BGV...)"));
	pTypes->append(new FunctionalTypeINJ_6B02(INJ_6B02, "GAS_INJ_6B02", "Gas Injection type 6B02"));
	pTypes->append(new FunctionalTypeVRJ_TC(VRJ_TC, "VRJ_TC", "Thermocouples Remote IO"));
	pTypes->append(new FunctionalTypeVITE(VITE, "VITE", "Thermocouple Temperature"));
	pTypes->append(new FunctionalTypeVPS(VPS, "VPS", "Sublimation Pumps"));
	pTypes->append(new FunctionalTypeVPS_Process(VPS_Process, "VPS_Process", "Master Process for Sublimation Pumps"));
	pTypes->append(new FunctionalTypeV8DI_FE(V8DI_FE, "V8DI_FE", "Digital Input monitor for interlock signals"));
	pTypes->append(new FunctionalTypeALARM_DO(ALARM_DO, "ALARM_DO", "DO Alarm generator"));
	pTypes->append(new FunctionalType(EXT_ALARM, "EXT_ALARM", "External interlock/alarm target"));
	pTypes->append(new FunctionalType(VPN, "VPN", "NEG pump"));
	pTypes->append(new FunctionalTypeVP_NEG(VP_NEG, "VP_NEG", "Control for chain of NEG pumps"));
	pTypes->append(new FunctionalTypeVPNMUX(VPNMUX, "VPNMUX", "Multiplexor for VP_NEGs"));
	pTypes->append(new FunctionalTypeITL_CMNT(ITL_CMNT, "ITL_CMNT", "CryoMaintain Interlock Source"));
	pTypes->append(new FunctionalTypeITL_CMUX(ITL_CMUX, "ITL_CMUX", "CryoMaintain Interlock Multiplexor"));
	pTypes->append(new FunctionalTypeVRE(VRE, "VRE", "Bake-Out racks(Wireless mobiles)"));
	pTypes->append(new FunctionalTypeVG(VG_A_RO, "VG_A_RO", "Generic Analog Read Only Gauge"));
	pTypes->append(new FunctionalType(VOPS_2PS, "VOPS_2PS", "Double Overpressure Switch"));
	pTypes->append(new FunctionalTypeVELO_Process(VELO_PROCESS, "VELO_PROCESS", "VELO Processes"));
	

	/*
	pTypes->append(new FunctionalType(VV_PS_CMW, "VVS_PS_CMW", "Valves in PSR - from CMW"));
	pTypes->append(new FunctionalType(VPI_PS_CMW, "VPI_PS_CMW", "Ion pumps in PSR - from CMW"));
	pTypes->append(new FunctionalType(VGR_PS_CMW, "VGR_PS_CMW", "Pirani Gauges in PSR - from CMW"));
	pTypes->append(new FunctionalType(VGP_PS_CMW, "VGP_PS_CMW", "Penning Gauges in PSR - from CMW"));
	*/

	// Active equipment in VELO
	// Note from apaivaer: this will most likely disappear. These functional types were created
	// for equipment that is read from a PLC. With the restructuring of VELO, we will create real control types
	pTypes->append(new FunctionalType(VV_VELO, "VV_VELO", "Valves in VELO"));
	pTypes->append(new FunctionalType(VPT_VELO, "VPT_VELO", "Turbo pumps in VELO"));
	pTypes->append(new FunctionalType(VPR_VELO, "VPR_VELO", "Roughing pumps in VELO"));
	pTypes->append(new FunctionalType(VGA_VELO, "VGA_VELO", "Absolute pressure gauges in VELO"));
	pTypes->append(new FunctionalType(VGD_VELO, "VGD_VELO", "Differential pressure gauges in VELO"));
	pTypes->append(new FunctionalType(VINT_VELO, "VINT_VELO", "Interlocks in VELO"));
	pTypes->append(new FunctionalType(VPRO_VELO, "VPRO_VELO", "Process status in VELO"));
	pTypes->append(new FunctionalType(VOPS_VELO, "VOPS_VELO", "Over pressure switch in VELO"));

	// PLC
	pTypes->append(new FunctionalTypePLC(PLC, "PLC", "PLC health"));

	// Controllers
	pTypes->append(new FunctionalType(VRCG, "VRCG", "TPG-300 instance"));
	pTypes->append(new FunctionalType(TPG_CONFIG, "TPG_CONFIG", "TPG configurator in PLC"));
	pTypes->append(new FunctionalType(VR_GT, "VR_GT", "TPG-300 instance"));

	// Summary equipment, not coming from database, but existing in PVSS
	pTypes->append(new FunctionalType(MP_ACCESS, "MP_ACCESS", "Access control in main part"));
	pTypes->append(new FunctionalType(SECT_VPI_SUM, "SECT_VPI_SUM", "VRPI summary in vacuum sector"));

	// Global devices
	pTypes->append(new FunctionalTypeSMS_MACHINE_MODE(SMS_MACHINE_MODE, "SMS_MACHINE_MODE", "Machine mode for SMS"));

	// Fill in criteria for all functional types
	for(int idx = 0 ; idx < pTypes->count() ; idx++)
	{
		FunctionalType *pType = pTypes->at(idx);
		pType->fillCriteria(pType);
	}
}


/*
**
**	FUNCTION
**		Check if given name matches wildcard. Wildcard includes:
**		* - matches 0 or more of any characters
**		? - mathes any one character
**
**	PARAMETERS
**		eqpName		- Name of passive equipment
**		wildCard	- Wildcard to check name against
**
**	RETURNS
**		Type of equipment
**
**	CAUTIONS
**		None
*/
bool FunctionalType::nameMatchesWildCard(const char *name, const char *wildCard)
{
	while(*name && *wildCard)
	{
		if(*wildCard == '*')
		{
			wildCard++;
			if(!(*wildCard))
			{
				return true;
			}
			while(*name)
			{
				if(nameMatchesWildCard(name, wildCard))
				{
					return true;
				}
				name++;
			}
			return false;
		}
		else if(*wildCard == '?')
		{
			/*
			name++;
			wildCard++;
			*/
		}
		else if(*name != *wildCard)
		{
			return false;
		}
		name++;
		wildCard++;
	}
	if(*name || *wildCard)
	{
		return false;
	}
	return true;
}


/*
**	FUNCTION
**		Add criteria with given parameters to list of criteria in this class
**
**	ARGUMENTS
**		funcType	- Equipment functional type
**		type	- criteria type
**		subType	- criteria subtype
**		name	- criteria name
**
**	RETURNS
**		Pointer to new criteria instance
**
**	CAUTIONS
**		Method does not check if such criteria already exists
*/
EqpMsgCriteria *FunctionalType::addCriteria(int funcType, int type, int subType, const char *name)
{
	EqpMsgCriteria *pNewCriteria = new EqpMsgCriteria(funcType, type, subType, name);
	pCriteria->append(pNewCriteria);
	return pNewCriteria;
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources.
**		This method can only return keys, common to all functional types.
**		Specific cases shall be processed by subclasses
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalType::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = "Unknown";
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::Error:
		result = "Error";
		break;
	case EqpMsgCriteria::Warning:
		result = "Warning";
		break;
	default:
		break;
	}
	return result;
}

int FunctionalType::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	bool success = false;
	switch(critType)
	{
	case EqpMsgCriteria::Error:
		if(!strcasecmp(name, "error"))
		{
			success = true;
		}
		break;
	case EqpMsgCriteria::Warning:
		if(!strcasecmp(name, "warning"))
		{
			success = true;
		}
		break;
	default:
		break;
	}
	if(!success)
	{
		errMsg = "unknown crtieria <";
		errMsg += name;
		errMsg += ">";
	}
	return 0;
}

