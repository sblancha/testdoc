
/**@name LIBRARY: vclMachineMode.ctl

@author: Leonid Kopylov (IHEP, Protvino, Russia)

Creation Date: 22/02/2008

Modification History: 
  15.06.2009 L.Kopylov Porting to PVSS 3.6

version 1.0

External functions:	
								
Internal function:

Purpose:
This library contains common declarations/functions to provide access
for machine mode to PVSS application

Usage: Public

PVSS manager usage: UI (PVSS00NV), UI extended (PVSS00NG), CTRL

Constraints: 
	. PVSS version: 3.1
	. operating system: W2000, WXP and Linux but tested only under W2000 and Linux
	. distributed system: yes.
*/

// Name of DP type containing machine mode data
const string VAC_MACHINE_MODE_DP_TYPE = "_VacMachineMode";


// LhcVacGetMachineModeDp
/**
Purpose:
Return name of DP containing machine mode information. If such DP
does not exist - it will be created.

Arguments:
		modeDpName	-	string, [out] Name of DP will be returned here. In case
										of error returned string is empty
Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void LhcVacGetMachineModeDp(string &modeDpName)
{
  modeDpName = VAC_MACHINE_MODE_DP_TYPE;
  if(dpExists(modeDpName))
  {
    return;
  }
  if(dpCreate(VAC_MACHINE_MODE_DP_TYPE, VAC_MACHINE_MODE_DP_TYPE) < 0)
  {
    DebugTN("dpCreate(" + VAC_MACHINE_MODE_DP_TYPE + ") failed");
    modeDpName = "";
    return;
  }
  if(!dpExists(modeDpName))
  {
    DebugTN("DP " + VAC_MACHINE_MODE_DP_TYPE + " was not created");
    modeDpName = "";
    return;
  }
}
