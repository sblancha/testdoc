#ifndef EQPSELECTOR_H
#define	EQPSELECTOR_H

// EWO interface for EqpSelectorControl widget

#include <BaseExternWidget.hxx>

#include "EqpSelectorControl.h"

class EWO_EXPORT EqpSelector : public BaseExternWidget
{
	Q_OBJECT

public:
	EqpSelector(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void selectionChange(void);

private:
	EqpSelectorControl	*baseWidget;
};

#endif	// EQPSELECTOR_H
