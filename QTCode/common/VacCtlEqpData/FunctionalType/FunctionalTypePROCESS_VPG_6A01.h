#ifndef FUNCTIONALTYPEPROCESS_VPG_6A01_H
#define FUNCTIONALTYPEPROCESS_VPG_6A01_H

// Process - pumping group 6A01

#include "FunctionalTypePROCESS.h"

class VACCTLEQPDATA_EXPORT FunctionalTypePROCESS_VPG_6A01 : public FunctionalTypePROCESS
{
public:
	FunctionalTypePROCESS_VPG_6A01(int type, const char *name, const char *description);
	virtual ~FunctionalTypePROCESS_VPG_6A01() {}

protected:
	virtual QString getStepResourceBase(void);
};

#endif	// FUNCTIONALTYPEPROCESS_VPG_6A01_H
