/************************************************************
CTRL script to monitor LHC sector valves (VVS_LHC)
Reaction time to OPEN/CLOSE actions
*************************************************************/

#uses "VacCtlEqpDataPvss"	  // Load DLL or so
#uses "vclDevCommon.ctl"    // Load CTRL library
#uses "vclDIPConfig.ctl"		// Load CTRL library 

bool TEST_MODE = FALSE;
uint HISTORIC_SAMPLES = 100;
uint STATE_CLOSED = 5;
uint STATE_OPENED = 3;
uint STATE_UNDEFINED = 1;
uint ARCHIVE_SAMPLES = 20;
float REACTION_TIME_THRESHOLD = 20; // seconds

int BUFFER_TIME = 900; // seconds (15 minutes)
dyn_dyn_string DELAY_NOTIFICATIONS;
dyn_dyn_string SOFTW_NOTIFICATIONS;
string SENDER = "andre.rocha@cern.ch";
string RECEIVER = "vac-valve-monitoring@cern.ch";
string RECEIVER_TEST = "claudia.dias@cern.ch";
string DELAY_SUBJECT = "Valve Reaction Time Delay";
string SOFTW_SUBJECT = "Code Error Detected";

string TTO_STRING = ".Actuation.thresholdToOpen";
string TTC_STRING = ".Actuation.thresholdToClose";
string CYCLE_STRING = ".Actuation.cycleNumber";


/* NOTIFICATION AND DEBUG FUNCTIONS */

  // Print valve DPs
  void printArray(dyn_string arrayToPrint) {
    for(int i = 1; i <= dynlen(arrayToPrint); i++) {
       if(TEST_MODE) DebugTN(arrayToPrint);
    }
  }


  // Valve delay user notification
  void writeValveDelayReport(string valve, string actuation, string actuation_number,
		string valve_reaction, string valve_th, time time_change){
	int i;
	string message;
	
	message = "VALVE:\t\t" + valve + "\n";
	message += "\tACTUATION:\t" + actuation + " (TOTAL: " + actuation_number + ")\n";
	message += "\tREACT TIME:\t" + valve_reaction + "\n";
	message += "\tTHRESHOLD:\t" + valve_th + "\n";
	message += "\tTOC:\t\t" + (string)time_change + "\n";
	
	i = dynlen(DELAY_NOTIFICATIONS)+1;
	DELAY_NOTIFICATIONS[i][1] = SENDER;
	DELAY_NOTIFICATIONS[i][2] = RECEIVER;
	DELAY_NOTIFICATIONS[i][3] = DELAY_SUBJECT;
	DELAY_NOTIFICATIONS[i][4] = message;
	
	if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/writeValveDelayReport] Delay: " + message);
}


  // Software error user notification
  void writeErrorReport(dyn_errClass err, time timeOfError = getCurrentTime()){
	int i;
	string message;
	
	if (dynlen(err) <= 0) return;
	
	message = "CODE:\t\t" + getErrorCode(err) + "\n";
	message += "\tTYPE:\t\t" + getErrorType(err) + "\n";
	message += "\tUSER ID:\t" + getErrorUserId(err) + "\n";
	message += "\tDESCRIPTION:\t" + getErrorText(err) + "\n";
	message += "\tTOC:\t\t" + (string)timeOfError + "\n";
	
	i=dynlen(SOFTW_NOTIFICATIONS)+1;
	SOFTW_NOTIFICATIONS[i][1] = SENDER;
	SOFTW_NOTIFICATIONS[i][2] = RECEIVER;
	SOFTW_NOTIFICATIONS[i][3] = SOFTW_SUBJECT;
	SOFTW_NOTIFICATIONS[i][4] = message;

	if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/writeErrorReport] Error: " + message);
}


  // Organize delay Notifications
  dyn_dyn_string delayNotifications(){
	/*	NOTIFICATION [i] STRUCTURE:
		[i][1] (string)		sender
		[i][2] (string)		receiver/address
		[i][3] (string)		subject
		[i][4] (dyn_string)	message/content */

	string address;
	string sender;
	string subject;
	
	dyn_string addresses;
	dyn_string messages;
	dyn_dyn_anytype DELAY_NOTIFICATIONS_aux;
	
	dyn_dyn_string notification_to_send;
	
	int j;
	int i;
	
	// makes a list of UNIQUE addresses to send the notifications
	while (dynlen(DELAY_NOTIFICATIONS) != 0){
	
		// starts setting a new mail
		dynClear(messages);
		dynClear(addresses);
		
		sender = DELAY_NOTIFICATIONS[1][1];
		address = DELAY_NOTIFICATIONS[1][2];
		DELAY_NOTIFICATIONS_aux = DELAY_NOTIFICATIONS;

		dynDynTurn(DELAY_NOTIFICATIONS_aux);
		addresses = DELAY_NOTIFICATIONS_aux[2];
		
		// gathers all notifications with the same address
		j = dynContains(addresses, address);
		i = 0;
		while ((dynlen(DELAY_NOTIFICATIONS) > 0)&&(j != 0)&&(j != -1)){
			i++;
			messages += "\n[" + i + "]\t" + DELAY_NOTIFICATIONS[j][3] + "\n";
			messages += "\t" + DELAY_NOTIFICATIONS[j][4] + "\n\n";
			
			dynRemove(DELAY_NOTIFICATIONS,j);
			dynRemove(addresses,j);
			j = dynContains(addresses, address);
		}
		
		// sends the email
		subject = "[Vacuum Controls] Valve Reaction Time Monitoring - Delay";
		
		i = dynlen(notification_to_send) + 1;
		notification_to_send[i][1] = sender;
		notification_to_send[i][2] = address;
		notification_to_send[i][3] = subject;
		notification_to_send[i][4] = messages;
	}
	return notification_to_send;
}

  
  // Organize software Notifications
  dyn_dyn_string softwareNotifications(){
	/*	NOTIFICATION [i] STRUCTURE:
		[i][1] (string)		sender
		[i][2] (string)		receiver/address
		[i][3] (string)		subject
		[i][4] (dyn_string)	message/content */

	string address;
	string sender;
	string subject;
	
	dyn_string addresses;
	dyn_string messages;
	dyn_dyn_anytype SOFTW_NOTIFICATIONS_aux;
	
	dyn_dyn_string notification_to_send;
	
	int j;
	int i;
	
	// makes a list of UNIQUE addresses to send the notifications
	while (dynlen(SOFTW_NOTIFICATIONS) != 0){
	
		// starts setting a new mail
		dynClear(messages);
		dynClear(addresses);
		
		sender = SOFTW_NOTIFICATIONS[1][1];
		address = SOFTW_NOTIFICATIONS[1][2];
		SOFTW_NOTIFICATIONS_aux = SOFTW_NOTIFICATIONS;

		dynDynTurn(SOFTW_NOTIFICATIONS_aux);
		addresses = SOFTW_NOTIFICATIONS_aux[2];
		
		// gathers all notifications with the same address
		j = dynContains(addresses, address);
		i = 0;
		while ((dynlen(SOFTW_NOTIFICATIONS) > 0)&&(j != 0)&&(j != -1)){
		
			i++;
			messages += "\n[" + i + "]\t" + SOFTW_NOTIFICATIONS[j][3] + "\n";
			messages += "\t" + SOFTW_NOTIFICATIONS[j][4] + "\n\n";
			
			dynRemove(SOFTW_NOTIFICATIONS,j);
			dynRemove(addresses,j);
			j = dynContains(addresses, address);
		}
		
		// sends the email
		subject = "[Vacuum Controls] Valve Reaction Time Monitoring - Software error";
		
		i = dynlen(notification_to_send) + 1;
		notification_to_send[i][1] = sender;
		notification_to_send[i][2] = address;
		notification_to_send[i][3] = subject;
		notification_to_send[i][4] = messages;
	}
	return notification_to_send;
}


  // Notification Buffer, sends notification emails each INTERVAL seconds
  void notificationBuffer(int interval=BUFFER_TIME, int opt=1){
	dyn_dyn_string notification_to_send;
	dynClear(notification_to_send);
	
	while (true){
		// checks notifications to send mail each INTERVAL seconds
		delay(interval);
		
		if (TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/notificationBuffer] New " + interval + " seconds passed.");

		// defining which notifications to send
		switch(opt){
			case 1:
				notification_to_send = delayNotifications();
				break;
			case 2:
				notification_to_send = softwareNotifications();
				break;
		}
		
		while (dynlen(notification_to_send) > 0){
			send_email(notification_to_send[1][1], notification_to_send[1][2], notification_to_send[1][3], notification_to_send[1][4]);
			if (TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/notificationBuffer] NOTIFICATION EMAIL SENT.");
			dynRemove(notification_to_send,1);
		}
	}
}


  // Send email
  int send_email(string sender, string address, string subject, dyn_string content){
	dyn_dyn_string email_content;
	string tempString;
	int ret;
	
	email_content[1] = address;
    email_content[2]=  sender;
    email_content[3] = subject;
	
	
    for(int i = 1; i <= dynlen(content); i++)
    { 
        sprintf(tempString, "%s\t\n", content[i]);
        email_content[4]= email_content[4] + tempString;
	}
    emSendMail ("cernmx.cern.ch",getHostname(),email_content,ret);
	
	dynClear(email_content);
	return(ret);
}


/* DATAPOINT HANDLING  */
  
  //Get all valve DPs
  dyn_string getValveStateDPs() {
      dyn_string allDpNames = dpNames("*", "VVS_LHC");
      return allDpNames;
  }


  //Register all DPs of type VVS_LHC for change of valve state var
  int connectStateChangeDPs(dyn_string dpsToConnect) {
	string dpName;

    for(int i = 1; i <= dynlen(dpsToConnect); i++) {
		dyn_errClass err;
		dpName = dpsToConnect[i] + ".State";
		
		if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/connectStateChangeDPs] Registering dp " + dpName);     
		dpConnect("stateChanged", false ,dpName , dpName + ":_online.._stime");
	   
		//Error checking and logging
		err = getLastError();
		if(dynlen(err) > 0) { 
			writeErrorReport(err);
		}
    }
    return 1;
  }

  

  

  // Valve state changed()
void stateChanged(string dpName, uint state, string valveTimeDpe, time timeOfChange) {

  string valve = dpSubStr(dpName,DPSUB_DP);

  if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] Valve: " + valve +
		" is now on state " + state + ", TOC: " + (string)timeOfChange);
  
	// If new state is OPENED or CLOSED
  if((state == STATE_CLOSED) || (state == STATE_OPENED)){
	dyn_uint vvstateArchiveValues;
	dyn_time vvstateTimestamps;
	uint stateTrigger;
	time stateTriggerTime;
	int i;
	
	dpGetPeriod(getCurrentTime()-1, getCurrentTime(), ARCHIVE_SAMPLES, dpName, vvstateArchiveValues, vvstateTimestamps);
    
	if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] Valve " + dpName + " new state: " + state + " \nhistoric states: " + vvstateArchiveValues + "\ntime stamps: " + vvstateTimestamps);
	
    //Check last state recorded in archive before the new one
	if (dynlen(vvstateArchiveValues) == 0){
		if(TEST_MODE) DebugTN("Valve " + valve + " state " + state + " : archive is empty");
		return;
	} 
    if(vvstateArchiveValues[dynlen(vvstateArchiveValues)] == state) {
		if (vvstateTimestamps[dynlen(vvstateTimestamps)] == timeOfChange){
			i = dynlen(vvstateArchiveValues)-1;
			if(TEST_MODE) DebugTN("Valve " + valve + " state " + state + " : archive already contains new state");
		} else {
			if(TEST_MODE) DebugTN("Valve " + valve + " state " + state + " : no state change");
			return;
		}
    } else {
		i = dynlen(vvstateArchiveValues);
    }
	//Looks for the last state different that UNDEFINED
	while ((vvstateArchiveValues[i] == STATE_UNDEFINED)&(i>0)){
		i--;
		if(TEST_MODE && i==0) DebugTN("Valve " + valve + " state " + state + " : archive does not contain new state yet");
		if (i==0) return;
	}
	if (vvstateArchiveValues[i] == STATE_UNDEFINED) return;
	
	
	// gets State change trigger information
	dpGet(valve + ".Actuation.stateTrigger", stateTrigger);
	dpGet(valve + ".Actuation.stateTrigger:_online.._stime", stateTriggerTime);
	if (TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] Change Trigger for valve " + valve + " is " + stateTrigger);
	if (TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] Change Trigger @ " + (string)stateTriggerTime);
	
	// if the last state different than UNDEFINED is the opposite of the new state (opened/closed)
	// AND the state change triggered is the same as the new state
	if((state == stateTrigger)&(vvstateArchiveValues[i] != state)){
		dyn_uint plcstateArchiveValues;
		dyn_time plcstateTimeStamps;
		dyn_string exceptionInfo;
		string valve_reaction_dpe;

		string plcName, plcAlarm;
		int j;
		
		//Only checks PLC state if NOT in test mode
		if (!TEST_MODE){
			//Find PLC device name
			LhcVacDevicePlc(valve, plcName, plcAlarm, exceptionInfo);
			if(dynlen(exceptionInfo) > 0){
				if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] Error finding valve PLC.");
				return;
			}
			
			//Checks if alarm DPE exists (_unSystemAlarm_DPE_PLC_XXX)
			if(!dpExists(plcAlarm)){
				if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] Bad configured PLC <" + plcName + "> nas no PLC Alarm configured");
				return;
			}
			
			//Checks if DP VPLC_A_PLCM_XXX exists
			string dpName = "VPLC_A_" + plcName;
			if(!dpExists(dpName)){
				//Checks if DP VPLC_A0_PLCM_XXX exists
				dpName = "VPLC_A0_" + plcName;
				if(!dpExists(dpName)){
					if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] Bad configured Valve <" + valve + "> nas no PLC attribute");
					return;
				}
			}
			
			//Gets PLC states for the time period between states
			dpGetPeriod(vvstateTimestamps[i], getCurrentTime(), 1, dpName + ".AL1",plcstateArchiveValues, plcstateTimeStamps);

			//Checks if PLC was DEAD during the time period between states
			if (dynSum(plcstateArchiveValues)>0){
				if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] New valve cycle is not valid, PLC was DEAD during this time period.");
				return;
			}
		}
		
		// reaction time calculation: from the moment the open/close instruction was sent at SCADA, until the actual state change
		if (stateTriggerTime <= timeOfChange){
			
			float reaction_time; 
			reaction_time = intervalSecond(stateTriggerTime, timeOfChange); //only regarding hours
			if (reaction_time == -1) return;
			
			if (TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] valve reaction time: " + reaction_time);
			
			if (TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] timeOfChange: " + hour(timeOfChange) + " " +
				minute(timeOfChange) + " " + second(timeOfChange) + " " + milliSecond(timeOfChange));
			if (TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] stateTriggerTime: " + hour(stateTriggerTime) + " " +
				minute(stateTriggerTime) + " " + second(stateTriggerTime) + " " + milliSecond(stateTriggerTime));
			if (TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] valve reaction time: " + reaction_time);
			
			// checks if reaction time calculated is acceptable (if not, means some state was not properly archived)
			if (reaction_time > REACTION_TIME_THRESHOLD){
				if (TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/stateChanged] Reaction time calculated for valve " + valve + " is not valid. (" + reaction_time + " seconds )");
				return;
			}
			
			//Record time to close/open to DPe
			float valve_th;	
			if(state==STATE_CLOSED){
				valve_reaction_dpe = valve + ".Actuation.timeToClose";
				dpGet(valve+TTC_STRING,valve_th);
			}else if(state==STATE_OPENED){
				valve_reaction_dpe = valve + ".Actuation.timeToOpen";
				dpGet(valve+TTO_STRING,valve_th);
			}
			if(valve_reaction_dpe == "") return;
			dpSet(valve_reaction_dpe,reaction_time);
			
			//Update cycle number
			uint valveActuations;
			dpGet(valve+CYCLE_STRING,valveActuations);
			valveActuations++;
			dpSet(valve+CYCLE_STRING,valveActuations);
			
			// If reaction time is higher than the valve's threshold
			if(reaction_time > valve_th){
				string actuation;
				
				// Registers a new delay
				if(state==STATE_CLOSED) actuation = "CLOSE";
				if(state==STATE_OPENED) actuation = "OPEN";
				
				writeValveDelayReport(valve, actuation,valveActuations, reaction_time, valve_th, timeOfChange);
			}
		}
	}
  }
  return;
}

// calulates the seconds between two time intervals t1 and t2, where t1 =< t2
// ONLY regarding hours
float intervalSecond(time t1, time t2){
	if (t1>t2) return -1;
	float deltaT = period(t2) - period(t1);
	
  if(milliSecond(t2) >= milliSecond(t1)){
		deltaT = deltaT + (milliSecond(t2)-milliSecond(t1))*0.001;
	}else{
		deltaT = deltaT + (milliSecond(t2)+1000-milliSecond(t1))*0.001;
	}
	return deltaT;
}

// only if machine is LHC (!)
bool InitStaticDataLHC( ){
	dyn_string	exceptionInfo, mainParts;
	long		lResult;
	bool		isLhcData;
	// Set accelerator name to be used by other components
	if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/InitStaticData] Starting ..."); 
	LhcVacGetMachineId( exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ){
		if(TEST_MODE) DebugTN("[InitStaticData] Failed to read machine name: " + exceptionInfo );
		return false;
	}
	if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/InitStaticData] Machine is " + glAccelerator );
	VacResourcesParseFile( PROJ_PATH + "/data/VacResources.txt", glAccelerator, exceptionInfo );
	isLhcData = glAccelerator == "LHC" ? true : false;
	if (!isLhcData){
		return false;
	}
	// Initalize passive machine data
	lResult = LhcVacEqpInitPassive( isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo );
	if( lResult < 0 ){
		if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/InitStaticData] LhcVacEqpInitPassive() failed: " + exceptionInfo );
		return false;
	}
	// Initialzie active equipment information
	lResult = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
			exceptionInfo );
	if( lResult < 0 ){
		if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/InitStaticData] LhcVacEqpInitActive() failed: " + exceptionInfo );
		return false;
	}
	if(TEST_MODE) DebugTN("[vcsValveReactionTimes.ctl/InitStaticData] Done");
	LhcVacInitDevFunc();
	return true;
}



/* MAIN  */

main() {
  if (TEST_MODE) RECEIVER = RECEIVER_TEST;
	
  // Initializing static data from LHC vacuum
  if(!InitStaticDataLHC()) return;
  
  dynClear(DELAY_NOTIFICATIONS);
  dynClear(SOFTW_NOTIFICATIONS);
  
  // Initializing the DELAY notification mails buffer
  startThread("notificationBuffer",BUFFER_TIME,1);
  
  // Initializing the SOFTWARE notification mails buffer
  startThread("notificationBuffer",BUFFER_TIME,2);
  
  //Connect StateChange DPs
  connectStateChangeDPs(getValveStateDPs());  
}

