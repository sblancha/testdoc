//	Implementation of GroupTable class
/////////////////////////////////////////////////////////////////////////////////

#include "GroupTable.h"
#include "PlatformDef.h"

#include <QHeaderView>

#include <math.h>

//////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/Destruction
//////////////////////////////////////////////////////////////////////////////////

GroupTable::GroupTable(QWidget *parent)
	: QTableView(parent) {
	setModel(&model);
	setSelectionMode(QAbstractItemView::SingleSelection);
	setSelectionBehavior(QAbstractItemView::SelectRows);
	setSortingEnabled(false);
	horizontalHeader()->show();
	verticalHeader()->hide();
}

GroupTable::~GroupTable() {
}

void GroupTable::resizeEvent(QResizeEvent *pEvent) {
	QTableView::resizeEvent(pEvent);
	int totalWidth = width();
	int wd = (int)rint(totalWidth * 0.04);
	setColumnWidth(SmsGroup::Id, wd);

	wd = (int)rint(totalWidth * 0.08);
	setColumnWidth(SmsGroup::Owner, wd);
	
	wd = (int)rint(totalWidth * 0.18);
	setColumnWidth(SmsGroup::Name, wd);
	
	wd = (int)rint(totalWidth * 0.12);
	setColumnWidth(SmsGroup::Type, wd);
	
	wd = (int)rint(totalWidth * 0.12);
	setColumnWidth(SmsGroup::Scopes, wd);

	wd = (int)rint(totalWidth * 0.12);
	setColumnWidth(SmsGroup::Recipients, wd);
	
	wd = (int)rint(totalWidth * 0.05);
	setColumnWidth(SmsGroup::ConfigNb, wd);

	wd = (int)rint(totalWidth * 0.10);
	setColumnWidth(SmsGroup::State, wd);
	
	wd = (int)rint(totalWidth * 0.05);
	setColumnWidth(SmsGroup::Events, wd);
	
	wd = (int)rint(totalWidth * 0.04);
	setColumnWidth(SmsGroup::Msg, wd);

	wd = (int)rint(totalWidth * 0.10);
	setColumnWidth(SmsGroup::Error, wd);
}

const QString GroupTable::getSelectedGroup(void) {
	QModelIndexList selections = selectedIndexes();
	if(selections.isEmpty()) {
		return QString();
	}
	return model.getGroupAtRow(selections.at(0).row());
}

