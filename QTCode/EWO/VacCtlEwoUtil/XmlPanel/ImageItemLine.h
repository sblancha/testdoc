#ifndef	IMAGINEITEMLINE_H
#define	IMAGINEITEMLINE_H

#include "ImageItem.h"

class VACCTLEWOUTIL_EXPORT ImageItemLine : public ImageItem
{
public:
	ImageItemLine();
	~ImageItemLine();

	virtual void draw(QPainter & painter);
	virtual bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	// Access
	inline int getStartX(void) const { return startX; }
	inline void setStartX(int coord) { startX = coord; }
	inline int getStartY(void) const { return startY; }
	inline void setStartY(int coord) { startY = coord; }
	inline int getEndX(void) const { return endX; }
	inline void setEndX(int coord) { endX = coord; }
	inline int getEndY(void) const { return endY; }
	inline void setEndY(int coord) { endY = coord; }

protected:
	// Start/end coordinates
	int		startX;
	int		startY;
	int		endX;
	int		endY;

	virtual void parseProp(QXmlStreamReader &reader);
	bool distanceToPoint(const QPoint &point, float &distance);
};

#endif	// IMAGINEITEMLINE_H
