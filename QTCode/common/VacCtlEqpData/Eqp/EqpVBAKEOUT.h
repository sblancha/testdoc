#ifndef	EQPVBAKEOUT_H
#define	EQPVBAKEOUT_H

// VBAKEOUT - mobile bakeout rack. In fact, it is just Eqp wrapper for BakeoutWorkDp

#define	MAX_CHANNEL	(24)

#include "Eqp.h"

class BakeoutWorkDp;

class VACCTLEQPDATA_EXPORT EqpVBAKEOUT : public Eqp
{
	Q_OBJECT

public:
	EqpVBAKEOUT(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVBAKEOUT(const EqpVBAKEOUT &src) : Eqp(src) {}
#endif
	~EqpVBAKEOUT();

	virtual void setWorkDp(BakeoutWorkDp *pNewWorkDp);
	virtual bool isActive(DataEnum::DataMode /* mode */) const { return pWorkDp != NULL; }

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	bool isPlcAlarm(DataEnum::DataMode mode) const;
	unsigned getRR1(DataEnum::DataMode mode) const;
	inline int getMaxChannel(void) const { return MAX_CHANNEL; }
	unsigned getChannelState(int channel, DataEnum::DataMode mode) const;
	unsigned getCtlChannelState(int channel, DataEnum::DataMode mode) const;

protected:

	// Work DP name - not-NULL when activated
	BakeoutWorkDp		*pWorkDp;
};

#endif	// EQPVBAKEOUT_H
