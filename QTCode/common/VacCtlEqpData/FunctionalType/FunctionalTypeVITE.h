#ifndef FUNCTIONALTYPEVITE_H
#define	FUNCTIONALTYPEVITE_H

// Special functional type for thermocouples value - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVITE : public FunctionalType
{
public:

	FunctionalTypeVITE(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVITE() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);
};

#endif	// FUNCTIONALTYPEVITE_H
