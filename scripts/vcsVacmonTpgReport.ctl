/****************************************************************************************************************************/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* name SCRIPT: TGP300StateWebReport.ctl
*******************************************************************
***				author:	Fabien ANTONIOTTI 	(CERN)				***
*******************************************************************

//History

// 07/09/2015 - Andre Rocha - Fixed bug: duplicated TPG300 reports
// 22/09/2017 - Jorge Fraga - changed write report to use DB

From Sources : 
	1) lhcPanel_VRGT.pnl			author: Leonid Kopylov 		(IHEP, Protvino, Russia)
	2) vcsValveStateReport.ctl		author: Mikhail Mikheev 	(IHEP, Protvino, Russia)

Creation Date: 8/04/2011
version 1.0 :
*/
#uses "VacCtlEqpDataPvss"	// Load DLL
#uses "vclSectors.ctl"	// Load CTRL library
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "vclDevCommon.ctl"	// Load CTRL library
#uses "vclResources.ctl"   // Load CTRL library
#uses "vclMachine.ctl"		// Load CTRL library
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/****************************************************************************************************************************/
// General variables
bool TEST_MODE = FALSE;

// Bit mask - communicator is busy processing request
const unsigned	TPG_CONFIG_BUSY = 0x1000000u;

// Timeout for quering many values
const int	MANY_VALUES_TIMEOUT = 50;

// Timeout for querying one value
const int       ONE_VALUE_TIMEOUT = 20;

// DP name of TPG300 device
string		vrcgName, vrcgNameLast;

// DP name of VRGT device
string		vrgtDpName;
// readable VRGT name
string          devName;
// DB number of Gauge device in PLC
int				gaugeDbNbr;

// Current step number of communication process
int				step;

// Global variables used during communication by both daa callback
// and communication engine
unsigned	readRR1, readRR2, readRR3, writeWR3,
		extVtpgDbNb, extChannel, extParamBits;
float		readRR4, readRR5, writeWR4, writeWR5;

// Time when comunication process was started
time		startTime;

// Time when request was sent to PLC
time		requestTime;

// Flag indicating that query is in progress
bool		isQueryInProgress;

// New byte value to be written to PLC
unsigned	newByteValue;

// New floating point value to be written to PLC
float		newFloatValue;

// Old password
int		oldPassword;

// Family, type and subType
int family, type, subType;

// Flag indicating if we are waiting for busy flag from PLC
bool		waitingForBusyFlag;

// Error code set by PLC during write
int		writeErrCode;

// Flag for producing debug messages
bool		debug = false;

// Error codes from relays are located here
dyn_anytype    errorList;
// current number og query
int	       queryNumber = 0;

// table of devices
dyn_string A_DBdevices_to_write ;

// counter of table
int i_counttable = 0;

// string
string s_Temp_separat = "----------";

// already handled TPG300
dyn_string handledTPG300list;

/////////////////////////////////////////////////
//    TEST VARIABLES
//

bool test_mode = false;

mapping reportTPG;
// Database
string DRIVER = "QOCI8";
string DATABASE = "cerndb1";
string USER = "vacmon";
string PASSW = "vacmon2017ICM";
string CONNECTION_NAME = "test_vacmon";

/////////////////////////////////////////////////
main()
{
  InitStaticData();
  if(!setDBConnection()) return; // Initialization of DB access parameters
  
  string        dpName, slaveDpName, str;
  dyn_string 	slaveDpNames, exceptionInfo;
  int 		n, i, m, vrcgChannel;
  dyn_string mpList;
  LhcVacGetAllMainParts(mpList);    //<<<<<<<<<< Read all main parts
  int nMps = dynlen(mpList); 		// Nb Main parts
  
 for(int mpIdx = 1 ; mpIdx <= nMps ; mpIdx++)
  {
    dyn_string sectList;
	string s_channel;
    LhcVacGetSectorsOfMainPart(mpList[mpIdx], sectList);  //<<<<<<<<<<<< Read all sectors of next main part
    int nSectors = dynlen(sectList);
	//DebugTN("Nb sector : " + nSectors );	// Nb sector
    for(int sectIdx = 1 ; sectIdx <= nSectors ; sectIdx++)
    {
      string sectName = sectList[sectIdx];
      dyn_string VGP_Dps, exceptionInfo;  // VGP_Dps => VGP_T DP type penning gauge controlled by TPG300
      LhcVacGetDevicesOfDpTypesAtSectors(makeDynString(sectName), makeDynString("VGP_T", "VG_PT"), VGP_Dps, exceptionInfo); //<<<<<< Read all devices of given type in sector
      int nVGP = dynlen(VGP_Dps);	  // Nb VGP controlled by TPG300
      //DebugTN("Nb VGP : " + nVGP + "** " + sectName);
	  for(int eqpIdx = 1 ; eqpIdx <= nVGP ; eqpIdx++)
      {
        // init
		queryNumber = 0;
		mappingClear(reportTPG);
		reportTPG["eqpa1"] = "";
		reportTPG["eqpa2"] = "";
		reportTPG["eqpb1"] = "";
		reportTPG["eqpb2"] = "";		
		reportTPG["machine"] = glAccelerator;
		// DPname of VGP
		slaveDpName = VGP_Dps[eqpIdx];
		if(debug) {
				DebugTN("Debug TPG300 issues"); 
				DebugTN("Gauge name: " + slaveDpName );			
			}
		//DebugTN("DPname of VGP : " + slaveDpName + " / " + vrcgChannel); 
		//vrcgName = 'DP name' of TPG300 for given gauge
		LhcVacEqpMaster(dpSubStr(slaveDpName, DPSUB_DP), vrcgName, vrcgChannel);
		if(strlen(vrcgName) == 0)
		  {
			if(debug) {
				DebugTN("Debug TPG300 issues"); 
				DebugTN("DP name for TPG300 not found");			
			}
			return;
		  }
    
		//DebugTN("DP name of TPG300 : " + vrcgName + " / " + vrcgNameLast); 
		//------------------------------------------------------------------------------------------
		
     //Check if TPG300 is already handled    
    if (!dynContains(handledTPG300list,vrcgName))
		{
       
			//Add to TPG300 handled list
			dynAppend(handledTPG300list, vrcgName);
      
			// sector name
			reportTPG["sector"] = sectName;
			reportTPG["mainpart"] = mpList[mpIdx];
			
			vrcgNameLast = vrcgName;
			//A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = vrcgName;
			
			// devName = visible name of TPG300 (DP name converted to visible name )
			LhcVacDisplayName(vrcgName, devName, exceptionInfo);
			if(dynlen(exceptionInfo) > 0)
			  {
        DebugN("No DisplayName for ", vrcgName);
				return;
			  }
			
			reportTPG["eqpname"] = devName;
			
			LhcVacEqpConfig(dpSubStr(slaveDpName, DPSUB_DP),vrgtDpName, gaugeDbNbr);
			if(strlen(vrgtDpName) == 0)
			  {
				if(debug) {
					DebugTN("Debug TPG300 issues"); 
					DebugTN("No EqpConfig for ", slaveDpName);			
				}				
				vrgtDpName = "NOT USED";
			  }
			
			reportTPG["secondname"] = vrgtDpName;

			LhcVacEqpSlaves(vrcgName, slaveDpNames);
			if(dynlen(slaveDpNames) == 0) {
				if(debug) {
				DebugTN("Debug TPG300 issues"); 
				DebugTN("No slaves for ", vrcgName);		
				}
				return;
			 }
			 
			if(debug) {
				DebugTN("Debug TPG300 issues"); 
				DebugTN("Slaves for TPG300 " + vrcgName + ": " + slaveDpNames);		
				}
			
			if(debug) {
				DebugTN("Debug TPG300 issues"); 
				DebugTN(A_DBdevices_to_write);			
			}
			
			
			// fill gauge fields of gauge table	with get data
			for(n = 1; n <= dynlen(slaveDpNames); n++)
			  {
				LhcVacGetCtlAttributes(slaveDpNames[n], family, type, subType);
				if((family == 6) && (type == 1))
				{
					continue;  // This is not a gauge, but rather VR_GT
				}
				//DebugTN("Gauge  : " + slaveDpNames[n]); 
				//A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = slaveDpNames[n];
				LhcVacEqpMaster(slaveDpNames[n], vrcgName, vrcgChannel);
				if(strlen(vrcgName) == 0)
				{
				DebugN("No Master for ", slaveDpNames[n]);
				  return;
				}
				LhcVacDisplayName(slaveDpNames[n], devName, exceptionInfo);
				//DebugTN("Device name gauge  : " + devName + " / " + exceptionInfo + " / " + vrcgChannel); 
				switch(vrcgChannel)
				  {
				  case 1:
					reportTPG["eqpa1"] = devName;
					break;
				  case 2:
					reportTPG["eqpa2"] = devName;
					break;
				  case 3:
					reportTPG["eqpb1"] = devName;
					break;
				  case 4:
					reportTPG["eqpb2"] = devName;
					break;
				  }
			  }

			QueryAllData();
			//DebugTN(A_DBdevices_to_write);//WriteReport();
			if(!WriteToDB())
				return;
		}
	  }
	}
}
}
void QueryAllData()
{
  // From now on 2(?) ways of getting TPG300 data exist:
  //  1) using TPG_CONFIG device (like it was before)
  //  2) using VG_GT device - new, values can be read directly from DPEs
  // Decide which way shall be used for TPG300 being processed:
  // VR_GT shall refer to TPG300 with attribute MasterName
  //DebugTN("Master is:", vrcgName);
  dyn_string refDpNames;
  LhcVacGetByAttrValue("MasterName", vrcgName, refDpNames);
  for(int refIdx = dynlen(refDpNames) ; refIdx > 0 ; refIdx--)
  {
    //DebugTN("Check " + refDpNames[refIdx]);
    int family, type, subType;
    LhcVacGetCtlAttributes(refDpNames[refIdx], family, type, subType);
    if((family == 6) && (type == 1))
    {
      //DebugTN("Found !!!");
      QueryAllDataUsingVR_GT(refDpNames[refIdx]);
      return;
    }
  }
  
  // If we are at this point - no VR_GT was found, use TPG_CONFIG
  QueryAllDataUsingTpgConfig();
}

void QueryAllDataUsingTpgConfig()
{
  string	str;
  dyn_string	exceptionInfo;
  unsigned	virtualMask;
  int		n, totalQueries = 9;
  int		virtualParam;

  // fill Programm Version fields
  queryNumber++;
  GetVtpgData(0, 0xFu, exceptionInfo);
  DisplayProgram(exceptionInfo);

  // fill common fields	
  queryNumber++;
  dynClear(exceptionInfo);
  GetVtpgData(7, 0xFu, exceptionInfo);
  DisplayCardName("acardtype", (readRR3 >> 8) & 0xFF, exceptionInfo);
  DisplayCardName("bcardtype", readRR3 & 0xFF, exceptionInfo);
  DisplayUnit(exceptionInfo);
  DisplayUnderrange(exceptionInfo);
  
  // 	fill filters fields for gauges (A1, A2, B1, B2) table and ALL
  //	data for relays table
  for(n = 1; n <= 6; n++)
  {
    queryNumber++;
    dynClear(exceptionInfo);
	A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = s_Temp_separat ; 
	str = "";
	switch(n)
	  {
	  case 1:
		str = "1" ;
		break;
	  case 2:
		str = "2" ;
		break;
	  case 3:
		str = "3" ;
		break;
	  case 4:
		str = "4" ;
		break;
	  case 5:
		str = "A" ;
		break;
	  case 6:
		str = "B" ;
		break;
	  }
	A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = "Relay : " + str;
	
	if(n <= 4) { virtualParam = 63; }
    else { virtualParam = 62;}
    GetVtpgData(n, virtualParam, exceptionInfo);

    // Analyze received data and fill correspondent tables
    DisplaySource(n, exceptionInfo);
	if(n <= 4)  { DisplayFilter(n, exceptionInfo); }
	else { A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = "Filter : --";};
	DisplayValid(n, exceptionInfo);
	DisplayStatus(n, exceptionInfo);
	DisplayUpThreshold(n, exceptionInfo);
	DisplayLowThreshold(n, exceptionInfo);
  }
}

void QueryAllDataUsingVR_GT(string dpName)
{
//DebugTN("QueryAllDataUsingVR_GT(" + dpName + "): start");
  string	str;
  dyn_string	exceptionInfo;

  // fill Programm Version fields
  unsigned uValue;
  dpGet(dpName + ".ProgVersion", uValue);
  readRR2 = 0;
  readRR3 = uValue;
  DisplayProgram(exceptionInfo);

  // fill common fields	
  dpGet(dpName + ".BoardIDs", uValue);
  DisplayCardName("acardtype", (uValue >> 8) & 0xFF, exceptionInfo);
  DisplayCardName("bcardtype", uValue & 0xFF, exceptionInfo);
  
  dpGet(dpName + ".RR2", uValue);
  readRR3 = (uValue << 12) & 0x03000000u;
  DisplayUnit(exceptionInfo);
  readRR3 = (uValue << 1) & 0x10000u;
  DisplayUnderrange(exceptionInfo);
  
  // 	fill filters fields for gauges (A1, A2, B1, B2) table and ALL
  //	data for relays table
  unsigned uSources;
  dpGet(dpName + ".Sources", uSources);
  for(int n = 1; n <= 6; n++)
  {
  	A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = s_Temp_separat ; 
	  str = "";
	  switch(n)
	  {
	  case 1:
		str = "1" ;
		break;
	  case 2:
		str = "2" ;
		break;
	  case 3:
		str = "3" ;
		break;
	  case 4:
		str = "4" ;
		break;
	  case 5:
		str = "A" ;
		break;
	  case 6:
		str = "B" ;
		break;
	  }
	  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = "Relay : " + str;
	
    // Analyze received data and fill correspondent tables
    switch(n)
    {
    case 1:
      readRR3 = (uSources >> 8) & 0xF00;
      break;
    case 2:
      readRR3 = (uSources >> 12) & 0xF00;
      break;
    case 3:
      readRR3 = (uSources) & 0xF00;
      break;
    case 4:
      readRR3 = (uSources >> 4) & 0xF00;
      break;
    case 5:
      readRR3 = (uSources << 8) &0xF00;
      break;
    case 6:
      readRR3 = (uSources << 4) & 0xF00;
      break;
    }
    DisplaySource(n, exceptionInfo);
    
	  if(n <= 4)
    {
      switch(n)
      {
      case 1:
        readRR3 = (uValue << 24) & 0x03000000u;
        break;
      case 2:
        readRR3 = (uValue << 22) & 0x03000000u;
        break;
      case 3:
        readRR3 = (uValue << 20) & 0x03000000u;
        break;
      case 4:
        readRR3 = (uValue << 18) & 0x03000000u;
        break;
      }
      DisplayFilter(n, exceptionInfo);
    }
  	else
    {
      A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = "Filter : --";
    };
    
    switch(n)
    {
    case 1:
      readRR3 = (uValue >> 1) & 0x10000;
      break;
    case 2:
      readRR3 = (uValue >> 3) & 0x10000;
      break;
    case 3:
      readRR3 = (uValue >> 5) & 0x10000;
      break;
    case 4:
      readRR3 = (uValue >> 7) & 0x10000;
      break;
    case 5:
      readRR3 = (uValue << 7) & 0x10000;
      break;
    case 6:
      readRR3 = (uValue << 5) & 0x10000;
      break;
    }
	  DisplayValid(n, exceptionInfo);
    
    switch(n)
    {
    case 1:
      readRR3 = (uValue >> 16) & 0x1;
      break;
    case 2:
      readRR3 = (uValue >> 18) & 0x1;
      break;
    case 3:
      readRR3 = (uValue >> 20) & 0x1;
      break;
    case 4:
      readRR3 = (uValue >> 22) & 0x1;
      break;
    case 5:
      readRR3 = (uValue >> 8) & 0x1;
      break;
    case 6:
      readRR3 = (uValue >> 10) & 0x1;
      break;
    }
	  DisplayStatus(n, exceptionInfo);

    string dpeNameLastPart;
    switch(n)
    {
    case 1:
    case 2:
    case 3:
    case 4:
      dpeNameLastPart = "R" + n;
      break;
    case 5:
      dpeNameLastPart = "CCA";
      break;
    case 6:
      dpeNameLastPart = "CCB";
      break;
    }
    float fLimitU, fLimitL;
    dpGet(dpName + ".UpperThresh" + dpeNameLastPart, fLimitU,
          dpName + ".LowerThresh" + dpeNameLastPart, fLimitL);
    readRR4 = fLimitU;
    readRR5 = fLimitL;
  	DisplayUpThreshold(n, exceptionInfo);
  	DisplayLowThreshold(n, exceptionInfo);
  }
}

void GetVtpgData(unsigned channel, unsigned paramBits, dyn_string &exceptionInfo)
{
  int      n;
  unsigned data;
  bool     success = false;
  time     now;
  //DebugTN("GetVtpgData()");
  step = 1;
  extVtpgDbNb = gaugeDbNbr << 16;
  extChannel = channel << 8;
  extParamBits = paramBits << 16;
  dynClear(exceptionInfo);
	
  dpConnect("ConfigDataCb", true,
    vrgtDpName + ".RR1",
    vrgtDpName + ".RR2");

  startTime = getCurrentTime();
  while(true)
  {
	if(step == 4)
    {
      success = true;
	  break;
    }
    now = getCurrentTime();
    if(step == 2)		// Request has been sent, waiting for busy flag
    {
    
	  if((period(now) - period(requestTime)) >= 4) // Probably we've just missed busy flag
      {
        if(readRR1 == (extVtpgDbNb | extChannel | 0x1u))
        {
          success = true;
          break;
        }
      }
    }
    // Don't wait too long, 30 seconds ??? is a limit
    if((period(now) - period(startTime)) > MANY_VALUES_TIMEOUT) break;
	delay(0, 200);
  }
  //Debug("dpDisconnect");
  dpDisconnect("ConfigDataCb",
    vrgtDpName + ".RR1",
    vrgtDpName + ".RR2");
  if(success)
  {
	//Debug("dpGet success");
    dpGet(vrgtDpName + ".RR2", readRR2,
      vrgtDpName + ".RR3", readRR3,
      vrgtDpName + ".RR4", readRR4,
      vrgtDpName + ".RR5", readRR5);
  }
  else
  {
    DebugTN("dpGet of " + vrgtDpName + ": no success ");
	fwException_raise(exceptionInfo, 
      "ERROR", "Read parameters for <" + vrgtDpName + "> failed", "");
  }
  //DebugTN("Fin GetVtpgData");
}

// Callback for data from TPG config - reading configuration
void ConfigDataCb(string dp1, unsigned	rr1, string dp2, unsigned rr2)
{
  bool	dbNbSet = false, channelSet = false;

  if(test_mode) {  
    delay(0,500);  
    step = 4;
    return;  
    
  }  
  
  //DebugTN("ConfigDataCb " + dp1 + " / " + dp2);
  readRR1 = rr1;
  readRR2 = rr2;
  switch(step)
  {
  case 1:
    if((rr2 & TPG_CONFIG_BUSY) == 0)
    {
      dpSet(vrgtDpName + ".WR2", extParamBits,
        vrgtDpName + ".WR1", (extVtpgDbNb | extChannel | 0x1u));
      requestTime = getCurrentTime();
      step++;
    }
    break;
  case 2:
    if((rr2 & TPG_CONFIG_BUSY) != 0) step++;
    break;
  case 3:
    if((rr2 & TPG_CONFIG_BUSY) == 0)
    {
      /*
      sprintf(buf, "expect %08X, got %08X", (extVtpgDbNb | extChannel | 0x1u), rr1);
      DebugTN(buf);
      */
      if(rr1 ==  (extVtpgDbNb | extChannel | 0x1u)) step++;
    }
    break;
  }
  AddErrorToList( );
}

void DisplayProgram(dyn_string exceptionInfo)
{
  // DebugTN("DisplayProgram()");
  string	str;
 
  if((dynlen(exceptionInfo) == 0) && ((readRR2 & 0xFF0000) == 0))
  {
    unsigned	v = readRR3 & 0xFFu;
    if(v == 0) v = 7;
    else if(v > 7) v = 7;
    sprintf(str, "%lX", (readRR3 >> 8));
    dyn_string	abbrString = makeDynString("A","B","C","D","E","F","00");
    str = str + "-" + abbrString[v];
  }
  else
  {
	str  = (readRR2 & 0xFF0000) != 0 ?
      ("error " + ((readRR2 & 0xFF0000) >> 16)) :
      "timeout";
    //A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = str ;
  }	
reportTPG["program"] = str ;
}

void DisplayCardName(string textName, unsigned code, dyn_string exceptionInfo)
{
// DebugTN("DisplayCardName()");
  string	str;
 
  if((dynlen(exceptionInfo) == 0) && ((readRR2 & 0xFF0000) == 0))
  {
    switch(code)
    {
    case 0:
      str = "No card!";
      break;
    case 7:
      str = "PE300-T10";
      break;
    case 8:
      str = "PE300-T11";
      break;
    case 10:
      str = "PE300-DT9";
      break;
    case 15:
      str = "CP300-T9L";
      break;
    case 18:
      str = "CP300-T10L";
      break;
    case 20:
      str = "CP300-T11L";
      break;
    case 255:
      str = "Unknown (" + code + ")";
      break; 
    default:
      str = "Unknown (" + code + ")";
      break;
    }
  }
  else
  {
	str = textName + " card : " + (readRR2 & 0xFF0000) != 0 ?
      ("error " + ((readRR2 & 0xFF0000) >> 16)) :
      "timeout";
  }	
  reportTPG[textName] = str;
}

void DisplayUnit(dyn_string exceptionInfo)
{
  // DebugTN("DisplayUnit()");
  string	str;
 
  if((dynlen(exceptionInfo) == 0) && ((readRR2 & 0xFF0000) == 0))
  {
    switch((readRR3 >> 24) & 0xFF)
    {
    case 1:
      str = "mbar";
      break;
    case 2:
      str = "Torr";
      break;
    case 3:
      str = "Pa";
      break;                                                         
    default:
      str = "Unknown";
    }
  }
  else
  {
    str = (readRR2 & 0xFF0000) != 0 ?
      ("error " + ((readRR2 & 0xFF0000) >> 16)) :
      "timeout";
  }	
  reportTPG["unit"] = str;

}

void DisplayUnderrange(dyn_string exceptionInfo)
{
  // DebugTN("DisplayUnderrange()");
  string	str;
 
  if((dynlen(exceptionInfo) == 0) && ((readRR2 & 0xFF0000) == 0))
  {
	
	switch((readRR3 >> 16) & 0xFF)
    {
    case 0:
      str = "OFF";
      break;
    case 1:
      str = "ON";
      break;                                                        
    default:
      str = "Unknown";
    }
  }
  else
  {
	str = (readRR2 & 0xFF0000) != 0 ?
      ("error " + ((readRR2 & 0xFF0000) >> 16)) :
      "timeout";
  }	
  reportTPG["underrange"] = str;
}

void DisplayFilter(int row, dyn_string exceptionInfo)
{
  // DebugTN("DisplayFilter(" + row + ")");
  string	str;
  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = "Filter : ";

  if((dynlen(exceptionInfo) == 0) && ((readRR2 & 0xFF0000) == 0))
  {
    switch((readRR3 >> 24) & 0xFF)
    {
    case 1:
      str = "Fast";
      break;
    case 2:
      str = "Medium";
      break;
    case 3:
      str = "Slow";
      break;                                                         
    default:
      str = "Unknown";
    }
  }
  else if((readRR2 & 0xFF0000) != 0)  {   str =  ((readRR2 & 0xFF0000) >> 16); }
  else {  str =  "timeout"; }
  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)] += str;
}

void DisplayValid(int row, dyn_string exceptionInfo)
{
  string	str;
  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = "Valid : " ;
  if((dynlen(exceptionInfo) == 0) && ((readRR2 & 0xFF0000) == 0))
  {
    if (((readRR3 >> 16) & 0xFF) == 1) 
    {
      str = "Valid";
    }
    else 
    {
      str = "Invalid";
    }
  }
  else if((readRR2 & 0xFF0000) != 0)  {	str =  ((readRR2 & 0xFF0000) >> 16);  }
  else  {	str =  " ";  }
  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)] += str ;
}

void DisplaySource(int row, dyn_string exceptionInfo)
{
  // DebugTN("DisplaySource(" + row + ")");
   string	str;
   A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = "Source : ";
  if((dynlen(exceptionInfo) == 0) && ((readRR2 & 0xFF0000) == 0))
  {
    dyn_string abbrString = makeDynString("A1","A2","B1","B2","No","No");
    int m = (readRR3 >> 8) & 0xFF;
    if ((m < 1) || (m > 4)) m = 5;
    str =  abbrString[m];
  }
  else if((readRR2 & 0xFF0000) != 0) {  str = ((readRR2 & 0xFF0000) >> 16); }
  else {	str = "" ;  }
  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)] += str;
}

void DisplayStatus(int row, dyn_string exceptionInfo)
{
  // DebugTN("DisplayStatus(" + row + ")");
  string	str;
  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = "Status : ";
  if((dynlen(exceptionInfo) == 0) && ((readRR2 & 0xFF0000) == 0))
  {
    if ((readRR3 & 0xFF) == 1) str = "Activ";
    else str = "Deact";
  }
  else if((readRR2 & 0xFF0000) != 0) {str = ((readRR2 & 0xFF0000) >> 16);}
  else {str = ""; }
  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)] += str;
}

void DisplayUpThreshold(int row, dyn_string exceptionInfo)
{
  string	str;
  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = "UpThreshold : ";
  if((dynlen(exceptionInfo) == 0) && ((readRR2 & 0xFF0000) == 0)){ sprintf(str, "%8.2E", readRR4); }
  else if((readRR2 & 0xFF0000) != 0) {str =  ((readRR2 & 0xFF0000) >> 16); }
  else {  str = "";}
  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)] += str;
}

void DisplayLowThreshold(int row, dyn_string exceptionInfo)
{
  string str;
  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)+1] = "LowThreshold : ";
  if((dynlen(exceptionInfo) == 0) && ((readRR2 & 0xFF0000) == 0)) {  sprintf(str, "%8.2E", readRR5); }
  else if((readRR2 & 0xFF0000) != 0) { str = ((readRR2 & 0xFF0000) >> 16); }
  else {str=  "";}
  A_DBdevices_to_write[dynlen(A_DBdevices_to_write)] += str;
}

void AddErrorToList( )
{
  if( dynlen(errorList) == 0 )
  {
    errorList = makeDynUInt(0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0);
  }
//  DebugTN("errorList ", errorList,  " queryNumber " ,queryNumber);
  if( (readRR2 & 0xFF0000) != 0)
  {
    errorList[queryNumber] = ((readRR2 & 0xFF0000) >> 16);
  }
}
 
/**InitStaticData from csValveStateReport.ctl by author: Mikhail Mikheev (IHEP, Protvino, Russia)
Constraints: PVSS version: 3.0 / operating system: WXP and Linux / distributed system: yes
*/
bool InitStaticData( )
{
	dyn_string	exceptionInfo, mainParts;
	long		lResult;
	bool		isLhcData;
	// Set accelerator name to be used by other components
	//DebugTN("InitStaticData"); 
	LhcVacGetMachineId( exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "TGP300StateWebReport.ctl: Failed to read machine name: " + exceptionInfo );
		return false;
	}
	DebugTN( "TGP300StateWebReport.ctl: Machine is " + glAccelerator );
	VacResourcesParseFile( PROJ_PATH + "/data/VacResources.txt", glAccelerator, exceptionInfo );
	isLhcData = glAccelerator == "LHC" ? true : false;
	// Initalize passive machine data
	lResult = LhcVacEqpInitPassive( isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo );
	if( lResult < 0 )
	{
		DebugTN( "TGP300StateWebReport.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo );
		return false;
	}
	//DebugTN( "TGP300StateWebReport.ctl: LhcVacEqpInitPassive() done" );

	// Initialzie active equipment information
	lResult = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
			exceptionInfo );
	if( lResult < 0 )
	{
		DebugTN( "TGP300StateWebReport.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo );
		return false;
	}
	//DebugTN( "TGP300StateWebReport.ctl: LhcVacEqpInitActive() done" );

	DebugTN( "TGP300StateWebReport.ctl: InitStaticData() done" );
	LhcVacInitDevFunc();
	return true;
}

WriteReport()
{
	int			err;
	string 		fileName;
	file    	fileToPrint;
	string		buf,  strToPrint, str;
	time		curTime;
   dyn_dyn_string report;
   int nlines= dynlen(A_DBdevices_to_write);
	
	curTime = getCurrentTime();
	//VRGP(-11).233.7L1.R_2011-08-09_10h35m23s.txt
	str= formatTime("_%Y-%m-%d_%Hh%Mm%Ss", curTime);
	buf = "TPG300 : " + A_DBdevices_to_write[3] + " " + formatTime("%Y-%m-%d %H:%M:%S", curTime); 
	//fileName = "C:\\vacWebMonitoring\\Screenshots\\Tables\" + A_DBdevices_to_write[2] + ".txt";  
	fileName = "C:\\vacWebMonitoring\\PVSS_Projects\\TPG300_State_report\\Temp\\" +   glAccelerator + "\\" + A_DBdevices_to_write[3] + str +".txt";
   //fileName = "C:\\temp\\" + A_DBdevices_to_write[3] + str +".txt";  
	//DebugTN(fileName);
   fileToPrint = fopen (fileName, "w+" );
	err = ferror(fileToPrint); // export error
	if (err!=0)
	{
		DebugN("TGP300StateWebReport.ctl::Open file: Error no. ",err," occurred");
		return;
	}
	
	fputs(buf + "\r\n", fileToPrint); 
	for(int i = 1; i <= nlines; i++)
		{
			fputs(A_DBdevices_to_write[i] +"\r\n", fileToPrint); 
		}  
   fclose(fileToPrint); // close file
}

bool WriteToDB()
{
	
	//fill mapping
	 dbCommand cmd;
	 string cmdText, errTxt;
	//reportLine["eqpname"] = A_DBdevices_to_write[3];
	
	dyn_string splitStr;
	for(int i = 0; i<6;i++)
	{
		splitStr = strsplit(A_DBdevices_to_write[2 + i*8],":");
		reportTPG["relaynumber"] = strltrim(splitStr[2], " ");
		splitStr = strsplit(A_DBdevices_to_write[3 + i*8],":");
		reportTPG["relaysource"] = strltrim(splitStr[2], " ");
		splitStr = strsplit(A_DBdevices_to_write[4 + i*8],":");
		reportTPG["relayfilter"] = strltrim(splitStr[2], " ");
		splitStr = strsplit(A_DBdevices_to_write[5 + i*8],":");
		reportTPG["relayvalid"] = strltrim(splitStr[2], " ");
		splitStr = strsplit(A_DBdevices_to_write[6 + i*8],":");
		reportTPG["relaystatus"] = strltrim(splitStr[2], " ");
		splitStr = strsplit(A_DBdevices_to_write[7 + i*8],":");
		reportTPG["relayupthreshold"] = strltrim(splitStr[2], " ");
		splitStr = strsplit(A_DBdevices_to_write[8 + i*8],":");
		reportTPG["relaylowthreshold"] = strltrim(splitStr[2], " ");
		
		switch(reportTPG["relaysource"])
		{
			case "A1":
				reportTPG["relaygauge"] = reportTPG["eqpa1"];
				break;
			case "A2":
				reportTPG["relaygauge"] = reportTPG["eqpa2"];
				break;
			case "B1":
				reportTPG["relaygauge"] = reportTPG["eqpb1"];
				break;
			case "B2":
				reportTPG["relaygauge"] = reportTPG["eqpb2"];
				break;
			default:
				reportTPG["relaygauge"] = "";
				break;	
		}
		
		cmdText = "INSERT INTO VACMON_TPG300CONFIGS (EQPNAME,PROGRAM,"+
                   "ACARDTYPE,BCARDTYPE,VALUEUNIT,VALUEUNDERRANGE," +
                   "EQPA1,EQPA2,EQPB1, EQPB2," +
                   "RELAYNUMBER,RELAYSOURCE, RELAYGAUGE, RELAYFILTER,RELAYVALID, " +
				   "RELAYSTATUS,RELAYUPTHRESHOLD,RELAYLOWTHRESHOLD," + 
				   "LOCMAINPART,LOCSECTOR,LOCMACHINE,EQPDATE, EQPSECONDNAME)" +
                   "VALUES (:eqpname,:program," +
                   ":acardtype,:bcardtype,:unit,:underrange," +
                   ":eqpa1,:eqpa2,:eqpb1,:eqpb2," +
				   ":relaynumber,:relaysource,:relaygauge,:relayfilter,:relayvalid," +
				   ":relaystatus, :relayupthreshold,:relaylowthreshold," +
                   ":mainpart,:sector,:machine,localtimestamp,:secondname)";
				   
	    fwDbStartCommand(CONNECTION_NAME, cmdText, cmd);
		if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
  
		fwDbBindParams(cmd, reportTPG);
		if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
      
		fwDbExecuteCommand(cmd);
		if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
      
		fwDbFinishCommand(cmd);
		if (fwDbCheckError(errTxt)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};		   
	}
	
	//reportLine["acardtype"] 
	
	//DebugTN();
	
	return true;
	
	
}

/*
**	FUNCTION
**		Sets the DB connection
**	ARGUMENTS
**		connectionName(string) - by default defined in the top of the file
**    database(string) - by default defined in the top of the file
**    username(string) - by default defined in the top of the file
**    password(string) - by default defined in the top of the file
**    driver(string) - by default defined in the top of the file
**  RETURNS
**		true/false
**  CAUTIONS
**		None
*/
bool setDBConnection(string connectionName = CONNECTION_NAME, string database
        = DATABASE, string username = USER, string password = PASSW, string driver = DRIVER){

  if (!isFunctionDefined("fwDbOption")) {
     DebugTN("[VacMon Ctl] ERROR: CtrlRDBAccess library is not available. Check your installation and config file");
     return -1;
  }
    
  string errTxt;
  dbConnection dbCon;

  // check if connection with that name already present
  int rc=fwDbGetConnection(connectionName,dbCon);
  if (rc==-1){
    fwDbCheckError(errTxt,dbCon);
    DebugTN("[VacMon Ctl] ERROR Getting the connection",errTxt);
    return false;
  } else if (rc==1) {
    // connection with such name does not exist yet. Create it
    string connectString = "Driver=" + driver + ";Database=" + database + ";User=" + username + ";Password=" + password;
    fwDbOpenConnection(connectString, dbCon,connectionName);
    if (fwDbCheckError(errTxt,dbCon)){DebugTN("[VacMon Ctl] ERROR WHILE OPENING DATABASE",errTxt);return -1;};
  }
  if(TEST_MODE){
    // get and print the status of the connection via "Inspect" mechanism
    mixed retVal; // note! the argument to fwDbOption must be of type mixed!
    rc=fwDbOption("Inspect",dbCon,retVal);
    DebugTN("[VacMon Ctl] " + rc + retVal);
  }
  return true;
}