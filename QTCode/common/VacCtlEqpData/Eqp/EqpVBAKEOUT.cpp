//	Implementation of EqpVBAKEOUT class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVBAKEOUT.h"

#include "BakeoutWorkDp.h"

#include "DataPool.h"
#include "FunctionalTypeVBAKEOUT.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"
#include "ResourcePool.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <qcolor.h>

#define	RR1_VALID		(0x40000000)
#define	RR1_SAFETY		(0x01000000)

// Color definitions. Every definition is arguments for QColor::setRgb()
#define	COLOR_OK			0,255,0
#define	COLOR_ERROR			255,0,0
#define	COLOR_WARNING		255,255,0
#define	COLOR_UNDEFINED		15,127,192
#define	COLOR_NOT_ACTIVE	0,127,127


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

EqpVBAKEOUT::EqpVBAKEOUT(const Eqp &source) : Eqp(source)
{
	pWorkDp = NULL;
	functionalType = FunctionalType::VBAKEOUT;
}

EqpVBAKEOUT::~EqpVBAKEOUT()
{
}

void EqpVBAKEOUT::setWorkDp(BakeoutWorkDp *pNewWorkDp)
{
	if(pNewWorkDp == pWorkDp)
	{
		return;
	}
	bool emitActivated = pNewWorkDp && (!pWorkDp);
	bool emitDeactivated = pWorkDp && (!pNewWorkDp);
	if(pWorkDp)
	{
		for(int idx = 0 ; idx < pSignalDestinations->count() ; idx++)
		{
			InterfaceEqp *pDest = pSignalDestinations->at(idx);
			disconnect(pDest, DataEnum::Online);
		}
	}
	pWorkDp = pNewWorkDp;
	if(pWorkDp)
	{
		for(int idx = 0 ; idx < pSignalDestinations->count() ; idx++)
		{
			InterfaceEqp *pDest = pSignalDestinations->at(idx);
			connect(pDest, DataEnum::Online);
		}
	}
	if(emitActivated || emitDeactivated)
	{
//		qDebug("Emitting mobileStateChanged(%s)\n", name);
		emit mobileStateChanged(this, DataEnum::Online);
	}
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVBAKEOUT::connect(InterfaceEqp *pDst, DataEnum::DataMode /* mode */)
{
	if(pWorkDp)
	{
//		qDebug("EqpVBAKEOUT::connect(): connecting for destination\n");
		pWorkDp->connectRr1(pDst);
		for(int channel = 1 ; channel <= MAX_BAKEOUT_CHANNEL ; channel++)
		{
			pWorkDp->connectChannelState(pDst, channel);
		}
		for(int channel = 1 ; channel <= MAX_BAKEOUT_CTL_CHANNEL ; channel++)
		{
			pWorkDp->connectCtlChannelState(pDst, channel);
		}
	}
	if(!pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->append(pDst);
	}
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVBAKEOUT::disconnect(InterfaceEqp *pDst, DataEnum::DataMode /* mode */)
{
	if(pWorkDp)
	{
		pWorkDp->disconnectRr1(pDst);
		for(int channel = 1 ; channel <= MAX_BAKEOUT_CHANNEL ; channel++)
		{
			pWorkDp->disconnectChannelState(pDst, channel);
		}
		for(int channel = 1 ; channel <= MAX_BAKEOUT_CTL_CHANNEL ; channel++)
		{
			pWorkDp->disconnectCtlChannelState(pDst, channel);
		}
	}
	if(pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->removeAll(pDst);
	}
}

bool EqpVBAKEOUT::isPlcAlarm(DataEnum::DataMode /* mode */) const
{
	if(pWorkDp)
	{
		return pWorkDp->isPlcAlarm();
	}
	return false;
}

unsigned EqpVBAKEOUT::getRR1(DataEnum::DataMode /* mode */) const
{
	if(pWorkDp)
	{
		return pWorkDp->getRr1();
	}
	return 0;
}

unsigned EqpVBAKEOUT::getChannelState(int channel, DataEnum::DataMode /* mode */) const
{
	if(pWorkDp)
	{
		return pWorkDp->getChannelState(channel);
	}
	return 0;
}

unsigned EqpVBAKEOUT::getCtlChannelState(int channel, DataEnum::DataMode /* mode */) const
{
	if(pWorkDp)
	{
		return pWorkDp->getCtlChannelState(channel);
	}
	return 0;
}

void EqpVBAKEOUT::checkCriteria(EqpMsgCriteria *pCriteria)
{
//qDebug("checkCriteria(%s) - start\n", name);
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}
//qDebug("checkCriteria(%s) - ftype\n", name);

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}
//qDebug("checkCriteria(%s) - type\n", name);

	// Without PLC connection does not match any criteria
	if(isPlcAlarm(DataEnum::Online))
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}
//qDebug("checkCriteria(%s) - no alarm, pWorkDp = %X\n", name, (unsigned)pWorkDp);

	// Connected/disconnected state is checked first
	if(!pWorkDp)
	{
		if((pCriteria->getType() == EqpMsgCriteria::MainState) &&
			(pCriteria->getSubType() == FunctionalTypeVBAKEOUT::Connected))
		{
/*
printf("EqpVBAKEOUT::checkCriteria(%s): not active online, criteria reverse %d\n",
name, pCriteria->isReverse());
fflush(stdout);
*/
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
		}
		else
		{
			pCriteria->setMatch(false, false, 0);
		}
		return;
	}
	else
	{
		if((pCriteria->getType() == EqpMsgCriteria::MainState) &&
			(pCriteria->getSubType() == FunctionalTypeVBAKEOUT::Connected))
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
	}


	// In invalid state - only matches Error/warning criteria
	unsigned rr1Online = getRR1(DataEnum::Online);
//qDebug("checkCriteria(%s) - RR1 = %X\n", name, rr1Online);
	if(!(rr1Online & RR1_VALID))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	// State is valid - decide based on criteria
//	bool isSafety = (rr1Online & RR1_SAFETY) != 0;
//qDebug("checkCriteria(%s) - valid, safety %d, RR1 %X\n", name, (int)isSafety, rr1Online);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case FunctionalTypeVBAKEOUT::Warning:
			// Regulation channels
			for(int channel = 1 ; channel <= MAX_BAKEOUT_CHANNEL ; channel++)
			{
				if(rr1Online & (1<<(channel - 1)))
				{
					int channelState = getChannelState(channel, DataEnum::Online);
//qDebug("    channelState[%d] = %d\n", channel, channelState);
					if((channelState & 0xFF) > 0)
					{
						QString resourceName = "VBAKEOUT.RC.State_";
						resourceName += QString::number(channelState & 0xFF);	// pure error code
						resourceName += ".Kind";
						if(channelState & 0x100)	// SAF bit - safety state of channel
						{
							resourceName += ".SAF";
						}
						int kind;
						if(ResourcePool::getInstance().getIntValue(resourceName, kind) == ResourcePool::OK)
						{
							if(kind >= 1)	// 0 = OK, 1 = Warning, 2 = Error
							{
								pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
								return;
							}
						}
					}
				}
			}
			// Control channels
			for(int channel = 1 ; channel <= MAX_BAKEOUT_CTL_CHANNEL ; channel++)
			{
				int channelState = getCtlChannelState(channel, DataEnum::Online);
//qDebug("    CTL channelState[%d] = %d\n", channel, channelState);
				if((channelState & 0xFF) > 0)
				{
					QString resourceName = "VBAKEOUT.CC.Status_";
					resourceName += QString::number(channelState & 0xFF);
					resourceName += ".Kind";
					int kind;
					if(ResourcePool::getInstance().getIntValue(resourceName, kind) == ResourcePool::OK)
					{
						if(kind >= 1)	// 0 = OK, 1 = Warning, 2 = Error
						{
							pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
							return;
						}
					}
				}
			}
			break;
		case FunctionalTypeVBAKEOUT::Error:
			// Regulation channels
			for(int channel = 1 ; channel <= MAX_BAKEOUT_CHANNEL ; channel++)
			{
				if(rr1Online & (1<<(channel - 1)))
				{
					int channelState = getChannelState(channel, DataEnum::Online);
//qDebug("    channelState[%d] = %d\n", channel, channelState);
					if((channelState & 0xFF) > 0)
					{
						QString resourceName = "VBAKEOUT.RC.State_";
						resourceName += QString::number(channelState & 0xFF);	// pure error code
						resourceName += ".Kind";
						if(channelState & 0x100)	// SAF bit - safety state of channel
						{
							resourceName += ".SAF";
						}
						int kind;
						if(ResourcePool::getInstance().getIntValue(resourceName, kind) == ResourcePool::OK)
						{
							if(kind >= 2)	// 0 = OK, 1 = Warning, 2 = Error
							{
								pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
								return;
							}
						}
					}
				}
			}
			// Control channels
			for(int channel = 1 ; channel <= MAX_BAKEOUT_CTL_CHANNEL ; channel++)
			{
				int channelState = getCtlChannelState(channel, DataEnum::Online);
//qDebug("    CTL channelState[%d] = %d\n", channel, channelState);
				if((channelState & 0xFF) > 0)
				{
					QString resourceName = "VBAKEOUT.CC.Status_";
					resourceName += QString::number(channelState & 0xFF);
					resourceName += ".Kind";
					int kind;
					if(ResourcePool::getInstance().getIntValue(resourceName, kind) == ResourcePool::OK)
					{
						if(kind >= 2)	// 0 = OK, 1 = Warning, 2 = Error
						{
							pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
							return;
						}
					}
				}
			}
			break;
		default:
			break;
		}
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}


