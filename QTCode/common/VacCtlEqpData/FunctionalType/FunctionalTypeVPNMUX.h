#ifndef	FUNCTIONALTYPEVPNMUX_H
#define	FUNCTIONALTYPEVPNMUX_H

// Special functional type for VPNMUX - multiplexor for VP_NEGs

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVPNMUX : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Off = 1,
		On = 2
	};

	FunctionalTypeVPNMUX(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVPNMUX() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVPNMUX_H
