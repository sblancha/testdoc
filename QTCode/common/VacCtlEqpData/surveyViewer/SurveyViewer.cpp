#include "SurveyViewer.h"

#include <qlayout.h>
#include <qfiledialog.h>
#include <qapplication.h>
#include <qcursor.h>
#include <QHeaderView>
#include <QStatusBar>
#include <QMessageBox>

#include "DataPool.h"
#include "Eqp.h"

// Constructor/destructor

SurveyViewer::SurveyViewer(QWidget *parent, Qt::WindowFlags flags) :
	QMainWindow(parent, flags)
{
	setWindowTitle("Survey Data Viewer");

	// Create widgets
	pLineViewer = new LineViewer(this);
	pLineViewer->show();
	pFileButton = new QPushButton("Open file...", this);
//	pLabel = new QLabel(this);
//	pLabel->setText("%");
	pLineTable = new QTableWidget(1, 1, this);
	pLineTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Line"));
//	pLineTable->setHorizontalHeaderItem(1, new QTableWidgetItem("View"));
	pLineTable->setColumnWidth(0, 120);
//	pLineTable->setColumnWidth(1, 40);
	pLineTable->verticalHeader()->hide();
	/*
	pLineTable->setLeftMargin(0);
	pLineTable->setColumnReadOnly(0, true);
	*/
	pLineTable->setMaximumWidth(160);
	pLineTable->setMinimumWidth(160);
	QFont font("Times", 8, QFont::Normal);
	pLineTable->setFont(font);
	connect(pLineTable, SIGNAL(itemClicked(QTableWidgetItem *)), this, SLOT(lineChecked(QTableWidgetItem *)));

	// Connect signals
	connect(pFileButton, SIGNAL(clicked()), this, SLOT(newFile()));
	connect(pLineViewer, SIGNAL(eqpSelected(Eqp *)), this, SLOT(eqpSelected(Eqp *)));

	// Lay out widgets
	QWidget *pContainer = new QWidget(this);
	QGridLayout *pGrid = new QGridLayout(pContainer);	// , 2, 2, 0, 2);
	pGrid->addWidget(pLineViewer, 0, 0, 2, 1);
	pGrid->addWidget(pLineTable, 0, 1);
	pGrid->addWidget(pFileButton, 1, 1);
//	pGrid->addWidget(pLabel, 1, 0);
	pGrid->setColumnStretch(0, 20);
	pGrid->setRowStretch(0, 20);
//	setLayout(pGrid);
/*
	QLayout *pLayout = layout();
	if(pLayout)
	{
		pLayout->addItem(pGrid);
	}
*/
	setCentralWidget(pContainer);
	setStatusBar(new QStatusBar(this));
}

SurveyViewer::~SurveyViewer()
{
}


void SurveyViewer::newFile(void)
{
	QString path = QFileDialog::getExistingDirectory(this, "Choose directory",
		"/home/kopylov");
	if(path.isNull())
	{
		return;
	}
	DataPool &pool = DataPool::getInstance();
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	QStringList errList;
	pool.reset();
	int coco = pool.initPassive(false, true, path.toLatin1().constData(), "ALL", errList);
	QApplication::restoreOverrideCursor();
	if(coco < 0)
	{
		QString message = "pool.initPassive() failed: coco = ";
		message += QString::number(coco);
		message += "\n";
//		printf("Failed: coco = %d:\n", coco);
		foreach(QString msg, errList)
		{
			message += msg;
			message += "\n";
//			printf("%s\n", msg.toAscii().constData());
		}
		QMessageBox::warning(this, "Read data failed", message);
		return;
	}
	QList<BeamLine *> &lines = pool.getLines();
	pLineTable->setRowCount(lines.count());
	for(int row = 0 ; row < lines.count() ; row++)
	{
		BeamLine *pLine = lines.at(row);
		QTableWidgetItem *pItem = new QTableWidgetItem(pLine->getName());
		pItem->setFlags(pItem->flags() | Qt::ItemIsUserCheckable);
		pItem->setCheckState(Qt::Unchecked);
		pLineTable->setItem(row, 0, pItem);
		/*
		QCheckTableItem *pCheck = new QCheckTableItem(pLineTable, "");
		pCheck->setChecked(false);
		pLineTable->setItem(row++, 1, pCheck);
		*/
		pLine->setHidden(true);
	}
	pLineViewer->update();
}

void SurveyViewer::lineChecked(QTableWidgetItem *pItem)
{
	if(!pItem)
	{
		return;
	}
	QString lineName = pItem->text();
	DataPool &pool = DataPool::getInstance();
	BeamLine *pLine = pool.getLine(lineName.toLatin1().constData());
	if(pLine)
	{
		pLine->setHidden(pItem->checkState() != Qt::Checked);
		pLineViewer->update();
	}
}

void SurveyViewer::eqpSelected(Eqp *pEqp)
{
	if(!pEqp)
	{
//		pLabel->setText("%");
		statusBar()->showMessage("");
	}
	else
	{
		char	buf[1024];
		sprintf(buf, "%s: %s @ %f\n", pEqp->getName(), pEqp->getSurveyPart(), pEqp->getStart());
//		pLabel->setText(buf);
		statusBar()->showMessage(buf);
	}
}

