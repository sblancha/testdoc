#include "VacIconGaugePirani.h"

#include <qpainter.h>

/**
@brief FUNCTION PROTECTED draw symbol inside 
@param[in]  painter	Painter object
*/
void VacIconGaugePirani::drawSpecific(QPainter &painter) {
	painter.setPen(Qt::black);
	QPolygon points(8);
	points.setPoint(0, 4, 9);
	points.setPoint(1, 6, 12);
	points.setPoint(2, 7, 12);
	points.setPoint(3, 9, 7);
	points.setPoint(4, 10, 7);
	points.setPoint(5, 13, 12);
	points.setPoint(6, 14, 12);
	points.setPoint(7, 16, 9);
	painter.drawPolyline(points);
}
