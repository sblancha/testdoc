#ifndef	HISTORYEQPDATA_H
#define	HISTORYEQPDATA_H

#include "VacCtlEwoUtilExport.h"

// Class holding history data for one device

#include "HistoryDataRange.h"

#include "Eqp.h"

#include <QList>
#include <QColor>
#include <stdio.h>

#include <QPainter>
class TimeAxis;
class VerticalAxis;
class TimeAxisWidget;
class VerticalAxisWidget;

class HistoryProcessor;

class VACCTLEWOUTIL_EXPORT HistoryEqpData : public InterfaceEqp
{
	Q_OBJECT

public:
	HistoryEqpData(Eqp *pEqp, const QDateTime &absStartTime);
	HistoryEqpData(Eqp *pEqp, const char *dpeSuffixName, DataEnum::ValueType dpeValType, const QString &yAxisTypeName, const QDateTime &absStartTime);
	HistoryEqpData(Eqp *pEqp, const char *dpe, int bitNbr, const char *bitVisibleName, const QDateTime &absStartTime);
	virtual ~HistoryEqpData();

	void setTimeLimits(const QDateTime &start, const QDateTime &end);
	void addHistoryValue(int id, const QDateTime &time, const QVariant &value);
	void finishHistoryRange(int id);
	int findLimits(float &min, float &max);
	void draw(QPainter &painter, TimeAxis *pTimeAxis, VerticalAxis *pValueAxis);
	void draw(QPainter &painter, TimeAxisWidget *pTimeAxis, VerticalAxisWidget *pValueAxis);
	bool getValueAt(QDateTime &time, float &value);

	virtual void setFilterType(HistoryDataRange::FilterType newType);

	void addExportValues(const QDateTime &minTime, const QDateTime &maxTime, HistoryProcessor &processor);

	void dump(FILE *pFile);

	// Access
	inline const Eqp *getEqp(void) const { return pEqp; }
	inline const QByteArray &getDpeName(void) const { return dpeName; }
	inline const QByteArray &getPureDpeName(void) const { return pureDpeName; }
	inline bool isBitState(void) const { return bitState; }
	inline int getBitNbr(void) const { return bitNbr; }
	inline const QString &getAxisTypeName(void) const { return axisTypeName; }
	inline const QDateTime &getCreated(void) const { return created; }
	inline QColor &getColor(void) { return color; }
	inline void setColor(const QColor &newColor) { color = newColor; }
	inline const QDateTime &getAbsStartTime(void) const { return absStartTime; }
	inline void setAbsStartTime(const QDateTime &newTime) { absStartTime = newTime; }
	inline DataEnum::ValueType getValueType(void) const { return valueType; }
	inline float getValueCoeff(void) const { return valueCoeff; }
	inline void setValueCoeff(float coeff) { valueCoeff = coeff; }
	virtual inline bool isSelected(void) const { return selected; }
	virtual inline void setSelected(bool flag) { selected = flag; }

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value,
		DataEnum::DataMode mode, const QDateTime &timeStamp);

signals:
	void onlineValueArrived(void);

protected:
	// Pointer to device for which this instance keeps data
	Eqp			*pEqp;

	// Value type
	DataEnum::ValueType	valueType;

	// Full DPE name for value
	QByteArray	dpeName;

	// Pure DPE name for bit history
	QByteArray	pureDpeName;

	// Visible bit name for bit history
	QByteArray	bitName;

	// Name of Y-axis type
	QString		axisTypeName;

	// Time when this device has been added to history
	QDateTime	created;
  	
	// Absolute start time of historical data
	QDateTime	absStartTime;

	// List of offline history values for device
	QList<HistoryDataRange *>	*pOfflineList;

	// List of online history values for device
	QList<HistoryDataRange *>	*pOnlineList;

	// Pointer to last found range with given ID, used to keep pointer
	// to range with given ID when massive data arrival is processed
	HistoryDataRange			*pCurrentRange;

	// Color used to draw this devices
	QColor		color;

	// Coefficient to be applied to coming values, default is 1
	float		valueCoeff;

	// Bit number for bit history
	int			bitNbr;

	// Filter type
	HistoryDataRange::FilterType filterType;

	// Flag indicating if this bit state history data
	bool		bitState;

	// Flag indicating if this item is selected
	bool		selected;

	// Constructor without arguments
	HistoryEqpData();

	void initMembers(void);

	void addOnlineValue(const QVariant &value, const QDateTime &timeStamp);

	bool splitDpName(const char *dp, char **ppDpName, char **ppDpeName);
};

#endif	// HISTORYEQPDATA_H
