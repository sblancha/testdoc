//	Implementation of MainLHC class
////////////////////////////////////////////////////////////////////////////////////

#include "MainLHC.h"
#include "DataPool.h"

#include <qlayout.h>
#include <qbitmap.h>
#include <qpainter.h>
#include <QHeaderView>
#include <QApplication>

MainLHC::MainLHC(QWidget *parent, Qt::WindowFlags flags)
	: VacMainView(parent, flags)
{
	// Initialize members
	selected = ringSelection = false;
	readOnly = false;

	// Build layout of control
	buildLayout();

	// Connect to signals
	connect(pImage, SIGNAL(mousePress(int, int, int, const char *)),
		this, SLOT(imageMousePress(int, int, int, const char *)));
	connect(pCombo, SIGNAL(activated(const QString &)),
		this, SLOT(comboActivated(const QString &)));
	connect(pAddStartPb, SIGNAL(clicked()), this, SLOT(addAtStart()));
	connect(pDelStartPb, SIGNAL(clicked()), this, SLOT(delAtStart()));
	connect(pAddEndPb, SIGNAL(clicked()), this, SLOT(addAtEnd()));
	connect(pDelEndPb, SIGNAL(clicked()), this, SLOT(delAtEnd()));
	connect(pRedCb, SIGNAL(clicked()), this, SLOT(switchRedCb()));
	connect(pBlueCb, SIGNAL(clicked()), this, SLOT(switchBlueCb()));
	connect(pCryoCb, SIGNAL(clicked()), this, SLOT(switchCryoCb()));
	connect(pQrlCb, SIGNAL(clicked()), this, SLOT(switchQrlCb()));
}

MainLHC::~MainLHC()
{
}

/*
**	FUNCTION
**		Return list of selected sector names
**
**	PARAMETERS
**		allSectors	- true if all sectors in list are needed;
**						false if only first and last sectors are needed
**
** RETURNS
**		List of sector names, list can be empty
**
** CAUTIONS
**		None
*/
QStringList MainLHC::getSelectedSectors(bool allSectors)
{
	QStringList result;
	if(allSectors)
	{
		for(int row = 0 ; row < pTable->rowCount() ; row++)
		{
			result.append(pTable->item(row, 0)->text());
		}
	}
	else
	{
		if(pTable->rowCount() > 0)
		{
			if(pTable->rowCount() == 1)
			{
				result.append(pTable->item(0, 0)->text());
			}
			else
			{
				int startRow = 0;
				DataPool &pool = DataPool::getInstance();

				// Find sector to use. Do not use DSL sector if there >1 sectors in list
				for(startRow = 0 ; startRow < pTable->rowCount() ; startRow++)
				{
					Sector *pSector = pool.findSectorData(pTable->item(startRow, 0)->text().toLatin1());
					if(pSector)
					{
						if(pSector->getVacType() != VacType::DSL)
						{
							result.append(pSector->getName());
							break;
						}
					}
				}

				// Find last sector to use, again ignore DSL sector
				for(int row = pTable->rowCount() - 1 ; row > startRow ; row--)
				{
					Sector *pSector = pool.findSectorData(pTable->item(row, 0)->text().toLatin1());
					if(pSector)
					{
						if(pSector->getVacType() != VacType::DSL)
						{
							result.append(pSector->getName());
							break;
						}
					}
				}
			}
		}
	}
	return result;
}


/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void MainLHC::buildLayout(void)
{
	// Images for +/- buttons
	QBitmap plusImage(7, 7);
	plusImage.clear();
	QPainter painterPlus(&plusImage);
	painterPlus.setPen(Qt::color1);
	painterPlus.drawLine(0, 3, 7, 3);
	painterPlus.drawLine(3, 0, 3, 7);

	QBitmap minusImage(7, 7);
	minusImage.clear();
	QPainter painterMinus(&minusImage);
	painterMinus.setPen(Qt::color1);
	painterMinus.drawLine(0, 3, 7, 3);

	// 1) Main layout - image on the left and all controls on the right
	QHBoxLayout *mainBox = new QHBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);
	pImage = new MainLHCImage(this);
	mainBox->addWidget(pImage, 10);

	// 2) Vertical layout with:
	//	- main part combo box
	//	- horizontal layout with 4 buttons and sector list
	//	- horizontal layout with 4 check boxes
	QVBoxLayout *ctlBox = new QVBoxLayout();
	ctlBox->setContentsMargins(0, 0, 0, 0);
	ctlBox->setSpacing(0);
	mainBox->addLayout(ctlBox);

	pMpBorder = new QGroupBox("Main parts", this);
	pMpBorder->setFlat(true);
//	pMpBorder->setInsideMargin(3);
//	pMpBorder->setInsideSpacing(0);
	ctlBox->addWidget(pMpBorder);

	pCombo = new QComboBox(this);
	pCombo->setEditable(false);
	ctlBox->addWidget(pCombo);

	pSectorBorder = new QGroupBox("Sectors", this);
//	pSectorBorder->setInsideMargin(0);
//	pSectorBorder->setInsideSpacing(0);
	pSectorBorder->setFlat(true);
	ctlBox->addWidget(pSectorBorder);

	QHBoxLayout *sectorCtl = new QHBoxLayout();
	sectorCtl->setContentsMargins(0, 0, 0, 0);
	sectorCtl->setSpacing(0);
	ctlBox->addLayout(sectorCtl);

	QSize buttonSize(15, 15);
	QVBoxLayout *buttonBox = new QVBoxLayout();
	buttonBox->setContentsMargins(0, 0, 0, 0);
	buttonBox->setSpacing(0);
	sectorCtl->addLayout(buttonBox);
	pAddStartPb = new QPushButton(plusImage, NULL, this);
	pAddStartPb->setFixedSize(buttonSize);
#ifndef Q_OS_WIN
//	pAddStartPb->setFlat(true);
#endif
	buttonBox->addWidget(pAddStartPb);

	pDelStartPb = new QPushButton(minusImage, NULL, this);
	pDelStartPb->setFixedSize(buttonSize);
#ifndef Q_OS_WIN
//	pDelStartPb->setFlat(true);
#endif
	buttonBox->addWidget(pDelStartPb);

	buttonBox->addStretch(10);

	pDelEndPb = new QPushButton(minusImage, NULL, this);
	pDelEndPb->setFixedSize(buttonSize);
#ifndef Q_OS_WIN
//	pDelEndPb->setFlat(true);
#endif
	buttonBox->addWidget(pDelEndPb);

	pAddEndPb = new QPushButton(plusImage, NULL, this);
	pAddEndPb->setFixedSize(buttonSize);
#ifndef Q_OS_WIN
//	pAddEndPb->setFlat(true);
#endif
	buttonBox->addWidget(pAddEndPb);

	pTable = new QTableWidget(1, 1, this);
	pTable->verticalHeader()->hide();
	pTable->horizontalHeader()->hide();
//	pTable->setLeftMargin(0);
//	pTable->setTopMargin(0);
//	pTable->setColumnReadOnly(0, true);
	/*
	pTable->setMaximumWidth(160);
	pTable->setMinimumWidth(160);
	*/
//	pTable->setMinimumWidth(50);
	pTable->setMaximumWidth(110);
	sectorCtl->addWidget(pTable, 10);

	QHBoxLayout *check = new QHBoxLayout();
	check->setContentsMargins(0, 0, 0, 0);
	check->setSpacing(0);
	ctlBox->addLayout(check);
	pRedCb = new QCheckBox("R", this);
	check->addWidget(pRedCb);
	pBlueCb = new QCheckBox("B", this);
	check->addWidget(pBlueCb);
	pCryoCb = new QCheckBox("M", this);
	check->addWidget(pCryoCb);
	pQrlCb = new QCheckBox("Q", this);
	check->addWidget(pQrlCb);
}

/*
**	FUNCTION
**		Change 'read-only' state of widget.
**
**	ARGUMENTS
**		flag	- New state of 'read-only'
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void MainLHC::setReadOnly(bool flag)
{
	if(readOnly == flag)
	{
		return;
	}
	readOnly = flag;
	if(readOnly)
	{
		pQrlCb->hide();
		pCryoCb->hide();
		pBlueCb->hide();
		pRedCb->hide();
		pTable->hide();
		pAddEndPb->hide();
		pDelEndPb->hide();
		pDelStartPb->hide();
		pAddStartPb->hide();
		pSectorBorder->hide();
		pCombo->hide();
		pMpBorder->hide();
	}
	else
	{
		pMpBorder->show();
		pCombo->show();
		pSectorBorder->show();
		pAddStartPb->show();
		pDelStartPb->show();
		pDelEndPb->show();
		pAddEndPb->show();
		pTable->show();
		pRedCb->show();
		pBlueCb->show();
		pCryoCb->show();
		pQrlCb->show();
	}
}

void MainLHC::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
	if(readOnly)
	{
		return;
	}
	// Fix width of sector list
/*
//	pTable->setMaximumWidth(pTable->width());
	int maxWidth = width() - height() - 17;
	if(maxWidth < 100)
	{
		maxWidth = 100;
	}
	pTable->setMaximumWidth(maxWidth);
*/
	// Fix widht of the only column
	pTable->setColumnWidth(0, pTable->width() - 4);
}

/*
**	FUNCTION
**		Notify control that data initialization has been finished
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Number of beam lines in data pool
**
**	CAUTIONS
**		None
*/
int MainLHC::dataReady(void)
{
	pImage->reset();
	pImage->renewImage();
	buildMpCombo();
	DataPool &pool = DataPool::getInstance();
	return pool.getLines().count();
}

/*
**	FUNCTION
**		Build content of combo box for main part selection. Only main parts,
**		having at least one sector with draw part on visible beam line, are shown
**		in combo
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::buildMpCombo(void)
{
	if(readOnly)
	{
		return;
	}
	pCombo->clear();
	pCombo->addItem("");
	DataPool &pool = DataPool::getInstance();
	QStringList	strList;
	/* Try to display all main parts, even those which are not visible on main view
	** L.Kopylov 20.10.2010
	pool.findMainPartNames(strList, true);
	*/
	pool.findMainPartNames(strList, false);
	pCombo->addItems(strList);
	pTable->setRowCount(0);
	selected = ringSelection = false;
	pFirstSector = pLastSector = NULL;
	setSectorButtonsSens();
	setCheckBoxesSens();
	emit selectionChanged();
}

/*
**	FUNCTION
**		Mouse has been pressed on sector in image. Depending on what button has been pressed:
**		- left button - update selection within this control
**		- right button - emit signal for outside world
**
**	ARGUMENTS
**		button		- what button was pressed, see Qt::ButtonState enum
**		x			- X-coordinate of mouse pointer
**		y			- Y-coordinate of mouse pointer
**		sectorName	- Name of sector where button has been pressed
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::imageMousePress(int button, int x, int y, const char *sectorName)
{
	/* L.Kopylov 13.09.2012
	if(readOnly)
	{
		return;
	}
	*/
	switch(button)
	{
	case Qt::LeftButton:
		{
			DataPool &pool = DataPool::getInstance();
			Sector *pSector = pool.findSectorData(sectorName);
			Q_ASSERT(pSector);
			selectFromSector(pSector);
		}
		break;
	case Qt::RightButton:
		{
			emit sectorMousePress(2, x, y, sectorName);
		}
		break;
	}
}

/*
**	FUNCTION
**		Make selection based on sector selected in LHC image - select
**		main part of selected sector
**
**	ARGUMENTS
**		pSector	- selected sector
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::selectFromSector(Sector *pSector)
{
	/* L.Kopylov 13.09.2012
	if(readOnly)
	{
		return;
	}
	*/
	int	vacType = pSector->getVacType();
	// Selection is done differently for LHC ring and injection/dump lines
	if(vacType == VacType::Beam)	// Injection or dump line
	{
		MainPart *pMainPart = pSector->getFirstMainPart();
		QStringList sectorList;
		DataPool &pool = DataPool::getInstance();
		pool.findSectorsInMainPart(pMainPart->getName(), sectorList, true);
		setLineSelection(sectorList);
	}
	else	// LHC main ring
	{
		buildRangeFromSector(pSector);
	}
}

/*
**	FUNCTION
**		Slot - item has been selected in main part combo box
**
**	ARGUMENTS
**		string	- String of selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::comboActivated(const QString &string)
{
	DataPool &pool = DataPool::getInstance();
	MainPart *pMainPart = pool.findMainPartData(string.toLatin1());
	if(!pMainPart)	// No selection
	{
		QStringList sectorList;
		mainVacMask = vacMask = VacType::None;
		pFirstSector = pLastSector = NULL;
		setRingSelection(sectorList);
		return;
	}

	// Selection is done differently for LHC ring and injection/dump lines
	if(pMainPart->getVacTypeMask() == VacType::Beam)	// injection/dump lines
	{
		QStringList	sectorList;
		/* Try to allow selection of all sectors, even not visible
		** L.Kopylov 20.10.2010
		pool.findSectorsInMainPart(pMainPart->getName(), sectorList, true);
		*/
		pool.findSectorsInMainPart(pMainPart->getName(), sectorList, false);
		setLineSelection(sectorList);
	}
	else	// LHC ring selection
	{
		QStringList sectorList;
		QList<MainPart *> mpList;
		mpList.append(pMainPart);
		mainVacMask = vacMask = VacType::None;
		buildRangeFromMps(mpList, sectorList);
		mainVacMask = vacMask;
		setRingSelection(sectorList);
	}
}

/*
**	FUNCTION
**		Slot - '+' button at the beginning of sector list has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::addAtStart(void)
{
	if(ringSelection)
	{
		changeRangeLimits(true, 1);
	}
	else
	{
		DataPool &pool = DataPool::getInstance();
		Sector *pSector = pool.findNeighbourSector(pTable->item(0, 0)->text().toLatin1(), NULL, -1);
		if(pSector)
		{
			pTable->setRowCount(pTable->rowCount() + 1);
			for(int n = pTable->rowCount() - 1 ; n >= 0 ; n--)
			{
				setTableCellText(n, 0, pTable->item(n - 1, 0)->text());
			}
			setTableCellText(0, 0, pSector->getName());
			pTable->resizeRowsToContents();
		}
		setLineSelectionFromTable();
	}
}

/*
**	FUNCTION
**		Slot - '-' button at the beginning of sector list has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::delAtStart(void)
{
	if(ringSelection)
	{
		changeRangeLimits(true, -1);
	}
	else
	{
		pTable->removeRow(0);
		setLineSelectionFromTable();
	}
}

/*
**	FUNCTION
**		Slot - '-' button at the end of sector list has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::delAtEnd(void)
{
	if(ringSelection)
	{
		changeRangeLimits(false, -1);
	}
	else
	{
		pTable->removeRow(pTable->rowCount() - 1);
		setLineSelectionFromTable();
	}
}

/*
**	FUNCTION
**		Slot - '+' button at the end of sector list has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::addAtEnd(void)
{
	if(ringSelection)
	{
		changeRangeLimits(false, 1);
	}
	else
	{
		DataPool &pool = DataPool::getInstance();
		Sector *pSector = pool.findNeighbourSector(pTable->item(pTable->rowCount() - 1, 0)->text().toLatin1(), NULL, 1);
		if(pSector)
		{
			pTable->setRowCount(pTable->rowCount() + 1);
			setTableCellText(pTable->rowCount() - 1, 0, pSector->getName());
			pTable->resizeRowsToContents();
		}
		setLineSelectionFromTable();
	}
}

/*
**	FUNCTION
**		Slot - red beam check box has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::switchRedCb(void)
{
	changeVacSelection(VacType::RedBeam, pRedCb->isChecked());
}

/*
**	FUNCTION
**		Slot - blue beam check box has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::switchBlueCb(void)
{
	changeVacSelection(VacType::BlueBeam, pBlueCb->isChecked());
}

/*
**	FUNCTION
**		Slot - CRYO beam check box has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::switchCryoCb(void)
{
	changeVacSelection(VacType::Cryo, pCryoCb->isChecked());
}

/*
**	FUNCTION
**		Slot - QRL beam check box has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::switchQrlCb(void)
{
	changeVacSelection(VacType::Qrl, pQrlCb->isChecked());
}

/*
**	FUNCTION
**		Change vacuum type selection on main LHC ring in response to
**		check boxes check/uncheck
**
**	ARGUMENTS
**		vacType	- Vacuum type mask corresponding to check box
**		isSet	- New state of check box
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::changeVacSelection(int vacType, bool isSet)
{
	if(isSet)
	{
		vacMask |= vacType;
	}
	else
	{
		vacMask &= ~vacType;
	}
	changeRangeType();
}

/*
**	FUNCTION
**		Set selection on LHC ring.
**
**	ARGUMENTS
**		sectorNames	- List of sector names to be selected
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::setRingSelection(QStringList &sectorNames)
{
	ringSelection = true;
	selected = sectorNames.count() > 0;
	pTable->setRowCount(sectorNames.count());
	int	n = 0;
	foreach(QString sectorName, sectorNames)
	{
		setTableCellText(n++, 0, sectorName);
	}
	pTable->resizeRowsToContents();
	setMainPartName();
	// Update state of checkboxes with vacuum types
	pRedCb->setChecked((vacMask & VacType::RedBeam) != 0);
	pBlueCb->setChecked((vacMask & VacType::BlueBeam) != 0);
	pCryoCb->setChecked((vacMask & VacType::Cryo) != 0);
	pQrlCb->setChecked((vacMask & VacType::Qrl) != 0);

	pImage->setRingSelection(pFirstSector, pLastSector);

	emit selectionChanged();
}

/*
**	FUNCTION
**		Set selection on injection/dump lines.
**
**	ARGUMENTS
**		sectorList - list of selected sector names
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::setLineSelection(QStringList &sectorList)
{
	pImage->setLineSelection(sectorList);
	pTable->setRowCount(sectorList.count());
	int n = 0;
	foreach(QString sectorName, sectorList)
	{
		setTableCellText(n++, 0, sectorName);
	}
	pTable->resizeRowsToContents();
	setLineSelectionFromTable();
}

/*
**	FUNCTION
**		Set selection on injection/dump lines.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Sector list shall be filled in before calling this method
*/
void MainLHC::setLineSelectionFromTable(void)
{
	ringSelection = false;
	selected = pTable->rowCount() > 0;
	if(selected)
	{
		DataPool &pool = DataPool::getInstance();
		pFirstSector = pool.findSectorData(pTable->item(0, 0)->text().toLatin1());
		pLastSector = pool.findSectorData(pTable->item(pTable->rowCount() - 1, 0)->text().toLatin1());
	}
	else
	{
		pFirstSector = pLastSector = NULL;
	}
	mainVacMask = vacMask = VacType::Beam;
	setMainPartName();
	emit selectionChanged();
}

/*
**	FUNCTION
**		Set name of main part shown in combo box.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		List of sectors shall be updated BEFORE invoking this method
*/
void MainLHC::setMainPartName(void)
{
	MainPart	*pMainPart = NULL;
	DataPool	&pool = DataPool::getInstance();
	for(int n = pTable->rowCount() - 1 ; n >= 0 ; n--)
	{
		Sector *pSector = pool.findSectorData(pTable->item(n, 0)->text().toLatin1());
		if(pMainPart)
		{
			if(!pSector->isInMainPart(pMainPart))
			{
				pMainPart = NULL;	// Different main parts
				break;
			}
		}
		else	// Take 1st main part of sector
		{
			const QList<MainPartMap *> &mapList = pSector->getMainPartMaps();
			if (!mapList.isEmpty()) {
				MainPartMap *pMap = mapList.first();
				pMainPart = pMap->getMainPart();
			}
		}
	}
	if(pMainPart)
	{
		pCombo->setCurrentIndex(pCombo->findText(pMainPart->getName()));
	}
	else
	{
		pCombo->setCurrentIndex(0);
	}
	setSectorButtonsSens();
	setCheckBoxesSens();
}

/*
**	FUNCTION
**		Set sensitivity of add/remove buttons, controlling sector list
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::setSectorButtonsSens(void)
{
	if(readOnly)
	{
		return;
	}
	if(!pTable->rowCount())	// List is empty
	{
		pAddStartPb->setEnabled(false);
		pDelStartPb->setEnabled(false);
		pAddEndPb->setEnabled(false);
		pDelEndPb->setEnabled(false);
		return;
	}
	pDelStartPb->setEnabled(true);
	pDelEndPb->setEnabled(true);

	if(ringSelection)	// Selection on LHC ring
	{
		pAddStartPb->setEnabled(canExtendRange(true));
		pAddEndPb->setEnabled(canExtendRange(false));
	}
	else	// Selection on injection/dump lines
	{
		DataPool &pool = DataPool::getInstance();
		Sector *pSector = pool.findNeighbourSector(pTable->item(0, 0)->text().toLatin1(), NULL, -1);
		if(pSector)
		{
			pAddStartPb->setEnabled(!tableContains(pSector->getName()));
		}
		else
		{
			pAddStartPb->setEnabled(false);
		}
		pSector = pool.findNeighbourSector(pTable->item(pTable->rowCount() - 1, 0)->text().toLatin1(), NULL, 1);
		if(pSector)
		{
			pAddEndPb->setEnabled(!tableContains(pSector->getName()));
		}
		else
		{
			pAddEndPb->setEnabled(false);
		}
	}
}

/*
**	FUNCTION
**		Set sensitivity of vacuum type selection checkboxes
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::setCheckBoxesSens(void)
{
	pRedCb->setEnabled(ringSelection ? ((mainVacMask & VacType::RedBeam) == 0) : false); 
	pBlueCb->setEnabled(ringSelection ? ((mainVacMask & VacType::BlueBeam) == 0) : false); 
	pQrlCb->setEnabled(ringSelection ? ((mainVacMask & VacType::Qrl) == 0) : false); 
	pCryoCb->setEnabled(ringSelection ? ((mainVacMask & VacType::Cryo) == 0) : false);
	if(!ringSelection)
	{
		pRedCb->setChecked(false);
		pBlueCb->setChecked(false);
		pQrlCb->setChecked(false);
		pCryoCb->setChecked(false);
	}
}

/*
**	FUNCTION
**		Check if sector list contains item with given name
**
**	ARGUMENTS
**		name	- Name of sector to check
**
**	RETURNS
**		true	- if such name found in sector list;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool MainLHC::tableContains(const char *name)
{
	for(int n = pTable->rowCount() - 1 ; n >= 0 ; n--)
	{
		if(pTable->item(n, 0)->text() == name)
		{
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Build LHC ring selection range for one sector selected.
**		When selected sector is either Q or M - method includes also
**		all sectors of 'adjucent' main part (Q->M, M->Q). However,
**		resulting mainVacMask still contains ONLY type of selected sector
**
**	PARAMETERS
**    	pSector		- Pointer to selected sector
**
**	RETURNS
** 		None
**
** CAUTIONS
**		pSector shall not be NULL
**
** FUNCTION DEFINITION
*/
void MainLHC::buildRangeFromSector(Sector *pSector)
{
	vacMask = mainVacMask = VacType::None;
	QList<MainPart *> mpList;
	if(pSector->getVacType() == VacType::Qrl)
	{
		mainVacMask = VacType::Qrl;
		mpList.append(pSector->getFirstMainPart());
		MainPart *pAdjucentMp = getAdjucentMainPart(mpList.first(), VacType::Cryo);
		if(pAdjucentMp)
		{
			mpList.append(pAdjucentMp);
		}
	}
	else if(pSector->getVacType() == VacType::Cryo)
	{
		mainVacMask = VacType::Cryo;
		mpList.append(pSector->getFirstMainPart());
		MainPart *pAdjucentMp = getAdjucentMainPart(mpList.first(), VacType::Qrl);
		if(pAdjucentMp)
		{
			mpList.append(pAdjucentMp);
		}
	}
	else
	{
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int idx = 0 ; idx < maps.count() ; idx++)
		{
			MainPartMap *pMap = maps.at(idx);
			mpList.append(pMap->getMainPart());
		}
	}
	QStringList sectorList;
	buildRangeFromMps(mpList, sectorList);
//	mainVacMask = vacMask;
	if((vacMask & (VacType::Qrl | VacType::Cryo)) != 0)
	{
		vacMask = VacType::Qrl | VacType::Cryo;
	}
	else
	{
		mainVacMask = vacMask;
	}
	setRingSelection(sectorList);
}

/*
**	FUNCTION
**    Check if current selection range on LHC ring can be extended.
**
**	EXTERNAL
**
**
**	PARAMETERS
**    changeAtStart	- true if start of selection shall be changed, false otherwise
**
**	RETURNS
**    true	- If selection range can be extended;
**    false	- otherwise.
**
**	CAUTIONS
**    NONE
*/
bool MainLHC::canExtendRange(bool changeAtStart)
{
	Sector		*pLimitSector, *pSector = NULL;
	DataPool	&pool = DataPool::getInstance();
	QList<Sector *> &sectors = pool.getSectors();

	// Locate iterator on start sector for search
	pLimitSector = changeAtStart ? pFirstSector : pLastSector;
	int sectIdx;
	for(sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		pSector = sectors.at(sectIdx);
		if(pSector == pLimitSector)
		{
			break;
		}
	}
	Q_ASSERT(pSector);

	// Start search from current iterator location
	pLimitSector = changeAtStart ? pLastSector : pFirstSector;
	while(true)
	{
		if(changeAtStart)
		{
			sectIdx--;
			if(sectIdx < 0)
			{
				sectIdx = sectors.count() - 1;
			}
		}
		else
		{
			sectIdx++;
			if(sectIdx >= sectors.count())
			{
				sectIdx = 0;
			}
		}
		pSector = sectors.at(sectIdx);
		if(pSector == pLimitSector)
		{
			break;
		}
		switch(pSector->getVacType())
		{
		case VacType::CrossBeam:
		case VacType::CommonBeam:
			if(mainVacMask & (VacType::RedBeam | VacType::BlueBeam))
			{
				return true;
			}
			break;
		case VacType::Qrl:	// Selection is limited to one main part
		case VacType::Cryo:
			if(pSector->getVacType() != mainVacMask)
			{
				break;	// ????? LIK 12.09.2008
			}
			return pSector->hasCommonMainPart(changeAtStart ? pFirstSector : pLastSector);
		default:		// Any sector of the same vacuum type
			if((pSector->getVacType() & mainVacMask) == pSector->getVacType())
			{
				return true;
			}
			break;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Modify limits of LHC ring selection range.
**		This is helper method for LHC main view
**
**	PARAMETERS
**		changeAtStart	- true if start of selection shall be changed, false otherwise
**		direction		- direction for range change (1 - extend, -1 - shrink)
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::changeRangeLimits(bool changeAtStart, int direction)
{
	QStringList	sectorList;
	// Shrink one sector selection
	if((pFirstSector == pLastSector) && (direction < 0))
	{
		pFirstSector = pLastSector = NULL;
		mainVacMask = vacMask = VacType::None;
		setRingSelection(sectorList);
		return;
	}

	// Find neighbour sector for given mainVacType
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	Sector *pLimitSector;
	int sectIdx;
	if(changeAtStart)
	{
		for(sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
		{
			Sector *pSector = sectors.at(sectIdx);
			if(pSector == pFirstSector)
			{
				break;
			}
		}
		// Shrink unlimited, extend up to last sector
		pLimitSector = direction < 0 ? NULL : pLastSector;
		direction = - direction;	// !!!!!!
	}
	else
	{
		for(sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
		{
			Sector *pSector = sectors.at(sectIdx);
			if(pSector == pLastSector)
			{
				break;
			}
		}
		// Shrink unlimited, extend up to first sector
		pLimitSector = direction < 0 ? NULL : pFirstSector;
	}
	while(true)
	{
		Sector *pSector;
		if(direction < 0)
		{
			sectIdx--;
			if(sectIdx < 0)
			{
				sectIdx = sectors.count() - 1;
			}
		}
		else
		{
			sectIdx++;
			if(sectIdx >= sectors.count())
			{
				sectIdx = 0;
			}
		}
		pSector = sectors.at(sectIdx);
		if(pSector == pLimitSector)
		{
			break;
		}
		bool useThisSector = false;
		switch(pSector->getVacType())
		{
		case VacType::CrossBeam:
		case VacType::CommonBeam:
			if(mainVacMask & (VacType::RedBeam | VacType::BlueBeam))
			{
				useThisSector = true;
			}
			break;
		default:
			if((pSector->getVacType() & mainVacMask) == pSector->getVacType())
			{
				useThisSector = true;
			}
			break;
		}
		if(useThisSector)
		{
			if(changeAtStart)
			{
				pFirstSector = pSector;
			}
			else
			{
				pLastSector = pSector;
			}
			break;
		}
	}

	// Build sector list for new range
	changeRangeType();
}

/*
**	FUNCTION
**		Build LHC ring selection range for new vacuum type mask in
**		previously defined range of ring sectors. This methiod shall NOT
**		modify pFirstSector and pLastSector - it only builds list of sectors
**		in a range given by these two sectors.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainLHC::changeRangeType(void)
{
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	Sector *pSector = NULL;
	int sectIdx;
	Sector *pStartSector = pFirstSector,
			*pEndSector = pLastSector;
	if(vacMask != mainVacMask)
	{
		// Find new start sector. It can be sector overlapping with the very first sector
		for(sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
		{
			pSector = sectors.at(sectIdx);
			if(pSector == pLastSector)
			{
				break;
			}
		}
		while(true)
		{
			sectIdx++;
			if(sectIdx >= sectors.count())
			{
				sectIdx = 0;
			}
			pSector = sectors.at(sectIdx);
			if(pSector == pFirstSector)
			{
				break;
			}
			if(!areRingRangesOverlap(pFirstSector->getStart(), pLastSector->getEnd(),
				pSector->getStart(), pSector->getEnd(), false))
			{
				break;
			}
		}
		while(pSector != pFirstSector)
		{
			sectIdx++;
			if(sectIdx >= sectors.count())
			{
				sectIdx = 0;
			}
			pSector = sectors.at(sectIdx);
			if(pSector == pFirstSector)
			{
				break;
			}
			if(areRingRangesOverlap(pFirstSector->getStart(), pLastSector->getEnd(),
				pSector->getStart(), pSector->getEnd(), false))
			{
				pStartSector = pSector;
				break;
			}
		}
		// Find new last sector. It can be sector overlapping with the very last sector
		for(sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
		{
			pSector = sectors.at(sectIdx);
			if(pSector == pLastSector)
			{
				break;
			}
		}
		while(true)
		{
			sectIdx++;
			if(sectIdx >= sectors.count())
			{
				sectIdx = 0;
			}
			pSector = sectors.at(sectIdx);
			if(pSector == pFirstSector)
			{
				break;
			}
			if(!areRingRangesOverlap(pFirstSector->getStart(), pLastSector->getEnd(),
				pSector->getStart(), pSector->getEnd(), false))
			{
				break;
			}
			pEndSector = pSector;
		}
	}
	// Build resulting list of sector names of appropriate vacuum type
	QStringList sectorList;
	for(sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		pSector = sectors.at(sectIdx);
		if(pSector == pStartSector)
		{
			break;
		}
	}
	while(true)
	{
		bool useThisSector = false;
		// check vacuum type
		switch(pSector->getVacType())
		{
		case VacType::CrossBeam:
		case VacType::CommonBeam:
			useThisSector = (vacMask & (VacType::RedBeam | VacType::BlueBeam)) != 0;
			break;
		default:
			useThisSector = (vacMask & pSector->getVacType()) != 0;
			break;
		}
		if(useThisSector)
		{
			if(areRingRangesOverlap(pFirstSector->getStart(), pLastSector->getEnd(),
				pSector->getStart(), pSector->getEnd(), false))
			{
				addSectorToList(sectorList, pSector);
			}
		}
		if(pSector == pEndSector)
		{
			break;
		}
		sectIdx++;
		if(sectIdx >= sectors.count())
		{
			sectIdx = 0;
		}
		pSector = sectors.at(sectIdx);
	}
	setRingSelection(sectorList);
}

/*
**	FUNCTION
**		Check if two coordinate ranges on the ring overlap
**
**	PARAMETERS
**		outerStart		- Coordinate for 'outer' range start
**		outerEnd		- Coordinate for 'outer' range end
**		innserStart		- Coordinate for 'inner' range start
**		innerEnd		- Coordinate for 'inner' range end
**		isRecursiveCall	- True if this is recursive call
**
** RETURNS
**		true	- if ranges overlap;
**		false	- otherwise
**
** CAUTIONS
**		The method shall be called with isRecursiveCall = false: it is used internally
**		to prevent infinite call depth
**
** FUNCTION DEFINITION
*/
bool MainLHC::areRingRangesOverlap(float outerStart, float outerEnd,
	float innerStart, float innerEnd, bool isRecursiveCall)
{
	if((innerStart == outerStart) && (innerEnd == outerEnd))
	{
		return true;
	}
	if(outerStart > outerEnd)	// Outer range includes ring start
	{
		if(innerStart > innerEnd)	// Inner range also includes ring start
		{
			return true;
		}
		if(innerStart > outerStart)
		{
			return true;
		}
		if(innerEnd > outerStart)
		{
			return true;
		}
		if(innerEnd < outerEnd)
		{
			return true;
		}
		if(innerStart < outerEnd)
		{
			return true;
		}
	}
	else	// Outer range does not include ring start
	{
		// Is inner range at least partially within the outer?
		if((outerStart < innerStart) && (innerStart < outerEnd))
		{
			return true;
		}
		if((outerStart < innerEnd) && (innerEnd < outerEnd))
		{
			return true;
		}
		// Is the outer range at least partially within inner?
		if(!isRecursiveCall)
		{
			return areRingRangesOverlap(innerStart, innerEnd, outerStart, outerEnd, true);
		}
	}
	return false;
}

void MainLHC::setTableCellText(int row, int col, const QString &text)
{
	if(pTable->item(row, col))
	{
		pTable->item(row, col)->setText(text);
	}
	else
	{
		pTable->setItem(row, col, new QTableWidgetItem(text));
	}
}

////////////////////////////////////////////////////////////////////////////////////////
////////////////// Access //////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
void MainLHC::setMainFont(const QFont &newFont)
{
printf("MainLHC::setMainFont(): %s\n", newFont.toString().toLatin1().constData());
fflush(stdout);
	pImage->setFont(newFont);
	pCombo->setFont(newFont);
	pTable->setFont(newFont);
	pRedCb->setFont(newFont);
	pRedCb->setFont(newFont);
	pBlueCb->setFont(newFont);
	pCryoCb->setFont(newFont);
	pQrlCb->setFont(newFont);
	QWidget::setFont(newFont);
//	QApplication::setFont(newFont);
	pImage->renewImage();
printf("MainLHC::setMainFont(): result %s\n", font().toString().toLatin1().constData());
fflush(stdout);
}


////////////////////////////////////////////////////////////////////////////////////////
////////////////// Implementation of VacMainView abstract methods //////////////////////
////////////////////////////////////////////////////////////////////////////////////////
void MainLHC::dialogRequest(int type, unsigned vacTypes, const QStringList &sectorList, DataEnum::DataMode mode)
{
	emit dialogRequested(type, vacTypes, sectorList, mode);
}

