//	Implementation of ProfileLhcDialog class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileLhcDialog.h"

#include "ProfileLhcView.h"

#include "Eqp.h"
#include "VacIcon.h"

#include "VacMainView.h"
#include "Sector.h"
#include "DataPool.h"
#include "ResourcePool.h"

#include <qpushbutton.h>
#include <qmenu.h>
#include <qlabel.h>
#include <qcombobox.h>
#include <qlayout.h>
#include <qdesktopwidget.h>
#include <qapplication.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

ProfileLhcDialog::ProfileLhcDialog(unsigned vacTypeMask, Sector *pFirstSector,
	Sector *pLastSector, DataEnum::DataMode mode) :
	QDialog(NULL /* VacMainView::getInstance() */, Qt::Window),
	LhcRingSelection()
{
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle("Profile LHC");
	beamVacLimitsChanged = cryoVacLimitsChanged = cryoTempLimitsChanged = false;
	this->vacTypeMask = vacTypeMask;
	this->pFirstSector = pFirstSector;
	this->pLastSector = pLastSector;
	this->mode = mode;
	pActionSectorNames = NULL;
	pView = NULL;

	/*
	const QList<VacMainView *> &mainList = VacMainView::getInstanceList();
	for(int idx = 0 ; idx < mainList.count() ; idx++)
	{
		VacMainView *pInstance = mainList.at(idx);
		if(pInstance->isPvssEvents())
		{
			QObject::connect(this, SIGNAL(profileDpDown(int, int, int, int, int, const char *)),
				pInstance, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
		}
	}
	*/
	VacMainView *pMainView = VacMainView::getInstance();
	if(pMainView)
	{
		QObject::connect(this, SIGNAL(profileDpDown(int, int, int, int, int, const char *)),
			pMainView, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
	}

	buildLayout();
	setTitle();
	buildInitialEqpMask();
	buildView(true);
	if(mode == DataEnum::Replay)
	{
		VacMainView::replayDialogOpened(this);
	}
}

ProfileLhcDialog::~ProfileLhcDialog()
{
	if(mode == DataEnum::Replay)
	{
		VacMainView::replayDialogDeleted(this);
	}
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileLhcDialog::buildLayout(void)
{
	// 1) Main layout - menu, labels and combobox on top, scrolled view on bottom
	mainBox = new QVBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);

	// 2) Horizontal layout with
	//	- buttons for activating popup menus
	//	- 2 icons for beam intensities
	//	- title label
	//	- ComboBox with main part selection
	QHBoxLayout *topBox = new QHBoxLayout();
	topBox->setSpacing(0);
	mainBox->addLayout(topBox);

	// 2.1 all popup menus
	pFilePb = new QPushButton("&File", this);
	pFilePb->setFlat(true);
	QSize size = pFilePb->fontMetrics().size(Qt::TextSingleLine, "File");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pFilePb->setFixedSize(size);
	buildFileMenu();
	pFilePb->setMenu(pFileMenu);
	topBox->addWidget(pFilePb);

	pViewPb = new QPushButton("&View", this);
	pViewPb->setFlat(true);
	size = pViewPb->fontMetrics().size(Qt::TextSingleLine, "View");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pViewPb->setFixedSize(size);
	buildViewMenu();
	pViewPb->setMenu(pViewMenu);
	topBox->addWidget(pViewPb);

	pHelpPb = new QPushButton("&Help", this);
	pHelpPb->setFlat(true);
	size = pHelpPb->fontMetrics().size(Qt::TextSingleLine, "Help");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pHelpPb->setFixedSize(size);
	buildHelpMenu();
	pHelpPb->setMenu(pHelpMenu);
	topBox->addWidget(pHelpPb);


	// 2.2 icons for beam intensity display
	QList<Eqp *> intList;
	getBeamIntesityEqp(intList);
	for(int idx = 0 ; idx < intList.count() ; idx++)
	{
		Eqp *pEqp = intList.at(idx);
		VacIcon *pIcon = VacIcon::getIcon(pEqp, this);
		if(pIcon)
		{
			pIcon->setEqp(pEqp);
			pIcon->setMode(mode);
			pIcon->connect();
			topBox->addSpacing(5);
			topBox->addWidget(pIcon);
		}
	}

	// 2.3 profile title
	pTitleLabel = new QLabel("Profile", this);
	topBox->addWidget(pTitleLabel, 10);	// The only resizable widget on top of dialog
	pTitleLabel->setAlignment(Qt::AlignHCenter);

	// 2.4 Combo box for main part selection
	buildMainPartCombo();
	topBox->addWidget(pCombo);
}

/*
**	FUNCTION
**		Build 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileLhcDialog::buildFileMenu(void)
{
	pFileMenu = new QMenu(this);
	pFileMenu->addAction("Print...", this, SLOT(filePrint()));
	pFileMenu->addSeparator();
	pFileMenu->addAction("Close", this, SLOT(deleteLater()));
}

/*
**	FUNCTION
**		Build 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileLhcDialog::buildViewMenu(void)
{
	// First build masks containing all possible types for profile on
	// corresponding vacuum type
	allCryoEqpMask.append(FunctionalType::VGM);
	allCryoEqpMask.append(FunctionalType::VGR);
	allCryoEqpMask.append(FunctionalType::VGP);
	allCryoEqpMask.append(FunctionalType::VPGMPR);
	allCryoEqpMask.append(FunctionalType::CRYO_TT);
	allCryoEqpMask.append(FunctionalType::CRYO_TT_EXT);

	allBeamEqpMask.append(FunctionalType::VGM);
	allBeamEqpMask.append(FunctionalType::VGR);
	allBeamEqpMask.append(FunctionalType::VGP);
	allBeamEqpMask.append(FunctionalType::VGI);
	allBeamEqpMask.append(FunctionalType::VGTR);
	allBeamEqpMask.append(FunctionalType::VPI);
	allBeamEqpMask.append(FunctionalType::VPGMPR);
	allBeamEqpMask.append(FunctionalType::CRYO_TT);
	allBeamEqpMask.append(FunctionalType::CRYO_TT_EXT);

	pViewMenu = new QMenu(this);

	// Vacuum type submenu
	pVacSubMenu = pViewMenu->addMenu("Vacuum");
	QAction *pAction = pVacSubMenu->addAction("QRL");
	pAction->setData(VacType::Qrl);
	pAction->setCheckable(true);
	pAction->setChecked(vacTypeMask & VacType::Qrl);
	vacTypeActions.append(pAction);

	pAction = pVacSubMenu->addAction("CRYO");
	pAction->setData(VacType::Cryo);
	pAction->setCheckable(true);
	pAction->setChecked(vacTypeMask & VacType::Cryo);
	vacTypeActions.append(pAction);

	pAction = pVacSubMenu->addAction("Blue Beam");
	pAction->setData(VacType::BlueBeam);
	pAction->setCheckable(true);
	pAction->setChecked(vacTypeMask & VacType::BlueBeam);
	vacTypeActions.append(pAction);

	pAction = pVacSubMenu->addAction("Red Beam");
	pAction->setData(VacType::RedBeam);
	pAction->setCheckable(true);
	pAction->setChecked(vacTypeMask & VacType::RedBeam);
	vacTypeActions.append(pAction);
	
	// Equipment on isolation vacuum submenu
	pCryoEqpSubMenu = pViewMenu->addMenu("Equipment on isol. vacuum");
	const QList<FunctionalType *> &cryoTypes = allCryoEqpMask.getList();
	FunctionalType *pItem;
	int idx;
	for(idx = 0 ; idx < cryoTypes.count() ; idx++)
	{
		pItem = cryoTypes.at(idx);
		pAction = pCryoEqpSubMenu->addAction(pItem->getDescription());
		pAction->setCheckable(true);
		pAction->setData(pItem->getType());
		pAction->setChecked(cryoEqpMask.contains(pItem->getType()));
		cryoTypeActions.append(pAction);
	}

	// Equipment on beam vacuum submenu
	pBeamEqpSubMenu = pViewMenu->addMenu("Equipment on beam vacuum");
	const QList<FunctionalType *> &beamTypes = allBeamEqpMask.getList();
	for(idx = 0 ; idx < beamTypes.count() ; idx++)
	{
		pItem = beamTypes.at(idx);
		pAction = pBeamEqpSubMenu->addAction(pItem->getDescription());
		pAction->setCheckable(true);
		pAction->setData(pItem->getType());
		pAction->setChecked(beamEqpMask.contains(pItem->getType()));
		beamTypeActions.append(pAction);
	}

	// Show/hide sector names
	pViewMenu->addSeparator();
	pActionSectorNames = pViewMenu->addAction("Sector Names");
	pActionSectorNames->setData(111999);
	pActionSectorNames->setCheckable(true);
	ResourcePool &pool = ResourcePool::getInstance();
	QString resourceName = "Profile.ShowSectorNames";
	bool flag = true;
	pool.getBoolValue(resourceName, flag);
	pActionSectorNames->setChecked(true); // Set sector names true as default
	connect(pActionSectorNames, SIGNAL(toggled(bool)), this, SLOT(sectNamesToggled(bool)));

	// Open other views
	pViewMenu->addSeparator();
	pViewMenu->addAction("Sector...", this, SLOT(viewSector()));
	pViewMenu->addAction("Synoptic...", this, SLOT(viewSynoptic()));
	connect(pViewMenu, SIGNAL(triggered(QAction *)), this, SLOT(typeMenu(QAction *)));
}

/*
**	FUNCTION
**		Build 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileLhcDialog::buildHelpMenu(void)
{
	pHelpMenu = new QMenu(this);
	pHelpMenu->addAction("User manual...", this, SLOT(help()));
}

/*
**	FUNCTION
**		Build main part combo box, insert main part names which can be selected
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileLhcDialog::buildMainPartCombo(void)
{
	pCombo = new QComboBox(this);
	pCombo->setEditable(false);
	pCombo->clear();
	DataPool &pool = DataPool::getInstance();
	QStringList strList;
	pool.findMainPartNames(strList, true);
	QList<Sector *> &sectors = pool.getSectors();
	foreach(QString mpName, strList)
	{
		MainPart *pMainPart = pool.findMainPartData(mpName.toLatin1());
		unsigned vacType = pMainPart->getVacTypeMask();
		if(!(vacType & vacTypeMask))
		{
			continue;
		}
		bool skip = false;
		for(int idx = 0 ; idx < sectors.count() ; idx++)
		{
			Sector *pSector = sectors.at(idx);
			if(pSector->getSpecSynPanel().isEmpty())
			{
				continue;
			}
			if(pSector->isOuter())
			{
				continue;
			}
			if(pSector->isInMainPart(pMainPart))
			{
				skip = true;
				break;
			}
		}
		if(!skip)
		{
			// if(pool.areSectorsOfMainPartContinuous(pMainPart))
			{
				pCombo->addItem(mpName);
			}
		}
	}
	selectInitialMp();
	connect(pCombo, SIGNAL(activated(const QString &)),
		this, SLOT(comboActivated(const QString &)));
}

/*
**	FUNCTION
**		Build initial equipment type mask
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileLhcDialog::buildInitialEqpMask(void)
{
	cryoEqpMask = allCryoEqpMask;
	beamEqpMask = allBeamEqpMask;
	ResourcePool &pool = ResourcePool::getInstance();
	if(pool.getEqpTypesMask("Profile.IsolationVacuumEqpTypes", cryoEqpMask) == ResourcePool::NotFound)
	{
		cryoEqpMask = allCryoEqpMask;
	}
	if(pool.getEqpTypesMask("Profile.BeamVacuumEqpTypes", beamEqpMask) == ResourcePool::NotFound)
	{
		beamEqpMask = allBeamEqpMask;
	}

	// Set items checked in menu by default
	const QList<FunctionalType *> &cryoTypeList = allCryoEqpMask.getList();
	FunctionalType *pItem;
	int idx;
	for(idx = 0 ; idx < cryoTypeList.count() ; idx++)
	{
		pItem = cryoTypeList.at(idx);
		for(int typeIdx = 0 ; typeIdx < cryoTypeActions.count() ; typeIdx++)
		{
			QAction *pAction = cryoTypeActions.at(typeIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				pAction->setChecked(cryoEqpMask.contains(pItem->getType()));
				break;
			}
		}
	}
	const QList<FunctionalType *> &beamTypeList = allBeamEqpMask.getList();
	for(idx = 0 ; idx < beamTypeList.count() ; idx++)
	{
		pItem = beamTypeList.at(idx);
		for(int typeIdx = 0 ; typeIdx < beamTypeActions.count() ; typeIdx++)
		{
			QAction *pAction = beamTypeActions.at(typeIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				pAction->setChecked(beamEqpMask.contains(pItem->getType()));
				break;
			}
		}
	}
}


/*
**	FUNCTION
**		Set initial selection in main part combo box
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileLhcDialog::selectInitialMp(void)
{
	DataPool &pool = DataPool::getInstance();
	int nMps = pCombo->count(), n;
	int *mps = (int *)calloc(nMps, sizeof(int));
	Sector *pSector = pFirstSector;
	for(n = 0 ; n < nMps ; n++)
	{
		MainPart *pMainPart = pool.findMainPartData(pCombo->itemText(n).toLatin1());
		if(pSector->isInMainPart(pMainPart))
		{
			mps[n]++;
		}
	}
	pSector = pLastSector;
	for(n = 0 ; n < nMps ; n++)
	{
		MainPart *pMainPart = pool.findMainPartData(pCombo->itemText(n).toLatin1());
		if(pSector->isInMainPart(pMainPart))
		{
			mps[n]++;
		}
	}

	int bestIdx = -1, bestWeight = 0;
	for(n = 0 ; n < nMps ; n++)
	{
		if(mps[n] > bestWeight)
		{
			bestWeight = mps[n];
			bestIdx = n;
		}
	}
	free((void *)mps);
	pCombo->setCurrentIndex(bestIdx);
}

/*
**	FUNCTION
**		Set text and color for title label widget
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::setTitle(void)
{
	if(mode == DataEnum::Replay)
	{
		QPalette palette = pTitleLabel->palette();
		palette.setColor(QPalette::Window, Qt::cyan);
		pTitleLabel->setPalette(palette);
		pTitleLabel->setAutoFillBackground(true);
	}
	QString title = "Profile, sector";
	if(pFirstSector == pLastSector)
	{
		title += " ";
		title += pFirstSector->getName();
	}
	else
	{
		title += "s ";
		title += pFirstSector->getName();
		title += " ... ";
		title += pLastSector->getName();
	}
	pTitleLabel->setText(title);
}

/*
**	FUNCTION
**		Build profile view according to current selection
**
**	ARGUMENTS
**		init	- Flag indicating if method is called from constructor
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::buildView(bool init)
{
	if(pView)
	{
		delete pView;
		pView = NULL;
	}
//qDebug("ProfileLhcDialog::buildView(): start\n");
	// Create instance of profile view
	pView = ProfileLhcView::create(this, pFirstSector, pLastSector, mode);
//qDebug("ProfileLhcDialog::buildView(): create, pView %X\n", (unsigned)pView);

	if(!pView)
	{
		printf("ProfileLhcDialog::buildView(): NULL pView\n");
		fflush(stdout);
		return;
	}
	connect(pView, SIGNAL(cryoLimitsChanged(int, int)),
		this, SLOT(cryoLimitsChange(int, int)));
	connect(pView, SIGNAL(beamLimitsChanged(int, int)),
		this, SLOT(beamLimitsChange(int, int)));
	connect(pView, SIGNAL(linLimitsChanged(int, int)),
		this, SLOT(linLimitsChange(int, int)));

//qDebug("ProfileLhcDialog::buildView(): connect\n");
	pView->init(allCryoEqpMask, allBeamEqpMask);
//qDebug("ProfileLhcDialog::buildView(): init\n");

	// Analyze equipment types in view, set menu accordingly
	setTypesMenu();
//qDebug("ProfileLhcDialog::buildView(): set types menu\n");

	// Set limits for new profile
	// L.Kopylov 29.09.2010 if(init)
	{
		int min = -8, max = -2;
		ResourcePool &pool = ResourcePool::getInstance();
		if(cryoVacLimitsChanged)
		{
			min = cryoVacMin;
			max = cryoVacMax;
		}
		else
		{
			if(pool.getIntValue("Profile.IsolationVacuumScaleMin", min) == ResourcePool::NotFound)
			{
				min = -8;
			}
			if(pool.getIntValue("Profile.IsolationVacuumScaleMax", max) == ResourcePool::NotFound)
			{
				max = 2;
			}
		}
		pView->setCryoLimits(min, max);
//qDebug("ProfileLhcDialog::buildView(): setCryoLimits\n");

		if(beamVacLimitsChanged)
		{
			min = beamVacMin;
			max = beamVacMax;
		}
		else
		{
			if(pool.getIntValue("Profile.BeamVacuumScaleMin", min) == ResourcePool::NotFound)
			{
				min = -12;
			}
			if(pool.getIntValue("Profile.BeamVacuumScaleMax", max) == ResourcePool::NotFound)
			{
				max = 6;
			}
		}
		pView->setBeamLimits(min, max);
//qDebug("ProfileLhcDialog::buildView(): setBeamLimits\n");

		if(cryoTempLimitsChanged)
		{
			min = cryoTempMin;
			max = cryoTempMax;
		}
		else
		{
			if(pool.getIntValue("Profile.TemperatureScaleMin", min) == ResourcePool::NotFound)
			{
				min = 0;
			}
			if(pool.getIntValue("Profile.TemperatureScaleMax", max) == ResourcePool::NotFound)
			{
				max = 20;
			}
		}
		pView->setLinLimits(min, max);
//qDebug("ProfileLhcDialog::buildView(): setLinLimits\n");
	}

	// Build content of profile view with current selection mask
	pView->setShowSectorNames(pActionSectorNames->isChecked());
	pView->applyMask(vacTypeMask, cryoEqpMask, beamEqpMask);
//qDebug("ProfileLhcDialog::buildView(): applyMask\n");

	// Add view to main box
	mainBox->addWidget(pView, 10);
	pView->show();
//qDebug("ProfileLhcDialog::buildView(): show\n");

	connect(pView, SIGNAL(mobileStateChanged()), this, SLOT(mobileStateChange()));
	connect(pView, SIGNAL(dpMouseDown(int, int, int, int, const char *)),
		this, SLOT(dpMouseDown(int, int, int, int, const char *)));

//qDebug("ProfileLhcDialog::buildView(): connect\n");
	// Set own size
	if(init)
	{
		QDesktopWidget *pDesktop = QApplication::desktop();
		const QRect &screen = pDesktop->screenGeometry(pDesktop->screenNumber(this));
		resize(screen.width() - 100, screen.height() / 2);
	}
//qDebug("ProfileLhcDialog::buildView(): FINISH\n");
}

/*
**	FUNCTION
**		Slot activated when 'Print' item is selected in 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::filePrint(void)
{
	VacMainView::printWidget(this);
}

void ProfileLhcDialog::sectNamesToggled(bool checked)
{
	//qDebug("Action from show sectors menu, checked = %d\n", checked);
	pView->setShowSectorNames(checked);
}

/*
**	FUNCTION
**		Slot activated when 'Sector' item is selected in 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::viewSector(void)
{
	makeDialogRequest(VacMainView::DialogSector);
}

/*
**	FUNCTION
**		Slot activated when 'Synoptic' item is selected in 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::viewSynoptic(void)
{
	makeDialogRequest(VacMainView::DialogSynoptic);
}

/*
**	FUNCTION
**		Make request to main view to show another dialog for data
**		shown in this dialog
**
**	ARGUMENTS
**		type	- Type of dialog to be opened, see enum in VacMainView class
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::makeDialogRequest(int type)
{
	QStringList sectorList;
	sectorList.append(pFirstSector->getName());
	sectorList.append(pLastSector->getName());
	VacMainView *pMainView = VacMainView::getInstance();
	pMainView->dialogRequest(type, vacTypeMask, sectorList, mode);
}

/*
**	FUNCTION
**		Slot activated when 'User manual' item is selected in 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::help(void)
{
}

void ProfileLhcDialog::typeMenu(QAction *pAction)
{
	if(!pAction->data().isValid())
	{
		return;
	}
	int idx;
	for(idx = 0 ; idx < vacTypeActions.count() ; idx++)
	{
		if(vacTypeActions.at(idx) == pAction)
		{
			vacTypeMenu(pAction);
			return;
		}
	}
	for(idx = 0 ; idx < cryoTypeActions.count() ; idx++)
	{
		if(cryoTypeActions.at(idx) == pAction)
		{
			cryoEqpTypeMenu(pAction);
			return;
		}
	}
	for(idx = 0 ; idx < beamTypeActions.count() ; idx++)
	{
		if(beamTypeActions.at(idx) == pAction)
		{
			beamEqpTypeMenu(pAction);
			return;
		}
	}
}

/*
**	FUNCTION
**		Slot activated when one of vacuum type items is selected
**		in 'Vacuum' submenu of 'View' menu
**
**	ARGUMENTS
**		id	- Bit mask corresponding to selected vacuum type
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::vacTypeMenu(QAction *pAction)
{
	int id = pAction->data().toInt();
	if(vacTypeMask & id)
	{
		vacTypeMask &= ~id;
		pAction->setChecked(false);
	}
	else
	{
		vacTypeMask |= id;
		pAction->setChecked(true);
	}
	pView->applyMask(vacTypeMask, cryoEqpMask, beamEqpMask);
}

/*
**	FUNCTION
**		Slot activated when one of equipment type items is selected
**		in 'Equipment on isolation vacuum' submenu of 'View' menu
**
**	ARGUMENTS
**		id	- Functional type corresponding to selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::cryoEqpTypeMenu(QAction *pAction)
{
	int id = pAction->data().toInt();
	if(cryoEqpMask.contains(id))
	{
		cryoEqpMask.remove(id);
		pAction->setChecked(false);
	}
	else
	{
		cryoEqpMask.append(id);
		pAction->setChecked(true);
	}
	pView->applyMask(vacTypeMask, cryoEqpMask, beamEqpMask);
}

/*
**	FUNCTION
**		Slot activated when one of equipment type items is selected
**		in 'Equipment on beam vacuum' submenu of 'View' menu
**
**	ARGUMENTS
**		id	- Functional type corresponding to selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::beamEqpTypeMenu(QAction *pAction)
{
	int id = pAction->data().toInt();
	if(beamEqpMask.contains(id))
	{
		beamEqpMask.remove(id);
		pAction->setChecked(false);
	}
	else
	{
		beamEqpMask.append(id);
		pAction->setChecked(true);
	}
	pView->applyMask(vacTypeMask, cryoEqpMask, beamEqpMask);
}

/*
**	FUNCTION
**		Slot activated when item is selected in main part combo box
**
**	ARGUMENTS
**		name	- name of selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::comboActivated(const QString &name)
{
	DataPool &pool = DataPool::getInstance();
	MainPart *pMainPart = pool.findMainPartData(name.toLatin1());
	if(!pMainPart)	// No selection
	{
		return;
	}

	// Selection is done differently for LHC ring and injection/dump lines
	QStringList	sectorList;
	if(pMainPart->getVacTypeMask() == VacType::Beam)	// injection/dump lines
	{
		pool.findSectorsInMainPart(pMainPart->getName(), sectorList, true);
	}
	else	// LHC ring selection
	{
		QList<MainPart *> mpList;
		mpList.append(pMainPart);
		mainVacMask = vacMask = VacType::None;
		buildRangeFromMps(mpList, sectorList);
	}
//qDebug("ProfileLhcDialog::comboActivated(): build sector list OK\n");

	Sector *pNewStart = NULL, *pNewEnd = NULL;
	// DSL sector shall not be used as start or end sector
	foreach(QString sectName, sectorList)
	{
		Sector *pSector = pool.findSectorData(sectName.toLatin1());
		if(pSector->getVacType() == VacType::DSL)
		{
			continue;
		}
		if(!pNewStart)
		{
			pNewStart = pSector;
		}
		pNewEnd = pSector;
	}
	if((!pNewStart) || (!pNewEnd))
	{
		selectInitialMp();
		return;
	}
//qDebug("ProfileLhcDialog::comboActivated(): find start/end OK\n");
	pFirstSector = pNewStart;
	pLastSector = pNewEnd;
	buildView(false);
//qDebug("ProfileLhcDialog::comboActivated(): buildView() OK\n");
	setTitle();
//qDebug("ProfileLhcDialog::comboActivated(): setTitle() OK\n");
}

/*
**	FUNCTION
**		Slot activated when state of mobile equipment in one of lines has been changed
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::mobileStateChange(void)
{
	setTypesMenu();
}

/*
**	FUNCTION
**		Slot activated when one of devices has been clicked on profile
**
**	ARGUMENTS
**		button	- Mouse button number
**		mode	- Data acquisition mode
**		x		- X-coordinate of click (mapped to screen coordinates)
**		y		- Y-coordinate of click (mapped to screen coordinates)
**		dpName	- Name of DP under mouse
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::dpMouseDown(int button, int mode, int x, int y, const char *dpName)
{
	emit profileDpDown(button, mode, x, y, 0, dpName);
}

/*
**	FUNCTION
**		Set sensitivitiy/check state of items in equipment type menus
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::setTypesMenu(void)
{
	unsigned vacMask;
	VacEqpTypeMask cryoMask, beamMask;
	pView->analyzeTypes(vacMask, cryoMask, beamMask);

	const QList<FunctionalType *> &cryoTypes = allCryoEqpMask.getList();
	FunctionalType *pItem;
	int idx;
	for(idx = 0 ; idx < cryoTypes.count() ; idx++)
	{
		pItem = cryoTypes.at(idx);
		for(int actIdx = 0 ; actIdx < cryoTypeActions.count() ; actIdx++)
		{
			QAction *pAction = cryoTypeActions.at(actIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				pAction->setEnabled(cryoMask.contains(pItem->getType()));
				pAction->setChecked(cryoEqpMask.contains(pItem->getType()));
				break;
			}
		}			
	}
	const QList<FunctionalType *> &beamTypes = allBeamEqpMask.getList();
	for(idx = 0 ; idx < beamTypes.count() ; idx++)
	{
		pItem = beamTypes.at(idx);
		for(int actIdx = 0 ; actIdx < beamTypeActions.count() ; actIdx++)
		{
			QAction *pAction = beamTypeActions.at(actIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				pAction->setEnabled(beamMask.contains(pItem->getType()));
				pAction->setChecked(beamEqpMask.contains(pItem->getType()));
				break;
			}
		}			
	}
}


/*
**	FUNCTION
**		Slot activated when limits for isolation vacuum have been changed in profile.
**		Save limits for future reuse.
**
**	ARGUMENTS
**		min	- New axis minimum
**		max	- New axis maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::cryoLimitsChange(int min, int max)
{
	cryoVacMin = min;
	cryoVacMax = max;
	cryoVacLimitsChanged = true;
}

/*
**	FUNCTION
**		Slot activated when limits for beam vacuum have been changed in profile.
**		Save limits for future reuse.
**
**	ARGUMENTS
**		min	- New axis minimum
**		max	- New axis maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::beamLimitsChange(int min, int max)
{
	beamVacMin = min;
	beamVacMax = max;
	beamVacLimitsChanged = true;
}

/*
**	FUNCTION
**		Slot activated when limits for cryo temperatures have been changed in profile.
**		Save limits for future reuse.
**
**	ARGUMENTS
**		min	- New axis minimum
**		max	- New axis maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcDialog::linLimitsChange(int min, int max)
{
	cryoTempMin = min;
	cryoTempMax = max;
	cryoTempLimitsChanged = true;
}

