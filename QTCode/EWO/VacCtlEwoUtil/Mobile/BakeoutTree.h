#ifndef	BAKEOUTTREE_H
#define	BAKEOUTTREE_H

// Tree displaying all active bakeout racks (online) and allowing to select one of them

#include <QTreeWidget>

#include "DataEnum.h"

class Eqp;
class Sector;

class BakeoutTree : public QTreeWidget
{
	Q_OBJECT

public:
	BakeoutTree(QWidget *parent = 0);
	~BakeoutTree();

signals:
	void selectChanged(Eqp *pEqp);

protected:
	Eqp	*pSelectedEqp;

	void rebuildTree(void);
	QTreeWidgetItem *addSectorToTree(Sector *pSector, QTreeWidgetItem **ppMpItem);
	void addSectorEqp(QTreeWidgetItem *pSectItem, Sector *pSector, const QList<Eqp *> &activeList,
		int &eqpIndex);
	Eqp *findSelectedEqp(QTreeWidgetItem *pParent);
	void selectEqpInTree(QTreeWidgetItem *pParent, Eqp *pEqp);

private slots:
	void eqpListChanged(DataEnum::DataMode mode);
	void treeItemClicked(QTreeWidgetItem *pItem, int column);
};

#endif	// BAKEOUTTREE_H
