#uses "VacCtlEqpDataPvss"
#uses "vclDevCommon.ctl"
#uses "CtrlRDBAccess"

// CONSTANT PARAMETERS *********************************************

// General variables
bool TEST_MODE = TRUE;
int TIME_DELAY = 20; // (s) max time the table can be without valves updated
int UPDATED_RECENTLY = FALSE;

// Database
string DRIVER = "QOCI8";
string DATABASE = "cerndb1";
string USER = "vacmon";
string PASSW = "vacmon2017ICM";
string CONNECTION_NAME = "valve_state_dbconnection";
string VALVE_TABLE = "VACMON_VALVES";
string LASTUPDATE_TABLE = "VACMON_TABLES_TIMESTAMP";
string MACHINE_NAME; // defined @ InitStaticData
time   CURRENT_TIMESTAMP;

// Main state bits masks to be analyzed
uint RR1_VALID	=     0x40000000u;
uint RR1_TST_PIN	=   0x10000000u;
uint RR1_ERROR	=     0x00800000u;
uint RR1_WARNING	=   0x00400000u;
uint RR1_PR_SRC_OK	= 0x00100000u;
uint RR1_OPEN	=       0x00020000u;
uint RR1_CLOSED	=     0x00010000u;

uint LHC_RR2_CIR_VVS_AFTER	=  0x0800u;
uint LHC_RR2_CIR_VVS_BEFORE	=  0x0400u;
uint LHC_RR2_CIR_TEMP	=        0x0200u;
uint LHC_RR2_CIR_PRESS	=      0x0100u;

uint LHC_RR2_SIR_VVS_AFTER	=  0x0008u;
uint LHC_RR2_SIR_VVS_BEFORE	=  0x0004u;
uint LHC_RR2_SIR_TEMP	=        0x0002;
uint LHC_RR2_SIR_PRESS	=      0x0001u;

uint SPS_RR2_DELTAPRESS_BEFORE =  0x2000u;
uint SPS_RR2_DELTAPRESS_AFTER =  0x1000u;
uint SPS_RR2_GLOBALPRESS_BEFORE =  0x0800u;
uint SPS_RR2_GLOBALPRESS_AFTER =  0x0400u;
uint SPS_RR2_LOCALPRESS_BEFORE = 0x0200u;
uint SPS_RR2_LOCALPRESS_AFTER = 0x0100u;
uint SPS_RR2_SIR_GLOBALPRESS_BEFORE =  0x0008u;
uint SPS_RR2_SIR_GLOBALPRESS_AFTER =  0x0004u;
uint SPS_RR2_SIR_LOCALPRESS_BEFORE = 0x0002u;
uint SPS_RR2_SIR_LOCALPRESS_AFTER = 0x0001u;

uint CPS_RR2_LOCAL_AFTER = 0x0200u;
uint CPS_RR2_LOCAL_BEFORE = 0x0100u;
uint CPS_RR2_SIR_LOCAL_AFTER = 0x0002u;
uint CPS_RR2_SIR_LOCAL_BEFORE = 0x0001u;


// MAIN **************************************************************
main() {
  if(!InitStaticData()) return; // Initialization of CTRL DLL
  if(!setDBConnection()) return; // Initialization of DB access parameters 
  
  deleteUnusedValves(); // delete from table valves that are no longer used
  updateValveListDB(); // Insert into ORACLE db missing valves
  setDPConnection(); // Connect to valve DPs
  
  while (true){
    delay(TIME_DELAY);
    if(!UPDATED_RECENTLY) updateValveListDB();
    UPDATED_RECENTLY = FALSE;
  }
  
  fwDbCloseConnection(CONNECTION_NAME);
}


// VALVE STATUS FUNCTIONS ********************************************

/*
**	FUNCTION
**		Sets the dpConnect for all the devices.
**	ARGUMENTS
**    None
**  RETURNS
**		None
**  CAUTIONS
**		None
*/
void setDPConnection(){
  dyn_string allDpRealNames = getValveDPs()[1];
  
  if (dynlen(allDpRealNames) == 0) return;
  
  for (int i=1; i<= dynlen(allDpRealNames); i++){
    dpConnect("valveStateChanged", TRUE, allDpRealNames[i] + ".RR1:_online.._value",
      allDpRealNames[i] + ".RR1:_online.._stime",
      allDpRealNames[i] + ".RR2:_online.._value",
  	  allDpRealNames[i] + ".RR2:_online.._stime");
   }
}


/*
**	FUNCTION
**		Called everytime a VVS_LHC device state changes
**	ARGUMENTS
**    dpname1(string) - DPE name
**		stateRR1(uint) - valve's read register 1
**    dpname2(string) - DPE name
**    timeRR1(time) - time of change
**    dpname3(string) - DPE name
**		stateRR2(uint) - valve's read register 2
**    dpname4(string) - DPE name
**    timeRR2(time) - time of change
**  RETURNS
**		None
**  CAUTIONS
**		None
*/
void valveStateChanged(string dpname1, uint stateRR1, string dpname2, time timeRR1,
                       string dpname3, uint stateRR2, string dpname4, time timeRR2){
  dyn_string exceptionInfo;
  mapping valveStatus = getStatus(stateRR1,stateRR2);
  valveStatus["realname"] = dpSubStr(dpname1, DPSUB_DP);
  
  string valvedisplayname;
  LhcVacDisplayName( valveStatus["realname"], valvedisplayname, exceptionInfo);
  valveStatus["displayname"] = valvedisplayname;
      
  float dcum;
  LhcVacDeviceLocation(dpSubStr(dpname1, DPSUB_DP),dcum,exceptionInfo);
  valveStatus["dcum"] = dcum;
  
  valveStatus["machine"] = MACHINE_NAME;
  valveStatus["valvetimestamp"] = timeRR1;
  
  updateValveDB(valveStatus);
  updateTimestampTableDB(timeRR1);
}


/*
**	FUNCTION
**		Maps the valve status depending on the devices' read registers 1 and 2
**	ARGUMENTS
**		rr1(uint) - valve's read register 1
**    rr2(uint) - valve's read register 2
**  RETURNS
**		valveStatus(mapping)
**  CAUTIONS
**		None
*/
mapping getStatus(uint rr1, uint rr2){
  mapping valveStatus;
  
    valveStatus["statusundefined"] = false;
    valveStatus["statusvalid"] = true;
    valveStatus["statuserror"] = false;
    valveStatus["statusclosed"] = false;
    
    valveStatus["statuscurrentinterlock"] = false;
    valveStatus["statuswarning"] = false;
    
	switch(MACHINE_NAME)
	{
		case "LHC":
			valveStatus["statusinterlocktemperature"] = false;
			valveStatus["statusinterlockvalve"] = false;
			valveStatus["statusinterlockpressure"] = false;
		
			if ((rr1 & RR1_OPEN) && (rr1 & RR1_CLOSED))
			  valveStatus["statusundefined"] = true;
			if ((!(rr1 & RR1_OPEN)) && (!(rr1 & RR1_CLOSED)))
			  valveStatus["statusundefined"] = true;
			if (!(rr1 & RR1_VALID))
			  valveStatus["statusvalid"] = false;
			if (rr1 & RR1_ERROR)
			  valveStatus["statuserror"] = true;
			if ((!(rr1 & RR1_OPEN)) && (rr1 & RR1_CLOSED))
			  valveStatus["statusclosed"] = true;
			if ((rr2 & LHC_RR2_CIR_TEMP) )
			  valveStatus["statusinterlocktemperature"] = true;
			if ((rr2 & LHC_RR2_CIR_VVS_AFTER)||(rr2 & LHC_RR2_CIR_VVS_BEFORE) )
			  valveStatus["statusinterlockvalve"] = true;
			if ((rr2 & LHC_RR2_CIR_PRESS) || !(rr1 & RR1_PR_SRC_OK) )
			  valveStatus["statusinterlockpressure"] = true;
			if (valveStatus["statusinterlocktemperature"]||
				valveStatus["statusinterlockvalve"]||valveStatus["statusinterlockpressure"])
			  valveStatus["statuscurrentinterlock"] = true;
			if (rr1 & RR1_WARNING)
			  valveStatus["statuswarning"] = true;
			break;
		case "SPS":
		
			valveStatus["statusinterlockdeltap"] = false;
			valveStatus["statusinterlocklocal"] = false;
			valveStatus["statusinterlockglobal"] = false;
		
			if ((rr1 & RR1_OPEN) && (rr1 & RR1_CLOSED))
			  valveStatus["statusundefined"] = true;
			if ((!(rr1 & RR1_OPEN)) && (!(rr1 & RR1_CLOSED)))
			  valveStatus["statusundefined"] = true;
			if (!(rr1 & RR1_VALID))
			  valveStatus["statusvalid"] = false;
			if (rr1 & RR1_ERROR)
			  valveStatus["statuserror"] = true;
			if ((!(rr1 & RR1_OPEN)) && (rr1 & RR1_CLOSED))
			  valveStatus["statusclosed"] = true;
		    
			if((rr2 & SPS_RR2_DELTAPRESS_BEFORE) || (rr2 & SPS_RR2_DELTAPRESS_AFTER))
				valveStatus["statusinterlockdeltap"] = true;
			if((rr2 & SPS_RR2_GLOBALPRESS_BEFORE) || (rr2 & SPS_RR2_GLOBALPRESS_AFTER))
				valveStatus["statusinterlockglobal"] = true;
			if((rr2 & SPS_RR2_LOCALPRESS_BEFORE) || (rr2 & SPS_RR2_LOCALPRESS_AFTER))
				valveStatus["statusinterlocklocal"] = true;
			
			if (valveStatus["statusinterlockdeltap"]||
				valveStatus["statusinterlocklocal"]||valveStatus["statusinterlockglobal"])
			  valveStatus["statuscurrentinterlock"] = true;
			if (rr1 & RR1_WARNING)
			  valveStatus["statuswarning"] = true;
			break;
		case "ComplexPS":
			valveStatus["statusinterlocklocal"] = false;
			
			if ((rr1 & RR1_OPEN) && (rr1 & RR1_CLOSED))
			  valveStatus["statusundefined"] = true;
			if ((!(rr1 & RR1_OPEN)) && (!(rr1 & RR1_CLOSED)))
			  valveStatus["statusundefined"] = true;
			if (!(rr1 & RR1_VALID))
			  valveStatus["statusvalid"] = false;
			if (rr1 & RR1_ERROR)
			  valveStatus["statuserror"] = true;
			if ((!(rr1 & RR1_OPEN)) && (rr1 & RR1_CLOSED))
			  valveStatus["statusclosed"] = true;
			if((rr2 & CPS_RR2_LOCAL_AFTER) || rr2 & (CPS_RR2_LOCAL_BEFORE))
			  valveStatus["statusinterlocklocal"] = true;
			if (valveStatus["statusinterlocklocal"])
			  valveStatus["statuscurrentinterlock"] = true;
			if (rr1 & RR1_WARNING)
			  valveStatus["statuswarning"] = true;
			break;	
	}
    
  return valveStatus;
}



/*
**	FUNCTION
**		Updates the timestamp table in the DB.
**	ARGUMENTS
**		tstamp (time) - timestamp to update table
**  RETURNS
**		true/false
**  CAUTIONS
**		None
*/
bool updateTimestampTableDB(time tstamp = CURRENT_TIMESTAMP){
  string errTxt;
  dbCommand cmd;
  string cmdText;
  
  cmdText = "UPDATE " + LASTUPDATE_TABLE + " " +
            "SET LAST_UPDATED= sysdate " +
            "WHERE TABLE_NAME='"+ VALVE_TABLE +"'";
    
  fwDbStartCommand(CONNECTION_NAME, cmdText, cmd);
  if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return -1;};
  
  fwDbExecuteCommand(cmd);
  if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return -1;};
      
  fwDbFinishCommand(cmd);
  if (fwDbCheckError(errTxt)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
  
  return true;
}





// DATABASE CONNECTION FUNCTIONS ********************************************

/*
**	FUNCTION
**		Sets the DB connection
**	ARGUMENTS
**		connectionName(string) - by default defined in the top of the file
**    database(string) - by default defined in the top of the file
**    username(string) - by default defined in the top of the file
**    password(string) - by default defined in the top of the file
**    driver(string) - by default defined in the top of the file
**  RETURNS
**		true/false
**  CAUTIONS
**		None
*/
bool setDBConnection(string connectionName = CONNECTION_NAME, string database
        = DATABASE, string username = USER, string password = PASSW, string driver = DRIVER){

  if (!isFunctionDefined("fwDbOption")) {
     DebugTN("[VacMon Ctl] ERROR: CtrlRDBAccess library is not available. Check your installation and config file");
     return -1;
  }
    
  string errTxt;
  dbConnection dbCon;

  // check if connection with that name already present
  int rc=fwDbGetConnection(connectionName,dbCon);
  if (rc==-1){
    fwDbCheckError(errTxt,dbCon);
    DebugTN("[VacMon Ctl] ERROR Getting the connection",errTxt);
    return false;
  } else if (rc==1) {
    // connection with such name does not exist yet. Create it
    string connectString = "Driver=" + driver + ";Database=" + database + ";User=" + username + ";Password=" + password;
    fwDbOpenConnection(connectString, dbCon,connectionName);
    if (fwDbCheckError(errTxt,dbCon)){DebugTN("[VacMon Ctl] ERROR WHILE OPENING DATABASE",errTxt);return -1;};
  }
  if(TEST_MODE){
    // get and print the status of the connection via "Inspect" mechanism
    mixed retVal; // note! the argument to fwDbOption must be of type mixed!
    rc=fwDbOption("Inspect",dbCon,retVal);
    DebugTN("[VacMon Ctl] " + rc + retVal);
  }
  return true;
}


/*
**	FUNCTION
**		Runs a command to the DB, that might or might not return a result.
**	ARGUMENTS
**		dbCommand(string)
**    getData(bool) - false by default, true is a result is expected from the command.
**  RETURNS
**		data(dyn_dyn_mixed) - if command has a result
**  CAUTIONS
**		None
*/
dyn_dyn_mixed executeDBCommand(string dbcommand, bool getData=false){
  
  string errTxt;
  dbCommand cmd;
  dyn_dyn_mixed data;
  dynClear(data);
  
  fwDbStartCommand(CONNECTION_NAME, dbcommand, cmd);
  if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return -1;};
  
  fwDbExecuteCommand(cmd);
  if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return -1;};
  
  if(!getData) return data;
  
  fwDbGetData(cmd, data);
  if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return -1;};
  
  fwDbFinishCommand(cmd);
  if (fwDbCheckError(errTxt)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return -1;};
  return data;
}


/*
**	FUNCTION
**		Inserts a valve in the DB.
**	ARGUMENTS
**		valveStatus(mapping) - mapping: realname, displayname(only if !update),
**                    statusundefined, statusvalid, statuserror, statusclosed,
**                    statusinterlocktemperature, statusinterlockvalve, statusinterlockpressure,
**                    statuscurrentinterlock, statuswarning
**  RETURNS
**		true/false
**  CAUTIONS
**		None
*/
bool addValve2DB(mapping valveStatus){
  string errTxt;
  dbCommand cmd;
  string cmdText;
  
  if(MACHINE_NAME == "LHC")
	    cmdText = "INSERT INTO " + VALVE_TABLE + " (VALVEREALNAME,VALVEDISPLAYNAME,"+
					   "STATUSUNDEFINED,STATUSVALID,STATUSERROR,STATUSCLOSED," +
					   "STATUSINTERLOCKTEMPERATURE,STATUSINTERLOCKVALVE,STATUSINTERLOCKPRESSURE," +
					   "STATUSCURRENTINTERLOCK,STATUSWARNING,VALVEDCUM,MACHINE,DEVICETIMESTAMP) " +
					   "VALUES (:realname,:displayname," +
					   ":statusundefined,:statusvalid,:statuserror,:statusclosed," +
					   ":statusinterlocktemperature,:statusinterlockvalve,:statusinterlockpressure," +
					   ":statuscurrentinterlock,:statuswarning,:dcum,:machine,:valvetimestamp)";
					   
	else if(MACHINE_NAME == "SPS")
		cmdText = "INSERT INTO " + VALVE_TABLE + " (VALVEREALNAME,VALVEDISPLAYNAME,"+
					   "STATUSUNDEFINED,STATUSVALID,STATUSERROR,STATUSCLOSED," +
					   "STATUSINTERLOCKDELTAP,STATUSINTERLOCKGLOBAL,STATUSINTERLOCKLOCAL," +
					   "STATUSCURRENTINTERLOCK,STATUSWARNING,VALVEDCUM,MACHINE,DEVICETIMESTAMP) " +
					   "VALUES (:realname,:displayname," +
					   ":statusundefined,:statusvalid,:statuserror,:statusclosed," +
					   ":statusinterlockdeltap,:statusinterlockglobal,:statusinterlocklocal," +
					   ":statuscurrentinterlock,:statuswarning,:dcum,:machine,:valvetimestamp)";
					   
	else if(MACHINE_NAME == "ComplexPS")
		cmdText = "INSERT INTO " + VALVE_TABLE + " (VALVEREALNAME,VALVEDISPLAYNAME,"+
			   "STATUSUNDEFINED,STATUSVALID,STATUSERROR,STATUSCLOSED," +
			   "STATUSINTERLOCKLOCAL," +
			   "STATUSCURRENTINTERLOCK,STATUSWARNING,VALVEDCUM,MACHINE,DEVICETIMESTAMP) " +
			   "VALUES (:realname,:displayname," +
			   ":statusundefined,:statusvalid,:statuserror,:statusclosed," +
			   ":statusinterlocklocal," +
			   ":statuscurrentinterlock,:statuswarning,:dcum,:machine,:valvetimestamp)";
  
  fwDbStartCommand(CONNECTION_NAME, cmdText, cmd);
  if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
  
  fwDbBindParams(cmd, valveStatus);
  if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
      
  fwDbExecuteCommand(cmd);
  if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
      
  fwDbFinishCommand(cmd);
  if (fwDbCheckError(errTxt)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
  
  return true;
}


/*
**	FUNCTION
**		Updates a valve in the DB.
**	ARGUMENTS
**		valveStatus(mapping) - mapping: realname, displayname(only if !update),
**                    statusundefined, statusvalid, statuserror, statusclosed,
**                    statusinterlocktemperature, statusinterlockvalve, statusinterlockpressure,
**                    statuscurrentinterlock, statuswarning
**  RETURNS
**		true/false
**  CAUTIONS
**		None
*/
bool updateValveDB(mapping valveStatus){
  string errTxt;
  dbCommand cmd;
  string cmdText;
  
  if(MACHINE_NAME == "LHC")
	  cmdText = "UPDATE " + VALVE_TABLE + " " +
				"SET STATUSUNDEFINED=:statusundefined," +
				"STATUSVALID=:statusvalid," +
				"STATUSERROR=:statuserror," +
				"STATUSCLOSED=:statusclosed," +
				"STATUSINTERLOCKTEMPERATURE=:statusinterlocktemperature," +
				"STATUSINTERLOCKVALVE=:statusinterlockvalve," +
				"STATUSINTERLOCKPRESSURE=:statusinterlockpressure," +
				"STATUSCURRENTINTERLOCK=:statuscurrentinterlock," +
				"STATUSWARNING=:statuswarning, " +
				"VALVEDCUM=:dcum, " +
				"MACHINE=:machine, " +
				"VALVEDISPLAYNAME=:displayname, " +
				"DEVICETIMESTAMP=:valvetimestamp " +
				"WHERE VALVEREALNAME=:realname";
  else if(MACHINE_NAME == "SPS")
	  cmdText = "UPDATE " + VALVE_TABLE + " " +
				"SET STATUSUNDEFINED=:statusundefined," +
				"STATUSVALID=:statusvalid," +
				"STATUSERROR=:statuserror," +
				"STATUSCLOSED=:statusclosed," +
				"STATUSINTERLOCKLOCAL=:statusinterlocklocal," +
				"STATUSINTERLOCKDELTAP=:statusinterlockdeltap," +
				"STATUSINTERLOCKGLOBAL=:statusinterlockglobal," +
				"STATUSCURRENTINTERLOCK=:statuscurrentinterlock," +
				"STATUSWARNING=:statuswarning, " +
				"VALVEDCUM=:dcum, " +
				"MACHINE=:machine, " +
				"VALVEDISPLAYNAME=:displayname, " +
				"DEVICETIMESTAMP=:valvetimestamp " +
				"WHERE VALVEREALNAME=:realname";
	else if(MACHINE_NAME == "ComplexPS")			
	  cmdText = "UPDATE " + VALVE_TABLE + " " +
				"SET STATUSUNDEFINED=:statusundefined," +
				"STATUSVALID=:statusvalid," +
				"STATUSERROR=:statuserror," +
				"STATUSCLOSED=:statusclosed," +
				"STATUSINTERLOCKLOCAL=:statusinterlocklocal," +
				"STATUSCURRENTINTERLOCK=:statuscurrentinterlock," +
				"STATUSWARNING=:statuswarning, " +
				"VALVEDCUM=:dcum, " +
				"MACHINE=:machine, " +
				"VALVEDISPLAYNAME=:displayname, " +
				"DEVICETIMESTAMP=:valvetimestamp " +
				"WHERE VALVEREALNAME=:realname";
				
  fwDbStartCommand(CONNECTION_NAME, cmdText, cmd);
  if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
  
  fwDbBindParams(cmd, valveStatus);
  if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
      
  fwDbExecuteCommand(cmd);
  if (fwDbCheckError(errTxt,cmd)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
      
  fwDbFinishCommand(cmd);
  if (fwDbCheckError(errTxt)){DebugTN("[VacMon Ctl] DB ERROR",errTxt);return false;};
  
  UPDATED_RECENTLY = TRUE;
  return true;
}


/*
**	FUNCTION
**		Runs the first time the CTL is put into production, to check if
**      new VVS_LHC devices have to be added to the DB list
**	ARGUMENTS
**    None
**  RETURNS
**		None
**  CAUTIONS
**		None
*/
void updateValveListDB(){
  dyn_dyn_string allDpNames = getValveDPs();
  
  if (dynlen(allDpNames) == 0) return;
  
  dyn_string allDpRealNames = allDpNames[1];
  dyn_string allDpDisplayNames = allDpNames[2];
  dyn_string exceptionInfo;
  
  CURRENT_TIMESTAMP = getCurrentTime();
    
  for (int i=1; i<= dynlen(allDpRealNames); i++){
    dyn_dyn_mixed data = executeDBCommand("SELECT * " + 
                                          "FROM " + VALVE_TABLE + " " + 
                                          "WHERE valverealname='" + allDpRealNames[i] + "'", true);
    
    uint RR1, RR2;
    float dcum;
    dpGet(allDpRealNames[i] + ".RR1", RR1);
    dpGet(allDpRealNames[i] + ".RR2", RR2);
    mapping valveStatus = getStatus(RR1,RR2);
    valveStatus["realname"] = allDpRealNames[i];
    valveStatus["displayname"] = allDpDisplayNames[i];
    
    LhcVacDeviceLocation(dpSubStr(allDpRealNames[i], DPSUB_DP),dcum,exceptionInfo);
    valveStatus["dcum"] = dcum;
    
    valveStatus["machine"] = MACHINE_NAME;
    valveStatus["valvetimestamp"] = CURRENT_TIMESTAMP;
    
    if (dynlen(data) == 0){      
      addValve2DB(valveStatus);
    }else{
      updateValveDB(valveStatus);
    }
    dynClear(data);
  }
  updateTimestampTableDB();
}


// GENERAL FUNCTIONS ******************************************************

/*
**	FUNCTION
**		Gets the complete valve list, with both real and display names.
**	ARGUMENTS
**		None
**  RETURNS
**		allDpNames(dyn_dyn_string) - list of valves
**  CAUTIONS
**		None
*/
dyn_dyn_string getValveDPs() {
  dyn_string allDpRealNames;
  dyn_string allDpDisplayNames;
  dyn_dyn_string allDpNames;
  dyn_string exceptionInfo;
  
  switch(MACHINE_NAME){
    case "LHC":
      allDpRealNames= dpNames("*", "VVS_LHC");
	  VALVE_TABLE = "VACMON_VALVES_LHC";
      break;
    case "SPS":
      allDpRealNames= dpNames("*", "VVS_S");
	  dynAppend(allDpRealNames,dpNames("*", "VVS_SV"));
      VALVE_TABLE = "VACMON_VALVES_SPS";
      break;
    case "ComplexPS":
	  VALVE_TABLE = "VACMON_VALVES_CPS";
	  
      allDpRealNames=dpNames("*", "VVS_PS");
	  dynAppend(allDpRealNames,dpNames("*", "VVS_PSB_SR"));
	  dynAppend(allDpRealNames,dpNames("*", "VVS_PSB_SUM"));
	
      break;
    default:
      return "";
  }
  
	for(int i = 1; i <= dynlen(allDpRealNames); i++) {
    LhcVacDisplayName( dpSubStr(allDpRealNames[i], DPSUB_DP), allDpDisplayNames[i], exceptionInfo);
    allDpRealNames[i] = dpSubStr(allDpRealNames[i], DPSUB_DP);
	  
	}
  allDpNames[1] = allDpRealNames;
  allDpNames[2] = allDpDisplayNames;
  return allDpNames;
}


/*
**	FUNCTION
**		Initializes static data.
**	ARGUMENTS
**		None
**  RETURNS
**		true/false
**  CAUTIONS
**		None
*/
bool InitStaticData( ){
	dyn_string	exceptionInfo, mainParts;
	long		lResult;
	bool		isLhcData;
  
	// Set accelerator name to be used by other components
	if(TEST_MODE) DebugTN("[VacMon Ctl] Starting InitStaticData..."); 
	LhcVacGetMachineId( exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ){
		if(TEST_MODE) DebugTN("[VacMon Ctl] Failed to read machine name: " + exceptionInfo );
		return false;
	}
	if(TEST_MODE) DebugTN("[VacMon Ctl] Machine is " + glAccelerator );
	MACHINE_NAME = glAccelerator;
  if(TEST_MODE) DebugTN(" MACHINE : " + MACHINE_NAME);
  VacResourcesParseFile( PROJ_PATH + "/data/VacResources.txt", glAccelerator, exceptionInfo );
  
	isLhcData = glAccelerator == "LHC" ? true : false;
  
	// Initalize passive machine data
	lResult = LhcVacEqpInitPassive( isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo );
	if( lResult < 0 ){
		if(TEST_MODE) DebugTN("[VacMon Ctl] LhcVacEqpInitPassive() failed: " + exceptionInfo );
		return false;
	}
  
	// Initialzie active equipment information
	lResult = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
			exceptionInfo );
	if( lResult < 0 ){
		if(TEST_MODE) DebugTN("[VacMon Ctl] LhcVacEqpInitActive() failed: " + exceptionInfo );
		return false;
	}
	if(TEST_MODE) DebugTN("[VacMon Ctl] InitStaticData Done");
	LhcVacInitDevFunc();
	return true;
}

void deleteUnusedValves(){
	
	dyn_dyn_string allDpNames = getValveDPs();
  
	if (dynlen(allDpNames) == 0) return;
  
  dyn_string allDpRealNames = allDpNames[1];
  dyn_string allDpDisplayNames = allDpNames[2];
  dyn_string exceptionInfo;
  
  dyn_dyn_mixed data = executeDBCommand("SELECT valverealname " + 
                                          "FROM " + VALVE_TABLE, true);
				
  if(dynlen(data) > 0 )				
  {
	  for(int i = 1;i<=dynlen(data);i++)
	  {
		  if(!dynContains(allDpRealNames, data[i][1]))
		  {
			 executeDBCommand("DELETE FROM " + VALVE_TABLE + " WHERE valverealname = '" + data[i][1] + "'" );
		  }
		  
	  }
  }
  

	
}