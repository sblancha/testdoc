/** @defgroup gr_vacPanelNotifications dialogs/vacPanelNotifications.pnl
vacPanelNotifications.ctl is the scope library of the panel vacPanelNotifications.pnl in the directory dialogs/. \n
vacPanelNotifications.pnl is a panel to create and run group of notifications configurations. \n
\n 
The creation of notifications configuration is long and difficult, this panel helps to create multiple pre-defined configurations. \n
\n
There are 5 groups and 16 configurations. \n
The parameters are specified in the document: \n
https://svnvacplc.web.cern.ch/repo/Specifications/NotificationGroupConfigs.xlsx \n
\n
< b> Notification Group Types: < /b>  \n
  #  | Group Type Name          |Scope Type| Machine Mode                            | Configuration types
  ---|--------------------------|----------|-----------------------------------------|------------------------------------------
  1  | NEG Activation           | Sector   | SHUTDOWN, CRYO/ACCESS, ACCESS           | 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13
  2  | Sector On Work           | Sector   | SHUTDOWN, CRYO/ACCESS, ACCESS           | 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13  
  3  | MainPart On Work         | MainPart | SHUTDOWN, CRYO/ACCESS, ACCESS           | 1, 2, 4, 6, 13
  4  | LHC Beam Operation       | MainPart | RUN, PROTON, MD, STEADY, BEAM SETUP,ION | 2, 3, 14, 17 
  5  | LHC Insulation Operation | MainPart | RUN, PROTON, MD, STEADY, BEAM SETUP,ION | 1, 2, 4, 12, 15, 16, 17
\n
< b> Notification Group Configurations: < /b>  \n
  #  | Config Type Name                          | Eqp Type | Crit. | F. Criteria |Rever.|MinDur.|LoThresh|UpThresh| Message
  ---|-------------------------------------------|----------|-------|-------------|------|-------|--------|--------|----------------------------------
  1  | VPG(Wireless+Fixed) disconnected > 10mins | VPG      | State | Connected   | Y    | 10min |        |        | {eqp} is disconnected (more than 10mins)
  2  | VPG(Wireless+Fixed) error > 10mins        | VPG      | Error | Error       | N    | 10min |        |        | {eqp}: Error (more than 10mins)
  3  | VPG(Wireless+Fixed) VVR1 not opened       | VPG      | State | Vvr1Open    | Y    | 10min |        |        | {eqp}: VVR1 is not OPEN
  4  | VPG(Fixed) VVR2 not opened                | VPG      | State | Vvr2Open    | Y    | 10min |        |        | {eqp}: VVR2 is not OPEN
  5  | VPG(Profibus Mobile) disconnected > 10mins| VPGM     | State | Connected   | Y    | 10min |        |        | {eqp} is Disconnected
  6  | VPG(Profibus Mobile) error > 10mins       | VPGM     | Error | Error       | N    | 10min |        |        | {eqp} is in error
  7  | VPG(Profibus Mobile) VVR1 not opened      | VPGM     | State | VvrOpen     | Y    | 10min |        |        | {eqp}: VVR is not OPEN
  8  | Bakeout(Wireless) disconnected > 10mins   | VRE      | State | Connected   | Y    | 10min |        |        | {eqp} is Disconnected
  9  | Bakeout(Wireless) error > 10mins          | VRE      | Error | Error       | N    | 10min |        |        | {eqp}: error {error}
  10 | Bakeout(Profibus Mobile) disconn > 10mins | VBAKEOUT | State | Connected   | Y    | 10min |        |        | {eqp} is Disconnected
  11 | Bakeout(Profibus Mobile) error > 10mins   | VBAKEOUT | State | ERROR       | N    | 10min |        |        | {eqp}: Error
  12 | Pirani above 0.05 mbar > 30secs           | VGR      | Value | PR          | N    | 30sec | 0.01mb | 0.05mb | {eqp}: {value} > {highLimit}
  13 | Valves(Sector+Standard) opened            | VV       | State | Open        | N    |       |        |        | {eqp} is opened
  14 | Penning above 1E-7 mbar > 30secs          | VGP      | Value | PR          | N    |       | 8E-8mb | 1E-7mb | {eqp}: {value} > {highLimit}
  15 | CryoAlarm not Ok                          | VACOK    | State | OK          | Y    |       |        |        | {eqp} is not OK
  16 | Membrane above 10 mbar > 30secs           | VGM      | Value | PR          | N    | 30sec | 5mb    | 10mb   | {eqp}: {value} > {highLimit} 
  17 | Valves(Sector+Standard) not opened        | VV       | State | Open        | Y    |       |        |        | {eqp} is not opened
\n
The main item of the panel is an \b EWO table from the Qt/C++ librairy NotificationGroupTableEwo.ewo, it has the below columns: \n
    Column Name           | Title             |Description
    ----------------------|-------------------|-----------
    Id                    | ID                | ID number of the Group 
    Owner                 | Owner             | Creator of the group of configuration 
    Name                  | Name              | Description name 
    Type                  | Group Type        | Group type from 1 to 5 (see above)  
    Scopes                | Sectors/MainParts | List of sectors or mainpart as scope of the configurations 
    Recipients            | Recipients        | Email,SMS recipients list 
    State                 | State             | Activation status of the configuration 
    Events                | Events            | Number of time the criteria have been met 
    Msg                   | Sent              | Number of notification sent
    Error                 | Error             | Error state of the configuration initialization 
    
*/
#uses "VacCtlEqpDataPvss"	// FOR TEST ONLY !!!
#uses "vclEqpConfig.ctl"    // Load CTRL library
#uses "vclDevCommon.ctl"    // Load CTRL library
#uses "vclMachine.ctl"      // Load CTRL library
#uses "vclSmsConfig.ctl"
//
//
//
// =====================
// PANEL EVENT FUNCTIONS
// =====================
//
//
//
/**
@brief Initialisation of the panel vacPanelNotifications
@details Panel Event Function
*/ 
void initVacPanelNotifications() {
  setWindowTitle(myModuleName(), "Notifications Group parametrisation");
  //---
  // Load static data pool
  string timeStamp;
  LhcVacGetTimeStamp(timeStamp);
  if(timeStamp == "") { // Init static data
    dyn_string	exceptionInfo;
    // Get accelerator name to be used by other components
    LhcVacGetMachineId(exceptionInfo);
    if(dynlen(exceptionInfo) > 0) {
      string errMessage = "vacPanelNotifications: +++++ FATAL: Failed to read machine name: " + exceptionInfo;
      dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return;
    }
    DebugTN("vacPanelNotifications: Machine is " + glAccelerator);
    bool isLhcData = glAccelerator == "LHC" ? true : false;
    VacResourcesParseFile(PROJ_PATH + "/data/VacCtlResources.txt", glAccelerator, exceptionInfo);
    LhcVacFinishLongActPart();
    if(dynlen(exceptionInfo) > 0) {
      fwExceptionHandling_display(exceptionInfo);
    }
    // Initalize passive machine data
    int ret = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
    if(ret < 0) {
      string errMessage = "vacPanelNotifications: +++++ FATAL: LhcVacEqpInitPassive() failed: " + exceptionInfo;
      dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return;
    }
    DebugTN("vacPanelNotifications: LhcVacEqpInitPassive() done");
    LhcVacInitDevFunc();
    DebugTN("vacPanelNotifications: LhcVacInitDevFunc() done");
    // Initialzie active equipment information
    ret = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
    exceptionInfo);
    if(ret < 0) {
      string errMessage = "vacPanelNotifications: +++++ FATAL: LhcVacEqpInitActive() failed: " + exceptionInfo;
      dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return;
    }
    DebugTN( "vacPanelNotifications: InitStaticData() done" );
  }
  // Find all configuration DPs, connect to them
  updateGroupList();
  //dpConnect("MachineModeCb", dpName + ".Mode");
  
}
/**
@brief Initialisation of the panel VacPanelNotificationsEdit
@details Panel Event Function
*/
void initVacPanelNotificationsEdit(string dpName, bool isInactive ) {
  if(dpName == "") {
    PanelOff();
    return;
  }
  if(!dpExists(dpName)) {
    PanelOff();
    return;
  }
  // if group running desable buttons
  
  // Is this my configuration or not?
  bool isMyConfig = VacSmsIsMyConfiguration(dpName);
  if (!isMyConfig) {
    DescriptionText.enabled(false);
    groupTypeComboBox.enabled(false);
    ScopePb.enabled(false);
    RecipientsPb.enabled(false);
    isActivateText.visible(false);
    isNotMyConfigText.visible(true);
    DescriptionText.enabled(false);
  }
  // if group running desable buttons
  else if (!isInactive) {
    DescriptionText.enabled(true);
    groupTypeComboBox.enabled(false);
    ScopePb.enabled(false);
    RecipientsPb.enabled(false);
    isActivateText.visible(true);
    isNotMyConfigText.visible(false);
    DescriptionText.enabled(true);    
  }
  else {
    DescriptionText.enabled(true);
    groupTypeComboBox.enabled(true);
    ScopePb.enabled(true);
    RecipientsPb.enabled(true);
    isActivateText.visible(false);
    isNotMyConfigText.visible(false);
    DescriptionText.enabled(true);        
  }
    
  // ---  
  // Call backs for Buttons/Fields animation
  //dpConnect("EnableActivatePbCb", dpName + ".Type", dpName + ".Recipients", dpName + ".ScopeType", dpName + ".Scopes");   
  //dpConnect("VisibleNameCb", dpName + ".VisibleName");
  dpConnect("ScopeCb", dpName + ".Scopes", dpName + ".Type");
  dpConnect("RecipientsCb", dpName + ".Recipients");
  dpConnect("ScopeTypeCb", dpName + ".Type");        
  dyn_string names;
  dyn_int scopeTypes;
  dyn_string groupTypeNames; 
  getGroupTypeList(groupTypeNames, scopeTypes);
  groupTypeComboBox.appendItem("None");
  for (int n = 1 ; n <= dynlen(groupTypeNames); n++) {  
    groupTypeComboBox.appendItem(groupTypeNames[n]);
  }
  dpConnect("GroupTypeCb", dpName + ".Type");
  // ---  
  // Call backs for activation animation
  /*
  VacNotificationActivationDpConnect(dpName,
                                     dpNameConfigs, Activate_dpeConfigs , 
                                                    Activated_dpeConfigs ,
                                                    Initialized_dpeConfigs , 
                                                    MessagesSent_dpeConfigs ,
                                                    ErrorMessage_dpeConfigs, 
                                                    EventCount_dpeConfigs);
  */
}
/**
@brief Panel Function Action when New button is clicked
@details Panel Function
*/
void buttonNewClicked() {
  dyn_float dfReturn;
  dyn_string dsReturn;
  if(!VacUserHasMinimumPrivilege()) {
    ChildPanelOnCentralModalReturn("vision/dialogs/Privilege.pnl", "Missing Privilege",
                                    makeDynString("$1: You have not enough privilege to create notificaton"), dfReturn, dsReturn);         
    return;
  }
  string dpName;
  dyn_string exceptionInfo;
  int id;
  createGroupDp(dpName, id, exceptionInfo);
  if(dynlen(exceptionInfo) > 0) {
    fwExceptionHandling_display(exceptionInfo);
    return;
  }
  dpSet(dpName + ".Owner", LhcVacGetUserName(),
        dpName + ".ID", id);
  dyn_string configDpList = VacSmsBuildGroupContent(dpName);
  Table.addGroup(dpName, configDpList);
  ChildPanelOnCentralModal("vision/dialogs/vacPanelNotificationsEdit.pnl", "Group Configuration Details",
                           makeDynString("$dpName:" + dpName, "$isMyConfig:true", "$isInactive:true"));
}
/**
@brief Panel Function Action when Edit button is clicked
@details Panel Function
*/
void buttonEditClicked() {
  dyn_float dfReturn;
  dyn_string dsReturn;
  if(!VacUserHasMinimumPrivilege()) {
    ChildPanelOnCentralModalReturn("vision/dialogs/Privilege.pnl", "Missing Privilege",
                                    makeDynString("$1: You have not enough privilege to edit notifications! "), dfReturn, dsReturn);   
    return;
  }
  bool isInactive;
  dyn_string selected = Table.getSelectedGroup();
  if (dynlen(selected) < 2) {
    ChildPanelOnCentralModal("vision/dialogs/Message.pnl", "Missing Selection",
                                    makeDynString("$1: Please select a notification in the table! "));
    return;
  }    
  string dpName = selected[1];
  string state = selected[2]; 
  if(dpName == "") {
    return;
  }
  bool isMyConfig = VacSmsIsMyConfiguration(dpName);
  string sMyConfig;  
  string sInactive;  
  if(isMyConfig) {
    sMyConfig = "true";
  }
  if(state == "Inactive") {
    sInactive = "true";
  }
  ChildPanelOnCentralModal("vision/dialogs/vacPanelNotificationsEdit.pnl", "Group Configuration Details",
                           makeDynString("$dpName:" + dpName, "$isMyConfig:" + sMyConfig, "$isInactive:" + sInactive));  
  }
/**
@brief Panel Function Action when Delete button is clicked
@details Panel Function
*/
void processDelete()
{
  dyn_float dfReturn;
  dyn_string dsReturn;  
  dyn_string selected = Table.getSelectedGroup();
  if (dynlen(selected) < 1) {
    ChildPanelOnCentralModal("vision/dialogs/Message.pnl", "Missing Selection",
                                                         makeDynString("$1: Please select a notification in the table! "));
    return;
  }    
  string dpName = selected[1];
  if(dpName == "") {
    return;
  }
  ChildPanelOnCentralModalReturn("vision/MessageInfo", "WARNING",
    makeDynString("$1:Are you sure to delete selected Group configuration ?",
    "$2:Yes", "$3:No"), dfReturn, dsReturn);
  if(dynlen(dfReturn) < 1) {
    return;
  }
  if(dfReturn[1] != 1) {
    return;
  }
  VacNotificationDeactivate(dpName);
  Table.deleteGroup(dpName); 
  VacNotificationUnlinkGroup(dpName);
  dpDelete(dpName);
  updateGroupList();
}
/**
@brief Reset counters in config dp
@details Panel Function
*/
void resetCounters(dyn_string messagesSent_dpeConfigs, dyn_string eventCount_dpeConfigs) {
  if(dynlen(messagesSent_dpeConfigs) != dynlen(eventCount_dpeConfigs)) {
    string errMessage = "vacPanelNotifications: +++++ FATAL: resetCounters failed: " + exceptionInfo;
    dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);    
    return;
  }
  for(int n = 1; n <= dynlen(messagesSent_dpeConfigs); n++) {
    dpSetWait(messagesSent_dpeConfigs[n], 0);
    dpSetWait(eventCount_dpeConfigs[n], 0);
  }
}
    
             
//
//
//
// ==================
// EWO EVENT FUNCTION
// ==================
//
//
//
/**
@brief dpConnect needed for ewo NotificationGroupTable
@details EWO Event Function
*/
void notificationGroupTableEwoConnectRequest() {    
  //DebugTN("vacPanelNotifications: connectRequest()");
  int nProcessed;
  //----
  //--- Disconnect  
  string dpName = Table.nextDisconnectRequest();
  while(dpName != "") {
    //DebugTN(dpName + " disconnect");
    nProcessed++;
    if((nProcessed % 50) == 0) {
      ConnectText.foreCol = "{0,0,255}";
      ConnectText.text = "" + nProcessed;
    }
    string dpType = dpTypeName(dpName);
    if(dpType == SMS_CONFIG_DP_TYPE) {
      DebugTN(dpName + " config disconnect");      
      dpDisconnect("ConfigCb",
        dpName + ".DpNumber",
        dpName + ".State.Activate",
        dpName + ".State.Activated",
        dpName + ".State.Initialized",
        dpName + ".State.ErrorMessage");
    }
    else {
      DebugTN(dpName + " group disconnect");      
      dpDisconnect("DescriptionGroupCb",
        dpName + ".VisibleName",
        dpName + ".Owner",
        dpName + ".ID",
        dpName + ".Type",
        dpName + ".Scopes",
        dpName + ".Recipients");
    }
    dpName = Table.nextDisconnectRequest();
  }
  //----
  //--- Connect
  nProcessed = 0;
  dpName = Table.nextConnectRequest();
  while(dpName != "") {
    nProcessed++;
    if((nProcessed % 50) == 0){
      ConnectText.foreCol = "{0,255,0}";
      ConnectText.text = "" + nProcessed;
    }
    string dpType = dpTypeName(dpName);
    if(dpType == SMS_CONFIG_DP_TYPE) {
      DebugTN(dpName + " config connect");      
      dpConnect("ConfigCb",
        dpName + ".DpNumber",
        dpName + ".State.Activate",
        dpName + ".State.Activated",
        dpName + ".State.Initialized",
        dpName + ".State.ErrorMessage");
    }
    else {
      DebugTN(dpName + " group connect");      
      dpConnect("DescriptionGroupCb",
        dpName + ".VisibleName",
        dpName + ".Owner",
        dpName + ".ID",
        dpName + ".Type",
        dpName + ".Scopes",
        dpName + ".Recipients");
    }
    dpName = Table.nextConnectRequest();
  }
  ConnectText.text = "";
}

/**
@brief selectionChanged needed for ewo NotificationGroupTable
@details EWO Event Function
*/
void notificationGroupTableEwoSelectionChanged() {
  //DebugTN("vacPanelNotifications: selectionChanged()");
  //SetButtonSensitivity
  dyn_string selected = Table.getSelectedGroup();
  if (dynlen(selected) < 3) {
    return;
  }
  string dpName = selected[1];
  bool exist = dpName != "";
  string state = selected[2];
  bool isInactive = state == "Inactive";
  string setup = selected[3];
  bool isSetup = setup == "true";
  
  bool myConfig = false;
  if(exist) {
    myConfig = VacSmsIsMyConfiguration(dpName);
  }
  EditPb.enabled = exist;
  DeletePb.enabled = exist && myConfig;
  //ActivatePb.enabled = exist && myConfig && isInactive;
  //DeactivatePb.enabled = exist && myConfig && (!isInactive);
}
/**
@brief tableDoubleClick needed for ewo NotificationGroupTable
@details EWO Event Function
*/
void notificationGroupTableEwoDoubleClick(int row, int col) {
  //Process Edit
  string dpName = Table.getSelectedConfig();
  if(dpName == "") {
    return;
  }
  string dpType = dpTypeName(dpName);
  bool isGroup = dpType != SMS_CONFIG_DP_TYPE;
  OpenDetailPanel(dpName, isGroup);
}
//
//
//
// =====================
// PRIVATE FUNCTIONS
// =====================
//
//
//
/**
@brief Update the list of group in NotificationGroupTable
@details Private Function
*/
void updateGroupList() {
  Table.clear();
  dyn_string dpList = dpNames("*", SMS_CONFIG_GROUP_DP_TYPE);
  int nDps = dynlen(dpList);
  for(int n = 1 ; n <= nDps ; n++) {
    ProgressText.text = "Analyzing group config " + n + "/" + nDps;
    string dpName = dpSubStr(dpList[n], DPSUB_DP);
    if(groupShallBeVisible(dpName)) {
      dyn_string configDpList = VacSmsBuildGroupContent(dpName);
      Table.addGroup(dpName, configDpList);
    }
  }
  //notificationGroupTableEwoSelectionChanged();
  ProgressText.text = "";
}

bool MayEditConfiguration()
{
  if(ActivatedText.visible) {
    dyn_string exceptionInfo;
    fwException_raise(exceptionInfo, "ERROR", "MayEditConfiguration(): editing activated configuration is not allowed", "");
    fwExceptionHandling_display(exceptionInfo);
    return false;
  }
  return true;
}
void SetNewDescription(string newDescription, string dpName, bool isMyConfig) {
  //DebugTN("---------------------------SetNewDescription ", newDescription);  
  if(!isMyConfig) {
    return;
  }
  string oldDescription;
  //DebugTN("---------------------------SetNewDescription ", dpName);  
  dpGet(dpName + ".VisibleName", oldDescription);
  if(oldDescription == newDescription) {
    return;
  }
  if(newDescription == "") {
    return;
  }
  dpSet(dpName + ".VisibleName", newDescription);
}
int machineModeCode(string sMode) {
  for(int mode = 0 ; mode < 32 ; mode++) {
    float fCode = pow(2.0, (float)mode);
    string resourceName = "MachineMode" + mode;
    string modeName = VacResourcesGetValue(resourceName, VAC_RESOURCE_STRING, "");
    if(modeName == sMode) {
      return (int)fCode;
    }
  }
  return 0;
}
bool groupShallBeVisible(string dpName) {
  if(myGroupCheckBox.state(0)) {
    string owner;
    dpGet(dpName + ".Owner", owner);
    string user = LhcVacGetUserName();
    if(user != owner) {
        return false;
    }
  }
  return true;
}
void getGroupTypeList(dyn_string &names, dyn_int &scopeTypes) {
  dynClear(names);
  dynClear(scopeTypes);
  string groupResource = "Notification.Group";
  string nameResourceSuffix = ".Name";
  string scopeResourceSuffix = ".Scope";
  int typeNb = 0;  
  bool isAnotherType = true;
  while (isAnotherType) {
    typeNb ++;    
    string val = VacResourcesGetValue(groupResource + typeNb + nameResourceSuffix, VAC_RESOURCE_STRING, "NoGroupType");
    if ( (typeNb == 1) && (val == "NoGroupType") ) {
      string errMessage = "vacPanelNotifications.ctl: +++++ FATAL: getGroupTypeList() No group type defined in resources file";
      dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
      throwError(err);
      return;
    }
    else if (val == "NoGroupType") {
      isAnotherType = false;
    }
    else {
      string scopeT = VacResourcesGetValue(groupResource + typeNb + scopeResourceSuffix, VAC_RESOURCE_STRING, "NoScopeType");
      if (scopeT == "NoGroupType") {
        string errMessage = "vacPanelNotifications.ctl: +++++ FATAL: getGroupTypeList() No scope type for group" + typeNb;
        dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
        throwError(err);
        return;
      }
      dynAppend(names, val);
      if (scopeT == "Sector") {
        dynAppend(scopeTypes, SCOPE_TYPE_SECTOR);
      }
      else if (scopeT == "MainPart") {
      dynAppend(scopeTypes, SCOPE_TYPE_MAIN_PART);
      }
      else {
        string errMessage = "vacPanelNotifications.ctl: +++++ FATAL: getGroupTypeList() scope type " +  scopeT + " unknown";
        dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
        throwError(err);
        return;     
      }    
    }  
  }    
}
/**
@brief If not exist, create single confid DP from notification Group
@details Private Function
*/
void createConfigDp(string configVisibleName, string &dpName, dyn_string &exceptionInfo) {
  // Check if already exist if yes do not create but just report dpName
  dyn_string dpList = dpNames("*", SMS_CONFIG_DP_TYPE);
  bool isExist = false;
  for(int n = dynlen(dpList); n > 0; n--) {
    string name;
    dpGet(dpList[n] + ".VisibleName", name);
    if(name == configVisibleName) {
      dpName =  dpList[n];     
      isExist = true;
      return;      
    }
  }
  if(!isExist) {
    string text = "SELECT 'MAX(_original.._value)' FROM '*.DpNumber' WHERE (_DPT=\"" +
      SMS_CONFIG_DP_TYPE + "\")";
    dyn_dyn_anytype tab;
    dpQuery(text, tab);
    int nReturned = dynlen(tab);
    int dpNumber = 1;
    if(nReturned > 1) {
      dpNumber = tab[2][2] + 1;
    }
    for( ; dpNumber <= 2000000000 ; dpNumber++) {
      sprintf(dpName, "%s_%d", SMS_CONFIG_DP_TYPE, dpNumber);
      if(dpExists(dpName)) {
        continue;
      }
      dpCreate(dpName, SMS_CONFIG_DP_TYPE);
      if(!dpExists(dpName)) {
        fwException_raise(exceptionInfo, "ERROR", "createConfigDp(): failed to create DP <" + dpName + ">", "");
        dpName = "";
      }
      dpSetWait(dpName + ".DpNumber", dpNumber);
      // Set config visible name    
      dpSetWait(dpName + ".VisibleName", configVisibleName);  
      return;
    }
    fwException_raise(exceptionInfo, "createConfigDp(): too many DPs have been created", "");
    dpName = "";
  }  
}
/**
@brief Common Function Create DP Notification Group
@details Private Function
*/
void createGroupDp(string &dpName, int &id, dyn_string &exceptionInfo) {
  int	dpNumber = 1;
  for(int dpNumber = 1 ; dpNumber <= 999999 ; dpNumber++) {
    sprintf(dpName, "%s_%06d", SMS_CONFIG_GROUP_DP_TYPE, dpNumber);
    if(dpExists(dpName)) {
      continue;
    }
    dpCreate(dpName, SMS_CONFIG_GROUP_DP_TYPE);
    id = dpNumber;
    if(!dpExists(dpName)) {
      fwException_raise(exceptionInfo, "ERROR", "LhcSmsCreateConfigGroupDp(): failed to create DP <" + dpName + ">", "");
      dpName = "";
    }
    return;
  }
  fwException_raise(exceptionInfo, "LhcSmsCreateConfigGroupDp(): too many DPs have been created", "");
  dpName = "";
}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// =====================
// CALL BACK FUNCTIONS
// =====================
//
//
//
void DescriptionGroupCb(string dpe1, string description, string dpe2, string owner,
                        string dpe3, ulong id,           string dpe4, int type, 
                        string dpe5, string scopes,      string dpe4, string recipients) {
  string dpName = dpSubStr(dpe1, DPSUB_DP);
  Table.setGroupOwner(dpName, owner);
  Table.setGroupName(dpName, description);
  Table.setGroupId(dpName, id);
  string groupResource = "Notification.Group";
  string nameResourceSuffix = ".Name";
  string sType = VacResourcesGetValue(groupResource + type + nameResourceSuffix, VAC_RESOURCE_STRING, "None");
  Table.setGroupType(dpName, sType);
  Table.setGroupScopes(dpName, scopes);
  Table.setGroupRecipients(dpName, recipients);
}

void ConfigCb(string dpe1, int id, string dpe2, bool isActivate, 
              string dpe3, bool isActivated, string dpe4, bool isInitialized,
              string dpe4, string error) {
  string dpName = dpSubStr(dpe1, DPSUB_DP);
  Table.setConfigId(dpName, id);
  Table.setConfigFlags(dpName, isActivate, isActivated, isInitialized);
  Table.setConfigError(dpName, error);
}

/*
void StateDpeConfigCb(string dpeActive,      bool isDpe1, 
                      string dpeActivated,   bool isDpe2,
                      string dpeInitialized, bool isDpe3) {
  string configDpName = dpSubStr(dpeActive, DPSUB_DP);
  string groupDpName;
  dpGet(configDpName + ".Group", groupDpName);  
  //DebugTN("Set Flags ", groupDpName); 
  dyn_string dpList = dpNames("*", SMS_CONFIG_DP_TYPE);
  dyn_bool actives, activateds, initializeds;
  bool isActive = true, isActivated = true, isInitialized  = true;
  int nb = 0;
  for(int n = 1; n <= dynlen(dpList); n++) {
    string groupLinked;
    bool flag;    
    dpGet(dpList[n] + ".Group", groupLinked);
    if(groupLinked == groupDpName) {
      nb ++;
      dpGet(dpList[n] + ".State.Activate", flag);
      //DebugTN(dpList[n] + ".State.Activate", flag);
      if(!flag) {
        isActive = false;
      }
      dpGet(dpList[n] + ".State.Activated", flag);
      if(!flag) {
        isActivated = false;
      }
      dpGet(dpList[n] + ".State.Initialized", flag);
      if(!flag) {
        isInitialized = false;
      }
    }    
  }
  if(nb == 0) {
    isActive = false;
    isActivated = false;
    isInitialized  = false;
  }
  //DebugTN("Activate bool before set", isActive);
  Table.setGroupFlags(groupDpName, isActive, isActivated, isInitialized);
}

void ErrorDpeConfigCb(string dpeError, string isDpe1) {
  string configDpName = dpSubStr(dpeError, DPSUB_DP);
  string groupDpName;
  dpGet(configDpName + ".Group", groupDpName);
  //DebugTN("----- ERROR_MSG ---- group: ", groupDpName);  
  dyn_string dpList = dpNames("*", SMS_CONFIG_DP_TYPE);
  string msg = "";
  int nb = 0;
  for(int n = 1; n <= dynlen(dpList); n++) {
    string groupLinked;
    string flag;    
    dpGet(dpList[n] + ".Group", groupLinked);
    if(groupLinked == groupDpName) {
      dpGet(dpList[n] + ".State.ErrorMessage", flag);
      if(flag != "") {
        nb ++;        
        string dp = dpSubStr( dpList[n], DPSUB_DP );
        int dpNb;
        dpGet(dp + ".DpNumber", dpNb);
        msg = "configID" + dpNb + " " + flag;
        //DebugTN("----- ERROR_MSG ---- configID" + dpNb + " ", flag);
        Table.setGroupError(groupDpName, msg);
      }
    }    
  }
  if(nb == 0){
    Table.setGroupError(groupDpName, msg);
  }
}

void StatisticsCb(string dpe1, int nMessages, string dpe2, string errMsg, string dpe3, uint nEvents) {
  string dpName = dpSubStr(dpe1, DPSUB_DP);
  Table.setConfigNumMsg(dpName, nMessages);
// !!!!! TODO error
  Table.setConfigNumEvents(dpName, nEvents);
}
*/

void GroupNameCb(string dpe, string name) {
  DescriptionText.text = name;
}

void ScopeTypeCb(string dpe, int groupType) {
  string dpName = dpSubStr( dpe, DPSUB_DP ); 
  string groupResource = "Notification.Group";
  string scopeResourceSuffix = ".Scope";  
  string scopeT = VacResourcesGetValue(groupResource + groupType + scopeResourceSuffix, VAC_RESOURCE_STRING, "NoScopeType");
  if (scopeT == "NoGroupType") {
    string errMessage = "vacPanelNotifications.ctl: +++++ FATAL: getGroupTypeList() No scope type for group" + typeNb;
    dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    throwError(err);
    return;
  }
  else if (scopeT == "Sector") {
    G_scopeType = SCOPE_TYPE_SECTOR;
  }
  else if (scopeT == "MainPart") {
    G_scopeType = SCOPE_TYPE_MAIN_PART;
  }
  else {
    //string errMessage = "vacPanelNotifications.ctl: +++++ FATAL: getGroupTypeList() unknown scope type " + scopeT;
    //dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
    //throwError(err);
    return;
  }   
}

void GroupTypeCb(string dpe, int type) {
  //DebugTN("-----------GroupTypeCb dpe: " + dpe + " type: ", type);  
  groupTypeComboBox.selectedPos(type + 1);
  string groupResource = "Notification.Group";
  string nameResourceSuffix = ".Modes";
  string sMode = VacResourcesGetValue(groupResource + type + nameResourceSuffix, VAC_RESOURCE_STRING, "Unknown");
  machineModeText.text(sMode);
}

void ScopeCb(string dpe1, dyn_string names, string dpe2, int type) {
  if(dynlen(names) == 0) {
    ScopeText.text = "Not defined";
  }
  else {
    string text = dynlen(names) + " ";
    switch(type)
    {
    case SCOPE_TYPE_SECTOR:
      text += "sector(s)";
      break;
    case SCOPE_TYPE_MAIN_PART:
      text += "main part(s)";
      break;
    default:
      text += "unknwon name(s)";
      break;
    }
    ScopeText.text = text;
  }
}

void RecipientsCb(string dpe, dyn_string recipients) {
  if(dynlen(recipients) == 0) {
    RecipientsText.text = "No recipients";
  }
  else {
    RecipientsText.text = dynlen(recipients) + " recipient(s)";
  }
}
void ActivateDpeConfigCb(dyn_string dpes, dyn_bool activateRequests) {
  bool visible = false;  
  for(int n = 1; n <= dynlen(activateRequests); n++) {
    if(activateRequests[n]) {
      visible = true;
    }
  }
  ActivateText.visible(visible);  
}  
void ActivatedDpeConfigCb(dyn_string dpes, dyn_bool activateds) {
  bool visible = true;  
  for(int n = 1; n <= dynlen(activateds); n++) {
    if(!activateds[n]) {
      visible = false;
    }
  }
  ActivatedText.visible(visible);  
}    
void InitializedDpeConfigCb(dyn_string dpes, dyn_bool initializeds) {
  bool visible = true;  
  for(int n = 1; n <= dynlen(initializeds); n++) {
    if(!initializeds[n]) {
      visible = false;
    }
  }
  InitializedText.visible(visible);  
}    
void MessageSentDpeConfigCb(dyn_string dpes, dyn_int messagesSents) {
  int sent = 0;  
  for(int n = 1; n <= dynlen(messagesSents); n++) {
    sent = sent + messagesSents[n];
  }
  MsgCountText.text(sent);  
}
void EventCountDpeConfigCb(dyn_string dpes, dyn_int events) {
  int eventCount = 0;  
  for(int n = 1; n <= dynlen(events); n++) {
    eventCount = eventCount + events[n];
  }
  EventCountText.text(eventCount);  
}
void ErrorMessageDpeConfigCb(dyn_string dpes, dyn_string errs) {
  string error = "";  
  for(int n = 1; n <= dynlen(errs); n++) {
    if(errs[n] != "") {
      string dp = dpSubStr( dpes[n], DPSUB_DP );
      int dpNb;
      dpGet(dp + ".DpNumber", dpNb);
      error = "configID" + dpNb + " " + errs[n];
    }
  }
  ErrorText.text(error);  
}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// ==================
// COMMON FUNCTIONS
// ==================
//
//
//
/**
@brief Create corresponding config dp from notification Group
@details Common Function
*/
dyn_string VacNotificationCreateConfigs(string groupDpName) {
  // ------------------  
  // ------ Deactivate
  // ------------------
  VacNotificationDeactivate(groupDpName);
  // ------------------  
  // ------ Activate
  // ------------------  
  dyn_string exceptionInfo;  
  int groupType;
  dyn_string createdDps;
  // ---  
  // --- unlink previous configs with this group
  VacNotificationUnlinkGroup(groupDpName);
  // ---  
  // --- Get Parameter setup of the group
  // Get Notification Type of group  
  dpGet(groupDpName + ".Type", groupType);
  // Get Notification Modes and convert to a code (2^n)  
  string modeList = VacResourcesGetValue("Notification.Group" + groupType + ".Modes", VAC_RESOURCE_STRING, "");
  dyn_string sModes = strsplit(modeList, "|");
  int modesCode = 0; 
  for (int n = 1; n <= dynlen(sModes); n ++) {    
    modesCode = modesCode + machineModeCode(sModes[n]);
    //DebugTN("---- VacNotificationCreateConfigs() ---- " + groupDpName + "  modesCode=",  modesCode);   
  }    
  // Get configs
  string configNbList = VacResourcesGetValue("Notification.Group" + groupType + ".Confs", VAC_RESOURCE_STRING, "");
  dyn_string sConfigNbs = strsplit(configNbList, "|");
  
  for (int n = 1; n <= dynlen(sConfigNbs); n ++) {
    //dynAppend(configNbs, (int)sConfigNbs[n]);
    long groupId;
    dpGet(groupDpName + ".ID", groupId); 
    string configVisibleName = "_group" + groupId + "_config" + sConfigNbs[n];    
    // Create config DP    
    string configDpName;
    //bool isCreated = false;
    createConfigDp(configVisibleName, configDpName, exceptionInfo);
    dynAppend(createdDps, configDpName);
    // Set config message    
    string message = VacResourcesGetValue("Notification.Conf" + sConfigNbs[n] + ".Message", VAC_RESOURCE_STRING, "");
    dpSetWait(configDpName + ".Message", message);
    // Set config recipients     
    dyn_string recipients;
    dpGet(groupDpName + ".Recipients", recipients);    
    dpSetWait(configDpName + ".Recipients", recipients);
    // Set config scope        
    int scopeType;
    dpGet(groupDpName + ".ScopeType", scopeType);    
    dpSetWait(configDpName + ".Scope.Type", scopeType);
    dyn_string scopes;
    dpGet(groupDpName + ".Scopes", scopes);    
    dpSetWait(configDpName + ".Scope.Names", scopes);
    // Set config criteria    
    dyn_int functionalTypes;
    int eqpType = VacResourcesGetValue("Notification.Conf"+ sConfigNbs[n] + ".EqpType", VAC_RESOURCE_INT, 0);
    //DebugTN("eqpType: ", eqpType);  
    dynAppend(functionalTypes, eqpType);
    //DebugTN("functionalTypes: ", functionalTypes);  
    dpSetWait(configDpName + ".Criteria.FunctionalTypes", functionalTypes);
    dyn_int minDurations;
    int minDur = (int)VacResourcesGetValue("Notification.Conf"+ sConfigNbs[n] + ".MinDuration", VAC_RESOURCE_INT, 0);
    dynAppend(minDurations, minDur);
    dpSetWait(configDpName + ".Criteria.MinDurations", minDurations);
    string reverse = VacResourcesGetValue("Notification.Conf"+ sConfigNbs[n] + ".Reverse", VAC_RESOURCE_STRING, "False");
    bool isReverse = false;
    if (reverse == "True") {
      isReverse = true;
    }    
    dpSetWait(configDpName + ".Criteria.ReverseFlags", makeDynBool(isReverse));
    // Criteria
    string criteria = VacResourcesGetValue("Notification.Conf"+ sConfigNbs[n] + ".FunctionalCriteria", VAC_RESOURCE_STRING, "");    
    dyn_int funcTypes, types, subTypes;
    dyn_string criterias;    
    int ret = VacSmsGetFuncTypeCriteria(eqpType, funcTypes, types, subTypes, criterias);
    int type = 0;
    int subType = 0;
    bool found = false;    
    for (int n = 1; n <= dynlen(criterias); n ++) {
      if (criteria == criterias[n]) {
        found = true;
        type = types[n];
        subType = subTypes[n];
        break;
      }
    }
    if (!found) {
        string errMessage = "vacPanelNotifications: +++++ FATAL: Criteria name " + criteria + " not found in code";
        dyn_errClass err = makeError("vcErrors", PRIO_SEVERE, ERR_CONTROL, 2, errMessage);
        throwError(err);
    }    
    dpSetWait(configDpName + ".Criteria.Types", makeDynInt(type));    
    dpSetWait(configDpName + ".Criteria.Subtypes", makeDynInt(subType));
    // Lower limit
    dyn_float lowerLimits;
    string sLowerLimit = VacResourcesGetValue("Notification.Conf"+ sConfigNbs[n] + ".LowerLimit", VAC_RESOURCE_STRING, "0");
    float lowerLimit = (float)sLowerLimit;    
    dynAppend(lowerLimits, lowerLimit);
    dpSetWait(configDpName + ".Criteria.LowerLimits", lowerLimits);
    // Upper limit
    dyn_float upperLimits;
    string sUpperLimit = VacResourcesGetValue("Notification.Conf"+ sConfigNbs[n] + ".UpperLimit", VAC_RESOURCE_STRING, "0");
    float upperLimit = (float)sUpperLimit;    
    dynAppend(upperLimits, upperLimit);
    dpSetWait(configDpName + ".Criteria.UpperLimits", upperLimits);

    // Set config State
    dpSetWait(configDpName + ".State.Activate", true);
    // Set config owner     
    string owner;
    dpGet(groupDpName + ".Owner", owner);    
    dpSetWait(configDpName + ".Owner", owner);
    // Set config group         
    dpSetWait(configDpName + ".Group", groupDpName);
    // Set Mode
    if(modesCode == 0) {
      dpSetWait(configDpName + ".State.ErrorMessage", "Error: No mode");
    }
    else {
      //DebugTN(" Mode : " + configDpName + ".Mode", modesCode);  
      dpSetWait(configDpName + ".Mode", modesCode);
    }
    // Reset all other dpe in case of config dp already exist before
    //int intReset;    
    //dpSetWait(configDpName + ".MsgFiltering.Type", intReset);
    //dpSetWait(configDpName + ".MsgFiltering.DeadTime", intReset);
    //dpSetWait(configDpName + ".MsgFiltering.Grouping.Type", intReset);
    //dpSetWait(configDpName + ".MsgFiltering.Grouping.Interval", intReset);
    //dpSetWait(configDpName + ".MsgFiltering.Grouping.Count", intReset);
    //dyn_int dynIntReset;
    //dpSetWait(configDpName + ".Scope.UsageFilter.FunctionalTypes", dynIntReset);
    dpSetWait(configDpName + ".Criteria.JoinType", 0);
    dpSetWait(configDpName + ".State.MessagesSent", 0);
    dpSetWait(configDpName + ".State.EventCount", 0);
  }
  updateGroupList();
  //DebugTN("list of config: ", configNbs);  
  //return createdDps;  
}  
/**
@brief Deactivate corresponding config dp from notification Group
@details Common Function
*/
void VacNotificationDeactivate(string groupDpName) {
  dyn_string dpList = dpNames("*", SMS_CONFIG_DP_TYPE);
  for(int n = dynlen(dpList); n > 0; n--) {
    string groupLinked;
    dpGet(dpList[n] + ".Group", groupLinked);
    if(groupLinked == groupDpName) {
      dpSet(dpList[n] + ".State.Activate", false);      
      //DebugTN(dpList[n] + " deactivated");
    }
  }
}
/**
@brief Unlink corresponding config dp from notification Group
@details Common Function
*/
void VacNotificationUnlinkGroup(string groupDpName) {
  dyn_string dpList = dpNames("*", SMS_CONFIG_DP_TYPE);
  for(int n = dynlen(dpList); n > 0; n--) {
    string groupLinked;
    dpGet(dpList[n] + ".Group", groupLinked);
    if(groupLinked == groupDpName) {
      dpSet(dpList[n] + ".Group", "");      
      //DebugTN(dpList[n] + " unlinked");
    }
  }
}

/**
@brief dpConnect to activation dpe
@details Common Function

void VacNotificationActivationDpConnect(string groupDpName,
                                        dyn_string &dpNameConfigs, dyn_string &Activate_dpeConfigs , 
                                                                   dyn_string &Activated_dpeConfigs ,
                                                                   dyn_string &Initialized_dpeConfigs , 
                                                                   dyn_string &MessagesSent_dpeConfigs ,
                                                                   dyn_string &ErrorMessage_dpeConfigs, 
                                                                   dyn_string &EventCount_dpeConfigs) {
  if(dynlen(Activate_dpeConfigs) > 0) {  
    dpDisconnect("ActivateDpeConfigCb", Activate_dpeConfigs);
  }
  if(dynlen(Activate_dpeConfigs) > 0) {    
    dpDisconnect("ActivatedDpeConfigCb", Activated_dpeConfigs);    
  }
  if(dynlen(Activate_dpeConfigs) > 0) {
    dpDisconnect("InitializedDpeConfigCb", Initialized_dpeConfigs);    
  }
  if(dynlen(Activate_dpeConfigs) > 0) {
    dpDisconnect("MessageSentDpeConfigCb", MessagesSent_dpeConfigs);
  }
  if(dynlen(Activate_dpeConfigs) > 0) {
    dpDisconnect("ErrorMessageDpeConfigCb", ErrorMessage_dpeConfigs);
  }
  if(dynlen(Activate_dpeConfigs) > 0) {
    dpDisconnect("EventCountDpeConfigCb", EventCount_dpeConfigs);
  }
  // 
  dynClear(dpNameConfigs);
  dynClear(Activate_dpeConfigs);
  dynClear(Activated_dpeConfigs);
  dynClear(Initialized_dpeConfigs); 
  dynClear(MessagesSent_dpeConfigs); 
  dynClear(ErrorMessage_dpeConfigs); 
  dynClear(EventCount_dpeConfigs);
  dyn_string dpList = dpNames("*", SMS_CONFIG_DP_TYPE);
  int nb = 0;  
  for(int n = 1; n <= dynlen(dpList); n++) {
    string groupLinked;
    dpGet(dpList[n] + ".Group", groupLinked);
    if(groupLinked == groupDpName) {
      nb++;      
      dynAppend(dpNameConfigs, dpList[n]);
      dynAppend(Activate_dpeConfigs, dpList[n] + ".State.Activate");
      dynAppend(Activated_dpeConfigs, dpList[n] + ".State.Activated");
      dynAppend(Initialized_dpeConfigs, dpList[n] + ".State.Initialized");
      dynAppend(MessagesSent_dpeConfigs, dpList[n] + ".State.MessagesSent");
      dynAppend(ErrorMessage_dpeConfigs, dpList[n] + ".State.ErrorMessage");
      dynAppend(EventCount_dpeConfigs, dpList[n] + ".State.EventCount");
    }
  }
  ConfigNumberText.text(nb);
  //
  if(dynlen(dpNameConfigs) > 0) {
    dpConnect("ActivateDpeConfigCb", Activate_dpeConfigs);    
    dpConnect("ActivatedDpeConfigCb", Activated_dpeConfigs);    
    dpConnect("InitializedDpeConfigCb", Initialized_dpeConfigs);    
    dpConnect("MessageSentDpeConfigCb", MessagesSent_dpeConfigs);
    dpConnect("ErrorMessageDpeConfigCb", ErrorMessage_dpeConfigs);
    dpConnect("EventCountDpeConfigCb", EventCount_dpeConfigs);
  }
}
*/
