#ifndef	EQPV8DI_FE_H
#define	EQPV8DI_FE_H

//	8 DI monitor for interlocks with FE timestamping

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpV8DI_FE : public Eqp
{
	Q_OBJECT

public:
	EqpV8DI_FE(const Eqp &source);
#ifdef Q_OS_WIN
	EqpV8DI_FE(const EqpV8DI_FE &src) : Eqp(src) {}
#endif
	~EqpV8DI_FE();

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getItlDi0String(QString &string, DataEnum::DataMode mode);
	virtual void getItlDi0Color(QColor &color, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline unsigned getRR2(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr2Replay : rr2Online; }
	virtual int getErrorCode(DataEnum::DataMode mode);
	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR2 DPE for online mode
	unsigned	rr2Online;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for replay mode
	unsigned	rr2Replay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getItlDi0Color(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getItlDi0String(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
#endif
	
};

#endif	// EQPV8DI_FE_H
