#ifndef LHCREGION_H
#define LHCREGION_H

#include "VacCtlEqpDataExport.h"

// Class holding LHC region information

#include <stdio.h>

class VACCTLEQPDATA_EXPORT LhcRegion
{
public:
	// Region types
	enum
	{
		None = 0,
		BlueOut = 1,
		RedOut = 2,
		Cross = 3
	};

	// Constructor
	LhcRegion(float start, float end, int type);

	// Access
	inline float getStart(void) { return start; };
	inline void setStart(float start) { this->start = start; }
	inline float getEnd(void) { return end; }
	inline void setEnd(float end) { this->end = end; }
	inline int getType(void) { return type; }

	// Other methods
	bool isCoordInside(float coord);

	void dump(FILE *pFile, int idx);

protected:
	// Start coordinate of region
	float	start;

	// End coordinate of region;
	float	end;

	// Region type - see const above
	int		type;
};

#endif	// LHCREGION_H
