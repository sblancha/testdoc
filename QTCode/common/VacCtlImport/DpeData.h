#ifndef DPEDATA_H
#define	DPEDATA_H

#include "VacCtlImportExport.h"

//	Class holding information on one DPE

#include "FileParser.h"

#include <qstring.h>

class ImportPool;

#define DEFAULT_ADDRESS_TYPE	"S7"
#define CMW_ADDRESS_TYPE	"CMW"

class VACCTLIMPORT_EXPORT DpeData : public FileParser
{
public:
	DpeData(const char *name, const char *dpName,  int lineNo);
	virtual ~DpeData() {}

	static void setImportPool(ImportPool *pImportPool);
	static DpeData *createFromFile(FILE *pFile, int &lineNo, const char *dpName,
		const char *name, QStringList &errList);

	void dump(FILE *pFile);

	// Access
	inline const char *getName(void) { return name; }
	inline const char *getDpName(void) { return dpName; }
	inline const char *getAddressType(void) { return addressType; }
	inline int getDrvNum(void) { return drvNum; }
	inline const char *getReference(void) { return reference; }
	inline int getDirection(void) { return direction; }
	inline const char *getTransType(void) { return transType; }
	inline int getOldNew(void) { return oldNew; }
	inline const char *getConnect(void) { return connect; }
	inline const char *getGroup(void) { return group; };
	inline int getArchiveIndex(void) { return archiveIndex; }
	inline const char *getValueFormat(void) { return valueFormat; }
	inline int getLineNo(void) { return lineNo; }
	inline bool isAddressChange(void) { return addressChange; }
	inline void setAddressChange(bool change) { addressChange = change; }
	inline int getArchiveChange(void) { return archiveChange; }
	inline void setArchiveChange(int change) { archiveChange = change; }
	inline bool isValueFormatChange(void) { return valueFormatChange; }
	inline void setValueFormatChange(bool change) { valueFormatChange = change; }
	inline bool isClearAll(void) { return clearAll; }
	inline void setClearAll(bool clear) { clearAll = clear; }
	inline int getUpdateCounter(void) { return updateCounter; }
	inline void setUpdateCounter(int counter) { updateCounter = counter; }
	inline const QString &getOldArchiveName(void) const { return oldArchiveName; }
	inline void setOldArchiveName(const char *name) { oldArchiveName = name; }
	inline int getArchSmoothType(void) const { return archSmoothType; }
	inline int getArchTimeInterval(void) const { return archTimeInterval; }
	inline int getArchDeadBand(void) const { return archDeadBand; }
	inline const char *getExportToCMW(void) const { return exportToCMW; }
	inline const char *getExportToDIP(void) const { return exportToDIP; }
	inline bool isDipConfigChange(void) const { return dipConfigChange; }
	inline void setDipConfigChange(bool value) { dipConfigChange = value; }

protected:
	// DPE name
	char				*name;

	// Name of DP where this DPE appears - used for error messages
	const char			*dpName;

	// Periphery address type
	char				*addressType;

	// Driver number for periphery address
	int					drvNum;

	// Reference for periphery address
	char				*reference;

	// Direction for periphery address
	int					direction;

	// Transfer type for periphery address
	char				*transType;

	// Old/new compare flag for periphery address
	int					oldNew;

	// Connection name for periphery address
	char				*connect;

	// Group name for periphery address
	char				*group;

	// Index of PVSS archive in list of archives (first archive has index 1)
	int					archiveIndex;

	// Smooth type for archiving of this DPE 11.10.2012
	int					archSmoothType;

	// Time interval for archiving smoothing of this DPE 11.10.2012
	int					archTimeInterval;

	// Dead band for archiving smoothing of this DPE 11.10.2012
	int					archDeadBand;

	// Value format
	char				*valueFormat;

	// Line number in input file where this DPE was defined
	int					lineNo;

	// The following flags are NOT set as a result of file processing -
	// they are set by PVSS code who compares old and new DPE parameters

	// Flag indicating that periphery address for DPE shall be changed
	bool				addressChange;

	// Flag (enum in PVSS code) indicating that archiving parameters for DPE shall be changed
	int					archiveChange;

	// Flag indicating that value format for DPE shall be changed
	bool				valueFormatChange;

	// Flag indicating that all settings for DPE shall be removed
	bool				clearAll;

	// The counter of DPE value updates - used only by lhcVacMobileEqp.ctl script
	int					updateCounter;

	// Old archive name
	QString				oldArchiveName;

	// Name for publishing to CMW
	char				*exportToCMW;

	// Name for publishing to DIP
	char				*exportToDIP;

	// Flag indicating that DIP config parameters for DPE shall be changed
	bool				dipConfigChange;

	// Reference to ImportPool instance
	static ImportPool		*pPool;

	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);
	void checkConsistency(QStringList &errList);
};

#endif	// DPEDATA_H
