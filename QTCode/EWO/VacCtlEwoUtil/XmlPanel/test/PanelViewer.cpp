
#include "PanelViewer.h"

PanelViewer::PanelViewer(QWidget *parent, const QString &fileName) :
	QMainWindow(parent, 0)
{
	setWindowTitle("XML image viewer");
	pSynoptic = new XmlSynViewer(this, fileName);
	setCentralWidget(pSynoptic);
}

PanelViewer::~PanelViewer()
{
}

