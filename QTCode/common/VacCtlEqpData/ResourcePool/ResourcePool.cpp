//	Implementation of ResourcePool class
///////////////////////////////////////////////////////////////////////////////////////

#include "ResourcePool.h"
#include "EqpMsgCriteria.h"

#include "PlatformDef.h"

#include <QRegExp>
#include <QStringList>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

// Type of section being processed in input file
enum
{
	SectionTypeResourceAll = 0,		// Section for all machines where resources shall be read
	SectionTypeResourceMachine = 1,	// Section for selected machines where resources shall be read
	SectionTypeEqpTypeMask = 2,		// Section where equipment type masks shall be read
	SectionTypeSkip = 3				// Section shall be skipped
};

ResourcePool ResourcePool::instance;

ResourcePool &ResourcePool::getInstance(void)
{
	return instance;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ResourcePool::ResourcePool()
{
	pResources = new QHash<QString, ResourceValue *>();
	pEqpMasks = new QList<ResourceEqpMask *>();
	mode = ParseResources;
	reportResourceNotFound = false;
}

ResourcePool::~ResourcePool()
{
	while(!pEqpMasks->isEmpty())
	{
		delete pEqpMasks->takeFirst();
	}
	delete pEqpMasks;
	foreach(ResourceValue *pValue, *pResources)
	{
		delete pValue;
	}
	delete pResources;
}

/*
**
**	FUNCTION
**		Read and parse input file with resources
**
**	PARAMETERS
**		machineName	- Name of machine for which resources are read
**		fileName	- Name of file where resources shall be read
**		errList		- File parsing errors will be added to this variable
**
**	RETURNS
**		Number of resources for this machine read from file, or
**		-1 in case of fatal error
**
**	CAUTIONS
**		None
*/
int ResourcePool::parseFile(const char *machineName, const char *fileName, QStringList &errList)
{
	// Prepare...
	this->machineName = machineName;
	pResources->clear();
	mode = ParseResources;
	reportResourceNotFound = false;

	// Open file
	if(!fileName)
	{
		errList.append("Empty file name");
		return -1;
	}
	if(!*fileName)
	{
		errList.append("Empty file name");
		return -1;
	}
	sectionType = SectionTypeResourceAll;	// Default
	return FileParser::parseFile(fileName, errList);
}

/*
**
** FUNCTION
**		Process tokens of single line read from input file
**
** PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
** RETURNS
**		1	- tokens have been processed successfully, more lines are
**				expected.
**		0	- tokens have been processed successfully, no more lines are
**				expected.
**		-1	- tokens have not been processed, i.e. error in input file.
**
** CAUTIONS
**		None
*/
int ResourcePool::processLineTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	const QString & /* longValue */, QStringList &errList)
{
	if(nTokens == 1)	// Can be section start
	{
		return parseSectionStart(lineNo, tokens[0], errList);
	}

	// Resource value
	if(sectionType == SectionTypeSkip)
	{
		return 1;
	}
	else if(sectionType == SectionTypeEqpTypeMask)
	{
		int value;
		if(sscanf(tokens[1], "%d", &value) != 1)
		{
			errList.append(QString("ResourcePool::processLineTokens(): line %1: bad value format <%2> for <%3>").arg(lineNo).arg(tokens[1]).arg(tokens[0]));
			return -1;
		}
		pEqpMasks->append(new ResourceEqpMask(tokens[0], value));
	}
	else
	{
		ResourceValue *pValue = pResources->value(tokens[0]);
		if(pValue)	// Already known resource
		{
			// Resources from machine section override other resource
			if(sectionType == SectionTypeResourceMachine)
			{
				delete pResources->take(tokens[0]);
				pValue = new ResourceValue(tokens[1], sectionType);
				pResources->insert(tokens[0], pValue);
			}
		}
		else	// New resource
		{
			pValue = new ResourceValue(tokens[1], sectionType);
			pResources->insert(tokens[0], pValue);
		}
	}
	return 1;
}

/*
**
** FUNCTION
**		Process start of section token
**
** PARAMETERS
**		lineNo	- Line number in file.
**		token	- Word read from file.
**		errList	- Variable where error message shall be written
**					in case of errors.
**
** RETURNS
**		1	- tokens have been processed successfully, more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
** CAUTIONS
**		None
*/
int ResourcePool::parseSectionStart(int lineNo, char *token, QStringList &errList)
{
	if(*token != '[')	// No, it is not section start
	{
		errList.append(QString("Error in line %1: unexpected <%2>").arg(lineNo).arg(token));
		return -1;
	}
	int strLen = (int)strlen(token);
	if(token[strLen - 1] != ']')	// Section must be in [] brackets
	{
		errList.append(QString("Error in line %1: bad format for section start <%2>").arg(lineNo).arg(token));
		return -1;
	}
	token[strLen - 1] = '\0';	// Remove ] at the end of line
	token++;
	char *sectionName = FileParser::trim(token);
	if(!strcasecmp(sectionName, "Diagnostics"))
	{
		reportResourceNotFound = true;
	}
	else if(!strcasecmp(sectionName, "EqpTypeMask"))
	{
		sectionType = SectionTypeEqpTypeMask;
	}
	else if(!strcasecmp(sectionName, "All"))
	{
		sectionType = SectionTypeResourceAll;
	}
	else if(!strcmp(sectionName, machineName.toLatin1()))
	{
		sectionType = SectionTypeResourceMachine;
	}
	else
	{
		sectionType = SectionTypeSkip;
	}
	return 1;
}

/*
**
** FUNCTION
**		Get integer value of resource with given name
**
** PARAMETERS
**		name	- Resource name
**		result	- Variable where resulting value will be put
**
** RETURNS
**		OK			- resource with given name was found;
**		NotFound	- resource was not found
**
** CAUTIONS
**		None
*/
int ResourcePool::getIntValue(const QString &name, int &result)
{
	result = 0;
	ResourceValue *pValue = pResources->value(name);
	if(!pValue)
	{
		return NotFound;
	}
	result = pValue->getValue().toInt();
	return OK;
}

/*
**
** FUNCTION
**		Get unsigned integer value of resource with given name
**
** PARAMETERS
**		name	- Resource name
**		result	- Variable where resulting value will be put
**
** RETURNS
**		OK			- resource with given name was found;
**		NotFound	- resource was not found
**
** CAUTIONS
**		None
*/
int ResourcePool::getUintValue(const QString &name, unsigned &result)
{
	ResourceValue *pValue = pResources->value(name);
	if(!pValue)
	{
		return NotFound;
	}
	bool success;
	result = pValue->getValue().toUInt(&success, 16);
	return OK;
}

/*
**
** FUNCTION
**		Get float value of resource with given name
**
** PARAMETERS
**		name	- Resource name
**		result	- Variable where resulting value will be put
**
** RETURNS
**		OK			- resource with given name was found;
**		NotFound	- resource was not found
**
** CAUTIONS
**		None
*/
int ResourcePool::getFloatValue(const QString &name, float &result)
{
	ResourceValue *pValue = pResources->value(name);
	if(!pValue)
	{
		return NotFound;
	}
	result = pValue->getValue().toFloat();
	return OK;
}

/*
**
** FUNCTION
**		Get boolean value of resource with given name
**
** PARAMETERS
**		name	- Resource name
**		result	- Variable where resulting value will be put
**
** RETURNS
**		OK			- resource with given name was found;
**		NotFound	- resource was not found
**
** CAUTIONS
**		None
*/
int ResourcePool::getBoolValue(const QString &name, bool &result)
{
	result = false;
	ResourceValue *pValue = pResources->value(name);
	if(!pValue)
	{
		return NotFound;
	}
	if((!strcmp(pValue->getValue().toLatin1(), "true")) ||
		(!strcmp(pValue->getValue().toLatin1(), "yes")))
	{
		result = true;
	}
	return OK;
}

/*
**
** FUNCTION
**		Get string value of resource with given name
**
** PARAMETERS
**		name	- Resource name
**		result	- Variable where resulting value will be put
**
** RETURNS
**		OK			- resource with given name was found;
**		NotFound	- resource was not found
**
** CAUTIONS
**		None
*/
int ResourcePool::getStringValue(const QString &name, QString &result)
{
	result.truncate(0);
	ResourceValue *pValue = pResources->value(name);
	if(!pValue)
	{
		return NotFound;
	}
	result = pValue->getValue();
	return OK;
}

/*
**
** FUNCTION
**		Get equipment functional type array value of resource with given name
**
** PARAMETERS
**		name	- Resource name
**		result	- Variable where resulting value will be put
**
** RETURNS
**		OK			- resource with given name was found;
**		NotFound	- resource was not found
**
** CAUTIONS
**		None
*/
int ResourcePool::getEqpTypesMask(const QString &name, VacEqpTypeMask &result)
{
	ResourceValue *pValue = pResources->value(name);
	if(!pValue)
	{
		return NotFound;
	}
	result.clear();
	QRegExp regExp("[ \\t]");	// Word breaks?
	QStringList strList = pValue->getValue().split(regExp, QString::SkipEmptyParts);
	foreach(ResourceEqpMask *pMask , *pEqpMasks)
	{
		for(QStringList::Iterator it = strList.begin() ; it != strList.end() ; ++it)
		{
			if((*it) == pMask->getName())
			{
				result.append(pMask->getValue());
				break;
			}
		}
	}
	return OK;
}
