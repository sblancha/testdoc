//	Implementation of EqpVP_VG_PS_CMW class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVP_VG_PS_CMW.h"

#include "DataPool.h"
#include "FunctionalTypeVG.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0

//	Main state bits to be analyzed
#define	RR1_STANDBY		(4)
#define RR1_PUMPING		(5)
#define	RR1_STOPPING	(6)
#define RR1_STARTING	(7)
#define	RR1_SUBLIMATING	(8)
#define	RR1_DEGASING	(9)
#define	RR1_ON			(10)
#define	RR1_OFF			(11)
#define	RR1_MODUL		(12)
#define	RR1_RUNNING		(13)
#define RR1_CYCLING		(14)
#define RR1_UNDEFINED	(1)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVP_VG_PS_CMW::EqpVP_VG_PS_CMW(const Eqp &source) : Eqp(source)
{
	rr1Online = rr1Replay = 0;
	prOnline = prReplay = 0;
	driverOkOnline = driverOkReplay = false;

	switch(ctrlSubType)
	{
	case 2:	// Pirani gauges VGR
		functionalType = FunctionalType::VGR;
		break;
	case 3:	// Penning gauges VGP
		functionalType = FunctionalType::VGP;
		break;
	case 4:	// Ion pumps VPI
		functionalType = FunctionalType::VPI;
		break;
	case 5:	// Ion gauge VGI
		functionalType = FunctionalType::VGI;
		break;
	}
}

EqpVP_VG_PS_CMW::~EqpVP_VG_PS_CMW()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_VG_PS_CMW::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "DriverOK");
	addDpeNameToList(stateDpes, "RR1");
}

/*
**	FUNCTION
**		Return list of all DPEs, related to analog value of device. The list is used
**		to build value history
**
**	ARGUMENTS
**		valueDpes	- Variable where all value DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_VG_PS_CMW::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	addDpeNameToList(valueDpes, "PR");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_VG_PS_CMW::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "DriverOK", mode);
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "PR", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_VG_PS_CMW::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "PR", mode);
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "DriverOK", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_VG_PS_CMW::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "PR"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(prOnline == newValue)
			{
				return;
			}
			prOnline = newValue;
		}
	}
	else if(!strcmp(dpe, "DriverOK"))
	{
		if(value.canConvert(QVariant::Bool))
		{
			bool newValue = value.toBool();
			if(driverOkOnline == newValue)
			{
				return;
			}
			driverOkOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "PR"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(prReplay == newValue)
			{
				return;
			}
			prReplay = newValue;
		}
	}
	else if(!strcmp(dpe, "DriverOK"))
	{
		if(value.canConvert(QVariant::Bool))
		{
			bool newValue = value.toBool();
			if(driverOkReplay == newValue)
			{
				return;
			}
			driverOkReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainColor(driverOkReplay, rr1Replay, prReplay, color);
	}
	else
	{
		getMainColor(driverOkOnline, rr1Online, prOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Return main value for gauge - pressure
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVP_VG_PS_CMW::getMainValue(DataEnum::DataMode mode)
{
	return mode == DataEnum::Replay ? prReplay : prOnline;
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		driverOk	- Value of DriverOK DPE
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getMainColor(bool driverOk, unsigned rr1, float /* pr */, QColor &color)
{
	if(!driverOk)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	switch(rr1)
	{
	case RR1_ON:
		color.setRgb(COLOR_ON);
		break;
	case RR1_OFF:
		color.setRgb(COLOR_OFF);
		break;
	default:
		color.setRgb(COLOR_UNDEFINED);
		break;
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainStateString(driverOkReplay, rr1Replay, prReplay, string);
	}
	else
	{
		getMainStateString(driverOkOnline, rr1Online, prOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getMainValueString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainValueString(driverOkReplay, rr1Replay, prReplay, string);
	}
	else
	{
		getMainValueString(driverOkOnline, rr1Online, prOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		driverOk	- Value of DriverOK DPE
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getMainValueString(bool /* driverOk */, unsigned /* rr1 */, float pr, QString &string)
{
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%8.2E", pr);
#else
	sprintf(buf, "%8.2E", pr);
#endif
	string = buf;
}
#endif


/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		driverOk	- Value of DriverOK DPE
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getMainStateString(bool driverOk, unsigned rr1, float /* pr */, QString &string)
{
	if(!driverOk)
	{
		string = "Driver error";
		return;
	}
	switch(rr1)
	{
	case RR1_STANDBY:
		string = "Standby";
		break;
	case RR1_PUMPING:
		string = "Pumping";
		break;
	case RR1_STOPPING:
		string = "Stopping";
		break;
	case RR1_STARTING:
		string = "Starting";
		break;
	case RR1_SUBLIMATING:
		string = "Sublimating";
		break;
	case RR1_DEGASING:
		string = "Degasing";
		break;
	case RR1_ON:
		string = "ON";
		break;
	case RR1_OFF:
		string = "OFF";
		break;
	case RR1_MODUL:
		string = "Modulating";
		break;
	case RR1_RUNNING:
		string = "Running";
		break;
	case RR1_CYCLING:
		string = "Cycling";
		break;
	case RR1_UNDEFINED:
		string = "Undefined";
		break;
	default:
		{
			char buf[32];
#ifdef Q_OS_WIN
			sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "Unknown (%d)", rr1);
#else
			sprintf(buf, "Unknown (%d)", rr1);
#endif
			string = buf;
		}
		break;
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainStateStringForMenu(driverOkReplay, rr1Replay, prReplay, string);
	}
	else
	{
		getMainStateStringForMenu(driverOkOnline, rr1Online, prOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		driverOk	- Value of DriverOK DPE
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getMainStateStringForMenu(bool driverOk, unsigned rr1, float pr, QString &string)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainValueString(driverOk, rr1, pr, valueString);
	string += valueString;
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getToolTipString(driverOkReplay, rr1Replay, prReplay, string);
	}
	else
	{
		getToolTipString(driverOkOnline, rr1Online, prOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		driverOk	- Value of DriverOK DPE
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getToolTipString(bool driverOk, unsigned rr1, float pr, QString &string)
{
	getMainStateStringForMenu(driverOk, rr1, pr, string);
}
#endif

/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("DriverOK");
	dpeNames.append("RR1");
	QList<QVariant *> values;
	if(!pPool->getDpValues(dpName, ts, dpeNames, values))
	{
		return;
	}
	if (values.isEmpty()) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	bool driverOk = pVariant->toBool();
	delete pVariant;
	if (values.isEmpty()) {
		return;
	}
	pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(driverOk, rr1, 0, state);
	getMainColor(driverOk, rr1, 0, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif





/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	addDpeNameToList(dpes, "DriverOK");
	addDpeNameToList(dpes, "RR1");
	addDpeNameToList(dpes, "PR");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history)
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVP_VG_PS_CMW::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	bool driverOk = mode == DataEnum::Replay ? driverOkReplay : driverOkOnline;
	if(!driverOk)
	{
		return criteria.isAccess() ? 1 : 0;
	}

	unsigned rr1 = mode ==  DataEnum::Replay ? rr1Replay : rr1Online;
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	switch(rr1)
	{
	case RR1_ON:
		state = DevListCriteria::EqpStateNormal;
		break;
	case RR1_OFF:
		state = DevListCriteria::EqpStateAbnormal;
		break;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if(rr1 == RR1_UNDEFINED)
		{
			return 1;
		}
	}

	// Check pressure if needed
	if(criteria.isUsePressureLimit())
	{
		float pr = mode == DataEnum::Replay ? prReplay : prOnline;
		if(pr > criteria.getPressureLimit())
		{
			return 1;
		}
	}

	// Last chance - criteria on pressure grow
	if(criteria.isUsePressureGrowLimit())
	{
		// Can not decide right now, more info is needed
		return 2;
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Return parameters for history query, required for device list
**
**	ARGUMENTS
**		dpe		- [out] DPE, for which history is required
**		start	- [out] start time of history query
**		end		- [out] end time of history query
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_VG_PS_CMW::getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUsePressureGrowLimit())
	{
		dpe = "";
		return;
	}
	dpe = dpName;
	dpe += ".PR";
	start = criteria.getPressureStartTime();
	if(criteria.isPressureEndTimeNow())
	{
		end = QDateTime::currentDateTime();
	}
	else
	{
		end = criteria.getPressureEndTime();
	}
}
#endif

/*
**	FUNCTION
**		Return grow factor for main value (pressure) to check if
**		device matches device list criteria
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Pressure grow factor; or
**		0 if criteria does not contain pressure grow
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVP_VG_PS_CMW::getGrowFactorForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUsePressureGrowLimit())
	{
		return 0.0;
	}
	return criteria.getPressureGrowLimit();
}

int EqpVP_VG_PS_CMW::getSpikeFilterForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUsePressureGrowLimit())
	{
		return 0;
	}
	return criteria.getPressureSpikeInterval();
}

#endif

/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_VG_PS_CMW::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// Without driver success does not match any criteria
	if(!driverOkOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// State is valid - decide based on criteria
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case FunctionalTypeVG::Off:
			if(rr1Online == RR1_OFF)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVG::On:
			if(rr1Online == RR1_ON)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	case EqpMsgCriteria::Error:
		if(rr1Online == RR1_UNDEFINED)
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else
		{
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	case EqpMsgCriteria::MainValue:
		if(rr1Online == RR1_ON)
		{
			// It can be (and it was observed) that callback for PR=10000
			// arrives before callback for RR1 with state=OFF. Reverse
			// situation can also occur when switching ON.
			// To avoid sending wrong messages in such cases value 10000
			// is considered to be invalid in all cases and does not change
			// criteria matching
			if(prOnline > 9999)
			{
				return;
			}
			if(pCriteria->isReverse())
			{
				if(prOnline > pCriteria->getUpperLimit())
				{
					pCriteria->setMatch(false, true, prOnline);
					return;
				}
				else if(prOnline < pCriteria->getLowerLimit())
				{
					pCriteria->setMatch(true, true, prOnline);
					return;
				}
				// Between lower and upper limit - no change
				return;
			}
			else
			{
				if(prOnline > pCriteria->getUpperLimit())
				{
					pCriteria->setMatch(true, true, prOnline);
					return;
				}
				else if(prOnline < pCriteria->getLowerLimit())
				{
					pCriteria->setMatch(false, true, prOnline);
					return;
				}
				// Between lower and upper limit - no change
				return;
			}
		}
		break;
	default:
		break;
	}
}

