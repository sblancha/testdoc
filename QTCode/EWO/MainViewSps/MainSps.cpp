//	Implementation of MainSps class
////////////////////////////////////////////////////////////////////////////////////

#include "MainSps.h"
#include "DataPool.h"

#include "Eqp.h"

#include <QLayout>
#include <QBitmap>
#include <QPainter>
#include <QHeaderView>

MainSps::MainSps(QWidget *parent, Qt::WindowFlags flags)
	: VacMainView(parent, flags)
{
	// Initialize members
	readOnly = false;

	// Build layout of control
	buildLayout();

	// Connect to signals
	connect(pImage, SIGNAL(mousePress(int, int, int, const char *)),
		this, SLOT(imageMousePress(int, int, int, const char *)));
	connect(pCombo, SIGNAL(activated(const QString &)),
		this, SLOT(comboActivated(const QString &)));
	connect(pAddStartPb, SIGNAL(clicked()), this, SLOT(addAtStart()));
	connect(pDelStartPb, SIGNAL(clicked()), this, SLOT(delAtStart()));
	connect(pAddEndPb, SIGNAL(clicked()), this, SLOT(addAtEnd()));
	connect(pDelEndPb, SIGNAL(clicked()), this, SLOT(delAtEnd()));
}

MainSps::~MainSps()
{
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void MainSps::buildLayout(void)
{
	// Images for +/- buttons
	QBitmap plusImage(7, 7);
	plusImage.clear();
	QPainter painterPlus(&plusImage);
	painterPlus.setPen(Qt::color1);
	painterPlus.drawLine(0, 3, 7, 3);
	painterPlus.drawLine(3, 0, 3, 7);

	QBitmap minusImage(7, 7);
	minusImage.clear();
	QPainter painterMinus(&minusImage);
	painterMinus.setPen(Qt::color1);
	painterMinus.drawLine(0, 3, 7, 3);

	// 1) Main layout - image on the left and all controls on the right
	QHBoxLayout *mainBox = new QHBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);
	pImage = new MainSpsImage(this);
	mainBox->addWidget(pImage, 10);

	// 2) Vertical layout with:
	//	- main part combo box
	//	- horizontal layout with 4 buttons and sector list
	QVBoxLayout *ctlBox = new QVBoxLayout();
	ctlBox->setSpacing(0);
	mainBox->addLayout(ctlBox);

	pMpBorder = new QGroupBox("Main parts", this);
	pMpBorder->setFlat(true);
	ctlBox->addWidget(pMpBorder);

	pCombo = new QComboBox(this);
	pCombo->setEditable(false);
	ctlBox->addWidget(pCombo);

	pSectorBorder = new QGroupBox("Sectors", this);
	pSectorBorder->setFlat(true);
	ctlBox->addWidget(pSectorBorder);

	QHBoxLayout *sectorCtl = new QHBoxLayout();
	sectorCtl->setContentsMargins(0, 0, 0, 0);
	sectorCtl->setSpacing(0);
	ctlBox->addLayout(sectorCtl);

	QSize buttonSize(15, 15);
	QVBoxLayout *buttonBox = new QVBoxLayout();
	buttonBox->setContentsMargins(0, 0, 0, 0);
	buttonBox->setSpacing(0);
	sectorCtl->addLayout(buttonBox);
	pAddStartPb = new QPushButton(plusImage, "", this);
//	pAddStartPb->setFlat(true);
	pAddStartPb->setFixedSize(buttonSize);
	buttonBox->addWidget(pAddStartPb);

	pDelStartPb = new QPushButton(minusImage, "", this);
//	pDelStartPb->setFlat(true);
	pDelStartPb->setFixedSize(buttonSize);
	buttonBox->addWidget(pDelStartPb);

	buttonBox->addStretch(10);

	pDelEndPb = new QPushButton(minusImage, "", this);
//	pDelEndPb->setFlat(true);
	pDelEndPb->setFixedSize(buttonSize);
	buttonBox->addWidget(pDelEndPb);

	pAddEndPb = new QPushButton(plusImage, "", this);
//	pAddEndPb->setFlat(true);
	pAddEndPb->setFixedSize(buttonSize);
	buttonBox->addWidget(pAddEndPb);

	pTable = new QTableWidget(1, 1, this);
	pTable->verticalHeader()->hide();
	pTable->horizontalHeader()->hide();
	pTable->horizontalHeader()->setStretchLastSection(true);
//	pTable->setLeftMargin(0);
//	pTable->setTopMargin(0);
//	pTable->setColumnReadOnly(0, true);
	pTable->setMaximumWidth(100);
	sectorCtl->addWidget(pTable, 10);

	/*
	pTestPb = new QPushButton("Test G", this, "test button");
	ctlBox->addWidget(pTestPb);
	connect(pTestPb, SIGNAL(clicked()), this, SLOT(testAction()));

	pTestPb2 = new QPushButton("Test R", this, "test button 2");
	ctlBox->addWidget(pTestPb2);
	connect(pTestPb2, SIGNAL(clicked()), this, SLOT(testAction2()));
	*/
}

/*
**	FUNCTION
**		Switch read-only mode of control: hide/show widgets which are not
**		used in read-only mode
**
**	ARGUMENTS
**		flag	- New value for read-only flag
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSps::setReadOnly(bool flag)
{
	if(flag == readOnly)
	{
		return;
	}
	readOnly = flag;
	if(readOnly)
	{
		pTable->hide();
		pAddEndPb->hide();
		pDelEndPb->hide();
		pDelStartPb->hide();
		pAddStartPb->hide();
		pSectorBorder->hide();
		pCombo->hide();
		pMpBorder->hide();
	}
	else
	{
		pMpBorder->show();
		pCombo->show();
		pSectorBorder->show();
		pAddStartPb->show();
		pDelStartPb->show();
		pDelEndPb->show();
		pAddEndPb->show();
		pTable->show();
	}
}

void MainSps::testAction(void)
{
	//printf("Test button pressed\n");
	const char *dp = "BI_VVS10";
	const char *dpe = "RR1";
	unsigned value = 0x620E0000;
	DataPool &pool = DataPool::getInstance();
	Eqp *pEqp = pool.findEqpByDpName(dp);
	if(pEqp)
	{
		//printf("DP found\n");
		QVariant *pVar = new QVariant(value);
		QDateTime now = QDateTime::currentDateTime();
		pEqp->newOnlineValue(dpe, *pVar, now);
		delete pVar;
	}
	emit selectionChanged();
}

void MainSps::testAction2(void)
{
	//printf("Test2 button pressed\n");
	const char *dp = "BI_VVS10";
	const char *dpe = "RR1";
	unsigned value = 0x620D0000;
	DataPool &pool = DataPool::getInstance();
	Eqp *pEqp = pool.findEqpByDpName(dp);
	if(pEqp)
	{
		//printf("DP found\n");
		QVariant *pVar = new QVariant(value);
		QDateTime now = QDateTime::currentDateTime();
		pEqp->newOnlineValue(dpe, *pVar, now);
		delete pVar;
	}
	emit selectionChanged();
}

void MainSps::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
/*
printf("MainSps::resizeEvent(): size hints are:\n");
QSize size = pMpBorder->sizeHint();
printf("  pMpBorder: %d x %d\n", size.width(), size.height());
size = pCombo->sizeHint();
printf("  pCombo: %d x %d\n", size.width(), size.height());
size = pSectorBorder->sizeHint();
printf("  pSectorBorder: %d x %d\n", size.width(), size.height());
size = pTable->sizeHint();
printf("  pTable: %d x %d\n", size.width(), size.height());
fflush(stdout);
*/
	/* Not clear why is it needed - always square??? L.Kopylov 06.09.2011
	pImage->setFixedWidth(pImage->height());
	*/

	/* Original variant below 27.08.2010 L.Kopylov
	// Fix width of sector list
	pTable->setMaximumWidth(pTable->width());
	*/
	// Fix widht of the only column
	pTable->setColumnWidth(0, pTable->width() - 4);
}

/*
**	FUNCTION
**		Notify control that data initialization has been finished
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Number of beam lines in data pool
**
**	CAUTIONS
**		None
*/
int MainSps::dataReady(void)
{
	pImage->renewImage();
	buildMpCombo();
	DataPool &pool = DataPool::getInstance();
	return pool.getLines().count();
}

/*
**	FUNCTION
**		Build content of combo box for main part selection. Only main parts,
**		having at least one sector with draw part on visible beam line, are shown
**		in combo
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSps::buildMpCombo(void)
{
	pCombo->clear();
	pCombo->addItem("");
	DataPool &pool = DataPool::getInstance();
	QStringList	strList;
	pool.findMainPartNames(strList, true);
	pCombo->addItems(strList);
	pTable->setRowCount(0);
	setSectorButtonsSens();
	emit selectionChanged();
}

/*
**	FUNCTION
**		Mouse has been pressed on sector in image. Depending on what button has been pressed:
**		- left button - update selection within this control
**		- right button - emit signal for outside world
**
**	ARGUMENTS
**		button		- what button was pressed, see Qt::ButtonState enum
**		x			- X-coordinate of mouse pointer
**		y			- Y-coordinate of mouse pointer
**		sectorName	- Name of sector where button has been pressed
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSps::imageMousePress(int button, int x, int y, const char *sectorName)
{
	switch(button)
	{
	case Qt::LeftButton:
		{
			DataPool &pool = DataPool::getInstance();
			Sector *pSector = pool.findSectorData(sectorName);
			Q_ASSERT(pSector);
			selectFromSector(pSector);
		}
		break;
	case Qt::RightButton:
		{
			emit sectorMouseDown(2, x, y, sectorName);
		}
		break;
	}
}

/*
**	FUNCTION
**		Make selection based on sector selected in machine image - select
**		main part of selected sector
**
**	ARGUMENTS
**		pSector	- selected sector
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSps::selectFromSector(Sector *pSector)
{
	MainPart *pMainPart = pSector->getFirstMainPart();
	QStringList sectorList;
	DataPool &pool = DataPool::getInstance();
	pool.findSectorsInMainPart(pMainPart->getName(), sectorList, true);
	setSelection(sectorList);
}

/*
**	FUNCTION
**		Slot - item has been selected in main part combo box
**
**	ARGUMENTS
**		string	- String of selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSps::comboActivated(const QString &string)
{
	DataPool &pool = DataPool::getInstance();
	MainPart *pMainPart = pool.findMainPartData(string.toLatin1());
	QStringList sectorList;
	if(pMainPart)	// No selection
	{
		pool.findSectorsInMainPart(pMainPart->getName(), sectorList, true);
	}
	setSelection(sectorList);
}

/*
**	FUNCTION
**		Slot - '+' button at the beginning of sector list has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSps::addAtStart(void)
{
	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pool.findNeighbourSector(pTable->item(0, 0)->text().toLatin1(), NULL, -1);
	if(pSector)
	{
		pTable->setRowCount(pTable->rowCount() + 1);
		for(int n = pTable->rowCount() - 1 ; n > 0 ; n--)
		{
			setTableCellText(n, 0, pTable->item(n - 1, 0)->text());
		}
		setTableCellText(0, 0, pSector->getName());
		pTable->resizeRowsToContents();
	}
	setSelectionFromTable();
}

/*
**	FUNCTION
**		Slot - '-' button at the beginning of sector list has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSps::delAtStart(void)
{
	pTable->removeRow(0);
	setSelectionFromTable();
}

/*
**	FUNCTION
**		Slot - '-' button at the end of sector list has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSps::delAtEnd(void)
{
	pTable->removeRow(pTable->rowCount() - 1);
	setSelectionFromTable();
}

/*
**	FUNCTION
**		Slot - '+' button at the end of sector list has been clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSps::addAtEnd(void)
{
	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pool.findNeighbourSector(pTable->item(pTable->rowCount() - 1, 0)->text().toLatin1(), NULL, 1);
	if(pSector)
	{
		pTable->setRowCount(pTable->rowCount() + 1);
		setTableCellText(pTable->rowCount() - 1, 0, pSector->getName());
		pTable->resizeRowsToContents();
	}
	setSelectionFromTable();
}

/*
**	FUNCTION
**		Set selection for given sector list
**
**	ARGUMENTS
**		sectorList - list of selected sector names
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSps::setSelection(QStringList &sectorList)
{
	// Update list of sectors in table
	pTable->setRowCount(sectorList.count());
	int n = 0;
	foreach(const QString sectorName, sectorList)
	{
		setTableCellText(n, 0, sectorName);
		n++;
	}
	pTable->resizeRowsToContents();
	setSelectionFromTable();
}

/*
**	FUNCTION
**		Set selection based on sector table content
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Sector list shall be filled in before calling this method
*/
void MainSps::setSelectionFromTable(void)
{
	// Update selection on image
	DataPool &pool = DataPool::getInstance();
	QList<Sector *> selectedSectors;
	for(int row = 0 ; row < pTable->rowCount() ; row++)
	{
		Sector *pSector = pool.findSectorData(pTable->item(row, 0)->text().toLatin1());
		if(pSector)
		{
			selectedSectors.append(pSector);
		}
	}
	pImage->setSelectedSectors(selectedSectors);

	setMainPartName();
	emit selectionChanged();
}

/*
**	FUNCTION
**		Set name of main part shown in combo box.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		List of sectors shall be updated BEFORE invoking this method
*/
void MainSps::setMainPartName(void)
{
	MainPart	*pMainPart = NULL;
	DataPool	&pool = DataPool::getInstance();
	for(int n = pTable->rowCount() - 1 ; n >= 0 ; n--)
	{
		Sector *pSector = pool.findSectorData(pTable->item(n, 0)->text().toLatin1());
		if(pMainPart)
		{
			if(!pSector->isInMainPart(pMainPart))
			{
				pMainPart = NULL;	// Different main parts
				break;
			}
		}
		else	// Take 1st main part of sector
		{
			pMainPart = pSector->getFirstMainPart();
		}
	}
	if(pMainPart)
	{
		pCombo->setCurrentIndex(pCombo->findText(pMainPart->getName()));
	}
	else
	{
		pCombo->setCurrentIndex(0);
	}
	setSectorButtonsSens();
}

/*
**	FUNCTION
**		Set sensitivity of add/remove buttons, controlling sector list
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainSps::setSectorButtonsSens(void)
{
	if(!pTable->rowCount())	// List is empty
	{
		pAddStartPb->setEnabled(false);
		pDelStartPb->setEnabled(false);
		pAddEndPb->setEnabled(false);
		pDelEndPb->setEnabled(false);
		return;
	}
	pDelStartPb->setEnabled(true);
	pDelEndPb->setEnabled(true);

	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pool.findNeighbourSector(pTable->item(0, 0)->text().toLatin1(), NULL, -1);
	if(pSector)
	{
		pAddStartPb->setEnabled(!tableContains(pSector->getName()));
	}
	else
	{
		pAddStartPb->setEnabled(false);
	}
	pSector = pool.findNeighbourSector(pTable->item(pTable->rowCount() - 1, 0)->text().toLatin1(), NULL, 1);
	if(pSector)
	{
		pAddEndPb->setEnabled(!tableContains(pSector->getName()));
	}
	else
	{
		pAddEndPb->setEnabled(false);
	}
}

/*
**	FUNCTION
**		Check if sector list contains item with given name
**
**	ARGUMENTS
**		name	- Name of sector to check
**
**	RETURNS
**		true	- if such name found in sector list;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool MainSps::tableContains(const char *name)
{
	for(int n = pTable->rowCount() - 1 ; n >= 0 ; n--)
	{
		if(pTable->item(n, 0)->text() == name)
		{
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Check if selected sector list forms continuous sequence of sectors,
**		i.e. it is possible to pass all sectors in list without passing
**		other sectors
**
**	PARAMETERS
**		None
**
** RETURNS
**		true  - if selected sector list form continuous sequence,
**		false - otherwise.
**
** CAUTIONS
**		None
*/
bool MainSps::isContSelection(void)
{
	// Build list of selected sectors
	if(!pTable->rowCount())
	{
		return false;
	}
	QList<Sector *> sectorList;
	DataPool &pool = DataPool::getInstance();
	for(int row = pTable->rowCount() - 1 ; row >= 0 ; row--)
	{
		Sector *pSector = pool.findSectorData(pTable->item(row, 0)->text().toLatin1());
		if(pSector)
		{
			sectorList.append(pSector);
		}
	}
	if(!sectorList.count())
	{
		return false;
	}
	return pool.isSectorListContinuous(sectorList);
}

/*
**	FUNCTION
**		Return list of selected sector names
**
**	PARAMETERS
**		None
**
** RETURNS
**		List of sector names, list can be empty
**
** CAUTIONS
**		None
*/
QStringList MainSps::getSelectedSectors(void)
{
	QStringList result;
	for(int row = 0 ; row < pTable->rowCount() ; row++)
	{
		result.append(pTable->item(row, 0)->text());
	}
	return result;
}

void MainSps::setTableCellText(int row, int col, const QString &text)
{
	if(pTable->item(row, col))
	{
		pTable->item(row, col)->setText(text);
	}
	else
	{
		pTable->setItem(row, col, new QTableWidgetItem(text));
	}
}

////////////////////////////////////////////////////////////////////////////////////////
////////////////// Access //////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//void MainSps::setFont(const QFont &font)
void MainSps::changeEvent(QEvent *pEvent)
{
	if(pEvent->type() == QEvent::FontChange)
	{
		pImage->setFont(font());
		pCombo->setFont(font());
		pTable->setFont(font());
		// QWidget::setFont(font);
		pImage->renewImage();
	}
}

////////////////////////////////////////////////////////////////////////////////////////
////////////////// Implementation of VacMainView abstract methods //////////////////////
////////////////////////////////////////////////////////////////////////////////////////
void MainSps::dialogRequest(int type, unsigned vacTypes, const QStringList &sectorList, DataEnum::DataMode mode)
{
	emit dialogRequested(type, vacTypes, sectorList, mode);
}
