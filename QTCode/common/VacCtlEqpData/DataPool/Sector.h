#ifndef SECTOR_H
#define SECTOR_H

#include "VacCtlEqpDataExport.h"

// Sector class - holds information about vacuum sector

#include "FileParser.h"
#include "MainPartMap.h"

#ifndef PVSS_SERVER_VERSION
#include "SectorDrawPart.h"
#endif	// PVSS_SERVER_VERSION

#include <qstring.h>
#include <qlist.h>
#include <qobject.h>



class DataPool;
class Eqp;

class Sector : public QObject, public FileParser
{
	Q_OBJECT

public:
	VACCTLEQPDATA_EXPORT Sector(const char *name);
	VACCTLEQPDATA_EXPORT virtual ~Sector();

	// [VACCO-948] [VACCO-1645]
	// Sector possible states:
	#define SECTOR_OPERATIONAL		(0x00000001)
	#define SECTOR_ONWORK			(0x00000002)
	#define SECTOR_VENTED			(0x00000004)

	static void setDataPool(DataPool *pPool) { pDataPool = pPool; }

	static Sector *createFromFile(FILE *pFile, int &lineNo,
		char **tokens, int nTokens, QStringList &errList);

	Sector *clone(void);

	void addEqp(Eqp *pEqp) { eqpList.append(pEqp); }

#ifndef PVSS_SERVER_VERSION
	void addDrawPart(BeamLine *pLine, BeamLinePart *pStartPart,
		int startEqpIdx, BeamLinePart *pEndPart, int endEqpIdx, bool isCircleClose);
#endif	// PVSS_SERVER_VERSION

	VACCTLEQPDATA_EXPORT void dump(short idx, FILE *pFile);

	// Implementation of FileParser abstract method
	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);

	// Access
	VACCTLEQPDATA_EXPORT inline const char *getName(void) const { return name; }
	VACCTLEQPDATA_EXPORT inline const char *getPvssName(void) const { return pvssName; }
	VACCTLEQPDATA_EXPORT inline int getVacType(void) const { return vacType; }
	VACCTLEQPDATA_EXPORT inline const QList<MainPartMap *> &getMainPartMaps(void) const { return mpMaps; }
	VACCTLEQPDATA_EXPORT inline Sector *getNewSector(void) const { return pNewSector; }
	VACCTLEQPDATA_EXPORT inline void setNewSector(Sector *pNewSector) { this->pNewSector = pNewSector; }
	VACCTLEQPDATA_EXPORT inline bool isOuter(void) const { return outer; }
	VACCTLEQPDATA_EXPORT inline void setOuter(bool outer) { this->outer = outer; }
	/*
	VACCTLEQPDATA_EXPORT inline bool isSpecSynoptic(void) const { return specSynoptic; }
	VACCTLEQPDATA_EXPORT inline void setSpecSynoptic(bool spec) { specSynoptic = spec; }
	*/
	VACCTLEQPDATA_EXPORT inline const QString &getSpecSynPanel(void) const { return specSynPanel; }
	VACCTLEQPDATA_EXPORT inline void setSpecSynPanel(const QString &name) { specSynPanel = name; }
	VACCTLEQPDATA_EXPORT inline const QString &getSpecSectPanel(void) const { return specSectPanel; }
	VACCTLEQPDATA_EXPORT inline void setSpecSectPanel(const QString &name) { specSectPanel = name; }
	VACCTLEQPDATA_EXPORT inline QList<Eqp *> &getEqpList(void) { return eqpList; }
	VACCTLEQPDATA_EXPORT inline int getBeamOrder(void) { return beamOrder; }
	VACCTLEQPDATA_EXPORT inline void setBeamOrder(int order) { this->beamOrder = order; }

	// Other methods
	VACCTLEQPDATA_EXPORT bool isInMainPart(MainPart *pMainPart);
	VACCTLEQPDATA_EXPORT const QString getDomainInMainPart(MainPart *pMainPart);
	VACCTLEQPDATA_EXPORT bool hasCommonMainPart(Sector *pOtherSect);
	VACCTLEQPDATA_EXPORT MainPart *getFirstMainPart(void);

		// [VACCO-929] Methods for the sector selection by the user
	VACCTLEQPDATA_EXPORT void setSelected(bool selected, bool flag);
	VACCTLEQPDATA_EXPORT inline bool isSelected(void) { return selected; }

	// [VACCO-948] [VACCO-1645] Methods for the vacuum on work status change
	VACCTLEQPDATA_EXPORT bool isOperational(void);
	VACCTLEQPDATA_EXPORT bool isOnWork(void);
	VACCTLEQPDATA_EXPORT bool isVented(void);
	VACCTLEQPDATA_EXPORT void stateChanged(unsigned newState);
	VACCTLEQPDATA_EXPORT QString getStateString(void);
	VACCTLEQPDATA_EXPORT inline void getMainStateStringForMenu(QString & string)
	{
		string = getName();
		string += ": ";
		string += getStateString();
	}

#ifndef PVSS_SERVER_VERSION
	VACCTLEQPDATA_EXPORT inline float getStart(void) { return start; }
	VACCTLEQPDATA_EXPORT inline void setStart(float start) { this->start = start; }
	VACCTLEQPDATA_EXPORT inline float getEnd(void) { return end; }
	VACCTLEQPDATA_EXPORT inline void setEnd(float end) { this->end = end; }

	VACCTLEQPDATA_EXPORT inline bool isOnRedBeam(void) { return onRedBeam; };
	VACCTLEQPDATA_EXPORT inline Eqp *getRedStartEqp(void) { return pRedStartEqp; }
	VACCTLEQPDATA_EXPORT inline float getRedStart(void) { return redStart; }
	VACCTLEQPDATA_EXPORT void setRedStartEqp(Eqp *pEqp);
	VACCTLEQPDATA_EXPORT inline Eqp *getRedEndEqp(void) { return pRedEndEqp; }
	VACCTLEQPDATA_EXPORT inline float getRedEnd(void) { return redEnd; }
	VACCTLEQPDATA_EXPORT void setRedEndEqp(Eqp *pEqp);

	VACCTLEQPDATA_EXPORT inline bool isOnBlueBeam(void) { return onBlueBeam; }
	VACCTLEQPDATA_EXPORT inline Eqp *getBlueStartEqp(void) { return pBlueStartEqp; }
	VACCTLEQPDATA_EXPORT inline float getBlueStart(void) { return blueStart; }
	VACCTLEQPDATA_EXPORT void setBlueStartEqp(Eqp *pEqp);
	VACCTLEQPDATA_EXPORT inline Eqp *getBlueEndEqp(void) { return pBlueEndEqp; }
	VACCTLEQPDATA_EXPORT inline float getBlueEnd(void) { return blueEnd; }
	VACCTLEQPDATA_EXPORT void setBlueEndEqp(Eqp *pEqp);

	VACCTLEQPDATA_EXPORT inline bool isOnCrossBeam(void) { return onCrossBeam; }
	VACCTLEQPDATA_EXPORT inline Eqp *getCrossStartEqp(void) { return pCrossStartEqp; }
	VACCTLEQPDATA_EXPORT inline float getCrossStart(void) { return crossStart; }
	VACCTLEQPDATA_EXPORT void setCrossStartEqp(Eqp *pEqp);
	VACCTLEQPDATA_EXPORT inline Eqp *getCrossEndEqp(void) { return pCrossEndEqp; }
	VACCTLEQPDATA_EXPORT inline float getCrossEnd(void) { return crossEnd; }
	VACCTLEQPDATA_EXPORT void setCrossEndEqp(Eqp *pEqp);

	VACCTLEQPDATA_EXPORT inline bool isRedOut(void) { return redOut; }

	VACCTLEQPDATA_EXPORT inline QList<SectorDrawPart *> &getDrawParts(void) { return drawParts; }
	VACCTLEQPDATA_EXPORT void finishBeamBorders(BeamLine *pLine);
	
#endif	// PVSS_SERVER_VERSION

signals:
	// [VACCO-929]
	// Notify listeners that the selection state of sector has changed
	//	Arguments:
	//		pSect		- Pointer to sector - source of signal (normally - this)
	void synSectSelectChanged(Sector *pSect);

	// [VACCO-948] [VACCO-1645] Notifies listeners that a vacuum sector onwork status changed
	void synSectStateChanged(Sector *pSect);

protected:
	// Pointer to data pool
	static DataPool			*pDataPool;

	// True name
	char					*name;

	// [VACCO-948] [VACCO-1645] Sector state, currently: Operational, OnWork, Vented
	unsigned int			state;

	// Name used in PVSS
	char					*pvssName;

	// Start coordinate of sector - used for QRL and CRYO sectors
	float					start;

	// End coordinate of sector - used for QRL and CRYO sectors
	float					end;

	// Vacuum type (subsystem) of sector (see class VacType)
	int						vacType;

	// Value used for ordering beam sectors of LHC ring
	int						beamOrder;

	// Mapping to main parts and domains
	QList<MainPartMap *>	mpMaps;

	// List of all devices in this sector - to speed up 'eqp in sector' functionality
	QList<Eqp *>			eqpList;

	// New pointer to this sector - used when building ordered sector list
	Sector					*pNewSector;

	// Flag indicating if sector is 'outer' sector, i.e. no equipment
	// in this sector controllable by our system.
	bool					outer;

	/*
	// Flag indicating if synoptic/sector view for this sector can not be drawn
	// automatically - manually made panel shall be used
	bool					specSynoptic;
	*/

	// Name of panel, built manually in PVSS, to be used as synoptic view for this sector
	QString					specSynPanel;

	// Name of panel, built manually in PVSS, to be used as sector view for this sector
	QString					specSectPanel;

#ifndef PVSS_SERVER_VERSION

	// Pointer to device at the start of sector on RED beam vacuum
	// Used only for beam vacuum sectors of LHC
	Eqp						*pRedStartEqp;

	// Start coordinate of sector on RED beam vacuum
	// Used only for beam vacuum sectors of LHC
	float					redStart;

	// Pointer to device at the end of sector on RED beam vacuum
	// Used only for beam vacuum sectors of LHC
	Eqp						*pRedEndEqp;

	// End coordinate of sector on RED beam vacuum
	// Used only for beam vacuum sectors of LHC
	float					redEnd;


	// Pointer to device at the start of sector on BLUE beam vacuum
	// Used only for beam vacuum sectors of LHC
	Eqp						*pBlueStartEqp;

	// Start coordinate of sector on RED beam vacuum
	// Used only for beam vacuum sectors of LHC
	float					blueStart;

	// Pointer to device at the end of sector on BLUE beam vacuum
	// Used only for beam vacuum sectors of LHC
	Eqp						*pBlueEndEqp;

	// End coordinate of sector on BLUE beam vacuum
	// Used only for beam vacuum sectors of LHC
	float					blueEnd;


	// Pointer to device at the start of sector on COMMON or CROSS beam vacuum
	// Used only for beam vacuum sectors of LHC
	Eqp						*pCrossStartEqp;

	// Start coordinate of sector on COMMON or CROSS beam vacuum
	// Used only for beam vacuum sectors of LHC
	float					crossStart;

	// Pointer to device at the end of sector on COMMON or CROSS beam vacuum
	// Used only for beam vacuum sectors of LHC
	Eqp					*pCrossEndEqp;

	// End coordinate of sector on COMMON or CROSS beam vacuum
	// Used only for beam vacuum sectors of LHC
	float					crossEnd;

	// Flag indicating if this sector has part located on RED beam vacuum
	// Used only for beam vacuum sectors of LHC
	bool					onRedBeam;

	// Flag indicating if this sector has part located on BLUE beam vacuum
	// Used only for beam vacuum sectors of LHC
	bool					onBlueBeam;

	// Flag indicating if this sector has part located on COMMON or CROSS beam vacuum
	// Used only for beam vacuum sectors of LHC
	bool					onCrossBeam;

	// Flag indicating if this sector is located on area where RED beam is outer beam
	// Used only for beam vacuum sectors of LHC
	bool					redOut;

	// Drawing parts of sector
	QList<SectorDrawPart *>	drawParts;

	// [VACCO-929] Flag indicating if sector is selected by the user.
	bool 			selected;

#endif	// PVSS_SERVER_VERSION
};

#endif	// SECTOR_H
