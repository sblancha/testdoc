#ifndef BEAMLINEPART_H
#define BEAMLINEPART_H

#include "VacCtlEqpDataExport.h"

// Class holding information for one part of beam line
// Eqipment in eqpList is placed in the following order:
// 1) active equipment read from file *.for_DLL, order of
//    this equipment is unpredictable
// 2) Passive equipment read from *.ora file, this equipment
//    appears in the order it appears in *.ora file, it is assumed
//    they are ordered according to their location when file
//    was generated

#include <stdio.h>

#include <qlist.h>

// Minimum allowed hole between two elements
#define	MIN_HOLE	((float)0.002)


class Eqp;
#include <QStringList>

class VACCTLEQPDATA_EXPORT BeamLinePart
{
public:
	virtual ~BeamLinePart();

	static BeamLinePart *create(char *name, char *start, char *end, int lineNo, QStringList &errList);
	static BeamLinePart *create(char *name, float start, float end);

	void processLinac2(void);

	bool addPassiveEqp(Eqp *pEqp);
	void addPureSurveyEqp(Eqp *pEqp);

	int coordInPart(float coord, float &resultX, float &resultY, Eqp **ppEqp, bool testEqp = false);

	void calculateGeoLimits(void);
	void clearActive(void);
	void buildOrderedEqp(void);
#ifndef PVSS_SERVER_VERSION
	void buildBeamSectorBorders(void);
#endif
	int getEqpIdx(Eqp *pEqp);
	const char *getEqpName(int eqpIdx);

	void shortenEqp(void);

	// Access
	inline const char *getName(void) const { return name; }

	inline float getStart(void) const { return start; }
	inline void setStart(float start) { this->start = start; }
	inline float getEnd(void) const { return end; }
	inline void setEnd(float end) { this->end = end; }

	inline QList<Eqp *> &getEqpList(void) { return *pEqpList; }
	inline const Eqp *getFirstEqp(void) const { return pFirstEqp; }
	inline float getMinX(void) const { return minX; }
	inline float getMaxX(void) const { return maxX; }
	inline float getMinY(void) const { return minY; }
	inline float getMaxY(void) const { return maxY; }
	inline bool isReverse(void) const { return reverse; }
	inline bool isReverseBeamDirection(void) const { return reverseBeamDirection; }
	inline void setReverseBeamDirection(bool flag) { reverseBeamDirection = flag; }
	inline Eqp **getEqpPtrs(void) const { return eqpPtrs; }
	inline int getNEqpPtrs(void) const { return nEqpPtrs; }

	void dump(FILE *pFile, int index);

protected:
	BeamLinePart(const char *name, float start, float end);

	// Name of beam line part
	const char		*name;

	// Start coordinate
	float		start;

	// End coordinate
	float		end;

	// List of all equipment in this part
	QList<Eqp *>	*pEqpList;

	// Pointer to first passive equipment in this part, active equipment is added
	// to the list BEFORE this equipment
	Eqp			*pFirstEqp;

	// Array of pointers to equipment, items in array are ordered according to coordinate
	Eqp			**eqpPtrs;

	// Number of equipment pointers in array
	int		nEqpPtrs;

	// Minimum X-coordinate for all devices in part (SURVEY)
	float		minX;

	// Maximum X-coordinate for all devices in part (SURVEY)
	float		maxX;

	// Minimum Y-coordinate for all devices in part (SURVEY)
	float		minY;

	// Maximum Y-coordinate for all devices in part (SURVEY)
	float		maxY;

	// true if part is used in revese order, i.e. start > end
	bool		reverse;

	// true if beam direction for icons in this line part shall be reverse compared to 'normal'
	bool		reverseBeamDirection;

	void coordInArray(float coord, float &resultX, float &resultY, bool testEqp = false);
	void coordInList(float coord, float &resultX, float &resultY);

};

#endif	// BEAMLINEPART_H
