#ifndef PROFILESECTOR_H
#define PROFILESECTOR_H


// Class holding parameters for one sector area in profile

#include "Sector.h"
#include "Eqp.h"

#include <QRect>
#include <QPainter>

class ProfileSector
{
public:
	ProfileSector(Sector *pSector);
	ProfileSector(const ProfileSector &source);
	~ProfileSector() {}

	void calculateLabelRect(QPainter &painter, int areaBottom);
	void moveLabelRectUp(void);
	void drawLabel(QPainter &painter);

	// Access
	inline Sector *getSector(void) const { return pSector; }
	inline void setSector(Sector *pSector) { this->pSector = pSector; }

	inline int getLeft(void) const { return left; }
	inline void setLeft(int coord) { left = coord; }

	inline int getRight(void) const { return right; }
	inline void setRight(int coord) { right = coord; }

	inline bool isLeftWithinDevice(void) const { return leftWithinDevice; }
	inline void setLeftWithinDevice(bool flag) { leftWithinDevice = flag; }

	inline bool isRightWithinDevice(void) const { return rightWithinDevice; }
	inline void setRightWithinDevice(bool flag) { rightWithinDevice = flag; }

	inline const QRect &getLabelRect(void) const { return labelRect; }

protected:
	// Vacuum sector corresponding to this area
	Sector	*pSector;

	// Rectangle = container for sector label
	QRect	labelRect;

	// Left coordinate of area
	int		left;

	// Right coordinate of area
	int		right;

	// Flag indicating if left coordinate is within device, shown in profile
	bool	leftWithinDevice;

	// Flag indicating if right coordinate is within device, shown in profile
	bool	rightWithinDevice;
};

#endif	// PROFILESECTOR_H
