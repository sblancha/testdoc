//	Implementation of EqpVR_PI class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVR_PI.h"

#include "EqpVP_IP.h"

#include "DataPool.h"
#include "FunctionalTypeVPI.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0
#define COLOR_WARNING			255,255,0		//yellow

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define	RR1_LOCAL		(0x10000000)
#define	RR1_MANUAL		(0x04000000)
#define	RR1_ON			(0x02000000)
#define	RR1_OFF			(0x01000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_POWER_CYCLE_IN_PROGRESS	(0x00020000)
#define	RR1_REINIT_IN_PROGRESS	(0x00010000)
#define RR1_WARNING_CODE_MASK	(0x0000FF00)
#define RR1_ERROR_CODE_MASK	(0x000000FF)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVR_PI::EqpVR_PI(const Eqp &source) : Eqp(source)
{
	rr1Online = rr1Replay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::VRPI;
}

EqpVR_PI::~EqpVR_PI()
{
}





/*
**	FUNCTION
**		Find slave (pumps or set of pumps) connected to this controller.
**
**	ARGUMENTS
**		slaves		- Variable where slave DP names will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_PI::getSlaves(QStringList &slaves)
{
	slaves.clear();
	DataPool &pool = DataPool::getInstance();

	// Add slaves acccording to controller visible name
	//const char *visibleName = getVisibleName();
	const QHash<QByteArray, Eqp *> &eqpList = pool.getEqpDict();
	foreach(Eqp *pEqp , eqpList) {
		QString eqpDpType = (QString)pEqp->getDpType();
		if ((eqpDpType == "VR_PI_CHAN") || (eqpDpType == "VA_RI")) {
			QString masterDpName;
			int channel;
			pEqp->getMaster(masterDpName, channel);
			if (masterDpName == (QString)dpName){
					slaves.append(pEqp->getDpName());
			}
		}
	}
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_PI::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "RR1");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_PI::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "RR1", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_PI::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "RR1", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_PI::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_PI::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_PI::dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_PI::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_PI::getMainColor(unsigned rr1, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr1 & RR1_ERROR)
	{
		color.setRgb(COLOR_ERROR);
	}
	else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if(rr1 & RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else if(rr1 & RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_PI::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_PI::getMainStateString(unsigned rr1, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr1 & RR1_ON)
	{
		if(rr1 & RR1_OFF)
		{
			string = "Error";
		}
		else
		{
			string = "Controller OK";
		}
	}
	else if(rr1 & RR1_OFF)
	{
		string = "Controller Stopped";
	}
	else
	{
		string = "Undefined";
	}
}
#endif


/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_PI::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("RR1");
	QList<QVariant *> values;
	if(!pPool->getDpValues(dpName, ts, dpeNames, values))
	{
		return;
	}
	if (values.isEmpty()) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, false, state);
	getMainColor(rr1, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpVR_PI::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	return rr1 & RR1_ERROR_CODE_MASK;
}




/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_PI::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "RR1");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVR_PI::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if((rr1 & (RR1_ON | RR1_OFF)) == RR1_ON)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if((rr1 & (RR1_ON | RR1_OFF)) == RR1_OFF)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if(rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if(criteria.isWarnings())
	{
		if(rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}
	return 0;
}
#endif



/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVR_PI::checkCriteria(EqpMsgCriteria *pCriteria)
{
	checkCriteria(pCriteria, 0);
}

void EqpVR_PI::checkCriteria(EqpMsgCriteria *pCriteria, float pr)
{
	// Only accept criteria for this functional type
	if((pCriteria->getFunctionalType() != functionalType) &&
		(pCriteria->getFunctionalType() != FunctionalType::VPI))
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if(checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	// Without PLC connection does not match any criteria
	if(plcAlarmOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// In invalid state - only matches Error/warning criteria
	if(!(rr1Online & RR1_VALID))
	{
		switch(pCriteria->getType())
		{
		case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		default:
			pCriteria->setMatch(false, false, 0);
			return;
		}
	}

	// State is valid - decide based on criteria
	unsigned maskedRr1 = rr1Online & (RR1_ON | RR1_OFF);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		if(pCriteria->getLowerLimit() > 0)	// OFF -> ON transition only if pressure if low enough
		{
			// Does criteria 'think' the recent state is OFF?
			bool recentStateOff = false;
			switch(pCriteria->getSubType())
			{
			case FunctionalTypeVPI::Off:
				if(pCriteria->isReverse())
				{
					recentStateOff = ! pCriteria->isMatch();
				}
				else
				{
					recentStateOff = pCriteria->isMatch();
				}
				break;
			case FunctionalTypeVPI::On:
				if(pCriteria->isReverse())
				{
					recentStateOff = pCriteria->isMatch();
				}
				else
				{
					recentStateOff = ! pCriteria->isMatch();
				}
				break;
			default:
				break;
			}
			// Make decision on new state (taking into account OFF -> ON transition)
			if(maskedRr1 == RR1_OFF)
			{
				if(pCriteria->getSubType() == FunctionalTypeVPI::Off)
				{
					pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				}
				else if(pCriteria->getSubType() == FunctionalTypeVPI::On)
				{
					pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				}
			}
			else if(maskedRr1 == RR1_ON)
			{
				if(recentStateOff)
				{
					if(pr < pCriteria->getLowerLimit())	// Became ON, even taking into account lower limit
					{
						if(pCriteria->getSubType() == FunctionalTypeVPI::On)
						{
							pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
						}
						else if(pCriteria->getSubType() == FunctionalTypeVPI::Off)
						{
							pCriteria->setMatch(pCriteria->isReverse(), false, 0);
						}
					}
					else	// Still OFF - taking into account lower limit
					{
						if(pCriteria->getSubType() == FunctionalTypeVPI::Off)
						{
							pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
						}
						else if(pCriteria->getSubType() == FunctionalTypeVPI::On)
						{
							pCriteria->setMatch(pCriteria->isReverse(), false, 0);
						}
					}
				}
				else
				{
					if(pCriteria->getSubType() == FunctionalTypeVPI::On)
					{
						pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
					}
					else if(pCriteria->getSubType() == FunctionalTypeVPI::Off)
					{
						pCriteria->setMatch(pCriteria->isReverse(), false, 0);
					}
				}
			}
			else	// State is neither ON nor OFF
			{
				if(pCriteria->getSubType() == FunctionalTypeVPI::On)
				{
					pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				}
				else if(pCriteria->getSubType() == FunctionalTypeVPI::Off)
				{
					pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				}
			}
			return;	// VERY IMPORTANT !!!
		}
		else	// 'old' style - based on RR1 only
		{
			switch(pCriteria->getSubType())
			{
			case FunctionalTypeVPI::Off:
				if(maskedRr1 == RR1_OFF)
				{
					pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
					return;
				}
				else
				{
					pCriteria->setMatch(pCriteria->isReverse(), false, 0);
					return;
				}
				break;
			case FunctionalTypeVPI::On:
				if(maskedRr1 == RR1_ON)
				{
					pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
					return;
				}
				else
				{
					pCriteria->setMatch(pCriteria->isReverse(), false, 0);
					return;
				}
				break;
			}
		}
		break;
	case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
	default:
		break;
	}
}

