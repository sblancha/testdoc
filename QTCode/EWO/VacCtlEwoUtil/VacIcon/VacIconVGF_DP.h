#ifndef	VACICONVGF_DP_H
#define	VACICONVGF_DP_H

//	Icon for VGF_DP - very similar to one for VG_STD - VGF(R+P)

#include "VacIconVG_STD.h"

class VACCTLEWOUTIL_EXPORT VacIconVGF_DP : public VacIconVG_STD
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVGF_DP(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVGF_DP();

	// Override VacIconVG_STD's methods
	virtual void setEqp(Eqp *pEqp);
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setDirection(VacIconContainer::Direction newDirection);

protected:
	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter);
	virtual void drawImageVertical(QPainter &painter, int offset);
	virtual void drawImageHorizontal(QPainter &painter, int offset);
	virtual void setPainterForPenning(QPainter &painter, QBrush &brush);

	// Dummy implementation of abstract method - never used
	virtual void drawSpecific(QPainter & /* painter */) {}
};

#endif	// VACICONVGF_DP_H
