#ifndef DATEEDITTABLEITEM_H
#define	DATEEDITTABLEITEM_H

#include "VacCtlEwoUtilExport.h"

// Subclass of QTableItem, new functionality is: date editor

#include <qtable.h>
#include <qdatetime.h>

#define DATE_EDIT_TABLE_ITEM	(12346)

class VACCTLEWOUTIL_EXPORT DateEditTableItem : public QTableItem
{
public:
	DateEditTableItem(QTable *table, EditType et) :
		QTableItem(table, et)
	{}

	DateEditTableItem(QTable *table, EditType et, const QString &text) :
		QTableItem(table, et, text)
	{}

	DateEditTableItem(QTable *table, EditType et, const QString &text, const QPixmap &p) :
		QTableItem(table, et, text, p)
	{}

	virtual QWidget *createEditor(void) const;
	virtual void setContentFromEditor(QWidget *pEditor);

	virtual int rtti(void) const { return DATE_EDIT_TABLE_ITEM; }

protected:
	// Value of date
	QDate		date;
};

#endif	// DATEEDITTABLEITEM_H
