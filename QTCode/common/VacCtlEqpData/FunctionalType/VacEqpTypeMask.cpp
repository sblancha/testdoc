//	Implementation of VacEqpTypeMask class
/////////////////////////////////////////////////////////////////////////////////

#include "VacEqpTypeMask.h"
#include "EqpMsgCriteria.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

VacEqpTypeMask::VacEqpTypeMask()
{
	// List contains instances from FunctionalType
	pTypeList = new QList<FunctionalType *>();
}

VacEqpTypeMask::VacEqpTypeMask(const VacEqpTypeMask &source)
{
	pTypeList = new QList<FunctionalType *>(*(source.pTypeList));
}	

VacEqpTypeMask & VacEqpTypeMask::operator = (const VacEqpTypeMask &other)
{
	if(this == &other)
	{
		return *this;
	}
	*pTypeList = *(other.pTypeList);
	return *this;
}

VacEqpTypeMask::~VacEqpTypeMask()
{
	delete pTypeList;
}

/*
**	FUNCTION
**		Build mask containing all possible types
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Type mask with all possible types
**
**	CAUTIONS
**		None
*/
VacEqpTypeMask VacEqpTypeMask::getAllTypes(void)
{
	QList<FunctionalType *> &types = FunctionalType::getTypes();
	VacEqpTypeMask allMask;
	foreach(FunctionalType *pType , types)
	{
		allMask.append(pType->getType());
	}
	return allMask;
}

/*
**	FUNCTION
**		Check if list contains item with given type
**
**	ARGUMENTS
**		type	- Type to check
**
**	RETURNS
**		true - if list contains given type;
**		false - otherwise
**
**	CAUTIONS
**		None
*/
bool VacEqpTypeMask::contains(int type) const
{
	foreach(FunctionalType *pItem , *pTypeList)
	{
		if(pItem->getType() == type)
		{
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Append given functional type to list
**
**	ARGUMENTS
**		type	- Type to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacEqpTypeMask::append(int type)
{
	if(contains(type))
	{
		return;
	}
	QList<FunctionalType *> &types = FunctionalType::getTypes();
	foreach(FunctionalType *pItem , types)
	{
		if(pItem->getType() == type)
		{
			pTypeList->append(pItem);
			break;
		}
	}
}

/*
**	FUNCTION
**		Remove given functional type from list
**
**	ARGUMENTS
**		type	- Type to be removed
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacEqpTypeMask::remove(int type)
{
	if(!contains(type))
	{
		return;
	}
	foreach(FunctionalType *pItem , *pTypeList)
	{
		if(pItem->getType() == type)
		{
			pTypeList->removeOne(pItem);
			break;
		}
	}
}

/*
**	FUNCTION
**		Check if this instance is equal to another instance. Two instances
**		are equal if they bot contain the same set ot types
**
**	ARGUMENTS
**		other	- Other instance to be compared with this instance.
**
**	RETURNS
**		true	- If other instance is equal to this instance;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacEqpTypeMask::equals(const VacEqpTypeMask &other)
{
	if(other.pTypeList->count() != pTypeList->count())
	{
		return false;
	}
	foreach(FunctionalType *pItem , *pTypeList)
	{
		if(!other.contains(pItem->getType()))
		{
			return false;
		}
	}
	// Most probably it is enough to make comparison in one direction only
	// because we already checked that number of types is equal in both instances
	return true;
}

/*
**	FUNCTION
**		Generate string listing all functional types
**
**	PARAMETERS
**		None
**
**	RETURNS
**		String with names of all functional types
**
**	CAUTIONS
**		None
*/
QString VacEqpTypeMask::toString(void)
{
	QString result;
	foreach(FunctionalType *pType , *pTypeList)
	{
		result += " ";
		result += pType->getName();
	}
	return result;
}

/*
**
** FUNCTION
**		Dump content for debugging purpose
**
** PARAMETERS
**		pFile	- Pointer to file opened for writing
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void VacEqpTypeMask::dump(FILE *pFile)
{
	if(!pFile)
	{
		return;
	}
	for(int n = 0 ; n < pTypeList->count() ; n++)
	{
		FunctionalType *pItem = pTypeList->at(n);
		fprintf(pFile, "    %d: type %d (%s): %s\n", n, pItem->getType(),
			pItem->getName(), pItem->getDescription());
	}
}
