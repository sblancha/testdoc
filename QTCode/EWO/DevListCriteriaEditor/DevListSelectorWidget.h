#ifndef DEVLISTSELECTORWIDGET_H
#define	DEVLISTSELECTORWIDGET_H

// Widget combinining scope and criteria selection widgets for device list

#include "EqpTypeSelectorWidget.h"
#include "DevListCriteriaEditorWidget.h"
#include "SingleEqpSelectorWidget.h"
#include "DataModeEditorWidget.h"

#include <qtabwidget.h>
#include <qvariant.h>

class DevListSelectorWidget : public QTabWidget
{
	Q_OBJECT

	Q_PROPERTY(int mode READ getMode WRITE setMode);
	Q_PROPERTY(int dataMode READ getDataMode WRITE setDataMode);
	Q_PROPERTY(QDateTime dataTime READ getDataTime WRITE setDataTime);


public:
	DevListSelectorWidget(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~DevListSelectorWidget();

	void initSectorContent(void) { pScopeSelector->initSectorContent(); }
	void setSelectedSectors(const QStringList &sectorList) { pScopeSelector->setSelectedSectors(sectorList); }
	void getSelectedSectors(QStringList &result) const { pScopeSelector->getSelectedSectors(result); }
	void getDpNames(QStringList &result) const { pScopeSelector->getDpNames(result); }
	bool isScopeEmpty(void) { return pScopeSelector->isEmpty(); }

	bool isNonVacSelected(void) const { return pScopeSelector->isNonVacSelected(); }
	void setNonVacSelected(bool flag) { pScopeSelector->setNonVacSelected(flag); }

	bool isCriteriaEmpty(void) const { return pCriteriaEditor->isCriteriaEmpty(); }
	bool isCriteriaValid(void) const { return pCriteriaEditor->isCriteriaValid(); }

	QList<QVariant> getCriteriaFlags(void) const { return pCriteriaEditor->getCriteriaFlags(); }
	void setCriteriaFlags(QList<QVariant> &flags) { pCriteriaEditor->setCriteriaFlags(flags); }

	QList<QVariant> getCriteriaValues(void) const { return pCriteriaEditor->getCriteriaValues(); }
	void setCriteriaValues(QList<QVariant> values) { pCriteriaEditor->setCriteriaValues(values); }

	QList<QVariant> getCriteriaTimes(void) const { return pCriteriaEditor->getCriteriaTimes(); }
	void setCriteriaTimes(QList<QVariant> values) { pCriteriaEditor->setCriteriaTimes(values); }

	QList<QVariant> getSelectedEqpTypes(void)  { return pScopeSelector->getSelectedEqpTypes(); }
	void setSelectedEqpTypes(QList<QVariant> &types) { pScopeSelector->setSelectedEqpTypes(types); }

	const QString getNextSelectedDp(void) { return pEqpSelector->getNextSelectedDp(); }


	// Access
	inline int getMode(void) const { return mode; }
	void setMode(int mode);
	int getDataMode(void) const;
	inline void setDataMode(int /* mode */) { /* dummy method */ }
	const QDateTime getDataTime(void) const;
	inline void setDataTime(const QDateTime &/* mode */) { /* dummy method */ }

signals:
	void scopeChanged(void);
	void criteriaChanged(void);
	void addEqpRequest(void);
	void modeChanged(int mode);
	void removeSelected(void);
	void removeAll(void);
	void dataModeChanged(void);

protected:
	// Sector selection widget
	EqpTypeSelectorWidget		*pScopeSelector;

	// Criteria editor widget
	DevListCriteriaEditorWidget	*pCriteriaEditor;

	// Single device selector widget
	SingleEqpSelectorWidget	*pEqpSelector;

	// Data mode editor
	DataModeEditorWidget		*pDataModeEditor;

	// Last mode of dialog: 0 = 'usual', 1 = 'manual eqp selection'
	int	mode;

private slots:
	void tabCurrentChanged(int tabIndex);
	void dataModeChange(void);

};

#endif	// DEVLISTSELECTORWIDGET_H
