#ifndef BEAMLINECONNTYPE_H
#define BEAMLINECONNTYPE_H

#include "VacCtlEqpDataExport.h"

// Definitions for beam line connection types

#include <QStringList>

class VACCTLEQPDATA_EXPORT BeamLineConnType
{
public:
	enum
	{
		None = 0,
		LeftIn = 1,
		LeftOut = 2,
		RightIn = 3,
		RightOut = 4
	};

	static int decode(const char *connType, int lineNo, QStringList &errList);
};

#endif	// BEAMLINECONNTYPE_H
