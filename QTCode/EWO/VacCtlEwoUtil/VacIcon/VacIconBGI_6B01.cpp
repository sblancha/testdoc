//	Implementation of VacIconBGI_6B01 class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconBGI_6B01.h"

#include "EqpProcess.h"

#include "VacType.h"
// #include "FunctionalType.h"
#include "VacEqpTypeMask.h"

#include <qpainter.h>


// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define	COLOR_STATE_ERROR		255,102,255
#define COLOR_SERVICE			192,0,255
#define COLOR_ERROR				255,0,0
#define	COLOR_WARNING			255,255,0

#define COLOR_WHITE			255,255,255
#define	COLOR_GREEN			0,255,0
#define	COLOR_YELLOW		255,255,0
#define	COLOR_RED			255,64,64
#define	COLOR_PURPLE		192,0,255

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_ON		(0x02000000)
#define	RR1_OFF		(0x01000000)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconBGI_6B01::VacIconBGI_6B01(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f), bgiLabel("BGI")
{
	pEqp = NULL;
	QFont myFont(font());
	bool fontChanged = false;
	if(myFont.pixelSize() > 0)
	{
		myFont.setPixelSize((int)rint(myFont.pixelSize() * 1.5));
		fontChanged = true;
	}
	else if(myFont.pointSize() > 0)
	{
		myFont.setPointSize((int)rint(myFont.pointSize() * 1.5));
		fontChanged = true;
	}
	if(fontChanged)
	{
		setFont(myFont);
	}
	QRect labelRect = fontMetrics().boundingRect("BGV");	// Use bigger of BGI/BGV
	setFixedSize(labelRect.width() + 6, labelRect.width() + 6);
}

VacIconBGI_6B01::~VacIconBGI_6B01()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		It is supposed that this method will not be used for CRYO_TT
*/
VacIconGeometry VacIconBGI_6B01::getGeometry(const VacEqpTypeMask & /* mask */)
{
#ifdef Q_OS_WIN
	return VacIconGeometry(width(), height(), width() >> 1, height() >> 1, 1);
#else
	return VacIconGeometry(width(), height(), width() >> 1, (height() >> 1) - 1, 1);
#endif
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconBGI_6B01::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpProcess"), "VacIconBGI_6B01::setEqp", pEqp->getDpName());
		this->pEqp = (EqpProcess *)pEqp;
		if(strstr(pEqp->getName(), "BGV"))
		{
			bgiLabel = "BGV";
		}
		else
		{
			bgiLabel = "BGI";
		}
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconBGI_6B01::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconBGI_6B01::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("RR2");
	dpes.append("RR4");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconBGI_6B01::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconBGI_6B01::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconBGI_6B01::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	QRect myRect = rect();
	painter.eraseRect(myRect);
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Beam pipe connections
	QPen pen(pipeColor, pipeWidth);
	painter.setPen(pen);
	painter.drawLine(0, myRect.height() >> 1, 3, myRect.height() >> 1);
	painter.drawLine(myRect.width() - 4, myRect.height() >> 1, myRect.width(), myRect.height() >> 1);

	int partHeight = (myRect.height() - 4) / 3;

	// Fill with color - upper parts with error/warning status
	setErrorColor(painter);
	painter.fillRect(2, 2, myRect.width() - 4, partHeight, painter.brush());

	// Fill with color - central part with text
	QRect labelRect = painter.fontMetrics().boundingRect(bgiLabel);
	setMainColor(painter);
	painter.fillRect(2, partHeight + 2, myRect.width() - 4, partHeight, painter.brush());

	// Fill with color - bottom part with mode color
	setModeColor(painter);
	painter.fillRect(2, partHeight * 2 + 2, myRect.width() - 4, partHeight, painter.brush());

	// common border
	painter.setPen(Qt::gray);
	painter.setBrush(Qt::NoBrush);
	painter.drawRect(2, 2, myRect.width() - 4, myRect.height() - 4);

	// Draw label
	painter.setPen(Qt::black);
	int labelY = 0;
	#ifdef Q_OS_WIN
		labelY = myRect.center().y() + (labelRect.height() >> 1) - 3;
	#else
		labelY = myRect.center().y() + (labelRect.height() >> 2);
	#endif
	painter.drawText(myRect.center().x() - (labelRect.width() >> 1) - 1, labelY, bgiLabel);
}

void VacIconBGI_6B01::setMainColor(QPainter &painter)
{
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
			color.setRgb(COLOR_NOT_CONTROL);
			break;
		case Eqp::NotConnected:
			color.setRgb(COLOR_NOT_CONNECTED);
			break;
		default:
			if(!pEqp->isPlcAlarm(mode))
			{
				unsigned rr1 = pEqp->getRR1(mode);
				if(rr1 & RR1_VALID)
				{
					unsigned objState = pEqp->getRR4(mode) >> 8;
					switch(objState)
					{
					case 0:
					case 7:
						color.setRgb(COLOR_RED);
						break;
					case 1:
						color.setRgb(COLOR_WHITE);
						break;
					case 2:
					case 3:
					case 4:
					case 5:
					case 9:
					case 11:
						color.setRgb(COLOR_GREEN);
						break;
					case 6:
						color.setRgb(COLOR_PURPLE);
						break;
					case 8:
					case 10:
					case 12:
						color.setRgb(COLOR_YELLOW);
						break;
					default:
						break;
					}
				}
			}
			break;
		}
	}
	QBrush brush(color);
	painter.setBrush(brush);
}

void VacIconBGI_6B01::setErrorColor(QPainter &painter)
{
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
			color.setRgb(COLOR_NOT_CONTROL);
			break;
		case Eqp::NotConnected:
			color.setRgb(COLOR_NOT_CONNECTED);
			break;
		default:
			if(!pEqp->isPlcAlarm(mode))
			{
				unsigned rr1 = pEqp->getRR1(mode);
				if(rr1 & RR1_VALID)
				{
					if(rr1 & RR1_ERROR)
					{
						color.setRgb(COLOR_ERROR);
					}
					else if(rr1 & RR1_WARNING)
					{
						color.setRgb(COLOR_WARNING);
					}
					else
					{
						color.setRgb(COLOR_ON);
					}
				}
			}
			break;
		}
	}
	QBrush brush(color);
	painter.setBrush(brush);
}

void VacIconBGI_6B01::setModeColor(QPainter &painter)
{
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
			color.setRgb(COLOR_NOT_CONTROL);
			break;
		case Eqp::NotConnected:
			color.setRgb(COLOR_NOT_CONNECTED);
			break;
		default:
			if(!pEqp->isPlcAlarm(mode))
			{
				unsigned rr1 = pEqp->getRR1(mode);
				if(rr1 & RR1_VALID)
				{
					unsigned rr2 = pEqp->getRR2(mode);
					int mode = (rr2 & 0xFF00) >> 8;
	//				qDebug("RR2 = %X, mode %d\n", rr2, mode);
					switch(mode)
					{
					case 1:	// Injection
						color.setRgb(COLOR_ON);
						break;
					case 2:	// Maintenance
						color.setRgb(COLOR_SERVICE);
						break;
					default:
						color.setRgb(COLOR_STATE_ERROR);
						break;
					}
				}
			}
			break;
		}
	}
	QBrush brush(color);
	painter.setBrush(brush);
}


