#ifndef LEGENDLABEL_H
#define	LEGENDLABEL_H

#include "VacCtlEwoUtilExport.h"

// Class holding parameters of legend label (to appear on main view, but
// can also be used on other views)

#include <qstring.h>

class VACCTLEWOUTIL_EXPORT LegendLabel
{
public:
	LegendLabel(int x, int y, const char *text);
	LegendLabel(int x, int y, int angle, const char *text);

	// Access
	inline QString &getText(void) { return text; }
	inline int getX(void) const { return x; }
	inline int getY(void) const { return y; }
	inline int getAngle(void) const { return angle; }

protected:
	// Label text
	QString	text;

	// X-coordinate of label
	int		x;

	// Y-coordinate of label
	int		y;

	// Rotation for label [degrees]
	int		angle;
};

#endif	// LEGENDLABEL_H
