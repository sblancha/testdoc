
// Declarations for fast log

// Minimum time interval for fast log [sec]
const int LHC_VAC_FAST_LOG_MIN_INTERVAL = 1;

// Name of archive used for fast log. Names of archives are built by adding number (1, 2, 3...) to this string.
const string LHC_VAC_FAST_LOG_ARCH = "FA_";

// Name of DP type for fast log session parameters
const string LHC_VAC_FAST_LOG_DDP_TYPE = "_VacDDP";

// Name of DP for fast log session parameters: this name plus number (1, 2, 3...)
const string LHC_VAC_FAST_LOG_DDP = "_VacDDP_";

// Name of DP type containing nmber of existing log sessions
const string LHC_VAC_FAST_LOG_DDP_CNT_DP_TYPE = "_VacFAF";

// Name of DP containing nmber of existing log sessions
const string LHC_VAC_FAST_LOG_DDP_CNT_DP = "_VacFAF";

// Name of DP type for ADP used for fast log
const string LHC_VAC_FAST_LOG_ADP_TYPE = "_VacADP";

// Template for all ADP names: this string + fast archive number + "_" + ADP number within fast archive
const string LHC_VAC_FAST_LOG_ADP_TEMPLATE = "_VacADP_";

// Enums for fast log session state
const int LHC_VAC_FAST_LOG_SESSION_NONE = 0;
const int LHC_VAC_FAST_LOG_SESSION_CONFIG = 1;
const int LHC_VAC_FAST_LOG_SESSION_READY = 2;
const int LHC_VAC_FAST_LOG_SESSION_STARTING = 3;
const int LHC_VAC_FAST_LOG_SESSION_START_FAILED = 4;
const int LHC_VAC_FAST_LOG_SESSION_RUNNING = 5;
const int LHC_VAC_FAST_LOG_SESSION_STOPPING = 6;
const int LHC_VAC_FAST_LOG_SESSION_STOP_FAILED = 7;
const int LHC_VAC_FAST_LOG_SESSION_FINISHED = 8;
const int LHC_VAC_FAST_LOG_SESSION_HISTORY = 9;

// Estimations of running fast log session health
const int LHC_VAC_FAST_LOG_SESSION_UNDEF = 0;
const int LHC_VAC_FAST_LOG_SESSION_OK = 1;
const int LHC_VAC_FAST_LOG_SESSION_WARNING = 2;
const int LHC_VAC_FAST_LOG_SESSION_BAD = 3;
const int LHC_VAC_FAST_LOG_SESSION_FAULT = 4;

//LhcVacFastLogArchiveDps:
/** Return DP names of PVSS archives which are created for fast log

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	
	@param archDpList: out, list of PVSS archive DPs
	@param exceptionInfo: out, details of any exceptions are returned here

	@return None.
	@author L.Kopylov
*/
void LhcVacFastLogArchiveDps( dyn_string &archDpList, dyn_string &exceptionInfo )
{
	dyn_string		dpList;
	string			archName;
	int				n, listLen, archNum, state;
	dyn_errClass	err;

	dynClear( archDpList );
	dpList = dpNames( "*", "_ValueArchive" );

	// Select only archives named according to naming conventions for fast log archives
	listLen = dynlen( dpList );
	for( n = 1 ; n <= listLen ; n++ )
	{
		if( isReduDp( dpList[n] ) ) continue;
		dpGet( dpList[n] + ".state:_original.._value", state );
		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogArchiveDps(): Failed to get state for <" + dpList[n] + ">: " + err, "" );
			throwError( err );
			return;
		}
		if( state != 1 ) continue;
		dpGet( dpList[n] + ".general.arName:_original.._value", archName );
		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogArchiveDps(): Failed to get name for <" + dpList[n] + ">: " + err, "" );
			throwError( err );
			return;
		}
		if( strpos( archName, LHC_VAC_FAST_LOG_ARCH ) != 0 ) continue;
		if( sscanf( archName, LHC_VAC_FAST_LOG_ARCH + "%d", archNum ) != 1 ) continue;
		archName = dpList[n];
		dynAppend( archDpList, archName );
	}
}
//LhcVacFastLogArchiveParam:
/** Return archive parameters of given PVSS archive used for fast log

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param archName: in, name of archive
	@param vaDp: out, name of value archive DP for this archive
	@param archNum: out, fast archive number
	@param archSize: out, number of DPEs for archive
	@param entriesNum: out, number of entries for every DPE
	@param exceptionInfo: out, details of any exceptions are returned here

	@return None.
	@author L.Kopylov
*/
void LhcVacFastLogArchiveParam( string archName, string &vaDp, int &archNum, int &archSize,
	int &entriesNum, dyn_string &exceptionInfo )
{
	dyn_string		dpList;
	int				listLen, n, state;
	string			archClass;
	dyn_errClass	err;

	// Find parameters of archive
	dpList = dpNames( "*", "_ValueArchive" );

	// Select only archives named according to naming conventions for fast log archives
	listLen = dynlen( dpList );
	vaDp = "";
	for( n = 1 ; n <= listLen ; n++ )
	{
		if( isReduDp( dpList[n] ) ) continue;
		dpGet( dpList[n] + ".state:_original.._value", state );
		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogArchiveParam(): Failed to get state for <" + dpList[n] + ">: " + err, "" );
			throwError( err );
			return;
		}
		if( state != 1 ) continue;
		dpGet( dpList[n] + ".general.arName:_original.._value", archClass );
		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogArchiveParam(): Failed to get name for <" + dpList[n] + ">: " + err, "" );
			throwError( err );
			return;
		}
		if( archClass != archName ) continue;
		if( sscanf( archName, LHC_VAC_FAST_LOG_ARCH + "%d", archNum ) != 1 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogArchiveParam(): Bad archive name format <" + archName + ">", "" );
			return;
		}
		vaDp = dpList[n];
		break;
	}
	if( vaDp == "" )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacFastLogArchiveParam(): Archive <" + archName + "> not found", "" );
		return;
	}

	// Get size of archive
	dpGet( vaDp + ".size.maxDpElGet:_original.._value", archSize,
		vaDp + ".size.maxValuesGet:_original.._value", entriesNum );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacFastLogArchiveParam(): Failed to get number of DPEs for <" + vaDp + ">", "" );
		throwError( err );
		return;
	}
}
//LhcVacFastLogGetDdps:
/** Return list of all existing DDPs for fast log sessions

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpList: out, list of all existing DDPs for fast log sessions
	@param exceptionInfo: out, details of any exceptions are returned here

	@return None.
	@author L.Kopylov
*/
void LhcVacFastLogGetDdps( dyn_string &dpList, dyn_string &exceptionInfo )
{
	dpList = dpNames( "*", LHC_VAC_FAST_LOG_DDP_TYPE );
}
//LhcVacFastLogNewSession:
/** Create DDP for new fast log session and return name of DP for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param archName: in, name of archive to which new session will be connected
	@copyFrom: in, name of another session DDP from where real DPs shall be copied.
	@param ddp: out, name of DDP for new session
	@param exceptionInfo: out, details of any exceptions are returned here

	@return None.
	@author L.Kopylov
*/
void LhcVacFastLogNewSession( string archName, string copyFrom, string &ddp, dyn_string &exceptionInfo )
{
	dyn_string		dpList;
	string			archClass, newDpName;
	int				n, listLen, num, maxNum;
	dyn_errClass	err;

	// Check if source DP for copy exists
	if( copyFrom != "" )
	{
		if( ! dpExists( copyFrom ) )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogNewSession(): DP <" + copyFrom + "> to copy from does not exist", "" );
			return;
		}
	}

	// Find archive DP
	dpList = dpNames( "*", "_ValueArchive" );

	listLen = dynlen( dpList );
	for( n = 1 ; n <= listLen ; n++ )
	{
		if( isReduDp( dpList[n] ) ) continue;
		dpGet( dpList[n] + ".general.arName:_original.._value", archClass );
		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogNewSession(): Failed to get name for <" + dpList[n] + ">: " + err, "" );
			throwError( err );
			return;
		}
		if( archClass == archName ) break;
	}
	if( archClass != archName )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacFastLogNewSession(): Nonexisten archive class <" + archName + ">", "" );
		return;
	}

	// Find number for new session
	dpList = dpNames( "*", LHC_VAC_FAST_LOG_DDP_TYPE );
	for( n = dynlen( dpList ) ; n > 0 ; n-- )
	{
		if( sscanf( dpSubStr( dpList[n], DPSUB_DP), LHC_VAC_FAST_LOG_DDP + "%d", num ) != 1 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogNewSession(): Bad DDP name <" + dpList[n] + ">", "" );
			return;
		}
		if( num > maxNum ) maxNum = num;
	}
	maxNum = maxNum + 1;

	// Create DDP, set initial parameters
	newDpName = LHC_VAC_FAST_LOG_DDP + maxNum;
	dpCreate( newDpName, LHC_VAC_FAST_LOG_DDP_TYPE );
	if( ! dpExists( newDpName ) )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacFastLogNewSession(): Failed to create DP <" + newDpName + ">", "" );
		return;
	}
	dpSetWait( newDpName + ".archName:_original.._value", archName,
		newDpName + ".status:_original.._value", LHC_VAC_FAST_LOG_SESSION_CONFIG );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacFastLogNewSession(): Failed to set initial parameters for <" + newDpname + ">", "" );
		throwError( err );
		return;
	}

	// Copy real DPs from another session
	if( copyFrom != "" )
	{
		dpGet( copyFrom + ".rdpName:_original.._value", dpList );
		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogNewSession(): Failed to get old session real DPs from <" +
				copyFrom + ">", "" );
			throwError( err );
			dpDelete( newDpName );
			return;
		}
		dpSetWait( newDpName + ".rdpName:_original.._value", dpList );
		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogNewSession(): Failed to copy real DPs from <" + copyFrom + ">", "" );
			throwError( err );
			dpDelete( newDpName );
			return;
		}
	}

	// Update counter of existing sessions
	dpList = dpNames( "*", LHC_VAC_FAST_LOG_DDP_TYPE );
	dpSetWait( LHC_VAC_FAST_LOG_DDP_CNT_DP + ".pointNum:_original.._value", dynlen(dpList ) );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacFastLogNewSession(): Failed to set new session counter " + num, "" );
		throwError( err );
		return;
	}
	ddp = newDpName;
}
//LhcVacFastLogDeleteSession:
/** Delete session contained in given DDP

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param ddpName: in, name DDP to be deleted
	@param exceptionInfo: out, details of any exceptions are returned here

	@return None.
	@author L.Kopylov
*/
void LhcVacFastLogDeleteSession( string ddpName, dyn_string &exceptionInfo )
{
	int				status, num;
	dyn_errClass	err;
	string			statusStr;
	dyn_string		dpList;

	// Check if DDP exists
	if( ddpName == "" )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacFastLogDeleteSession(): Empty DDP name", "" );
		return;
	}
	if( ! dpExists( ddpName ) )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacFastLogDeleteSession(): Session DDP <" + ddpName + "> does not exist", "" );
		return;
	}

	// Check for status: running session can not be deleted
	dpGet( ddpName + ".status:_original.._value", status );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacFastLogDeleteSession(): Failed to get session status for <" + ddpName + ">", "" );
		throwError( err );
		return;
	}
	switch( status )
	{
	case LHC_VAC_FAST_LOG_SESSION_STARTING:
	case LHC_VAC_FAST_LOG_SESSION_RUNNING:
	case LHC_VAC_FAST_LOG_SESSION_STOPPING:
		LhcVacFastLogGetSessionStateString( status, statusStr, exceptionInfo );
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacFastLogDeleteSession(): Can't delete " + statusStr + "session for <" +
				ddpName + ">", "" );
		return;
		break;
	}

	// Delete DP
	dpDelete( ddpName );

	// Update counter of existing sessions
	dpList = dpNames( "*", LHC_VAC_FAST_LOG_DDP_TYPE );
	dpSetWait( LHC_VAC_FAST_LOG_DDP_CNT_DP + ".pointNum:_original.._value", dynlen( dpList ) );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacFastLogDeleteSession(): Failed to set new session counter " + num, "" );
		throwError( err );
		return;
	}
}
//LhcVacFastLogCreateADP:
/** Create ADP for one PVSS archive used for fast log, assign ADPs to archive

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param archName: in, name of archive to which ADPs shall be created
	@param cnt: out, total number of ADPs
	@param exceptionInfo: out, details of any exceptions are returned here

	@return None.
	@author L.Kopylov
*/
void LhcVacFastLogCreateADP( string archName, int &cnt, dyn_string &exceptionInfo )
{
	dyn_string		dpList;
	int				listLen, n, archNum, archSize, nEntries, oldInd;
	string			archClass, vaDp, adpName, dpe;
	dyn_errClass	err;

	// Find parameters of archive
	LhcVacFastLogArchiveParam( archName, vaDp, archNum, archSize, nEntries, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	
	// Find all ADPs already created for this archive
	dpList = dpNames( LHC_VAC_FAST_LOG_ADP_TEMPLATE + archNum + "_*", LHC_VAC_FAST_LOG_ADP_TYPE );

	// Make pure DP names
	for( n = dynlen( dpList ) ; n > 0 ; n-- )
	{
		dpList[n] = dpSubStr( dpList[n], DPSUB_DP );
	}

	// Process create part
	for( n = 1 ; n <= archSize ; n++ )
	{
		if( LhcVacReportLongAct( n, archSize ) )
		{
			fwException_raise( exceptionInfo,
		 		"INFO", "LhcVacFastLogCreateADP: execution cancelled", "" );
		 	return;
		}
		adpName = LHC_VAC_FAST_LOG_ADP_TEMPLATE + archNum + "_" + n;
		if( ( oldInd = dynContains( dpList, adpName ) ) > 0 )
		{
			dynRemove( dpList, oldInd );
			continue;
		}

		// Create new ADP
		dpCreate( adpName, LHC_VAC_FAST_LOG_ADP_TYPE );
		if( ! dpExists( adpName ) )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogCreateADP(): Failed to get number of DPEs for <" + vaDp + ">", "" );
			return;
		}
		// Set archiving parameters for new ADP
		dpe = adpName + ".PR";
    	dpSetWait(dpe + ":_archive.._type", DPCONFIG_DB_ARCHIVEINFO, 
			dpe + ":_archive.._archive", true,
			dpe + ":_archive.1._class", vaDp,
			dpe + ":_archive.1._type", DPATTR_TIME_SMOOTH ); 		// Smooth type is fixed
    	err = getLastError( ); 
    	if( dynlen( err ) > 0 )
    	{ 
			throwError(err); 
			fwException_raise(exceptionInfo, "ERROR", "LhcVacFastLogCreateADP(): Could not create archiving config for <" +
				adpName + ">", "");
			return;
		} 
    	dpSetWait( dpe + ":_archive.1._std_type", 1,
			dpe + ":_archive.1._std_tol", 0,
			dpe + ":_archive.1._std_time", 600 );	// Default is 10 minutes
		err = getLastError(); 
    	if( dynlen( err ) > 0 )
    	{ 
			throwError(err); 
			fwException_raise(exceptionInfo, "ERROR", "LhcVacFastLogCreateADP(): Could not set smoothing parameters for <" +
				adpName + ">", "");
			return;
		} 
	}

	// Delete extra ADPs
	for( n = dynlen( dpList ) ; n > 0 ; n-- )
	{
		dpDelete( dpList[n] );
	}
	dpList = dpNames( LHC_VAC_FAST_LOG_ADP_TEMPLATE + archNum + "_*", LHC_VAC_FAST_LOG_ADP_TYPE );
	cnt = dynlen( dpList );
}
//LhcVacFastLogGetSessionStateString:
/** Converts status enum into string to be shown

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param status: in, session status
	@param statusStr: out, result string
	@param exceptionInfo: out, details of any exceptions are returned here

	@return None.
	@author SME
*/
void LhcVacFastLogGetSessionStateString( int status, string &statusStr,
	dyn_string &exceptionInfo )
{
	switch( status )
	{
	case LHC_VAC_FAST_LOG_SESSION_NONE:
		statusStr = "Free";
		break;
	case LHC_VAC_FAST_LOG_SESSION_CONFIG:
		statusStr = "Configuration";
		break;
	case LHC_VAC_FAST_LOG_SESSION_READY:
		statusStr = "Ready";
		break;
	case LHC_VAC_FAST_LOG_SESSION_STARTING:
		statusStr = "Starting";
		break;
	case LHC_VAC_FAST_LOG_SESSION_START_FAILED:
		statusStr = "Start failed";
		break;
	case LHC_VAC_FAST_LOG_SESSION_RUNNING:
		statusStr = "Running";
		break;
	case LHC_VAC_FAST_LOG_SESSION_STOPPING:
		statusStr = "Stopping";
		break;
	case LHC_VAC_FAST_LOG_SESSION_STOP_FAILED:
		statusStr = "Stop failed";
		break;	
	case LHC_VAC_FAST_LOG_SESSION_FINISHED:
		statusStr = "Finished";
		break;	
	case LHC_VAC_FAST_LOG_SESSION_HISTORY:
		statusStr = "History";
		break;
	default:
		fwException_raise( exceptionInfo,
		 	"ERROR", "LhcVacFastLogGetSessionStateString: unsupported status <" + status + ">", "" );
	}
}
