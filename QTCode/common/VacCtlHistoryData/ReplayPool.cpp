//	Implementation of ReplayPool class
/////////////////////////////////////////////////////////////////////////////////

#include "ReplayPool.h"

static ReplayPool	instance;
ReplayPool &ReplayPool::getInstance(void) { return instance; }


/*
**	FUNCTION
**		Add reference to DPE with given name to replay data pool
**
**	PARAMETERS
**		dpName	- DP name
**		dpeName	- DPE name, may be NULL if DP has no DPEs in it
**
**	RETURNS
**		true	- if DPE list is changed AND no other DPEs which are not ready,
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool ReplayPool::addDpe(const char *dpName, const char *dpeName)
{
	QByteArray dpe = makeDpeName(dpName, dpeName);
	ReplayPool &pool = ReplayPool::getInstance();
	return pool.addDpe(dpe);
}

/*
**	FUNCTION
**		Remove reference to DPE with given name from replay data pool
**
**	PARAMETERS
**		dpName	- DP name
**		dpeName	- DPE name, may be NULL if DP has no DPEs in it
**
**	RETURNS
**		true	- if DPE has been removed from pool,
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool ReplayPool::removeDpe(const char *dpName, const char *dpeName)
{
	QByteArray dpe = makeDpeName(dpName, dpeName);
	ReplayPool &pool = ReplayPool::getInstance();
	return pool.removeDpe(dpe);
}

/*
**	FUNCTION
**		Notify replay data pool that DPE with given name got value from
**		PVSS, so corresponding entry in pool can be marked as ready
**
**	PARAMETERS
**		dpName	- DP name
**		dpeName	- DPE name, may be NULL if DP has no DPEs in it
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ReplayPool::notifyDataArrived(const char *dpName, const char *dpeName)
{
	QByteArray dpe = makeDpeName(dpName, dpeName);
	ReplayPool &pool = ReplayPool::getInstance();
	pool.notifyDataArrived(dpe);
}

/*
**	FUNCTION
**		Build full DPE name from DP name and DPE name
**
**	PARAMETERS
**		dpName	- DP name
**		dpeName	- DPE name, may be NULL if DP has no DPEs in it
**
**	RETURNS
**		Resulting DPE name
**
**	CAUTIONS
**		None
*/
QByteArray ReplayPool::makeDpeName(const char *dpName, const char *dpeName)
{
	QByteArray name(dpName);
	if(dpeName)
	{
		name += ".";
		name += dpeName;
	}
	return name;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

ReplayPool::ReplayPool()
{
	mode = None;
	pDpes = new QHash<QByteArray, DpeValuesSet *>();
	pLastPlayedDpe = NULL;
	pLastPlayedValue = NULL;
}

ReplayPool::ReplayPool(const ReplayPool &source) : QObject()
{
	mode = None;
	pDpes = new QHash<QByteArray, DpeValuesSet *>(*(source.pDpes));
	pLastPlayedDpe = NULL;
	pLastPlayedValue = NULL;
}

ReplayPool::~ReplayPool()
{
	foreach(DpeValuesSet *pDpe, *pDpes)
	{
		delete pDpe;
	}
	delete pDpes;
}

ReplayPool & ReplayPool::operator = (const ReplayPool &other)
{
	mode = None;
	*pDpes = *(other.pDpes);
	pLastPlayedDpe = NULL;
	pLastPlayedValue = NULL;
	return *this;
}

/*
**	FUNCTION
**		Add DPE with given name to list of DPEs (if it is not there yet),
**		or increment reference counter for existing DPE (if DPE is already in list)
**
**	PARAMETERS
**		dpeName	- Name of DPE to be added
**
**	RETURNS
**		true	- if DPE list is changed AND no other DPEs which are not ready,
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool ReplayPool::addDpe(const char *dpeName)
{
	mutex.lock();
	bool result = false;
	DpeValuesSet	*pDpeData = pDpes->value(dpeName);
	if(!pDpeData)	// DPE is not known yet
	{
		// Check if there are other DPEs which are not ready
		foreach(pDpeData, *pDpes)
		{
			if(!pDpeData->isReady())
			{
				result = true;
				break;
			}
		}
		result = ! result;
		pDpeData = new DpeValuesSet(dpeName);
		pDpes->insert(dpeName, pDpeData);
	}
	pDpeData->setRefCount(pDpeData->getRefCount() + 1);
//qDebug("ReplayPool::addDpe(%s): count %d result %d\n", dpeName, pDpes->count(), (int)result);
	mutex.unlock();
	if(result)
	{
		emit dpeListChanged();
	}
	return result;
}

/*
**	FUNCTION
**		Decrement reference count for DPE with given name. If reference count
**		became 0 - remove DPE from list.
**
**	PARAMETERS
**		dpeName	- Name of DPE to be removed
**
**	RETURNS
**		true	- if DPE has been removed from pool,
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool ReplayPool::removeDpe(const char *dpeName)
{
	mutex.lock();
	bool result = false;
	DpeValuesSet	*pDpeData = pDpes->value(dpeName);
	if(pDpeData)	// DPE is already known
	{
		if(pDpeData->getRefCount() == 1)
		{
			pDpes->remove(dpeName);
			result = true;
		}
		else
		{
			pDpeData->setRefCount(pDpeData->getRefCount() - 1);
		}
	}
	mutex.unlock();
	return result;
}

/*
**	FUNCTION
**		Notify replay data pool that DPE with given name got value from
**		PVSS, so corresponding entry in pool can be marked as ready
**
**	PARAMETERS
**		dpeName	- DPE name, may be NULL if DP has no DPEs in it
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ReplayPool::notifyDataArrived(const char *dpeName)
{
	DpeValuesSet *pDpeData = pDpes->value(dpeName);
//	qDebug("ReplayPool::notifyDataArrived(%s): pointer %X", dpeName, (unsigned)pDpeData);
	if(pDpeData)
	{
		pDpeData->setReady(true);
	}
}

/*
**	FUNCTION
**		Reset recent list of DPEs for new mode set in PVSS
**		(either switch from PLAY to DRAG, or reverse).
**		Mark all DPEs as not ready such that PVSS will process
**		all of them for new mode.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Number of DPEs in pool
**
**	CAUTIONS
**		None
*/
int ReplayPool::reset(void)
{
	mutex.lock();
	foreach(DpeValuesSet *pDpeData, *pDpes)
	{
		pDpeData->clear();
	}

	mode = None;
	pLastPlayedDpe = NULL;
	pLastPlayedValue = NULL;

	int result = pDpes->count();
	mutex.unlock();
	return result;
}


/*
**	FUNCTION
**		Return next DPE name to be processed by PVSS - i.e. next DPE which
**		is not ready yet
**
**	PARAMETERS
**		dpeName	- Variable where DPE name will be written
**
**	RETURNS
**		1	= next DPE found;
**		0	= no more DPEs to process
**
**	CAUTIONS
**		None
*/
int ReplayPool::nextDpe(QByteArray &dpeName)
{
	mutex.lock();
	dpeName = "";
	int result = 0;
	QHash<QByteArray, DpeValuesSet *>::iterator iter;
	for(iter = pDpes->begin() ; iter != pDpes->end() ; ++iter)
	{
		DpeValuesSet *pDpeData = iter.value();
		if(!pDpeData->isReady())
		{
			dpeName = iter.key();
			result = 1;
			break;
		}
	}
	mutex.unlock();
	return result;
}

/*
**	FUNCTION
**		Return list of next DPE names to be processed by PVSS - i.e. next DPE which
**		is not ready yet
**
**	PARAMETERS
**		dpeNames	- Variable where DPE name will be written
**
**	RETURNS
**		Number of DPEs to process
**
**	CAUTIONS
**		None
*/
int ReplayPool::nextDpes(QList<QByteArray> &dpeNames)
{
	mutex.lock();
	dpeNames.clear();
	int result = 0;
	QHash<QByteArray, DpeValuesSet *>::iterator iter;
	for(iter = pDpes->begin() ; iter != pDpes->end() ; ++iter)
	{
		DpeValuesSet *pDpeData = iter.value();
		if(!pDpeData->isReady())
		{
			QByteArray dpeName(iter.key());
			dpeNames.append(dpeName);
			result++;
		}
	}
	mutex.unlock();
	return result;
}

/*
**	FUNCTION
**		New values for PLAY PVSS mode arrived from PVSS for DP.
**		Store the values in the list for future processing
**
**	PARAMETERS
**		dpeName			- DPE name
**		newTime			- Timestamp for new values
**		newValue		- New values
**		useFirstValue	- flag indicating if current index for DPE shall be set to 0
**
**	RETURNS
**		1	= DPE found and data successfully stored;
**		0	= DPE is not found
**		-1	= error when processing data
**
**	CAUTIONS
**		None
*/
int ReplayPool::setValue(const char *dpeName, const QDateTime &newTime,
	const QVariant &newValue, bool useFirstValue)
{
	mutex.lock();
	int result = 0;
	DpeValuesSet *pDpeData = pDpes->value(dpeName);
	if(pDpeData)
	{
		result = pDpeData->addValue(newTime, newValue, useFirstValue);
		if(result > 0)
		{
			if(mode != None)
			{
				pDpeData->setIndex(playTime, (mode == Forward ? true : false));
			}
		}
	}
	mutex.unlock();
	return result;
}

/*
**	FUNCTION
**		Build arrays in all DPEs for further fats search.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ReplayPool::process(void)
{
	mutex.lock();
	foreach(DpeValuesSet *pDpeData, *pDpes)
	{
		pDpeData->sort();
	}
	mutex.unlock();
}


/*
**	FUNCTION
**		Find data for next event (event driven mode)
**
**	PARAMETERS
**		dpeName		- Variable where name of DPE for which values are returned is written
**		ts			- Variable where timestamp for value is written
**		value		- Variable where DPE value is written
**
**	RETURNS
**		0	- in case of success;
**		1	- DPE which is not ready was found
**
**	CAUTIONS
**		None
*/
int ReplayPool::getNextEventData(QByteArray &dpeName, QDateTime &ts, QVariant &value)
{
	mutex.lock();

	// If switch from play backward to play forward - move all inidices by -1
	// in order to play just played values once again and avoid big jump in time
	// This solves 'jump back in time' problem, but introduces another
	// problem: display will be 'frozen' until all previously played events
	// will be replayed once more. Decision is left to user.
	if(mode == Backward)
	{
		foreach(DpeValuesSet *pDpeData, *pDpes)
		{
			if(!pDpeData->getRefCount() == 0)
			{
				continue;
			}
			if(!pDpeData->isReady())
			{
				mutex.unlock();
				return 1;
			}
			if(pDpeData->getIndex() >= 0)
			{
				pDpeData->setIndex(pDpeData->getIndex() - 1);
			}
		}
	}
	// If this method was called - we are playing in forward direction
	mode = Forward;

	// Find next DPE to use
	DpeValuesSet	*pUseDpe = NULL;
	QDateTime minTs = QDateTime::currentDateTime();
	minTs = minTs.addDays(30);	// Something in the future
	int totalRefCount = 0;
	foreach(DpeValuesSet *pDpeData, *pDpes)
	{
		if(!pDpeData->getRefCount())
		{
			continue;
		}
		if(!pDpeData->isReady())
		{
			mutex.unlock();
			return 1;
		}
		totalRefCount++;
		const DpeValue *pValue = pDpeData->getNext();
		if(pValue)
		{
			if(pValue->getTs() < minTs)
			{	
				minTs = pValue->getTs();
				pUseDpe = pDpeData;
			}
		}
	}
	if(pUseDpe)
	{
		pUseDpe->setIndex(pUseDpe->getIndex() + 1);
		const DpeValue *pValue = pUseDpe->getCurrent();
		ts = playTime = pValue->getTs();
		value = pValue->getValue();
		dpeName = pUseDpe->getName();
		setLastPlayed(pUseDpe, pValue);
	}
	else
	{
		checkLastPlayed(totalRefCount, dpeName, ts, value);
	}
	mutex.unlock();
	return 0;
}

/*
**	FUNCTION
**		Find data for previous event (event driven mode)
**
**	PARAMETERS
**		dpeName		- Variable where name of DPE for which values are returned is written
**		ts			- Variable where timestamp for value is written
**		value		- Variable where DPE value is written
**
**	RETURNS
**		0	- in case of success;
**		1	- DPE which is not ready was found
**
**	CAUTIONS
**		None
*/
int ReplayPool::getPreviousEventData(QByteArray &dpeName, QDateTime &ts, QVariant &value)
{
	mutex.lock();

	// If switch from play forward to play backward - move all inidices by +1
	// in order to play just played values once again and avoid big jump in time
	// This solves 'jump forward in time' problem, but introduces another
	// problem: display will be 'frozen' until all previously played events
	// will be replayed once more. Decision is left to user.
	if(mode == Forward)
	{
		foreach(DpeValuesSet *pDpeData, *pDpes)
		{
			if(!pDpeData->getRefCount())
			{
				continue;
			}
			if(!pDpeData->isReady())
			{
				mutex.unlock();
				return 1;
			}
			if((pDpeData->getIndex() >= 0) && pDpeData->getNext())
			{
				pDpeData->setIndex(pDpeData->getIndex() + 1);
			}
		}
	}

	// If this method was called - we are playing in backward direction
	mode = Backward;

		// Find previous DPE to use
	DpeValuesSet	*pUseDpe = NULL;
	QDateTime maxTs = QDateTime::currentDateTime();
	maxTs = maxTs.addYears(-20);	// Far in the past
	int totalRefCount = 0;
	foreach(DpeValuesSet *pDpeData, *pDpes)
	{
		if(!pDpeData->getRefCount())
		{
			continue;
		}
		if(!pDpeData->isReady())
		{
			mutex.unlock();
			return 1;
		}
		totalRefCount++;
		const DpeValue *pValue = pDpeData->getPrevious();
		//qDebug("ReplayPool::getPreviousEventData(): %s -> %X\n", pDpeData->getName().constData(), (unsigned)pValue);
		if(pValue)
		{
			/*
			qDebug("ReplayPool::getPreviousEventData(): %s TS %s maxTs %s\n", pDpeData->getName().constData(), pValue->getTs().toString(Qt::ISODate).toAscii().constData(),
				maxTs.toString(Qt::ISODate).toAscii().constData());
			*/
			if(pValue->getTs() > maxTs)
			{	
				maxTs = pValue->getTs();
				pUseDpe = pDpeData;
			}
		}
	}
	if(pUseDpe)
	{
		//qDebug("ReplayPool::getPreviousEventData(): FOUND %s -> index %d\n", pUseDpe->getName().constData(), pUseDpe->getIndex());
		pUseDpe->setIndex(pUseDpe->getIndex() - 1);
		const DpeValue *pValue = pUseDpe->getCurrent();
		ts = playTime = pValue->getTs();
		value = pValue->getValue();
		dpeName = pUseDpe->getName();
		setLastPlayed(pUseDpe, pValue);
		// set currentIndex to initial value -1, this DPE has no values al all
		if(pUseDpe->getIndex() == 0)
		{
			pUseDpe->setIndex(-1);
		}
	}
	else
	{
		checkLastPlayed(totalRefCount, dpeName, ts, value);
	}
	mutex.unlock();
	return 0;
}

/*
**	FUNCTION
**		Next or previous event to play were not found. Check if we can use
**		last played value instead, or we must return empty DPE name (== play
**		procedure is finished). Last played value can be used to keep player
**		engine in 'Play' mode while data for new equipment to show are being
**		prepared.
**
**	PARAMETERS
**		totalRefCount	- Counter of DPEs with non-zero refCount
**		dpeName			- Variable where last played DPE name is written
**		ts				- Variable where timestamp of last played value is written
**		value			- Variable where last played value is written
**
**	RETURNS
**		None
**
*/
void ReplayPool::checkLastPlayed(int totalRefCount, QByteArray &dpeName, QDateTime &ts, QVariant &value)
{
	if(totalRefCount)
	{
		dpeName = "";
	}
	else if(pLastPlayedDpe)	// Return last played value (if any)
	{
		ts = playTime = pLastPlayedValue->getTs();
		value = pLastPlayedValue->getValue();
		dpeName = pLastPlayedDpe->getName();
	}
	else
	{
		dpeName = "";
	}
}

/*
**	FUNCTION
**		Set current ts and internal dpe indexes (time driven mode)
**
**	PARAMETERS
**		ts			- Timestamp for which the indexes will be set
**		forward		- Direction where the indexes are looking for
**
**	RETURNS
**		0	- in case of success;
**		1	- in new DPE has been added to pool - and that DPE has no data yet
**
** CAUTIONS
**		None
*/
int ReplayPool::setPlayTime(const QDateTime &ts, bool forward)
{
	mutex.lock();
	mode = forward ? Forward : Backward;
	foreach(DpeValuesSet *pDpeData, *pDpes)
	{
		if(!pDpeData->getRefCount())
		{
			continue;
		}
		if(!pDpeData->isReady())
		{
			mutex.unlock();
			return 1;
		}
		pDpeData->setIndex(ts, forward);
	}
	mutex.unlock();
	return 0;
}

/*
**	FUNCTION
**		Get dpe data according to index previously set with setPlayTime (time driven mode)
**
**	PARAMETERS
**		dpeName		- Variable where DPE name will be written
**		value		- Variable where value for DPE will be written
**
**	RETURNS
**		0	- in case of success;
**		1	- in new DPE has been added to pool - and that DPE has no data yet
**
** CAUTIONS
**		None
*/
int ReplayPool::getPlayData(QByteArray &dpeName, QVariant &value)
{
	mutex.lock();
	dpeName = "";
	QHash<QByteArray, DpeValuesSet *>::iterator iter;
	for(iter = pDpes->begin() ; iter != pDpes->end() ; ++iter)
	{
		DpeValuesSet *pDpeData = iter.value();
		if(!pDpeData->getRefCount())
		{
			continue;
		}
		if(!pDpeData->isReady())
		{
			mutex.unlock();
			return 1;
		}
		if(!pDpeData->isNotReported())
		{
			continue;
		}
		dpeName = iter.key();
		pDpeData->setNotReported(false);
		const DpeValue *pValue = pDpeData->getCurrent();
		if(pValue)
		{
			value = pValue->getValue();
		}
		else
		{
			value = QVariant(0);
		}
		break;
	}
	mutex.unlock();
	return 0;
}

