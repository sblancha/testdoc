//	Implementation of ProfileLhcVpgConnector class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileLhcVpgConnector.h"

#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

ProfileLhcVpgConnector::ProfileLhcVpgConnector(Eqp *pEqp, DataEnum::DataMode mode) :
	InterfaceEqp()
{
	this->pEqp = pEqp;
	this->mode = mode;
	pEqp->connect(this, mode);
}

ProfileLhcVpgConnector::~ProfileLhcVpgConnector()
{
	pEqp->disconnect(this, mode);
}

