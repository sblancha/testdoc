//	Implementation of PlotImage class
/////////////////////////////////////////////////////////////////////////////////

#include "PlotImage.h"
#include "HistoryImage.h"

#include <qpainter.h>
#include <QMouseEvent>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

PlotImage::PlotImage(QWidget *parent) :
	QWidget(parent), zoomRect()
{
	setAutoFillBackground(true);
	pointerLocation = 0;
	motionType = MouseMotionNone;
}

PlotImage::~PlotImage()
{
}

void PlotImage::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);

	emit resized();
}

void PlotImage::mousePressEvent(QMouseEvent *pEvent)
{
	motionType = MouseMotionNone;
	switch(pEvent->button() & Qt::MouseButtonMask)
	{
	case Qt::LeftButton:
		if(pEvent->modifiers() & Qt::ShiftModifier)	// Start rectangle selection
		{
			motionType = MouseMotionArea;
			startZoomX = pEvent->x();
			startZoomY = pEvent->y();
			zoomRect.setCoords(pEvent->x(), pEvent->y(), pEvent->x(), pEvent->y());
		}
		else if(pEvent->modifiers() & Qt::ControlModifier)	// Select closest device
		{
		}
		else	// Just move pointer
		{
			motionType = MouseMotionPointer;
			movePointerTo(pEvent->x());
		}
		break;
	default:
		break;
	}
}

void PlotImage::mouseReleaseEvent(QMouseEvent *pEvent)
{
	switch(motionType)
	{
	case MouseMotionPointer:
		movePointerTo(pEvent->x());
		break;
	case MouseMotionArea:
		update(zoomRect);
		if(zoomRect.width() || zoomRect.height())
		{
			emit zoomRectFinished(zoomRect);
		}
		break;
	default:
		break;
	}
	motionType = MouseMotionNone;
}


void PlotImage::movePointerTo(int newX)
{
	if((0 > newX) || (newX >= width()))
	{
		return;
	}
	update(pointerLocation - 1, 0, 3, height());
	pointerLocation = newX;
	update(pointerLocation - 1, 0, 3, height());
	emit pointerMoved(pointerLocation);
}


void PlotImage::mouseMoveEvent(QMouseEvent *pEvent)
{
	switch(motionType)
	{
	case MouseMotionPointer:
		movePointerTo(pEvent->x());
		break;
	case MouseMotionArea:
		moveZoomRectangle(pEvent->x(), pEvent->y());
		break;
	default:
		break;
	}
}

void PlotImage::paintEvent(QPaintEvent *pEvent)
{
	QWidget::paintEvent(pEvent);

	QRect paintRect = pEvent->rect();

	QPainter painter(this);

	QPen pen(Qt::magenta);
	painter.setPen(pen);

	// Draw zoom rectangle if we are in zoom mode
	if(motionType == MouseMotionArea)
	{
		if(paintRect.intersects(zoomRect))
		{
			painter.drawRect(zoomRect);
		}
	}

	// Draw vertical pointer - always
	if((paintRect.left() <= pointerLocation) && (pointerLocation <= paintRect.right()))
	{
		painter.drawLine(pointerLocation, 0, pointerLocation, height());
	}
}

/*
**	FUNCTION
**		Move one corner of zoom rectangle to current mouse point,
**		other corner of zoom rectangle ramains at point where zooming
**		operation was started
**
**	PARAMETERS
**		x	- X-coordinate of mouse pointer
**		y	- Y-coordinate of mouse pointer
**
**	RETURNS
**		true	- If mouse pointer is within 
**
**	CAUTIONS
**		None
**
*/
void PlotImage::moveZoomRectangle(int x, int y)
{
	if((zoomRect.width() > 0) && (zoomRect.height() > 0))
	{
		update(zoomRect);
	}
	int leftX, rightX, topY, bottomY;
	if(x < startZoomX)
	{
		leftX = x;
		rightX = startZoomX;
	}
	else
	{
		leftX = startZoomX;
		rightX = x;
	}
	if(y < startZoomY)
	{
		topY = y;
		bottomY = startZoomY;
	}
	else
	{
		topY = startZoomY;
		bottomY = y;
	}
	zoomRect.setCoords(leftX, topY, rightX, bottomY);
	if((zoomRect.width() > 0) && (zoomRect.height() > 0))
	{
		update();
	}
}

