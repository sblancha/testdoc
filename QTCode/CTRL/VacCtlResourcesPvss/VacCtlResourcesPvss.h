#ifndef _VACCTLRESOURCESPVSS_H_
#define _VACCTLRESOURCESPVSS_H_


#include "VacCtlPvssUtil.h"

# ifdef _WIN32
#   define  PVSS_EXPORT  __declspec(dllexport)
# else
#   define  PVSS_EXPORT
# endif


#include <QMutex>

#include "ResourcePool.h"

class AnyTypeVar;

class VacCtlResourcesPvss : public VacCtlPvssUtil
{
public:
    VacCtlResourcesPvss(BaseExternHdl *nextHdl, PVSSulong funcCount, FunctionListRec fnList[])
      : VacCtlPvssUtil(nextHdl, funcCount, fnList) {}

    virtual const Variable *execute(ExecuteParamRec &param);

protected:
	AnyTypeVar executeParseFile(ExecuteParamRec &param);
	AnyTypeVar executeGetValue(ExecuteParamRec &param);
	AnyTypeVar executeGetSpecificValue(ExecuteParamRec &param);

	int getResourceValue(const char *resourceName, int dataType, AnyTypeVar &result);
};

#endif	// _VACCTLRESOURCESPVSS_H_
