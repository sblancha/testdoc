#
# Project file for qmake utility to build ExcelWriterEwo EWO
#
#	01.04.2009	L.Kopylov
#		Initial version
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = ExcelWriterEwo

INCLUDEPATH += ../../common/VacCtlUtil \
	../../common/Platform \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp \
	../VacCtlEwoUtil \
	../VacCtlEwoUtil/ExcelWriter \
	../VacCtlEwoUtil/HistoryProcessor

HEADERS =	ExcelWriterEwo.h

SOURCES = ExcelWriterEwo.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlEqpData \
	-lVacCtlHistoryData \
	-lVacCtlUtil

