// CTRL Library
// created from lhcVacDeviceFileParser.ctl for QT
// @SAA 20may10

// Constants and functions used to parse file exported from device DB
// and to interprete parsing results
// Parsing itself is realized in DLL, this file just contains information
// for interpreting parsing results.

//#uses "VacImport"		//BEFORE_QT - Load DLL or so file
#uses "VacCtlImportPvss" //QT - Load DLL or so file

// Completion codes for DLL functions

const int OK = 0;	// Function processed successfully
const int FAILURE = -1;	// Function failed

// Parse file produced by database export
//
// int VacImportParseFile(string fileName, string machineName, bool isMobile, dyn_string &exceptionInfo);
//
// ARGUMENTS:
//		fileName			- [in], name of file to be parsed
//		machineName		- [in], Name of machine for which file is being parsed (LHC,SPS...)
//		isMobile			- [in], flag indicating if input file contains address information for
//													mobile equipment [true], or
//													if input file contains data for equipment import to PVSS [false]
//		exceptionInfo	-	[out], all errors found during file parsing are returned here.
//
//	RETURN
//		OK			- Parsing was successful (though still there could be errors/inconsistencies, they
//						are reported in exceptionInfo)
//		FAILURE	- Bad arguments, or Parsing failed - bad format of input file
//

// Return total number of device read from file
//
//	int VacImportGetDeviceCount(void);
//
//	ARGUMENTS
//		None
//
//	RETURN
//		Number of devices read from file


// Return all parameters of device with given number
//
// int VacImportGetDeviceDataByIndex(  int index, dyn_anytype &dp, dyn_dyn_anytype &dpe, dyn_string &exceptionInfo );
//
//	ARGUMENTS
//		index					- [in], index of device for which data are requested, must be in range
//											[1...value returned by VacImportGetDeviceCount()]
//		dp						- [out], device head parameters are returned here,
//													see constant definitions below
//		dpe						- [out], all DPE parameters are returned here,
//													see constant definitions below
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		OK			- Data for device have been found;
//		FAILURE	- Bad arguments, or Data for device were not found (bad index argument)
//

//=====================================================================================
// ======================== Indexes of values returned in 'dp; argument: ==============
//=====================================================================================

// Name of DP [string]
const int VAC_DP_NAME = 1;

// Name of DP type [string]
const int VAC_DP_DP_TYPE = 2;

// Mobile type [int]
const int VAC_DP_MOBILE_TYPE = 3;

// Line number in input file where device description found [int]
const int VAC_DP_LINE_NO = 4;

// Flag indicating that DpFunc shall be added for DP on  dpe=STATE [bool]
const int VAC_DP_FUNC_SET = 5;

//=====================================================================================
// ======================== Indexes of values returned in 'dpe[n]; argument: ==========
//=====================================================================================
// Name of DPE [string]
const int VAC_DPE_NAME = 1;

// Address type [string]
const int VAC_DPE_ADDRESS_TYPE = 2;

// Driver number [int]
const int VAC_DPE_ADDRESS_DRV_NUM = 3;

// Address reference [string]
const int VAC_DPE_ADDRESS_REFERENCE = 4;

// Data direction (in=1/out=2) [int]
const int VAC_DPE_ADDRESS_DIRECTION = 5;

// Data transfer type [string]
const int VAC_DPE_ADDRESS_TRANSTYPE = 6;

// Flag for old/new value comparison (1=compare,0=not compare) [int]
const int VAC_DPE_ADDRESS_OLD_NEW = 7;

// Address connection name (OPC server or S7 connection) [string]
const int VAC_DPE_ADDRESS_CONNECT = 8;

// Address group name (OPC group or S7 poll group) [string]
const int VAC_DPE_ADDRESS_GROUP = 9;

// Index of PVSS archive  returned by VacImportGetAllArchives() <archives[VAC_DPE_ARCHIVE_IDX][]> [int]
const int VAC_DPE_ARCHIVE_IDX = 10;

// Value format for DPE [string]
const int VAC_DPE_VALUE_FORMAT = 11;

// Line number in input file where DPE information was found [int]
const int VAC_DPE_LINE_NO = 12;

// Flag indicating that _address config for DPE shall be changed [bool]
// After file parsing the value is <false>, it can be set to <true> by
// calling VacImportSetDpeAddressChange( ), see below
const int VAC_DPE_ADDRESS_CHANGE = 13;

// Flag indicating that _archive config for DPE shall be changed [int]
// After file parsing the value is 0, it can be set to another integer value by
// calling VacImportSetDpeArchiveChange( ), see below
const int VAC_DPE_ARCHIVE_CHANGE = 14;

// Flag indicating that value format for DPE shall be changed [bool]
// After file parsing the value is <false>, it can be set to <true> by
// calling VacImportSetDpeValueFormatChange( ), see below
const int VAC_DPE_VALUE_FORMAT_CHANGE = 15;

// Flag indicating that all of: address, archive and value format [bool]
// shall be cleared for DPE. After file parsing no DPE with this
// flag set to true exist, such DPE(s) can be ADDED to device by
// calling VacImportAddDpe(), see below
const int VAC_DPE_CLEAR_ALL = 16;

// Name of old PVSS archive for DPE - required if archive config shall
// be deleted for DPE in order to know on which archive fileswitch
// shall be performed
const int VAC_DPE_OLD_ARCHIVE_NAME = 17;

//===================== Extra fields - added 11.10.2012 L.Kopylov ==================

// Smooth type for DPE archiving
const int VAC_DPE_ARCH_SMOOTH_TYPE = 18;

// Time interval for archiving smoothing
const int VAC_DPE_ARCH_TIME_INTERVAL = 19;

// Dead band for archiving smoothing
const int VAC_DPE_ARCH_DEAD_BAND = 20;

//===================== Extra fields - added 06.03.2015 L.Kopylov ==================

// Name to publish DPE in CMW
const int VAC_DPE_EXPORT_TO_CMW = 21;

// Name to publish DPE in DIP
const int VAC_DPE_EXPORT_TO_DIP = 22;

// Flag indicating that DIP publishing config for DPE shall be changed [bool]
// After file parsing the value is <false>, it can be set to <true> by
// calling VacImportSetDipConfigChange( ), see below
const int VAC_DPE_DIP_CONFIG_CHANGE = 23;



// Return all parameters of device with given DP name and DP type name
// The function is used to find address information for mobile equipment
// to be activated when name of source DP and name of target DP type
// are known.
//
// int VacImportGetDeviceDataByName( string dpName, string dpType, dyn_anytype &dp, dyn_dynanytype &dpe, dyn_string 	&exceptionInfo );
//
//	ARGUMENTS
//		dpName				- [in], Name of DP for which data are requested
//		dpType				- [in], Name of DP type for which data are requested
//		dp						- [out], device head parameters are returned here,
//													see constant definitions above
//		dpe						- [out], all DPE parameters are returned here,
//													see constant definitions above
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		OK			- Data for device have been found;
//		FAILURE	- Bad arguments, or Data for device were not found
//

// Returns all parameters for all PVSS archives read from input file
//
//	int VacImportGetAllArchives( dyn_dyn_anytype &archives, dyn_string &exceptionInfo );
//
//	ARGUMENTS
//		archives		- [out], Parameters for all archives read from input file,
//														see constants below
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		number of Archives found in import file
//		FAILURE		- Bad arguments, error

//=====================================================================================
// ======================== Indexes of values returned in 'arhives[n]; argument: ==========
//=====================================================================================
// Name of archive [string]
const int VAC_ARCHIVE_NAME = 1;

// Smooth type [int]
const int VAC_ARCHIVE_SMOOTH_TYPE = 2;

// Time interval for time smoothing [int]
const int VAC_ARCHIVE_TIME_INTERVAL = 3;

// Dead band for value smoothing [int]
const int VAC_ARCHIVE_DEAD_BAND = 4;

// Number of DPEs referring to this archive [int]
const int VAC_ARCHIVE_DPE_COUNT = 5;

// Line number in input file where archive definition was found [int]
const int VAC_ARCHIVE_LINE_NO = 6;

// Name of archive DP [string]. The name is not read from file, rather
// it can be set when processing data read from file
const int VAC_ARCHIVE_DP = 7;

// Required total depth of archive in days [int]

const int VAC_ARCHIVE_TOTAL_DEPTH = 8;


// Returns all parameters for all S7 poll groups read from input file
//
//	int VacImportGetAllPollGroups(  dyn_dyn_anytype &groups, dyn_string &exceptionInfo );
//
//	ARGUMENTS
//		groups			- [out], Parameters for all groups read from input file,
//														see constants below
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		OK			- Function executed successfully
//		FAILURE	- Bad arguments

//=====================================================================================
// ======================== Indexes of values returned in 'groups[n]; argument: ==========
//=====================================================================================
// Name of poll group [string]
const int VAC_POLL_GROUP_NAME = 1;

// Polling period for group [int]
const int VAC_POLL_GROUP_PERIOD = 2;

// Line number in input file where poll group was defined [int]
const int VAC_POLL_GROUP_LINE_NO = 3;



// Returns all parameters for all S7 connections read from input file
//
//	int VacImportGetAllS7Connection(  dyn_dyn_anytype &connections, dyn_string &exceptionInfo );
//
//	ARGUMENTS
//		connections		- [out], Parameters for all connections read from input file,
//														see constants below
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		OK			- Function executed successfully
//		FAILURE	- Bad arguments

//=====================================================================================
// ======================== Indexes of values returned in 'connections[n]; argument: ==========
//=====================================================================================
// Name of connection [string]
const int VAC_CONNECTION_NAME = 1;

// IP address of PLC for connection [string]
const int VAC_CONNECTION_IP = 2;

// Type of connection [int]
const int VAC_CONNECTION_TYPE = 3;

// Rack number [int]
const int VAC_CONNECTION_RACK = 4;

// Slot number of CPU in PLC [int]
const int VAC_CONNECTION_SLOT = 5;

// Line number in input file where connection is defined
const int VAC_CONNECTION_LINE_NO = 6;

// Flag indicating if connection has 'valid' IP address [bool]
const int VAC_CONNECTION_IP_VALID = 7;

// Returns all parameters for all periphery drivers read from input file
//
//	int VacImportGetDrivers(dyn_dyn_anytype &driverInfo, dyn_string &exceptionInfo);
//
//	ARGUMENTS
//		driverInfo		- [out], Parameters for all periphery drivers read from input file,
//														see constants below
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		OK			- Function executed successfully
//		FAILURE	- Bad arguments

//=====================================================================================
//================== Indexes of values returned in 'driverInfo[n]; argument: ==========
//=====================================================================================
// Driver number [int]
const int VAC_DRIVER_NUMBER = 1;

// Driver (address) type [string]
const int VAC_DRIVER_TYPE = 2;

// Number of DPEs using this driver [int]
const int VAC_DRIVER_DPE_USE = 3;



// Returns timestamp read from input file
//
//	int VacImportGetTimeStamp(string &timeStamp);
//
//	ARGUMENTS
//		timeStamp		- [out], Timestamp read from input file
//
//	RETURN
//		OK			- Function executed successfully
//		FAILURE	- Bad arguments



// Sets for given DPE flag indicating that _address config shall be changed
//
//	int VacImportSetDpeAddressChange( string dpName, string dpeName, dyn_string &exceptionInfo );
//
//	ARGUMENTS
//		dpName				- [in], DP name for which address shall be changed
//		dpeName				- [in], DPE name in DP for which address shall be changed
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		OK			- Function executed successfully
//		FAILURE	- Bad arguments, or unknown combination of DP+DPE



// Sets for given DPE flag indicating that _archive config shall be changed
//
//	int VacImportSetDpeArchiveChange(  string dpName, string dpeName, int changeType, string oldArchiveName, dyn_string &exceptionInfo );
//
//	ARGUMENTS
//		dpName				- [in], DP name for which archive shall be changed
//		dpeName				- [in], DPE name in DP for which archive shall be changed
//		changeType			- [in], type (enumarated) of change for DPE archive config
//  oldArchiveName - [in], name of old archive used by this DPE
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		OK			- Function executed successfully
//		FAILURE	- Bad arguments, or unknown combination of DP+DPE



// Sets for given DPE flag indicating that value format shall be changed
//
//	int  VacImportSetDpeValueFormatChange( string dpName, string dpeName, dyn_string &exceptionInfo );
//
//	ARGUMENTS
//		dpName				- [in], DP name for which value format shall be changed
//		dpeName				- [in], DPE name in DP for which value format shall be changed
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		OK			- Function executed successfully
//		FAILURE	- Bad arguments, or unknown combination of DP+DPE




// Add to given DP new DPE (DPE was not found in input file) for which all of:
//	address, archive and value format must be cleared
//
//	int VacImportAddDpe( string dpName, string dpeName, dyn_string &exceptionInfo );
//
//	ARGUMENTS
//		dpName				- [in], DP name to which DPE shall be added
//		dpeName				- [in], DPE name in DP for which address, archive and value format shall be cleared
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		OK			- Function executed successfully
//		FAILURE	- Bad arguments, or unknown DP, or existing combination of DP+DPE

// Set PVSS archive DP name for given archive name
//
//	int VacImportSetArchiveDp( string archiveName, string archiveDpName, dyn_string &exceptionInfo );
//
//	ARGUMENTS
//		archiveName			- [in], Name of PVSS archive
//		archiveDpName		- [in], Name of internal PVSS DP corresponding to archive
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		Number of DPEs affected (>=0)	- Function executed successfully
//		FAILURE							- Bad arguments, or unknown archiveName



// Sets for given DPE flag indicating that DIP publishing config shall be changed
//
//	int  VacImportSetDipConfigChange( string dpName, string dpeName, dyn_string &exceptionInfo );
//
//	ARGUMENTS
//		dpName				- [in], DP name for which DIP publishing config shall be changed
//		dpeName				- [in], DPE name in DP for which value format shall be changed
//		exceptionInfo	-	[out], all errors found are returned here.
//
//	RETURN
//		OK			- Function executed successfully
//		FAILURE	- Bad arguments, or unknown combination of DP+DPE


