#ifndef	SYNLHCLINEBEAM_H
#define	SYNLHCLINEBEAM_H

// Beam vacuum line of LHC synoptic view

#include "SynLhcLine.h"
#include "SynLhcInjDumpLine.h"
#include <QPainter>

class SynLhcLineBeam : public SynLhcLine  
{
public:
	SynLhcLineBeam(QWidget *parent, int type, float start, float end);
	virtual ~SynLhcLineBeam();

	virtual void clear( void );
	virtual void init(void);
	virtual void analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allCryoEqpMask, VacEqpTypeMask &allBeamEqpMask);
	virtual int build(unsigned vacTypeMask, VacEqpTypeMask &eqpMask);
	virtual int draw(QPainter &painter, int viewStart, int viewEnd);
	virtual void calcVerticalLimits(void);
	virtual void moveVertically(int &delta);
	virtual void decideForSectorBorders(void);
	virtual void finishGeometry(void);
	virtual void setViewStart(int viewStart, int viewWidth, bool dragging);
	virtual const char *closestItem(int mouseX, int mouseY);
	virtual void dump(FILE *pFile);

	virtual void setSectorGeometry(void); // [VACCO-929]
	virtual SynLhcSectorLabel* getSectorFromXY(int sectx, int secty); // [VACCO-929]
	virtual void drawSectSelection(QPainter &painter); // [VACCO-929]
	virtual QList<SynLhcInjDumpLine *>	getChildLines(void) { return childLines; }; // [VACCO-929]

	virtual bool findDevicePosition(Eqp *pEqp, int &position, bool &visible);
	virtual void setMode(DataEnum::DataMode newMode);

	virtual bool isAlwaysIncludeSectorBorder(void) { return alwaysIncludeSectorBorder; }
	virtual void setAlwaysIncludeSectorBorder(bool flag);

protected:
	// Y-ascent for this line only - without taking into account child lines
	int		ascentThis;

	// Y-descent for this line only - without taking into account child lines
	int		descentThis;

	// List of child lines
	QList<SynLhcInjDumpLine *>	childLines;

	// Implementation of LhcLineParts's abstract methods
	virtual LhcLineParts::BuildType linePartMethod(void) { return LhcLineParts::TypeBeam; }
	virtual bool isMySector(Sector * /* pSector */) { return false; }
	virtual bool isMyEqp(Eqp *pEqp, int &partIdx);
	virtual bool checkEqpVacType(Eqp *pEqp, int &partIdx);
	virtual bool eqpMatchesPartType(Eqp *pEqp, int partType);
	virtual int vacTypeOfRegion(int regionType);

	virtual void buildEqpGroups(void);
	virtual int calcSectorBorderPosition(SynLhcEqpItem *pItem, int connPos);
	virtual void drawPartStart(QPainter &painter, int lineStart);
	virtual void drawPartEnd(QPainter &painter, int lineEnd);
	virtual void drawEqpSectorBorder(Eqp *pEqp, int xCenter, QPainter &painter);

	virtual void addChildLine(Eqp *pConnEqp, int partIdx);
	virtual void placeEqp(float &coord, int &connPos, float &endCoord, int &endConnPos);
};
#endif	// SYNLHCLINEBEAM_H
