//	Implementation of ProfileItem class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileItem.h"

#include "Eqp.h"

#include "MobileType.h"


/////////////////////////////////////////////////////////////////////////////////
////////////////// Construction/destruction

ProfileItem::ProfileItem(Eqp *pEqp, DataEnum::DataMode mode)
{
	this->pEqp = pEqp;
	this->mode = mode;
	value = 0;
	valueValid = selected = flashColor = false;
	hidden = true;	// !!!
	ctlStatus = pEqp->getCtlStatus(mode);

	// If this is mobile equipment - connect to mobile state change
	// signals of device in order to react on connection/disconnection
	if(pEqp->getMobileType() == MobileType::OnFlange)
	{
		QObject::connect(pEqp, SIGNAL(mobileStateChanged(Eqp *, DataEnum::DataMode)),
			this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
	}
}

ProfileItem::~ProfileItem()
{
	if(!hidden)
	{
		pEqp->disconnect(this, mode);
	}
	if(pEqp->getMobileType() == MobileType::OnFlange)
	{
		QObject::disconnect(pEqp, SIGNAL(mobileStateChanged(Eqp *, DataEnum::DataMode)),
			this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
	}
}

/*
**	FUNCTION
**		Connect to equipment data when item becomes visible in profile,
**		read immediately last known value for device to be ready for display
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileItem::show(void)
{
	if(!hidden)
	{
		return;
	}
	pEqp->connect(this, mode);
	hidden = false;
	value = pEqp->getMainValue(mode);
	valueValid = pEqp->isMainValueValid(mode);
	pEqp->getMainColor(color, mode);
	selected = pEqp->isSelected();
}

/*
**	FUNCTION
**		Disconnect from equipment data when item becomes invisible in profile
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileItem::hide(void)
{
	if(hidden)
	{
		return;
	}
	hidden = true;
	pEqp->disconnect(this, mode);
}

/*
**	FUNCTION
**		Slot activated by mobile equipment when it's activity has been changed
**
**	ARGUMENTS
**		pSrc	- Pointer to source device
**		mMode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileItem::mobileStateChange(Eqp * /* pSrc */, DataEnum::DataMode mode)
{
	switch(mode)
	{
	case DataEnum::Online:
	case DataEnum::Polling:
		if(this->mode == DataEnum::Replay)
		{
			return;
		}
		break;
	case DataEnum::Replay:
		if(this->mode != DataEnum::Replay)
		{
			return;
		}
		break;
	}
	if(!hidden)
	{
		emit eqpSetChanged();
	}
	else
	{
		emit eqpMaskChanged();
	}
}

/*
**	FUNCTION
**		Slot activated when value for one of device's DPEs has been changed
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileItem::dpeChange(Eqp * /* pSrc */, const char * /* dpeName */,
	DataEnum::Source /* source */, const QVariant & /* value */, DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	switch(mode)
	{
	case DataEnum::Online:
	case DataEnum::Polling:
		if(this->mode == DataEnum::Replay)
		{
			return;
		}
		break;
	case DataEnum::Replay:
		if(this->mode != DataEnum::Replay)
		{
			return;
		}
		break;
	}
	analyzeCtlStatusChange();
	if(hidden)
	{
		return;
	}
	value = pEqp->getMainValue(mode);
	valueValid = pEqp->isMainValueValid(mode);
	QColor newColor;
	pEqp->getMainColor(newColor, mode);
	bool colorChanged = false;
	if(color != newColor)
	{
		color = newColor;
		colorChanged = true;
	}
	emit eqpDataChanged(this, colorChanged);
}

void ProfileItem::analyzeCtlStatusChange(void)
{
	if(ctlStatus == pEqp->getCtlStatus(mode))
	{
		return;
	}
	bool oldVisible = shallBeVisible();
	ctlStatus = pEqp->getCtlStatus(mode);
	if(oldVisible != shallBeVisible())
	{
		emit visibilityChanged(this);
	}
}

bool ProfileItem::shallBeVisible(void)
{
	bool result = false;
	switch(ctlStatus)
	{
	case Eqp::Used:
	case Eqp::TempRemoved:
		result = true;
		break;
	default:
		break;
	}
	if(result)
	{
		return pEqp->isActive(mode);
	}
	return false;
}

/*
**	FUNCTION
**		Slot activated when device's selection has been changed
**
**	ARGUMENTS
**		pSrc		- Pointer to object - source of signal
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileItem::selectChange(Eqp * /* pSrc */)
{
	if(selected != pEqp->isSelected())
	{
		selected = pEqp->isSelected();
		emit eqpSelectChanged(this);
	}
}

