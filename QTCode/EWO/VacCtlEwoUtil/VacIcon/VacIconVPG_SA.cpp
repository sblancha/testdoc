//	Implementation of VacIconVPG_SA	class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPG_SA.h"

#include "EqpVPG_SA.h"

#include <qpainter.h>
#include <QMouseEvent>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OPEN				0,255,0
#define COLOR_CLOSED			255,0,0
#define	COLOR_ERROR				255,102,255
#define	COLOR_WARNING			255,255,102

// Colors for control and interlocks
#define COLOR_GREEN				0,255,0
#define COLOR_YELLOW			255,255,0
#define COLOR_RED				255,0,0
#define	COLOR_WHITE				255,255,255

//	Main state bits to be analyzed
//	Main state bits to be analyzed
#define	RR1_VALID			(0x40000000)
#define RR1_ERROR			(0x00800000)
#define	RR1_WARNING			(0x00400000)
#define	RR1_ON				(0x00020000)
#define	RR1_OFF				(0x00010000)

#define	RR2_VVI_OPEN		(0x08000000)
#define	RR2_VVI_CLOSED		(0x04000000)
#define	RR2_TMP_SPEED_OK	(0x00400000)
#define	RR2_TMP_ON			(0x00080000)
#define	RR2_PP_ON			(0x00010000)
#define	RR2_VVR_OPEN		(0x00000200)
#define	RR2_VVR_CLOSED		(0x00000100)



/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
VacIconVPG_SA::VacIconVPG_SA(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(19, 41);
	setAreaGeometry();
}

VacIconVPG_SA::~VacIconVPG_SA()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for symbol areas rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_SA::setAreaGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		valve.setRect(1, 3, 17, 17);
		pump.setRect(1, 23, 17, 17);
		break;
	case VacIconContainer::Down:
		pump.setRect(1, 1, 17, 17);
		valve.setRect(1, 20, 17, 17);
		break;
	case VacIconContainer::Left:
		pump.setRect(1, 1, 17, 17);
		valve.setRect(20, 1, 17, 17);
		break;
	case VacIconContainer::Right:
		valve.setRect(3, 1, 17, 17);
		pump.setRect(23, 1, 17, 17);
		break;
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPG_SA::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		return VacIconGeometry(19, 41, 9, 44, -1);
		break;
	default:
		break;
	}
	return VacIconGeometry(41, 19, 41, 9, -1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_SA::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(19);
		size.setHeight(41);
		break;
	default:
		size.setWidth(41);
		size.setHeight(19);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreaGeometry();
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_SA::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVPG_SA"), "VacIconVPG_SA::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVPG_SA *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_SA::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_SA::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("RR2");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_SA::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_SA::getToolTip(const QPoint &pos, QString &text, QRect &rect)
{
	if(pEqp && connected)
	{
		// Where is the point? If in one of valve - show valve text,
		// otherwise show valve name and state
		if(valve.contains(pos))
		{
			text = pEqp->getVisibleName();
			QString state;
			pEqp->getPartStateString(Eqp::EqpPartVVR, state, mode);
			text += ": ";
			text += state;
			rect = valve;
		}
		else if(pump.contains(pos))
		{
			pEqp->getToolTipString(text, mode);
			rect = pump;
		}
	}
	else
	{
		rect = this->rect();
		text = "Not connected";
	}
	return true;
}

/*
**	FUNCTION
**		Override VacIcon's method in order to have different menus for different
**		parts of icon
**
**	ARGUMENTS
**		pEvent	- Pointer to mouse event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_SA::mousePressEvent(QMouseEvent *pEvent)
{
	if(pEqp)
	{
		QPoint mousePoint(pEvent->x(), pEvent->y());
		QPoint screenPoint = mapToGlobal(mousePoint);
		int buttonNbr = 0;
		switch(pEvent->button())
		{
		case Qt::LeftButton:
			buttonNbr = 1;
			break;
		case Qt::RightButton:
			buttonNbr = 2;
			break;
		default:
			break;
		}
		if(buttonNbr)
		{
			if(valve.contains(mousePoint))
			{
				emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), Eqp::EqpPartVVR, pEqp->getDpName());
			}
			else
			{
				emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), 0, pEqp->getDpName());
			}
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPG_SA::paintEvent(QPaintEvent * /* pEvent */)
{
	drawImage(true);
}
void VacIconVPG_SA::drawImage(bool clearArea)
{
	unsigned rr1 = 0, rr2 = 0;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		rr2 = pEqp->getRR2(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	if(clearArea)
	{
		painter.eraseRect(0, 0, width(), height());
	}
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	drawValve(painter, ctlStatus, rr1, rr2, plcAlarm);

	drawPump(painter, ctlStatus, rr1, rr2, plcAlarm);
}

void VacIconVPG_SA::drawValve(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm)
{
	// Save painter coordinate system - interlocks will be drawn
	// in original (non-translated) coordinate system
	painter.save();

	// Prepare painter's coordinate system for image drawing
	// Coordinate system (0,0) is center of valve symbol
	painter.translate(valve.center().x(), valve.center().y());

	// Turn coordinate system according to direction
	int offset = 0;
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		offset = -1;
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		offset = -1;
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	// Draw vacuum pipe - two short lines
	painter.fillRect(offset - (pipeWidth >> 1), -13, pipeWidth, 5, pipeColor);
	painter.fillRect(offset - (pipeWidth >> 1), 8, pipeWidth, 5, pipeColor);
	
	// Draw valve image
	setPainterForValve(painter, ctlStatus, rr1, rr2, plcAlarm);

	QPolygon points(4);
	points.setPoint(0, -8, -8);
	points.setPoint(1, 8, -8);
	points.setPoint(2, -8, 8);
	points.setPoint(3, 8, 8);
	painter.drawPolygon(points);

	// Restore coordinate system to original
	painter.restore();
}

void VacIconVPG_SA::drawPump(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm)
{
	QColor	color;
	painter.setPen(Qt::black);
	bool drawRect = true;
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		painter.fillRect(pump, color);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		painter.fillRect(pump, color);
		break;
	default:
		if(plcAlarm)
		{
			color.setRgb(COLOR_UNDEFINED);
			painter.fillRect(pump, color);
		}
		else if(!(rr1 & RR1_VALID))
		{
			color.setRgb(COLOR_UNDEFINED);
			painter.fillRect(pump, color);
		}
		else if(rr1 & RR1_ERROR)
		{
			color.setRgb(COLOR_ERROR);
			painter.fillRect(pump, color);
		}
		else if(rr1 & RR1_WARNING)
		{
			color.setRgb(COLOR_WARNING);
			painter.fillRect(pump, color);
		}
		else
		{
			unsigned maskedOnOff = rr1 & (RR1_ON | RR1_OFF);
			if(maskedOnOff == RR1_ON)
			{
				// ON - basci color is green. However, it is completely green if:
				//	- TMP is ON
				//	- PP is ON
				//	- VVI is open
				bool fullOn = (rr2 & RR2_PP_ON) &&
					(rr2 & RR2_TMP_ON) && (rr2 & RR2_TMP_SPEED_OK) &&
					((rr2 & (RR2_VVI_OPEN | RR2_VVI_CLOSED)) == RR2_VVI_OPEN);
				if(fullOn)
				{
					color.setRgb(COLOR_GREEN);
					painter.fillRect(pump, color);
				}
				else
				{
					QPolygon points(3);
					points.setPoint(0, pump.left(), pump.top());
					points.setPoint(1, pump.right(), pump.top());
					points.setPoint(2, pump.left(), pump.bottom());
					color.setRgb(COLOR_GREEN);
					painter.setBrush(color);
					painter.drawPolygon(points);
					points.setPoint(0, pump.right(), pump.top());
					points.setPoint(1, pump.right(), pump.bottom());
					points.setPoint(2, pump.left(), pump.bottom());
					color.setRgb(COLOR_WHITE);
					painter.setBrush(color);
					painter.drawPolygon(points);
					drawRect = false;
				}
			}
			else if(maskedOnOff == RR1_OFF)
			{
				color.setRgb(COLOR_WHITE);
				painter.fillRect(pump, color);
				if(rr2 & RR2_PP_ON)
				{
					color.setRgb(COLOR_GREEN);
				}
				else
				{
					color.setRgb(COLOR_RED);
				}
				painter.setBrush(color);
				painter.setPen(Qt::NoPen);
				// drawPie() to have circle filled with brush, drawArc() does not fill
				painter.drawPie(pump.left() + 2, pump.top() + 2, pump.width() - 5, pump.height() - 5,
					0, 360 * 16);
			}
			else if(maskedOnOff)	// Both ON and OFF
			{
				color.setRgb(COLOR_ERROR);
				painter.setBrush(color);
				painter.drawRect(pump);
			}
			else	// Neither ON nor OFF
			{
				color.setRgb(COLOR_UNDEFINED);
				painter.setBrush(color);
				painter.drawRect(pump);
			}
		}
		break;
	}

	// Draw pump rectangle, circle and line
	painter.setPen(Qt::black);
	painter.setBrush(Qt::NoBrush);
	if(drawRect)
	{
		painter.drawRect(pump.left(), pump.top(), pump.width() - 1, pump.height() - 1);
	}
	painter.drawArc(pump.left() + 2, pump.top() + 2, pump.width() - 5, pump.height() - 5,
			0, 360 * 16);
	QPoint center = pump.center();
	painter.drawLine(center.x() - 3, center.y(), center.x() + 3, center.y());
}

void VacIconVPG_SA::setPainterForValve(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm)
{
	QColor fillColor(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		fillColor.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		fillColor.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr2 & RR2_VVR_OPEN)
				{
					if(rr2 & RR2_VVR_CLOSED)
					{
						fillColor.setRgb(COLOR_ERROR);
					}
					else
					{
						fillColor.setRgb(COLOR_OPEN);
					}
				}
				else if(rr2 & RR2_VVR_CLOSED)
				{
					fillColor.setRgb(COLOR_CLOSED);
				}
			}
		}
		break;
	}
	painter.setBrush(fillColor);
	painter.setPen(Qt::black);
}

