#ifndef	VACICONVLV_ANA_H
#define	VACICONVLV_ANA_H

//	Icon for VLV_ANA

#include "VacIcon.h"

class EqpVLV_ANA;

class VACCTLEWOUTIL_EXPORT VacIconVLV_ANA : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVLV_ANA(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVLV_ANA();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);


protected:
	// Pointer to my device
	EqpVLV_ANA *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);
	
	void setPainterForControl(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1,  bool plcAlarm);
	int setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm);

};

#endif	// VACICONVLV_ANA_H
