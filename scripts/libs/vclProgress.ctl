
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Support for long lasting actions: progress bar, possibility to cancel...

//
time progressTimeStart;

// Name of progress bar
global string      glLhcVacPrBarName;

// Progress bar shape
global shape       glLhcVacPrBar;

// Flag indicating if progress bar is used to indicate execution progress
global bool        glLhcVacHasPrBar;

// Flag indicating if long lasting action has been cancelled
global bool        glLhcVacActCancel;

// Stored percent of exection done until now
global dyn_float   glLhcVacDoneBefore;

// Stored weights of long lasting action parts
global dyn_float   glLhcVacPartWeight;

// Start long lasting action, barName - name of bar control where progress will be reported
void LhcVacStartLongAct(string barName)
{
  glLhcVacPrBarName = barName;
  if(barName != "")
  {
    glLhcVacPrBar = getShape(barName);
    glLhcVacPrBar.totalSteps(1000);
    glLhcVacPrBar.progress(0);
    glLhcVacHasPrBar = true;
  }
  else
  {
    glLhcVacHasPrBar = false;
  }
  glLhcVacDoneBefore = makeDynFloat(0.0);
  dynClear(glLhcVacPartWeight);
  glLhcVacActCancel = false;
}
// Cancel long lasting action
void LhcVacSetLongActCancel(bool newState)
{
  glLhcVacActCancel = newState;
  if(newState)
  {
    glLhcVacHasPrBar = false;
  }
}
// Get cancel status of long lasting action
bool LhcVacGetLongActCancel()
{
  return glLhcVacActCancel;
}
// Start part of long lasting action, percent - what part of the whole process
// is covered by this part. true will be returned if action is cancelled
bool LhcVacStartLongActPart(float percent)
{
  if(glLhcVacHasPrBar)
  {
    dynAppend(glLhcVacPartWeight, percent /100.);
    dynAppend(glLhcVacDoneBefore, glLhcVacPrBar.progress / 1000.);
  }
  return glLhcVacActCancel;
}
// Finish last started part of long lasting action
// Function returns true if the whole action is cancelled
bool LhcVacFinishLongActPart( )
{
  if(glLhcVacHasPrBar)
  {
    int listLen = dynlen(glLhcVacPartWeight);
    LhcVacReportLongAct(100., 100.);
    if(listLen > 0)
    {
      dynRemove(glLhcVacPartWeight, listLen);
      if((listLen = dynlen(glLhcVacDoneBefore)) > 0)
      {
        dynRemove(glLhcVacDoneBefore, listLen);
      }
    }
  }
  return glLhcVacActCancel;
}
// Report progress of long lasting action (or part of action)
// done - how much was done (for eample, number of items processed),less then total.
// total - how much shall be done in total (for example, total number of items
//				to be processed.
// Function returns true if the whole action is cancelled
bool LhcVacReportLongAct(float done, float total)
{
  if(glLhcVacActCancel)
  {
    return true;	// Cancelled
  }
  if(!glLhcVacHasPrBar)
  {
     return false; // No progress bar to report
  }

  int	listLen = dynlen(glLhcVacPartWeight);
  float	value, doneBefore;

  value = done / total;
  if(value < 0.0)
  {
    value = 0.0;
  }
  else if(value > 1.0)
  {
    value = 1.0;
  }

  // Calculate weight coefficient to be applied to this value
  for(int  n = 1 ; n <= listLen ; n++)
  {
    value *= glLhcVacPartWeight[n];
  }
  if(value < 0.0)
  {
    value = 0.0;
  }
  else if(value > 1.0)
  {
     value = 1.0;
  }

  if((listLen = dynlen(glLhcVacDoneBefore)) > 0)
  {
    doneBefore = glLhcVacDoneBefore[listLen];
  }
  else
  {
    doneBefore = 0.0;
  }
  glLhcVacPrBar.progress((doneBefore + value ) * 1000.);
  return false;
}


// --------------------------------------------------------
// Progress bar processing functions section
// Alternative progress bar/label support made by ASA
// --------------------------------------------------------

//ProgressLabelStart:
//
//	Prepare progress bar
//
//	@param progressBar: in, shape of progress bar
//	@param progressBarLabel: in, shape of progress bar label
//
//    @return none
//    @author ASA
//    @mdf ASA 19sep11
//
void ProgressLabelStart( shape progressBarLabel )
{
  //DebugN( "ProgressLabelStart", progressBarLabel );
  
  progressTimeStart = getCurrentTime();

  progressBarLabel.text = "";
  progressBarLabel.visible = true;
  progressBarLabel.backCol = "<{51,255,0},7,{214,211,206},5,{214,211,206},0,{0,0,0},0,{0,0,0},0,{0,0,0},0>";
}
//ProgressLabelSet:
//
//	Set value on progress bar label
//
//	@param value: in, value to set on progress bar
//	@param value100percent: in, maximum value may be set on progress bar
//      @displayPeriod: in, period in sec
//        
//    @return none
//    @author ASA
//    @mdf ASA 19sep11
//
void ProgressLabelSet( shape progressBarLabel, int value, int value100percent )
{  
  float  percent, progressTime;
  time   progressTimeNow;
  string buf;
  int pTimeMin,pTimeSec; 

  progressTimeNow = getCurrentTime( );
  //progressTime = period( progressTimeNow - progressTimeStart ) / 60.;
  
  pTimeSec = period( progressTimeNow - progressTimeStart );
  pTimeMin = pTimeSec / 60;
  pTimeSec = pTimeSec - (pTimeMin * 60);
  
  if(value100percent == value)
  {
    percent = 100.;
  }
  else
  {
    if(value100percent == 0)
    {
      percent = 0.;
    }
    else
    {
      percent = ((float)value * 100) / (float)value100percent;
    }
  }
  //sprintf( buf, "%5.1f%", percent );
  //sprintf( buf, "%5.1f%%,%7.2fMin", percent, progressTime );
  sprintf( buf, "%5.1f%% %5dmin %2dsec", percent, pTimeMin, pTimeSec );
  //DebugN( "vclProgress.ctl:ProgressLabelSet", buf, progressTime );  
  progressBarLabel.text = buf;
}
//ProgressLabelFinish:
//
//	Finish progress bar
//
//	@param progressBarLabel: in, shape of progress bar label
//	@param message: in, message to display on progress bar label
//
//	@return none
//	@author ASA
//      @mdf ASA 19sep11
//
void ProgressLabelFinish( shape progressBarLabel, string message )
{
  //DebugN( "ProgressLabelFinish", progressBarLabel );
  
  progressBarLabel.backCol = "green";
  if( message != "" )
      progressBarLabel.text = message;
  else
      ProgressLabelSet( progressBarLabel, 100, 100 );
  return;
}
//ProgressLabelClear:
//
//	Set unvisible progress bar label
//
//	@param progressBarLabel: in, shape of progress bar label
//
//	@return none
//	@author ASA
//      @mdf ASA 19sep11
//
void ProgressLabelClear( shape progressBarLabel )
{
  //DebugN( "ProgressLabelClear", progressBarLabel );
  progressBarLabel.visible = false;
}

