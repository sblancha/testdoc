#ifndef MAINVIEWTABLEDEQPCONNECT_H
#define	MAINVIEWTABLEDEQPCONNECT_H

//	Class provides equipment connection functionality for
//	MainViewTabbed. MainViewTabbed itself can not derive from
//	InterfaceEqp directly because both InterfaceEqp and QWidget
//	are derived from QObject - and such situation can not be
//	handled by Qt correctly. Message from one of Qt forums:
//
//	Your class inherits from multiple QObject based classes, normally
//	in C++ this could be handled by virtual inheritance, but some parts
//	of QObject is not made compativle with this, so that is no go.

#include "InterfaceEqp.h"

// Class holding parameters of one DP connection
class OneDpConnection
{
public:
	OneDpConnection(Eqp *pEqp, int tag);
	~OneDpConnection() {}

	// Access
	Eqp *getEqp(void) const { return pEqp; }
	int getTag(void) const { return tag; }

protected:

	// Pointer to device
	Eqp			*pEqp;

	// tag for callback
	int			tag;
};

class MainViewTabledEqpConnect : public InterfaceEqp
{
	Q_OBJECT

public:
	MainViewTabledEqpConnect();
	virtual ~MainViewTabledEqpConnect();

	int connectDp(const char *dp, int tag);
	int disconnectDp(const char *dp);
	int disconnectAll(void);

	int getEqpCount(void) const { return dps.count(); }

public slots:
	// Override slot of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

signals:
	// Signal is emitted when state of one of devices has been changed
	void dpDataChanged(OneDpConnection *pDpData);

protected:
	// List of all connected DPs
	QList<OneDpConnection *>	dps;
};

#endif	// MAINVIEWTABLEDEQPCONNECT_H
