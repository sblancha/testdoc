#ifndef	VACICONVP_STDIO_H
#define	VACICONVP_STDIO_H

// Icon for VP_STDIO	- PP+Valve in TDC2 of SPS

#include "VacIcon.h"

class EqpVP_STDIO;

class VACCTLEWOUTIL_EXPORT VacIconVP_STDIO : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVP_STDIO(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVP_STDIO();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

	// Override method of VacIcon 
	virtual void setDirection(VacIconContainer::Direction newDirection);

protected:
	// Pointer to my device
	EqpVP_STDIO *pEqp;

	// Areas for pump and valve symbols. Drawing can be easy using QPainter
	// coordinate transformations, but finding areas for tooltips is not so
	// evident. May be it is more convenient just to keep in memory coordinates
	// of all distinct areas

	QRect	pump;
	QRect	vvr;

	void setAreaGeometry(void);

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawPipes(QPainter &painter);
	virtual void drawVvr(QPainter &painter);
	virtual void drawPump(QPainter &painter);
};

#endif	// VACICONVP_STDIO_H
