#include "Rack.h"
#include "RackEqp.h"
#include "Eqp.h"

Rack::Rack()
{

}

Rack::~Rack()
{
	qDeleteAll(listOfEquipment);
	listOfEquipment.clear();
}

void Rack::addEquipment(RackEqp * pRackEqp)
{

	listOfEquipment.append(pRackEqp);
}

int Rack::getNumberOfEquipment()
{
	return(listOfEquipment.size());
}

QString Rack::getEqpNameAt(int index)
{
	if (index >= 0 && index < this->getNumberOfEquipment())
	{
		return listOfEquipment.at(index)->getName();
	}
	else
	{
		return Q_NULLPTR;
	}
}

int Rack::getPositionY(int index)
{
	if (index >= 0 && index < this->getNumberOfEquipment())
	{
		return listOfEquipment.at(index)->getPositionY();
	}
	else
	{
		return -2;
	}
}

int Rack::getPositionX(int index)
{
	if (index >= 0 && index < this->getNumberOfEquipment())
	{
		return listOfEquipment.at(index)->getPositionX();
	}
	else
	{
		return -2;
	}
}

float Rack::getSizeY(int index)
{
	if (index >= 0 && index < this->getNumberOfEquipment())
	{
		return listOfEquipment.at(index)->getSizeY();
	}
	else
	{
		return -2.0;
	}
}

float Rack::getSizeX(int index)
{
	if (index >= 0 && index < this->getNumberOfEquipment())
	{
		return listOfEquipment.at(index)->getSizeX();
	}
	else
	{
		return -2.0;
	}
}

QString Rack::getEqpType(int index)
{
	if (index >= 0 && index < this->getNumberOfEquipment())
	{
		return listOfEquipment.at(index)->getType();
	}
	else
	{
		return Q_NULLPTR;
	}
}

Eqp * Rack::getEqpPtr(int index)
{
	if (index >= 0 && index < this->getNumberOfEquipment())
	{
		return listOfEquipment.at(index)->getEqpPointer();
	}
	else
	{
		return Q_NULLPTR;
	}
}

int Rack::setEqpAt(int index, Eqp * pEqp)
{
	if (index >= 0 && index < getNumberOfEquipment())
	{
		RackEqp * rackEqp= listOfEquipment.at(index);
		rackEqp->setEqpPointer(pEqp);


		return(0);
	}
	return(-1);
}

bool Rack::containsEqp(QString eqpName)
{
	for (int i = 0; i < listOfEquipment.length();i++)
	{
		if (eqpName.compare(listOfEquipment.at(i)->getName()) == 0)
		{
			return true;
		}
	}
	return false;
}

RackEqp * Rack::getRackEqp(int index)
{
	if (index >= 0 && index < getNumberOfEquipment())
	{
		return listOfEquipment.at(index);
	}
	else
		return NULL;
}