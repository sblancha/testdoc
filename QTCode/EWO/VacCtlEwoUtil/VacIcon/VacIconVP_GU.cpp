/**
* @brief Class implementation for widget Vacuum Pump - Group Unified (VP_GU)
* @see VacIcon
*/

#include "VacIconVP_GU.h"

#include "EqpVP_GU.h"

#include "DataPool.h"

#include <qpainter.h>
#include <QMouseEvent>

VacIconVP_GU::VacIconVP_GU(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(40, 60);
	setAreaGeometry();
}

VacIconVP_GU::~VacIconVP_GU()
{
	disconnect();
}

/**
@brief FUNCTION Calculate geometry for symbol areas rectangles for current direction and beamDirection.
*/
void VacIconVP_GU::setAreaGeometry(void) {
	switch(direction) {
	case VacIconContainer::Up:
		// vvr1.setRect(12, 3, 14, 14); //middle 
		vvr1.setRect(3, 3, 14, 14); //left, later vvr1 is moved when isVvr2
		vvr2.setRect(24, 3, 14, 14); //right
		
		body.setRect(11, 22, 18, 18);
		break;
	case VacIconContainer::Down:
		//vvr1.setRect(12, (60 - (3 + 14)), 14, 14); //middle
		vvr1.setRect(3, (60 - (3 + 14)), 14, 14); //left, later vvr1 is moved when isVvr2
		vvr2.setRect(24, (60 - (3 + 14)), 14, 14); //right
				
		body.setRect(11, 18, 18, 18); // 18 y size of analog value
		break;
	default:
		vvr1.setRect(3, 3, 14, 14); //left
		vvr2.setRect(24, 3, 14, 14); //right
		body.setRect(11, 20, 18, 18);
		break;
	}
}
/**
@brief FUNCTION Return geometry for this icon
@param[in]   mask	Vacuum equipment mask, not used here
*/
VacIconGeometry VacIconVP_GU::getGeometry(const VacEqpTypeMask & /* mask */) {
	switch(direction) {
	case VacIconContainer::Up:
		return VacIconGeometry(40, 60, 20, 70, -1);
		break;
	case VacIconContainer::Down:
		return VacIconGeometry(40, 60, 20, 70, 1);
		break;
	default:
		return VacIconGeometry(40, 60, 20, 70, -1);
		break;
	}
}

/**
@brief FUNCTION Override method of VacIcon: geometry of this icon depends on direction
@param[in]   newDirection	new icon direction
*/
void VacIconVP_GU::setDirection(VacIconContainer::Direction newDirection) {
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(40);
		size.setHeight(60);
		break;
	default:
		size.setWidth(40);
		size.setHeight(60);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreaGeometry();
}

/**
@brief FUNCTION Set reference to equipment for this icon
@param[in]   pEqp	Pointer to device to be used by this icon
*/
void VacIconVP_GU::setEqp(Eqp *pEqp) {
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVP_GU"), "VacIconVP_GU::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVP_GU *)pEqp;
		QString attrName = pEqp->getAttrValue("MasterName");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				;//do nothing not supposed to have child
			}

		}
		if (pEqp->inherits("EqpVP_GU")) {
			if (((EqpVP_GU *)pEqp)->isVvr2()) {
				isVvr2 = true;
			}
			else {
				isVvr2 = false;
			}
		}
	}
	forceRedraw();
}

/**
@brief FUNCTION Set new data acquisition mode for this icon
@param[in]   newMode	New data aqn mode (online, replay...)
*/
void VacIconVP_GU::setMode(DataEnum::DataMode newMode) {
	mode = newMode;
	forceRedraw();
}

/**
@brief FUNCTION Set color used for pipe lines drawing
@param[in]   newColor	Color used for pipe drawing
*/
void VacIconVP_GU::setPipeColor(const QColor &newColor) {
	VacIcon::setPipeColor(newColor);
}

/**
@brief FUNCTION Connect to DPEs of device, return true if done
*/
bool VacIconVP_GU::connect(void) {
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("RR1");
	dpes.append("RR2");
	dpes.append("State");
	dpes.append("PR");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/**
@brief FUNCTION Disconnect DPEs of device, return true if done
*/
bool VacIconVP_GU::disconnect(void) {
	bool result = false;
	if(connected && pEqp) {
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/**
@brief FUNCTION Calculate text and rectangle for tooltip of this icon, return true if tooltip shown
@param[in]  pos		Position of mouse pointer WITHIN this widget
@param[out]	text	Variable where this method shall write tooltip text to be shown
@param[out]	rect	Variable where this method shall write rectangle for visibility	of returned tooltip: tooltip will be hidden when mouse pointer left
*/
bool VacIconVP_GU::getToolTip(const QPoint &pos, QString &text, QRect &rect) {
	if( (pEqp && connected) && (!pos.isNull()) ) {
		pEqp->getToolTipString(text, mode);
		rect = body;
	}
	else {
		rect = this->rect();
		text = "Not connected";
	}
	return true;
}

/**
@brief FUNCTION Override VacIcon's method in order to have different menus for different parts of icon
@param[in]  pEvent	Pointer to mouse event
*/
void VacIconVP_GU::mousePressEvent(QMouseEvent *pEvent) {
	if(pEqp) {
		QPoint mousePoint(pEvent->x(), pEvent->y());
		QPoint screenPoint = mapToGlobal(mousePoint);
		int buttonNbr = 0;
		switch(pEvent->button()) {
		case Qt::LeftButton:
			buttonNbr = 1;
			break;
		case Qt::RightButton:
			buttonNbr = 2;
			break;
		default:
			break;
		}
		if(buttonNbr) {
			if (vvr1.contains(mousePoint)) {
				emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), Eqp::EqpPartVVR1, pEqp->getDpName());
			}
			else if (vvr2.contains(mousePoint)) {
				emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), Eqp::EqpPartVVR2, pEqp->getDpName());
			}
			else {
				emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), 0, pEqp->getDpName());
			}
		}
	}
}

/**
@brief FUNCTION Icon drawing implementation
@param[in]  pEvent	Pointer to paint event
*/
void VacIconVP_GU::paintEvent(QPaintEvent * /* pEvent */) {
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp) {
		if(pEqp->isSelected()) {
			drawSelection(painter);
		}
	}
	drawPipes(painter, isVvr2);
	if (isVvr2) {
		drawVvr(painter, vvr2, "VVR2");
	}
	else {
		vvr1.moveLeft(body.center().x() - round(vvr1.width() / 2) +2 ); // move vvr1 to the middle moveLeft is move left corner +2 correction
	}
	drawVvr(painter, vvr1, "VVR1");
	// Blinking behavior
	if (pEqp)
		isBlinking = pEqp->isAlertNotAck();
	else
		isBlinking = false;
	if (isBlinking && !isBlinkVisible)
		return; // hiden phase of blinking

	drawBody(painter);
	
	double pr = 0.0;
	unsigned rr1 = 0;
	if (pEqp) {
		pr = pEqp->getPR();
		rr1 = pEqp->getRR1(DataEnum::Online);
	}
	if ((mode != DataEnum::Replay) && 
		(pr > 1E-14) && (pr < 1E4) &&
		(rr1 & RR1_VALID_VPGU)) {
		// If Pressure valid and online display value
		QString sPr = QString::number(pr, 'E', 0);
		drawPressureValue(painter, sPr);
	}
	if(!pEqp) {
		painter.setPen(Qt::black);
	}
}

/**
@brief FUNCTION PROTECTED Pipes of icon drawing
@param[in]  painter	Painter object
@param[in]  isVvr2	Pumping group with 2 VVR
*/
void VacIconVP_GU::drawPipes(QPainter &painter, bool isVvr2) {
	switch(direction) {
	case VacIconContainer::Up:
		// Line from VVR to outside
		painter.fillRect(vvr1.center().x() - pipeWidth / 2, -2, pipeWidth ? pipeWidth : 3, 4, pipeColor);
		if (isVvr2) {
			painter.fillRect(vvr2.center().x() - pipeWidth / 2, -2, pipeWidth ? pipeWidth : 3, 4, pipeColor);
		}
		// Connect pump to valve
		if (isVvr2) {
			// vvr1 to body
			painter.fillRect(vvr1.center().x() - 2, vvr1.bottom(), 
							 2 , body.top() - vvr1.bottom(),
							 pipeColor);
			painter.fillRect(body.center().x(), body.top(),
							 vvr1.center().x() -2 - body.center().x(), 2,
							 pipeColor);
			// vvr2 to body
			painter.fillRect(vvr2.center().x() + 2, vvr2.bottom(),
							 2, body.top() - vvr2.bottom(),
							 pipeColor);
			painter.fillRect(body.center().x(), body.top(),
							 vvr2.center().x() + 4 - body.center().x(), 2,
							 pipeColor);
		}
		else {
			painter.fillRect(body.center().x(),              vvr1.bottom(),
											 3, body.top() - vvr1.bottom(), pipeColor);
		}
		break;
	case VacIconContainer::Down: // above line
		// Line from VVR to outside
		painter.fillRect(vvr1.center().x() - pipeWidth / 2, vvr1.bottom(), pipeWidth ? pipeWidth : 3, 4, pipeColor);
		if (isVvr2) {
			painter.fillRect(vvr2.center().x() - pipeWidth / 2, vvr2.bottom(), pipeWidth ? pipeWidth : 3, 4, pipeColor);
		}
		// Connect pump to valve
		if (isVvr2) {
			// vvr1 to body
			painter.fillRect(vvr1.center().x() - 2, vvr1.top(),
							 2, body.bottom() - vvr1.top(),
							pipeColor);
			painter.fillRect(body.center().x(), body.bottom(),
							 vvr1.center().x() - 2 - body.center().x(), 2,
							 pipeColor);
			// vvr2 to body
			painter.fillRect(vvr2.center().x() + 2, vvr2.top(),
							 2, body.bottom() - vvr2.top(),
							 pipeColor);
			painter.fillRect(body.center().x(), body.bottom(),
							 vvr2.center().x() +4 - body.center().x(), 2,
							 pipeColor);
		}
		else {
			painter.fillRect(body.center().x(), body.bottom(),
							 3, vvr1.top() - body.bottom(), pipeColor);
		}
		break;
	case VacIconContainer::Left:
		// Line from VVR to outside
		painter.fillRect(vvr1.right(), vvr1.center().y() - pipeWidth / 2, pipeWidth ? pipeWidth : 3, 4, pipeColor);
		if (isVvr2) {
			painter.fillRect(vvr2.right(), vvr2.center().y() - pipeWidth / 2, pipeWidth ? pipeWidth : 3, 4, pipeColor);
		}
		// Connect pump to valve
		painter.fillRect(body.right(), body.center().y() - 1,
			vvr1.left() - body.right(), 3, pipeColor);
		if (isVvr2) {
			painter.fillRect(body.right(), body.center().y() - 1, vvr2.left() - body.right(), 3, pipeColor);
		}
		break;
	case VacIconContainer::Right:
		// Line from VVR to outside
		painter.fillRect(-2, vvr1.center().y() - pipeWidth / 2, pipeWidth ? pipeWidth : 3, 4, pipeColor);
		if (isVvr2) {
			painter.fillRect(-2, vvr2.center().y() - pipeWidth / 2, pipeWidth ? pipeWidth : 3, 4, pipeColor);
		}
		// Connect pump to valves
		painter.fillRect(vvr1.right(), body.center().y() - 1,
			body.left() - vvr1.right(), 3, pipeColor);
		if (isVvr2) {
			painter.fillRect(vvr2.right(), body.center().y() - 1, body.left() - vvr2.right(), 3, pipeColor);
		}
		break;
	}
}

void VacIconVP_GU::drawVvr(QPainter &painter, QRect vvr, QString subEqpName)
{
	// Save painter coordinate system - interlocks will be drawn
	// in original (non-translated) coordinate system
	painter.save();
	// Prepare painter's coordinate system for image drawing
	// Coordinate system (0,0) is center of valve symbol
	painter.translate(vvr.center().x(), vvr.center().y());

	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	
	//Define color
	QColor color;
	if (!pEqp)
		color.setRgb(COLOR_GP_NOEQP); // if no eqp icon color is violet
	else
		pEqp->getSubEqpColor(subEqpName, color, mode);
	painter.setBrush(color);
	painter.setPen(Qt::black);
	
	// Draw valve image
	QPolygon points(4);
	points.setPoint(0, -8, -8);
	points.setPoint(1, 8, -8);
	points.setPoint(2, -8, 8);
	points.setPoint(3, 8, 8);
	painter.drawPolygon(points);

	// Restore coordinate system to original
	painter.restore();
}

void VacIconVP_GU::drawBody(QPainter &painter)
{
	//Define the color and pressure value
	QColor	color;
	if (!pEqp) 
		color.setRgb(COLOR_GP_NOEQP); // if no eqp icon color is violet
	else {
		pEqp->getMainColor(color, mode);
	}
	painter.fillRect(body, color);
	painter.setPen(Qt::black);
	painter.drawRect(body);
	
	painter.setBrush(Qt::NoBrush);

	// Draw symbol
	painter.drawArc(body.left() + 1, body.top() + 1, body.width() - 2, body.height() - 2,
		0, 360 * 16);
	QPoint center = body.center();
	painter.drawLine(center.x() - 3, center.y(), center.x() + 3, center.y());
}

void VacIconVP_GU::drawPressureValue(QPainter &painter, QString &sPr)
{
	int xPosition = 1;
	int yPosition = 56;
	painter.setPen(Qt::black);
	
	if ( direction == VacIconContainer::Down) {
		yPosition = 14;
	}
	painter.drawText(xPosition, yPosition, sPr); // drawText x,y origin (0,0) is top-left
}

