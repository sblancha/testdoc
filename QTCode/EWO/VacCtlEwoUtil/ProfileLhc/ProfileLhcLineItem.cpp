//	Implementation of ProfileLhcLineItem class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileLhcLineItem.h"

#include "Eqp.h"

#include "MobileType.h"

#include "VacMainView.h"

#include "Sector.h"
#include "DebugCtl.h"

#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
////////////////// Construction/destruction

ProfileLhcLineItem::ProfileLhcLineItem(Eqp *pEqp, DataEnum::DataMode mode)
{
	this->pEqp = pEqp;
	this->mode = mode;
	value = 0;
	valid = flashing = colorChanged = selected = false;
	hidden = true;	// !!!
	barIdx = -1;
	ctlStatus = pEqp->getCtlStatus(mode);
	atSectorBorder = pEqp->isSectorBorder();
	start = realStart = pEqp->getStart();

	// If this is mobile equipment - connect to mobile state change
	// signals of device in order to react on connection/disconnection
	if(pEqp->getMobileType() == MobileType::OnFlange)
	{
		QObject::connect(pEqp, SIGNAL(mobileStateChanged(Eqp *, DataEnum::DataMode)),
			this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
	}
}

ProfileLhcLineItem::~ProfileLhcLineItem()
{
	if(!hidden)
	{
		pEqp->disconnect(this, mode);
	}
	if(pEqp->getMobileType() == MobileType::OnFlange)
	{
		QObject::disconnect(pEqp, SIGNAL(mobileStateChanged(Eqp *, DataEnum::DataMode)),
			this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
	}
}

/*
**	FUNCTION
**		Connect to equipment data when item becomes visible in profile,
**		read immediately last known value for device to be ready for display
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineItem::show(void)
{
	if(!hidden)
	{
		return;
	}
	pEqp->connect(this, mode);
	if(!pEqp->getAttrValue("CtlParent").isEmpty())
	{
		QObject::connect(pEqp, SIGNAL(sectorsChanged(Eqp *)), this, SLOT(sectorsChange(Eqp *)));
	}
	hidden = false;
	value = pEqp->getMainValue(mode);
	valid = pEqp->isMainValueValid(mode);
	pEqp->getMainColor(color, mode);
}

/*
**	FUNCTION
**		Disconnect from equipment data when item becomes invisible in profile
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineItem::hide(void)
{
	if(hidden)
	{
		return;
	}
	hidden = true;
	pEqp->disconnect(this, mode);
	if(!pEqp->getAttrValue("CtlParent").isEmpty())
	{
		QObject::disconnect(pEqp, SIGNAL(sectorsChanged(Eqp *)), this, SLOT(sectorsChange(Eqp *)));
	}
}

/*
**	FUNCTION
**		Slot activated by mobile equipment when it's activity has been changed
**
**	ARGUMENTS
**		pSrc	- Pointer to source device
**		mMode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineItem::mobileStateChange(Eqp * /* pSrc */, DataEnum::DataMode mode)
{
	switch(mode)
	{
	case DataEnum::Online:
	case DataEnum::Polling:
		if(this->mode == DataEnum::Replay)
		{
			return;
		}
		break;
	case DataEnum::Replay:
		if(this->mode != DataEnum::Replay)
		{
			return;
		}
		break;
	}
	emit mobileStateChanged(this);
}

/*
**	FUNCTION
**		Slot activated when value for one of device's DPEs has been changed
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineItem::dpeChange(Eqp * /* pSrc */, const char * /* dpeName */,
	DataEnum::Source /* source */, const QVariant & /* value */, DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	if(hidden)
	{
		return;
	}
	analyzeCtlStatusChange();
	switch(mode)
	{
	case DataEnum::Online:
	case DataEnum::Polling:
		if(this->mode == DataEnum::Replay)
		{
			return;
		}
		break;
	case DataEnum::Replay:
		if(this->mode != DataEnum::Replay)
		{
			return;
		}
		break;
	}
	value = pEqp->getMainValue(mode);
	valid = pEqp->isMainValueValid(mode);
	QColor newColor;
	pEqp->getMainColor(newColor, mode);
	bool colorChanged = false;
	if(color != newColor)
	{
		color = newColor;
		colorChanged = true;
	}
	emit eqpDataChanged(this, colorChanged);
}

void ProfileLhcLineItem::analyzeCtlStatusChange(void)
{
	if(ctlStatus == pEqp->getCtlStatus(mode))
	{
		return;
	}
	bool oldVisible = shallBeVisible();
	ctlStatus = pEqp->getCtlStatus(mode);
	if(oldVisible != shallBeVisible())
	{
		emit visibilityChanged(this);
	}
}

bool ProfileLhcLineItem::shallBeVisible(void)
{
	bool result = false;
	switch(ctlStatus)
	{
	case Eqp::Used:
	case Eqp::TempRemoved:
		result = true;
		break;
	default:
		break;
	}
	if(result)
	{
		return pEqp->isActive(mode);
	}
	return false;
}

/*
**	FUNCTION
**		Slot activated when device's selection has been changed
**
**	ARGUMENTS
**		pSrc		- Pointer to object - source of signal
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineItem::selectChange(Eqp * /* pSrc */)
{
	if(selected != pEqp->isSelected())
	{
		selected = pEqp->isSelected();
		emit eqpSelectChanged(false);
	}
}

/*
**	FUNCTION
**		Slot activated when device's sector reference has been changed
**
**	ARGUMENTS
**		pSrc		- Pointer to object - source of signal
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineItem::sectorsChange(Eqp * /* pSrc */)
{
	emit eqpDataChanged(this, true);
}

/*
**	FUNCTION
**		Draw this item
**
**	PARAMETERS
**		painter				- Painter used for drawing
**		backColor			- Background color of graph
**		flashBarsOnly		- true if only flashing bars shall be redrawn
**		useAltFlashColor	- true if flashing bars shall be drawn using alternative color
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineItem::draw(QPainter &painter, const QColor &backColor, bool flashBarsOnly, bool useAltFlashColor)
{
	if(hidden)
	{
		return;
	}
	if(!shallBeVisible())
	{
		return;
	}
	if(flashBarsOnly && (!flashing))
	{
		return;
	}
	QColor useColor = useAltFlashColor ? VacMainView::getProfileAltBarColor() : color;
	if((!valid) && (!useAltFlashColor))
	{
		useColor = backColor;
	}
	painter.setPen(useColor);
	if(!rect.width())
	{
		painter.drawLine(rect.left(), rect.top(), rect.left(), rect.bottom());
	}
	else
	{
		painter.fillRect(rect, useColor);
	}

	// Highlight selected bar
	if((!selected) || useAltFlashColor)
	{
		return;
	}
	painter.setPen(useColor.dark(200));
	int stepValue = rect.width() > 16 ? 5 : 2, i;
	for(i = rect.left() + 1 ; i < rect.right() ; i += stepValue)
	{
		painter.drawLine(i, rect.top(), i, rect.bottom());
	}
	for(i = rect.top() + 1 ; i < rect.bottom() ; i += stepValue)
	{
		painter.drawLine(rect.left(), i, rect.right(), i);
	}
}

/*
**	FUNCTION
**		Dump content to output file - for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for write
**		idx		- Index of this item in list
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineItem::dump(FILE *pFile, int idx)
{
	if(!DebugCtl::isProfile())
	{
		return;
	}
	fprintf(pFile, "  %04d: <%s> (%9.3f, real %9.3f) bar %d value %8.2e %s: (%d,%d)-(%d,%d)%s%s%s%s\n", idx,
		pEqp->getName(), start, realStart, barIdx, value, (valid ? "" : "INVALID"),
		rect.left(), rect.top(), rect.right(), rect.bottom(),
		(hidden ? " HIDDEN" : " "),
		(flashing ? " FLASH" : " "),
		(selected ? " SELECTED" : " "),
		(atSectorBorder ? "SECTOR BORDER" : ""));
	Sector *pSectBefore = pEqp->getSectorBefore(mode),
		*pSectAfter = pEqp->getSectorAfter(mode);
	fprintf(pFile, "     before <%s> after <%s>\n",
		(pSectBefore ? pSectBefore->getName() : "none"),
		(pSectAfter ? pSectAfter->getName() : "none"));
}

