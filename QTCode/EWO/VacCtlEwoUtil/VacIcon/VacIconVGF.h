#ifndef	VACICONVGF_H
#define	VACICONVGF_H

//	Icon for VGF

#include "VacIconVG.h"

class EqpVGF;

class VACCTLEWOUTIL_EXPORT VacIconVGF : public VacIconVG
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVGF(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVGF();

	// Override VacIconVG's methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setDirection(VacIconContainer::Direction newDirection);

protected:
	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus);

	// Dummy implementation of abstract method - never used
	virtual void drawSpecific(QPainter & /* painter */) {}
};

#endif	// VACICONVGF_H
