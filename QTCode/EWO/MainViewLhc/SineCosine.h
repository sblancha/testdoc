#ifndef SINECOSINE_H
#define SINECOSINE_H

// Class holding sin() and cos() values for one angle - used
// to reduce claculation time

#include <stdio.h>
class SineCosine
{
public:
	SineCosine(double angle);
	~SineCosine() {};


	// Access
	inline double getAngle(void) { return angle; }
	void setAngle(double angle);
	inline double getSinValue(void) { return sinValue; }
	inline double getCosValue(void) { return cosValue; }

	void dump(FILE *pFile, int index);
	
protected:
	double	angle;
	double	sinValue;
	double	cosValue;
	float	pos;
};

#endif	// SINECOSINE_H
