#ifndef SMSCONFIGTABLE_H
#define	SMSCONFIGTABLE_H

// EWO interface for ConfigTable widget

#include <BaseExternWidget.hxx>

#include "ConfigTable.h"

class EWO_EXPORT SmsConfigTable : public BaseExternWidget
{
	Q_OBJECT

public:
	SmsConfigTable(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void selectionChange(const QItemSelection &selected, const QItemSelection &deselected);
	void connectRequest(void);
	void doubleClick(const QModelIndex &index);

private:
	ConfigTable	*baseWidget;

	QVariant invokeAddConfig(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeAddGroupConfig(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeAddToGroup(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeDeleteConfig(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetConfigId(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetConfigOwner(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetConfigName(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetConfigFlags(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetConfigNumMsg(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetConfigNumEvents(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeGetConfigAtRow(const QString &name, QList<QVariant> &values, QString &error);

};

#endif	// SMSCONFIGTABLE_H
