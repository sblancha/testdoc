/**
  @brief Bitmasks for V_TCP command bits (WR1 - 16 bits)
**/
const unsigned V_TCP_W_ENABLE = 0x0100;
const unsigned V_TCP_W_DISABLE = 0x0200;
const unsigned V_TCP_W_DISCONNECT = 0x0800;
//-----------------------
//Actions for V_TCP  type
/** 
@brief Execute action for V_TCP. It has been checked before call if action is known.
@param[in]     dpName    DP name
@param[in]     dpType:   DP type
@param[in]     act       Enum for action to be executed.
@param[in,out] exceptionInfo   Details of any exceptions are returned here
*/
void LhcVacDeviceAction_V_TCP(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo) {
  
  unsigned		   newWr1;
  bool			   possible;
  dyn_errClass		err;
  unsigned        act;
  mapping         mConnectDpes;
        
  act = action[1] & LVA_ACTION_MASK;

  switch(act)
  {
  case LVA_V_TCP_ENABLE: 
    newWr1 = V_TCP_W_ENABLE;
    break;
  case LVA_V_TCP_DISABLE: 
    newWr1 = V_TCP_W_DISABLE;
    break;
  case LVA_V_TCP_DISCONNECT: 
    newWr1 = V_TCP_W_DISCONNECT;
    break;
  }
  // Execute
  dpSetWait(dpName + ".WR1", newWr1);
  err = getLastError();
  if(dynlen(err) > 0) {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
  
}
