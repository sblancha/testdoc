#ifndef BEAMLINEWEIGHT_H
#define	BEAMLINEWEIGHT_H

//	Class used to keep 'weight' on individual beam lines

class BeamLine;

class BeamLineWeight
{
public:
	BeamLineWeight(BeamLine *pBeamLine)
	{
		this->pBeamLine = pBeamLine;
		weight = 0;
	}
	~BeamLineWeight() {}

	// Access
	inline BeamLine *getLine(void) const { return pBeamLine; }
	inline int getWeight(void) const { return weight; }
	inline void increment(void) { weight++; }

protected:
	// Pointer to beam line
	BeamLine	*pBeamLine;

	// Weight of this beam line
	int			weight;
};

#endif	// BEAMLINEWEIGHT_H
