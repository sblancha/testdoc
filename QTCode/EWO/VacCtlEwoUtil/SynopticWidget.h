#ifndef	SYNOPTICWIDGET_H
#define	SYNOPTICWIDGET_H

#include "VacCtlEwoUtilExport.h"

//	Subclass of QWidget that defines one special method for tooltip support
//	on synoptic view

#include "qwidget.h"

class VACCTLEWOUTIL_EXPORT SynopticToolTip;

class SynopticWidget : public QWidget
{
	Q_OBJECT

public:
	SynopticWidget(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	~SynopticWidget();

	virtual bool getToolTip(const QPoint &point, QString &text, QRect &rect) = 0;

protected:
	bool event(QEvent *pEvent);
};

#endif	// SYNOPTICWIDGET_H
