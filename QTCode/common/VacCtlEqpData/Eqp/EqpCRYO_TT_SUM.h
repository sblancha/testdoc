#ifndef	EQPCRYO_TT_SUM_H
#define	EQPCRYO_TT_SUM_H

//	CRYO_TT_SUM - summary of cold mass (CM) and beam screen (BS) themometers

#include "Eqp.h"

#include "VacType.h"

class VACCTLEQPDATA_EXPORT EqpCRYO_TT_SUM : public Eqp
{
	Q_OBJECT

public:
	EqpCRYO_TT_SUM(const Eqp &source);
#ifdef Q_OS_WIN
	EqpCRYO_TT_SUM(const EqpCRYO_TT_SUM &src) : Eqp(src) {}
#endif
	~EqpCRYO_TT_SUM();

	virtual inline bool isCryoThermometer(void) const { return true; }
	virtual int getVacType(void) const { return VacType::RedBeam | VacType::BlueBeam | VacType::CrossBeam | VacType::CommonBeam; }

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::CryoTemperature; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode mode);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
	virtual void getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end);
	virtual float getGrowFactorForDevList(void);
	virtual int getSpikeFilterForDevList(void);
#endif

	// Access for essential values
	inline unsigned getCMRR1(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? cmRr1Replay : cmRr1Online; }
	inline float getCMT(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? cmTReplay : cmTOnline; }
	inline unsigned getBSINRR1(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? bsInRr1Replay : bsInRr1Online; }
	inline float getBSINT(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? bsInTReplay : bsInTOnline; }
	inline unsigned getBSOUTRR1(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? bsOutRr1Replay : bsOutRr1Online; }
	inline float getBSOUTT(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? bsOutTReplay : bsOutTOnline; }
	
protected:
	// Last known value of CM.RR1 DPE for online mode
	unsigned	cmRr1Online;

	// Last known value of CM.RR1 DPE for replay mode
	unsigned	cmRr1Replay;

	// Last known value of BSIN.RR1 DPE for online mode
	unsigned	bsInRr1Online;

	// Last known value of BSIN.RR1 DPE for replay mode
	unsigned	bsInRr1Replay;

	// Last known value of BSOUT.RR1 DPE for online mode
	unsigned	bsOutRr1Online;

	// Last known value of BSOUT.RR1 DPE for replay mode
	unsigned	bsOutRr1Replay;

	// Last known value of CM.T DPE for online mode
	float	cmTOnline;

	// Last known value of CM.T DPE for replay mode
	float	cmTReplay;

	// Last known value of BSIN.T DPE for online mode
	float	bsInTOnline;

	// Last known value of BSIN.T DPE for replay mode
	float	bsInTReplay;

	// Last known value of BSOUT.T DPE for online mode
	float	bsOutTOnline;

	// Last known value of BSOUT.T DPE for replay mode
	float	bsOutTReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, float cmT, float bsT, QColor &color);
	virtual void getMainStateString(unsigned cmRr1, unsigned bsRr1, float cmT, float bsT, QString &string);
	virtual void getMainValueString(unsigned cmRr1, unsigned bsRr1, float cmT, float bsT, QString &string);
	virtual void getMainStateStringForMenu(unsigned cmRr1, unsigned bsRr1, float cmT, float bsT, QString &string);
	virtual void getToolTipString(unsigned cmRr1, unsigned bsRr1, float cmT, float bsT, QString &string);
#endif
};


#endif	// EQPCRYO_TT_SUM_H
