//	Implementation of MobileHistoryQuery class
/////////////////////////////////////////////////////////////////////////////////

#include "MobileHistoryQuery.h"

MobileHistoryQuery	MobileHistoryQuery::instance;

MobileHistoryQuery &MobileHistoryQuery::getInstance(void) { return instance; }

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

MobileHistoryQuery::MobileHistoryQuery()
{
	requestPending = signalSent = false;
}

MobileHistoryQuery::~MobileHistoryQuery()
{
}

/*
**	FUNCTION
**		Add query
**
**	ARGUMENTS
**		start	- Start of required time range
**		end		- End of required time range
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryQuery::add(const QDateTime &start, const QDateTime &end)
{
	mutex.lock();
	startTime = start;
	endTime = end;
	requestPending = true;
	mutex.unlock();
	if(!signalSent)
	{
		signalSent = true;
		emit changed();
	}
}

/*
**	FUNCTION
**		Clear pending requests, normally method is called when
**		mobile history dialog is closed
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryQuery::clear(void)
{
	mutex.lock();
	requestPending = signalSent = false;
	mutex.unlock();
}

/*
**	FUNCTION
**		Get parameters of pending request (if any)
**
**	ARGUMENTS
**		start	- Variable where start time of pending request will be written
**		end		- Variable where end time of pending request will be written
**
**	RETURNS
**		true	- if pending request has been found;
**		false	- if no pending requests
**
**	CAUTIONS
**		None
*/
bool MobileHistoryQuery::getNext(QDateTime &start, QDateTime &end)
{
	mutex.lock();
	bool result = requestPending;
	if(result)
	{
		start = startTime;
		end = endTime;
	}
	requestPending = signalSent = false;
	mutex.unlock();
	return result;
}

