//	Implementation of SynLhcView class
/////////////////////////////////////////////////////////////////////////////////

#include "SynLhcView.h"
#include "Eqp.h"
#include "EqpMsgCriteria.h"

#include "SynLhcLineIsol.h"
#include "SynLhcLinePassive.h"
#include "SynLhcLineBeam.h"
#include "SynLhcLineBeamCommon.h"

#include "VacMainView.h"

#include "DataPool.h"
#include "Sector.h"
#include "DebugCtl.h"

#include <QPainter>
#include <QPaintEvent>

#include <QDateTime>
#include <QDir>

#define max(a,b) ((a) > (b) ? (a) : (b))

/*
**	FUNCTION
**		Create new instance of LHC synoptic view
**
**	PARAMETERS
**		parent			- Pointer to parent widget
**		pFirstSector	- Pointer to first sector
**		pLastSector		- Pointer to last sector
**		mode			- Data acquisition mode for synoptic
**
**	RETURNS
**		Pointer to new instance of synoptic view,
**		NULL in case of error.
**
**	CAUTIONS
**		None
*/
SynLhcView *SynLhcView::create(QWidget *parent, Sector *pFirstSector, Sector *pLastSector,
	DataEnum::DataMode mode, bool hideMagnets)
{
	float	start = pFirstSector->getStart(),
			end = pLastSector->getEnd();
	if(start == end)
	{
		return NULL;
	}

	// Build new instance
	SynLhcView	*pView = new SynLhcView(parent);
	pView->pStartSector = pFirstSector;
	pView->pEndSector = pLastSector;
	pView->start = start;
	pView->end = end;
	pView->mode = mode;
	pView->build(hideMagnets);

	return pView;
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

SynLhcView::SynLhcView(QWidget *parent, Qt::WindowFlags f) :
	SynopticWidget(parent, f)
{
	pStartSector = NULL;
	pEndSector = NULL;
	totalWidth = totalHeight = viewStart = 0;
	vacTypeMask = 0;

	// [VACCO-948] [VACCO-1645] Connecting the synoptic widget to event of SectorClicked
	QObject::connect(this, SIGNAL(mouseDown(int, int, int, int, int, int, int)),
		this, SLOT(synSectorClicked(int, int, int, int, int, int, int)));

	// [VACCO-929] Connecting the synoptic widget to the main view in case the user clicks on a sector
	VacMainView *pMainView = VacMainView::getInstance();
	if (pMainView)
	{
		QObject::connect(this, SIGNAL(synSectorMouseDown(int, int, int, int, const char *, bool)),
			pMainView, SIGNAL(synSectorMouseDown(int, int, int, int, const char *, bool)));
	}
}

SynLhcView::~SynLhcView()
{
	while(!lines.isEmpty())
	{
		disconnectSectorSelection(lines.first());
		delete lines.takeFirst();
	}

}

/*
**	FUNCTION
**		Build all lines in synoptic view
**
**	PARAMETERS
**		hideMagnets	- true if magnets shall be hidden
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::build(bool hideMagnets)
{
	lines.append(new SynLhcLineIsol(this, SynLhcLine::Qrl, start, end));
	lines.append(new SynLhcLineIsol(this, SynLhcLine::Cryo, start, end));
	lines.append(new SynLhcLinePassive(this, SynLhcLine::Pas, start, end));
	lines.append(new SynLhcLineBeam(this, SynLhcLine::OuterBeam, start, end));
	lines.append(new SynLhcLineBeam(this, SynLhcLine::CrossBeam, start, end));
	lines.append(new SynLhcLineBeamCommon(this, SynLhcLine::CommonBeam, start, end));
	lines.append(new SynLhcLineBeam(this, SynLhcLine::InnerBeam, start, end));
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->setStartSector(pStartSector);
		pLine->setEndSector(pEndSector);
		pLine->setMode(mode);
		pLine->setHideMagnets(hideMagnets);
		pLine->connectToMobile();
		pLine->connectToWirelessMobile();
		connect(pLine, SIGNAL(mobileStateChanged()), this, SIGNAL(mobileStateChanged()));
	}
}

bool SynLhcView::findDevicePosition(Eqp *pEqp, int &eqpPosition, bool &eqpVisible)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		//qDebug("SynLhcView::findDevicePosition(%s): check line %d / %d\n", pEqp->getDpName(), idx, lines.count());
		SynLhcLine *pLine = lines.at(idx);
		if(pLine->findDevicePosition(pEqp, eqpPosition, eqpVisible))
		{
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Analyze which equipment types can be shown in view
**
**	PARAMETERS
**		allVacTypeMask	- OR'ed bit mask for all vacuum types
**		allCryoEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**							to be shown in this view
**		allBeamEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**							to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allCryoEqpMask,
		VacEqpTypeMask &allBeamEqpMask)
{
	allVacTypeMask = 0x0;
	allCryoEqpMask.clear();
	allBeamEqpMask.clear();
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->analyzeTypes(allVacTypeMask, allCryoEqpMask, allBeamEqpMask);
	}
}

/*
**	FUNCTION
**		Apply new masks to the view
**
**	PARAMETERS
**		allVacTypeMask	- OR'ed bit mask for all vacuum types
**		allCryoEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**							to be shown in this view
**		allBeamEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**							to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::applyMask(unsigned vacTypeMask, VacEqpTypeMask &cryoEqpMask, VacEqpTypeMask &beamEqpMask)
{
	buildView(vacTypeMask, cryoEqpMask, beamEqpMask);
	QSize mySize(totalWidth, totalHeight);
	setFixedSize(mySize);
	update();
}

/*
**	FUNCTION
**		Check if tooltip shall be shown for given mouse pointer position
**
**	PARAMETERS
**		point	- Mouse pointer position
**		text	- Variable where tool tip text shall be written
**		rect	- Variable where boundaries for returned tooltip text shall
**					be written
**
**	RETURNS
**		true	- If tooltip text shall be shown for given mouse position;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SynLhcView::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		if(pLine->getToolTip(point, text, rect))
		{
			return true;
		}
	}
	return false;
}


/*
**	FUNCTION
**		Rebuild geometries of all individual lines if necessary,
**		then build overall geometry of the whole view.
**
**	PARAMETERS
**		vacTypeMask	- OR'ed bit mask for all vacuum types
**		cryoEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**						to be shown in this view
**		beamEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**						to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::buildView(unsigned vacTypeMask, VacEqpTypeMask &cryoEqpMask,
		VacEqpTypeMask &beamEqpMask)
{
	// Check if new device lists shall be built
	bool needToRebuild = false;
	SynLhcLine *pLine;
	int idx;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		switch(pLine->getType())
		{
		case SynLhcLine::Qrl:
		case SynLhcLine::Cryo:
			needToRebuild = pLine->isRebuildRequired(vacTypeMask, cryoEqpMask);
			break;
		default:
			needToRebuild = pLine->isRebuildRequired(vacTypeMask, beamEqpMask);
			break;
		}
		if(needToRebuild)
		{
			break;
		}
	}
	if (redrawRequiredForMobile) {
		needToRebuild = true;
	}
	if(!needToRebuild) {
		return;
	}
	redrawRequiredForMobile = false;
	this->cryoEqpMask = cryoEqpMask;
	this->beamEqpMask = beamEqpMask;
	this->vacTypeMask = vacTypeMask;
	totalWidth = totalHeight = 0;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		switch(pLine->getType())
		{
		case SynLhcLine::Qrl:
		case SynLhcLine::Cryo:
			pLine->build(vacTypeMask, cryoEqpMask);
			break;
		default:
			pLine->build(vacTypeMask, beamEqpMask);
			break;
		}
	}

	if(DebugCtl::isSynoptic())
	{
		QString dumpFileName = QDir::tempPath();
		#ifdef Q_OS_WIN
			dumpFileName += "\\SynLhcPass1.txt";
		#else
			dumpFileName += "/SynLhcPass1.txt";
		#endif
		dump(dumpFileName.toLatin1().constData());
	}
	buildGeometry();
	if(DebugCtl::isSynoptic())
	{
		QString dumpFileName = QDir::tempPath();
		#ifdef Q_OS_WIN
			dumpFileName += "\\SynLhcPass3.txt";
		#else
			dumpFileName += "/SynLhcPass3.txt";
		#endif
		dump(dumpFileName.toLatin1().constData());
	}
}

/*
**	FUNCTION
**		Build overall geometry of the whole view.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::buildGeometry(void)
{
	buildLinesGeometry();
	finishLineParts();
	calcVerticalPos();
	// Supply necessary parameters for common vacuum beam line
	SynLhcLine *pLine = findLineOfType(SynLhcLine::CommonBeam);
	if(pLine)
	{
		SynLhcLine *pSrcLine = findLineOfType(SynLhcLine::OuterBeam);
		pLine->copyEqp(pSrcLine, true);
		pSrcLine = findLineOfType(SynLhcLine::InnerBeam);
		pLine->copyEqp(pSrcLine, false);
	}

	// Finally move all active icons to calculated positions
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		pLine->finishGeometry();
		pLine->setSectorGeometry(); // [VACCO-929] Setting the start and end coordinates for every sector
		connectSectorSelection(pLine); // [VACCO-929] Setting connections between SynLhcView and the sectors in this synoptic,
									   //	so that whenever a sector changes state, the SynLhcView widget is updated.


	}
}


/*
**	FUNCTION
**		[VACCO-929]	[VACCO-948] [VACCO-1645]
**		Connects the SynLhcView widget to the sectors of the lines and childlines in the synoptic
**
**	PARAMETERS
**		Pointer to the a SynLhcLine
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::connectSectorSelection(SynLhcLine * pLine){
	for (int idx2 = 0; idx2 < pLine->getSectorLabelList().count(); idx2++){
		Sector *pSector = pLine->getSectorLabelList().at(idx2)->getSector();
		QObject::connect(pSector, SIGNAL(synSectSelectChanged(Sector *)),
			this, SLOT(forceRedraw()));
		QObject::connect(pSector, SIGNAL(synSectStateChanged(Sector *)), // [VACCO-948] [VACCO-1645]
			this, SLOT(forceRedraw()));
	}

	// [VACCO-929] Setting connections between SynLhcView and the sectors in this synoptic lines' childlines,
	//				this applies for SynLhcLineBeam and the childlines SynLhcInjDumpLine
	QList<SynLhcInjDumpLine *> pChildLines = pLine->getChildLines();
	if (pChildLines.isEmpty()) return;

	for (int idx2 = 0; idx2 < pChildLines.count(); idx2++){
		SynLhcInjDumpLine *pChildLine = pChildLines.at(idx2);
		for (int idx3 = 0; idx3 < pChildLine->getSectorLabelList().count(); idx3++){
			Sector *pSector = pChildLine->getSectorLabelList().at(idx3)->getSector();
			QObject::connect(pSector, SIGNAL(synSectSelectChanged(Sector *)),
				this, SLOT(forceRedraw()));
			QObject::connect(pSector, SIGNAL(synSectStateChanged(Sector *)), // [VACCO-948] [VACCO-1645]
				this, SLOT(forceRedraw()));
		}
	}
	return;
}


/*
**	FUNCTION
**		[VACCO-929] [VACCO-948] [VACCO-1645]
**		Disconnects the SynLhcView widget to the sectors of the lines and childlines in the synoptic
**
**	PARAMETERS
**		Pointer to the a SynLhcLine
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::disconnectSectorSelection(SynLhcLine * pLine){
	for (int idx2 = 0; idx2 < pLine->getSectorLabelList().count(); idx2++){
		Sector *pSector = pLine->getSectorLabelList().at(idx2)->getSector();
		QObject::disconnect(pSector, SIGNAL(synSectSelectChanged(Sector *)),
			this, SLOT(forceRedraw()));
		QObject::disconnect(pSector, SIGNAL(synSectStateChanged(Sector *)), // [VACCO-948] [VACCO-1645]
			this, SLOT(forceRedraw()));
	}

	// [VACCO-929] Setting disconnections between SynLhcView and the sectors in this synoptic lines' childlines,
	//				this applies for SynLhcLineBeam and the childlines SynLhcInjDumpLine
	QList<SynLhcInjDumpLine *> pChildLines = pLine->getChildLines();
	if (pChildLines.isEmpty()) return;

	for (int idx2 = 0; idx2 < pChildLines.count(); idx2++){
		SynLhcInjDumpLine *pChildLine = pChildLines.at(idx2);
		for (int idx3 = 0; idx3 < pChildLine->getSectorLabelList().count(); idx3++){
			Sector *pSector = pChildLine->getSectorLabelList().at(idx3)->getSector();
			QObject::disconnect(pSector, SIGNAL(synSectSelectChanged(Sector *)),
				this, SLOT(forceRedraw()));
			QObject::disconnect(pSector, SIGNAL(synSectStateChanged(Sector *)), // [VACCO-948] [VACCO-1645]
				this, SLOT(forceRedraw()));
		}
	}
	return;
}


/*
**	FUNCTION
**		Find line of given type
**
**	PARAMETERS
**		type	- Line type to find
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
SynLhcLine *SynLhcView::findLineOfType(int type)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		if(pLine->getType() == type)
		{
			return pLine;
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Build geometry of all individual lines in view
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::buildLinesGeometry(void)
{
	SynLhcLine *pLine;
	int idx;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		pLine->init();
	}

	float coord = -DataPool::getInstance().getLhcRingLength();
	float endCoord = coord;
	int	connPos = LEFT_MARGIN, endConnPos = LEFT_MARGIN, order = 1;

	// Process all lines
	pLine = NULL;
	while((pLine = nextLine(pLine)))
	{
		pLine->placeItem(order++, coord, connPos, endCoord, endConnPos);
		for(int otherIdx = 0 ; otherIdx < lines.count() ; otherIdx++)
		{
			SynLhcLine *pOther = lines.at(otherIdx);
			if(pOther != pLine)
			{
				pOther->correctLastPosition(coord, connPos, order);
			}
		}
	}

	// Set flags indicating if start/end of sector shall be drawn or not
	// Add another sector (empty) at the end of synoptic if necessary
	// Note that lines shall be processed in REVERSE order
	for(idx = lines.count() - 1 ; idx >= 0 ; idx--)
	{
		pLine = lines.at(idx);
		pLine->decideForSectorBorders();
	}
}

/*
**	FUNCTION
**		Finish geometry for all individual lines in view
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::finishLineParts(void)
{
	totalWidth = 0;

	// Find maximum coordinate on all line types
	SynLhcLine *pLine;
	int idx;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(pLine->getEndX() > totalWidth)
		{
			totalWidth = pLine->getEndX();
		}
	}

	// Assign end coordinate to unfinished lines
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		pLine->finishPartsSectors(totalWidth);
	}
	totalWidth += RIGHT_MARGIN;
}

/*
**	FUNCTION
**		Calculate vertical positions for all lines
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::calcVerticalPos(void)
{
	SynLhcLine *pLine;
	int idx;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		pLine->addAlarmIcons();	// L.Kopylov 20.11.2013
		pLine->calcVerticalLimits();
	}
	if(DebugCtl::isSynoptic())
	{
		QString dumpFileName = QDir::tempPath();
		#ifdef Q_OS_WIN
			dumpFileName += "\\SynLhcPass2.txt";
		#else
			dumpFileName += "/SynLhcPass2.txt";
		#endif
		dump(dumpFileName.toLatin1().constData());
	}

	int delta = TOP_MARGIN;
	pLine = findLineOfType(SynLhcLine::Qrl);
	pLine->moveVertically(delta);
	delta += QRL_CRYO_LINE_OFFSET;
	pLine = findLineOfType(SynLhcLine::Cryo);
	pLine->moveVertically(delta);
	delta += LINE_START_END_HEIGHT * 2;
	pLine = findLineOfType(SynLhcLine::OuterBeam);
	pLine->moveVertically(delta);
	pLine = findLineOfType(SynLhcLine::Pas);
	pLine->moveVertically(delta);
	pLine = findLineOfType(SynLhcLine::CrossBeam);
	pLine->moveVertically(delta);
	delta -= pLine->getDescent();
	delta += 2;
	pLine = findLineOfType(SynLhcLine::InnerBeam);
	pLine->moveVertically(delta);
	SynLhcLine *pInnerBeamLine = findLineOfType(SynLhcLine::InnerBeam),
				*pCrossBeamLine = findLineOfType(SynLhcLine::CrossBeam);
	totalHeight = BOTTOM_MARGIN + max(
		pInnerBeamLine->getY() + pInnerBeamLine->getDescent(),
		pCrossBeamLine->getY() + pCrossBeamLine->getDescent());


	// Write position of cross line to outer and inner beam lines
	pInnerBeamLine->setCrossLineY(pCrossBeamLine->getY());
	pCrossBeamLine->setCrossLineY(pCrossBeamLine->getY());
	pLine = findLineOfType(SynLhcLine::OuterBeam);
	pLine->setCrossLineY(pCrossBeamLine->getY());
}

/*
**	FUNCTION
**		Find next line to be processed for elements positionnig
**
**	PARAMETERS
**		pPrevLine	- Pointer to previously found line
**
**	RETURNS
**		Pointer to next line to be processed, or
**		NULL if no more lines to process
**
**	CAUTIONS
**		None
*/
SynLhcLine *SynLhcView::nextLine(SynLhcLine *pPrevLine)
{
	SynLhcLine	*pBestLine = NULL;
	float		bestCoord = HUGE_COORDINATE;

	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		float coord = pLine->nextPosition();
		int result = compareCoordInView(bestCoord, coord);
		switch(result)
		{
		case 0:	// new coordinate is the same as old one
			// Make sure that equipment in another line with the same
			// coordinate will be processed BEFORE equipment in the
			// same line having the same coordinate. This is the way
			// to ensure equipment with the same coordinates on different
			// lines will use the same bar index
			if((pBestLine == pPrevLine) && (coord < HUGE_COORDINATE))
			{
				bestCoord = coord;
				pBestLine = pLine;
			}
			break;
		case -1:	// New coordinate is before the old one
			bestCoord = coord;
			pBestLine = pLine;
			break;
		}
	}
	return pBestLine;
}

/*
**	FUNCTION
**		Compare two coordinates within the view.
**		especially if range coordinates for view includes start of
**		ring.
**
**	PARAMETERS
**		bestCoord	- Previously found best coordinate
**		newCoord	- New coordinate to be compared with best coordinate
**
**	RETURNS
**		-1	- newCoord is before bestCoord;
**		0	- newCoord is the same as bestCoord;
**		1	- newCoord is after bestCoord;
**
**	CAUTIONS
**		It is expected that start != end
**
*/
int SynLhcView::compareCoordInView(float bestCoord, float newCoord)
{
	if(newCoord == bestCoord)
	{
		return 0;
	}
	if(newCoord == HUGE_COORDINATE)
	{
		return 1;
	}
	if(bestCoord == HUGE_COORDINATE)
	{
		return -1;
	}

	if(newCoord < bestCoord)
	{
		return -1;
	}

	/* L.Kopylov 06.12.2011
	** Coordinates are always increasing, even if view includes IP1. So, let's
	** try simple criteria above
	if(start < end)	// View range does not include start of ring
	{
		if(newCoord < bestCoord)
		{
			return -1;
		}
	}
	else	// View range includes start of ring
	{
		if(newCoord >= start)	// New coordinate is still before ring start
		{
			if(bestCoord <= end)	// Best coordinate is already after ring start
			{
				return -1;
			}
			else	// Best coordinate is still before ring start
			{
				if(newCoord < bestCoord)
				{
					return -1;
				}
			}
		}
		else	// New coordinate is already after ring start
		{
			if(bestCoord <= start)	// Best coordinate is also after ring start
			{
				if(newCoord < bestCoord)
				{
					return -1;
				}
			}
		}
	}
	*/
	return 1;
}

/*
**	FUNCTION
**		Override Widget's paintEvent() method: draw all background lines
**
**	PARAMETERS
**		pEvent	- Pointer to event data
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::paintEvent(QPaintEvent *pEvent)
{
	const QRect &rect = pEvent->rect();
	if(rect.isNull())
	{
		return;
	}
	QPainter painter(this);
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->draw(painter, rect.left(), rect.right());
	}
}

/*
**	FUNCTION
**		Set start position and width of view: activate icons which are
**		visible, deactivate icons which are not visible
**
**	PARAMETERS
**		viewStart	- Start position of visible part
**		viewWidth	- width of visible part
**		dragging	- true if this method is called from scroll bar dragging
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::setViewStart(int viewStart, int viewWidth, bool dragging)
{
	this->viewStart = viewStart;
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->setViewStart(viewStart, viewWidth, dragging);
	}
}

void SynLhcView::setAlwaysIncludeSectorBorder(bool value) {
	for (int idx = 0; idx < lines.count(); idx++) {
		SynLhcLine *pLine = lines.at(idx);
		pLine->setAlwaysIncludeSectorBorder(value);
	}

}

/*
**	FUNCTION
**		Write all parameters to output file for debugging .
**
**	PARAMETERS
**		fileName	- Name of output file
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::dump(const char *fileName)
{
	if(!DebugCtl::isSynoptic())
	{
		return;
	}
	FILE	*pFile;
#ifdef Q_OS_WIN
	if(fopen_s(&pFile, fileName, "w"))
	{
		return;
	}
#else
	if(!(pFile = fopen(fileName, "w")))
	{
		return;
	}
#endif
	fprintf(pFile, "LHC from %9.3f (%s) to %9.3f (%s)\n",
		start, pStartSector->getName(),
		end, pEndSector->getName());
	fprintf(pFile, "  vacTypeMask %08X\n", vacTypeMask);
	fprintf(pFile, "  cryoEqpMask:\n");
	cryoEqpMask.dump(pFile);
	fprintf(pFile, "  beamEqpMask:\n");
	beamEqpMask.dump(pFile);
	fprintf( pFile, "  total: width %d height %d\n", totalWidth, totalHeight);
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->dump(pFile);
	}
	fclose(pFile);
}


/*
**	FUNCTION
**		[VACCO-929]
**		QWidget event that occurs whenever a mouse button is pressed in a widget.
**
**	PARAMETERS
**		pEvent	- contains parameters that describe a mouse event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::mousePressEvent(QMouseEvent *pEvent)
{
	QPoint screenPoint = pEvent->pos();
	int ewoMouseX = screenPoint.x();
	int ewoMouseY = screenPoint.y();
	screenPoint = pEvent->globalPos();
	int globalMouseX = screenPoint.x();
	int globalMouseY = screenPoint.y();
	int buttonNbr = 0;

	switch (pEvent->button()){
	case Qt::LeftButton:
		buttonNbr = 1;
		break;
	case Qt::RightButton:
		buttonNbr = 2;
		break;
	default:
		break;
	}

	emit mouseDown(buttonNbr, mode, ewoMouseX, ewoMouseY, globalMouseX, globalMouseY, 0);
	return;
}


/*
**	FUNCTION
**		[VACCO-929]	[VACCO-948] [VACCO-1645]
**		Decides which action to do, in case the user pressed a mouse button on a sector
**
**	ARGUMENTS
**		button		- specifies wich mouse button was pressed
**		mode		- 
**		x, y		- event coordinates
**		extra		-
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
bool SynLhcView::synSectorClicked(int button, int mode, int localX, int localY, int globalX, int globalY, int extra)
{
	SynLhcSectorLabel *sectorSelected = getSectorFromXY(localX, localY);
	if (sectorSelected == NULL) return false;
	bool selection = sectorSelected->getSector()->isSelected();
	
	switch (button){
		case Qt::LeftButton: {
			selection = !selection;
			break;
		}
		case Qt::RightButton: {
			selection = true;
			break;
		}
		default:
			break;
	}

	sectorSelected->getSector()->setSelected(selection, false);
	emit synSectorMouseDown(button, mode, globalX, globalY, sectorSelected->getSector()->getPvssName(), sectorSelected->getSector()->isSelected());
	return true;
}


/*
**	FUNCTION
**		[VACCO-929]
**		Checks if there is any sector in the region of the coordinates XY
**
**	ARGUMENTS
**		mouseX, mouseY		- panel coordinates
**
**	RETURNS
**		Pointer to the SynLhcSectorLabel in the region of the coords, if any
**
**	CAUTIONS
**		None
*/
SynLhcSectorLabel* SynLhcView::getSectorFromXY(int mouseX, int mouseY)
{
	SynLhcSectorLabel *sectorSelected = NULL;
	for (int idx = 0; idx < lines.count(); idx++){
		SynLhcLine *pLine = lines.at(idx);
		sectorSelected = pLine->getSectorFromXY(mouseX, mouseY);
		if (sectorSelected != NULL) break;
	}
	return sectorSelected;
}


/*
**	FUNCTION
**		[VACCO-929]
**		Force redraw of this widget
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcView::forceRedraw()
{
	update();
}
/**
@brief PUBLIC FUNCTION Show mobiles not connected in all lines
*/
void SynLhcView::showMobilesNotConnected(void) {
	for (int idx = 0; idx < lines.count(); idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->showMobilesNotConnectedInLine();
	}
	// to force redraw:
	redrawRequiredForMobile = true;
	return;
}

/**
@brief PUBLIC FUNCTION Hide mobiles not connected in all lines
*/
void SynLhcView::hideMobilesNotConnected(void) {
	for (int idx = 0; idx < lines.count(); idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->hideMobilesNotConnectedInLine();
	}
	// to force redraw:
	redrawRequiredForMobile = true;
	return;
}