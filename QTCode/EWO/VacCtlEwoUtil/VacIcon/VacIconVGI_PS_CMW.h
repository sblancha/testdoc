#ifndef	VACICONVGI_PS_CMW_H
#define	VACICONVGI_PS_CMW_H

//	Icon for VGI

#include "VacIconVG_PS_CMW.h"

class VACCTLEWOUTIL_EXPORT VacIconVGI_PS_CMW : public VacIconVG_PS_CMW
{
public:
	VacIconVGI_PS_CMW(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG_PS_CMW(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGI_PS_CMW_H
