#ifndef EQPVPNMUX_H
#define EQPVPNMUX_H

//	VPNMUX - multiplexor for VP_NEGs

#include "Eqp.h"

class EqpVP_NEG;

class VACCTLEQPDATA_EXPORT EqpVPNMUX : public Eqp
{
	Q_OBJECT

public:
	EqpVPNMUX(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVPNMUX(const EqpVPNMUX &src) : Eqp(src) {}
#endif
	~EqpVPNMUX();

	virtual void addSlave(EqpVP_NEG *pSlave) { slaveEqps.append(pSlave); }

	virtual void getSlaves(QStringList &slaves);
	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::CurrentVRPM; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode mode);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Method to be called by VPI: VPI supplies pressure value
	virtual void checkCriteria(EqpMsgCriteria *pCriteria, float pr);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline unsigned getRR2(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr2Replay : rr2Online; }
	virtual int getErrorCode(DataEnum::DataMode mode);

	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// List of slave pumps
	QList<EqpVP_NEG *> slaveEqps;

	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for online mode
	unsigned	rr2Online;

	// Last known value of RR2 DPE for replay mode
	unsigned	rr2Replay;

	// Last known value for STATE.I_Value DPE for online mode
	float		currentOnline;

	// Last known value for STATE.I_Value DPE for replay mode
	float		currentReplay;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, unsigned rr2, float current, bool plcAlarm, QString &string);
#endif

	virtual bool isValueValid(bool plcAlarm, unsigned rr1);

};

#endif	// EQPVPNMUX_H
