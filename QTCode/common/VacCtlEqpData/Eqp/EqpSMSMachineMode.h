#ifndef	EQPSMSMACHINEMODE_H
#define	EQPSMSMACHINEMODE_H

// Machine mode for SMS notification

#include "Eqp.h"


class VACCTLEQPDATA_EXPORT EqpSMSMachineMode : public Eqp
{
	Q_OBJECT

public:
	EqpSMSMachineMode(const char *dpName, const char *visibleName, const char *dpeName);
#ifdef Q_OS_WIN
	EqpSMSMachineMode(const EqpSMSMachineMode &src) : Eqp(src) {}
#endif
	~EqpSMSMachineMode();

	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::Intensity; }
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode /* mode */) { return true; }
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);

	virtual void getDpesForDevList(QStringList &dpes);
	virtual void convertValueToString(float value, QString &result, EqpPart part);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline unsigned getMode(DataEnum::DataMode /* mode */) { return modeOnline; }
	
protected:
	// Last known value of machine mode DPE for online mode
	unsigned	modeOnline;

	QByteArray	dpeName;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainValueString(unsigned modeValue, QString &string);
	virtual void getMainStateStringForMenu(unsigned modeValue, QString &string);
#endif
};

#endif	// EQPSMSMACHINEMODE_H
