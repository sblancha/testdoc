//	Implementation of ProfileLhcLineBeam class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileLhcLineBeam.h"
#include "ProfileLhcView.h"
#include "VacLinePart.h"

#include "VerticalAxis.h"

#include "Eqp.h"
#include "EqpType.h"

#include "DataPool.h"

/*
**	FUNCTION
**		Assign bar index to current device in profile given
**		by curEqpIdx
**
**	PARAMETERS
**		coord	- Last processed physical location, value can be
**					changed by this method
**		barIdx	- Last used bar index, value can be changed by this
**					method.
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineBeam::placeItem(float &coord, int &barIdx)
{
	if(curEqpIdx >= eqps.count())
	{
		return;		// Can not happen, just to make sure
	}
	ProfileLhcLineItem *pItem = eqps.at(curEqpIdx);
	if(pItem->isHidden())	// LIK 24.06.2009 && (!pItem->isAtSectorBorder()))
	{
		pItem->setBarIdx(barIdx < 0 ? 0 : barIdx);
		curEqpIdx++;
		return;
	}

	// Make sure bar numbers increase with coordinate
	if(pItem->getStart() > coord)
	{
		if(coord >= 0.0)
		{
			barIdx++;
		}
		coord = pItem->getStart();
	}

	// Make sure bar does not overlap with prevfious visible bar
	if(barIdx < 0)
	{
		barIdx = 0;
	}
	else
	{
		for(int idx = curEqpIdx - 1 ; idx >= 0 ; idx--)
		{
			ProfileLhcLineItem *pPrev = eqps.at(idx);
			if(!pPrev->shallBeVisible())
			{
				continue;
			}
			if(pPrev->isHidden() && (!pPrev->isAtSectorBorder()))
			{
				continue;
			}
			if(barIdx <= pPrev->getBarIdx())
			{
				barIdx = pPrev->getBarIdx() + 1;
			}
			break;
		}
	}
	// Assign bar number, move to next equipment index
	pItem->setBarIdx(barIdx);
	curEqpIdx++;
}

/*
**	FUNCTION
**		Build geometry of all sector areas be shown in profile
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineBeam::buildSectors(QFontMetrics &fontMetrics)
{
	sectors.clear();
	ProfileLineSector	sector;

	sector.setLeftX(pLogAxis->getAxisLocation());
	sector.setRightX(sector.getLeftX());

	int n;
	for(n = 0 ; n < eqps.count() ; n++)
	{
		ProfileLhcLineItem *pItem = eqps.at(n);
		if(!pItem->isAtSectorBorder())
		{
			continue;
		}
		if(!n)
		{
			continue;
		}
		/* VV does not get 'individual' barIdx, instead it gets barIdx of last
		** device before it. LIK 24.06.2009
		sector.setRightX((pItem->getRect().left() + pItem->getRect().right()) >> 1);
		*/
		ProfileLhcLineItem *pNext = NULL;
		for(int i = n + 1 ; i < eqps.count() ; i++)
		{
			pNext = eqps.at(i);
			if(pNext->getBarIdx() > 0)
			{
				break;
			}
		}
		if(pNext)
		{
			sector.setRightX((pItem->getRect().right() + pNext->getRect().left()) >> 1);
		}
		else
		{
			sector.setRightX(pItem->getRect().right());
		}
		sector.setRightEqpIdx(n);
		if(sector.getLeftEqpIdx() < 0)
		{
			sector.setLeftEqpIdx(n);
		}
		sector.setSector1(pItem->getEqp()->getSectorBefore());
		addSector(&sector);
		sector.setLeftX(sector.getRightX());
		sector.setLeftEqpIdx(n);
	}
	if(sector.getRightX() != sector.getLeftX())
	{
		addSector(&sector);
	}

	if(sectors.isEmpty())	// No sector borders, take sector of any device
	{
		for(n = 0 ; n < eqps.count() ; n++)
		{
			ProfileLhcLineItem *pItem = eqps.at(n);
			if(pItem->getEqp()->getSectorBefore())
			{
				sector.setLeftEqpIdx(0);
				sector.setRightEqpIdx(eqps.count() - 1);
				sector.setSector1(pItem->getEqp()->getSectorBefore());
				addSector(&sector);
				break;
			}
		}
	}

	// Set borders for first and last sectors
	if(sectors.count())
	{
		ProfileLineSector *pLast = sectors.last();
		pLast->setRightX(parent->isHasLinearEqp() ? pLinAxis->getAxisLocation() : area.right());
	}
	axisMinLocation = calculateSectorLabels(fontMetrics);
}


/*
**	FUNCTION
**		Draw background of beam vacuum sector areas
**		Default drawing method using hatch brush to draw sector
**		background. Color of brush is determined by vacuum type,
**		type of hatch is changed between sectors
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineBeam::drawSectorsHatchFill(QPainter &painter)
{
	QRect	rect;
	rect.setTop(topLogY);
	rect.setBottom(area.bottom());

	// Draw even sectors
	int n;
	ProfileLineSector *pSect;
	for(n = 0 ; n < sectors.count() ; n++)
	{
		if(n & 0x1)
		{
			continue;
		}
		pSect = sectors.at(n);
		rect.setLeft(pSect->getLeftX());
		rect.setRight(pSect->getRightX());
		painter.fillRect(rect, sectorBackColor(pSect->getVacType()));
	}

	// Draw odd sectors
	for(n = 0 ; n < sectors.count() ; n++)
	{
		if(!(n & 0x1))
		{
			continue;
		}
		pSect = sectors.at(n);
		rect.setLeft(pSect->getLeftX());
		rect.setRight(pSect->getRightX());
		painter.fillRect(rect, sectorBackColor(pSect->getVacType()));
	}
}

/*
**	FUNCTION
**		Draw background of beam vacuum sector areas
**		Method using solid brush to draw sector
**		background and vertical line to draw border between sectors.
**		Color of brush is determined by vacuum type.
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcLineBeam::drawSectorsWithBorders(QPainter &painter)
{
	QRect	rect;
	rect.setTop(topLogY);
	rect.setBottom(area.bottom());

	QPen pen(Qt::black, 0, Qt::DotLine);
	painter.setPen(pen);

	// Draw all sectors
	for(int n = 0 ; n < sectors.count() ; n++)
	{
		ProfileLineSector *pSect = sectors.at(n);
		rect.setLeft(pSect->getLeftX() + 1);	// In order to have space for vertical line
		rect.setRight(pSect->getRightX());
		painter.fillRect(rect, sectorBackColor(pSect->getVacType()));
		painter.drawLine(rect.right(), rect.top(), rect.right(), rect.bottom());
	}
}

/*
**	FUNCTION
**		Check if given device shall appear in this profile line. Coordinate
**		of device is NOT checked.
**
**	PARAMETERS
**		pEqp	- Pointer to device in question
**		partIdx	- Index of line part, where device is located, will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
bool ProfileLhcLineBeam::isMyEqp(Eqp *pEqp, int &partIdx)
{
	if(pEqp->getType() != EqpType::VacDevice)
	{
		return false;
	}
	// Skip NotControl + NotConnected - NO, NotConnected can change on the fly
	switch(pEqp->getCtlStatus(mode))
	{
	case Eqp::NotControl:
		return false;
	default:
		break;
	}
	/* Add all equipment - even not active at the moment
	if(!pEqp->isActive(mode))
	{
		return false;	// Mobile equipment - not activated
	}
	*/

	// Controllable vacuum equipment is checked here
	if(!allowedEqpMask.contains(pEqp->getFunctionalType()))
	{
		if(!pEqp->isSectorBorder())
		{
			return false;
		}
	}

	// Check vacuum type and location
	float	eqpStart;
	if(start < end)
	{
		eqpStart = pEqp->getStart();
	}
	else
	{
		if(pEqp->getStart() >= start)
		{
			eqpStart = pEqp->getStart();
		}
		else
		{
			eqpStart = pEqp->getStart() + DataPool::getInstance().getLhcRingLength();
		}
	}
	for(partIdx = 0 ; partIdx < pPartList->count() ; partIdx++)
	{
		VacLinePart *pPart = pPartList->at(partIdx);
		if(pPart->getVacType() != pEqp->getVacType())
		{
			switch(pPart->getVacType())
			{
			case VacType::RedBeam:
			case VacType::BlueBeam:
			case VacType::CrossBeam:
			case VacType::CommonBeam:
				if((pEqp->getVacType() & VacType::CrossBeam) || (pEqp->getVacType() & VacType::CommonBeam))
				{
					break;
				}
				/* NOBREAK */
			default:
				continue;
			}
		}
		if((pPart->getStartPos() <= eqpStart) && (eqpStart <= pPart->getEndPos()))
		{
			// CRYO thermometers are sitting on several vacuums simultaneously, hence, they
			// require special processing
			if(pEqp->isCryoThermometer())
			{
				return isMyCryoT(pEqp);
			}
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Check if given CRYO thermometer shall appear in this profile line.
**		Problem with CRYO thermometers: they belong to several vacuum types, hence,
**		they can appear on beam line which is hidden. So we'll find LHC beam location
**		region where equipment is stitting and check if equipment on that region shall
**		be visible
**		L.Kopylov 21.04.2008
**
**	PARAMETERS
**		pEqp	- Pointer to device in question
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
bool ProfileLhcLineBeam::isMyCryoT(Eqp *pEqp)
{
	if(pEqp->isSkipOnSynoptic())
	{
		return false;
	}
	LhcRegion *pRegion = NULL;
	QList<LhcRegion *> &regList = DataPool::getInstance().getLhcBeamLocs();
	for(int idx = 0 ; idx < regList.count() ; idx++)
	{
		LhcRegion *pReg = regList.at(idx);
		if(pReg->getStart() < pReg->getEnd())	// 'normal' region
		{
			if((pReg->getStart() <= pEqp->getStart()) && (pEqp->getStart() <= pReg->getEnd()))
			{
				pRegion = pReg;
				break;
			}
		}
		else	// Region including IP1
		{
			if((pReg->getStart() <= pEqp->getStart()) || (pEqp->getStart() <= pReg->getEnd()))
			{
				pRegion = pReg;
				break;
			}
		}
	}
	if(!pRegion)	// Not possible, just protection
	{
		return true;
	}
	switch(pRegion->getType())
	{
	case LhcRegion::BlueOut:
		if((type == OuterBeam) && (!(vacTypeMask & VacType::BlueBeam)))
		{
			return false;
		}
		if((type == InnerBeam) && (!(vacTypeMask & VacType::RedBeam)))
		{
			return false;
		}
		break;
	case LhcRegion::RedOut:
		if((type == OuterBeam) && (!(vacTypeMask & VacType::RedBeam)))
		{
			return false;
		}
		if((type == InnerBeam) && (!(vacTypeMask & VacType::BlueBeam)))
		{
			return false;
		}
		break;
	case LhcRegion::Cross:
		if(!(vacTypeMask & (VacType::BlueBeam | VacType::RedBeam)))
		{
			return false;
		}
		break;
	}
	return true;
}
