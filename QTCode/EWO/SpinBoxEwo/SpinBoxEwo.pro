# Project file for qmake utility to build SpinBoxEwo EWO
#
#	03.11.2010	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = SpinBoxEwo

HEADERS = SpinBoxEwo.h

SOURCES = SpinBoxEwo.cpp

