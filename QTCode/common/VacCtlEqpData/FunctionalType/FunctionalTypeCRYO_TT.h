#ifndef FUNCTIONALTYPECRYO_TT_H
#define	FUNCTIONALTYPECRYO_TT_H

// Special functional type for CRYO_TT - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeCRYO_TT : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Valid = 1
	};

	// Subtypes for value limits - different limits can be given to
	// thermometers with different 'nominal' temperature
	typedef enum
	{
		NominalTemp_2 = 2,
		NominalTemp_4 = 4
	} NominalTemp;

	FunctionalTypeCRYO_TT(int type, const char *name, const char *description);
	virtual ~FunctionalTypeCRYO_TT() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPECRYO_TT_H
