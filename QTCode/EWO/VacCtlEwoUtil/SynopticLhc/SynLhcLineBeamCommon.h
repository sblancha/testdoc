#ifndef SYNLHCLINEBEAMCOMMON_H
#define	SYNLHCLINEBEAMCOMMON_H

// Synoptic line showing devices on .C vacuum

#include "SynLhcLineBeam.h"

class SynLhcLineBeamCommon : public SynLhcLineBeam  
{
public:
	SynLhcLineBeamCommon(QWidget *parent, int type, float start, float end);
	virtual ~SynLhcLineBeamCommon();

	virtual void analyzeTypes(unsigned & /* allVacTypeMask */, VacEqpTypeMask & /* allCryoEqpMask */, VacEqpTypeMask & /* allBeamEqpMask */) {}
	virtual void AnalyzeTypes(unsigned & /* allVacTypeMask */, VacEqpTypeMask & /* allEqpMask */) {}

	virtual void copyEqp(SynLhcLine *pSrcLine, bool isUpperLine);

protected:
	// List of common volume coordinates
	QList<QRect *>	volumes;

	virtual LhcLineParts::BuildType linePartMethod(void) { return LhcLineParts::TypeNone; }

	virtual bool isMyEqp(Eqp * /* pEqp */, int & /*partIdx */) { return false; }
	virtual bool isMySector(Sector * /* pSect */) { return false; }
	virtual void buildCommonVolumes(void);
	virtual void drawParts(QPainter &painter, int viewStart, int viewEnd);
};

#endif	//SYNLHCLINEBEAMCOMMON_H
