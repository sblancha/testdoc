#ifndef FUNCTIONALTYPEVRJ_TC_H
#define	FUNCTIONALTYPEVRJ_TC_H

// Special functional type for thermocouples remote IO - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVRJ_TC : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Off = 1,
		On = 2
	};

	FunctionalTypeVRJ_TC(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVRJ_TC() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);
};

#endif	// FUNCTIONALTYPEVRJ_TC_H
