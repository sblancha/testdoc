#ifndef RACKICONHIDDEN_H
#define RACKICONHIDDEN_H

#include "RackIcon.h"

class Eqp;

class VACCTLEWOUTIL_EXPORT RackIconHidden : public RackIcon
{
	Q_OBJECT
public:
	RackIconHidden(QWidget * parent, Qt::WindowFlags f = 0);
	virtual ~RackIconHidden();

	virtual void setEqp(Eqp * pEqp);
	virtual bool connect(void);
	virtual bool disconnect(void);
protected:
	Eqp * pEqp; //pointer to equipment, if it exists
	virtual void paintEvent(QPaintEvent *pEvent);

};

#endif
