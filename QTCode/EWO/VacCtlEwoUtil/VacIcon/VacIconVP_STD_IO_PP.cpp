//	Implementation of VacIconVP_STD_IO_PP class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVP_STD_IO_PP.h"

#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVP_STD_IO_PP::VacIconVP_STD_IO_PP(QWidget *parent, Qt::WindowFlags f) :
	VacIconVP_STD_IO(parent, f)
{
}

VacIconVP_STD_IO_PP::~VacIconVP_STD_IO_PP()
{
}

void VacIconVP_STD_IO_PP::drawType(QPainter &painter)
{
	painter.setPen(Qt::black);
	painter.drawArc(-8, -8, 17, 17, 0, 5760);
	painter.drawArc(-5, -5, 11, 11, 0, 5760);
	painter.drawLine(0, -9, -2, -7);
	painter.drawLine(0, -9, 2, -7);
}
