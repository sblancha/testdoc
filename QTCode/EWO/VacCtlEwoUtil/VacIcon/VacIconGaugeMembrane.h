/**
* @brief Class implementation for widget Gauge Membrane
* @see VacIcon
*/
#ifndef	VACICONGAUGEMEMBRANE_H
#define	VACICONGAUGEMEMBRANE_H

#include "VacIconVG_U.h"

class VACCTLEWOUTIL_EXPORT VacIconGaugeMembrane : public VacIconVG_U {
public:
	VacIconGaugeMembrane(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG_U(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONAUGEMEMBRANE_H
