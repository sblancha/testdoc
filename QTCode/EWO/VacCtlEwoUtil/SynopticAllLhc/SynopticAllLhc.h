#ifndef	SYNOPTICALLLHC_H
#define	SYNOPTICALLLHC_H

// Dialog for displaying synoptic of all cryogeinc vacuum of LHC

#include "SynopticAllLhcWidget.h"

#include "DataEnum.h"

#include <qdialog.h>

class SynopticAllLhc : public QDialog, public LhcRingSelection
{
	Q_OBJECT

public:
	SynopticAllLhc(DataEnum::DataMode mode);
	~SynopticAllLhc();

	static bool alreadyOpen(DataEnum::DataMode mode);

protected:
	// Instances of 2 dialogs for online mode
	static SynopticAllLhc	*onlineDialogs[2];

	// Instances of 2 dialogs for replay mode
	static SynopticAllLhc	*replayDialogs[2];

	// Widget with images
	SynopticAllLhcWidget	*pWidget;

	// Index of this instance in array of dialogs (can be 0 or 1)
	int						myIndex;

	// Data acquisition mode
	DataEnum::DataMode		mode;

	virtual void resizeEvent(QResizeEvent *e);

	SynopticAllLhc *getPairDialog(void);
	void setThisDialog(SynopticAllLhc *pDialog);
};

#endif	// SYNOPTICALLLHC_H
