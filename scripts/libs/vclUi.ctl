// CTRL Library
// created from lhcVacUi.ctl for QT
// @SAA jun10
//
// 20dec10 remark VacFaExport @SAA
//

/*

	This string literals are used to open deddicated Help pages
	Literals name appear in Click event of Help button while
  value of the literal should be inserted in PDF file as destination
 	link (named destination) in Adobe Acrobat.

*/

//#uses "VacFaExport" // 20dec10 remark VacFaExport @SAA
const string HELP_MAIN = "vacSum.pdf";
const string HELP_NOTIFICATION = "SmsNotification.pdf";

const string HELP_TOC = "vacToc";
//Machine name will be added automatically: MainPanelSPS/LEIR/LHC
const string HELP_MAIN_PANEL = "vacMainDialog"; 
const string HELP_SECTOR = "vacSector";
const string HELP_SYNOPTIC = "vacSynoptic";
const string HELP_PROFILE = "vacProfile";
const string HELP_PRESS_HISTORY = "vacPresHistory";
const string HELP_STATE_HISTORY = "vacStateHistory";
const string HELP_HISTORY_QUERY = "vacStateHistQuery";
const string HELP_DEV_LIST = "vacDevList";
const string HELP_VAC_OK = "vacVacOK";
const string HELP_GROUP_ACT = "vacGroupAct";
const string HELP_ACT_RESULT = "vacActResult";
const string HELP_PANEL_VGF_C = "vacPanelVGF_C";
//VGM
const string HELP_PANEL_VGM_C = "vacPanelVGM_C";
const string HELP_PANEL_VGM_C0 = "vacPanelVGM_C0";
//VGP
const string HELP_PANEL_VGP_T = "vacPanelVGP_T";
const string HELP_PANEL_VGP_C = "vacPanelVGP_C";
//VGR
const string HELP_PANEL_VGR_C = "vacPanelVGR_C";
const string HELP_PANEL_VGR_C0 = "PanelVGR_C0";
const string HELP_PANEL_VGR_T = "vacPanelVGR_T";
//VPG
const string HELP_PANEL_VPG_EA = "vacPanelVPG_EA";
const string HELP_PANEL_VPG_FA = "vacPanelVPG_FA";
//VV
const string HELP_PANEL_VVF_S = "vacPanelVVF_S";
const string HELP_PANEL_VVS_S = "vacPanelVVS_S";
const string HELP_PANEL_VVS_LHC = "vacPanelVVS_LHC";
const string HELP_PANEL_VVS_SV = "vacPanelVVS_SV";
//VRPI
const string HELP_PANEL_VRPI_W = "vacPanelVRPI_W";
const string HELP_PANEL_VRPI_L = "vacPanelVRPI_L";
//VRCG
const string HELP_PANEL_VRCG = "vacPanelVRCG";
//VGI
const string HELP_PANEL_VGI_T = "vacPanelVGI_T";
const string HELP_PANEL_SYNOPTIC_GIS = "synopticGIS";

//============================================
// ShowHelp is defined in dll VacFaExportExternHdl.cpp
// Open AdobReader and show pdf file accorfing to commandParam optiones
// ShowHelp(string commandParam, string filePath)
//============================================
void ShowHelpTopicOLD(string namedDest)
{
  string helpPath = PROJ_PATH + "help/en_US.iso88591";
  string helpFile = "/vacSum.pdf";
	string cmdAcro = " /A pagemode=0&nameddest=" + namedDest + " " ;
        DebugN( cmdAcro, helpPath + helpFile );
	ShowHelp(cmdAcro, helpPath + helpFile);

}
//ShowHelpTopic:
//
//
void ShowHelpTopic( string helpTopic )
{
  int replaced;
  string helpFile, path, browserCommand;
  dyn_string exceptionInfo;

  DebugN( "ShowHelpTopic():", helpTopic );
  
  switch (helpTopic) {
    case "notification" :
      helpFile = HELP_NOTIFICATION;
      break;
    default :
      helpFile = HELP_MAIN;  
  } 
  
  //helpFile = "vacSum.pdf";
  path = getPath( HELP_REL_PATH, helpFile );               
  if( path == "" )
  {
    fwException_raise( exceptionInfo, "ERROR",
      "Could not find the help file " + helpFile, "" );
      fwExceptionHandling_display( exceptionInfo );
    return;
  }
  
  path = "file://" + path + "#nameddest=" + helpTopic;
  DebugN( path );
  //std_help( browserCommand );
  
  // If there were no exceptions, then display the help
	
  fwOpenProgressBar("Help", "Opening help file...", 1);
  if(_WIN32)
  {
    DebugN( "Display Help in Windows" );
    // In fwGeneral.help.helpBrowserCommandWindows can be for example
    // cmd /c start K:\Programs\"Mozilla Firefox"\firefox $1
    // or
    // cmd /c start iexplore $1
    dpGet( "fwGeneral.help.helpBrowserCommandWindows", browserCommand );
    replaced = strreplace( browserCommand, "$1", path );
    if( replaced == 0 ) browserCommand = browserCommand + " " + path;
  }
  else
  {
    DebugN( "Display Help in Linux" );
    dpGet( "fwGeneral.help.helpBrowserCommandLinux", browserCommand );
    replaced = strreplace( browserCommand, "$1", path );
    if( replaced == 0 )  browserCommand = browserCommand + " " + path;
    //system("start iexplore " + path + " &");
  }
  
  DebugN( browserCommand );
  system( browserCommand );
  fwCloseProgressBar( );
}
//
void ShowPdf(string fileName)
{
  string helpPath = PROJ_PATH + "help/en_US.iso88591/" + fileName;
	string cmdAcro = " ";
	ShowHelp(cmdAcro, helpPath);

}
//
void ShowIconLegend()
{
  string helpFile = "/LhcIconsLegend.pdf";
  string helpPath = PROJ_PATH + "help/en_US.iso88591" + helpFile;
	string cmdAcro = " /A pagemode=0 ";
	ShowHelp(cmdAcro, helpPath);
}

void ShowWebLink(string url)
{
  if ( _WIN32 )
  {
    system("start iexplore " + url);
  }
  else
  {
    system(PVSS_BIN_PATH + "/StartHelp " + url);
  }
}
