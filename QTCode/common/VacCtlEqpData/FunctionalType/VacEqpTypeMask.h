#ifndef VACEQPTYPEMASK_H
#define	VACEQPTYPEMASK_H

// List of equipment functional types plus some methods to manipulate the list

#include "FunctionalType.h"

#include <qlist.h>

#include <stdio.h>

class VACCTLEQPDATA_EXPORT VacEqpTypeMask
{
public:
	VacEqpTypeMask();
	VacEqpTypeMask(const VacEqpTypeMask &source);
	~VacEqpTypeMask();

	static VacEqpTypeMask getAllTypes(void);

	bool contains(int type) const;
	void append(int type);
	void remove(int type);
	void clear(void) { pTypeList->clear(); }
	bool equals(const VacEqpTypeMask &other);
	QString toString(void);

	bool operator == (const VacEqpTypeMask &other) { return equals(other); }
	bool operator != (const VacEqpTypeMask &other) { return !equals(other); }
	VacEqpTypeMask & operator = (const VacEqpTypeMask &other);

	const QList<FunctionalType *> &getList(void) const { return *pTypeList; }

	void dump(FILE *pFile);

protected:
	// List of functional types in mask
	QList<FunctionalType *>	*pTypeList;
};

#endif	// VACEQPTYPEMASK_H
