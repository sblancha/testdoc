//	Implementation of SynViewXml class
/////////////////////////////////////////////////////////////////////////////////

#include "SynViewXml.h"

SynViewXml *SynViewXml::create(QWidget *parent, const QString &fileName, DataEnum::DataMode mode)
{
	SynViewXml *pView = new SynViewXml(parent);
	pView->pSynoptic = XmlSynoptic::createFromFile(pView, fileName, mode, pView->errMsg);
	int minWidth = 0, minHeight = 0;
	if(pView->pSynoptic)
	{
		minWidth = pView->pSynoptic->getWidth();
		minHeight = pView->pSynoptic->getHeight();
	}
	else
	{
		if(pView->errMsg.isEmpty())
		{
			pView->errMsg = "No synoptic data";
		}
	}
	if(!pView->errMsg.isEmpty())
	{
		QRect errRect = pView->fontMetrics().boundingRect(pView->errMsg);
		int size = errRect.width() + 50;
		if(size > minWidth)
		{
			minWidth = size;
		}
		size = errRect.height() + 50;
		if(size > minHeight)
		{
			minHeight = size;
		}
	}
	pView->setFixedWidth(minWidth);
	pView->setFixedHeight(minHeight);
	return pView;
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

SynViewXml::SynViewXml(QWidget *parent, Qt::WindowFlags f) : SynView(parent, f)
{
	pSynoptic = NULL;
}

SynViewXml::~SynViewXml()
{
	if(pSynoptic)
	{
		delete pSynoptic;
	}
}

void SynViewXml::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	if(pSynoptic)
	{
		pSynoptic->draw(painter);
		QTransform noTransform;
		painter.setTransform(noTransform);
		painter.resetTransform();
		return;
	}
	if(!errMsg.isEmpty())
	{
		painter.drawText(rect(), Qt::AlignHCenter | Qt::AlignVCenter, errMsg);
	}
}

bool SynViewXml::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	if(pSynoptic)
	{
		return pSynoptic->getToolTip(point, text, rect);
	}
	return false;
}

