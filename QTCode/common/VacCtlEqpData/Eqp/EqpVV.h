#ifndef	EQPVV_H
#define EQPVV_H

//	VV - valves

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVV : public Eqp
{
	Q_OBJECT

public:
	EqpVV(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVV(const EqpVV &src) : Eqp(src) {}
#endif
	~EqpVV();

	virtual void postProcess(void);
	virtual void getStateDpes(QStringList &stateDpes);

	virtual const QString findPlcName(void);
	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual bool isSpecColor(void);
	virtual inline bool isForSectorView(void) { return !skipOnSynoptic; }

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual int getDrawOrder(void);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
	virtual bool getInterlockColor(DataEnum::DataMode mode, QColor &color, QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList);
	virtual MainEqpStatus getMainStatus(void);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline unsigned getRR2(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr2Replay : rr2Online; }
	virtual int getErrorCode(DataEnum::DataMode mode);
	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR2 DPE for online mode
	unsigned	rr2Online;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for replay mode
	unsigned	rr2Replay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

	// Flag indicating if this type has RR2 DPE
	bool		haveRr2;

	// Flag indicating if this device has TS1 DPEs
	bool		haveTs1;

	// Flag indicating if this device has TS1 DPEs
	bool		haveTs2;

	// Flag indicating if this device has EqpStatus DPE
	bool		haveEqpStatus;

	void processInterlockSource(const char *attrName, bool isAfter);

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual bool getInterlockColorVVS_PS(DataEnum::DataMode mode, QColor &color, QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList);
	virtual bool getInterlockColorVVS_SPS(DataEnum::DataMode mode, QColor &color, QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList);
	virtual bool getInterlockColorVVS_LHC(DataEnum::DataMode mode, QColor &color, QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList);
	virtual bool getInterlockColorVV_STD_IO(DataEnum::DataMode mode, QColor &color, QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList);
#endif
};

#endif	// EQPVV_H
