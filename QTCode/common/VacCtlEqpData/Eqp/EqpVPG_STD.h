#ifndef	EQPVPG_STD_H
#define	EQPVPG_STD_H

//	EqpVPG_STD	- STD pumping group in LHC

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVPG_STD : public Eqp
{
	Q_OBJECT

public:
	EqpVPG_STD(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVPG_STD(const EqpVPG_STD &src) : Eqp(src) {}
#endif
	~EqpVPG_STD();

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
	virtual void getPartColor(int part, QColor &color, DataEnum::DataMode mode);
	virtual void getPartStateString(int part, QString &string, DataEnum::DataMode mode);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline unsigned getRR2(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr2Replay : rr2Online; }
	virtual int getErrorCode(DataEnum::DataMode mode);
	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR2 DPE for online mode
	unsigned	rr2Online;

	// Last known value of 
	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for replay mode
	unsigned	rr2Replay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getPartColorPump(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVR(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorPP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorTMP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVD(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVI(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVT(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);

	virtual void getPartStateStringPump(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVR(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getPartStateStringPP(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getPartStateStringTMP(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVD(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVI(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVP(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVT(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
#endif
};

#endif	// EQPVPG_STD_H
