//	Implementation of BakeoutRackProfileImage class
/////////////////////////////////////////////////////////////////////////////////

#include "BakeoutRackProfileImage.h"
#include "BakeoutPlotImage.h"

#include "ExtendedLabel.h"
#include "VacMainView.h"

#include "VerticalAxisWidget.h"

#include <QLayout>
#include <QPixmap>
#include <QTimer>
#include <QMouseEvent>
#include <QApplication>

#include <math.h>

#include "PlatformDef.h"
#include "DebugCtl.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

BakeoutRackProfileImage::BakeoutRackProfileImage(QWidget *parent, Qt::WindowFlags f) :
	QWidget(parent, f)
{
	viewWidth = viewHeight = 0;
	pEqp = NULL;
	newEqpSet = true;

	setMinimumSize(QSize(600, 400));

	// Build layout
	QHBoxLayout *mainLayout = new QHBoxLayout(this);
	mainLayout->setContentsMargins(0, 0, 0, 0);
	mainLayout->setSpacing(0);

	pAxis = new VerticalAxisWidget(DataEnum::Temperature, this);
	pAxis->setLeftOfPlot(true);
	pAxis->show();
	pAxis->setLimits(0, 350);
	pAxis->setActive(true);
	mainLayout->addWidget(pAxis);
	connect(pAxis, SIGNAL(limitsChanged(bool)), this, SLOT(axisLimitsChanged(bool)));

	pImage = new BakeoutPlotImage(this);
	mainLayout->addWidget(pImage, 10);
	pImage->show();
	connect(pImage, SIGNAL(resized()), this, SLOT(imageResized()));
	connect(pImage, SIGNAL(pointerMoved(QMouseEvent *)), this, SLOT(imageMouseMove(QMouseEvent *)));
	connect(pImage, SIGNAL(mouseLeave()), this, SLOT(imageMouseLeave()));
	

	pLabel = new ExtendedLabel(pImage);
	pLabel->setLabelType(ExtendedLabel::Transparent);
	QColor labelBg(255, 255, 204, 200);
	pLabel->setBackColor(labelBg);
	pLabel->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
	pLabel->hide();

	pTimer = new QTimer(this);
	pTimer->setSingleShot(true);
	connect(pTimer, SIGNAL(timeout()), pLabel, SLOT(hide()));

	pRedrawTimer = new QTimer(this);
	pRedrawTimer->setSingleShot(true);
	connect(pRedrawTimer, SIGNAL(timeout()), this, SLOT(redrawTimerDone()));


	connect(&state, SIGNAL(changed(void)), this, SLOT(stateChange(void)));

	// Finally...
	rebuildImage();
}

BakeoutRackProfileImage::~BakeoutRackProfileImage()
{
	while(!channels.isEmpty())
	{
		delete channels.takeFirst();
	}
}

/////////////////////////////////////////////////////////////////////////////////
// Event processing
/////////////////////////////////////////////////////////////////////////////////
void BakeoutRackProfileImage::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
	rebuildImage();
}

void BakeoutRackProfileImage::imageMouseMove(QMouseEvent *pEvent)
{
	BakeoutRackChannel *pItem = findItemUnderMouse(pEvent->x(), pEvent->y());
	if(pItem)
	{
		showBarLabel(pItem, pEvent->y());
	}
	else
	{
		pTimer->stop();
		pLabel->hide();
	}
}

void BakeoutRackProfileImage::imageMouseLeave(void)
{
	// pLabel->hide();
}

void BakeoutRackProfileImage::imageResized(void)
{
	pLabel->hide();
}

void BakeoutRackProfileImage::axisLimitsChanged(bool /* scrolling */)
{
	rebuildImage();
}

/*
**	FUNCTION
**		Set device to be shown in view
**
**	PARAMETERS
**		pEqp	- Pointer to device to be shown
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutRackProfileImage::setEqp(Eqp *pEqp)
{
	if(pEqp == this->pEqp)
	{
		return;
	}
	while(!channels.isEmpty())
	{
		delete channels.takeFirst();
	}
	this->pEqp = pEqp;
	if(pEqp)
	{
		for(int n = 1 ; n <= 24 ; n++)
		{
			BakeoutRackChannel *pChannel = new BakeoutRackChannel(pEqp, n);
			channels.append(pChannel);
			connect(pChannel, SIGNAL(eqpDataChanged(BakeoutRackChannel *, bool)),
				this, SLOT(eqpDataChange(BakeoutRackChannel *, bool)));
		}
	}
	state.setEqp(pEqp);
	newEqpSet = true;
	rebuildImage();
}

/*
**	FUNCTION
**		Slot activated when redraw timeout expired
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutRackProfileImage::redrawTimerDone(void)
{
	rebuildImage();
}

/*
**	FUNCTION
**		Build new profile image, set image as background for this widget
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutRackProfileImage::rebuildImage(void)
{
	pRedrawTimer->stop();
	QPixmap bgPixmap(pImage->width(), pImage->height());
	QPalette pal = QApplication::palette();
	bgPixmap.fill(pal.color(QPalette::Window));
	QPainter painter(&bgPixmap);
	if(prepare(painter))
	{
		drawBars(painter);
	}
	pal.setBrush(QPalette::Window, bgPixmap);
	pImage->setPalette(pal);
	drawTime = QTime::currentTime();
}

/*
**
**	FUNCTION
**		Prepare all data for drawing, draw axis if necessary
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		true in case of success
**		false in case of error
**
**	CAUTIONS
**		None
**
*/
bool BakeoutRackProfileImage::prepare(QPainter &painter)
{
	// Prepare vertical axis
	QRect plotRect = pImage->rect();
	pAxis->drawOnPlot(painter, plotRect);
	// Build all geometries (bars)
	newEqpSet = false;
	viewWidth = plotRect.width();
	viewHeight = plotRect.height();
	buildBarGeom(plotRect);
	if(DebugCtl::isProfile())
	{
		const char *fileName;
		#ifdef Q_OS_WIN
			fileName = "C:\\BakeoutRackProfileImage.txt";
		#else
			fileName = "/user/vacin/BakeoutRackProfileImage.txt";
		#endif
		dump(fileName);
	}

	// Draw rectangle, axis and Y-axis labels
	/*
	painter.drawLine(pAxis->getAxisLocation(), pAxis->getScaleTop(),
		pAxis->getAxisLocation(), pAxis->getScaleBottom());
	painter.drawLine(viewRect.right(), pAxis->getScaleTop(),
		viewRect.right(), pAxis->getScaleBottom());
	*/
	return true;
}

/*
**	FUNCTION
**		Build geometry of all bars to be shown in profile
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Vertical axis must be prepared before calling this method.
*/
void BakeoutRackProfileImage::buildBarGeom(const QRect &plotRect)
{
	if(!channels.count())
	{
		return;
	}

	// Calculate graph area limits
	int leftX = plotRect.left();
	int rightX = plotRect.right() - 2;
	
	// Calculate one bar width and horizontal scale coefficients
	double xScale = (double)(rightX - leftX) / (double)24;
	double oneBarWidth = xScale * ((double)VacMainView::getProfileBarWidth() / 100.0);
	bool useNarrowBar = oneBarWidth < 2.0;

	// Calculate geometry of all bars
	double barNumber = 0.0;
	for(int idx = 0 ; idx < channels.count() ; idx++)
	{
		BakeoutRackChannel *pItem = channels.at(idx);
		if(useNarrowBar)
		{
			pItem->setNarrow((int)rint(leftX + (barNumber + 0.5) * xScale));
		}
		else
		{
			int left = (int)rint(leftX + (barNumber + 0.5) * xScale - oneBarWidth / 2.);
			pItem->setWide(left, (int)rint(left + oneBarWidth));
		}
		barVerticalGeom(pItem);
		barNumber += 1.0;
	}
}

/*
**	FUNCTION
**		Calculate vertical bar geometry for single rack channel
**
**	PARAMETERS
**		pItem		- Pointer to channel data
**		scaleMin	- Minimum of vertical scale
**		scaleMax	- Maximum of vertical scale
**		scaleHeight	- Height of vertical scale in LOGARITHMIC units
**
**	RETURNS
**		true	- if bar top or bootm coordinate is changed;
**		false	- otherwise
**
**	CAUTIONS
**		None
**
*/
bool BakeoutRackProfileImage::barVerticalGeom(BakeoutRackChannel *pItem)
{
	QRect plotRect(pImage->rect());
	int barTop, barBottom = plotRect.bottom() - 1;

	// SET
	pAxis->valueToScreen(pItem->getValueSet(), barTop);
	pItem->setSetCoord(barTop);

	// T
	if(pItem->isValueValid())
	{
		pAxis->valueToScreen(pItem->getValue(), barTop);
	}
	else
	{
		barBottom = viewHeight - 1;
		barTop = plotRect.top() + 2;
	}

	if((barTop != pItem->getTop()) || (barBottom != pItem->getBottom()))
	{
		pItem->setTop(barTop);
		pItem->setBottom(barBottom);
		return true;
	}

	return false;
}

/*
**	FUNCTION
**		Draw bars
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void BakeoutRackProfileImage::drawBars(QPainter &painter)
{
	QColor useColor;
	BakeoutRackChannel *pItem;
	int idx;
	for(idx = 0 ; idx < channels.count() ; idx++)
	{
		pItem = channels.at(idx);
		useColor = pItem->getColor();
		if((!pItem->isValueValid()) && (useColor == Qt::black))
		{
			QPalette pal = QApplication::palette();
			useColor = pal.color(QPalette::Window);
		}
		painter.setPen(useColor);
		if(pItem->getLeft() == pItem->getRight())
		{
			painter.drawLine(pItem->getLeft(), pItem->getTop(), pItem->getLeft(), pItem->getBottom());
		}
		else
		{
			painter.fillRect(pItem->getRect(), useColor);
		}
	}

	// Draw SET values
	painter.setPen(QColor(0, 128, 128));
	useColor.setRgb(255, 0, 255);
	for(idx = 0 ; idx < channels.count() ; idx++)
	{
		pItem = channels.at(idx);
		painter.fillRect(pItem->getRectSet(), useColor);
	}	
}

//////////////////////////////////////////////////////////////////////////////////////
//////////// Dummy methods for the moment

void BakeoutRackProfileImage::showBarLabel(BakeoutRackChannel *pItem, int y)
{
	pTimer->stop();
	pLabel->setText(pItem->getStatusString());
	pLabel->adjustSize();
	QRect labelRect = pLabel->rect();
	int x = (pItem->getLeft() + pItem->getRight()) >> 1;
	if((x + labelRect.width()) > pImage->width())
	{
		x -= labelRect.width();
	}
	if((y + labelRect.height()) > pImage->height())
	{
		y -= labelRect.height();
	}
	pLabel->move(x, y);
	pLabel->show();
	pTimer->start(5000);
}

void BakeoutRackProfileImage::eqpDataChange(BakeoutRackChannel *pSrc, bool colorChanged)
{
	if(barVerticalGeom(pSrc) || colorChanged)
	{
		pRedrawTimer->stop();
		int elapsed = drawTime.msecsTo(QTime::currentTime());
		if(elapsed < 0)	// wrap around 24 hours
		{
			rebuildImage();
		}
		else if(elapsed > 1000)
		{
			rebuildImage();
		}
		else
		{
			pRedrawTimer->start(300);
		}
	}
}

BakeoutRackChannel *BakeoutRackProfileImage::findItemUnderMouse(int x, int /* y */)
{
	for(int idx = 0 ; idx < channels.count() ; idx++)
	{
		BakeoutRackChannel *pChannel = channels.at(idx);
		if((pChannel->getLeft() <= x) && (x <= pChannel->getRight()))
		{
			return pChannel;
		}
	}
	return NULL;
}

void BakeoutRackProfileImage::stateChange(void)
{
	unsigned rr1 = state.getRr1();
	unsigned mask = 1;
	for(int idx = 0 ; idx < channels.count() ; idx++)
	{
		BakeoutRackChannel *pChannel = channels.at(idx);
		pChannel->setActive((rr1 & mask) != 0);
		mask = mask << 1;
	}
	emit stateChanged();
}

void BakeoutRackProfileImage::dump(const char * /* fileName */)
{
}




