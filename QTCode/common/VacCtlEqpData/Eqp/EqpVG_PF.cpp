/**
* @brief Class implementation for Vacuum Gauge - for VPGF_LHC (VG_PF)
* @see Eqp
*/

#include "DataPool.h"
#include "FunctionalTypeVG.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#include "ResourcePool.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include "EqpVG_PF.h"
#include <QColor>


EqpVG_PF::EqpVG_PF(const Eqp &source) : Eqp(source) {
	rr1Online = rr1Replay = stateOnline = stateReplay = 0;
	prOnline = prReplay = 1E4;
	comAlarmOnline = comAlarmReplay = false;
	alertActive = alertNotAck = false;
	functionalType = FunctionalType::VGF; //TO DO replace by VG
	warningStOnline = errorStOnline = 0;
}
EqpVG_PF::~EqpVG_PF(){
}
/**
@brief FUNCTION get the master name, for instance the communication object V_TCP.
@param[out]   master		Master eqp instance name.
*/
void EqpVG_PF::getMaster(QString &master) {
	master = getAttrValue("MasterName");
}
/**
@brief FUNCTION OVERRIDE Return list of all DPEs, related to state of device for state history.
@param[out]   stateDpes		list of name of dpes used to calculate state.
*/
void EqpVG_PF::getStateDpes(QStringList &stateDpes) {
	stateDpes.clear();
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "objectSt");
	addDpeNameToList(stateDpes, "errorSt");
	addDpeNameToList(stateDpes, "warningSt");
}
/**
@brief FUNCTION OVERRIDE Return list of all DPEs, related to pressure value of device for trend history.
@param[out]   stateDpes		list of name of dpes used to calculate state.
*/
void EqpVG_PF::getValueDpes(QStringList &valueDpes) {
	valueDpes.clear();
	addDpeNameToList(valueDpes, "PR");
}
/**
@brief FUNCTION Connect DPEs of interest for widget animation.
@param[in]   *pDst		Class to be notified about change in this device.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVG_PF::connect(InterfaceEqp *pDst, DataEnum::DataMode mode) {
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "objectSt", mode);
	connectDpe(pDst, "PR", mode);
	connectDpe(pDst, "AL1:_alert_hdl.._ack_possible", mode);
}
/**
@brief FUNCTION Disconnect DPEs of interest for animation.
@param[in]   *pDst		Class to be notified about change in this device.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVG_PF::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode) {
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "objectSt", mode);
	disconnectDpe(pDst, "PR", mode);
	connectDpe(pDst, "AL1:_alert_hdl.._ack_possible", mode);
}
/**
@brief FUNCTION Process new online value arrived from front end to update widget animation.
@param[in]   dpe		name of dpe updated.
@param[in]	 value		new value of the dpe.
*/
void EqpVG_PF::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp) {
	if (!strcmp(dpe, "RR1"))	{
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (rr1Online == newValue)
				return;
			rr1Online = newValue;
		}
	}
	else if (!strcmp(dpe, "objectSt"))	{
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (stateOnline == newValue)
				return;
			stateOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "PR")) {
		if (value.canConvert(QVariant::Double)) {
			double newValue = value.toDouble();
			if (prOnline == newValue)
				return;
			prOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "AL1")) {
		if (value.canConvert(QVariant::Bool)) {
			bool newValue = value.toBool();
			if (alertNotAck == newValue)
				return;
			alertNotAck = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}
#ifndef PVSS_SERVER_VERSION
/**
@brief FUNCTION Process new replay value arrived from front end to update widget animation.
@param[in]   dpe		name of dpe updated.
@param[in]	 value		new value of the dpe.
*/
void EqpVG_PF::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp) {
	if (!strcmp(dpe, "RR1")) {
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (rr1Replay == newValue)
				return;
			rr1Replay = newValue;
		}
	}
	else if (!strcmp(dpe, "objectSt"))	{
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (stateReplay == newValue)
				return;
			stateReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "PR")) {
		if (value.canConvert(QVariant::Double)) {
			double newValue = value.toDouble();
			if (prReplay == newValue)
				return;
			prReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif
/**
@brief SLOT FUNCTION activated by communication object when communication state change
@param[in]   pCom		Pointer of emmiter (communication object).
@param[in]   dpeName	DPE change source (see enum above)
@param[in]	 value		New value of the dpe.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVG_PF::dpeChange(Eqp *pCom, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value,
	DataEnum::DataMode mode, const QDateTime &timeStamp) {
	bool alarm = !pCom->isAlive(mode);
	if (mode == DataEnum::Replay) {
		if (alarm == comAlarmReplay)
			return;
		comAlarmReplay = alarm;
	}
	else {
		if (alarm == comAlarmOnline)
			return;
		comAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}
/**
@brief Return main value for gauge - pressure
@param[in] mode	data acquisition mode
*/
#ifndef PVSS_SERVER_VERSION
float EqpVG_PF::getMainValue(DataEnum::DataMode mode) {
	return mode == DataEnum::Replay ? prReplay : prOnline;
}
#endif
/**
@brief Main value always valid for this control type
@param[in] mode	data acquisition mode
*/
#ifndef PVSS_SERVER_VERSION
bool EqpVG_PF::isMainValueValid(DataEnum::DataMode /* mode */) {
	return true;
}
#endif
/**
@brief Check if main value for gauge(pressure) can be negative
*/
#ifndef PVSS_SERVER_VERSION
bool EqpVG_PF::isMainValueNegative(void) const {
	return false;	// NEVER
}
#endif
/**
@brief FUNCTION Return main color of device
@param[out]   color		main color value
@param[in]	  mode		Aqn mode (online, replay...)
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PF::getMainColor(QColor &color, DataEnum::DataMode mode) {
	if (mode == DataEnum::Replay)
		getMainColor(rr1Replay, stateReplay, comAlarmReplay, color);
	else
		getMainColor(rr1Online, stateOnline, comAlarmOnline, color);
}
#endif
/**
@brief PROTECTED FUNCTION Calculate main color of device
@details Calculate color according to state meaning:
@param[in]   state			State value
@param[in]	 comAlarm	Alarm value of the Tcp communication object
@param[out]	 color			Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PF::getMainColor(unsigned rr1, unsigned state, bool comAlarm, QColor &color) {
	// RR define in Eqp.h
	//#define	RR1_16B_GP_VALID			(0x4000) // 0x40 00
	//#define	RR1_16B_GP_ERROR			(0x0080) // 0x00 80
	//#define	RR1_16B_GP_WARNING			(0x0040) // 0x00 40
	//#define	RR1_16B_GP_ON				(0x0200) // 0x00 02
	//#define	RR1_16B_GP_OFF				(0x0100) // 0x00 01
	// state: GAUGE STATUS (0: Measurement OK; 1: Under Range; 2: Over Range; 3: Error; 4:Off; 5: No Hardware)
	// First check Communication is Ok and eqp valid
	if (comAlarm || (state == 5) || !(rr1 & RR1_16B_GP_VALID)) {
		color.setRgb(COLOR_GP_INVALID);
		return;
	}
	// Then check Error
	if (rr1 & RR1_16B_GP_ERROR) {
		color.setRgb(COLOR_GP_ERROR);
		return;
	}
	// Check Warning
	if (rr1 & RR1_16B_GP_WARNING) {
		color.setRgb(COLOR_GP_WARNING);
		return;
	}
	// Check RR1
	if (rr1 & RR1_16B_GP_OFF) {
		color.setRgb(COLOR_GP_OFF);
		return;
	}
	if (rr1 & RR1_16B_GP_ON) {
		// Finally check state
		if ((state == 0) || (state == 2)) {
			color.setRgb(COLOR_GP_ON);
			return;
		}
		if (state == 1) {
			color.setRgb(COLOR_GP_UR);
			return;
		}
		color.setRgb(COLOR_GP_ERROR);
		return;
	}
	color.setRgb(COLOR_GP_INVALID);
	return;
}
#endif

/**
@brief FUNCTION Return main state string of device
@param[out]   string	main state string value
@param[in]	  mode		Aqn mode (online, replay...)
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PF::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if (mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, stateReplay, prReplay, comAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, stateOnline, prOnline, comAlarmOnline, string);
	}
}
#endif

/**
@brief PROTECTED FUNCTION Calculate main state string of device
@details String value get from ressource file
@param[in]   rr1			read register 1 value
@param[in]   state			State value
@param[in]	 comAlarm	Alarm value of the Tcp communication object
@param[out]	 string			Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PF::getMainStateString(unsigned rr1, unsigned state, double pr, bool comAlarm, QString &string, bool isStateHistory /*= false*/)
{
	if (comAlarm)
	{
		string = "Communication Error";
		return;
	}
	if (!(rr1 & RR1_16B_GP_VALID))
	{
		string = "Invalid";
		return;
	}

	char resourceName[256];

#ifdef Q_OS_WIN
	//sprintf_s(resourceName, sizeof(resourceName) / sizeof(resourceName[0]), "VG_PF.State_%d", state);
#else
	//sprintf(resourceName, "VG_PF.State_%d", state);
#endif

	if (ResourcePool::getInstance().getStringValue(resourceName, string) == ResourcePool::OK) {
		if (!isStateHistory) {
			string = string + ": " + QString::number(pr);
			string = string + " mbar";
			return;
		}
		else { // for state history
			return;
		}
	}
	string = "Undefined state";
}
#endif

/**
@brief FUNCTION Calculate main state string for contextual menu of device
@details Same as getMainStateString(), ":"+PR already in mainState
@param[in]	  mode		Aqn mode (online, replay...)
@param[out]	 string		Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PF::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	getMainStateString(string, mode);
}
#endif

/**
@brief FUNCTION Calculate tool tip
@param[in]	  mode		Aqn mode (online, replay...)
@param[out]	 string		Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PF::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	if (mode == DataEnum::Replay)
	{
		getToolTipString(rr1Replay, stateReplay, prReplay, comAlarmReplay, string);
	}
	else
	{
		getToolTipString(rr1Online, stateOnline, prOnline, comAlarmOnline, string);
	}
}
#endif

/**
@brief PROTECTED FUNCTION Calculate tool tip
@param[in]   rr1			read register 1 value
@param[in]   state			State value
@param[in]	 comAlarm	Alarm value of the Tcp communication object
@param[out]	 string			Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PF::getToolTipString(unsigned rr1, unsigned state, double pr, bool comAlarm, QString &string)
{
	QString stateString;
	getMainStateString(rr1, state, pr, comAlarm, stateString);
	string = getVisibleName();
	string += ": ";
	string += stateString;
}
#endif

/**
@brief FUNCTION Calculate main state string and color for moment in the past, data are taken from state archive data pool
@param[in]   pPool			Pointer to state history data pool
@param[in]	 ts				Moment when state is needed
@param[out]	 stateString	Variable where resulting state string will be written
@param[out]	 color			Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PF::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
	QString &stateString, QColor &color)
{
	stateString = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("RR1");
	dpeNames.append("State");
	QList<QVariant *> values;
	if (!pPool->getDpValues(dpName, ts, dpeNames, values))
		return;
	if (values.isEmpty())
		return;
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	if (values.isEmpty())
		return;
	pVariant = values.takeFirst();
	unsigned state = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, state, 0.0, false, stateString, true); //pr = 0.0 because not used for stateHistory string
	getMainColor(rr1, state, false, color);
	while (!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/**
@brief FUNCTION Decide which DPEs shall be read to check if this device matches current device list criteria or not
@param[out]	 dpes		Variable where all required DPEs will be added
*/
#ifndef PVSS_SERVER_VERSION
void EqpVG_PF::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if (!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if (pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "State");
}
#endif

/**
@brief FUNCTION Return code to check if device in recent state matches device list criteria or not
@details Return 0 = Device does not match criteria; 1 = Device matches criteria; 2 = More information is needed (history)-NOT for valves
*/
#ifndef PVSS_SERVER_VERSION
int EqpVG_PF::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	DataEnum::DataMode mode = criteria.getMode();
	bool active = mode == DataEnum::Replay ? activeReplay : activeOnline;
	if (!active)	{
		if (criteria.isNotActive()) {
			return 1;
		}
		else {
			return 0;
		}
	}
	if (criteria.isEmpty()) {
		return 1;	// No criteria - all devices are needed
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if (!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if (pPlc)
		{
			if (!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if (!(rr1 & RR1_16B_GP_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	unsigned state = DevListCriteria::EqpStateOther;
	if ((rr1 & (RR1_16B_GP_ON | RR1_16B_GP_OFF)) == RR1_16B_GP_ON)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if ((rr1 & (RR1_16B_GP_ON | RR1_16B_GP_OFF)) == RR1_16B_GP_OFF)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if (criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if (criteria.isErrors())
	{
		if (rr1 & RR1_16B_GP_ERROR)
		{
			return 1;
		}
	}
	if (criteria.isWarnings())
	{
		if (rr1 & (RR1_16B_GP_ERROR | RR1_16B_GP_WARNING))
		{
			return 1;
		}
	}
	return 0;
}
#endif
/**
@brief FUNCTION Check if device matches criteria for E-mail/SMS notification
@param[in, out]   pCriteria	Pointer to criteria to check
*/
void EqpVG_PF::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if (pCriteria->getFunctionalType() != functionalType) {
		return;
	}

	if (pCriteria->getType() == EqpMsgCriteria::None) {
		return;
	}
	switch (pCriteria->getType()) {
	case EqpMsgCriteria::Error:
		if (rr1Online & RR1_16B_GP_ERROR) {
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else {
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;


	case EqpMsgCriteria::Warning:
		if (rr1Online & RR1_16B_GP_WARNING) {
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else {
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;

	case EqpMsgCriteria::MainValue:
		if (pCriteria->isReverse()) {
			if (prOnline > pCriteria->getUpperLimit()) {
				pCriteria->setMatch(false, true, prOnline);
				return;
			}
			else if (prOnline < pCriteria->getLowerLimit()) {
				pCriteria->setMatch(true, true, prOnline);
				return;
			}
			// Between lower and upper limit - no change
			return;
		}
		else {
			if (prOnline > pCriteria->getUpperLimit()) {
				pCriteria->setMatch(true, true, prOnline);
				return;
			}
			else if (prOnline < pCriteria->getLowerLimit()) {
				pCriteria->setMatch(false, true, prOnline);
				return;
			}
			// Between lower and upper limit - no change
			return;
		}
		break;

	case EqpMsgCriteria::MainState:
		// TO DO
		//switch (pCriteria->getSubType()) {
		//}
		break;
	}
	pCriteria->setMatch(false, false, 0);
}

