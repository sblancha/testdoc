#ifndef	INTERLOCKICONVRGPT_H
#define	INTERLOCKICONVRGPT_H

#include "qwidget.h"
#include "InterlockIcon.h"

class InterlockIconVRGPT : public InterlockIcon
{
	Q_OBJECT
public:
	InterlockIconVRGPT(QWidget * parent = NULL);
	inline InterlockIconClass getIconClass() { return InterlockSource; };
	void moveIcon(int x, int y);
	QPoint getConnectionPoint(QString connectorName);
	void getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber);
	QRect getBoundingRect();
	virtual ~InterlockIconVRGPT();
private:
	virtual void paintEvent(QPaintEvent *pEvent);
	QString convertToExp(QString floatString);
	QString mapGaugeNumberToSlot(QString gaugeNumber);
};

#endif