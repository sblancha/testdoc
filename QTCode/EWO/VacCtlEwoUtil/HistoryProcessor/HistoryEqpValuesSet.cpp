//	Implementaion of HistoryEqpValuesSet class
/////////////////////////////////////////////////////////////////////////////////

#include "HistoryEqpValuesSet.h"

#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

HistoryEqpValuesSet::HistoryEqpValuesSet(Eqp *pEqp, const QString &bitDpeName, int bitNumber, const QString &bitVisibleName) :
	bitDpe(bitDpeName), bitName(bitVisibleName)
{
	this->pEqp = pEqp;
	bitNbr = bitNumber;
	index = 0xFFFFFFFF;
	previousValueSet = false;
	previousValue = 0;
}

HistoryEqpValuesSet::HistoryEqpValuesSet(Eqp *pEqp, const QString &dpeName) : dpe(dpeName)
{
	this->pEqp = pEqp;
	index = 0xFFFFFFFF;
	previousValueSet = false;
	previousValue = 0;
}

HistoryEqpValuesSet::~HistoryEqpValuesSet()
{
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}

/*
**	FUNCTION
**		Add value to list of values for this device if given
**		timestamp is not in list yet
**
**	ARGUMENTS
**		time	- Time stamp of value to be added
**		value	- value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		It is expected that timestamps will arrive in ASCENDING order,
**		so method checks timestamp existing starting from list end
*/
void HistoryEqpValuesSet::addValue(const QDateTime &time, float value)
{
	for(int idx = values.count() - 1 ; idx >= 0 ; idx--)
	{
		HistoryEqpValue *pValue = values.at(idx);
		if(pValue->getTime() >= time)
		{
			return;
		}
		else if(pValue->getTime() < time)
		{
			break;
		}
	}
	values.append(new HistoryEqpValue(time, value));
}

/*
**	FUNCTION
**		Return timestamp at given index
**
**	ARGUMENTS
**		timeIndex	- required timestamp index
**		time		- variable where result will be written
**
**	RETURNS
**		true if time is found; or
**		false if index is out of range
**
**	CAUTIONS
**		None
*/
bool HistoryEqpValuesSet::getTimeAt(int timeIndex, QDateTime &time)
{
	if(timeIndex >= values.count())
	{
		return false;
	}
	if (timeIndex < 0) {
		if (previousValueSet) {
			time = previousTs;
			return true;
		}
		return false;
	}
	HistoryEqpValue *pValue = values.at(index);
	time = pValue->getTime();
	return true;
}

/*
**	FUNCTION
**		Return value at given timestamp, if timestamp is not found - return
**		last value BEFORE that timestamp, or value at the very first timestamp
**		if time is before the very first time
**
**	ARGUMENTS
**		time	- required timestamp
**
**	RETURNS
**		Value at given time
**
**	CAUTIONS
**		None
*/
float HistoryEqpValuesSet::getValueAt(const QDateTime &time)
{
/*
if(!strcmp(pEqp->getName(), "VGPB.220.1R1.X"))
{
	qDebug("Start search %X\n", (unsigned long)time);
}
*/
	if(!values.count())
	{
		return 0;
	}
	if(time < values.first()->getTime())
	{
		if (previousValueSet) {
			return previousValue;
		}
		return values.first()->getValue();
	}

	// Use binary search
	int	high = values.count(), low = -1, probe;
	while((high - low) > 1)
	{
		probe = (low + high) >> 1;
		HistoryEqpValue *pValue = values.at(probe);
		if(pValue->getTime() > time)
		{
			high = probe;
		}
		else
		{
			low = probe;
		}
	}
/*
if(!strcmp(pEqp->getName(), "VGPB.220.1R1.X"))
{
	qDebug(" search finished low %d high %d\n", low, high);
}
*/
	if(low < 0)	// We shall not get here - this means timestamp is before the very first
				// timestamp in the list, but this was checked above
	{
		return 0;
	}
	HistoryEqpValue *pValue = NULL;
	if(high < (int)values.count())
	{
		pValue = values.at(high);
/*
if(!strcmp(pEqp->getName(), "VGPB.220.1R1.X"))
{
	qDebug("  time at hight %X\n", (unsigned long)pValue->getTime());
}
*/
		if(pValue->getTime() <= time)
		{
			return pValue->getValue();
		}
	}
	pValue = values.at(low);
/*
if(!strcmp(pEqp->getName(), "VGPB.220.1R1.X"))
{
	qDebug("  time at low %X\n", (unsigned long)pValue->getTime());
}
*/
	if(pValue->getTime() <= time)
	{
		return pValue->getValue();
	}
	if(!low)
	{
		return pValue->getValue();
	}
	return values.at(--low)->getValue();
}

