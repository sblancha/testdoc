//	Implementation of VacIconVOPS_VELO class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVOPS_VELO.h"

#include "EqpVOPS_VELO.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OK				0,255,0
#define COLOR_BAD				255,0,0
#define	COLOR_ERROR				255,102,255
#define	COLOR_WARNING			255,255,102

//	Main state bits to be analyzed
//	Main state bits to be analyzed
#define	RR1_VALID			(0x40000000)
#define RR1_ERROR			(0x00800000)
#define	RR1_WARNING			(0x00400000)
#define	RR1_A				(0x00080000)
#define	RR1_A_CABLE			(0x00040000)
#define	RR1_B				(0x00020000)
#define	RR1_B_CABLE			(0x00010000)


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
VacIconVOPS_VELO::VacIconVOPS_VELO(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(40, 21);
	setAreaGeometry();
}

VacIconVOPS_VELO::~VacIconVOPS_VELO()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for symbol areas rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVOPS_VELO::setAreaGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		a.setRect(1, 1, 19, 19);
		b.setRect(20, 1, 19, 19);
		break;
	case VacIconContainer::Down:
		a.setRect(20, 1, 19, 19);
		b.setRect(1, 1, 19, 19);
		break;
	case VacIconContainer::Left:
		a.setRect(1, 20, 19, 19);
		b.setRect(1, 1, 19, 19);
		break;
	case VacIconContainer::Right:
		a.setRect(1, 1, 19, 19);
		b.setRect(1, 20, 19, 19);
		break;
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVOPS_VELO::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		return VacIconGeometry(40, 21, 20, 10, 0);
		break;
	default:
		break;
	}
	return VacIconGeometry(21, 40, 10, 20, -1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVOPS_VELO::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(41);
		size.setHeight(21);
		break;
	default:
		size.setWidth(21);
		size.setHeight(41);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreaGeometry();
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVOPS_VELO::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVOPS_VELO"), "VacIconVOPS_VELO::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVOPS_VELO *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVOPS_VELO::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVOPS_VELO::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVOPS_VELO::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVOPS_VELO::getToolTip(const QPoint &pos, QString &text, QRect &rect)
{
	if(pEqp && connected)
	{
		// Where is the point? If in one of valve - show valve text,
		// otherwise show valve name and state
		if(a.contains(pos))
		{
			text = pEqp->getVisibleName();
			QString state;
			pEqp->getPartStateString(Eqp::EqpPartVOPS_A, state, mode);
			text += ": ";
			text += state;
			rect = a;
		}
		else if(b.contains(pos))
		{
			text = pEqp->getVisibleName();
			QString state;
			pEqp->getPartStateString(Eqp::EqpPartVOPS_B, state, mode);
			text += ": ";
			text += state;
			rect = b;
		}
	}
	else
	{
		rect = this->rect();
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVOPS_VELO::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	drawA(painter, ctlStatus, rr1, plcAlarm);

	drawB(painter, ctlStatus, rr1, plcAlarm);
}

void VacIconVOPS_VELO::drawA(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	// Save painter coordinate system - other part will be use own translation
	painter.save();

	// Prepare painter's coordinate system for image drawing
	// Coordinate system (0,0) is center of A symbol
	painter.translate(a.center().x(), a.center().y());

	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}

	// Draw filled circle
	setPainterForA(painter, ctlStatus, rr1, plcAlarm);
	QPen pen = painter.pen();
	painter.setPen(Qt::NoPen);
	painter.drawPie(-9, -9, 19, 19, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(-9, -9, 19, 19, 0, 360 * 16);

	// Draw A-specific interior of circle
	painter.setPen(Qt::black);

	// vertical line
	painter.drawLine(0, -8, 0, 8);

	// Horizontal arc
	painter.drawArc(-20, -40, 40, 40, -117 * 16, 54 * 16);

	// Two lines from vertical line to circle border
	painter.drawLine(0, 3, -4, 7);
	painter.drawLine(0, 3, 4, 7);

	// Two lines forming '+' symbol
	painter.setPen(QPen(Qt::black, 2));
	painter.drawLine(2, 3, 8, 3);
	painter.drawLine(5, 0, 5, 6);

	// Restore coordinate system to original
	painter.restore();
}

void VacIconVOPS_VELO::drawB(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	// Here we do not need to save painter coordinate system - this is the last part to draw

	// Prepare painter's coordinate system for image drawing
	// Coordinate system (0,0) is center of B symbol
	painter.translate(b.center().x(), b.center().y());

	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}

	// Draw filled circle
	setPainterForB(painter, ctlStatus, rr1, plcAlarm);
	QPen pen = painter.pen();
	painter.setPen(Qt::NoPen);
	painter.drawPie(-9, -9, 19, 19, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(-9, -9, 19, 19, 0, 360 * 16);

	// Draw A-specific interior of circle
	painter.setPen(Qt::black);

	// vertical line
	painter.drawLine(0, -8, 0, 8);

	// Horizontal line
	painter.drawLine(-8, 0, 8, 0);

	// Two lines from vertical line to circle border
	painter.drawLine(0, 3, -4, 7);
	painter.drawLine(0, 3, 4, 7);

	// Line forming '-' symbol
	painter.setPen(QPen(Qt::black, 2));
	painter.drawLine(2, 3, 8, 3);
}

void VacIconVOPS_VELO::setPainterForA(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	QColor fillColor(COLOR_UNDEFINED);
	QColor lineColor = Qt::black;
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		fillColor.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		fillColor.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				// Line color - depends on errors/warnings
				if(rr1 & RR1_ERROR)
				{
					lineColor.setRgb(COLOR_ERROR);
				}
				else if(rr1 & RR1_WARNING)
				{
					lineColor.setRgb(COLOR_WARNING);
				}
				// Fill color - depends on state (plus some warnings)
				if(rr1 & RR1_A_CABLE)
				{
					if(rr1 & RR1_A)
					{
						fillColor.setRgb(COLOR_OK);
					}
					else
					{
						fillColor.setRgb(COLOR_BAD);
					}
				}
			}
		}
		break;
	}
	painter.setBrush(fillColor);
	QPen pen(lineColor, lineColor == Qt::black ? 1 : 2);
	painter.setPen(pen);
}

void VacIconVOPS_VELO::setPainterForB(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	QColor fillColor(COLOR_UNDEFINED);
	QColor lineColor = Qt::black;
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		fillColor.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		fillColor.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				// Line color - depends on errors/warnings
				if(rr1 & RR1_ERROR)
				{
					lineColor.setRgb(COLOR_ERROR);
				}
				else if(rr1 & RR1_WARNING)
				{
					lineColor.setRgb(COLOR_WARNING);
				}
				// Fill color - depends on state (plus some warnings)
				if(rr1 & RR1_B_CABLE)
				{
					if(!(rr1 & RR1_B))
					{
						fillColor.setRgb(COLOR_OK);
					}
					else
					{
						fillColor.setRgb(COLOR_BAD);
					}
				}
			}
		}
		break;
	}
	painter.setBrush(fillColor);
	QPen pen(lineColor, lineColor == Qt::black ? 1 : 2);
	painter.setPen(pen);
}

