#ifndef PROFILEALLLHCBEAMWIDGET_H
#define	PROFILEALLLHCBEAMWIDGET_H

#include "VacCtlEwoUtilExport.h"

//	All LHC profile - for blue/red beams

#include "DataEnum.h"
#include "VacEqpTypeMask.h"
#include "LhcRingSelection.h"

#include <qwidget.h>

#include "ProfileLhcSingleBeamView.h"
class ActiveLabel;
class MainPart;

class ProfileAllLHCBeamWidget : public QWidget, public LhcRingSelection
{
	Q_OBJECT

public:
	VACCTLEWOUTIL_EXPORT  ProfileAllLHCBeamWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);
	VACCTLEWOUTIL_EXPORT ~ProfileAllLHCBeamWidget();

	VACCTLEWOUTIL_EXPORT void setBeamLimits(int min, int max);
	VACCTLEWOUTIL_EXPORT void setLinLimits(int min, int max);
	VACCTLEWOUTIL_EXPORT void analyzeTypes(VacEqpTypeMask &allMpMask);
	VACCTLEWOUTIL_EXPORT void applyMask(VacEqpTypeMask &newEqpMask);

	// Access
	VACCTLEWOUTIL_EXPORT inline int getLhcPartIndex(void) const { return lhcPartIndex; }
	VACCTLEWOUTIL_EXPORT void setLhcPartIndex(int index);
	VACCTLEWOUTIL_EXPORT inline VacEqpTypeMask & getAllEqpMask(void) { return allEqpMask; }
	VACCTLEWOUTIL_EXPORT inline VacEqpTypeMask & getEqpMask(void) { return eqpMask; }

signals:
	void profileDpDown(int button, int mode, int x, int y, int dummy, const char *dpName);
	void beamLimitsChanged(int min, int max);
	void linLimitsChanged(int min, int max);
	void labelPressed(QMouseEvent *e);

protected:
	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// List of profile images
	QList<ProfileLhcSingleBeamView *>	images;

	// Equipment type mask for all possible equipment types
	VacEqpTypeMask				allEqpMask;

	// Equipment type mask
	VacEqpTypeMask				eqpMask;

	// Index of this instance in array of dialogs
	int							lhcPartIndex;

	void buildInitialEqpMask(void);
	void buildLayout(void);
	bool buildSectorRange(MainPart *pMp, Sector **ppFirstSector, Sector **ppLastSector);

protected slots:
	void beamLimitsChange(int min, int max);
	void linLimitsChange(int min, int max);

private slots:
	void dpMouseDown(int button, int mode, int x, int y, const char *dpName);
	void labelPress(QMouseEvent *e);
};


#endif	// PROFILEALLLHCBEAMWIDGET_H
