#ifndef	EQPMESSAGE_H
#define	EQPMESSAGE_H

#include "VacCtlOnlineNotificationExport.h"

// Class responsible for building human-readable message when device matches
// given criteria and message shall be sent

#include "EqpMessagePart.h"

#include <qlist.h>


class EqpState;
class ComplexCriteria;
class MessageGroup;

class VACCTLONLINENOTIFICATION_EXPORT EqpMessage
{
public:
	virtual ~EqpMessage();

	static EqpMessage *create(const char *string, QString &errMsg);

	QString buildMessage(EqpState *pState, QString &constantPart, ComplexCriteria *pCriteria);
	QString buildMessage(MessageGroup *pGroup);

protected:
	EqpMessage(const QList<EqpMessagePart *> &partList);

	// List of parts of which resulting message is built
	QList<EqpMessagePart *> *pPartList;
};

#endif	// EQPMESSAGE_H
