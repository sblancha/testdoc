#ifndef FUNCTIONALTYPEVPG_H
#define FUNCTIONALTYPEVPG_H

// Special functional type for pumping groups - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVPG : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Connected = 1,
		On = 2,
		Off = 3,
		VvrOpen = 4,
		VvrClosed = 5,
		TmpOn = 6,
		TmpOff = 7,
		Stopped = 8,
		Nominal = 9,
		Vvr1Open = 10,
		Vvr1Closed = 11,
		Vvr2Open = 12,
		Vvr2Closed = 13

	};

	FunctionalTypeVPG(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVPG() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVPG_H
