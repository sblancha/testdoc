#include "BeamLineConnType.h"

#include "PlatformDef.h"

#include <qstringlist.h>

#include <stdio.h>

/*
**
** FUNCTION
**		Decode connection type read from file
**
** PARAMETERS
**		connType	- String presentation of connection type
**		lineNo		- line number in file.
**		errList		- Variable where error message shall be written
**						in case of errors.
**
** RETURNS
**		Decoded connection type, or
**		-1	- in case of error
**
** CAUTIONS
**		None
*/
int BeamLineConnType::decode(const char *connType, int lineNo, QStringList &errList)
{
	if(!strcasecmp(connType, "li"))
	{
		return LeftIn;
	}
	else if(!strcasecmp(connType, "lo"))
	{
		return LeftOut;
	}
	else if(!strcasecmp(connType, "ri"))
	{
		return RightIn;
	}
	else if(!strcasecmp(connType, "ro"))
	{
		return RightOut;
	}
	errList.append(QString("Line %1: unknown connection type <%2>").arg(lineNo).arg(connType));
	return None;
}
