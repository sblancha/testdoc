//	Implementation of VacIconVPRO_VELO class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPRO_VELO.h"

#include "EqpVPRO_VELO.h"

#include <qpainter.h>

// Labels for 4 modes
const char *VacIconVPRO_VELO::labels[] =
	{
		"Busy",
		"Ready",
		"Error",
		"Mode"
	};

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_BIT_SET			255,152,63
#define COLOR_BIT_NOT_SET		127,127,127

//	Main state bits to be analyzed
#define	RR1_BUSY		(0x0020)
#define	RR1_ERROR		(0x0010)
#define	RR1_MODE		(0x0008)
#define	RR1_READY		(0x0004)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVPRO_VELO::VacIconVPRO_VELO(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	// Icon must be large enough to include 4 labels and device name.
	// We do not know device name in advance - let's hope it will never be
	// longer than 4 labels
	int totalWidth = 0, totalHeight = 0;
	maxLabelHeight = 0;
	for(int i = 0 ; i < 4 ; i++)
	{
		QRect labelRect = fontMetrics().boundingRect(labels[i]);
		totalWidth += labelRect.width() + 2 + 1;
		if(maxLabelHeight < labelRect.height())
		{
			maxLabelHeight = labelRect.height();
		}
	}
	totalHeight = maxLabelHeight + 2;	// Space above/below string

	QFont boldFont(font());
	boldFont.setBold(true);
	QFontMetrics metrics(boldFont);
	QRect labelRect = metrics.boundingRect("VPRO.123");
	totalHeight += labelRect.height() + 2;
	setFixedSize(totalWidth + 2, totalHeight + 2);
}

VacIconVPRO_VELO::~VacIconVPRO_VELO()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		It is supposed that this method will not be used for CRYO_TT
*/
VacIconGeometry VacIconVPRO_VELO::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(width(), height(), width() >> 1, height() >> 1, 1);
}


/*
**	FUNCTION
**		Set reference to equipment for this icon
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPRO_VELO::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVPRO_VELO"), "VacIconVPRO_VELO::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVPRO_VELO *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPRO_VELO::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPRO_VELO::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPRO_VELO::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPRO_VELO::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPRO_VELO::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	QRect myRect = rect();
	painter.eraseRect(myRect);
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// 4 labels on bottom
	painter.setPen(Qt::black);
	QColor color(COLOR_UNDEFINED);
	int textX = 2;
	int textY = height() - 2;
	unsigned rr1 = 0;
	const char *eqpName = "Not connected";
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		switch(ctlStatus)
		{
		case Eqp::NotControl:
			eqpName = "Not Controlled";
			break;
		case Eqp::NotConnected:
			eqpName = "Not Connected";
			break;
		default:
			eqpName = pEqp->getVisibleName();
			rr1 = pEqp->getRR1(mode);
			break;
		}
	}
	// Label Busy
	if(pEqp)
	{
		switch(ctlStatus)
		{
		case Eqp::NotControl:
			color.setRgb(COLOR_NOT_CONTROL);
			break;
		case Eqp::NotConnected:
			color.setRgb(COLOR_NOT_CONNECTED);
			break;
		default:
			if(rr1 & RR1_BUSY)
			{
				color.setRgb(COLOR_BIT_SET);
			}
			else
			{
				color.setRgb(COLOR_BIT_NOT_SET);
			}
			break;
		}
	}
	int labelWidth = painter.fontMetrics().width(labels[0]);
	painter.fillRect(textX, textY - maxLabelHeight - 1, labelWidth + 2,
		maxLabelHeight + 2, color);
	painter.drawText(textX, textY, labels[0]);
	textX += labelWidth + 3;

	// Label Ready
	if(pEqp)
	{
		switch(ctlStatus)
		{
		case Eqp::NotControl:
			color.setRgb(COLOR_NOT_CONTROL);
			break;
		case Eqp::NotConnected:
			color.setRgb(COLOR_NOT_CONNECTED);
			break;
		default:
			if(rr1 & RR1_READY)
			{
				color.setRgb(COLOR_BIT_SET);
			}
			else
			{
				color.setRgb(COLOR_BIT_NOT_SET);
			}
			break;
		}
	}
	labelWidth = painter.fontMetrics().width(labels[1]);
	painter.fillRect(textX, textY - maxLabelHeight - 1, labelWidth + 2,
		maxLabelHeight + 2, color);
	painter.drawText(textX, textY, labels[1]);
	textX += labelWidth + 3;

	// Label Error
	if(pEqp)
	{
		switch(ctlStatus)
		{
		case Eqp::NotControl:
			color.setRgb(COLOR_NOT_CONTROL);
			break;
		case Eqp::NotConnected:
			color.setRgb(COLOR_NOT_CONNECTED);
			break;
		default:
			if(rr1 & RR1_ERROR)
			{
				color.setRgb(COLOR_BIT_SET);
			}
			else
			{
				color.setRgb(COLOR_BIT_NOT_SET);
			}
			break;
		}
	}
	labelWidth = painter.fontMetrics().width(labels[2]);
	painter.fillRect(textX, textY - maxLabelHeight - 1, labelWidth + 2,
		maxLabelHeight + 2, color);
	painter.drawText(textX, textY, labels[2]);
	textX += labelWidth + 3;

	// Label Mode
	if(pEqp)
	{
		switch(ctlStatus)
		{
		case Eqp::NotControl:
			color.setRgb(COLOR_NOT_CONTROL);
			break;
		case Eqp::NotConnected:
			color.setRgb(COLOR_NOT_CONNECTED);
			break;
		default:
			if(rr1 & RR1_MODE)
			{
				color.setRgb(COLOR_BIT_SET);
			}
			else
			{
				color.setRgb(COLOR_BIT_NOT_SET);
			}
			break;
		}
	}
	labelWidth = painter.fontMetrics().width(labels[3]);
	painter.fillRect(textX, textY - maxLabelHeight - 1, labelWidth + 2,
		maxLabelHeight + 2, color);
	painter.drawText(textX, textY, labels[3]);
	textX += labelWidth + 3;

	// Device name - bold font on default background
	QFont boldFont(painter.font());
	boldFont.setBold(true);
	painter.setFont(boldFont);
	QRect labelRect = painter.fontMetrics().boundingRect(eqpName);
	painter.drawText(myRect.center().x() - (labelRect.width() >> 1),
		labelRect.height() + 1, eqpName);
}

