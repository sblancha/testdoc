#ifndef	VACICONEXP_AREA_H
#define	VACICONEXP_AREA_H

// Icon for experimantal area - no online/replay data, just image

#include "VacIcon.h"

class EqpEXP_AREA;

class VACCTLEWOUTIL_EXPORT VacIconEXP_AREA : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconEXP_AREA(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconEXP_AREA();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);
	virtual void setMode(DataEnum::DataMode /* newMode */) {}
	virtual bool connect(void) { return false; }
	virtual bool disconnect(void) { return false; }

protected:
	// Pointer to my device
	EqpEXP_AREA *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACICONEXP_AREA_H
