#ifndef	IMAGINEITEMGENERIC_H
#define	IMAGINEITEMGENERIC_H

#include "ImageItem.h"

#include "VacIcon.h"

class VACCTLEWOUTIL_EXPORT ImageItemGeneric : public ImageItem
{
public:
	ImageItemGeneric();
	~ImageItemGeneric();

	void reset(void);
	VacIcon	*getIcon(QWidget *parent, DataEnum::DataMode mode);
	virtual void draw(QPainter &painter);
	virtual Eqp *getEqp(void);
	virtual bool checkForInterlockSource(void);
	virtual bool addAlarmSources(QList<ImageItem *> &allItems, QList<VacIcon *> &allIcons);

protected:
	// Start/end coordinates
	int		startX;
	int		startY;

	// Icon direction
	int 	direction;

	// Beam direction
	int		beamDirection;

	// Equipment type
	int		eqpType;

	// DP name
	QString	dpName;

	// Beam pipe color
	QColor	beamPipeColor;

	// Flag - is this VacIconEwo.ewo
	bool	icon;

	// Icon - needed for drawing interlock sources arrow
	VacIcon	*pEqpIcon;

	// Flag to identify if this device is interlock source for valve which is after this device on view
	bool		intlSourceForward;

	// Flag to identify if this device is interlock source for valve which is before this device on view
	bool		intlSourceBackward;

	virtual void parseProp(QXmlStreamReader &reader);
	void parseExtended(QXmlStreamReader &reader);
};

#endif	// IMAGINEITEMGENERIC_H
