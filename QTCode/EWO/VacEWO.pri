#	Common definitions for building vacuum-specific EWOs

TEMPLATE = lib

QT += widgets

OBJECTS_DIR = ../obj

INCLUDEPATH += $$(API_ROOT)/include/EWO

LIBS += -L../../../bin

unix {
	LIBS += -L$$(API_ROOT)/lib.$$(PLATFORM) -lewo
	DESTDIR = ../../../bin/widgets/linux-rhel-x86_64
	CONFIG += qt thread release warn_on separate_debug_info
	CONFIG += plugin no_plugin_name_prefix
	QMAKE_CLEAN += ${TARGET} ${TARGET}.debug ${QMAKE_MOC_SRC}
}

win32 {
	LIBS += -L$$(API_ROOT)/lib.winnt -lewo
	DESTDIR = ../../../bin/widgets/windows-64
	CONFIG += qt dll thread release
	QMAKE_LFLAGS += /nodefaultlib:libc
	QMAKE_LFLAGS += /nodefaultlib:libcp
	win32:QMAKE_CXXFLAGS+=/Zi
	win32:QMAKE_LFLAGS+= /INCREMENTAL:NO /Debug
	RC_FILE = VersInfo.rc
}

#DEFINES += _VACDEBUG

