#ifndef	STDEQPMESSAGE_H
#define	STDEQPMESSAGE_H

#include "VacCtlEqpDataExport.h"

// Standard message for equipment

#include <qstring.h>

class VACCTLEQPDATA_EXPORT StdEqpMessage
{
public:
	typedef enum
	{
		SourceTypeEqp = 1,
		SourceTypeSector = 2,
		SourceTypeMainPart = 3
	} MessageSourceType;

	StdEqpMessage(int type, bool reverse, const char *text);
	~StdEqpMessage() {}

	// Access
	inline const QString &getText(void) const { return text; }
	inline int getType(void) const { return type; }
	inline bool isReverse(void) const { return reverse; }

protected:
	// Message text
	QString				text;

	// Source type for this message - see enum above
	int					type;

	// Flag indicating if this message if for 'reverse' condition
	bool				reverse;
};

#endif	// STDEQPMESSAGE_H
