#ifndef	MESSAGEQUEUE_H
#define	MESSAGEQUEUE_H

#include "VacCtlOnlineNotificationExport.h"

// Queue of messages to be sent by PVSS script

#include <qstring.h>
#include <qlist.h>
#include <qmutex.h>

class MessageQueueItem;

class VACCTLONLINENOTIFICATION_EXPORT MessageQueue
{
public:
	static void addMessage(const QString &configName, const QString &message);
	static void addMessage(const QString &configName, const QString &message, const QString &msgBody);

	static MessageQueue &getInstance(void);

	bool getNext(QString &configName, QString &message, QString &msgBody);

	MessageQueue();
	~MessageQueue();

protected:
	void addMessage(MessageQueueItem *pItem);

	// The only instance of this class
	static MessageQueue	instance;

	// List of pending messages
	QList<MessageQueueItem *>	*pList;
	
	// Mutex to protect list from parallel access
	QMutex	mutex;
};

#endif	// MESSAGEQUEUE_H
