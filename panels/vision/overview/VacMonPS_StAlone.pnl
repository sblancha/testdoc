V 12
1
LANG:1 0 
PANEL,-1 -1 1100 657 N "_3DFace" 1
"$dataPart"
"main()
{
  // Get machine name
  dyn_string exceptionInfo;
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
    InitPb.text = \"Failed to read machine name\";
    return;
  }

  LhcVacStartLongAct(\"ProgressBar\");
  // Read all resources
  LhcVacStartLongActPart(10.);
  VacResourcesParseFile(PROJ_PATH + \"/data/VacCtlResourcesWeb.txt\", glAccelerator, exceptionInfo);
  LhcVacFinishLongActPart();
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }

  // Check if we shall display only part of all data
  if(isDollarDefined(\"$dataPart\"))
  {
    DebugTN(\"$dataPart\", $dataPart);
    LhcVacSetDataPart($dataPart);
  }

  // Initialize static (survey) accelerator data
  LhcVacStartLongActPart(20.);
  int coco = LhcVacEqpInitPassive(false, PROJ_PATH + \"/data/\", glAccelerator, exceptionInfo);
  LhcVacFinishLongActPart();
  if(coco < 0)
  {
    fwExceptionHandling_display(exceptionInfo);
    InitPb.text = \"Failed to initialize survey data\";
    LhcVacStartLongAct(\"\");
    return;
  }

  // Initialize device functionalities
  LhcVacStartLongActPart(10.);
  LhcVacInitDevFunc();
  LhcVacFinishLongActPart();

  LhcVacStartLongActPart(60.);
  coco = LhcVacRenewVacEqpData(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN(exceptionInfo);
    ConfigProblemText.visible = true;
  }
  else
  {
    ConfigProblemText.visible = false;
  }
		
  // Verify timestamp
  string pvssTimeStamp, dllTimeStamp;
  LhcVacGetDataTimeStamp(pvssTimeStamp, exceptionInfo);
  LhcVacGetTimeStamp(dllTimeStamp);
  if(pvssTimeStamp != dllTimeStamp)
  {
    ChildPanelOnCentralModal(\"vision/MessageInfo1\", \"WARNING\",
      makeDynString(\"PVSS data <\" + pvssTimeStamp +
      \">\\n DLL data <\" + dllTimeStamp + \">\"));
  }

  LhcVacFinishLongActPart();
  LhcVacStartLongAct(\"\");

  ShowLegendLabels();
  Layout.dataReady();	// Finish all data
  LayerOff(2);
  LayerOn(1);

  InitAccesModeIcons();

  // Connect to data reload progress DPE
  string dpeName = \"_VacMachineInfo.ImportInProgress\";
  if(dpExists(dpeName))
  {
    dpConnect(\"DataReloadCb\", dpeName);
  }

  /* UNICOS calls added to solve problem of \"UNICOS panels have controls not enabled\",
  ** solution provided by F.Bernard
  */
  /*
  unGraphicalFrame_getLibsAndFunctions(makeDynString(), g_dsLibs, g_dsHMIFunctions); 
  unGraphicalFrame_initHMI();
  */
  /* End of UNICOS by F.Bernard */

  Profile1.dataReady();
  Profile2.dataReady();
  Profile3.dataReady();
  Profile4.dataReady();
  Profile5.dataReady();
  Profile6.dataReady();
  Profile7.dataReady();
  Profile8.dataReady();
  Profile9.dataReady();
  Profile10.dataReady();
  Profile11.dataReady();
}" 0
 E E E E 1 -1 -1 0  10 50
""0  1
E "#uses \"VacCtlEqpDataPvss\"  // Load .so or DLL

#uses \"vclResources.ctl\"   // Load CTRL library
#uses \"vclProgress.ctl\"    // Load CTRL library
#uses \"vclEqpConfig.ctl\"   // Load CTRL library
#uses \"vclReplay.ctl\"      // Load CTRL library
#uses \"vclUi.ctl\"          // Load CTRL library (@SAA Help Support)
#uses \"vclPanelCommon.ctl\"

/* Variables added to solve problem of \"UNICOS panels have controls not enabled\",
** solution provided by F.Bernard
*/
/*
dyn_string g_dsDeviceType; 
int g_iCommitTime, g_iWarningTime; 
dyn_string g_dsAutologoutUsers; 
dyn_string g_dsConfiguredFrontEndType; 
dyn_string g_dsLibs, g_dsHMIFunctions; 
*/
/* End of variable by F.Bernard */


// Names of menus and commands behind every menu item
dyn_string    menuNames;
dyn_dyn_uint  menuItems;


// Read from resource and add to layout all geneda labels
void ShowLegendLabels()
{
  int n = 1;
  while(true)
  {
    string resource = glAccelerator + (isDollarDefined(\"$dataPart\") ? \"_\" + $dataPart : \"\") + \"_Legend\" + n;
    int x = VacResourcesGetValue(resource + \".X\", VAC_RESOURCE_INT, -1);
    if(x < 0)
    {
      break;
    }
    int y = VacResourcesGetValue(resource + \".Y\", VAC_RESOURCE_INT, -1);
    if(y < 0)
    {
      break;
    }
    int angle = VacResourcesGetValue(resource + \".Angle\", VAC_RESOURCE_INT, -10000);
    string text = VacResourcesGetValue(resource + \".Text\", VAC_RESOURCE_STRING, \"\");
    if(text == \"\")
    {
      break;
    }
    if((angle != 0) && (angle != -10000))
    {
      Layout.addRotatedLegendLabel(x, y, angle, text);
    }
    else
    {
      Layout.addLegendLabel(x, y, text);
    }
    n++;
  }
}

// Initialize all access mode icons
void InitAccesModeIcons()
{
  dyn_string mainParts;
  LhcVacGetAllMainParts(mainParts);
  string dataPart;
  if(isDollarDefined(\"$dataPart\"))
  {
    dataPart = $dataPart;
  }

  int iconIdx = 1;
  for(int n = dynlen(mainParts) ; n > 0 ; n--)
  {
    string resourceBase = \"AccessMode.\" +
      (dataPart != \"\" ? (dataPart + \".\") : \"\") + mainParts[n] + \".\";
    int x = VacResourcesGetValue(resourceBase + \"CoordinateX\", VAC_RESOURCE_INT, -1);
    if(x < 0)
    {
      continue;
    }
    int y = VacResourcesGetValue(resourceBase + \"CoordinateY\", VAC_RESOURCE_INT, -1);
    if(y < 0)
    {
      continue;
    }
    // Coordinates are defined - does DP exist?
    string dpName, dpeName;
    LhcVacGetDpeForAccessMode(mainParts[n], dpName, dpeName);
    if(!dpExists(dpeName))
    {
      // It is responsability of vcsAccessMode.ctl script to create
      // and PUBLISH via DIP access mode DPs - we use them 'as is'
      continue;
    }

    // Eveything exists - activate icon
    shape icon = getShape(\"Access\" + iconIdx);
    if(icon == 0)
    {
      DebugTN(\"MainSps.pnl:InitAccesModeIcons(): Access icon # \" +
        (iconIdx - 1) + \" does not exist\");
      break;
    }
    icon.dpName = dpName;
    icon.connect();
    icon.position(x, y);
    iconIdx++;
  }
  // Hide unused access icons
  for( ; iconIdx <= 20 ; iconIdx++)
  {
    shape icon = getShape(\"Access\" + iconIdx);
    if(icon == 0)
    {
      break;
    }
    icon.visible(false);
  }
}

// Callback for DPs which were requested by Layout EWO
void NewValueCb(string dpe, anytype value, string dpeTs, time ts)
{
  string dpeName = dpSubStr(dpe, DPSUB_DP_EL);
  LhcVacNewOnlineValue(dpeName, value, ts);
}

// Callback to be called when data import started on server
void DataReloadCb(string dpe, bool state)
{
  if(state)
  {
    LayerOn(8);
  }
}
" 0
 2
"CBRef" "1"
"EClose" E
""
DISPLAY_LAYER, 0 0 1 0 1 0 1 0 1 0 1 0 1 0 0 0
LAYER, 0 
1
LANG:1 0 
29 66
"Layout"
""
1 8 15.9588290233185 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
63 0 0 0 0 0
E E E
0
1
LANG:1 0 

2
"profileBarWidth" "int 80"
"readOnly" "bool TRUE"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  8 19 539 410
15 MainViewSps.ewo
1
0 "dpConnectRequest" "()"
"  DebugTN(\"dpConnectRequest()\"); 
  string dpe; 
  bool connect; 
  LhcVacNextDpConnect(dpe, connect); 
  while(dpe != \"\") 
  { 
//    DebugTN(dpe, connect);
    if(VacIsDriverOkDPE(dpe))
    {
      time now = getCurrentTime();
      LhcVacNewOnlineValue(dpe, true, now);
    }
    else
    {
      if(connect) 
      { 
        dpConnect(\"NewValueCb\", dpe, dpe + \":_original.._stime\"); 
      } 
      else 
      { 
        dpDisconnect(\"NewValueCb\", dpe, dpe + \":_original.._stime\"); 
      }
    }
    LhcVacNextDpConnect(dpe, connect); 
  }"
E15 7
"CLOCK1"
""
1 0.999999999999861 1 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
2 0 0 0 0 0
E E E
1
1
LANG:1 0 

0
1
LANG:1 25 Sans,-1,15,5,50,0,0,0,0,0
0  1 1 175 21
E 1 1 "%H:%M:%S" "%d.%m.%Y"
1 1 0 1
1
LANG:1 25 Sans,-1,15,5,50,0,0,0,0,0
0  1
LANG:1 25 Sans,-1,15,5,50,0,0,0,0,0
0 
2 10
"ConfigProblemText"
""
1 198 -2 E E E 1 E 0 E N "STD_used" E N "_Transparent" E E
 E E
5 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  1 E 198 -2 343 18
0 2 2 "0s" 0 0 0 64 0 0  198 -2 1
1
LANG:1 25 Sans,-1,15,5,50,0,0,0,0,0
0 1
LANG:1 22 Configuration problems
1 90 2 "" 5
0
1 91 2 "" 4
0
29 31
"Access1"
""
1 0 290 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
20 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  0 290 20 304
14 VacIconEwo.ewo
0
E29 32
"Access2"
""
1 0 307 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
22 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  0 307 20 321
14 VacIconEwo.ewo
0
E29 33
"Access3"
""
1 0 324 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
24 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  0 324 20 338
14 VacIconEwo.ewo
0
E29 34
"Access4"
""
1 0 342 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
26 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  0 342 20 356
14 VacIconEwo.ewo
0
E29 35
"Access5"
""
1 0 359 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
28 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  0 359 20 373
14 VacIconEwo.ewo
0
E29 36
"Access6"
""
1 24 290 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
30 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  24 290 44 304
14 VacIconEwo.ewo
0
E29 37
"Access7"
""
1 24 307 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
32 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  24 307 44 321
14 VacIconEwo.ewo
0
E29 38
"Access8"
""
1 24 324 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
34 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  24 324 44 338
14 VacIconEwo.ewo
0
E29 39
"Access9"
""
1 24 342 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
36 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  24 342 44 356
14 VacIconEwo.ewo
0
E29 40
"Access10"
""
1 24 359 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
38 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  24 359 44 373
14 VacIconEwo.ewo
0
E29 41
"Access11"
""
1 48 290 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
40 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  48 290 68 315
14 VacIconEwo.ewo
0
E29 42
"Access12"
""
1 48 307 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
42 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  48 307 68 321
14 VacIconEwo.ewo
0
E29 43
"Access13"
""
1 48 324 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
44 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  48 324 68 338
14 VacIconEwo.ewo
0
E29 44
"Access14"
""
1 48 342 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
46 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  48 342 68 356
14 VacIconEwo.ewo
0
E29 45
"Access15"
""
1 48 359 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
48 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  48 359 68 373
14 VacIconEwo.ewo
0
E29 46
"Access16"
""
1 72 290 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
50 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  72 290 92 304
14 VacIconEwo.ewo
0
E29 47
"Access17"
""
1 72 307 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
52 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  72 307 92 321
14 VacIconEwo.ewo
0
E29 48
"Access18"
""
1 72 324 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
54 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  72 324 92 338
14 VacIconEwo.ewo
0
E29 49
"Access19"
""
1 72 342 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
56 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  72 342 92 356
14 VacIconEwo.ewo
0
E29 50
"Access20"
""
1 72 359 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
58 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"eqpType" "enum 10000"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  72 359 92 373
14 VacIconEwo.ewo
0
E1 92 3 "" 1
0
29 70
"Profile1"
""
1 540 0 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
64 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"mainPartName" "string Linac2"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  540 0 1097 71
14 ProfileEwo.ewo
0
E29 74
"Profile2"
""
1 541 90 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
66 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"mainPartName" "string TL_Linacs"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  540 73 1097 144
14 ProfileEwo.ewo
0
E29 75
"Profile3"
""
1 542 180 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
68 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"mainPartName" "string BI-BT"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  540 146 1097 217
14 ProfileEwo.ewo
0
E29 76
"Profile4"
""
1 542 270 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
70 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"mainPartName" "string PSB"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  540 219 1097 290
14 ProfileEwo.ewo
0
E29 77
"Profile5"
""
1 544 469 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
72 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"mainPartName" "string BTY"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  542 365 1099 436
14 ProfileEwo.ewo
0
E29 78
"Profile6"
""
1 544 523 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
74 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"mainPartName" "string TT2"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  540 438 1097 509
14 ProfileEwo.ewo
0
E29 79
"Profile7"
""
1 576 612 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
76 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"mainPartName" "string Linac3"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  540 510 1097 581
14 ProfileEwo.ewo
0
E29 80
"Profile8"
""
1 546 701 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
78 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"mainPartName" "string LEIR"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  540 582 1097 653
14 ProfileEwo.ewo
0
E29 84
"Profile9"
""
1 545 292 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
79 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"mainPartName" "string PSR"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  540 292 1097 363
14 ProfileEwo.ewo
0
E29 88
"Profile11"
""
1 0 580 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
81 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"mainPartName" "string DE"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  0 580 531 651
14 ProfileEwo.ewo
0
E29 89
"Profile10"
""
1 540 512 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
83 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"mainPartName" "string AD"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  0 510 531 581
14 ProfileEwo.ewo
0
E0
LAYER, 1 
1
LANG:1 0 
29 13
"ProgressBar"
""
1 0 160 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
7 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"progress" "int 0"
"totalSteps" "int 1000"
"centerIndicator" "bool TRUE"
1
LANG:1 30 Sans Serif,9,-1,5,50,0,0,0,0,0
0  0 160 534 180
11 ProgressBar
0
E13 15
"InitPb"
""
1 90 200 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
9 0 0 0 0 0
E E E
1
1
LANG:1 0 

0
1
LANG:1 25 Sans,-1,15,5,50,0,0,0,0,0
0  88 198 442 232

T 
1
LANG:1 28 Initializing, please wait...
E E E E
0
LAYER, 2 
1
LANG:1 0 
0
LAYER, 3 
1
LANG:1 0 
0
LAYER, 4 
1
LANG:1 0 
0
LAYER, 5 
1
LANG:1 0 
0
LAYER, 6 
1
LANG:1 0 
0
LAYER, 7 
1
LANG:1 0 
13 16
"RealoadPb1"
""
1 20 50 E E E 1 E 1 E N "alarmGingUnq" E N "_Button" E E
 E E
10 0 0 0 0 0
E E E
1
1
LANG:1 0 

0
1
LANG:1 28 Courier,-1,20,5,75,0,0,0,0,0
0  18 48 535 101

T 
1
LANG:1 33 Data reload on server in progress
E E E E
13 17
"ReloadPb2"
""
1 20 230 E E E 1 E 1 E N "alarmGingUnq" E N "_Button" E E
 E E
12 0 0 0 0 0
E E E
1
1
LANG:1 0 

0
1
LANG:1 28 Courier,-1,20,5,75,0,0,0,0,0
0  18 228 526 279

T 
1
LANG:1 28 Please exit the application.
E E E E
0
3 2 "LogList" -1
"objects\\UN_MESSAGETEXT\\unMessageText.pnl" 20 562 T 19 1.38047206251874 0 2.47747747747748 0 -953.342342342342
0
3 3 "PANEL_REF4" -1
"vision\\objects\\lhcVacIcon_UnAlrm.pnl" 494 31 T 59 1 0 1 14 -32
0
0
