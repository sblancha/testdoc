// SMS/e-mail/ notification for vacuum interlock (vacOK)
#uses "VacCtlEqpDataPvss"	// Load DLL
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "email.ctl"	// Load PVSS standard CTRL library
#uses "vclDevCommon.ctl"	// Load CTRL library
#uses "vclOkSmsConfig.ctl"	// Load CTRL library

//#uses "VacEqpData"	// Load DLL or so
//#uses "lhcVacSectors.ctl"	// Load CTRL library
//#uses "lhcVacEqpConfig.ctl"	// Load CTRL library
//#uses "lhcVacOkSmsConfig.ctl"	// Load CTRL library
//#uses "lhcVacDevCommon.ctl"	// Load CTRL library


//********************** General configuration *********************************
// SMTP server
string smtpServer;

// E-mail sender
string sender;

// E-mail sender client
string	client;

//silence time
int dTime;
// Recipients for problems in all sectors
dyn_string	recipientsAll;

// Sectors with specific recipients
dyn_string	arcsSpecific;

// Recipients for specific sectors
dyn_string	recipientsSpecific;

// Sectors where notification is disabled
dyn_string	sectorsOff;

//Count alarm during dead time
int	alarmCount;
//Delay count
int	delayCount;
//Filter flag TRUE when is activated
bool	filterFlag;
string delaySector;
string delayName;
//Alert types
const unsigned VAC_OK_GREEN_FLAG = 1;
const unsigned VAC_OK_RED_FLAG = 2;
const unsigned VAC_OK_BLUE_FLAG = 3;
const unsigned VAC_OK_PLC_GOOD = 4;
const unsigned VAC_OK_PLC_BAD = 5;
const unsigned VAC_OK_PRESSURE = 6;
const unsigned VAC_OK_DELAY_ALARM = 7;


dyn_string		smsDevice, smsArc;
dyn_string		maskedPlc;
dyn_bool			maskedPlcState;		
dyn_dyn_time	mailTimes;
dyn_time			arcTimes;
dyn_anytype		msgQueue;
bool					initFlag = true;
//quaziGlobals
dyn_string glVacMainParts;
dyn_dyn_string glVacSectors, glSectDomains;

//************* Counter of E-mail send failures *******************************
// IF emSendMail() returned an error - the script does NOT remove message
// from message queue, so it will try to resend the message. However, there
// is a chance that first message will never be sent successfully, so all
// other messagesin the queue will be blocked forever.
// In order to avoid such blocking we introduce a counter of unsuccessfull
// attempts to send a message
int sendFailureCounter;
//List of gauge used to follow the pressure and report when pressure value exceed 1e-2 mBar
dyn_string	gaugeList;
//List of last pressures values 
dyn_float	  gaugePres;
//List of last gauge alarm 
dyn_bool	  gaugeAlarm;

// value used to compare with gauge pressure and report if it was exceeded;
const float	prAlertValue = 1e-2;

//==========================================================================
void main()
{
	// Initialize static data
	if( ! InitStaticData() ) return;

	// Connect to E-mail configuration DPEs
	if( ! ConnectToConfig() ) return;
//DebugN("vcsOkSms.ctl::ConnectToConfig -- OK");
	if( ! ConnectDeviceList() ) return;
//DebugN("vcsOkSms.ctl::ConnectDeviceList -- OK");
	
	LhcVacInitDevFunc( );

bool coco = InitGaugeData();
	// Wait forever, check for timing parameters from time to time
	initFlag = false;
	filterFlag = false;
	delayCount = alarmCount = 0;
	while( true )
	{
		delay( 2 );
		if(delayCount > 0)
		{
			delayCount--;
		}
		else if (filterFlag)
		{
			filterFlag = false;
			PrepareStatistic();
		}                 
		SendMessages( );
		if (!filterFlag)
			alarmCount = 0;
	}
}
PrepareStatistic()
{
	int	i;
	int nMaxmaskedPlc = dynlen(maskedPlc);
	alarmCount = 0;
	for (i=1; i<= nMaxmaskedPlc; i++)
		if(maskedPlcState[i])
			alarmCount++;
//DebugN("vcsOkSms.ctl: alarmCount", alarmCount);
	dynClear(maskedPlc);
	dynClear(maskedPlcState);
	if(alarmCount < 1)
		return;
	dynAppend( msgQueue, (string)delayName );
	dynAppend( msgQueue, (string)delaySector);
	dynAppend( msgQueue, (int) VAC_OK_DELAY_ALARM );

}
/** SendMessages
Purpose:
Send E-mails for all messages pending in the message queue

Parameters:
	- None

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void SendMessages( )
{
	dyn_string	recipients;
	int					n, coco;
	string 			alertMsg;

	while( dynlen( msgQueue ) > 1 )
	{
		string	vacOkDev = msgQueue[1];
		string	alarmSector = msgQueue[2];
		int			alarmType = msgQueue[3];
		// Find all recipients which shall be notified
		FindRecipients( alarmSector, recipients);

		if( dynlen( recipients ) == 0 )
		{
			dynRemove( msgQueue, 1 );
			dynRemove( msgQueue, 1 );
			dynRemove( msgQueue, 1 );
			continue;
		}
		// Prepare and send message
		dyn_string	email = makeDynString( "" );
		for( n = dynlen( recipients ) ; n > 0 ; n-- )
		{
			if( email[1] != "" ) email[1] += ";";
			email[1] += recipients[n];
		}
		email[2] = sender;
		switch( alarmType )
		{
		case VAC_OK_RED_FLAG:				// VAC ok is BAD
			email[3] = vacOkDev + " BAD";
			break;
		case VAC_OK_GREEN_FLAG:	// VAC ok is GOOD
			email[3] = vacOkDev + "GOOD";
			break;
		case VAC_OK_BLUE_FLAG:			// validity is bad
			email[3] = vacOkDev + " signal not valid";
			break;
		case VAC_OK_PLC_BAD:			// vacok PLC is bad
			email[3] = vacOkDev + " Alarm";
			break;
		case VAC_OK_PRESSURE:			// pressure cross an alarm level
			sprintf(alertMsg, "%6.2e", prAlertValue);
			email[3] = vacOkDev + " Pressure > " + alertMsg;
			break;
		case VAC_OK_DELAY_ALARM: //Messga about delaid PLC alarms
				sprintf(alertMsg, "still %d active PLC Alarm. Reset", alarmCount);
			email[3] = " Masked Alarm:  " + alertMsg;
			break;
		}
		email[4] = "";
		DebugTN( "vcsOkSms.ctl: message to " + email[1] + " is '" + email[3] + "'" );
		emSendMail( smtpServer, client, email, coco );
		if( coco < 0 )	// Failed, make pause then try again
		{
			if( sendFailureCounter < 3 )
			{
				sendFailureCounter++;
				return;
			}
		}
		sendFailureCounter = 0;	// Reset failures counter
		// Remove message from the queue
		dynRemove( msgQueue, 1 );
		dynRemove( msgQueue, 1 );
		dynRemove( msgQueue, 1 );
	}
}
/** FindRecipients
Purpose:
Find all recipients which shall be notified in case of problems
in given arc. The function also checks other conditions:
- if notification for sector is disabled
- if all parameters for mail sending are available

Parameters:
	- sector - [in] name of vacuum sector where alarm condition is detected
	- recipients - [out] list of recipients who shall be notified

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void FindRecipients( string sector, dyn_string &recipients)
{
	string arcName;
	dynClear( recipients );
	// Check if general E-mail notification parameters are available
	if( ( smtpServer == "" ) || ( client == "" ) || ( sender == "" ) )
		return;
	// Check if notification is disabled for this sector
	if( dynContains( sectorsOff, sector ) > 0 ) return;
	// Check if specific recipients for this sector exist
	int n;

	VacOkFindArcForSector(sector, arcName);
//DebugN("vcsOkSms.ctl: FindRecipientsForSectors:", sector, arcName);
	for( n = dynlen( arcsSpecific ) ; n > 0 ; n-- )
	{
		if( arcsSpecific[n] == arcName )
		{
			if( dynContains( recipients, recipientsSpecific[n] ) <= 0 )
			{
				string	recipient = recipientsSpecific[n];
				dynAppend( recipients, recipient );
			}
		}
	}
	if(VacOkSmsIsCryoSector( sector ))
	{
//DebugN("vcsOkSms.ctl: VacOkSmsIsCryoSector:", sector, arcName);

		for( n = dynlen( recipientsAll ) ; n > 0 ; n-- )
		{
			if( dynContains( recipients, recipientsAll[n] ) <= 0 )
			{
				string	tmp = recipientsAll[n];
				dynAppend( recipients, tmp );
			}
		}
	}

}
/** ReportAlarm
Purpose:
Check if alarm of given type for sector of given DP shall be sent. If so -
add parameters of message to be sent to the message queue

Parameters:
	- dpName - [in] name of DP where alarm condition is detected
	- alarmType - [in] type of alarm detected, see ALARM_TYPE_xxx constants

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void ReportAlarm( string dpName, int alarmType, string alarmDp)
{
	dyn_string	recipients, exceptionInfo;
	string	sector1, sector2, mainPart, arcName, displName;
	string	nameToCheck;
	int	mailSectorIdx, mailArcIdx;
//DebugTN("vcsOkSms.ctl: ReportAlarm(" + dpName + "," + alarmType + ")" );
	// Find sector where device is located
	int	vacType;
	bool		isBorder, coco;
	LhcVacDeviceVacLocation( dpName, vacType, sector1, sector2, mainPart, isBorder, exceptionInfo);
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "vcsOkSms.ctl: ERROR: ReportAlarm(): LhcVacDeviceVacLocation(" + dpName + ") failed:",
			exceptionInfo );
		return;
	}
	if( sector1 == "" )
	{
		DebugTN( "vcsOkSms.ctl: ERROR: ReportAlarm(): no sector for <" + dpName + ">" );
		return;
	}
	// Check if notification is disabled for this sector
	if( dynContains( sectorsOff, sector1 ) > 0 )
	{
	// DebugTN("vcsOkSms.ctl: INFO: sector " + sector1 + " of " + dpName + " deactivated" );
		return;
	}
//Find corresponding Arc
	string alarmSector = sector1;
	// Check if there are any recipienta for the event
	if(( alarmType == VAC_OK_PLC_BAD)||( alarmType == VAC_OK_PLC_GOOD))
		nameToCheck = alarmDp;
	else
	{
		LhcVacDisplayName( dpName, displName, exceptionInfo );
		if( dynlen( exceptionInfo ) > 0 )
		{
			DebugTN( "vcsOkSms.ctl: ERROR: LhcVacDisplayName():" + nameToCheck + ") failed:",
				exceptionInfo );
			return;
		}
		nameToCheck = displName;
	}
	for( int n = dynlen( msgQueue ) - 1 ; n > 0 ; n -= 3 )
	{

		if( msgQueue[n] != nameToCheck ) continue;
		if( msgQueue[n+2] == alarmType )
		{
			/*
			DebugTN("vcsOkSms.ctl: INFO: message for " + nameToCheck + " of type " +
				alarmType + " is already in the queue" );
			*/
			return;	// Message is already in the queue
		}
	}
	// Check if notification of this type for this sector was sent not long time ago
	// nnn minutes dead time ????DebugN("smsDevice:", smsDevice);
	time	now = getCurrentTime();
	switch(alarmType)
	{
		case VAC_OK_PRESSURE:		// VPG pressure > 1e10-2 - block all message except first from arc
	 		coco = VacOkFindArcForSector(alarmSector, arcName );
			mailArcIdx = dynContains( smsArc, arcName );
			if( mailArcIdx > 0 )		//arc already in mailing list
			{
				if( ( now - arcTimes[mailArcIdx]) < ( 60 * dTime ) )
				{
					DebugTN("vcsOkSms.ctl: INFO: message for Arc:" + arcName + " was sent less than " + dTime + " minutes ago" );
		 			return;
				}
				else
				{
					arcTimes[mailArcIdx] = getCurrentTime();;
				}
			}
			else
			{
				mailArcIdx = dynAppend( smsArc, arcName );
				time	farInThePast = makeTime( 1980, 1, 1 );
				dynAppend(arcTimes, farInThePast);
				arcTimes[mailArcIdx] = getCurrentTime();;
			}
			break;
		case VAC_OK_PLC_GOOD: // PLC ALARM
			if(filterFlag == true)
			{
				int alarmId = dynContains(maskedPlc, alarmDp);
				if(alarmId > 0)
					 maskedPlcState[alarmId] = false;
				else
				{
					dynAppend(maskedPlc, alarmDp);
					dynAppend(maskedPlcState, false);
				}
			}
			return;
			break;
		case VAC_OK_PLC_BAD: // PLC ALARM
			if(filterFlag == false)
			{
				filterFlag = true;
				delayCount = 30;
				delaySector = alarmSector;
				delayName = nameToCheck;

			}
			else
			{
//Find alarm name in the list
				int alarmId = dynContains(maskedPlc, alarmDp);
				if(alarmId > 0)
					 maskedPlcState[alarmId] = true;
				else
				{
					dynAppend(maskedPlc, alarmDp);
					dynAppend(maskedPlcState, true);
				}
				return;
			}
			break;
		default:						//all other alarms come here
			mailSectorIdx = dynContains( smsDevice, nameToCheck );
			if( mailSectorIdx > 0 )
			{
//DebugN("Found in the list:", nameToCheck, mailSectorIdx, alarmType);

				if( ( now - mailTimes[mailSectorIdx][alarmType] ) < ( 60 * dTime ) )
				{
					DebugTN("vcsOkSms.ctl: INFO: message for " + nameToCheck + " of type " +
						alarmType + " was sent less than " + dTime + " minutes ago" );
		 		return;
				}
			}
	// Put timestamp when message was sent last time
			int	mailSectorIdx = dynContains( smsDevice, nameToCheck );
			if( mailSectorIdx <= 0 )
			{
				mailSectorIdx = dynAppend( smsDevice, nameToCheck );
				time	farInThePast = makeTime( 1980, 1, 1 );
				dyn_time newMailTimes;
				for(int n = 1 ; n <= 4 ; n++ )
				{
					dynAppend( newMailTimes, ( alarmType == n ? getCurrentTime() : farInThePast ) );
				}
				dynAppend( mailTimes, newMailTimes );
			}
			else
			{
				mailTimes[mailSectorIdx][alarmType] = getCurrentTime();
			}
			break;
	}
	dynAppend( msgQueue, (string)nameToCheck );
	dynAppend( msgQueue, (string)alarmSector);
	dynAppend( msgQueue, (int)alarmType );
}

/* InitStaticData
Purpose:
Read all machine configuration data from file(s)

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool InitStaticData( )
{
	dyn_string	exceptionInfo;
	long				coco;
	bool				isLhcData;

	// Set accelerator name to be used by other components
	LhcVacGetMachineId( exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "vcsOkSms.ctl: Failed to read machine name: " + exceptionInfo );
		return false;
	}
	DebugTN( "vcsOkSms.ctl: Machine is " + glAccelerator );
	isLhcData = glAccelerator == "LHC" ? true : false;
	// Initalize passive machine data
	coco = LhcVacEqpInitPassive( isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo );
	if( coco < 0 )
	{
		DebugTN( "vcsOkSms.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo );
		return false;
	}
	DebugTN( "vcsOkSms.ctl: LhcVacEqpInitPassive() done" );

	// Initialzie active equipment information
	coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
			exceptionInfo );
	if( coco < 0 )
	{
		DebugTN( "vcsOkSms.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo );
		return false;
	}
	DebugTN( "vcsOkSms.ctl: LhcVacEqpInitActive() done" );

	DebugTN( "vcsOkSms.ctl: InitStaticData() done" );
	return true;
}
/** ConnectToConfig
Purpose:
Connect to DPEs containing E-mail notification configuration

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToConfig( )
{
	string				configDp;
	dyn_string		exceptionInfo;
	dyn_errClass	err;

	VacSmsGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "vcsOkSms.ctl: FATAL: " + exceptionInfo );
		return false;
	}

	if( dpConnect( "ServerCb", configDp + ".server" ) < 0 )
	{
		err = getLastError();
		DebugTN( "vcsOkSms.ctl: FATAL: dpConnect(" + configDp + ".server) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "ClientCb", configDp + ".client" ) < 0 )
	{
		err = getLastError();
		DebugTN( "vcsOkSms.ctl: FATAL: dpConnect(" + configDp + ".client) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "SenderCb", configDp + ".sender" ) < 0 )
	{
		err = getLastError();
		DebugTN( "vcsOkSms.ctl: FATAL: dpConnect(" + configDp + ".sender) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "DeadTimeCb", configDp + ".deadTime" ) < 0 )
	{
		err = getLastError();
		DebugTN( "vcsOkSms.ctl: FATAL: dpConnect(" + configDp + ".deadTime) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "RecipientsAllCb", configDp + ".recipientsAll" ) < 0 )
	{
		err = getLastError();
		DebugTN( "vcsOkSms.ctl: FATAL: dpConnect(" + configDp + ".recipientsAll) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "SpecificCb", configDp + ".arcsSpecific",
		configDp + ".recipientsSpecific" ) < 0 )
	{
		err = getLastError();
		DebugTN( "vcsOkSms.ctl: FATAL: dpConnect(" + configDp + ".arcsSpecific) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "DisableCb", configDp + ".sectorsOff")< 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacOkSms.ctl: FATAL: dpConnect(" + configDp + ".sectorsOff) failed" );
		throwError( err );
		return false;
	}
	return true;
}
// Callbacks from DP containing general E-mail configuration
void ServerCb( string dpe, string newServer )
{
	smtpServer = newServer;
}
void ClientCb( string dpe, string newClient )
{
	client = newClient;
}
void SenderCb( string dpe, string newSender )
{
	sender = newSender;
}
void DeadTimeCb( string dpe, int newDTime )
{
	dTime = newDTime;
}
void RecipientsAllCb( string dpe, dyn_string newRecipientsAll )
{
	recipientsAll = newRecipientsAll;
}
void SpecificCb( string dpe1, dyn_string newArcsSpecific, string dpe2,
	dyn_string newRecipientsSpecific )
{
	arcsSpecific = newArcsSpecific;
	recipientsSpecific = newRecipientsSpecific;
}
void DisableCb( string dpe1, dyn_string newSectorsOff)
{
	sectorsOff = newSectorsOff;
}
//***********************************************************************************
// Get names of all vacOK devices belonging to sectors selected in sector list,
// add connect to dp
bool ConnectDeviceList( )
{
	int 		n, coco, listLen, vac, sectorLen;
	string		sectName, dpName, plcDp, alarmDp, mainPart, devName, dpType;
	dyn_string	exceptionInfo, deviceList, sectorList, err;
	if( LhcVacGetAllSectors( glVacMainParts, glVacSectors, glSectDomains, exceptionInfo ) < 0 )
	{
		DebugTN("vcsOkSms.ctl: LhcVacGetAllSectors() failed:", exceptionInfo );
		return -1;
	}
	DebugTN( "vcsOkSms.ctl: LhcVacGetAllSectors() done" );
	for( n = 1 ; n <= dynlen(glVacMainParts) ; n++ )
	{
		dynAppend(sectorList, glVacSectors[n]);
	}
//DebugN("ConnectDeviceList::SectList:", dynlen(sectorList));
	LhcVacGetDevicesAtSectors( sectorList, deviceList, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		fwExceptionHandling_display( exceptionInfo );
		return false;
	}

	// Display returned devices in table
	listLen = dynlen( deviceList );
//DebugN("ConnectDeviceList::deviceList:", listLen);
	for( n = 1 ; n <= listLen ; n++ )
	{
		dpName =  deviceList[n];
		dynClear( exceptionInfo );
		LhcVacDpType( dpName, dpType, exceptionInfo );
		if( dynlen( exceptionInfo ) > 0 )
		{
			fwExceptionHandling_display( exceptionInfo );
			continue;
		}
		LhcVacDevicePlc( dpName, plcDp, alarmDp, exceptionInfo );
		if( dynlen( exceptionInfo ) > 0 )
		{
			fwExceptionHandling_display( exceptionInfo );
			continue;
		}
//if(strlen(alarmDp) > 0);
//  DebugN("Plc+Alarm", dpName, plcDp, alarmDp );
		switch( dpType )
		{
		case "CRYO_VACOK_RO":
		case "CRYO_VACOK_PLC":
		case "CRYO_VACOK":
			if( alarmDp != "" )
				dpConnect("GetVacOkPlc", false, dpName + ".RR1:_online.._value",
				alarmDp + ".alarm" );
			else
				dpConnect("GetVacOk", false, dpName + ".RR1:_online.._value");
			err = getLastError( );
			if( dynlen( err ) > 0 )
			{
			 	DebugTN( dpName, err );
				return false;
			}
			break;
		}
	}
	return true;
}
void GetVacOk ( string dp1, unsigned stateRR1)
{
	ProcessVacOk ( dp1, stateRR1, "", 0);
}
void GetVacOkPlc ( string dp1, unsigned stateRR1, string alarmDp, unsigned alarm)
{
//DebugN("GetVacOkPlc",dp1,alarmDp, alarm);
  	ProcessVacOk ( dp1, stateRR1, alarmDp, alarm);
}
void ProcessVacOk ( string dp, unsigned stateRR1, string alarmDp, unsigned alarm)
{
	string			dpName, dpType, mainPart, sAlarmDp, tmp;
	string			arcName, sector1, sector2;
	bool 				isBorder;	
	int					vac, alarmType, strPos;
	dyn_string  exceptionInfo;  
	unsigned    state, errCode;
	dpName = dpSubStr( dp, DPSUB_DP );
	tmp = dpSubStr( alarmDp, DPSUB_DP );
	strPos = strpos(tmp, DPE_pattern);
	sAlarmDp = substr (tmp, strPos + strlen(DPE_pattern));
	errCode = stateRR1 & 0xffu;
	state = (stateRR1 & 0xff00u)>>8;
//DebugN("PLCAlarm", dp, alarmDp, alarm);
	if(alarm != 0)
//PlcAlarm
	{
		alarmType = VAC_OK_PLC_BAD;
		ReportAlarm(dpName, alarmType, sAlarmDp);
		return;
	}
	else if (strlen(alarmDp) > 0)
	{
		alarmType = VAC_OK_PLC_GOOD;
		ReportAlarm(dpName, alarmType, sAlarmDp);
	}
	if ( (state & 0x40u ) != 0 )
	{
		if(state & 0x1u)
//Came to OK	
		{
			alarmType = VAC_OK_GREEN_FLAG;
			return;
		}
		else
//RED -> reporting
			alarmType = VAC_OK_RED_FLAG;
	}
	else
	{
//Bad validity				
		alarmType = VAC_OK_BLUE_FLAG;
	}

	ReportAlarm(dpName, alarmType, "");
}

bool InitGaugeData( )
{
	int					n, vacType, i, nVpci;
	dyn_string	exceptionInfo, eqpList, err;
	string			dpType, eqpName, ctlParent;
	unsigned		dpTypeMask;

	dynClear( gaugeList );
	for( n = dynlen( glVacMainParts ) ; n > 0 ; n-- )
	{
		LhcVacGetVacTypeOfMainPart( glVacMainParts[n], vacType );
		//Look for  QRL/CRYO vacuum (main devices are gauges)
		if( ( vacType & ( LVA_VACUUM_QRL | LVA_VACUUM_CRYO ) ) != 0 )	// QRL/CRYO
		{
			// Build list of gauges
			LhcVacGetDevicesAtMainParts( makeDynString( glVacMainParts[n] ), eqpList, exceptionInfo );
			if( dynlen( exceptionInfo ) > 0 )
			{
				DebugTN( "vcsOkSms: LhcVacGetDevicesAtMainParts(" + glVacMainParts[n] +
					") failed: " + exceptionInfo );
				dynClear( exceptionInfo );
				continue;
			}
			for( i = dynlen( eqpList ) ; i > 0 ; i-- )
			{
				LhcVacDpType( eqpList[i], dpType, exceptionInfo );
				if( dynlen( exceptionInfo ) > 0 )
				{
					DebugTN( "vcsOkSms: LhcVacDpType(" + eqpList[i] +
						") failed: " + exceptionInfo );
					dynClear( exceptionInfo );
					continue;
				}
				dpTypeMask = LhcVacDpTypeGroup( dpType );
				if( ( dpTypeMask == VAC_DP_TYPE_GROUP_VGR ) != 0 )
				{

					// Ignore gauges which are part of pumping group
					LhcVacGetAttribute( eqpList[i], "CtlParent", ctlParent );
					if( ctlParent == "" )
					{
						eqpName = eqpList[i];
						dynAppend( gaugeList, (string)eqpName );
						dynAppend( gaugePres, 1e-3);
						dynAppend( gaugeAlarm, false);
						dpConnect("GetPressure", true, eqpName + ".PR", eqpName + ".RR1:_online.._value");
						err = getLastError( );
						if( dynlen( err ) > 0 )
						{
			 				DebugTN("vcsOkSms.ctl: dpConnect(" + eqpName + ") failed", err );
							return false;
						}
					}
				}
			}
		}
	}
	return true;
}
void GetPressure ( string dpe1, float pressure, string dpe2, unsigned state)
{
	ProcessPressure ( dpe1, pressure,  dpe2,  state);
}
void ProcessPressure (string dp, float pressure, string dpe2, unsigned state)
{
int			id, alarmType;
float		lastValue;
string	dpName;
	if((state & VG_SPS_R_PRESS_VALID) == 0)
		return;
	dpName = dpSubStr(dp, DPSUB_DP );
	id = dynContains(gaugeList, dpName);
//DebugN("Event:", dpName, pressure, id);
	if(id)
	{
//DebugN("InitOrBad:", id, initFlag, pressure, gaugePres[id]);
		if ((initFlag) || (gaugePres[id] >= prAlertValue))
//Either init phaze or pressure already crossed alert level -> do not report just store the value
		{
			gaugePres[id] = pressure;
			return;
		}
//Event of pressure crossing alert level
		else
		{
//DebugN("EventProcessed:", dp, pressure, gaugePres[id]);
			gaugePres[id] = pressure;
			alarmType = VAC_OK_PRESSURE;
			if(pressure >= prAlertValue)
			{
				if(gaugeAlarm[id] == false)
				{
					ReportAlarm(dpName, alarmType, "");
					dpSet(dpName+".AL1", true);
					gaugeAlarm[id] = true;
				}
				else	
					gaugeAlarm[id] = false;
			}
			else
			{
				if(gaugeAlarm[id] == true)
				{
					dpSet(dpName+".AL1", false);
				}
				else	
					gaugeAlarm[id] = false;
			}
			return;
		}
	}
}

