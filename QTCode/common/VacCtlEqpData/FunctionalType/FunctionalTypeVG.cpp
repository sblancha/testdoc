//	Implementation of FunctionalTypeVG class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVG.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include <stdio.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeVG::FunctionalTypeVG(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVG::fillCriteria(FunctionalType *pType)
{
	switch(type)
	{
	case FunctionalType::VGM:
		fillCriteriaVGM(pType);
		break;
	case FunctionalType::VGR:
		fillCriteriaVGR(pType);
		break;
	case FunctionalType::VGP:
		fillCriteriaVGP(pType);
		break;
	case FunctionalType::VGI:
		fillCriteriaVGI(pType);
		break;
	case FunctionalType::VGF:
		fillCriteriaVGF(pType);
		break;
	case FunctionalType::VPGMPR:
		fillCriteriaVPGMPR(pType);
		break;
	case FunctionalType::VGTR:
		fillCriteriaVGTR(pType);
		break;
	case FunctionalType::VG_A_RO:
		fillCriteriaVG_A_RO(pType); 
		break;
	default:
		break;
	}
}

void FunctionalTypeVG::fillCriteriaVGM(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "Membrane Gauge is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Membrane gauge is OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Membrane gauge is not OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Membrane gauge is OFF in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Membrane gauge is not OFF in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "Membrane Gauge is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Membrane gauge is ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Membrane gauge is not ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Membrane gauge is ON in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Membrane gauge is not ON in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, 0, "Membrane Gauge Pressure above limit");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Membrane gauge pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Membrane gauge pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Membrane gauge pressure {value} in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Membrane gauge pressure {value} in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "Membrane gauge ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Membrane gauge in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Membrane gauge in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Membrane gauge in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Membrane gauge in main part {mainPart}: no error");
}

void FunctionalTypeVG::fillCriteriaVGR(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria =  pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "Pirani Gauge is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Pirani gauge is OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Pirani gauge is not OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Pirani gauge is OFF in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Pirani gauge is not OFF in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "Pirani Gauge is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Pirani gauge is ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Pirani gauge is not ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Pirani gauge is ON in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Pirani gauge is not ON in main part {mainPart}");


	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, 0, "Pirani gauge Pressure above limit");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Pirani gauge pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Pirani gauge pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Pirani gauge pressure {value} in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Pirani gauge pressure {value} in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "Pirani gauge ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Pirani gauge in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Pirani gauge in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Pirani gauge in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Pirani gauge in main part {mainPart}: no error");
}

void FunctionalTypeVG::fillCriteriaVGP(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria =  pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "Penning Gauge is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Penning gauge is OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Penning gauge is not OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Penning gauge is OFF in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Penning gauge is not OFF in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "Penning Gauge is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Penning gauge is ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Penning gauge is not ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Penning gauge is ON in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Penning gauge is not ON in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, 0, "Penning Gauge Pressure above limit");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Penning gauge pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Penning gauge pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Penning gauge pressure {value} in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Penning gauge pressure {value} in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "Penning gauge ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Penning gauge in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Penning gauge in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Penning gauge in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Penning gauge in main part {mainPart}: no error");
}

void FunctionalTypeVG::fillCriteriaVGI(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria =  pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "Ion Gauge is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Ion gauge is OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Ion gauge is not OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Ion gauge is OFF in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Ion gauge is not OFF in main part {mainPart}");


	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "Ion Gauge is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Ion gauge is ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Ion gauge is not ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Ion gauge is ON in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Ion gauge is not ON in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, 0, "Pressure above limit");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Ion gauge pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Ion gauge pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Ion gauge pressure {value} in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Ion gauge pressure {value} in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "Ion gauge ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Ion gauge in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Ion gauge in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Ion gauge in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Ion gauge in main part {mainPart}: no error");
}

void FunctionalTypeVG::fillCriteriaVGF(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria =  pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "Full Range Gauge is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Full range gauge is OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Full range gauge is not OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Full range gauge is OFF in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Full range gauge is not OFF in main part {mainPart}");


	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "Full Range Gauge is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Full range gauge is ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Full range gauge is not ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Full range gauge is ON in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Full range gauge is not ON in main part {mainPart}");


	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, 0, "Full Range Pressure above limit");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Full range gauge pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Full range gauge pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Full range gauge pressure {value} in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Full range gauge pressure {value} in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "Full range gauge ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Full range gauge in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Full range gauge in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Full range gauge in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Full range gauge in main part {mainPart}: no error");
}

void FunctionalTypeVG::fillCriteriaVPGMPR(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria =  pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "Full Range Gauge is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Gauge of VPGM is OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Gauge of VPGM gauge is not OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Gauge of VPGM is OFF in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Gauge of VPGM gauge is not OFF in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "Gauge of VPGM is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Gauge of VPGM is ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Gauge of VPGM gauge is not ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Gauge of VPGM is ON in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Gauge of VPGM gauge is not ON in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, 0, "Gauge of VPGM Pressure above limit");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Gauge of VPGM pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Gauge of VPGM pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Gauge of VPGM pressure {value} in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Gauge of VPGM pressure {value} in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "Gauge of VPGM ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Gauge of VPGM in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Gauge of VPGM in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Gauge of VPGM in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Gauge of VPGM in main part {mainPart}: no error");
}

void FunctionalTypeVG::fillCriteriaVGTR(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "Pressure Transmitter is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Pressure Transmitter is OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Pressure Transmitter is not OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Pressure Transmitter is OFF in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Pressure Transmitter is not OFF in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "Pressure Transmitter is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Pressure Transmitter is ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Membrane gauge is not ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Pressure Transmitter is ON in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Pressure Transmitter is not ON in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, 0, "Pressure Transmitter Pressure above limit");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: pressure {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Pressure Transmitter pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Pressure Transmitter pressure {value} in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Pressure Transmitter pressure {value} in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Pressure Transmitter pressure {value} in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "Pressure Transmitter ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Pressure Transmitter in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Pressure Transmitter in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Pressure Transmitter in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Pressure Transmitter in main part {mainPart}: no error");
}

void FunctionalTypeVG::fillCriteriaVG_A_RO(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "Gauge is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Analog gauge is OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Analog gauge is not OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Analog gauge is OFF in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Analog gauge is not OFF in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "Gauge is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Analog gauge is ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Analog gauge is not ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Analog gauge is ON in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Analog gauge is not ON in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "Gauge ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Analog gauge in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Analog gauge in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Analog gauge in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Analog gauge in main part {mainPart}: no error");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeVG::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case Off:
			result = "OFF";
			break;
		case On:
			result = "ON";
			break;
		default:
			break;
		}
		break;
	case EqpMsgCriteria::MainValue:
		result = "PR";
		break;
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeVG::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "OFF"))
		{
			result = Off;
		}
		else if(!strcasecmp(name, "ON"))
		{
			result = On;
		}
		break;
	case EqpMsgCriteria::MainValue:
		if(!strcasecmp(name, "PR"))
		{
			return 0;
		}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}

