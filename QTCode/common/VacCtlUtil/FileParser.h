// FileParser.h: interface for the FileParser class.
//
//////////////////////////////////////////////////////////////////////

#ifndef FILEPARSER_H
#define FILEPARSER_H

#include "VacCtlUtilExport.h"

#include <stdio.h>
#include <stdlib.h>

#include <QString>

#include <QStringList>

/**
**	Class encapsulating basic functionality of parsing input text file:
**	read file line by line;
**	recognize empty lines or lines with comments, skip them;
**	split every 'useful' line to single tokens separated by '=' and/or spaces.
**	Processing (i.e. interpretation) or every line's tokens is not
**	implemented in this class - implementation is left to subclasses
**	of this class.
**
**	Class may parse file in two major modes:
**	1) 'Normal' mode - string read from file is split to single words
**	2) 'Resource' - string read from single line is split to at most 2
**		parts, separator is '=' character.
**	Default mode is 'Normal'
**
*/

class VACCTLUTIL_EXPORT FileParser  
{
public:
	FileParser();
	virtual ~FileParser();

	int parseFile(const char *pFileName, QStringList &errList);
	int parseFile(FILE *f, int &lineNo, QStringList &errList);
	static char *pathToSystem(const char *pFileName);
	static const char *fileVersion(const char *pFileName);

protected:
	// Parsing mode
	enum
	{
		ParseNormal = 0,
		ParseResources = 1
	};
	int	mode;

	bool nextFileLine(FILE *f, int &lineNo);
	bool splitLine(char *line);
	bool splitLineNormal(char *pText);
	bool splitLineResource(char *pText);
	char *trim(char *inName);
	void addToken(const char *token);
	void freeTokens();
	int getNumberOfTokens();
	QString getCurrentToken(int index);

	// No implementation for the following method, subclasses shall
	// implement it. Return value from method shall be:
	// > 0  - line tokens have been processed successfully , more lines expected
	// == 0 - line tokens have been processed successfully, no more lines expected
	// < 0  - line tokens can not be processed, i.e. error in input file
	// In the latter case method shall write message explaining error reason to
	// pErrMsg argument. There are few macros below which can help in this.
	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList) = 0;
private:
	char	**tokens;
	short	*maxTokenSize;
	short	nTokens;
	short	maxTokens;
	QString attrLongValue;
};

// Macro to compare number of tokens from line with expected number of words
// The macro checks if token count from file exactly equals to expected
#ifdef Q_OS_WIN

#define CHECK_TOKEN_COUNT(nTokens, nExpected, lineNo, funcName) \
if(nTokens != nExpected) \
{ \
	char	buf[1024]; \
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "Line %d: bad format for <%s>: %s: %d expected, %d found", \
		(lineNo), tokens[0], funcName, nExpected, nTokens); \
	errList.append(buf); \
	return -1; \
}

#else

#define CHECK_TOKEN_COUNT(nTokens, nExpected, lineNo, funcName) \
if(nTokens != nExpected) \
{ \
	char	buf[1024]; \
	sprintf(buf, "Line %d: bad format for <%s>: %s: %d expected, %d found", \
		(lineNo), tokens[0], funcName, nExpected, nTokens); \
	errList.append(buf); \
	return -1; \
}

#endif

// Macro to compare number of tokens from line with expected number of words
// The macro checks if token count from is AT LEAST equals expected
#ifdef Q_OS_WIN

#define CHECK_MIN_TOKEN_COUNT(nTokens, nExpected, lineNo, funcName) \
if(nTokens < nExpected) \
{ \
	char	buf[1024]; \
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "Line %d: bad format for <%s>: %s: at least %d expected, %d found", \
		(lineNo), tokens[0], funcName, nExpected, nTokens); \
	errList.append(buf); \
	return -1; \
}

#else

#define CHECK_MIN_TOKEN_COUNT(nTokens, nExpected, lineNo, funcName) \
if(nTokens < nExpected) \
{ \
	char	buf[1024]; \
	sprintf(buf, "Line %d: bad format for <%s>: %s: at least %d expected, %d found", \
		(lineNo), tokens[0], funcName, nExpected, nTokens); \
	errList.append(buf); \
	return -1; \
}

#endif

// Macro to compare number of tokens from line with expected number of words
// The macro checks if token count from is in range between minimum and maximum expected
#ifdef Q_OS_WIN

#define CHECK_TOKEN_COUNT_RANGE(nTokens, minExpected, maxExpected, lineNo, funcName) \
if( nTokens < minExpected ) \
{ \
	char	buf[1024]; \
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "Line %d: bad format for <%s>: %s: at least %d expected, %d found", \
		(lineNo), tokens[0], funcName, minExpected, nTokens); \
	errList.append(buf); \
	return -1; \
}\
if(nTokens > maxExpected) \
{ \
	char	buf[1024]; \
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "Line %d: bad format for <%s>: %s: at most %d expected, %d found", \
		(lineNo), tokens[0], funcName, maxExpected, nTokens); \
	errList.append(buf); \
	return -1; \
}

#else

#define CHECK_TOKEN_COUNT_RANGE(nTokens, minExpected, maxExpected, lineNo, funcName) \
if( nTokens < minExpected ) \
{ \
	char	buf[1024]; \
	sprintf(buf, "Line %d: bad format for <%s>: %s: at least %d expected, %d found", \
		(lineNo), tokens[0], funcName, minExpected, nTokens); \
	errList.append(buf); \
	return -1; \
}\
if(nTokens > maxExpected) \
{ \
	char	buf[1024]; \
	sprintf(buf, "Line %d: bad format for <%s>: %s: at most %d expected, %d found", \
		(lineNo), tokens[0], funcName, maxExpected, nTokens); \
	errList.append(buf); \
	return -1; \
}

#endif

// Few macros to report parsing errors - the difference is just a number of arguments
#ifdef Q_OS_WIN

#define PARSING_ERR(fmt, arg1) \
{ \
	char	buf[1024]; \
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), fmt, arg1); \
	errList.append(buf); \
	return -1; \
}

#define PARSING_ERR2(fmt, arg1, arg2) \
{ \
	char	buf[1024]; \
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), fmt, arg1, arg2); \
	errList.append(buf); \
	return -1; \
}

#define PARSING_ERR3(fmt, arg1, arg2, arg3) \
{ \
	char	buf[1024]; \
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), fmt, arg1, arg2, arg3); \
	errList.append(buf); \
	return -1; \
}

#else

#define PARSING_ERR(fmt, arg1) \
{ \
	char	buf[1024]; \
	sprintf(buf, fmt, arg1); \
	errList.append(buf); \
	return -1; \
}

#define PARSING_ERR2(fmt, arg1, arg2) \
{ \
	char	buf[1024]; \
	sprintf(buf, fmt, arg1, arg2); \
	errList.append(buf); \
	return -1; \
}

#define PARSING_ERR3(fmt, arg1, arg2, arg3) \
{ \
	char	buf[1024]; \
	sprintf(buf, fmt, arg1, arg2, arg3); \
	errList.append(buf); \
	return -1; \
}

#endif

// Few macros for parsing warnings - the major difference from errors is that
// waring does not contain return
#ifdef Q_OS_WIN

#define PARSING_WARNING1(fmt, arg1) \
{ \
	char	buf[1024]; \
	sprintf_s(buf,  sizeof(buf) / sizeof(buf[0]), fmt, arg1); \
	errList.append(buf); \
}

#define PARSING_WARNING2(fmt, arg1, arg2) \
{ \
	char	buf[1024]; \
	sprintf_s(buf,  sizeof(buf) / sizeof(buf[0]), fmt, arg1, arg2); \
	errList.append(buf); \
}

#else

#define PARSING_WARNING1(fmt, arg1) \
{ \
	char	buf[1024]; \
	sprintf(buf, fmt, arg1); \
	errList.append(buf); \
}

#define PARSING_WARNING2(fmt, arg1, arg2) \
{ \
	char	buf[1024]; \
	sprintf(buf, fmt, arg1, arg2); \
	errList.append(buf); \
}

#endif

#endif // !defined(FILEPARSER_H)
