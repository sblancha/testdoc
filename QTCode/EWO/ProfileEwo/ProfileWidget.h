#ifndef PROFILEWIDGET_H
#define	PROFILEWIDGET_H

// Widget to be used in EWO for PS/SPS profile

#include "DataEnum.h"
#include "VacEqpTypeMask.h"

#include <qwidget.h>

class ProfileImage;
class ExtendedLabel;
class DataPool;
class MainPart;
class Sector;

class ProfileWidget : public QWidget
{
	Q_OBJECT

	Q_PROPERTY(QString mainPartName READ getMainPartName WRITE setMainPartName);
	Q_PROPERTY(QString startSectorName READ getStartSectorName WRITE setStartSectorName);
	Q_PROPERTY(QString endSectorName READ getEndSectorName WRITE setEndSectorName);
	Q_PROPERTY(QString eqpTypesResource READ getEqpTypesResource WRITE setEqpTypesResource);
	Q_PROPERTY(QString axisMinResource READ getAxisMinResource WRITE setAxisMinResource);
	Q_PROPERTY(QString axisMaxResource READ getAxisMaxResource WRITE setAxisMaxResource);
	Q_PROPERTY(QString labelText READ getLabelText WRITE setLabelText);
	Q_PROPERTY(bool showLabel READ isShowLabel WRITE setShowLabel);


public:
	ProfileWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);
	~ProfileWidget();

	void dataReady(void);

	// Access for properties above
	inline const QString &getMainPartName(void) const { return mainPartName; }
	void setMainPartName(const QString &name);
	inline const QString &getStartSectorName(void) const { return startSectorName; }
	void setStartSectorName(const QString &name) {startSectorName = name; }
	inline const QString &getEndSectorName(void) const { return endSectorName; }
	void setEndSectorName(const QString &name) {endSectorName = name; }
	inline const QString &getEqpTypesResource(void) const { return eqpTypeResource; }
	void setEqpTypesResource(const QString &name) { eqpTypeResource = name; }
	inline const QString &getAxisMinResource(void) const { return axisMinResource; }
	void setAxisMinResource(const QString &name) { axisMinResource = name; }
	inline const QString &getAxisMaxResource(void) const { return axisMaxResource; }
	void setAxisMaxResource(const QString &name) { axisMaxResource = name; }
	inline bool isShowLabel(void) const { return showLabel; }
	void setShowLabel(bool value) { showLabel = value; }
	inline const QString &getLabelText(void) const { return labelText; }
	inline void setLabelText(const QString &name) { labelText = name; }
	
signals:
	void profileDpDown(int button, int mode, int x, int y, int dummy, const char *dpName);

protected:
	// Two ways for selecting area are supported:
	// 1) main part
	// 2) start + end sectors

	// Name of main part to be displayed
	QString		mainPartName;

	// Name of start sector to be displayed
	QString		startSectorName;

	// Name of end sector to be displayed
	QString		endSectorName;

	// Text to be shown in label. If empty - default is either main part
	// name (for main part selection), or 'startSector - endSector' (for
	// sector selection)
	QString		labelText;

	// Resource name to get list of equipment types to show in profile
	QString		eqpTypeResource;

	// Resource name to get default minimum for vertical axis
	QString		axisMinResource;

	// Resource name to get default maximum for vertical axis
	QString		axisMaxResource;

	// Text to be shown if image is not visible
	QString		errorText;

	// Flag indicating if label shall drawn above image
	bool			showLabel;

	// Flag indicating if dataReady() was invoked
	bool			dataReadyCalled;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Profile image
	ProfileImage	*pView;

	// List of main part labels
	ExtendedLabel		*pLabel;

	// Equipment type mask for all possible equipment types
	VacEqpTypeMask			allEqpMask;

	// Equipment type mask
	VacEqpTypeMask			eqpMask;

	void buildInitialEqpMask(void);
	void buildLayout(void);

	void findStartEndFromSectors(DataPool &pool, Sector **ppStartSector, Sector **ppEndSector);
	void findStartEndFromMainPart(DataPool &pool, Sector **ppStartSector, Sector **ppEndSector);

	virtual void paintEvent(QPaintEvent *event);

private slots:
	void dpMouseDown(int button, int mode, int x, int y, const char *dpName);
};

#endif	// PROFILEWIDGET_H
