#include "XmlSynoptic.h"
#include "PanelViewer.h"

#include <qapplication.h>

#include <QString>

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	QString fileName("/home/kopylov/WinPVSS_projects/vac_ps_1/vac_ps_1/panels/vision/synoptics/SynopticCLEX.xml");
	QString errMsg;
//	XmlSynoptic *pSynoptic = XmlSynoptic::createFromFile(NULL, fileName, DataEnum::Online, errMsg);
	PanelViewer viewer(NULL, fileName);
//	app.setMainWidget(&viewer);
	viewer.show();
	return app.exec();
}
