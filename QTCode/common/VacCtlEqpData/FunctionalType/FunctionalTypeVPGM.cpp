//	Implementation of FunctionalTypeVPGM class
////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVPGM.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeVPGM::FunctionalTypeVPGM(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVPGM::fillCriteria(FunctionalType *pType)
{
	// Own criteria
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Connected, "Mobile Pumping group is connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is Disconnected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPGM in sector {sector} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPGM in sector {sector} is Disconnected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPGM in main part {mainPart} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPGM in main part {mainPart} is Disconnected");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "Pumping group is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPGM in sector {sector} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPGM in sector {sector} is NOT ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPGM in main part {mainPart} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPGM in main part {mainPart} is NOT ON");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "Pumping group is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPGM in sector {sector} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPGM in sector {sector} is NOT OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPGM in main part {mainPart} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPGM in main part {mainPart} is NOT OFF");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, VvrOpen, "VVR of Pumping group is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} VVR is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} VVR is NOT OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VVR of VPGM in sector {sector} is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VVR of VPGM in sector {sector} is NOT OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VVR of VPGM in main part {mainPart} is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VVR of VPGM in main part {mainPart} is NOT OPEN");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, VvrClosed, "VVR of Pumping group is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} VVR is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} VVR is NOT CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VVR of VPGM in sector {sector} is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VVR of VPGM in sector {sector} is NOT CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VVR of VPGM in main part {mainPart} is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VVR of VPGM in main part {mainPart} is NOT CLOSED");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, TmpOn, "TMP of Pumping group is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} TMP is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} TMP is NOT ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"TMP of VPGM in sector {sector} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"TMP of VPGM in sector {sector} is NOT ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"TMP of VPGM in main part {mainPart} is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"TMP of VPGM in main part {mainPart} is NOT ON");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, TmpOff, "TMP of Pumping group is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} TMP is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} TMP is NOT OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"TMP of VPGM in sector {sector} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"TMP of VPGM in sector {sector} is NOT OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"TMP of VPGM in main part {mainPart} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"TMP of VPGM in main part {mainPart} is NOT OFF");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "VPGM ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPGM in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPGM in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPGM in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPGM in main part {mainPart}: no error");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeVPGM::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case Connected:
			result = "Connected";
			break;
		case Off:
			result = "OFF";
			break;
		case On:
			result = "ON";
			break;
		case VvrOpen:
			result = "VvrOpen";
			break;
		case VvrClosed:
			result = "VvrClosed";
			break;
		case TmpOn:
			result = "TmpOn";
			break;
		case TmpOff:
			result = "TmpOff";
			break;
		default:
			break;
		}
		break;
	case EqpMsgCriteria::MainValue:
		result = "PR";
		break;
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeVPGM::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "CONNECTED"))
		{
			result = Connected;
		}
		else if(!strcasecmp(name, "OFF"))
		{
			result = Off;
		}
		else if(!strcasecmp(name, "ON"))
		{
			result = On;
		}
		else if(!strcasecmp(name, "VvrOpen"))
		{
			result = VvrOpen;
		}
		else if(!strcasecmp(name, "VvrClosed"))
		{
			result = VvrClosed;
		}
		else if(!strcasecmp(name, "TmpOn"))
		{
			result = TmpOn;
		}
		else if(!strcasecmp(name, "TmpOff"))
		{
			result = TmpOff;
		}
		break;
	case EqpMsgCriteria::MainValue:
		if(!strcasecmp(name, "PR"))
		{
			return 0;
		}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}

