//	Implementation of DpeValueSet class
/////////////////////////////////////////////////////////////////////////////////

#include "DpeValuesSet.h"

#include <stdlib.h>

static bool DpeValueLessThan(DpeValue *pValue1, DpeValue *pValue2)
{
	return pValue1->getTs() < pValue2->getTs();
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

DpeValuesSet::DpeValuesSet(const char *nameIn) : name(nameIn)
{
	pValues = new QList<DpeValue *>();
	index = -1;
	ready = false;
	refCount = 0;
}

DpeValuesSet::DpeValuesSet(const DpeValuesSet &source)
{
	pValues = new QList<DpeValue *>(*(source.pValues));
	name = source.name;
	ready = source.ready;
	refCount = 0;
	sort();
}

DpeValuesSet::~DpeValuesSet()
{
	while(!pValues->isEmpty())
	{
		delete pValues->takeFirst();
	}
	delete pValues;
}

DpeValuesSet & DpeValuesSet::operator = (const DpeValuesSet &other)
{
	*pValues = *(other.pValues);
	name = other.name;
	ready = other.ready;
	refCount = 0;
	sort();
	return *this;
}

/*
**	FUNCTION
**		Clear content
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeValuesSet::clear(void)
{
	while(!pValues->isEmpty())
	{
		delete pValues->takeFirst();
	}
	ready = false;
	index = -1;
}


/*
**	FUNCTION
**		Add value at given timestamp to DPE data - for state history processing
**
**	ARGUMENTS
**		ts		- Timestamp
**		value	- Value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		It is NOT checked is such timestamp already exists in the list
*/
void DpeValuesSet::append(const QDateTime &ts, const QVariant &value)
{
	pValues->append(new DpeValue(ts, value));
}

/*
**	FUNCTION
**		Add archive value for this DPE - for replay mode
**
**	PARAMETERS
**		newTime			- Timestamps for new values
**		newValue		- New values
**		useFirstValue	- flag indicating if current index shall be reset to 0
**
**	RETURNS
**		1	- in case of success,
**		-1	- in case of error
**
**	CAUTIONS
**		None
*/
int DpeValuesSet::addValue(const QDateTime &ts, const QVariant &value, bool useFirstValue)
{
	if(!findValueAt(ts))
	{
		pValues->append(new DpeValue(ts, value));
	}
	ready = true;
	if(useFirstValue && pValues->count())
	{
		index = 0;
	}
	return 1;
}

/*
**	FUNCTION
**		Build array of values, ordered by timestamp for further fast search
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeValuesSet::sort(void)
{
	if(pValues->isEmpty())
	{
		return;
	}
	qSort(pValues->begin(), pValues->end(), DpeValueLessThan);
	index = -1;
}

/*
**	FUNCTION
**		Find value with given timestamp
**
**	PARAMETERS
**		ts	- Timestamp of value to find
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Binary search algorithm is used, so ordered array 'values' is REQUIRED
*/
DpeValue *DpeValuesSet::findValueAt(const QDateTime &ts)
{
	int	high = pValues->count(), low = -1, probe;
	while((high - low) > 1)
	{
		probe = (low + high) >> 1;
		if(pValues->at(probe)->getTs() > ts)
		{
			high = probe;
		}
		else
		{
			low = probe;
		}
	}
	if(low < 0)
	{
		return NULL;
	}
	return pValues->at(low)->getTs() == ts ? pValues->at(low) : NULL;
}

/*
**	FUNCTION
**		Find value with timestamp BEFORE given timestamp
**
**	PARAMETERS
**		ts	- Timestamp
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Binary search algorithm is used, so ordered array 'values' is REQUIRED
**		Value with timestamp exactly equal to 'ts' not necessary exists
*/
DpeValue *DpeValuesSet::findPreviousValue(const QDateTime &ts)
{
	int dummy;
	return findPreviousValue(ts, dummy);
}

/*
**	FUNCTION
**		Find value with timestamp BEFORE given timestamp
**
**	PARAMETERS
**		ts			- Timestamp
**		valueIdx	- Variable where index of found variable will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Binary search algorithm is used, so ordered array 'values' is REQUIRED
**		Value with timestamp exactly equal to 'ts' not necessary exists
*/
DpeValue *DpeValuesSet::findPreviousValue(const QDateTime &ts, int &valueIdx)
{
	int	high = pValues->count(), low = -1, probe;
	while((high - low) > 1)
	{
		probe = (low + high) >> 1;
		if(pValues->at(probe)->getTs() > ts)
		{
			high = probe;
		}
		else
		{
			low = probe;
		}
	}

	// After cycle the following possibilities exist for resulting 'low' value
	// 1) low < 0 - array does not contain timestamps before input timestamp
	// 2) timestamp of value at index low is equal to input timestamp
	// 3) timestamp of value at index low is before input timestamp
	if(low < 0)
	{
		valueIdx = -1;
		return NULL;
	}
	if(pValues->at(low)->getTs() == ts)
	{
		for( ; low >= 0 ; low--)
		{
			if(pValues->at(low)->getTs() < ts)
			{
				valueIdx = low;
				return pValues->at(low);
			}
		}
		valueIdx = -1;
		return NULL;	// No timestamp before
	}
	valueIdx = low;
	return pValues->at(low);
}

/*
**	FUNCTION
**		Find value with timestamp AFTER given timestamp
**
**	PARAMETERS
**		ts	- Timestamp
**
**	RETURNS
**		None
**
** CAUTIONS
**		Binary search algorithm is used, so ordered array 'values' is REQUIRED
**		Value with timestamp exactly equal to 'ts' not necessary exists
*/
DpeValue *DpeValuesSet::findNextValue(const QDateTime &ts)
{
	int dummy;
	return findNextValue(ts, dummy);
}

/*
**	FUNCTION
**		Find value with timestamp AFTER given timestamp
**
**	PARAMETERS
**		ts			- Timestamp
**		valueIdx	- Variable where index of found value will be written
**
**	RETURNS
**		None
**
** CAUTIONS
**		Binary search algorithm is used, so ordered array 'values' is REQUIRED
**		Value with timestamp exactly equal to 'ts' not necessary exists
*/
DpeValue *DpeValuesSet::findNextValue(const QDateTime &ts, int &valueIdx)
{
	int	high = pValues->count(), low = -1, probe;
	while((high - low) > 1)
	{
		probe = (low + high) >> 1;
		if(pValues->at(probe)->getTs() < ts)
		{
			low = probe;
		}
		else
		{
			high = probe;
		}
	}

	// After cycle the following possibilities exist for resulting 'high' value
	// 1) high >= number of values - array does not contain timestamps after input timestamp
	// 2) timestamp of value at index high is equal to input timestamp
	// 3) timestamp of value at index high is after input timestamp
	int nValues = pValues->count();
	if(high >= nValues)
	{
		valueIdx = -1;
		return NULL;
	}
	if(pValues->at(high)->getTs() == ts)
	{
		for( ; high < nValues ; high++)
		{
			if(pValues->at(high)->getTs() > ts)
			{
				valueIdx = high;
				return pValues->at(high);
			}
		}
		valueIdx = -1;
		return NULL;	// No timestamp after
	}
	valueIdx = high;
	return pValues->at(high);
}

/*
**	FUNCTION
**		Set current index in this DPE for time driven mode
**
**	PARAMETERS
**		ts			- in, timestamp for which the index shall be set
**		forward		- in, defines direction where the indexes are looking for
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeValuesSet::setIndex(const QDateTime &ts, bool forward)
{
	if(forward)
	{
		findPreviousValue(ts, index);
	}
	else
	{
		findNextValue(ts, index);
		if(index < 0)	// next index isn't found
		{
			index = pValues->count() - 1;
		}
	}
	notReported = true;
}

/*
**	FUNCTION
**		Return value from sorted array with given index
**
**	PARAMETERS
**		index	- Index of value
**
**	RETURNS
**		Pointer to value with given index; or
**		NULL if there is no such index
**
** CAUTIONS
**		None
*/
const DpeValue *DpeValuesSet::getValueAt(int index) const
{
	if((index < 0) || (index >= (int)pValues->count()))
	{
		return NULL;
	}
	if(pValues->isEmpty())
	{
		return NULL;
	}
	return pValues->at(index);
}
