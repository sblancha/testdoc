//	Implementation of EqpV_TCP class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpV_TCP.h"
#include "DataPool.h"
#include "FunctionalTypePLC.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_NOEQP				255, 192, 203	//pink
#define COLOR_INVALID			0,0,255			//blue
#define COLOR_ON				0,255,0			//green
#define COLOR_OPEN				0,255,0			//green
#define COLOR_CLOSED			255,0,0			//red
#define COLOR_UNDER_RANGE		0,100,0			//darkgreen 				
#define COLOR_OFF				255,255,255		//white
#define	COLOR_ERROR				238, 130, 238	//darkviolet
#define	COLOR_WARNING			255,255,0		//yellow

//	Main state bits to be analyzed
#define	RR1_SCADA_READY				(0x1000) // (0x10 00)
#define	RR1_FE_READY				(0x0800) // (0x08 00)
#define	RR1_RECOGNIZED				(0x0400) // (0x04 00)
#define	RR1_CONNECTED				(0x0200) // (0x02 00)
#define	RR1_ENABLE					(0x0100) // (0x01 00)
#define	RR1_ERR_DISCONNECT			(0x0000) // (0x00 00)
#define	RR1_ERR_SCADA_CONN_ID		(0x0000) // (0x00 00)
#define	RR1_ERR_OCC					(0x0000) // (0x00 00)
#define	RR1_ERR_POS_BUSY			(0x0000) // (0x00 00)
#define	RR1_ERR_POS					(0x0000) // (0x00 00)
#define	RR1_ERR_TYPE				(0x0000) // (0x00 00)
#define	RR1_ERR_CONN_ID				(0x0000) // (0x00 00)
#define RR1_ERR_MASK				(0x00FF)

EqpV_TCP::EqpV_TCP(const Eqp &source) : EqpPLC(source) {
	alarmOnline = alarmReplay = 0;
	rr1Online = rr1Replay = 0;
	functionalType = FunctionalType::PLC;
}

/**
@brief FUNCTION Return list of all DPEs, related to state of device. The list is used o build state history
@param[out]	stateDpes	Variable where all state DPEs shall be added
*/
void EqpV_TCP::getStateDpes(QStringList &stateDpes) {
	stateDpes.clear();
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "AL1");
}

/**
@brief FUNCTION Connect DPEs of interest for this device to obtain information required for display
@param[in]	pDst	Pointer to destination class who wants to be notified about changes in this device's state
@param[in]  mode	Data acquisition mode
*/
void EqpV_TCP::connect(InterfaceEqp *pDst, DataEnum::DataMode mode) {
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "AL1", mode);
	// Emit signal immediately - this is major difference from other types
	QVariant value(mode == DataEnum::Replay ? alarmReplay : alarmOnline);
	emit dpeChanged(this, "AL1", DataEnum::Own, value, mode, mode == DataEnum::Replay ? timeStampReplay : timeStampOnline);
}

/**
@brief FUNCTION Disconnect previously connected DPEs of this device
@param[in]	pDst	Pointer to destination class who wants to be notified about changes in this device's state
@param[in]  mode	Data acquisition mode
*/
void EqpV_TCP::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode) {
	disconnectDpe(pDst, "AL1", mode);
	disconnectDpe(pDst, "RR1", mode);
}

/**
@brief FUNCTION Process new online value arrived from Unicos system integrity
@param[in]	dpe		DPE name
@param[in]	value	DPE value
*/
void EqpV_TCP::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp) {
	if (!strcmp(dpe, "RR1"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if (!strcmp(dpe, "AL1"))
	{
		if (value.canConvert(QVariant::Bool))
		{
			bool newValue = value.toBool();
			if (alarmOnline == newValue)
			{
				return;
			}
			alarmOnline = newValue;
			timeStampOnline = timeStamp;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/**
@brief FUNCTION Process new replay value arrived from Unicos system integrity
@param[in]	dpe		DPE name
@param[in]	value	DPE value
*/
#ifndef PVSS_SERVER_VERSION
void EqpV_TCP::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp) {
	if (!strcmp(dpe, "RR1")) {
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if (!strcmp(dpe, "AL1")) {
		if (value.canConvert(QVariant::Bool)) {
			bool newValue = value.toBool();
			if (alarmReplay == newValue)
				return;
			alarmReplay = newValue;
			timeStampReplay = timeStamp;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/**
@brief FUNCTION check if communication (V_TCP) is alive, return true if ok, false if bad
@param[in]	mode	Data acquisition mode
*/
bool EqpV_TCP::isAlive(DataEnum::DataMode mode) const {
	return mode == DataEnum::Replay ? (alarmReplay == 0) : (alarmOnline == 0);
}

/**
@brief FUNCTION Calculate main color of device
@param[in]	color	Variable where resulting color will be put
@param[in]	mode	Data acquisition mode
*/
#ifndef PVSS_SERVER_VERSION
void EqpV_TCP::getMainColor(QColor &color, DataEnum::DataMode mode) {
	if (mode == DataEnum::Replay)
	{
		getMainColor(alarmReplay, rr1Replay, color);
	}
	else
	{
		getMainColor(alarmOnline, rr1Online, color);
	}
}
#endif

/**
@brief FUNCTION Calculate main color of device
@param[in]	alarm	Value of V_TCP alarm
@param[in]	rr1		Value of DPE RR1
@param[out]	color	Calculated color
*/
#ifndef PVSS_SERVER_VERSION
void EqpV_TCP::getMainColor(int alarm, unsigned rr1, QColor &color)
{
	if (alarm) {
		color.setRgb(COLOR_INVALID);
		return;
	}
	if (rr1 & RR1_ERR_MASK) {
		color.setRgb(COLOR_ERROR);
		return;
	}
	if (rr1 & RR1_CONNECTED) {
		color.setRgb(COLOR_ON);
		return;
	}
	else
	{
		color.setRgb(COLOR_OFF);
	}
}
#endif

/**
@brief FUNCTION Calculate main state string of device
@param[out]	string	Variable where resulting string will be put
@param[in]	mode	Data acquisition mode
*/
#ifndef PVSS_SERVER_VERSION
void EqpV_TCP::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if (mode == DataEnum::Replay)
	{
		getMainStateString(alarmReplay, rr1Replay, string);
	}
	else
	{
		getMainStateString(alarmOnline, rr1Replay, string);
	}
}
#endif

/**
@brief FUNCTION Calculate main state string of device
@param[in]	alarm	Value of V_TCP alarm
@param[in]	rr1		Value of DPE RR1
@param[out]	string	Variable where resulting string will be put
*/
#ifndef PVSS_SERVER_VERSION
void EqpV_TCP::getMainStateString(int alarm, unsigned rr1, QString &string)
{
	if (alarm) {
		string = "Dead";
		return;
	}
	
	if (rr1 & RR1_ERR_DISCONNECT)
		string = "Error Disconnected";
	else if (rr1 & RR1_ERR_SCADA_CONN_ID)
		string = "Error SCADA Conn ID";
	else if (rr1 & RR1_ERR_OCC)
		string = "Error Occurence";
	else if (rr1 & RR1_ERR_POS_BUSY)
		string = "Error Position Busy";
	else if (rr1 & RR1_ERR_POS)
		string = "Error Position";
	else if (rr1 & RR1_ERR_TYPE)
		string = "Error Type";
	else if (rr1 & RR1_ERR_CONN_ID)
		string = "Error Conn ID";
	else if (rr1 & RR1_SCADA_READY)
		string = "OK: SCADA Connected";
	else if (rr1 & RR1_FE_READY)
		string = "Front End Ready";
	else if (rr1 & RR1_RECOGNIZED)
		string = "Recognized";
	else if (rr1 & RR1_CONNECTED)
		string = "Master Connected";
	else if (rr1 & RR1_ENABLE)
		string = "Enabled";
	else 
		string = "Undefined";
}
#endif

/**
@brief FUNCTION Calculate main state string and color for for moment in the past
@param[in]	pPool	Pointer to state history data pool
@param[in]	ts		Moment when state is needed
@param[out]	state	Variable where resulting state string will be written
@param[out]	color	Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpV_TCP::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
	QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("RR1");
	dpeNames.append("AL1");
	QList<QVariant *> values;
	if (!pPool->getDpValues(dpName, ts, dpeNames, values))
	{
		return;
	}
	if (values.isEmpty()) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toInt();
	delete pVariant;
	if (values.isEmpty()) {
		return;
	}
	pVariant = values.takeFirst();
	bool alarm = pVariant->toBool();
	delete pVariant;

	getMainStateString(alarm, rr1, state);
	getMainColor(alarm, rr1, color);
	while (!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/**
@brief FUNCTION Check if device matches criteria for E-mail/SMS notification
@param[in]	pCriteria	- Pointer to criteria to check
*/
void EqpV_TCP::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if (pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if (pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if (checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	switch (pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch (pCriteria->getSubType())
		{
		case FunctionalTypePLC::OK:
			if (!alarmOnline)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}

