//	Implementation of ProfileLhcView class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileLhcView.h"

#include "ProfileLhcLineIsol.h"
#include "ProfileLhcLineBeam.h"

#include "VacMainView.h"
#include "ExtendedLabel.h"

#include "DataPool.h"
#include "Eqp.h"

#include <QTimer>
#include <QSpinBox>
#include <QPixmap>
#include <QMouseEvent>
#include <QApplication>

#include <math.h>

#include "PlatformDef.h"
#include "DebugCtl.h"

/*
**	FUNCTION
**		Create new instance of LHC profile view
**
**	PARAMETERS
**		parent			- Pointer to parent widget
**		pFirstSector	- Pointer to first sector
**		pLastSector		- Pointer to last sector
**		mode			- Data acquisition mode for synoptic
**
**	RETURNS
**		Pointer to new instance of profile view,
**		NULL in case of error.
**
**	CAUTIONS
**		None
*/
ProfileLhcView *ProfileLhcView::create(QWidget *parent, Sector *pFirstSector, Sector *pLastSector,
	DataEnum::DataMode mode)
{
	float	start = pFirstSector->getStart(),
			end = pLastSector->getEnd();
	if(start == end)
	{
		return NULL;
	}

	// Build new instance
	ProfileLhcView	*pView = new ProfileLhcView(parent);
	pView->pStartSector = pFirstSector;
	pView->pEndSector = pLastSector;
	pView->start = start;
	pView->end = end;
	pView->mode = mode;

	return pView;
}


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction


ProfileLhcView::ProfileLhcView(QWidget *parent, Qt::WindowFlags f) :
	QWidget(parent, f)
{
	setAutoFillBackground(true);
	vacTypeMask = 0;
	maxBarIdx = -1;
	hasLinearEqp = linearSelected = false;
	newMaskApplied = true;
	showSectorNames = false;
	cryoAxisTop = cryoAxisBottom = beamAxisTop = beamAxisBottom = linAxisTop = linAxisBottom = 0;
	logAxisX = linAxisX = 0;

	setMinimumSize(QSize(400, 300));

	pLabel = new ExtendedLabel(this);
	pLabel->setLabelType(ExtendedLabel::Transparent);
	pLabel->setBackColor(QColor(255, 255, 204, 204));
	/*
	QPalette palette = pLabel->palette();
	palette.setColor(QPalette::Window, QColor(255, 255, 204));
	pLabel->setPalette(palette);
	pLabel->setAutoFillBackground(true);
	*/
	pLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	pLabel->hide();

	pTimer = new QTimer(this);
	pTimer->setSingleShot(true);
	connect(pTimer, SIGNAL(timeout()), pLabel, SLOT(hide()));

	pRedrawTimer = new QTimer(this);
	pRedrawTimer->setSingleShot(true);
	connect(pRedrawTimer, SIGNAL(timeout()), this, SLOT(redrawTimerDone()));

	pSpinBox = new QSpinBox(this);
	pSpinBox->setRange(-18, 4);
	pSpinBox->setSingleStep(1);
	QString prefix("1E");
	pSpinBox->setPrefix(prefix);
	pSpinBox->hide();
	QObject::connect(pSpinBox, SIGNAL(valueChanged(int)), this, SLOT(spinBoxChange(int)));

	setMouseTracking(true);
	scaleType = ScaleNone;
	editScaleMin = settingSpinBox = false;
}

ProfileLhcView::~ProfileLhcView()
{
	while(!lines.isEmpty())
	{
		delete lines.takeFirst();
	}
}

/////////////////////////////////////////////////////////////////////////////////
// Event processing
/////////////////////////////////////////////////////////////////////////////////
void ProfileLhcView::resizeEvent(QResizeEvent *event)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		pLine->setArea(rect());
	}
	buildGeometry();
	rebuildImage();
	QWidget::resizeEvent(event);
}

void ProfileLhcView::mouseMoveEvent(QMouseEvent *pEvent)
{
	QRect rect;
	Eqp *pEqp = findItemUnderMouse(pEvent->x(), pEvent->y(), rect);
	if(pEqp)
	{
		showBarLabel(pEqp, rect, pEvent->y());
	}
	else
	{
		pLabel->hide();
		if(pEvent->x() < logAxisX)
		{
			showLogScaleControl(pEvent->y());
		}
		else if((pEvent->x() > linAxisX) && hasLinearEqp)
		{
			showLinScaleControl(pEvent->y());
		}
		else
		{
			pSpinBox->hide();
		}
	}
}

void ProfileLhcView::mousePressEvent(QMouseEvent *pEvent)
{
	QRect rect;
	Eqp *pEqp = findItemUnderMouse(pEvent->x(), pEvent->y(), rect);
	if(pEqp)
	{
		int buttonNbr = 0;
		switch(pEvent->button())
		{
		case Qt::LeftButton:
			buttonNbr = 1;
			break;
		case Qt::RightButton:
			buttonNbr = 2;
			break;
		default:	// Just to make Linux compiler happy
			break;
		}
		if(!buttonNbr)
		{
			return;
		}
		QPoint point = mapToGlobal(pEvent->pos());
		emit dpMouseDown(buttonNbr, mode, point.x(), point.y(), pEqp->getDpName());
	}
}

void ProfileLhcView::mouseDoubleClickEvent(QMouseEvent *pEvent)
{
	QRect rect;
	Eqp *pEqp = findItemUnderMouse(pEvent->x(), pEvent->y(), rect);
	if(pEqp)
	{
		VacMainView *pMainView = VacMainView::getInstance();
		if(pMainView)
		{
			pMainView->emitEqpDoubleClick(pEqp->getDpName(), mode);
		}
	}
}


void ProfileLhcView::leaveEvent(QEvent * /* pEvent */)
{
	pTimer->stop();
	pLabel->hide();
	pSpinBox->hide();
}


/*
**	FUNCTION
**		Initialize all lines in view
**
**	PARAMETERS
**		cryoMask	- Mask containing all allowed eqp. types for cryo vacuum
**		beamMask	- Mask containing all allowed eqp. types for beam vacuum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::init(const VacEqpTypeMask &cryoMask, const VacEqpTypeMask &beamMask)
{
	allowedCryoEqpMask = cryoMask;
	allowedBeamEqpMask = beamMask;
	lines.append(new ProfileLhcLineIsol(this, ProfileLhcLine::Qrl, start, end));
	lines.append(new ProfileLhcLineIsol(this, ProfileLhcLine::Cryo, start, end));
	lines.append(new ProfileLhcLineBeam(this, ProfileLhcLine::OuterBeam, start, end));
	lines.append(new ProfileLhcLineBeam(this, ProfileLhcLine::InnerBeam, start, end));

	// Finally...
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		pLine->setMode(mode);
		pLine->setShowSectorNames(showSectorNames);
		switch(pLine->getType())
		{
		case ProfileLhcLine::Qrl:
			pLine->setVacTypeMask(VacType::Qrl);
			pLine->build(allowedCryoEqpMask);
			break;
		case ProfileLhcLine::Cryo:
			pLine->setVacTypeMask(VacType::Cryo);
			pLine->build(allowedCryoEqpMask);
			break;
		default:
			pLine->setVacTypeMask(VacType::BlueBeam | VacType::RedBeam | VacType::CrossBeam | VacType::CommonBeam);
			pLine->build(allowedBeamEqpMask);
			break;
		}
		connect(pLine, SIGNAL(eqpDataChanged()), this, SLOT(eqpDataChange()));
		connect(pLine, SIGNAL(eqpSelectChanged(bool)), this, SLOT(eqpSelectChange(bool)));
		connect(pLine, SIGNAL(mobileStateChanged(bool)), this, SLOT(mobileStateChange(bool)));
		connect(pLine, SIGNAL(eqpVisibilityChanged()), this, SLOT(eqpVisibilityChange()));
	}
	rebuildImage();
}

void ProfileLhcView::setShowSectorNames(bool flag)
{
	if(flag == showSectorNames)
	{
		return;
	}
	showSectorNames = flag;
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		pLine->setShowSectorNames(flag);
	}
	buildGeometry();
	rebuildImage();
}

/*
**	FUNCTION
**		Find best screen position for given coordinate. The method is called
**		AFTER all pressure measurement equipment and sectors have been assigned
**		screen coordinate to find position for thermometers areas
**
**	PARAMETERS
**		coord	- Coordinate in question (LHC ring)
**
**	RETURNS
**		Horizontal screen coordinate for given world coordinate
**
**	CAUTIONS
**		None
*/
int ProfileLhcView::bestPositionForEqp(float coord)
{
	int	result = 0;
	float	delta = 2 * DataPool::getInstance().getLhcRingLength();
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		if(!pLine->isHidden())
		{
			pLine->bestPositionForEqp(coord, delta, result);
		}
	}
	return result;
}

/*
**	FUNCTION
**		Analyze which equipment types will be shown in view
**
** PARAMETERS
**		vacMask		- OR'ed bit mask for all vacuum types
**		cryoMask	- Equipment type mask for all equipment on isolation vacuum
**						to be shown in this view
**		beamMask	- Equipment type mask for all equipment on beam vacuum
**						to be shown in this view
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void ProfileLhcView::analyzeTypes(unsigned &vacMask, VacEqpTypeMask &cryoMask,
		VacEqpTypeMask &beamMask)
{
	vacMask = 0;
	cryoMask.clear();
	beamMask.clear();
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		// unsigned curMask = pLine->getVacTypeMask();
		// pLine->setVacTypeMask(0xFFFFFFFF);
		switch(pLine->getType())
		{
		case ProfileLhcLine::Qrl:
		case ProfileLhcLine::Cryo:
			pLine->analyzeTypes(vacMask, cryoMask);
			break;
		default:
			pLine->analyzeTypes(vacMask, beamMask);
			break;
		}
		// pLine->setVacTypeMask(curMask);
	}
}

/*
**	FUNCTION
**		Apply new masks to the view
**
**	PARAMETERS
**		allVacTypeMask	- OR'ed bit mask for all vacuum types
**		allCryoEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**							to be shown in this view
**		allBeamEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**							to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::applyMask(unsigned vacTypeMask, VacEqpTypeMask &cryoEqpMask, VacEqpTypeMask &beamEqpMask)
{
//qDebug("ProfileLhcView::applyMask: connect\n");
	if(vacTypeMask & (VacType::RedBeam | VacType::BlueBeam))
	{
		vacTypeMask |= VacType::CrossBeam | VacType::CommonBeam;
	}

	bool newVacTypeMask = this->vacTypeMask != vacTypeMask;
	newMaskApplied = newVacTypeMask ||
		(this->cryoEqpMask != cryoEqpMask) || (this->beamEqpMask != beamEqpMask);
	this->vacTypeMask = vacTypeMask;
	this->cryoEqpMask = cryoEqpMask;
	this->beamEqpMask = beamEqpMask;

	if(DebugCtl::isProfile())
	{
		#ifdef Q_OS_WIN
			dump("C:\\ProfileLhcPass1.txt");
		#else
			dump("/home/kopylov/debug/ProfileLhcPass1.txt");
		#endif
	}
//qDebug("ProfileLhcView::applyMask: before buildGeometry\n");
	buildGeometry();
//qDebug("ProfileLhcView::applyMask: before rebuildImage\n");
	rebuildImage();
//qDebug("ProfileLhcView::applyMask: FINISH\n");
}

/*
**	FUNCTION
**		Set new limits for CRYO vertical scale
**
**	PARAMETERS
**		newMin	- New scale minimum
**		newMax	- New scale maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcView::setCryoLimits(int newMin, int newMax)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		pLine->setLogCryoLimits(newMin, newMax);
	}
	buildGeometry();
	rebuildImage();
}

/*
**	FUNCTION
**		Set new limits for BEAM vertical scale
**
**	PARAMETERS
**		newMin	- New scale minimum
**		newMax	- New scale maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcView::setBeamLimits(int newMin, int newMax)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		pLine->setLogBeamLimits(newMin, newMax);
	}
	buildGeometry();
	rebuildImage();
}

/*
**	FUNCTION
**		Set new limits for linear vertical scale
**
**	PARAMETERS
**		newMin	- New scale minimum
**		newMax	- New scale maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void ProfileLhcView::setLinLimits(int newMin, int newMax)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		pLine->setLinLimits(newMin, newMax);
	}
	buildGeometry();
	rebuildImage();
}


/*
**	FUNCTION
**		Build geometry of all individual lines in view
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::buildGeometry(void)
{
	// Prepare all lines for process
	int	nGraphs = 0, idx;
	ProfileLhcLine *pLine;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		switch(pLine->getType())
		{
		case ProfileLhcLine::Qrl:
			setLineVisibility(pLine, (unsigned)VacType::Qrl, cryoEqpMask, nGraphs);
			break;
		case ProfileLhcLine::Cryo:
			setLineVisibility(pLine, (unsigned)VacType::Cryo, cryoEqpMask, nGraphs);
			break;
		default:
			setLineVisibility(pLine, (unsigned)(VacType::RedBeam | VacType::BlueBeam), beamEqpMask, nGraphs);
			break;
		}
	}

	if(!nGraphs)
	{
		return;
	}

	// Assign bar indices in all lines
	maxBarIdx = -1;
	pLine = NULL;
	float coord = -1;
	while((pLine = nextLine(pLine)))
	{
		pLine->placeItem(coord, maxBarIdx);
	}

	// Calculate vertical coordinates for all graphs
	float oneGraphHeight = (float)height() / nGraphs;
	int graphTop = 0;
	QRect lineArea(rect());
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(pLine->isHidden())
		{
			continue;
		}
		lineArea.setTop(graphTop);
		lineArea.setBottom((int)rint(graphTop - 2 + oneGraphHeight));
		pLine->setArea(lineArea);
		graphTop = lineArea.bottom() + 2;
	}

	// Check if linear axis shall appear: it will appear if there are thermometers at
	// least in one line
	hasLinearEqp = false;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(!pLine->isHidden())
		{
			pLine->checkLinEqpExistence(hasLinearEqp);
		}
	}

	// Calculate space required for logarithmic axis
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(pLine->isHidden())
		{
			continue;
		}
		pLine->calcAxisGeometry(logAxisX);
	}

	// Adjust axis geometry for all lines in such a way that vertical
	// axis line has the same location for all graphs.
	QFontMetrics fm = fontMetrics();
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(pLine->isHidden())
		{
			continue;
		}
		pLine->calculateAxisMinLocation(fm);
		pLine->setAxisLocation(logAxisX);
		linAxisX = pLine->getLinAxisX();
	}

	// Calculate bar geometries in all lines
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(pLine->isHidden())
		{
			continue;
		}
		pLine->calcBarGeometry(maxBarIdx);
	}
	// Calculate final sector geometries in all lines
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(pLine->isHidden())
		{
			continue;
		}
		pLine->buildSectors(fm);
	}
	// After that - calculate CRYO TT geometries in all lines
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(pLine->isHidden())
		{
			continue;
		}
		pLine->calcCryoTtGeometry();
	}
	if(DebugCtl::isProfile())
	{
		#ifdef Q_OS_WIN
			dump("C:\\ProfileLhcPass2.txt");
		#else
			dump("/home/kopylov/debug/ProfileLhcPass2.txt");
		#endif
	}
}

/*
**	FUNCTION
**		Set visibility of profile line according to input vacuum type mask
**
**	PARAMETERS
**		pLine		- Pointer to line whose visibility shall be set
**		vacMask		- Vacuum type mask
**		eqpMask		- Equipment mask to be applied for line
**		nGraphs		- Counter of visible lines, value will be increased if line shall be visible
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::setLineVisibility(ProfileLhcLine *pLine, unsigned vacMask, VacEqpTypeMask &eqpMask, int &nGraphs)
{
	bool maskMatches = vacMask & vacTypeMask;
	if(maskMatches)
	{
		pLine->setHidden(false);
		pLine->applyMask(eqpMask);
		nGraphs++;
	}
	else
	{
		pLine->hide();
	}
}

/*
**	FUNCTION
**		Find next line to be processed for bar positionnig
**
**	PARAMETERS
**		pPrevLine	- Pointer to previously found line
**
**	RETURNS
**		Pointer to next line to be processed, or
**		NULL if no more lines to process
**
**	CAUTIONS
**		None
*/
ProfileLhcLine *ProfileLhcView::nextLine(ProfileLhcLine *pPrevLine)
{
	ProfileLhcLine	*pBestLine = NULL;
	float			bestCoord = HUGE_COORDINATE;

	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		if(pLine->isHidden())
		{
			continue;
		}
		float coord = pLine->nextPosition();
		int compareResult = compareCoordInView(bestCoord, coord);
		switch(compareResult)
		{
		case 0:	// new coordinate is the same as old one
			// Make sure that equipment in another line with the same
			// coordinate will be processed BEFORE equipment in the
			// same line having the same coordinate. This is the way
			// to ensure equipment with the same coordinates on different
			// lines will use the same bar index
			if((pBestLine == pPrevLine) && (coord < HUGE_COORDINATE))
			{
				bestCoord = coord;
				pBestLine = pLine;
			}
			break;
		case -1:	// New coordinate is before the old one
			bestCoord = coord;
			pBestLine = pLine;
			break;
		}
	}
	return pBestLine;
}

/*
**	FUNCTION
**		Compare two coordinates within the view.
**		especially if range coordinates for view includes start of
**		ring.
**
**	PARAMETERS
**		bestCoord	- Previously found best coordinate
**		newCoord	- New coordinate to be compared with best coordinate
**
**	RETURNS
**		-1	- newCoord is before bestCoord;
**		0	- newCoord is the same as bestCoord;
**		1	- newCoord is after bestCoord;
**
**	CAUTIONS
**		It is expected that start != end
**
*/
int ProfileLhcView::compareCoordInView(float bestCoord, float newCoord)
{
	if(newCoord == bestCoord)
	{
		return 0;
	}
	if(newCoord == HUGE_COORDINATE)
	{
		return 1;
	}
	if(bestCoord == HUGE_COORDINATE)
	{
		return -1;
	}

	if(start < end)	// View range does not include start of ring
	{
		if(newCoord < bestCoord)
		{
			return -1;
		}
	}
	else	// View range includes start of ring
	{
		if(newCoord >= start)	// New coordinate is still before ring start
		{
			if(bestCoord <= end)	// Best coordinate is already after ring start
			{
				return -1;
			}
			else	// Best coordinate is still before ring start
			{
				if(newCoord < bestCoord)
				{
					return -1;
				}
			}
		}
		else	// New coordinate is already after ring start
		{
			if(bestCoord <= start)	// Best coordinate is also after ring start
			{
				if(newCoord < bestCoord)
				{
					return -1;
				}
			}
		}
	}
	return 1;
}

/*
**	FUNCTION
**		Slot activated when redraw timeout expired
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::redrawTimerDone(void)
{
	rebuildImage();
}

/*
**	FUNCTION
**		Build new profile image, set image as background for this widget
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::rebuildImage(void)
{
//qDebug("ProfileLhcView::rebuildImage: start\n");
	pRedrawTimer->stop();
	QPixmap bgPixmap(size());
	QPalette pal = QApplication::palette();
	bgPixmap.fill(pal.color(QPalette::Window));
	QPainter painter(&bgPixmap);
//qDebug("ProfileLhcView::rebuildImage: painter\n");
	draw(painter, false, false);
//qDebug("ProfileLhcView::rebuildImage: draw\n");
	pal.setBrush(QPalette::Window, bgPixmap);
//qDebug("ProfileLhcView::rebuildImage: setBrush\n");
	setPalette(pal);
//qDebug("ProfileLhcView::rebuildImage: setPalette\n");
	drawTime = QTime::currentTime();
//qDebug("ProfileLhcView::rebuildImage: FINISH\n");
}


/*
**	FUNCTION
**		Prepare and draw profile
**
**	PARAMETERS
**		painter				- Painter used for drawing
**		flashingOnly		- true if only flasing bars shall be redrawn
**		useAltFlashColor	- true if flashing bars shall be drawn using alternative color
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::draw(QPainter &painter, bool flashingOnly, bool useAltFlashColor)
{
//qDebug("ProfileLhcView::draw: start\n");
	if(!maxBarIdx)
	{
		return;
	}
	int idx;
	ProfileLhcLine *pLine;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(pLine->isHidden())
		{
			continue;
		}
//qDebug("ProfileLhcView::draw: draw line %d type %d\n", idx, pLine->getType());
		pLine->draw(painter, flashingOnly, useAltFlashColor);
	}

	if(DebugCtl::isProfile())
	{
		#ifdef Q_OS_WIN
			dump("C:\\ProfileLhcPass3.txt");
		#else
			dump("/home/kopylov/debug/ProfileLhcPass3.txt");
		#endif
	}

	// Calculate vertical locations for axis - will be used as basis
	// for +/- buttons location
	cryoAxisTop = cryoAxisBottom = beamAxisTop = beamAxisBottom = linAxisTop = linAxisBottom = 0;
	bool	isFirstCryo = true,
			isFirstBeam = true,
			isFirstLin = true;
	for(idx = 0 ; idx < lines.count() ; idx++)
	{
		pLine = lines.at(idx);
		if(pLine->isHidden())
		{
			continue;
		}
		switch(pLine->getType())
		{
		case ProfileLhcLine::Qrl:
		case ProfileLhcLine::Cryo:
			if(isFirstCryo)
			{
				cryoAxisTop = pLine->getScaleTop();
				isFirstCryo = false;
			}
			if(pLine->getScaleBottom() > cryoAxisBottom)
			{
				cryoAxisBottom = pLine->getScaleBottom();
			}
			break;
		case ProfileLhcLine::OuterBeam:
		case ProfileLhcLine::InnerBeam:
			if(isFirstBeam)
			{
				beamAxisTop = pLine->getScaleTop();
				isFirstBeam = false;
			}
			if(pLine->getScaleBottom() > beamAxisBottom)
			{
				beamAxisBottom = pLine->getScaleBottom();
			}
			break;
		}
		if(isFirstLin)
		{
			linAxisTop = pLine->getScaleTop();
			isFirstLin = false;
		}
		if(pLine->getScaleBottom() > linAxisBottom)
		{
			linAxisBottom = pLine->getScaleBottom();
		}
	}
//qDebug("ProfileLhcView::draw: FINISH\n");
}


/*
**	FUNCTION
**		Slot activated when data for one of devices have been changed
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::eqpDataChange(void)
{
	pRedrawTimer->stop();
	int elapsed = drawTime.msecsTo(QTime::currentTime());
	if(elapsed < 0)	// wrap around 24 hours
	{
		rebuildImage();
	}
	else if(elapsed > 1000)
	{
		rebuildImage();
	}
	else
	{
		pRedrawTimer->start(300);
	}
}

/*
**	FUNCTION
**		Slot activated when selection state for one of devices have been changed
**
**	PARAMETERS
**		linear	- true if selected equipment uses linear vertical scale
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::eqpSelectChange(bool linear)
{
	linearSelected = linear;
	rebuildImage();
}

/*
**	FUNCTION
**		Slot activated when activity of mobile device in one of lines has been changed
**
**	PARAMETERS
**		redrawNeeded	- true if profile shall be redrawn as a result of this event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::mobileStateChange(bool redrawNeeded)
{
	if(redrawNeeded)
	{
		pRedrawTimer->stop();
		buildGeometry();
		rebuildImage();
	}
	emit mobileStateChanged();
}

void ProfileLhcView::eqpVisibilityChange(void)
{
	pRedrawTimer->stop();
	buildGeometry();
	rebuildImage();
}


/*
**	FUNCTION
**		Show label for bar under mouse pointer
**
**	PARAMETERS
**		pEqp	- Pointer to device under mouse pointer
**		rect	- Device presentation area on profile
**		y		- Y-coordinate of mouse pointer
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::showBarLabel(Eqp *pEqp, const QRect &rect, int y)
{
	pTimer->stop();
	pSpinBox->hide();
	QString text = pEqp->getSectorBefore()->getName();
	if(pEqp->isSectorBorder())
	{
		text += " / ";
		text += pEqp->getSectorAfter()->getName();
	}
	text += "\n";
	QString state;
	pEqp->getToolTipString(state, mode);
	text += state;
	pLabel->setText(text);
	pLabel->adjustSize();
	int	x = (rect.left() + rect.right()) / 2;
	if((x + pLabel->width()) > width())
	{
		x -= pLabel->width();
	}
	if((y + pLabel->height()) > height())
	{
		y -= pLabel->height();
	}
	pLabel->move(x, y);
	pLabel->show();
	pTimer->start(5000);
}

/*
**	FUNCTION
**		It has been found that mouse pointer is within vertical scale
**		area for logarithmic scale. Decide if spin box for scale control
**		shall be shown, if yes - configure and show it
**
**	PARAMETERS
**		y		- Y-coordinate of mouse pointer
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::showLogScaleControl(int y)
{
	ScaleType newScaleType = ScaleNone;
	bool	newEditMin = false;

	// Are we close enough to min or max of linear scale
	int scaleHeight = cryoAxisBottom - cryoAxisTop;
	if(scaleHeight)
	{
		if((y > cryoAxisTop) && (y < (cryoAxisTop + (scaleHeight >> 2))))
		{
			newScaleType = ScaleCryo;
			newEditMin = false;
		}
		else if((y < cryoAxisBottom) && (y > (cryoAxisBottom - (scaleHeight >> 2))))
		{
			newScaleType = ScaleCryo;
			newEditMin = true;
		}
	}
	if(newScaleType == ScaleNone)
	{
		scaleHeight = beamAxisBottom - beamAxisTop;
		if(scaleHeight)
		{
			if((y > beamAxisTop) && (y < (beamAxisTop + (scaleHeight >> 2))))
			{
				newScaleType = ScaleBeam;
				newEditMin = false;
			}
			else if((y < beamAxisBottom) && (y > (beamAxisBottom - (scaleHeight >> 2))))
			{
				newScaleType = ScaleBeam;
				newEditMin = true;
			}
		}
	}

	if(!pSpinBox->isHidden())
	{
		if((scaleType == newScaleType) && (editScaleMin == newEditMin))
		{
			return;
		}
		else	// We have to switch to editing another limit
		{
			pSpinBox->hide();
		}
	}
	else if(newScaleType == ScaleNone)
	{
		pSpinBox->hide();
		return;
	}

	int scaleTop = 0, scaleBottom = 0;
	ProfileLhcLine *pLine;
	switch(newScaleType)
	{
	case ScaleCryo:
		scaleTop = cryoAxisTop;
		scaleBottom = cryoAxisBottom;
		pLine = findLine(ProfileLhcLine::Qrl);
		break;
	case ScaleBeam:
		scaleTop = beamAxisTop;
		scaleBottom = beamAxisBottom;
		pLine = findLine(ProfileLhcLine::OuterBeam);
		break;
	default:
		return;
	}
	int useMin, useMax;
	pLine->getLogLimits(useMin, useMax);
	editScaleMin = newEditMin;
	scaleType = newScaleType;
	settingSpinBox = true;
	if(editScaleMin)
	{
		pSpinBox->setValue(useMin);
		pSpinBox->setRange(-18, 3);
		pSpinBox->adjustSize();
		pSpinBox->move(0, scaleBottom - pSpinBox->height() - 2);
	}
	else
	{
		pSpinBox->setValue(useMax);
		pSpinBox->setRange(-17, 4);
		pSpinBox->adjustSize();
		pSpinBox->move(0, scaleTop + 2);
	}
	pSpinBox->setSingleStep(1);
	pSpinBox->setPrefix("1E");
	pSpinBox->show();
	settingSpinBox = false;
}

/*
**	FUNCTION
**		It has been found that mouse pointer is within vertical scale
**		area for linear scale. Decide if spin box for scale control
**		shall be shown, if yes - configure and show it
**
**	PARAMETERS
**		y		- Y-coordinate of mouse pointer
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::showLinScaleControl(int y)
{
	ScaleType newScaleType = ScaleNone;
	bool	newEditMin = false;

	// Are we close enough to min or max of CRYO scale
	int scaleHeight = linAxisBottom - linAxisTop;
	if(scaleHeight)
	{
		if((y > linAxisTop) && (y < (linAxisTop + (scaleHeight >> 2))))
		{
			newScaleType = ScaleLin;
			newEditMin = false;
		}
		else if((y < linAxisBottom) && (y > (linAxisBottom - (scaleHeight >> 2))))
		{
			newScaleType = ScaleLin;
			newEditMin = true;
		}
	}

	if(!pSpinBox->isHidden())
	{
		if((scaleType == newScaleType) && (editScaleMin == newEditMin))
		{
			return;
		}
		else	// We have to switch to editing another limit
		{
			pSpinBox->hide();
		}
	}
	else if(newScaleType == ScaleNone)
	{
		pSpinBox->hide();
		return;
	}

	// May take limits from any line - they all use the same scale limits
	if (lines.isEmpty()) {
		return;
	}
	ProfileLhcLine *pLine = lines.first();
	int useMin, useMax;
	pLine->getLinLimits(useMin, useMax);
	editScaleMin = newEditMin;
	scaleType = newScaleType;
	settingSpinBox = true;
	if(editScaleMin)
	{
		pSpinBox->setValue(useMin);
		pSpinBox->setRange(0, 495);
		pSpinBox->adjustSize();
		pSpinBox->move(width() - pSpinBox->width(), linAxisBottom - pSpinBox->height() - 2);
	}
	else
	{
		pSpinBox->setValue(useMax);
		pSpinBox->setRange(5, 500);
		pSpinBox->adjustSize();
		pSpinBox->move(width() - pSpinBox->width(), linAxisTop + 2);
	}
	pSpinBox->setSingleStep(5);
	pSpinBox->setPrefix(NULL);
	pSpinBox->show();
	settingSpinBox = false;
}

/*
**	FUNCTION
**		Find profile line of given type
**
**	PARAMETERS
**		type	- Required line type
**
**	RETURNS
**		Pointer to line of required type
**
**	CAUTIONS
**		None
*/
ProfileLhcLine *ProfileLhcView::findLine(int type)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		if(pLine->getType() == type)
		{
			return pLine;
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Slot activated when axis limit has been changed in SpinBox
**
**	PARAMETERS
**		value	- New SpinBox value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::spinBoxChange(int value)
{
	if(settingSpinBox)
	{
		return;
	}
	switch(scaleType)
	{
	case ScaleCryo:
		cryoSpinBoxChange(value);
		break;
	case ScaleBeam:
		beamSpinBoxChange(value);
		break;
	case ScaleLin:
		linSpinBoxChange(value);
		break;
	default:
		break;
	}
}

/*
**	FUNCTION
**		Update vertical limits for CRYO vertical axis from spin box value
**
**	PARAMETERS
**		value	- New SpinBox value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::cryoSpinBoxChange(int value)
{
	ProfileLhcLine *pLine = findLine(ProfileLhcLine::Qrl);
	int useMin, useMax;
	pLine->getLogLimits(useMin, useMax);
	if(editScaleMin)
	{
		useMin = value;
		if(useMax <= useMin)
		{
			useMax = useMin + 1;
		}
	}
	else
	{
		useMax = value;
		if(useMax <= useMin)
		{
			useMin = useMax - 1;
		}
	}
	setCryoLimits(useMin, useMax);
	emit cryoLimitsChanged(useMin, useMax);
}

/*
**	FUNCTION
**		Update vertical limits for BEAM vertical axis from spin box value
**
**	PARAMETERS
**		value	- New SpinBox value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::beamSpinBoxChange(int value)
{
	ProfileLhcLine *pLine = findLine(ProfileLhcLine::OuterBeam);
	int useMin, useMax;
	pLine->getLogLimits(useMin, useMax);
	if(editScaleMin)
	{
		useMin = value;
		if(useMax <= useMin)
		{
			useMax = useMin + 1;
		}
	}
	else
	{
		useMax = value;
		if(useMax <= useMin)
		{
			useMin = useMax - 1;
		}
	}
	setBeamLimits(useMin, useMax);
	emit beamLimitsChanged(useMin, useMax);
}

/*
**	FUNCTION
**		Update vertical limits for linear vertical axis from spin box value
**
**	PARAMETERS
**		value	- New SpinBox value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::linSpinBoxChange(int value)
{
	if (lines.isEmpty()) {
		return;
	}
	ProfileLhcLine *pLine = lines.first();
	int useMin, useMax;
	pLine->getLinLimits(useMin, useMax);
	if(editScaleMin)
	{
		useMin = value;
		if(useMax <= useMin)
		{
			useMax = useMin + 5;
		}
	}
	else
	{
		useMax = value;
		if(useMax <= useMin)
		{
			useMin = useMax - 5;
		}
	}
	setLinLimits(useMin, useMax);
	emit linLimitsChanged(useMin, useMax);
}

/*
**	FUNCTION
**		Find profile device located under mouse pointer
**
**	PARAMETERS
**		x		- Mouse X coordinate
**		y		- Mouse Y coordinate
**		rect	- Device representation area will be returned here
**
**	RETURNS
**		Pointer to profile device under mouse pointer; or
**		NULL if there is no profile item under mouse pointer
**
**	CAUTIONS
**		None
*/
Eqp *ProfileLhcView::findItemUnderMouse(int x, int y, QRect &rect)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		if(!pLine->isHidden())
		{
			Eqp *pEqp = pLine->findItemUnderMouse(x, y, rect);
			if(pEqp)
			{
				return pEqp;
			}
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Write all parameters to output file for debugging .
**
**	PARAMETERS
**		fileName	- Name of output file
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcView::dump(const char *fileName)
{
	if(!DebugCtl::isProfile())
	{
		return;
	}
	FILE	*pFile;
#ifdef Q_OS_WIN
	if(fopen_s(&pFile, fileName, "w"))
	{
		return;
	}
#else
	if(!(pFile = fopen(fileName, "w")))
	{
		return;
	}
#endif
	fprintf(pFile, "LHC from %s (%f) to %s (%f)\n", pStartSector->getName(), start,
		pEndSector->getName(), end);
	fprintf(pFile, "  allowedCryoEqpMask:\n");
	allowedCryoEqpMask.dump(pFile);
	fprintf(pFile, "  allowedBeamEqpMask:\n");
	allowedBeamEqpMask.dump(pFile);
	fprintf(pFile, "  cryoEqpMask:\n");
	cryoEqpMask.dump(pFile);
	fprintf(pFile, "  beamEqpMask:\n");
	beamEqpMask.dump(pFile);

	fprintf(pFile, "  maxBarIdx %d\n", maxBarIdx);
	fprintf(pFile, "  log axis %d CRYO (%d %d) BEAM (%d %d)\n", logAxisX,
		cryoAxisTop, cryoAxisBottom, beamAxisTop, beamAxisBottom);
	fprintf(pFile, "  lin axis %d (%d %d)\n", linAxisX,
		linAxisTop, linAxisBottom);
	fprintf(pFile, "  hasLinearEqp %d linearSelected %d\n", hasLinearEqp, linearSelected);
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		pLine->dump(pFile);
	}
	fclose(pFile);
}

