//	Implementation of VacCtlStateArchive class
/////////////////////////////////////////////////////////////////////////////////

#include "VacCtlStateArchive.h"

#include <qstringlist.h>

#include <stdlib.h>

QList<VacCtlStateArchive *>	*VacCtlStateArchive::pInstances = new QList<VacCtlStateArchive *>();

/*
**	FUNCTION
**		Create new instance, set unique ID for it, return ID of new instance
**
**	PARAMETERS
**		None
**
**	RETURNS
**		ID of new created instance
**
** CAUTIONS
**		None
*/
int VacCtlStateArchive::create(void)
{
	int maxId = 0;
	for(int idx = 0 ; idx < pInstances->count() ; idx++)
	{
		VacCtlStateArchive *pItem = pInstances->at(idx);
		if(pItem->getId() > maxId)
		{
			maxId = pItem->getId();
		}
	}
	maxId++;
	pInstances->append(new VacCtlStateArchive(maxId));
	return maxId;
}

/*
**	FUNCTION
**		Find instance with given ID
**
**	PARAMETERS
**		id	- ID of required instance
**
**	RETURNS
**		Pointer to instance with given ID; or
**		NULL if such instance does not exist
**
** CAUTIONS
**		None
*/
VacCtlStateArchive *VacCtlStateArchive::findInstance(int id)
{
	for(int idx = 0 ; idx < pInstances->count() ; idx++)
	{
		VacCtlStateArchive *pItem = pInstances->at(idx);
		if(pItem->getId() == id)
		{
			return pItem;
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Delete instance with given ID
**
**	PARAMETERS
**		id	- ID of instance to be deleted
**
**	RETURNS
**		true	- if instance has been found (and deleted);
**		false	- if instance with given ID was not found
**
** CAUTIONS
**		None
*/
bool VacCtlStateArchive::deleteInstance(int id)
{
	VacCtlStateArchive *pItem = findInstance(id);
	if(pItem)
	{
		pInstances->removeOne(pItem);
		return true;
	}
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

VacCtlStateArchive::VacCtlStateArchive(int id)
{
	this->id = id;
	pDps = new QList<DpValuesSet *>();
	allTs = NULL;
	nAllTs = 0;
}

VacCtlStateArchive::~VacCtlStateArchive()
{
	clear();
	delete pDps;
}

/*
**	FUNCTION
**		Clear content, free all memory allocated for data
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void VacCtlStateArchive::clear(void)
{
	while(!pDps->isEmpty())
	{
		delete pDps->takeFirst();
	}
	if(allTs)
	{
		free((void *)allTs);
	}
	allTs = NULL;
	nAllTs = 0;
}

/*
**	FUNCTION
**		Add value at given timestamp to DPE data of given DP
**
**	ARGUMENTS
**		dpName	- Name of DP
**		dpeName	- Name of DPE
**		ts		- Timestamp
**		value	- Value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlStateArchive::append(const char *dpName, const char *dpeName,
	const QDateTime &ts, const QVariant &value)
{
	DpValuesSet	*pDp = findDp(dpName);
	if(!pDp)
	{
		pDp = new DpValuesSet(dpName);
		pDps->append(pDp);
	}
	pDp->append(dpeName, ts, value);
}

/*
**	FUNCTION
**		All data for all DPs and DPEs have been added to pool.
**		Sort data for all DPs and DPEs, build resulting array of
**		all timestamps
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlStateArchive::sort(void)
{
	DpValuesSet *pDp;
	int idx;

	// Sort all data for every DP
	for(idx = 0 ; idx < pDps->count() ; idx++)
	{
		pDps->at(idx)->sort();
	}

	// Prepare for building array of all timestamps
	if(allTs)
	{
		free((void *)allTs);
		allTs = NULL;
		nAllTs = 0;
	}
	QList<DpeValuesSet *> dpes;
	for(idx = 0 ; idx < pDps->count() ; idx++)
	{
		pDp = pDps->at(idx);
		const QList<DpeValuesSet *> dpeList(pDp->getDpes());
		for(int dpeIdx = 0 ; dpeIdx < dpeList.count() ; dpeIdx++)
		{
			DpeValuesSet *pDpe = dpeList.at(dpeIdx);
			dpes.append(pDpe);
			pDpe->setIndex(0);
		}
	}

	// Do the job
	QDateTime lastTs;
	while(true)
	{
		const QDateTime		*pBestTs = NULL;
		DpeValuesSet	*pBestDpe = NULL;
		for(idx = 0 ; idx < dpes.count() ; idx++)
		{
			DpeValuesSet *pDpe = dpes.at(idx);
			const DpeValue *pValue = pDpe->getValueAt(pDpe->getIndex());
			if(!pValue)
			{
				continue;
			}
			if(!pBestTs)
			{
				pBestTs = &(pValue->getTs());
				pBestDpe = pDpe;
			}
			else if(*pBestTs > pValue->getTs())
			{
				pBestTs = &(pValue->getTs());
				pBestDpe = pDpe;
			}
		}
		if(!pBestDpe)
		{
			break;	// No more values to process
		}
		if(*pBestTs != lastTs)	 // New timestamp, add new pointer to allTs array
		{
			if(!(nAllTs & 0x1F))
			{
				allTs = (const QDateTime **)realloc((void *)allTs, (nAllTs + 0x20) * sizeof(QDateTime *));
			}
			allTs[nAllTs++] = pBestTs;
			lastTs = *pBestTs;
		}
		// Move to next timestamp to be processed !!!!
		pBestDpe->setIndex(pBestDpe->getIndex() + 1);
	}
}

/*
**	FUNCTION
**		Find values of all required DPEs for given DP at given moment.
**		At least one of DPEs must have value exactly at that moment,
**		for other DPEs values from previous moment can be borrowed.
**		'Success' (== all values found) is only achieved when:
**		- At least for one of DPEs in list there is a value exactly
**			at given moment
**		- For all other DPEs there is a value before given moment
**
**		If DPE is not known in this data pool - the reason could be that
**		DPE just was not archived. For such DPEs method assumes 'there is
**		value 0 far in the past'.
**
**	ARGUMENTS
**		dpName		- Name of DP
**		ts			- Timestamp when values are required
**		dpeNames	- List of DPE names for which values are required
**		values		- List where values will be added IN THE SAME ORDER
**						as order of DPE names in dpeNames
**
**	RETURNS
**		true	= values have been found;
**		false	= No values at that moment
**
**	CAUTIONS
**		New instances of QVariant are added to values, caller is responsible for
**		deleting them (simple way is to set autoDelete flag to true for values).
*/
bool VacCtlStateArchive::getDpValues(const char *dpName, const QDateTime &ts,
	QList<QByteArray> &dpeNames, QList<QVariant *> &values)
{
	values.clear();
	DpValuesSet *pDp = findDp(dpName);
	if(!pDp)
	{
		return false;
	}

	// First check that at least one DPE has value at requited moment
	DpeValuesSet	*pExactDpe = NULL;
	DpeValue		*pExactValue = NULL;
	int idx;
	for(idx = 0 ; idx < dpeNames.count() ; idx++)
	{
		DpeValuesSet *pDpe = pDp->findDpeData(dpeNames.at(idx));
		if(pDpe)
		{
			DpeValue *pValue = pDpe->findValueAt(ts);
			if(pValue)
			{
				pExactDpe = pDpe;
				pExactValue = pValue;
				break;
			}
		}
	}
	if(!pExactDpe)
	{
		return false;	// No DPE having value at required moment
	}

	// At least one exact value found, find all other values and add them
	// to resulting list.
	for(idx = 0 ; idx < dpeNames.count() ; idx++)
	{
		DpeValuesSet *pDpe = pDp->findDpeData(dpeNames.at(idx));
		QVariant *pResult = NULL;
		if(!pDpe)
		{
			pResult = new QVariant(0);	// 0 far in the past
		}
		else
		{
			if(pDpe == pExactDpe)
			{
				pResult = new QVariant(pExactValue->getValue());
			}
			else
			{
				DpeValue *pValue = pDpe->findValueAt(ts);
				if(!pValue)
				{
					pValue = pDpe->findPreviousValue(ts);
				}
				if(pValue)
				{
					pResult = new QVariant(pValue->getValue());
				}
				else
				{
					pResult = new QVariant(0);	// 0 far in the past return false;	// No exact or previous value
				}
			}
		}
		values.append(pResult);
	}
	return true;
}

/*
**	FUNCTION
**		Find timestamp for given DP BEFORE given timestamp
**
**	PARAMETERS
**		dpName		- Name of DP for which values are required
**		ts			- Timestamp before which other timestamp is required
**		prevTs		- Previous timestamp for this DP will be put to this variable
**		errMsg		- Pointer to buffer where error message shall be written in case of error
**
**	RETURNS
**		>0	- Previous timestamp has been found
**		0	- No previous timestamp
**		-1	- in case of error (no data for requested DP)
**
**	CAUTIONS
**		None
*/
int VacCtlStateArchive::getPreviousTs(const char *dpName, const QDateTime &ts, QDateTime &prevTs,
	QString &errMsg)
{
	DpValuesSet *pDp = findDp(dpName);
	if(!pDp)
	{
		errMsg = "DP <";
		errMsg += dpName;
		errMsg += "> not found in state archive pool";
		return( -1 );
	}
	return getPreviousTs(pDp, ts, prevTs);
}

/*
**	FUNCTION
**		Find timestamp for given DP BEFORE given timestamp
**
**	PARAMETERS
**		pDp			- Pointer to DP data for which values are required
**		ts			- Timestamp before which other timestamp is required
**		prevTs		- Previous timestamp for this DP will be put to this variable
**
**	RETURNS
**		>0	- Previous timestamp has been found
**		0	- No previous timestamp
**		-1	- in case of error (no data for requested DP)
**
**	CAUTIONS
**		None
*/
int VacCtlStateArchive::getPreviousTs(DpValuesSet *pDp, const QDateTime &ts, QDateTime &prevTs)
{
	QDateTime result;	// NULL value
	const QList<DpeValuesSet *> &dpes = pDp->getDpes();
	for(int idx = 0 ; idx < dpes.count() ; idx++)
	{
		DpeValuesSet *pDpe = dpes.at(idx);
		DpeValue *pValue = pDpe->findPreviousValue(ts);
		if(pValue)
		{
			if(result.isNull())
			{
				result = pValue->getTs();
			}
			else if(pValue->getTs() > result)
			{
				result = pValue->getTs();
			}
		}
	}
	if(result.isNull())
	{
		return 0;
	}
	prevTs = result;
	return 1;
}

/*
**	FUNCTION
**		Find timestamp for given DP AFTER given timestamp
**
**	PARAMETERS
**		dpName		- Name of DP for which values are required
**		ts			- Timestamp after which other timestamp is required
**		nextTs		- Next timestamp for this DP will be put to this variable
**		errMsg		- Pointer to buffer where error message shall be written in case of error
**
**	RETURNS
**		>0	- Next timestamp has been found
**		0	- No next timestamp
**		-1	- in case of error (no data for requested DP)
**
**	CAUTIONS
**		None
*/
int VacCtlStateArchive::getNextTs(const char *dpName, const QDateTime &ts, QDateTime &nextTs,
	QString &errMsg)
{
	DpValuesSet *pDp = findDp(dpName);
	if(!pDp)
	{
		errMsg = "DP <";
		errMsg += dpName;
		errMsg += "> not found in state archive pool";
		return( -1 );
	}
	return getNextTs(pDp, ts, nextTs);
}

/*
**	FUNCTION
**		Find timestamp for given DP AFTER given timestamp
**
**	PARAMETERS
**		pDp			- Pointer to DP data for which values are required
**		ts			- Timestamp after which other timestamp is required
**		nextTs		- Next timestamp for this DP will be put to this variable
**
**	RETURNS
**		>0	- Next timestamp has been found
**		0	- No next timestamp
**		-1	- in case of error (no data for requested DP)
**
**	CAUTIONS
**		None
*/
int VacCtlStateArchive::getNextTs(DpValuesSet *pDp, const QDateTime &ts, QDateTime &nextTs)
{
	QDateTime result;	// NULL value
	const QList<DpeValuesSet *> &dpes = pDp->getDpes();
	for(int idx = 0 ; idx < dpes.count() ; idx++)
	{
		DpeValuesSet *pDpe = dpes.at(idx);
		DpeValue *pValue = pDpe->findNextValue(ts);
		if(pValue)
		{
			if(result.isNull())
			{
				result = pValue->getTs();
			}
			else if(pValue->getTs() < result)
			{
				result = pValue->getTs();
			}
		}
	}
	if(result.isNull())
	{
		return 0;
	}
	nextTs = result;
	return 1;
}

/*
**	FUNCTION
**		Get indexes of previous and next timestamps where values exist for given DP.
**		Indixes are in list of ALL timestamps, not in timestamps related to this DP!!!
**
**	PARAMETERS
**		dpName		- Name of DP for which values are required
**		ts			- Timestamp for which neighbour timestamps are required
**		prevIdx		- Index of previous timestamp for this DP will be put to this variable,
**						0 if no previous value for DP
**		nextIdx		- Index of next timestamp for this DP will be put to this variable,
**						0 if no next value for this DP
**		errMsg		- Pointer to buffer where error message shall be written in case of error
**
** RETURNS
**		>0	- At least one of neighbours found
**		0	- No neighbours
**		-1	- in case of error (no data for requested DP)
**
** CAUTIONS
**		The buffer allocated by caller for errMsg must be large enough for error message
*/
int VacCtlStateArchive::getNeighbourIndexes(const char *dpName, const QDateTime &ts,
	int &prevIdx, int &nextIdx, QString &errMsg)
{
	int	coco = 0;
	DpValuesSet *pDp = findDp(dpName);
	if(!pDp)
	{
		errMsg = "DP <";
		errMsg += dpName;
		errMsg += "> not found in state archive pool";
		return( -1 );
	}

	// Previous
	QDateTime prevTs;
	if(getPreviousTs(pDp, ts, prevTs) > 0)
	{
		prevIdx = findTsIndex(prevTs);
		if(prevIdx)
		{
			coco++;
		}
	}
	else
	{
		prevIdx = 0;
	}

	// Next
	QDateTime nextTs;
	if(getNextTs(pDp, ts, nextTs) > 0)
	{
		nextIdx = findTsIndex(nextTs);
		if(nextIdx)
		{
			coco++;
		}
	}
	else
	{
		nextIdx = 0;
	}
	return coco;
}

/*
**	FUNCTION
**		Find data for DP with given name
**
**	ARGUMENTS
**		dpName	- Name of DP
**
**	RETURNS
**		Pointer to data for given DP; or
**		NULL if such DP is not found
**
**	CAUTIONS
**		None
*/
DpValuesSet	*VacCtlStateArchive::findDp(const char *dpName)
{
	for(int idx = 0 ; idx < pDps->count() ; idx++)
	{
		DpValuesSet *pDp = pDps->at(idx);
		if(pDp->getName() == dpName)
		{
			return pDp;
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Find index of timestamp in array of all timestamps
**
** PARAMETERS
**		ts	- Timestamp for which index is required
**
** RETURNS
**		Index of timestamp in array, or
**		0 if timestamp not found
**
** CAUTIONS
**		None
*/
int VacCtlStateArchive::findTsIndex(const QDateTime &ts)
{
	int	high = nAllTs, low = -1, probe;
	while((high - low) > 1)
	{
		probe = (low + high) >> 1;
		if( *(allTs[probe]) > ts)
		{
			high = probe;
		}
		else
		{
			low = probe;
		}
	}
	if(low < 0)
	{
		return 0;
	}
	return *(allTs[low]) == ts ? (low + 1) : 0;
}
