#ifndef	MESSAGEFILTERINGRULE_H
#define	MESSAGEFILTERINGRULE_H

#include "VacCtlOnlineNotificationExport.h"

// Description of message filtering rules

#include <qstring.h>

class VACCTLONLINENOTIFICATION_EXPORT MessageFiltering
{
public:
	/*
	// Type of filtering
	typedef enum
	{
		None = 0,		// No filtering - messages are sent ass they appear
		DeadTime = 1,	// Messages are sent not too often
		Grouping = 2	// Individual messages are grouped
	} FilterType;

	// Type of grouping - how messages are grouped accoring to source
	typedef enum
	{
		GroupMachine = 0,	// independently on source
		GroupMainPart = 1,	// all messages of main part
		GroupSector = 2,	// all messages of vacuum sector
		GroupArc = 3		// all messages of LHC arc
	} GroupType;
	*/

	MessageFiltering();
	virtual ~MessageFiltering();

	// Access
	inline int getType(void) const { return type; }
	void setType(int newType, QString &errMsg);
	inline int getDeadTime(void) const { return deadTime; }
	void setDeadTime(int newTime, QString &errMsg);
	inline int getGroupType(void) const { return groupType; }
	void setGroupType(int newType, QString &errMsg);
	inline int getGroupLimit(void) const { return groupLimit; }
	void setGroupLimit(int newLimit, QString &errMsg);
	inline int getGroupInterval(void) const { return groupInterval; }
	void setGroupInterval(int interval, QString &errMsg);
	inline bool isGroupSendInitial(void) const { return groupSendInitial; }
	inline void setGroupSendInitial(bool flag) { groupSendInitial = flag; }
	inline bool isValid(void) const { return valid; }

protected:
	// Type of filtering - see enum above
	int		type;

	// Dead time for messages: the same message is sent not more often than
	// this number of seconds
	int		deadTime;

	// Grouping: how individual messages are grouped together - see enum above
	int		groupType;

	// Grouping: limit for message count to decide if single message shall replace
	// all individual messages with single group message
	int		groupLimit;

	// Grouping: time interval during which individual messages are just counted
	// instead of being sent
	int		groupInterval;

	// Grouping: flag indicating if 1st message of group is sent immediately or not
	bool	groupSendInitial;

	// Flag indicating if this filtering as a whole is valid
	bool	valid;
};

#endif	// MESSAGEFILTERINGRULE_H
