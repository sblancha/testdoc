#ifndef	PROFILELHCLINEISOL_H
#define	PROFILELHCLINEISOL_H

// LHC profile line - isolation vacuum

#include "ProfileLhcLine.h"

#include "RealCryoSector.h"

#include "ProfileLhcVpgConnector.h"

class ProfileLhcLineIsol : public ProfileLhcLine  
{
	Q_OBJECT

public:
	ProfileLhcLineIsol(ProfileLhcView *parent, int type, float start, float end);
	~ProfileLhcLineIsol();

	virtual void clear(void);
	virtual void applyMask(VacEqpTypeMask &mask);
	virtual float nextPosition(void);
	virtual void placeItem(float& coord, int& barIdx);
	virtual void calcBarGeometry(int &maxBarIdx);
	virtual void buildSectors(QFontMetrics &fontMetrics);
	virtual void bestPositionForEqp(float coord, float &bestDelta, int &screenCoord);

	virtual void dump(FILE *pFile);

protected:
	QList<RealCryoSector *>	realSectors;

	// List of VPG_BP devices which may affect sector reference of gauges
	QList<ProfileLhcVpgConnector *>	vpgs;

	// Index of next real sector to be processed
	int					curSectorIdx;

	// true if real sector shall be placed when placing graph items
	bool				placeSector;

	virtual void buildEquipment(void);
	void addRealSector(Sector *pSector);
	void checkCoordAndBar(float &coord, int &barIdx, float newCoord);
	int compareToSectorBorder(float coord, float sectorBorder);
	bool isEqpOfThisSector(Sector *pSector, Eqp *pEqp);
	void addSector(ProfileLineSector *pSector, int areaNumberInSector);
	virtual void drawSectorsHatchFill(QPainter &painter);
	virtual void drawSectorsWithBorders(QPainter &painter);


	virtual bool isMyEqp(Eqp *pEqp, int &partIdx);
	virtual void addEqp(Eqp *pEqp, int partIdx);

	// Implementation of LhcLineParts abstract methods
	virtual bool isMySector(Sector *pSect);
	virtual LhcLineParts::BuildType linePartMethod(void) { return LhcLineParts::TypeSector; }

private slots:
	void sectorChange(Eqp *pSrc);
};


#endif	// PROFILELHCLINEISOL_H
