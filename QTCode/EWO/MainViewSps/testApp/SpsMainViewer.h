#ifndef SPSMAINVIEWR_H
#define SPSMAINVIEWR_H

// Test application for SPS main view EWO

#include "MainSps.h"

#include <qmainwindow.h>
#include <qpushbutton.h>

class SpsMainViewer : public QMainWindow
{
	Q_OBJECT

public:
	SpsMainViewer(QWidget *parent = NULL, const char *name = NULL, WFlags = WType_TopLevel);
	~SpsMainViewer();

protected slots:
	void readData(void);

protected:
	MainSps	*pView;

};

#endif	// SPSMAINVIEWR_H
