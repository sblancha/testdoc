#ifndef	VACICONVPG_SA_VG_H
#define	VACICONVPG_SA_VG_H

// Icon for VPG_SA_VG	- LHC standalone pumping group with gauge

#include "VacIconVPG_SA.h"

class VACCTLEWOUTIL_EXPORT VacIconVPG_SA_VG : public VacIconVPG_SA
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPG_SA_VG(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPG_SA_VG();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual void setMode(DataEnum::DataMode newMode);
	virtual void setPipeColor(const QColor &newColor);
	virtual bool connect(void);
	virtual bool disconnect(void);

	// Override method of VacIcon 
	virtual void setDirection(VacIconContainer::Direction newDirection);

protected:
	// Areas for gauge symbol(s)
	QRect	vgr;	// Or VGF... approximately
	QRect	vgp;

	// Child gauges icons
	VacIcon *pIconVGR;
	VacIcon *pIconVGP;
	VacIcon	*pIconVGF;

	virtual void setAreaGeometry(void);
	VacIconContainer::Direction calculateVgrDirection(void);
	VacIconContainer::Direction calculateVgpDirection(void);

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACICONVPG_SA_VG_H
