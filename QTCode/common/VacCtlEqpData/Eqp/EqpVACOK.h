#ifndef	EQPVACOK_H
#define	EQPVACOK_H

//	VACOK - interlocks/alarms

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVACOK : public Eqp
{
	Q_OBJECT

public:
	// Logic type
	typedef enum
	{
		AND = 0,
		OR = 1
	} LogicType;

	EqpVACOK(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVACOK(const EqpVACOK &src) : Eqp(src) {}
#endif
	~EqpVACOK();

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual bool isSpecColor(void);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual int getDrawOrder(void);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline LogicType getLogicType(void) const { return logicType; }
	bool isValid(void);
	bool isOK(void);
	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Logic type
	LogicType		logicType;

	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, bool plcAlarm, QString &string);
#endif
};

#endif	// EQPVACOK_H
