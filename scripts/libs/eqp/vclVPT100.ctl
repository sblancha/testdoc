
// PT100 thermometer on dump lines

const unsigned  VPT100_R_OFF = 0x00010000u,
  VPT100_R_ON = 0x00020000u,
  VPT100_R_T_VALID = 0x00040000u,
  VPT100_R_WARNINGS = 0x00400000u,
  VPT100_R_ERRORS = 0x00800000u,
  VPT100_R_VALID = 0x40000000u;
		
	

//LhcVacNameString_VPT100:
/** Build string to be shown as a first item of popup menu for PT100 thermometer

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name
  @param mainData: in, contains device specific data
    1 - stateRR1
    2 - temperature
    3 - alarm
  @param menuNameString: out, resulting string for 1st menu item is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacNameString_VPT100(string dpName, dyn_anytype mainData, string &menuNameString,
	dyn_string &exceptionInfo)
{
  string devName, addString;

  dpName = dpSubStr(dpName, DPSUB_DP);
  LhcVacDisplayName(dpName, devName, exceptionInfo);
  if(mainData[3] != 0)
  {
    addString = VPT100_STATE_PLC_ERROR;
  }
  else if(mainData[1] & VPT100_R_VALID)
  {
    sprintf(addString, "%d", mainData[2]);
  }
  if(addString != "")
  {
    menuNameString = "PUSH_BUTTON," + devName + " : " + addString + ",0,1";
  }
  else
  {
    menuNameString = "PUSH_BUTTON," + devName + ",0,1";
  }
}
//LhcVacActionPossible_VPT100:
/** No actions for the time being

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name
  @param dpType: in, DP type
  @param act: in, action to be executed
  @param possible: out, result (allowed/not allowed) is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_VPT100(string dpName, string dpType, dyn_anytype act, bool &possible,
  dyn_string &exceptionInfo)
{
  possible = false;
}

//LhcVacDeviceAction_VPT100:
/** Execute action for transmitter. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VPT100(string dpName, string dpType, dyn_anytype action, dyn_string &exceptionInfo)
{
  mapping   mConnectDpes;
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act & LVA_ACTION_MASK)
  {
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["T"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = false;
    mConnectDpes["AL2"] = false;
    mConnectDpes["AL3"] = false;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["T"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = false;
    mConnectDpes["AL2"] = false;
    mConnectDpes["AL3"] = false;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }
//  fwException_raise(exceptionInfo,  "ERROR", "LhcVacDeviceAction_VPT100: NOT YET IMPLEMENTED", "");
}



