//	Implementation of FunctionalTypeV8DI_FE class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeV8DI_FE.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include <stdio.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeV8DI_FE::FunctionalTypeV8DI_FE(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeV8DI_FE::fillCriteria(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Active, "Active Signal(s)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: at least one active signal");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no active signals");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Active digital input signal in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"No active digital input signal in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Active digital input signal in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"No active digital input signal in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Unacknowledged, "Unacknowledged Signal(s)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: at least one unacknowledged signal");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no unacknowledged signals");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Unacknowldged digital input signal in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"No Unacknowldged digital input signal in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Unacknowldged digital input signal in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"No Unacknowldged digital input signal in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "Digital input ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Digital Input in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Digital Input in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Digital Input in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Digital Input in main part {mainPart}: no error");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeV8DI_FE::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case Active:
			result = "Active";
			break;
		case Unacknowledged:
			result = "Unacknowledged";
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
	return result;
}

int FunctionalTypeV8DI_FE::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "Active"))
		{
			result = Active;
		}
		else if(!strcasecmp(name, "Unacknowledged"))
		{
			result = Unacknowledged;
		}
		break;
	default:
		break;
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}

