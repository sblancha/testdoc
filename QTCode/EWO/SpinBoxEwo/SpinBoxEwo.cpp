//	Implementation of SpinBoxEwo class
/////////////////////////////////////////////////////////////////////////////////

#include "SpinBoxEwo.h"

EWO_PLUGIN(SpinBoxEwo)

SpinBoxEwo::SpinBoxEwo(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new QSpinBox(parent);
	setting = false;
	connect(baseWidget, SIGNAL(valueChanged(int)),
		this, SLOT(valueChange(int)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList SpinBoxEwo::signalList(void) const
{
	QStringList list;
	list.append("valueChanged()");
	return list;
}

/*
**	FUNCTION
**		Slot receiving valveChanged signal of QDateTimeEdit, emit
**		signal 'valueChanged' to PVSS
**
**	ARGUMENTS
**		value	- New date + time value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SpinBoxEwo::valueChange(int /* value */)
{
	if(setting)
	{
		return;
	}
	setting = true;
	emit signal("valueChanged");
	setting = false;
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList SpinBoxEwo::methodList(void) const
{
	QStringList list;
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant SpinBoxEwo::invokeMethod(const QString &name,
	QList<QVariant> & /* values */, QString &error)
{
	error = "Uknown method SpinBoxEwo.";
	error += name;
	return QVariant();
}

