#ifndef RACKICON_H
#define RACKICON_H

#include "VacCtlEwoUtilExport.h"
#include "VacIconConnection.h"

#include <qwidget.h>

class VACCTLEWOUTIL_EXPORT RackIcon : public QWidget
{
	Q_OBJECT

signals :
	void mouseDown(int button, int mode, int x, int y, int extra, const char *eqpName);
public:

	enum eqpType
	{
		VRPITHJA = 0,
		VRPITHJB,
		VRPITCFD,
		VRGPT300 = 40,
		VRGPK001,
		VRGCT001 = 50,
		VRPCT001,
		VRLRI,
		VRPTP350,
		VRPTP120,
		VRVCL,
		VRVCX,
		GENERIC = 1000
	};

	// Note parent argument is mandatory in constructor
	RackIcon(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~RackIcon();

	static RackIcon * getIcon(QString type, QWidget * parent);
	static eqpType getType(QString eqpType);
	// Connect to device's DPEs
	virtual bool connect(void) = 0;

	// Disconnect from device's DPEs
	virtual bool disconnect(void) = 0;

	// Assign equipment to icon - after that icon can take device's data
	virtual void setEqp(Eqp *pEqp) = 0;

	//set size of widget
	virtual void setSize(int sizeX, int sizeY);

	virtual inline void setAttributeList(QMap<QString, QString> &map){ attributes = map;};

	virtual void setName(QString name);
	virtual inline QString getName(){ return equipmentName; };

	//functions not expected to be redefined by children classes
	inline void setScale(qreal newScale){ this->scale = newScale; };
	void recalculatePosition(int pixelRackWidth, int pixelRackHeight, int pixelRackFrameWidth, int pixelRackFrameHeight);
	void setPosition(int positionX, int positionY);
	virtual void selectEqp(void);
	virtual void deselectEqp(void);

public slots:
	// Force redraw of this icon
	virtual void forceRedraw(void);

signals:
	void selectedEqp(RackIcon * pIcon);

protected:
	QWidget * parent;
	VacIconConnection	connection;

	//flag to determine if device is connected
	bool connected;

	//flag to select equipment
	bool isSelected;

	//information about equipment properties
	int sizeX;
	int sizeY;
	int positionX;//position in 1-100 scale inside rack width
	int positionY;//position in rack units
	qreal scale;
	QMap<QString, QString> attributes;

	QString equipmentName;
	
	// React on mouse press
	virtual void mousePressEvent(QMouseEvent *pEvent);
};

#endif