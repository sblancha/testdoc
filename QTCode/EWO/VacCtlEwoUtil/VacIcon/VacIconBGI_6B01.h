#ifndef	VACICONBGI_6B01_H
#define	VACICONBGI_6B01_H

//	Icon for BEAM intensity

#include "VacIcon.h"

class EqpProcess;

class VACCTLEWOUTIL_EXPORT VacIconBGI_6B01 : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconBGI_6B01(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconBGI_6B01();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpProcess *pEqp;

	// Label to draw (BGI or BGV), default is BGI
	QString		bgiLabel;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void setMainColor(QPainter &painter);
	virtual void setErrorColor(QPainter &painter);
	virtual void setModeColor(QPainter &painter);
};

#endif	// VACICONBGI_6B01_H
