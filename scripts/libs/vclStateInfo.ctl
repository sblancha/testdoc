
#uses "vclResources.ctl"	// Load CTRL library

/**
** Set of functions providing redable status information for different DP types.
** The primary source of information is resource file
*/

// Template for name of resource holding bit number in status
// The name of resource is built by adding bit number to this string
const string RESOURCE_BIT_NUMBER = "Bit_";

// Name of resource holding short name for bit state
const string RESOURCE_SHORT_BIT_NAME = "ShortName";

// Name of resource holding full name for bit state
const string RESOURCE_LONG_BIT_NAME = "LongName";

// Name of resource holding LSB (Least Significant Bit) number for error code in status
const string RESOURCE_ERR_CODE_LSB = "ErrorCodeLSB";

// Name of resource holding MSB (Most Significant Bit) number for error code in status
const string RESOURCE_ERR_CODE_MSB = "ErrorCodeMSB";

// Template for name of resource holding error message corresponding to given error code.
// The name of resource is built by adding error code to this string
const string RESOURCE_ERROR_MSG = "Error_";

// Template for name of resource holding the Read register containing the error code
// The name of resource is built by adding error code to this string
const string RESOURCE_ERROR_RR = "ErrorRR";

// Name of resource holding LSB (Least Significant Bit) number for warning code in status
const string RESOURCE_WARNING_CODE_LSB = "WarningCodeLSB";

// Name of resource holding MSB (Most Significant Bit) number for warning code in status
const string RESOURCE_WARNING_CODE_MSB = "WarningCodeMSB";

// Template for name of resource holding error message corresponding to given warning code.
// The name of resource is built by adding warning code to this string
const string RESOURCE_WARNING_MSG = "Warning_";

// Template for name of resource holding the Read register containing the warning code
// The name of resource is built by adding error code to this string
const string RESOURCE_WARNING_RR = "WarningRR";

/** VacStateBitInfo
**
** Find state bit information for given DP name and DPE name
**
**  @param dpName         - [in], Name of DP for which information is required
**  @param dpeName        - [in], Name of DPE containing status information
**  @param msb            - [out], Highest bit number with non-empty status bit information
**  @param shortBitNames  - [out], array of short names for status bits,
**                            length of array is equal to msb,
**                            name for bit N is stored in shortBitNames[N+1]
**  @param longBitNames   - [out], array of full names for status bits,
**                            length of array is equal to msb,
**                            name for bit N is stored in longBitNames[N+1]
**  @param exceptionInfo  - [out], any errors found are reported here
*/
void VacStateBitInfo(string dpName, string dpeName, int &msb, dyn_string &shortBitNames,
  dyn_string &longBitNames, dyn_string &exceptionInfo)
{
  msb = -1;
  dynClear(shortBitNames);
  dynClear(longBitNames);

  string dpType;
  if((dpType = dpTypeName(dpName)) == "")
  {
    fwException_raise(exceptionInfo,
      "ERROR", "VacStateBitInfo(): dpTypeName(" + dpName + ") failed", "");
    return;
  }

  dyn_string  shortNames, longNames;
  for(int bitNbr = 31 ; bitNbr >= 0 ; bitNbr--)	// At most 32 bits per status word
  {
    string resValue = VacResourcesGetValue(dpType + "." + dpeName + "." +
      RESOURCE_BIT_NUMBER + bitNbr + "." + RESOURCE_SHORT_BIT_NAME, VAC_RESOURCE_STRING, "");
    if(resValue != "")
    {
      if(msb < bitNbr)
      {
        msb = bitNbr;
      }
      dynAppend(shortNames, resValue);
    }
    else if(bitNbr <= msb)
    {
      dynAppend(shortNames, resValue);
    }
    resValue = VacResourcesGetValue(dpType + "." + dpeName + "." +
      RESOURCE_BIT_NUMBER + bitNbr + "." + RESOURCE_LONG_BIT_NAME, VAC_RESOURCE_STRING, "");
    if(resValue != "")
    {
      if(msb < bitNbr)
      {
        msb = bitNbr;
      }
      dynAppend(longNames, resValue);
    }
    else if(bitNbr <= msb)
    {
      dynAppend(longNames, resValue);
    }		
  }

  if(dynlen(shortNames) == 0)
  {
    return;
  }

  // Copy names to output arrays. Note that shortNames and longNames contain bit names
  // in REVERSE order
  for(int bitNbr = msb + 1 ; bitNbr > 0 ; bitNbr--)
  {
    string resValue;
    if(bitNbr <= dynlen(shortNames))
    {
      resValue = shortNames[bitNbr];
      dynAppend(shortBitNames, resValue);
    }
    else
    {
      dynAppend(shortBitNames, "");
    }
    if(bitNbr <= dynlen(longNames))
    {
      resValue = longNames[bitNbr];
      dynAppend(longBitNames, resValue);
    }
    else
    {
      dynAppend(longBitNames, "");
    }
  }
}

/** VacStateErrorCode
**
** Extract error code value from status value for given DPE
**
**  @param dpName    - [in], Name of DP for which information is required
**  @param dpType    - [in/out], Name of DP type, if empty - name of dpType will be retrieved from dpName
**  @param dpeName   - [in], Name of DPE containing status information
**  @param dpeValue  - [in], DPE value
**
**  @return - Error code extracted from status value
*/
int VacStateErrorCode(string dpName, string &dpType, string dpeName, unsigned dpeValue)
{
  if(dpType == "")
  {
    if((dpType = dpTypeName(dpName)) == "")
    {
      DebugTN("ERROR: VacStateErrorCode(): dpTypeName(" + dpName + ") failed");
      return 0;
    }
  }

  int errLsb = VacResourcesGetValue(dpType + "." + dpeName + "." + RESOURCE_ERR_CODE_LSB, VAC_RESOURCE_INT, -1);
  if(errLsb < 0)
  {
    return 0;
  }
  int errMsb = VacResourcesGetValue(dpType + "." + dpeName + "." + RESOURCE_ERR_CODE_MSB, VAC_RESOURCE_INT, -1);
  if(errMsb < 0)
  {
    return 0;
  }

  if(errMsb < errLsb)
  {
    DebugTN("ERROR: VacStateErrorCode(" + dpName + "," + dpeName + "): error code LSB " + errLsb + ", MSB " + errMsb);
    return 0;
  }

  int result = (dpeValue >> errLsb) & ((1 << (errMsb - errLsb + 1)) - 1);
  return result;
}

/** VacStateErrorString
**
** Extract error code value from status value for given DPE and return
** string representation of this error
**
**  @param dpName    - [in], Name of DP for which information is required
**  @param dpeName   - [in], Name of DPE containing status information
**  @param dpeValue  - [in], DPE value
**
**  @return - String representation of error code extracted from status value
*/
string VacStateErrorString(string dpName, string dpeName, unsigned dpeValue)
{
  string dpType;

  int errCode = VacStateErrorCode(dpName, dpType, dpeName, dpeValue);
  if(errCode == 0)
  {
    return "";
  }

  // First try to read DP type specific error:
  string result = VacResourcesGetValue(dpType + "." + RESOURCE_ERROR_MSG + errCode,
    VAC_RESOURCE_STRING, "");

  // If not found - try to find common error
  if(result == "")
  {
    result = VacResourcesGetValue(RESOURCE_ERROR_MSG + errCode, VAC_RESOURCE_STRING, "");
  }

  // If not found anyway - produce some message
  if(result == "")
  {
    sprintf(result, "Unknown error code %d", errCode);
  }
  return result;
}
/** VacStateWarningString
**
** Extract warning code value from status value for given DPE and return
** string representation of this warning
**
**  @param dpName    - [in], Name of DP for which information is required
**  @param dpeName   - [in], Name of DPE containing status information
**  @param dpeValue  - [in], DPE value
**
**  @return - String representation of warning code extracted from status value
*/
string VacStateWarningString(string dpName, string dpeName, unsigned dpeValue)
{
  string dpType;

  int warningCode = VacStateWarningCode(dpName, dpType, dpeName, dpeValue);
  if(warningCode == 0)
  {
    return "";
  }

  // Try to read DP type specific warning:
  string result = VacResourcesGetValue(dpType + "." + RESOURCE_WARNING_MSG + warningCode,
    VAC_RESOURCE_STRING, "");

  if(result == "")
  {
    sprintf(result, "Unknown warning code %d", warningCode);
  }
  return result;
}
/** VacStateWarningCode
**
** Extract warning code value from status value for given DPE
**
**  @param dpName    - [in], Name of DP for which information is required
**  @param dpType    - [in/out], Name of DP type, if empty - name of dpType will be retrieved from dpName
**  @param dpeName   - [in], Name of DPE containing status information
**  @param dpeValue  - [in], DPE value
**
**  @return - Warning code extracted from status value
*/
int VacStateWarningCode(string dpName, string &dpType, string dpeName, unsigned dpeValue)
{
  if(dpType == "")
  {
    if((dpType = dpTypeName(dpName)) == "")
    {
      DebugTN("ERROR: VacStateWarningCode(): dpTypeName(" + dpName + ") failed");
      return 0;
    }
  }

  int warningLsb = VacResourcesGetValue(dpType + "." + dpeName + "." + RESOURCE_WARNING_CODE_LSB, VAC_RESOURCE_INT, -1);
  if(warningLsb < 0)
  {
    return 0;
  }
  int warningMsb = VacResourcesGetValue(dpType + "." + dpeName + "." + RESOURCE_WARNING_CODE_MSB, VAC_RESOURCE_INT, -1);
  if(warningMsb < 0)
  {
    return 0;
  }

  if(warningMsb < warningLsb)
  {
    DebugTN("ERROR: VacStateWarningCode(" + dpName + "," + dpeName + "): warning code LSB " + warningLsb + ", MSB " + warningMsb);
    return 0;
  }

  int result = (dpeValue >> warningLsb) & ((1 << (warningMsb - warningLsb + 1)) - 1);
  return result;
}
