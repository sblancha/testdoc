#ifndef FUNCTIONALTYPEVPGM_H
#define FUNCTIONALTYPEVPGM_H

// Special functional type for mobile pumping groups - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVPGM : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Connected = 1,
		On = 2,
		Off = 3,
		VvrOpen = 4,
		VvrClosed = 5,
		TmpOn = 6,
		TmpOff = 7
	};

	FunctionalTypeVPGM(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVPGM() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVPGM_H
