//	Implementation of VacCtlUtil class
/////////////////////////////////////////////////////////////////////////////////

#include "VacCtlUtil.h"

#include <stdarg.h>
#include <ctype.h>

int VacCtlUtil::sprintf(char *buf, const char *fmt, ...)
{
	va_list(argList);
	va_start(argList, fmt);
#ifdef _WIN32
	int coco = vsprintf_s(buf, 32, fmt, argList);
#else
	int coco = vsprintf(buf, fmt, argList);
#endif
	va_end(argList);

	// The method is used to print with %E format, remove from result
	// extra zero in power
	int len = (int)strlen(buf), digitCount = 0;
	char *c = &buf[len - 1];
	bool eFound = false;
	for( ; c != buf ; c--)
	{
		if(isdigit((int)(*c)))
		{
			digitCount++;
		}
		else if(((*c) == 'E') || ((*c) == 'e'))
		{
			eFound = true;
			break;
		}
	}
	if(eFound && (digitCount > 2))
	{
		for( ; digitCount > 0 ; digitCount--)
		{
			buf[len - digitCount] = buf[len - digitCount + 1];
		
		}
	}
	return coco;				
}
