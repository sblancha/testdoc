//	Implementation of VacIconVG_DP_VGM class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVG_DP_VGM.h"

#include "EqpVG_DP.h"

#include <QPainter>


void VacIconVG_DP_VGM::drawSpecific(QPainter &painter)
{
	painter.setPen(Qt::black);
	painter.drawLine(1, 10, 20, 10);
}
