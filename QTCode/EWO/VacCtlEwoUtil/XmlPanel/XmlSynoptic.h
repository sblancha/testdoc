#ifndef XMLSYNOPTIC_H
#define	XMLSYNOPTIC_H

//	Definition of PVSS panel read from XML

#include "ImageItem.h"
#include "DataPool.h"
#include "VacIcon.h"

#include <QXmlStreamReader>

#include <QWidget>

class VACCTLEWOUTIL_EXPORT XmlSynoptic : public QObject
{
	Q_OBJECT

public:
	XmlSynoptic(QWidget *parent);
	virtual ~XmlSynoptic();

	static XmlSynoptic *createFromFile(QWidget *pParent, const QString &fileName,
		DataEnum::DataMode newMode, QString &errMsg);

	virtual void draw(QPainter &painter);
	virtual bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	// Access
	inline int getWidth(void) const { return width; }
	inline int getHeight(void) const { return height; }

protected:
	// Parent - needed mainly for icons
	QWidget	*pParent;

	// Sizes
	int		width;
	int		height;

	// Mode for active elements
	DataEnum::DataMode mode;

	// List of 'passive' panel items
	QList<ImageItem *>	items;

	// List of 'active' panel items
	QList<VacIcon *>	icons;

	void assignInterlockSources(void);
	bool parse(QXmlStreamReader &reader, QString &errMsg);
	void parsePanel(QXmlStreamReader &reader);
	void parsePanelProperties(QXmlStreamReader &reader);
	void parseShapes(QXmlStreamReader &reader);
	void parseShape(QXmlStreamReader &reader);
	void parseLine(QXmlStreamReader &reader);
	void parseRectangle(QXmlStreamReader &reader);
	void parsePolygon(QXmlStreamReader &reader);
	void parsePrimitiveText(QXmlStreamReader &reader);
	void parseFrame(QXmlStreamReader &reader);
	void parseEllipse(QXmlStreamReader &reader);
	void parseGeneric(QXmlStreamReader &reader);

	void parseUnknown(QXmlStreamReader &reader);

private slots:
	void mobileStateChange(Eqp *pSrc, DataEnum::DataMode eqpMode);

};

#endif	// XMLSYNOPTIC_H
