//	Implementation of DateEditEwo class
/////////////////////////////////////////////////////////////////////////////////

#include "DateEditEwo.h"

EWO_PLUGIN(DateEditEwo)

DateEditEwo::DateEditEwo(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new DateEditWidget(parent);
	QFont fnt("Helvetica", 10);
	baseWidget->setFont(fnt);
	connect(baseWidget, SIGNAL(dateTimeChanged(const QDateTime &)),
		this, SLOT(dateTimeChange(const QDateTime &)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList DateEditEwo::signalList(void) const
{
	QStringList list;
	list.append("ValueChanged(time value)");
	return list;
}

/*
**	FUNCTION
**		Slot receiving valveChanged signal of QDateTimeEdit, emit
**		signal 'valueChanged' to PVSS
**
**	ARGUMENTS
**		value	- New date + time value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DateEditEwo::dateTimeChange(const QDateTime &value)
{
	emit signal("ValueChanged", QVariant(value));
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList DateEditEwo::methodList(void) const
{
	QStringList list;
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant DateEditEwo::invokeMethod(const QString & /* name */,
	QList<QVariant> & /* values */, QString &error)
{
	error = "No methods in this EWO";
	return QVariant();
}
