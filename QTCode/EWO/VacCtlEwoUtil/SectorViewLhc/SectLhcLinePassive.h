#ifndef	SECTLHCLINEPASSIVE_H
#define	SECTLHCLINEPASSIVE_H

// Passive equipment line in LHC sector view

#include "SynLhcLinePassive.h"

class SectLhcLinePassive : public SynLhcLinePassive  
{
public:
	SectLhcLinePassive(QWidget *parent, int type, float start, float end) :
		SynLhcLinePassive(parent, type, start, end)
	{
		eqpAboveLine = false;
	}
	virtual ~SectLhcLinePassive() {}

protected:
	virtual bool isMyEqp(Eqp *pEqp, int &partIdx);
};

#endif	// SECTLHCLINEPASSIVE_H
