#ifndef	SECTVIEW_H
#define	SECTVIEW_H

// Sector view image for machines like PS, SPS...

#include "SectLine.h"

#include "SynView.h"

class SectView : public SynView
{
	Q_OBJECT

public:
	SectView(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	~SectView();

	static SectView *create(QWidget *parent, Sector *pFirstSector, Sector *pLastSector,
		DataEnum::DataMode mode);

	// Implementation of SynView's method
//	virtual void buildSectorView(void);

protected:
	// First sector to be shown
	Sector	*pStartSector;

	// Last sector to be shown
	Sector	*pEndSector;

	// Override SynView's method: sector view is built in another way
	virtual void buildView(VacEqpTypeMask &mask);
};

#endif	// SECTVIEW_H
