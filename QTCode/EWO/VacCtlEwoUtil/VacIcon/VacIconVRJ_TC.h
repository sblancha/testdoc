#ifndef	VACICONVRJ_TC_H
#define	VACICONVRJ_TC_H

//	Icon for thermocouples patch panel

#include "VacIcon.h"

class EqpVRJ_TC;

class VACCTLEWOUTIL_EXPORT VacIconVRJ_TC : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVRJ_TC(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVRJ_TC();

	// Override
	virtual inline bool isConnectedToVacuum(void) { return false; }

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVRJ_TC *pEqp;

	// Number of slave devices
	int			nSlaves;

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACICONVRJ_TC_H
