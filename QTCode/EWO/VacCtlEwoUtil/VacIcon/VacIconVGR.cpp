//	Implementation of VacIconVGR class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGR.h"

#include <qpainter.h>

// We only need to implement drawSpecific() method

// Draw 'Pirani' symbol in the center of icon
void VacIconVGR::drawSpecific(QPainter &painter)
{
	painter.setPen(Qt::black);
	/*
	QPointArray points(10);
	points.setPoint(0, 3, 9);
	points.setPoint(1, 5, 9);
	points.setPoint(2, 5, 13);
	points.setPoint(3, 8, 13);
	points.setPoint(4, 8, 7);
	points.setPoint(5, 12, 7);
	points.setPoint(6, 12, 13);
	points.setPoint(7, 15, 13);
	points.setPoint(8, 15, 9);
	points.setPoint(9, 17, 9);
	*/
	QPolygon points(8);
	points.setPoint(0, 4, 9);
	points.setPoint(1, 6, 12);
	points.setPoint(2, 7, 12);
	points.setPoint(3, 9, 7);
	points.setPoint(4, 10, 7);
	points.setPoint(5, 13, 12);
	points.setPoint(6, 14, 12);
	points.setPoint(7, 16, 9);
	painter.drawPolyline(points);
}
