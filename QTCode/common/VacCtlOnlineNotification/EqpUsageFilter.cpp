//	Implementation of EqpUsageFilter class
////////////////////////////////////////////////////////////////////////////////

#include "EqpUsageFilter.h"

#include "Eqp.h"
#include "DataPool.h"
#include "FunctionalType.h"

EqpUsageFilter::EqpUsageFilter()
{
	funcType = 0;
	matchingListReady = false;
}

EqpUsageFilter::~EqpUsageFilter()
{
}

void EqpUsageFilter::addFilterItem(int funcTypeId, QByteArray attrName, bool reverse)
{
	// Check before adding to avoid internal contradictions
	for(int idx = 0 ; idx < filterList.count() ; idx++)
	{
		EqpUsageFilterItem *pItem = filterList.at(idx);
		if((pItem->getTypeId() == funcTypeId) && (pItem->getAttrName() == attrName))
		{
			return;
		}
	}
	filterList.append(new EqpUsageFilterItem(funcTypeId, attrName, reverse));
}

bool EqpUsageFilter::checkEqp(Eqp *pEqp)
{
	if(!matchingListReady)
	{
		buildMatchingList();
	}
	return matchingList.contains(pEqp);
}
	
void EqpUsageFilter::buildMatchingList(void)
{
	//qDebug("EqpUsageFilter::buildMatchingList: start at %s funcType %d\n", QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz").toLatin1().constData(), funcType);
	bool hasNormal = false, hasReverse = false;
	for(int idx = 0 ; idx < filterList.count() ; idx++)
	{
		EqpUsageFilterItem *pItem = filterList.at(idx);
		if(pItem->isReverse())
		{
			hasReverse = true;
		}
		else
		{
			hasNormal = true;
		}
	}

	addAllMatching(hasNormal);
	// qDebug("EqpUsageFilter::buildMatchingList: addAllMatching() done at %s, %d devices found\n", QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz").toLatin1().constData(), matchingList.count());
	removeReverseMatching(hasReverse);
	matchingListReady = true;
	//qDebug("EqpUsageFilter::buildMatchingList: FINISH at %s, %d devices found\n", QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz").toLatin1().constData(), matchingList.count());
}

void EqpUsageFilter::addAllMatching(bool hasCriteria)
{
	// There is at least one case where selecting one funcTypeId implicitely assumes selecting
	// also another one. So, let's use list
	QList<int> funcTypeList;
	buildMainFuncTypeList(funcTypeList);

	DataPool &pool = DataPool::getInstance();
	const QHash<QByteArray, Eqp *> &allEqp = pool.getEqpDict();
	if(hasCriteria)
	{
		foreach(Eqp *pEqp, allEqp)
		{
			for(int idx = 0 ; idx < filterList.count() ; idx++)
			{
				EqpUsageFilterItem *pItem = filterList.at(idx);
				if(pItem->isReverse())
				{
					continue;
				}
				if(pEqp->getFunctionalType() != pItem->getTypeId())
				{
					continue;
				}
				const QString attrValue = pEqp->getAttrValue(pItem->getAttrName().constData());
				Eqp *pOtherEqp = pool.findEqpByDpName(attrValue.toLatin1().constData());
				if(pOtherEqp)
				{
					//qDebug("EqpUsageFilter::addAllMatching(): found other Eqp %s for %s attr %s\n", pOtherEqp->getDpName(), pEqp->getDpName(), pItem->getAttrName().constData());
					if(funcTypeList.contains(pOtherEqp->getFunctionalType()))
					{
						if(!matchingList.contains(pOtherEqp))
						{
							matchingList.append(pOtherEqp);
						}
					}
				}

			}
		}
	}
	else	// Just all devices of required functional type
	{
		foreach(Eqp *pEqp, allEqp)
		{
			if(funcTypeList.contains(pEqp->getFunctionalType()))
			{
				matchingList.append(pEqp);
			}
		}
	}
}

void EqpUsageFilter::removeReverseMatching(bool hasCriteria)
{
	if(!hasCriteria)
	{
		return;
	}
	if(matchingList.count() == 0)
	{
		return;
	}
	// There is at least one case where selecting one funcTypeId implicitely assumes selecting
	// also another one. So, let's use list
	QList<int> funcTypeList;
	buildMainFuncTypeList(funcTypeList);

	DataPool &pool = DataPool::getInstance();
	const QHash<QByteArray, Eqp *> &allEqp = pool.getEqpDict();
	foreach(Eqp *pEqp, allEqp)
	{
		for(int idx = 0 ; idx < filterList.count() ; idx++)
		{
			EqpUsageFilterItem *pItem = filterList.at(idx);
			if(!pItem->isReverse())
			{
				continue;
			}
			if(pEqp->getFunctionalType() != pItem->getTypeId())
			{
				continue;
			}
			const QString attrValue = pEqp->getAttrValue(pItem->getAttrName().constData());
			Eqp *pOtherEqp = pool.findEqpByDpName(attrValue.toLatin1().constData());
			if(pOtherEqp)
			{
				if(funcTypeList.contains(pOtherEqp->getFunctionalType()))
				{
					matchingList.removeOne(pOtherEqp);
				}
			}
		}
	}
}

void EqpUsageFilter::buildMainFuncTypeList(QList<int> &funcTypeList)
{
	funcTypeList.append(funcType);
	switch(funcType)
	{
	case FunctionalType::VPI:
		funcTypeList.append(FunctionalType::VRPI);
		break;
	default:
		break;
	}
}
