// library provides a hiding/showing of specified DPs
// author : Merker S.

// Name of dp where hidden Dps is kept
const string  HIDDEN_DP_NAME = "_VacEqpHide";

void LhcVacBuildHiddenDpList(dyn_string &hiddenDpList, dyn_string &exceptionInfo)
{
	dyn_errClass 	err;

  if( !dpExists( HIDDEN_DP_NAME ) )
	{
    dpCreate( HIDDEN_DP_NAME, HIDDEN_DP_NAME);
 	  err = getLastError( );
	  if( dynlen( err ) > 0 )
	  {
		  fwException_raise( exceptionInfo, 
			  "ERROR", "LhcVacBuildHiddenDpList(): Failed to create DP <" + HIDDEN_DP_NAME + ">: " + err, "" );
		  return;
	 }
  }
  dpGet(HIDDEN_DP_NAME + ".list", hiddenDpList);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    dyn_string exceptionInfo;
    fwException_raise(exceptionInfo, "ERROR", "LhcVacBuildHiddenDpList(): dpGet(hiddenDpName) failed:" + err, "");
    return;
  }
}
void LhcVacHideDps(dyn_string &exceptionInfo)
{
	dyn_errClass 	err;
  dyn_string    hiddenDpList;
  if( !dpExists( HIDDEN_DP_NAME ) )
	{
    // nothing to do    
    return;
  }
  dpGet(HIDDEN_DP_NAME + ".list", hiddenDpList);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    dyn_string exceptionInfo;
    fwException_raise(exceptionInfo, "ERROR", "LhcVacHideDps(): dpGet(hiddenDpName) failed:" + err, "");
    return;
  }
  int hiddenDpListLen = dynlen(hiddenDpList);
  for(int n = 1; n<=hiddenDpListLen; n++)
  {
    LhcVacEqpSetActive(hiddenDpList[n], false);
  }    
}
