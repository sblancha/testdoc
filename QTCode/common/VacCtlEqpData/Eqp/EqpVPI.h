#ifndef	EQPVPI_H
#define	EQPVPI_H

//	VPI - ion pupm

#include "Eqp.h"
#include "EqpVRPI.h"	// strongly depends on VRPI

class VACCTLEQPDATA_EXPORT EqpVPI : public Eqp
{
	Q_OBJECT

public:
	EqpVPI(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVPI(const EqpVPI &src) : Eqp(src) {}
#endif
	~EqpVPI();

	virtual void postProcess(void);

	virtual void getMaster(QString &masterDp, int &masterChannel);
	virtual inline Eqp *getMaster(void) { return (Eqp *)pMaster; }
	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void setSelected(bool selected, bool isFromPvss);
	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::Pressure; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode mode);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
#endif

	virtual int getErrorCode(DataEnum::DataMode mode);
	virtual void getErrorString(DataEnum::DataMode mode, QString &result);

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
	virtual void getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end);
	virtual float getGrowFactorForDevList(void);
	virtual int getSpikeFilterForDevList(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);


	// Access for essential values
	bool isPlcAlarm(DataEnum::DataMode mode) const;
	inline float getPR(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? prReplay : prOnline; }

	// In fact, these values come from master
	virtual CtlStatus getCtlStatus(DataEnum::DataMode mode) const;
	unsigned getRR1(DataEnum::DataMode mode) const;
	bool isBlockedOff(DataEnum::DataMode mode) const;

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);
	virtual void selectChange(Eqp * /* pScr */) {}

	// VPI shall react to selection (via it's master - VRPI). Normally connection
	// to signal of VRPI is done in connectDpe() method (of VRPI in this case).
	// However, disconnect causes also disconnect from selection signal.
	// Even if second ('permanent') connection to selection signal is done -
	// disconnect() disconnects both (including 'permanent' connection).
	// This was discovered in pressure history where PR is connected, but
	// RR1 is not.
	// In order to solve this problem two major changes were introduced:
	// 1) VPI has it's own NEW slot for processing selection signals from VRPI
	//		(see below)
	// 2) New virtual method postProcess() has been added to Eqp class, the
	//		purpose is to do what particular device needs AFTER all devices
	//		have been read from file. This allows to open VPI history without
	//		first opening VPI 'details', including VRPI state

	virtual void masterSelectChange(Eqp *pSrc);

protected:
	// Pointer to VRPI controlling this VPI
	EqpVRPI	*pMaster;

	// Number of connect requests to this Eqp - in order to decide if
	// we need to connect to/disconnect from master (VRPI)
	int		nOnlineConnects;
	int		nReplayConnects;
	int		nPollingConnects;
	
	// Last known value of PR DPE for online mode
	float	prOnline;

	// Last known value of PR DPE for replay mode
	float	prReplay;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool	plcAlarmOnline;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool	plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainValueString(unsigned rr1, float pr, bool plcAlarm, QString &string);
	virtual void getMainStateStringForMenu(unsigned rr1, float pr, bool plcAlarm, QString &string);
	virtual void getMainValueStringForMenu(unsigned rr1, float pr, bool plcAlarm, QString &string);
#endif

};

#endif	// EQPVPI_H
