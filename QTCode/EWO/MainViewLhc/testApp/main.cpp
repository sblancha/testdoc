#include "LhcMainViewer.h"

#include <qapplication.h>
#include <qobjectlist.h>

static void printChildren(QObject *pParent, int offset);
static char *offsetString(int offset);

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	LhcMainViewer viewer(NULL, "LHC main viewer");
	app.setMainWidget(&viewer);
	printChildren(&viewer, 0);
	viewer.show();
	return app.exec();
}

static void printChildren(QObject *pParent, int offset)
{
	char	*shown = "N/A";
	if(pParent->inherits("QWidget"))
	{
		if(((QWidget *)pParent)->isShown())
		{
			shown = "YES";
		}
		else
		{
			shown = "NO";
		}
	}
	printf("%s%s: %s %s\n", offsetString(offset), pParent->className(), pParent->name(),
		shown);
	if(!pParent->children())
	{
		return;
	}
	QObjectListIterator iter(*pParent->children());
	QObject *pObj;
	while((pObj = iter.current()))
	{
		++iter;
		printChildren(pObj, offset + 1);
	}
}


static char *offsetString(int offset)
{
	static char buf[512];
	for(int i = 0 ; i < offset ; i++)
	{
		buf[i] = ' ';
	}
	buf[offset] = '\0';
	return buf;
}

