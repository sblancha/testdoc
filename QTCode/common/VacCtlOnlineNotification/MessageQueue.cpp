//	Implementation of MessageQueue class
/////////////////////////////////////////////////////////////////////////////////

#include "MessageQueue.h"
#include "MessageQueueItem.h"

MessageQueue	MessageQueue::instance;

MessageQueue &MessageQueue::getInstance(void)
{
	return instance;
}

/*
**	FUNCTION
**		Add message to list of pending messages
**
**	ARGUMENTS
**		configName	- Messages configuration name
**		messages	- Message text
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageQueue::addMessage(const QString &configName, const QString &message)
{
	instance.addMessage(new MessageQueueItem(configName, message));
}

/*
**	FUNCTION
**		Add message to list of pending messages
**
**	ARGUMENTS
**		configName	- Messages configuration name
**		messages	- Message text
**		msgBody		- E-mail body
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageQueue::addMessage(const QString &configName, const QString &message, const QString &msgBody)
{
	instance.addMessage(new MessageQueueItem(configName, message, msgBody));
}

//////////////////////////////////////////////////////////////////////////////////
////////////////////// Construction/destruction

MessageQueue::MessageQueue()
{
	pList = new QList<MessageQueueItem *>();
}

MessageQueue::~MessageQueue()
{
	while(!pList->isEmpty())
	{
		delete pList->takeFirst();
	}
	delete pList;
}


/*
**	FUNCTION
**		Add message to list of pending messages
**
**	ARGUMENTS
**		pItem	- Pointer to queue item to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageQueue::addMessage(MessageQueueItem *pItem)
{
	mutex.lock();
	pList->append(pItem);
	mutex.unlock();
}

/*
**	FUNCTION
**		Get next message from queue
**
**	ARGUMENTS
**		configName	- variable where configuration name will be written
**		message		- Variable where message subjectwill be written
**		msgBody		- Variable where message body will be written
**
**	RETURNS
**		true	- if queue is not empty;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool MessageQueue::getNext(QString &configName, QString &message, QString &msgBody)
{
	mutex.lock();
	bool result;
	if(pList->isEmpty())
	{
		result = false;
	}
	else
	{
		result = true;
		MessageQueueItem *pItem = pList->takeFirst();
		configName = pItem->getConfigName();
		message = pItem->getText();
		msgBody = pItem->getBody();
		delete pItem;
	}
	mutex.unlock();
	return result;
}

