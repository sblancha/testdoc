//	Implementation of VacIconVGTR class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGTR.h"

#include "EqpVGTR.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0
#define COLOR_WARNING			255,255,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_ON			(0x00020000)
#define	RR1_OFF			(0x00010000)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVGTR::VacIconVGTR(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 21);
}

VacIconVGTR::~VacIconVGTR()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVGTR::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(21, 21, 10, 24, 1);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVGTR::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVGTR"), "VacIconVGTR::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVGTR *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVGTR::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVGTR::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("PR");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVGTR::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVGTR::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVGTR::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0;
	int pr = 0;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		pr = pEqp->getPR(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Gauge image is NOT rotated (does not depend on direction), but
	// connecting pipe IS rotated
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		painter.fillRect(10 - pipeWidth / 2, 18, pipeWidth ? pipeWidth : 1, 4, pipeColor);
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.fillRect(10 - pipeWidth / 2, 0, pipeWidth ? pipeWidth : 1, 4, pipeColor);
		break;
	case VacIconContainer::Left:	// 90 degrees clockwise
		painter.fillRect(0, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		break;
	case VacIconContainer::Right:	// 90 degrees conuterclockwise
		painter.fillRect(18, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		break;
	}
	// Pipe is ready - draw gauge image
	drawImage(painter, ctlStatus, rr1, pr, plcAlarm);
}

void VacIconVGTR::drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, int pr, bool plcAlarm)
{
	setPainterForMainImage(painter, ctlStatus, rr1, pr, plcAlarm);

	// Filled circle
	QPen pen = painter.pen();
	painter.setPen(Qt::NoPen);
	painter.drawPie(1, 1, 19, 19, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawArc(1, 1, 19, 19, 0, 360 * 16);

	// Arrow - as a symbol of VGTR
	QPen arrowPen(Qt::black, 2);
	painter.setPen(arrowPen);
	painter.drawLine(5, 15, 15, 5);
	painter.drawLine(15, 6, 8, 6);
	painter.drawLine(15, 6, 15, 13);
}

void VacIconVGTR::setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, int /* pr */, bool plcAlarm)
{
	QColor fillColor(COLOR_UNDEFINED);
	QColor lineColor = Qt::black;
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		fillColor.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		fillColor.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				// Line color - depends on errors/warnings
				if(rr1 & RR1_ERROR)
				{
					lineColor.setRgb(COLOR_ERROR);
				}
				else if(rr1 & RR1_WARNING)
				{
					lineColor.setRgb(COLOR_WARNING);
				}
				// Fill color - depends on ON/OFF state (plus some warnings)
				if(rr1 & RR1_ON)
				{
					fillColor.setRgb(COLOR_ON);
					if(rr1 & RR1_OFF)
					{
						fillColor.setRgb(COLOR_ERROR);
					}
				}
				else if(rr1 & RR1_OFF)
				{
					fillColor.setRgb(COLOR_OFF);
				}
			}
		}
		break;
	}
	painter.setBrush(fillColor);
	QPen pen(lineColor, lineColor == Qt::black ? 1 : 2);
	painter.setPen(pen);
}

