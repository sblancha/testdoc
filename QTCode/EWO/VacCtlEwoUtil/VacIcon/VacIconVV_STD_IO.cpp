//	Implementation of VacIconVV_STD_IO class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVV_STD_IO.h"
#include "FlashingRect.h"

#include "EqpVV_STD.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255

#define	RR1_VALID			(0x40000000)
#define	RR1_OPEN_ENABLE		(0x08000000)	// Used by VV_STD_6E01 only
#define RR1_MANUAL			(0x04000000)
#define RR1_INTERLOCK		(0x00200000)
#define	RR1_ON_INTERLOCK	(0x00100000)
#define	RR1_AUTO_ON_ORDER	(0x00080000)
#define	RR1_MAN_ON_ORDER	(0x00040000)
#define RR1_FORCED			(0x00010000)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVV_STD_IO::VacIconVV_STD_IO(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	pFlash = NULL;
	pBlockedRect = NULL;
	setFixedSize(21, 27);
}

VacIconVV_STD_IO::~VacIconVV_STD_IO()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVV_STD_IO::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Left:
	case VacIconContainer::Right:
		return VacIconGeometry(27, 21, 10, 15, 0);
	default:
		break;
	}
	return VacIconGeometry(21, 27, 10, 10, 0);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVV_STD_IO::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(21);
		size.setHeight(27);
		break;
	default:
		size.setWidth(27);
		size.setHeight(21);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVV_STD_IO::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVV_STD"), "VacIconVV_STD_IO::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVV_STD *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVV_STD_IO::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVV_STD_IO::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	if(pEqp->isHaveBlockedOff())
	{
		dpes.append("BlockedOFF");
	}
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVV_STD_IO::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVV_STD_IO::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVV_STD_IO::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Draw BlockeOFF state (if any)
	bool blockedOff = false;
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			break;
		default:
			blockedOff = pEqp->isBlockedOff(mode);
			break;
		}
	}
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
	case VacIconContainer::Down:
		drawHorizontal(painter);
		displayBlockedOFF(blockedOff, false);
		break;
	case VacIconContainer::Left:	// 90 degrees
	case VacIconContainer::Right:	// 90 degrees clockwise
		drawVertical(painter);
		displayBlockedOFF(blockedOff, true);
		break;
	}
}


void VacIconVV_STD_IO::drawVertical(QPainter &painter)
{
	// Draw vacuum pipe - two short lines
	painter.fillRect(9, 0, 3, 5, pipeColor);
	painter.fillRect(9, 19, 3, 5, pipeColor);
	
	// Draw valve image
	setPainterForMainImage(painter);

	// 1) Rectangle connecting big rectangle on top with valve image
	QPolygon points(4);
	points.setPoint(0, 10, 8);
	points.setPoint(1, 16, 8);
	points.setPoint(2, 16, 12);
	points.setPoint(3, 10, 12);
	painter.drawPolygon(points);

	// 2) big rectangle on top
	QBrush brushSave(painter.brush());
	QPen penSave(painter.pen());
	setPainterForControl(painter);
	points.setPoint(0, 16, 6);
	points.setPoint(1, 19, 6);
	points.setPoint(2, 19, 14);
	points.setPoint(3, 16, 14);
	painter.drawPolygon(points);
	// Specific for VV_STD_6E01: flashing rectangle if warning == 100
	displayFlash(points);

	// 3) Valve image
	painter.setBrush(brushSave);
	painter.setPen(penSave);
	points.setPoint(0, 1, 1);
	points.setPoint(1, 19, 19);
	points.setPoint(2, 1, 19);
	points.setPoint(3, 19, 1);
	painter.drawPolygon(points);

	drawSymbols(painter, true);
}

void VacIconVV_STD_IO::drawHorizontal(QPainter &painter)
{
	// Draw vacuum pipe - two short lines
	painter.fillRect(0, 14, 5, 3, pipeColor);
	painter.fillRect(18, 14, 5, 3, pipeColor);
	
	// Draw valve image
	setPainterForMainImage(painter);

	// 1) Rectangle connecting big rectangle on top with valve image
	QPolygon points(4);
	points.setPoint(0, 8, 9);
	points.setPoint(1, 12, 9);
	points.setPoint(2, 12, 15);
	points.setPoint(3, 8, 15);
	painter.drawPolygon(points);

	// 2) big rectangle on top
	QBrush brushSave(painter.brush());
	QPen penSave(painter.pen());
	setPainterForControl(painter);
	points.setPoint(0, 6, 6);
	points.setPoint(1, 14, 6);
	points.setPoint(2, 14, 9);
	points.setPoint(3, 6, 9);
	painter.drawPolygon(points);
	// Specific for VV_STD_6E01: flashing rectangle if warning == 100
	displayFlash(points);

	// 3) Valve image
	painter.setBrush(brushSave);
	painter.setPen(penSave);
	points.setPoint(0, 1, 6);
	points.setPoint(1, 19, 25);
	points.setPoint(2, 19, 6);
	points.setPoint(3, 1, 25);
	painter.drawPolygon(points);

	drawSymbols(painter, false);
}

void VacIconVV_STD_IO::displayFlash(const QPolygon &points)
{
	bool displayFlash = false;
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			break;
		default:
			if(pEqp->getCtrlType() == 10)	// VV_STD_6E01
			{
				if(!pEqp->isPlcAlarm(mode))
				{
					unsigned rr1 = pEqp->getRR1(mode);
					if((rr1 & RR1_VALID) && (rr1 & RR1_OPEN_ENABLE))
					{
						if(((rr1 & 0xFF00) >> 8) == 100)
						{
							displayFlash = true;
						}
					}
				}
			}
			break;
		}
	}
	if(displayFlash)
	{
		if(!pFlash)
		{
			pFlash = new FlashingRect(this);
			pFlash->setGeometry(points.boundingRect());
			pFlash->show();
		}
	}
	else
	{
		if(pFlash)
		{
			delete pFlash;
			pFlash = NULL;
		}
	}
}

void VacIconVV_STD_IO::displayBlockedOFF(bool blockedOff, bool isVertical)
{
	if(blockedOff)
	{
		if(!pBlockedRect)
		{
			pBlockedRect = new FlashingRect(this);
			pBlockedRect->setColor(Qt::black);
			if(isVertical)
			{
				pBlockedRect->setGeometry(8, 3, 5, 15);
			}
			else
			{
				pBlockedRect->setGeometry(3, 14, 15, 5);
			}
			pBlockedRect->show();
		}
	}
	else
	{
		if(pBlockedRect)
		{
			delete pBlockedRect;
			pBlockedRect = NULL;
		}
	}
}

void VacIconVV_STD_IO::drawSymbols(QPainter &painter, bool vertical)
{
	if (pEqp) {
		// if family/type/subtype = 1/7/3 VV_STD_RO draw a L for Local
		if ( (pEqp->getCtrlFamily() == 1) &&
			 (pEqp->getCtrlType() == 7) && 
			 (pEqp->getCtrlSubType() == 3) ) {
			drawL(painter, vertical);
		}
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			return;
		default:
			break;
		}
		if(pEqp->isPlcAlarm(mode))
		{
			return;	// Invalid
		}
		unsigned rr1 = pEqp->getRR1(mode);
		if(!(rr1 & RR1_VALID))
		{
			return;	// Invalid
		}

		if(rr1 & RR1_FORCED)
		{
			drawForced(painter, vertical);
		}
		else if(rr1 & RR1_MANUAL)
		{
			drawManual(painter, vertical);
		}

		if(rr1 & RR1_INTERLOCK)
		{
			drawFullInterlock(painter, vertical);
		}
		else if(rr1 & RR1_ON_INTERLOCK)
		{
			drawStartInterlock(painter, vertical);
		}

		// If control type VV_STD_RO: do not draw ON/OFF label
		if (pEqp->getCtrlSubType() != 3) {
			// If not VV_STD_RO: draw ON/OFF label
			if (rr1 & (RR1_MANUAL | RR1_FORCED))
			{
				if (rr1 & RR1_MAN_ON_ORDER)
				{
					drawCmdOn(painter, vertical);
				}
				else
				{
					drawCmdOff(painter, vertical);
				}
			}
			else
			{
				if (rr1 & RR1_AUTO_ON_ORDER)
				{
					drawCmdOn(painter, vertical);
				}
				else
				{
					drawCmdOff(painter, vertical);
				}
			}
		}
	}
	else {
		drawManual(painter, vertical);
		drawFullInterlock(painter, vertical);
		drawL(painter, vertical);
	}
}

void VacIconVV_STD_IO::drawForced(QPainter &painter, bool vertical)
{
	painter.setPen(Qt::red);
	if(vertical)
	{
		painter.drawLine(21, 1, 21, 5);
		painter.drawLine(22, 1, 23, 1);
		painter.drawLine(21, 3, 22, 3);
	}
	else
	{
		painter.drawLine(1, 1, 1, 5);
		painter.drawLine(2, 1, 3, 1);
		painter.drawLine(1, 3, 2, 3);
	}
}

void VacIconVV_STD_IO::drawManual(QPainter &painter, bool vertical)
{
	painter.setPen(Qt::black);
	if(vertical)
	{
		painter.drawLine(21, 1, 21, 5);
		painter.drawLine(25, 1, 25, 5);
		painter.drawLine(23, 3, 23, 4);
		painter.drawLine(22, 2, 22, 2);
		painter.drawLine(24, 2, 24, 2);
	}
	else
	{
		painter.drawLine(1, 1, 1, 5);
		painter.drawLine(5, 1, 5, 5);
		painter.drawLine(3, 3, 3, 4);
		painter.drawLine(2, 2, 2, 2);
		painter.drawLine(4, 2, 4, 2);
	}
}

void VacIconVV_STD_IO::drawFullInterlock(QPainter &painter, bool vertical)
{
	painter.setPen(Qt::red);
	if(vertical)
	{
		// F
		painter.drawLine(1, 7, 5, 7);
		painter.drawLine(5, 7, 5, 9);
		painter.drawLine(3, 7, 3, 8);
		// I
		painter.drawLine(1, 11, 1, 13);
		painter.drawLine(2, 12, 4, 12);
		painter.drawLine(5, 11, 5, 13);
	}
	else
	{
		// F
		painter.drawLine(7, 21, 7, 25);
		painter.drawLine(8, 21, 9, 21);
		painter.drawLine(7, 23, 8, 23);
		// I
		painter.drawLine(11, 21, 13, 21);
		painter.drawLine(12, 22, 12, 24);
		painter.drawLine(11, 25, 13, 25);
	}
}

void VacIconVV_STD_IO::drawStartInterlock(QPainter &painter, bool vertical)
{
	painter.setPen(Qt::darkYellow);
	if(vertical)
	{
		// S
		painter.drawLine(1, 7, 1, 8);
		painter.drawLine(2, 9, 3, 9);
		painter.drawLine(3, 8, 3, 9);
		painter.drawLine(4, 7, 5, 7);
		painter.drawLine(5, 8, 5, 9);
		// I
		painter.drawLine(1, 11, 1, 13);
		painter.drawLine(2, 12, 4, 12);
		painter.drawLine(5, 11, 5, 13);
	}
	else
	{
		// S
		painter.drawLine(7, 21, 9, 21);
		painter.drawLine(7, 21, 7, 22);
		painter.drawLine(8, 23, 9, 23);
		painter.drawLine(9, 24, 9, 25);
		painter.drawLine(7, 25, 8, 25);
		// I
		painter.drawLine(11, 21, 13, 21);
		painter.drawLine(12, 22, 12, 24);
		painter.drawLine(11, 25, 13, 25);
	}
}

void VacIconVV_STD_IO::drawCmdOff(QPainter &painter, bool vertical)
{
	painter.setPen(Qt::red);
	if(vertical)
	{
		// O
		painter.drawLine(22, 8, 24, 8);
		painter.drawLine(21, 9, 21, 10);
		painter.drawLine(25, 9, 25, 10);
		painter.drawLine(22, 11, 24, 11);
		// F
		painter.drawLine(21, 13, 25, 13);
		painter.drawLine(23, 13, 23, 14);
		painter.drawLine(25, 14, 25, 15);
		// F
		painter.drawLine(21, 17, 25, 17);
		painter.drawLine(23, 17, 23, 18);
		painter.drawLine(25, 18, 25, 19);
	}
	else
	{
		// O
		painter.drawLine(9, 2, 9, 4);
		painter.drawLine(10, 1, 11, 1);
		painter.drawLine(10, 5, 11, 5);
		painter.drawLine(12, 2, 12, 4);
		// F
		painter.drawLine(14, 1, 14, 5);
		painter.drawLine(15, 1, 16, 1);
		painter.drawLine(14, 3, 15, 3);
		// F
		painter.drawLine(18, 1, 18, 5);
		painter.drawLine(19, 1, 20, 1);
		painter.drawLine(18, 3, 19, 3);
	}
}

void VacIconVV_STD_IO::drawCmdOn(QPainter &painter, bool vertical)
{
	painter.setPen(Qt::darkGreen);
	if(vertical)
	{
		// O
		painter.drawLine(22, 11, 24, 11);
		painter.drawLine(21, 12, 21, 13);
		painter.drawLine(25, 12, 25, 13);
		painter.drawLine(22, 14, 24, 14);
		// N
		painter.drawLine(21, 16, 25, 16);
		painter.drawLine(21, 19, 25, 19);
		painter.drawLine(22, 19, 25, 16);
	}
	else
	{
		// O
		painter.drawLine(11, 2, 11, 4);
		painter.drawLine(12, 1, 13, 1);
		painter.drawLine(12, 5, 13, 5);
		painter.drawLine(14, 2, 14, 4);
		// N
		painter.drawLine(16, 1, 16, 5);
		painter.drawLine(19, 1, 19, 5);
		painter.drawLine(16, 2, 19, 5);
	}
}
/**
* @brief For read only valve draw a black L at the place of the Cmd order letters
*/
void VacIconVV_STD_IO::drawL(QPainter &painter, bool vertical)
{
	painter.save();
	QPen lPen;
	lPen.setColor(Qt::black);
	lPen.setWidth(2);
	painter.setPen(lPen);
	if (vertical) {
		// L
		painter.drawLine(21, 15, 25, 15);
		painter.drawLine(21, 15, 21, 18);
	}
	else {
		// L
		painter.drawLine(16, 1, 16, 5);
		painter.drawLine(16, 5, 19, 5);
	}
	painter.restore();
}


void VacIconVV_STD_IO::setPainterForControl(QPainter &painter)
{
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		int order;
		QColor borderColor;
		pEqp->getInterlockColor(mode, color, borderColor, order, NULL);
	}
	painter.setBrush(color);
	painter.setPen(Qt::black);
}

void VacIconVV_STD_IO::setPainterForMainImage(QPainter &painter)
{
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		pEqp->getMainColor(color, mode);
	}
	QBrush brush(color);
	painter.setBrush(brush);
	painter.setPen(Qt::black);
}
