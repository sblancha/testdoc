#ifndef	EQPVV_PUL_H
#define	EQPVV_PUL_H

//	Standard valve for LHC BGI

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVV_PUL: public Eqp
{
	Q_OBJECT

public:
	EqpVV_PUL(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVV_PUL(const EqpVV_PUL &src) : Eqp(src) {}
#endif
	~EqpVV_PUL();

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
	// Special color of interlock for device, returns true if there is special color
	virtual bool getInterlockColor(DataEnum::DataMode mode, QColor &color, QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList);
	virtual MainEqpStatus getMainStatus(void);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline unsigned getRR2(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr2Replay : rr2Online; }
	virtual int getErrorCode(DataEnum::DataMode mode);
	inline bool isBlockedOff(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? blockedOffReplay : blockedOffOnline;
	}

	inline bool isHaveBlockedOff(void) { return haveBlockedOff; }
	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online, rr2Online;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR2 DPE for replay mode
	unsigned	rr1Replay, rr2Replay;

	// Last knwon value of BlockedOFF DPE for online mode
	bool		blockedOffOnline;

	// Last known value of BlockedOFF DPE for replay mode
	bool		blockedOffReplay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

	// Flag indicating if this device has BlockedOFF DPE
	bool		haveBlockedOff;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, bool plcAlarm, QString &string);
#endif
	
};

#endif	// EQPVV_PUL_H
