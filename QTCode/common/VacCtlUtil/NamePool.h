// NamePool.h: interface for the NamePool class.
//
//////////////////////////////////////////////////////////////////////

#ifndef NAMEPOOL_H
#define NAMEPOOL_H

/*
** Class to keep a lot of names in a few relatively large
** string buffers. The main goal of introducing this class
** is to avoid many allocations of small memory blocks for
** storing many individual names. Instead, this class allocates
** memory in relatively large blocks, then new names are added
** to these blocks as new names appear - and callers use pointers
** to memory in NamePool's large blocks.
**
** This approach can be used for large amount of named items which
** appear and disappear all together, for example, equipment read
** from file.
**
** Names stored in pool are subdivided to two major categories:
**  1) 'Single-use' names like equipment name. When request to add
**     such name to pool comes - it is highly probable that such name
**     neither appear in the past nor will appear in the future, so
**     we shall just add new name without any checks.
**  2) 'Multi-use' names like equipment type name. On the contrary
**     to 'single-use' names, it is highly probable that the same
**     name will be added to pool many times, so it could be a good
**     idea to check if such name is already in pool, if yes - do
**     not add new name but return pointer to already stored.
**
** Of course, this is the caller who shall know in advance if name
** he wants to add to pool is 'single-use' or 'multi-use' name.
*/

#include "VacCtlUtilExport.h"

class VACCTLUTIL_EXPORT NamePool  
{
public:
	NamePool();
	virtual ~NamePool();

	char *addString(const char *pString);
	char *addMultiUseString(const char *pString);
	void clear(void);

private:
	// Array of buffers used for 'single-use' strings
	char	**suBuffers;

	// Number of items in array suBuffers
	int		nSuBuffers;

	// Next free byte index in current 'single-use' buffer
	int		suCharIdx;

	// Array of buffers used for 'multy-use' strings
	char	**muBuffers;

	// Number of items in array muBuffers
	int		nMuBuffers;

	// Next free byte index in current 'multi-use' buffer
	int		muCharIdx;
};

#endif // !defined(NAMEPOOL_H)
