/**
* @brief Class implementation for Vacuum Pump - Group Unified (VP_GU)
* @see Eqp
*/

#include "DataPool.h"
#include "FunctionalTypeVPG.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#include "ResourcePool.h"

#include "MobileType.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include "EqpVP_GU.h"
#include <QColor>


EqpVP_GU::EqpVP_GU(const Eqp &source) : Eqp(source) {
	rr1Online = rr1Replay = rr2Online = rr2Replay = stateOnline = stateReplay = 0;
	prOnline = prReplay = 1E4;
	comAlarmOnline = comAlarmReplay = false;
	alertActive = alertNotAck = false;
	functionalType = FunctionalType::VPG;
	warningStOnline = errorStOnline = stepStOnline = vptRotationStOnline = vptCurrentStOnline = vppCurrentStOnline = 0;
	degCStOnline = 999.9;
	//Check the mobile type, if it is mobile deactivate
	int mType = this->getMobileType();
	if (mType != MobileType::Fixed){
		activeOnline = activeReplay = false;
	}
	hasVvr2 = false;
	hasIndependantVgLeft = false;
	hasIndependantVgRight = false;
	if ((ctrlFamily == 106) && (ctrlType == 11)) {
		if (ctrlSubType == 2) { //VPGRB
			hasIndependantVgLeft = true;
		}
		if (   (ctrlSubType == 3)  //VPGRC
			|| (ctrlSubType ==10)  //VPGFG
			|| (ctrlSubType == 11) //VPGFH
									) { 
			hasIndependantVgLeft = true;
			hasIndependantVgRight = true;
		}
		if (   (ctrlSubType == 4) //VPGFA
			|| (ctrlSubType == 5) //VPGFB 
			|| (ctrlSubType == 8) //VPGFE
			|| (ctrlSubType ==13) //VPGFJ
								 ) { 
			hasIndependantVgLeft = true;
			hasIndependantVgRight = true;
			hasVvr2 = true;
		}

	}
}
EqpVP_GU::~EqpVP_GU(){
}
/**
@brief SIGNAL FUNCTION Notify that this device's activity has been changed for given mode.
@param[in]   mode		aqn mode (online, replay...).
@param[in]   active		mode is active.
*/
void EqpVP_GU::setActive(DataEnum::DataMode mode, bool active){
	switch (mode) {
	case DataEnum::Replay:
		if (active == activeReplay)
			return;
		else
			activeReplay = active;
		break;
	default:
		if (active == activeOnline)
			return;
		
		else
			activeOnline = active;
		break;
	}
	if (mobileType != MobileType::Fixed) 
		emit mobileConnectionChanged(this, mode);
}
/**
@brief FUNCTION get the master name, for instance the communication object V_TCP.
@param[out]   master		Master eqp instance name.
*/
void EqpVP_GU::getMaster(QString &master) {
	master = getAttrValue("MasterName");
}
/**
@brief FUNCTION OVERRIDE Return list of all DPEs, related to state of device for state history.
@param[out]   stateDpes		list of name of dpes used to calculate state.
*/
void EqpVP_GU::getStateDpes(QStringList &stateDpes) {
	stateDpes.clear();
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "State");
	addDpeNameToList(stateDpes, "RR2");
	addDpeNameToList(stateDpes, "errorSt");
	addDpeNameToList(stateDpes, "warningSt");
}
/**
@brief FUNCTION OVERRIDE Return list of all DPEs, related to pressure value of device for trend history.
@param[out]   stateDpes		list of name of dpes used to calculate state.
*/
void EqpVP_GU::getValueDpes(QStringList &valueDpes) {
	valueDpes.clear();
	addDpeNameToList(valueDpes, "PR");
}
/**
@brief FUNCTION Connect DPEs of interest for widget animation.
@param[in]   *pDst		Class to be notified about change in this device.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVP_GU::connect(InterfaceEqp *pDst, DataEnum::DataMode mode) {
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "RR2", mode);
	connectDpe(pDst, "State", mode);
	connectDpe(pDst, "PR", mode);
	connectDpe(pDst, "AL1:_alert_hdl.._ack_possible", mode);
}
/**
@brief FUNCTION Disconnect DPEs of interest for animation.
@param[in]   *pDst		Class to be notified about change in this device.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVP_GU::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode) {
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "RR2", mode);
	disconnectDpe(pDst, "State", mode);
	disconnectDpe(pDst, "PR", mode);
	disconnectDpe(pDst, "AL1:_alert_hdl.._ack_possible", mode);
}
/**
@brief FUNCTION Process new online value arrived from front end to update widget animation.
@param[in]   dpe		name of dpe updated.
@param[in]	 value		new value of the dpe.
*/
void EqpVP_GU::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp) {
	if(!strcmp(dpe, "RR1"))	{
		if(value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
				return;
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2")) {
		if(value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if(rr2Online == newValue) {
				return;
			}
			rr2Online = newValue;
		}
	}
	else if (!strcmp(dpe, "State"))	{
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (stateOnline == newValue)
				return;
			stateOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "PR")) {
		if (value.canConvert(QVariant::Double)) {
			double newValue = value.toDouble();
			if (prOnline == newValue)
				return;
			prOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "AL1")) {
		if (value.canConvert(QVariant::Bool)) {
			bool newValue = value.toBool();
			if (alertNotAck == newValue)
				return;
			alertNotAck = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}
#ifndef PVSS_SERVER_VERSION
/**
@brief FUNCTION Process new replay value arrived from front end to update widget animation.
@param[in]   dpe		name of dpe updated.
@param[in]	 value		new value of the dpe.
*/
void EqpVP_GU::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp) {
	if (!strcmp(dpe, "RR1")) {
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (rr1Replay == newValue)
				return;
			rr1Replay = newValue;
		}
	}
	else if (!strcmp(dpe, "RR2")) {
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (rr2Replay == newValue)
				return;
			rr2Replay = newValue;
		}
	}
	else if (!strcmp(dpe, "State"))	{
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (stateReplay == newValue) 
				return;
			stateReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "PR")) {
		if (value.canConvert(QVariant::Double)) {
			double newValue = value.toDouble();
			if (prReplay == newValue)
				return;
			prReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif
/**
@brief SLOT FUNCTION activated by communication object when communication state change
@param[in]   pCom		Pointer of emmiter (communication object).
@param[in]   dpeName	DPE change source (see enum above)
@param[in]	 value		New value of the dpe.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVP_GU::dpeChange(Eqp *pCom, const char *dpeName,
						 DataEnum::Source /* source */, const QVariant &value, 
						 DataEnum::DataMode mode, const QDateTime &timeStamp) {
	bool alarm = !pCom->isAlive(mode);
	if(mode == DataEnum::Replay) {
		if(alarm == comAlarmReplay)	
			return;
		comAlarmReplay = alarm;
	}
	else {
		if(alarm == comAlarmOnline)
			return;
		comAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}
/**
@brief Return main value for gauge - pressure
@param[in] mode	data acquisition mode
*/
#ifndef PVSS_SERVER_VERSION
float EqpVP_GU::getMainValue(DataEnum::DataMode mode) {
	return mode == DataEnum::Replay ? prReplay : prOnline;
}
#endif
/**
@brief Main value always valid for this control type
@param[in] mode	data acquisition mode
*/
#ifndef PVSS_SERVER_VERSION
bool EqpVP_GU::isMainValueValid(DataEnum::DataMode /* mode */) {
	return true;
}
#endif
/**
@brief Check if main value for gauge(pressure) can be negative
*/
#ifndef PVSS_SERVER_VERSION
bool EqpVP_GU::isMainValueNegative(void) const {
	return false;	// NEVER
}
#endif
/**
@brief FUNCTION Return main color of device
@param[out]   color		main color value
@param[in]	  mode		Aqn mode (online, replay...)
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getMainColor(QColor &color, DataEnum::DataMode mode) {
	if(mode == DataEnum::Replay)
		getMainColor(rr1Replay, stateReplay, comAlarmReplay, color);
	else
		getMainColor(rr1Online, stateOnline, comAlarmOnline, color);
}
#endif
/**
@brief PROTECTED FUNCTION Calculate main color of device
@details Calculate color according to state meaning:
! Pumping group not connected to SCADA, only state defined by SCADA script
		VP_GU.State_0 = Not Connected - COLOR_INVALID
! Initial state after for example control unit start
		VP_GU.State_1 = Initial - COLOR_OFF
! Pumping group is in "Stopped" mode after a normal venting
		VP_GU.State_2 = Stopped(vent) - COLOR_OFF
! Pumping group starting and not ready for safe pumping (Primary pump is starting, Turbo moleculare pump is accelerating...)
		VP_GU.State_3 = Starting(VVR cl) - COLOR_STARTING
! Pumping group ready for safety pumping (e.g.: Turbo moleculare pump nominal and safe pumping delay is reached)
		VP_GM.State_4 = Nominal(VVR cl) - COLOR_ON
! Pumping group is pumping the vessel it is connected to
		VP_GU.State_5 = Pumping(VVR op) - COLOR_ON
! Turbo moleculare pump is recovering (lost is nominal speed) and valve connected to vessel (VVR) still opened
		VP_GU.State_6 = Recovering(VVR op) - COLOR_WARNING
! Turbo moleculare pump is recovering (lost is nominal speed) and valve connected to vessel (VVR) is closed
		VP_GU.State_7 = Recovering(VVR cl) - COLOR_WARNING
! Pumping group starting and valve connected to vessel (VVR) has been forced open
		VP_GU.State_8 = Starting(VVR op) - COLOR_WARNING
! Pumping group has been stopped without venting
		VP_GU.State_9 = Stopped(no vent) - COLOR_WARNING
! Pumping group in service mode ongoing (Leak detection in progress, bakeout, primary pump test mode in progress...)
		VP_GU.State_10 = Services - COLOR_WARNING
! Pumping group stopped and vented after an error occured
		VP_GU.State_11 = Error Stopped(vent) - COLOR_ERROR
! Pumping group stopped but not vented after an error occured
		VP_GU.State_12 = Error Stopped(no vent) - COLOR_ERROR
! Pumping group in unknown state after an error occured
		VP_GU.State_13 = Error unknown state - COLOR_ERROR
! Pumping group Communication error
		VP_GU.State_14 = Error Communication - COLOR_INVALID

@param[in]   state			State value
@param[in]	 comAlarm	Alarm value of the Tcp communication object
@param[out]	 color			Calculated return value 	
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getMainColor(unsigned rr1, unsigned state, bool comAlarm, QColor &color) {
	// First check Communication is Ok and eqp valid
	if (comAlarm || (state == 0) || !(rr1 & RR1_VALID_VPGU)) {
		color.setRgb(COLOR_GP_INVALID);
		return;
	}
	// Then check Error
	if (rr1 & RR1_ERROR_VPGU) {
		color.setRgb(COLOR_GP_ERROR);
		return;
	}
	// Check Warning
	if (rr1 & RR1_WARNING_VPGU) {
		color.setRgb(COLOR_GP_WARNING);
		return;
	}
	// Finally use State dpe 
	if(state < 3) {
		color.setRgb(COLOR_GP_OFF);
		return;
	}
	if (state < 4) {
		color.setRgb(COLOR_GP_WARNING);
		return;
	}
	if(state < 6) {
		color.setRgb(COLOR_GP_ON);
		return;
	}
	if (state < 11) {
		color.setRgb(COLOR_GP_WARNING);
		return;
	}
	if (state < 14) {
		color.setRgb(COLOR_GP_ERROR);
		return;
	}
	else {
		color.setRgb(COLOR_GP_INVALID);
	}
}
#endif

/**
@brief FUNCTION Return main state string of device
@param[out]   string	main state string value
@param[in]	  mode		Aqn mode (online, replay...)
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, stateReplay, prReplay, comAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, stateOnline, prOnline, comAlarmOnline, string);
	}
}
#endif

/**
@brief PROTECTED FUNCTION Calculate main state string of device
@details String value get from ressource file 
@param[in]   rr1			read register 1 value
@param[in]   state			State value
@param[in]	 comAlarm	Alarm value of the Tcp communication object
@param[out]	 string			Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getMainStateString(unsigned rr1, unsigned state, double pr, bool comAlarm, QString &string, bool isStateHistory /*= false*/)
{
	if(comAlarm)
	{
		string = "Communication Error";
		return;
	}
	if (!(rr1 & RR1_VALID_VPGU))
	{
		string = "Invalid";
		return;
	}

	char resourceName[256];

#ifdef Q_OS_WIN
	sprintf_s(resourceName, sizeof(resourceName) / sizeof(resourceName[0]), "VP_GU.State_%d", state);
#else
	sprintf(resourceName, "VP_GU.State_%d", state);
#endif

	if (ResourcePool::getInstance().getStringValue(resourceName, string) == ResourcePool::OK) {
		if (!isStateHistory) {
			string = string + ": " + QString::number(pr, 'E', 1);
			string = string + " mbar";
			return;
		}
		else { // for state history
			return;
		}
	}
	string = "Undefined state";
}
#endif

/**
@brief FUNCTION Calculate main state string for contextual menu of device
@details Same as getMainStateString(), ":"+PR already in mainState
@param[in]	  mode		Aqn mode (online, replay...)
@param[out]	 string		Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainStateString(valueString, mode);
	string += valueString;
}
#endif

/**
@brief FUNCTION Calculate tool tip
@param[in]	  mode		Aqn mode (online, replay...)
@param[out]	 string		Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getToolTipString(rr1Replay, stateReplay, prReplay, comAlarmReplay, string);
	}
	else
	{
		getToolTipString(rr1Online, stateOnline , prOnline, comAlarmOnline, string);
	}
}
#endif

/**
@brief PROTECTED FUNCTION Calculate tool tip
@param[in]   rr1			read register 1 value
@param[in]   state			State value
@param[in]	 comAlarm	Alarm value of the Tcp communication object
@param[out]	 string			Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getToolTipString(unsigned rr1, unsigned state, double pr, bool comAlarm, QString &string)
{
	QString stateString;
	getMainStateString(rr1, state, pr, comAlarm, stateString);
	string = getVisibleName();
	string += ": ";
	string += stateString;
}
#endif

/**
@brief FUNCTION Calculate main state string and color for moment in the past, data are taken from state archive data pool
@param[in]   pPool			Pointer to state history data pool
@param[in]	 ts				Moment when state is needed
@param[out]	 stateString	Variable where resulting state string will be written
@param[out]	 color			Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &stateString, QColor &color)
{
	stateString = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("RR1");
	dpeNames.append("State");
	QList<QVariant *> values;
	if(!pPool->getDpValues(dpName, ts, dpeNames, values))
		return;
	if (values.isEmpty()) 
		return;
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	if (values.isEmpty())
		return;
	pVariant = values.takeFirst();
	unsigned state = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, state, 0.0, false, stateString, true); //pr = 0.0 because not used for stateHistory string
	getMainColor(rr1, state, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/**
@brief FUNCTION Decide which DPEs shall be read to check if this device matches current device list criteria or not
@param[out]	 dpes		Variable where all required DPEs will be added
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "State");
}
#endif

/**
@brief FUNCTION Return code to check if device in recent state matches device list criteria or not
@details Return 0 = Device does not match criteria; 1 = Device matches criteria; 2 = More information is needed (history)-NOT for valves
*/
#ifndef PVSS_SERVER_VERSION
int EqpVP_GU::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	DataEnum::DataMode mode = criteria.getMode();
	bool active = mode == DataEnum::Replay ? activeReplay : activeOnline;
	if(!active)	{
		if (criteria.isNotActive()) {
			return 1;
		}
		else {
			return 0;
		}
	}
	if(criteria.isEmpty()) {
		return 1;	// No criteria - all devices are needed
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if (!(rr1 & RR1_VALID_VPGU))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	unsigned state = DevListCriteria::EqpStateOther;
	if ((rr1 & (RR1_ON_VPGU | RR1_OFF_VPGU)) == RR1_ON_VPGU)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if ((rr1 & (RR1_ON_VPGU | RR1_OFF_VPGU)) == RR1_OFF_VPGU)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if (rr1 & RR1_ERROR_VPGU)
		{
			return 1;
		}
	}
	if(criteria.isWarnings())
	{
		if (rr1 & (RR1_ERROR_VPGU | RR1_WARNING_VPGU))
		{
			return 1;
		}
	}
	return 0;
}
#endif

/**
@brief FUNCTION Calculate color for given part of device
@param[in]   subEqp			Sub equipment type (VVR1, VVR2, VPT, VPP, VVI, VVD, VVP, VVT)
@param[in]	 mode			Aqn mode (online, replay...)
@param[out]	 color			Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getSubEqpColor(QString subEqp, QColor &color, DataEnum::DataMode mode)
{
	//unsigned state = mode == DataEnum::Replay ? stateReplay : stateOnline;
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	unsigned rr2 = mode == DataEnum::Replay ? rr2Replay : rr2Online;
	unsigned state = mode == DataEnum::Replay ? stateReplay : stateOnline;
	bool useAlarm = mode == DataEnum::Replay ? comAlarmReplay : comAlarmOnline;
	
	unsigned rr1Open, rr1Closed, rr2Warning, rr2Error = 0;

	// First check Communication is Ok and eqp valid
	if (useAlarm || (state == 0) || !(rr1 & RR1_VALID_VPGU)) {
		color.setRgb(COLOR_GP_INVALID);
		return;
	}
		
	if (subEqp == "VVR1")
		getColorVVR1(rr1, rr2, color);
	else if (subEqp == "VVR2")
		getColorVVR2(rr1, rr2, color);
	else if (subEqp == "VPT")
		getColorVPT(rr1, rr2, color);
	else if (subEqp == "VPP")
		getColorVPP(rr1, rr2, color);

	else if (subEqp == "VVI") {
		rr1Open = RR1_VVI_OPEN_VPGU;
		rr1Closed = RR1_VVI_CLOSED_VPGU;
		rr2Warning = RR2_VVI_WARNING_VPGU;
		rr2Error = RR2_VVI_ERROR_VPGU;
		getColorVV(rr1, rr2, rr1Open, rr1Closed, rr2Warning, rr2Error, color);
	}
	else if (subEqp == "VVD"){
		rr1Open = RR1_VVD_OPEN_VPGU;
		rr1Closed = RR1_VVD_CLOSED_VPGU;
		rr2Warning = RR2_VVD_WARNING_VPGU;
		rr2Error = RR2_VVD_ERROR_VPGU;
		getColorVV_NC(rr1, rr2, rr1Open, rr1Closed, rr2Warning, rr2Error, color);
	}
	else if (subEqp == "VVT"){
		rr1Open = RR1_VVT_OPEN_VPGU;
		rr1Closed = RR1_VVT_CLOSED_VPGU;
		rr2Warning = RR2_VVT_WARNING_VPGU;
		rr2Error = RR2_VVT_ERROR_VPGU;
		getColorVV_NC(rr1, rr2, rr1Open, rr1Closed, rr2Warning, rr2Error, color);
	}
	else if (subEqp == "VVP"){
		rr1Open = RR1_VVP_OPEN_VPGU;
		rr1Closed = RR1_VVP_CLOSED_VPGU;
		rr2Warning = RR2_VVP_WARNING_VPGU;
		rr2Error = RR2_VVP_ERROR_VPGU;
		getColorVV_NC(rr1, rr2, rr1Open, rr1Closed, rr2Warning, rr2Error, color);
	}
	else
		color.setRgb(COLOR_GP_NOEQP);
	
}
#endif

/**
@brief PRIVATE FUNCTION Calculate color for VVR1 part of device
@param[in]   rr1			Value of RR1 DPE
@param[in]   rr2			Value of RR2 DPE
@param[out]	 color			Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getColorVVR1(unsigned rr1, unsigned rr2, QColor &color)
{
	if (rr2 & RR2_VVR1_ERROR_VPGU)
	{
		color.setRgb(COLOR_GP_ERROR);
	}
	else if (rr2 & RR2_VVR1_WARNING_VPGU)
	{
		color.setRgb(COLOR_GP_WARNING);
	}
	else if (rr1 & RR1_VVR1_OPEN_VPGU)
	{
		if (rr1 & RR1_VVR1_CLOSED_VPGU)
		{
			color.setRgb(COLOR_GP_ERROR);
		}
		else
		{
			color.setRgb(COLOR_GP_OPEN);
		}
	}
	else if (rr1 & RR1_VVR1_CLOSED_VPGU)
	{
		color.setRgb(COLOR_GP_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_GP_INVALID);
	}
}
#endif

/**
@brief PRIVATE FUNCTION Calculate color for VVR2 part of device
@param[in]   rr1			Value of RR1 DPE
@param[in]   rr2			Value of RR2 DPE
@param[out]	 color			Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getColorVVR2(unsigned rr1, unsigned rr2, QColor &color)
{
	if (rr2 & RR2_VVR2_ERROR_VPGU)
	{
		color.setRgb(COLOR_GP_ERROR);
	}
	else if (rr2 & RR2_VVR2_WARNING_VPGU)
	{
		color.setRgb(COLOR_GP_WARNING);
	}
	else if (rr1 & RR1_VVR2_OPEN_VPGU)
	{
		if (rr1 & RR1_VVR2_CLOSED_VPGU)
		{
			color.setRgb(COLOR_GP_ERROR);
		}
		else
		{
			color.setRgb(COLOR_GP_OPEN);
		}
	}
	else if (rr1 & RR1_VVR2_CLOSED_VPGU)
	{
		color.setRgb(COLOR_GP_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_GP_INVALID);
	}
}
#endif

/**
@brief PRIVATE FUNCTION Calculate color for VPT part of device
@param[in]   rr1			Value of RR1 DPE
@param[in]   rr2			Value of RR2 DPE
@param[out]	 color			Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getColorVPT(unsigned rr1, unsigned rr2, QColor &color)
{
	if (rr2 & RR2_VPT_ERROR_VPGU)
	{
		color.setRgb(COLOR_GP_ERROR);
	}
	else if (rr2 & RR2_VPT_WARNING_VPGU)
	{
		color.setRgb(COLOR_GP_WARNING);
	}
	else if (rr1 & RR1_VPT_NOMINAL_VPGU)
	{
		if (!(rr1 & RR1_VPT_ON_VPGU))
		{
			color.setRgb(COLOR_GP_ERROR);
		}
		else
		{
			color.setRgb(COLOR_GP_ON);
		}
	}
	else
	{
		color.setRgb(COLOR_GP_OFF);
	}
}
#endif

/**
@brief PRIVATE FUNCTION Calculate color for VPP part of device
@param[in]   rr1			Value of RR1 DPE
@param[in]   rr2			Value of RR2 DPE
@param[out]	 color			Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getColorVPP(unsigned rr1, unsigned rr2, QColor &color)
{
	if (rr2 & RR2_VPP_ERROR_VPGU)
	{
		color.setRgb(COLOR_GP_ERROR);
	}
	else if (rr2 & RR2_VPP_WARNING_VPGU)
	{
		color.setRgb(COLOR_GP_WARNING);
	}
	else if (rr1 & RR1_VPP_ON_VPGU)
	{
		color.setRgb(COLOR_GP_ON);
	}
	else
	{
		color.setRgb(COLOR_GP_OFF);
	}
}
#endif

/**
@brief PRIVATE FUNCTION Calculate color for VV sub-device of VPG
@param[in]  rr1			Value of RR1 DPE
@param[in]  rr2			Value of RR2 DPE
@param[in]	rr1Open		Bit register value when valve open     
@param[in]	rr1Closed	Bit register value when valve closed
@param[in]	rr2Warning	Bit register value when valve warning
@param[in]	rr2Error	Bit register value when valve error
@param[out]	 color			Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getColorVV(unsigned rr1, unsigned rr2, 
						  unsigned rr1Open, unsigned rr1Closed, unsigned rr2Warning, unsigned rr2Error, 
						  QColor &color)
{
	if (rr2 & rr2Error)
	{
		color.setRgb(COLOR_GP_ERROR);
	}
	else if (rr2 & rr2Warning)
	{
		color.setRgb(COLOR_GP_WARNING);
	}
	else if (rr1 & rr1Open)
	{
		if (rr1 & rr1Closed)
		{
			color.setRgb(COLOR_GP_ERROR);
		}
		else
		{
			color.setRgb(COLOR_GP_OPEN);
		}
	}
	else if (rr1 & rr1Closed)
	{
		color.setRgb(COLOR_GP_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_GP_INVALID);
	}
}
#endif

/**
@brief PRIVATE FUNCTION Calculate color for VV normally closed sub-device of VPG
@param[in]  rr1			Value of RR1 DPE
@param[in]  rr2			Value of RR2 DPE
@param[in]	rr1Open		Bit register value when valve open
@param[in]	rr1Closed	Bit register value when valve closed
@param[in]	rr2Warning	Bit register value when valve warning
@param[in]	rr2Error	Bit register value when valve error
@param[out]	 color			Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_GU::getColorVV_NC(unsigned rr1, unsigned rr2,
	unsigned rr1Open, unsigned rr1Closed, unsigned rr2Warning, unsigned rr2Error,
	QColor &color)
{
	if (rr2 & rr2Error)
	{
		color.setRgb(COLOR_GP_ERROR);
	}
	else if (rr2 & rr2Warning)
	{
		color.setRgb(COLOR_GP_WARNING);
	}
	else if (rr1 & rr1Open)
	{
		if (rr1 & rr1Closed)
		{
			color.setRgb(COLOR_GP_ERROR);
		}
		else
		{
			color.setRgb(COLOR_GP_OPEN);
		}
	}
	else if (rr1 & rr1Closed)
	{
		color.setRgb(COLOR_GP_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_GP_INVALID);
	}
}
#endif

/**
@brief FUNCTION Check if device matches criteria for E-mail/SMS notification
@param[in, out]   pCriteria	Pointer to criteria to check
*/
void EqpVP_GU::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType) {
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None) {
		return;
	}

	unsigned maskedVvr1Rr1 = rr1Online & (RR1_VVR1_OPEN_VPGU | RR1_VVR1_CLOSED_VPGU);
	unsigned maskedVvr2Rr1 = rr1Online & (RR1_VVR2_OPEN_VPGU | RR1_VVR2_CLOSED_VPGU);
	
	switch(pCriteria->getType()) {
	case EqpMsgCriteria::Error:
		if ( (rr1Online & RR1_ERROR_VPGU) || (!(rr1Online & RR1_VALID_VPGU)) ){
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else {
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;


	case EqpMsgCriteria::Warning:
		if (rr1Online & RR1_WARNING_VPGU) {
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else {
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	
	case EqpMsgCriteria::MainValue:
		if (pCriteria->isReverse()) {
			if (prOnline > pCriteria->getUpperLimit()) {
				pCriteria->setMatch(false, true, prOnline);
				return;
			}
			else if (prOnline < pCriteria->getLowerLimit()) {
				pCriteria->setMatch(true, true, prOnline);
				return;
			}
			// Between lower and upper limit - no change
			return;
		}
		else {
			if (prOnline > pCriteria->getUpperLimit()) {
				pCriteria->setMatch(true, true, prOnline);
				return;
			}
			else if (prOnline < pCriteria->getLowerLimit()) {
				pCriteria->setMatch(false, true, prOnline);
				return;
			}
			// Between lower and upper limit - no change
			return;
		}
		break;

	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType()) {
		
		case FunctionalTypeVPG::Connected:
			if (activeOnline && (!comAlarmOnline)) {
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else {
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
	
		case FunctionalTypeVPG::Nominal:
			if( (stateOnline == STATE_NOMINAL_VVR_CL) 
					|| (stateOnline == STATE_NOMINAL_VVR_OP) ) 	{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else {
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;

		case FunctionalTypeVPG::Stopped:
			if ( (stateOnline != STATE_NOMINAL_VVR_CL) 
					&& (stateOnline != STATE_NOMINAL_VVR_OP)
					&& (stateOnline != STATE_RECOVERING_VVR_CL)	
					&& (stateOnline != STATE_RECOVERING_VVR_OP)	
					&& (stateOnline != STATE_SERVICES) ) {

				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else {
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;

		case FunctionalTypeVPG::Vvr1Open:
			if (maskedVvr1Rr1 == RR1_VVR1_OPEN_VPGU) {
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else {
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVPG::Vvr1Closed:
			if (maskedVvr1Rr1 == RR1_VVR1_CLOSED_VPGU) {
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else {
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;

		case FunctionalTypeVPG::Vvr2Open:
			if (maskedVvr2Rr1 == RR1_VVR2_OPEN_VPGU) {
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else {
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;

		case FunctionalTypeVPG::Vvr2Closed:
			if (maskedVvr2Rr1 == RR1_VVR2_CLOSED_VPGU) {
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else {
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	}
	pCriteria->setMatch(false, false, 0);
}
