#ifndef	DPEHISTORYDEPTHQUERY_H
#define	DPEHISTORYDEPTHQUERY_H

#include "VacCtlUtilExport.h"

// Class buffering DPE history depth requests to be sent to PVSS

#include <qobject.h>
#include <qlist.h>
#include <qmutex.h>
#include <qdatetime.h>

class DpeHistoryDepthQueryItem
{
public:
	DpeHistoryDepthQueryItem(const char *dpeIn, const QDateTime &startTime, int mobileType)
		: dpe(dpeIn), start(startTime)
	{
		this->mobileType = mobileType;
	}
	~DpeHistoryDepthQueryItem() {}

	// Access
	inline const QByteArray &getDpe(void) const { return dpe; }
	inline int getMobileType(void) const { return mobileType; }
	inline const QDateTime &getStart(void) const { return start; }

protected:
	// DPE name
	QByteArray	dpe;

	// Start time of history query time range
	QDateTime	start;

	// equipment's mobile type
	int			mobileType;
};

class VACCTLUTIL_EXPORT DpeHistoryDepthQuery : public QObject
{
	Q_OBJECT

public:
	static DpeHistoryDepthQuery &getInstance(void);

	DpeHistoryDepthQuery();
	DpeHistoryDepthQuery(const DpeHistoryDepthQuery &source);
	virtual ~DpeHistoryDepthQuery();
	DpeHistoryDepthQuery & operator = (const DpeHistoryDepthQuery &other);

	void add(const char *dpe, const QDateTime &start, int mobileType);
	void clear(void);
	static int getPendingCount(void);

	bool getNext(QByteArray &dpe, QDateTime &start, int &mobileType);

signals:
	void changed(void);

protected:

	// The only instance of this class
	static DpeHistoryDepthQuery instance;

	// List of pending requests
	QList<DpeHistoryDepthQueryItem *>	*pItems;

	// Mutex to protect list from parallel access
	QMutex	mutex;

	// Flag indicating if signal on content change has been sent
	bool	signalSent;
};

#endif	// DPEHISTORYDEPTHQUERY_H
