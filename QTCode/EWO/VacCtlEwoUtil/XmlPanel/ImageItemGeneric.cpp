//	Implementation of ImageItemGeneric class
/////////////////////////////////////////////////////////////////////////////////

#include "ImageItemGeneric.h"

#include "DataPool.h"
#include "Eqp.h"
#include "FunctionalType.h"
#include "VacMainView.h"
#include "VacEqpTypeMask.h"

#include <QLocale>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

//////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
//////////////////////////////////////////////////////////////////////////////////

ImageItemGeneric::ImageItemGeneric() : ImageItem()
{
	reset();
}

ImageItemGeneric::~ImageItemGeneric()
{
}

void ImageItemGeneric::reset(void)
{
	startX = startY = eqpType = 0;
	pEqpIcon = NULL;
	intlSourceForward = intlSourceBackward = false;
	direction = VacIconContainer::Up;
	beamDirection = VacIconContainer::LeftToRight;
	icon = false;
	dpName = "";
	beamPipeColor.setRgb(0, 0, 255);
}

VacIcon	*ImageItemGeneric::getIcon(QWidget *parent, DataEnum::DataMode mode)
{
#ifdef XML_PANEL_PARSE
	printf("ImageItemGeneric::getIcon(): icon %d eqpType %d dpName %s\n",
		(int)icon, eqpType, dpName.toLatin1().constData());
	fflush(stdout);
#endif
	if(!icon)
	{
		return NULL;
	}
	if(!visible)	// L.Kopylov 28.06.2012
	{
		return NULL;
	}
	VacIcon *pIcon = VacIcon::getIcon((VacIconContainer::EqpType) eqpType, parent);
	if(pIcon)
	{
		pIcon->move(startX, startY);
		pIcon->setDirection((VacIconContainer::Direction)direction);
		pIcon->setBeamDirection((VacIconContainer::BeamDirection)beamDirection);
		pIcon->setPipeColor(beamPipeColor);
		pIcon->setMode(mode);
		Eqp *pEqp = DataPool::getInstance().findEqpByDpName(dpName.toLatin1().constData());
		if(pEqp)
		{
			pIcon->setEqp(pEqp);
			pIcon->connect();
		}
		pIcon->show();
	}
	pEqpIcon = pIcon;
	return pIcon;
}

Eqp *ImageItemGeneric::getEqp(void)
{
	return pEqpIcon ? pEqpIcon->getEqp() : NULL;
}

bool ImageItemGeneric::checkForInterlockSource(void)
{
	if(!pEqpIcon)
	{
		return false;
	}
	Eqp *pEqp = pEqpIcon->getEqp();
	if(!pEqp)
	{
		return false;
	}
	if(pEqp->isIntlSourcePrev())
	{
		intlSourceForward = true;
	}
	if(pEqp->isIntlSourceNext())
	{
		intlSourceBackward = true;
	}
	return intlSourceForward || intlSourceBackward;
}

bool ImageItemGeneric::addAlarmSources(QList<ImageItem *> &allItems, QList<VacIcon *> &allIcons)
{
	if(!pEqpIcon)
	{
		return false;
	}
	if(!visible)
	{
		return false;
	}
	Eqp *pEqp = pEqpIcon->getEqp();
	if(!pEqp)
	{
		return false;
	}
	const QList<Eqp *> &targets = pEqp->getExtTargets();
	if(targets.isEmpty())
	{
		return false;
	}
	VacIconContainer::Direction direction = pEqpIcon->getSlaveDirection();
	int iconX = 0, iconY = 0;
	bool	vertCenter = false, horCenter = false;
	switch(direction)
	{
	case VacIconContainer::Up:
		iconX = pEqpIcon->geometry().center().x();
		iconY = pEqpIcon->geometry().top() - ((intlSourceBackward || intlSourceForward) ? 9 : 1);
		horCenter = true;
		break;
	case VacIconContainer::Down:
		iconX = pEqpIcon->geometry().center().x();
		iconY = pEqpIcon->geometry().bottom() + ((intlSourceBackward || intlSourceForward) ? 9 : 1);
		horCenter = true;
		break;
	case VacIconContainer::Left:
		iconX = pEqpIcon->geometry().left() - ((intlSourceBackward || intlSourceForward) ? 9 : 1);;
		iconY = pEqpIcon->geometry().center().y();
		vertCenter = true;
		break;
	case VacIconContainer::Right:
		iconX = pEqpIcon->geometry().right() + ((intlSourceBackward || intlSourceForward) ? 9 : 1);;
		iconY = pEqpIcon->geometry().center().y();
		vertCenter = true;
		break;
	}
	VacIcon *pIcon = NULL;
	ImageItemGeneric *pNewItem = NULL;
	VacEqpTypeMask	dummyMask;
	for(int targetIdx = 0 ; targetIdx < targets.count() ; targetIdx++)
	{
		pEqp = targets.at(targetIdx);
		pIcon = VacIcon::getIcon(pEqp, pEqpIcon->parentWidget());
		if(!pIcon)
		{
			continue;
		}
		pIcon->setDirection(direction);
		VacIconGeometry geometry = pIcon->getGeometry(dummyMask);
		// Finally - add new device to list
		pNewItem = new ImageItemGeneric();
		pNewItem->pEqpIcon = pIcon;
		pNewItem->icon = true;
		pNewItem->visible = true;
		if(!targetIdx)	// Add offset for 1st icon
		{
			switch(direction)
			{
			case VacIconContainer::Up:	// Add height for 1st icon above
				iconY -= geometry.getHeight();
				break;
			case VacIconContainer::Left:
				iconX -= geometry.getWidth();
				break;
			default:	// to make gcc happy
				break;
			}
		}
		QRect rect(iconX - (horCenter ? (geometry.getWidth() >> 1) : 0),
			iconY - (vertCenter ? (geometry.getHeight() >> 1) : 0),
			geometry.getWidth(), geometry.getHeight());
		pIcon->move(rect.x(), rect.y());
		pIcon->setMode(pEqpIcon->getMode());
		pIcon->connect();
		pNewItem->startX = rect.x();
		pNewItem->startY = rect.y();
		allItems.append(pNewItem);
		allIcons.append(pIcon);

		switch(direction)
		{
		case VacIconContainer::Up:
			iconY -= geometry.getHeight() + 1;
			break;
		case VacIconContainer::Down:
			iconY += geometry.getHeight() + 1;
			break;
		case VacIconContainer::Left:
			iconX -= geometry.getWidth() + 1;
			break;
		case VacIconContainer::Right:
			iconX += geometry.getWidth() + 1;
			break;
		}
	}
	return true;
}

void ImageItemGeneric::draw(QPainter &painter)
{
	if(!pEqpIcon)
	{
		return;
	}
	if(!pEqpIcon->isVisible())
	{
		return;
	}
	pEqpIcon->drawInterlockSrcArrow(painter, intlSourceBackward, intlSourceForward);
}

void ImageItemGeneric::parseProp(QXmlStreamReader &reader)
{
	QStringRef propName = reader.attributes().value("name");

	if(!propName.compare("Location", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		startX = (int)rint(c.toDouble(str.section(' ', 0, 0)));
		startY = (int)rint(c.toDouble(str.section(' ', 1, 1)));
	}
	else if(!propName.compare("ObjectType", Qt::CaseInsensitive))
	{
		icon = !reader.readElementText().compare("VacIconEwo.ewo", Qt::CaseInsensitive);
	}
	else if(!propName.compare("dpName", Qt::CaseInsensitive))
	{
		dpName = reader.readElementText();
	}
	else if(!propName.compare("direction", Qt::CaseInsensitive))
	{
		direction = reader.readElementText().toInt();
	}
	else if(!propName.compare("beamDirection", Qt::CaseInsensitive))
	{
		beamDirection = reader.readElementText().toInt();
	}
	else if(!propName.compare("eqpType", Qt::CaseInsensitive))
	{
		eqpType = reader.readElementText().toInt();
	}
	else if(!propName.compare("pipeColor", Qt::CaseInsensitive))
	{
		int r, g, b;
		QString text = reader.readElementText();
		if(sscanf(text.toLatin1().constData(), "{%d,%d,%d}", &r, &g, &b) == 3)
		{
			beamPipeColor.setRgb(r, g, b);
		}
	}
	else
	{
		ImageItem::parseProp(reader);
		return;
	}
}

