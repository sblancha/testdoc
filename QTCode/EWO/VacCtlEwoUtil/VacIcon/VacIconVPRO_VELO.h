#ifndef	VACICONVPRO_VELO_H
#define	VACICONVPRO_VELO_H

//	Icon for VELO process

#include "VacIcon.h"

class EqpVPRO_VELO;

class VACCTLEWOUTIL_EXPORT VacIconVPRO_VELO : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPRO_VELO(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPRO_VELO();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVPRO_VELO *pEqp;

	// Maximum height of 4 labels
	int		maxLabelHeight;

	static const char *labels[];
	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACICONVPRO_VELO_H
