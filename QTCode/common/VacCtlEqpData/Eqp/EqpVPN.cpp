//	Implementation of EqpVPN class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVPN.h"

#include "DataPool.h"
#include "FunctionalType.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#include <QColor>
#include <QDateTime>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVPN::EqpVPN(const Eqp &source) : Eqp(source)
{
	functionalType = FunctionalType::VPN;
	pMaster = NULL;
	nOnlineConnects = nReplayConnects = nPollingConnects = 0;
}

EqpVPN::~EqpVPN()
{
}

/*
**	FUNCTION
**		After all devices have been read from file - find master and
**		make 'permanent' connection to 'selectChanged() signal of master
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPN::postProcess(void)
{
	const QString masterDp = getAttrValue("MasterName");
	if(!masterDp.isEmpty())
	{
		DataPool &pool = DataPool::getInstance();
		Eqp *pEqp = pool.findEqpByDpName(masterDp.toLatin1());
		if(pEqp)
		{
			Q_ASSERT_X(pEqp->inherits("EqpVP_NEG"), "EqpVPN::postProcess", pEqp->getDpName());
			pMaster = (EqpVP_NEG *)pEqp;
			pMaster->addSlave(this);

			// In order to stay PERMANENTLY connected to master's selection
			selected = pMaster->isSelected();
			QObject::connect(pMaster, SIGNAL(selectChanged(Eqp *)), this, SLOT(masterSelectChange(Eqp *)));
		}
	}
}

/*
**	FUNCTION
**		Find master DP and master channel for this pump.
**
**	ARGUMENTS
**		masterDP		- Variable where master DP name will be written
**		masterChannel	- Variable where channel number in master will
**							be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPN::getMaster(QString &masterDp, int &masterChannel)
{
	masterDp = getAttrValue("MasterName");
	if(masterDp.isEmpty())
	{
		masterChannel = 0;
		return;
	}
	masterChannel = getAttrValue("MasterChannel").toInt();
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPN::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	if(pMaster)
	{
		pMaster->getStateDpes(stateDpes);
	}
}

/*
**	FUNCTION
**		Return list of all DPEs, related to analog value of device. The list is used
**		to build value history
**
**	ARGUMENTS
**		valueDpes	- Variable where all value DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPN::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	if(pMaster)
	{
		pMaster->getValueDpes(valueDpes);
	}
}


/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Dataacquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPN::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	// If this is the very first connection for given mode - we also need to
	// connect to master(VP_NEG)
	bool needMasterConnect = true;
	switch(mode)
	{
	case DataEnum::Online:
		needMasterConnect = nOnlineConnects == 0;
		break;
	case DataEnum::Replay:
		needMasterConnect = nReplayConnects == 0;
		break;
	case DataEnum::Polling:
		needMasterConnect = nPollingConnects == 0;
		break;
	}
	if(needMasterConnect)	// The very first connection
	{
		if(pMaster)
		{
			pMaster->connect(this, mode);
			// Selecting VP_NEG must select this device
			selected = pMaster->isSelected();
		}
	}
	connectDpe(pDst, "EqpStatus", mode);
	switch(mode)
	{
	case DataEnum::Online:
		nOnlineConnects++;
		break;
	case DataEnum::Replay:
		nReplayConnects++;
		break;
	case DataEnum::Polling:
		nPollingConnects++;
		break;
	}
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPN::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "EqpStatus", mode);
	switch(mode)
	{
	case DataEnum::Online:
		if(nOnlineConnects > 0)
		{
			nOnlineConnects--;
		}
		break;
	case DataEnum::Replay:
		if(nReplayConnects > 0)
		{
			nReplayConnects--;
		}
		break;
	case DataEnum::Polling:
		if(nPollingConnects > 0)
		{
			nPollingConnects--;
		}
		break;
	}
	// If this was the very last connection - we don't need connection to master anymore
	bool needMasterDisconnect = true;
	switch(mode)
	{
	case DataEnum::Online:
		needMasterDisconnect = nOnlineConnects == 0;
		break;
	case DataEnum::Replay:
		needMasterDisconnect = nReplayConnects == 0;
		break;
	case DataEnum::Polling:
		needMasterDisconnect = nPollingConnects == 0;
		break;
	}
	if(needMasterDisconnect)
	{
		if(pMaster)	// If we did not find it before - does not matter
		{
			pMaster->disconnect(this, mode);
		}
	}
}

Eqp::CtlStatus EqpVPN::getCtlStatus(DataEnum::DataMode mode) const
{
	// NotConnected/NotControlled status of device itself OVERRIDES ctl status of master
	CtlStatus ctlStatus = Eqp::getCtlStatus(mode);
	switch(ctlStatus)
	{
	case Eqp::NotConnected:
	case Eqp::NotControl:
		return ctlStatus;
	default:	// To make Linux compiler happy
		break;
	}
	if(!pMaster)
	{
		return Eqp::NotControl;
	}
	return pMaster->getCtlStatus(mode);
}

/*
**	FUNCTION
**		Process selection request, pass request to master
**
**	ARGUMENTS
**		selected	- New selected state
**		isFromPvss	- true if selection request came from PVSS
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPN::setSelected(bool selected, bool isFromPvss)
{
	if(!isFromPvss)
	{
		Eqp::setSelected(selected, isFromPvss);
		return;
	}
	if(pMaster)
	{
		pMaster->setSelected(selected, isFromPvss);
	}
}

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We connected
**		to VRPI, which in turn could connect toPLC, so we need to check where
**		signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPN::dpeChange(Eqp * /* pSrc */, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	if(source == DataEnum::Plc)
	{
		emit dpeChanged(this, dpeName, source, value, mode, timeStamp);
	}
	else	// VP_NEG
	{
		emit dpeChanged(this, dpeName, DataEnum::Parent, value, mode, timeStamp);
	}
}

/*
**	FUNCTION
**		Calculate main color of device. Color is taken from master
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPN::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		//qDebug("isColorByCtlStatus() = true for %s\n", dpName);
		return;
	}
	if(!pMaster)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else
	{
		pMaster->getMainColor(color, mode);
	}
}
#endif

int EqpVPN::getErrorCode(DataEnum::DataMode mode)
{
	if(pMaster)
	{
		return pMaster->getErrorCode(mode);
	}
	return 0;	// ????????
}

void EqpVPN::getErrorString(DataEnum::DataMode mode, QString &result)
{
	if(pMaster)
	{
		pMaster->getErrorString(mode, result);
	}
	else
	{
		result = "No master";
	}
}

/*
**	FUNCTION
**		Return main value for pump - pressure
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVPN::getMainValue(DataEnum::DataMode mode)
{
	return pMaster ? pMaster->getMainValue(mode) : 0;
}
#endif

/*
**	FUNCTION
**		Check if main value for pump (pressure) is valid or not.
**		Latest known requirement: even invalid value shall be shown,
**		so the value is always considered to be valid
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
bool EqpVPN::isMainValueValid(DataEnum::DataMode mode)
{
	return pMaster ? pMaster->isMainValueValid(mode) : false;
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device. Just use main state string of VRPI
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPN::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(!pMaster)
	{
		string = getVisibleName();
		string += ": No master";
	}
	else
	{
		pMaster->getMainStateString(string, mode);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPN::getMainValueString(QString &string, DataEnum::DataMode mode)
{
	if(!pMaster)
	{
		string = getVisibleName();
		string += ": No master";
	}
	else
	{
		pMaster->getMainValueString(string, mode);
	}
}
#endif



/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPN::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	string = getVisibleName();
	string += ": ";
	QString state;
	if(isStateStringByCtlStatus(state, mode))
	{
		string += state;
		return;
	}
	if(!pMaster)
	{
		string += "No master";
	}
	else
	{
		pMaster->getMainStateString(state, mode);
		string += state;
	}
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPN::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	getMainStateStringForMenu(string, mode);
}
#endif

/*
**	FUNCTION
**		Slot activated by master when it's selection state was changed.
**		We update our selection state and emit signal
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPN::masterSelectChange(Eqp *pSrc)
{
	if(selected != pSrc->isSelected())
	{
		selected = ! selected;
		emit selectChanged(this);
	}
}





/*
**	FUNCTION
**		[VACCO-278]
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPN::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	if(!pMaster)
	{
		return;
	}
	pMaster->getDpesForDevList(dpes);
}
#endif


/*
**	FUNCTION
**		[VACCO-278]
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVPN::checkDevListCrit(void)
{
	if(!pMaster)
	{
		return 1;	// This is HUGE error - must be always shown
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	if(pMaster->checkDevListCrit() == 1)
	{
		return 1;
	}

	//DataEnum::DataMode mode = criteria.getMode();
	return 0;
}
#endif