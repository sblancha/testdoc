#ifndef	SYNLHCSECTORLABEL_H
#define	SYNLHCSECTORLABEL_H

// Class holding information on sector labels in LHC synoptic view

#include <stdio.h>
#include <QList>
#include <QLine>
#include <QObject>

class Sector;

class SynLhcSectorLabel : public QObject
{
	Q_OBJECT

public:
	SynLhcSectorLabel(Sector *pSector);
	~SynLhcSectorLabel();

	void clearGraph(void);
	int addToGraph(int x1, int y1, int x2, int y2);
	int addToGraph(const QPoint &p1, const QPoint &p2);
	bool isPointOnSector(int x, int y, int tolerance) const;
	bool isPointOnSector(const QPoint &p, int tolerance) const;
	
	// Access
	inline Sector *getSector(void) const { return pSector; }
	inline int getStart(void) const { return start; }
	inline void setStart(int coord) { start = coord; }
	inline int getEnd(void) const { return end; }
	inline void setEnd(int coord) { end = coord; }
	inline float getEndPos(void) const { return endPos; }
	inline void setEndPos(float pos) { endPos = pos; }
	inline bool isByEqp(void) const { return byEqp; }
	inline void setByEqp(bool flag) { byEqp = flag; }
	inline bool isDrawStart(void) const { return drawStart; }
	inline void setDrawStart(bool flag) { drawStart = flag; }
	inline bool isDrawEnd(void) const { return drawEnd; }
	inline void setDrawEnd(bool flag) { drawEnd = flag; }

	inline int getStartOrder(void) const { return startOrder; }
	inline void setStartOrder(int order) { startOrder = order; }
	inline int getEndOrder(void) const { return endOrder; }
	inline void setEndOrder(int order) { endOrder = order; }
	inline int getCorrDelta(void) const { return corrDelta; }
	inline void setCorrDelta(int delta) { corrDelta = delta; }
	void dump(FILE *pFile, int idx);

	inline const QList<QLine *> &getSectorGraph(void) const { return sectorGraph; } // [VACCO-929]
	inline void setSectorGraph(QList<QLine *> sectorRep) { sectorGraph = sectorRep; } // [VACCO-929]

protected:
	// Pointer to sector data
	Sector		*pSector;

	// Start of sector on unlimited drawing area
	int			start;

	// End of sector on unlimited drawing area
	int			end;

	// Physical position of sector end
	float		endPos;

	int		startOrder;	// Start processing order - for debugging only
	int		endOrder;	// End processing order - for debugging only
	int		corrDelta;	// Correction of screen position

	// true if coordinate is taken from device, false - from line part
	bool		byEqp;

	// true if start of part shall be drawn
	bool		drawStart;

	// true if end of part shall be drawn
	bool		drawEnd;

	// [VACCO-929] Lines that represent the Sector in a specific synoptic panel
	QList <QLine *>	sectorGraph;

};

#endif	// SYNLHCSECTORLABEL_H
