//	Implementation of ProfileAllLHCBeam class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileAllLHCBeam.h"
#include "ProfileAllLHCBeamWidget.h"

#include "VacMainView.h"
#include "ResourcePool.h"
#include "DataPool.h"

#include <qlayout.h>
#include <qdesktopwidget.h>
#include <qapplication.h>
#include <qlabel.h>
#include <qmenu.h>
#include <qcursor.h>
#include <qinputdialog.h>
#include <QMouseEvent>


ProfileAllLHCBeam	*ProfileAllLHCBeam::dialogs[2] = { NULL, NULL };

/*
**	FUNCTION
**		Check if instance of this dialog (which. in fact, consists of 2 dialogs) is open
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if dialog instance is open
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
bool ProfileAllLHCBeam::alreadyOpen(void)
{
	return dialogs[0] || dialogs[1];
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

ProfileAllLHCBeam::ProfileAllLHCBeam() :
	QDialog(NULL /* VacMainView::getInstance() */, Qt::Window)
{
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle("Pressure Profile - all of LHC by beam type");
	pWidget = new ProfileAllLHCBeamWidget(this);

	connect(pWidget, SIGNAL(beamLimitsChanged(int, int)),
		this, SLOT(beamLimitsChange(int, int)));
	connect(pWidget, SIGNAL(linLimitsChanged(int, int)),
		this, SLOT(linLimitsChange(int, int)));
	connect(pWidget, SIGNAL(labelPressed(QMouseEvent *)),
		this, SLOT(labelPress(QMouseEvent *)));

	// What is my number - 1st or 2nd? Decided based on free slot in array of dialogs
	if(dialogs[0])
	{
		if(dialogs[1])
		{
			deleteLater();
			return;
		}
		myIndex = 1;
	}
	else
	{
		myIndex = 0;
	}
	dialogs[myIndex] = this;
	pWidget->setLhcPartIndex(myIndex);

	// Set large initial size
	QDesktopWidget *pDesktop = QApplication::desktop();
	const QRect &screen = pDesktop->screenGeometry(pDesktop->screenNumber(this));
	resize(screen.width() - 50, screen.height() - 50);

	// Check if 2nd dialog shall be created
	int otherIndex = myIndex ? 0 : 1;
	if(dialogs[otherIndex])
	{
		return;
	}
	ProfileAllLHCBeam *otherDialog = new ProfileAllLHCBeam();
	otherDialog->show();
}

ProfileAllLHCBeam::~ProfileAllLHCBeam()
{
	dialogs[myIndex] = NULL;
	int otherIndex = myIndex ? 0 : 1;
	if(dialogs[otherIndex])
	{
		dialogs[otherIndex]->deleteLater();
	}
}

void ProfileAllLHCBeam::resizeEvent(QResizeEvent * /* e */)
{
	pWidget->resize(width(), height());
}

/*
**	FUNCTION
**		Slot activated when vertical limits for beam pressure have been changed
**		in one of graphs
**
**	ARGUMENTS
**		min	- New minimum value
**		max	- New maximum value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeam::beamLimitsChange(int min, int max)
{
	int otherIndex = myIndex ? 0 : 1;
	if(dialogs[otherIndex])
	{
		dialogs[otherIndex]->setBeamLimits(min, max);
	}
}

/*
**	FUNCTION
**		Slot activated when vertical limits for temperature have been changed
**		in one of graphs
**
**	ARGUMENTS
**		min	- New minimum value
**		max	- New maximum value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeam::linLimitsChange(int min, int max)
{
	int otherIndex = myIndex ? 0 : 1;
	if(dialogs[otherIndex])
	{
		dialogs[otherIndex]->setLinLimits(min, max);
	}
}

/*
**	FUNCTION
**		Analyze which equipment types are available in graphs of this dialog
**
**	ARGUMENTS
**		allMpMask	- Variable where resulting types will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeam::analyzeTypes(VacEqpTypeMask &allMpMask)
{
	if(pWidget)
	{
		pWidget->analyzeTypes(allMpMask);
	}
}

/*
**	FUNCTION
**		Slot activated when one of MP 'labels' is pressed in unedrlying widget
**
**	ARGUMENTS
**		e	- Pointer to mouse event that caused signal to emit
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeam::labelPress(QMouseEvent *e)
{
	if(e->button() != Qt::RightButton)
	{
		return;
	}

	// Find which equipment types are present in all profiles
	VacEqpTypeMask allMpMask;
	analyzeTypes(allMpMask);
	int otherIndex = myIndex ? 0 : 1;
	if(dialogs[otherIndex])
	{
		dialogs[otherIndex]->analyzeTypes(allMpMask);
	}

	// Start building menu. First part - equipment types
	QMenu *pMenu = new QMenu(this);
	pMenu->addAction("<b>Equipment</b>");
	const QList<FunctionalType *> &types = pWidget->getAllEqpMask().getList();
	for(int idx = 0 ; idx < types.count() ; idx++)
	{
		FunctionalType *pType = types.at(idx);
		if(allMpMask.contains(pType->getType()))
		{
			QAction *pAction = pMenu->addAction(pType->getDescription());
			pAction->setData(pType->getType());
			pAction->setCheckable(true);
			pAction->setChecked(pWidget->getEqpMask().contains(pType->getType()));
		}
	}

	// 2nd part - polling period change
	pMenu->addSeparator();
	pPeriodMenuAction = pMenu->addAction("Period...");

	connect(pMenu, SIGNAL(triggered(QAction *)), this, SLOT(mpMenu(QAction *)));

	// Finally - show menu
	pMenu->exec(QCursor::pos());
	delete pMenu;
}

/*
**	FUNCTION
**		Apply new equipment type mask to all graphs of this dialog
**
**	ARGUMENTS
**		newEqpMask	- New equipment type mask.
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeam::applyMask(VacEqpTypeMask &newEqpMask)
{
	if(pWidget)
	{
		pWidget->applyMask(newEqpMask);
	}
}

/*
**	FUNCTION
**		Slot activated when item in popup menu is selected
**
**	ARGUMENTS
**		item	- ID of item, positive values are functional type IDs
**					to be added/removed from profile;
**					negative values have special meaning (see method
**					labelPressed() where menu is built).
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeam::mpMenu(QAction *pAction)
{
	if(pAction == pPeriodMenuAction)	// Show dialog to change refresh period
	{
		bool ok;
		int period = QInputDialog::getInt(this, "Refresh delay", "Enter new refresh delay [sec]:",
			VacMainView::getPollingPeriod(), 1, 300, 1, &ok);
		if(ok)
		{
			VacMainView::setPollingPeriod(period);
		}
	}
	else if(pAction->data().isValid())
	{
		int item = pAction->data().toInt();
		VacEqpTypeMask &allEqpMask = pWidget->getAllEqpMask();
		VacEqpTypeMask &eqpMask = pWidget->getEqpMask();
		if(allEqpMask.contains(item))	// Eqp type selection
		{
			if(eqpMask.contains(item))
			{
				eqpMask.remove(item);
			}
			else
			{
				eqpMask.append(item);
			}
			applyMask(eqpMask);
			int otherIndex = myIndex ? 0 : 1;
			if(dialogs[otherIndex])
			{
				dialogs[otherIndex]->applyMask(eqpMask);
			}
		}
	}
}
