#ifndef EXCELWRITEREWO_H
#define EXCELWRITEREWO_H

// EWO interface for ExcelWriterWidget widget

#include <BaseExternWidget.hxx>

#include "ExcelWriterWidget.h"

class EWO_EXPORT ExcelWriterEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	ExcelWriterEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void contentRequest(void);

private:
	ExcelWriterWidget	*baseWidget;

	QVariant invokeClear(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeAddRow(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetCellFormat(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetCellColor(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetCellText(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetCellDateTime(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetCellDateTimeWithMsec(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetCellValue(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetCellComment(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetColumnTitle(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetColumnWidth(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeWrite(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeAddHistoryValue(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeWriteHistory(const QString &name, QList<QVariant> &values, QString &error);
};

#endif	// EXCELWRITEREWO_H
