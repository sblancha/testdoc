#ifndef PROFILEEWO_H
#define	PROFILEEWO_H

// EWO interface for PS/SPS profile widget

#include <BaseExternWidget.hxx>

#include "ProfileWidget.h"

class EWO_EXPORT ProfileEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	ProfileEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void dpMouseDown(int button, int mode, int x, int y, int extra, const char *dpName);
	void dpConnectChanged(void);
	void dpeHistoryQueryQueueChange(void);
	void dpPollingChanged(void);
	void pollingPeriodChanged(int period);
	void mobileHistoryQueryChange(void);
	void dpeHistoryDepthQueryChange(void);

private:
	ProfileWidget	*baseWidget;

	QVariant invokeDataReady(const QString &name, QList<QVariant> &values,
		QString &error);
};


#endif	// PROFILEEWO_H
