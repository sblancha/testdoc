//LhcVacSetMainState:
/** Set text and color for widget according to device stste
Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param widget: in, name. of widget to set
	@action: in, name of DP.
	@param in, mode of dialog
*/
void LhcVacSetMainState(string widget, string dpName, int mode)
{
  string	status, colorString;
  
  LhcVacEqpMainColor(dpName, mode, colorString);
  LhcVacEqpMainStateString(dpName, mode, status);
  setValue( widget, "backCol", colorString );
  setValue( widget, "text", status );
}

dyn_string VacBuildProcessModeMenu(string dpName, int currentMode, dyn_string &exceptionInfo)
{
  int  typeIndex;
  string dpType;
  dyn_string result;

  // First try to get device-specific error message
  if((typeIndex = LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo)) < 1)
  {
    errMsg = "VacBuildProcessModeMenu: unsupported DP type <" + dpType + "> of <" + dpName + ">";
    return(result);
  }

  // There must be a resource - which of modes can be set from current mode depending on device subtype
  int family,type,subType;  
  LhcVacGetCtlAttributes(dpName, family, type, subType);
//  currentMode++;
  string enabled = VacResourcesGetValue(dpType + ".Subtype_" + subType + ".Transition_" + currentMode, VAC_RESOURCE_STRING, "");
  // Build lists of all enabled and disabled modes. In resulting menu first enabled
  // modes will appear, then all disabled modes
  dyn_int enabledModes, disabledModes;
  dyn_string enabledModeNames, disabledModeNames;
  for(int mode = 1 ; mode < 8 ; mode++)
  {
    string modeName = VacResourcesGetValue(dpType + ".Mode_" + mode + ".ShortName", VAC_RESOURCE_STRING, "");
    if(modeName == "")
    {
      continue;  // No such mode
    }
    bool hideItemInMenu = VacResourcesGetValue(dpType + ".Mode_" + mode + ".HideItemInMenu", VAC_RESOURCE_BOOL, FALSE);
    if(hideItemInMenu)
    {
      continue;  // No such mode
    }
    bool allowed = false;

    if(mode != currentMode)
    {
      if(mode <= strlen(enabled))
      {
        if(enabled[mode -1] == 'Y')
        {
          allowed = true;
        }
      }
    }

    if(allowed)
    {
      dynAppend(enabledModes, mode);
      dynAppend(enabledModeNames, modeName);
    }
    else
    {
      dynAppend(disabledModes, mode);
      dynAppend(disabledModeNames, modeName);
    }
  }

  // Ready, build the menu. First - allowed modes
  for(int n = 1 ; n <= dynlen(enabledModes) ; n++)
  {
    dynAppend(result, "PUSH_BUTTON, " + enabledModeNames[n] + ", " +
      enabledModes[n] + ", 1");
  }

  // Next - disabled modes
  if(dynlen(result) > 0)
  {
    dynAppend(result, "SEPARATOR");
  }
  for(int n = 1 ; n <= dynlen(disabledModes) ; n++)
  {
    dynAppend(result, "PUSH_BUTTON, " + disabledModeNames[n] + ", " +
      disabledModes[n] + ", 0");
  }
  return result;
}

int ProcessBlockedOFF(string dpName, string dpType, unsigned act,
  dyn_string &exceptionInfo)
{
  bool	        newValue;

  switch(act)
  {
  case LVA_DEVICE_SET_BLOCKED_OFF:
    newValue = true;
    break;
  case LVA_DEVICE_CLEAR_BLOCKED_OFF:
    newValue = false;
    break;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "ProcessBlockedOFF: unknown action " + act, "");
    return LVA_FAILURE;
  }

  dpSetWait(dpName + ".BlockedOFF", newValue);
  dyn_errClass  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "Action Execution: execution failed:" + err , "");
    return LVA_FAILURE;
  }
  return LVA_OK;
}

bool VacIsDeviceUsedInProcess(string dpName, string masterType, dyn_string usageList)
{
  string masterDpName, usage;
  LhcVacEqpMasterProcess(dpName, masterDpName, usage);
  if(masterDpName == "")
  {
    return false;
  }
  string masterDpType = dpTypeName(masterDpName);
  if(masterType != masterDpType)
  {
    return false;
  }
  if(dynlen(usageList) > 0)
  {
    for(int n = dynlen(usageList) ; n > 0 ; n--)
    {
      if(usage == usageList[n])
      {
        return true;
      }
    }
    return false;
  }
  return true;
}
// Write new value to DPE, raise exception if failed

void LhcVacSetActionDpeValue(string dpe, anytype value, dyn_string &exceptionInfo)
{
  DebugTN("dpe set=", dpe);
  DebugTN("value set=", value);      
  dpSetWait(dpe, value);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacSetActionDpeValue(): failed to write value to " + dpe + ": " + err , "");
  }
}
// Write new values to DPE list , raise exception if failed

int LhcVacSetActionDpeValues(string dpName, mapping valuesToWrite, dyn_string &exceptionInfo)
{
  dyn_anytype keys = mappingKeys(valuesToWrite), values;
  dyn_string dpes;
  // WR1 to be written separately
  string dpeWr1;
  anytype valueWr1;
  for(int idx = 1 ; idx <= dynlen(keys) ; idx++)
  {
    string dpeName;
    if(dpName == "")
    {
      dpeName = (string)keys[idx];
    }
    else
    {
      dpeName = dpName + (string)keys[idx];
    }
    if(strpos(dpeName, ".WR1") >= 0)
    {
      dpeWr1 = dpeName;
      valueWr1 = valuesToWrite[keys[idx]];
    }
    else
    {
      dynAppend(dpes, dpeName);
      dynAppend(values, valuesToWrite[keys[idx]]);
    }
  }

  for(int idx = dynlen(dpes) ; idx > 0 ; idx--)
  {
    //DebugTN("dpe set=", dpes[idx]);
    //DebugTN("value set=", values[idx]);    
    dpSet(dpes[idx], values[idx]);
    dyn_errClass err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacSetActionDpeValues(): failed to write value to " + dpes + ": " + err , "");
      return LVA_FAILURE;
    }
  }
  if(dpeWr1 != "")
  {
    dpSet(dpeWr1, valueWr1);
    dyn_errClass err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacSetActionDpeValues(): failed to write value to " + dpeWr1 + ": " + err , "");
      return LVA_FAILURE;
    }
  }
  return LVA_OK;
}
