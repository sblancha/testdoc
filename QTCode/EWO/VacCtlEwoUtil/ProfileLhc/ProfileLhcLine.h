#ifndef	PROFILELHCLINE_H
#define	PROFILELHCLINE_H

// One line of LHC pressure/temperature profile

#include "LhcLineParts.h"

#include "ProfileLhcLineItem.h"
#include "ProfileThermometer.h"
#include "ProfileLineSector.h"
#include "Eqp.h"
#include "EqpMsgCriteria.h"

#include "VerticalAxis.h"

#include "VacEqpTypeMask.h"


#include <stdio.h>

class Sector;

class ProfileLhcView;

class ProfileLhcLine : public QObject, public LhcLineParts
{
	Q_OBJECT

public:
	// Possible types of profile line
	typedef enum
	{
		Qrl = 0,
		Cryo = 1,
		OuterBeam = 2,
		InnerBeam = 3
	} ProfileVacSys;

	ProfileLhcLine(ProfileLhcView *parent, int type, float start, float end);
	virtual ~ProfileLhcLine();

	virtual void clear(void);
	virtual void build(VacEqpTypeMask &allowedEqpMask);
	virtual void analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allEqpMask);
	virtual void applyMask(VacEqpTypeMask &eqpMask);
	virtual void hide(void);
	virtual float nextPosition(void);
	virtual void placeItem(float &coord, int &barIdx);
	virtual void checkLinEqpExistence(bool &hasLinearEqp);
	virtual void setLogCryoLimits(int min, int max);
	virtual void setLogBeamLimits(int min, int max);
	virtual void setLinLimits(int min, int max);
	virtual void calcAxisGeometry(int &axisLocation);
	virtual void calculateAxisMinLocation(QFontMetrics &fontMetrics);
	virtual void setAxisLocation(int axisLocation);
	virtual void calcBarGeometry(int &maxBarIdx);
	virtual void buildSectors(QFontMetrics & /* fontMetrics */) {}
	virtual void calcCryoTtGeometry(void);
	virtual void bestPositionForEqp(float coord, float &bestDelta, int &screenCoord);

	void draw(QPainter &painter, bool flashingOnly, bool useAltFlashColor);

	virtual Eqp *findItemUnderMouse(int x, int y, QRect &rect);

	virtual void dump(FILE *pFile);

	// Access
	inline void setStart(float coord) { start = coord; }
	inline void setEnd(float coord) { end = coord; }
	inline int getType(void) const { return type; }
	inline void setMode(DataEnum::DataMode newMode) { mode = newMode; }
	inline void setArea(const QRect &area) { this->area = area; }
	inline unsigned getVacTypeMask(void) const { return vacTypeMask; }
	inline void setVacTypeMask(unsigned mask) { vacTypeMask = mask; }
	inline bool isHidden(void) const { return hidden; }
	inline void setHidden(bool flag) { hidden = flag; }

	inline int getScaleTop(void) const { return pLogAxis->getScaleTop(); }
	inline int getScaleBottom(void) const { return pLogAxis->getScaleBottom(); }
	inline int getLinAxisX(void) const { return pLinAxis->getAxisLocation(); }
	inline void getLogLimits(int &min, int &max) const { pLogAxis->getLimits(min, max); }
	inline void getLinLimits(int &min, int &max) const { pLinAxis->getLimits(min, max); }
	inline bool isShowSectorNames(void) const { return showSectorNames; }
	inline void setShowSectorNames(bool flag) { showSectorNames = flag; }
	inline int getAxisMinLocation(void) const { return axisMinLocation; }

signals:
	void eqpDataChanged(void);
	void eqpSelectChanged(bool linear);
	void mobileStateChanged(bool redrawNeeded);
	void eqpVisibilityChanged(void);

protected:
	// Widget where line will be drawn
	ProfileLhcView			*parent;

	// Pointer to start sector
	Sector					*pStartSector;

	// Pointer to end sector
	Sector					*pEndSector;

	// Pointer to logarithmic axis used by this line
	VerticalAxis			*pLogAxis;

	// Pointer to linear axis used by this line
	VerticalAxis			*pLinAxis;

	// List of pressure measurement devices in this line
	QList<ProfileLhcLineItem *>	eqps;

	// List of CRYO thermometers in this line
	QList<ProfileThermometer *>	cryoTs;

	// List of sector areas in this line
	QList<ProfileLineSector *>		sectors;

	// Equipment type mask for this line
	VacEqpTypeMask 					eqpMask;

	// Equipment type masks of all equipment potentially allowed on this line
	VacEqpTypeMask					allowedEqpMask;

	// Area available for this line in graph
	QRect							area;


	// X-coordinate for left edge of bars area
	int								leftX;

	// X-coordinate for right edge of basr area
	int								rightX;

	// Y-coordinate for top edge of logarithmic scale
	int								topLogY;

	// Y-coordinate for bottom edge of logarithmic scale
	int								bottomLogY;

	// Y-coordinate for top edge of linear scale
	int								topLinY;

	// Y-coordinate for bottom edge of linear scale
	int								bottomLinY;

	// Data acquisition mode for this line
	DataEnum::DataMode		mode;


	// Vacuum type mask
	unsigned				vacTypeMask;

	// Index of equipment being processed
	int					curEqpIdx;

	// Index of CRYO thermoemeter being processed
	int					curCryoTIdx;

	// Location for minimum of vertical axies
	int					axisMinLocation;
	
	// Width of one bar
	double					oneBarWidth;

	// Number of bars to be drawn
	int						nBarsToDraw;

	// Type of this line
	int						type;

	// Flag indicating if narrow bars (1 pixel) are used
	bool					useNarrowBar;

	// Flag indicating if this line is hidden
	bool					hidden;

	// Flag inidcating if sector names shall be shown beneath graph area
	bool					showSectorNames;

	virtual void buildEquipment(void);
	virtual void barVerticalGeom(ProfileLhcLineItem *pItem, double scaleMin, double scaleMax,
		double scaleHeight, int useMin);
	virtual void cryoTVerticalGeom(ProfileThermometer *pItem, int useMin, int useMax);

	virtual void addSector(ProfileLineSector *pSector);
	QColor sectorBackColor(int vacType);

	virtual int numberOfCryoThermPerSector(Sector *pSector, int subType);

	virtual int calculateSectorLabels(QFontMetrics &fontMetrics);
	virtual void drawSectors(QPainter &painter);
	virtual void drawSectorsWithBorders(QPainter & /* painter */) {}
	virtual void drawSectorsHatchFill(QPainter & /* painter */) {}
	virtual void drawBars(QPainter &painter, bool flashingOnly, bool useAltFlashColor);
	virtual void drawLinear(QPainter &painter);

	virtual Eqp *closestCryoT(int x, int y, QRect &rect);

	virtual bool isMyEqp(Eqp * /* pEqp */, int & /* partIdx */) { return false; }
	virtual void addEqp(Eqp *pEqp, int partIdx);

	// Implementation of LhcLineParts's methods
	virtual bool isMySector(Sector * /* pSect */) { return false; }
	virtual int vacTypeOfRegion(int regionType);

private slots:
	void eqpDataChange(ProfileLhcLineItem *pItem, bool colorChanged);
	void thermoDataChange(ProfileThermometer *pItem);
	void mobileStateChange(ProfileLhcLineItem *pItem);
	void itemVisibilityChanged(ProfileLhcLineItem *pItem);
};

#endif	// PROFILELHCLINE_H
