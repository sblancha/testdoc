
// Pressure transmitter (0-250 bar)

const unsigned	
  VGTR_R_OFF = 0x00010000u,
  VGTR_R_ON = 0x00020000u,
  VGTR_R_PRESS_VALID = 0x00040000u,
  VGTR_R_WARNINGS = 0x00400000u,
  VGTR_R_ERRORS = 0x00800000u,
  VGTR_R_VALID = 0x40000000u;
//LhcVacActionPossible_VGTR:
/** No actions for the time being

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, action to be executed
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_VGTR(string dpName, string dpType, const dyn_anytype action, bool &possible,
	dyn_string &exceptionInfo)
{
  possible = false;
}
//LhcVacDeviceAction_VGTR:
/** Execute action for transmitter. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VGTR(string dpName, string dpType, const dyn_anytype action, dyn_string &exceptionInfo)
{
  fwException_raise(exceptionInfo,  "ERROR", "LhcVacDeviceAction_VGTR: NOT YET IMPLEMENTED", "" );
}
