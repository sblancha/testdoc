// Common action definitions
#uses "VacCtlHistoryDataPvss"	   // Load DLL(s) or so

// Instead of passing LHC selection parameters via $Parameters and then
// parsing those parameters - save selection in global variables and
// provide function to retrieve those global variables in panel
// initialization event.
global dyn_string lhcVacCurSectList;
global unsigned	lhcVacCurVacType;
global dyn_anytype lhcVacCurAction;
global dyn_string lhcVacCurActDpTypeList;

// Extra filters for equipment to participate in global action
global bool lhcVacEqpFilterRed;
global bool lhcVacEqpFilterBlue;


// Scope and command for last executed action
global dyn_string lhcVacLastActSectList;
global dyn_anytype lhcVacLastAction;
global unsigned  lhcVacLastVacType;
global dyn_string  lhcVacLastActDpTypeList;
global dyn_string  lhcVacLastActDpList;

// Extra filters for equipment of last global action
global bool lhcVacLastEqpFilterRed;
global bool lhcVacLastEqpFilterBlue;


// 'LVA' stands for 'LHC_VAC_ACT'

// Bitmasks for subfields of action value
const unsigned LVA_DEVICE_MASK = 0x80000000u;      // Action for device, missing bit - dialog action
const unsigned LVA_NEED_SEL_MASK = 0x40000000u;    // Action requires sector selection
const unsigned LVA_NEED_CONT_SEL_MASK = 0x20000000u;  // Action requires continuous sector selection
const unsigned LVA_NEED_VAC_SEL_MASK = 0x18000000u;  // Action requires vacuum type selection
const unsigned LVA_SECTOR_ACTION_MASK = 0x04000000u;	// Action for list of sectors
const unsigned LVA_MAIN_PART_ACTION_MASK = 0x02000000u;	// Action for list of main parts
const unsigned LVA_GROUP_ACTION_MASK = 0x06000000u;      // Both SECTOR and MAIN_PART
const unsigned LVA_ACTION_MASK = 0x00FFFFFFu;  // All actions (device/dialogs) must fit to this mask

// Possible values - how many vacuums shall be selected for action
const unsigned LVA_AT_LEAST_ONE_VAC = 0x10000000u;
const unsigned LVA_EXACTLY_ONE_VAC = 0x08000000u;


// Actions - vacuum devices
const unsigned LVA_DEVICE_OPEN = 0x00000001u;
const unsigned LVA_DEVICE_CLOSE = 0x00000002u;
const unsigned LVA_DEVICE_ON = 0x00000003u;
const unsigned LVA_DEVICE_OFF = 0x00000004u;
const unsigned LVA_DEVICE_START = 0x00000005u;
const unsigned LVA_DEVICE_STOP = 0x00000006u;
const unsigned LVA_DEVICE_AUTO = 0x00000007u;
const unsigned LVA_DEVICE_RESET = 0x00000008u;

const unsigned LVA_DEVICE_SUBLIM_ON = 0x00000009u;
const unsigned LVA_DEVICE_SUBLIM_OFF = 0x0000000Au;
const unsigned LVA_DEVICE_DEGAS_ON = 0x0000000Bu;
const unsigned LVA_DEVICE_DEGAS_OFF = 0x0000000Cu;
const unsigned LVA_DEVICE_MODUL_ON = 0x0000000Du;
const unsigned LVA_DEVICE_MODUL_OFF = 0x0000000Eu;
const unsigned LVA_DEVICE_FILAMENT_1 = 0x0000000Fu;
const unsigned LVA_DEVICE_FILAMENT_2 = 0x00000010u;
const unsigned LVA_DEVICE_MEASURE_ON = 0x00000011u;
const unsigned LVA_DEVICE_MEASURE_OFF = 0x00000012u;
const unsigned LVA_DEVICE_CYCLE_ON = 0x00000013u;
const unsigned LVA_DEVICE_CYCLE_OFF = 0x00000014u;
const unsigned LVA_DEVICE_RATE_ON = 0x00000015u;
const unsigned LVA_DEVICE_RATE_OFF = 0x00000016u;
const unsigned LVA_DEVICE_TMP_STANDBY_OFF = 0x00000017u;
const unsigned LVA_DEVICE_TMP_STANDBY_ON = 0x00000018u;
const unsigned LVA_DEVICE_VVR_FORCED_OFF = 0x00000019u;
const unsigned LVA_DEVICE_VVR_FORCED_ON = 0x0000001Au;
const unsigned LVA_DEVICE_RESTART_MODE_NO_CHANGE = 0x0000001Bu;
const unsigned LVA_DEVICE_RESTART_MODE_AUTO = 0x0000001Cu;
const unsigned LVA_DEVICE_RESTART_MODE_MANUAL_VPG_STOP = 0x0000001Du;
const unsigned LVA_DEVICE_RESTART_MODE_MANUAL_VPG_START = 0x0000001Eu;
const unsigned LVA_DEVICE_VVR1_OPEN = 0x0000001Fu;
const unsigned LVA_DEVICE_VVR1_CLOSE = 0x00000020u;
const unsigned LVA_DEVICE_VVR2_OPEN = 0x00000021u;
const unsigned LVA_DEVICE_VVR2_CLOSE = 0x00000022u;
const unsigned LVA_DEVICE_VVR_OPEN = 0x00000023u;
const unsigned LVA_DEVICE_VVR_CLOSE = 0x00000024u;
const unsigned LVA_DEVICE_VVT_OPEN = 0x00000025u;
const unsigned LVA_DEVICE_VVT_CLOSE = 0x00000026u;
const unsigned LVA_DEVICE_VRGT_CONFIG = 0x000000027u;

const unsigned LVA_DEVICE_FORCED_ON = 0x00000028u;
const unsigned LVA_DEVICE_ON_AUTO = 0x00000029u;
const unsigned LVA_ACCESS_MODE_ON= 0x0000002Au;
const unsigned LVA_ACCESS_MODE_OFF= 0x0000002Bu;
// Partially OPEN & CLOSE specially for analog valve
const unsigned LVA_DEVICE_PART_OPEN= 0x0000002Cu;
const unsigned LVA_DEVICE_PART_CLOSE= 0x0000002Du;

// 30.11.2010 new commands specially for VPG_SA
const unsigned LVA_DEVICE_RESTART_MODE_VPG_LAST_KNOWN_STATE = 0x0000002Eu;
const unsigned LVA_DEVICE_RESTART_MODE_VPG_STOPPED_PP_ON = 0x0000002Fu;

// New action for VPG BP & SA: forced OFF PP
const unsigned LVA_DEVICE_VPG_PP_FORCED_OFF = 0x00000030u;

// New actions for VRPI/VGP/...: blocked OFF (not possible to switch ON with group actions)
const unsigned LVA_DEVICE_SET_BLOCKED_OFF = 0x00000031u;
const unsigned LVA_DEVICE_CLEAR_BLOCKED_OFF = 0x00000032u;

// New action for VPG: set service date
const unsigned LVA_ACTION_VPG_SERVICE_DATE = 0x00000033u;

// New action for VV: set temperature threshold for interlock
const unsigned LVA_ACTION_VV_TEMP_INTERLOCK  = 0x00000034u;
// Action for access zone
const unsigned LVA_ACCESS_ZONE_OFF = 0x00000035u;
const unsigned LVA_ACCESS_ZONE_ON = 0x00000036u;
// Action for solenoid power supply
const unsigned LVA_DEVICE_VRPM_SET_CURRENT = 0x00000037u;
const unsigned LVA_DEVICE_VRPM_SET_VOLTAGE = 0x00000038u;
const unsigned LVA_DEVICE_VRPM_REMOTE_SHUTDOWN = 0x00000039u;
const unsigned LVA_DEVICE_VRPM_CB_ON = 0x00000040u;
// Action for process(VINJ_6B02, 6A01...)
const unsigned LVA_DEVICE_PROC_SET_MODE = 0x00000041u;
const unsigned LVA_DEVICE_FORCE = 0x00000042u;
const unsigned LVA_DEVICE_MANUAL = 0x00000043u;

// Actions for setting VGI special parameters
const unsigned LVA_VGI_SET_EMISSION_CURRENT = 0x00000044u;
const unsigned LVA_VGI_SET_RESIDUAL_PRESSURE = 0x00000045u;
const unsigned LVA_VGI_SET_MODULATION_FACTOR = 0x00000046u;
const unsigned LVA_VGI_SET_SENSIBILITY = 0x00000047u;
const unsigned LVA_VGI_SET_GAIN = 0x00000048u;
const unsigned LVA_VGI_SET_DEGAS_TIME = 0x00000049u;
const unsigned LVA_VGI_SET_MODULATION_TIME = 0x0000004Au;
const unsigned LVA_VGI_SET_DEGAS_CURRENT = 0x0000004Bu;
const unsigned LVA_VGI_SET_GRID_VOLTAGE = 0x0000004Cu;
const unsigned LVA_VGI_SET_ANALOG_OUT_OFFSET = 0x0000004Du;
const unsigned LVA_VGI_SET_OVER_PANGE_PRESSURE = 0x0000004Eu;
const unsigned LVA_VGI_ENABLE_LOCAL = 0x0000004Fu;
const unsigned LVA_VGI_DISABLE_LOCAL = 0x00000050u;
const unsigned LVA_VGI_AUTO_GAIN = 0x00000051u;
const unsigned LVA_VGI_DEFAULT_PARAM = 0x00000052u;
const unsigned LVA_VGI_MBAR_UNIT = 0x00000053u;
// Actions for setting mode for VPG_6A
const unsigned LVA_VPG_PUMP = 0x00000054u;
const unsigned LVA_VPG_VENT = 0x00000055u;
const unsigned LVA_VPG_VENT_TURBO = 0x00000056u;
// Actions for PLC
const unsigned LVA_PLC_PING = 0x00000057u;
const unsigned LVA_PLC_RESET = 0x00000058u;
const unsigned LVA_PLC_INIT = 0x00000059u;
// Actions for VG_STD
const unsigned LVA_VG_STD_FORCE = 0x00000060u;
const unsigned LVA_VG_STD_MANUAL = 0x00000061u;
const unsigned LVA_VG_STD_LOCAL = 0x00000062u;
const unsigned LVA_VG_STD_REMOTE = 0x00000063u;

const unsigned LVA_DEVICE_ACK = 0x00000064u;
const unsigned LVA_DEVICE_BYPASS = 0x00000065u;
// Actions for setting mode for VPG_6E01
const unsigned LVA_DEVICE_STOP_VPG = 0x00000066u;

// Actions for TPG300 hot start settings
const unsigned LVA_TPG300_HOT_START_DISABLE = 0x00000067u;
const unsigned LVA_TPG300_HOT_START_ENABLE = 0x00000068u;
// Actions for VP_NEG, VPN_MUX, VPN hot start settings
const unsigned LVA_DEVICE_VPN_SETMODE = 0x00000069u;
const unsigned LVA_DEVICE_VPN_MANUAL_VOLT_SETPOINT = 0x0000006Au;
const unsigned LVA_DEVICE_VPN_MANUAL_CUR_SETPOINT = 0x0000006Bu;
const unsigned LVA_DEVICE_VPN_MANUAL_TIME_SETPOINT = 0x0000006Cu;
const unsigned LVA_DEVICE_SETMUX_CHANNEL = 0x0000006Du;
// Actions specific for VG_DP
const unsigned LVA_DEVICE_SET_LOW_THRESHOLD = 0x0000006Eu;
const unsigned LVA_DEVICE_SET_HIGH_THRESHOLD = 0x0000006Fu;
const unsigned LVA_DEVICE_ENABLE_PENNING = 0x00000070u;
const unsigned LVA_DEVICE_DISABLE_PENNING = 0x00000071u;
// Action specific for VPS
const unsigned LVA_DEVICE_VPS_SWITCH_FILAMENT= 0x00000072u;    
const unsigned LVA_DEVICE_VPS_RESET_COUNTERS = 0x00000073u;
const unsigned LVA_DEVICE_VPS_FILAMENT1 = 0x00000074u;
const unsigned LVA_DEVICE_VPS_FILAMENT2 = 0x00000075u;
const unsigned LVA_DEVICE_VPS_SET_DEGASSING_MODE = 0x00000076u; 
const unsigned LVA_DEVICE_VPS_SET_SUBLIMATION_MODE = 0x00000077u;
const unsigned LVA_DEVICE_VPS_SET_SUBLIMATION_TIME = 0x00000078u;
const unsigned LVA_DEVICE_VPS_SET_SUBLIMATION_RATE = 0x00000079u;
const unsigned LVA_DEVICE_VPS_SET_DEGASSING_CURRENT = 0x0000007Au;
const unsigned LVA_DEVICE_VPS_APPLY_SUBLIMATION = 0x0000007Bu;
const unsigned LVA_DEVICE_VPS_APPLY_DEGASSING = 0x0000007Cu;

// Action specific for VPS_Process
const unsigned LVA_DEVICE_VPS_PROCESS_START_NOW = 0x0000007Du;
const unsigned LVA_DEVICE_VPS_PROCESS_START_ONCE = 0x0000007Eu;
const unsigned LVA_DEVICE_VPS_PROCESS_APPLY_SET = 0x0000007Fu;
const unsigned LVA_DEVICE_VPS_PROCESS_APPLY_START = 0x00000080u;
// Action (global) specific for VPG_6A01
const unsigned LVA_DEVICE_VVR1_MANUAL = 0x00000081u;
const unsigned LVA_DEVICE_VVR2_MANUAL = 0x00000082u;

// Action VR_PI_CHAN
const unsigned LVA_DEVICE_STEP = 0x00000083u;
const unsigned LVA_DEVICE_3KV_FIXED = 0x00000084u;
const unsigned LVA_DEVICE_5KV_FIXED = 0x00000085u;

// Action specific for VPC_HCCC not used here -> check vclVPC_HCCC.ctl
const unsigned LVA_DEVICE_VPC_HCCC_REGEN = 0x00000401u;
const unsigned LVA_DEVICE_VPC_HCCC_COOLDOWN = 0x00000600u;
const unsigned LVA_DEVICE_VPC_HCCC_RESET_COMP = 0x00000402u;

// Actions which can be execute to the most of device types:
// set device to NotConnected, or return it back to controlled
// See VTL #2673
const unsigned LVA_DEVICE_SET_NOT_CONNECTED = 0x00000090u;
const unsigned LVA_DEVICE_SET_CONNECTED = 0x00000091u;

// The only action for VR_GT
const unsigned LVA_DEVICE_SET_CMD = 0x00000092u;
// The only action for VG_PT
const unsigned LVA_DEVICE_VG_PT_ON_FORCED= 0x00000093u;
const unsigned LVA_DEVICE_VG_PT_ON_PROTECT= 0x00000094u;
const unsigned LVA_DEVICE_VG_PT_OFF= 0x00000095u;

// Actions for PressureSwitch protection of VPG_6A01
const unsigned LVA_DEVICE_PS_PROT_ON = 0x00000096u;
const unsigned LVA_DEVICE_PS_PROT_OFF = 0x00000097u;

// Action specific for VVS_LHC, for setting Reaction Time
const unsigned LVA_VVS_LHC_EDIT_THRESHOLD_ON = 0x00000098u;

// Actions for VPG_MBLK
const unsigned LVA_VPG_MBLK_REMOTE = 0x00000099u;
const unsigned LVA_VPG_MBLK_LOCAL = 0x0000009Au;
const unsigned LVA_VPG_MBLK_VVR2_FORCE = 0x0000009Bu;
const unsigned LVA_VPG_MBLK_VVR2_UNFORCE = 0x0000009Cu;
const unsigned LVA_VPG_MBLK_VVR1_FORCE = 0x0000009Du;
const unsigned LVA_VPG_MBLK_VVR1_UNFORCE = 0x0000009Eu;
const unsigned LVA_VPG_MBLK_VG2_ON = 0x0000009Fu;
const unsigned LVA_VPG_MBLK_VG2_OFF = 0x000000A0u;
const unsigned LVA_VPG_MBLK_VG1_ON = 0x000000A1u;
const unsigned LVA_VPG_MBLK_VG1_OFF = 0x000000A2u;
const unsigned LVA_VPG_MBLK_SET_MODE = 0x000000A3u;
const unsigned LVA_VPG_SET_MODE_OFF = 0x000000A4u;
const unsigned LVA_VPG_SET_MODE_STOP = 0x000000A5u;
const unsigned LVA_VPG_SET_MODE_PUMP = 0x000000A6u;
const unsigned LVA_VPG_SET_MODE_VENT_TURBO = 0x000000A7u;
const unsigned LVA_VPG_SET_MODE_VENT_ALL = 0x000000A8u;
const unsigned LVA_VPG_SET_MODE_LEAK_DET = 0x000000A9u;
const unsigned LVA_VPG_SET_MODE_LARGE_LEAK_DET = 0x000000AAu;
const unsigned LVA_VPG_MBLK_RESET_VPP_THERM = 0x000000ABu;
const unsigned LVA_VPG_MBLK_RESET_TMP_ERR = 0x000000ACu;

// Work Order creation
const unsigned LVA_OPEN_INTERLOCKS = 0x000000B4u;
const unsigned LVA_DEVICE_WORKORDER = 0x000000B5u;
const unsigned LVA_VSECTOR_OPERATIONAL = 0x000000B6u; // [VACCO-929] [VACCO-948] [VACCO-1645]
const unsigned LVA_VSECTOR_ONWORK = 0x000000B7u;      // [VACCO-929] [VACCO-948] [VACCO-1645]
const unsigned LVA_VSECTOR_VENTED = 0x000000B8u;      // [VACCO-929] [VACCO-948] [VACCO-1645]

// Additional actions for TPG300
const unsigned LVA_DEVICE_SAVEH = 0x000000BAu;	// save hotstart order to TPG300

// Additional for VPG
const unsigned LVA_DEVICE_BAKE_OFF = 0x000000BAu; 
const unsigned LVA_DEVICE_BAKE_ON  = 0x000000BBu;
const unsigned LVA_DEVICE_VVR1_FORCE  = 0x000000BCu;
const unsigned LVA_DEVICE_VVR1_UNFORCE= 0x000000BDu;
const unsigned LVA_DEVICE_VVR2_FORCE  = 0x000000BEu;
const unsigned LVA_DEVICE_VVR2_UNFORCE= 0x000000BFu;
const unsigned LVA_DEVICE_VG1_ON      = 0x000000C0u;
const unsigned LVA_DEVICE_VG1_OFF     = 0x000000C1u;
const unsigned LVA_DEVICE_VPP_RESET   = 0x000000C2u;
const unsigned LVA_DEVICE_VPT_RESET   = 0x000000C3u;

// Action for the alert feature
const unsigned LVA_ALERT_ACKNOWLEDGE =  0x000000C4u;

// Additional generic device action
const unsigned LVA_DEVICE_STANDBY = 0x000000C5u;
const unsigned LVA_DEVICE_SAFEON = 0x000000C6u;
const unsigned LVA_DEVICE_SAFEOFF = 0x000000C7u;
const unsigned LVA_DEVICE_PCSON = 0x000000C8u;
const unsigned LVA_DEVICE_PCSOFF = 0x000000C9u;

// Actions for VR_PI but may be generic
const unsigned LVA_DEVICE_PROFIBUS = 0x000000CAu;
const unsigned LVA_DEVICE_LOCAL = 0x000000CBu;
const unsigned LVA_DEVICE_REMOTE_RESET = 0x000000CCu;
const unsigned LVA_DEVICE_POWER_CYCLE = 0x000000CDu;
// Actions for VR_PI
const unsigned LVA_VR_PI_RD_HWERRORCODE = 0x000000CEu;
const unsigned LVA_VR_PI_RD_FIRMWAREVERS = 0x000000CFu;
const unsigned LVA_VR_PI_RD_HWCTRLCONFIG = 0x000000D0u;
const unsigned LVA_VR_PI_RD_AUTOSTART = 0x000000D1u;
const unsigned LVA_VR_PI_RD_FANMODE = 0x000000D2u;
const unsigned LVA_VR_PI_RD_PRUNIT = 0x000000D3u;
const unsigned LVA_VR_PI_RD_FILTER = 0x000000D4u;
const unsigned LVA_VR_PI_WR_AUTOSTART_OFF = 0x000000D5u;
const unsigned LVA_VR_PI_WR_AUTOSTART_LASTKNOWN = 0x000000D6u;
const unsigned LVA_VR_PI_WR_FANMODE_ONLY = 0x000000D7u;
const unsigned LVA_VR_PI_WR_FANMODE_ALWAYS = 0x000000D8u;
const unsigned LVA_VR_PI_WR_PRUNIT_MBAR = 0x000000D9u;
const unsigned LVA_VR_PI_WR_PRUNIT_CURRENT = 0x000000DAu;
const unsigned LVA_VR_PI_WR_FILTER_FAST = 0x000000DBu;
const unsigned LVA_VR_PI_WR_FILTER_MEDIUM = 0x000000DCu;
const unsigned LVA_VR_PI_WR_FILTER_SLOW = 0x000000DDu;
// Actions for VR_PI Relays VA_RI
const unsigned LVA_VA_RI_SOURCE_NEWVALUE_2HV = 0x000000DEu;
const unsigned LVA_VA_RI_SOURCE_LASTORDER = 0x000000DFu;
const unsigned LVA_VA_RI_SOURCE_DEFAULT = 0x000000E0u;
const unsigned LVA_VA_RI_THRESHOLD_NEWVALUE = 0x000000E1u;
const unsigned LVA_VA_RI_THRESHOLD_LASTORDER = 0x000000E2u;
const unsigned LVA_VA_RI_THRESHOLD_DEFAULT = 0x000000E3u;
const unsigned LVA_VA_RI_HYST_NEWVALUE = 0x000000E4u;
const unsigned LVA_VA_RI_HYST_LASTORDER = 0x000000E5u;
const unsigned LVA_VA_RI_HYST_DEFAULT = 0x000000E6u;
const unsigned LVA_VA_RI_UPDATE = 0x000000E7u;
const unsigned LVA_VA_RI_SOURCE_NEWVALUE_4HV = 0x000000E8u;
const unsigned LVA_VA_RI_SET_UNIT_MBAR = 0x000000E9u;
// Actions for VR_PI but may be generic
const unsigned LVA_DEVICE_RESET_WRS = 0x000000EAu;
// Actions for VP_GU eqp type VPGMA
const unsigned LVA_DEVICE_TIGHT_TEST_ON = 0x000000EBu;
const unsigned LVA_DEVICE_TIGHT_TEST_OFF = 0x000000ECu;
const unsigned LVA_DEVICE_RESET_VPP_ALR = 0x000000EDu;
const unsigned LVA_DEVICE_PURGE = 0x000000EEu;
// Actions for VP_GU
const unsigned LVA_DEVICE_VENT = 0x000000EFu;
// Actions for V_TCP
const unsigned LVA_V_TCP_ENABLE = 0x000000F0u;
const unsigned LVA_V_TCP_DISABLE = 0x000000F1u;
const unsigned LVA_V_TCP_DISCONNECT = 0x000000F2u;
// Action VP_GU (others)
const unsigned LVA_DEVICE_STOP_PP_ON = 0x000000F3u;
// Action VG_U (others)
const unsigned LVA_DEVICE_SET_ALERT_THRESH = 0x000000F4u;
const unsigned LVA_DEVICE_SET_FILTER_TIME = 0x000000F5u;
const unsigned LVA_DEVICE_SET_PROT_CHAN_SOURCE = 0x000000F6u;
const unsigned LVA_DEVICE_SET_PROT_UP_THRESH = 0x000000F7u;
const unsigned LVA_DEVICE_SET_PROT_LOW_THRESH = 0x000000F8u;
// Action specific VP_TP
const unsigned LVA_DEVICE_SPD_TO_ROTATION_SP = 0x000000F9u;
const unsigned LVA_DEVICE_SPD_TO_NOMINAL = 0x000000FAu;

////////////////////////////////////////////////////////////////
// Actions - open dialogs
// Note usage of high bits (need selection etc.)
////////////////////////////////////////////////////////////////
const unsigned LVA_OPEN_MAIN = 0x00000001u;
const unsigned LVA_OPEN_PROFILE = 0x60000002u;
const unsigned LVA_OPEN_SYNOPTIC = 0x60000003u;
const unsigned LVA_OPEN_SECTOR = 0x60000004u;
/* Let's try if it will work without selection. L.Kopylov 20.10.2010
const unsigned LVA_OPEN_DEV_LIST = 0x40000005u;
*/
const unsigned LVA_OPEN_DEV_LIST = 0x00000005u;

const unsigned LVA_OPEN_DETAIL_PANEL = 0x00000006u;
const unsigned LVA_OPEN_CONFIG_PANEL = 0x00000007u;
const unsigned LVA_OPEN_PRINT_SETUP = 0x00000008u;
const unsigned LVA_OPEN_ACT_RESULT = 0x00000009u;
const unsigned LVA_OPEN_ALERTS = 0x0000000Au;
const unsigned LVA_OPEN_SBS_ARCHIVE_CONFIG = 0x0000000Bu;
const unsigned LVA_OPEN_SBS_SESSION_MANAGER = 0x0000000Cu;
const unsigned LVA_OPEN_SBS_SESSION_EXPORT = 0x0000000Du;
const unsigned LVA_OPEN_SBS_SESSION_TREND = 0x0000000Eu;
const unsigned LVA_OPEN_PR_HISTORY = 0x0000000Fu;
const unsigned LVA_OPEN_HELP = 0x00000010u;
const unsigned LVA_OPEN_ALL_PROFILE = 0x00000011u;
const unsigned LVA_OPEN_CREATE_UI_DP = 0x00000012u;
const unsigned LVA_OPEN_SYSTEM_INTEGRITY = 0x00000013u;
const unsigned LVA_OPEN_GLOBAL_ACTION = 0x00000014u;
const unsigned LVA_OPEN_DPE_ARCHIVE_CONFIG = 0x00000015u;
const unsigned LVA_OPEN_VACOK = 0x60000016u;
const unsigned LVA_OPEN_STATE_HISTORY = 0x60000017u;
const unsigned LVA_OPEN_STATE_DETAILS_HISTORY = 0x60000018u;
const unsigned LVA_OPEN_BAKEOUT_HISTORY = 0x00000019u;
const unsigned LVA_OPEN_VPGM_SEARCH = 0x0000001Au;
const unsigned LVA_OPEN_ALARM_EMAIL_CONFIG = 0x0000001Bu;
const unsigned LVA_OPEN_MOBILE_EQP = 0x0000001Cu;
const unsigned LVA_OPEN_BAKEOUT_DETAILS = 0x0000001Du;
const unsigned LVA_OPEN_BAKEOUT_RACK_PROFILE = 0x0000001Eu;
const unsigned LVA_OPEN_EXP_AREA_DETAILS = 0x0000001Fu;
const unsigned LVA_OPEN_ALARM_MASK_CONFIG = 0x00000020u;
const unsigned LVA_OPEN_EQP_OSC_FILTER = 0x00000021u;
const unsigned LVA_OPEN_COMMENT = 0x00000022u;
const unsigned LVA_OPEN_ALL_LHC_PROFILE = 0x00000023u;
const unsigned LVA_OPEN_ALL_LHC_SYNOPTIC = 0x00000024u;
const unsigned LVA_OPEN_ACCESS_MODE = 0x00000025u;
const unsigned LVA_OPEN_VACOK_SMS_CONFIG = 0x00000026u;
const unsigned LVA_OPEN_VALVE_INTRLCK_CONFIG = 0x00000027u;
const unsigned LVA_OPEN_BEAM_MODE_EMAIL_CONFIG = 0x00000028u;
const unsigned LVA_OPEN_VPG_SERVICE = 0x00000029u;
const unsigned LVA_OPEN_TEMP_INTERLOCK = 0x0000002Au;
const unsigned LVA_OPEN_ALL_LHC_PROFILE_BEAM_TYPE = 0x0000002Bu;
const unsigned LVA_OPEN_THERM_LIST = 0x0000002Cu;
const unsigned LVA_OPEN_PREDEFINED_COMMENTS = 0x0000002Du;
const unsigned LVA_OPEN_VGP_EMAIL_CONFIG = 0x0000002Eu;
const unsigned LVA_OPEN_COMMENT_SEARCH = 0x0000002Fu;
const unsigned LVA_OPEN_ACCESS_CONTROL = 0x00000030u;
const unsigned LVA_OPEN_ACCESS_ZONE = 0x00000031u;
const unsigned LVA_OPEN_SMS_GENERAL = 0x00000032u;
const unsigned LVA_OPEN_VVS_SERVICE = 0x00000033u;
const unsigned LVA_OPEN_VVS_INTERLOCKS = 0x00000034u;
const unsigned LVA_OPEN_VVS_BRIDGES = 0x00000035u;
const unsigned LVA_OPEN_BEAM_PARAM_CONFIG = 0x00000036u;
const unsigned LVA_OPEN_VVS_ZONE_MAP = 0x00000037u;
const unsigned LVA_OPEN_BEAM_PARAM = 0x00000038u;
const unsigned LVA_OPEN_SOLENOID_TABLE= 0x00000039u;
const unsigned LVA_OPEN_BIT_HISTORY = 0x00000042u;
const unsigned LVA_OPEN_HISTORY_CONFIG = 0x00000043u;
const unsigned LVA_OPEN_MACHINE_MODE = 0x00000044u;
const unsigned LVA_OPEN_ACTION_PRIVELEGE = 0x00000045u;
const unsigned LVA_OPEN_VVS_INTERLOCKS_SPS = 0x00000046u;
const unsigned LVA_OPEN_MON_PROFILE_SPS  = 0x00000047u;
const unsigned LVA_OPEN_MON_PROFILE_PS  = 0x00000048u;
const unsigned LVA_OPEN_MON_PROFILE_LHC_BEAM  = 0x00000049u;
const unsigned LVA_OPEN_MON_PROFILE_LHC_ISO  = 0x0000004Au;
const unsigned LVA_OPEN_MON_PROFILE_LHC_CRYO  = 0x0000004Bu;
const unsigned LVA_OPEN_ARCHIVE_HISTORY = 0x0000004Cu;
const unsigned LVA_OPEN_HIDE_EQUIPMENT = 0x0000004Du;
const unsigned LVA_OPEN_FLANGE_LIST = 0x0000004Eu;
const unsigned LVA_OPEN_SETPOINT_HISTORY = 0x0000004Fu;
const unsigned LVA_OPEN_DIGITAL_INPUT_MONITOR = 0x00000050u;
const unsigned LVA_OPEN_TPG300_HOT_START = 0x00000051u;
const unsigned LVA_OPEN_VPN_MUX = 0x00000052u;
const unsigned LVA_OPEN_DIGITAL_OUTPUT_ALARM = 0x00000053u;
const unsigned LVA_OPEN_PIQUET_DEV_LIST = 0x00000054u;
const unsigned LVA_OPEN_PIQUET_CONFIG = 0x00000055u;
const unsigned LVA_OPEN_VPS_CONTROL = 0x00000056u;
const unsigned LVA_OPEN_CONTROLLER_PANEL = 0x00000057u;
const unsigned LVA_OPEN_VVS_MONITORING = 0x00000058u;
const unsigned LVA_OPEN_ARCHIVE_SESSION = 0x00000059u;
const unsigned LVA_OPEN_NV_HISTORY = 0x0000005Au;
const unsigned LVA_OPEN_EQP_HELP = 0x0000005Bu;
const unsigned LVA_OPEN_WORKORDER_CONFIGURATION = 0x0000005Cu;
const unsigned LVA_OPEN_BEAM_PARAM_SPS = 0x0000005Du;
//const unsigned LVA_OPEN_PRESSURE_MAP = 0x0000005Eu; // BVO requests to remove this feature
const unsigned LVA_OPEN_VP_IP_CONTROLLER = 0x0000005Fu;
const unsigned LVA_OPEN_SIMCARD = 0x00000060u;
const unsigned LVA_OPEN_VPLC_PF_CONFIG = 0x00000061u;
const unsigned LVA_DISABLE_MAN_CONTROL = 0x00000062u;
const unsigned LVA_ENABLE_MAN_CONTROL = 0x00000063u;
const unsigned LVA_SKIP_STEP = 0x00000064u;
const unsigned LVA_BALANCE = 0x00000065u;
const unsigned LVA_EVACUATE = 0x00000066u;
const unsigned LVA_PREPARE = 0x00000067u;
const unsigned LVA_FORCE_DELAY_EVAC_MEAS = 0x00000068u;
const unsigned LVA_VELO_START_IONPUMP = 0x00000069u;
const unsigned LVA_FORCE_DELAY_VENT_MEAS = 0x0000006Au;
const unsigned LVA_VELO_SMOG_INJECT = 0x0000006Bu;
const unsigned LVA_VELO_SMOG_START = 0x0000006Cu;
const unsigned LVA_VELO_SMOG_DISABLE = 0x0000006Du;
const unsigned LVA_VELO_SMOG_ENABLE = 0x0000006Eu;
const unsigned LVA_VELO_SMOG_SET_TURBO_SPEED = 0x0000006Fu;
const unsigned LVA_VELO_PS300_BAKE_OFF_CMD   = 0x00000083u;
const unsigned LVA_VELO_PS300_BAKE_ON_CMD    = 0x00000084u;
const unsigned LVA_VELO_VESSEL_BAKE_OFF_CMD  = 0x00000085u;
const unsigned LVA_VELO_VESSEL_BAKE_ON_CMD   = 0x00000086u;
const unsigned LVA_VELO_PS300_ITL_UNFORCE    = 0x00000087u;
const unsigned LVA_VELO_PS300_ITL_FORCE      = 0x00000088u;
const unsigned LVA_VELO_VESSEL_ITL_UNFORCE   = 0x00000089u;
const unsigned LVA_VELO_VESSEL_ITL_FORCE     = 0x0000008Au;
const unsigned LVA_PS300_GV302_SET_TEMP_SETPOINT = 0x0000008Bu;
const unsigned LVA_PS300_TP301_SET_TEMP_SETPOINT = 0x0000008Cu;
const unsigned LVA_PS300_BAKEOUT_SET_HYST_VAL    = 0x0000008Du;
const unsigned LVA_ITL_GIS_V5_UNFORCE = 0x0000008Eu;
const unsigned LVA_ITL_GIS_V5_FORCE = 0x0000008Fu;
const unsigned LVA_ITL_MOTION_UNFORCE = 0x00000090u;
const unsigned LVA_ITL_MOTION_FORCE = 0x00000091u;
const unsigned LVA_ITL_COOLING_UNFORCE = 0x00000092u;  
const unsigned LVA_ITL_COOLING_FORCE = 0x00000093u;
const unsigned LVA_ITL_UPS_UNFORCE = 0x00000094u;
const unsigned LVA_ITL_UPS_FORCE = 0x00000095u;
const unsigned LVA_ITL_VVS_1R8_UNFORCE = 0x00000096u;
const unsigned LVA_ITL_VVS_1R8_FORCE = 0x00000097u;
const unsigned LVA_ITL_ACLSMOKE_UNFORCE = 0x00000098u;
const unsigned LVA_ITL_ACLSMOKE_FORCE = 0x00000099u;
const unsigned LVA_ITL_WTRCOOL_UNFORCE = 0x0000009Au;
const unsigned LVA_ITL_WTRCOOL_FORCE = 0x0000009Bu;
const unsigned LVA_ITL_GIS_UNFORCE = 0x0000009Cu;
const unsigned LVA_ITL_GIS_FORCE = 0x0000009Du;
const unsigned LVA_ITL_VVS_1L8_UNFORCE = 0x0000009Eu;
const unsigned LVA_ITL_VVS_1L8_FORCE = 0x0000009Fu;
const unsigned LVA_ITL_OP_TRIGGERED_RESET_CMD = 0x000000A0u;
const unsigned LVA_START_BAKEOUT = 0x000000A1u;
const unsigned LVA_END_BAKEOUT = 0x000000A2u;
const unsigned LVA_VELO_SMOG_FINISH = 0x000000A3u;
const unsigned LVA_VELO_STOP_IONPUMP = 0x000000A4u;
const unsigned LVA_ACT_RP_TOGGLE = 0x000000A5u;
const unsigned LVA_IGN_RP101_TOGGLE = 0x000000A6u;
const unsigned LVA_IGN_RP201_TOGGLE = 0x000000A7u;
const unsigned LVA_IGN_PI102_TOGGLE = 0x000000A8u;
const unsigned LVA_IGN_PI202_TOGGLE = 0x000000A9u;
const unsigned LVA_IGN_PI421_TOGGLE = 0x000000AAu;
const unsigned LVA_IGN_PI422_TOGGLE = 0x000000ABu;
const unsigned LVA_IGN_PE421_TOGGLE = 0x000000ACu;
const unsigned LVA_IGN_PE422_TOGGLE = 0x000000ADu;
const unsigned LVA_IGN_PI411_TOGGLE = 0x000000AEu;
const unsigned LVA_IGN_PI412_TOGGLE = 0x000000AFu;
const unsigned LVA_IGN_PE411_TOGGLE = 0x000000B0u;
const unsigned LVA_IGN_PE412_TOGGLE = 0x000000B1u;
const unsigned LVA_IGN_IP431_TOGGLE = 0x000000B2u;
const unsigned LVA_IGN_IP441_TOGGLE = 0x000000B3u;
const unsigned LVA_OPP_10_TOGGLE = 0x000000B4u;
const unsigned LVA_OPP_10_RESET = 0x000000B5u;
const unsigned LVA_OPP_25_TOGGLE = 0x000000B6u;
const unsigned LVA_OPP_25_RESET = 0x000000B7u;
const unsigned LVA_OPEN_ALARM_BOOLEANCALC = 0x000000B8u;
const unsigned LVA_OPEN_NOTIFICATION = 0x000000B9u;
const unsigned LVA_DEVICE_RACK = 0x000000BAu;

// Constants for dialogs implemented in Qt - must match
// enum in Qt code
const int QT_DIALOG_SYNOPTIC = 1;
const int QT_DIALOG_PROFILE = 2;
const int QT_DIALOG_SECTOR = 3;
const int QT_DIALOG_PROFILE_ALL_SPS = 4;
const int QT_DIALOG_SYNOPTIC_LHC = 5;
const int QT_DIALOG_PROFILE_LHC = 6;
const int QT_DIALOG_PROFILE_ALL_LHC_IN_OUT = 7;
const int QT_DIALOG_PROFILE_ALL_LHC_BEAM = 8;
const int QT_DIALOG_SYNOPTIC_ALL_LHC = 9;
const int QT_DIALOG_SECTOR_LHC = 10;
const int QT_DIALOG_MOBILE_HISTORY = 11;
const int QT_DIALOG_BAKEOUT_RACK_PROFILE = 12;
const int QT_DIALOG_BAKEOUT_RACK_DETAILS = 13;
const int QT_DIALOG_RACKS = 14;
const int QT_DIALOG_INTERLOCKS = 15;

// Return codes
const int LVA_OK = 0;
const int LVA_FAILURE = -1;


//max curves per one trend window
const int	LVA_MAX_DP_IN_HISTORY = 10;


// LvaDialogAction
/** Execute 'dialog' action (open/raise...)

Usage: LHC Vacuum control internal

PVSS manager usage: VISION

  @param act: in, action to be executed
    1 = action (bit mask, see above) [unsigned]
    2 = mode (online/replay) [int]
    3 = historyId, for history profile dialog [int]
    4 = load, dialog mode (safe/load)  for history config dialog [bool]
  @param sectList: in, list of selected sectors.
  @param dpName: in, Name of selected DP
  @param vacType: in, vacuum type selection bit mask
  @param exceptionInfo: out, details of any exceptions are returned here

  @return None.
  @author L.Kopylov
*/
int LvaDialogAction(dyn_anytype act, dyn_string sectList, const string dpName,
    const unsigned vacType, dyn_string &exceptionInfo)
{
  if(dynlen(act) < 2)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LvaDialogAction: invalid act argument", "");
    return LVA_FAILURE;
  }

  // When action come from VSECT_VPI_SUM - sector list is empty, for such
  // device sector list shall contain the only sector
  if((dynlen(sectList) == 0) && (dpName != ""))
  {
    int vac;
    string sector1, sector2, mainPart;
    bool isBorder;
    LhcVacDeviceVacLocation(dpName, vac, sector1, sector2, mainPart, isBorder, exceptionInfo);
    if(sector1 != "")
    {
      dynAppend(sectList, sector1);
    }
    if(isBorder && (sector2 != sector1))
    {
      dynAppend(sectList, sector2);
    }
  }

  // Decide what and how to show
  string fileName, panelNameBase, useDpName, useDpType, configName, dpType, resizeMode = "None";
  string resourceName, masterDp, masterDpType;
  int configRef;
  bool manyAllowed;
  dyn_string dollars;
  int qtDialogType = 0, channel;
  string concurrentPanel;  // If having this panel opened shall prevent opening requested panel
  switch(act[1])
  {
  case LVA_OPEN_PROFILE:
    switch(glAccelerator)
    {
    case "LHC":
      qtDialogType = vacType == LVA_VACUUM_ANY_BEAM ? QT_DIALOG_PROFILE : QT_DIALOG_PROFILE_LHC;
      break;
    default:
      qtDialogType = QT_DIALOG_PROFILE;
      break;
    }
    break;
  case LVA_OPEN_ALL_PROFILE:
    qtDialogType = QT_DIALOG_PROFILE_ALL_SPS;
    break;
  case LVA_OPEN_SYNOPTIC:
    fileName = LhcVacGetSpecSynoptic(sectList);
    fileName = "";  // L.Kopylov 25.05.2012
    if(fileName == "")  // Nothing special
    {
      switch(glAccelerator)
      {
      case "LHC":
        qtDialogType = vacType == LVA_VACUUM_ANY_BEAM ? QT_DIALOG_SYNOPTIC : QT_DIALOG_SYNOPTIC_LHC;
        break;
      default:
        qtDialogType = QT_DIALOG_SYNOPTIC;
        break;
      }
    }
    else
    {
      panelNameBase = "Synoptic";
      manyAllowed = true;
    }
    break;
  case LVA_OPEN_SECTOR:
    fileName = LhcVacGetSpecSectView(sectList);
    fileName = "";  // L.Kopylov 29.05.2012
    if(fileName == "")  // Nothing special
    {
      switch(glAccelerator)
      {
      case "LHC":
        qtDialogType = vacType == LVA_VACUUM_ANY_BEAM ? QT_DIALOG_SECTOR : QT_DIALOG_SECTOR_LHC;
        break;
      default:
        qtDialogType = QT_DIALOG_SECTOR;
        break;
      }
    }
    else
    {
      panelNameBase = "SectorView";
      manyAllowed = true;
    }
    break;
  case LVA_OPEN_DEV_LIST:
    fileName = "vision/dialogs/DeviceListExt.pnl";
//    fileName = "vision/dialogs/DeviceList.pnl";
    panelNameBase = "DeviceList";
    manyAllowed = false;
    concurrentPanel = "VMR_Device_List";
    resizeMode = "Scale";  // !!!!!!!
    break;
  case LVA_OPEN_PIQUET_DEV_LIST:
    fileName = "vision/dialogs/DevListPiquet.pnl";
    panelNameBase = "VMR_Device_List";
    manyAllowed = false;
    concurrentPanel = "DeviceList";
    break;
  case LVA_OPEN_PIQUET_CONFIG:
    fileName = "vision/dialogs/DevListPiquetConfig.pnl";
    panelNameBase = "VMR_Device_List_Config";
    manyAllowed = false;
    break;
  case LVA_OPEN_VPS_CONTROL:
    fileName = "vision/dialogs/VpsControl.pnl";
    panelNameBase = "VPS_Control";
    manyAllowed = false;
    break;
  case LVA_OPEN_VACOK:
    fileName = "vision/dialogs/vacOK.pnl";
    panelNameBase = "Alarms_for_CRYO";
    manyAllowed = false;
    resizeMode = "Scale";  // !!!!!!!
    break;
  case LVA_OPEN_STATE_HISTORY:
    fileName = "vision/dialogs/stateHistoryQuery.pnl";
    panelNameBase = "State History Query";
    manyAllowed = false;
    break;
  case LVA_OPEN_ACT_RESULT:
    if(dynlen(lhcVacLastAction) == 0)
    {
      fwException_raise(exceptionInfo,
        "ERROR", "Action Result: the last action (if any) was not group action", "");
      return LVA_FAILURE;
    }
    fileName = "vision/dialogs/ActResult.pnl";
    panelNameBase = "ActionResult";
    manyAllowed = false;
    break;
  case LVA_OPEN_ALERTS:
    fileName = "vision/dialogs/Alerts.pnl";
    panelNameBase = "Alerts";
    manyAllowed = false;
    break;
  case LVA_OPEN_SBS_ARCHIVE_CONFIG:
    fileName = "vision/archiving/archiveSelect.pnl";
    panelNameBase = "Fast Log Archive Config";
    manyAllowed = false;
    break;
  case LVA_OPEN_DPE_ARCHIVE_CONFIG:
    fileName = "vision/archiving/archiveDPE.pnl";
    panelNameBase = "archiveDPE";
    manyAllowed = false;
    break;		
  case LVA_OPEN_SBS_SESSION_MANAGER:
    fileName = "vision/archiving/FastLogSessions.pnl";
    panelNameBase = "Fast Log Session Manager";
    manyAllowed = false;
    break;
  case LVA_OPEN_SBS_SESSION_EXPORT:
    // TODO
    break;
  case LVA_OPEN_SBS_SESSION_TREND:
    // TODO
    break;
  case LVA_OPEN_DETAIL_PANEL:
    LhcVacGetDevicePanel(dpName, dpType, useDpName, useDpType, fileName, panelNameBase, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return LVA_FAILURE;
    }
    manyAllowed = false;
    dollars = makeDynString("$dpName:" + useDpName, "$slaveDpName:" + dpName);
    LhcVacDisplayName(dpSubStr(useDpName, DPSUB_DP), panelNameBase, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return LVA_FAILURE;
    }
    switch(act[2])
    {
    case DIALOG_MODE_REPLAY:
      panelNameBase += "_Replay";
      break;
    }
    break;
  case LVA_OPEN_CONTROLLER_PANEL:
    LhcVacGetControllerPanel(dpName, dpType, useDpName, useDpType, fileName, panelNameBase, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return LVA_FAILURE;
    }
	manyAllowed = false;
    dollars = makeDynString("$dpName:" + useDpName, "$slaveDpName:" + dpName);
    LhcVacDisplayName(dpSubStr(useDpName, DPSUB_DP), panelNameBase, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return LVA_FAILURE;
    }
    switch(act[2])
    {
    case DIALOG_MODE_REPLAY:
      panelNameBase += "_Replay";
      break;
    }
    break;
  case LVA_OPEN_VP_IP_CONTROLLER:
    LhcVacEqpMaster(dpName, masterDp, channel);
    masterDpType = dpTypeName(masterDp);    
    LhcVacGetControllerPanel(masterDp, masterDpType, useDpName, useDpType, fileName, panelNameBase, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return LVA_FAILURE;
    }
	manyAllowed = false;
    dollars = makeDynString("$dpName:" + useDpName, "$slaveDpName:" + dpName);
    LhcVacDisplayName(dpSubStr(useDpName, DPSUB_DP), panelNameBase, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return LVA_FAILURE;
    }
    switch(act[2])
    {
    case DIALOG_MODE_REPLAY:
      panelNameBase += "_Replay";
      break;
    }
    break;    
   case LVA_OPEN_CONFIG_PANEL:
    LhcVacEqpConfig(dpName, configName, configRef, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return LVA_FAILURE;
    }
    //---- IF THERE IS NO CONFIG_TPG CONTROL TYPE, USE VR_GT CONTROL TYPE ----
	if(configName == "")
    {
	  string masterName;
	  LhcVacGetAttribute(dpName, "MasterName", masterName);
	  if(masterName == "")
	  {
	    fwException_raise(exceptionInfo, "ERROR", "LvaDialogAction: no master for <" + dpName + ">", "");
	    return LVA_FAILURE;
	  }
	  dyn_string resultDpNames;
	  LhcVacGetByAttrValue("MasterName", masterName, resultDpNames);
	  string resultDpType;
	  bool isVR_GT = FALSE;
	  for(int index = dynlen(resultDpNames) ; index > 0 ; index--)
	  {	
	    LhcVacDpType(resultDpNames[index], resultDpType, exceptionInfo);
		if (resultDpType == "VR_GT")	
		{
		  isVR_GT = TRUE;
		  LhcVacGetDevicePanel(resultDpNames[index], resultDpType, useDpName, useDpType, fileName, panelNameBase, exceptionInfo);
		  if(dynlen(exceptionInfo) > 0)
		  {
		    fwException_raise(exceptionInfo, "ERROR", "LvaDialogAction: no panel found for VR_GT <" + resultDpName + ">", "of the gauge  <" + dpName + ">", "");
		  	return LVA_FAILURE;
		  }
		  LhcVacDisplayName(dpSubStr(useDpName, DPSUB_DP), panelNameBase, exceptionInfo);
		  if(dynlen(exceptionInfo) > 0)
		  {
			fwException_raise(exceptionInfo, "ERROR", "LvaDialogAction: no display name found for <" + useDpName + ">", "");
			return LVA_FAILURE;
		  }
		  dollars = makeDynString("$dpName:" + resultDpNames[index], "$slaveDpName:" + dpName);
		  manyAllowed = false;
		}
	  }
	  if (isVR_GT == FALSE)	
	  {
		fwException_raise(exceptionInfo, "ERROR", "LvaDialogAction: no VR_GT found for <" + dpName + ">", "");
		return LVA_FAILURE;
	  }
    }
	else
	{
	  LhcVacGetDevicePanel(configName, dpType, useDpName, useDpType, fileName, panelNameBase, exceptionInfo);
	  if(dynlen(exceptionInfo) > 0)
	  {
	    return LVA_FAILURE;
	  }
	  manyAllowed = false;
	  dollars = makeDynString("$dpName:" + configName, "$slaveDpName:" + dpName);
	  LhcVacEqpMaster(dpName, configName, configRef);
	  LhcVacDisplayName(configName, panelNameBase, exceptionInfo);
	  if(dynlen(exceptionInfo) > 0)
	  {
	    return LVA_FAILURE;
	  }
	  // TPG300 may be controlled by VR_GT instead of TPG_CONFIG
	  {
	    dyn_string refDpNames;
		LhcVacGetByAttrValue("MasterName", configName, refDpNames);
		for(int refIdx = dynlen(refDpNames) ; refIdx > 0 ; refIdx--)
		{
		  // DebugTN("Check " + refDpNames[refIdx]);
		  int family, type, subType;
		  LhcVacGetCtlAttributes(refDpNames[refIdx], family, type, subType);
		  if(family == 6)
		  {
			// DebugTN("Found !!!");
			dpType = "";
			LhcVacGetDevicePanel(refDpNames[refIdx], dpType, useDpName, useDpType, fileName, panelNameBase, exceptionInfo);
			if(dynlen(exceptionInfo) > 0)
			{
			  return LVA_FAILURE;
			}
			LhcVacDisplayName(dpSubStr(useDpName, DPSUB_DP), panelNameBase, exceptionInfo);
			if(dynlen(exceptionInfo) > 0)
			{
			  return LVA_FAILURE;
			}
			dollars = makeDynString("$dpName:" + refDpNames[refIdx], "$slaveDpName:" + dpName);
		  }
		}
	  }
	}
    break;
  case LVA_OPEN_PR_HISTORY:
    return LhcVacAddDpToHistory(dpName, exceptionInfo);
    break;
  case LVA_OPEN_NV_HISTORY:
  {
	string dpeName;
	string yAxis;
	string dpType = dpTypeName(dpName);
	if (dpType == "VV_AO"){
		dpeName = "SetPointR";
		yAxis = "Percentage";
	} else if(dpType == "VGTR_C0"){
		dpeName = "PR";
		yAxis = "HighPressure";
	} else{
		return LVA_OK;
		break;
	}
	int depth = GetDpeHistoryDepth(dpName + "." + dpeName, exceptionInfo);
	return  depth == 0 ? 0 : LhcVacAddDpeToHistoryDialog(dpName, dpeName, yAxis, depth, exceptionInfo);
    break;
  }
  case LVA_OPEN_SETPOINT_HISTORY:
    return LhcVacAddDpToHistory(dpName, exceptionInfo);
    break;
  case LVA_OPEN_BIT_HISTORY:
    return LhcVacAddDpToBitHistory(dpName, exceptionInfo);
    break;
  case LVA_OPEN_STATE_DETAILS_HISTORY:
    LhcVacAddDpToStateHistory(makeDynString(dpName), true, exceptionInfo);
    return(dynlen(exceptionInfo) > 0 ? LVA_FAILURE : LVA_OK);
  case LVA_OPEN_CREATE_UI_DP:
    fileName = "vision/admin/createUIMInternalDP.pnl";
    panelNameBase = "Create DP for UI";
    manyAllowed = false;
    break;
  case LVA_OPEN_SYSTEM_INTEGRITY:
    fileName = "vision/systemIntegrity/systemIntegrityConfiguration.pnl";
    panelNameBase = "System integrity configuration";
    manyAllowed = false;
    break;
  case LVA_OPEN_GLOBAL_ACTION:
    fileName = "vision/dialogs/GroupActionNew.pnl";
    panelNameBase = "Global Action";
    manyAllowed = false;
    break;
  case LVA_OPEN_ACCESS_ZONE:
    fileName = "vision/dialogs/AccessZone.pnl";
    panelNameBase = "Access Zone";
    manyAllowed = false;
        break;
  case LVA_OPEN_BAKEOUT_HISTORY:
    // TODO
    break;
  case LVA_OPEN_VPGM_SEARCH:
    // TODO
    break;
  case LVA_OPEN_ALARM_EMAIL_CONFIG:
    fileName = "vision/admin/emailAlarmConfig.pnl";
    panelNameBase = "Alarm E-mail Configuration";
    manyAllowed = false;
    break;
  case LVA_OPEN_ALARM_MASK_CONFIG:
    fileName = "vision/admin/alarmMaskConfig.pnl";
    panelNameBase = "Alarm Mask Configuration";
    manyAllowed = false;
    break;
  case LVA_OPEN_EQP_OSC_FILTER:
    fileName = "vision/admin/eqpOscFilter.pnl";
    panelNameBase = "Filter Equipment oscillations";
    manyAllowed = false;
    break;
  case LVA_OPEN_BAKEOUT_DETAILS:
    qtDialogType = QT_DIALOG_BAKEOUT_RACK_DETAILS;
    break;
  case LVA_OPEN_BAKEOUT_RACK_PROFILE:
    qtDialogType = QT_DIALOG_BAKEOUT_RACK_PROFILE;
    break;
  case LVA_OPEN_MOBILE_EQP:
    qtDialogType = QT_DIALOG_MOBILE_HISTORY;
    break;
  case LVA_OPEN_EXP_AREA_DETAILS:
    if(LhcVacDecideHowToOpenExpAreaDetails(dpName, sectList, qtDialogType, fileName, panelNameBase, manyAllowed, exceptionInfo) != LVA_OK)
    {
      return LVA_FAILURE;
    }
    break;
  case LVA_OPEN_COMMENT:
    fileName = "vision/dialogs/DevComment.pnl";
    panelNameBase = "Comment for '" + dpSubStr(dpName, DPSUB_DP) + "'";
    dollars = makeDynString("$dpName:" + dpName);
    manyAllowed = false;
    break;
  case LVA_OPEN_ALL_LHC_PROFILE:
    qtDialogType = QT_DIALOG_PROFILE_ALL_LHC_IN_OUT;
    break;
  case LVA_OPEN_ALL_LHC_PROFILE_BEAM_TYPE:
    qtDialogType = QT_DIALOG_PROFILE_ALL_LHC_BEAM;
    break;
  case LVA_OPEN_ALL_LHC_SYNOPTIC:
    qtDialogType = QT_DIALOG_SYNOPTIC_ALL_LHC;
    break;
  case LVA_OPEN_ACCESS_MODE:
    fileName = "vision/dialogs/AccessMode.pnl";
    panelNameBase = "Set Access Mode";
    manyAllowed = true;
    break;	
  case LVA_OPEN_ACCESS_ZONE:
    fileName = "";
    panelNameBase = "Set Access Zone";
    manyAllowed = false;
    break;
   case LVA_OPEN_SMS_GENERAL:
    fileName = "vision/sms/SmsConfigList.pnl";
    panelNameBase = "Genric SMS Configuration";
    manyAllowed = false;
    break;
   case LVA_OPEN_NOTIFICATION:
    fileName = "vision/dialogs/vacPanelNotifications.pnl";
    panelNameBase = "Set-up of Group Notifications";
    manyAllowed = false;
    break; 
   case LVA_OPEN_VACOK_SMS_CONFIG:
    fileName = "vision/admin/vacOkSmsConfig.pnl";
    panelNameBase = "VacOK SMS Configuration";
    manyAllowed = false;
    break;
  case LVA_OPEN_VALVE_INTRLCK_CONFIG:
    fileName = "vision/admin/valveInterlockEmailConfig.pnl";
    panelNameBase = "Valves Interlock Notification Configuration";
    manyAllowed = false;
    break;
  case LVA_OPEN_BEAM_MODE_EMAIL_CONFIG:
    fileName = "vision/admin/BeamModeEmailConfig.pnl";
    panelNameBase = "Beam Mode Inconsistencies Report Configuration";
    manyAllowed = false;
    break;
  case LVA_OPEN_VPG_SERVICE:
    fileName = "vision/dialogs/TmpHistoryQuery.pnl";
    panelNameBase = "Pumping Groups Service Statistic";
    manyAllowed = false;
    break;
  case LVA_OPEN_VVS_SERVICE:
    fileName = "vision/dialogs/ValveHistoryQuery.pnl";
    panelNameBase = "Valve Service Statistic";
    manyAllowed = false;
    break;
   case LVA_OPEN_VVS_INTERLOCKS:
    fileName = "vision/dialogs/ValveInterlockList.pnl";
    panelNameBase = "Valve Interlock List";
    manyAllowed = false;
    break;
   case LVA_OPEN_VVS_BRIDGES:
    fileName = "vision/admin/ValveBridgeSearch.pnl";
    panelNameBase = "Valve Interlock Bridge search";
    manyAllowed = false;
    break;
   case LVA_OPEN_VVS_MONITORING:
    fileName = "vision/dialogs/valveMonitoring.pnl";
    panelNameBase = "Valve Monitoring";
    manyAllowed = false;
	break;
   case  LVA_OPEN_BEAM_PARAM_CONFIG:  
    fileName = "vision/admin/BeamParameterScriptConfig.pnl";
    panelNameBase = "Configuration of Beam Parameters Calculation Script";
    manyAllowed = false;
    break;
  case LVA_OPEN_VVS_ZONE_MAP:
    fileName = "vision/dialogs/ValvesOfAccessZones.pnl";
    panelNameBase = "Mapping of Valves to Access Zones";
    manyAllowed = false;
    break;
  case LVA_OPEN_TEMP_INTERLOCK:
    fileName = "vision/admin/TempLimitInterlock.pnl";
    panelNameBase = "Set Temperature Interlock Limit";
    manyAllowed = false;
    break;
  case LVA_OPEN_THERM_LIST:
    fileName = "vision/dialogs/ThermometerList.pnl";
    panelNameBase = "Cryo Thermometer List";
    manyAllowed = false;
    break;
  case LVA_OPEN_PREDEFINED_COMMENTS:
    fileName = "vision/admin/DevCommentEditor.pnl";
    panelNameBase = "Predefined Comment List";
    manyAllowed = false;
    break;
  case LVA_OPEN_VGP_EMAIL_CONFIG:
    fileName = "vision/admin/emailVgpAlarmConfig.pnl";
    panelNameBase = "VGP and T alarm configuration";
    manyAllowed = false;
    break;
  case LVA_OPEN_COMMENT_SEARCH:
    fileName = "vision/dialogs/DevCommentSearch.pnl";
    panelNameBase = "Search Device Comments";
    manyAllowed = false;
    break;
  case LVA_OPEN_ACCESS_CONTROL:
    fileName = "vision/dialogs/vcAccessControl.pnl";
    panelNameBase = "Access Control";
    manyAllowed = false;
    break;
  case LVA_OPEN_BEAM_PARAM:
    fileName = "vision/dialogs/BeamParamTable.pnl";
    panelNameBase = "Beam Parameters";
    manyAllowed = false;
    break;
  case LVA_OPEN_BEAM_PARAM_SPS:
    fileName = "vision/dialogs/BeamParamTableSPS.pnl";
	panelNameBase = "Beam Parameters";
	manyAllowed = false;
	break;
  case LVA_OPEN_SOLENOID_TABLE:
    fileName = "vision/dialogs/SolenoidTable.pnl";
    panelNameBase = "Solenoid Table";
    manyAllowed = false;
    resizeMode = "Scale";  // !!!!!!!
    break;
 case LVA_OPEN_HISTORY_CONFIG:
    fileName = "vision/dialogs/HistoryProfile.pnl";
    panelNameBase = "History Config";
    if(dynlen(act) == 4 )
    {
      dollars = makeDynString("$load:" + act[4] , "$historyId:" + act[3]);
    }
    else
    {
      dollars = makeDynString("$load:" + true, "$historyId:-1");
    }
    manyAllowed = false;
    break;
  case LVA_OPEN_MACHINE_MODE:
    fileName = "vision/admin/MachineMode.pnl";
    panelNameBase = "Machine mode (for SMS)";
    manyAllowed = false;
    break;
  case LVA_OPEN_ACTION_PRIVELEGE:
    fileName = "vision/admin/ActionPrivList.pnl";
    panelNameBase = "Privelege required to execute Actions";
    manyAllowed = false;
    break;
   case LVA_OPEN_VVS_INTERLOCKS_SPS:
    fileName = "vision/dialogs/ValveInterlockListSps.pnl";
    panelNameBase = "SPS Valve Interlock List";
    manyAllowed = false;
    break;
   case LVA_OPEN_MON_PROFILE_SPS:
    fileName = "vision/overview/VacMonSPS_Intern.pnl";
    panelNameBase = "SPS Monitor Profile";
    manyAllowed = false;
    break;
   case LVA_OPEN_MON_PROFILE_PS:
    fileName = "vision/overview/VacMonPS_Intern.pnl";
    panelNameBase = "PS Monitor Profile";
    manyAllowed = false;
    dollars = makeDynString("$dataPart:PS");
    break;
   case LVA_OPEN_MON_PROFILE_LHC_BEAM:
    fileName = "vision/overview/LhcMonProfileBeamIntern.pnl";
    panelNameBase = "LHC Monitor Beam Profile";
    manyAllowed = false;
    break;
   case LVA_OPEN_MON_PROFILE_LHC_ISO:
    fileName = "vision/overview/LhcMonProfileIsoIntern.pnl";
    panelNameBase = "LHC Monitor Isolation Profile";
    manyAllowed = false;
    break;
   case LVA_OPEN_MON_PROFILE_LHC_CRYO:
    fileName = "vision/overview/VacMonLhcSynopticCryo.pnl";
    panelNameBase = "LHC Monitor Cryo Profile";
    manyAllowed = false;
    break;    
   case LVA_OPEN_ARCHIVE_HISTORY:
    fileName = "vision/admin/ArchiveHistory.pnl";
    panelNameBase = "LHC Archive history";
    manyAllowed = false;
    break;    
   case LVA_OPEN_HIDE_EQUIPMENT:
    fileName = "vision/admin/HideDeviceCriteria.pnl";
    panelNameBase = "Hide/show device list";
    manyAllowed = false;
    break;    
   case LVA_OPEN_FLANGE_LIST:
    fileName = "vision/dialogs/FlangesList.pnl";
    panelNameBase = "Flanges for Mobile";
    manyAllowed = false;
    break;
   case LVA_OPEN_DIGITAL_INPUT_MONITOR:
    fileName = "vision/eqpDetails/8DI_FE.pnl";
    panelNameBase = "Digital Input Monitor";
    manyAllowed = false;
    break;
  case LVA_OPEN_TPG300_HOT_START:
    fileName = "vision/admin/VR_GT_Params.pnl";
    panelNameBase = "TPG300 Params(VR_GT)";
    manyAllowed = false;
    break;
  case LVA_OPEN_SIMCARD:
    fileName = "vision/dialogs/MobileSimCardsList.pnl";
    panelNameBase = "Mobile SIM Cards";
    manyAllowed = false;
    break;
  case LVA_OPEN_VPLC_PF_CONFIG:
    fileName = "vision/admin/vacPanelConfigVPLC_PF.pnl";
    panelNameBase = "Config VPG Fixed PLCs";
    manyAllowed = false;
    break;
  case LVA_OPEN_DIGITAL_OUTPUT_ALARM:
    fileName = "vision/dialogs/alarmDo.pnl";
    panelNameBase = "Digital Output Alarm Generator";
    manyAllowed = false;
    break;
  case LVA_OPEN_ARCHIVE_SESSION:
    fileName = "vision/archiving/archiveSession.pnl";
    panelNameBase = "Archive session";
    manyAllowed = false;
    break;
  case LVA_OPEN_EQP_HELP:
   {
		string dpType = dpTypeName(dpName);
		if (dpType == "VVS_LHC"){
			fileName = "vision/eqpDetails/eqpHelp/VVS_LHC.pnl";
			panelNameBase = "Help for '" + dpSubStr(dpName, DPSUB_DP) + "' device";
			dollars = makeDynString("$dpName:" + dpName);
			manyAllowed = true;
		}else if (dpType == "VPI"){
			fileName = "vision/eqpDetails/eqpHelp/VPI.pnl";
			panelNameBase = "Help for '" + dpSubStr(dpName, DPSUB_DP) + "' device";
			dollars = makeDynString("$dpName:" + dpName);
			manyAllowed = true;
		}else if (dpType == "VGR_T"){
			fileName = "vision/eqpDetails/eqpHelp/VGR_T.pnl";
			panelNameBase = "Help for '" + dpSubStr(dpName, DPSUB_DP) + "' device";
			dollars = makeDynString("$dpName:" + dpName);
			manyAllowed = true;
		}
		else if (dpType == "VGP_T"){
			fileName = "vision/eqpDetails/eqpHelp/VGP_T.pnl";
			panelNameBase = "Help for '" + dpSubStr(dpName, DPSUB_DP) + "' device";
			dollars = makeDynString("$dpName:" + dpName);
			manyAllowed = true;
		}
	break;
   }
  case LVA_OPEN_WORKORDER_CONFIGURATION:
    {
      fileName = "vision/dialogs/WorkOrderServiceConfiguration.pnl";
      panelNameBase = "Work Order Service Configuration";
      break;
    }
  case LVA_DEVICE_WORKORDER:
    {
      
      if (dpExists("WorkOrderService")) {
        //Check mode for inforEAM WO creation (specific or generic equipment based)
        bool genericEquipmentOnly;
        dpGet("WorkOrderService.GenericEquipmentOnly",genericEquipmentOnly);
        if(!genericEquipmentOnly) fileName = "vision/dialogs/WorkOrderCreate.pnl";
        else fileName = "vision/dialogs/WorkOrderCreateGenEqp.pnl";
        panelNameBase = "Work Order Creation for '" + dpSubStr(dpName, DPSUB_DP) + "' device";
        dollars = makeDynString("$dpName:" + dpName);
      }
      break;
    }
	case LVA_DEVICE_RACK:
   {
		qtDialogType = QT_DIALOG_RACKS;
		manyAllowed = true;
		break;
   }
   case LVA_OPEN_INTERLOCKS:
   {
		qtDialogType = QT_DIALOG_INTERLOCKS;
		manyAllowed = true;
		break;
   }
  case LVA_ALERT_ACKNOWLEDGE:
    dpSetWait(dpSubStr(dpName, DPSUB_DP) +".AL1:_alert_hdl.._ack", DPATTR_ACKTYPE_MULTIPLE);      
    return LVA_OK;
  case LVA_OPEN_ALARM_BOOLEANCALC:
    fileName = "vision/alarms/alarmConfiguration.pnl";
    panelNameBase = "Alarm Configuration";
    break;
  default:
    fwException_raise(exceptionInfo,
      "ERROR", "LvaDialogAction: Unsupported action " + act[1], "");
    return LVA_FAILURE;
  }

  string panelName;
  string moduleName;
  if(qtDialogType == 0)  // Usual PVSS panel
  {
    string moduleNameBase = panelNameBase;
    if(concurrentPanel != "")
    {
      if(isModuleOpen(concurrentPanel))
      {
        int coco; 
        restorePanel(concurrentPanel, concurrentPanel);
        coco = stayOnTop(TRUE, concurrentPanel);  
        coco = stayOnTop(FALSE, concurrentPanel);
        fwException_raise(exceptionInfo, "ERROR", "Panel can not opened together with " + concurrentPanel, "");
        return LVA_FAILURE;
      }
    }
    // If many panels are not allowed and panel is already opened - raise it on top???
    if(!manyAllowed)
    {
      if(isModuleOpen(moduleNameBase))
      {
        int coco; 
        restorePanel(panelNameBase, moduleNameBase);
        coco = stayOnTop(TRUE, moduleNameBase);  
        coco = stayOnTop(FALSE, moduleNameBase);
        return LVA_OK;
     }
      /*
      if(isPanelOpen(panelNameBase))
      {
        // Raise panel !!!!!!!!!!!! LIK
        restorePanel(panelNameBase);
        return LVA_OK;
      }
      */
      panelName = panelNameBase;
      moduleName = moduleNameBase;
    }
    else
    {
      int index;
      for(index = 1 ; index < 10 ; index++)
      {
        panelName = panelNameBase + index;
        moduleName = moduleNameBase + index;
        if(!isModuleOpen(moduleName))
        {
          break;
        }
        /*
        if(!isPanelOpen(panelName))
        {
          break;
        }
        */
      }
      if(index >= 10)	// Too many...
      {
        fwException_raise(exceptionInfo,
          "ERROR", "LvaDialogAction: Too many panels " + panelNameBase + " have been opened", "");
        return LVA_FAILURE;
      }
    }
  }

  // Save current selection
  lhcVacCurSectList = sectList;
  lhcVacCurVacType = vacType;

  if(dynlen(dollars) == 0)
  {
    dollars = makeDynString("$mode:" + act[2]);
  }
  else
  {
    dynAppend(dollars, "$mode:" + act[2]);
  }

  // If dialog is opened in replay mode make sure replay control panel is also opened
  if(act[2] == DIALOG_MODE_REPLAY)
  {
    string	replayControlPanel = "ReplayControl";
    if(!isPanelOpen(replayControlPanel))
    {
      string module = "ReplayModule";
      ModuleOnWithPanel(module, 0, 0, 0, 0, 1, 1, resizeMode,
        "vision/dialogs/ReplayControl.pnl", replayControlPanel, makeDynString());
      bool bFlag = true;
      stayOnTop(bFlag, module);
    }
  }

  // Finally open the child panel
  if(qtDialogType == 0)  // Usual PVSS panel
  {
    ModuleOnWithPanel(moduleName, -1, -1, 0, 0, 0, 0, resizeMode, fileName, panelName, dollars);
    int coco; 
    restorePanel(panelName, moduleName);
    coco = stayOnTop(TRUE, moduleName);  
    coco = stayOnTop(FALSE, moduleName);
    
    
    /*
    ChildPanelOnParent(fileName, panelName, rootPanel(), dollars, 0, 0);
    */
  }
  else  // Qt dialog
  {
    if(qtDialogType == QT_DIALOG_RACKS || qtDialogType == QT_DIALOG_INTERLOCKS)
  	{
	  string displayName;
		LhcVacDisplayName(dpName, displayName, exceptionInfo);     
	 
	  if(qtDialogType == QT_DIALOG_RACKS)
	  {
		string rackControlParent;  
		int coco = LhcVacGetAttribute(dpName, "RackControlParent", rackControlParent); 
				
		if(rackControlParent == "")
		{
		   fwException_raise(exceptionInfo,
		  "ERROR", "Rack control parent attribute is not defined for " + displayName, "");
		  return LVA_FAILURE;
		}
	  }		

	  LhcVacOpenDocumentationDialog(qtDialogType, vacType, displayName, act[2], exceptionInfo);
  	}
	else
	{
		if(dynlen(sectList) > 0)
		{
		  return LhcVacOpenDialog(qtDialogType, vacType, sectList[1],
			sectList[dynlen(sectList)], act[2], exceptionInfo);
		}
		else
		{
		  return LhcVacOpenDialog(qtDialogType, vacType, "", "", act[2], exceptionInfo);
		}
	}
  }
  return LVA_OK;
}

// LvaDialogAction
/** Execute 'dialog' action (open/raise...) based on equipment name

Usage: LHC Vacuum control internal

PVSS manager usage: VISION

  @param act: in, action to be executed
    1 = action (bit mask, see above) [unsigned]
    2 = mode (online/replay) [int]
    3 = historyId, for history profile dialog [int]
    4 = load, dialog mode (safe/load)  for history config dialog [bool]
  @param sectList: in, list of selected sectors.
  @param dpName: in, Name of selected DP
  @param vacType: in, vacuum type selection bit mask
  @param exceptionInfo: out, details of any exceptions are returned here

  @return None.
  @author L.Kopylov
*/
int LvaDocumentationDialogAction(dyn_anytype act, const string eqpName, dyn_string &exceptionInfo)
{
  if(dynlen(act) < 2)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LvaDocumentationDialogAction: invalid act argument", "");
    return LVA_FAILURE;
  }
   string panelNameBase, fileName, resizeMode = "None";
   dyn_string dollars;
   bool manyAllowed = true;
   switch(act[1])
   {
		case LVA_DEVICE_WORKORDER:
			
			if (dpExists("WorkOrderService"))
			{
				//Check mode for inforEAM WO creation (specific or generic equipment based)
				bool genericEquipmentOnly;
				dpGet("WorkOrderService.GenericEquipmentOnly",genericEquipmentOnly);
				if(!genericEquipmentOnly) fileName = "vision/dialogs/WorkOrderCreate.pnl";
				else fileName = "vision/dialogs/WorkOrderCreateGenEqp.pnl";
				panelNameBase = "Work Order Creation for '" + eqpName + "' device";
				dollars = makeDynString("$eqpName:" + eqpName);
			}
			else
			{
				fwException_raise(exceptionInfo,
				"ERROR", "LvaDialogAction: WorkOrderService dp not found - " + act[1], "");
				return LVA_FAILURE;
			
			}
			break;
		default:
		    fwException_raise(exceptionInfo,
			"ERROR", "LvaDialogAction: Unsupported action " + act[1], "");
			return LVA_FAILURE;
		
   }
   string panelName;
  string moduleName;
    string moduleNameBase = panelNameBase;
    // If many panels are not allowed and panel is already opened - raise it on top???
    if(!manyAllowed)
    {
      if(isModuleOpen(moduleNameBase))
      {
        int coco; 
        restorePanel(panelNameBase, moduleNameBase);
        coco = stayOnTop(TRUE, moduleNameBase);  
        coco = stayOnTop(FALSE, moduleNameBase);
        return LVA_OK;
     }
      /*
      if(isPanelOpen(panelNameBase))
      {
        // Raise panel !!!!!!!!!!!! LIK
        restorePanel(panelNameBase);
        return LVA_OK;
      }
      */
      panelName = panelNameBase;
      moduleName = moduleNameBase;
    }
    else
    {
      int index;
      for(index = 1 ; index < 10 ; index++)
      {
        panelName = panelNameBase + index;
        moduleName = moduleNameBase + index;
        if(!isModuleOpen(moduleName))
        {
          break;
        }
        /*
        if(!isPanelOpen(panelName))
        {
          break;
        }
        */
      }
      if(index >= 10)	// Too many...
      {
        fwException_raise(exceptionInfo,
          "ERROR", "LvaDialogAction: Too many panels " + panelNameBase + " have been opened", "");
        return LVA_FAILURE;
      }
    }
  
	ModuleOnWithPanel(moduleName, -1, -1, 0, 0, 0, 0, resizeMode, fileName, panelName, dollars);
    int coco; 
    restorePanel(panelName, moduleName);
    coco = stayOnTop(TRUE, moduleName);  
    coco = stayOnTop(FALSE, moduleName);
    return LVA_OK;
}


// Check if special synoptic dialog shall be shown for given sector selection
string LhcVacGetSpecSynoptic(const dyn_string sectList)
{
  if(dynlen(sectList) == 0)
  {
    return "";
  }
  // Search for CLEX main part
  int mpIdx;
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if(LhcVacIsSectorInMainPart(sectList[n], "CLEX") != 0)
    {
      return "vision/synoptics/SynopticCLEX.xml";
    }
  }

  // Search for DE main part
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if(LhcVacIsSectorInMainPart(sectList[n], "DE") != 0)
    {
      return "vision/synoptics/SynopticDE.pnl";
    }
  }

  // Check if list contains ITE or LT20 sectors
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if((sectList[n] == "ITE") || (sectList[n] == "LT20"))
    {
      return "vision/synoptics/SynopticBTL.pnl";
    }
  }

  // Check if list contains both BI10 and BT10 sectors
  int nMatches = 0;
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if(sectList[n] == "BI10")
    {
      nMatches++;
    }
    if(sectList[n] == "BT10")
    {
      nMatches++;
    }
  }
  if(nMatches > 1)
  {
    return "vision/synoptics/SynopticBI_BT.pnl";
  }

  // Synoptic for GIS and similar in LHC
  if(sectList[1] == "BGI_5L4")
  {
    return "vision/synoptics/Synoptic_GIS_BGI_5L4.pnl";
  }
  else if(sectList[1] == "BGI_5R4")
  {
    return "vision/synoptics/Synoptic_GIS_BGI_5R4.pnl";
  }
  else if(sectList[1] == "GIS_IP1")
  {
    return "vision/synoptics//Synoptic_GIS_IP1.pnl";
  }
  else if(sectList[1] == "GIS_IP2")
  {
    return "vision/synoptics/Synoptic_GIS_IP2.pnl";
  }
  else if(sectList[1] == "GIS_IP5")
  {
    return "vision/synoptics/Synoptic_GIS_IP5.pnl";
  }
  else if(sectList[1] == "VELO")
  {
    return "vision/synoptics/SynopticVELO.pnl";
  }
  else if(sectList[1] == "VELO_GIS")
  {
    return "vision/synoptics/SynopticVELO_GIS.pnl";
  }
   else if(sectList[1] == "H2")
  {
    return "vision/synoptics/H2-H4.xml";
  }
   else if(sectList[1] == "H6")
  {
    return "vision/synoptics/H6-H8.xml";
  }
   else if(sectList[1] == "CNGS-Decaytube")
  {
    DebugN("CNGS Reqest detected");
    return "vision/synoptics/SynopticCNGS.xml";
  }

  // Check if list contains sectors of LINAC4
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if((sectList[n] == "L4L10") || (sectList[n] == "L4L20"))
    {
      return "vision/synoptics/SynopticLinac4.pnl";
    }
  }

  // Check if list contains sectors of LINAC
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if((sectList[n] == "LP") || (sectList[n] == "LI") || (sectList[n] == "LA"))
    {
      return "vision/synoptics/SynopticLinac2.pnl";
    }
  }
  
    // Check if list contains sectors of LINAC3
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if((sectList[n] == "IP") || (sectList[n] == "ITL") || (sectList[n] == "IAQ") || (sectList[n] == "ITM") || (sectList[n] == "IA") || (sectList[n] == "ITF") || (sectList[n] == "ITFE") || (sectList[n] == "ITFS"))
    {
      return "vision/synoptics/SynopticLinac3.xml";
    }
  }
  
  // Check if list contains sectors of ZoneNord
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if((sectList[n] == "803") || (sectList[n] == "804") || (sectList[n] == "801"))
    {
      return "vision/synoptics/SynopticZN.xml";
    }
  }
   
  // Search for NA62 main part
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if(LhcVacIsSectorInMainPart(sectList[n], "NA62") != 0)
    {
      return "vision/synoptics/SynopticNA62_S5-6-7.xml";
    }
  }
  // Search for EAR1 or EAR2 main part (nTOF)
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if( (LhcVacIsSectorInMainPart(sectList[n], "EAR1") != 0) ||
	    (LhcVacIsSectorInMainPart(sectList[n], "EAR2") != 0) )
    {
      return "vision/synoptics/SynopticNTOF.xml";
    }
  }
  // Search L4TS main part (Linac4 Test Stand)
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if( (LhcVacIsSectorInMainPart(sectList[n], "Linac4TS") != 0) )
    {
      return "vision/synoptics/SynopticL4TS.xml";
    }
  }
  // Search for MOBILE_TEST main part (VACCO_TB2)
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if(LhcVacIsSectorInMainPart(sectList[n], "TEST_MOBILE") != 0) 
    {
      return "vision/synoptics/SynopticMobile.xml";
    }
  }
  // AWAKE
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if( (LhcVacIsSectorInMainPart(sectList[n], "AWAKE") != 0) )
    {
      return "vision/synoptics/Synoptic_AWAKE_1.xml";
    }
  }
  // Transfer lines of ELENA
  if(dynContains(sectList, "LNI-LNE") > 0)
  {
    return "vision/synoptics/SynopticELENA_LNT.xml";
  }
  // PSB injection sector
  if(dynContains(sectList, "BRI10") > 0)
  {
    return "vision/synoptics/SynopticBRI.xml";
  }
  return "";
}

// Check if special sector view dialog shall be shown for given sector selection
string LhcVacGetSpecSectView(const dyn_string sectList)
{
  if(dynlen(sectList) == 0)
  {
    return "";
  }
  // Check for CLEX main part
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if(LhcVacIsSectorInMainPart(sectList[n], "CLEX") != 0)
    {
      return "vision/synoptics/MainPartCLEX.pnl";
    }
  }

  // Check if list contains ITE or LT20 sectors
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if((sectList[n] == "ITE") || (sectList[n] == "LT20"))
    {
      return "vision/synoptics/MainPartBTL.pnl";
    }
  }

  // Check if list contains both BI10 and BT10 sectors
  int nMatches = 0;
  for(int n = dynlen(sectList) ; n > 0 ; n--)
  {
    if(sectList[n] == "BI10")
    {
      nMatches++;
    }
    if(sectList[n] == "BT10")
    {
      nMatches++;
    }
  }
  if(nMatches > 1)
  {
    return "vision/synoptics/MainPartBI_BT.pnl";
  }
  return "";
}

/**
 * Decide how details for experimantal area device shall be opened.
 * Usually details of experimental area is standalone panel with synoptic.
 * It can be opened in two majow ways:
 *  - as standalone panel
 *  - as manually prepared panel for synoptic
 * @param dpName The name of DP for experimental area
 * @param sectList List of sectors, to be modified by this function if result shall
 *                be opened as manually prepared synoptic
 * @param qtDialogType Will be set by this function to QT_DIALOG_SYNOPTIC if panel
 *                to be opened as manually prepared synoptic
 * @param fileName The file name for experimental area details will be returned in this variable
 * @param panelNameBase The base name for panel to be opened
 * @param manyAllowed true will be written to this variable if multiple panels are allowed
 * @param exceptionInfo Standard exception reporting variable
 * @return LVA_OK if processing was successful, otherwise LVA_FAILURE
 */
int LhcVacDecideHowToOpenExpAreaDetails(string dpName, dyn_string &sectList, int &qtDialogType,
                                        string &fileName, string &panelNameBase, bool &manyAllowed,
                                        dyn_string &exceptionInfo)
{
  string resourceName = "EXP_AREA." + dpName + ".ShowInSynoptic";
  bool showInSynoptic = VacResourcesGetValue(resourceName, VAC_RESOURCE_BOOL, false);
  if(showInSynoptic)
  {
    qtDialogType = QT_DIALOG_SYNOPTIC;
    panelNameBase = "Synoptic";
    manyAllowed = true;
    resourceName = "EXP_AREA." + dpName + ".SynSectors";
    string sectors = VacResourcesGetValue(resourceName, VAC_RESOURCE_STRING, "");
    if(sectors == "")
    {
      fwException_raise(exceptionInfo, "ERROR",
      "LhcVacDecideHowToOpenExpAreaDetails(): No value for resource " + resourceName, "");
      return LVA_FAILURE;
    }
    sectList = strsplit(sectors, ',');
    return LVA_OK;
  }
  resourceName = "EXP_AREA." + dpName + ".PanelFile";
  fileName = VacResourcesGetValue(resourceName, VAC_RESOURCE_STRING, "");
  if(fileName == "")
  {
    fwException_raise(exceptionInfo, "ERROR",
      "LhcVacDecideHowToOpenExpAreaDetails(): No value for resource " + resourceName, "");
    return LVA_FAILURE;
  }
  resourceName = "EXP_AREA." + dpName + ".PanelTitle";
  panelNameBase = VacResourcesGetValue(resourceName, VAC_RESOURCE_STRING, "");
  if(panelNameBase == "")
  {
    fwException_raise(exceptionInfo, "ERROR",
      "LhcVacDecideHowToOpenExpAreaDetails(): No value for resource " + resourceName, "");
    return LVA_FAILURE;
  }
  manyAllowed = false;
  return LVA_OK;
}

// Return last saved selection
void LhcVacGetLastSelection(int &vacType, dyn_string &sectList)
{
  vacType = lhcVacCurVacType;
  sectList = lhcVacCurSectList;
}

// Return new group action parameters
void LhcVacGetCurGroupAction(dyn_anytype &act, dyn_string &dpTypeList)
{
  act = lhcVacCurAction;
  dpTypeList = lhcVacCurActDpTypeList;
}

// Set extra equipment filters for global action
void LhcVacSetActEqpFilter(bool red, bool blue)
{
  lhcVacEqpFilterRed = red;
  lhcVacEqpFilterBlue = blue;
}

// Get extra equipment filters for global action
void LhcVacGetActEqpFilter(bool &red, bool &blue)
{
  if(glAccelerator == "LHC")
  {
    red = lhcVacEqpFilterRed;
    blue = lhcVacEqpFilterBlue;
  }
  else	// Only applicable to LHC machine
  {
    red = blue = true;
  }
}

// Save parameters of executed group action
void LhcVacSaveGroupActParam(dyn_anytype act, unsigned vacType, dyn_string sectList,
                             dyn_string dpTypeList, dyn_string dpNameList)
{
  dynClear(lhcVacLastAction);
  int listLen = dynlen(act);
  for(int n = 1 ; n <= listLen ; n++)
  {
    dynAppend(lhcVacLastAction, act[n]);
  }
  lhcVacLastVacType = vacType;
  listLen = dynlen(sectList);
  dynClear(lhcVacLastActSectList);
  for(int n = 1 ; n <= listLen ; n++)
  {
    string interm = sectList[n];
    dynAppend(lhcVacLastActSectList, interm);
  }
  listLen = dynlen(dpTypeList);
  dynClear(lhcVacLastActDpTypeList);
  for(int n = 1 ; n <= listLen ; n++)
  {
    string interm = dpTypeList[n];
    dynAppend(lhcVacLastActDpTypeList, interm);
  }
  listLen = dynlen(dpNameList);
  dynClear(lhcVacLastActDpList);
  for(int n = 1 ; n <= listLen ; n++)
  {
    string interm = dpNameList[n];
    dynAppend(lhcVacLastActDpList, interm);
  }
}

// Save equipment filters for last global action
void LhcVacSetGroupActFilters(bool red, bool blue)
{
  lhcVacLastEqpFilterRed = red;
  lhcVacLastEqpFilterBlue = blue;
}

// Get parameters of last executed group action
void LhcVacLastGroupActParam(dyn_anytype &act, unsigned &vacType, dyn_string &sectList, dyn_string &dpTypeList,
	dyn_string &dpNameList)
{
  act = lhcVacLastAction;
  vacType = lhcVacLastVacType;
  sectList = lhcVacLastActSectList;
  dpTypeList = lhcVacLastActDpTypeList;
  dpNameList = lhcVacLastActDpList;
}

// Save equipment filters for last global action
void LhcVacGetGroupActFilters(bool &red, bool &blue)
{
  red = lhcVacLastEqpFilterRed;
  blue = lhcVacLastEqpFilterBlue;
}

int LhcVacAddDpToHistory(string dpName, dyn_string &exceptionInfo)
{
  dyn_string valueDpes;
  LhcVacEqpValueDpes(dpName, valueDpes);
  if(dynlen(valueDpes) == 0)
  {
    string devName;
    LhcVacDisplayName(dpName, devName, exceptionInfo);
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacAddDpToHistory(): <" + devName + "> does not have value DPE(s)", "");
    return -1;
  }

  // Get history depth for DPE (just take 1st DPE from list)
  string dpe = valueDpes[1];

  int depth = GetDpeHistoryDepth(valueDpes[1], exceptionInfo);
  return  depth == 0 ? 0 : LhcVacAddToHistoryDialog(dpName, depth, exceptionInfo);
}

int GetDpeHistoryDepth(string dpe, dyn_string &exceptionInfo)
{
  time firstTime;
  VacEqpFirstArchiveTime(dpe, firstTime);
  return getCurrentTime() - firstTime;
}
  
int LhcVacAddDpToBitHistory(string dpName, dyn_string &exceptionInfo)
{
  dyn_string  dsReturn;
  dyn_float  dfReturn;
  ChildPanelOnCentralModalReturn ("vision/dialogs/BitSelectionDialog.pnl", "Bit Selection",
    makeDynString("$dpName:" + dpName), dfReturn, dsReturn);
  if(dynlen(dsReturn) < 4)
  {
    return 0;
  }
  dpName = dsReturn[1];  // BitSelectionDialog could change DP name, for example VPI -> VRPI
  string dpeName = dsReturn[2];
  int bitNbr = dsReturn[3];
  string bitName = dsReturn[4];

  // Get history depth for DPE
  string dpe = dpName + "." + dpeName;
  int depth = GetDpeHistoryDepth(dpe, exceptionInfo);
  return  depth == 0 ? 0 : LhcVacAddBitToHistoryDialog(dpName, dpeName, 
    bitNbr, bitName, depth, exceptionInfo);
}

// ******************************************************************
// Device slection in MMI: special internal DP to communicate
// name of recently selected device between opened panels
//
// Generate name of DP used for indication of active device
void LhcVacActiveDpName(string &dpName, dyn_string &exceptionInfo)
{
  dpName = "_VacDevSelect_" + myManNum();
  // Create DP if needed
  if(! dpExists(dpName))
  {
    dpCreate(dpName, "_VacDevSelect");
    dyn_errClass err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacActiveDpName(): Failed to create DP <" + dpName + ":" + err , "");
      return;	 
     }        
  }
}

// return name of DPE will be used for indication of selected device
void LhcVacGetAdpName(string &adpName, dyn_string &exceptionInfo)
{
  string			dpName;

  LhcVacActiveDpName(dpName, exceptionInfo);
  adpName = dpName + ".ADP";
}

// Set value for  "_VacDevSelect_N".
// Will be used for indication of selected device 
void LhcVacSetAdpValue(string dpName, dyn_string &exceptionInfo)
{
  string  adpName;
  LhcVacGetAdpName(adpName, exceptionInfo);

  dpSetWait(adpName, dpName);
  dyn_errClass err = getLastError();
  if (dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacSetAdpValue(): failed to set " + adpName + ": " + err , "");
    return;
  }	
}


// ******************************************************************
// special internal DP to save the names of Dps are used in state history
// Generate name of this DP

void LhcVacStateHistoryDpName(bool clearContent, string &dpName, dyn_string &exceptionInfo)
{
  dyn_errClass 	err;

  dpName ="_VacStateHistory_" + myManNum();

  // Create DP if needed
  if(! dpExists(dpName))
  {
    dpCreate(dpName, "_VacStateHistory");
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacStateHistoryDpName(): Failed to create DP <" + dpName + ">: " + err, "");
      return;
    }
  }
  if(clearContent)
  {
    dpSetWait(dpName + ".DP", makeDynString());
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacStateHistoryDpName(): Failed to clear content of <" + result + ">: " + err, "");
      return;
    }
  }
}

// Add given DPE to list of DPEs shown in state history
void LhcVacAddDpToStateHistory(dyn_string addDpList, bool hasToOpenDialog, dyn_string &exceptionInfo)
{
  string historyModule = "State_History";
  bool isOpen;
  if(hasToOpenDialog)
  {
    bool isOpen = isModuleOpen(historyModule);
    // clean dpList & DLL data pool according to state of dialog
    if(!isOpen)
    {
      LhcVacStateHistoryClearDpList(exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        return;
      }
    }
  }
  string historyDpName;
  LhcVacStateHistoryDpName(false, historyDpName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }

  // Get current content of history
  dyn_string dpList;
  dpGet(historyDpName + ".DP", dpList);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacAddDpToStateHistory(): failed to read current config from < " + historyDpName +
      " >:" + err, "");
    return;
  }
  int listLen = dynlen(addDpList);
  if(listLen == 0)
  {
    return;
  }
  for(int n =1; n <= listLen; n++)
  {
    // Check if DP is already in list
    string dpName = addDpList[n];
    if(dynContains(dpList, dpName) > 0)
    {
      continue;
    }
    dynAppend(dpList, dpName);
  }
	
  // Set new values for DP
  dpSetWait( historyDpName + ".DP", dpList);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacAddDpToStateHistory(): failed to write new config to < " + historyDpName + " >:" + err, "");
    return;
  }
	
  if(! isOpen && hasToOpenDialog)
  {
    dyn_string dollars = makeDynString("$fromYy:", "$fromMo:","$fromDd:","$fromHh:","$fromMi:","$fromSs:","$toYy:","$toMo:","$toDd:","$toHh:","$toMi:","$toSs:");
    ModuleOnWithPanel(historyModule, -1, -1, 0, 0, 0, 0, "None", "vision/dialogs/StateArchiveDetails.pnl", "State_History", dollars);
  }
}

// Return the DP name which shall be used to retrive state history
// from device DP name. For the time being the main
// purpose is to replace VPI with VRPI
string LhcVacDpNameForStateHistory(string dpName, dyn_string &exceptionInfo)
{
  string dialDpName, dialDpType, dpType;
  dpType = dpTypeName(dpName);
  LhcVacGetDpForAct(dpName, dpType, dialDpName, dialDpType, exceptionInfo);
  return dialDpName;
}

// Remove given DPE from list of DPEs shown in state history panel.
void LhcVacRemoveDpFromStateHistory(string dpName, dyn_string &exceptionInfo)
{
  string  historyDpName;
	
  LhcVacStateHistoryDpName(false, historyDpName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }

  // Get current content of trend
  dpGet(historyDpName + ".DP", dpList);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacRemoveDpFromStateHistory(): failed to read current config from < " + historyDpName + " >:" + err, "");
    return;
  }

  // Find DP in list
  int dpIndex = dynContains(dpList, dpName);
  if(dpIndex < 1)
  {
    return;		// Not found
  }

  // Correct lists, write back to DP
  dynRemove(dpList, dpIndex);
  dpSetWait( historyDpName + ".DP", dpList);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacRemoveDpFromStateHistory(): failed to write new config to < " + historyDpName + " >:" + err, "");
  }
}

// Remove all DPEs from list of DPEs shown in state history panel.
void LhcVacStateHistoryClearDpList(dyn_string &exceptionInfo)
{
  string historyDpName;
	
  LhcVacStateHistoryDpName(false, historyDpName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }

  dpSetWait(historyDpName + ".DP", makeDynString());
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacStateHistoryClearDpList(): Failed to clear content of <" + result + ">: " + err, "");
    return;
  }
}
//***********************************************************************
// History trend support: special internal DP to communicate
// DP name to history panel
// Variant #2 using DP with DPEs of type dyn_string and dyn_uint
//
//
// Generate name of DP used for trend parametrization
void LhcVacHistoryCtlDpName( bool clearContent, string &dpName, dyn_string &exceptionInfo )
{
	dyn_errClass 	err;

	dpName = "_VacHistoryCtl_" + myManNum();

	// Create DP if needed
	if( ! dpExists( dpName ) )
	{
 		dpCreate( dpName, "_VacHistoryCtl");
 		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacHistoryCtlDpName(): Failed to create DP <" + dpName + ">: " + err, "" );
			return;
		}
	}

	if( clearContent )
	{
		dpSetWait( dpName + ".DP:_original.._value", makeDynString( "" ),
			dpName + ".Order:_original.._value", makeDynUInt( 0 ) );
 		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacHistoryCtlDpName(): Failed to clear content of <" + result + ">: " + err, "" );
			return;
		}
	}
	return;
}
