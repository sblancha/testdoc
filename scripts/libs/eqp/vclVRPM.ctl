// Bitmasks for VRPM status bits (RR1 - 32 bits)
const unsigned // RR1
	VRPM_SPS_R_LOCKED = 0x80000000u,
	VRPM_SPS_R_VALID = 0x40000000u,
	VRPM_SPS_R_REMOTE = 0x20000000u,
	VRPM_SPS_R_AUTO = 0x8000000u,
	VRPM_SPS_R_ON_DIC = 0x02000000u,
	VRPM_SPS_R_ERRORS = 0x00800000u,
	VRPM_SPS_R_WARNINGS = 0x00400000u,
	VRPM_SPS_R_INTRERLOCK =  0x00200000u,
        VRPM_SPS_CB_ON =  0x00080000u,
        VRPM_SPS_CB_OFF =  0x00040000u;
const unsigned // RR2
        VRPM_SPS_R_ACF = 0x8000,
	VRPM_SPS_R_DCF = 0x4000u,
        VRPM_SPS_R_LIM = 0x2000,
	VRPM_SPS_R_OT = 0x1000u,
        VRPM_SPS_R_PSOL = 0x0800u,
	VRPM_SPS_R_CCS = 0x0400u,
	VRPM_SPS_R_RSD = 0x0200u;
        
const unsigned VRPM_SPS_W_REMOTE = 0x2000,
	VRPM_SPS_W_LOCAL = 0x1000u,
        VRPM_SPS_W_AUTO = 0x0800u,
	VRPM_SPS_W_ON = 0x0200u,
	VRPM_SPS_W_OFF = 0x0100u,
	VRPM_SPS_W_RSD_DOC = 0x0002u,
        VRPM_SPS_W_CB_ON = 0x0008u;
// The high and low threshold (current & voltage)  depending of ctrl type
// equipment
const int LOW_CURRENT_THRESHOLD_VRPMA = 0,
        LOW_CURRENT_THRESHOLD_VRPMB = 0,
        HIGH_CURRENT_THRESHOLD_VRPMA = 25000,
        HIGH_CURRENT_THRESHOLD_VRPMB = 20000,
        LOW_VOLTAGE_THRESHOLD_VRPMA = 0,
        LOW_VOLTAGE_THRESHOLD_VRPMB = 0,
        HIGH_VOLTAGE_THRESHOLD_VRPMA = 350000,
        HIGH_VOLTAGE_THRESHOLD_VRPMB = 200000;
        
//LhcVacActionPossible_VRPM:
/** Check if Solenoid power supply is in state that allows executing actions for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param dpType: in, DP type
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_VRPM(string dpName, string dpType, dyn_anytype act, bool &possible,
  dyn_string &exceptionInfo)
{
  dyn_errClass 	err;
  unsigned	state;
  string	dpNameAct, dpTypeAct;
		
  LhcVacGetDpForAct(dpName, dpType, dpNameAct, dpTypeAct, exceptionInfo);
  dpGet(dpNameAct + ".RR1", state);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacActionPossible_VRPM: Read RR1 for <" + dpNameAct + "> failed: " + err, "");
    possible = false;
    return;
  }
  if((state & VRPM_SPS_R_LOCKED) != 0)
  {
    possible = false;
  }
  else
  {
    possible = true;
  }
}
//LhcVacDeviceAction_VRPM:
/** Execute action for solenoid power supply. It has been checked before call that this DP type
	understands this action.

  Discussions with S.Blanchsrd on 07.02.2011 came to conclusion:
   whatever command is sent from PVSS - we must write all 3 DPEs: WR1 + WmA + WmV,
    and WR1 must contain REMOTE=1.
   The reason is: after reading and executing command PLC clears corresponding value
   in WR_DB, so when next command arrives - value in WR_DB may be zero.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VRPM(string dpName, string dpType, dyn_anytype action, dyn_string &exceptionInfo)
{
  unsigned	wr1;
  int       mA, mV;
  mapping   mConnectDpes;
  dpGet(dpName + ".WR1", wr1,
        dpName + ".WmA", mA,
        dpName + ".WmV", mV);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
     "ERROR", "LhcVacDeviceAction_VRPM(): dpGet(" + dpName + ") failed:" + err , "");
    return;
  }
  wr1 = VRPM_SPS_W_REMOTE;  // Set remote bit
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
    wr1 |= VRPM_SPS_W_ON;
    break;
  case LVA_DEVICE_OFF:
    wr1 |= VRPM_SPS_W_OFF;
    break;
  case LVA_DEVICE_AUTO:
    wr1 |= VRPM_SPS_W_AUTO;
    break;
  case LVA_DEVICE_VRPM_REMOTE_SHUTDOWN:
    wr1 |= VRPM_SPS_W_RSD_DOC;
    break;
  case LVA_DEVICE_VRPM_SET_CURRENT:
    mA = action[2];
    break;
  case LVA_DEVICE_VRPM_SET_VOLTAGE:
    mV = action[2];
    break;
  case LVA_DEVICE_VRPM_CB_ON:
    wr1 = VRPM_SPS_W_CB_ON;  // Remote bit is not needed for this command !!!
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["RmA"] = NO_VALUE_CHANGE;
    mConnectDpes["RmV"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WmA"] = NO_VALUE_CHANGE;
    mConnectDpes["WmV"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["RmA"] = NO_VALUE_CHANGE;
    mConnectDpes["RmV"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WmA"] = NO_VALUE_CHANGE;
    mConnectDpes["WmV"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VRPM(" + dpName + "):  unknown action " + act, "");
    return;
  }

  // Check if action is possible
  bool possible;
  LhcVacActionPossible_VRPM(dpName, dpType, act, possible, exceptionInfo);
  if(dynlen(exceptionInfo) != 0)
  {
     return;
  }
  if(!possible)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VRPM(" + dpName + "): Action forbidden", "");
    return;
  }

  // Execute action
  dpSetWait(dpName + ".WR1", wr1,
        dpName + ".WmA", mA,
        dpName + ".WmV", mV);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
     "ERROR", "LhcVacDeviceAction_VRPM(): dpSet(" + dpName + ") failed:" + err , "");
  }
}
