#ifndef	PROFILELHCLINEBEAM_H
#define	PROFILELHCLINEBEAM_H

// One line in LHC profile - beam vacuum

#include "ProfileLhcLine.h"

class ProfileLhcLineBeam : public ProfileLhcLine
{
	Q_OBJECT

public:
	ProfileLhcLineBeam(ProfileLhcView *parent, int type, float start, float end) :
		ProfileLhcLine(parent, type, start, end) {}
	virtual ~ProfileLhcLineBeam() {}

	virtual void placeItem(float &coord, int &barIdx);
	virtual void buildSectors(QFontMetrics &fontMetrics);

protected:

	virtual void drawSectorsHatchFill(QPainter &painter);
	virtual void drawSectorsWithBorders(QPainter &painter);
	virtual bool isMyEqp(Eqp *pEqp, int &partIdx);

	virtual bool isMyCryoT(Eqp *pEqp);

	// Implementation of LhcLineParts abstract methods
	virtual LhcLineParts::BuildType linePartMethod(void) { return LhcLineParts::TypeBeam; }
};

#endif	// PROFILELHCLINEBEAM_H
