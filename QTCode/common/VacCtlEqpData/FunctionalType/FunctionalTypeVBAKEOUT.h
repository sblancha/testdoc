#ifndef	FUNCTIONALTYPEVBAKEOUT_H
#define	FUNCTIONALTYPEVBAKEOUT_H

// Special functional type for bakeout racks - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVBAKEOUT : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Connected = 1,
		Warning = 2,
		Error = 3
	};

	FunctionalTypeVBAKEOUT(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVBAKEOUT() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVBAKEOUT_H
