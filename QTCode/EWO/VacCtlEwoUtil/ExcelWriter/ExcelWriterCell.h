#ifndef EXCELWRITERCELL_H
#define EXCELWRITERCELL_H

// Class holding data for one cell of Excel file to be generated

#include <qcolor.h>
#include <qstring.h>
#include <qdatetime.h>

#include <stdio.h>
#include <stdlib.h>

class ExcelCellFormat
{
public:
	ExcelCellFormat(const QColor &cellColor, int cellFormat) : color(cellColor)
	{
		format = cellFormat;
	}
	~ExcelCellFormat() {}

	int operator == (const ExcelCellFormat &other) const
	{
		return (other.color == color) && (other.format == format);
	}

	// Access
	inline const QColor &getColor(void) const { return color; }
	inline int getFormat(void) const { return format; }

protected:
	// Color of cell
	QColor	color;

	// Format of cell
	int		format;
};


class ExcelWriterCell
{
public:
	// Enum describing cell formats for Excel file
	typedef enum
	{
		String = 0,
		DateTime = 1,
		Float = 2,
		DateTimeWithMsec = 3,
		Integer = 4
	} Format;

	ExcelWriterCell(int column);
	~ExcelWriterCell();

	QString write(FILE *pFile);
	QString writeCsv(FILE *pFile, bool firstCell);

	// Access
	inline int getColumn(void) const { return column; }
	inline ExcelCellFormat getFormat(void) const { return ExcelCellFormat(color, format); }
	inline void setFormat(Format format) { this->format = format; }
	inline void setColor(const QColor &color) { this->color = color; }
	inline void setString(const QString &string) { this->string = string; }
	inline void setComment(const QString &comment) { this->comment = comment; }
	void setDateTime(const QDateTime &dateTime);
	void setDateTimeWithMsec(const QDateTime &dateTime);
	void setValue(float value);
	void setIntValue(int value);
	void setStyleIndex(int index) { styleIndex = index; }
	void setScientificStyleIndex(int index) { styleIndex = index; }

protected:
	// Column number in row
	int 		column;

	// Format for this cell in Excel sheet
	Format		format;

	// Color for this cell in Excle sheet (default = white)
	QColor		color;

	// String to be written to output file
	QString		string;

	// Comment to be written to output file
	QString		comment;

	// Style index for output
	int			styleIndex;
};

#endif	// EXCELWRITERCELL_H
