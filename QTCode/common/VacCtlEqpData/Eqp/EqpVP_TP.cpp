//	Implementation of EqpVP_TP class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVP_TP.h"

#include "DataPool.h"
#include "FunctionalTypeVP_STDIO.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_STARTING			127,255,127
#define COLOR_OFF				255,255,255
#define	COLOR_ERROR				255,102,102
#define	COLOR_WARNING			255,255,0
#define	COLOR_TMP_ACCELERATE	255,255,0

//	Main state bits to be analyzed
#define	RR1_VALID			(0x40000000)
#define RR1_ERROR			(0x00800000)
#define	RR1_WARNING			(0x00400000)
#define	RR1_NOMINAL_SPEED	(0x08000000)
#define	RR1_ON				(0x02000000)
#define	RR1_ON_DO			(0x00020000)
//#define	RR1_OFF			(0x00010000)
#define RR1_ERROR_CODE_MASK	(0x000000FF)
#define RR1_WARNING_CODE_MASK	(0x0000FF00)

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

EqpVP_TP::EqpVP_TP(const Eqp &source) : Eqp(source)
{
	rr1Online = rr1Replay = 0;
	rotationOnline = rotationReplay = -999;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::VP_STDIO;
}

EqpVP_TP::~EqpVP_TP()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_TP::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "ROTATION");
	addDpeNameToList(stateDpes, "RR1");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_TP::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "ROTATION", mode);
	connectDpe(pDst, "RR1", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_TP::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "ROTATION", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_TP::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if (!strcmp(dpe, "ROTATION"))
	{
		float newValue = value.toFloat();
		if(rotationOnline == newValue)
		{
			return;
		}
		rotationOnline = newValue;
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_TP::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "ROTATION"))
	{
		float newValue = value.toInt();
		if(rotationReplay == newValue)
		{
			return;
		}
		rotationReplay = newValue;
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_TP::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_TP::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_TP::getMainColor(unsigned rr1, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(rr1 & RR1_ERROR)
	{
		color.setRgb(COLOR_ERROR);
	}
	// No errors - check ON/OFF state. F.Bellorini wrote: ON_DO and ((DI_ON and not DI_NS) or not (DI_ON and DI_NS)) and not DI_Error
	else if(isStarting(rr1))
	{
		color.setRgb(COLOR_STARTING);
	}
	else if(rr1 & RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else if(rr1 & RR1_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else
	{
		color.setRgb(COLOR_OFF);
	}
	/*
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	*/
}
#endif

bool EqpVP_TP::isStarting(unsigned rr1) const
{
	// VTL 2499 - see new variant below return (rr1 & RR1_ON_DO) && (((rr1 & RR1_ON) && (!(rr1 & RR1_NOMINAL_SPEED))) || ((!(rr1 & RR1_ON)) && (!(rr1 & RR1_NOMINAL_SPEED))));
	return (rr1 & RR1_ON) && (!(rr1 & RR1_NOMINAL_SPEED)) && (rr1 & RR1_ON_DO);
}

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_TP::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, rotationReplay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, rotationOnline, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_TP::getMainStateString(unsigned rr1, float rotation, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
		return;
	}
	if(rr1 & RR1_ERROR)	// Still different strings for ON/OFF
	{
		if(rr1 & RR1_ON)
		{
			string = "ON (error)";
		}
		else
		{
			string = "OFF (error)";
		}
	}
	// No errors - check ON/OFF state
	else if(rr1 & RR1_WARNING)
	{
		if(isStarting(rr1))
		{
			string = "Starting (warning)";
		}
		else if(rr1 & RR1_ON)
		{
			string = "ON (warning)";
		}
		else
		{
			string = "OFF (warning)";
		}
	}
	else if(isStarting(rr1))
	{
		string = "Starting";
	}
	else if(rr1 & RR1_ON)
	{
		string = "ON";
	}
	else
	{
		string = "OFF";
	}

	QString rotString = QString::number(rotation);
	string = string + ": " + rotString + "%";	
	/*
	#ifdef Q_OS_WIN
		qDebug("rotation state string = %s \n" ,string);
	#else
		printf("rotation state string = %s \n" ,string);
		fflush(stdout);
	#endif
	*/
}
#endif


/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_TP::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainStateString(valueString, mode);
	string += valueString;
}
#endif

/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_TP::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("ROTATION");
	dpeNames.append("RR1");
	QList<QVariant *> values;
	if(!pPool->getDpValues(dpName, ts, dpeNames, values))
	{
		return;
	}
	if (values.isEmpty()) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	float rotation = pVariant->toFloat();
	delete pVariant;
	if (values.isEmpty()) {
		return;
	}
	pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, rotation, false, state);
	getMainColor(rr1, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_TP::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "ROTATION");
	addDpeNameToList(dpes, "RR1");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVP_TP::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if(rr1 & RR1_ON)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if(rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if(criteria.isWarnings())
	{
		if(rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpVP_TP::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	return rr1 & RR1_ERROR_CODE_MASK;
}





/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_TP::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if(checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	// Without PLC connection does not match any criteria
	if(plcAlarmOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// In invalid state - only matches Error/warning criteria
	if(!(rr1Online & RR1_VALID))
	{
		switch(pCriteria->getType())
		{
		case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		default:
			pCriteria->setMatch(false, false, 0);
			return;
		}
	}

	// State is valid - decide based on criteria
	unsigned maskedRr1 = rr1Online & RR1_ON;
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case FunctionalTypeVP_STDIO::On:
			if(maskedRr1 == RR1_ON)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVP_STDIO::Off:
			if(maskedRr1 != RR1_ON)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}

