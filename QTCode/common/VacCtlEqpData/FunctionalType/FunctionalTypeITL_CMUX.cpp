//	Implementation of FunctionalTypeITL_CMUX class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeITL_CMUX.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include <stdio.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeITL_CMUX::FunctionalTypeITL_CMUX(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeITL_CMUX::fillCriteria(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Interlock, "Interlock");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: CRYO Maintain Interlock");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: NO CRYO Maintain Interlock");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"CRYO Maintain Interlock in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"NO CRYO Maintain Interlock in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"CRYO Maintain Interlock in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"NO CRYO Maintain Interlock in main part {mainPart}");

}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeITL_CMUX::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case Interlock:
			result = "Interlock";
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
	return result;
}

int FunctionalTypeITL_CMUX::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "Interlock"))
		{
			result = Interlock;
		}
		break;
	default:
		break;
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}

