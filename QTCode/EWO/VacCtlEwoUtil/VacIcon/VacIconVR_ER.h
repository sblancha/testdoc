// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Component Name: VacCtlEwoUtil librairy
Class: Widget Vacuum contRol - bakE-out Rack (VR_ER)
Language: Qt/C++

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: This file contains the defined classes for the icon and widget

Limitations: To be used with vacuum framework.

Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------
#ifndef	VACICONVR_ER_H
#define	VACICONVR_ER_H

#include "VacIcon.h"

class EqpVR_ER;

class VACCTLEWOUTIL_EXPORT VacIconVR_ER : public VacIcon {
	Q_OBJECT
public:
	// Note parent argument is mandatory in constructor
	VacIconVR_ER(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVR_ER();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVR_ER *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, unsigned rr1, bool comAlarm);
	virtual void setPainterForImage(QPainter &painter, Eqp::CtlStatus ctlStatus, bool comAlarm);

};

#endif	// VACICONVR_ER_H


