#ifndef VACICONVVS_PS_CMW_H
#define	VACICONVVS_PS_CMW_H

// Icon for VVS_PS_CMW - PSR, data from CMW

#include "VacIcon.h"

class EqpVV_PS_CMW;

class VACCTLEWOUTIL_EXPORT VacIconVVS_PS_CMW : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVVS_PS_CMW(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVVS_PS_CMW();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVV_PS_CMW *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter);
	
	void setPainterForMainImage(QPainter &painter);
};

#endif	// VACICONVVS_PS_CMW_H
