#ifndef _VACCTLONLINENOTIFICATION_H_
#define _VACCTLONLINENOTIFICATION_H_

//	Class implementing some auxilliary methods for other classes,
//	which will be derived from BaseExternHdl. It is expected that
//	other classes for PVSS CTRL DLL will be drived from this class


# ifdef _WIN32
#   define  PVSS_EXPORT  __declspec(dllexport)
# else
#   define  PVSS_EXPORT
# endif


#include "VacCtlPvssUtil.h"


class VacCtlOnlineNotificationPvss : public VacCtlPvssUtil
{
public:
	VacCtlOnlineNotificationPvss(BaseExternHdl *nextHdl, PVSSulong funcCount, FunctionListRec fnList[])
      : VacCtlPvssUtil(nextHdl, funcCount, fnList) {}
	~VacCtlOnlineNotificationPvss() {}
	
	virtual const Variable *execute(ExecuteParamRec &param);

protected:
	int processGetFuncTypes(ExecuteParamRec &param);
	int processGetFuncTypeCriteria(ExecuteParamRec &param);
	int processGetCriteriaValues(ExecuteParamRec &param);
	int processGetStdCriteriaMessage(ExecuteParamRec &param);
	int processSetCriteria(ExecuteParamRec &param);
	int processSetUsageFilter(ExecuteParamRec &param);
	int processSetMessage(ExecuteParamRec &param);
	int processSetScope(ExecuteParamRec &param);
	int processClearGrouping(ExecuteParamRec &param);
	int processSetDeadTime(ExecuteParamRec &param);
	int processSetGrouping(ExecuteParamRec &param);
	int processSetConfigActive(ExecuteParamRec &param);
	int processIsConfigActive(ExecuteParamRec &param);
	int processSetConfigInitialized(ExecuteParamRec &param);
	int processDeleteConfig(ExecuteParamRec &param);
	int processCheckGroupExpired(ExecuteParamRec &param);
	int processGetNextMessage(ExecuteParamRec &param);

	int processConfigPrepareForWrite(ExecuteParamRec &param);
	int processConfigAdd(ExecuteParamRec &param);
	int processSetVisibleName(ExecuteParamRec &param);
	int processConfigSetMode(ExecuteParamRec &param);
	int processConfigSetMainType(ExecuteParamRec &param);
	int processConfigSetUsageFilter(ExecuteParamRec &param);
	int processConfigSetScopeType(ExecuteParamRec &param);
	int processConfigSetScopeItems(ExecuteParamRec &param);
	int processConfigSetCritJoinType(ExecuteParamRec &param);
	int processConfigAddCriteria(ExecuteParamRec &param);
	int processConfigSetMessage(ExecuteParamRec &param);
	int processConfigSetFiltering(ExecuteParamRec &param);
	int processConfigSetGroupMessage(ExecuteParamRec &param);
	int processConfigSetRecipients(ExecuteParamRec &param);
	int processConfigWrite(ExecuteParamRec &param);

	int processConfigParse(ExecuteParamRec &param);
	int processConfigFirstParsed(ExecuteParamRec &param);
	int processConfigNextParsed(ExecuteParamRec &param);
	int processGetVisibleName(ExecuteParamRec &param);
	int processConfigGetMode(ExecuteParamRec &param);
	int processConfigGetMainType(ExecuteParamRec &param);
	int processConfigGetUsageFilter(ExecuteParamRec &param);
	int processConfigGetScopeType(ExecuteParamRec &param);
	int processConfigGetScopeItems(ExecuteParamRec &param);
	int processConfigGetCritJoinType(ExecuteParamRec &param);
	int processConfigGetCriteria(ExecuteParamRec &param);
	int processConfigGetMessage(ExecuteParamRec &param);
	int processConfigGetFiltering(ExecuteParamRec &param);
	int processConfigGetGroupMessage(ExecuteParamRec &param);
	int processConfigGetRecipients(ExecuteParamRec &param);

	int processGetFuncTypeUsage(ExecuteParamRec &param);

	void buildMainFuncTypeList(int funcTypeId, QList<int> &funcTypeList);

};

#endif	// _VACCTLONLINENOTIFICATION_H_
