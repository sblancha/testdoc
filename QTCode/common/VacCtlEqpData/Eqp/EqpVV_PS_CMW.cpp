//	Implementation of EqpVV_PS_CMW class
////////////////////////////////////////////////////////////////////////////////////

#include "EqpVV_PS_CMW.h"
#include "DataPool.h"
#include "FunctionalTypeVV.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OPEN				0,255,0
#define COLOR_CLOSED			255,0,0
#define	COLOR_ERROR				255,102,255
#define	COLOR_WARNING			255,255,0

//	Main state bits to be analyzed
#define	RR1_OPEN		(3)
#define	RR1_CLOSED		(2)
#define RR1_CLOSING		(15)
#define RR1_OPENING		(16)
#define RR1_UNDEFINED	(1)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVV_PS_CMW::EqpVV_PS_CMW(const Eqp &source) : Eqp(source)
{
	rr1Online = rr1Replay = 0;
	driverOkOnline = driverOkReplay = false;
	functionalType = FunctionalType::VV;
}

EqpVV_PS_CMW::~EqpVV_PS_CMW()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV_PS_CMW::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "DriverOK");
	addDpeNameToList(stateDpes, "RR1");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV_PS_CMW::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "DriverOK", mode);
	connectDpe(pDst, "RR1", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV_PS_CMW::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "DriverOK", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV_PS_CMW::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "DriverOK"))
	{
		if(value.canConvert(QVariant::Bool))
		{
			bool newValue = value.toBool();
			if(driverOkOnline == newValue)
			{
				return;
			}
			driverOkOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_PS_CMW::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "DriverOK"))
	{
		if(value.canConvert(QVariant::Bool))
		{
			bool newValue = value.toBool();
			if(driverOkReplay == newValue)
			{
				return;
			}
			driverOkReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_PS_CMW::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainColor(driverOkReplay, rr1Replay, color);
	}
	else
	{
		getMainColor(driverOkOnline, rr1Online, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		driverOk	- Value of DriverOK DPE
**		rr1			- Value of RR1 DPE
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_PS_CMW::getMainColor(bool driverOk, unsigned rr1, QColor &color)
{
	if(!driverOk)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	switch(rr1)
	{
	case RR1_OPEN:
		color.setRgb(COLOR_OPEN);
		break;
	case RR1_CLOSED:
		color.setRgb(COLOR_CLOSED);
		break;
	case RR1_CLOSING:
	case RR1_OPENING:
		color.setRgb(COLOR_UNDEFINED);
		break;
	case RR1_UNDEFINED:
	default:
		color.setRgb(COLOR_ERROR);
		break;
	}
}
#endif

/*
**	FUNCTION
**		Calculate draw order for device. Method is only used for drawing
**		main views (LHC/SPS) => only online data are checked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Draw order, items with higher order are drawn on top of items
**		with low order
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVV_PS_CMW::getDrawOrder(void)
{
	int result = 1;
	if(!driverOkOnline)
	{
		result = 2;
		return result;
	}
	switch(rr1Online)
	{
	case RR1_OPEN:
		result = 0;
		break;
	case RR1_CLOSED:
		result = 3;
		break;
	case RR1_CLOSING:
	case RR1_OPENING:
		result = 1;
		break;
	case RR1_UNDEFINED:
	default:
		result = 2;
		break;
	}
	return result;
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_PS_CMW::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainStateString(driverOkReplay, rr1Replay, string);
	}
	else
	{
		getMainStateString(driverOkOnline, rr1Online, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		driverOk	- Value of DriverOK DPE
**		rr1			- Value of RR1 DPE
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_PS_CMW::getMainStateString(bool driverOk, unsigned rr1, QString &string)
{
	if(!driverOk)
	{
		string = "Driver error";
		return;
	}
	switch(rr1)
	{
	case RR1_OPEN:
		string = "Open";
		break;
	case RR1_CLOSED:
		string = "Closed";
		break;
	case RR1_CLOSING:
		string = "Closing";
		break;
	case RR1_OPENING:
		string = "Opening";
		break;
	case RR1_UNDEFINED:
		string = "Undefined";
		break;
	default:
		{
			char buf[32];
#ifdef Q_OS_WIN
			sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "Unknown (%d)", rr1);
#else
			sprintf(buf, "Unknown (%d)", rr1);
#endif
			string = buf;
		}
		break;
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_PS_CMW::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainStateStringForMenu(driverOkReplay, rr1Replay, string);
	}
	else
	{
		getMainStateStringForMenu(driverOkOnline, rr1Online, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		driverOk	- Value of DriverOK DPE
**		rr1			- Value of RR1 DPE
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_PS_CMW::getMainStateStringForMenu(bool driverOk, unsigned rr1, QString &string)
{
	QString state;
	getMainStateString(driverOk, rr1, state);
	string = getVisibleName();
	string += ": ";
	string += state;
}
#endif

#ifndef PVSS_SERVER_VERSION
void EqpVV_PS_CMW::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getToolTipString(driverOkReplay, rr1Replay, string);
	}
	else
	{
		getToolTipString(driverOkOnline, rr1Online, string);
	}
}
#endif

#ifndef PVSS_SERVER_VERSION
void EqpVV_PS_CMW::getToolTipString(bool driverOk, unsigned rr1, QString &string)
{
	QString state;
	getMainStateString(driverOk, rr1, state);
	string = getVisibleName();
	string += ": ";
	string += state;
}
#endif

/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_PS_CMW::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("DriverOK");
	dpeNames.append("RR1");
	QList<QVariant *> values;
	if(!pPool->getDpValues(dpName, ts, dpeNames, values))
	{
		return;
	}
	if (values.isEmpty()) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	bool driverOk = pVariant->toBool();
	delete pVariant;
	if (values.isEmpty()) {
		return;
	}
	pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(driverOk, rr1, state);
	getMainColor(driverOk, rr1, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif




/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_PS_CMW::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	addDpeNameToList(dpes, "DriverOK");
	addDpeNameToList(dpes, "RR1");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVV_PS_CMW::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// Check if state is valid
	bool driverOk = mode == DataEnum::Replay ? driverOkReplay : driverOkOnline;
	if(!driverOk)
	{
		return criteria.isAccess() ? 1 : 0;
	}

	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;

		// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	switch(rr1)
	{
	case RR1_OPEN:
		state = DevListCriteria::EqpStateNormal;
		break;
	case RR1_CLOSED:
		state = DevListCriteria::EqpStateAbnormal;
		break;
	default:
		break;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}
	return 0;
}
#endif


/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV_PS_CMW::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// Without driver success does not match any criteria
	if(!driverOkOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// State is always valid - decide based on criteria
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case FunctionalTypeVV::Closed:
			if(rr1Online == RR1_CLOSED)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::Open:
			if(rr1Online == RR1_OPEN)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	default:
		break;
	}
}

