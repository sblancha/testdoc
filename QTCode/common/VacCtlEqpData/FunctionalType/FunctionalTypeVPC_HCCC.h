#ifndef FUNCTIONALTYPEVPC_HCCC_H
#define FUNCTIONALTYPEVPC_HCCC_H

// Special functional type for Vacuum Pump Cryogenic with HSR-HCC Controller - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVPC_HCCC : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		RecoverNotUsed = 1,
		OnNominal = 2,  
		Starting = 3, //Prevac or Cooldown
		Error = 4,
		Off = 5,
		ServicesNotUsed = 6		
	};

	// CONSTRUCTOR
	FunctionalTypeVPC_HCCC(int type, const char *name, const char *description);
	// DESTRUCTOR
	virtual ~FunctionalTypeVPC_HCCC() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVPC_HCCC_H
