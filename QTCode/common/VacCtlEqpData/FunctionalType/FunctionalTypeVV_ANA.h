#ifndef FUNCTIONALTYPEVV_ANA_H
#define	FUNCTIONALTYPEVV_ANA_H

// Special functional type for analog valves - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVV_ANA : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Open = 1,
		Closed = 2
	};

	FunctionalTypeVV_ANA(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVV_ANA() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVG_H
