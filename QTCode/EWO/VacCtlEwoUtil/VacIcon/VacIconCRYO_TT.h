#ifndef	VACICONCRYO_TT_T
#define	VACICONCRYO_TT_T

//	Icon for CRYO thermometer

#include "VacIcon.h"

class EqpCRYO_TT;

class VACCTLEWOUTIL_EXPORT VacIconCRYO_TT : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconCRYO_TT(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconCRYO_TT();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask, bool forBeamVac);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpCRYO_TT *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACICONCRYO_TT_T
