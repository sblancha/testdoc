#ifndef	SECTVIEWXML_H
#define	SECTVIEWXML_H

// Sector view for displaying manually built PVSS panel in XML format

#include "SectView.h"

#include "XmlSynoptic.h"

class SectViewXml : public SectView
{
	Q_OBJECT

public:
	SectViewXml(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	~SectViewXml();

	static SectViewXml *create(QWidget *parent, const QString &fileName, DataEnum::DataMode mode);

protected:
	XmlSynoptic	*pSynoptic;

	QString		errMsg;

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// SECTVIEWXML_H
