#ifndef	REALCRYOSECTOR_H
#define	REALCRYOSECTOR_H

// Class holding data for real sector included in profile line

#include <stdio.h>

class Sector;
#include "Eqp.h"
#include "EqpMsgCriteria.h"

class RealCryoSector
{
public:
	RealCryoSector(Sector *pSector, float viewStart, float viewEnd);
	~RealCryoSector() {}

	void dump(FILE *pFile, int idx);

	// Access
	inline Sector *getSector(void) const { return pSector; }
	inline float getStart(void) const { return start; }
	inline float getEnd(void) const { return end; }
	inline int getStartBarIdx(void) const { return startBarIdx; }
	inline void setStartBarIdx(int idx) { startBarIdx = idx; }
	inline int getEndBarIdx(void) const { return endBarIdx; }
	inline void setEndBarIdx(int idx) { endBarIdx = idx; }
	inline int getLeftX(void) const { return leftX; }
	inline void setLeftX(int coord) { leftX = coord; }
	inline int getRightX(void) const { return rightX; }
	inline void setRightX(int coord) { rightX = coord; }
	inline bool isStartInside(void) const { return startInside; }
	inline bool isEndInside(void) const { return endInside; }
	inline bool isStartPassed(void) const { return startPassed; }
	inline void setStartPassed(bool flag) { startPassed = flag; }

protected:
	// Pointer to vacuum sector
	Sector	*pSector;

	// Start coordinate of sector, lhcRingLength can be added
	float	start;

	// End coordinate of sector, lhcRingLength can be added
	float	end;

	// Bar index corresponding to start of sector
	int		startBarIdx;

	// Bar index corresponding to end of sector
	int		endBarIdx;

	// X coordinate of sector start
	int		leftX;

	// X coordinate of sector end
	int		rightX;

	// true if start of sector is inside the line
	bool	startInside;

	// true if end of sector is inside the line
	bool	endInside;

	// Flag indicating if start of sector has been passed
	bool	startPassed;
};

#endif	// REALCRYOSECTOR_H
