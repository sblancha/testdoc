//	Implementation of EqpMsgCriteria class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "FunctionalType.h"

#include "ResourcePool.h"

#include <stdio.h>

const char *EqpMsgCriteria::critTypeToString(int type)
{
	switch(type)
	{
	case None:
		return "None";
	case MainState:
		return "State";
	case MainValue:
		return "Value";
	case ActiveState:
		return "Activity";
	case Error:
		return "Error";
	case Warning:
		return "Warning";
	default:
		break;
	}
	static char result[32];
#ifdef Q_OS_WIN
	sprintf_s(result, sizeof(result) / sizeof(result[0]), "%d", type);
#else
	sprintf(result, "%d", type);
#endif
	return result;
}

int EqpMsgCriteria::critTypeParse(const char *string, QString &errMsg)
{
	int result = 0;
	if(!string)
	{
		errMsg += "NULL criteria type";
	}
	else if(!(*string))
	{
		errMsg += "Empty criteria type";
	}
	else if(!strcasecmp(string, "none"))
	{
		result = None;
	}
	else if(!strcasecmp(string, "state"))
	{
		result = MainState;
	}
	else if(!strcasecmp(string, "value"))
	{
		result = MainValue;
	}
	else if(!strcasecmp(string, "activity"))
	{
		result = ActiveState;
	}
	else if(!strcasecmp(string, "error"))
	{
		result = Error;
	}
	else if(!strcasecmp(string, "warning"))
	{
		result = Warning;
	}
	else
	{
		errMsg += "Unsupported criteria type <";
		errMsg += string;
		errMsg += ">";
	}
	return result;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
////////////////////////////////////////////////////////////////////////////////

EqpMsgCriteria::EqpMsgCriteria(int functionalType, int type, int subType, const char *name)
{
	this->functionalType = functionalType;
	this->type = type;
	this->subType = subType;
	this->name = name;
	upperLimit = lowerLimit = 0;
	errCode = 0;
	warningCode = 0;
	reverse = false;
	match = type == None;	// Criteria of no type always matches
	minDuration = 0;
	pendingMatch = false;
	matchByValue = false;
	eventValue = 0;
	pMsgList = new QList<StdEqpMessage *>();
}

EqpMsgCriteria::EqpMsgCriteria(const EqpMsgCriteria &source) :
	name(source.name), eventTime(source.eventTime)
{
	functionalType = source.functionalType;
	type = source.type;
	subType = source.subType;
	lowerLimit = source.lowerLimit;
	upperLimit = source.upperLimit;
	errCode = source.errCode;
	warningCode = source.warningCode;
	reverse = source.reverse;
	match = source.match;
	matchByValue = source.matchByValue;
	eventValue = source.eventValue;
	minDuration = source.minDuration;
	pendingMatch = source.pendingMatch;
	pMsgList = new QList<StdEqpMessage *>(*(source.pMsgList));
}

EqpMsgCriteria::~EqpMsgCriteria()
{
	while(!pMsgList->isEmpty())
	{
		delete pMsgList->takeFirst();
	}
	delete pMsgList;
}

EqpMsgCriteria & EqpMsgCriteria::operator = (const EqpMsgCriteria &other)
{
	name = other.name;
	functionalType = other.functionalType;
	type = other.type;
	subType = other.subType;
	lowerLimit = other.lowerLimit;
	upperLimit = other.upperLimit;
	errCode = other.errCode;
	warningCode = other.warningCode;
	reverse = other.reverse;
	match = other.match;
	matchByValue = other.matchByValue;
	eventValue = other.eventValue;
	minDuration = other.minDuration;
	pendingMatch = other.pendingMatch;
	eventTime = other.eventTime;
	*pMsgList = *(other.pMsgList);
	return *this;
}

/*
**	FUNCTION
**		Add standard message for this critera - to be issued when criteria is fulfilled
**
**	ARGUMENTS
**		type	- message source type (see MessageSourceType enum in StdEqpMessage class)
**		reverse	- true if this is message for the case when criteria is NOT fulfilled
**		text	- message text
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpMsgCriteria::addStdMessage(int type, bool reverse, const char *text)
{
	FunctionalType *pType = FunctionalType::findTypeData(functionalType);
	if(pType)
	{
		QString resourceName("SmsMsg.");
		resourceName += pType->getName();
		resourceName += ".";
		resourceName += pType->getCriteriaKey(this);
		switch(type)
		{
			case StdEqpMessage::SourceTypeSector:
			resourceName += ".Sector";
			break;
		case StdEqpMessage::SourceTypeMainPart:
			resourceName += ".MainPart";
			break;
		default:
			resourceName += ".Eqp";
			break;
		}
		if(reverse)
		{
			resourceName += ".Reverse";
		}

		QString result;
		if(ResourcePool::getInstance().getStringValue(resourceName.toLatin1(), result) != ResourcePool::OK)
		{
			result = text;
		}
		pMsgList->append(new StdEqpMessage(type, reverse, result.toLatin1()));
	}
	else	// this shall not happen
	{
		pMsgList->append(new StdEqpMessage(type, reverse, text));
	}
}

/*
**	FUNCTION
**		Get standard message for this critera - to be issued when criteria is fulfilled
**
**	ARGUMENTS
**		type	- message source type (see MessageSourceType enum in StdEqpMessage class)
**		reverse	- true if this is message for the case when criteria is NOT fulfilled
**
**	RETURNS
**		Pointer to message text; or
**		NULL if there is no standrad message
**
**	CAUTIONS
**		None
*/
const char *EqpMsgCriteria::getStdMessage(int type, bool reverse)
{
	for(int idx = 0 ; idx < pMsgList->count() ; idx++)
	{
		StdEqpMessage *pMsg = pMsgList->at(idx);
		if((pMsg->getType() == type) && (pMsg->isReverse() == reverse))
		{
			return pMsg->getText().toLatin1();
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Set flag indicating if criteria matches. Setting true can be delayed if
**		minDuration > 0.
**
**	ARGUMENTS
**		flag	- New flag to set
**		isValue	- true if criteria matches because of value
**		value	- value that caused criteria to match, only makes sense if isValue=true
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpMsgCriteria::setMatch(bool flag, bool isValue, float value)
{
/*
printf("EqpMsgCriteria::setMatch(%d) match %d pending %d\n", (int)flag,
(int)match, (int)pendingMatch);
fflush(stdout);
*/
	if(flag)
	{
		if(match || pendingMatch)
		{
			return;
		}
/*
printf("EqpMsgCriteria::setMatch(%d) minDuration %d isValue %d value %g, this=%X\n",
	(int)flag, minDuration, (int)isValue, value, (unsigned)this);
fflush(stdout);
*/
		eventTime = QDateTime::currentDateTime();
		matchByValue = isValue;
		eventValue = value;
		if(minDuration > 0)
		{
			pendingMatch = true;
		}
		else
		{
			match = true;
			pendingMatch = false;
		}
	}
	else
	{
		match = pendingMatch = false;
	}
/*
printf("EqpMsgCriteria::setMatch(%d) finish: match %d pendingMatch %d\n", (int)flag, (int)match, (int)pendingMatch);
fflush(stdout);
*/
}

/*
**	FUNCTION
**		Reset pending match state for all criteria, used to avoid sending
**		a lot of messages shortly after notification config is initialized.
**		If at this moment 'pending match' is detected - it is converted to
**		'real match'.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpMsgCriteria::resetPendingMatch(void)
{
	if(pendingMatch)
	{
		match = true;
		pendingMatch = false;
	}
}

/*
**	FUNCTION
**		Check if pending match condition shall be converted to 'real' match condition
**
**	ARGUMENTS
**		now	- Current date + time
**
**	RETURNS
**		true	- If pending match for this criteria has been converted to real one
**
**	CAUTIONS
**		None
*/
bool EqpMsgCriteria::checkPendingMatch(const QDateTime &now)
{
/*
printf("EqpMsgCriteria::checkPendingMatch(): match %d pendingMatch %d\n", (int)match, (int)pendingMatch);
fflush(stdout);
*/
	if(match || (!pendingMatch))
	{
		return false;
	}
/*
printf("EqpMsgCriteria::checkPendingMatch(): %d secs of %d\n", eventTime.secsTo(now), minDuration);
fflush(stdout);
*/

	if(eventTime.secsTo(now) >= minDuration)
	{
		match = true;
		pendingMatch = false;
		return true;
	}
	return false;
}
