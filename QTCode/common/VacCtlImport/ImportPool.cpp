//	Implementation of ImportPool class
////////////////////////////////////////////////////////////////////////////////

#include "ImportPool.h"

#include "PlatformDef.h"

#include <QStringList>

ImportPool	ImportPool::instance;
bool ImportPool::debugMode = false;

ImportPool &ImportPool::getInstance(void)
{
	return instance;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
ImportPool::ImportPool()
{
	pDps = new QHash<QByteArray, DpData *>();
	pArchives = new QList<Archive *>();
	pPollGroups = new QList<PollGroup *>();
	pS7Connections = new QList<S7Connection *>();
	timestamp = NULL;
	machine = NULL;
	mobile = false;
	// Set reference to itself
	DpData::setImportPool(this);
	DpeData::setImportPool(this);
	Archive::setImportPool(this);
	PollGroup::setImportPool(this);
	S7Connection::setImportPool(this);
}

ImportPool::~ImportPool()
{
	while(!pS7Connections->isEmpty())
	{
		delete pS7Connections->takeFirst();
	}
	delete pS7Connections;
	while(!pPollGroups->isEmpty())
	{
		delete pPollGroups->takeFirst();
	}
	delete pPollGroups;
	while(!pArchives->isEmpty())
	{
		delete pArchives->takeFirst();
	}
	delete pArchives;
	foreach(DpData *pDp, *pDps)
	{
		delete pDp;
	}
	delete pDps;
}

/*
**
**	FUNCTION
**		Set mobile mode, clear recent content
**
**	PARAMETERS
**		mobile	- New mobile mode to set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ImportPool::setMobile(bool mobile)
{
	this->mobile = mobile;
	timestamp = NULL;
	machine = NULL;
	pDps->clear();
	pArchives->clear();
	pPollGroups->clear();
	pS7Connections->clear();
	namePool.clear();
	DriverRef::clear();
}

/*
**
**	FUNCTION
**		Read and parse input file
**
**	PARAMETERS
**		fileName	- Name of file to parse
**		machineName	- Name of machine for which file will be used
**		errList		- variable where errors be written in case of errors
**
**	RETURNS
**		value returned by FileParser's parseFile() method.
**
**	CAUTIONS
**		None
*/
int ImportPool::processFile(const char *fileName, const char *machineName, QStringList &errList)
{
	machine = namePool.addString(machineName);
	int coco = FileParser::parseFile(fileName, errList);
	if(debugMode)
	{
#ifdef Q_OS_WIN
		const char	*dumpFileName = "C:\\Import.txt";
		FILE *pFile = NULL;
		if(fopen_s(&pFile, dumpFileName, "w"))
		{
			pFile = NULL;
		}
#else
		const char	*dumpFileName = "/home/kopylov/Import.txt";
		FILE *pFile = fopen(dumpFileName, "w");
#endif
		if(pFile)
		{
			dump(pFile);
			fclose(pFile);
		}
	}
	return coco;
}

/*
**
**	FUNCTION
**		Process tokens of one line in input file - implementation of
**		FileParser's abstract method
**
**	PARAMETERS
**		pFile		- Pointer to file opened for reading
**		lineNo		- Current line number in file
**		tokens		- Array of tokens in line
**		nTokens		- Number of tokens in array
**		errList		- Variable where error message will be added in case of errors
**
** RETURNS
**		> 0  - line successfully processed, more lines expected
**		== 0 - line successfully processed, no more lines expected
**		< 0  - line can not be processed (unknown keyword. missing arguments etc.)
**
** CAUTIONS
**		None
*/
int ImportPool::processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString & /* longValue */, QStringList &errList)
{
	const char	*funcName = "ImportPool::processLineTokens()";

	if(!strcasecmp(tokens[0], "Device"))		// Start of device
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(!mobile )	// DP names can not appear more than once
		{
			DpData *pDp = (*pDps)[tokens[1]];
			if(pDp)
			{
				PARSING_WARNING2("Line %d: duplicate DP <%s>", lineNo, tokens[1]);
				return 1;
			}
		}
		DpData *pDp = DpData::createFromFile(pFile, lineNo, tokens[1], errList);
		if(!pDp)
		{
			return -1;
		}
		if(mobile)	// DP name + DP type pair can not appear more than once
		{
			char key[1024];
#ifdef Q_OS_WIN
			strcpy_s(key, sizeof(key) / sizeof(key[0]), tokens[1]);
			strcat_s(key, sizeof(key) / sizeof(key[0]), "|");
			if(pDp->getDpType())
			{
				strcat_s(key, sizeof(key) / sizeof(key[0]), pDp->getDpType());
			}
#else
			strcpy(key, tokens[1]);
			strcat(key, "|");
			if(pDp->getDpType())
			{
				strcat(key, pDp->getDpType());
			}
#endif
			if((*pDps)[key])
			{
				PARSING_WARNING2("Line %d: duplicating DP <%s>", lineNo, key);
				delete pDp;
				return 1;
			}
		}
		// Add new DP to list
		char key[1024];
#ifdef Q_OS_WIN
		strcpy_s(key, sizeof(key) / sizeof(key[0]), tokens[1]);
		if(mobile)
		{
			strcat_s(key, sizeof(key) / sizeof(key[0]), "|");
			if(pDp->getDpType())
			{
				strcat_s(key, sizeof(key) / sizeof(key[0]), pDp->getDpType());
			}
		}
#else
		strcpy(key, tokens[1]);
		if(mobile)
		{
			strcat(key, "|");
			if(pDp->getDpType())
			{
				strcat(key, pDp->getDpType());
			}
		}
#endif
		pDps->insert(key, pDp);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Accelerator"))	
	{		
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(strcmp(machine, tokens[1]))
		{
			PARSING_ERR2("File is for machine <%s>, but required <%s>", tokens[1], machine);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Timestamp"))	
	{
		
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		timestamp = namePool.addString(tokens[1]);
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Archive"))		// Start of archive
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		Archive *pArchive = Archive::createFromFile(pFile, lineNo, tokens[1], errList);
		if(!pArchive)
		{
			return -1;
		}
		if(findArchive(tokens[1]))
		{
			PARSING_WARNING2("Line %d: duplicating archive <%s>", lineNo, tokens[1]);
			delete pArchive;
		}
		else
		{
			pArchives->append(pArchive);
		}
		return 1;
		
	}
	else if(!strcasecmp(tokens[0], "S7Connection"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		S7Connection *pConnect = S7Connection::createFromFile(pFile, lineNo, tokens[1], errList);
		if(!pConnect)
		{
			return -1;
		}
		if(findS7Connection(tokens[1]))
		{
			PARSING_WARNING2("Line %d: duplicate S7Connection <%s>", lineNo, tokens[1]);
			delete pConnect;
		}
		else
		{
			pS7Connections->append(pConnect);
		}
		return 1;
		
	}
	else if(!strcasecmp(tokens[0], "S7PollGroup"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		PollGroup *pPollGroup = PollGroup::createFromFile(pFile, lineNo, tokens[1], errList);
		if(!pPollGroup)
		{
			return -1;
		}
		if(findPollGroup(tokens[1]))
		{
			PARSING_WARNING2("Line %d: duplicating PollGroup <%s>", lineNo, tokens[1]);
			delete pPollGroup;
		}
		else
		{
			pPollGroups->append(pPollGroup);
		}
		return 1;
		
	}
	PARSING_ERR2("Line %d: unexpected <%s>", lineNo, tokens[0]);
}

/*
**
**	FUNCTION
**		Find data for archive with given name
**
**	PARAMETERS
**		name	- Name of archive to find
**
**	RETURNS
**		Pointer to archive data, or
**		NULL if such archive was not found
**
**	CAUTIONS
**		None
*/
Archive *ImportPool::findArchive(const char *name)
{
	foreach(Archive *pItem, *pArchives)
	{
		if(!strcmp(pItem->getName(), name))
		{
			return pItem;
		}
	}
	return NULL;
}

/*
**
**	FUNCTION
**		Find data for S7 connection with given name
**
**	PARAMETERS
**		name	- Name of S7 connection to find
**
**	RETURNS
**		Pointer to connection data, or
**		NULL if such connection was not found
**
**	CAUTIONS
**		None
*/
S7Connection *ImportPool::findS7Connection(const char *name)
{
	foreach(S7Connection *pItem, *pS7Connections)
	{
		if(!strcmp(pItem->getName(), name))
		{
			return pItem;
		}
	}
	return NULL;
}

/*
**
**	FUNCTION
**		Find data for poll group with given name
**
**	PARAMETERS
**		name	- Name of poll group to find
**
**	RETURNS
**		Pointer to group data, or
**		NULL if such group was not found
**
**	CAUTIONS
**		None
*/
PollGroup *ImportPool::findPollGroup(const char *name)
{
	foreach(PollGroup *pItem, *pPollGroups)
	{
		if(!strcmp(pItem->getName(), name))
		{
			return pItem;
		}
	}
	return NULL;
}

/*
**
**	FUNCTION
**		Find data for DP with given name - version for fixed data where
**		DP has single DP type
**
**	PARAMETERS
**		name	- Name of DP to find
**
**	RETURNS
**		Pointer to DP data, or
**		NULL if such DP was not found
**
**	CAUTIONS
**		None
*/
DpData *ImportPool::findDp(const char *name)
{
	return pDps->value(name);
}

/*
**
**	FUNCTION
**		Find data for DP with given name - version for mobile data where
**		DP can appear with different DP types
**
**	PARAMETERS
**		name	- Name of DP to find
**		dpType	- Name of DP type
**
**	RETURNS
**		Pointer to DP data, or
**		NULL if such DP + Dp type was not found
**
**	CAUTIONS
**		None
*/
DpData *ImportPool::findDp(const char *name, const char *dpType)
{
	if(!dpType)
	{
		return pDps->value(name);
	}
	if(!*dpType)
	{
		return pDps->value(name);
	}
	char	key[1024];
#ifdef Q_OS_WIN
	strcpy_s(key, sizeof(key) / sizeof(key[0]), name);
	strcat_s(key, sizeof(key) / sizeof(key[0]), "|");
	strcat_s(key, sizeof(key) / sizeof(key[0]), dpType);
#else
	strcpy(key, name);
	strcat(key, "|");
	strcat(key, dpType);
#endif
	return pDps->value(key);
}

/*
**	FUNCTION
**		Add DPE reference to archive
**
**	PARAMETERS
**		name	- Name of archive
**
**	RETURNS
**		Index of archive in archive list (first item has index 1), or
**		0 if archive is not found
**
**	CAUTIONS
**		None
*/
int ImportPool::checkArchiveReference(const char *name)
{
	for(int idx = 0 ; idx < pArchives->count() ; idx++)
	{
		Archive *pItem = pArchives->at(idx);
		if(!strcmp(pItem->getName(), name))
		{
			pItem->incrementRefCount();
			return idx + 1;
		}
	}
	return 0;
}

/*
**	FUNCTION
**		Add DPE reference to S7 connection
**
**	PARAMETERS
**		name	- Name of S7 connection
**
**	RETURNS
**		Index of connection in connection list (first item has index 1), or
**		0 if connection is not found
**
**	CAUTIONS
**		None
*/
int ImportPool::checkS7ConnectReference(const char *name)
{
	// L.Kopylov 01.12.2011 When parsing mobile addresses - S7 connections are not defined in file
	if(mobile)
	{
		return 1;
	}
	for(int idx = 0 ; idx < pS7Connections->count() ; idx++)
	{
		S7Connection *pItem = pS7Connections->at(idx);
		if(!strcmp(pItem->getName(), name))
		{
			return idx + 1;
		}
	}
	return 0;
}

/*
**	FUNCTION
**		Add reference to poll group
**
**	PARAMETERS
**		name	- Name of poll group
**
**	RETURNS
**		Index of group in poll group list (first item has index 1), or
**		0 if group is not found
**
**	CAUTIONS
**		None
*/
int ImportPool::checkPollGroupReference(const char *name)
{
	// L.Kopylov 01.12.2011 When parsing mobile addresses - poll groups are not defined in file
	if(mobile)
	{
		return 1;
	}
	for(int idx = 0 ; idx < pPollGroups->count() ; idx++)
	{
		PollGroup *pItem = pPollGroups->at(idx);
		if(!strcmp(pItem->getName(), name))
		{
			return idx + 1;
		}
	}
	return 0;
}

/*
**
**	FUNCTION
**		Dump instance content to output file for debugging
**
**	PARAMETERS
**		pFile		- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ImportPool::dump(FILE *pFile)
{
	if(!debugMode)
	{
		return;
	}
	fprintf(pFile, "MOBILE %d machine <%s> timestamp <%s>\n", mobile, machine, timestamp);

	fprintf(pFile, "\n################### ARCHIVES (%d) #######################\n",
		pArchives->count());
	foreach(Archive *pArchive, *pArchives)
	{
		pArchive->dump(pFile);
	}

	fprintf(pFile, "\n################### S7 Connection (%d) ##################\n",
		pS7Connections->count());
	foreach(S7Connection *pS7Conn, *pS7Connections)
	{
		pS7Conn->dump(pFile);
	}

	fprintf(pFile, "\n################### Poll Groups (%d) ###################\n",
		pPollGroups->count());
	foreach(PollGroup *pPollGroup, *pPollGroups)
	{
		pPollGroup->dump(pFile);
	}

	fprintf(pFile, "\n################### Drivers #######################\n");
	DriverRef::dump(pFile);

	fprintf(pFile, "\n################### DPs (%d of %d) ###########################\n",
		pDps->count(), pDps->size());
	foreach(DpData *pItem, *pDps)
	{
		pItem->dump(pFile);
	}
}
