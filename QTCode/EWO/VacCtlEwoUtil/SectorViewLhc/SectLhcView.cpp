//	Implementation of SectLhcView class
/////////////////////////////////////////////////////////////////////////////////

#include "SectLhcView.h"
#include "SectLhcLine.h"
#include "SectLhcLinePassive.h"

#include "Sector.h"
#include "DebugCtl.h"

#define max(a,b) ((a) > (b) ? (a) : (b))


/*
**	FUNCTION
**		Create new instance of LHC sector view
**
**	PARAMETERS
**		parent			- Pointer to parent widget
**		pFirstSector	- Pointer to first sector
**		pLastSector		- Pointer to last sector
**		mode			- Data acquisition mode for synoptic
**
**	RETURNS
**		Pointer to new instance of synoptic view,
**		NULL in case of error.
**
**	CAUTIONS
**		None
*/
SectLhcView *SectLhcView::create(QWidget *parent, Sector *pFirstSector, Sector *pLastSector,
	DataEnum::DataMode mode)
{
	float	start = pFirstSector->getStart(),
			end = pLastSector->getEnd();

	if(start == end)
	{
		return NULL;
	}

	// Build new instance
	SectLhcView	*pView = new SectLhcView(parent, 0);
	pView->pStartSector = pFirstSector;
	pView->pEndSector = pLastSector;
	pView->start = start;
	pView->end = end;
	pView->mode = mode;
	pView->build(true);

	return pView;
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

SectLhcView::SectLhcView(QWidget *parent, Qt::WindowFlags f) :
	SynLhcView(parent, f)
{
	ready = false;
}

SectLhcView::~SectLhcView()
{
}

/*
**	FUNCTION
**		Build all lines in sector view
**
**	PARAMETERS
**		hideMagnets	- true if magnets shall be hidden, not used here
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void SectLhcView::build(bool /* hideMagnets */)
{
	lines.append(new SectLhcLine(this, SectLhcLine::OuterBeam, start, end));
	lines.append(new SectLhcLine(this, SectLhcLine::CrossBeam, start, end));
	lines.append(new SectLhcLine(this, SectLhcLine::InnerBeam, start, end));
	lines.append(new SectLhcLinePassive(this, SectLhcLine::Passive, start, end));
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->setStartSector(pStartSector);
		pLine->setEndSector(pEndSector);
		pLine->setMode(mode);
	}
}

/*
**	FUNCTION
**		Rebuild geometries of all individual lines if necessary,
**		then build overall geometry of the whole view.
**
**	PARAMETERS
**		vacTypeMask	- OR'ed bit mask for all vacuum types
**		cryoEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**						to be shown in this view
**		beamEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**						to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcView::buildView(unsigned /* vacTypeMask */, VacEqpTypeMask & /* cryoEqpMask */,
	VacEqpTypeMask & /* beamEqpMask */)
{
	if(ready)
	{
		return;
	}
	ready = true;
	VacEqpTypeMask allTypesMask = VacEqpTypeMask::getAllTypes();
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->build(VacType::RedBeam | VacType::BlueBeam, allTypesMask);
	}
	if(DebugCtl::isSynoptic())
	{
		#ifdef Q_OS_WIN
			dump("C:\\SectLhcPass1.txt");
		#else
			dump("/home/kopylov/SectLhcPass1.txt");
		#endif
	}

	buildGeometry();

	if(DebugCtl::isSynoptic())
	{
		#ifdef Q_OS_WIN
			dump("C:\\SectLhcPass2.txt");
		#else
			dump("/home/kopylov/SectLhcPass2.txt");
		#endif
	}
}

/*
**	FUNCTION
**		Build overall geometry of the whole view.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcView::buildGeometry(void)
{
	buildLinesGeometry();
	finishLineParts();
	calcVerticalPos();

	// Finally move all active icons to calculated positions
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->finishGeometry();
	}
}

/*
**	FUNCTION
**		Calculate vertical positions for all lines
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcView::calcVerticalPos(void)
{
	SynLhcLine *pLine;
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->calcVerticalLimits();
	}
	int delta = TOP_MARGIN;

	pLine = findLineOfType(SectLhcLine::OuterBeam);
	pLine->moveVertically(delta);

	pLine = findLineOfType(SectLhcLine::Passive);
	pLine->moveVertically(delta);

	pLine = findLineOfType(SectLhcLine::CrossBeam);
	pLine->moveVertically(delta);
	delta -= pLine->getDescent();

	delta += 2;
	pLine = findLineOfType(SectLhcLine::InnerBeam);
	pLine->moveVertically(delta);

	SynLhcLine *pInnerBeamLine = findLineOfType(SectLhcLine::InnerBeam),
				*pCrossBeamLine = findLineOfType(SectLhcLine::CrossBeam);
	totalHeight = BOTTOM_MARGIN + max(
		pInnerBeamLine->getY() + pInnerBeamLine->getDescent(),
		pCrossBeamLine->getY() + pCrossBeamLine->getDescent());

	// Write position of cross line to outer and inner beam lines
	pInnerBeamLine->setCrossLineY(pCrossBeamLine->getY());
	pCrossBeamLine->setCrossLineY(pCrossBeamLine->getY());

	pLine = findLineOfType(SectLhcLine::OuterBeam);
	pLine->setCrossLineY(pCrossBeamLine->getY());

	// Final geometry for sector summary icons
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		SynLhcLine *pLine = lines.at(idx);
		pLine->finalSectorIconsGeometry();
	}
}
