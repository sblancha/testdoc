//	Implementation of DebugCtl class
/////////////////////////////////////////////////////////////////////////////////

#include "DebugCtl.h"

bool	DebugCtl::data = false;
bool	DebugCtl::mainView = false;
bool	DebugCtl::axis = false;
bool	DebugCtl::history = false;
bool	DebugCtl::synoptic = false;
bool	DebugCtl::profile = false;
bool	DebugCtl::sms = false;
