#ifndef PLCCONFIG_H
#define PLCCONFIG_H

#include "VacCtlEqpDataExport.h"

// Class holding mobile equipment configuration in one PLC

#include "FileParser.h"

#include <qlist.h>

#include <QString>
class DataPool;

class VACCTLEQPDATA_EXPORT PlcConfig : public FileParser  
{
public:
	PlcConfig();
	virtual ~PlcConfig();

	// Create new instance
	static int createFromFile(FILE *pFile, int &lineNo,
		char **tokens, int nTokens, QStringList &errList);

	static void clear(void);
	static PlcConfig *findPlc(const char *name);

	// Implementation of FileParser abstract method
	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);

	// Access
	inline const char *getName(void) { return name; }
	inline const QStringList &getFlanges(void) { return *pFlanges; }
	inline const QStringList &getSectors(void) { return *pSectors; }

protected:
	// List of PlcConfig instances - it is required to find previously created instance
	static QList<PlcConfig *>	*pInstanceList;

	// PLC name
	char				*name;

	// List of all flagne names for mobile equipment
	QStringList	*pFlanges;

	// List of sector names for mobile equipment
	QStringList	*pSectors;
};

#endif	// PLCCONFIG_H
