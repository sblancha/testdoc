#ifndef MOBILECOLORLIST_H
#define	MOBILECOLORLIST_H

// Class holding list of colors for mobile equipment dialog

#include <QList>

#include <QColor>
#include <QPixmap>

class MobileColorList
{
public:

	static QPixmap *getVacIcon(int vacType, bool open);

	static QPixmap *getPrIcon(int index);

	static QPixmap *getBakeoutIcon(int channel);

	static QColor *getPrColor(int index);
	static QColor *getBakeoutColor(int channel);
	
	MobileColorList();
	~MobileColorList();

protected:
	// The only instance of this class
	static MobileColorList instance;

	// Array of QPixmaps for QRL vacuum (0=closed, 1 = open)
	QPixmap *qrlIcons[2];

	// Array of QPixmaps for CRYO vacuum (0=closed, 1 = open)
	QPixmap *cryoIcons[2];

	// Array of QPixmaps for RedBeam vacuum (0=closed, 1 = open)
	QPixmap *redBeamIcons[2];

	// Array of QPixmaps for BlueBeam vacuum (0=closed, 1 = open)
	QPixmap *blueBeamIcons[2];

	// Array of QPixmaps for CrossBeam vacuum (0=closed, 1 = open)
	QPixmap *crossBeamIcons[2];

	// Array of QPixmaps for AnyBeam vacuum (0=closed, 1 = open)
	QPixmap *anyBeamIcons[2];

	// List of icons for pressure values
	QList<QPixmap *>	prIcons;

	// List of icons for bakeout channels
	QList<QPixmap *>	tIcons;

	// List of colors used for temperatures curves
	QList<QColor *>	tColors;

	// List of colors used for pressure curves
	QList<QColor *>	prColors;

	void fillColors(void);
	void fillVacPixmaps(void);
	void fillPrPixmaps(void);
	void fillTPixmaps(void);
};

#endif	// MOBILECOLORLIST_H
