//	Simple test program for VacImport DLL

#include "ImportPool.h"

#include <qstrlist.h>

int main(int argc, char **argv)
{
	ImportPool &pool = ImportPool::getInstance();
	pool.setMobile(false);
	QStrList errList;
	printf("Start\n");
	int coco = pool.processFile("/home/kopylov/PVSS_projects/vac_lik_lhc_1/vac_lik_lhc_1/data/LHC.for_Import",
			"LHC", errList);
	printf("Finish, coco = %d\n", coco);
	for(char *msg = errList.first() ; msg ; msg = errList.next())
	{
		printf("%s\n", msg);
	}
	return coco;
}
