#
# Project file for qmake utility to build Synoptic for CRYO vacuum of all LHC
#
#	06.10.2010	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = SynopticAllLhcEwo

INCLUDEPATH += ../VacCtlEwoUtil \
	../VacCtlEwoUtil/SynopticAllLhc \
	../../common/VacCtlUtil \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/Platform \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp

HEADERS = SynopticAllLhcEwo.h

SOURCES = SynopticAllLhcEwo.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlHistoryData \
	-lVacCtlEqpData \
	-lVacCtlUtil

