#ifndef PLATFORMDEF_H
#define PLATFORMDEF_H

#include <qglobal.h>

#include <stdlib.h>
#include <string.h>

// Forced Q_ASSERT
#if defined(Q_ASSERT)
#undef Q_ASSERT
#endif

#define Q_ASSERT(cond) ((!(cond)) ? qt_assert(#cond,__FILE__,__LINE__) : qt_noop())

// Forced Q_ASSERT_X
#if defined(Q_ASSERT_X)
#undef Q_ASSERT_X
#endif

#define Q_ASSERT_X(cond, where, what) ((!(cond)) ? qt_assert_x(where, what,__FILE__,__LINE__) : qt_noop())

#ifdef Q_OS_WIN

#ifndef strcasecmp
#define strcasecmp _stricmp
#endif
#include <malloc.h>
#define alloca _alloca

#include <time.h>

#include <math.h>

// L.Kopylov 05.09.2016 #include <qapplication.h>

inline double rint(double value)
{
	long truncated = (long)value;
	if(fabs(truncated - value) < 0.5)
	{
		return truncated;
	}
	return truncated + (value < 0 ? -1 : 1);
	/*
	double result = value;
	if(value < 0)
	{
		double minVal = ceil(value);
		if((value - minVal) < 0.5)
		{
			result = minVal;
		}
		else
		{
			result = floor(value);
		}
	}
	else
	{
		double minVal = floor(value);
		if((value - minVal) < 0.5)
		{
			result = minVal;
		}
		else
		{
			result = ceil(value);
		}
	}
	return result;
	*/
}

#ifndef M_PI
#define	M_PI	(3.1415926)
#endif

#else

#include <math.h>

#endif	// Q_OS_WIN

/*
# ifdef _WIN32
#   define  DLL_CLASS_EXPORT __declspec(dllexport)
# else
#   define  DLL_CLASS_EXPORT
# endif
*/

// See http://lists.trolltech.com/qt4-preview-feedback/2006-06/thread00002-0.html
// The ling above suggests to export (using __declspec(dllexport)) not the
// whole class, but rather only public members used from outside DLL
//
// L.Kopylov 12.02.2009
// The method above works fine, but but not for classes with Q_OBJECT:
// methods from moc_* file are NOT exported, so singnal/slot machanism
// can not be used.
// there is another proposal from Qt: to use pointers instead of
// instances, ot even to use d-pointer pattern. Let's try...
/*
#ifdef _WIN32

// See http://support.microsoft.com/default.aspx?scid=KB;EN-US;168958
#ifndef IMP_CLASS_TEMPLATE

#define DECLSPECIFIER __declspec(dllexport)
#define EXPIMP_TEMPLATE

#else

#define DECLSPECIFIER __declspec(dllimport)
#define EXPIMP_TEMPLATE extern

#endif 

#endif
*/

#endif	// PLATFORMDEF_H
