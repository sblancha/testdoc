#ifndef	EqpVG_A_RO_H
#define	EqpVG_A_RO_H

//	Vacuum Generic Analog Gauge - Read only

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVG_A_RO : public Eqp
{
	Q_OBJECT

public:
	EqpVG_A_RO(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVG_A_RO(const EqpVG_A_RO &src) : Eqp(src) {}
#endif
	~EqpVG_A_RO();

	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::Pressure; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode mode);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
	virtual void getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end);
	virtual float getGrowFactorForDevList(void);
	virtual int getSpikeFilterForDevList(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline int getPR(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? prReplay : prOnline; }
	virtual int getErrorCode(DataEnum::DataMode mode);

	public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of PR DPE for online mode
	float	prOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of PR DPE for replay mode
	int	prReplay;

	// Last known value of objState for online mode
	unsigned objStateOnline;

	// Last known value of objState for replay mode
	unsigned objStateReplay;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, int pr, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, int pr, bool plcAlarm, QString &string);
	virtual void getMainValueString(unsigned rr1, int pr, bool plcAlarm, QString &string);
#endif

};


#endif	// EqpVG_A_RO_H
