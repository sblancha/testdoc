//	Implementation of LegendLabel class
/////////////////////////////////////////////////////////////////////////////////

#include "LegendLabel.h"

LegendLabel::LegendLabel(int x, int y, const char *text)
{
	this->x = x;
	this->y = y;
	this->text = text;
	angle = 0;
}

LegendLabel::LegendLabel(int x, int y, int angle, const char *text)
{
	this->x = x;
	this->y = y;
	this->angle = angle;
	this->text = text;
}
