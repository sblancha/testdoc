
/**@name LIBRARY: lhcVacMobile.ctl

@author: Leonid Kopylov (IHEP, Protvino, Russia)

Creation Date: 14/06/2006

Modification History: 
  14.06.2008
  Porting to PVSS 3.6
  03.05.2018
  Add constants for WireLess Mobile {sblancha}

version 1.1

External functions:	
								
Internal function:

Purpose:
This library contains constants declaration for common support of mobile vacuum equipment
and support for future versions of import

Usage: Public

PVSS manager usage: UI (PVSS00NV), UI extended (PVSS00NG), CTRL

Constraints: 
	. PVSS version: from 3.6
	. operating system: Linux and Windows
	. distributed system: yes.
*/
#uses "vclDevCommon.ctl"	// Load CTRL library

// @brief pumping group DP Type
const string VPG_DP_TYPE = "VP_GU";
// @brief bakeout cabinet DP Type
const string VRER_DP_TYPE = "VR_ER";
// @brief bakeout regulation channel DP Type
const string VREC_DP_TYPE = "VR_EC";
// @brief bakeout alarm channel DP Type
const string VREA_DP_TYPE = "VR_EA";

//=====================================================================================
//============= Classification of equipment with respect to it's mobility =============
//=====================================================================================

// Classification of equipment: fixed equipment
const int VAC_MOBILE_TYPE_FIXED = 0;

// Classification of equipment: mobile equipment container
const int VAC_MOBILE_TYPE_CONTAINER = 1;

// Classification of equipment: mobile equipment on flange
const int VAC_MOBILE_TYPE_ON_FLANGE = 2;

// Classification of equipment: mobile equipment on sector
const int VAC_MOBILE_TYPE_ON_SECTOR = 3;

// Classification of equipment: special DP containing list of mobile equipment in system
const int VAC_MOBILE_TYPE_EQP_LIST = 4;

// Classification of equipment: summary pressure of isolation vacuum sector
const int VAC_MOBILE_TYPE_SUM_ISOL_SECTOR_PR = 5;

// Classification of equipment: PLC alarm (to be reported to LASER)
const int VAC_MOBILE_TYPE_PLC_ALARM = 6;

// Classification of equipment: PLC health counter
const int VAC_MOBILE_TYPE_PLC_COUNTER = 7;

// Classification of equipment DP: Master Wireless Eqp for TCP communication
const int VAC_MOBILE_TYPE_MASTER = 8;

// Classification of equipment DP: Position Instance Wireless Eqp for TCP communication
const int VAC_MOBILE_TYPE_POSINSTANCE = 9;

//=====================================================================================
//============= State of mobile equipment connection as they reported by PLC ==========
//=====================================================================================

// Connection ready signal has been sent to PVSS
const int VAC_MOBILE_PLC_CONNECT_READY_TO_PVSS = 12;

// Activation OK signal has been received from PVSS
const int VAC_MOBILE_PLC_CONNECT_ACTIVATE_OK = 13;

// Data transfer slave PLC -> PVSS is in progress - start of state range
const int VAC_MOBILE_PLC_CONNECT_DATA_TRANSFER_START = 14;

// Data transfer slave PLC -> PVSS is in progress - end of state range
const int VAC_MOBILE_PLC_CONNECT_DATA_TRANSFER_END = 127;

//=====================================================================================
//============= Types of connected mobile equipment as they reported by PLC ===========
//=====================================================================================

// Type of connected equipment - mobile pumping group type A (insulation roughing)
const int VAC_MOBILE_EQP_TYPE_VPGMA = 30;

// Type of connected equipment - mobile pumping group type B (insulation turbo)
const int VAC_MOBILE_EQP_TYPE_VPGMB = 31;

// Type of connected equipment - mobile pumping group type C (beam arc turbo)
const int VAC_MOBILE_EQP_TYPE_VPGMC = 32;

// Type of connected equipment - mobile pumping group type D (beam LSS turbo)
const int VAC_MOBILE_EQP_TYPE_VPGMD = 33;

// Type of connected equipment - mobile pumping group type D (beam LSS turbo)
const int VAC_MOBILE_EQP_TYPE_VPGME = 34;

// Type of connected equipment - bake-out rack
const int VAC_MOBILE_EQP_TYPE_BAKEOUT_RACK = 40; // Used by Profibus Mobile system only
const int VAC_MOBILE_EQP_TYPE_VREMA = 40;
const int VAC_MOBILE_EQP_TYPE_VREMB = 41;
//=====================================================================================
//=====Front End Mobile State reported by PLC Server in Wireless mobile system ========
//=====================================================================================
/// @brief Front End Mobile State
const int FE_RR1_MOBILE_READY_MASK      = 0x0800u; // 08 00

//=====================================================================================
//============= Names of special DP types and/or DPs ==================================
//=====================================================================================

// Name of DP with list of active mobile equipment - one DP per system
const string VAC_MOBILE_LIST_DP = "_VacMobileList";
// Name of DP with list of active Wireless mobile equipment - one DP per system
const string VAC_MOBILEWL_LIST_DP = "_VacWlActiveDpList";

// VacConnectToMobileList
/**
Purpose:
Connect to DP containing list of active mobile equipment

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/

void VacConnectToMobileList(string workProc)
{
  //PROFIBUS MOBILE  
  if(dpExists(VAC_MOBILE_LIST_DP)) {
    // First report all existing BAKEOUT DPs...
    dyn_string dpList = dpNames("*", "VBAKEOUT");
    for(int n = dynlen(dpList) ; n > 0 ; n--)
    {
      string dpName = dpSubStr(dpList[n], DPSUB_DP);
      LhcVacAddBakeoutWorkDP(dpName);
    }

    // ... then connect to list of active mobile equipment
    if(workProc == "")
    {
      workProc = "VacMobileListCb";
    }
    dpConnect(workProc,
      VAC_MOBILE_LIST_DP + ".DP_Names",
      VAC_MOBILE_LIST_DP + ".WorkDPNames",
      VAC_MOBILE_LIST_DP + ".SourceDpNames",
      VAC_MOBILE_LIST_DP + ".State");
  }
  //WIRELESS MOBILE
  if(dpExists(VAC_MOBILEWL_LIST_DP)) {
    dpConnect("VacMobileWlListCb", 
        VAC_MOBILEWL_LIST_DP + ".tcpDpNames",
        VAC_MOBILEWL_LIST_DP + ".masterDpNames",
        VAC_MOBILEWL_LIST_DP + ".posInstanceDpNames");
  }
}

void VacMobileListCb(string dpe1, dyn_string mobileDpNames,
  string dpe2, dyn_string workDpNames, string dpe3, dyn_string sourceDpNames,
  string dpe4, dyn_bool activeFlags)
{
  LhcVacSetMobileEqp(mobileDpNames, workDpNames, sourceDpNames, activeFlags);
}

void VacMobileWlListCb(string dpe1, dyn_string tcpDps, 
                       string dpe2, dyn_string masterDps, 
                       string dpe3, dyn_string instanceDps) {
  //DebugTN("WL call back called",   instanceDps, masterDps, tcpDps);
  LhcVacSetWireLessEqp(instanceDps, masterDps, tcpDps);
}

/**
@brief Append dyn with dp names filtered by dp type and mobile type
@param[in, out]  dpNames             DP names to append                
@param[in]       dpTypeFilter        DP type filter
@param[in]       mobileTypeFilter    Mobile type Filter
@param[in, out]  exceptionInfo       Exception Info
*/
void AppendDpNamesMobileTypeFiltered(dyn_string &dps, string dpTypeFilter, string mobileTypeFilter, dyn_string &exceptionInfo) {    
  dyn_string DpNamesList;
  dynClear(DpNamesList);
  //DebugTN("AppendDpNamesMobileTypeFiltered(): dpType filter = " + dpTypeFilter);  
  DpNamesList = dpNames("*", dpTypeFilter);
  //DebugTN("AppendDpNamesMobileTypeFiltered(): dp List :", DpNamesList);  
  //Keep only instance DP with mobileType = POSINSTANCE
  bool isActive;
  int mobileType;
  for (int n = dynlen(DpNamesList); n > 0; n--) {
    string dpName = dpSubStr(DpNamesList[n], DPSUB_DP);
    LhcVacMobileProp(dpName, mobileType, isActive, exceptionInfo);
    //DebugTN("AppendDpNamesMobileTypeFiltered(): mobile type = " +  mobileType);  
    if (mobileType == mobileTypeFilter)
      dynAppend(dps, dpName);
  }
}
