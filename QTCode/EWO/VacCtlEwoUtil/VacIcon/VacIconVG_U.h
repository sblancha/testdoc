// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Component Name: VacCtlEwoUtil librairy
Class: Widget Vacuum Gauge - Unified (VG_U)
Language: Qt/C++

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: This file contains the defined classes for the icon and widget Vacuum Gauge - Unified (VP_U)

Limitations: To be used with vacuum framework.

Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------
#ifndef	VACICONVG_U_H
#define	VACICONVG_U_H

#include "VacIcon.h"

class Eqp;

class VACCTLEWOUTIL_EXPORT VacIconVG_U : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVG_U(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVG_U();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual VacIconContainer::Direction getSlaveDirection(void) const;
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);
	virtual void drawInterlockSrcArrow(QPainter &painter, bool before, bool after);

protected:
	// Pointer to my device
	Eqp *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus);
	virtual void drawSpecific(QPainter &painter) = 0;
	void setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus);
	virtual void drawPressureValue(QPainter &painter, QString &sPr);
};

#endif	// VACICONVG_U_H
