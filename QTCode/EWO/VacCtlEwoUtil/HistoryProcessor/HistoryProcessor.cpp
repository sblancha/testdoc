//	Implementation of HistoryProcessor class
/////////////////////////////////////////////////////////////////////////////////

#include "HistoryProcessor.h"

#include "ExcelWriter.h"

#include "Eqp.h"


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

HistoryProcessor::HistoryProcessor()
{
	pEqpList = new QList<HistoryEqpValuesSet *>();
	interval = 0;
	pLastSet = NULL;
}

HistoryProcessor::~HistoryProcessor()
{
	clear();
	delete pEqpList;
}

/*
**	FUNCTION
**		Clear content
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryProcessor::clear(void)
{
	while(!pEqpList->isEmpty())
	{
		delete pEqpList->takeFirst();
	}
	pLastSet = NULL;
}

/*
**	FUNCTION
**		Add value to list of values for given device
**
**	ARGUMENTS
**		pEqp	- Pointer to device
**		time	- Time stamp of value to be added
**		value	- value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryProcessor::addValue(Eqp *pEqp, const QString &bitDpe, int bitNbr, const QString &bitName, const QDateTime &time, float value)
{
	//qDebug("HistoryProcessor::addValue(%s, %s, %d)\n", pEqp->getDpName(), bitDpe.toAscii().constData(), bitNbr);
	if(pLastSet)
	{
		if((pLastSet->getEqp() == pEqp) && (pLastSet->getBitDpe() == bitDpe) && (pLastSet->getBitNbr() == bitNbr))
		{
			pLastSet->addValue(time, value);
			return;
		}
	}
	for(int idx = pEqpList->count() - 1 ; idx >= 0 ; idx--)
	{
		pLastSet = pEqpList->at(idx);
		if((pLastSet->getEqp() == pEqp) && (pLastSet->getBitDpe() == bitDpe) && (pLastSet->getBitNbr() == bitNbr))
		{
			pLastSet->addValue(time, value);
			return;
		}
	}
	if(pEqpList->count() > 253)
	{
		return;	// Excel will not be able to show > 255 columns
	}

	pLastSet = new HistoryEqpValuesSet(pEqp, bitDpe, bitNbr, bitName);
	pEqpList->append(pLastSet);
	pLastSet->addValue(time, value);
}

void HistoryProcessor::setPreviousValue(Eqp *pEqp, const QString &bitDpe, int bitNbr, const QString &bitName, const QDateTime &minTime, float value) {
	if (pLastSet) {
		if ((pLastSet->getEqp() == pEqp) && (pLastSet->getBitDpe() == bitDpe) && (pLastSet->getBitNbr() == bitNbr)) {
			pLastSet->setPreviousValue(minTime, value);
			return;
		}
	}
	for (int idx = pEqpList->count() - 1; idx >= 0; idx--) {
		pLastSet = pEqpList->at(idx);
		if ((pLastSet->getEqp() == pEqp) && (pLastSet->getBitDpe() == bitDpe) && (pLastSet->getBitNbr() == bitNbr)) {
			pLastSet->setPreviousValue(minTime, value);
			return;
		}
	}
	if (pEqpList->count() > 253) {
		return;	// Excel will not be able to show > 255 columns
	}

	pLastSet = new HistoryEqpValuesSet(pEqp, bitDpe, bitNbr, bitName);
	pEqpList->append(pLastSet);
	pLastSet->setPreviousValue(minTime, value);
}

/*
**	FUNCTION
**		Add value to list of values for given device
**
**	ARGUMENTS
**		pEqp	- Pointer to device
**		dpe		- DPE name of this device
**		time	- Time stamp of value to be added
**		value	- value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryProcessor::addValue(Eqp *pEqp, const QString &dpe, const QDateTime &time, float value)
{
	if(pLastSet)
	{
		if((pLastSet->getEqp() == pEqp) && (pLastSet->getDpe() == dpe))
		{
			pLastSet->addValue(time, value);
			return;
		}
	}
	for(int idx = pEqpList->count() - 1 ; idx >= 0 ; idx--)
	{
		pLastSet = pEqpList->at(idx);
		if((pLastSet->getEqp() == pEqp) && (pLastSet->getDpe() == dpe))
		{
			pLastSet->addValue(time, value);
			return;
		}
	}
	if(pEqpList->count() > 253)
	{
		return;	// Excel will not be able to show > 255 columns
	}

	pLastSet = new HistoryEqpValuesSet(pEqp, dpe);
	pEqpList->append(pLastSet);
	pLastSet->addValue(time, value);
}

void HistoryProcessor::setPreviousValue(Eqp *pEqp, const QString &dpe, const QDateTime &minTime, float value) {
	if (pLastSet) {
		if ((pLastSet->getEqp() == pEqp) && (pLastSet->getDpe() == dpe)) {
			pLastSet->setPreviousValue(minTime, value);
			return;
		}
	}
	for (int idx = pEqpList->count() - 1; idx >= 0; idx--) {
		pLastSet = pEqpList->at(idx);
		if ((pLastSet->getEqp() == pEqp) && (pLastSet->getDpe() == dpe)) {
			pLastSet->setPreviousValue(minTime, value);
			return;
		}
	}
	if (pEqpList->count() > 253) {
		return;	// Excel will not be able to show > 255 columns
	}

	pLastSet = new HistoryEqpValuesSet(pEqp, dpe);
	pEqpList->append(pLastSet);
	pLastSet->setPreviousValue(minTime, value);
}

/*
**	FUNCTION
**		Write collected values to output file
**
**	ARGUMENTS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		Empty string in case of success; or
**		string with error message in case of error
**
**	CAUTIONS
**		None
*/
QString HistoryProcessor::write(FILE *pFile, bool writeCsv)
{
	ExcelWriter writer;

	return write(pFile, &writer, writeCsv);
}

/*
**	FUNCTION
**		Write collected values to output file using given Excel writer instance
**
**	ARGUMENTS
**		pFile	- Pointer to file opened for writing
**		PWriter	- Pointer to instance of Excel writer
**
**	RETURNS
**		Empty string in case of success; or
**		string with error message in case of error
**
**	CAUTIONS
**		None
*/
QString HistoryProcessor::write(FILE *pFile, ExcelWriter *pWriter, bool writeCsv)
{
	if(!pFile)
	{
		return QString("No output file");
	}
	if(!pEqpList->count())
	{
		return QString("No history values for output");
	}

	// Prepare column titles and geometry
	pWriter->clear();

	// First column - timestamp
	pWriter->setColumnTitle(1, "Timestamp");
	pWriter->setColumnWidth(1, 100);

	// Next columns - devices
	int col = 2, idx;
	for(idx = 0 ; idx < pEqpList->count() ; idx++)
	{
		HistoryEqpValuesSet *pSet = pEqpList->at(idx);
		QString title(pSet->getEqp()->getVisibleName());
		if(!pSet->getBitName().isEmpty())
		{
			title += ":";
			title += pSet->getBitName();
		}
		else if(!pSet->getDpe().isEmpty())
		{
			title += ".";
			title += pSet->getDpe();
		}
		pWriter->setColumnTitle(col, title);
		pWriter->setColumnWidth(col, 120);
		pSet->setColumn(col++);
	}

	// Build array of all timestamps
	QList<QDateTime> allTimesList;
	if(!buildAllTimes(allTimesList))
	{
		return QString("No values for devices");
	}

	// Prepare data for Excel sheet(s)
	QDateTime lastTime = allTimesList.first().addYears(-10);
	for(int n = 0 ; n < allTimesList.count() ; n++)
	{
		if(lastTime.secsTo(allTimesList.at(n)) < interval)
		{
			continue;
		}
		int row = pWriter->addRow();
		lastTime = allTimesList.at(n);
		pWriter->setCellValue(row, 1, lastTime, true);
		pWriter->setCellFormat(row, 1, ExcelWriterCell::DateTimeWithMsec);
		for(idx = 0 ; idx < pEqpList->count() ; idx++)
		{
			HistoryEqpValuesSet *pSet = pEqpList->at(idx);
			switch(pSet->getEqp()->getMainValueType())
			{
			case DataEnum::FillNumber:
				pWriter->setCellValue(row, pSet->getColumn(), (int)pSet->getValueAt(lastTime));
				pWriter->setCellFormat(row, pSet->getColumn(), ExcelWriterCell::Integer);
				break;
			default:
				pWriter->setCellValue(row, pSet->getColumn(), pSet->getValueAt(lastTime));
				pWriter->setCellFormat(row, pSet->getColumn(), ExcelWriterCell::Float);
				break;
			}
		}
	}
	// Free timestamp array - not required anymore
	allTimesList.clear();
	if(writeCsv)
	{
		return pWriter->writeCsv(pFile);
	}
	return pWriter->write(pFile);
}

/*
**	FUNCTION
**		Build array of all timestamps for all devices in order
**
**	ARGUMENTS
**		nAllTimes	- Variable where number of times in list will be written
**
**	RETURNS
**		Pointer to array of times; or
**		NULL if no times were found
**
**	CAUTIONS
**		None
*/
int HistoryProcessor::buildAllTimes(QList<QDateTime> &allTimesList)
{
	// Prepare
	int idx;
	for(idx = 0 ; idx < pEqpList->count() ; idx++)
	{
		HistoryEqpValuesSet *pSet = pEqpList->at(idx);
		pSet->setStartIndex();
	}

	// Do the job
	QDateTime lastTime = QDateTime::currentDateTime().addYears(-100);
	while(true)
	{
		HistoryEqpValuesSet *pBestSet = NULL;
		QDateTime bestTime(QDateTime::currentDateTime().addYears(100));
		for(idx = 0 ; idx < pEqpList->count() ; idx++)
		{
			HistoryEqpValuesSet *pSet = pEqpList->at(idx);
			QDateTime time;
			if(!pSet->getTimeAt(pSet->getIndex(), time))	// No more values in this set
			{
				continue;
			}
			if(time < bestTime)
			{
				bestTime = time;
				pBestSet = pSet;
			}
		}
		if(!pBestSet)
		{
			break;	// No more values in all sets
		}
		pBestSet->setIndex(pBestSet->getIndex() + 1);
		if(bestTime != lastTime)	// New time, add to array
		{
			allTimesList.append(bestTime);
			lastTime = bestTime;
		}
	}
	return allTimesList.count();
}

