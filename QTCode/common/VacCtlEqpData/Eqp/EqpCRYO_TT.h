#ifndef	EQPCRYO_TT_H
#define	EQPCRYO_TT_H

//	CRYO_TT - CRYO thermometrs

#include "Eqp.h"


class VACCTLEQPDATA_EXPORT EqpCRYO_TT : public Eqp
{
	Q_OBJECT

public:
	EqpCRYO_TT(const Eqp &source);
#ifdef Q_OS_WIN
	EqpCRYO_TT(const EqpCRYO_TT &src) : Eqp(src) {}
#endif
	~EqpCRYO_TT();

	typedef enum
	{
		QrlLineB = 1,
		QrlLineC = 2,
		CmAvg = 3,
		CmMin = 4,
		CmMax = 5,
		RF = 6
	} ThermometerType;

	virtual inline bool isCryoThermometer(void) const { return true; }
	virtual inline bool isValveInterlock(void) const { return valveInterlock; }
	virtual inline void setValveInterlock(bool flag) { valveInterlock = flag; }
	virtual int getVacType(void) const;

	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::CryoTemperature; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode mode);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
	virtual void getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end);
	virtual float getGrowFactorForDevList(void);
	virtual int getSpikeFilterForDevList(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline unsigned getRR1(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline float getT(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? tReplay : tOnline; }
	
protected:
	// Nominal temperature for this thermometer (2 or 4, constant)
	int		nominalT;

	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of T DPE for online mode
	float	tOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of T DPE for replay mode
	float	tReplay;

	// Flag indicating if this thermometer is used as valve interlock
	bool		valveInterlock;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, float t, QColor &color);
	virtual void getMainValueString(unsigned rr1, float t, QString &string);
#endif
};

#endif	// EQPCRYO_TT_H
