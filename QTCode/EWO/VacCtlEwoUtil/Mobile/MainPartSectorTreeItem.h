#ifndef	MAINPARTSECTORTREEITEM_H
#define	MAINPARTSECTORTREEITEM_H

//	Item of tree view representing main part or sector

#include <QTreeWidget>

#include "MobileTreeItemTypes.h"

class MainPartSectorTreeItem : public QTreeWidgetItem
{
public:
	MainPartSectorTreeItem(QTreeWidget *parent, const QString &text, int vacType);
	MainPartSectorTreeItem(QTreeWidgetItem *parent, const QString &text, int vacType);
	~MainPartSectorTreeItem();

	virtual void setExpanded(bool open);

protected:
	// Vacuum type for this item
	int	vacType;

};

#endif	// MAINPARTSECTORTREEITEM_H
