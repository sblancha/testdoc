#ifndef EQPUSAGEFILTER_H
#define	EQPUSAGEFILTER_H

#include "VacCtlOnlineNotificationExport.h"

// Additionl filtering of equipment based on it's usage

#include "EqpUsageFilterItem.h"

#include <QList>
#include <QDateTime>	// For debugging only

class Eqp;

class VACCTLONLINENOTIFICATION_EXPORT EqpUsageFilter
{
public:
	EqpUsageFilter();
	~EqpUsageFilter();

	inline void clear(void) { filterList.clear(); matchingList.clear(); matchingListReady = false; }
	void addFilterItem(int funcTypeId, QByteArray attrName, bool reverse);
	inline void setMainType(int type) { funcType = type; matchingList.clear(); matchingListReady = false; }
	inline bool eqpMatches(Eqp *pEqp) { return filterList.count() == 0 ? true : checkEqp(pEqp); }

protected:
	QList<EqpUsageFilterItem *>	filterList;

	// It takes too long to check signle device against criteria: ~6-8 msec per device,
	// which is not acceptable when several hundreds/thousands of devices to be checked.
	// So list of devices matching criteria is built once and then checkDevice() only checks
	// if device is in that list or not. Keeping list of memory means extra memory usage,
	// but let's assume modern computers have enough memory installed.
	// By this reason here we also need to know main functional type which will use this
	// criteria.
	int		funcType;

	QList<Eqp *>matchingList;

	bool	matchingListReady;

	bool checkEqp(Eqp *pEqp);
	void buildMatchingList(void);
	void addAllMatching(bool hasCriteria);
	void removeReverseMatching(bool hasCriteria);
	void buildMainFuncTypeList(QList<int> &funcTypeList);
};

#endif	// EQPUSAGEFILTER_H

