#
# Project file for qmake utility to build Profile all of SPS EWO
#
#	19.10.2010	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = ProfileAllSpsEwo

INCLUDEPATH += ../VacCtlEwoUtil \
	../VacCtlEwoUtil/ProfileAllSPS \
	../VacCtlEwoUtil/Profile \
	../VacCtlEwoUtil/Axis \
	../../common/VacCtlUtil \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/Platform \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp

HEADERS = ProfileAllSpsEwo.h

SOURCES = ProfileAllSpsEwo.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlHistoryData \
	-lVacCtlEqpData \
	-lVacCtlUtil

