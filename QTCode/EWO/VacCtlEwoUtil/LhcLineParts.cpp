//	Implementation of LhcLineParts class
/////////////////////////////////////////////////////////////////////////////////

#include "LhcLineParts.h"
#include "VacLinePart.h"

#include "DataPool.h"
#include "Sector.h"
#include "LhcRegion.h"

#include "Eqp.h"
#include "EqpType.h"

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

LhcLineParts::LhcLineParts(float start, float end)
{
	this->start = start;
	this->end = end;
	pPartList = new QList<VacLinePart *>();
}

LhcLineParts::~LhcLineParts()
{
	clear();
	delete pPartList;
}

/*
**	FUNCTION
**		Build all line parts
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcLineParts::buildLineParts(void)
{
	clear();

	includesIP1 = start > end;

	BuildType	method = linePartMethod();

	// Line parts can be built differently
	switch(method)
	{
	case TypeSector:
		buildSectorBasedParts();
		break;
	case TypeBeam:
		buildBeamBasedParts();
		break;
	default:	// To make Linux compiler happy
		break;
	}

	// Decide for which line parts start/end shall be drawn
	// Start/end shall NOT be drawn if neighbour part ends/starts
	// at the same coordinate. This is only required for QRL and CRYO lines
	if(method == TypeSector)
	{
		DataPool &pool = DataPool::getInstance();
		QList<Sector *> &sectList = pool.getSectors();
		for(int partIdx = 0 ; partIdx < pPartList->count() ; partIdx++)
		{
			VacLinePart *pPart = pPartList->at(partIdx);
			pPart->setDrawStart(true);
			pPart->setDrawEnd(true);
			for(int sectIdx = 0 ; sectIdx < sectList.count() ; sectIdx++)
			{
				Sector *pSector = sectList.at(sectIdx);
				if(pSector->getVacType() == pPart->getVacType())
				{
					if(pSector->getEnd() == pPart->getRealStart())
					{
						pPart->setDrawStart(false);
					}
					if(pSector->getStart() == pPart->getRealEnd())
					{
						pPart->setDrawEnd(false);
					}
				}
				if((!pPart->isDrawStart()) && (!pPart->isDrawEnd()))
				{
					break;
				}
			}
		}
	}
	else
	{
		for(int partIdx = 0 ; partIdx < pPartList->count() ; partIdx++)
		{
			VacLinePart *pPart = pPartList->at(partIdx);
			pPart->setDrawStart(true);
			pPart->setDrawEnd(true);
		}
	}
}

/*
**	FUNCTION
**		Build all line parts to be shown on this line - line parts
**		are built based on sector geometry. This approach is used
**		for QRL and CRYO vacuum where sector coordinates are primary
**		source of data.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcLineParts::buildSectorBasedParts(void)
{
	QList<Sector *> &sectList = DataPool::getInstance().getSectors();
	
	// Find first sector to use
	Sector *pSector = NULL;
	int sectIdx;
	for(sectIdx = 0 ; sectIdx < sectList.count() ; sectIdx++)
	{
		Sector *pSect = sectList.at(sectIdx);
		if(!isMySector(pSect))
		{
			continue;
		}
		if(pSect->getEnd() > start)
		{
			pSector = pSect;
			break;
		}
	}
	if((!pSector) && (start < end))
	{
		return;	// No line parts
	}

	// Add sectors after first sector found
	for( ; sectIdx < sectList.count() ; sectIdx++)
	{
		pSector = sectList.at(sectIdx);
		if(!isMySector(pSector))
		{
			continue;
		}
		if(start < end)	// Otherwise coordinate range includes IP1
		{
			if(pSector->getStart() >= end)		// LIK 15.04.2008 - before condition was '>'
			{
				break;
			}
		}
		addLinePart(pSector, false);
	}
	if(start < end)
	{
		return;
	}

	// Add parts from IP1 (zero coordinate) to end of coordinates range
	for(sectIdx = 0 ; sectIdx < sectList.count() ; sectIdx++)
	{
		pSector = sectList.at(sectIdx);
		if(!isMySector(pSector))
		{
			continue;
		}
		if(pSector->getStart() >= end)		// LIK 15.04.2008 - before condition was '>'
		{
			break;
		}
		addLinePart(pSector, true);
	}
}

/*
**	FUNCTION
**		Build all line parts to be shown on this line - line parts
**		are built based on beam vacuum locations. This approach is used
**		for beam vacuums where beam location coordinates are primary
**		source of data.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcLineParts::buildBeamBasedParts(void)
{
	bool	afterIp1 = false;

	DataPool &pool = DataPool::getInstance();
	QList<LhcRegion *> &locs = pool.getLhcBeamLocs();

	// Find first beam location to use
	// It must be really FIRST location - the order is important, so first
	// let's find the location which is NOT used for sure, then find location
	// to use starting search from location NOT used.
	// This approach has a weak point: it will not work if ALL of LHC shall
	// be displayed, but this never happens in real life
	LhcRegion *pStartRegion = NULL, *pRegion;
	int idx;
	for(idx = 0 ; idx < locs.count() ; idx++)
	{
		pRegion = locs.at(idx);
		if(!isLocationInRange(pRegion))
		{
			pStartRegion = pRegion;
			break;
		}
	}

	// Now find first location to use
	pRegion = pStartRegion;
	while(true)
	{
		if(!pRegion)
		{
			idx = 0;
			pRegion = locs.at(idx);
		}
		if(isLocationInRange(pRegion))
		{
			pStartRegion = pRegion;
			break;
		}
		idx++;
		if(idx >= locs.count())
		{
			idx = 0;
		}
		pRegion = locs.at(idx);
		if(pRegion == pStartRegion)
		{
			return;	// Should never happen, just prevent infinite loop
		}
	}

	// Add parts after first beam location found
	pRegion = pStartRegion;
	while(true)
	{
		//qDebug("region from %f to %f type %d afterIp1 %s\n", pRegion->getStart(), pRegion->getEnd(), pRegion->getType(), (afterIp1 ? "TRUE" : "FALSE"));
		if(!isLocationInRange(pRegion))
		{
			//qDebug("break\n");
			break;
		}
		int	vacType = vacTypeOfRegion(pRegion->getType());
		if(vacType != VacType::None)
		{
			addLinePart(vacType, pRegion, afterIp1);
		}
		if(pRegion->getStart() > pRegion->getEnd())
		{
			if(start > end)	// LIK 10.03.2008 - if() condition added
			{
				afterIp1 = pPartList->count() > 0;
			}
		}
		idx++;
		if(idx >= locs.count())
		{
			idx = 0;
			if(start > end)	// LIK 11.11.2011 - if() condition added
			{
				afterIp1 = pPartList->count() > 0;
			}
		}
		pRegion = locs.at(idx);
		if(pRegion == pStartRegion)
		{
			break;
		}
	}
}

/*
**	FUNCTION
**		Check if given region of LHC ring overlaps (at least partially)
**		with coordinates range
**
**	PARAMETERS
**		pRegion	- Pointer to region in question
**
**	RETURNS
**		true	- If region overlaps with coordinate range;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool LhcLineParts::isLocationInRange(LhcRegion *pRegion)
{
	if(includesIP1)
	{
		if(pRegion->getStart() > pRegion->getEnd())	// Region also include IP1
		{
			if(start >= pRegion->getStart())
			{
				return true;
			}
			if(end <= pRegion->getEnd())
			{
				return true;
			}
			if((start < pRegion->getStart()) && (end > pRegion->getEnd()))
			{
				return true;
			}
		}
		else	// Region does not include IP1
		{
			if(pRegion->getStart() <= end)
			{
				return true;
			}
			if(pRegion->getEnd() >= start)
			{
				return true;
			}
		}
	}
	else	// Coordinate range does not include IP1
	{
		if(pRegion->getStart() > pRegion->getEnd())	// Region includes IP1
		{
			if(end >= pRegion->getStart())
			{
				return true;
			}
			if(start <= pRegion->getEnd())
			{
				return true;
			}
		}
		else	// Region does not include IP1
		{
			if(start > pRegion->getEnd())
			{
				return false;
			}
			if(end < pRegion->getStart())
			{
				return false;
			}
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Add one part of line to list of parts - part corresonds to vacuum sector.
**		If sector contains connection to another line (DSL) - add DSL sector BEFORE
**		adding this sector
**
**	PARAMETERS
**		pSector		- Pointer to sector corresponding to new line part
**		afterIp1	- true if we passed IP1 in our search
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void LhcLineParts::addLinePart(Sector *pSector, bool afterIp1)
{
	// Check if sector contains start/end of another line (DSL)
	DataPool &pool = DataPool::getInstance();
	BeamLine *pLhc = pool.getCircularLine();
	if(pLhc)
	{
		QList<BeamLinePart *> &parts = pLhc->getParts();
		for(int idx = 0 ; idx < parts.count() ; idx++)
		{
			BeamLinePart *pPart = parts.at(idx);
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if(pEqp->getVacType() != pSector->getVacType())
				{
					continue;
				}
				if(pEqp->getStart() > pSector->getEnd())
				{
					break;
				}
				if(pEqp->getStart() < pSector->getStart())
				{
					continue;
				}
				if((pEqp->getType() != EqpType::OtherLineStart) && (pEqp->getType() != EqpType::OtherLineEnd))
				{
					continue;
				}
				// Start or end of another line has been found, find sector on that line
				// IT IS EXPECTED THAT child line only contains one sector
				BeamLine *pChildLine = pool.getLine(pEqp->getName());
				if(pChildLine)
				{
					Sector *pChildSector = NULL;
					QList<BeamLinePart *> &childParts = pChildLine->getParts();
					for(int childIdx = 0 ; childIdx < childParts.count() ; childIdx++)
					{
						BeamLinePart *pChildPart = childParts.at(childIdx);
						Eqp **childEqpPtrs = pChildPart->getEqpPtrs();
						int nChildEqpPtrs = pChildPart->getNEqpPtrs();
						for(int childEqpIdx = 0 ; childEqpIdx < nChildEqpPtrs ; childEqpIdx++)
						{
							pEqp = childEqpPtrs[childEqpIdx];
							if((pChildSector = pEqp->getSectorBefore()))
							{
								break;
							}
						}
						if(pChildSector)
						{
							break;
						}
					}
					if(pChildSector)	// Add VERY SMALL line part
					{
						addLinePart(pChildSector->getVacType(),
							(float)(pSector->getStart() - 0.5),
							(float)(pSector->getStart() - 0.01), afterIp1);
						VacLinePart *pPart = pPartList->last();
						pPart->setSector(pChildSector);
						pPart->setStarted(true);
						pPart->setFinished(true);
					}
					break;	// Stop search anyway
				}
			}
		}
	}

	addLinePart(pSector->getVacType(), pSector->getStart(), pSector->getEnd(), afterIp1);
	VacLinePart *pPart = pPartList->last();
	pPart->setSector(pSector);
}

/*
**	FUNCTION
**		Add one part of line to list of parts - part corresonds to beam line part
**
** PARAMETERS
**		vacType		- Vacuum type for new part
**		pRegion		- Pointer to LHC region corresponding to new line part
**		afterIp1	- true if we passed IP1 in our search
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void LhcLineParts::addLinePart(int vacType, LhcRegion *pRegion, bool afterIp1)
{
	addLinePart(vacType, pRegion->getStart(), pRegion->getEnd(), afterIp1);
}

/*
**	FUNCTION
**		Add one part of line to list of parts
**
**	PARAMETERS
**		partVacType	- Vacuum type of new part
**		partStart	- Start coordinate of new part
**		partEnd		- End coordinate of new part
**		afterIp1	- true if we passed IP1 in our search
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcLineParts::addLinePart(int partVacType, float partStart, float partEnd, bool afterIp1)
{
//	qDebug("addLinePart(vacType %d start %f end %f afterIp1 %s)\n", partVacType, partStart, partEnd, (afterIp1 ? "TRUE" : "FALSE"));
	VacLinePart *pPrevPart = NULL;
	if(!pPartList->isEmpty())
	{
		pPrevPart = pPartList->last();
	}
	VacLinePart *pPart = new VacLinePart(partVacType, partStart, partEnd, afterIp1);
	pPartList->append(pPart);

	if(afterIp1)	// Adding parts after passing IP1
	{
		if(partStart > partEnd)	// Part itself includes IP1
		{
			pPart->setStartPos(partStart);
		}
		else	// Really part AFTER IP1
		{
			pPart->setStartPos(partStart + DataPool::getInstance().getLhcRingLength());
		}
		pPart->setEndPos(partEnd + DataPool::getInstance().getLhcRingLength());
		pPart->setStarted(true);
		pPart->setFinished(partEnd <= end);
	}
	else	// Adding parts before adding IP1
	{
		pPart->setStartPos(partStart);
		/*
		// L.Kopylov 05.12.2011 the very first part shall not start before view start
		if(pPartList->count() == 1)
		{
			if(pPart->getStartPos() < start)
			{
				pPart->setStartPos(start);
			}
		}
		*/
		pPart->setEndPos(partEnd);
		if((partStart > partEnd) && (start > end))	// Part itself includes IP1 and range includes IP1
		{
			pPart->setEndPos(partEnd + DataPool::getInstance().getLhcRingLength());
			pPart->setFinished(partStart > start);
		}
		else
		{
			if((partStart > partEnd) && (start < end))	// Take only required part around IP1
			{
				/* L.Kopylov 31.08.2012: here 2 variants are possible: either our part of interest is before IP1 or after.
				** New code - below
				pPart->setStartPos(start);
				*/
				if(start < partEnd)	// we are after IP1
				{
					pPart->setStartPos(start);
				}
				else	// We are before IP1
				{
					pPart->setEndPos(end);	// + DataPool::getInstance().getLhcRingLength());
				}
			}
			pPart->setFinished(partEnd <= end);
		}
		pPart->setStarted(partStart >= start);
	}
	// Decide if line part/end shall be visible or not. For sector based parts
	// another check will be done later, so this check here works only for beam-based
	// parts (beam vacuum)
	if(pPrevPart)
	{
		pPrevPart->setFinished(true);
	}
	/* L.Kopylov 17.01.2014 - see another variant above using pPrevPart, the variant below produces waring
	/opt/Qt/Qt-4.8.4/include/QtCore/qlist.h: In member function ‘void LhcLineParts::addLinePart(int, float, float, bool)’:
	/opt/Qt/Qt-4.8.4/include/QtCore/qlist.h:447: warning: assuming signed overflow does not occur when assuming that (X - c) > X is always false
	if(pPartList->count() > 1)
	{
		pPartList->at(pPartList->count() - 2)->setFinished(true);            <<<<<<<<<< This line
	}
	*/
}
