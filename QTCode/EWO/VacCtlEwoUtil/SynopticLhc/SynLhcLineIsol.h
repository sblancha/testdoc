#ifndef	SYNLHCLINEISOL_H
#define	SYNLHCLINEISOL_H

// Isolation vacuum line of LHC synoptic view

#include "SynLhcLine.h"

class SynLhcLineIsol : public SynLhcLine
{
public:
	SynLhcLineIsol(QWidget *parent, int type, float start, float end);
	virtual ~SynLhcLineIsol();

	virtual void decideForSectorBorders(void);
	virtual LhcLineParts::BuildType linePartMethod(void) { return LhcLineParts::TypeSector; }

	virtual void analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allCryoEqpMask, VacEqpTypeMask &allBeamEqpMask);
	virtual bool findDevicePosition(Eqp *pEqp, int &position, bool &visible);

	virtual void setSectorGeometry(void); // [VACCO-929]

protected:
	// Implementation of LhcLineParts's abstract methods
	virtual bool isMySector(Sector *pSect);
	virtual bool isMyEqp(Eqp *pEqp, int &partIdx);

	virtual void buildEquipment(void);
	virtual void analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allEqpMask);
	virtual int calcSectorBorderPosition(SynLhcEqpItem *pItem, int connPos);
	virtual void calcVerticalLimits(void);
	virtual void drawPartStart(QPainter &painter, int coord);
	virtual void drawPartEnd(QPainter &painter, int coord);
	virtual void drawSectLabels(QPainter &painter, int viewStart, int viewEnd);
};

#endif	// SYNLHCLINEISOL_H
