#ifndef	VACICONVV_PLUS_PP_H
#define	VACICONVV_PLUS_PP_H

// Icon for VLV

#include "VacIcon.h"

class EqpVV;

class VACCTLEWOUTIL_EXPORT VacIconVV_PLUS_PP : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVV_PLUS_PP(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVV_PLUS_PP();

	// Implementation of VacIcon's abstract methods
	virtual void setDirection(VacIconContainer::Direction newDirection);
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);


protected:
	// Pointer to my device
	EqpVV *pEqp;

	// Areas for pump and valve symbols. Drawing can be easy using QPainter
	// coordinate transformations, but finding areas for tooltips is not so
	// evident. May be it is more convenient just to keep in memory coordinates
	// of all distinct areas

	QRect	pump;
	QRect	vvr;

	void setAreasGeometry(void);

	virtual void paintEvent(QPaintEvent *pEvent);
	void drawPipes(QPainter &painter);
	virtual void drawVvr(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
	void drawPump(QPainter &painter);
	
	void setPainterForControl(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1,  bool plcAlarm);
	void setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
};

#endif	// VACICONVV_PLUS_PP_H
