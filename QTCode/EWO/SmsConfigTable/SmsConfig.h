#ifndef SMSCONFIG_H
#define	SMSCONFIG_H

// Data for simple SMS configuration

#include <qobject.h>
#include <qstring.h>
#include <qcolor.h>

// Definitions of colors. Every definition to be used for setRgb()
#define	COLOR_INACTIVE	200,200,200
#define	COLOR_STARTING	255,255,200
#define	COLOR_RUNNING	200,255,200


class SmsConfig : public QObject
{
	Q_OBJECT

public:

	// Types of information, used as column indices in table
	enum
	{
		Id = 0,
		Owner = 1,
		Group = 2,
		Config = 3,
		Error = 4,
		State = 5,
		Events = 6,
		Msg = 7
	};

	SmsConfig(const QString &configDpName);
	~SmsConfig();

	bool operator < (const SmsConfig &other) const;
	void setFlags(bool activeFlag, bool activatedFlag, bool initializedFlag);

	// Access
	virtual bool isGroup(void) const { return false; }
	virtual inline int getNumChildren(void) const { return 0; }
	inline const QString &getDpName(void) const { return dpName; }
	inline int getId(void) const { return id; }
	virtual void setId(int value);
	inline const QString &getOwner(void) const { return owner; }
	virtual void setOwner(const QString &name);
	inline const QString &getVisibleName(void) const { return visibleName; }
	virtual void setVisibleName(const QString &name);
	virtual const QString &getError(void) const { return error; }
	virtual void setError(const QString &msg);
	inline int getNumMsg(void) const { return numMsg; }
	virtual void setNumMsg(int num);
	inline unsigned getNumEvents(void) const { return numEvents; }
	virtual void setNumEvents(unsigned num);
	inline bool isActive(void) const { return active; }
	void setActive(bool flag);
	inline bool isActivated(void) const { return activated; }
	void setActivated(bool flag);
	inline bool isInitialized(void) const { return initialized; }
	void setInitialized(bool flag);
	inline bool isDeleted(void) const { return deleted; }
	virtual void setDeleted(bool flag) { deleted = flag; }
	inline int getRow(void) const { return row; }
	inline void setRow(int idx) { row = idx; }
	inline const QString &getState(void) const { return state; }
	inline const QColor &getColor(void) const { return color; }
	virtual inline int getNumErrors(void) const { return 0; }
	inline bool isConnected(void) { return connected; }
	inline void setConnected(bool flag) { connected = flag; }
	static inline int getSortColumn(void) { return sortColumn; }
	static inline void setSortColumn(int col) { sortColumn = col; }
	static inline Qt::SortOrder getSortOrder(void) { return sortOrder; }
	static inline void setSortOrder(Qt::SortOrder order) { sortOrder = order; }

signals:
	void changed(SmsConfig *pSrc);
	void numMsgChanged(SmsConfig *pSrc, int delta);
	void numEventsChanged(SmsConfig *pSrc, int delta);

protected:
	// Column index, that is used for sorting
	static int		sortColumn;

	// Direction for sorting
	static Qt::SortOrder	sortOrder;

	// DP name
	QString	dpName;

	// ID of this configuration
	int		id;

	// Owner of this configuration
	QString	owner;

	// Visible configuration name
	QString	visibleName;

	// Visible state
	QString	state;

	// Error string
	QString	error;

	// Color corresponding to state
	QColor	color;

	// Number of messages sent
	int		numMsg;

	// Number of events detected
	unsigned numEvents;

	// Row index in table, -1 if not shown in table
	int		row;


	// Config is Active
	bool	active;

	// Config is Activated
	bool	activated;

	// Config is Initialized
	bool	initialized;

	// Config is deleted
	bool	deleted;

	// dpConnect() for this config has been sent
	bool	connected;

	virtual void calculateState(void);
};

#endif	// SMSCONFIG_H
