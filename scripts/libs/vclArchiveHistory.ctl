// Archive states
#uses "vclArchiveConfig.ctl"	// Load CTRL library
#uses "vclDeviceFileParser.ctl"	// Load CTRL library

// Name (number) of items returned by QueryHistoryArchiveDpeChanges()
// result[x][1] - timestamp,
// result[x][2] - dpe,
// result[x][3] - archiveName,
// result[x][4] - isActive,
// result[x][5] - smoothType,
// result[x][6] - interval,
// result[x][7] - deadband,
// result[x][8] - userName,
// result[x][9] - source));

const int
	VAC_ARCHIVE_TS = 1,
	VAC_ARCHIVE_DPE  = 2,
	VAC_ARCHIVE_ARCHIVE_NAME = 3,
	VAC_ARCHIVE_IS_ACTIVE = 4,
	VAC_ARCHIVE_SMOOTHING = 5,
	VAC_ARCHIVE_TIME = 6,
	VAC_ARCHIVE_DEADBAND = 7,
	VAC_ARCHIVE_USER = 8,
	VAC_ARCHIVE_SOURCE = 9;

const int
	SOURCE_UNKNOWN = 0,
	SOURCE_ARCHIVE_HISTORY_PANEL  = 1,
	SOURCE_SCRIPT = 2,
	SOURCE_IMPORT = 3;

const string VAC_ARCHIVE_HISTORY_DP = "_VacArchiveHistory";
const string NAME_OFF_ARCHIVE_HISTORY = "ValueArchive_0000";

// items for smoothingList dyn_string
const string STR_SMOOTH_TYPE_NONE = "NO SMOOTHING";
const string STR_SMOOTH_TYPE_TIME = "TIME";
const string STR_SMOOTH_TYPE_VALUE = "VALUE";
const string STR_SMOOTH_TYPE_TIME_AND_VALUE = "TIME AND VALUE";
const string STR_SMOOTH_TYPE_TIME_OR_VALUE = "TIME OR VALUE";
const string STR_SMOOTH_TYPE_REL_VALUE = "RELATIVE VALUE";
const string STR_SMOOTH_TYPE_TIME_AND_REL_VALUE = "TIME AND RELATIVE VALUE";

// items for sourceList dyn_string
const string STR_SOURCE_UNKNOWN = "UNKNOWN";
const string STR_SOURCE_PANEL = "PANEL";
const string STR_SOURCE_SCRIPT = "SCRIPT";
const string STR_SOURCE_IMPORT = "IMPORT";

// Return Dp for history archiving
// If Dp doesn't exist create it

void LhcVacGetArchiveHistoryDp( string &archiveDpName, dyn_string &exceptionInfo )
{
	dyn_errClass 	err;


  if( !dpExists( VAC_ARCHIVE_HISTORY_DP ) )
	{
    dpCreate( VAC_ARCHIVE_HISTORY_DP, VAC_ARCHIVE_HISTORY_DP);
 	  err = getLastError( );
	  if( dynlen( err ) > 0 )
	  {
		  fwException_raise( exceptionInfo, 
			  "ERROR", "LhcVacGetArchiveHistoryDp(): Failed to create DP <" + VAC_ARCHIVE_HISTORY_DP + ">: " + err, "" );
		  return;
	  }
    int smoothKind = SMOOTH_TYPE_NONE;
    float deadband = 0.0;
    float timeInterval = 0.0;
    dyn_string dpeNames = makeDynString("FullDpeName", "ArchiveData");
    for( int n = 1; n <= dynlen(dpeNames); n++ )
    {
      SetArchiveParamsForArchiveClassName( VAC_ARCHIVE_HISTORY_DP + "." + dpeNames[n], NAME_OFF_ARCHIVE_HISTORY,
	      smoothKind, deadband, timeInterval, exceptionInfo );
      if( dynlen( exceptionInfo ) > 0 )
	    {
		    fwException_raise( exceptionInfo, 
			   "ERROR", "LhcVacGetArchiveHistoryDp(): Failed to set archive <" + NAME_OFF_ARCHIVE_HISTORY + "> for <" +
          VAC_ARCHIVE_HISTORY_DP + "." + dpeNames[n] + ">: " + err, "" );
		    return;
	    }
    }
	}
  archiveDpName = VAC_ARCHIVE_HISTORY_DP;
}
// make record  for "_VacArchiveHistory"
// ArchiveData dpe contains ordered data
// 1- fullDpeName
// 2- archiveName
// 3- isActive
// 4- smoothType
// 5- interval
// 6- deadband
// 7- userName
// 8- source
void WriteArchiveHistoryRecord( string dpe, bool isActive, int smoothType, float interval, float deadband,
  int source, dyn_string &exceptionInfo )
{
  dyn_errClass 	err;
  string        archiveDpName; 
  
  LhcVacGetArchiveHistoryDp( archiveDpName, exceptionInfo );
  if( dynlen( exceptionInfo ) > 0 )
  {
    return;
  }
  if( dpe == "")
  {
    fwException_raise( exceptionInfo, 
     "ERROR", "WriteArchiveHistoryRecord(): <" + VAC_ARCHIVE_HISTORY_DP + ".FullDpeName> doesn't specified", "" );
    return;
  }
  dpe = dpSubStr(dpe, DPSUB_DP_EL);
  string userName = LhcVacGetUserName();
  bool   isArchived;
  string archiveClass;
  int    smoothKind;
  float  lDeadband, timeInterval;
      
  GetArchiveParams( dpe , isArchived, archiveClass, smoothKind, lDeadband, timeInterval ); 
  dpSet(VAC_ARCHIVE_HISTORY_DP + ".FullDpeName" , dpe,
            VAC_ARCHIVE_HISTORY_DP + ".ArchiveData" , makeDynString(
            dpe,
            archiveClass,
            isActive,
            smoothType,
            interval,
            deadband,
            userName,
            source));
  err = getLastError( );
  
  if( dynlen( err ) > 0 )
  {
    fwException_raise( exceptionInfo, 
     "ERROR", "WriteArchiveHistoryRecord(): dpSet failed for <" + VAC_ARCHIVE_HISTORY_DP + ">: " + err, "" );
    return;
  }
}
void AdjustSmoothingParams( int smoothingType, float &deadband, float &interval)
{
  switch (smoothingType)
  {   
    case SMOOTH_TYPE_NONE:
      deadband = 0.0;
      interval = 0.0;
      break;
    case SMOOTH_TYPE_TIME:
      deadband = 0.0;
       break;
    case SMOOTH_TYPE_VALUE:
      interval = 0.0;
      break;
    case SMOOTH_TYPE_TIME_AND_VALUE:
      break;
    case SMOOTH_TYPE_TIME_OR_VALUE:
      break;
    case SMOOTH_TYPE_REL_VALUE:
      interval = 0.0;
      break;
    case SMOOTH_TYPE_TIME_AND_REL_VALUE:
      break;
  }
}
void GetOriginalArchiveParams(string fullDpeName, bool &hasArchive, int &origSmoothType,
  float &origDeadband, float &origInterval, dyn_string &exceptionInfo)
{
  string           dpName,dpType;
  dyn_dyn_anytype  dpeAttr;
  dyn_string 	     dpNameList;
  dyn_anytype      dpAttr;
//const int VAC_DPE_NAME = 1;
//const int VAC_DPE_ARCH_SMOOTH_TYPE = 18;
//const int VAC_DPE_ARCH_TIME_INTERVAL = 19;
//const int VAC_DPE_ARCH_DEAD_BAND = 20;
  
  ParseFullDpeName(fullDpeName, dpNameList);
  VacImportGetDeviceDataByName( dpNameList[1], dpType, dpAttr, dpeAttr, exceptionInfo );
  if( dynlen( exceptionInfo ) > 0 )
  {
    return;
  }
  int nDpe = dynlen(dpeAttr);
  for(int dpeIdx = 1; dpeIdx <= nDpe; dpeIdx++ )
  {
//    DebugTN(  " orig ", 
//        dpeAttr[dpeIdx][VAC_DPE_NAME],
//        dpeAttr[dpeIdx][VAC_DPE_ARCH_SMOOTH_TYPE],
//        dpeAttr[dpeIdx][VAC_DPE_ARCH_DEAD_BAND],
//        dpeAttr[dpeIdx][VAC_DPE_ARCH_TIME_INTERVAL]);
     if(dpeAttr[dpeIdx][VAC_DPE_NAME] == dpNameList[2])
     {
       hasArchive = (dpeAttr[dpeIdx][VAC_DPE_ARCHIVE_IDX] <=0 ) ? false : true;
       origSmoothType = dpeAttr[dpeIdx][VAC_DPE_ARCH_SMOOTH_TYPE];
       origDeadband = dpeAttr[dpeIdx][VAC_DPE_ARCH_DEAD_BAND];
       origInterval = dpeAttr[dpeIdx][VAC_DPE_ARCH_TIME_INTERVAL];
       AdjustSmoothingParams(origSmoothType, origDeadband, origInterval);
       return;
     }
   }
}  
void ParseFullDpeName(string fullDpeName, dyn_string &dpNameList)
{
   dyn_string dpPartList = strsplit(fullDpeName, ".");
   string dpeName;
   int    nDpPartList = dynlen(dpPartList);

   for( int n = 1; n <= nDpPartList; n++)
   {
     if(n == 1)
     {
       continue;
     }
     dpeName += dpPartList[n];
     if(n < nDpPartList)
     {
       dpeName += ".";
     }
   }
   dpNameList = makeDynString(dpPartList[1], dpeName);
} 

// performs query to get change of Archive parameters
// result[x] contains ordered values
// result[x][1] - timestamp,
// result[x][2] - dpe,
// result[x][3] - archiveName,
// result[x][4] - isActive,
// result[x][5] - smoothType,
// result[x][6] - interval,
// result[x][7] - deadband,
// result[x][8] - userName,
// result[x][9] - source));
// if allChanges = true? all changes for dpe will be returned, otherwice only recent change

void QueryHistoryArchiveDpeChanges(string dpeName, bool allChanges, dyn_dyn_anytype &result)
{
  string          query;
  dyn_dyn_anytype tab;
  string          archiveHistopyDp; 
  dyn_string      exceptionInfo;
  
  LhcVacGetArchiveHistoryDp( archiveHistopyDp, exceptionInfo );
  if( dynlen( exceptionInfo ) > 0 )
  {
    return;
  }

  string selectString = (allChanges ) ? "'_original.._stime'" : "'MAX(_original.._stime)'";
  
  query = "SELECT ALL '_original.._value'," + selectString + " FROM '" + archiveHistopyDp + ".FullDpeName'";
   if(dpeName != "")
  {
     query += " WHERE '_original.._value'=\"" + dpeName + "\"";
  }
  query += " SORT BY 2 DESC";
  dpQuery(query, tab);
  dynRemove(tab, 1);
  int nTabRows = dynlen(tab);
//DebugTN("Query 1: " + query, tab, dynlen(tab));
  if(nTabRows == 0)
  {
    // no data found
    return;
  }
  if(getType(tab[1][2]) == ANYTYPE_VAR)
  {
    // Not initialized anytype == empty record
    return;
  }
  int rowIdx = 1;
  for(int n = 1; n <=nTabRows; n++)
  {
    string lQuery = "SELECT ALL '_original.._value'  FROM '" + archiveHistopyDp + ".ArchiveData'";
    dyn_dyn_anytype lTab;
    string ts = tab[n][3];
    lQuery += " WHERE '_original.._stime'=\"" + ts + "\"";
    lQuery += " SORT BY 0";
//  DebugTN(" lQuery ", lQuery); 
    dpQuery(lQuery, lTab);
//DebugTN("Query 2: " + lQuery, lTab);
    dynRemove(lTab, 1);
    if(getType(lTab[1][2]) == ANYTYPE_VAR)
    {
    // Not initialized anytype == empty record
      continue;
    }
    int nLTabRows = dynlen(lTab);
 
    for( int rIdx = 1; rIdx <= nLTabRows; rIdx++)
    {
       dyn_anytype rowData = lTab[rIdx][2];
       if(rowData[1] != dpeName)
       {
         // tail from other dpe with same ts
         continue;
       }
       
       dynInsertAt(rowData, tab[n][3], 1);
       result[rowIdx] = rowData;
       rowIdx++;
    } 
//    DebugTN(" result ", result);
  }
}

