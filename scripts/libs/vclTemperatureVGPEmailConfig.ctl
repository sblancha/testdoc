
// Name of DP type containing E-mail configuration
const string VAC_VGP_EMAIL_CONFIG_DP_TYPE = "_VacVgpEmailConfig";

// Name of DP containing E-mail configuration
const string VAC_VGP_EMAIL_CONFIG_DP = "_VacVgpEmailConfig";

// Check if DP with email config exists, if not - create such DP
void VacVgpEmailGetConfigDp(string &configDpName, dyn_string &exceptionInfo)
{
	configDpName = "";
	if(dpExists(VAC_VGP_EMAIL_CONFIG_DP))
	{
		configDpName = VAC_VGP_EMAIL_CONFIG_DP;
		return;
	}
	if(dpCreate(VAC_VGP_EMAIL_CONFIG_DP, VAC_VGP_EMAIL_CONFIG_DP_TYPE) < 0)
	{
		fwException_raise(exceptionInfo, "ERROR", "VacVgpEmailGetConfigDp(): dpCreate() failed", "" );
		return;
	}
	if(!dpExists(VAC_VGP_EMAIL_CONFIG_DP))
	{
		fwException_raise(exceptionInfo, "ERROR", "VacVgpEmailGetConfigDp(): config DP not created", "");
		return;
	}
	configDpName = VAC_VGP_EMAIL_CONFIG_DP;

	// Set default values for new DP
	dpSetWait(configDpName + ".server", "cernmx.cern.ch");
	dpSetWait(configDpName + ".client", "cern.ch");
	dpSetWait(configDpName + ".deadTime", 60);
	dpSetWait(configDpName + ".sender", "vacin@cern.ch");
	dpSetWait(configDpName + ".prLimit", 1.E-8);
	dpSetWait(configDpName + ".tLimit2", 2.3);
	dpSetWait(configDpName + ".tLimit4", 5.5);
}
