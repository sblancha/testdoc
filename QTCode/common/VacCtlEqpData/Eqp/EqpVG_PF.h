// ---------------------------------------------------------------------------------------------------------------------
	/**
	System:         WinCC_OA 3.15 Vacuum framework
	Component Name: VacCtlEqpData librairy
	Class: Equipment Vacuum Gauge - for pumping group VPGF_LHC
	Language: C++

	License: CERN
	Licensed Material - Property of CERN

	(c) Copyright CERN 2017

	Address:
	CERN, Technology Departement, Vacuum Controls
	Route de Meyrin
	CH-1211 Geneva

	Description: This file contains the defined classes for the equipment Vacuum Gauge - for pumping group VPGF_LHC (VG_PF)

	Limitations: To be used with vacuum framework.

	Function:

	Database tables used:

	Thread Safe: No

	Extendable: No

	Platform Dependencies: Linux, Windows

	Compiler Options: none
	*/
	// ---------------------------------------------------------------------------------------------------------------------
#ifndef EQPVG_PF_H
#define	EQPVG_PF_H

	//	VG_PF - Vacuum Gauge

#include "Eqp.h"

	// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
	// Generic purpose color defined in eqp.h

	//	Main state bits to be analyzed
//#define	RR1_VALID_VGPF				(0x40000000) // (0x40 00  00 00)


	class VACCTLEQPDATA_EXPORT EqpVG_PF : public Eqp
	{
		Q_OBJECT

	public:
		EqpVG_PF(const Eqp &source);
#ifdef Q_OS_WIN
		EqpVG_PF(const EqpVG_PF &src) : Eqp(src) {}
#endif
		~EqpVG_PF();

		virtual void getMaster(QString &masterDp); // no master channel , int &masterChannel);
		virtual void getStateDpes(QStringList &stateDpes);
		virtual void getValueDpes(QStringList &valueDpes);

		virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
		virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

		virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
		void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

		virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::Pressure; }
		virtual float getMainValue(DataEnum::DataMode mode);
		virtual bool isMainValueValid(DataEnum::DataMode mode);
		virtual bool isMainValueNegative(void) const;

		virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
		virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
		virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
		virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
		virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
			QString &state, QColor &color);
#endif

		// Device list support
#ifndef PVSS_SERVER_VERSION
		virtual void getDpesForDevList(QStringList &dpes);
		virtual int checkDevListCrit(void);
#endif

		virtual void checkCriteria(EqpMsgCriteria *pCriteria);

		// Access for essential values
		inline bool isComAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? comAlarmReplay : comAlarmOnline; }
		inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
		inline int getState(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? stateReplay : stateOnline; }

		virtual int getWarningSt() const { return warningStOnline; }
		virtual int getErrorSt() const { return errorStOnline; }
		virtual double getPR() const { return prOnline; }

		public slots:
		virtual void dpeChange(Eqp *pSrc, const char *dpeName,
			DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

	protected:
		// Last known value of RR1 DPE 
		unsigned	rr1Online;
		unsigned	rr1Replay;

		// Last known value of the state of the  for online mode
		unsigned stateOnline;
		unsigned stateReplay;

		// Last known value of pressure value in mbar for online mode
		double prOnline;
		double prReplay;

		// Last knwon state of TCP connection to mobile (true == alarm) for online mode
		bool		comAlarmOnline;
		bool		comAlarmReplay;

		// Last known value of warning code status  for online mode
		unsigned	warningStOnline;
		unsigned	warningStReplay;

		// Last known value of error code status for online mode
		unsigned	errorStOnline;
		unsigned	errorStReplay;

#ifndef PVSS_SERVER_VERSION
		virtual void getMainColor(unsigned rr1, unsigned state, bool comAlarm, QColor &color);
		virtual void getMainStateString(unsigned rr1, unsigned state, double pr, bool comAlarm, QString &string, bool isHistoryState = false);
		virtual void getToolTipString(unsigned rr1, unsigned state, double pr, bool comAlarm, QString &string);
#endif
	};


#endif	// EQPVG_PF_H


