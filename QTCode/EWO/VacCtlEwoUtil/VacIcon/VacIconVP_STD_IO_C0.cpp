//	Implementation of VacIconVP_STD_IO_C0 class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVP_STD_IO_C0.h"

#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVP_STD_IO_C0::VacIconVP_STD_IO_C0(QWidget *parent, Qt::WindowFlags f) :
	VacIconVP_STD_IO(parent, f)
{
}

VacIconVP_STD_IO_C0::~VacIconVP_STD_IO_C0()
{
}

void VacIconVP_STD_IO_C0::drawType(QPainter &painter)
{
	painter.setPen(Qt::black);

	// the 90DegRot "M" in the left Middle
	QPolygon M90Deg(5);
	M90Deg.setPoint(0, -9, -5);
	M90Deg.setPoint(1, 0, -5);
	M90Deg.setPoint(2, -5, 0);
	M90Deg.setPoint(3, 0, 5);
	M90Deg.setPoint(4, -9, 5);
	painter.drawPolygon(M90Deg);
}
