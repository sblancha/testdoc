//	Implementation of S7Connection class
//////////////////////////////////////////////////////////////////////////////////

#include "S7Connection.h"
#include "ImportConstants.h"
#include "ImportPool.h"

#include "PlatformDef.h"

#include <QStringList>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

ImportPool	*S7Connection::pPool = NULL;

void S7Connection::setImportPool(ImportPool *pImportPool)
{
	pPool = pImportPool;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

S7Connection::S7Connection(const char *name, int lineNo)
{
	this->name = pPool->addString(name);
	ipAddress = NULL;
	this->lineNo = lineNo;
	type = rack = slot = EMPTY_INT_VALUE;
	ipAddressValid = false;
}

/*
**
**	FUNCTION
**		Create new S7Connection instance from data read in file.
**
**	PARAMETERS
**		pFile		- Pointer to file being read.
**		lineNo		- Line number in file.
**		name		- name of new archive
**		errList		- List where error message shall be added
**						in case of errors.
**
**	RETURNS
**		Pointer to new S7Connection instance created from data in file;
**		NULL in case of error.
**
**	CAUTIONS
**		It is responsability of caller to destroy returned instance using 'delete'
**		operator.
*/
S7Connection *S7Connection::createFromFile(FILE *pFile, int &lineNo, const char *name,
	QStringList &errList)
{
	S7Connection	*pNew = new S7Connection(name, lineNo);
	if(pNew->parseFile(pFile, lineNo, errList) < 0)
	{
		delete pNew;
		return NULL;
	}
	
	if(pNew->getType() == EMPTY_INT_VALUE)
	{
		PARSING_WARNING1("'ConnectionType' isn't specified for S7 connection <%s>", pNew->getName());
	}
	if(pNew->getRack() == EMPTY_INT_VALUE)
	{
		PARSING_WARNING1("'Rack' isn't specified for S7 connection <%s>", pNew->getName());
	}
	if(pNew->getSlot() == EMPTY_INT_VALUE)
	{
		PARSING_WARNING1("'Slot' isn't specified for S7 connection <%s>", pNew->getName());
	}
	if(!pNew->getIpAddress())
	{
		PARSING_WARNING1("'IpAddress' isn't specified or invalid for S7 connection <%s>", pNew->getName());
	}
	return(pNew);
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int S7Connection::processLineTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	const QString & /* longValue */, QStringList &errList)
{
	const char *funcName = "S7Connection::processLineTokens()";
	if(!strcasecmp(tokens[0], "IPAddress"))	
	{
		
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if((ipAddressValid = checkIpAddress(tokens[1])))
		{
			ipAddress = pPool->addString(tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "ConnectionType"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &type) != 1)
		{
			PARSING_WARNING2("Line %d: bad 'ConnectionType' format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Rack"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &rack) != 1)
		{
			PARSING_WARNING2("Line %d: bad 'Rack' format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "Slot"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &slot) != 1)
		{
			PARSING_WARNING2("Line %d: bad 'Slot' format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "EndS7Connection"))
	{
		return 0;
	}
	PARSING_ERR2("Line %d: unexpected <%s>", lineNo, tokens[0]);
}

/*
**
**	FUNCTION
**		Check validity of string, containing IP address
**
**	PARAMETERS
**		address	- String containing IP address
**
**	RETURNS
**		true	- address seemd to be valid;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool S7Connection::checkIpAddress(const char *address)
{
	int  num1, num2, num3, num4;

	if(sscanf(address, "%d.%d.%d.%d", &num1, &num2, &num3, &num4) != 4)
	{
		return false;
	}
	return	((0 <= num1) && (num1 <= 255)) &&
			((0 <= num2) && (num2 <= 255)) &&
			((0 <= num3) && (num3 <= 255)) &&
			((0 <= num4) && (num4 <= 255));
}

/*
**
**	FUNCTION
**		Dump instance content to output file for debugging
**
**	PARAMETERS
**		pFile		- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void S7Connection::dump(FILE *pFile)
{
	if(!ImportPool::isDebugMode())
	{
		return;
	}
	fprintf(pFile, "================ S7 Connection <%s>: lineNo %d\n", name, lineNo);
	fprintf(pFile, "   IP <%s> (%s) type %d rack %d slot %d\n", ipAddress,
		(ipAddressValid ? "VALID" : "NOT VALID"), type, rack, slot);
}
