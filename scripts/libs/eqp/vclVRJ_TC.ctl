/***********************************************************************************
****************** Constants for VPVELO **************************
*/
void LhcVacDeviceAction_VPJ_TC(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  mapping         mConnectDpes;
  unsigned        act;
  
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    for(int channel = 0 ; channel < 16 ; channel++)
    {
      string dpeName;
      sprintf(dpeName, "CH%02d.Status", channel);
      mConnectDpes[dpeName] = NO_VALUE_CHANGE;;
      sprintf(dpeName, "CH%02d.Value", channel);
      mConnectDpes[dpeName] = NO_VALUE_CHANGE;;
    }
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    for(int channel = 0 ; channel < 16 ; channel++)
    {
      string dpeName;
      sprintf(dpeName, "CH%02d.Status", channel);
      mConnectDpes[dpeName] = NO_VALUE_CHANGE;;
      sprintf(dpeName, "CH%02d.Value", channel);
      mConnectDpes[dpeName] = NO_VALUE_CHANGE;;
    }
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
   default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }
}
