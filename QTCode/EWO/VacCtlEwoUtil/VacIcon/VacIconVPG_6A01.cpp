//	Implementation of VacIconVPG_6A01 class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPG_6A01.h"

#include "EqpProcess.h"

#include <qpainter.h>

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_ON		(0x02000000)
#define	RR1_OFF		(0x01000000)

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF			255,255,255
#define	COLOR_WARNING		255,255,0
#define	COLOR_ERROR				255,102,255

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVPG_6A01::VacIconVPG_6A01(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 21);
}

VacIconVPG_6A01::~VacIconVPG_6A01()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPG_6A01::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(21, 21, 10, 10, 0);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpProcess"), "VacIconVPG_6A01::setEqp", pEqp->getDpName());
		this->pEqp = (EqpProcess *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6A01::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("RR2");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6A01::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6A01::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPG_6A01::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
// L.Kopylov 08.10.2013	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Prepare painter's coordinate system for drawing
	// Coordinate system (0,0) is center of widget
	painter.translate(width() / 2, height() / 2);

	// Vacuum pipe line according to direction
	if(drawConnectionPipe)
	{
		QPen pen(pipeColor, pipeWidth);
		painter.setPen(pen);
		switch(direction)
		{
		case VacIconContainer::Up:	// No rotation
			painter.drawLine(0, -10, 0, -7);
			break;
		case VacIconContainer::Down:	// 180 degrees
			painter.drawLine(0, 7, 0, 10);
			break;
		case VacIconContainer::Left:	// 90 degrees clockwise
			painter.drawLine(7, 0, 10, 0);
			break;
		case VacIconContainer::Right:	// 90 degrees counterclockwise
			painter.drawLine(-10, 0, -7, 0);
			break;
		}
	}

	// Coordinate system is ready - draw
	drawImage(painter);
}

void VacIconVPG_6A01::drawImage(QPainter &painter)
{
	// 'background' of image - depends on process mode
	bool plcAlarm = true;
	unsigned rr1 = 0;
	int seqStep = 0;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
		rr1 = pEqp->getRR1(mode);
		seqStep = pEqp->getRR2(mode) & 0xFF;
	}
	QColor color(COLOR_UNDEFINED);
	painter.setPen(Qt::black);
	bool drawRect = true;
	QPolygon points(3);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		painter.setBrush(color);
		painter.fillRect(-9, -9, 19, 19, painter.brush());
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		painter.setBrush(color);
		painter.fillRect(-9, -9, 19, 19, painter.brush());
		break;
	default:
		if(plcAlarm || (!(rr1 & RR1_VALID)))
		{
			painter.setBrush(color);
			painter.fillRect(-9, -9, 19, 19, painter.brush());
		}
		else	// Valid, result depends on mode
		{
			switch(seqStep)
			{
			case 1:		// Stop
			case 2:		// PRIMARY_PUMPING
			case 6:		// VPP_OFF
			case 7:		// CHECK_VENT_TURBO
			case 8:		// VENTING_TURBO
			case 9:		// VENTED_TERBO
			case 10:	// CHECK_VENT_ALL
			case 11:	// VENTING_ALL
			case 12:	// VENTED_ALL
				color.setRgb(COLOR_OFF);	// White
				painter.setBrush(color);
				painter.fillRect(-9, -9, 19, 19, painter.brush());
				break;
			case 3:		// ACCELERATING
			case 5:		// RECOVERING
				points.setPoint(0, -9, -9);
				points.setPoint(1, 9, -9);
				points.setPoint(2, -9, 9);
				color.setRgb(COLOR_ON);
				painter.setBrush(color);
				painter.drawPolygon(points);
				points.setPoint(0, 9, -9);
				points.setPoint(1, 9, 9);
				points.setPoint(2, -9, 9);
				color.setRgb(COLOR_OFF);
				painter.setBrush(color);
				painter.drawPolygon(points);
				drawRect = false;
				break;
			case 4:	// NOMINAL
			case 13:	// LD_PREPARE
			case 14:	// LD_IN_PROGRESS
			case 15:	// LDL_PREPARE
			case 16:	// LDL_IN_PROGRESS:
				color.setRgb(COLOR_ON);	// Green
				painter.setBrush(color);
				painter.fillRect(-9, -9, 19, 19, painter.brush());
				break;
			default:	// unknown mode
				painter.setBrush(color);
				painter.fillRect(-9, -9, 19, 19, painter.brush());
				break;
			}

			// If there is error or warning - indicate with 2 small triangles in lower left and upper right corners
			drawErrorWarning(painter, rr1);

			// In mode where VPP is OFF - circle shall be filled with red color, when mode = PIMARY_PUMPING - filled with green color
			bool fillCircle = false;
			switch(seqStep)
			{
			case 6:		// VPP_OFF
			case 10:	// CHECK_VENT_ALL
			case 11:	// VENTING_ALL
			case 12:	// VENTED_ALL
				painter.setBrush(Qt::red);
				fillCircle = true;
				break;
			case 2:		// PRIMARY_PUMPING
				painter.setBrush(Qt::green);
				fillCircle = true;
				break;
			}
			if(fillCircle)
			{
				painter.setPen(Qt::NoPen);
				// drawPie() to have circle filled with brush, drawArc() does not fill
				painter.drawPie(-6, -6, 12, 12, 0, 360 * 16);
			}
		}
		break;
	}

	// Finally draw circle and line in the middle of circle
	painter.setPen(Qt::black);
	painter.setBrush(Qt::NoBrush);
	painter.drawArc(-6, -6, 12, 12, 0, 5760);
	painter.drawLine(-5, 0, 5, 0);
	if(drawRect)
	{
		painter.drawRect(-9, -9, 18, 18);
	}
}

void VacIconVPG_6A01::drawErrorWarning(QPainter &painter, unsigned rr1)
{
	QColor color;
	if(rr1 & RR1_ERROR)
	{
		color = Qt::red;
	}
	else if(rr1 & RR1_WARNING)
	{
		color = Qt::yellow;
	}
	else
	{
		return;
	}
	painter.setBrush(color);
	painter.setPen(Qt::NoPen);
	QPolygon points(3);

	points.setPoint(0, -9, 9);
	points.setPoint(1, -9, 2);
	points.setPoint(2, -2, 9);
	painter.drawPolygon(points);

	points.setPoint(0, 9, -9);
	points.setPoint(1, 9, -3);
	points.setPoint(2, 3, -9);
	painter.drawPolygon(points);

	painter.setPen(Qt::black);
}
