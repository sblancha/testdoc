//	Implementation of TimeAxis class
/////////////////////////////////////////////////////////////////////////////////

#include "TimeAxis.h"

#include "VacMainView.h"
#include "DebugCtl.h"

#include <qpainter.h>

#include <stdlib.h>
#include <math.h>

#include "PlatformDef.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

TimeAxis::TimeAxis()
{
	logarithmic = false;
	needPrepare = true;
	showLabels = false;
	autoLogAxisUnit = true;
}

TimeAxis::~TimeAxis()
{
}

/*
**	FUNCTION
**		Set geometry of graph drawing area.
**
**	PARAMETERS
**		rect	- Graph area rectangle
**
** 	RETURNS
**		None
**
** 	CAUTIONS
**		None
*/
void TimeAxis::setGraphArea(const QRect &rect)
{
	if(graphArea.width() != rect.width())
	{
		needPrepare = true;
	}
	else if(graphArea.height() != rect.height())
	{
		needPrepare = true;
	}
	else if(graphArea.left() != rect.left())
	{
		needPrepare = true;
	}
	else if(graphArea.top() != rect.top())
	{
		needPrepare = true;
	}
	graphArea = rect;
}

/*
**	FUNCTION
**		Set time limits for axis
**
**	PARAMETERS
**		newMin	- Start of time range
**		newMax	- End of time range
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxis::setLimits(const QDateTime &newMin, const QDateTime &newMax)
{
	QDateTime	useMin, useMax;

	if(newMin <= newMax)
	{
		useMin = newMin;
		useMax = newMax;
	}
	else
	{
		useMin = newMax;
		useMax = newMin;
	}
	if(useMin.secsTo(useMax) < 60)
	{
		useMin = useMax.addSecs(-60);
	}

	// set msec to zero - L.Kopylov 23.08.2010
	QTime time(useMin.time());
	if(time.msec() > 0)
	{
		time.setHMS(time.hour(), time.minute(), time.second());
		useMin.setTime(time);
		useMin = useMin.addSecs(1);

		time = useMax.time();
		time.setHMS(time.hour(), time.minute(), time.second());
		useMax.setTime(time);
		useMax = useMax.addSecs(1);
	}

	if(min != useMin)
	{
		needPrepare = true;
		minAsMsec = useMin.toMSecsSinceEpoch();
	}
	min = useMin;
	if(max != useMax)
	{
		needPrepare = true;
		maxAsMsec = useMax.toMSecsSinceEpoch();
	}
	max = useMax;
}

/*
**	FUNCTION
**		Set flag indicating if axis shall be linear or logarithmic
**
**	PARAMETERS
**		flag	- Flag indicating if logarithmic axis shall be used
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxis::setLogScale(bool flag)
{
	if(logarithmic != flag)
	{
		needPrepare = true;
	}
	logarithmic = flag;
}

/*
**	FUNCTION
**		Set new unit for logarithmic axis
**
**	PARAMETERS
**		unit	- New value to set. Setting value < 1 switches to
**					automatic calculation mode.
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxis::setLogAxisUnit(unsigned unit)
{
	if(unit < 1)
	{
		autoLogAxisUnit = true;
		return;
	}
	logAxisUnit = unit;
	autoLogAxisUnit = false;
}

/*
**	FUNCTION
**		Convert time value to coordinate in window
**
**	PARAMETERS
**		value		- Time value to be converted
**		screenX		- Variable where resulting screen coordinate will be written
**
** RETURNS
**		Enumerated conversion success flag
**
** CAUTIONS
**		None
*/
TimeAxis::TimeRange TimeAxis::timeToScreen(const QDateTime &value, int &screenX) const
{
	TimeRange	result = WithinRange;

	if(value <= min)
	{
		screenX = scaleLeft;
		result = value < min ? BeforeMin : WithinRange;
	}
	else if(value >= max)
	{
		screenX = scaleRight;
		result = value > max ? AfterMax : WithinRange;
	}
	else
	{
		if(logarithmic )
		{
			double val = log10((double)min.secsTo(value) / (double)logAxisUnit);
			if(val > 0)
			{
				screenX = scaleLeft +
					(int)rint(val / logMax * (double)(scaleRight - scaleLeft));
			}
			else
			{
				screenX = scaleLeft;
			}
		}
		else
		{
			screenX = scaleLeft +
				(int)rint((double)min.secsTo(value) / (double)min.secsTo(max) *
				(double)(scaleRight - scaleLeft));
		}
		result = WithinRange;
	}
	return result;
}

TimeAxis::TimeRange TimeAxis::timeAsMsecToScreen(qint64 value, int &screenX) const
{
	TimeRange	result = WithinRange;

	if (value <= minAsMsec)	{
		screenX = scaleLeft;
		result = value < minAsMsec ? BeforeMin : WithinRange;
	}
	else if (value >= maxAsMsec) {
		screenX = scaleRight;
		result = value > maxAsMsec ? AfterMax : WithinRange;
	}
	else {
		if (logarithmic) {
			double val = log10((double)(value - minAsMsec) / (double)logAxisUnit / 1000.);	// 1000 - because work with msec here
			if (val > 0)
			{
				screenX = scaleLeft +
					(int)rint(val / logMax * (double)(scaleRight - scaleLeft));
			}
			else
			{
				screenX = scaleLeft;
			}
		}
		else
		{
			screenX = scaleLeft +
				(int)rint((double)(value - minAsMsec) / (double)(maxAsMsec - minAsMsec) *
				(double)(scaleRight - scaleLeft));
		}
		result = WithinRange;
	}
	return result;
}

/*
**	FUNCTION
**		Convert coordinate in window to time value
**
**	PARAMETERS
**		screenX	- X-coordinate in graph area
**		label	- Variable where string representation of resulting
**					time will be written.
**
**	RETURNS
**		Time corresponding to screen coordinate
**
**	CAUTIONS
**		None
*/
QDateTime TimeAxis::screenToTime(int screenX, QString &label)
{
	QDateTime	result;

	if(screenX <= scaleLeft)
	{
		result = min;
	}
	else if(screenX >= scaleRight)
	{
		result = max;
	}
	else
	{
		double scaleFactor, deltaTime;
		if(logarithmic )
		{
			scaleFactor = logMax / (double)(scaleRight - scaleLeft);
			deltaTime = logAxisUnit * pow(10., (double)(screenX - scaleLeft) * scaleFactor);
		}
		else
		{
			scaleFactor = (double)min.secsTo(max) / (double)(scaleRight - scaleLeft);
			deltaTime = (double)(screenX - scaleLeft) * scaleFactor;
		}
		int deltaSec = (int)deltaTime;
		result = min.addSecs(deltaSec);
		int msec = (int)(1000.0 * (deltaTime - deltaSec));
		if(msec > 0)
		{
			QTime time(result.time());
			time.setHMS(time.hour(), time.minute(), time.second(), msec);
			result.setTime(time);
		}
	}
/*
printf("TimeAxis::screenToTime(): result = %s\n",
result.toString("yyyy-MM-dd hh:mm:ss.zzz").ascii());
fflush(stdout);
*/
	label = result.toString("dd-MM-yy hh:mm:ss");
	return result;
}

/*
**	FUNCTION
**		Calculate height which will be consumed by scale labels
**
**	PARAMETERS
**		fontMetrics	- Font metrics that will be used for drawing
**
** 	RETURNS
**		height for scale labels
**
**	CAUTIONS
**		Calculates height of fixed label string assuming real
**		labels will not be very different.
*/
int TimeAxis::findLabelHeight(const QFontMetrics &fontMetrics)
{
	int	result = 0;

	if(!logarithmic)
	{
		QString label;
		makeLabel(label, QDateTime::currentDateTime());
		QSize size;
		labelExtents(fontMetrics, label, size);
		result = size.height() + 2;
	}
	else
	{
		QString label = "100";
		QSize size;
		labelExtents(fontMetrics, label, size);
		result = size.height() + 2;
	}
	return result;
}

/*
**	FUNCTION
**		Draw time axis
**
**	PARAMETERS
**		panter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxis::draw(QPainter &painter)
{
	if(needPrepare)
	{
		prepare(painter.fontMetrics());
	}

	painter.setPen(VacMainView::getGraphAxisColor());
	if(logarithmic)
	{
		drawLog(painter);
	}
	else
	{
		drawLin(painter);
	}
}

/*
**	FUNCTION
**		Prepare all axis drawing parameters
**
**	PARAMETERS
**		fontMetrics	- Metrics of font that will be used for drawing
**
**	RETURNS
**		true if axis parameters have been changed,
**		false otherwise
**
**	CAUTIONS
**		None
*/
bool TimeAxis::prepare(const QFontMetrics &fontMetrics)
{
	needPrepare = false;
	scaleLeft = graphArea.left();
	scaleRight = graphArea.right();

	if(logarithmic )
	{
		calculateLogAxis(fontMetrics);
	}
	else
	{
		calculateLinAxis(fontMetrics);
	}
	return true;
}

/*
**	FUNCTION
**		Calculate parameters of linear axis
**
**	PARAMETERS
**		fontMetrics	- Metrics of font that will be used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxis::calculateLinAxis(const QFontMetrics &fontMetrics)
{
	int labelHeight = findLabelHeight(fontMetrics);
	if(labelHeight > (graphArea.height() - graphArea.top()))
	{
		showLabels = false;	// No labels because no space for them
		axisLocation = graphArea.bottom() - 1;
		return;
	}
	axisLocation = graphArea.bottom() - labelHeight;
	showLabels = true;

	// Calculate label drawing parameters
	// Algorithm uses the following assumptions:
	// 1) Minumum displayed time range is 1 minute
	// 2) Reasonable steps between labels are:
	//		10, 20 sec,
	//		1,2,5,10 min,
	//		1,2,5,10 hours,
	//		1,2,5, days,
	//		10,20,50 days etc.
	// 3) First label shall be drawn at time which is multiple of step between labesl
	// 4) Labels shall not overlap
	// 5) The format used to represent time is fixed
	bool labelsOverlap = false;
	labelStep = 0;		// Let's start with minimum possible step
	do
	{
		labelStep = nextLabelStep(labelStep);
		// Find first label
		int mod = min.toTime_t() % labelStep;
		if(mod)
		{
			startLabelTime = min.addSecs(labelStep - mod);
		}
		else
		{
			startLabelTime = min;
		}
		if(startLabelTime > max) 	// No labels
		{
			labelsOverlap = true;
			break;
		}
		int prevLabelRight = 0;
		labelsOverlap = false;
		for(QDateTime value = startLabelTime ; value <= max ; value = value.addSecs(labelStep))
		{
			QString label;
			makeLabel(label, value);
			int labelX;
			timeToScreen(value, labelX);
			QSize size;
			labelExtents(fontMetrics, label, size);
			if(prevLabelRight && ((labelX - size.width() / 2 - size.width() / 8 ) < prevLabelRight))
			{
				labelsOverlap = true;
				break;
			}
			prevLabelRight = labelX + size.width() / 2 + size.width() / 8;
		}
	} while(labelsOverlap);
	if(labelsOverlap)		// No space for labels
	{
		axisLocation = graphArea.bottom() - 1;
		showLabels = false;
	}
}

/*
**	FUNCTION
**		Calculate parameters of logarithmic axis
**
**	PARAMETERS
**		fontMetrics	- Metrics of font that will be used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxis::calculateLogAxis(const QFontMetrics &fontMetrics)
{
	double	scaleWidth = log10((double)min.secsTo(max));

	if(autoLogAxisUnit || (logAxisUnit < 1))
	{
		if(scaleWidth < 3)
		{
			logAxisUnit = 1;					// 1 second
		}
		else if(scaleWidth < 4)
		{
			logAxisUnit = 10;					// 10 seconds
		}
		else if(scaleWidth < 5)
		{
			logAxisUnit = 60;					// 1 minute
		}
		else if(scaleWidth < 6)
		{
			logAxisUnit = 600;					// 10 minutes
		}
		else if(scaleWidth < 7)
		{
			logAxisUnit = 60 * 60;				// 1 hour
		}
		else if(scaleWidth < 8)
		{
			logAxisUnit = 10 * 60 * 60;			// 10 hours
		}
		else if(scaleWidth < 9)
		{
			logAxisUnit = 24 * 60 * 60;			// 1 day
		}
		else
		{
			logAxisUnit = 10 * 24 * 60 * 60;	// 10 days
		}
		// !!!!!!!!!!!!!!!!!!!!!! Override the algorithm above
		// logAxisUnit = 1;
	}
	logMax = log10((double)min.secsTo(max) / (double)logAxisUnit);
	axisLocation = graphArea.bottom() - findLabelHeight(fontMetrics);
}

/*
**	FUNCTION
**		Draw linear time axis
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Pen used for drawing shall be set in painter BEFORE calling this method
*/
void TimeAxis::drawLin(QPainter &painter)
{
	if(!showLabels)
	{
		return;	// No labels
	}
	for(QDateTime value = startLabelTime ; value <= max ; value = value.addSecs(labelStep))
	{
		int	x;
		timeToScreen(value, x);
		painter.drawLine(x, axisLocation, x, graphArea.top());
		QString label;
		makeLabel(label, value);
		drawLabel(painter, label, x);
	}
}

/*
**	FUNCTION
**		Draw logarithmic time axis
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Pen used for drawing shall be set in painter BEFORE calling this method
*/
void TimeAxis::drawLog(QPainter &painter)
{
	// Decide what will be the first label to draw
	int count = 1;
	switch(logAxisUnit)
	{
	case 1:			// Unit is 1 sec, 1st label at 10 sec
	case 60:		// Unit is 1 min, 1st label at 10 min
	case 3600:		// Unit is 1 hour, 1st label at 10 hours
	case 86400:		// Unit is 1 day, 1st label at 10 days
		count = 10;
		break;
	case 10:		// Unit is 10 sec, 1st label at 100 sec
	case 600:		// Unit is 10 min, 1st label at 100 min
	case 36000:		// Unit is 10 hours, 1st label at 100 hours
	case 864000:	// Unit is 10 days, 1st label at 100 days
		count = 100;
		break;
	}

	// Draw axis grid and labels if necessary
	bool	isFirstLabel = true, isFinished = false;
	for(unsigned step = logAxisUnit ; ! isFinished ; step *= 10 )
	{
		int x = 0;
		QString label;
		for(unsigned n = 2 ; n <= 10 ; n++)
		{
			QDateTime value = min.addSecs(n * step);
			if(timeToScreen(value, x) == AfterMax)
			{
				isFinished = true;
				break;
			}
			painter.drawLine(x, axisLocation, x, graphArea.top());
			if(n < 10)
			{
				continue;
			}
			if(isFirstLabel)
			{
				label.setNum(count);
				switch(logAxisUnit)
				{
				case 1:
				case 10:
					label += "s";
					break;
				case 60:
				case 600:
					label += "m";
					break;
				case 3600:
				case 36000:
					label += "h";
					break;
				case 86400:
				case 864000:
					label += "d" ;
					break;
				}
				isFirstLabel = false;
			}
			else
			{
				label.setNum(count);
			}
			count *= 10;
			drawLabel(painter, label, x);
		}
	}
}

/*
**	FUNCTION
**		Draw label consistinig of more than one line of text
**
**	PARAMETERS
**		painter	- Painter used for drawing
**		label	- Label to draw
**		x		- X-coordinate of label center
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxis::drawLabel(QPainter &painter, QString &label, int x)
{
	char	*asciiLabel = (char *)malloc(label.length() + 1);
#ifdef Q_OS_WIN
	strcpy_s(asciiLabel, label.length() + 1, label.toLatin1().constData());
#else
	strcpy(asciiLabel, label.toLatin1().constData());
#endif
	int	y = axisLocation + 2;

#ifdef Q_OS_WIN
	char *nextToken = NULL;
	char *token = strtok_s(asciiLabel, "\n", &nextToken);
#else
	char *token = strtok(asciiLabel, "\n");
#endif
	QFontMetrics fm = painter.fontMetrics();
	while(token)
	{
		QRect size = fm.boundingRect(token);
		y += size.height();
		painter.drawText(x - size.width() / 2, y - fm.descent(), token);
		y += 2;
#ifdef Q_OS_WIN
		token = strtok_s(NULL, "\n", &nextToken);
#else
		token = strtok(NULL, "\n");
#endif
	}
	free((void *)asciiLabel);
}

/*
**	FUNCTION
**		Calculate overall size of label consisting of more then one line of text
**
**	PARAMETERS
**		fontMetrics	- Metrics of font used for drawing
**		label		- Label whose size shall be calculated
**		size		- Variable where result will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxis::labelExtents(const QFontMetrics &fontMetrics, QString &label, QSize &size)
{
	char	*asciiLabel = (char *)malloc(label.length() + 1);
#ifdef Q_OS_WIN
	strcpy_s(asciiLabel, label.length() + 1, label.toLatin1().constData());
#else
	strcpy(asciiLabel, label.toLatin1().constData());
#endif

	size.setWidth(0);
	size.setHeight(0);

#ifdef Q_OS_WIN
	char *nextToken = NULL;
	char *token = strtok_s(asciiLabel, "\n", &nextToken);
#else
	char *token = strtok(asciiLabel, "\n");
#endif
	while(token)
	{
		QRect tokenSize = fontMetrics.boundingRect(token);
		if(size.height())
		{
			size.setHeight(size.height() + 2);
		}
		size.setHeight(size.height() + tokenSize.height());
		if(tokenSize.width() > size.width())
		{
			size.setWidth(tokenSize.width());
		}
#ifdef Q_OS_WIN
		token = strtok_s(NULL, "\n", &nextToken);
#else
		token = strtok(NULL, "\n");
#endif
	}
	free((void *)asciiLabel);
}

/*
**	FUNCTION
**		Prepare string representation of given time value.
**
**	PARAMETERS
**		label	- Variable where this method will put result
**		value	- Value to be shown on axis
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxis::makeLabel(QString &label, const QDateTime &value)
{
	label = value.toString("dd-MM-yyyy\nhh:mm:ss");
}

/*
**	FUNCTION
**		Calculate next step between time labels to use. The following
**		steps are used:
**			10, 20 sec,
**			1,2,5,10 min,
**			1,2,5,10 hours,
**			1,2,5, days,
**			10,20,50 days etc.
**
**	PARAMETERS
**		step	- previously used step between labels
**
**	RETURNS
**		New step between labels - always greater than previous one
**
**	CAUTIONS
**		None
*/
int TimeAxis::nextLabelStep(int step)
{
	// If previous step is 0 - start with 10 seconds step
	if(!step)
	{
		return 10;
	}

	// Do we already work with days?
	int nUnits = step / (60 * 60 * 24);
	if(nUnits)
	{
		nUnits = nextScaleStep(nUnits, false);
		return nUnits * 60 * 60 * 24;
	}

	// Not yet - do we work with hours?
	nUnits = step / (60 * 60);
	if(nUnits)
	{
		nUnits = nextScaleStep(nUnits, false);
		// Maximum hours is 10, then switch to days
		if(nUnits > 10)
		{
			return 1 * 24 * 60 * 60;	// 1 day
		}
		return nUnits * 60 * 60;
	}

	// Not yet = do we work with minutes?
	nUnits = step / 60;
	if(nUnits)
	{
		nUnits = nextScaleStep(nUnits, true);
		// Maximum minutes is 30, then switch to hours
		if(nUnits > 30)
		{
			return 1 * 60 * 60;	// 1 hour
		}
		return nUnits * 60;
	}

	// Still working with seconds
	nUnits = nextScaleStep(step, true);
	// Maximum seconds is 30, then switch to minutes
	if(nUnits > 30)
	{
		return 1 * 60;	// 1 min
	}
	return nUnits;
}

/*
**	FUNCTION
**		Calculate next number of units (sec,min,hors, days) between labels.
**		Number of units is taken from the sequence: 1,2,5,10,20,50.
**		If units are 60-based (seconds and minutes) then also use 30.
**
**	PARAMETERS
**		step		- previously used step between labels
**		use60Base	- true if units are 60-based
**
**	RETURNS
**		New step between labels - always greater than previous one
**
** CAUTIONS
**		None
*/
int TimeAxis::nextScaleStep(int step, bool use60Base)
{
	int	result = step, multiple = 1;

	if(step >= 100)
	{
		multiple = step / 100;
	}
	switch(step )
	{
	case 1:
		result = 2;
		break;
	case 2:
		result = 5;
		break;
	case 5:
		result = 10;
		break;
	case 10:
		result = 20;
		break;
	case 20:
		result = use60Base ? 30 : 50;
		break;
	case 30:
		result = 50;
		break;
	case 50:
		result = 100;
		break;
	}
	return result * multiple;
}

/*
**	FUNCTION
**		Dump axis parameters to text file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeAxis::dump(FILE *pFile)
{
	if(!DebugCtl::isAxis())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	fprintf(pFile, "###################### TIME AXIS\n");
	fprintf(pFile, "  left %d top %d width %d height %d\n",
		graphArea.left(), graphArea.top(), graphArea.width(), graphArea.height());
	fprintf(pFile, "  min %s max %s (range = %ld) %s\n",
		min.toString("dd-MM-yyyy hh:mm:ss.zzz").toLatin1().constData(),
		max.toString("dd-MM-yyyy hh:mm:ss.zzz").toLatin1().constData(),
		(long)min.secsTo(max), (logarithmic ? "LOGARITHMIC" : "LINEAR"));
	fprintf(pFile, "  axis %d scale: left %d right %d\n",
		axisLocation, scaleLeft, scaleRight);
	if(logarithmic )
	{
		fprintf(pFile, "  logAxisUnit %d, logMax %f\n", logAxisUnit, logMax);
	}
	else
	{
		fprintf(pFile, "  labelStep %d startLabelTime %s\n",
			labelStep, startLabelTime.toString("dd-MM-yyyy hh:mm:ss.zzz").toLatin1().constData());
	}
	fprintf(pFile, "\n");
}
