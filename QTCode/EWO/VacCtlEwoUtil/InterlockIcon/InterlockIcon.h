#ifndef	INTERLOCKICON_H
#define	INTERLOCKICON_H

#include "qwidget.h"
#include "InterlockEqp.h"
#include "InterlockIconConnection.h"

typedef enum
{
	Control = 1,
	TypeCInterlock,
	TypeDInterlock,
	InterlockSource
} InterlockIconClass;

class InterlockIcon : public QWidget
{
	Q_OBJECT
public:
	InterlockIcon(QWidget * parent = NULL, Qt::WindowFlags f = 0);
	static InterlockIcon * getIcon(InterlockEqp * pIntEqp, QWidget * parent);
	inline void setName(QString name){ this->name = name; };
	inline void setAttributes(QMap<QString, QString> &attributes){ this->attributes = QMap<QString, QString>(attributes); };
	inline QString getName(){ return name; };
	inline void setBeamColor(QColor newColor){ this->beamColor = newColor; };
	virtual InterlockIconClass getIconClass() = 0;
	inline void setVisible(bool newState) { visible = newState; };
	virtual void moveIcon(int x, int y);
	virtual QPoint getConnectionPoint(QString connectorName) = 0;
	virtual void getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber) = 0;
	virtual QRect getBoundingRect() = 0;
	bool operator ==(const InterlockIcon &icon);
	virtual ~InterlockIcon();
protected:
	QWidget * parent;
	QString name;
	QColor beamColor;
	QMap<QString, QString> attributes;
	QHash<QString, InterlockIcon *> parentIcons;
	QHash<QString, InterlockIcon *> childrenIcons;
	bool visible;
private:
	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif