// Eqp.cpp: implementation of the Eqp class.
//
//////////////////////////////////////////////////////////////////////

#include "Eqp.h"
#include "EqpType.h"
#include "DataPool.h"
#include "PlatformDef.h"
#include "DebugCtl.h"
#include "MobileType.h"
#include "FunctionalType.h"
#include "EqpMsgCriteria.h"
#include "ResourcePool.h"

#include "DpPolling.h"

#ifndef PVSS_SERVER_VERSION
#include "ReplayPool.h"
#include "VacCtlStateArchive.h"
#endif

// Implementations of particular equipment types
#include "EqpPLC.h"
#include "EqpV_TCP.h"
#include "EqpVA_RI.h"
#include "EqpVV.h"
#include "EqpVG.h"
#include "EqpVG_U.h"
#include "EqpVG_STD.h"
#include "EqpVRPI.h"
#include "EqpVPI.h"
#include "EqpVRCG.h"
#include "EqpVPG_EA.h"
#include "EqpVCLDX.h"
#include "EqpCOLDEX.h"
#include "EqpVACOK.h"
#include "EqpVPG_SA.h"
#include "EqpVPG_BP.h"
#include "EqpVPG_STD.h"
#include "EqpVPG_MBLK.h"
#include "EqpVP_STDIO.h"
#include "EqpVPC_HCCC.h"
#include "EqpCRYO_TT.h"
#include "EqpVGTR.h"
#include "EqpEXP_AREA.h"
#include "EqpCRYO_TT_SUM.h"
#include "EqpBEAM_Intens.h"
#include "EqpVLV_ANA.h"
#include "EqpVPT100.h"
#include "EqpVBeamParamFloat.h"
#include "EqpVBeamParamInt.h"
#include "EqpVRPM.h"
#include "EqpVIES.h"
#include "EqpVV_PS_CMW.h"
#include "EqpVP_VG_PS_CMW.h"
#include "EqpVV_AO.h"
#include "EqpVV_STD.h"
#include "EqpVV_PUL.h"
#include "EqpProcess.h"
#include "EqpINJ_6B02.h"
#include "EqpEXT_ALARM.h"
#include "EqpVRJ_TC.h"
#include "EqpVITE.h"
#include "EqpVPN.h"
#include "EqpVP_NEG.h"
#include "EqpVPNMUX.h"
#include "EqpVG_DP.h"
#include "EqpVPS.h"
#include "EqpVPS_Process.h"
#include "EqpITL_CMNT.h"
#include "EqpITL_CMUX.h"
#include "EqpVG_PT.h"
#include "EqpVP_IP.h"
#include "EqpVR_PI_CHAN.h"
#include "EqpVR_PI.h"
#include "EqpVP_TP.h"
#include "EqpVP_GU.h"
#include "EqpVR_GT.h"
#include "EqpVG_A_RO.h"

#include "EqpVPGM.h"
#include "EqpVPGMPR.h"
#include "EqpVBAKEOUT.h"
#include "EqpVR_ER.h"
#include "EqpVR_EC.h"
#include "EqpVR_EA.h"

#include "EqpVPR_VELO.h"
#include "EqpVPT_VELO.h"
#include "EqpVOPS_VELO.h"
#include "EqpVINT_VELO.h"
#include "EqpVPRO_VELO.h"
#include "EqpVOPS_2PS.h"

#include "EqpV8DI_FE.h"
#include "EqpVPGF_LHC.h"
#include "EqpVG_PF.h"

#include "EqpVELO_P_Main.h"
#include "EqpVELO_P_Interlocks.h"
#include "EqpVELO_P_Bakeout.h"

#include <QString>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

Eqp			Eqp::parser;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
Eqp::Eqp()
{
	pAttrList = new QHash<QByteArray, QString>();
	pDpeConnections = new QList<DpeConnection *>();
	pSignalDestinations = new QList<InterfaceEqp *>();
	initVar();
}
Eqp::Eqp(const Eqp &source) : InterfaceEqp(), FileParser()
{
	pAttrList = new QHash<QByteArray, QString>();
	pDpeConnections = new QList<DpeConnection *>();
	pSignalDestinations = new QList<InterfaceEqp *>();
	initVar();
	copyContent(source);
}
Eqp::Eqp(int type)
{
	pAttrList = new QHash<QByteArray, QString>();
	pDpeConnections = new QList<DpeConnection *>();
	pSignalDestinations = new QList<InterfaceEqp *>();
	initVar();
	this->type = type;
}

Eqp::Eqp(int type, char *name)
{
	pAttrList = new QHash<QByteArray, QString>();
	pDpeConnections = new QList<DpeConnection *>();
	pSignalDestinations = new QList<InterfaceEqp *>();
	initVar();
	this->type = type;
	dpName = DataPool::getInstance().addString(name);
}

Eqp & Eqp::operator = (const Eqp &other)
{
	initVar();
	copyContent(other);
	return *this;
}

// Initialize all properties to 'unspecified' value
void Eqp::initVar(void)
{
	ctlStatusOnline = ctlStatusReplay = Used;
	type = EqpType::None;
	ctrlFamily = ctrlType = ctrlSubType = 0;
	flangeName = NULL;
	name = functionalName = dpName = alias = dpType = surveyPart = NULL;
	typeName = NULL;
	pMainPart = NULL;
	pSectorBefore = pSectorAfter = NULL;
	domain = "";
	pMasterProcess = NULL;
	usageInMasterProcess = "";
	// By default fixed equipment is active, mobile equipment shall override this behavior
	activeOnline = activeReplay = true;
#ifndef PVSS_SERVER_VERSION
	skipOnSynoptic = false;
#endif
	vacType = VacType::None;
	startX = startY = endX = endY = start = length = 0.0;
	pAttrList->clear();
	mobileType = MobileType::Fixed;
	indexInSector = 0;
	functionalType = FunctionalType::None;
	selected = ordered = false;
	surveyPos = accessPointDistance = 0;
	intlSourcePrev = intlSourceNext = false;
}

// Copy content from source equipment
void Eqp::copyContent(const Eqp &source)
{
	ctlStatusOnline = source.ctlStatusOnline;
	ctlStatusReplay = source.ctlStatusReplay;
	alias = source.alias;
	*pAttrList = *(source.pAttrList);
	ctrlFamily = source.ctrlFamily;
	ctrlSubType = source.ctrlSubType;
	ctrlType = source.ctrlType;
	domain = source.domain;
	dpName = source.dpName;
	dpType = source.dpType;
	typeName = source.typeName;
	flangeName = source.flangeName;
	functionalName = source.functionalName;
	indexInSector = source.indexInSector;
	length = source.length;
	pMainPart = source.pMainPart;
	mobileType = source.mobileType;
	name = source.name;
	functionalType = source.functionalType;
	pSectorBefore = source.pSectorBefore;
	pSectorAfter = source.pSectorAfter;
	pMasterProcess = source.pMasterProcess;
	usageInMasterProcess = source.usageInMasterProcess;
#ifndef PVSS_SERVER_VERSION
	skipOnSynoptic = source.skipOnSynoptic;
#endif
	start = source.start;
	surveyPart = source.surveyPart;
	surveyPos = source.surveyPos;
	accessPointDistance = source.accessPointDistance;
	type = source.type;
	vacType = source.vacType;
	startX = source.startX;
	startY = source.startY;
	endX = source.endX;
	endY = source.endY;
	intlSourcePrev = source.intlSourcePrev;
	intlSourceNext = source.intlSourceNext;
}


Eqp::~Eqp()
{
}


/*
**
** FUNCTION
**		Create new Eqp instance from data read in file. Passive equipment is
**		created from one line in input file, active equipment requires more
**		lines to cover all information.
**
** PARAMETERS
**		type		- Type of equipment to be created, see EqpType class
**		pFile		- Pointer to file being read.
**		pLineNo		- Pointer to line number in file.
**		tokens		- Pointer to array of tokens in file.
**		nTokens		- Number of tokens.
**		errList		- List where error message shall be added
**						in case of errors.
**
** RETURNS
**		Pointer to new Eqp instance created from data in file;
**		NULL in case of error.
**
** CAUTIONS
**		It is responsability of caller to destroy returned instance using 'delete'
**		operator.
*/
Eqp *Eqp::createFromFile(int type, FILE *pFile, int &lineNo,
	char **tokens, int nTokens, QStringList &errList)
{
	parser.initVar();
	parser.type = type;
	QString dummy;
	if(type == EqpType::Passive)
	{
		if(parser.processLineTokens(pFile, lineNo, tokens, nTokens, dummy, errList) < 0)
		{
			return NULL;
		}
		parser.functionalType = FunctionalType::findPassiveType(parser.name);
	}
	else if(type == EqpType::VacDevice)
	{
		if(nTokens != 2)
		{
			errList.append(QString("Line %1: bad format for <%2>").arg(lineNo).arg(tokens[0]));
			return NULL;
		}
		parser.dpName = DataPool::getInstance().addString(tokens[1]);
		if(parser.parseFile(pFile, lineNo, errList) < 0)
		{
			return NULL;
		}
	}
	else
	{
		errList.append(QString("Line %1: Eqp::CreateFromFile() - unsupported type %2").arg(lineNo).arg(type));
	}

	//////////////////////////////////////////////////////////////////////////////////
	// We know everything about device we can get from file, now specific
	// device type can be created
	//////////////////////////////////////////////////////////////////////////////////
	Eqp *pEqp = createTypeSpecific();
	return pEqp;
}

/*
**
** FUNCTION
**		Create instance of device based on information in parser instance.
**		It is this method who decides which exacttly subclass of Eqp shall
**		be created
**
** PARAMETERS
**		None
**
** RETURNS
**		Pointer to new instance
**
** CAUTIONS
**		None
*/
Eqp *Eqp::createTypeSpecific(void)
{
	if ((parser.mobileType == MobileType::Fixed) || (parser.mobileType == MobileType::Instance))// || (parser.mobileType == MobileType::Master) 
	{
		return createTypeSpecificFixed();
	}
	return createMobile();
}

/*
**
** FUNCTION
**		Create instance of device based on information in parser instance.
**		It is this method who decides which exacttly subclass of Eqp shall
**		be created. The method is used for fixed equipment
**
** PARAMETERS
**		None
**
** RETURNS
**		Pointer to new instance
**
** CAUTIONS
**		None
*/
Eqp *Eqp::createTypeSpecificFixed(void)
{
	Eqp *pEqp = NULL;
	switch(parser.ctrlFamily)
	{
	case 1:	// FAMILY Valves
		switch(parser.ctrlType)
		{
		case 1:	// Different valves of SPS, including COLDEX
			switch(parser.ctrlSubType)
			{
			case 1:		// VVS_S
			case 257:	// VVS_S read-only
			case 2:		// VVS_SV
			case 258:	// VVS_SV read-only
			case 3:		// VVF_S
			case 259:	// VVF_S read-only
			case 12:
				pEqp = new EqpVV(parser);
				break;
			case 255:	// VCLDX
				pEqp = new EqpVCLDX(parser);
				break;
				break;
			}
			break;
		case 2:	// Valves for Zone Nord TODO
		case 3:	// VVS for LHC
		case 5:	// VVS for PS
		case 6:	// VV_PULSED for SPS
			pEqp = new EqpVV(parser);
			break;
		case 4:	// Analog valve for LHC-BGI
			pEqp = new EqpVLV_ANA(parser);
			break;
		case 7:	// Standard IO valve (1 DO, 2 DI)
		case 10:	// Standard IO valve (1 DO, 2 DI) - for VPG_6E01 in Linac2
			pEqp = new EqpVV_STD(parser);
			break;
		case 11:	// Vacuum Valve Pulsed
			pEqp = new EqpVV_PUL(parser);
			break;
		case 12:	// Sector Valve with SVCU DP interface for all machines
			pEqp = new EqpVV(parser);
			break;
		case 8:	// Analog valve for LHC-BGI - new version
			pEqp = new EqpVV_AO(parser);
			break;
		case 32:	// Gate valves in VELO (VVG_VELO)
		case 33:	// Process valves in VELO (VVP_VELO)
		case 34:	// Safety valves in VELO (VVS_VELO)
			pEqp = new EqpVV(parser);
			break;
		case 62:	// High Vacuum Valve part of the HCC Controller
			pEqp = new EqpVV_STD(parser);
			break;
		}
		break;

	case 2:	// FAMILY Ion pumps VPI
		pEqp = new EqpVPI(parser);
		break;

	case 3:	// FAMILY Multiplexor, or VPS control
		switch(parser.ctrlType)
		{
		case 1:	// VPS control
			switch(parser.ctrlSubType)
			{
			case 1:	// VPS master process
				pEqp = new EqpVPS_Process(parser);
				break;
			case 2:	// VPS
				pEqp = new EqpVPS(parser);
				break;
			}
			break;
		case 34:	// VPNMUX
			pEqp = new EqpVPNMUX(parser);
			break;
		}
		break;

	case 4:	// FAMILY VP - Vacuum Pumps
		switch(parser.ctrlType)
		{
		case 1:	// VP_STDIO
		case 3:	// VP_PULSED_PP (variant of VP_STDIO)
			pEqp = new EqpVP_STDIO(parser);
			break;
		case 2:		// VP_TP Turbo moleculare pump profibus interface
			pEqp = new EqpVP_TP(parser);
			break;
		case 4:	// VP_IP - Ion pump controlled by unit with profibus interface
			pEqp = new EqpVP_IP(parser);
			break;
		case 5: // VP_GU - Vacuum Pump - Group Unified
			pEqp = new EqpVP_GU(parser);
			break;
		case 32:	// VPT_VELO
			pEqp = new EqpVPT_VELO(parser);
			break;
		case 33:	// VP_NEG
			pEqp = new EqpVP_NEG(parser);
			break;
		case 62:	// VPC_HCCC
			pEqp = new EqpVPC_HCCC(parser);
			break;

		}
		break;

	case 5:	// FAMILY Roughing pump in VELO detector
		pEqp = new EqpVPR_VELO(parser);
		break;

	case 6:	// FAMILY VR - Vacuum Controllers
		switch(parser.ctrlType)
		{
		case 1:	// VR_GT:
			pEqp = new EqpVR_GT(parser);
			break;
		case 4:	// VR_PI controller and channel
			switch(parser.ctrlSubType)
			{
			case 2: // VR_PI controller
				pEqp = new EqpVR_PI(parser);
				break;
			case 3: // VR_PI_IO_L controller
				pEqp = new EqpVRPI(parser);
				break;
			case 4: // VR_PI_IO_S controller
			case 5: // VR_PI_IO_WLEP
				pEqp = new EqpVRPI(parser);
				break;
			case 21: // VR_PI_CHAN channel
				pEqp = new EqpVR_PI_CHAN(parser);
				break;
			}
			break;
		}
		break;

	case 202:// FAMILY Power supplies for ion pump VRPI (including VELO)
		pEqp = new EqpVRPI(parser);
		break;

	case 9: // FAMILY Pennings TPG300
		pEqp = new EqpVG_PT(parser);
		break;

	case 10:	// FAMILY Pirani gauges VGR
	case 11:	// FAMILY Penning gauges VGP
	case 12:	// FAMILY Full range gauges, Hot Cathod - VGFH
	case 13:	// FAMILY Full range gauges, Cold Cathod - VGFC
	case 14:	// FAMILY Ionization gauges VGI
	case 15:	// FAMILY Membrane gauges VGM
	case 17:	// FAMILY Differential pressure gauge in VELO 
		switch (parser.ctrlType)
		{
		case 6:
			pEqp = new EqpVG_U(parser);
			break;
		case 33: 
			pEqp = new EqpVG_A_RO(parser);
			break;
		default:
			pEqp = new EqpVG(parser);
			break;
		}
		break;
	case 16:	// FAMILY Vacuum Gauge
		switch(parser.ctrlType)
		{
		case 1:	// VG_STD
			pEqp = new EqpVG_STD(parser);
			break;
		case 4: // VG_PF Gauge of Fixed pumping group VPGF_LHC
			pEqp = new EqpVG_PF(parser);
		case 21:	// VG_DP
			pEqp = new EqpVG_DP(parser);
			break;
		case 32:	// Absolute pressure gauge in VELO
			pEqp = new EqpVG(parser);
			break;
		}
		break;
	case 18:	// FAMILY Pressure transmitter VGTR
		pEqp = new EqpVGTR(parser);
		break;
	
	case 30:	// FAMILY Thermocopules
		switch(parser.ctrlType)
		{
		case 1:	// Thermocopule VT100
			pEqp = new EqpVPT100(parser);
			break;
		case 2:
			switch(parser.ctrlSubType)
			{
			case 1:	// Patch panel for Thermocouple Type E (Read Only)
				pEqp = new EqpVRJ_TC(parser);
				break;
			case 257:	// Temperature of Thermocouple Type E (Read Only)
				pEqp = new EqpVITE(parser);
				break;
			}
		}
		break;

	case 32:	// FAMILY  Overpressure switch VELO
		switch (parser.ctrlType)
		{
		case 1:
			// Double overpressure switch
			pEqp = new EqpVOPS_2PS(parser);
			break;
		case 32:
			// Overpressure switch in VELO
			pEqp = new EqpVOPS_VELO(parser);
			break;
		}
		break;

	case 40:	// FAMILY Generic IO
		switch (parser.ctrlType)
		{
		case 1: // DI object
			switch (parser.ctrlSubType)
			{
			case 1: // V8DI_FE
				pEqp = new EqpV8DI_FE(parser);
				break;
			}
			break;
		}
		break;
		
	case 64:	// FAMILY Process VELO
		switch (parser.ctrlType) 
		{
		case 1: 
			pEqp = new EqpVELO_P_Main(parser);
			break;
		case 2:
			pEqp = new EqpVELO_P_Bakeout(parser);
			break;
		case 3:
			pEqp = new EqpVELO_P_Interlocks(parser);
			break;
		case 32:
			pEqp = new EqpVPRO_VELO(parser);
			break;
		}
		break;

	case 100:	// FAMILY Legacy Fixed pumping groups for LHC 
		switch(parser.ctrlType)
		{
		case 1:	// LHC bypass (VPGFA, VPGFB)
			pEqp = new EqpVPG_BP(parser);
			break;
		case 2:	// LHC standlone (VPGFC, VPGFD, VPGFC for UJ33)
			pEqp = new EqpVPG_SA(parser);
			break;
		}
		break;

	case 101:	// FAMILY STD Fixed pumping groups
		pEqp = new EqpVPG_STD(parser);
		break;

	case 105:	// FAMILY Fixed pumping groups for SPS (North Area & CNGS)
		pEqp = new EqpVPG_EA(parser);
		break;

	case 106:	// FAMILY Vacuum Pumping Group
		switch (parser.ctrlType)
		{
		case 1:				
			pEqp = new EqpProcess(parser);
			break;
		case 2:
			pEqp = new EqpVPG_MBLK(parser);
			break;
		case 11:
			pEqp = new EqpVP_GU(parser);
			break;
		}
		break;
	case 107: // FAMILY Process Gas Injection 
		switch(parser.ctrlType)
		{
		case 1:	// Process BGI_6B01
			switch(parser.ctrlSubType)
			{
			case 1:
			case 2:
				pEqp = new EqpProcess(parser);
				break;
			}
			break;
		case 2:	// Gas injection in LINAC4
			pEqp = new EqpINJ_6B02(parser);
			break;
		}
		break;
	case 110:	// FAMILY Process VPG_6E01
		pEqp = new EqpProcess(parser);
		break;




	case 121:	// FAMILY BAKEOUT_VRE Bakeout device types
		switch (parser.ctrlType) {
		case 1:	// Bakeout Rack/Cabinet 
			pEqp = new EqpVR_ER(parser);
			break;
		case 2:	// Bakeout Channel regulation
			pEqp = new EqpVR_EC(parser);
			break;
		case 3:	// Bakeout Alarm channel
			pEqp = new EqpVR_EA(parser);
			break;
		}
		break;





	case 150:	// FAMILY Interlocks VACOK
		switch(parser.ctrlType)
		{
		//case 5:	// Vacuum hardware Interlock - Relay of Ion pump controller
		//	pEqp = new EqpVI_RI(parser);
		//	break;
		case 32:	// VELO interlocks
			pEqp = new EqpVINT_VELO(parser);
			break;
		default:	// CRYO VACOK
			pEqp = new EqpVACOK(parser);
		break;
		}
		break;

	case 151:	// FAMILY Vacuum hardware Alarm
		switch (parser.ctrlType) {
		case 1: // Relay of Ion pump controller
			pEqp = new EqpVA_RI(parser);
			break;
		}
		break;

	case 160:	// FAMILY Different interlocks
		switch(parser.ctrlType)
		{
		case 3:	// Software Interlock with CRYO systems for VVS
			pEqp = new EqpITL_CMNT(parser);
			break;
		case 4:	// Multiplexer for SW CRYO ITL for VVS
			pEqp = new EqpITL_CMUX(parser);
			break;
		}
		break;

	case 204:	// FAMILY VRPM - power supply for solenoids
		pEqp = new EqpVRPM(parser);
		break;

	case 205:	// FAMILY COLDEX
		pEqp = new EqpCOLDEX(parser);
		break;

	case 210:	// FAMILY Auxilliary devices
		switch(parser.ctrlType)
		{
		case 1:	// TPG300
			pEqp = new EqpVRCG(parser);
			break;
		}
		break;

	case 211:	// FAMILY Communication Eqp
		switch (parser.ctrlType)
		{
		case 17:	// TYPE V_TCP TCP Communication
			pEqp = new EqpV_TCP(parser);
			break;
		}
		break;

	case 255:	// FAMILY PLC-related
		switch(parser.ctrlType)
		{
		case 1:	// Master PLC S7-400
		case 2:	// Master PLC S7-300
		case 3:	// Slave PLC S7-300
		case 10: // PLC 1200
		case 11: // PLC of VPGF_LHC
		case 16: // PLC 1500 for WL Mobile
			pEqp = new EqpPLC(parser);
			break;
		}
		break;

	case 254:	// FAMILY Auxilliary types added by SUP TODO
		switch(parser.ctrlType)
		{
		case 1:	// Vacuum barier between two sectors - no DP
			break;
		case 2:	// Experimental Area EXP_AREA
			pEqp = new EqpEXP_AREA(parser);
			break;
		case 3:	// Alarms of VELO - pure alarms DP
			break;
		case 4:	// Cryo Thermometers VCRYO_TT
			pEqp = new EqpCRYO_TT(parser);
			break;
		case 5:	// LHC Beam Intensity
			pEqp = new EqpBEAM_Intens(parser);
			break;
		case 6:	// Summary of Cryo Thermometers in one CRYO cell CRYO_TT_SUM
			pEqp = new EqpCRYO_TT_SUM(parser);
			break;
		case 7:	// Summary valves in PSB (2 or 4 real valves) VVS_PSB_SUM
			pEqp = new EqpVV(parser);
			break;
		case 8:	// Alarms for external target
			pEqp = new EqpEXT_ALARM(parser);
			break;
		case 50:	// Beam parameters
			switch(parser.ctrlSubType)
			{
			case 1:	// Beam current [mA]
			case 2:	// Beam energy
			case 3:	// Crit. energy
			case 4:	// PhF
			case 5:	// PhDm
			case 6:	// PhDmA
				pEqp = new EqpVBeamParamFloat(parser);
				break;
			case 10:	// Fill number
				pEqp = new EqpVBeamParamInt(parser);
				break;
			case 20:	// Fill schema
				break;
			case 30:	// Fill name (description)
				break;
			}
			break;
		case 70:	// Solenoids in LHC
			pEqp = new EqpVIES(parser);
			break;
		case 80:	// Equipment for PSR - data from CMW
			switch(parser.ctrlSubType)
			{
			case 1:	// VVS
				pEqp = new EqpVV_PS_CMW(parser);
				break;
			case 2:	// VGR
			case 3:	// VGP
			case 4:	// VPI
			case 5:	// VGI
				pEqp = new EqpVP_VG_PS_CMW(parser);
				break;
			}
			break;
		case 90:	// NEG pump
			pEqp = new EqpVPN(parser);
			break;
		}
		break;
	}
	if(!pEqp)
	{
		pEqp = new Eqp(parser);
	}
	return pEqp;
}

/*
**
** FUNCTION
**		Create instance of mobile device based on information in parser instance.
**
** PARAMETERS
**		None
**
** RETURNS
**		Pointer to new instance
**
** CAUTIONS
**		None
*/
Eqp *Eqp::createMobile(void)
{
	Eqp *pEqp = NULL;
	switch(parser.mobileType)
	{
	case MobileType::OnFlange:
		// Either VPGM or VPGMPR, decide based on MasterName attribute
		if(!parser.getAttrValue("MasterName").isEmpty())
		{
			pEqp = new EqpVPGM(parser);
		}
		else
		{
			pEqp = new EqpVPGMPR(parser);
		}
		break;
	case MobileType::OnSector:	// Bakeout rack
		pEqp = new EqpVBAKEOUT(parser);
		break;
	}
	if(!pEqp)
	{
		pEqp = new Eqp(parser);
	}
	return pEqp;
}

/*
**
** FUNCTION
**		Make a copy of this instance, method is required if the same device appears
**		in more than one beam line (example - part of LHC: it is included into both
**		LHC ring line and DSL line)
**
** PARAMETERS
**		None
**
** RETURNS
**		Pointer to copy of this device
**
** CAUTIONS
**		None
*/
Eqp *Eqp::clone(void)
{
	Eqp *pCopy = new Eqp(type);
	pCopy->copyContent(*this);
	return pCopy;
}

/*
**
** FUNCTION
**		Return value of attribute with given name
**
** PARAMETERS
**		attrName	- Required attribute name
**
** RETURNS
**		Pointer to string with attribute value, or
**		NULL if such attribute was not found
**
** CAUTIONS
**		None
*/
const QString Eqp::getAttrValue(const char *attrName) const
{
	return pAttrList->value(attrName);
}

/*
**
**	FUNCTION
**		Build short name of passive equipment to be shown on synoptic
**		using name of device and passive equipment type.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Pointer to string with short name
**
**	CAUTIONS
**		Method returns pointer to static buffer, content of buffer
**		will be overwritten with next call to this method.
*/
const char *Eqp::makeShortName(void) const
{
	static char shortNameBuf[256];

	const char	*prefix = NULL;

	if(functionalName)
	{
		return functionalName;
	}

	if((functionalType == FunctionalType::ColdDipole) ||
		(functionalType == FunctionalType::WarmDipole))
	{
		prefix = "D";
	}
	else if((functionalType == FunctionalType::ColdQuadrupole) ||
		(functionalType == FunctionalType::WarmQuadrupole))
	{
		prefix = "Q";
	}
	else if((functionalType == FunctionalType::TDICollimator))
	{
		prefix = "TDI";
	}
	if(!prefix)
	{
		return name;
	}
#ifdef Q_OS_WIN
	strcpy_s(shortNameBuf, sizeof(shortNameBuf) / sizeof(shortNameBuf[0]), prefix);
#else
	strcpy(shortNameBuf, prefix);
#endif
	char	*lastPart;
	if((lastPart = strrchr(name, '.')))
	{
#ifdef Q_OS_WIN
		strcat_s(shortNameBuf, sizeof(shortNameBuf) / sizeof(shortNameBuf[0]), ++lastPart);
#else
		strcat(shortNameBuf, ++lastPart);
#endif
	}
	else if((lastPart = strrchr(name, '_')))
	{
#ifdef Q_OS_WIN
		strcat_s(shortNameBuf, sizeof(shortNameBuf) / sizeof(shortNameBuf[0]), ++lastPart);
#else
		strcat(shortNameBuf, ++lastPart);
#endif
	}
	else
	{
#ifdef Q_OS_WIN
		strcpy_s(shortNameBuf, sizeof(shortNameBuf) / sizeof(shortNameBuf[0]), name);
#else
		strcpy(shortNameBuf, name);
#endif
	}
	return shortNameBuf;
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int Eqp::processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
	const QString &longValue, QStringList &errList)
{
	int	coco = 0;

	if(type == EqpType::Passive)
	{
		if(DataPool::getInstance().isLhcFormat())
		{
			coco = processLhcTokens(pFile, lineNo, tokens, nTokens, errList);
		}
		else
		{
			coco = processSurveyTokens(pFile, lineNo, tokens, nTokens, errList);
		}
	}
	else if(type == EqpType::VacDevice)
	{
		coco = processCtlTokens(pFile, lineNo, tokens, nTokens, longValue, errList);
	}
	return coco;
}

/*
**
**	FUNCTION
**		Set reference to new sectors after ordered sector list has been built
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void Eqp::setNewSectors(void)
{
	if(pSectorBefore)
	{
		pSectorBefore = pSectorBefore->getNewSector();
	}
	if(pSectorAfter)
	{
		pSectorAfter = pSectorAfter->getNewSector();
	}
}

bool Eqp::convertValue(const QVariant &value, float &result, EqpPart /* part */)
{
	if(!value.canConvert(QVariant::Double))
	{
		result = 0;
		return false;
	}
	result = (float)value.toDouble();
	return true;
}

void Eqp::convertValueToString(float value, QString &result, EqpPart /* part */)
{
	char buf[16];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%8.2E", value);
#else
	sprintf(buf, "%8.2E", value);
#endif
	result = buf;
}

void Eqp::getValueDpes(QStringList &valueDpes, EqpPart /* part */)
{
	getValueDpes(valueDpes);
}

const char *Eqp::getLabelBefore(void)
{
	static char result[256];
	QString label = getAttrValue("LabelBefore");
	if(!label.isEmpty())
	{
#ifdef Q_OS_WIN
		strcpy_s(result, sizeof(result) / sizeof(result[0]), label.toLatin1().constData());
#else
		strcpy(result, label.toLatin1().constData());
#endif
		return result;
	}
	Sector *pSect = getSectorBefore();
	return pSect ? pSect->getName() : NULL;
}

const char *Eqp::getLabelAfter(void)
{
	static char result[256];
	QString label = getAttrValue("LabelAfter");
	if(!label.isEmpty())
	{
#ifdef Q_OS_WIN
		strcpy_s(result, sizeof(result) / sizeof(result[0]), label.toLatin1().constData());
#else
		strcpy(result, label.toLatin1().constData());
#endif
		return result;
	}
	Sector *pSect = getSectorAfter();
	return pSect ? pSect->getName() : NULL;
}

#ifndef PVSS_SERVER_VERSION

// For special values of EqpStatus DPE the value of DPE overrides whatever we can have in other DPEs,
// for the moment - two states are used: NotControl and NotConnected
bool Eqp::isColorByCtlStatus(QColor &color, DataEnum::DataMode mode) const
{
	return  isColorByCtlStatus(mode == DataEnum::Replay ? ctlStatusReplay : ctlStatusOnline, color);
}

bool Eqp::isColorByCtlStatus(CtlStatus statusToCheck, QColor &color) const
{
	switch(statusToCheck)
	{
	case NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		return true;
		break;
	case NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		return true;
		break;
	default:
		break;
	}
	return false;
}

bool Eqp::isStateStringByCtlStatus(QString &string, DataEnum::DataMode mode) const
{
	return isStateStringByCtlStatus(mode == DataEnum::Replay ? ctlStatusReplay : ctlStatusOnline, string);
}

bool Eqp::isStateStringByCtlStatus(CtlStatus statusToCheck, QString &string) const
{
	switch(statusToCheck)
	{
	case NotControl:
		string = "Not Controlled";
		return true;
		break;
	case NotConnected:
		string = "Not Connected";
		return true;
		break;
	default:
		break;
	}
	return false;
}

bool Eqp::isHistoryStateByCtlStatus(VacCtlStateArchive *pPool, QDateTime &ts, QList<QByteArray> &dpeNames, QList<QVariant *> &values,
	QString &state, QColor &color) const
{
	if (!pPool->getDpValues(dpName, ts, dpeNames, values)) {
		return true;	// No further analysis required
	}
	if (values.isEmpty()) {
		return true;
	}
	QVariant *pVariant = values.takeFirst();
	if (!pVariant) {
		return true;
	}
	CtlStatus useStatus = (CtlStatus)pVariant->toUInt();
	delete pVariant;
	if (isColorByCtlStatus(useStatus, color)) {
		isStateStringByCtlStatus(useStatus, state);
		while (!values.isEmpty()) {
			delete values.takeFirst();
		}
		return true;
	}
	return values.isEmpty();
}

bool Eqp::checkDevListNotConnectedCriteria(DataEnum::DataMode mode)
{
	return (mode == DataEnum::Replay ? ctlStatusReplay : ctlStatusOnline) == NotConnected;
}

#endif

/*
**
**	FUNCTION
**		Write device content to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**		idx		- Device index
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void Eqp::dump(FILE *pFile, int idx)
{
	if(!DebugCtl::isData())
	{
		return;
	}
	if(type == EqpType::Passive)
	{
		fprintf(pFile, "    %d: PAS <%s> (type %d) (%s) at %f length %f: start (%f, %f) end (%f, %f)\n",
			idx, name, functionalType, (functionalName != NULL ? functionalName : ""), start, length,
			startX, startY, endX, endY);
	}
	else if(type == EqpType::LineStart)
	{
		fprintf(pFile, "    %d: MY_START <%s> at %f: (%f, %f)\n",
			idx, name, start, startX, startY);
	}
	else if(type == EqpType::LineEnd)
	{
		fprintf(pFile, "    %d: MY_END <%s> at %f: (%f, %f)\n",
			idx, name, start, startX, startY);
	}
	else if(type == EqpType::OtherLineStart)
	{
		fprintf(pFile, "    %d: OTHER_START <%s> at %f: (%f, %f)\n",
			idx, name, start, startX, startY);
	}
	else if(type == EqpType::OtherLineEnd)
	{
		fprintf(pFile, "    %d: OTHER_END <%s> at %f: (%f, %f)\n",
			idx, name, start, startX, startY);
	}
	else if(type == EqpType::VacDevice)
	{
		fprintf(pFile, "    %d: ACTIVE <%s> at %f: vacType %d DpType <%s> sect1 %s sect2 %s MP %s (%f, %f) (accessPointDistance %9.3f)\n",
			idx, name, start, getVacType(), dpType,
			(pSectorBefore ? pSectorBefore->getName() : "NULL"),
			(pSectorAfter ? pSectorAfter->getName() : "NULL"),
			(pMainPart ? pMainPart->getName() : "NULL"),
			startX, startY, accessPointDistance);
		fprintf(pFile, "        funcType %d typeName %s mobileType %d isActive %d flange <%s> indexInSector %d\n",
			functionalType, (typeName ? typeName : "NULL"), mobileType, isActive(DataEnum::Online),
			(flangeName ? flangeName : "NONE"), indexInSector);
		if(isCryoThermometer())
		{
			fprintf(pFile, "        CRYO thermometer %s\n", (isValveInterlock() ? " VALVE INTERLOCK" : ""));
		}
		if(intlSourcePrev || intlSourceNext)
		{
			fprintf(pFile, "        INTL source: prev %s next %s\n",
				(intlSourcePrev ? "YES" : "NO"), (intlSourceNext ? "YES" : "NO"));
		}
		if(!extTargets.isEmpty())
		{
			fprintf(pFile, "        EXTERNAL TARGETS:\n");
			for(int n = 0 ; n < extTargets.count() ; n++)
			{
				Eqp *pEqp = extTargets.at(n);
				fprintf(pFile, "          %s\n", (pEqp ? pEqp->getDpName() : "null"));
			}
		}
	}
	else
	{
		fprintf(pFile, "    %d: Unknown type %d for <%s>\n", idx, type, name);
	}
	QHashIterator<QByteArray, QString> iter(*pAttrList);
	while(iter.hasNext())
	{
		iter.next();
		fprintf(pFile, "           Attr <%s> = <%s>\n",
			iter.key().constData(), iter.value().toLatin1().constData());
	}
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file, input
**		file has format generated from SURVEY database
**		Expectedd tokens content is:
**		0	- Survey partition name
**		1	- Family name
**		2	- Member name
**		3	- Start survey position of equipment
**		4	- End survey position of equipment
**		5	- X coordinate of element start
**		6	- Y coordinate of element start
**		7	- X coordinate of element end
**		8	- Y coordinate of element end
**
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
** RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
** CAUTIONS
**		None
*/
int Eqp::processSurveyTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	QStringList &errList )
{
	float	value;

	CHECK_TOKEN_COUNT(nTokens, 8, lineNo, "Eqp::ProcessSurveyTokens");

	surveyPart = DataPool::getInstance().addMultiUseString(tokens[0]);

	name = DataPool::getInstance().addString(tokens[1]);

	// Read all locations and coordinates
	if(sscanf(tokens[2], "%f", &surveyPos) != 1)
	{
		PARSING_ERR2("Bad format for start position <%s> in line %d", tokens[2], lineNo);
	}
	start = surveyPos;
	if(sscanf(tokens[3], "%f", &value) != 1)
	{
		PARSING_ERR2("Bad format for end position <%s> in line %d", tokens[3], lineNo);
	}
	// Do not try to get positive length - LIK 08.02.2005
	// length = ( value >= start ) ? ( value - start ) : ( start - value );
	length = value - start;
	if(length < 0.0)
	{
		length = 0.0;
	}
	if(sscanf(tokens[4], "%f", &startX) != 1)
	{
		PARSING_ERR2("Bad format for X coordinate of start <%s> in line %d", tokens[4], lineNo);
	}
	if(sscanf(tokens[5], "%f", &startY) != 1)
	{
		PARSING_ERR2("Bad format for Y coordinate of start <%s> in line %d", tokens[5], lineNo);
	}
	if(sscanf(tokens[6], "%f", &endX) != 1)
	{	
		PARSING_ERR2("Bad format for X coordinate of end <%s> in line %d", tokens[6], lineNo);
	}
	if(sscanf(tokens[7], "%f", &endY) != 1)
	{
		PARSING_ERR2("Bad format for Y coordinate of end <%s> in line %d", tokens[7], lineNo);
	}
	return 0;
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file, input
**		file has format generated from LHC database
**		Expectedd tokens content is:
**		0	- Survey partition name
**		1	- Equipment name
**		2	- Start survey position of equipment
**		3	- End survey position of equipment
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int Eqp::processLhcTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	QStringList &errList)
{
	float	value;

	CHECK_TOKEN_COUNT_RANGE(nTokens, 4, 5, lineNo, "Eqp::ProcessLhcTokens");

	surveyPart = DataPool::getInstance().addMultiUseString(tokens[0]);
	name = DataPool::getInstance().addString(tokens[1]);

	// Read all locations and coordinates
	if(sscanf(tokens[2], "%f", &surveyPos) != 1)
	{
		PARSING_ERR2("Bad format for start position <%s> in line %d", tokens[2], lineNo);
	}
	start = surveyPos;
	if(sscanf(tokens[3], "%f", &value) != 1)
	{
		PARSING_ERR2("Bad format for end position <%s> in line %d", tokens[3], lineNo);
	}
	length = (value >= start) ? (value - start) : (start - value);
	if(nTokens > 4)
	{
		functionalName = DataPool::getInstance().addString(tokens[4]);
	}
	return 0;
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file for controllable
**		equipment. device description consists of more than one lines, every
**		line has a format
**		KEYWORD [= value]
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
**	RETURNS
**		1	- tokens have been processed successfully, more input lines are
**				expected for this equipment.
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int Eqp::processCtlTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	const QString &longValue, QStringList &errList)
{
	// Device status
	if(!strcasecmp(tokens[0], "Status"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		int value;
		if(sscanf(tokens[1], "%d", &value) != 1)
		{
			PARSING_ERR3("Line %d: bad format for <%s> : <%s>", lineNo, tokens[0], tokens[1]);
		}
		switch(value)
		{
		case Used:
		case NotUsed:
		case TempRemoved:
		case PermRemoved:
		case NotControl:
		case NotConnected:
			ctlStatusOnline = (CtlStatus)value;
			ctlStatusReplay = ctlStatusOnline;
			break;
		default:
			PARSING_ERR3("Line %d: bad value for <%s> %d (must be [0...255]", lineNo, tokens[0], value);
		}
		return 1;
	}
	// Control Family
	else if(!strcasecmp(tokens[0], "CtlFamily"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		int value;
		if(sscanf(tokens[1], "%d", &value) != 1)
		{
			PARSING_ERR3("Line %d: bad format for <%s> : <%s>", lineNo, tokens[0], tokens[1]);
		}
		if((value < 0) || (value > 255))
		{
			PARSING_ERR3("Line %d: bad value for <%s> %d (must be [0...255]", lineNo, tokens[0], value);
		}
		ctrlFamily = (unsigned char)value;
		return 1;
	}
	// Control Type
	else if(!strcasecmp(tokens[0], "CtlType"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		int value;
		if(sscanf(tokens[1], "%d", &value) != 1)
		{
			PARSING_ERR3("Line %d: bad format for <%s> : <%s>", lineNo, tokens[0], tokens[1]);
		}
		if((value < 0) || (value > 255))
		{
			PARSING_ERR3("Line %d: bad value for <%s> %d (must be [0...255]", lineNo, tokens[0], value);
		}
		ctrlType = (unsigned char)value;
		return 1;
	}
	// Control SubType
	else if(!strcasecmp(tokens[0], "CtlSubType"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		int value;
		if(sscanf(tokens[1], "%d", &value) != 1)
		{
			PARSING_ERR3("Line %d: bad format for <%s> : <%s>", lineNo, tokens[0], tokens[1]);
		}
		if((value < 0) || (value > 65535))
		{
			PARSING_ERR3("Line %d: bad value for <%s> %d (must be [0...65535]", lineNo, tokens[0], value);
		}
		ctrlSubType = (unsigned short)value;
		return 1;
	}
	// RealName
	else if(!strcasecmp(tokens[0], "RealName"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		name = DataPool::getInstance().addString(tokens[1]);
		return 1;
	}
	// Alias
	else if(!strcasecmp(tokens[0], "Alias"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		alias = DataPool::getInstance().addString(tokens[1]);
		return 1;
	}
	// Flange name
	else if(!strcasecmp(tokens[0], "Flange"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		flangeName = DataPool::getInstance().addString(tokens[1]);
		return 1;
	}
	// DpType
	else if(!strcasecmp(tokens[0], "DpType"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		dpType = DataPool::getInstance().addMultiUseString(tokens[1]);
		return 1;
	}
	// MobileType
	else if(!strcasecmp(tokens[0], "MobileType"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		QString errMsg;
		int mobType = MobileType::decode(tokens[1], lineNo, errMsg);
		if(mobType < 0)
		{
			errList.append(errMsg);
			return -1;
		}
		mobileType = mobType;
		return 1;
	}
	// Partition
	else if(!strcasecmp(tokens[0], "Partition"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		surveyPart = DataPool::getInstance().addMultiUseString(tokens[1]);
		return 1;
	}
	// Position
	else if(!strcasecmp(tokens[0], "Position"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		if(sscanf(tokens[1], "%f", &surveyPos) != 1)
		{
			PARSING_ERR3("Line %d: bad format for <%s> : <%s>", lineNo, tokens[0], tokens[1]);
		}
		start = surveyPos;
		return 1;
	}
	// Sector1
	else if(!strcasecmp(tokens[0], "Sector1"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		if(!(pSectorBefore = DataPool::getInstance().findSectorData(tokens[1])))
		{
			PARSING_ERR2("Line %d: Unknown sector <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	// Sector2
	else if(!strcasecmp(tokens[0], "Sector2"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		if(!(pSectorAfter = DataPool::getInstance().findSectorData(tokens[1])))
		{
			PARSING_ERR2("Line %d: Unknown sector <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	// MainPart
	else if(!strcasecmp(tokens[0], "MainPart"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		if(!(pMainPart = DataPool::getInstance().findMainPartData(tokens[1])))
		{
			PARSING_ERR2("Line %d: Unknown main part <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	// IndexInSector
	else if(!strcasecmp(tokens[0], "IndexInSector"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		if(sscanf(tokens[1], "%d", &indexInSector) != 1)
		{
			PARSING_ERR3("Line %d: bad format for <%s> : <%s>", lineNo, tokens[0], tokens[1]);
		}
		return 1;
	}
	// SkipOnSynoptic
	else if(!strcasecmp(tokens[0], "SkipOnSynoptic"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens");
		short value;
		if(sscanf(tokens[1], "%hd", &value) != 1)
		{
			PARSING_ERR3("Line %d: bad format for <%s> : <%s>", lineNo, tokens[0], tokens[1]);
		}
#ifndef PVSS_SERVER_VERSION
		skipOnSynoptic = value != 0;
#endif
		return 1;
	}
	// Description - may contain several words
	else if(!strcasecmp(tokens[0], "Description"))
	{
		pAttrList->insert(tokens[0], longValue);
		return 1;
	}
	// EndDevice
	else if(!strcasecmp(tokens[0], "EndDevice"))
	{
		// Device has been read successfully. Calculate remaining parameters
		if(!name)
		{
			 name = dpName;
		}
		else
		{
			vacType = VacType::decodeName(name);
		}
		if(pMainPart)
		{
			// Check if main part is related to one of sectors
			bool isMainPartOk = false;
			if(pSectorBefore)
			{
				isMainPartOk = pSectorBefore->isInMainPart(pMainPart);
				if(isMainPartOk)
				{
					domain = pSectorBefore->getDomainInMainPart(pMainPart);
				}
				if((!isMainPartOk) && pSectorAfter)
				{
					isMainPartOk = pSectorAfter->isInMainPart(pMainPart);
					if(domain.isEmpty())
					{
						domain = pSectorAfter->getDomainInMainPart(pMainPart);
					}
				}
			}
			else if(pSectorAfter)
			{
				isMainPartOk = pSectorAfter->isInMainPart(pMainPart);
				domain = pSectorAfter->getDomainInMainPart(pMainPart);
			}
			else	// LIK 13.04.2007 - device can have main part reference, but no sector reference
			{
				isMainPartOk = true;	// TODO: can cause problems????
			}
			if(!isMainPartOk)
			{
				PARSING_ERR2("Line <%d>: main part of <%s> is not related to sector(s)",
					lineNo, name );
			}
		}
		return 0;	// Finish
	}

	// All others could be device attributes which are not interpreted by DLL
	CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "Eqp::ProcessCtlTokens" );
	pAttrList->insert(tokens[0], tokens[1]);

	// L.Kopylov 20.03.2014: if attribute 'domain' is found - add it to global list of domains
	if(!strcasecmp(tokens[0], "domain"))
	{
		DataPool::getInstance().findDomain(tokens[1], true);
	}

	return 1;
}

/*
**
**	FUNCTION
**		Compare this device with another device to order them.
**
**	PARAMETERS
**		pOtherEqp		- Pointer to another device to be compared with this one.
**		isReverseOrder	- true if eqipment is located in beam line part which
**							is used in reverse order, i.e. start>end
**
**	RETURNS
**		-1	- This device shall be before other device
**		1	- This device shall be after other device
**		0	- Order of two devices does not matter
**
** CAUTIONS
**		None
*/
int Eqp::compare(Eqp *pOtherEqp, bool isReverseOrder)
{
	if(isReverseOrder)
	{
		if(start < pOtherEqp->start)
		{
			return 1;
		}
		if(start > pOtherEqp->start)
		{
			return -1;
		}
		// Start coordinates are the same, further analysis is needed...
		// If two passive equipments - the one with less length shall come first
		if((type == EqpType::Passive) && (pOtherEqp->type == EqpType::Passive))
		{
			if(length < pOtherEqp->length)
			{
				return 1;
			}
			if(length > pOtherEqp->length)
			{
				return -1;
			}
		}
		// For controllable equipment with the same coordinate take into account sector(s)
		if((type != EqpType::VacDevice) || (pOtherEqp->type != EqpType::VacDevice))
		{
			return 0;
		}
		if(pSectorBefore == pOtherEqp->pSectorAfter)
		{
			return -1;
		}
		if(pSectorAfter == pOtherEqp->pSectorBefore)
		{
			return 1;
		}
	}
	else
	{
		if(start < pOtherEqp->start)
		{
			return -1;
		}
		if(start > pOtherEqp->start)
		{
			return 1;
		}
		// Start coordinates are the same, further analysis is needed...
		// If two passive equipments - the one with less length shall come first
		if((type == EqpType::Passive) && (pOtherEqp->type == EqpType::Passive))
		{
			if(length < pOtherEqp->length)
			{
				return -1;
			}
			if(length > pOtherEqp->length)
			{
				return 1;
			}
		}
		// For controllable equipment with the same coordinate take into account sector(s)
		if((type == EqpType::VacDevice) || (pOtherEqp->type == EqpType::VacDevice))
		{
			if(pSectorBefore == pOtherEqp->pSectorAfter)
			{
				return 1;
			}
			if(pSectorAfter == pOtherEqp->pSectorBefore)
			{
				return -1;
			}
		}
	}
	// No different values - order by name
	if(name && pOtherEqp->name)
	{
		return strcmp(name, pOtherEqp->name);
	}
	return 0;
}

void Eqp::addDpeNameToList(QStringList &list, const char *dpeName)
{
	char buf[512];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%s.%s", dpName, dpeName);
#else
	sprintf(buf, "%s.%s", dpName, dpeName);
#endif
	list.append(buf);
}

/*
**
**	FUNCTION
**		Find name of PLC, controlling this device. Default implementation
**		is simple, but, for example, VVS_PSB_SUM will use more complex
**		algorithm, hence, it must override this method
**
**	PARAMETERS
**		None
**
**	RETURNS
**		DP name of PLC; or
**		NULL if there is no PLC for this device
**
** CAUTIONS
**		None
*/
const QString Eqp::findPlcName(void)
{
	if (mobileType != MobileType::Instance)
		return pAttrList->value("PLC");
	else
		return comDpName;
}

/*
**
**	FUNCTION
**		Set selected state of this device. Default implementation just emits signal,
**		for complex selection objects (like VPCI+VPI) method shall be reimplemented
**
**	PARAMETERS
**		selected	- New selection state
**		isFromPvss	- Flag indicating if this is original call from PVSS
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void Eqp::setSelected(bool selected, bool /* isFromPvss */)
{
	if(this->selected == selected)
	{
		return;
	}
	this->selected = selected;
	emit selectChanged(this);
}

/*
**
**	FUNCTION
**		Connect another object to given DPE of this object. Method is used when
**		object requesting connection knows exactly what DPE is needed, even if
**		this object can not know what to do with that DPE
**
**	PARAMETERS
**		pDst	- Pointer to object that shall be notified in case of changes
**		dpeName	- Name of DPE which shall be connected
**		mode	- Data acquisition mode
**
**	RETURNS
**		-1	- This device shall be before other device
**		1	- This device shall be after other device
**		0	- Order of two devices does not matter
**
** CAUTIONS
**		None
*/
void Eqp::connectDpe(InterfaceEqp *pDst, const char *dpeName, DataEnum::DataMode mode)
{
	// Is this new connection?
	DpeConnection *pConnect = NULL;

	// Check also if this the very first connection of this device, if yes -
	// connection to PLC will also be done if this device is controlled by PLC
	bool needPlcConnect = true;
	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pConn = pDpeConnections->at(idx);
		if(!strcmp(pConn->getName(), dpeName))
		{
			pConnect = pConn;
		}
		if(pConn->getCount(mode) > 0)
		{
			needPlcConnect = false;
		}
	}
	if(!pConnect)
	{
		pConnect = new DpeConnection(dpeName);
		pDpeConnections->append(pConnect);
	}

	// Connect to PLC if required
	if(needPlcConnect)
	{
		// The most of devices use 1 PLC, but some use more
		QList<Eqp *> plcList;
		if(getPlcList(plcList))
		{
			for(int idx = 0 ; idx < plcList.size() ; idx++)
			{
				Eqp *pPlc = plcList.at(idx);
				pPlc->connect(this, mode);
			}
		}
		else
		{
			const QString plcName = findPlcName();
			if(!plcName.isEmpty())
			{
				Eqp *pPlc = DataPool::getInstance().findPlc(plcName.toLatin1());
				if(pPlc)
				{
					pPlc->connect(this, mode);
				}
			}
		}
	}

	// Is connection to DPE needed?
	if(!pConnect->getCount(mode))
	{
		switch(mode)
		{
		case DataEnum::Online:
			DpConnection::connect(dpName, dpeName);
			break;
		case DataEnum::Replay:
#ifndef PVSS_SERVER_VERSION
			ReplayPool::addDpe(dpName, dpeName);
#endif
			break;
		case DataEnum::Polling:
			DpPolling::connect(dpName, dpeName);
			break;
		}
	}
	pConnect->plusCount(mode);

	// If necessary - connect destination object to signals of this object
	if(!pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->append(pDst);
		QObject::connect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
		QObject::connect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
	}
}

void Eqp::connectSelection(InterfaceEqp *pDst)
{
	if(!pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->append(pDst);
		QObject::connect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
	}
}

/*
**
**	FUNCTION
**		Disconnect another object from given DPE of this object. Method is used when
**		object requesting connection knows exactly what DPE is needed, even if
**		this object can not know what to do with that DPE
**
**	PARAMETERS
**		pDst	- Pointer to object that requested disconnect
**		dpeName	- Name of DPE which shall be disconnected
**		mode	- Data acquisition mode
**
**	RETURNS
**		-1	- This device shall be before other device
**		1	- This device shall be after other device
**		0	- Order of two devices does not matter
**
** CAUTIONS
**		None
*/
void Eqp::disconnectDpe(InterfaceEqp *pDst, const char *dpeName, DataEnum::DataMode mode)
{
	// Find connection
	DpeConnection *pConnect = NULL;

	// Check also if disconnection from PLC will be needed - if this is the very last
	// DPE to disconnect on this device
	bool needPlcDisconnect = true;
	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pConn = pDpeConnections->at(idx);
		if(!strcmp(pConn->getName(), dpeName))
		{
			pConnect = pConn;
			if(pConn->getCount(mode) > 1)	// Compare to 1 because we'll decrement counter
			{
				needPlcDisconnect = false;
			}
		}
		else
		{
			if(pConn->getCount(mode) > 0)
			{
				needPlcDisconnect = false;
			}
		}
	}
	if(!pConnect)
	{
		return;
	}

	// Disconnect from PLC if required
	if(needPlcDisconnect)
	{
		// The most of devices use 1 PLC, but some use more
		QList<Eqp *> plcList;
		if(getPlcList(plcList))
		{
			for(int idx = 0 ; idx < plcList.size() ; idx++)
			{
				Eqp *pPlc = plcList.at(idx);
				pPlc->disconnect(this, mode);
			}
		}
		else
		{
			const QString plcName = findPlcName();
			if(!plcName.isEmpty())
			{
				Eqp *pPlc = DataPool::getInstance().findPlc(plcName.toLatin1());
				if(pPlc)
				{
					pPlc->disconnect(this, mode);
				}
			}
		}
	}

	// Is disconnection needed?
	pConnect->minusCount(mode);
	if(!pConnect->getCount(mode))
	{
		switch(mode)
		{
		case DataEnum::Online:
			DpConnection::disconnect(dpName, dpeName);
			break;
		case DataEnum::Replay:
#ifndef PVSS_SERVER_VERSION
			ReplayPool::removeDpe(dpName, dpeName);
#endif
			break;
		case DataEnum::Polling:
			DpPolling::disconnect(dpName, dpeName);
			break;
		}
	}

	// If necessary - disconnect destination object from signals of this object
	// Note that destination object will be disconnected from signals on FIRST
	// call to this method
	if(pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->removeOne(pDst);
		QObject::disconnect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
		QObject::disconnect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
	}
}

void Eqp::disconnectSelection(InterfaceEqp *pDst)
{
	if(pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->removeOne(pDst);
		QObject::disconnect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
	}
}

/*
**
**	FUNCTION
**		Return last known value for given DPE
**
**	PARAMETERS
**		dpeName	- Name of DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		Last known DPE value, or
**		dummy value if no last known
**
** CAUTIONS
**		None
*/
const QVariant &Eqp::getDpeValue(const char *dpeName, DataEnum::DataMode mode, QDateTime &timeStamp)
{
	static QVariant dummy;
	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pDpe = pDpeConnections->at(idx);
		if(!strcmp(pDpe->getName(), dpeName))
		{
			return pDpe->getValue(mode, timeStamp);
		}
	}
	return dummy;
}

/*
**
**	FUNCTION
**		Calculate string presentation of current error for given mode
**
**	PARAMETERS
**		mode	- Data acquisition mode
**		result	- Variable where resulting error text will be written
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void Eqp::getErrorString(DataEnum::DataMode mode, QString &result)
{
	result = "";
	int errCode = getErrorCode(mode);
	if(!errCode)
	{
		return;
	}
	char resourceName[256];
	if(dpType)
	{
#ifdef Q_OS_WIN
		sprintf_s(resourceName, sizeof(resourceName) / sizeof(resourceName[0]), "%s.Error_%d", dpType, errCode);
#else
		sprintf(resourceName, "%s.Error_%d", dpType, errCode);
#endif
		if(ResourcePool::getInstance().getStringValue(resourceName, result) == ResourcePool::OK)
		{
			return;
		}
	}
#ifdef Q_OS_WIN
	sprintf_s(resourceName, sizeof(resourceName) / sizeof(resourceName[0]), "Error_%d", errCode);
#else
	sprintf(resourceName, "Error_%d", errCode);
#endif
	if(ResourcePool::getInstance().getStringValue(resourceName, result) == ResourcePool::OK)
	{
		return;
	}
#ifdef Q_OS_WIN
	sprintf_s(resourceName, sizeof(resourceName) / sizeof(resourceName[0]), "Unknown error (%d)", errCode);
#else
	sprintf(resourceName, "Unknown error (%d)", errCode);
#endif
	result = resourceName;
}

// Default implementation for newOnlineValue() methods - just send signal
void Eqp::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	// Processing EqpStatus DPE is the same for all types.
	// Processing is limited:
	//	status NotControl can not be changed by online value
	if(!strcmp(dpe, "EqpStatus"))
	{
		if(ctlStatusOnline == NotControl)
		{
			return;
		}
		if(value.canConvert(QVariant::Int))
		{
			int newValue = value.toInt();
			if(ctlStatusOnline == (CtlStatus)newValue)
			{
				return;
			}
			if((CtlStatus)newValue == NotControl)
			{
				return;	// Status NotControl can not be set with online/replay data
			}
			ctlStatusOnline = (CtlStatus)newValue;
		}
	}
	
	// First store value for future use...
	bool dpeFound = false;
	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pDpe = pDpeConnections->at(idx);
		if(!strcmp(pDpe->getName(), dpe))
		{
			pDpe->setValue(DataEnum::Online, value, timeStamp);
			dpeFound = true;
		}
	}
	if(!dpeFound)
	{
		DpeConnection *pConnect = new DpeConnection(dpe);
		pDpeConnections->append(pConnect);
		pConnect->setValue(DataEnum::Online, value, timeStamp);
	}


	// ... then emit signal
	emit dpeChanged(this, dpe, DataEnum::Own, value, DataEnum::Online, timeStamp);
}

// Default implementation for newReplayValue() method - just send signal
#ifndef PVSS_SERVER_VERSION
void Eqp::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	// Processing EqpStatus DPE is the same for all types.
	// Processing is limited:
	//	status NotControl can not be changed by online value
	if(!strcmp(dpe, "EqpStatus"))
	{
		if(ctlStatusReplay == NotControl)
		{
			return;
		}
		if(value.canConvert(QVariant::Int))
		{
			int newValue = value.toInt();
			if(ctlStatusReplay == (CtlStatus)newValue)
			{
				return;
			}
			if((CtlStatus)newValue == NotControl)
			{
				return;	// Status NotControl can not be set with online/replay data
			}
			ctlStatusReplay = (CtlStatus)newValue;
		}
	}

	// First store value for future use...
	bool dpeFound = false;
	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pDpe = pDpeConnections->at(idx);
		if(!strcmp(pDpe->getName(), dpe))
		{
			pDpe->setValue(DataEnum::Replay, value, timeStamp);
			dpeFound = true;
		}
	}
	if(!dpeFound)
	{
		DpeConnection *pConnect = new DpeConnection(dpe);
		pDpeConnections->append(pConnect);
		pConnect->setValue(DataEnum::Replay, value, timeStamp);
	}

	// ... finally emit signal
	emit dpeChanged(this, dpe, DataEnum::Own, value, DataEnum::Replay, timeStamp);
}

void Eqp::getInterlockBorderColor(QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList)
{
	MainEqpStatus overallStatus = getMainStatus();
	if(pNeighborList != NULL)
	{
		for(int idx = 0 ; idx < pNeighborList->count() ; idx++)
		{
			Eqp *pNeighbor = pNeighborList->at(idx);
			MainEqpStatus status = pNeighbor->getMainStatus();
			if(status > overallStatus)
			{
				overallStatus = status;
			}
		}
	}
	switch(overallStatus)
	{
	case Good:
		order = 0;
		borderColor.setRgb(0, 255, 0);	// GREEN
		break;
	case Bad:
		order = 9;
		borderColor.setRgb(255, 0, 0);	// RED
		break;
	default:
		order = 10;
		borderColor.setRgb(0, 0, 255);	// BLUE
		break;
	}
}

/**
@brief FUNCTION Check mobile type and decide if this eqp active state may change
*/
bool Eqp::isActiveMayChange() {
	if (mobileType == MobileType::Instance)
		return true;
	else
		return false;
}

#endif

