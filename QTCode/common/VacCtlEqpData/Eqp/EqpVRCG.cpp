//	Implementation of EqpVRCG class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVRCG.h"

#include "FunctionalType.h"
#include "EqpMsgCriteria.h"
#include "DataPool.h"

EqpVRCG::EqpVRCG(const Eqp &source) : Eqp(source)
{
	functionalType = FunctionalType::VRCG;
}

void EqpVRCG::postProcess(void)
{
//qDebug("postProcess(%s)\n", dpName);
	// Find reference to sector(s): sector(s) of connected gauges
	// Too slow - may be will be used later?
	return;
	QStringList slaves;
	getSlaves(slaves);
	DataPool &pool = DataPool::getInstance();
	for(int idx = 0 ; idx < slaves.count() ; idx++)
	{
		QString dpName = slaves.at(idx);
		Eqp *pEqp = pool.findEqpByDpName(dpName.toLatin1().constData());
		if(pEqp->getSectorBefore())
		{
			if(!pSectorBefore)
			{
				pSectorBefore = pEqp->getSectorBefore();
			}
			else if(!pSectorAfter)
			{
				pSectorAfter = pEqp->getSectorAfter();
			}
			else
			{
				break;
			}
		}
	}
}

/*
**	FUNCTION
**		Build list of slave DP names
**
**	ARGUMENTS
**		slaves	- Variable where slave DP names shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVRCG::getSlaves(QStringList &slaves)
{
	slaves.clear();
	DataPool &pool = DataPool::getInstance();
	const QHash<QByteArray, Eqp *> &dict = pool.getEqpDict();
	foreach(Eqp *pEqp, dict)
	{
		const QString attrValue = pEqp->getAttrValue("MasterName");
		if(!attrValue.isEmpty())
		{
			if(attrValue == dpName)
			{
				slaves.append(pEqp->getDpName());
			}
		}
	}
}

