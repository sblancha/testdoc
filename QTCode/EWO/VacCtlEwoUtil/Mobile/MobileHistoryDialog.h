#ifndef	MOBILEHISTORYDIALOG_H
#define	MOBILEHISTORYDIALOG_H

// History of mobile equipment (VPGM, BAKEOUT) + history of VG in the same sectors

#include "DataEnum.h"
#include "Eqp.h"
#include "EqpMsgCriteria.h"

#include <QDialog>


class MobileHistoryImage;
class PlotImage;

class MobileEqpTree;
#include <QTreeWidgetItem>
class BakeoutChannelTreeItem;
#include <QLabel>
#include <QCheckBox>
#include <QSpinBox>
#include <QPushButton>
#include <QMenu>
#include <QDateEdit>
#include <QDate>
#include <QDateTime>
#include <QScrollBar>

class Sector;

class MobileHistoryDialog : public QDialog
{
	Q_OBJECT

public:
	MobileHistoryDialog();
	~MobileHistoryDialog();

	static void create(void);
	static bool addHistoryValue(int rangeId, const char *dpe, const QDateTime &time, const QVariant &value);
	static void finishHistoryRange(int rangeId, const char *dpe);
	static int setDpHistoryDepth(const char *dpName, const QDateTime &absStartTime);

signals:
	void dpMouseDown(int button, int mode, int x, int y, int extra, const char *dpName);

protected:

	// The only instances of dialog
	static MobileHistoryDialog *pInstance;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Check box to switch between current and historical content of tree
	QCheckBox			*pTreeStateCheckBox;

	// Date editor for start date of tree content
	QDateEdit			*pDateEdit;

	// Tree of main parts, sectors and devices
	MobileEqpTree		*pTree;

	// History image
	MobileHistoryImage	*pImage;

	// Horizontal scroll bar below image to scroll time range
	QScrollBar			*pScrollBar;

	// Label where time of mouse pointer is shown
	QLabel			*pTimeLabel;

	// Label where value of selected device at selected time is shown
	QLabel			*pValueLabel;

	// Checkbox to switch ON/OFF online mode of history
	QCheckBox		*pOnlineCheckBox;

	// Spin box to change number of minutes in displayed time range
	QSpinBox		*pMinSpinBox;

	// Spin box to change number of hours in displayed time range
	QSpinBox		*pHourSpinBox;

	// Spin box to change number of days in displayed time range
	QSpinBox		*pDaySpinBox;

	// Flag indicating if SpinBox value is set from code, so range cnange is not needed
	bool			settingSpinBoxes;

	// Flag indicating if ScrollBar value is set from code, so range cnange is not needed
	bool			settingScrollBar;

	// Flag indicating if Online check box is set from code
	bool			settingOnline;

	// Falg indicating if rack channel is being selected
	bool			settingSelection;

	void buildLayout(void);

	void rebuildTreeContent(DataEnum::DataMode mode);
	QTreeWidgetItem *addSectorToTree(Sector *pSector, QTreeWidgetItem **ppMpItem);
	void addSectorEqp(QTreeWidgetItem *pSectItem, Sector *pSector, const QList<Eqp *> &activeEqpIter, int &eqpIndex);

	void setEqpSelection(QTreeWidgetItem *pItem);
	void setRackChannelSelection(QTreeWidgetItem *pItem);
	void updateTreeCheckBoxes(QTreeWidgetItem *pItem);
	void updateEqpCheckBoxes(QTreeWidgetItem *pItem);
	void updateRackCheckBoxes(QTreeWidgetItem *pItem);
	void updateRackChannelCheckBoxes(QTreeWidgetItem *pItem);
	void updateRackChannelAppearance(BakeoutChannelTreeItem *pEqpItem);

	void showTimeRange(void);
	void setRangeFromSpinBoxes(void);

private slots:
	void treeCtlStateChange(bool on);
	void treeCtlDateChange(const QDate &startDate);

	void scrollBarValueChange(int value);
	void scrollBarSliderRelease(void);

	void onlineChange(bool on);

	void minChange(int value);
	void hourChange(int value);
	void dayChange(int value);

	void filePrint(void);
	void fileExport(void);
	void closePress(void);
	void helpPress(void);

	void eqpSelectChanged(Eqp *pEqp);
	void valueAtPointerChanged(const char *timeStr, const char *valueStr);
	void imageZoomed(void);
	void updateScrollBar(void);

	void eqpListChanged(DataEnum::DataMode mode);

	// Process tree signals
	void treeSelectionChanged(void);
	void treeCheckBoxPressed(QTreeWidgetItem *pItem);
};

#endif	// MOBILEHISTORYDIALOG_H
