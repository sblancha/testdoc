#uses "VacCtlEqpDataPvss"	// Load DLL
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "email.ctl"	// Load PVSS standard CTRL library
#uses "vclDevCommon.ctl"	// Load CTRL library
#uses "vclResources.ctl"   // Load CTRL library
#uses "vclMachine.ctl"		// Load CTRL library
#uses "vclSectors.ctl"	// Load CTRL library
#uses "vclAlarmMaskConfig.ctl"
#uses "vclOscEqp.ctl"
//#uses "lhcVacSectors.ctl"

// Name of DP where alarm masking configuration (sectors) is stored
string			configDpName;

// Name of DP where alarm E-mail notification configuration is stored
string reportDpName;

// Name of DP where configuration for dynmaic alarm mask is stored
string eqpOscConfigDp;

// Name of DP where configuration for automatic release of blocked alarms is stored
string releasDpName;

// List of sector names blocked by human
dyn_string	mSectorList;

// List of individual DP names belonging to sectors in mSectorList
dyn_string sectBlockList;

// List of DP names with alarms blocked by human
dyn_string manStopList;

// List of DP names, blocked by human, which are in 'good' state now
dyn_string proposeToReleas;


// List of DP names blocked automatically
dyn_string	alStopList;


// List of all DP names from where alarm callbacks arrived - for statistics
dyn_string alStatList;

// List of alarms change counters - one counter for every DP in alStatList
dyn_int			alCountList;

// List of timestamps when statistic collection started - one time for every DP in alStatList
dyn_time 		alTimeList;


// List of all DP names which were automatically blocked - for statistics
dyn_string blackStatList;

// List of alarms change counters - one counter for every DP in blackStatList
dyn_int			blackCountList;

// List of timestamps when statistic collection started - one time for every DP in blackStatList
dyn_time 		blackTimeList;


// List of all DP names which were manually blocked - for statistics
dyn_string grayStatList;

// List of alarms change counters - one counter for every DP in grayStatList
dyn_int			grayCountList;

// List of timestamps when statistic collection started - one time for every DP in grayStatList
dyn_time 		grayTimeList; 

// List of flags indicating that manually masked alarm DP is in good condition now
dyn_bool		grayResultList;   

// List of all DP names which were changing their state during the day 
dyn_string genStatList;

// List of change counters - one counter for every DP in genStatList
dyn_int		genCountList;


//Alarm queue
dyn_string	alarmQue;
//Alarm queue
dyn_time	 alarmTime;


// Limit for number of alarm events - count higher this causes alarm blocking
int 				nMax;

// Time interval [sec] to count alarm events for alarm blocking
int blockTimeLimit;

// Time interval [sec] to count alarm events for alarm release
int relTimeLimit;



// Flag to indicate that blocked alarm list DPE is being updated by this script itself
bool				blListChanged;

// Allow automatic release of automatically blocked DPs
bool allowReleas;

// Allow sending notification E-mails
bool allowNotif;

//	"black" lists for processing automatically blocked alarm due to oscillation
//	 "gray" lists for processing manualy blocked - check if oscillation goes down and notify  
        
// SMTP server
string smtpServer;

// E-mail sender
string sender;

// E-mail sender client
string	client;

// Recipients for alarm mask in all sectors
dyn_string	recipients;

//List of all sectors;
dyn_string  sectorList;

//Debug flag
bool debug = false;
bool debug0 = false;
////////////////

main()
{
	if( ! InitStaticData( ) ) return;

	dyn_string	exceptionInfo;

	VacAlarmMaskReleasDp(releasDpName, exceptionInfo );
	if(dynlen(exceptionInfo))
	{
		DebugTN("vcsMaskLASER.ctl ERROR: Processing in VacAlarmMaskReleasDp", exceptionInfo);
		return;
  }
	VacAlarmMaskConfigDp(configDpName, exceptionInfo );
	if(dynlen(exceptionInfo))
	{
		DebugTN("vcsMaskLASER.ctl ERROR: Processing in VacAlarmMaskConfigDp", exceptionInfo);
		return;
  }
	VacAlarmMaskReportDp(reportDpName, exceptionInfo );
	if(dynlen(exceptionInfo))
	{
		DebugTN("vcsMaskLASER.ctl ERROR: Processing in VacAlarmMaskReportDp", exceptionInfo);
	}
	if(debug)  
	{
		DebugTN("vcsMaskLASER.ctl: reportDpName", reportDpName);
	}
	LhcVacOscEqpDp(eqpOscConfigDp, exceptionInfo);
	if(dynlen(exceptionInfo))
	{
		DebugTN("vcsMaskLASER.ctl ERROR: Processing in LhcVacOscEqpDp", exceptionInfo);
		return;
  }
  dpConnect("EqpOscManListChanged", eqpOscConfigDp + ".manAlBlockList");
	dpConnect("SectorMaskChanged", configDpName + ".list");

  DebugTN("vcsMaskLASER.ctl SECTOR LIST:", dynlen(sectorList));

//Check whether sectors in the mask list still exist
	int i = 0;
	for( int n = dynlen(mSectorList) ; n > 0 ; n-- )
	{
		if(dynContains(sectorList, mSectorList[n]) == 0)
		{
			DebugTN("vcsMaskLASER.ctl NOT Anymore in sector list - REMOVE:", mSectorList[n]);
			dynRemove(mSectorList, n);
			i++;
		}
	}
	if(i)
		dpSet(configDpName + ".list", mSectorList);
	
// Found all dpTypes
	int nTypes = dynlen(glLhcVacDpTypes);
	for(int n = 1; n <= nTypes; n++)
	{
// Found all dp for selected dpType
		dyn_string dpNs = dpNames("*", glLhcVacDpTypes[n]);
		for(i = dynlen(dpNs) ; i > 0 ; i--)
		{
			string eqpName = dpSubStr(dpNs[i], DPSUB_DP);
			bool dpIsBlocked = false;
      for(int alIdx = 1 ; alIdx < 4 ; alIdx++)
			{
				if(dpExists(eqpName + ".AL" + alIdx) && dpExists(eqpName + ".AL" + alIdx + "_LASER"))
				{
					if(alIdx == 1)
					{
						int dpf;
						dpGet(eqpName + ".AL1_LASER:_dp_fct.._type", dpf);
						if(dpf == DPCONFIG_NONE)
						{
							dpIsBlocked = true;
							int mb = dynContains(manStopList, eqpName);
							int sb = dynContains(sectBlockList, eqpName);
							if ((mb+sb) > 0)
							{
								if(debug)  
								{
									DebugTN("vcsMaskLASER.ctl: AlreadyBlockedManuallyOrSector:", eqpName);
								}
							}
							else
							{
	 							synchronized (alStopList)
	     					{
									if(dynContains(alStopList, eqpName) < 1 )
									{
										dynAppend(alStopList, eqpName);
									}	
								}
							}
						}
					}
					dpConnect("AlarmCb", false, eqpName + ".AL" + alIdx);
				}
			}
			BlockDpFunction(eqpName, dpIsBlocked);
		}
	}

  if(debug)  
		DebugTN("vcsMaskLASER.ctl: GotAlarmBlockFromDp:", alStopList);

	dpSetWait(eqpOscConfigDp + ".alarmStopList", alStopList);

  dpConnect("EqpOscAlListChanged",  eqpOscConfigDp + ".alarmStopList"); 
 	dpConnect("EqpOscParamChanged",  eqpOscConfigDp + ".blockTimeLap", eqpOscConfigDp + 
 			".relTimeLap", eqpOscConfigDp + ".alThresh", eqpOscConfigDp + 
 			".allowAutoReleas", eqpOscConfigDp + ".sendNotif");

  if(ConnectToEmailConfig( ) == false)
	{
		DebugTN("vcsMaskLASER.ctl: ConnectToEmailConfig( ) FATAL_ERROR ");
//TBD what to do if notification service is not working
	}
	int relTime;
	dpGet(eqpOscConfigDp + ".relTimeLap", relTime);
	relTimeLimit = relTime*60; //in seconds
// Send mail about mask every working day at 8:00
	if(debug)
	{
		dpSet( reportDpName + ".validFrom", makeTime(2006,6,1),
      	reportDpName + ".validUntil", makeTime(2032,6,1),
        reportDpName + ".time", makeDynInt(), 				
        reportDpName + ".monthDay", makeDynInt(), 				
       	reportDpName + ".month", makeDynInt(), 				
        reportDpName + ".weekDay", makeDynInt(1,2,3,4,5,6,7), 
        reportDpName + ".month", makeDynInt(), 				
     		reportDpName + ".delay", 0,
      	reportDpName + ".interval", 4*relTimeLimit,
     		reportDpName + ".syncDay", -1,
     		reportDpName + ".syncWeekDay", -1,
    		reportDpName + ".syncTime", -1, 
     		reportDpName + ".syncMonth", -1);
  }
  else
 	{
 		dpSet( reportDpName + ".validFrom", makeTime(2006,6,1),
      	reportDpName + ".validUntil", makeTime(2032,6,1),
	      reportDpName + ".time", makeDynInt(28800), 				// 8:00 in the morning
	      reportDpName + ".time", makeDynInt(), 				// 8:00 in the morning
        reportDpName + ".time", makeDynInt(), 				
        reportDpName + ".monthDay", makeDynInt(), 				
       	reportDpName + ".month", makeDynInt(), 				
        reportDpName + ".weekDay", makeDynInt(1,2,3,4,5,6,7), 		//around the week		
        reportDpName + ".month", makeDynInt(), 				
     		reportDpName + ".delay", 0,
      	reportDpName + ".interval", -1,
     		reportDpName + ".syncDay", -1,
     		reportDpName + ".syncWeekDay", -1,
    		reportDpName + ".syncTime", -1, 
     		reportDpName + ".syncMonth", -1);
  }
 if(debug)  
	{
		DebugTN("vcsMaskLASER.ctl: Notification time initialized (sec) " + 4*relTimeLimit);
	}
//
//Config DP for timing loop to check non-oscillating alarm and releasing their blocking
//        
	if(dynlen(exceptionInfo))
	{
		DebugTN("vcsMaskLASER.ctl ERROR: Processing in VacAlarmMaskReleasDp", exceptionInfo);
	}
 	if(debug)
 	{  
		DebugTN("vcsMaskLASER.ctl: releasDpName", releasDpName);        
  }
	dpSet( 	releasDpName + ".validFrom", makeTime(2006,6,1),
      		releasDpName + ".validUntil", makeTime(2032,6,1),
        	releasDpName + ".time", makeDynInt(), 				// 8:00 in the morning
        	releasDpName + ".monthDay", makeDynInt(), 				
       		releasDpName + ".month", makeDynInt(), 				
        	releasDpName + ".weekDay", makeDynInt(1,2,3,4,5,6,7), 		//working days		
        	releasDpName + ".month", makeDynInt(), 				
     			releasDpName + ".delay", 0,
      		releasDpName + ".interval", relTimeLimit,
    			releasDpName + ".syncDay", -1,
     			releasDpName + ".syncWeekDay", -1,
    			releasDpName + ".syncTime", -1, 
     			releasDpName + ".syncMonth", -1);
	timedFunc ("CheckForNonOscillating", releasDpName);
	UpdateBlackList();
	timedFunc ("SendMessages", reportDpName);
	DebugTN("vcsMaskLASER.ctl: Init completed!!", relTimeLimit);
	DebugTN("vcsMaskLASER.ctl: Check for nonoscillating alarm each (sec)", relTimeLimit);
	while(1)
	{
		ProcessAlarmCb();
		delay(1);
	}	
}
void InitReleaseTimedDp()
{
// Check for nonoscillating alarm each relTimeLimit seconds
		dpSetWait( 	releasDpName + ".interval", relTimeLimit);
//		dpSetWait( 	reportDpName + ".interval", relTimeLimit*2);
}
void BlockDpFunction(string dp, bool blockAlarm)
{
	string p1, p2, dpFunc;
	for(int alIdx = 1 ; alIdx < 4 ; alIdx++)
	{
		if(dpExists(dp + ".AL" + alIdx) && dpExists(dp + ".AL" + alIdx + "_LASER"))
		{
			if(debug && alIdx == 1)
			{  
				DebugTN("vcsMaskLASER.ctl: BlockDpFunction:", dp, blockAlarm);
			}
			p2 =  dp + ".AL" + alIdx + "_LASER";    
			if(blockAlarm)
			{
				dpSetWait(p2 + ":_dp_fct.._type",  DPCONFIG_NONE);
				dpSetWait(p2,  false);
			}
			else
			{          
				p1 = dp + ".AL" + alIdx + ":_online.._value";
				dyn_string  ds = makeDynString(p1); 
				dpFunc  =   "p1";             
				dpSetWait(p2 + ":_dp_fct.._type", DPCONFIG_DP_FUNCTION,
					p2 + ":_dp_fct.._param", ds, 
   				p2 +":_dp_fct.._fct",  dpFunc);            
			}
		}
	}
}
// Callback process of modification in the manual alarm block lists 
void EqpOscManListChanged(string dpMan, dyn_string manList)
{
	manStopList = manList;
	UpdateGreyList();
}

// Callback process of modification of config parameters 

void EqpOscParamChanged(string dpInB, int blockTime,  string dpInR, int relTime, string dpN, int nAlMax, 
                        string dpFlag, bool autoFlag,  string dpMail, bool notifFlag)
{
	nMax = nAlMax;  
	blockTimeLimit = blockTime*60;
	relTimeLimit = relTime*60; 
	dpSetWait( 	releasDpName + ".interval", relTimeLimit);
	allowReleas =  autoFlag;
	allowNotif =  notifFlag;

     
	if(debug)  
		DebugTN("vcsMaskLASER.ctl: NotifRelis:", notifFlag, autoFlag);     

}
void EqpOscAlListChanged(string dpAl, dyn_string alList)
{
	int 	i, n;
	if(blListChanged)
	{
		blListChanged = false;
  }
}
void EqpOscAlListChanged0(string dpAl, dyn_string alList)
{
	int 	i, n;
	if(blListChanged)
	{
		blListChanged = false;
		return;     
  }
//Match new blocking list with actually blocked
//First check if all dp in config list are in blocking list. If not - remove;
	for(i=dynlen(alStopList); i >= 1 ; i--)
	{
			n = dynContains(alList, alStopList[i]);
			if (n > 0) 
			{
				continue;
			}	 
			BlockDpFunction(alStopList[i], false);
			synchronized (alStopList)
     	{
				dynRemove(alStopList, i);
			}	
	}
	for(i=dynlen(alList); i >= 1 ; i--)
	{
			n = dynContains(alStopList, alList[i]);
			if (n > 0) 
			{
				continue;
			}	 
			BlockDpFunction(alStopList[i], true);
			synchronized (alStopList)
     	{
				dynAppend(alStopList, alList[i]);
			}
	}
	if(debug)  
	{
		DebugTN("vcsMaskLASER.ctl: alStopList from Config:", alStopList);  
	}
}

//
//	Process modification of sectors alarm mask
//	dpName - configDp
//	list - list of masked sectors
//
void SectorMaskChanged(string dpName, dyn_string list)
{
	dyn_string	newSectors, removeSectors, deviceList, exceptionInfo;
	int	i, n, coco;
	for(i=1; i<= dynlen(list); i++)
	{
		n = dynContains(mSectorList, list[i]);
		if (n > 0) continue;
		dynAppend(newSectors, list[i]);
	}			
	for(i=1; i<= dynlen(mSectorList); i++)
	{
		n = dynContains( list, mSectorList[i]);
		if (n > 0)
			continue;
		dynAppend(removeSectors, mSectorList[i]);
	}
//First find all devices belong to recently Blocked sectors
	LhcVacGetDevicesAtSectors( newSectors, deviceList, exceptionInfo );
	if(dynlen(exceptionInfo))
	{
		DebugTN("vcsMaskLASER.ctl ERROR: LhcVacGetDevicesAtSectors");
	}

//And block Alarm for them
	for(i=1; i<= dynlen(deviceList); i++)
	{
		string name =  dpSubStr (deviceList[i], DPSUB_DP);
  if(strlen(name) == 0) continue;
   string dpType = dpTypeName (name);
		int dptMask =  LhcVacDpTypeGroup( dpType );
		if (dptMask == VAC_DP_TYPE_GROUP_VV)
		{
			if(debug)  
			{
				DebugTN("vcsMaskLASER.ctl: NoBlockingFromSectorsForValves", deviceList[i], dptMask);                  	
			}
			continue;
		}
		BlockDpFunction(deviceList[i], true);
		dynAppend(sectBlockList, deviceList[i]);
	}

//Second find all devices belong to recently Unblocked sectors
	if(dynlen(removeSectors) > 0)
	{
		dynClear(deviceList);
		LhcVacGetDevicesAtSectors( removeSectors, deviceList, exceptionInfo );
		if(dynlen(exceptionInfo))
		{
			DebugTN("vcsMaskLASER.ctl ERROR: LhcVacGetDevicesAtSectors");
		}
		if(debug)  
			DebugTN("vcsMaskLASER.ctl: UnBlockForSectors:", deviceList);
//And unblock Alarm for them
		for(i=1; i<= dynlen(deviceList); i++)
		{
			string dpType = dpTypeName (deviceList[i]);
			int dptMask =  LhcVacDpTypeGroup( dpType );
			if (dptMask == VAC_DP_TYPE_GROUP_VV)
			{
				if(debug)  
					DebugTN("vcsMaskLASER.ctl: NoUnBlockingFromSectorsForValves", deviceList[i], dptMask);                  	
				continue;
			}
			BlockDpFunction(deviceList[i],  false);
			int nn = dynContains(sectBlockList, deviceList[i]);
			if (nn > 0)
			{
				dynRemove(sectBlockList, nn);
			}
		}
	}	
	mSectorList = list;
}

void UpdateBlackList() synchronized(blackStatList) 
{
	dynClear(blackStatList);
	dynClear(blackCountList);
	dynClear(blackTimeList);
	for(int i=1; i <= dynlen(alStopList); i++)
	{
		dynAppend( blackStatList, alStopList[i]);
		dynAppend( blackCountList, 1);
		dynAppend( blackTimeList, getCurrentTime());
	}  
	if(debug)  
		DebugTN("vcsMaskLASER.ctl: UpdateBlackLists:", blackStatList); 
}

void UpdateGreyList() synchronized(grayStatList)
{
	dynClear(grayStatList);
	dynClear(grayCountList);
	dynClear(grayTimeList);
	dynClear(grayResultList);
	for(int i=1; i <= dynlen(manStopList); i++)
	{
		dynAppend( grayStatList, manStopList[i]);
		dynAppend( grayCountList, 1);
		dynAppend( grayTimeList, getCurrentTime());
		dynAppend( grayResultList, false);
	}  
	if(debug)  
		DebugTN("vcsMaskLASER.ctl: UpdateGreyList:", grayStatList); 
}

void CheckForNonOscillating(string timedDp, time before, time now )
{
	if(debug0)  
		DebugTN("vcsMaskLASER.ctl: CheckForNonOscillating: BlackList=========================",  blackStatList);
	dynClear(proposeToReleas);
	for(int i = dynlen(blackStatList); i >= 1; i--)
	{
		int  tp = period(getCurrentTime()) - period(blackTimeList[i]);
		if(debug)  
			DebugTN("vcsMaskLASER.ctl: Process blackList", blackStatList[i], tp, relTimeLimit);
		if (tp < relTimeLimit) 
			continue;
		if(debug)  
			DebugTN("vcsMaskLASER.ctl: TimeCheckPassed",  blackCountList[i], nMax);
		if( blackCountList[i] < nMax)   //Device is not oscillating anymore - remove blocking
		{
			int dn = dynContains(alStopList,  blackStatList[i]); 
			if (dn > 0)
			{
				if(debug)  
					DebugTN("vcsMaskLASER.ctl: CheckForNonOscillating: Release ----", blackStatList[i]);
				int gn = dynContains(grayStatList, blackStatList[i]);
				if(allowReleas)
				{  
					if(gn < 1)
					{
						BlockDpFunction(blackStatList[i], false);
						synchronized (alStopList)
     				{
							dynRemove(alStopList, dn);
						}
						blListChanged = true;
						int gs = dynContains(genStatList,  blackStatList[i]);
						if (gs > 0)
						{
							 genCountList[gs]++;
						}
						else
						{
							dynAppend(genStatList,  blackStatList[i]);
							dynAppend(genCountList, 1);
							
						}	 
					}
					synchronized(blackStatList)
					{
						dynRemove(blackStatList, i);
						dynRemove(blackCountList, i);
						dynRemove(blackTimeList, i);
					}
				} 
			}                   
		}
		else	//Doesn't pass check, try for another round
		{
			blackCountList[i] = 0;
			blackTimeList[i] = getCurrentTime();
		}
	}
	if(blListChanged)
	{
		synchronized(alStopList)
    {
			dpSetWait(eqpOscConfigDp + ".alarmStopList", alStopList);
		}	
	}
	if(debug0 && dynlen(genStatList) > 0)
	{
		for(int gn=1; gn <= dynlen(genStatList); gn++)
		{
			DebugN("vcsMaskLASER.ctl: " + genStatList[gn], genCountList[gn]);
		}
	}

//Now check Grey list - manually blocked alarm
	synchronized(grayStatList)
	{
		for(int i=1; i <= dynlen(grayStatList); i++)
		{
			int  tp =   period(getCurrentTime()) - period(grayTimeList[i]);
			if(debug)  
				DebugTN("vcsMaskLASER.ctl: Process grayList", grayStatList[i], tp, relTimeLimit);
			if (tp < relTimeLimit) 
				continue;
			if(debug)  
				DebugTN("vcsMaskLASER.ctl: TimeCheckPassed",  grayCountList[i], nMax);
			if( grayCountList[i] < nMax)   //Device is not oscillating anymore - register as good
			{
				dynAppend(proposeToReleas,  grayStatList[i]);
				grayCountList[i] = 0;
				grayTimeList[i] = getCurrentTime();
				grayResultList[i] = true;
				if(debug)  
				{
					DebugTN("vcsMaskLASER.ctl: CheckForNonOscillating: ManualToReleas ----", grayStatList[i]);
				}		
			}
			else	//Doesn't pass check, try for anote round
			{
				grayResultList[i] = false;
				grayCountList[i] = 0;
				grayTimeList[i] = getCurrentTime();
			}               
		}
		if(debug)  
			DebugTN("vcsMaskLASER.ctl: GrayList:", grayStatList);
	}
}

void AlarmCb(string dpName, bool alarm)  synchronized (alarmQue)
{
 	dynAppend(alarmQue, dpName);
	dynAppend(alarmTime, getCurrentTime());
}
void ProcessAlarmCb()
{
	if(dynlen(alarmQue) < 1)
	{	
	  return;
              }
	while(dynlen(alarmQue) >= 1)
	{
  	string pureDpName = dpSubStr(alarmQue[1], DPSUB_DP);
		time evTime = alarmTime[1];
		synchronized (alarmQue)
		{
			dynRemove(alarmQue, 1);
			dynRemove(alarmTime, 1);
		}
  	if(debug)
  	{
    	  DebugTN("vcsMaskLASER.ctl: ProcessAlarmCb", pureDpName, +" still in queue " + dynlen(alarmQue) + " events");
  	}
  	if(dynContains(alStopList,  pureDpName) > 0)		// Alarm is disabled for device - put in black list
  	{
    	  int ib = dynContains(blackStatList,  pureDpName); 
    	  if(ib > 0)		// Device is in black list - processibg
    	  {
            	if(debug){
    	    DebugTN("vcsMaskLASER.ctl: ProcessAlarmCb", pureDpName, +" IN BLACK LIST!!");
                }
                  blackCountList[ib]++;
    	  }
    	  else
    	  {
      	  synchronized(blackStatList)
     	   { 
        	      dynAppend( blackStatList, pureDpName);
        	      dynAppend( blackCountList, 1);
        	      dynAppend( blackTimeList, evTime);
      	    }	
    	  }             
   	}
	else if (dynContains(manStopList, pureDpName) > 0) //device is in manually blocked list - update statistic
	{
   	  int ib = dynContains(grayStatList,  pureDpName); 
    	  if(ib > 0)		// Device is in black list - processibg
    	  {
      	    grayCountList[ib]++;
    	  }
    	  else
    	  {
      	    synchronized(grayStatList)
     	   { 
        	  dynAppend( grayStatList, pureDpName);
        	  dynAppend( grayCountList, 1);
        	  dynAppend( grayTimeList, evTime);
      	  }	
    	}             
		}

		else			//device is not in any "color" list - just update white statistic
		{		
  		int dn = dynContains(alStatList,  pureDpName); 
  		if(dn > 0)		// Device is in stat list - processibg
  		{
    		alCountList[dn]++;
    		int  tp =   period(evTime) - period(alTimeList[dn]);
    		if(debug)  
    		{
      		DebugTN("vcsMaskLASER.ctl: Update statistics", pureDpName, alCountList[dn] + " changes", "in " +
      			 tp + " sec with lap " + blockTimeLimit + "sec");
    		}
    		if ( tp  <  blockTimeLimit ) 		//Device inside limit for oscillation  -> check for number of events
    		{
      		if  ( alCountList[dn] > nMax)	//Device exeed limit
      		{	
        		BlockDpFunction(pureDpName,  true);
        		alCountList[dn] = 0;
        		alTimeList[dn]  =  evTime; 
          	              dynAppend(alStopList, pureDpName);
        		blListChanged = true;
       		}	
    		}
    		else			//Device exeed limit for time lap  - reset statistic
    		{          
      		if(debug)
      		{
        		DebugTN("vcsMaskLASER.ctl: Reset Statistic:", pureDpName, tp);
      		}
      		alCountList[dn] = 0;
      		alTimeList[dn]  =  evTime; 
    		}
  		}  
  		else	// Device is not  in stat list - add
  		{
    		if(debug)
    		{
      		DebugTN("vcsMaskLASER.ctl: AddToStatisticList", pureDpName, dynlen(alCountList));
		    }
    		dynAppend( alStatList, pureDpName);
    		dynAppend( alCountList, 1);
    		dynAppend( alTimeList, evTime);
  		}   		
  	}
  }
  if(blListChanged)
  {
  	dpSetWait(eqpOscConfigDp + ".alarmStopList", alStopList);
  	blListChanged = false;    
  	if(debug)
  	{
     	DebugTN("vcsMaskLASER.ctl: AddToBlockingList", alStopList);
  	}
	}
}

/**InitStaticData
Purpose:
Read all machine configuration data from file(s)

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool InitStaticData( )
{
	dyn_string	exceptionInfo;
	long				coco;
	bool				isLhcData;
  dyn_string mainParts;
  dyn_dyn_string  vacSectors, sectDomains;
	LhcVacGetMachineId( exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "vcsMaskLASER.ctl: Failed to read machine name: " + exceptionInfo );
		return false;
	}
	DebugTN( "vcsMaskLASER.ctl: Machine is " + glAccelerator );
	isLhcData = glAccelerator == "LHC" ? true : false;
	VacResourcesParseFile( PROJ_PATH + "/data/VacResources.txt", glAccelerator, exceptionInfo );
	// Initalize passive machine data
	coco = LhcVacEqpInitPassive( isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo );
	if( coco < 0 )
	{
		DebugTN( "vcsMaskLASER.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo );
		return false;
	}
	DebugTN( "vcsMaskLASER.ctl: LhcVacEqpInitPassive() done" );

	// Initialzie active equipment information
	coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
			exceptionInfo );
	if( coco < 0 )
	{
		DebugTN( "vcsMaskLASER.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo );
		return false;
	}
	DebugTN( "vcsMaskLASER.ctl: LhcVacEqpInitActive() done" );
	LhcVacInitDevFunc();
	// Init sector data
  if( LhcVacGetAllSectors( mainParts, vacSectors, sectDomains, exceptionInfo ) < 0 )
  {
	  DebugTN("vcsMaskLASER.ctl: LhcVacGetAllSectors() failed",  exceptionInfo );
		  return -1;
  }
  int nMp, nMpSec, nSectors;
  int nMp = dynlen( vacSectors);
  for( int n = 1 ; n <= nMp ; n++ )
  {
    nMpSec = dynlen(vacSectors[n]);
    for( int k = 1 ; k <= nMpSec ; k++ )
    {
        dynAppend(sectorList, vacSectors[n][k]);
     }  
  }
  nSectors = dynlen(sectorList);   
//  DebugN("nSectors", nSectors);
//	if(debug)  
//		DebugTN( "vcsMaskLASER.ctl: LhcVacGetAllSectors() done" );
	return true;
}

void SendMessages(string timedDp, time before, time now )
{
  int		n, coco;
  bool 		flag;

  dpGet(eqpOscConfigDp + ".sendNotif", flag);
  if(!flag)
  {
    return;
  }
  if((dynlen(mSectorList) == 0) && (dynlen(alStopList) == 0))
  {
    if(debug)
    {
      DebugTN( "vcsMaskLASER.ctl: List of masked alarm is empty, no message to be send");
    }
    return;
  }

  // Prepare and send message
  dyn_string	email = makeDynString( "" );
  for( n = dynlen( recipients ) ; n > 0 ; n-- )
  {
    if( email[1] != "" ) email[1] += ";";
    email[1] += recipients[n];
  }
  email[2] = sender;
  email[3] = getHostname() + ":  LASER Alarm masked for " + glAccelerator;
  email[4] = "LASER Alarm masked in sectors:\n";
  for( n = dynlen( mSectorList ) ; n > 0 ; n-- )
  {
    email[4] += "\n";
    email[4] += mSectorList[n];
  }
  email[4] += "\n\n";
  email[4] += "Automatic release procedure for masked alarm is:   ";
  if(allowReleas)
  {
    email[4] += "ON";
  }
  else
  {
    email[4] += "OFF";
  }
  email[4] += "\n\n";
 	string buf;
  sprintf(buf, "%d", dynlen(alStopList));
  email[4] += "LASER Alarm masked automatically for Oscillating devices (" + buf + "):\n";
  for( n = dynlen(alStopList) ; n > 0 ; n-- )
  {
    email[4] += "\n";
    email[4] += alStopList[n];
  }
	if(dynlen(genStatList) > 0)
	{
  	email[4] += "\n\n";
  	email[4] += "Alarm MASK oscillation for device:\n";
  	email[4] += "\n\n";
  	email[4] += "Device            count:\n";
   	for( n = dynlen( genStatList ) ; n > 0 ; n-- )
    {
 			string buf;
  		sprintf(buf, "%d", genCountList[n]);
  		email[4] += genStatList[n] + "            " + buf;
  		email[4] += "\n";
  	}
		dynClear(genStatList);
		dynClear(genCountList);
	}
	if(dynlen(grayStatList) > 0)
  {
  	email[4] += "\n\n";
  	email[4] += "LASER Alarm manually masked are now in good condition:\n";
  	synchronized(grayStatList)
  	{
    	for( n = dynlen( grayStatList ) ; n > 0 ; n-- )
    	{
      	if(grayResultList[n])
      	{   
        	email[4] += "\n";
        	email[4] += grayStatList[n];
      	}       
    	}
    	email[4] += "\n\n";
    	email[4] += "LASER Alarm manually masked are still oscillating:\n";
    	for( n = dynlen(grayStatList) ; n > 0 ; n-- )
    	{
      	if(!grayResultList[n])
      	{   
        	email[4] += "\n";
        	email[4] += grayStatList[n];
      	}       
    	}
  	}
  }	
  if(debug)
  {
    DebugTN( "vcsMaskLASER.ctl: message:" + email);
  }
 
  emSendMail( smtpServer, client, email, coco );
  if( coco < 0 )	// Failed, make pause then try again
  {
    DebugTN( "vcsMaskLASER.ctl: FAILURE TO SEND MESSAGE", email);
  }
}

/** ConnectToConfig
Purpose:
Connect to DPEs containing E-mail notification configuration

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToEmailConfig( )
{
	string				configDp;
	dyn_string		exceptionInfo;
	dyn_errClass	err;


	if( dpConnect( "ServerCb", eqpOscConfigDp + ".server" ) < 0 )
	{
		err = getLastError();
		DebugTN( "vcsMaskLASER.ctl: FATAL: dpConnect(" + configDp + ".server) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "ClientCb", eqpOscConfigDp + ".client" ) < 0 )
	{
		err = getLastError();
		DebugTN( "vcsMaskLASER.ctl: FATAL: dpConnect(" + configDp + ".client) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "SenderCb", eqpOscConfigDp + ".sender" ) < 0 )
	{
		err = getLastError();
		DebugTN( "vcsMaskLASER.ctl: FATAL: dpConnect(" + configDp + ".sender) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "RecipientsCb", eqpOscConfigDp + ".recipients" ) < 0 )
	{
		err = getLastError();
		DebugTN( "vcsMaskLASER.ctl: FATAL: dpConnect(" + eqpOscConfigDp + ".recipients) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "AlarmStopListCb", eqpOscConfigDp + ".alarmStopList" ) < 0 )
	{
		err = getLastError();
		DebugTN( "vcsMaskLASER.ctl: FATAL: dpConnect(" + eqpOscConfigDp + ".alarmStopList) failed" );
		throwError( err );
		return false;
	}
        
	if(debug)  
		DebugTN("vcsMaskLASER.ctl: ConnectToEmailConfig() done");
	return true;
}

// Callbacks from DP containing general E-mail configuration
void ServerCb( string dpe, string newServer )
{
	smtpServer = newServer;
}
void ClientCb( string dpe, string newClient )
{
	client = newClient;
}
void SenderCb( string dpe, string newSender )
{
	sender = newSender;
}
void RecipientsCb( string dpe, dyn_string newRecipients )
{
	recipients = newRecipients;
  if(debug)  
		DebugTN("vcsMaskLASER.ctl: recipients", recipients);
}
void AlarmStopListCb( string dpe, dyn_string newList )
{
        dynClear(alStopList);
        dynAppend(alStopList, newList);
       if(debug)
      {  
	DebugTN("vcsMaskLASER.ctl: AlarmStopList", alStopList);
 	DebugTN("vcsMaskLASER.ctl: AlarmBlackList", blackStatList);
     }
     UpdateBlackList();
             if(debug)  
	DebugTN("vcsMaskLASER.ctl: FinalBlackList", blackStatList);

      }


