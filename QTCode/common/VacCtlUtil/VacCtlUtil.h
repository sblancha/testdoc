#ifndef VACCTLUTIL_H
#define	VACCTLUTIL_H

// Common utilities methods used by vacuum control application
#include "VacCtlUtilExport.h"

#include <stdio.h>

class VacCtlUtil
{
public:
	VACCTLUTIL_EXPORT static int sprintf(char *buf, const char *format, ...);
		
};

#endif	// VACCTLUTIL_H
