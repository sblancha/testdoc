//	Implementation of VacIconMP_ACCESS class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconMP_ACCESS.h"

#include "EqpMP_ACCESS.h"

#include <qpainter.h>
#include <qfont.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_GREEN				0,255,0
#define COLOR_RED				255,0,0


/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconMP_ACCESS::VacIconMP_ACCESS(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 15);
}

VacIconMP_ACCESS::~VacIconMP_ACCESS()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconMP_ACCESS::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(21, 15, 10, 7, 0);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconMP_ACCESS::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpMP_ACCESS"), "VacIconMP_ACCESS::setEqp", pEqp->getDpName());
		this->pEqp = (EqpMP_ACCESS *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconMP_ACCESS::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconMP_ACCESS::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("MODE");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconMP_ACCESS::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconMP_ACCESS::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/*
**	FUNCTION
**		Override VacIcon's method: forceRedaw() can come when this and parent
**		widgets are hidden
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconMP_ACCESS::forceRedraw(void)
{
	if(pEqp && connected)
	{
		if(pEqp->getMode(mode))
		{
			if(parentWidget()->inherits("VacIconContainer"))
			{
				if(parentWidget()->isHidden())
				{
					parentWidget()->show();
				}
			}
		}
	}
	update();
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconMP_ACCESS::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	if(pEqp && connected)
	{
		if(pEqp->getMode(mode))
		{
			painter.fillRect(rect(), Qt::red);

			// Two human images
			painter.fillRect(4, 1, 3, 3, Qt::black);
			painter.drawLine(5, 3, 5, 9);
			painter.drawLine(5, 9, 1, 18);
			painter.drawLine(5, 9, 9, 18);
			painter.drawLine(1, 5, 9, 5);

			painter.fillRect(14, 1, 3, 3, Qt::black);
			painter.drawLine(15, 3, 15, 9);
			painter.drawLine(15, 9, 11, 18);
			painter.drawLine(15, 9, 19, 18);
			painter.drawLine(11, 5, 19, 5);
		} // Else just keep invisible
		else
		{
			if(parentWidget()->inherits("VacIconContainer"))
			{
				if(!parentWidget()->isHidden())
				{
					parentWidget()->hide();
				}
			}
			// painter.eraseRect(rect());
		}
	}
	else
	{
		painter.fillRect(rect(), Qt::blue);
	}
}

