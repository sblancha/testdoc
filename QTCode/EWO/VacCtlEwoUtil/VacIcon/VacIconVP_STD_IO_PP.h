#ifndef	VACICONVP_STD_IO_PP_H
#define	VACICONVP_STD_IO_PP_H

//	Icon for VP_STDIO - PP

#include "VacIconVP_STD_IO.h"

class VACCTLEWOUTIL_EXPORT VacIconVP_STD_IO_PP : public VacIconVP_STD_IO
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVP_STD_IO_PP(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVP_STD_IO_PP();

protected:
	// Implement VacIconVP_STD_IO abstract method
	virtual void drawType(QPainter &painter);
};

#endif	// VACICONVP_STD_IO_PP_H
