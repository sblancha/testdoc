V 12
1
LANG:1 18 lhcValveDetailsPnl
PANEL,-1 -1 323 326 N "_3DFace" 2
"$dpName"
"$mode"
"main()
{
  string 	  devName, replayDpName;
  dyn_string  exceptionInfo;
  bool		  hasArchive;
	
  VacDpData.dpName = dpSubStr($dpName, DPSUB_DP);
  LhcVacDisplayName(VacDpData.dpName, devName, exceptionInfo);

  setValue(\"devNameText\", \"text\", devName); 
  string myModName = myModuleName();
  setWindowTitle(myModName, devName);
	
  switch($mode)
  {
  case DIALOG_MODE_ONLINE:
    VacDpData.mode = \"Online\";
    break;
  case DIALOG_MODE_REPLAY:
    VacDpData.mode = \"Replay\";
    devNameText.backCol = REPLAY_MODE_BACKGROUND;
    LhcVacGetReplayControlDpName(replayDpName);
    dpConnect(\"MainWindowChangedCb\", replayDpName + \".MainWindowState\");
    break;
  case DIALOG_MODE_POLLING:
    break;
  }
  VacDpData.connectDpe(\"EqpStatus\");
  VacDpData.connectDpe(\"RR1\");
  VacDpData.connectDpe(\"RunTime\");
  VacDpData.connectDpe(\"StartCounter\");
  VacDpData.connectDpe(\"SetPointR\");
 
  // Decide if pressure history button shall be enabled or not
  LhcVacHasDeviceValueArchive(VacDpData.dpName, hasArchive, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);  
    return;
  }   

  // Decide if State History button shall be enabled ot not
  LhcVacHasDeviceStateArchive(VacDpData.dpName, hasArchive, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);  
    return;
  }   
  stateHistoryPb.enabled = hasArchive;
  ShowNewEqpStatus(LhcVacEqpCtlStatus(VacDpData.dpName, $mode));
  unsigned rr1 = VacDpData.getDpeValue(\"RR1\");
  ShowNewStateRR1(rr1);
  float runTime = VacDpData.getDpeValue(\"RunTime\");
  ShowNewRunTime(runTime);
  unsigned value = VacDpData.getDpeValue(\"StartCounter\");
  ShowNewStartCounter(value);
  value = VacDpData.getDpeValue(\"SetPointR\");
  ShowNewSetPointR(value);
  
}
 " 0
 E E E E 1 -1 -1 0  0 0
""0  1
E "#uses \"vclDevCommon.ctl\"

const unsigned 
 VV_RR_BIT_LOCKED = 0x80000000u,
	VV_RR_BIT_VALID = 0x40000000u,
	VV_RR_BIT_HW_INT = 0x20000000u,
	VV_RR_BIT_LOCAL = 0x10000000u,
	VV_RR_BIT_MANUAL = 0x04000000u,
	VV_RR_BIT_OPEN_CMD = 0x02000000u,
	VV_RR_BIT_CLOSED_CMD = 0x01000000u,
	VV_RR_BIT_ERRORS = 0x00800000u,
	VV_RR_BIT_WARNINGS = 0x00400000u,
	VV_RR_BIT_INTLCK = 0x00200000u,
	VV_RR_BIT_OPEN_INT = 0x00100000u,
	VV_RR_BIT_AUTO = 0x00080000u,
	VV_RR_BIT_ACTUATED = 0x00040000u,
	VV_RR_BIT_OPEN_AO = 0x00020000u,
	VV_RR_BIT_FORCED = 0x00010000u;


// set point value
int       setPointValue;

// Flag indicating if user is dragging the slider
bool    slideDrag = false;

// Callback from replay control windows state
void MainWindowChangedCb(string dp, bool open)
{
  if(!open) PanelOff();
}

void ShowNewEqpStatus(unsigned status)
{
  switch(status)
  {
  case EQP_CTL_STATUS_NOT_CONTROL:
  case EQP_CTL_STATUS_NOT_CONNECTED:
    LayerOff(8);
    break;
  default:
    LayerOn(8);
    ShowNewSetPointR(VacDpData.getDpeValue(\"SetPointR\")); 
    ShowNewRunTime(VacDpData.getDpeValue(\"RunTime\")); 
    ShowNewStartCounter(VacDpData.getDpeValue(\"StartCounter\"));
    break;
  }
  ShowNewStateRR1(VacDpData.getDpeValue(\"RR1\")); 
}

// Display state information in panel
void ShowNewStateRR1(unsigned rr1)
{
  string     text, colorString, dpType;
  int	     errCode, warnCode;
  dyn_string exceptionInfo;

  LhcVacSetMainState(\"statusText\", VacDpData.dpName, VacDpData.modeAsInt());
 
  // Error code
  errCode = (rr1 & 0xFF);
  if(errCode != 0)
  {
    LhcVacErrMsgForCode(VacDpData.dpName, dpType, errCode, text, exceptionInfo);
    colorString = TEXT_COLOR_ERR;
  }
  else
  {
    colorString = TEXT_COLOR_NO_ERR;
    text = \"\";
  }
  setValue(\"errorText\", \"text\", text);
  setValue(\"errorText\", \"backCol\", colorString);

  // Warning code
  warnCode = (rr1 & 0xFF00) >> 8;
  if(warnCode != 0)
  {
    LhcVacWarningForCode(VacDpData.dpName, dpType, warnCode, text, exceptionInfo);
    colorString = TEXT_COLOR_ERR;
  }
  else
  {
    colorString = TEXT_COLOR_NO_ERR;
    text = \"\";
  }
  setValue(\"warningText\", \"text\", text);
  setValue(\"warningText\", \"backCol\", colorString);

	
 
  if((rr1 & VV_RR_BIT_VALID) == 0)
  {
    setValue(\"localText\", \"visible\", false);
    setValue(\"lockedText\", \"visible\", false);
    setValue(\"autoText\", \"visible\", false);
    setValue(\"interlockText\", \"visible\", false);
    hwInterlockText.visible = false;
    actuateText.visible = false;
    manualText.visible = false;
    openIntText.visible = false;
    return;
  }
  	// State bit indicators
  lockedText.visible = (rr1 & VV_RR_BIT_LOCKED) != 0;
  localText.visible = (rr1 & VV_RR_BIT_LOCAL) != 0;
  autoText.visible= (rr1 & VV_RR_BIT_AUTO) != 0;
  interlockText.visible = (rr1 & VV_RR_BIT_INTLCK) != 0;
  hwInterlockText.visible = (rr1 & VV_RR_BIT_HW_INT) != 0;
  actuateText.visible = (rr1 & VV_RR_BIT_ACTUATED) != 0;
  manualText.visible = (rr1 & VV_RR_BIT_MANUAL) != 0;
  openIntText.visible = (rr1 & VV_RR_BIT_OPEN_INT) != 0;
  openAoText.visible = (rr1 & VV_RR_BIT_OPEN_AO) != 0;
  forcedText.visible = (rr1 & VV_RR_BIT_FORCED) != 0;
}

void ShowNewSetPointR(unsigned value)
{
    SetPointRText.text = value;
    LhcVacSetMainState(\"statusText\", VacDpData.dpName, VacDpData.modeAsInt());

}
void ShowNewRunTime(float value)
{
  string runTime;
	
  sprintf(runTime, \"%6.1f\", value);
  setValue(\"runTimeText\", \"text\", runTime);
}
void ShowNewStartCounter(unsigned value)
{
  startCountText.text = value;
}

" 0
 2
"CBRef" "1"
"EClose" E
""
DISPLAY_LAYER, 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0
LAYER, 0 
1
LANG:1 6 Layer1
30 117
"refFrame"
""
1 1 158 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
117 0 0 0 0 0
E E E
1
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E 1 0 1.41935483870968 1 26.741935483871 0 E 1 158 315 190
1
LANG:1 32 Courier New,-1,12,5,69,0,0,0,0,0
0 1
LANG:1 7 Warning
30 127
"Frame1"
""
1 0 110 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
124 0 0 0 0 0
E E E
1
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E 1 0 1.25641025641026 1 25.0512820512818 0 E 0 70 315 110
1
LANG:1 26 Arial,-1,13,5,69,0,0,0,0,0
0 1
LANG:1 8 Setpoint
29 130
"VacDpData"
""
1 0 48 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
128 0 0 0 0 0
E E E
0
1
LANG:1 0 

2
"backgroundOrigin" "enum 0"
"mode" "enum 0"
1
LANG:1 37 MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0
0  0 48 5 53
16 VacDpDataEwo.ewo
1
0 "NewDpeValue" "(string dpe, int source, anytype value)"
"  if(source == SOURCE_PLC)
  {
    ShowNewStateRR1(VacDpData.getDpeValue(\"RR1\"));
  }
  else
  {
    if(dpe == \"RR1\")
    {
       ShowNewStateRR1(value);
    }
    else if(dpe == \"RunTime\")
    {
       ShowNewRunTime(value);
    }
    else if(dpe == \"StartCounter\")
    {
       ShowNewStartCounter(value);
    }
    else if(dpe == \"SetPointR\")
    {
       ShowNewSetPointR(value);
    }
    else if(dpe == \"EqpStatus\")
    {
      ShowNewEqpStatus(value);
    }
  }"
E30 139
"RunTimeFrame"
""
1 1 153 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
145 0 0 0 0 0
E E E
1
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E 0.492012779552715 0 1 1.50798722044729 51 0 E 1 112 315 157
1
LANG:1 32 Courier New,-1,12,5,69,0,0,0,0,0
0 1
LANG:1 8 Run Time
30 141
"StartCountFrame"
""
1 1 153 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
149 0 0 0 0 0
E E E
1
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E 0.492012779552715 0 1 161.507987220447 51 0 E 1 112 315 157
1
LANG:1 32 Courier New,-1,12,5,69,0,0,0,0,0
0 1
LANG:1 13 Start Counter
2 145
"statusText"
""
1 33.04843370495944 -61.32300353036055 E E E 1 E 1 E N {0,0,0} E N {255,255,255} E E
 "main()
{
  int           answer;
  dyn_string	exceptionInfo;
  unsigned      act;

  if((VacDpData.modeAsInt() == DIALOG_MODE_REPLAY) || (VacDpData.modeAsInt() == DIALOG_MODE_POLLING))
  { 
     return;
  }
  // In some states actions are not allowed
  unsigned ctlStatus = LhcVacEqpCtlStatus(VacDpData.dpName, VacDpData.modeAsInt());
  string actAllowedString = \"1\";
  switch(ctlStatus)
  {
  case EQP_CTL_STATUS_NOT_CONTROL:
  case EQP_CTL_STATUS_NOT_CONNECTED:
    actAllowedString = \"0\";
    break;
  }
  dyn_string popupString = makeDynString(
    \"PUSH_BUTTON, Close, 1,\" + actAllowedString,
    \"PUSH_BUTTON, Auto, 2,\" + actAllowedString,   
    \"PUSH_BUTTON, Manual, 3,\" + actAllowedString,   
    \"PUSH_BUTTON, Reset, 4,\" + actAllowedString,   
    \"PUSH_BUTTON, Force, 5,\" + actAllowedString   
     );
  string extraItem = LhcVacMenuItemForEqpCtlStatus(VacDpData.dpName, exceptionInfo);
  if(extraItem != \"\")
  {
    dynAppend(popupString, \"SEPARATOR\");
    dynAppend(popupString, extraItem);
  }

  popupMenu(popupString, answer);
  setPointValue = SetPointSpin.value;
  switch(answer)
  {
  case 1:
    act = ( setPointValue < 50 ) ? LVA_DEVICE_CLOSE : LVA_DEVICE_PART_CLOSE ;
    break;
  case 2:
    act = LVA_DEVICE_AUTO;
    break;
  case 3:
    act = LVA_DEVICE_MANUAL;
    break;
  case 4:
    act = LVA_DEVICE_RESET;
    break;
  case 5: 
    act = LVA_DEVICE_FORCE;
    break;
  case MENU_ID_NOT_CONNECTED:
    act = LVA_DEVICE_SET_NOT_CONNECTED;
    break;
  case MENU_ID_CONNECTED:
    act = LVA_DEVICE_SET_CONNECTED;
    break;
  }
  if(act)
  {
    LhcVacDeviceAction(makeDynAnytype(act,setPointValue), makeDynString(), makeDynString(), VacDpData.dpName, 0, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      fwExceptionHandling_display(exceptionInfo);  
      return;
    } 
  }
}" 0
 E
156 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  1 E 5 45 122 65
1 0 0 "0s" 0 0 0 65 0 0  5 45 1
1
LANG:1 26 Arial,11,-1,5,75,0,0,0,0,0
0 1
LANG:1 0 
0
LAYER, 1 
1
LANG:1 6 Layer2
2 0
"devNameText"
""
1 8 -1 E E E 1 E 1 E N {0,0,0} E N "_3DFace" E E
 E E
1 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  1 E 5 -1 305 27
0 0 0 "0s" 0 0 0 65 0 0  5 -1 1
1
LANG:1 32 Courier New,-1,16,5,69,0,0,0,0,0
0 1
LANG:1 0 
30 21
"Frame3"
""
1 0 62 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
22 0 0 0 0 0
E E E
1
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E 1 0 2.40540540540541 0 -46.1621621621623 0 E 0 30 315 68
1
LANG:1 26 Arial,10,-1,5,75,0,0,0,0,0
0 1
LANG:1 5 State
30 20
"Frame2"
""
1 1 153 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
21 0 0 0 0 0
E E E
1
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E 1 0 1 1 93 0 E 1 112 315 157
1
LANG:1 32 Courier New,-1,12,5,69,0,0,0,0,0
0 1
LANG:1 5 Error
13 17
"closePb"
""
1 191 299 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
18 0 0 0 0 0
E E E
0
1
LANG:1 11 Close panel

0
1
LANG:1 32 Courier New,-1,15,5,69,0,0,0,0,0
0  189 297 248 323

T 
1
LANG:1 5 Close
"main()
{
  PanelOff();
}" 0
 E E E
13 18
"helpPb"
""
1 254 299 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
19 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 32 Courier New,-1,15,5,69,0,0,0,0,0
0  252 297 315 323

T 
1
LANG:1 4 Help
"main()
{
	ShowHelpTopic(HELP_PANEL_VLV);
}" 0
 E E E
13 121
"stateHistoryPb"
""
1 4 299 E E E 0 E 1 E N "_ButtonText" E N "_Button" E E
 E E
120 0 0 0 0 0
E E E
0
1
LANG:1 18 Open state history

0
1
LANG:1 32 Courier New,-1,13,5,69,0,0,0,0,0
0  2 297 121 323

T 
1
LANG:1 13 State history
"main()
{
  dyn_string		exceptionInfo;

  LhcVacAddDpToStateHistory( makeDynString(VacDpData.dpName), true, exceptionInfo );
  if( dynlen( exceptionInfo ) > 0 )
  {
    fwExceptionHandling_display( exceptionInfo );
  }
}" 0
 E E E
13 128
"attrPb"
""
1 130 299 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
125 0 0 0 0 0
E E E
0
1
LANG:1 22 Show device attributes

0
1
LANG:1 32 Courier New,-1,15,5,69,0,0,0,0,0
0  128 297 180 323

T 
1
LANG:1 4 Attr
"main()
{
  LhcVacShowEqpAttributes(VacDpData.dpName);
}" 0
 E E E
0
LAYER, 2 
1
LANG:1 6 Layer3
0
LAYER, 3 
1
LANG:1 6 Layer4
0
LAYER, 4 
1
LANG:1 6 Layer5
0
LAYER, 5 
1
LANG:1 6 Layer6
0
LAYER, 6 
1
LANG:1 6 Layer7
0
LAYER, 7 
1
LANG:1 6 Layer8
2 143
"SetPointRText"
""
1 137 48 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
152 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  1 E 137 48 180 63
0 2 2 "0s" 0 0 0 64 0 0  137 48 1
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 1
LANG:1 0 
2 148
"PRIMITIVE_TEXT3"
""
1 194 48 E E E 1 E 1 E N "_WindowText" E N "_3DFace" E E
 E E
161 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  1 E 194 48 223 63
0 2 2 "0s" 0 0 0 64 0 0  194 48 1
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 1
LANG:1 4 0/00
2 136
"manualText"
""
1 7 68 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
139 0 0 0 0 0
E E E
0
1
LANG:1 9 Interlock

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 7 71 60 87
1 0 0 "0s" 0 0 0 65 0 0  7 71 1
1
LANG:1 26 Arial,11,-1,5,75,0,0,0,0,0
0 1
LANG:1 6 Manual
2 5
"localText"
""
1 65 71 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
6 0 0 0 0 0
E E E
0
1
LANG:1 30 Valve is in LOCAL control mode

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 65 71 119 87
1 0 0 "0s" 0 0 0 65 0 0  65 71 1
1
LANG:1 26 Arial,11,-1,5,75,0,0,0,0,0
0 1
LANG:1 5 Local
2 6
"lockedText"
""
1 123 71 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
7 0 0 0 0 0
E E E
0
1
LANG:1 15 Valve is LOCKED

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 123 71 170 87
1 0 0 "0s" 0 0 0 65 0 0  123 71 1
1
LANG:1 26 Arial,11,-1,5,75,0,0,0,0,0
0 1
LANG:1 4 Lock
2 122
"autoText"
""
1 174 71 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
120 0 0 0 0 0
E E E
0
1
LANG:1 21 Valve is in AUTO mode

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 174 71 216 87
1 0 0 "0s" 0 0 0 65 0 0  174 71 1
1
LANG:1 26 Arial,11,-1,5,75,0,0,0,0,0
0 1
LANG:1 4 Auto
2 135
"actuateText"
""
1 220 71 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
137 0 0 0 0 0
E E E
0
1
LANG:1 21 Valve is in AUTO mode

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 220 71 309 87
1 0 0 "0s" 0 0 0 65 0 0  220 71 1
1
LANG:1 26 Arial,11,-1,5,75,0,0,0,0,0
0 1
LANG:1 12 Value Actuat
2 137
"openAoText"
""
1 243 92 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
141 0 0 0 0 0
E E E
0
1
LANG:1 21 Valve is in AUTO mode

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 243 92 309 108
1 0 0 "0s" 0 0 0 65 0 0  243 92 1
1
LANG:1 26 Arial,11,-1,5,75,0,0,0,0,0
0 1
LANG:1 7 Open AO
2 138
"forcedText"
""
1 187 92 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
143 0 0 0 0 0
E E E
0
1
LANG:1 9 Interlock

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 187 92 239 108
1 0 0 "0s" 0 0 0 65 0 0  187 92 1
1
LANG:1 26 Arial,11,-1,5,75,0,0,0,0,0
0 1
LANG:1 6 Forced
2 134
"openIntText"
""
1 117 92 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
135 0 0 0 0 0
E E E
0
1
LANG:1 15 Valve is LOCKED

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 117 92 182 108
1 0 0 "0s" 0 0 0 65 0 0  117 92 1
1
LANG:1 26 Arial,11,-1,5,75,0,0,0,0,0
0 1
LANG:1 8 Open Int
2 123
"interlockText"
""
1 62 92 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
121 0 0 0 0 0
E E E
0
1
LANG:1 9 Interlock

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 62 92 112 108
1 0 0 "0s" 0 0 0 65 0 0  62 92 1
1
LANG:1 26 Arial,11,-1,5,75,0,0,0,0,0
0 1
LANG:1 7 Intlock
2 133
"hwInterlockText"
""
1 8 92 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
133 0 0 0 0 0
E E E
0
1
LANG:1 12 HW Interlock

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 8 92 57 108
1 0 0 "0s" 0 0 0 65 0 0  8 92 1
1
LANG:1 26 Arial,11,-1,5,75,0,0,0,0,0
0 1
LANG:1 6 HW Int
13 144
"ActuatePb"
""
1 145 131 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
154 0 0 0 0 0
E E E
0
1
LANG:1 17 Actuate new value

0
1
LANG:1 32 Courier New,-1,15,5,69,0,0,0,0,0
0  143 129 222 155

T 
1
LANG:1 7 Actuate
"main()
{
    dyn_string exceptionInfo;
    setPointValue = SetPointSpin.value;
    unsigned  act =  LVA_DEVICE_PART_OPEN;
    LhcVacDeviceAction(makeDynAnytype(act,setPointValue), makeDynString(), makeDynString(), VacDpData.dpName, 0, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      fwExceptionHandling_display(exceptionInfo);  
      return;
    } 

}" 0
 E E E
2 147
"PRIMITIVE_TEXT2"
""
1 99 134 E E E 1 E 1 E N "_WindowText" E N "_3DFace" E E
 E E
159 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  1 E 99 134 130 149
0 2 2 "0s" 0 0 0 64 0 0  99 134 1
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 1
LANG:1 4 0/00
29 131
"SetPointSpin"
""
1 10 129 E E E 1 E 1 E N "_3DText" E N "_Window" E E
 E E
129 0 0 0 0 0
E "main()
{
      setPointValue = SetPointSpin.value;

}" 0
 E
0
1
LANG:1 35 Set  valve open fraction in promile

2
"maxValue" "int 1000"
"maximum" "int 999"
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  10 129 95 154
14 SpinBoxEwo.ewo
0
E14 142
"startCountText"
""
1 167 177 E E E 1 E 1 E N "_WindowText" E N {212,208,200} E E
 E E
151 0 0 0 0 0
E E E
0
1
LANG:1 19 Start counter value

0
1
LANG:1 32 Courier New,-1,13,5,40,0,0,0,0,0
0  165 175 312 202
2 "0s" 0 0 0 0 0 -1  E E E
14 140
"runTimeText"
""
1 7 177 E E E 1 E 1 E N "_WindowText" E N {212,208,200} E E
 E E
147 0 0 0 0 0
E E E
0
1
LANG:1 14 Run time value

0
1
LANG:1 32 Courier New,-1,13,5,40,0,0,0,0,0
0  5 175 152 202
2 "0s" 0 0 0 0 0 -1  E E E
14 22
"errorText"
""
1 8 218 E E E 1 E 1 E N "_WindowText" E N {212,208,200} E E
 E E
23 0 0 0 0 0
E E E
0
1
LANG:1 10 Error text

0
1
LANG:1 32 Courier New,-1,13,5,40,0,0,0,0,0
0  6 216 311 243
2 "0s" 0 0 0 0 0 -1  E E E
14 132
"warningText"
""
1 11 265 E E E 1 E 1 E N "_WindowText" E N {212,208,200} E E
 E E
131 0 0 0 0 0
E E E
0
1
LANG:1 12 Warning text

0
1
LANG:1 32 Courier New,-1,13,5,40,0,0,0,0,0
0  9 263 311 290
2 "0s" 0 0 0 0 0 -1  E E E
0
0
