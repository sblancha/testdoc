//	Implementation of SynLhcEqpItem class
/////////////////////////////////////////////////////////////////////////////////

#include "SynLhcEqpItem.h"

#include "VacIcon.h"
#include "EqpType.h"
#include "Eqp.h"
#include "Sector.h"
#include "VacType.h"
#include "FunctionalType.h"
#include "EqpMsgCriteria.h"
#include "DebugCtl.h"

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

SynLhcEqpItem::SynLhcEqpItem(Eqp *pEqp)
{
	this->pEqp = pEqp;
	start = pEqp->getStart();
	type = pEqp->getType();
	sectorBorder = pEqp->isSectorBorder();
	pIcon = NULL;
	groupId = 0;
	iconY = verticalPos = 0;
	commonReady = startReady = endReady = outerEqp = false;
	intlSourceForward = intlSourceBackward = false;

	startOrder = endOrder = corrDelta = 0;
}

SynLhcEqpItem::~SynLhcEqpItem()
{
	if(pIcon)
	{
		delete pIcon;
		pIcon = NULL;
	}
}

/*
**	FUNCTION
**		Delete icon - it is not required in this line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcEqpItem::deleteIcon(void)
{
	if(pIcon)
	{
		delete pIcon;
		pIcon = NULL;
	}
}

/*
**	FUNCTION
**		Set icon reference to NULL - reference has been taken to another line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcEqpItem::nullIcon(void)
{
	pIcon = NULL;
}

/*
**	FUNCTION
**		Initialize icon and prepare geometry
**
**	PARAMETERS
**		parent			- Parent widget for icon
**		forBeamVacuum	- True if icon will be shown for beam vacuum
**		eqpMask			- Equipment type mask for all items in synoptic line
**		eqpAboveLine	- True if equipment is drawn above base line
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcEqpItem::initIcon(QWidget *parent, bool forBeamVacuum, VacEqpTypeMask &eqpMask, bool eqpAboveLine)
{
	// Mark outer devices
	if(pEqp->getSectorBefore() && (!pEqp->getSectorAfter()) && (pEqp->getVacType() & VacType::CommonBeam))
	{
		Sector *pSector = pEqp->getSectorBefore();
		if((pSector->getVacType() == VacType::CommonBeam) &&
			pSector->isOnBlueBeam() && pSector->isOnRedBeam() && (!pSector->isOnCrossBeam()))
		{
			outerEqp = true;
		}
	}
	if(!(pIcon = VacIcon::getIcon(pEqp, parent)))
	{
		return;
	}
	VacIconGeometry geometry = pIcon->getGeometry(eqpMask, forBeamVacuum);
	verticalPos = geometry.getVerticalPos();

	// Icons for solenoids always appear opposite (in vertical direction)
	// to other equipment on this line. L.Kopylov 26.04.2011
	bool thisEqpAboveLine = pIcon->isAlwaysOpposite() ? ! eqpAboveLine : eqpAboveLine;
	if(thisEqpAboveLine)
	{
		if(verticalPos < 0)
		{
			pIcon->setDirection(VacIconContainer::Down);
		}
		else
		{
			pIcon->setDirection(VacIconContainer::Up);
		}
	}
	else
	{
		if(verticalPos > 0)
		{
			pIcon->setDirection(VacIconContainer::Down);
		}
		else
		{
			pIcon->setDirection(VacIconContainer::Up);
		}
		
	}
	// After setting icon direction we can take other geometry parameters
	geometry = pIcon->getGeometry(eqpMask, forBeamVacuum);
	rect.moveLeft(geometry.getX());
	rect.setWidth(geometry.getWidth());
	rect.setHeight(geometry.getHeight());
	connPoint.setY(0);
	if(thisEqpAboveLine)
	{
		iconY = -geometry.getY();
	}
	else
	{
		iconY = geometry.getY() - geometry.getHeight() + 1;
	}
	rect.moveTop(iconY);

	// Very special geometry for valves on isolation vacuum
	if(!forBeamVacuum)
	{
		if(pEqp->getFunctionalType() == FunctionalType::VV)
		{
			int delta = thisEqpAboveLine ? ((rect.height() >> 1) + 3) : -((rect.height() >> 1) + 3);
			// It can be not only valve, but also VV+PP. L.Kopylov 27.03.2011
			const QString attr = pEqp->getAttrValue("SpecialHandling");
			if(attr == "1")
			{
				delta = 0;
			}
			if(delta)
			{
				moveVert(delta);
			}
			// iconY += delta;
		}
	}
}

/*
**	FUNCTION
**		Set pipe color to be used by active icon
**
**	PARAMETERS
**		color	- Color to use when drawing icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcEqpItem::setPipeColor(QColor &color)
{
	if(pIcon)
	{
		pIcon->setPipeColor(color);
	}
}

/*
**	FUNCTION
**		Move item as a whole in horizontal direction
**
**	PARAMETERS
**		delta	- Distance to move [Pixels]
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcEqpItem::moveHor(int delta)
{
	rect.moveLeft(rect.left() + delta);
	connPoint.setX(connPoint.x() + delta);
}

/*
**	FUNCTION
**		Move item as a whole in vertical direction
**
**	PARAMETERS
**		delta	- Distance to move [Pixels]
**
**	RETURNS
***		None
**
**	CAUTIONS
**		None
*/
void SynLhcEqpItem::moveVert(int delta)
{
	rect.moveTop(rect.top() + delta);
	connPoint.setY(connPoint.y() + delta);
}

/*
**	FUNCTION
**		Move active icon to calculated position
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcEqpItem::moveIcon(void)
{
	if(pIcon)
	{
		pIcon->move(rect.x(), rect.y());
		pIcon->resize(rect.width(), rect.height());
		pIcon->show();
	}
}

/*
**	FUNCTION
**		Check if this item contains CRYO thermometers - such items are
**		processed in special way in many cases
**
**	PARAMETERS
**		None
**
**	RETURNS
**		true	- If this item contains CRYO thermometer;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SynLhcEqpItem::isCryoThermometer(void)
{
	if(!pEqp)
	{
		return false;
	}
	return pEqp->isCryoThermometer();
}

/*
**	FUNCTION
**		Calculate name of this item
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Pointer to string with resulting name
**
**	CAUTIONS
**		None
*/
const char *SynLhcEqpItem::getName(void) const
{
	static const char	*nullName = "<null>";
	const char	*result = nullName;

	switch(type)
	{
	case EqpType::SectorBefore:
		if(pEqp)
		{
			result = pEqp->getName();
		}
		break;
	case EqpType::SectorAfter:
		if(pEqp)
		{
			result = pEqp->getName();
		}
		break;
	case EqpType::Aux:
		result = nullName;
		break;
	default:
		if(pEqp)
		{
			result = pEqp->getName();
		}
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Find name of sector to be drawn BEFORE this element
**
**	PARAMETERS
**		reverse	- true if equipment ine line is built in reverse order
**
**	RETURNS
**		Pointer to string with resulting name
**
**	CAUTIONS
**		It is supposed that method will only be called for elements with
**		sectorBorder = true
*/
const char *SynLhcEqpItem::getNameBefore(bool reverse) const
{
	/* L.Kopylov 03.03.2014 - see below
	if(reverse)
	{
		return pEqp->getSectorAfter()->getName();
	}
	return pEqp->getSectorBefore()->getName();
	*/
	if(reverse)
	{
		return pEqp->getLabelAfter();
	}
	return pEqp->getLabelBefore();
}

/*
**	FUNCTION
**		Find name of sector to be drawn AFTER this element
**
**	PARAMETERS
**		reverse	- true if equipment ine line is built in reverse order
**
**	RETURNS
**		Pointer to string with resulting name
**
**	CAUTIONS
**		It is supposed that method will only be called for elements with
**		sectorBorder = true
*/
const char *SynLhcEqpItem::getNameAfter(bool reverse) const
{
	/* L.Kopylov 03.03.2014 - see below
	if(reverse)
	{
		return pEqp->getSectorBefore()->getName();
	}
	return pEqp->getSectorAfter()->getName();
	*/
	if(reverse)
	{
		return pEqp->getLabelBefore();
	}
	return pEqp->getLabelAfter();
}

void SynLhcEqpItem::setRect(QRect &rect)
{
	this->rect = rect;
	if(pIcon)
	{
		pIcon->resize(rect.width(), rect.height());
	}
}

int SynLhcEqpItem::getIconHeight(void)
{
	return pIcon ? pIcon->height() : 0;
}

/*
**	FUNCTION
**		Write all parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**		idx		- Index of this item in the list
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcEqpItem::dump(FILE *pFile, int idx)
{
	if(!DebugCtl::isSynoptic())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	const char	*name = NULL;
	float	eqpStart = 0;
	switch(type)
	{
	case EqpType::VacDevice:
	case EqpType::OtherLineStart:
	case EqpType::OtherLineEnd:
		name = pEqp->getName();
		eqpStart = pEqp->getStart();
		break;
	case EqpType::SectorBefore:
	case EqpType::SectorAfter:
		name = getName();
		break;
	}
	fprintf(pFile, "  %04d: [%d] <%s> type %d (icon %lX) %.3f (%.3f) -> (%d %d) (%d x %d) conn %d %d %s %s\n",
		idx, startOrder, name, type, (unsigned long)pIcon, eqpStart, start,
		rect.x(), rect.y(), rect.width(), rect.height(),
		connPoint.x(), connPoint.y(), (sectorBorder ? "BORDER" : ""),
		(outerEqp ? "OUTER" : ""));
	fprintf(pFile, "      correct: %d\n", corrDelta);
	fprintf(pFile, "      groupId %d verticalPos %d border %d\n",
		groupId, verticalPos, sectorBorder);
}
