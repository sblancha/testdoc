// Bitmasks for VPG status bits (RR1 - 32 bits)
const unsigned VPG_R_VALID = 0x40000000u,
  VPG_R_REMOTE = 0x20000000u,
  VPG_R_ERRORS = 0x00800000u,
  VPG_R_WARNINGS = 0x00400000u,
  VPG_R_OFF = 0x00010000u,
  VPG_R_ON = 0x00020000u,
  VPG_R_INTLCK = 0x00200000u,
  VPG_R_AUTO = 0x08000000u,

// Bitmasks for VPG_EA status bits (RR1 - 32 bits)
  VPG_EA_SPS_R_VVR_OPEN = 0x00080000u,
  VPG_EA_SPS_R_VVR_CLOSE = 0x00040000u,

// Bitmasks for VPG_BP status bits (RR1 - 32 bits)
  VPG_R_PP_FORCED_OFF = 0x01000000u,
  VPG_R_SAFETY = 0x00100000u,
  VPG_R_VENT = 0x00080000u,
  VPG_R_PP_PRESS_OK = 0x00040000u,

// Bitmasks for VPG_BP and VPG_SA status bits (RR2 - 32 bits)
  VPG_R_VVD_OPEN = 0x20000000u,
  VPG_R_VVD_CLOSED = 0x10000000u,
  VPG_R_VVI_OPEN = 0x08000000u,
  VPG_R_VVI_CLOSED = 0x04000000u,
  VPG_R_VVP_OPEN = 0x02000000u,
  VPG_R_VVP_CLOSED = 0x01000000u,
  VPG_R_TMP_SPEED_OK = 0x00400000u,
  VPG_R_TMP_STANDBY = 0x00200000u,
  VPG_R_TMP_OK = 0x00100000u,
  VPG_R_TMP_ON = 0x00080000u,
  VPG_R_TMP_START_ENABLE = 0x00040000u,
  VPG_R_PP_OK = 0x00020000u,
  VPG_R_PP_ON = 0x00010000u,
	
// Specific bitmasks for VPG_BP (RR2 - 32 bits)

  VPG_BP_R_VVR2_CLOSED = 0x00000001u,
  VPG_BP_R_VVR2_OPEN = 0x00000002u,
  VPG_BP_R_VVR2_OPEN_ENABLE = 0x00000004u,
  VPG_BP_R_VVR2_INTERLOCK = 0x00000020u,
  VPG_BP_R_VVR2_WARNINGS = 0x00000040u,
  VPG_BP_R_VVR2_ERRORS = 0x00000080u,
  VPG_BP_R_VVR1_CLOSED = 0x00000100u,
  VPG_BP_R_VVR1_OPEN = 0x00000200u,
  VPG_BP_R_VVR1_OPEN_ENABLE = 0x00000400u,
  VPG_BP_R_VVR1_INTERLOCK = 0x00002000u,
  VPG_BP_R_VVR1_WARNINGS = 0x00004000u,
  VPG_BP_R_VVR1_ERRORS = 0x00008000u;
	
// Bitmasks for VPG_EA command bits (WR1 - 16 bits)
const unsigned VPG_EA_SPS_W_REMOTE = 0x2000,
  VPG_EA_SPS_W_LOCAL = 0x1000u,
  VPG_EA_SPS_W_VVR_OPEN = 0x0002u,
  VPG_EA_SPS_W_VVR_CLOSE = 0x0001u,
  VPG_EA_SPS_W_ON = 0x0200u,
  VPG_EA_SPS_W_OFF = 0x0100u,
// Bitmasks for VPG_BP and VPG_SA and VP_GU command bits (WR1 - 32 bits)
  VPG_W_RESTART_MODE_NO_CHANGE = 0x00000000u,
  VPG_W_RESTART_MODE_AUTO = 0x00000001u,
  VPG_W_RESTART_MODE_VPG_STOPPED = 0x00000002u,
  VPG_W_RESTART_MODE_VPG_STARTED = 0x00000003u,
  VPG_W_PP_FORCED_OFF = 0x00000010u,
  VPG_W_TMP_DELAY_START_OFF = 0x00000100u,
  VPG_W_TMP_DELAY_START_ON = 0x00000200u,
  VPG_W_VVI_CLOSE = 0x00000400u,
  VPG_W_VVI_OPEN = 0x00000800u,
  VPG_W_VVD_CLOSE = 0x00001000u,
  VPG_W_VVD_OPEN = 0x00002000u,
  VPG_W_VVP_CLOSE = 0x00004000u,
  VPG_W_VVP_OPEN = 0x00008000u,
  VPG_W_VVRS_FORCED_OFF = 0x00100000u,
  VPG_W_VVRS_FORCED_ON = 0x00200000u,
  VPG_W_TMP_STANDBY_OFF = 0x00400000u,
  VPG_W_TMP_STANDBY_ON = 0x00800000u,
  VPG_W_OFF = 0x01000000u,
  VPG_W_ON = 0x02000000u,
  VPG_W_MANUAL = 0x04000000u,
  VPG_W_AUTO = 0x08000000u,
  VPG_W_LOCAL = 0x10000000u,
  VPG_W_REMOTE = 0x20000000u,
  
  VPG_W_RESET        = 0x80000000u,
  VPG_W_VVR1_FORCE   = 0x00080000u,
  VPG_W_VVR1_UNFORCE = 0x00040000u, 
  VPG_W_VVR1_OPEN    = 0x00020000u,
  VPG_W_VVR1_CLOSE   = 0x00010000u,
  VPG_W_VVR2_FORCE   = 0x00800000u,
  VPG_W_VVR2_UNFORCE = 0x00400000u, 
  VPG_W_VVR2_OPEN    = 0x00200000u,
  VPG_W_VVR2_CLOSE   = 0x00100000u,
  VPG_W_VG1_ON       = 0x00002000u,
  VPG_W_VG1_OFF      = 0x00001000u,
  VPG_W_START        = 0x00000A00u,
  VPG_W_STOPNOVENT   = 0x00000900u,
  VPG_W_PURGE        = 0x00000B00u,
  VPG_W_STOP_PP_ON   = 0x00000B00u,
  VPG_W_VENT         = 0x00000C00u,  
  VPG_W_RESET_VPP    = 0x00000080u,                     
  VPG_W_RESET_VPT    = 0x00000040u,
  VPG_W_BAKE_ON      = 0x00000020u,
  VPG_W_BAKE_OFF     = 0x00000010u,
  VPG_W_ST_VAC_TST_ON  = 0x00000001u,
  VPG_W_ST_VAC_TST_OFF = 0x00000002u,
  VPG_W_RESET_VPP_ALR  = 0x00000040u,
                  	
// VPG_BP specific
  VPG_BP_W_VVR1_CLOSE = 0x00010000u,
  VPG_BP_W_VVR1_OPEN = 0x00020000u,
  VPG_BP_W_VVR2_CLOSE = 0x00040000u,
  VPG_BP_W_VVR2_OPEN = 0x00080000u,
// VPG_SA specific
  VPG_SA_W_VVR_CLOSE = 0x00010000u,
  VPG_SA_W_VVR_OPEN = 0x00020000u,
  VPG_W_RESTART_MODE_LAST_KNOWN_STATE = 0x00000001u,
  VPG_W_RESTART_MODE_VPG_STOPPED_PP_ON = 0x00000002u,
// VPG_GIS specific
  VPG_GIS_W_VVR_CLOSE = 0x00010000u,
  VPG_GIS_W_VVR_OPEN = 0x00020000u,
  VPG_GIS_R_VVR_INTERLOCK = 0x00002000u,

// VPG MBLK

  VPG_MBLK_W_RESET       = 0x80000000u,
  VPG_MBLK_W_REMOTE      = 0x20000000u,
  VPG_MBLK_W_LOCAL       = 0x10000000u,
  VPG_MBLK_W_MANUAL      = 0x04000000u,
  VPG_MBLK_W_ON          = 0x02000000u,
  VPG_MBLK_W_OFF         = 0x01000000u,
  VPG_MBLK_W_VVR2_FORCE  = 0x00800000u,
  VPG_MBLK_W_VVR2_UNFORCE= 0x00400000u,
  VPG_MBLK_W_VVR2_OPEN   = 0x00200000u,
  VPG_MBLK_W_VVR2_CLOSE  = 0x00100000u,
  VPG_MBLK_W_VVR1_FORCE  = 0x00080000u,
  VPG_MBLK_W_VVR1_UNFORCE= 0x00040000u,
  VPG_MBLK_W_VVR1_OPEN   = 0x00020000u,
  VPG_MBLK_W_VVR1_CLOSE  = 0x00010000u,
  VPG_MBLK_W_VG2_ON      = 0x00008000u,
  VPG_MBLK_W_VG2_OFF     = 0x00004000u,
  VPG_MBLK_W_VG1_ON      = 0x00002000u,
  VPG_MBLK_W_VG1_OFF     = 0x00001000u,
  VPG_MBLK_W_SET_MODE    = 0x00000800u,
  VPG_MBLK_W_MODE_OFF    = 0x00000800u,
  VPG_MBLK_W_MODE_STOP   = 0x00000900u,
  VPG_MBLK_W_MODE_PUMP   = 0x00000A00u,
  VPG_MBLK_W_MODE_VENT_TURBO = 0x00000B00u,
  VPG_MBLK_W_MODE_VENT_ALL = 0x00000C00u,
  VPG_MBLK_W_MODE_LEAK_DET = 0x00000D00u,
  VPG_MBLK_W_MODE_LARGE_LEAK_DET = 0x00000E00u,
  VPG_MBLK_W_MODE        = 0x00000700u,
  VPG_MBLK_W_RESET_VPP_TH= 0x00000080u,
  VPG_MBLK_W_RESET_TMP_ERR= 0x00000040u;

//Service date to be set for TPG
global time gServiceDate;
global int gInitOperTime;
//LhcVacDeviceAction_VPG:
/** Execute action for VPG. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VPG(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  switch(dpType)
  {
  case "VP_GU":
    LhcVacDeviceAction_VP_GU(dpName, dpType, action, exceptionInfo);
    break;  
  case "VPG_EA":
    LhcVacDeviceAction_VPG_EA(dpName, dpType, action, exceptionInfo);
    break;
  case "VPG_BP":
    LhcVacDeviceAction_VPG_BP(dpName, dpType, action, exceptionInfo);
    break;
  case "VPG_SA":
    LhcVacDeviceAction_VPG_SA(dpName, dpType, action, exceptionInfo);
    break;
  case "VPG_GIS":
    LhcVacDeviceAction_VPG_GIS(dpName, dpType, action, exceptionInfo);
    break;
  case "VPG_BGI":
    LhcVacDeviceAction_VPG_BGI(dpName, dpType, action, exceptionInfo);
    break;
  case "VPG_MBLK":
    LhcVacDeviceAction_VPG_MBLK(dpName, dpType, action, exceptionInfo);
    break;
  }
}

//Actions for VP_GU  type
/** 
@brief Execute action for VPG. It has been checked before call if action is known.
@param[in]     dpName    DP name
@param[in]     dpType:   DP type
@param[in]     act       Enum for action to be executed.
@param[in,out] exceptionInfo   Details of any exceptions are returned here
*/
void LhcVacDeviceAction_VP_GU(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo) {
  
  unsigned		   newWr1;
  bool			   possible;
  dyn_errClass		err;
  unsigned        act;
  mapping         mConnectDpes;
        
  act = action[1] & LVA_ACTION_MASK;

  switch(act)
  {
  case LVA_DEVICE_START: 
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_ON | VPG_W_START;
    break;
  case LVA_DEVICE_VENT: 
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_VENT;
    break;
  case LVA_DEVICE_STOP: 
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_STOPNOVENT;
    break;
  case LVA_DEVICE_PURGE: //for VPGMA only 
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_PURGE;
    break;  
  case LVA_DEVICE_RESET:
    newWr1 = VPG_W_REMOTE | VPG_W_RESET;
    break;
  case LVA_DEVICE_VVR1_FORCE:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_VVR1_FORCE;
    break;
  case LVA_DEVICE_VVR1_UNFORCE:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_VVR1_UNFORCE;
    break;
  case LVA_DEVICE_VVR1_OPEN:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_VVR1_OPEN;
    break;
  case LVA_DEVICE_VVR1_CLOSE:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_VVR1_CLOSE;
    break;
  case LVA_DEVICE_VVR2_FORCE:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_VVR2_FORCE;
    break;
  case LVA_DEVICE_VVR2_UNFORCE:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_VVR2_UNFORCE;
    break;
  case LVA_DEVICE_VVR2_OPEN:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_VVR2_OPEN;
    break;
  case LVA_DEVICE_VVR2_CLOSE:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_VVR2_CLOSE;
    break;
  case LVA_DEVICE_VG1_ON:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_VG1_ON;
    break;
  case LVA_DEVICE_VG1_OFF:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_VG1_OFF;
    break;
  case LVA_DEVICE_VPP_RESET:
    newWr1 = VPG_W_REMOTE | VPG_W_RESET_VPP;
    break;
  case LVA_DEVICE_VPT_RESET:
    newWr1 = VPG_W_REMOTE | VPG_W_RESET_VPT;
    break;
  case LVA_DEVICE_BAKE_ON:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_BAKE_ON;
    break;
  case LVA_DEVICE_BAKE_OFF:
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_BAKE_OFF;
    break;
  case LVA_DEVICE_TIGHT_TEST_ON:
    newWr1 = VPG_W_ST_VAC_TST_ON;
    break;
  case LVA_DEVICE_TIGHT_TEST_OFF:
    newWr1 = VPG_W_ST_VAC_TST_OFF;
    break;
  case LVA_DEVICE_RESET_VPP_ALR:
    newWr1 = VPG_W_RESET_VPP_ALR;
    break;
  case LVA_DEVICE_STOP_PP_ON: 
    newWr1 = VPG_W_REMOTE | VPG_W_MANUAL | VPG_W_STOP_PP_ON;
    break;  
  }
  // Execute
  dpSetWait(dpName + ".WR1", newWr1);
  err = getLastError();
  if(dynlen(err) > 0) {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
  
}

//LhcVacDeviceAction_VPG:
/** Execute action for VPG. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VPG_EA(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  unsigned		    newWr1;
  dyn_errClass		err;
  unsigned        act;
  mapping         mConnectDpes;
        
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
    newWr1 = VPG_EA_SPS_W_ON | VPG_EA_SPS_W_REMOTE;
    break;
  case LVA_DEVICE_OFF:
    newWr1 = VPG_EA_SPS_W_OFF | VPG_EA_SPS_W_REMOTE;
    break;
  case LVA_DEVICE_OPEN:
    newWr1 = VPG_EA_SPS_W_VVR_OPEN | VPG_EA_SPS_W_REMOTE;
    break;
  case LVA_DEVICE_CLOSE:
    newWr1 = VPG_EA_SPS_W_VVR_CLOSE | VPG_EA_SPS_W_REMOTE;
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }

  // Execute
  dpSetWait(dpName + ".WR1", newWr1);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
}

void LhcVacDeviceActionCommon_VPG(dyn_anytype action, unsigned &newWr1,
  dyn_string &exceptionInfo)
{
  dyn_errClass		err;
  unsigned              act;

  dynClear(exceptionInfo);
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
    newWr1 = VPG_W_ON | VPG_W_REMOTE | VPG_W_MANUAL;
    break;
  case LVA_DEVICE_OFF:
    newWr1 = VPG_W_OFF | VPG_W_REMOTE | VPG_W_MANUAL;
    break;
  case LVA_DEVICE_VPG_PP_FORCED_OFF:
    newWr1 = VPG_W_OFF | VPG_W_PP_FORCED_OFF | VPG_W_REMOTE | VPG_W_MANUAL;
    break;
  case LVA_DEVICE_TMP_STANDBY_OFF:
    newWr1 = VPG_W_TMP_STANDBY_OFF | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_TMP_STANDBY_ON:
    newWr1 = VPG_W_TMP_STANDBY_ON | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_VVR_FORCED_OFF:
    newWr1 = VPG_W_VVRS_FORCED_OFF | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_VVR_FORCED_ON:
    newWr1 = VPG_W_VVRS_FORCED_ON | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_RESTART_MODE_NO_CHANGE:
    newWr1 = VPG_W_RESTART_MODE_NO_CHANGE | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_RESTART_MODE_AUTO:
    newWr1 = VPG_W_RESTART_MODE_AUTO | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_RESTART_MODE_MANUAL_VPG_STOP:
    newWr1 = VPG_W_RESTART_MODE_VPG_STOPPED | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_RESTART_MODE_MANUAL_VPG_START:
    newWr1 = VPG_W_RESTART_MODE_VPG_STARTED | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_AUTO:
    newWr1 = VPG_W_AUTO | VPG_W_REMOTE;
    break;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }
}
void LhcVacDeviceAction_VPG_BP(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  unsigned		    newWr1;
  dyn_errClass		err;
  unsigned        act;
  mapping         mConnectDpes;
       
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_VVR1_OPEN:
    newWr1 = VPG_BP_W_VVR1_OPEN | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_VVR1_CLOSE:
    newWr1 = VPG_BP_W_VVR1_CLOSE | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_VVR2_OPEN:
    newWr1 = VPG_BP_W_VVR2_OPEN | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_VVR2_CLOSE:
    newWr1 = VPG_BP_W_VVR2_CLOSE | VPG_W_REMOTE;
    break;
  case LVA_ACTION_VPG_SERVICE_DATE:
    SetServiceDate(dpName, exceptionInfo);
    return;
    break;
  case LVA_DEVICE_RESTART_MODE_VPG_LAST_KNOWN_STATE:
    newWr1 = VPG_W_RESTART_MODE_LAST_KNOWN_STATE | VPG_W_REMOTE;   
    break;
  case LVA_DEVICE_RESTART_MODE_VPG_STOPPED_PP_ON:
    newWr1 = VPG_W_RESTART_MODE_VPG_STOPPED_PP_ON | VPG_W_REMOTE;   
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = false;
    mConnectDpes["AL2"] = false;
    mConnectDpes["AL3"] = false;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = NO_VALUE_CHANGE;
    mConnectDpes["AL2"] = NO_VALUE_CHANGE;
    mConnectDpes["AL3"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    LhcVacDeviceActionCommon_VPG(act, newWr1, exceptionInfo);
    break;
  }

  // Execute
  dpSetWait(dpName + ".WR1", newWr1);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
}
void LhcVacDeviceAction_VPG_SA(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  unsigned	      newWr1;
  dyn_errClass	  err;
  unsigned        act;
  mapping         mConnectDpes;
        
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_VVR_OPEN:
    newWr1 = VPG_SA_W_VVR_OPEN | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_VVR_CLOSE:
    newWr1 = VPG_SA_W_VVR_CLOSE | VPG_W_REMOTE;
    break;
  case LVA_ACTION_VPG_SERVICE_DATE:
    SetServiceDate(dpName, exceptionInfo);
    return;
    break;
  case LVA_DEVICE_RESTART_MODE_VPG_LAST_KNOWN_STATE:
    newWr1 = VPG_W_RESTART_MODE_LAST_KNOWN_STATE | VPG_W_REMOTE;   
    break;
  case LVA_DEVICE_RESTART_MODE_VPG_STOPPED_PP_ON:
    newWr1 = VPG_W_RESTART_MODE_VPG_STOPPED_PP_ON | VPG_W_REMOTE;   
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = false;
    mConnectDpes["AL2"] = false;
    mConnectDpes["AL3"] = false;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = NO_VALUE_CHANGE;
    mConnectDpes["AL2"] = NO_VALUE_CHANGE;
    mConnectDpes["AL3"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    LhcVacDeviceActionCommon_VPG(act, newWr1, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "Action Execution: unknown action " + act, "");
      return;
    }
  }

  // Execute
  dpSetWait(dpName + ".WR1", newWr1);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
}
void LhcVacDeviceAction_VPG_GIS(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  unsigned		    newWr1;
  bool			      possible;
  dyn_errClass		err;
  unsigned        act;
  mapping         mConnectDpes;
        
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_VVR_OPEN:
    newWr1 = VPG_GIS_W_VVR_OPEN | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_VVR_CLOSE:
    newWr1 = VPG_GIS_W_VVR_CLOSE | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["TMPSpeed"] = NO_VALUE_CHANGE;
    mConnectDpes["TMPCurrent"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["TMPSpeed"] = NO_VALUE_CHANGE;
    mConnectDpes["TMPCurrent"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    LhcVacDeviceActionCommon_VPG(act, newWr1, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "Action Execution: unknown action " + act, "");
      return;
    }
  }

  // Execute
  dpSetWait(dpName + ".WR1", newWr1);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
}

void LhcVacDeviceAction_VPG_BGI(string dpName, string dpType, dyn_anytype action, dyn_string &exceptionInfo)
{
  unsigned  newWr1;
  bool      possible;
  dyn_errClass err;
  unsigned  act = action[1] & LVA_ACTION_MASK;
  mapping   mConnectDpes;

  switch(act & LVA_ACTION_MASK)
  {
  case LVA_DEVICE_VVR_OPEN:
    newWr1 = VPG_GIS_W_VVR_OPEN | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_VVR_CLOSE:
    newWr1 = VPG_GIS_W_VVR_CLOSE | VPG_W_REMOTE;
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["TMPSpeed"] = NO_VALUE_CHANGE;
    mConnectDpes["TMPCurrent"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["TMPSpeed"] = NO_VALUE_CHANGE;
    mConnectDpes["TMPCurrent"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    LhcVacDeviceActionCommon_VPG(act, newWr1, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "Action Execution: unknown action " + act, "");
      return;
    }
  }

  // Execute
  dpSetWait(dpName + ".WR1", newWr1);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
}

///LhcVacActionPossible_VP_GU:
/** Check if VGP is in state that allows executing actions for it
// At this moment we have no criteria to forbide action
Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param action: in, action to be excuted
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_VP_GU(string dpName, string dpType, dyn_anytype action, bool &possible,
	                           dyn_string &exceptionInfo) {
  dyn_errClass 	err;  
  unsigned act = action[1];
  int eqpType = 0;  
  dpGet(dpName + ".eqpCode", eqpType); //method to get eqpType for mobile
  err = getLastError();
  if(dynlen(err) > 0) {
    fwException_raise(exceptionInfo, 
                      "ERROR", "LhcVacActionPossible_VP_GU: Read eqpCode for <" + dpName 
                      + "> failed: " + err, "");
    possible = false;
    return;
  }
  if( eqpType < 30) { //if eqp is not mobile but fixed VPG
    int ctrlFamily, ctrlType, ctrlSubType;  
    LhcVacGetCtlAttributes(dpName, ctrlFamily, ctrlType, ctrlSubType);
    if (ctrlSubType > 0) {
      eqpType = ctrlSubType;
    }  
  }
  // ACTION NOT POSSIBLE FOR ALL EQP TYPES
  if((act & LVA_ACTION_MASK) == LVA_DEVICE_PURGE) { // Purge order
    if(!(eqpType == 30)) { // if not VPGMA
      possible = false;
      return;
    }    
  }
  if((act & LVA_ACTION_MASK) == LVA_DEVICE_VENT) { //Stop and Vent order not for insulation VPG
    if( (eqpType == 4) || // VPGFA
        (eqpType == 5) || // VPGFB                      
        (eqpType == 6) || // VPGFC
        (eqpType == 7) || // VPGFD
        (eqpType == 8) || // VPGFE
        (eqpType == 11) || // VPGFH
        (eqpType == 13) || // VPGFJ
        (eqpType == 14) || // VPGFK
        (eqpType == 16) || // VPGFY
        (eqpType == 17) || // VPGFZ
        (eqpType == 31) || // VPGMB
        (eqpType == 34)    // VPGME
                       ) { // if Insulation VPG: VPGFA(4)->E(8) + VPGFH(11) + VPGFJ(13)->K(14) + VPGFY(16)->Z(17) + VPGMA(30) + VPGMB(31) + VPGME(34)  
      possible = false;
      return;
    }    
  }
  if((act & LVA_ACTION_MASK) == LVA_DEVICE_STOP_PP_ON) { //Stop with primary On order not for Beam VPG and not for VPGMA
    if( (eqpType == 15) || // VPGFL Beam Fixed VPG
        (eqpType == 32) || // VPGMC                      
        (eqpType == 30)) { // VPGMA
      possible = false;
      return;
    }    
  }
  possible = true;
}
//
// SetServiceDate
//
//
void SetServiceDate(string dpName, dyn_string &exceptionInfo)
{
  dpSetWait(dpName + ".StatisticsStart", gServiceDate);
  dyn_string	err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
  dpSetWait(dpName + ".InitOperTime", gInitOperTime);
  dyn_string	err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
}
//LhcVacDeviceVVR1BackColorVPGM:
/** Form enumerated color value and PVSS color string depending on VVR of VPG state.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param mainData: in, contains device specific data rr2
	@param colorString: out, PVSS color string is returned here.
*/
void LhcVacDeviceVVR1BackColorVPG(unsigned rr2, string &stringColor)
{
  stringColor = "";
  if((rr2 & VPG_BP_R_VVR1_ERRORS) != 0)
  {
    if((rr2 & VPG_BP_R_VVR1_OPEN) != 0)
    {
      if((rr2 & VPG_BP_R_VVR1_CLOSED) != 0)
      {
        stringColor = VV_COLOR_ERROR;
      }
      else
      {
        stringColor = VPGM_COLOR_ICON_NORMAL;
      }		
    }
  }
}
//LhcVacDeviceVVR2BackColorVPGM:
/** Form enumerated color value and PVSS color string depending on VVR of VPG state.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param mainData: in, contains device specific data rr2
	@param colorString: out, PVSS color string is returned here.
*/
void LhcVacDeviceVVR2BackColorVPG(unsigned rr2, string &stringColor)
{
  stringColor = "";
  if((rr2 & VPG_BP_R_VVR2_ERRORS) != 0)
  {
    if((rr2 & VPG_BP_R_VVR2_OPEN) != 0)
    {
      if((rr2 & VPG_BP_R_VVR2_CLOSED) != 0)
      {
        stringColor = VV_COLOR_ERROR;
      }
      else
      {
        stringColor = VPGM_COLOR_ICON_NORMAL;
      }		
    }
  }
}
/* Build single string representing restart mode of VPG
*/
void LhcVacRestartModeStringVPG(int mode, string &result)
{
  switch(mode)
  {
  case 1:
    result = "Last state";
    break;
  case 2:
    result = "VPG stop & PP ON";
    break;
  default:
    sprintf(result, "%d   ?", mode);
  break;
  }
}

//Actions for VPG MBLK type
/** Execute action for VPG. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/


void LhcVacDeviceAction_VPG_MBLK(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  
  unsigned		    newWr1;
  bool			      possible;
  dyn_errClass		err;
  unsigned        act;
  mapping         mConnectDpes;
        
  act = action[1] & LVA_ACTION_MASK;

  switch(act)
  {
  case LVA_DEVICE_RESET: 
    newWr1 = VPG_MBLK_W_RESET;
    break;
  case LVA_VPG_MBLK_REMOTE: 
    newWr1 = VPG_MBLK_W_REMOTE;
    break;
  case LVA_VPG_MBLK_LOCAL: 
    newWr1 = VPG_MBLK_W_LOCAL;
    break;
  case LVA_DEVICE_MANUAL: 
    newWr1 = VPG_MBLK_W_MANUAL;
    break;
  case LVA_DEVICE_ON: 
    newWr1 = VPG_MBLK_W_ON;
    break;
  case LVA_DEVICE_OFF: 
    newWr1 = VPG_MBLK_W_OFF;
    break;
  case LVA_VPG_MBLK_VVR2_FORCE: 
    newWr1 = VPG_MBLK_W_VVR2_FORCE;
    break; 
  case LVA_VPG_MBLK_VVR2_UNFORCE: 
    newWr1 = VPG_MBLK_W_VVR2_UNFORCE;
    break; 
   case LVA_DEVICE_VVR2_OPEN: 
    newWr1 = VPG_MBLK_W_VVR2_OPEN;
    break;  
  case LVA_DEVICE_VVR2_CLOSE: 
    newWr1 = VPG_MBLK_W_VVR2_CLOSE ;
    break;
  case LVA_VPG_MBLK_VVR1_FORCE: 
    newWr1 = VPG_MBLK_W_VVR1_FORCE;
    break;
  case LVA_VPG_MBLK_VVR1_UNFORCE: 
    newWr1 = VPG_MBLK_W_VVR1_UNFORCE;
    break;
  case LVA_DEVICE_VVR1_OPEN: 
    newWr1 = VPG_MBLK_W_VVR1_OPEN ;
    break;
  case LVA_DEVICE_VVR1_CLOSE: 
    newWr1 = VPG_MBLK_W_VVR1_CLOSE ;
    break;
  case LVA_VPG_MBLK_VG2_ON: 
    newWr1 = VPG_MBLK_W_VG2_ON ;
    break;
  case LVA_VPG_MBLK_VG2_OFF: 
    newWr1 = VPG_MBLK_W_VG2_OFF ;
    break;
  case LVA_VPG_MBLK_VG1_ON: 
    newWr1 = VPG_MBLK_W_VG1_ON ;
    break;
  case LVA_VPG_MBLK_VG1_OFF: 
    newWr1 = VPG_MBLK_W_VG1_OFF ;
    break;
  case LVA_VPG_SET_MODE_OFF:
    newWr1 = VPG_MBLK_W_VG1_OFF ;
    break;
  case LVA_VPG_SET_MODE_STOP:
    newWr1 = VPG_MBLK_W_MODE_STOP ;
    break;
  case LVA_VPG_SET_MODE_PUMP:
    newWr1 = VPG_MBLK_W_MODE_PUMP ;
    break;
  case LVA_VPG_SET_MODE_VENT_TURBO:
    newWr1 = VPG_MBLK_W_MODE_VENT_TURBO ;
    break;
  case LVA_VPG_SET_MODE_VENT_ALL:
    newWr1 = VPG_MBLK_W_MODE_VENT_ALL ;
    break;
  case LVA_VPG_SET_MODE_LEAK_DET:
    newWr1 = VPG_MBLK_W_MODE_LEAK_DET ;
    break;
  case LVA_VPG_SET_MODE_LARGE_LEAK_DET:
    newWr1 = VPG_MBLK_W_MODE_LARGE_LEAK_DET ;
    break;
  case LVA_VPG_MBLK_RESET_VPP_THERM:
    newWr1 = VPG_MBLK_W_RESET_VPP_TH ;
    break;
  case LVA_VPG_MBLK_RESET_TMP_ERR:
    newWr1 = VPG_MBLK_W_RESET_TMP_ERR ;
    break;
  }
 
  
  // Execute
  dpSetWait(dpName + ".WR1", newWr1);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
  
}

