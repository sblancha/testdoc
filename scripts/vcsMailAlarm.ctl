// E-mail notification for problems during bakeout procedure

#uses "VacCtlEqpDataPvss"	// Load DLL or so
#uses "vclSectors.ctl"	// Load CTRL library
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "vclEmailConfig.ctl"	// Load CTRL library
#uses "email.ctl"	// Load PVSS standard CTRL library
#uses "vclMobile.ctl"	// Load CTRL library
#uses "vclDevCommon.ctl"	// Load CTRL library

//**************************** Types of alarms to be sent **********************
const int ALARM_TYPE_VPGM = 1;				// VPGM disconnected or connection to PLC lost
const int ALARM_TYPE_VPGM_VALVE = 2;	// Valve in VPGM has been closed
const int ALARM_TYPE_GAUGE = 3;				// Gauge swicthed OFF
const int ALARM_TYPE_VRPI = 4;				// VPI switched OFF
const int ALARM_TYPE_BAKEOUT = 5;			// Bakeout channel error, 

const int ALARM_TYPE_MAX = 5;					// Maximum number for alarm type

//********************** General configuration *********************************
// SMTP server
string smtpServer;

// E-mail sender
string sender;

// E-mail sender client
string	client;

// Recipients for problems in all sectors
dyn_string	recipientsAll;

// Sectors with specific recipients
dyn_string	sectorsSpecific;

// Recipients for specific sectors
dyn_string	recipientsSpecific;

// Sectors where notification is disabled
dyn_string	sectorsOff;

// Times when notification for disabled sectors shall be enabled back
dyn_time		sectorsOffEndTime;

//************************ Gauges processing ***********************************
// List of gauges under surveilance. Every list items contains:
// 1) Gauge DP name [string]
// 2) Last known gauge state [unsigned]
// Items are in two different lists in order to use dynContains() for
// gauges search in list
dyn_string	gaugeDps;
dyn_uint		gaugeStates;

//************************ VPGM processing *************************************
// List of mobile pumping groups under survey. Every list item contains:
// 1) VPGM name [string]
// 2) VPGM RR1 [unsigned]
// 3) VPGM RR2 [unsigned]
// 4) Alarm of PLC where VPGM is connected
// 5) Name of PLC alarm DP for this VPGM
// Items are in three different lists in order to use dynContains() for
// VPGM search in list
dyn_string		vpgmDps;
dyn_dyn_uint	vpgmStates;
dyn_string		vpgmPlcAlarmDps;

//************************ VRPI processing ***********************************
// List of VRPIs under surveilance. Every list items contains:
// 1) VRPI DP name [string]
// 2) Last known VRPI state [unsigned]
// Items are in two different lists in order to use dynContains() for
// VPRI search in list
dyn_string	vrpiDps;
dyn_uint		vrpiStates;

//************************ VBAKEOUT processing ***********************************
// List of racks under surveilance. Every list items contains:
// 1) bakeout DP name [string]
// 2) bakeout work DP name [string]
// 3) Name of PLC alarm DP for this rack
// 4) A list of rack parameters:
//		1...24) Last known value for state of channel 1...24
//		25) Last known value for RR1 of rack
//		26) Last state of PLC alarm DP
// Items are in three different lists in order to use dynContains() for
// searching bakeout DP and work DP search in lists
dyn_string		bakeoutDps;
dyn_string		bakeoutWorkDps;
dyn_string		bakeoutPlcAlarmDps;
dyn_dyn_uint	bakeoutStates;


//************** Prevent too often mail sending ******************************
// List of sectors for which notification has been sent. The purpose
// of list is to avoid too often messages for the same sector.
// Every list item contains:
// 1) Sector name [string]
// 2) Time when VPGM disconnect notification has been sent [time]
// 3) Time when VPGM valve notification has been sent [time]
// 4) Time when gauge notification has been sent [time]
//
// 4 items above are spread over 2 lists:
// 1) List of sector names
// 2) List of times for corresponding sector
// The purpose is to be able to find given sector using dynContains()
dyn_string		mailSectors;
dyn_dyn_time	mailTimes;

//************** Queue of pending notifications ******************************
// Sending message with emSendMail() takes significant time, this can slow
// down processing of DPE callbacks. In order to avoid too long staying in
// callback - DPE callback itself does NOT send message. Instead, it just puts
// parameters of message to be sent to the queue. Then main thread of script
// sends messages from the queue one by one. With this approach DPE callbacks
// shall be fast enough to avoid event queue overflow.
// There is non-zero chance that message will be set AFTER notification for
// corresponding sector has been disabled - if it was put to the queue before
// notification was disabled. Let's hope it will be acceptable...

// Every message in queue is represented by two values:
// 1) Name of vacuum sector, or name of bakeout rack [string]
// 2) Type of alarm to be sent for the sector, one of constants ALARM_TYPE_xxx [int]
dyn_anytype	msgQueue;

//************* Counter of E-mail send failures *******************************
// IF emSendMail() returned an error - the script does NOT remove message
// from message queue, so it will try to resend the message. However, there
// is a chance that first message will never be sent successfully, so all
// other messagesin the queue will be blocked forever.
// In order to avoid such blocking we introduce a counter of unsuccessfull
// attempts to send a message
int sendFailureCounter;

// Flag indicating if intialization is finished
bool initializationIsFinished;

void main()
{
	// Initialize static data
	if( ! InitStaticData() ) return;

	// Connect to E-mail configuration DPEs
	if( ! ConnectToConfig() ) return;

	// Connect to mobile list connection DPs
	dpConnect( "MobileConnectCb",
		VAC_MOBILE_LIST_DP + ".DP_Names",
		VAC_MOBILE_LIST_DP + ".State",
		VAC_MOBILE_LIST_DP + ".SourceDpNames",
		VAC_MOBILE_LIST_DP + ".WorkDPNames" );


	// Connect to gauges which are able to work in AUTO mode
	if( ! ConnectToGauges( ) ) return;
	// COnnect to all VRPIs
	if( ! ConnectToVRPIs( ) ) return;

	// Wait forever, check for timing parameters from time to time
	while( true )
	{
		delay( 2 );
		CheckEndTimes();
		SendMessages();
    initializationIsFinished = true;
	}
}
/** CheckEndTimes
Purpose:
Check if time to disable notification for sector(s) expired. If so -
enable notification back and update list of disbaled sectors

Parameters:
	- None

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void CheckEndTimes( )
{
	time				now = getCurrentTime( );
	dyn_string	backToOnList;

	for( int n = dynlen( sectorsOff ) ; n > 0 ; n-- )
	{
		if( sectorsOffEndTime[n] <= now )
		{
			string	tmp = sectorsOff[n];
			dynAppend( backToOnList, tmp );
		}
	}
	if( dynlen( backToOnList ) == 0 ) return;
	dyn_string	exceptionInfo;
	VacEmailEnableSector( backToOnList, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: VacEmailEnableSector() failed: ", exceptionInfo );
	}
}
/** SendMessages
Purpose:
Send E-mails for all messages pending in the message queue

Parameters:
	- None

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void SendMessages( )
{
	dyn_string	recipients;
	int					n, coco;

	while( dynlen( msgQueue ) > 1 )
	{
    if(!initializationIsFinished)
    {
      dynRemove( msgQueue, 1 );
      dynRemove( msgQueue, 1 );
      continue;
    }
 		// DebugTN( "lhcVacMailAlarm.ctl: queue size is " + dynlen( msgQueue ) );
		string	alarmSector = msgQueue[1];
		int			alarmType = msgQueue[2];
		// Find all recipients which shall be notified
		FindRecipients( alarmSector, recipients );
		if( dynlen( recipients ) == 0 )
		{
			dynRemove( msgQueue, 1 );
			dynRemove( msgQueue, 1 );
			continue;
		}
		// Prepare and send message
		dyn_string	email = makeDynString( "" );
		for( n = dynlen( recipients ) ; n > 0 ; n-- )
		{
			if( email[1] != "" ) email[1] += ";";
			email[1] += recipients[n];
		}
		email[2] = sender;
		switch( alarmType )
		{
		case ALARM_TYPE_VPGM:				// VPGM disconnected or connection to PLC lost
			email[3] = "Alarm group in sector " + alarmSector;
			break;
		case ALARM_TYPE_VPGM_VALVE:	// Valve in VPGM has been closed
			email[3] = "Alarm group valve in sector " + alarmSector;
			break;
		case ALARM_TYPE_GAUGE:			// Gauge switched OFF
			email[3] = "Alarm gauge in sector " + alarmSector;
			break;
		case ALARM_TYPE_VRPI:				// Ion pump switched OFF
			email[3] = "Alarm ion pump in sector " + alarmSector;
			break;
		case ALARM_TYPE_BAKEOUT:	// Bakeout channel error
			email[3] = "Alarm bakeout rack in sector " + alarmSector;	// Here is rack name, not sector name
			break;
		}
		email[4] = "";
		DebugTN( "lhcVacMailAlarm.ctl: message to send is '" + email[3] + "'" );
		emSendMail( smtpServer, client, email, coco );
		if( coco < 0 )	// Failed, make pause then try again
		{
			if( sendFailureCounter < 3 )
			{
				sendFailureCounter++;
				return;
			}
		}
		sendFailureCounter = 0;	// Reset failures counter
		// Remove message from the queue
		dynRemove( msgQueue, 1 );
		dynRemove( msgQueue, 1 );
		// Put timestamp when message was sent last time
		int	mailSectorIdx = dynContains( mailSectors, alarmSector );
		if( mailSectorIdx <= 0 )
		{
			mailSectorIdx = dynAppend( mailSectors, alarmSector );
			time	farInThePast = makeTime( 1980, 1, 1 );
			dyn_time newMailTimes;
			for( n = 1 ; n <= ALARM_TYPE_MAX ; n++ )
			{
				dynAppend( newMailTimes, ( alarmType == n ? getCurrentTime() : farInThePast ) );
			}
			dynAppend( mailTimes, newMailTimes );
		}
		else
		{
			mailTimes[mailSectorIdx][alarmType] = getCurrentTime();
		}
	}
}
/** FindRecipients
Purpose:
Find all recipients which shall be notified in case of problems
in given sector. The function also checks other conditions:
- if notification for sector is disabled
- if all parameters for mail sending are available

Parameters:
	- sector - [in] name of vacuum sector where alarm condition is detected
	- recipients - [out] list of recipients who shall be notified

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void FindRecipients( string sector, dyn_string &recipients )
{
	dynClear( recipients );
	// Check if general E-mail notification parameters are available
	if( ( smtpServer == "" ) || ( client == "" ) || ( sender == "" ) )
		return;
	// Check if notification is disabled for this sector
	if( dynContains( sectorsOff, sector ) > 0 ) return;
	// Check if specific recipients for this sector exist
	int n;
	for( n = dynlen( sectorsSpecific ) ; n > 0 ; n-- )
	{
		if( sectorsSpecific[n] == sector )
		{
			if( dynContains( recipients, recipientsSpecific[n] ) <= 0 )
			{
				string	recipient = recipientsSpecific[n];
				dynAppend( recipients, recipient );
			}
		}
	}
	// Add recipients of all sectors
	for( n = dynlen( recipientsAll ) ; n > 0 ; n-- )
	{
		if( dynContains( recipients, recipientsAll[n] ) <= 0 )
		{
			string	tmp = recipientsAll[n];
			dynAppend( recipients, tmp );
		}
	}
}
/** ReportAlarm
Purpose:
Check if alarm of given type for sector of given DP shall be sent. If so -
add parameters of message to be sent to the message queue

Parameters:
	- dpName - [in] name of DP where alarm condition is detected
	- alarmType - [in] type of alarm detected, see ALARM_TYPE_xxx constants

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void ReportAlarm( string dpName, int alarmType )
{
	dyn_string	exceptionInfo;
	string	sector1, sector2, mainPart;

// DebugTN("lhcVacMailAlarm.ctl: ReportAlarm(" + dpName + "," + alarmType + ")" );
	// Find sector where device is located
	int	vacType;
	bool		isBorder;
	string dpForSector;
	if( alarmType == ALARM_TYPE_VRPI )	// Take sector of 1st VPI
	{
		dyn_string	slaveDpNames;
		LhcVacEqpSlaves( dpName, slaveDpNames);
		if( dynlen( slaveDpNames ) == 0 )
		{
			DebugTN( "lhcVacMailAlarm.ctl: ERROR: ReportAlarm(): No slaves for <" + dpName + ">" );
			return;
		}
		dpForSector = slaveDpNames[1];
	}
	else
	{
		dpForSector = dpName;
	}
	LhcVacDeviceVacLocation( dpForSector, vacType, sector1, sector2, mainPart, isBorder, exceptionInfo);
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: ERROR: ReportAlarm(): LhcVacDeviceVacLocation(" + dpForSector + ") failed:",
			exceptionInfo );
		return;
	}
	if( sector1 == "" )
	{
		DebugTN( "lhcVacMailAlarm.ctl: ERROR: ReportAlarm(): no sector for <" + dpForSector + ">" );
		return;
	}
	// Check if notification is disabled for this sector
	if( dynContains( sectorsOff, sector1 ) > 0 )
	{
		// DebugTN("hcVacMailAlarm.ctl: INFO: sector " + sector1 + " of " + dpName + " deactivated" );
		return;
	}

	// Check if such notification is pending in the queue
	string	nameToCheck;
	switch( alarmType )
	{
	case ALARM_TYPE_BAKEOUT:	// Use bakeout rack name
		/* LIK 05.06.2007
		LhcVacDisplayName( dpName, nameToCheck, exceptionInfo );
		if( nameToCheck == "" ) nameToCheck = dpName;
		*/
		nameToCheck = sector1;
		break;
	default:	// Use equipment sector name
		nameToCheck = sector1;
		break;
	}
	for( int n = dynlen( msgQueue ) - 1 ; n > 0 ; n -= 2 )
	{
		if( msgQueue[n] != nameToCheck ) continue;
		if( msgQueue[n+1] == alarmType )
		{
			/*
			DebugTN("hcVacMailAlarm.ctl: INFO: message for " + nameToCheck + " of type " +
				alarmType + " is already in the queue" );
			*/
			return;	// Message is already in the queue
		}
	}
	// Check if notification of this type for this sector was sent not long time ago
	// 15 minutes dead time ????
	int	mailSectorIdx = dynContains( mailSectors, nameToCheck );
	time	now = getCurrentTime();
	if( mailSectorIdx > 0 )
	{
		if( ( now - mailTimes[mailSectorIdx][alarmType] ) < ( 60 * 15 ) )
		{
			/*
			DebugTN("hcVacMailAlarm.ctl: INFO: message for " + nameToCheck + " of type " +
				alarmType + " was sent less than 15 minutes ago" );
			*/
		 	return;
		}
	}
	// Add message to the queue
	dynAppend( msgQueue, (string)nameToCheck );
	dynAppend( msgQueue, (int)alarmType );
}
/** MobileConnectCb
Purpose:
Callback for list of active mobile equipment in system.
If new mobile is connected - connect to new equipment.
If previously connected mobile is disconnected - send alarm and
disconnect from equipment that disappeared.

Parameters:
	- dpe1 - [in] name of DPE containing names of mobile equipment DPEs
	- mobileEqpList - [in] List of mobile equipment DP names
	- dpe2 - [in] name of DPE containing states for every mobile equipment
			in mobileEqpList
	- mobileStateList - [in] List of states for every mobile DP in mobileEqpList.
		Value 'true' means that corresponding mobile DP is active.
		Value 'false' means that corresponding mobile DP is deactivated
	- dpe3 - [in] name of DPE containing names of VMOB DPs for every mobile
	- sourceDpNames -[in] List of source VMOB DP names for every mobile DP
	- dpe4 - [in] Name of DPE containing list of work DPs
	- workDpList - [in] List of work DPs for all mobile eqp
Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void MobileConnectCb( string dpe1, dyn_string mobileEqpList, string dpe2,
	dyn_bool mobileStateList, string dpe3, dyn_string sourceDpList,
	string dpe4, dyn_string workDpList )
{
	CheckVpgmDisconnect( mobileEqpList, workDpList, mobileStateList );
	CheckVpgmConnect( mobileEqpList, workDpList, mobileStateList, sourceDpList );
	CheckBakeoutDisconnect( mobileEqpList, workDpList, mobileStateList, sourceDpList );
	CheckBakeoutConnect( mobileEqpList, workDpList, mobileStateList, sourceDpList );
}
/** CheckVpgmDisconnect
Purpose:
Check if previously connected VPGM has been disconnected based on
newest callback data from _VacMobileList DP

Parameters:
	- mobileEqpList - [in] List of mobile equipment DP names
	- workDpList - [in] List of work DPs for all mobile eqp - NOT USED NOW,
										but may be used if (when) VPGMS will be processed
										similar to bakeout racks
	- mobileStateList - [in] List of states for every mobile DP in mobileEqpList.
		Value 'true' means that corresponding mobile DP is active.
		Value 'false' means that corresponding mobile DP is deactivated
Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void CheckVpgmDisconnect( dyn_string mobileEqpList, dyn_string workDpList,
	dyn_bool mobileStateList )
{
	bool	toChange;
	int		i;

	for( int n = dynlen( vpgmDps ) ; n > 0 ; n-- )
	{
		// ERROR LIK 29.03.2007 if( ! dpExists( mobileEqpList[n] ) ) continue;	// bakeout rack
		toChange = false;
		if( ( i = dynContains( mobileEqpList, vpgmDps[n] ) ) > 0 )
		{
			if( ! mobileStateList[i] ) toChange = true;
		}
		else toChange = true;
		if( toChange )
		{
			string alarmDp = vpgmPlcAlarmDps[n];
			if( alarmDp != "" )
			{
				dpDisconnect( "VpgmStateCbPLC", vpgmDps[n] + ".RR1",
					vpgmDps[n] + ".RR2",
					alarmDp + ".alarm" );
			}
			else
			{
				dpDisconnect( "VpgmStateCbNoPLC", vpgmDps[n] + ".RR1",
					vpgmDps[n] + ".RR2" );
			}
			ReportAlarm( vpgmDps[n], ALARM_TYPE_VPGM );
			dynRemove( vpgmDps, n );
			dynRemove( vpgmStates, n );
			dynRemove( vpgmPlcAlarmDps, n );
		}
	}
}
/** CheckVpgmConnect
Purpose:
Check if new VPGM(s) have been connected based on
newest callback data from _VacMobileList DP

Parameters:
	- mobileEqpList - [in] List of mobile equipment DP names
	- workDpList - [in] List of work DPs for all mobile eqp - NOT USED NOW,
										but may be used if (when) VPGMS will be processed
										similar to bakeout racks
	- mobileStateList - [in] List of states for every mobile DP in mobileEqpList.
		Value 'true' means that corresponding mobile DP is active.
		Value 'false' means that corresponding mobile DP is deactivated
	- sourceDpNames -[in] List of source VMOB DP names for every mobile DP
Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void CheckVpgmConnect( dyn_string mobileEqpList, dyn_string workDpList,
	dyn_bool mobileStateList, dyn_string sourceDpList )
{
	int					n, i;
	string			plcDp, alarmDp;
	dyn_string	exceptionInfo;

	for( n = dynlen( mobileEqpList ) ; n > 0 ; n-- )
	{
		if( ! dpExists( mobileEqpList[n] ) ) continue;	// bakeout rack
		if( ! mobileStateList[n] ) continue;
		if( dynContains( vpgmDps, mobileEqpList[n] ) > 0 ) continue;
		string dpName = mobileEqpList[n];
		dynAppend( vpgmDps, dpName );
		dynAppend( vpgmStates, makeDynUInt( 0, 0, 0 ) );
		LhcVacDevicePlc( sourceDpList[n], plcDp, alarmDp, exceptionInfo );
		dynAppend( vpgmPlcAlarmDps, alarmDp );
		// DebugTN( dpName, plcDp, alarmDp, exceptionInfo );
		if( alarmDp != "" )
		{
			dpConnect( "VpgmStateCbPLC", dpName + ".RR1",
				dpName + ".RR2",
				alarmDp + ".alarm" );
		}
		else
		{
			dpConnect( "VpgmStateCbNoPLC", dpName + ".RR1",
				dpName + ".RR2" );
		}
	}
}
/** VpgmStateCbPLC
Purpose:
Callback for state of VPGM and PLC where VPGM is connected.

Parameters:
	- dpeRR1 - [in] name of DPE containing RR1 register of VPGM
	- stateRR1 - [in] value of RR1 register of VPGM
	- dpeRR2 - [in] name of DPE containing RR2 register of VPGM
	- stateRR2 - [in] value of RR2 register of VPGM
	- dpePLC - [in] name of DPE containing PLC alarm state
	- statePLC - [in] value of PLC alarm state (!=0 - connection to PLC lost)

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void VpgmStateCbPLC( string dpeRR1, unsigned stateRR1, string dpeRR2, unsigned stateRR2,
	string dpePLC, unsigned statePLC )
{
	string	dpName = dpSubStr( dpeRR1, DPSUB_DP );
	int	dpIdx = dynContains( vpgmDps, dpName );
	if( dpIdx <= 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: INTERNAL: VpgmStateCbPLC() from unexpected " + dpName );
		return;
	}
	unsigned	oldRR1 = vpgmStates[dpIdx][1],
						oldRR2 = vpgmStates[dpIdx][2],
						oldPLC = vpgmStates[dpIdx][3];
	vpgmStates[dpIdx][1] = stateRR1;
	vpgmStates[dpIdx][2] = stateRR2;
	vpgmStates[dpIdx][3] = statePLC;
	if( statePLC != 0 )	// Connection to PLC has been lost
	{
		if( oldPLC == 0 )
			ReportAlarm( dpName, ALARM_TYPE_VPGM );
		return;
	}
	if( ( stateRR1 & VPGM_R_VALID ) != 0 )	// State is valid
	{
		if( ( stateRR2 & VPGM_R_VVR_CLOSED ) == 0 ) return;	// VVR is not closed
	}
	else return;
	if( oldRR1 == 0 )
		ReportAlarm( dpName, ALARM_TYPE_VPGM_VALVE );
	else if( ( ( oldRR1 & VPGM_R_VALID ) != 0 ) &&
					 ( ( oldRR2 & VPGM_R_VVR_CLOSED ) == 0 ) )
	{
		ReportAlarm( dpName, ALARM_TYPE_VPGM_VALVE );
	}
}
/** VpgmStateCbNoPLC
Purpose:
Callback for state of VPGM - used if state of PLC, where VPGM connected,
is not monitored.

Parameters:
	- dpeRR1 - [in] name of DPE containing RR1 register of VPGM
	- stateRR1 - [in] value of RR1 register of VPGM
	- dpeRR2 - [in] name of DPE containing RR2 register of VPGM
	- stateRR2 - [in] value of RR2 register of VPGM

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void VpgmStateCbNoPLC( string dpeRR1, unsigned stateRR1, string dpeRR2, unsigned stateRR2 )
{
	VpgmStateCbPLC( dpeRR1, stateRR1, dpeRR2, stateRR2, "", 0 );
}
/** CheckBakeoutDisconnect
Purpose:
Check if previously connected bakeout rack has been disconnected based on
newest callback data from _VacMobileList DP

Parameters:
	- mobileEqpList - [in] List of mobile equipment DP names
	- workDpList - [in] List of work DPs for all mobile eqp
	- mobileStateList - [in] List of states for every mobile DP in mobileEqpList.
		Value 'true' means that corresponding mobile DP is active.
		Value 'false' means that corresponding mobile DP is deactivated
Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void CheckBakeoutDisconnect( dyn_string mobileEqpList, dyn_string workDpList,
	dyn_bool mobileStateList )
{
	bool	toChange;
	int		i;

	for( int n = dynlen( bakeoutDps ) ; n > 0 ; n-- )
	{
		// ERROR LIK 29/03/2007 if( dpExists( mobileEqpList[n] ) ) continue;	// not bakeout rack
		toChange = false;
		if( ( i = dynContains( mobileEqpList, bakeoutDps[n] ) ) > 0 )
		{
			if( ! mobileStateList[i] ) toChange = true;
		}
		else toChange = true;
		// DebugTN( "CheckBakeoutDisconnect():", bakeoutDps[n], toChange );
		if( toChange )
		{
			DisconnectBakeoutRack( bakeoutWorkDps[n], bakeoutPlcAlarmDps[n] );
			ReportAlarm( bakeoutDps[n], ALARM_TYPE_BAKEOUT );
			dynRemove( bakeoutDps, n );
			dynRemove( bakeoutWorkDps, n );
			dynRemove( bakeoutPlcAlarmDps, n );
			dynRemove( bakeoutStates, n );
		}
	}
}
/** CheckBakeoutConnect
Purpose:
Check if new bakeout rack(s) have been connected based on
newest callback data from _VacMobileList DP

Parameters:
	- mobileEqpList - [in] List of mobile equipment DP names
	- workDpList - [in] List of work DPs for all mobile eqp - NOT USED NOW,
										but may be used if (when) VPGMS will be processed
										similar to bakeout racks
	- mobileStateList - [in] List of states for every mobile DP in mobileEqpList.
		Value 'true' means that corresponding mobile DP is active.
		Value 'false' means that corresponding mobile DP is deactivated
	- sourceDpNames -[in] List of source VMOB DP names for every mobile DP
Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void CheckBakeoutConnect( dyn_string mobileEqpList, dyn_string workDpList,
	dyn_bool mobileStateList, dyn_string sourceDpList )
{
	int					n, i;
	string			plcDp, alarmDp;
	dyn_string	exceptionInfo;

	for( n = dynlen( mobileEqpList ) ; n > 0 ; n-- )
	{
		if( dpExists( mobileEqpList[n] ) ) continue;	// not a bakeout rack
		if( ! mobileStateList[n] ) continue;
		if( ( i = dynContains( bakeoutDps, mobileEqpList[n] ) ) > 0 )
		{
			if( bakeoutWorkDps[i] != workDpList[n] )	// The same rack on another work DP
			{
				DisconnectBakeoutRack( bakeoutWorkDps[i], bakeoutPlcAlarmDps[i] );
				bakeoutWorkDps[i] = workDpList[n];
				ConnectBakeoutRack( bakeoutWorkDps[i], sourceDpList[n], i );
			}
			continue;
		}
		string dpName = mobileEqpList[n];
		dynAppend( bakeoutDps, dpName );
		dpName = workDpList[n];
		dynAppend( bakeoutWorkDps, dpName );
		dyn_uint	rackState = makeDynUInt( 0, 0 );
		for( i = 1 ; i <= 24 ; i++ ) dynAppend( rackState, 0 );
		dynAppend( bakeoutStates, rackState );
		ConnectBakeoutRack( workDpList[n], sourceDpList[n], 0 );
	}
}
/** ConnectBakeoutRack
Purpose:
Subscribe to DPEs of bakeout rack and PLC health DPE (if any),
put name of PLC alarm DP for rack to bakeoutPlcAlarmDps list

Parameters:
	- workDp - [in] Name of work DP for bakeout rack
	- sourceDp - [in] Name of source DP (VMOB), used to get PLC name
	- targetIdx - [in] Index of this rack in list of racks, or 0 if
			this is new rack
Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void ConnectBakeoutRack( string workDp, string sourceDp, int targetIdx )
{
	string			plcDp, alarmDp, dpeName;
	dyn_string	exceptionInfo;
	int					n;

	LhcVacDevicePlc( sourceDp, plcDp, alarmDp, exceptionInfo );
	if( targetIdx > 0 )
		bakeoutPlcAlarmDps[targetIdx] = alarmDp;
	else
		dynAppend( bakeoutPlcAlarmDps, alarmDp );

	// DebugTN( workDp, sourceDp, plcDp, alarmDp, exceptionInfo );
	// Connect to states of all 24 channel
	for( n = 1 ; n <= 24 ; n++ )
	{
		sprintf( dpeName, "%s.CH%02d.STATE", workDp, n );
		dpConnect( "BakeoutChannelStateCb", dpeName );
	}
	// Connect to RR1 DPE of rack
	if( alarmDp != "" )
	{
		dpConnect( "BakeoutStateCbPLC", workDp + ".RR1",
			alarmDp + ".alarm" );
	}
	else
	{
		dpConnect( "BakeoutStateCbNoPLC", workDp + ".RR1" );
	}
}
/** DisconnectBakeoutRack
Purpose:
Disconnect from DPEs of bakeout rack and PLC health DPE (if any)

Parameters:
	- workDp - [in] Name of work DP for bakeout rack
	- plcAlarmDp - [in] Name of DP with PLC alarm state, may be empty string
Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void DisconnectBakeoutRack( string workDp, string plcAlarmDp )
{
	string			dpeName;
	int					n;

	// Disconnect from RR1 and (optionally) PLC alarm DPE
	if( plcAlarmDp != "" )
	{
		dpDisconnect( "BakeoutStateCbPLC", workDp + ".RR1",
			plcAlarmDp + ".alarm" );
	}
	else
	{
		dpDisconnect( "BakeoutStateCbNoPLC", workDp + ".RR1" );
	}
	// Disconnect from states of all 24 channel
	for( n = 1 ; n <= 24 ; n++ )
	{
		sprintf( dpeName, "%s.CH%02d.STATE", workDp, n );
		dpDisconnect( "BakeoutChannelStateCb", dpeName );
	}
}
/** BakeoutStateCbPLC
Purpose:
Callback for RR1 of bakeout rack and PLC where bakeout is connected.

Parameters:
	- dpeRR1 - [in] name of DPE containing RR1 register of bakeout rack
	- stateRR1 - [in] value of RR1 register of bakeout rack
	- dpePLC - [in] name of DPE containing PLC alarm state
	- statePLC - [in] value of PLC alarm state (!=0 - connection to PLC lost)

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void BakeoutStateCbPLC( string dpeRR1, unsigned stateRR1, string dpePLC, unsigned statePLC )
{
	string	dpName = dpSubStr( dpeRR1, DPSUB_DP );
	int	dpIdx = dynContains( bakeoutWorkDps, dpName );
	if( dpIdx <= 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: INTERNAL: BakeoutStateCbPLC() from unexpected " + dpName );
		return;
	}
	unsigned	oldRR1 = bakeoutStates[dpIdx][25],
						oldPLC = bakeoutStates[dpIdx][26];
	bakeoutStates[dpIdx][25] = stateRR1;
	bakeoutStates[dpIdx][26] = statePLC;

	if( statePLC != 0 )	// Connection to PLC has been lost
	{
		if( oldPLC == 0 )
		{
			ReportAlarm( bakeoutDps[dpIdx], ALARM_TYPE_BAKEOUT );
		}
		return;
	}
	if( ( stateRR1 & 0x40000000u ) == 0 )	// State is not valid
	{
		return;
	}
	if( ( stateRR1 & 0x01000000u ) == 0 )	// Rack is not in safety mode
	{
		for( int n = 1 ; n <= 24 ; n++ )
		{
			unsigned channelMask = 1 << ( n - 1 );
			if( ( ( stateRR1 & channelMask ) != 0 )	&&	// Channel n is active
				( ( oldRR1 & channelMask ) == 0 ) )				// And it was not active before
			{
				if( bakeoutStates[dpIdx][n] > 4 )
				{
					ReportAlarm( bakeoutDps[dpIdx], ALARM_TYPE_BAKEOUT );
				}
			}
		}
	}
	else
	{
		for( int n = 1 ; n <= 24 ; n++ )
		{
			unsigned channelMask = 1 << ( n - 1 );
			if( ( ( stateRR1 & channelMask ) != 0 )	&&	// Channel n is active
				( ( oldRR1 & channelMask ) == 0 ) )				// And it was not active before
			{
				if( bakeoutStates[dpIdx][n] > 3 )
				{
					ReportAlarm( bakeoutDps[dpIdx], ALARM_TYPE_BAKEOUT );
				}
			}
		}
	} 
}
/** BakeoutStateCbNoPLC
Purpose:
Callback for RR1 of bakeout rack - used if state of PLC, where rack is connected,
is not monitored.

Parameters:
	- dpeRR1 - [in] name of DPE containing RR1 register of bakeout rack
	- stateRR1 - [in] value of RR1 register of bakeout rack

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void BakeoutStateCbNoPLC( string dpeRR1, unsigned stateRR1 )
{
	BakeoutStateCbPLC( dpeRR1, stateRR1, "", 0 );
}
/** BakeoutChannelStateCb
Purpose:
Callback for state of one bakeout rack channel

Parameters:
	- dpeState - [in] name of DPE containing STATE register of bakeout rack channel
	- state - [in] value of STATE register of bakeout rack channel

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void BakeoutChannelStateCb( string dpeState, unsigned state )
{
	string	dpName = dpSubStr( dpeState, DPSUB_DP );
	int	dpIdx = dynContains( bakeoutWorkDps, dpName );
	if( dpIdx <= 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: INTERNAL: BakeoutChannelStateCb() from unexpected " + dpName );
		return;
	}
	string	dpDpe = dpSubStr( dpeState, DPSUB_DP_EL );
	int index = strtok( dpDpe, "CH" );
	if( index < 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: failed to find 'CH' in <" + dpDpe + ">" );
		return;
	}
	string channelNoString = substr( dpDpe, index + 2, 2 );
	int	channelIdx = channelNoString;
	if( ( channelIdx < 1 ) || ( channelIdx > 24 ) )
	{
		DebugTN( "lhcVacMailAlarm.ctl: bad channel index " + channelIdx + " from <" + dpDpe + ">" );
		return;
	}
	unsigned	oldState = bakeoutStates[dpIdx][channelIdx],
						stateRR1 = bakeoutStates[dpIdx][25],
						statePLC = bakeoutStates[dpIdx][26];
	bakeoutStates[dpIdx][channelIdx] = state;
	if( statePLC != 0 )	// Connection to PLC has been lost
	{
		return;
	}
	if( ( stateRR1 & 0x40000000u ) == 0 )	// State is not valid
	{
		return;
	}
	if( ( stateRR1 & 0x01000000u ) == 0 )	// Rack is not in safety mode
	{
		unsigned channelMask = 1 << ( channelIdx - 1 );
		if( ( stateRR1 & channelMask ) != 0 )	// Channel is active
		{
			if( ( state > 4 )		// New state is error
				&& ( oldState <= 4 ) )	// And there was no error before
			{
				ReportAlarm( bakeoutDps[dpIdx], ALARM_TYPE_BAKEOUT );
			}
		}
	}
	else	// Rack is in safety mode
	{
		unsigned channelMask = 1 << ( channelIdx - 1 );
		if( ( stateRR1 & channelMask ) != 0 )	// Channel is active
		{
			if( ( state > 3 )		// New state is error
				&& ( oldState <= 3 ) )	// And there was no error before
			{
				ReportAlarm( bakeoutDps[dpIdx], ALARM_TYPE_BAKEOUT );
			}
		}
	} 
}

/** ConnectToGauges
Purpose:
Connect to states of all gauges in beam vacuum sectors. Only
gauges supporting AUTO mode are included.

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToGauges( )
{
	if( ! ConnectToGaugesOfType( "VGP_C" ) ) return false;
	if( ! ConnectToGaugesOfType( "VGP_T" ) ) return false;
	return true;
}
/** ConnectToGaugesOfType
Purpose:
Connect to states of all gauges of given DP type in beam vacuum sectors.

Parameters:
	- taregtDpType - [in], name of DP type to which we shall connect

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToGaugesOfType( string targetDpType )
{
	dyn_string	exceptionInfo,
							allDpNames = dpNames( "*", targetDpType );

	for( int n = dynlen( allDpNames ) ; n > 0 ; n-- )
	{
		int	vacType;
		string	sector1, sector2, mainPart;
		bool		isBorder;
		string	gaugeDp = dpSubStr( allDpNames[n], DPSUB_DP );
		LhcVacDeviceVacLocation( gaugeDp, vacType, sector1, sector2, mainPart, isBorder, exceptionInfo);
		if( dynlen( exceptionInfo ) > 0 )
		{
			dynClear( exceptionInfo );
			continue;
		}
		bool	isMyGauge = true;
		// Ignore gauges on QRL and CRYO vacuum
		switch( vacType )
		{
		case LVA_VACUUM_QRL:
		case LVA_VACUUM_CRYO:
			isMyGauge = false;
			break;
		}
		if( ! isMyGauge ) continue;
		// Ignore gauges in ARC sectors. Name of ARC sector is hardcoded which is not so good...
		if( sector1 == "" ) continue;
		if( patternMatch( "ARC*", sector1 ) ) continue;
		// My gauge - add to list and connect
		string	dpe = gaugeDp + ".RR1";
		if( ! dpExists( dpe ) ) continue;
		dynAppend( gaugeDps, gaugeDp );
		dynAppend( gaugeStates, 0u );
		dpConnect( "GaugeStateCb", dpe );
	}
	DebugTN( "lhcVacMailAarm.ctl: Finish DP type " + targetDpType + ", " + dynlen( gaugeDps ) );
	return true;
}
/** GaugeStateCb
Purpose:
Callback for state of gauges.

Parameters:
	- dpe - [in] name of DPE containing RR1 register of gauge
	- state - [in] value of RR1 register of gauge

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void GaugeStateCb( string dpe, unsigned state )
{
	string	dpName = dpSubStr( dpe, DPSUB_DP );
	int			dpIdx = dynContains( gaugeDps, dpName );
	if( dpIdx <= 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: INTERNAL: callback from unextected DP <" + dpName + ">" );
		return;
	}
	// Analyze gauge state
	unsigned oldState = gaugeStates[dpIdx];
	gaugeStates[dpIdx] = state;
	if( ( state & VG_SPS_R_VALID ) == 0 ) // Data is not valid
		return;
/*	LIK 24.12.2006
	if( ( state & VG_SPS_R_AUTO ) == 0 )	// Gauge is not in AUTO mode
		return;
*/
	if( ( state & VG_SPS_R_PROTECT ) != 0 )	// Protect bit is set, compare to previous state
	{
		if( oldState == 0 )	// Very first data
		{
			ReportAlarm( dpName, ALARM_TYPE_GAUGE );
		}
		/*
		else if( ( ( oldState & VG_SPS_R_AUTO ) != 0 ) &&	// Was in AUTO mode before and
						 ( ( oldState & VG_SPS_R_PROTECT ) == 0 ) )	// Protect bit was not set
		*/
		else if( ( oldState & VG_SPS_R_PROTECT ) == 0 )	// and Protect bit was not set
		{
			ReportAlarm( dpName, ALARM_TYPE_GAUGE );
		}
	}
}
/** ConnectToVRPIs
Purpose:
Connect to states of all VRPIs in beam vacuum sectors.

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToVRPIs( )
{
	if( ! ConnectToVRPIsOfType( "VRPI_W" ) ) return false;
	if( ! ConnectToVRPIsOfType( "VRPI_L" ) ) return false;
	return true;
}
/** ConnectToVRPIsOfType
Purpose:
Connect to states of all VRPIs of given DP type in beam vacuum sectors.

Parameters:
	- taregtDpType - [in], name of DP type to which we shall connect

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToVRPIsOfType( string targetDpType )
{
	dyn_string	exceptionInfo,
							allDpNames = dpNames( "*", targetDpType );

	for( int n = dynlen( allDpNames ) ; n > 0 ; n-- )
	{
		int	vacType;
		string	sector1, sector2, mainPart;
		bool		isBorder;
		string	dpName = dpSubStr( allDpNames[n], DPSUB_DP );
		// Get sector name from slave VPI
		dyn_string	slaveDpNames;
		LhcVacEqpSlaves(dpName, slaveDpNames);
		if( dynlen( slaveDpNames ) == 0 )
		{
			DebugTN( "lhcVacMailAlarm.ctl: ERROR: ConnectToVRPIsOfType(): No slaves for <" + dpName + ">" );
			return false;
		}
		LhcVacDeviceVacLocation( slaveDpNames[1], vacType, sector1, sector2, mainPart, isBorder, exceptionInfo);
		if( dynlen( exceptionInfo ) > 0 )
		{
			dynClear( exceptionInfo );
			continue;
		}
		bool	isMyDp = true;
		// Ignore VRPIs on QRL and CRYO vacuum
		switch( vacType )
		{
		case LVA_VACUUM_QRL:
		case LVA_VACUUM_CRYO:
			isMyGauge = false;
			break;
		}
		if( ! isMyDp ) continue;
		// Ignore VPIs in ARC sectors. Name of ARC sector is hardcoded which is not so good...
		if( sector1 == "" ) continue;
		if( patternMatch( "ARC*", sector1 ) ) continue;
		// My VRPI - add to list and connect
		string	dpe = dpName + ".RR1";
		if( ! dpExists( dpe ) ) continue;
		dynAppend( vrpiDps, dpName );
		dynAppend( vrpiStates, 0u );
		dpConnect( "VrpiStateCb", dpe );
	}
	DebugTN( "lhcVacMailAarm.ctl: Finish DP type " + targetDpType + ", " + dynlen( vrpiDps ) );
	return true;
}
/** VrpiStateCb
Purpose:
Callback for state of VRPIs.

Parameters:
	- dpe - [in] name of DPE containing RR1 register of VRPI
	- state - [in] value of RR1 register of VRPI

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void VrpiStateCb( string dpe, unsigned state )
{
	string	dpName = dpSubStr( dpe, DPSUB_DP );
	int			dpIdx = dynContains( vrpiDps, dpName );
	if( dpIdx <= 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: INTERNAL: callback from unextected DP <" + dpName + ">" );
		return;
	}
	// Analyze gauge state
	unsigned oldState = vrpiStates[dpIdx];
	vrpiStates[dpIdx] = state;
	if( ( state & VRPI_W_R_VALID ) == 0 ) // Data is not valid
		return;
	if( ( state & VRPI_W_R_OFF ) != 0 )	// OFF bit is set, compare to previous state
	{
		if( oldState == 0 )	// Very first data - do not send alarm
		{
			// ReportAlarm( dpName, ALARM_TYPE_VRPI );
			return;
		}
		else if( ( oldState & VRPI_W_R_OFF ) == 0 )	// and OFF bit was not set
		{
			ReportAlarm( dpName, ALARM_TYPE_VRPI );
		}
	}
}
/** InitStaticData
Purpose:
Read all machine configuration data from file(s)

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool InitStaticData( )
{
	dyn_string	exceptionInfo;
	long				coco;
	bool				isLhcData;

	// Set accelerator name to be used by other components
	LhcVacGetMachineId( exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: Failed to read machine name: " + exceptionInfo );
		return false;
	}
	DebugTN( "lhcVacMailAlarm.ctl: Machine is " + glAccelerator );
	isLhcData = glAccelerator == "LHC" ? true : false;
	// Initalize passive machine data
	coco = LhcVacEqpInitPassive( isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo );
	if( coco < 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo );
		return false;
	}
	DebugTN( "lhcVacMailAlarm.ctl: LhcVacEqpInitPassive() done" );

	// Initialzie active equipment information
	coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
			exceptionInfo );
	if( coco < 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo );
		return false;
	}
	DebugTN( "lhcVacMailAlarm.ctl: LhcVacEqpInitActive() done" );

	DebugTN( "lhcVacMailAlarm.ctl: InitStaticData() done" );
	return true;
}
/** ConnectToConfig
Purpose:
Connect to DPEs containing E-mail notification configuration

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToConfig( )
{
	string				configDp;
	dyn_string		exceptionInfo;
	dyn_errClass	err;

	VacEmailGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "lhcVacMailAlarm.ctl: FATAL: " + exceptionInfo );
		return false;
	}

	if( dpConnect( "ServerCb", configDp + ".server" ) < 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacMailAlarm.ctl: FATAL: dpConnect(" + configDp + ".server) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "ClientCb", configDp + ".client" ) < 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacMailAlarm.ctl: FATAL: dpConnect(" + configDp + ".client) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "SenderCb", configDp + ".sender" ) < 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacMailAlarm.ctl: FATAL: dpConnect(" + configDp + ".sender) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "RecipientsAllCb", configDp + ".recipientsAll" ) < 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacMailAlarm.ctl: FATAL: dpConnect(" + configDp + ".recipientsAll) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "SpecificCb", configDp + ".sectorsSpecific",
		configDp + ".recipientsSpecific" ) < 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacMailAlarm.ctl: FATAL: dpConnect(" + configDp + ".sectorsSpecific) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "DisableCb", configDp + ".sectorsOff",
		configDp + ".sectorsOffEndTime" ) < 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacMailAlarm.ctl: FATAL: dpConnect(" + configDp + ".sectorsOff) failed" );
		throwError( err );
		return false;
	}
	return true;
}
// Callbacks from DP containing general E-mail configuration
void ServerCb( string dpe, string newServer )
{
	smtpServer = newServer;
}
void ClientCb( string dpe, string newClient )
{
	client = newClient;
}
void SenderCb( string dpe, string newSender )
{
	sender = newSender;
}
void RecipientsAllCb( string dpe, dyn_string newRecipientsAll )
{
	recipientsAll = newRecipientsAll;
}
void SpecificCb( string dpe1, dyn_string newSectorsSpecific, string dpe2,
	dyn_string newRecipientsSpecific )
{
	sectorsSpecific = newSectorsSpecific;
	recipientsSpecific = newRecipientsSpecific;
}
void DisableCb( string dpe1, dyn_string newSectorsOff, string dpe2,
	dyn_time newSectorsOffEndTime )
{
	sectorsOff = newSectorsOff;
	sectorsOffEndTime = newSectorsOffEndTime;
}




