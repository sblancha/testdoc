#ifndef	PROFILEALLLHCINOUTWIDGET_H
#define	PROFILEALLLHCINOUTWIDGET_H

#include "VacCtlEwoUtilExport.h"

//	All LHC profile - for inner/outer beams

#include "DataEnum.h"
#include "VacEqpTypeMask.h"
#include "LhcRingSelection.h"

#include <qwidget.h>

#include "ProfileLhcView.h"
#include "ExtendedLabel.h"
class MainPart;

class ProfileAllLHCInOutWidget : public QWidget, public LhcRingSelection
{
	Q_OBJECT

public:
	VACCTLEWOUTIL_EXPORT ProfileAllLHCInOutWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);
	VACCTLEWOUTIL_EXPORT ~ProfileAllLHCInOutWidget();

	VACCTLEWOUTIL_EXPORT void analyzeTypes(VacEqpTypeMask &allMpMask);
	VACCTLEWOUTIL_EXPORT void applyMask(VacEqpTypeMask &newEqpMask);
	VACCTLEWOUTIL_EXPORT void setBeamLimits(int min, int max);
	VACCTLEWOUTIL_EXPORT void setLinLimits(int min, int max);

	VACCTLEWOUTIL_EXPORT bool getSectorRange(MainPart *pMainPart, Sector **ppFirst, Sector **ppLast);

	// Access
	VACCTLEWOUTIL_EXPORT inline int getLhcPartIndex(void) const { return lhcPartIndex; }
	VACCTLEWOUTIL_EXPORT void setLhcPartIndex(int index);
	VACCTLEWOUTIL_EXPORT inline VacEqpTypeMask & getAllEqpMask(void) { return allEqpMask; }
	VACCTLEWOUTIL_EXPORT inline VacEqpTypeMask & getEqpMask(void) { return eqpMask; }

signals:
	void profileDpDown(int button, int mode, int x, int y, int dummy, const char *dpName);
	void beamLimitsChanged(int min, int max);
	void linLimitsChanged(int min, int max);
	void mpLabelPressed(QMouseEvent *e, MainPart *pMainPart);

protected:

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// List of profile images
	QList<ProfileLhcView *>	images;

	// List of main part labels
	QList<ExtendedLabel *>		mpLabels;

	// Equipment type mask for all possible equipment types
	VacEqpTypeMask				allEqpMask;

	// Equipment type mask
	VacEqpTypeMask				eqpMask;

	// Pointer to main part to which popup menu is related
	MainPart					*pMainPart;

	// Index of LHC part to be shown
	int							lhcPartIndex;

	void buildInitialEqpMask(void);
	void buildLayout(void);
	bool buildSectorRange(MainPart *pMp, Sector **ppFirstSector, Sector **ppLastSector);

protected slots:
	void beamLimitsChange(int min, int max);
	void linLimitsChange(int min, int max);

private slots:
	void dpMouseDown(int button, int mode, int x, int y, const char *dpName);
	void labelPressed(QMouseEvent *e);
//	void mpMenu(int item);
};

#endif	// PROFILEALLLHCINOUTWIDGET_H
