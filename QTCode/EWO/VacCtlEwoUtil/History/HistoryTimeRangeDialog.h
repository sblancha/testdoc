#ifndef	HISTORYTIMERANGEDIALOG_H
#define	HISTORYTIMERANGEDIALOG_H

// Dialog for specifying free time range for history

#include <qdialog.h>
#include <qdatetimeedit.h>
#include <qpushbutton.h>
#include <qlabel.h>

class HistoryTimeRangeDialog : public QDialog
{
	Q_OBJECT

public:
	HistoryTimeRangeDialog(QWidget *parent, const QDateTime &start, const QDateTime &end, bool endTimeEditable);
	~HistoryTimeRangeDialog();

	void setAllowedRange(const QDateTime &start);

protected:
	// Control widgets
	QDateTimeEdit	*pStartTime;
	QDateTimeEdit	*pEndTime;
	QLabel			*pMinLabel;
	QPushButton		*pOkPb;
	QPushButton		*pCancelPb;
	bool				endTimeEditable;
	bool				settingValue;

	// Minimum allowed date (== history depth)
	QDateTime		minDateTime;

signals:
	void rangeChanged(const QDateTime &startTime, const QDateTime &endTime);

private slots:
	void startTimeChanged(const QDateTime &value);
	void endTimeChanged(const QDateTime &value);
	void okClicked(void);
	void cancelClicked(void);
};

#endif	// HISTORYTIMERANGEDIALOG_H
