#ifndef	EQPSELECTORCONTROL_H
#define	EQPSELECTORCONTROL_H

// Class providing functionality for selection sector(s) from lists

#include <QWidget>

#include "MainPart.h"
#include "Sector.h"
#include "Eqp.h"

#include "EqpUsageFilter.h"

#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>

class EqpSelectorControl : public QWidget
{
	Q_OBJECT

	////////////////////////////////////////////////////////////////////////////
	///////////////////// Properties exposed to PVSS panel /////////////////////
	////////////////////////////////////////////////////////////////////////////
	Q_PROPERTY(int functionalType READ getFunctionalType WRITE setFunctionalType);

public:
	EqpSelectorControl(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~EqpSelectorControl();

	int initContent(void);
	void setSelectedEqp(const QStringList &dpList);
	void getSelectedEqp(QStringList &result) const;
	bool isEmpty(void);
	void setEqpUsageFilter(QList<int> &funcTypes, QList<QByteArray> &attrNames, QList<bool> &reverseFlags);

	// Access
	int getFunctionalType(void) const { return functionalType; }
	void setFunctionalType(int type);

signals:
	void selectionChanged(void);

protected:
	// List of main parts
	QListWidget	*pMainPartList;

	// List of selected sectors
	QListWidget	*pSectorList;

	// List of all sectors
	QListWidget	*pAllEqpList;

	// List of all sectors
	QListWidget	*pEqpList;

	// Button to move selected sectors from left to right list
	QPushButton	*pRightPb;

	// Button to move selected sectors from right to left list
	QPushButton	*pLeftPb;

	// List of all selected main parts
	QList<MainPart *>	selectedMps;

	// List of all selected sectors
	QList<Sector *>		selectedSectors;

	// Filter on equipment usage (optional)
	EqpUsageFilter		usageFilter;

	// Functional type - devices of this type shall be selected
	int		functionalType;

	// Flag inidcating if selection is being set by code - so don't react on it
	bool		setting;

	void getSelectedEqp(QListWidget *pList, QList<Eqp *> &result) const;
	void getAllEqp(QListWidget *pList, QList<Eqp *> &result) const;
	void selectInList(QListWidget *pList, const QString &name);
	bool isListItemSelected(QListWidget *pList, const QString &name);
	int findEqpInList(QListWidget *pList, Eqp *pEqp);

	bool addEqpIfMatches(Eqp *pEqp);

private slots:

	void mpSelectionChanged(void);
	void mpSelectAll(void);
	void mpUnselectAll(void);
	void mpInvertSelection(void);

	void sectorSelectionChanged(void);
	void sectorSelectAll(void);
	void sectorUnselectAll(void);
	void sectorInvertSelection(void);

	void allEqpSelectionChanged(void);
	void allEqpSelectAll(void);
	void allEqpUnselectAll(void);
	void allEqpInvertSelection(void);

	void eqpSelectionChanged(void);
	void eqpSelectAll(void);
	void eqpUnselectAll(void);
	void eqpInvertSelection(void);


	void mpListContextMenuRequest(const QPoint &point);
	void sectorListContextMenuRequest(const QPoint &point);
	void allEqpListContextMenuRequest(const QPoint &point);
	void eqpListContextMenuRequest(const QPoint &point);
	void rightAllPbClicked(void);
	void rightPbClicked(void);
	void leftPbClicked(void);
	void leftAllPbClicked(void);
};


#endif	// EQPSELECTORCONTROL_H
