#ifndef FUNCTIONALTYPEPROCESS_BGI_6B01_H
#define FUNCTIONALTYPEPROCESS_BGI_6B01_H

// Special functional type for processes - supreclass for all processes classes

#include "FunctionalTypePROCESS.h"

class VACCTLEQPDATA_EXPORT FunctionalTypePROCESS_BGI_6B01 : public FunctionalTypePROCESS
{
public:
	FunctionalTypePROCESS_BGI_6B01(int type, const char *name, const char *description);
	virtual ~FunctionalTypePROCESS_BGI_6B01() {}

protected:
	virtual QString getStepResourceBase(void);
};

#endif	// FUNCTIONALTYPEPROCESS_BGI_6B01_H
