#ifndef	POLLGROUP_H
#define	POLLGROUP_H

#include "VacCtlImportExport.h"

//	Class holding poll group data

#include "FileParser.h"

class ImportPool;

class VACCTLIMPORT_EXPORT PollGroup : public FileParser
{
public:
	~PollGroup() {}

	static void setImportPool(ImportPool *pImportPool);
	static PollGroup *createFromFile(FILE *pFile, int &lineNo, const char *name,
		QStringList &errList);

	void dump(FILE *pFile);

	// Access
	inline const char *getName(void) { return name; }
	inline int getPeriod(void) { return period; }
	inline int getLineNo(void) { return lineNo; }

protected:
	PollGroup(const char *name, int lineNo);

	// Reference to ImportPool instance
	static ImportPool	*pPool;

	// Name of group
	char			*name;

	// Polling period
	int				period;

	// Line number in input file where this group was defined
	int				lineNo;

	
	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);
};

#endif	// POLLGROUP_H
