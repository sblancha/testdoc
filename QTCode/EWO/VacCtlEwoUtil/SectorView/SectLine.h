#ifndef	SECTLINE_H
#define	SECTLINE_H

#include "SynLine.h"
#include "EqpMsgCriteria.h"

class BeamLine;
class BeamLinePart;
class Sector;
class Eqp;

class SectLine : public SynLine  
{
	Q_OBJECT

public:
	virtual ~SectLine();

	static SectLine *create(QWidget *parent, BeamLine *pLine, Sector *pStartSector, Sector *pEndSector);

	// Real implementation of SynLine's methods
	virtual int build(QList<Sector *> &sectorUsage);
	virtual void buildSectorIcons(void);

protected:
	// Line is only created via create() static method
	SectLine();

	// Override SynLine's way of drawing sector labels
	virtual void drawSectorBorder(QPainter &painter, SynEqpItem *pItem);
};

#endif	// SECTLINE_H
