/**
* @brief Class implementation for widget Vacuum Gauge - Unified (VG_U)
* @see VacIcon
*/
#include "VacIconVG_U.h"
#include "VacMainView.h"
#include "EqpVG_U.h"
#include <QPainter>

VacIconVG_U::VacIconVG_U(QWidget *parent, Qt::WindowFlags f) :
VacIcon(parent, f) {
	pEqp = NULL;
	setFixedSize(35, 35);
}

VacIconVG_U::~VacIconVG_U() {
	disconnect();
}

/**
@brief FUNCTION Calculate geometry for symbol areas rectangles for current direction and beamDirection.
*/
VacIconGeometry VacIconVG_U::getGeometry(const VacEqpTypeMask & /* mask */) {
	return VacIconGeometry(35, 35, 17, 40, 10);
}

VacIconContainer::Direction VacIconVG_U::getSlaveDirection(void) const {
	VacIconContainer::Direction result = VacIconContainer::Up;
	switch (direction) {
	case VacIconContainer::Up:
		result = VacIconContainer::Up;
		break;
	case VacIconContainer::Down:
		result = VacIconContainer::Down;
		break;
	case VacIconContainer::Left:
		result = VacIconContainer::Right;
		break;
	case VacIconContainer::Right:
		result = VacIconContainer::Left;
		break;
	}
	return result;
}

/**
@brief FUNCTION Set reference to equipment for this icon
@param[in]   pEqp	Pointer to device to be used by this icon
*/
void VacIconVG_U::setEqp(Eqp *pEqp) {
	disconnect();
	if (pEqp) {
		Q_ASSERT_X(pEqp->inherits("EqpVG_U"), "VacIconVG_U::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVG_U *)pEqp;
	}
	forceRedraw();
}

/**
@brief FUNCTION Set new data acquisition mode for this icon
@param[in]   newMode	New data aqn mode (online, replay...)
*/
void VacIconVG_U::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/**
@brief FUNCTION Connect to DPEs of device, return true if done
*/
bool VacIconVG_U::connect(void) {
	if (connected || (!pEqp)) {
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("objectSt");
	dpes.append("PR");
	if (pEqp->inherits("EqpVG_U")) {
		if (((EqpVG_U *)pEqp)->isHaveBlockedOff()) {
			dpes.append("BlockedOFF");
		}
	}
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/**
@brief FUNCTION Disconnect DPEs of device, return true if done
*/
bool VacIconVG_U::disconnect(void) {
	bool result = false;
	if (connected && pEqp) {
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/**
@brief FUNCTION Calculate text and rectangle for tooltip of this icon, return true if tooltip shown
@param[in]  pos		Position of mouse pointer WITHIN this widget
@param[out]	text	Variable where this method shall write tooltip text to be shown
@param[out]	rect	Variable where this method shall write rectangle for visibility	of returned tooltip: tooltip will be hidden when mouse pointer left
*/
bool VacIconVG_U::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect) {
	rect = this->rect();
	if (pEqp && connected) {
		pEqp->getToolTipString(text, mode);
	}
	else {
		text = "Not connected";
	}
	return true;
}

/**
@brief FUNCTION Icon drawing implementation
@param[in]  pEvent	Pointer to paint event
*/
void VacIconVG_U::paintEvent(QPaintEvent * /* pEvent */) {
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if (pEqp) {
		ctlStatus = pEqp->getCtlStatus(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if (pEqp) {
		if (pEqp->isSelected()) {
			drawSelection(painter);
		}
	}
	switch (direction) {
	case VacIconContainer::Up:	// On top of Line - No rotation
		painter.save();
		painter.translate(7, 12); // (35-21)/2 = 7 ; 12 = nb pixel to write pressure on top '9E-12'
		//Pipe
		painter.fillRect(10 - pipeWidth / 2, 18, pipeWidth ? pipeWidth : 1, 6, pipeColor);
		//Gauge image
		drawImage(painter, ctlStatus);
		painter.restore();
		break;
	case VacIconContainer::Down:	// Bottom the line - 180 degrees
		painter.save();
		painter.translate(7, 0); // (35-21)/2 = 7 ; 0 pressure writen on bottom '9E-12'
		//Pipe
		painter.fillRect(10 - pipeWidth / 2, 0, pipeWidth ? pipeWidth : 1, 6, pipeColor);
		//Gauge image
		drawImage(painter, ctlStatus);
		painter.restore();
		break;
	case VacIconContainer::Left:	// 90 degrees clockwise
		painter.save();
		painter.translate(0, 12); // 0 image stuck to the left ; 12 = nb pixel to write pressure on top '9E-12'
		painter.fillRect(0, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		//Gauge image
		drawImage(painter, ctlStatus);
		painter.restore();
		break;
	case VacIconContainer::Right:	// 90 degrees conuterclockwise
		painter.save();
		painter.translate(14, 12); // (35-21) image stuck to the Right ; 12 = nb pixel to write pressure on top '9E-12'
		painter.fillRect(18, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		//Gauge image
		drawImage(painter, ctlStatus);
		painter.restore();
		break;
	}
}

/**
@brief FUNCTION PROTECTED icon drawing
@param[in]  painter	Painter object
@param[in]  ctlStatus	Status of the widget (connected, not connected...)
*/
void VacIconVG_U::drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus) {
	setPainterForMainImage(painter, ctlStatus);
	// Filled circle
	QPen pen = painter.pen();
	painter.setPen(Qt::NoPen);
	painter.drawPie(1, 1, 19, 19, 0, 360 * 16);
	painter.setPen(pen);
	painter.drawEllipse(1, 1, 19, 19);
	// type-specific part of image
	drawSpecific(painter);
}

void VacIconVG_U::setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus) {
	QColor fillColor(COLOR_GP_INVALID);
	QColor lineColor = Qt::black;
	switch (ctlStatus) {
	case Eqp::NotControl:
		fillColor.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		fillColor.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if (pEqp) {
			pEqp->getMainColor(fillColor, mode);
		}
		else {
			fillColor.setRgb(COLOR_GP_NOEQP);
		}
		break;
	}
	painter.setBrush(fillColor);
	QPen pen(lineColor, lineColor == Qt::black ? 1 : 2);
	painter.setPen(pen);
}

void VacIconVG_U::drawInterlockSrcArrow(QPainter &painter, bool before, bool after)
{
	if (!(before || after))
	{
		return;
	}
	painter.resetTransform();
	painter.setPen(VacMainView::getSynPassiveLineColor());
	int start, end, pos;
	switch (direction)
	{
	case VacIconContainer::Down:
		start = x();
		end = start + width();
		pos = y() + height() + 5;
		painter.drawLine(start, pos, end, pos);
		if (before)
		{
			painter.drawLine(start, pos, start + 3, pos - 3);
			painter.drawLine(start, pos, start + 3, pos + 3);
		}
		if (after)
		{
			painter.drawLine(end, pos, end - 3, pos - 3);
			painter.drawLine(end, pos, end - 3, pos + 3);
		}
		break;
	case VacIconContainer::Up:	// 180 degrees
		start = x();
		end = start + width();
		pos = y() - 5;
		painter.drawLine(start, pos, end, pos);
		if (before)
		{
			painter.drawLine(start, pos, start + 3, pos - 3);
			painter.drawLine(start, pos, start + 3, pos + 3);
		}
		if (after)
		{
			painter.drawLine(end, pos, end - 3, pos - 3);
			painter.drawLine(end, pos, end - 3, pos + 3);
		}
		break;
	case VacIconContainer::Left:	// 90 degrees clockwise
		start = y();
		end = start + height();
		pos = x() + width() + 5;
		painter.drawLine(pos, start, pos, end);
		if (before)
		{
			painter.drawLine(pos, start, pos + 3, start + 3);
			painter.drawLine(pos, start, pos - 3, start + 3);
		}
		if (after)
		{
			painter.drawLine(pos, end, pos + 3, end - 3);
			painter.drawLine(pos, end, pos - 3, end - 3);
		}
		break;
	case VacIconContainer::Right:	// 90 degrees conuterclockwise
		start = y();
		end = start + height();
		pos = x() - 5;
		painter.drawLine(pos, start, pos, end);
		if (before)
		{
			painter.drawLine(pos, start, pos + 3, start + 3);
			painter.drawLine(pos, start, pos - 3, start + 3);
		}
		if (after)
		{
			painter.drawLine(pos, end, pos + 3, end - 3);
			painter.drawLine(pos, end, pos - 3, end - 3);
		}
		break;
	}
}

void VacIconVG_U::drawPressureValue(QPainter &painter, QString &sPr)
{
	int xPosition = -6; // due to translate 1-7
	int yPosition = -1; // 1-12(translate)+10(textOriginBottom-left)
	painter.setPen(Qt::black);

	if (direction == VacIconContainer::Down) {
		yPosition = 32; // 22-0(translate)+10(textOriginBottom-left)
	}
	else if (direction == VacIconContainer::Left) {
		xPosition = 1;
	}
	else if (direction == VacIconContainer::Right) {
		xPosition = -13; // due to translate 1-14
	}
	painter.drawText(xPosition, yPosition, sPr); 
}
