#ifndef FUNCTIONALTYPEVPGF_H
#define FUNCTIONALTYPEVPGF_H

// Special functional type for fixed pumping groups - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVPGF : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		On = 1,
		Off = 2,
		VvrOpen = 3,
		VvrClosed = 4,
		TmpOn = 5,
		TmpOff = 6
	};

	FunctionalTypeVPGF(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVPGF() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVPGF_H
