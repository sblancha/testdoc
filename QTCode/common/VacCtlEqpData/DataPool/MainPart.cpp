#include "MainPart.h"
#include "DataPool.h"

#include "Eqp.h"

#include "PlatformDef.h"

#include <QStringList>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

///////////////////////////////////////////////////////////////////////////////
//// Construction/Destruction
///////////////////////////////////////////////////////////////////////////////

MainPart::MainPart(const char *name)
{
	pvssName = NULL;
	start = end = 0.0;
	pMainLine = NULL;
	vacTypeMask = 0;
	this->name = DataPool::getInstance().addString(name);
}

MainPart::~MainPart()
{
}

/*
**
** FUNCTION
**		Create new MainPart instance from data read in file.
**
** PARAMETERS
**		pFile		- Pointer to file being read.
**		lineNo		- Line number in file.
**		tokens		- Pointer to array of tokens in file.
**		nTokens		- Number of tokens.
**		errList		- Viariable where error message shall be added
**						in case of errors.
**
** RETURNS
**		Pointer to new MainPart instance created from data in file;
**		NULL in case of error.
**
** CAUTIONS
**		It is responsability of caller to destroy returned instance using 'delete'
**		operator.
**
*/
MainPart *MainPart::createFromFile(FILE *pFile, int &lineNo,
	char **tokens, int nTokens, QStringList &errList)
{
	MainPart	*pNew;

	if(nTokens != 2)
	{
		errList.append(QString("Line %1: bad format for <%2>").arg(lineNo).arg(tokens[0]));
		return NULL;
	}
	pNew = new MainPart(tokens[1]);
	switch(pNew->parseFile(pFile, lineNo, errList))
	{
	case 0:
		return pNew;
	case 1:
		errList.append(QString("Line %1: incomplete main part <%2>").arg(lineNo).arg(pNew->name));
		break;
	}
	delete pNew;
	return NULL;
}

/*
**
** FUNCTION
**		Process tokens of single line read from input file.
**
** PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
** RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
** CAUTIONS
**		None
*/
int MainPart::processLineTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	const QString & /* longValue */, QStringList &errList)
{
	// PVSS name
	if(!strcasecmp(tokens[0], "PvssName"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "MainPart::ProcessLineTokens");
		if(pvssName)
		{
			PARSING_ERR2("Line %d: multiple PVSS names for <%s>", lineNo, name);
		}
		pvssName = DataPool::getInstance().addString(tokens[1]);
		return 1;
	}
	// Start location
	else if(!strcasecmp(tokens[0], "Start"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "MainPart::ProcessLineTokens" );
		if(sscanf(tokens[1], "%f", &start) != 1)
		{
			PARSING_ERR2("Line %d: bad start location format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	// End location
	else if(!strcasecmp(tokens[0], "End"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "MainPart::ProcessLineTokens");
		if(sscanf(tokens[1], "%f", &end) != 1)
		{
			PARSING_ERR2("Line %d: bad end location format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	// EndMainPart
	else if(!strcasecmp(tokens[0], "EndMainPart"))
	{
		return 0;
	}
	PARSING_ERR2("Line %d: unexpected <%s>", lineNo, tokens[0]);
}
