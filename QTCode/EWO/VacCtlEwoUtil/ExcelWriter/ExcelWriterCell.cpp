//	Implementation of ExcelWriterCell class
/////////////////////////////////////////////////////////////////////////////////

#include "ExcelWriterCell.h"

#include <QTextDocument>

#include <errno.h>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

ExcelWriterCell::ExcelWriterCell(int column) : color(Qt::white)
{
	this->column = column;
	styleIndex = -1;	// No style
	format = String;
}

ExcelWriterCell::~ExcelWriterCell()
{
}

/*
**	FUNCTION
**		Set date + time to be shown in cell
**
**	ARGUMENTS
**		dateTime	- Date + time value to be shown
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterCell::setDateTime(const QDateTime &dateTime)
{
	string = dateTime.toString("yyyy-MM-ddThh:mm:ss.000");
}

/*
**	FUNCTION
**		Set date + time with milliseconds to be shown in cell
**
**	ARGUMENTS
**		dateTime	- Date + time value to be shown
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterCell::setDateTimeWithMsec(const QDateTime &dateTime)
{
	string = dateTime.toString("yyyy-MM-ddThh:mm:ss.zzz");
}

/*
**	FUNCTION
**		Set floating point value to be shown in cell
**
**	ARGUMENTS
**		value	- Value to be shown
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterCell::setValue(float value)
{
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%8.2E", value);
#else
	sprintf(buf, "%8.2E", value);
#endif
	string = buf;
}

/*
**	FUNCTION
**		Set integer value to be shown in cell
**
**	ARGUMENTS
**		value	- Value to be shown
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterCell::setIntValue(int value)
{
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d", value);
#else
	sprintf(buf, "%d", value);
#endif
	string = buf;
}

/*
**	FUNCTION
**		Write cell content to output file, opened for writing
**
**	ARGUMENTS
**		pFile	- Pointer to file, opened for writing
**
**	RETURNS
**		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriterCell::write(FILE *pFile)
{
	if(!pFile)
	{
		return QString();
	}

	const char *type = NULL;
	switch(format)
	{
	case String:
		type = "String";
		break;
	case DateTime:
	case DateTimeWithMsec:
		type = "DateTime";
		break;
	case Float:
	case Integer:
		type = "Number";
		break;
	}
	int coco = 0;
	if(styleIndex < 0)
	{
		coco = fprintf(pFile, "    <Cell ss:Index=\"%d\"><Data ss:Type=\"%s\">%s</Data>",
			column, type, string.isEmpty() ? "" :  string.toHtmlEscaped().toUtf8().constData());
	}
	else
	{
		coco = fprintf(pFile, "    <Cell ss:Index=\"%d\" ss:StyleID=\"cellStyle%d\"><Data ss:Type=\"%s\">%s</Data>",
			column, styleIndex, type, string.isEmpty() ? "" : string.toHtmlEscaped().toUtf8().constData());
	}
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	if(!comment.isEmpty())
	{
		coco = fprintf(pFile, "<Comment\n\
      ss:Author=\"VacPVSS\"><ss:Data\n\
      xmlns=\"http://www.w3.org/TR/REC-html140\"><Font\n\
      html:Face=\"Tahoma\" x:CharSet=\"204\" html:Size=\"8\" html:Color=\"#000000\">%s</Font></ss:Data></Comment>",
			 comment.toHtmlEscaped().toUtf8().constData());
	}
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}
	coco = fprintf(pFile, "</Cell >\n");
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}	
	return QString();
}

/*
**	FUNCTION
**		Write cell content to output CSV file, opened for writing
**
**	ARGUMENTS
**		pFile	- Pointer to file, opened for writing
**
**	RETURNS
**		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriterCell::writeCsv(FILE *pFile, bool firstCell)
{
	if(!pFile)
	{
		return QString("File not open");
	}

	int coco = 0;
	switch(format)
	{
	case DateTime:
	case DateTimeWithMsec:
		coco = fprintf(pFile, firstCell ? "%s" : "\t%s", string.isEmpty() ? "" : string.replace("T", " ").toLatin1().constData());
		break;
	default:
		coco = fprintf(pFile, firstCell ? "%s" : "\t%s", string.isEmpty() ? "" : string.toLatin1().constData());
		break;
	}
	if(coco < 0)
	{
		return QString("File write failed: errno %1").arg(errno);
	}	
	return QString();
}

