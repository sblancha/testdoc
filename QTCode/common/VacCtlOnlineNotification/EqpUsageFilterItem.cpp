//	Implementation of EqpUsageFilterItem class
////////////////////////////////////////////////////////////////////////////////

#include "EqpUsageFilterItem.h"

EqpUsageFilterItem::EqpUsageFilterItem(int funcTypeId, const QByteArray &eqpAttrName, bool critReverse) : attrName(eqpAttrName)
{
	typeId = funcTypeId;
	reverse = critReverse;
}

EqpUsageFilterItem::~EqpUsageFilterItem()
{
}

