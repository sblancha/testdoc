//	Implementation of HistoryDataRange class
/////////////////////////////////////////////////////////////////////////////////

#include "HistoryDataRange.h"

#include "TimeAxis.h"
#include "VerticalAxis.h"
#include "TimeAxisWidget.h"
#include "VerticalAxisWidget.h"
#include "ScreenPoint.h"

#include "HistoryProcessor.h"

#include "VacMainView.h"
#include "DebugCtl.h"

#include <qpainter.h>
#include <qvariant.h>

#include <stdlib.h>

int HistoryDataRange::lastRangeId = 0;

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

HistoryDataRange::HistoryDataRange()
{
	id = ++lastRangeId;
	filterValuesPerPixel = 0;
	start = QDateTime::currentDateTime().addYears(1).toMSecsSinceEpoch();	// Far in the future
	end = QDateTime::currentDateTime().addYears(-10).toMSecsSinceEpoch();	// Far in the past
	requestSent = valueAdded = false;
	filterType = FilterTypeMinMax;
}

// Special constructor for offline ranges
HistoryDataRange::HistoryDataRange(const QDateTime &start, const QDateTime &end)
{
	id = ++lastRangeId;
	this->start = start.toMSecsSinceEpoch();
	this->end = end.toMSecsSinceEpoch();

/*
printf("HistoryDataRange::HistoryDataRange(%d from %s to %s)\n", id,
	start.toString("dd-MM-yyyy hh:mm:ss.zzz").ascii(), end.toString("dd-MM-yyyy hh:mm:ss.zzz").ascii());
fflush(stdout);
*/
	filterValuesPerPixel = 0;
	requestSent = valueAdded = false;
	filterType = FilterTypeMinMax;
}


HistoryDataRange::~HistoryDataRange()
{
}

void HistoryDataRange::setFilterType(FilterType newType)
{
	if(filterType == newType)
	{
		return;
	}
	filteredValuesList.clear();
	filteredTimesList.clear();
	filterType = newType;
}

/*
**	FUNCTION
**		Add online value to this value tange
**
**	PARAMETERS
**		value	- Value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDataRange::addOnlineValue(float value, const QDateTime &timeStamp)
{
	valuesList.append(value);
	timesList.append(timeStamp.toMSecsSinceEpoch());
	valueAdded = true;
}

/*
**	FUNCTION
**		Add offline value to this value range
**
**	PARAMETERS
**		requestId	- ID of request that caused data retrieval from archive
**		time		- Timestamp for value
**		value		- Value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDataRange::addValue(int requestId, const QDateTime &time, float value)
{
/*
printf("HistoryDataRange::addValue(%d from %s to %s): request %d time %s requestSent %s\n", id,
	start.toString("dd-MM-yyyy hh:mm:ss.zzz").ascii(), end.toString("dd-MM-yyyy hh:mm:ss.zzz").ascii(),
	requestId, time.toString("dd-MM-yyyy hh:mm:ss.zzz").ascii(),
	(requestSent ? "YES" : "NO"));
fflush(stdout);
*/
	// It is assumed that after request for this range has been sent - data will
	// arrive in time increasing order. But while request was not sent - data may
	// arrive in any order: they are just bonus values of other ranges
	qint64 newTime = time.toMSecsSinceEpoch();
	if(requestSent)
	{
		if(requestId != id)
		{
			// If request for this range was sent - we are not interesting
			// in 'tails' of other ranges.
			return;
		}
		if(!timesList.isEmpty())
		{
			if(timesList.last() >= newTime)
			{
				return;
			}
		}
		valuesList.append(value);
		timesList.append(newTime);
		valueAdded = true;
	}
	else
	{
		if(valuesList.isEmpty())
		{
			valuesList.append(value);
			timesList.append(newTime);
		}
		else
		{
			bool valueInserted = false;;
			for(int insertIdx = 0 ; insertIdx < timesList.count() ; insertIdx++)
			{
				if(timesList.at(insertIdx) > newTime)
				{
					valuesList.insert(insertIdx, value);
					timesList.insert(insertIdx, newTime);
					valueInserted = true;
					break;
				}
				else if(timesList.at(insertIdx) == newTime)
				{
					return;
				}
			}
			if(!valueInserted)	// Not found place to insert, add at the end
			{
				valuesList.append(value);
				timesList.append(newTime);
			}
			valueAdded = true;
		}
			
	}
/*
printf("HistoryDataRange::addValue(%d): time %d nValues %d\n", requestId, timeT, nValues);
fflush(stdout);
*/
}

void HistoryDataRange::setRequestSent(bool flag)
{
	if(requestSent == flag)
	{
		return;
	}
	if(!requestSent)
	{
		// Throw away existing data which could come here as bonus for other ranges
		filteredValuesList.clear();
		filteredTimesList.clear();
		valuesList.clear();
		timesList.clear();
	}
	requestSent = flag;
}


/*
**	FUNCTION
**		Add all values in give time range to class that will export them to Excel file
**
**	PARAMETERS
**		pEqp		- Pointer to device these data belong to, the value is not interpreted
**						by this method - just passed as is
**		startTime	- Start time of interval to be exported
**		endTime		- End time of interval to be exported
**		processor	- Instance where values shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDataRange::addExportValues(Eqp *pEqp, const QString &bitDpe, int bitNbr, const QString &bitName, const QDateTime &startTime, const QDateTime &endTime, HistoryProcessor &processor)
{
	qint64 startTimeAsMsec = startTime.toMSecsSinceEpoch(),
		endTimeAsMsec = endTime.toMSecsSinceEpoch();
	QDateTime valueTime(QDateTime::currentDateTime());
	for (int n = 0; n < timesList.count(); n++)
	{
		if(startTimeAsMsec >= timesList.at(n))
		{
			processor.setPreviousValue(pEqp, bitDpe, bitNbr, bitName, startTime, valuesList.at(n));
			continue;
		}
		if(timesList.at(n) > endTimeAsMsec)
		{
			return;
		}
		valueTime.setMSecsSinceEpoch(timesList.at(n));
		processor.addValue(pEqp, bitDpe, bitNbr, bitName, valueTime, valuesList.at(n));
	}
}

void HistoryDataRange::setLastAsPreviousExportValue(Eqp *pEqp, const QString &bitDpe, int bitNbr, const QString &bitName, const QDateTime &startTime, HistoryProcessor &processor) {
	if (!valuesList.isEmpty()) {
		processor.setPreviousValue(pEqp, bitDpe, bitNbr, bitName, startTime, valuesList.last());
	}
}

/*
**	FUNCTION
**		Add all values in give time range to class that will export them to Excel file
**
**	PARAMETERS
**		pEqp		- Pointer to device these data belong to, the value is not interpreted
**						by this method - just passed as is
**		dpe			- DPE name of this device
**		startTime	- Start time of interval to be exported
**		endTime		- End time of interval to be exported
**		processor	- Instance where values shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDataRange::addExportValues(Eqp *pEqp, const QString &dpe, const QDateTime &startTime, const QDateTime &endTime, HistoryProcessor &processor)
{
	qint64 startTimeAsMsec = startTime.toMSecsSinceEpoch(),
		endTimeAsMsec = endTime.toMSecsSinceEpoch();
	QDateTime valueTime(QDateTime::currentDateTime());
	for (int n = 0; n < timesList.count(); n++)
	{
		if(timesList.at(n) < startTimeAsMsec)
		{
			processor.setPreviousValue(pEqp, dpe, startTime, valuesList.at(n));
			continue;
		}
		if(timesList.at(n) > endTimeAsMsec)
		{
			return;
		}
		valueTime.setMSecsSinceEpoch(timesList.at(n));
		processor.addValue(pEqp, dpe, valueTime, valuesList.at(n));
	}
}

void HistoryDataRange::setLastAsPreviousExportValue(Eqp *pEqp, const QString &dpe, const QDateTime &startTime, HistoryProcessor &processor) {
	if (!valuesList.isEmpty()) {
		processor.setPreviousValue(pEqp, dpe, startTime, valuesList.last());
	}
}



/*
**	FUNCTION
**		Check if some timestamp(s) is present in both this and next range, if found -
**		remove timestamp(s) from THIS range
**
**	PARAMETERS
**		pNext	- Pointer to next range in chain
**
**	RETURNS
**		true	- If uplicates found
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool HistoryDataRange::checkForDuplicates(HistoryDataRange *pNext)
{
	bool	result = false;
	for(int n = timesList.count() - 1 ; n >= 0 ; n--)
	{
		bool found = false;
		for(int i = 0 ; i < pNext->timesList.count() ; i++)
		{
			if(pNext->timesList.at(i) == timesList.at(n))
			{
				found = true;
				break;
			}
			else if(pNext->timesList.at(i) > timesList.at(n))
			{
				break;
			}
		}
		if(found)
		{
			result = true;
			timesList.removeLast();
			valuesList.removeLast();
		}
		else
		{
			break;
		}
	}
	return result;
}

/*
**	FUNCTION
**		Find min and max over all values in range
**
**	PARAMETERS
**		min		- Variable where minimum value will be written
**		max		- Variable where max value will be written
**
**	RETURNS
**		Number of values checked
**
**	CAUTIONS
**		Only values in range HISTORY_LOW_VALUE...HISTORY_HIGH_VALUE
**		are taken into account
*/
int HistoryDataRange::findLimits(float &min, float &max)
{
	min = HISTORY_HIGH_VALUE;
	max = HISTORY_LOW_VALUE;
	int count = 0;
	for(int n = 0 ; n < valuesList.count() ; n++)
	{
		float value = valuesList.at(n);
		if((HISTORY_LOW_VALUE < value) && (value < HISTORY_HIGH_VALUE))
		{
			count++;
			if(value < min)
			{
				min = value;
			}
			if(value > max)
			{
				max = value;
			}
		}
	}
	return count;
}

/*
**	FUNCTION
**		Find value in this range at given time (or before that time)
**
**	PARAMETERS
**		time	- Time when value is needed
**		value	- Variable where found value will be returned
**
**	RETURNS
**		true	- If value was found;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool HistoryDataRange::getValueAt(const QDateTime &sec, float &value)
{
	if(timesList.isEmpty())
	{
		return false;
	}
	qint64 timeAsMsec = sec.toMSecsSinceEpoch();
	if (timesList.last() <= timeAsMsec)
	{
		value = valuesList.last();
		return true;
	}
	if(timesList.first() > timeAsMsec)
	{
		return false;
	}

	int	high = timesList.count(), low = -1, probe;
	while((high - low) > 1)
	{
		probe = (low + high) >> 1;
		if(timesList.at(probe) > timeAsMsec)
		{
			high = probe;
		}
		else
		{
			low = probe;
		}
	}
	if(low < 0)
	{
		value = valuesList.first();
		return true;
	}
	if(timesList.at(low) == timeAsMsec)
	{
		value = valuesList.at(low);
	}
	else
	{
		for( ; low >= 0 ; low--)
		{
			if(timesList.at(low) < timeAsMsec)
			{
				value = valuesList.at(low);
				break;
			}
		}
	}
	return true;
}

/*
**	FUNCTION
**		Draw all data in this range
**
**	PARAMETERS
**		painter				- Painter used for drawing
**		pTimeAxis			- Pointer to time (horizontal) axis
**		pValueAxis			- Pointer to value (vertical) axis
**		mainColor			- Main color used for drawing, all data are drawn with
**								this color, except for negative values (if negative values
**								are allowed for device)
**		lineWidth			- Width of line used for drawing
**		isNegativeAllowed	- Flag indicating if negative values are allowed for device
**		sp					- Parameters of last drawn point
**
**	RETURNS
**		true	- if range scan shall continue
**		false	- if range scan shall be stopped
**
**	CAUTIONS
**		None
*/
bool HistoryDataRange::draw(QPainter &painter, TimeAxis *pTimeAxis, VerticalAxis *pValueAxis,
	const QColor &mainColor, int lineWidth, bool isNegativeAllowed, ScreenPoint &sp)
{
	int	x, y;
	if(timesList.isEmpty())
	{
		return false;
	}
	pValueAxis->valueToScreen(valuesList.last(), y);
	if(pTimeAxis->timeAsMsecToScreen(timesList.last(), x) == TimeAxis::BeforeMin)
	{
		sp.setX(x);
		sp.setY(y);
		sp.setPrevExists(true);
		return false;
	}

	QList<float> *pUseValuesList = NULL;
	QList<qint64> *pUseTimesList = NULL;
	chooseDataToDraw(pTimeAxis, &pUseValuesList, &pUseTimesList);

	for(int n = 0 ; n < pUseTimesList->count() ; n++)
	{
		float value = pUseValuesList->at(n);
		if(isNegativeAllowed)
		{
			if(value < 0)
			{
				value = - value;
			}
		}
		pValueAxis->valueToScreen(value, y);
		int timeRange = pTimeAxis->timeAsMsecToScreen(pUseTimesList->at(n), x);
		if(timeRange == TimeAxis::BeforeMin)
		{
			sp.setX(x);
			sp.setY(y);
			sp.setPrevExists(true);
			continue;
		}
		if(sp.isPrevExists())
		{
			if(!sp.isDrawing())
			{
				if((sp.getTimeRange() == TimeAxis::BeforeMin) &&
					(timeRange == TimeAxis::AfterMax))	// No times in scale range
				{
					QPen pen(sp.isNegative() ? VacMainView::getHistoryNegativeColor() : mainColor,
						lineWidth);
					painter.setPen(pen);
					painter.drawLine(sp.getX(), sp.getY(), x, sp.getY());
					return true;
				}
				sp.setDrawing(true);
			}
			if(x != sp.getX())
			{
				QPen pen(sp.isNegative() ? VacMainView::getHistoryNegativeColor() : mainColor,
					lineWidth);
				painter.setPen(pen);
				painter.drawLine(sp.getX(), sp.getY(), x, sp.getY());
			}
			// Draw vertical line if Y coordinate is different from previous Y AND
			// next value is within displayed time range
			if((y != sp.getY()) &&
				((timeRange == TimeAxis::BeforeMin) || (timeRange == TimeAxis::WithinRange)))
			{
				QPen pen(sp.isNegative() ? VacMainView::getHistoryNegativeColor() : mainColor,
					lineWidth);
				painter.setPen(pen);
				painter.drawLine(x, sp.getY(), x, y);
			}
		}
		else
		{
			sp.setPrevExists(true);
		}
		if(timeRange == TimeAxis::AfterMax)
		{
			return true;
		}
		sp.setTimeRange(timeRange);
		sp.setX(x);
		sp.setY(y);
	}
	return false;
}

/*
**	FUNCTION
**		Draw all data in this range - the same as above, but uses another classes
**		for both axies
**
**	PARAMETERS
**		painter				- Painter used for drawing
**		pTimeAxis			- Pointer to time (horizontal) axis
**		pValueAxis			- Pointer to value (vertical) axis
**		mainColor			- Main color used for drawing, all data are drawn with
**								this color, except for negative values (if negative values
**								are allowed for device)
**		lineWidth			- Width of line used for drawing
**		isNegativeAllowed	- Flag indicating if negative values are allowed for device
**		sp					- Parameters of last drawn point
**
**	RETURNS
**		true	- if range scan shall continue
**		false	- if range scan shall be stopped
**
**	CAUTIONS
**		None
*/
bool HistoryDataRange::draw(QPainter &painter, TimeAxisWidget *pTimeAxis, VerticalAxisWidget *pValueAxis,
	const QColor &mainColor, int lineWidth, bool isNegativeAllowed, ScreenPoint &sp)
{
	int	x, y;
	if(valuesList.isEmpty())
	{
		return false;
	}
	pValueAxis->valueToScreen(valuesList.last(), y);
	if(pTimeAxis->timeAsMsecToScreen(timesList.last(), x) == TimeAxisWidget::BeforeMin)
	{
		sp.setX(x);
		sp.setY(y);
		sp.setPrevExists(true);
		return false;
	}
	QList<float> *pUseValuesList = NULL;
	QList<qint64> *pUseTimesList = NULL;
	chooseDataToDraw(pTimeAxis, &pUseValuesList, &pUseTimesList);
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("%d: drawing %d values", id, pUseValuesList->count());
#else
		printf("%d: drawing %d values", id, pUseValuesList->count());
		fflush(stdout);
#endif
	}

	for(int n = 0 ; n < pUseTimesList->count() ; n++)
	{
		float value = pUseValuesList->at(n);
		if(isNegativeAllowed)
		{
			if(value < 0)
			{
				value = - value;
			}
		}
		pValueAxis->valueToScreen(value, y);
		int timeRange = pTimeAxis->timeAsMsecToScreen(pUseTimesList->at(n), x);
		if(timeRange == TimeAxis::BeforeMin)
		{
			sp.setX(x);
			sp.setY(y);
			sp.setPrevExists(true);
			continue;
		}
		if(sp.isPrevExists())
		{
			if(!sp.isDrawing())
			{
				if((sp.getTimeRange() == TimeAxis::BeforeMin) &&
					(timeRange == TimeAxis::AfterMax))	// No times in scale range
				{
					QPen pen(sp.isNegative() ? VacMainView::getHistoryNegativeColor() : mainColor,
						lineWidth);
					painter.setPen(pen);
					painter.drawLine(sp.getX(), sp.getY(), x, sp.getY());
					return true;
				}
				sp.setDrawing(true);
			}
			if(x != sp.getX())
			{
				QPen pen(sp.isNegative() ? VacMainView::getHistoryNegativeColor() : mainColor,
					lineWidth);
				painter.setPen(pen);
				painter.drawLine(sp.getX(), sp.getY(), x, sp.getY());
			}
			// Draw vertical line if Y coordinate is different from previous Y AND
			// next value is within displayed time range
			if((y != sp.getY()) &&
				((timeRange == TimeAxis::BeforeMin) || (timeRange == TimeAxis::WithinRange)))
			{
				QPen pen(sp.isNegative() ? VacMainView::getHistoryNegativeColor() : mainColor,
					lineWidth);
				painter.setPen(pen);
				painter.drawLine(x, sp.getY(), x, y);
			}
		}
		else
		{
			sp.setPrevExists(true);
		}
		if(timeRange == TimeAxisWidget::AfterMax)
		{
			return true;
		}
		sp.setTimeRange(timeRange);
		sp.setX(x);
		sp.setY(y);
	}
	return false;
}

void HistoryDataRange::chooseDataToDraw(TimeAxis *pTimeAxis, QList<float> **ppUseValuesList, QList<qint64> **ppUseTimesList)
{
	*ppUseValuesList = &valuesList;
	*ppUseTimesList = &timesList;
	if(timesList.isEmpty())
	{
		return;
	}

	// Parameters of time axis
	int scaleRangePixels = pTimeAxis->getScaleRight() - pTimeAxis->getScaleLeft();
	if(scaleRangePixels <= 0)
	{
		return;
	}
	float scaleRangeSec = pTimeAxis->getMin().secsTo(pTimeAxis->getMax());
	float scaleSecPerPixel = scaleRangeSec / (float)scaleRangePixels;
	choseDataToDraw(scaleSecPerPixel, ppUseValuesList, ppUseTimesList);
}



void HistoryDataRange::chooseDataToDraw(TimeAxisWidget *pTimeAxis, QList<float> **ppUseValuesList, QList<qint64> **ppUseTimesList)
{
	*ppUseValuesList = &valuesList;
	*ppUseTimesList = &timesList;
	if(timesList.isEmpty())
	{
		return;
	}

	// Parameters of time axis
	int scaleRangePixels = pTimeAxis->getScaleRight() - pTimeAxis->getScaleLeft();
	if(scaleRangePixels <= 0)
	{
		return;
	}
	float scaleRangeMsec = pTimeAxis->getMaxAsMsec() - pTimeAxis->getMinAsMsec();
	float scaleMsecPerPixel = scaleRangeMsec / (float)scaleRangePixels;
	choseDataToDraw(scaleMsecPerPixel, ppUseValuesList, ppUseTimesList);
}

void HistoryDataRange::choseDataToDraw(float scaleMsecPerPixel, QList<float> **ppUseValuesList, QList<qint64> **ppUseTimesList)
{
	float myRangeMsec = timesList.last() - timesList.first();
	if(myRangeMsec <= 0)
	{
		return;
	}

	// Parameters of this range
	float totalPixels = myRangeMsec / scaleMsecPerPixel;
	float valuesPerPixel = (float)timesList.count() / totalPixels;
	if(valuesPerPixel < 8)
	{
		return;
	}

	// Decide - build new array or use existing one
	if((!filteredTimesList.isEmpty()) && filterValuesPerPixel)
	{
		float ratio = filterValuesPerPixel / valuesPerPixel;
		if((0.5 <= ratio) && (ratio <= 2) && (!valueAdded))
		{
			*ppUseValuesList = &filteredValuesList;
			*ppUseTimesList = &filteredTimesList;
			return;
		}
	}
	filteredTimesList.clear();
	filteredValuesList.clear();
	qint64 startTime = timesList.first(), endTime = timesList.last();
	float minValue = valuesList.first(), maxValue = valuesList.first(), lastValue = valuesList.first();
	for(int n = 1 ; n < timesList.count() ; n++)
	{
		if((timesList.at(n) - startTime) > scaleMsecPerPixel)
		{
			addFiltered(startTime, endTime, minValue, maxValue, lastValue);
			startTime = timesList.at(n);
			endTime = startTime;
			minValue = maxValue = lastValue = valuesList.at(n);
		}
		else
		{
			endTime = timesList.at(n);
			float value = valuesList.at(n);
			if(value < minValue)
			{
				minValue = value;
			}
			else if(value > maxValue)
			{
				maxValue = value;
			}
			lastValue = value;
		}
	}
	addFiltered(startTime, endTime, minValue, maxValue, lastValue);	// Add last range

	*ppUseValuesList = &filteredValuesList;
	*ppUseTimesList = &filteredTimesList;

	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("ID %d: valuesPerPixel %f old %f: values %d filtered values %d", id, valuesPerPixel, filterValuesPerPixel, valuesList.count(), filteredValuesList.count());
#else
		printf("ID %d: valuesPerPixel %f old %f: values %d filtered values %d", id, valuesPerPixel, filterValuesPerPixel, valuesList.count(), filteredValuesList.count());
		fflush(stdout);
#endif
	}
	filterValuesPerPixel = valuesPerPixel;
}

void HistoryDataRange::addFiltered(qint64 startTime, qint64 endTime, float minValue, float maxValue, float lastValue)
{
	switch(filterType)
	{
	case FilterTypeMin:
		addFilteredMin(startTime, endTime, minValue);
		break;
	default:
		addFilteredMinMax(startTime, endTime, minValue, maxValue, lastValue);
		break;
	}
}

void HistoryDataRange::addFilteredMinMax(qint64 startTime, qint64 endTime, float minValue, float maxValue, float lastValue)
{
	if(minValue == maxValue)
	{
		filteredValuesList.append(minValue);
		filteredTimesList.append(startTime + ((endTime - startTime) / 2));
	}
	else if(lastValue == maxValue)
	{
		filteredValuesList.append(minValue);
		filteredTimesList.append(startTime);
		filteredValuesList.append(maxValue);
		filteredTimesList.append(endTime);
	}
	else if(lastValue == minValue)
	{
		filteredValuesList.append(maxValue);
		filteredTimesList.append(startTime);
		filteredValuesList.append(minValue);
		filteredTimesList.append(endTime);
	}
	else
	{
		filteredValuesList.append(minValue);
		filteredTimesList.append(startTime);
		filteredValuesList.append(maxValue);
		filteredTimesList.append(endTime);
		filteredValuesList.append(lastValue);
		filteredTimesList.append(endTime);
	}
}

void HistoryDataRange::addFilteredMin(qint64 startTime, qint64 endTime, float minValue)
{
	filteredValuesList.append(minValue);
	filteredTimesList.append(startTime + ((endTime - startTime) / 2));
}


void HistoryDataRange::dump(FILE *pFile)
{
	if(!DebugCtl::isHistory())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	QDateTime startTime(QDateTime::currentDateTime());
	startTime.setMSecsSinceEpoch(start);
	QDateTime endTime(QDateTime::currentDateTime());
	endTime.setMSecsSinceEpoch(end);
	fprintf(pFile, "   RANGE from %s to %s: %s %d values\n",
		startTime.toString("yyyy-MM-dd hh:mm:ss.zzz").toLatin1().constData(),
		endTime.toString("yyyy-MM-dd hh:mm:ss.zzz").toLatin1().constData(),
		(requestSent ? "REQUEST SENT" : ""), valuesList.count());
	if(valuesList.count())
	{
		QDateTime dateTime;
		for(int n = 0 ; n < timesList.count() ; n++)
		{
			startTime.setMSecsSinceEpoch(timesList.at(n));
			fprintf(pFile, "      %d: %s\n", n, startTime.toString("yyyy-MM-dd hh:mm:ss.zzz").toLatin1().constData());
		}
	}
}
