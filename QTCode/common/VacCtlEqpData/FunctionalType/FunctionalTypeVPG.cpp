//	Implementation of FunctionalTypeVPG class
////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVPG.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeVPG::FunctionalTypeVPG(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVPG::fillCriteria(FunctionalType *pType) {
	// Own criteria
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Connected, "Pumping group is connected (mobile)");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is Disconnected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPG in sector {sector} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPG in sector {sector} is Disconnected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPG in main part {mainPart} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPG in main part {mainPart} is Disconnected");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Nominal, "Pumping group is Nominal Speed");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is Nominal Speed");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT Nominal Speed");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPG in sector {sector} is Nominal Speed");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPG in sector {sector} is NOT Nominal Speed");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPG in main part {mainPart} is Nominal Speed");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPG in main part {mainPart} is NOT Nominal Speed");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Stopped, "Pumping group is Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPG in sector {sector} is Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPG in sector {sector} is NOT Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPG in main part {mainPart} is Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPG in main part {mainPart} is NOT Stopped");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Vvr1Open, "VVR1 of Pumping group is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} VVR1 is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} VVR1 is NOT OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VVR1 of VPG in sector {sector} is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VVR1 of VPG in sector {sector} is NOT OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VVR1 of VPG in main part {mainPart} is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VVR1 of VPG in main part {mainPart} is NOT OPEN");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Vvr1Closed, "VVR1 of Pumping group is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} VVR1 is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} VVR1 is NOT CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VVR1 of VPG in sector {sector} is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VVR1 of VPG in sector {sector} is NOT CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VVR1 of VPG in main part {mainPart} is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VVR1 of VPG in main part {mainPart} is NOT CLOSED");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Vvr2Open, "VVR2 of Pumping group is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} VVR2 is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} VVR2 is NOT OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VVR2 of VPG in sector {sector} is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VVR2 of VPG in sector {sector} is NOT OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VVR2 of VPG in main part {mainPart} is OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VVR2 of VPG in main part {mainPart} is NOT OPEN");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Vvr2Closed, "VVR2 of Pumping group is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} VVR2 is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} VVR2 is NOT CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VVR2 of VPG in sector {sector} is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VVR2 of VPG in sector {sector} is NOT CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VVR2 of VPG in main part {mainPart} is CLOSED");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VVR2 of VPG in main part {mainPart} is NOT CLOSED");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "VPG Error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: Error {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no Error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPG in sector {sector}: Error {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPG in sector {sector}: no Error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPG in main part {mainPart}: Error {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPG in main part {mainPart}: no Error");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Warning, 0, "VPG Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPG in sector {sector}: Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPG in sector {sector}: no Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPG in main part {mainPart}: Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPG in main part {mainPart}: no Warning");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeVPG::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case Connected:
			result = "Connected";
			break;
		case Stopped:
			result = "Stopped";
			break;
		case Nominal:
			result = "Nominal";
			break;
		case Vvr1Open:
			result = "Vvr1Open";
			break;
		case Vvr1Closed:
			result = "Vvr1Closed";
			break;
		case Vvr2Open:
			result = "Vvr2Open";
			break;
		case Vvr2Closed:
			result = "Vvr2Closed";
			break;
		default:
			break;
		}
		break;
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeVPG::parseCriteriaKey(int critType, const char *name, QString &errMsg) {
	int result = 0;
	switch(critType) {
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "Connected")) {
			result = Connected;
		}
		else if(!strcasecmp(name, "Stopped")) {
			result = Stopped;
		}
		else if(!strcasecmp(name, "Nominal")) {
			result = Nominal;
		}
		else if(!strcasecmp(name, "Vvr1Open")) {
			result = Vvr1Open;
		}
		else if(!strcasecmp(name, "Vvr1Closed")) {
			result = Vvr1Closed;
		}
		else if (!strcasecmp(name, "Vvr2Open"))	{
			result = Vvr2Open;
		}
		else if (!strcasecmp(name, "Vvr2Closed")) {
			result = Vvr2Closed;
		}
		break;
	default:
		break;
	}
	if(result) {
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}