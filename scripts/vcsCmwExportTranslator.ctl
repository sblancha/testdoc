
/**@name SCRIPT: lhcVacCmwExportTranslator.ctl

@author: Leonid Kopylov (IHEP, Protvino, Russia), Serguei Merker (IHEP, Protvino, Russia)

Creation Date: 26/05/2008

Modification History: 
Modified: 07/07/2008
Modified: 03/11/2008

version 1.0

Purpose: 
Translate rather complex device values in PVSS to more simple values
to be exported via CMW.
Export from database shall generate file called <machine_name>_CMWTranslator.config
containing list of DPs which are exported via CMW.
This script on start reads that file, connects to required DPEs of those DPs
and in callbacks updates another DPE which is exported via CMW.
For the time being there is simple rules:
1) All validity and state are exported using DPE called State (uinteger16).
	Bits of this DPE are:
		0 = validity	(all equipment)
		1 = OPEN (valves only), ON (gauges, VPCIs...)
		2 = CLOSED (valves only), OFF (gauges, VPCIs...)
2) DPE PR is exported directly, so only integrated state shall be generated
by this script

Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. data point type needed: All which are exported
	. data point:
	. PVSS version: 3.1
	. operating system: WXP, NT, W2k and Linux, but tested only under WXP, W2k and Linux.
	. distributed system: yes.
*/

#uses "VacCtlEqpDataPvss"	//Load dll or so file
#uses "vclMachine.ctl"	// Load CTRL library
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "vclDIPConfig.ctl"	// Load CTRL library
#uses "unSystemIntegrity_DPE.ctl"	// Load CTRL library
#uses "vclDIPConfig.ctl"		// Load CTRL library
#uses "unSystemIntegrity.ctl"		// Load CTRL library

// Flag to enable debugging messages
bool	debug = false;

main()
{
  string masterCnfg;
  int   channel, cnfgRef, nConnected;

  DebugTN("vcsVacCmwTranslator.ctl: Starting");
  dyn_string exceptionInfo;
  // Set accelerator name to be used by other components
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsVacCmwTranslator.ctl: Failed to read machine name: " + exceptionInfo);
    return;
  }
	
  DebugTN("vcsVacCmwTranslator.ctl: Machine is " + glAccelerator);
  // There is something to translate, read machine configuration data
  if(!InitStaticData())
  {
    return;
  }

  // 1st item in array dpList[n][1] - name of Dp
  // 2nd item in array dpList[n][2] - boolean, if True - DIP processing (publishing) is needed
  dyn_dyn_anytype dpList = ReadConfigFile();
  if(dynlen(dpList) == 0)
  {
    return;
  }
  if(debug)
  {
    DebugTN("vcsVacCmwTranslator.ctl: dpList:", dpList);
  }

  // Process all required DPEs
  int nConnected = 0;
  for(int n = dynlen(dpList) ; n > 0 ; n--)
  {
    if(debug)
    {
      DebugTN("Process DP: ", dpList[n][1]);
    }
    string dpType = dpTypeName(dpList[n][1]);
    if(dpType == "")
    {
      DebugTN("vcsVacCmwTranslator.ctl: no DP type for <" + dpList[n][1] + ">");
      continue;
    }
    unsigned dpTypeMask = LhcVacDpTypeGroup(dpType);
    if(dpTypeMask == 0)
    {
      DebugTN("vcsVacCmwTranslator.ctl: no DP type mask for <" + dpList[n][1] +
        ">, DP type <" + dpType + ">");
      continue;
    }
    string alarmDp = "", plcDp;
    LhcVacDevicePlc(dpList[n][1], plcDp, alarmDp, exceptionInfo);
    bool skipDp = false;
    switch(dpTypeMask)
    {
    case VAC_DP_TYPE_GROUP_VV:	// Valves
      if(!dpExists(dpList[n][1] + ".State"))
      {
        DebugTN("vcsVacCmwTranslator.ctl: DP <" + dpList[n][1] +
          ">, DP type <" + dpType + ">: no 'State' DPE");
        skipDp = true;
        break;
      }
      SetDpFunctionAttributes(dpList[n][1], dpList[n][1], alarmDp); 
      if(dpList[n][2])
      {
        ProcessDIPPublishing(dpList[n][1], ".State"); 
      }
      break;
    case VAC_DP_TYPE_GROUP_VGM:	// membrane gauges
    case VAC_DP_TYPE_GROUP_VGR:	// Pirani gauges
    case VAC_DP_TYPE_GROUP_VGP:	// Pening gauges
    case VAC_DP_TYPE_GROUP_VGI:	// ionization gauges
    case VAC_DP_TYPE_GROUP_VGF:	// full range gauges
    case VAC_DP_TYPE_GROUP_VGTR:	// pressure transmitter
      if(!dpExists(dpList[n][1] + ".State"))
      {
        DebugTN("vcsVacCmwTranslator.ctl: DP <" + dpList[n][1] +
          ">, DP type <" + dpType + ">: no 'State' DPE");
        skipDp = true;
        break;
      }
      SetDpFunctionAttributes(dpList[n][1], dpList[n][1], alarmDp); 
      if(dpList[n][2])
      {
        ProcessDIPPublishing(dpList[n][1], ".State"); 
        ProcessDIPPublishing(dpList[n][1], ".PR"); 
      }
      break;
    case VAC_DP_TYPE_GROUP_VPI:	// ion pumps
      if(!dpExists(dpList[n][1] + ".State"))
      {
        DebugTN("vcsVacCmwTranslator.ctl: DP <" + dpList[n][1] +
          ">, DP type <" + dpType + ">: no 'State' DPE");
        skipDp = true;
        break;
      }
      string masterDp = "";
      LhcVacEqpMaster(dpList[n][1], masterDp, channel, exceptionInfo);
      if(masterDp == "")
      {
        DebugTN("vcsVacCmwTranslator.ctl: no master for <" + dpList[n][1] +
          ">, DP type <" + dpType + ">");
        break;
      }
      SetDpFunctionAttributes(masterDp, dpList[n][1], alarmDp); 
      if(dpList[n][2])
      {
        ProcessDIPPublishing(dpList[n][1], ".State"); 
        ProcessDIPPublishing(dpList[n][1], ".PR"); 
      }
      break;
    case VAC_DP_TYPE_GROUP_VRPM:	// Power supply for anti e-cloud solenoid
      if(!dpExists(dpList[n][1] + ".State"))
      {
        DebugTN("vcsVacCmwTranslator.ctl: DP <" + dpList[n][1] +
          ">, DP type <" + dpType + ">: no 'State' DPE");
        skipDp = true;
        break;
      }
      SetDpFunctionAttributesVRPM(dpList[n][1], dpList[n][1], alarmDp); 
      if(dpList[n][2])
      {
        ProcessDIPPublishing(dpList[n][1], ".State"); 
        ProcessDIPPublishing(dpList[n][1], ".RmA"); 
        ProcessDIPPublishing(dpList[n][1], ".RmV"); 
      }
      break;
      break;
    }
    if(skipDp)
    {
      continue;
    }
    nConnected++;
  }
  DebugTN("vcsVacCmwTranslator.ctl: finished, " + nConnected + " DPs initialized");
  dynClear(dpList);
}
// SetDpFunctionAttributes
/**
Purpose:
set dp_function for specified DP

Parameters:
	dpSource - string, dp - responsible for summary state
	dpTarget - string, the summary state to be set for this dp
	alarmDp - string, name of alarm Dp. Get by calling of LhcVacDevicePlc().
Return:
	- none

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void SetDpFunctionAttributes(string dpSource, string dpTarget, string alarmDp)
{
  string   function;
  dyn_string  exceptionInfo, params;

  if(alarmDp == "")
  {
    params = makeDynString(dpSubStr(dpSource, DPSUB_DP) + ".RR1:_online.._value");
    function = "((p1 & 0x40000000u)>>30)|((p1 & 0x00010000u)>>14)|((p1 & 0x00020000u)>>16)";
  }
  else
  {
    params = makeDynString(dpSubStr(alarmDp, DPSUB_DP) + ".alarm:_online.._value",
      dpSubStr(dpSource, DPSUB_DP) + ".RR1:_online.._value");
    function = "(~(p1*0xFFFFFFFFu)) & (((p2 & 0x40000000u)>>30)|((p2 & 0x00010000u)>>14)|((p2 & 0x00020000u)>>16))";
  }
  dpSetWait(dpTarget + ".State:_dp_fct.._type", DPCONFIG_DP_FUNCTION);
  dyn_errClass err = getLastError(); 
  if(dynlen(err) > 0)
  { 
    throwError(err);
    fwException_raise(exceptionInfo, "ERROR",
      "vcsVacCmwTranslator.ctl(SetDpFunctionAttributes()):  Could not create dp function config for <" +
      dpTarget + ">", "");
    DebugTN(exceptionInfo);
    return;
  }
  dpSetWait(dpTarget + ".State:_dp_fct.._param", params,
    dpTarget + ".State:_dp_fct.._fct", function);
  err = getLastError(); 
  if(dynlen(err) > 0)
  {
    DebugN("FAILURE dpSetWait for <", dpTarget + ".State:_dp_fct.._param> <", dpTarget + ".State:_dp_fct.._fct>");
    DebugN("PARAMS: ", params);
    DebugN("FUNCTION: ", function);
    throwError(err);
    fwException_raise(exceptionInfo, "ERROR",
      "vcsVacCmwTranslator.ctl(SetDpFunctionAttributes()):  Could not configure dp function for <" + dpTarget + ">", "");
    DebugTN(exceptionInfo);
  }
  return;
}

void SetDpFunctionAttributesVRPM(string dpSource, string dpTarget, string alarmDp)
{
  string   function;
  dyn_string  exceptionInfo, params;

  if(alarmDp == "")
  {
    params = makeDynString(dpSubStr(dpSource, DPSUB_DP) + ".RR1:_online.._value");
    function = "((p1 & 0x40000000u)>>30)|((p1 & 0x02000000u)>>24)|((p1 & 0x02000000u)?0x0:0x4)";
  }
  else
  {
    params = makeDynString(dpSubStr(alarmDp, DPSUB_DP) + ".alarm:_online.._value",
      dpSubStr(dpSource, DPSUB_DP) + ".RR1:_online.._value");
    function = "(~(p1*0xFFFFFFFFu)) & (((p2 & 0x40000000u)>>30)|((p2 & 0x02000000u)>>24)|((p2 & 0x02000000u)?0x0:0x4))";
  }
  dpSetWait(dpTarget + ".State:_dp_fct.._type", DPCONFIG_DP_FUNCTION);
  dyn_errClass err = getLastError(); 
  if(dynlen(err) > 0)
  { 
    throwError(err);
    fwException_raise(exceptionInfo, "ERROR",
      "vcsVacCmwTranslator.ctl(SetDpFunctionAttributes()):  Could not create dp function config for <" +
      dpTarget + ">", "");
    DebugTN(exceptionInfo);
    return;
  }
  dpSetWait(dpTarget + ".State:_dp_fct.._param", params,
    dpTarget + ".State:_dp_fct.._fct", function);
  err = getLastError(); 
  if(dynlen(err) > 0)
  {
    DebugN("FAILURE dpSetWait for <", dpTarget + ".State:_dp_fct.._param> <", dpTarget + ".State:_dp_fct.._fct>");
    DebugN("PARAMS: ", params);
    DebugN("FUNCTION: ", function);
    throwError(err);
    fwException_raise(exceptionInfo, "ERROR",
      "vcsVacCmwTranslator.ctl(SetDpFunctionAttributes()):  Could not configure dp function for <" + dpTarget + ">", "");
    DebugTN(exceptionInfo);
  }
  return;
}

// ReadConfigFile
/**
Purpose:
Read configuration file, generated by database export, to get list
of DP names which shall be translated

Parameters:
	- None

Return:
	- list of DP names, empty list in case of error(s)

Usage: Internal

PVSS manager usage: CTRL
Read config file + add to return list all valves of LHC
Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
dyn_dyn_anytype ReadConfigFile()
{
  // get list of all valves
  dyn_string  exceptionInfo;
  dyn_dyn_anytype  dpList;
  GetValveList(dpList, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsVacCmwTranslator.ctl : ReadConfigFile() failed to get ALL valves: " + exceptionInfo);
    return false;
  }

  // file parsing
  string fileName = DATA_PATH + "/" + glAccelerator + "_CMWTranslator.config";
  if(access(fileName, F_OK) != 0)
  {
    DebugTN("vcsVacCmwTranslator.ctl: file <" + fileName + "> does not exist, exiting...");
    return dpList;
  }
  file fp = fopen(fileName, "r");
  if(fp == 0)
  {
    DebugTN("vcsVacCmwTranslator.ctl: failed to open file <" + fileName + "> for reading, exiting...");
    return dpList;
  }
  string  		line;
  while(fgets(line, 512, fp) != 0)
  {
    dyn_string dpString = strsplit(line, " ");

    // first item (mandatory) - DP name, 2nd (no mandatory) - shows whether DIP publishing is needed
    string dpName = strltrim(strrtrim(dpString[1]));
    if(!dpExists(dpName))
    {
      DebugTN("vcsVacCmwTranslator.ctl: DP <" + dpName + "> from config file does not exist");
      continue;
    }
    if(dynlen(dpString) > 2)
    {
      DebugTN("vcsVacCmwTranslator.ctl: config file - bad consistency");
      continue;
    }
    bool dip = false;
    if(dynlen(dpString) == 2)
    {	
      if(strltrim(strrtrim(dpString[2])) != "DIP")
      {
        DebugTN("vcsVacCmwTranslator.ctl: config file - unknown item <" + strltrim(strrtrim(dpString[2])) + ">");
        continue;
      }
      else
      {
        dip = true;
      }
    }
    // look for this DpName in dpList and set DIP attribiute
    int dpIdx;
    for(int dpIdx = dynlen(dpList) ; dpIdx > 0 ; dpIdx--)
    {
      if(dpList[dpIdx][1] == dpName)
      {
        break;
      }
    }
    if(dpIdx <= 0)
    {
      dynAppend(dpList, makeDynAnytype(dpName, dip));
      dpIdx = dynlen(dpList);
    }
    else
    {
      dpList[dpIdx][2] = dip;
    }
    if(debug)
    {
      if(dpList[dpIdx][2])
      {
        DebugTN("set DIP attribute for: " + dpName);
      }
    }
  }
  fclose(fp);
  return dpList;
}
// InitStaticData
/**
Purpose:
Read all machine configuration data from file(s)

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool InitStaticData()
{
  dyn_string  exceptionInfo;

  bool isLhcData = glAccelerator == "LHC" ? true : false;

  // Initalize passive machine data
  int coco = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsVacCmwTranslator.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsVacCmwTranslator.ctl: LhcVacEqpInitPassive() done");

  // Initialzie active equipment information
  coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
			exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsVacCmwTranslator.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsVacCmwTranslator.ctl: LhcVacEqpInitActive() done");
  LhcVacInitDevFunc();
  DebugTN("vcsVacCmwTranslator.ctl: InitStaticData() done");
  return true;
}
// ProcessDIPPublishing
/**
Purpose:
Process publishing of DPE via DIP

Parameters:
	dp - dp to be published
	dpe - dpe to be published
Return:
	- none

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void ProcessDIPPublishing(string dp, string dpe) 
{
  dyn_string  exceptionInfo;
  if(debug)
  {
    DebugTN("ProcessDIPPublishing: ", dp, dpe);
  }
  string wrongPublName = LhcVacGetWrongPublishingNameCMW(dp);
  UnpublishWrongPublishing(dp + dpe, wrongPublName + "." + dpe, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsVacCmwTranslator.ctl:UnpublishWrongPublishing() failed for DPE <" + dpPublName + dpe + "> :" + exceptionInfo);
  }
  dynClear(exceptionInfo);
  UnpublishWrongPublishing(dp + dpe, wrongPublName + dpe, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsVacCmwTranslator.ctl:UnpublishWrongPublishing() failed for DPE <" + dpPublName + dpe + "> :" + exceptionInfo);
  }
  dynClear(exceptionInfo);
  string dpPublName = LhcVacGetPublishingNameCMW(dp);
  UnpublishWrongPublishing(dp + dpe, dpPublName + "." + dpe, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsVacCmwTranslator.ctl:UnpublishWrongPublishing() failed for DPE <" + dpPublName + dpe + "> :" + exceptionInfo);
  }
  dynClear(exceptionInfo);
  ProcessDpePublishing(dp + dpe, dpPublName + dpe, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsVacCmwTranslator.ctl:ProcessDIPPublishing() failed for DPE <" + dpPublName + dpe + "> :" + exceptionInfo);
  }
}
int GetValveList(dyn_dyn_anytype &valveList, dyn_string &exceptionInfo)
{
  string				dpName, dpType, iconFileName;
  int					coco, nDps, dpInd, verticalPos;
  unsigned			dpTypeMask;
  float					partSize;
  bool					isValve;

  dynClear(valveList);

  /*
  if(LhcVacGetAllSectors(glVacMainParts, glVacSectors, glSectDomains, exceptionInfo) < 0)
  {
    return -1;
  }
  */
  int nDpTypes = dynlen(glLhcVacDpTypes);
  for(int dpTypeInd = nDpTypes ; dpTypeInd > 0 ; dpTypeInd--)
  {
    bool skipDpType = true;
    switch(glLhcVacDpTypes[dpTypeInd])
    {
    case "VVS_LHC":
    case "VVS_PS":
    case "VVS_S":
    case "VVS_SV":
    case "VVF_S":
    case "VLV_ANA":
    case "VVS_PSB_SUM":
    case "VVS_PSB_SR":
      skipDpType = false;
      break;
    }
    if(skipDpType)
    {
      continue;
    }
    dyn_string allDpNames = dpNames("*", glLhcVacDpTypes[dpTypeInd]);
    int nDps = dynlen(allDpNames);
    for(int dpInd = 1 ; dpInd <= nDps ; dpInd++)
    {
      string dpName = dpSubStr(allDpNames[dpInd], DPSUB_DP);
      dynAppend(valveList, makeDynAnytype(dpName, false));
    }
  }
  return 0;
}

