#ifndef	VACICONINJ_6B02_H
#define	VACICONINJ_6B02_H

// Icon for INJ_6B02 - gas injection in LINAC4

#include "VacIcon.h"

class EqpINJ_6B02;

class VACCTLEWOUTIL_EXPORT VacIconINJ_6B02 : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconINJ_6B02(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconINJ_6B02();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

	// Override method of VacIcon 
	virtual void setDirection(VacIconContainer::Direction newDirection);

protected:
	// Pointer to my device
	EqpINJ_6B02 *pEqp;

	// Areas for pump and valve symbols. Drawing can be easy using QPainter
	// coordinate transformations, but finding areas for tooltips is not so
	// evident. May be it is more convenient just to keep in meory coordinates
	// of all distinct areas

	QRect	pump;
	QRect	valve;

	void setAreaGeometry(void);

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawValve(QPainter &painter);
	virtual void drawPump(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
	
	void setPainterForValve(QPainter &painter);

	// Override VacIcon's reaction on mouse press
	virtual void mousePressEvent(QMouseEvent *pEvent);
};

#endif	// VACICONINJ_6B02_H
