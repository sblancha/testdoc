// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Component Name: VacCtlEqpData librairy
Class: Equipment Vacuum Pump - Group Unified (VP_GU)
Language: C++

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: This file contains the defined classes for the equipment Vacuum Pump - Group Unified (VP_GU)

Limitations: To be used with vacuum framework.

Function: 

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------
#ifndef EQPVP_GU_H
#define	EQPVP_GU_H

//	VP_GU - Vacuum Pump - Group Unified 

#include "Eqp.h"

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Generic purpose color defined in eqp.h

//	Main state bits to be analyzed
#define	RR1_VALID_VPGU				(0x40000000) // (0x40 00  00 00)
#define RR1_ERROR_VPGU				(0x00800000) // (0x00 80  00 00)
#define	RR1_WARNING_VPGU			(0x00400000) // (0x00 40  00 00) 
#define	RR1_ON_VPGU					(0x00020000) // (0x00 02  00 00) 
#define	RR1_OFF_VPGU				(0x00010000) // (0x00 01  00 00) 
#define RR1_VVR1_OPEN_VPGU			(0x00000001) // (0x00 00  00 01)
#define RR1_VVR1_CLOSED_VPGU		(0x00000002) // (0x00 00  00 02)
#define RR1_VVR1_FORCED_VPGU		(0x00000004) // (0x00 00  00 04) 
#define RR1_VVR1_INTERLOCKED_VPGU	(0x00000008) // (0x00 00  00 08)
#define RR2_VVR1_ERROR_VPGU			(0x04000000) // (0x04 00  00 00)
#define RR2_VVR1_WARNING_VPGU		(0x00040000) // (0x00 04  00 00)
#define RR1_VVR2_OPEN_VPGU			(0x00000010) // (0x00 00  00 10)
#define RR1_VVR2_CLOSED_VPGU		(0x00000020) // (0x00 00  00 20)
#define RR1_VVR2_FORCED_VPGU		(0x00000040) // (0x00 00  00 40)
#define RR1_VVR2_INTERLOCKED_VPGU	(0x00000080) // (0x00 00  00 80)
#define RR2_VVR2_ERROR_VPGU			(0x08000000) // (0x08 00  00 00)
#define RR2_VVR2_WARNING_VPGU		(0x00080000) // (0x00 08  00 00)

#define RR1_VVI_OPEN_VPGU			(0x00000400) // (0x00 00  04 00)
#define RR1_VVI_CLOSED_VPGU			(0x00000800) // (0x00 00  08 00)
#define RR2_VVI_WARNING_VPGU		(0x00200000) // (0x00 20  00 00)
#define RR2_VVI_ERROR_VPGU			(0x20000000) // (0x20 00  00 00)
#define RR1_VVD_OPEN_VPGU			(0x00004000) // (0x00 00  40 00)
#define RR1_VVD_CLOSED_VPGU			(0x00008000) // (0x00 00  80 00)
#define RR2_VVD_WARNING_VPGU		(0x00800000) // (0x00 80  00 00)
#define RR2_VVD_ERROR_VPGU			(0x80000000) // (0x80 00  00 00)
#define RR1_VVT_OPEN_VPGU			(0x00000100) // (0x00 00  01 00)
#define RR1_VVT_CLOSED_VPGU			(0x00000200) // (0x00 00  02 00)
#define RR2_VVT_WARNING_VPGU		(0x00100000) // (0x00 10  00 00)
#define RR2_VVT_ERROR_VPGU			(0x10000000) // (0x10 00  00 00)
#define RR1_VVP_OPEN_VPGU			(0x00001000) // (0x00 00  10 00)
#define RR1_VVP_CLOSED_VPGU			(0x00002000) // (0x00 00  20 00)
#define RR2_VVP_WARNING_VPGU		(0x00400000) // (0x00 40  00 00)
#define RR2_VVP_ERROR_VPGU			(0x40000000) // (0x40 00  00 00)
#define RR1_VPT_ON_VPGU				(0x00200000) // (0x00 20  00 00)
#define RR1_VPT_NOMINAL_VPGU		(0x20000000) // (0x20 00  00 00)
#define RR2_VPT_WARNING_VPGU		(0x00010000) // (0x00 01  00 00)
#define RR2_VPT_ERROR_VPGU			(0x01000000) // (0x01 00  00 00)
#define RR1_VPP_ON_VPGU				(0x00010000) // (0x01 00  00 00)
#define RR1_VPP_THERM_PROT_VPGU		(0x00100000) // (0x00 10  00 00) set when ok
#define RR1_VPP_CABLE_VPGU			(0x00040000) // (0x00 04  00 00) set when ok
#define RR2_VPP_WARNING_VPGU		(0x00020000) // (0x00 02  00 00)
#define RR2_VPP_ERROR_VPGU			(0x02000000) // (0x02 00  00 00)

#define STATE_NOMINAL_VVR_CL		(4)
#define STATE_NOMINAL_VVR_OP		(5)
#define STATE_RECOVERING_VVR_CL		(6)
#define STATE_RECOVERING_VVR_OP		(7)
#define STATE_SERVICES				(10)

class VACCTLEQPDATA_EXPORT EqpVP_GU : public Eqp
{
	Q_OBJECT

public:
	EqpVP_GU(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVP_GU(const EqpVP_GU &src) : Eqp(src) {}
#endif
	~EqpVP_GU();

	virtual void setActive(DataEnum::DataMode mode, bool active);

	virtual void getMaster(QString &masterDp); // no master channel , int &masterChannel);
	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes); 

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	
	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::Pressure; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode mode);
	virtual bool isMainValueNegative(void) const;

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode); 
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
	virtual void getSubEqpColor(QString subEqp, QColor &color, DataEnum::DataMode mode);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isComAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? comAlarmReplay : comAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline unsigned getRR2(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr2Replay : rr2Online; }
	inline int getState(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? stateReplay : stateOnline; }

	inline bool isVvr2() const { return hasVvr2; }
	inline bool isVgLeft() const { return hasIndependantVgLeft; }
	inline bool isVgRight() const { return hasIndependantVgRight; }

	virtual int getWarningSt() const { return warningStOnline; }
	virtual int getErrorSt() const { return errorStOnline; }
	virtual int getStepSt() const { return stepStOnline; }
	virtual int getVptRotationSt() const { return vptRotationStOnline; }
	virtual int getVptCurrentSt() const { return vptCurrentStOnline; }
	virtual int getVppCurrentSt() const { return vppCurrentStOnline; }
	virtual double getPR() const { return prOnline; }
	virtual int getDegCSt() const { return degCStOnline; }

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE 
	unsigned	rr1Online;
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for online mode
	unsigned	rr2Online;
	unsigned	rr2Replay;


	// Last known value of the state of the  for online mode
	unsigned stateOnline;
	unsigned stateReplay;

	// Last known value of pressure value in mbar for online mode
	double prOnline;
	double prReplay;

	// Last knwon state of TCP connection to mobile (true == alarm) for online mode
	bool		comAlarmOnline;
	bool		comAlarmReplay;

	// Last known value of warning code status  for online mode
	unsigned	warningStOnline;
	unsigned	warningStReplay;

	// Last known value of error code status for online mode
	unsigned	errorStOnline;
	unsigned	errorStReplay;

	// Last known value of step number of the process for online mode
	unsigned	stepStOnline;

	// Last known value of rotation speed of turbo pump for online mode
	unsigned	vptRotationStOnline;

	// Last known value of motor current of the primary pump for online mode
	unsigned	vptCurrentStOnline;

	// Last known value of motor current of the primary pump for online mode
	unsigned	vppCurrentStOnline;

	// Last known value of temperature value of the pumping group (for bake-out) for online mode
	unsigned degCStOnline;

	// Pumping group with VVR2
	bool hasVvr2;

	// Pumping group with independant gauge left
	bool hasIndependantVgLeft;

	// Pumping group with independant gauge right
	bool hasIndependantVgRight;
	
#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned state, bool comAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, unsigned state, double pr, bool comAlarm, QString &string, bool isHistoryState = false);
	virtual void getToolTipString(unsigned rr1, unsigned state, double pr, bool comAlarm, QString &string);

private:
	virtual void getColorVVR1(unsigned rr1, unsigned rr2, QColor &color);
	virtual void getColorVVR2(unsigned rr1, unsigned rr2, QColor &color);
	virtual void getColorVPT(unsigned rr1, unsigned rr2, QColor &color);
	virtual void getColorVPP(unsigned rr1, unsigned rr2, QColor &color);
	virtual void getColorVV(unsigned rr1, unsigned rr2,
							unsigned rr1Open, unsigned rr1Closed, unsigned rr2Warning, unsigned rr2Error,
							QColor &color);
	virtual void getColorVV_NC(unsigned rr1, unsigned rr2,
		unsigned rr1Open, unsigned rr1Closed, unsigned rr2Warning, unsigned rr2Error,
		QColor &color);

#endif
};


#endif	// EQPVP_GU_H
