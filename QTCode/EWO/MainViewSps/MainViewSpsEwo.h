#ifndef MAINVIEWSPSEWO_H
#define MAINVIEWSPSEWO_H

// EWO interface for MainSps widget

#include "VacMainViewEwo.h"

#include "MainSps.h"

class EWO_EXPORT MainViewSpsEwo : public VacMainViewEwo
{
	Q_OBJECT

public:
	MainViewSpsEwo(QWidget *parent);
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private:
	QString		methodName;

	QVariant invokeDataReady(const QString &name, QList<QVariant> &values,
		QString &error);
	QVariant invokeIsSelected(const QString &name, QList<QVariant> &values,
		QString &error);
	QVariant invokeIsContSelection(const QString &name, QList<QVariant> &values,
		QString &error);
	QVariant invokeGetSelectedSectors(const QString &name, QList<QVariant> &values,
		QString &error);
	QVariant invokeAddLegendLabel(const QString &name, QList<QVariant> &values,
		QString &error);
	QVariant invokeAddRotatedLegendLabel(const QString &name, QList<QVariant> &values,
		QString &error);
};

#endif	// MAINVIEWSPSEWO_H
