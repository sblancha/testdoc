﻿/**
@defgroup apiFct List of Equipment API functions
The newExternHdl function has a fixed interface and must be exported so that it can be called by WCCOA.\n
The keyword '_declspec(dllexport)' has to be set only under windows for exporting the function newExternHdl.Under Linux
this keyword is not necessary.\n
The newExternHdl function must allocate a class object and return the pointer to it.\n
Parameter is nextHdl.\n
It returns a pointer to new class instance.\n
Usual way to create C++LibAPI for WCCOA BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl).\n
@section gen Generic API
- @ref LhcVacSetDataPart
- @ref LhcVacEqpInitPassive
- @ref LhcVacEqpInitActive
- @ref LhcVacEqpInitDocData
@section eqp Equipment API
- @ref LhcVacGetAttribute
- @ref LhcVacGetAllAttributes
- @ref LhcVacGetCtlAttributes
@section reg Region and Domain API
@section devlist Device List API
@section qt Qt Dialogs API

*/



#include "VacCtlEqpDataPvss.h"

#include "DataPool.h"
#include "Eqp.h"
#include "Rack.h"
#include "MobileType.h"
#include "DebugCtl.h"

#include "DpConnection.h"
#include "DpPolling.h"
#include "DpeHistoryQuery.h"
#include "MobilePool.h"
#include "MobileHistoryQuery.h"
#include "DpeHistoryDepthQuery.h"

#include "VacMainView.h"
#include "VacIcon.h"

#include <BitVar.hxx>
#include <FloatVar.hxx>
#include <UIntegerVar.hxx>
#include <AnyTypeVar.hxx>

#include <QVariant>
#include <string.h>

#ifndef PVSS_SERVER_VERSION
#include <QColor>
#include <QDateTime>
#include "VacCtlStateArchive.h"
#include "ReplayPool.h"
#include <QMessageBox>
#endif

#include <unistd.h>
#ifndef _WIN32
#include <pwd.h>
#include <errno.h>
#endif

static FunctionListRec fnList[] =
	{
		//Every structure in array contains:
		//Return-Value    function name
		//parameter list
		//true == thread-save
		{
			INTEGER_VAR, "LhcVacSetDataPart", 
			"(string partName)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpInitPassive", 
			"(bool isLhcStyle, string pathName, string machineName, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpInitActive",
			"(string machine, string fileName, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpInitRackData",
			"(string machine, string fileName, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpInitInterlockData",
			"(string machine, string fileName, dyn_string &exceptionInfo)",
			false
		},
  		{
			INTEGER_VAR, "LhcVacAddGlobalEqp",
			"(string dpName, string dpeName, string visibleName, int funcType, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetMinSectorLength",
			"(string lineName, int minLength)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetTimeStamp",
			"(string &timeStamp)",
			false
		},
		{
			INTEGER_VAR, "LhcVacLinuxUserName",
			"(string &osUserName)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetPicturePath",
			"(string &path)",
			false
		},

		//////////////////////// Main parts, sectors, domains ///////////////////
		{
			INTEGER_VAR, "LhcVacGetAllMainParts",
			"(dyn_string &mainParts)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetSectorsOfMainPart",
			"(string mainPart, dyn_string &sectors)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetAllSectors",
			"(dyn_string &mainParts, dyn_dyn_string &sectors, dyn_dyn_string &domains, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetAllDomains",
			"(dyn_string &domains)",
			false
		},
		{
			INTEGER_VAR, "LhcVacPvssNameOfSector",
			"(string sector, string &pvssName)",
			false
		},
		{
			INTEGER_VAR, "LhcVacPvssNameOfMainPart",
			"(string mainPart, string &pvssName)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetVacTypeOfSector",
			"(string sector, int &vacType)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetVacTypeOfMainPart",
			"(string mainPart, int &vacType)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDomainsOfMainPart",
			"(string mainPartName, dyn_string &domains, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDomainsOfSector",
			"(string sectorName, dyn_string &domains, dyn_string &exceptionInfo)",
			false
		},
		/* L.Kopylov 25.05.2012
		{
			INTEGER_VAR, "LhcVacSetSectorSpecSynoptic",
			"(string sectorName)",
			false
		},
		*/
		{
			INTEGER_VAR, "LhcVacSetSectorSpecSynPanel",
			"(string sectorName, string fileName)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetSectorSpecSectPanel",
			"(string sectorName, string fileName)",
			false
		},
		{
			INTEGER_VAR, "LhcVacIsSectorInMainPart",
			"(string sectorName, string mainPartName)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetSectorsOfDomains",
			"(dyn_string domains, dyn_string &sectors)",
			false
		},

		/////////////////////////////////////////////////////////////////////////
		//////////////////////// Equipment //////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////
		{
			INTEGER_VAR, "LhcVacSetPvssFunctionalType", 
			"(string dpType, int type)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDpType", 
			"(string dpName, string &dpType, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacMobileProp", 
			"(string dpName, int &mobileType, bool &isActive, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDisplayName",
			"(string name, string &result, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDpName",
			"(string name, string &result, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDeviceLocation",
			"(string dpName, float &coord, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDeviceAccessPointDistance",
			"(string dpName, float &coord, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDeviceVacLocation",
			"(string dpName, int &vac, string &sector1, string &sector2, string &mainPart, bool &isBorder, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDeviceDomain",
			"(string dpName, dyn_string &domains, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetAllAttributes",
			"(string dpName, dyn_string &attrList)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetAttribute",
			"(string dpName, string attrName, string &value)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetCtlAttributes",
			"(string dpName, int &family, int &type, int &subType)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetByAttrValue",
			"(string attrName, string value, dyn_string &result)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpCtlStatus",
			"(string dpName, int mode)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpMaster",
			"(string dpName, string &masterDp, int &masterChannel)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpConfig",
			"(string dpName, string &configDp, int &configChannel)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpSlaves",
			"(string dpName, dyn_string &slaveDps)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpTargets",
			"(string dpName, dyn_string &targetDps)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpMasterProcess",
			"(string dpName, string &masterDpName, string &usageInMaster)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpStateDpes",
			"(string dpName, dyn_string &stateDpes)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpValueDpes",
			"(string dpName, dyn_string &valueDpes)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSelectEqp",
			"(string dpName)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetDevicesAtSectors",
			"(dyn_string sectors, dyn_string &dpNames, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetDevicesOfDpTypesAtSectors",
			"(dyn_string sectors, dyn_string dpTypes, dyn_string &dpNames, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetDevicesAtMainParts",
			"(dyn_string mainParts, dyn_string &dpNames, dyn_string &exceptionInfo )",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetDevicesMatchingFilter",
			"(string filter, dyn_string &dpNames, dyn_string &eqpNames)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetThermometersForValve",
			"(string valveDp, dyn_string &thermometerDps, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetNeighbourValves",
			"(string valveDp, dyn_string &valvesBefore, dyn_string &valvesAfter, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacAddBakeoutWorkDP",
			"(string dpName)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetMobileEqp",
			"(dyn_string targetDps, dyn_string workDps, dyn_string sourceDps, dyn_bool activeFlags)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetWireLessEqp",
			"(dyn_string instanceDps, dyn_string masterDps, dyn_string tcpDps)",
			false
		},
		{
			INTEGER_VAR, "LhcVacAllEqpWithSameVisName",
			"(string dpName, dyn_string &result)",
			false
		},
		{
			INTEGER_VAR, "LhcVacNextMobileHistoryQuery",
			"(time &startTime, time &endTime)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetMobileEqpHistory",
			"(dyn_dyn_string mobileDps, dyn_dyn_bool activeFlags)",
			false
		},
		{
			INTEGER_VAR, "LhcVacNextDpeHistoryDepthQuery",
			"(string &dpe, time &startTime, int &mobileType)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetDpeHistoryDepth",
			"(string dpName, time absStartTime)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpSetActive",
			"(string dpName, bool flag)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetEqpIconType",
			"(string dpName, int &type)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetEqpByMasterAndChannelNb",
			"(string masterDpName, int channelNb, string dpType, string &dpName)",
			false
		},

		//////////////////////// Online and replay data for equipment ///////////
		{
			INTEGER_VAR, "LhcVacNextDpConnect",
			"(string &dpe, bool &connect)",
			false
		},
		{
			INTEGER_VAR, "LhcVacNextDpPolling",
			"(string &dpe, bool &connect)",
			false
		},
		{
			INTEGER_VAR, "LhcVacNewOnlineValue",
			"(string dp, anytype value, time timeStamp)",
			false
		},
		{
			INTEGER_VAR, "LhcVacNewReplayValue",
			"(string dp, anytype value, time timeStamp)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpMainStateString",
			"(string dpName, int mode, string &result)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpPartStateString",
			"(string dpName, int part, int mode, string &result)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpMainValueString",
			"(string dpName, int mode, string &result)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpMainStateStringForMenu",
			"(string dpName, int mode, string &result)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpMainColor",
			"(string dpName, int mode, string &result)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpPartColor",
			"(string dpName, int part, int mode, string &result)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSubEqpColor",
			"(string dpName, string subEqpType, int mode, string &result)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpDpeValue",
			"(string dp, string dpe, anytype &value)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpPlcAlarm",
			"(string dp, bool &alarm)",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpGetHistoryState",
			"(int id, string dpName, time ts, string &state, string &color, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacNextHistoryQuery",
			"(int &id, string &dpe, bool &bitState, int &bitNbr, time &start, time &end)",
			false
		},
		{
			INTEGER_VAR, "LhcVacAddHistoryValues",
			"(int id, string dpe, dyn_time times, dyn_anytype values, dyn_string &exceptionInfo)",
			false
		},

		///////////////////////// Device methods for device list support
		{
			INTEGER_VAR, "LhcVacDevListGetDpes",
			"(string dp, dyn_string &dpes)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDevListCheckCrit",
			"(string dp)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDevListGetHistoryParam",
			"(string dp, string &dpe, time &startTime, time &endTime)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDevListCheckHistory",
			"(string dp, dyn_anytype values)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDevListCheckValueGrow",
			"(string dp, dyn_anytype values, dyn_time times, anytype &minValue, time &firstLargeValueTime)",
			false
		},

		///////////////////////// Open dialogs realized in Qt
		{
			INTEGER_VAR, "LhcVacOpenDialog",
			"(int type, unsigned vacTypes, string firstSector, string lastSector, int mode, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacOpenDocumentationDialog",
			"(int type, unsigned vacTypes, string equipmentName, int mode, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacMakeDeviceVisible",
			"(string dpName)",
			false
		},
		{
			INTEGER_VAR, "LhcVacCloseReplayDialogs",
			"()",
			false
		},
		{
			INTEGER_VAR, "LhcVacConfirmEvConnLost",
			"(time connLostTime)",
			false
		},

		/////////////////////////// History dialogs and their configuration
		{
			INTEGER_VAR, "LhcVacAddToHistoryDialog",
			"(string dpName, int historyDepth, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacAddToHistoryInstance",
			"(int instanceId, string dpName, string colorString, int historyDepth, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacAddDpeToHistoryDialog",
			"(string dpName, string dpeName, string yAxisName, int historyDepth, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacAddDpeToHistoryInstance",
			"(int instanceId, string dpName, string dpeName, string yAxisName, string colorString, int historyDepth, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacAddBitToHistoryDialog",
			"(string dpName, string dpeName, int bitNbr, string bitName, int historyDepth, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacAddBitToHistoryInstance",
			"(int instanceId, string dpName, string dpeName, int bitNbr, string bitName, string colorString, int historyDepth, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR, "LhcVacCreateHistoryInstance",
			"()",
			false
		},
		{
			INTEGER_VAR, "LhcVacClearHistoryInstance",
			"(int instanceId)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetHistoryFilterType",
			"(int instanceId)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetHistoryFilterType",
			"(int instanceId, int type)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetAllHistoryItems",
			"(int instanceId, dyn_string &dps, dyn_string &dpes, dyn_int &bitNbrs, dyn_string &colors)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetHistoryTimeScale",
			"(int instanceId, time &start, time &end, bool &online, bool &logScale)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetHistoryTimeScale",
			"(int instanceId, time start, time end, bool online, bool logScale)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetHistoryVerticalScales",
			"(int instanceId, dyn_int &types, dyn_bool &logs, dyn_float &mins, dyn_float &maxs)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetHistoryVerticalScale",
			"(int instanceId, int type, bool log, float min, float max)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetHistoryTitle",
			"(int instanceId, string &title)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetHistoryTitle",
			"(int instanceId, string title)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetHistoryGeometry",
			"(int instanceId, int &x, int &y, int &w, int &h)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetHistoryGeometry",
			"(int instanceId, int x, int y, int w, int h)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetAllHistoryInstances",
			"(dyn_int &allIds)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetHistoryConfigName",
			"(int instanceId, string name)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetHistoryConfigName",
			"(int instanceId, string &name)",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetHistoryAllComments",
			"(int instanceId, dyn_string &comments)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetHistoryAllComments",
			"(int instanceId, dyn_string comments)",
			false
		},

		// Mobile equipment
		{
			INTEGER_VAR, "LhcVacDpForMobileOnFlange",
			"(string sourceDp, string targetDpType, int locationIndex, string &targetDp, dyn_string &exceptionInfo )",
			false
		},
		{
			INTEGER_VAR, "LhcVacDpForMobileOnSector",
			"(string sourceDp, string targetDpType, int locationIndex, int indexInSector, string &targetDp, dyn_string &exceptionInfo )",
			false
		},

		// Other...
		{
			INTEGER_VAR, "LhcVacSetDebugMode",
			"(string mode, bool flag)",
			false
		},

		// [VACCO-948] [VACCO-1645]
		{
			INTEGER_VAR, "LhcVacChangeSectorState",
			"(string dp, bool value)",
			false
		},
		{
			INTEGER_VAR, "LhcVacDisplayNameOfSector",
			"(string dpName, string &displayName, dyn_string &exceptionInfo)",
			false
		}


		/*
		{
			INTEGER_VAR, "LhcVacGetDevicesAtSectBorders",
			"(dyn_string &dpNames, dyn_string &exceptionInfo )",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetIconGeom",
			"(string dpType, unsigned dpTypeMask, string fileName, int width, int height, int iconX, int iconY, int verticalPos )",
			false
		},
		{
			INTEGER_VAR, "LhcVacAnyMpOfVacSector",
			"(string sector, string &mainPart )",
			false
		},
		{
			INTEGER_VAR, "LhcVacFirstSectorOfMp",
			"(string mainPart, string &sector )",
			false
		},
		{
			INTEGER_VAR, "LhcVacNextVacSector",
			"(string sector, string mainPart, string &nextSector )",
			false
		},
		{
			INTEGER_VAR, "LhcVacPrevVacSector",
			"(string sector, string mainPart, string &nextSector )",
			false
		},
		{
			INTEGER_VAR, "LhcVacAreSectorsOfMpContinuous",
			"(string mainPart, bool &result )",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetHistoryData",
			"(int handle, string dpName, dyn_float values, dyn_time times, dyn_string &exceptionInfo )",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetHistoryData",
			"(int handle, time &startTime, dyn_string &dps, dyn_dyn_time &times, dyn_dyn_float &values, dyn_string &exceptionInfo )",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetChildSectRef",
			"(string dpName, bool isSectBefore, bool isSectAfter, bool &isChanged )",
			false
		},
		{
			INTEGER_VAR, "LhcVacBuildRangeFromSector",
			"(string sector, int &vacType, string &firstSector, string &lastSector, dyn_string &sectorList )",
			false
		},
		{
			INTEGER_VAR, "LhcVacBuildRangeFromMp",
			"(string mainPart, int &vacType, string &firstSector, string &lastSector, dyn_string &sectorList )",
			false
		},
		{
			INTEGER_VAR, "LhcVacFileVersion",
			"(string fileName, string &fileVersion)",
			false
		},
		{
			INTEGER_VAR, "LhcVacParseMobileConnect",
			"(time startTime, time endTime, dyn_dyn_string dpNames, dyn_time nameTimes, dyn_dyn_bool dpStates, dyn_time stateTimed, dyn_string &mobDpNames, dyn_dyn_time &mobIntervals)",
			false
		},
		{
			INTEGER_VAR, "LhcVacSetHistoryChildSectRef",
			"(string dpName, bool isSectBefore, bool isSectAfter, bool &isChanged )",
			false
		},
		{
			INTEGER_VAR, "LhcVacEqpInLhcRange",
			"(string startSectorName, string endSectorName, string dpType, dyn_string &dpNames)",
			false
		},
	
		{
			INTEGER_VAR, "LhcVacGetDevicesOfTypeAtSectors",
			"(dyn_string sectors, unsigned dpTypeMask, dyn_string &result, dyn_string &exceptionInfo )",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetDevicesOfTypeAtMainParts",
			"(dyn_string mainParts, unsigned dpTypeMask, dyn_string &result, dyn_string &exceptionInfo )",
			false
		},
		{
			INTEGER_VAR, "LhcVacGetCryoVacOkPairs",
			"(unsigned dpTypeMask, dyn_string &pairs, dyn_string &singles)",
			false
		},
		*/
	};

/*
	// this line counts the number of functions you want to implement.
  	PVSSulong funcCount = sizeof(fnList) / sizeof(FunctionListRec);

	// now allocate the new instance
	return new VacCtlEqpDataPvss(nextHdl, funcCount, fnList);
}
*/

CTRL_EXTENSION(VacCtlEqpDataPvss, fnList)

/*
**
** FUNCTION
**		The following description has been borrowed from PVSS help:
**
**		This function is called every time we use one of the above
**		function names in a Ctrl script.
**		The argument is an aggregaton of function name, function number, 
**		the arguments passed to the Ctrl function, the "thread" context 
**		and user defined data.
**
** PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
** RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
const Variable *VacCtlEqpDataPvss::execute(ExecuteParamRec &param)
{
	enum
	{
		LhcVacSetDataPart = 0,
		LhcVacInitPassive,
		LhcVacInitActive,
		LhcVacInitRackData,
		LhcVacInitInterlockData,
		LhcVacAddGlobalEqp,
		LhcVacSetMinSectorLength,
		LhcVacGetTimeStamp,
		LhcVacLinuxUserName,
		LhcVacSetPicturePath,

		LhcVacGetAllMainParts,
		LhcVacGetSectorsOfMainPart,
		LhcVacGetAllSectors,
		LhcVacGetAllDomains,
		LhcVacPvssNameOfSector,
		LhcVacPvssNameOfMainPart,
		LhcVacGetVacTypeOfSector,
		LhcVacGetVacTypeOfMainPart,
		LhcVacDomainsOfMainPart,
		LhcVacDomainsOfSector,
		/* L.Kopylov 25.05.2012
		LhcVacSetSectorSpecSynoptic,
		*/
		LhcVacSetSectorSpecSynPanel,
		LhcVacSetSectorSpecSectPanel,
		LhcVacIsSectorInMainPart,
		LhcVacGetSectorsOfDomains,

		LhcVacSetPvssFunctionalType,
		LhcVacDpType,
		LhcVacMobileProp,
		LhcVacDisplayName,
		LhcVacDpName,
		LhcVacDeviceLocation,
		LhcVacDeviceAccessPointDistance,
		LhcVacDeviceVacLocation,
		LhcVacDeviceDomain,
		LhcVacGetAllAttributes,
		LhcVacGetAttribute,
		LhcVacGetCtlAttributes,
		LhcVacGetByAttrValue,
		LhcVacEqpCtlStatus,
		LhcVacEqpMaster,
		LhcVacEqpConfig,
		LhcVacEqpSlaves,
		LhcVacEqpTargets,
		LhcVacEqpMasterProcess,
		LhcVacEqpStateDpes,
		LhcVacEqpValueDpes,
		LhcVacSelectEqp,
		LhcVacGetDevicesAtSectors,
		LhcVacGetDevicesOfDpTypesAtSectors,
		LhcVacGetDevicesAtMainParts,
		LhcVacGetDevicesMatchingFilter,
		LhcVacGetThermometersForValve,
		LhcVacGetNeighbourValves,
		LhcVacAddBakeoutWorkDP,
		LhcVacSetMobileEqp,
		LhcVacSetWireLessEqp,
		LhcVacAllEqpWithSameVisName,
		LhcVacNextMobileHistoryQuery,
		LhcVacSetMobileEqpHistory,
		LhcVacNextDpeHistoryDepthQuery,
		LhcVacSetDpeHistoryDepth,
		LhcVacEqpSetActive,
		LhcVacGetEqpIconType,
		LhcVacGetEqpByMasterAndChannelNb,

		LhcVacNextDpConnect,
		LhcVacNextDpPolling,
		LhcVacNewOnlineValue,
		LhcVacNewReplayValue,
		LhcVacEqpMainStateString,
		LhcVacEqpPartStateString,
		LhcVacEqpMainValueString,
		LhcVacEqpMainStateStringForMenu,
		LhcVacEqpMainColor,
		LhcVacEqpPartColor,
		LhcVacSubEqpColor,
		LhcVacEqpDpeValue,
		LhcVacEqpPlcAlarm,
		LhcVacEqpGetHistoryState,
		LhcVacNextHistoryQuery,
		LhcVacAddHistoryValues,

		LhcVacDevListGetDpes,
		LhcVacDevListCheckCrit,
		LhcVacDevListGetHistoryParam,
		LhcVacDevListCheckHistory,
		LhcVacDevListCheckValueGrow,

		LhcVacOpenDialog,
		LhcVacOpenDocumentationDialog,
		LhcVacMakeDeviceVisible,
		LhcVacCloseReplayDialogs,
		LhcVacConfirmEvConnLost,
		LhcVacAddToHistoryDialog,
		LhcVacAddToHistoryInstance,
		LhcVacAddDpeToHistoryDialog,
		LhcVacAddDpeToHistoryInstance,
		LhcVacAddBitToHistoryDialog,
		LhcVacAddBitToHistoryInstance,
		LhcVacCreateHistoryInstance,
		LhcVacClearHistoryInstance,
		LhcVacGetHistoryFilterType,
		LhcVacSetHistoryFilterType,
		LhcVacGetAllHistoryItems,
		LhcVacGetHistoryTimeScale,
		LhcVacSetHistoryTimeScale,
		LhcVacGetHistoryVerticalScales,
		LhcVacSetHistoryVerticalScale,
		LhcVacGetHistoryTitle,
		LhcVacSetHistoryTitle,
		LhcVacGetHistoryGeometry,
		LhcVacSetHistoryGeometry,
		LhcVacGetAllHistoryInstances,
		LhcVacSetHistoryConfigName,
		LhcVacGetHistoryConfigName,
		LhcVacGetHistoryAllComments,
		LhcVacSetHistoryAllComments,
		LhcVacDpForMobileOnFlange,
		LhcVacDpForMobileOnSector,
		LhcVacSetDebugMode,
		LhcVacChangeSectorState, // [VACCO-948] [VACCO-1645]
		LhcVacDisplayNameOfSector, // [VACCO-948] [VACCO-1645]
		LhcVacGetRackEquipment


		/*
		LhcVacGetDevicesAtSectBorders,
		LhcVacSetIconGeom,
		LhcVacAnyMpOfVacSector,
		LhcVacFirstSectorOfMp,
		LhcVacNextVacSector,
		LhcVacPrevVacSector,
		LhcVacAreSectorsOfMpContinuous,
		LhcVacSetHistoryData,
		LhcVacGetHistoryData,
		LhcVacSetChildSectRef,
		LhcVacBuildRangeFromSector,
		LhcVacBuildRangeFromMp,
		LhcVacChangeRangeType,
		LhcVacChangeRangeLimits,
		LhcVacCanExtendRange,
		LhcVacAnalyzeTypes,
		LhcVacFileVersion,
		LhcVacParseMobileConnect,
		LhcVacSetHistoryChildSectRef,
		LhcVacEqpInLhcRange,
		LhcVacGetDevicesOfTypeAtSectors,
		LhcVacGetDevicesOfTypeAtMainParts,
		LhcVacGetCryoVacOkPairs
		*/
	};

	// TODO for the different return types you need corresponding static Variables
	static IntegerVar	integerVar;
	int					coco;

	integerVar.setValue(0);
	param.thread->clearLastError( );
	switch (param.funcNum)
	{
	case LhcVacSetDataPart:
		coco = processSetDataPart(param);
		break;
	case LhcVacInitPassive:
		coco = processInitPassive(param);
		break;
	case LhcVacInitActive:
		coco = processInitActive(param);
		break;
	case LhcVacInitRackData:
		coco = processInitRackData(param);
		break;
	case LhcVacInitInterlockData:
		coco = processInitInterlockData(param);
		break;
	case LhcVacAddGlobalEqp:
		coco = processAddGlobalEqp(param);
		break;
	case LhcVacSetMinSectorLength:
		coco = processSetMinSectorLength(param);
		break;
	case LhcVacGetTimeStamp:
		coco = processGetTimeStamp(param);
		break;
	case LhcVacLinuxUserName:
		coco = processLinuxUserName(param);
		break;
	case LhcVacSetPicturePath:
		coco = processSetPicturePath(param);
		break;

	case LhcVacGetAllMainParts:
		coco = processGetAllMainParts(param);
		break;
	case LhcVacGetSectorsOfMainPart:
		coco = processGetSectorsOfMainPart(param);
		break;
	case LhcVacGetAllSectors:
		coco = processGetAllSectors(param);
		break;
	case LhcVacGetAllDomains:
		coco = processGetAllDomains(param);
		break;
	case LhcVacPvssNameOfSector:
		coco = processPvssNameOfSector(param);
		break;
	case LhcVacPvssNameOfMainPart:
		coco = processPvssNameOfMainPart(param);
		break;
	case LhcVacGetVacTypeOfSector:
		coco = processVacTypeOfSector(param);
		break;
	case LhcVacGetVacTypeOfMainPart:
		coco = processVacTypeOfMainPart(param);
		break;
	case LhcVacDomainsOfMainPart:
		coco = processDomainsOfMainPart(param);
		break;
	case LhcVacDomainsOfSector:
		coco = processDomainsOfSector(param);
		break;
	/* L.Kopylov 25.05.2012
	case LhcVacSetSectorSpecSynoptic:
		coco = processSetSectorSpecSynoptic(param);
		break;
	*/
	case LhcVacSetSectorSpecSynPanel:
		coco = processSetSectorSpecSynPanel(param);
		break;
	case LhcVacSetSectorSpecSectPanel:
		coco = processSetSectorSpecSectPanel(param);
		break;
	case LhcVacIsSectorInMainPart:
		coco = processIsSectorInMainPart(param);
		break;
	case LhcVacGetSectorsOfDomains:
		coco = processGetSectorsOfDomains(param);
		break;

	case LhcVacSetPvssFunctionalType:
		coco = processSetPvssFunctionalType(param);
		break;
	case LhcVacDpType:
		coco = processDpType(param);
		break;
	case LhcVacMobileProp:
		coco = processMobileProp(param);
		break;
	case LhcVacDisplayName:
		coco = processDisplayName(param);
		break;
	case LhcVacDpName:
		coco = processDpName(param);
		break;
	case LhcVacDeviceLocation:
		coco = processDeviceLocation(param);
		break;
	case LhcVacDeviceAccessPointDistance:
		coco = processDeviceAccessPointDistance(param);
		break;
	case LhcVacDeviceVacLocation:
		coco = processDeviceVacLocation(param);
		break;
	case LhcVacDeviceDomain:
		coco = processDeviceDomain(param);
		break;
	case LhcVacGetAllAttributes:
		coco = processGetAllAttributes(param);
		break;
	case LhcVacGetAttribute:
		coco = processGetAttribute(param);
		break;
	case LhcVacGetCtlAttributes:
		coco = processGetCtlAttributes(param);
		break;
	case LhcVacGetByAttrValue:
		coco = processGetByAttrValue(param);
		break;
	case LhcVacEqpCtlStatus:
		coco = processEqpCtlStatus(param);
		break;
	case LhcVacEqpMaster:
		coco = processEqpMaster(param);
		break;
	case LhcVacEqpConfig:
		coco = processEqpConfig(param);
		break;
	case LhcVacEqpSlaves:
		coco = processEqpSlaves(param);
		break;
	case LhcVacEqpTargets:
		coco = processEqpTargets(param);
		break;
	case LhcVacEqpMasterProcess:
		coco = processEqpMasterProcess(param);
		break;
	case LhcVacEqpStateDpes:
		coco = processEqpStateDpes(param);
		break;
	case LhcVacEqpValueDpes:
		coco = processEqpValueDpes(param);
		break;
	case LhcVacSelectEqp:
		coco = processSelectEqp(param);
		break;
	case LhcVacGetDevicesAtSectors:
		coco = processGetDevicesAtSectors(param);
		break;
	case LhcVacGetDevicesOfDpTypesAtSectors:
		coco = processGetDevicesOfDpTypesAtSectors(param);
		break;
	case LhcVacGetDevicesAtMainParts:
		coco = processGetDevicesAtMainParts(param);
		break;
	case LhcVacGetDevicesMatchingFilter:
		coco = processGetDevicesMatchingFilter(param);
		break;
	case LhcVacGetThermometersForValve:
		coco = processGetThermometersForValve(param);
		break;
	case LhcVacGetNeighbourValves:
		coco = processGetNeighbourValves(param);
		break;
	case LhcVacAddBakeoutWorkDP:
		coco = processAddBakeoutWorkDP(param);
		break;
	case LhcVacSetMobileEqp:
		coco = processSetMobileEqp(param);
		break;
	case LhcVacSetWireLessEqp:
		coco = processSetWireLessEqp(param);
		break;
	case LhcVacAllEqpWithSameVisName:
		coco = processAllEqpWithSameVisName(param);
		break;
	case LhcVacNextMobileHistoryQuery:
		coco = processNextMobileHistoryQuery(param);
		break;
	case LhcVacSetMobileEqpHistory:
		coco = processSetMobileEqpHistory(param);
		break;
	case LhcVacNextDpeHistoryDepthQuery:
		coco = processNextDpeHistoryDepthQuery(param);
		break;
	case LhcVacSetDpeHistoryDepth:
		coco = processSetDpeHistoryDepth(param);
		break;
	case LhcVacEqpSetActive:
		coco = processEqpSetActive(param);
		break;
	case LhcVacGetEqpIconType:
		coco = processGetEqpIconType(param);
		break;
	case LhcVacGetEqpByMasterAndChannelNb:
		coco = processGetEqpByMasterAndChannelNb(param);
		break;
	case LhcVacNextDpConnect:
		coco = processNextDpConnect(param);
		break;
	case LhcVacNextDpPolling:
		coco = processNextDpPolling(param);
		break;
	case LhcVacNewOnlineValue:
		coco = processNewOnlineValue(param);
		break;
	case LhcVacNewReplayValue:
		coco = processNewReplayValue(param);
		break;
	case LhcVacEqpMainStateString:
		coco = processEqpMainStateString(param);
		break;
	case LhcVacEqpPartStateString:
		coco = processEqpPartStateString(param);
		break;
	case LhcVacEqpMainValueString:
		coco = processEqpMainValueString(param);
		break;
		
	case LhcVacEqpMainStateStringForMenu:
		coco = processEqpMainStateStringForMenu(param);
		break;
	case LhcVacEqpMainColor:
		coco = processEqpMainColor(param);
		break;
	case LhcVacEqpPartColor:
		coco = processEqpPartColor(param);
		break;
	case LhcVacSubEqpColor:
		coco = processSubEqpColor(param);
		break;
	case LhcVacEqpDpeValue:
		coco = processEqpDpeValue(param);
		break;
	case LhcVacEqpPlcAlarm:
		coco = processEqpPlcAlarm(param);
		break;
	case LhcVacEqpGetHistoryState:
		coco = processEqpGetHistoryState(param);
		break;
	case LhcVacNextHistoryQuery:
		coco = processNextHistoryQuery(param);
		break;
	case LhcVacAddHistoryValues:
		coco = processAddHistoryValues(param);
		break;

	case LhcVacDevListGetDpes:
		coco = processDevListGetDpes(param);
		break;
	case LhcVacDevListCheckCrit:
		coco = processDevListCheckCrit(param);
		break;
	case LhcVacDevListGetHistoryParam:
		coco = processDevListGetHistoryParam(param);
		break;
	case LhcVacDevListCheckHistory:
		coco = processDevListCheckHistory(param);
		break;
	case LhcVacDevListCheckValueGrow:
		coco = processDevListCheckValueGrow(param);
		break;

	case LhcVacOpenDialog:
		coco = processOpenDialog(param);
		break;
	case LhcVacOpenDocumentationDialog:
		coco = processOpenDocumentationDialog(param);
		break;
	case LhcVacMakeDeviceVisible:
		coco = processMakeDeviceVisible(param);
		break;
	case LhcVacCloseReplayDialogs:
		VacMainView::deleteReplayDialogs();
		coco = 0;
		break;
	case LhcVacConfirmEvConnLost:
		coco = processConfirmEvConnLost(param);
		break;

	case LhcVacAddToHistoryDialog:
		coco = processAddToHistoryDialog(param);
		break;
	case LhcVacAddToHistoryInstance:
		coco = processAddToHistoryInstance(param);
		break;
	case LhcVacAddDpeToHistoryDialog:
		coco = processAddDpeToHistoryDialog(param);
		break;
	case LhcVacAddDpeToHistoryInstance:
		coco = processAddDpeToHistoryInstance(param);
		break;
	case LhcVacAddBitToHistoryDialog:
		coco = processAddBitToHistoryDialog(param);
		break;
	case LhcVacAddBitToHistoryInstance:
		coco = processAddBitToHistoryInstance(param);
		break;
	case LhcVacCreateHistoryInstance:
		coco = processCreateHistoryInstance(param);
		break;
	case LhcVacClearHistoryInstance:
		coco = processClearHistoryInstance(param);
		break;
	case LhcVacGetHistoryFilterType:
		coco = processGetHistoryFilterType(param);
		break;
	case LhcVacSetHistoryFilterType:
		coco = processSetHistoryFilterType(param);
		break;
	case LhcVacGetAllHistoryItems:
		coco = processGetAllHistoryItems(param);
		break;
	case LhcVacGetHistoryTimeScale:
		coco = processGetHistoryTimeScale(param);
		break;
	case LhcVacSetHistoryTimeScale:
		coco = processSetHistoryTimeScale(param);
		break;
	case LhcVacGetHistoryVerticalScales:
		coco = processGetHistoryVerticalScales(param);
		break;
	case LhcVacSetHistoryVerticalScale:
		coco = processSetHistoryVerticalScale(param);
		break;
	case LhcVacGetHistoryTitle:
		coco = processGetHistoryTitle(param);
		break;
	case LhcVacSetHistoryTitle:
		coco = processSetHistoryTitle(param);
		break;
	case LhcVacGetHistoryGeometry:
		coco = processGetHistoryGeometry(param);
		break;
	case LhcVacSetHistoryGeometry:
		coco = processSetHistoryGeometry(param);
		break;
	case LhcVacGetAllHistoryInstances:
		coco = processGetAllHistoryInstances(param);
		break;
	case LhcVacSetHistoryConfigName:
		coco = processSetHistoryConfigName(param);
		break;
	case LhcVacGetHistoryConfigName:
		coco = processGetHistoryConfigName(param);
		break;
	case LhcVacGetHistoryAllComments:
		coco = progessGetHistoryAllComments(param);
		break;
	case LhcVacSetHistoryAllComments:
		coco = progessSetHistoryAllComments(param);
		break;

	case LhcVacDpForMobileOnFlange:
		coco = processDpForMobileOnFlange(param);
		break;
	case LhcVacDpForMobileOnSector:
		coco = processDpForMobileOnSector(param);
		break;
	case LhcVacSetDebugMode:
		coco = processSetDebugMode(param);
		break;
	case LhcVacChangeSectorState: // [VACCO-948] [VACCO-1645]
		coco = processChangeSectorState(param);
		break;
	case LhcVacDisplayNameOfSector:
		coco = processDisplayNameOfSector(param);
		break;

	default:
		coco = -1;
		break;
	}
	integerVar.setValue(coco);
	return &integerVar;
}
/**
@defgroup LhcVacSetDataPart LhcVacSetDataPart()
@section s int LhcVacSetDataPart(string partName)
@subsection ss Purpose
	Split all geography information, read from files, to two parts: visible and invisible. 
	The DataPool is initialized with all data from configuration files, but only beam lines, 
	linked to spefied dataPart will be 'visible'. 
	One (and the only) example where this is needed is WinCC OA project for Complex PS; in fact, 
	this project controls two independent installations: 
	Complex PS; CTF
@subsection arg WCCOA CTRL Arguments
	\b partName: 		The name of data part which shall be used; other data parts will be hidden \n
@subsection ret Return
	The function always returns 0, the value returned is meaningless.
@subsection caut Caution
	The function shall called before calling LhcVacEqpInitPassive().
@subsection ex Usage Example
	LhcVacSetDataPart("PS"); \n
	... \n
	\\\ Usually in main view panels it is used as following, to use $-parameter supplied to panel:
	if(isDollarDefined("$dataPart")) { \n
	LhcVacSetDataPart($dataPart); \n
	} \n
*/
/**
@brief GENERIC API Process call to LhcVacSetDataPart
*/
int VacCtlEqpDataPvss::processSetDataPart(ExecuteParamRec &param)
{
	TextVar		partName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "partName",		TEXT_VAR,		&partName,		NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	DataPool &pool = DataPool::getInstance();
	pool.setDataPart(partName);
	return 0;
}
/**
@defgroup LhcVacEqpInitPassive LhcVacEqpInitPassive()
@section s int LhcVacEqpInitPassive(string isLhcStyle, string pathName, string machineName, dyn_string &exceptionInfo)
@subsection ss Purpose
	Initialize non-vacuum equipment data from files, build beam lines and all non-controllable equipment in lines. 
	Note that names of input files are hardcoded: they are built from machine name accoding to predefined rules.
@subsection arg WCCOA CTRL Arguments
	\b isLhcStyle: 		true if input file has been generated from LHC DB,  false if file has been generated from SURVEY DB \n
	\b pathName: 		name of path where all required files shall be located \n
	\b machineName: 	name of machine for which data are read \n
	\b exceptionInfo:	details of any exceptions are returned here \n
@subsection ret Return
	The function always returns 0, the value returned is meaningless.
@subsection caut Caution
	None
@subsection ex Usage Example
	bool isLhcData = glAccelerator == "LHC" ? true : false; \n
	\\\ Initalize passive machine data \n
	ret = LhcVacEqpInitPassive( isLhcData, PROJ_PATH + "data", glAccelerator, exceptionInfo ); \n
*/
/**
@brief GENERIC API Process call to LhcVacInitPassive
*/
int VacCtlEqpDataPvss::processInitPassive(ExecuteParamRec &param)
{
	int			coco;
	BitVar		isLhcStyle;
	TextVar		pathName, machineName;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "isLhcStyle",		BIT_VAR,		&isLhcStyle,	NULL },
		{ "pathName",		TEXT_VAR,		&pathName,		NULL },
		{ "machineName",	TEXT_VAR,		&machineName,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QStringList errList;
	DataPool &pool = DataPool::getInstance();
	coco = pool.initPassive(isLhcStyle.getValue() != PVSS_FALSE, false, pathName, machineName, errList);
	addExceptionInfo(param, exceptionInfo, errList);
	return coco;
}
/**
@defgroup LhcVacEqpInitActive LhcVacEqpInitActive()
@section s int LhcVacEqpInitActive(string machine, string fileName, dyn_string &exceptionInfo) 
@subsection ss Purpose
	Initialize controllable equipment data from file, build internal lists of main parts and sectors
@subsection arg WCCOA CTRL Arguments
	\b machine: 		name of machine to be used \n
	\b fileName: 		name of configuration file  to be used. The name of file is build according to rule: AcceleratorName_for_DLL \n
	\b exceptionInfo:	details of any exceptions are returned here \n
@subsection ret Return
	The function always returns 0, the value returned is meaningless.
@subsection caut Caution
	The function shall called after calling  LhcVacEqpInitPassive
@subsection ex Usage Example
glAccelerator = "LHC"; \n
\\\ Initalize active machine data \n
ret = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data" + glAccelerator + ".for_DLL",  exceptionInfo); \n
*/
/**
@brief GENERIC API Process call to LhcVacEqpInitActive
*/
int VacCtlEqpDataPvss::processInitActive(ExecuteParamRec &param)
{
	int			coco;
	TextVar		machine, fileName;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "machine",		TEXT_VAR,		&machine,		NULL },
		{ "fileName",		TEXT_VAR,		&fileName,		NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QStringList errList;
	DataPool &pool = DataPool::getInstance();
	coco = pool.initActive(machine, fileName, errList);
	addExceptionInfo(param, exceptionInfo, errList);
	return coco;
}
/**
@defgroup LhcVacEqpInitDocData LhcVacEqpInitDocData()
@section s int LhcVacEqpInitDocData(string machine, string fileName, dyn_string &exceptionInfo)
@subsection ss Purpose
Initialize data for the rack layout feature
@subsection arg WCCOA CTRL Arguments
\b machine: 		name of machine to be used \n
\b fileName: 		name of configuration file  to be used. File with configuration to build the rack layout \n
\b exceptionInfo:	details of any exceptions are returned here \n
@subsection ret Return
The function always returns 0, the value returned is meaningless.
@subsection caut Caution
The function is usually called in main panel or in a script.
@subsection ex Usage Example
glAccelerator = "LHC"; \n
\\\ Initalize active rack layout data \n
ret = LhcVacEqpInitDocData(glAccelerator,PROJ_PATH + \"/data/\" + glAccelerator + \".for_Racks\",exceptionInfo); \n
*/
/**
@brief GENERIC API Process call to LhcVacEqpInitDocData
*/
int VacCtlEqpDataPvss::processInitRackData(ExecuteParamRec &param)
{
	int			coco;
	TextVar		machine, fileName;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "machine", TEXT_VAR, &machine, NULL },
		{ "fileName", TEXT_VAR, &fileName, NULL },
		{ "exceptionInfo", DYNTEXT_VAR, NULL, &exceptionInfo }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QStringList errList;
	DataPool &pool = DataPool::getInstance();
	coco = pool.initRackData(machine, fileName, errList);
	addExceptionInfo(param, exceptionInfo, errList);
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacEqpInitInterlockData
**		PVSS CTRL arguments are:
**			string machine
**			string fileName
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processInitInterlockData(ExecuteParamRec &param)
{
	int			coco;
	TextVar		machine, fileName;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "machine", TEXT_VAR, &machine, NULL },
		{ "fileName", TEXT_VAR, &fileName, NULL },
		{ "exceptionInfo", DYNTEXT_VAR, NULL, &exceptionInfo }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QStringList errList;
	DataPool &pool = DataPool::getInstance();
	coco = pool.initInterlockData(machine, fileName, errList);
	addExceptionInfo(param, exceptionInfo, errList);
	return coco;
}
/*
**	FUNCTION
**		Process call to LhcVacAddGlobalEqp()
**		PVSS CTRL arguments are:
**			string dpName
**			string dpeName
**			string visibleName
**			int funcType
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processAddGlobalEqp(ExecuteParamRec &param)
{
	TextVar		dpName, dpeName, visibleName;
	IntegerVar	funcType;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "dpeName",		TEXT_VAR,		&dpeName,		NULL },
		{ "visibleName",	TEXT_VAR,		&visibleName,	NULL },
		{ "funcType",		INTEGER_VAR,	&funcType,		NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	QString errMsg;
	DataPool &pool = DataPool::getInstance();
	int coco = pool.addGlobalDevice((const char *)dpName, (const char *)dpeName, (const char *)visibleName, funcType.getValue(), errMsg);
	if(!errMsg.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, errMsg);
	}
	return coco;
}

/*
** FUNCTION
**		Process call to LhcVacSetMinSectorLength()
**		PVSS CTRL arguments are:
**			string lineName
**			int minLength
**
** PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
** RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetMinSectorLength(ExecuteParamRec &param)
{
	TextVar		lineName;
	IntegerVar	minLength;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "lineName",	TEXT_VAR,		&lineName,	NULL },
		{ "minLength",	INTEGER_VAR,	&minLength,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	// Execute and return output arguments
	DataPool &pool = DataPool::getInstance();
	pool.setMinSectorLength(lineName, minLength);
	return 0;
}


/*
**	FUNCTION
**		Process call to 	LhcVacGetTimeStamp:
**		PVSS CTRL arguments are:
**			string &timeStamp
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetTimeStamp(ExecuteParamRec &param)
{
	Variable	*timeStamp;

	if(!(timeStamp = getOutArg(param, "timeStamp", TEXT_VAR, true)))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	if(pool.getTimeStamp())
	{
		((TextVar *)timeStamp)->setValue(pool.getTimeStamp());
	}
	else
	{
		((TextVar *)timeStamp)->setValue("");
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to 	LhcVacLinuxUserName()
**		PVSS CTRL arguments are:
**			string &osUserName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processLinuxUserName(ExecuteParamRec &param)
{
	Variable	*osUserName;

	if(!(osUserName = getOutArg(param, "osUserName", TEXT_VAR, true)))
	{
		return -1;
	}

#ifndef _WIN32
	struct passwd *pPwd = getpwuid(getuid());
	if(!pPwd)
	{
		char buf[256];
		sprintf(buf, "getpwuid() failed, errno %d", errno);
		showError(param, ErrClass::PARAMETERERROR, buf);
		((TextVar *)osUserName)->setValue("");
		return -1;
	}
	((TextVar *)osUserName)->setValue(pPwd->pw_name);
#else
	((TextVar *)osUserName)->setValue("NIY");
#endif
	return 0;
}

/*
**	FUNCTION
**		Process call to 	LhcVacSetPicturePath
**		PVSS CTRL arguments are:
**			string path
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetPicturePath(ExecuteParamRec &param)
{
	TextVar		path;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "path",	TEXT_VAR,	&path,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	DataPool &pool = DataPool::getInstance();
	QString realPath((const char *)path);
	if((!realPath.endsWith("\\")) && (!realPath.endsWith("/")))
	{
		realPath += "\\";
	}
	realPath = FileParser::pathToSystem(realPath.toLatin1());
	pool.setPicturePath(realPath.toLatin1());
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacGetMainParts()
**		PVSS CTRL arguments are:
**			dyn_string &mainParts
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetAllMainParts(ExecuteParamRec &param)
{
	Variable	*mainParts;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "mainParts",		DYNTEXT_VAR,	NULL,	&mainParts }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)mainParts)->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<MainPart *> &mpList = pool.getMainParts();
	if(mpList.isEmpty())
	{
		return -1;
	}
	const QList<Sector *> &sectors = pool.getSectors();
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		// Ignore DSL sector(s)
		if(pSector->getVacType() == VacType::DSL)
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			TextVar mpName;
			mpName.setValue(pMap->getMainPart()->getName());
			if(((DynVar *)mainParts)->isIn(mpName) != PVSS_TRUE)
			{
				((DynVar *)mainParts)->append(mpName);
			}
		}
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacGetSectorsOfMainPart()
**		PVSS CTRL arguments are:
**			string mainPart
**			dyn_string &sectors
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetSectorsOfMainPart(ExecuteParamRec &param)
{
	TextVar		mainPart;
	Variable	*sectors;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "mainPart",		TEXT_VAR,		&mainPart,	NULL },
		{ "sectors",		DYNTEXT_VAR,	NULL,	&sectors }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)sectors)->clear();
	DataPool &pool = DataPool::getInstance();
	MainPart *pMainPart = pool.findMainPartData(mainPart);
	if(!pMainPart)
	{
		return 0;
	}
	const QList<Sector *> &allSectors = pool.getSectors();
	for(int sectIdx = 0 ; sectIdx < allSectors.count() ; sectIdx++)
	{
		Sector *pSector = allSectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		// Ignore DSL sector(s)
		if(pSector->getVacType() == VacType::DSL)
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			if(pMap->getMainPart() != pMainPart)
			{
				continue;
			}
			addToDynStringVar((DynVar *)sectors, pSector->getName());
			break;
		}
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacGetAllSectors()
**		PVSS CTRL arguments are:
**			dyn_string &mainParts
**			dyn_dyn_string &sectors
**			dyn_dyn_string &domains
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetAllSectors(ExecuteParamRec &param)
{
	Variable	*mainParts, *sectors, *domains, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "mainParts",		DYNTEXT_VAR,	NULL,	&mainParts },
		{ "sectors",		DYNDYNTEXT_VAR,	NULL,	&sectors },
		{ "domains",		DYNDYNTEXT_VAR,	NULL,	&domains },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,	&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)mainParts)->clear();
	((DynVar *)sectors)->clear();
	((DynVar *)domains)->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<MainPart *> &mpList = pool.getMainParts();
	if(mpList.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, "Not initialized");
		return 0;
	}
	const QList<Sector *> &allSectors = pool.getSectors();
	for(int sectIdx = 0 ; sectIdx < allSectors.count() ; sectIdx++)
	{
		Sector *pSector = allSectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		// Ignore DSL sector(s)
		if(pSector->getVacType() == VacType::DSL)
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		int	mpIdx;
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			TextVar mpName;
			mpName.setValue(pMap->getMainPart()->getName());
			if(((DynVar *)mainParts)->isIn(mpName) == PVSS_TRUE)
			{
				for(mpIdx = ((DynVar *)mainParts)->getArrayLength( ) - 1 ; mpIdx >= 0 ; mpIdx--)
				{
					TextVar *pTextVar = (TextVar *)((DynVar *)mainParts)->getAt(mpIdx);
					if(!strcmp(pMap->getMainPart()->getName(), pTextVar->getValue()))
					{
						break;
					}
				}
				DynVar *pDynVar = (DynVar *)((DynVar *)sectors)->getAt(mpIdx);
				addToDynStringVar(pDynVar, pSector->getName());
				pDynVar = (DynVar *)((DynVar *)domains)->getAt(mpIdx);
				addToDynStringVar(pDynVar, pMap->getDomain().toLatin1());
			}
			else
			{
				((DynVar *)mainParts)->append(mpName);
				mpIdx = ((DynVar *)mainParts)->getArrayLength() - 1;
				DynVar dynVar;
				dynVar.clear();
				addToDynStringVar(&dynVar, pSector->getName());
				((DynVar *)sectors)->append(dynVar);
				dynVar.clear();
				addToDynStringVar(&dynVar, pMap->getDomain().toLatin1());
				((DynVar *)domains)->append(dynVar);
			}
		}
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacGetAllDomains()
**		PVSS CTRL arguments are:
**			dyn_string &domains
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetAllDomains(ExecuteParamRec &param)
{
	Variable	*domains;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "domains",		DYNTEXT_VAR,	NULL,	&domains }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)domains)->clear();
	DataPool &pool = DataPool::getInstance();
	const QStringList &allDomains = pool.getDomains();
	foreach(QString domain, allDomains)
	{
		addToDynStringVar((DynVar *)domains, domain.toLatin1());
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacPvssNameOfSector()
**		PVSS CTRL arguments are:
**			string sector
**			string &pvssName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processPvssNameOfSector(ExecuteParamRec &param)
{
	TextVar		sector;
	Variable	*pvssName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "sector",		TEXT_VAR,	&sector,	NULL		},
		{ "pvssName",	TEXT_VAR,	NULL,		&pvssName	}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pool.findSectorData(sector);
	if(pSector)
	{
		((TextVar *)pvssName)->setValue(pSector->getPvssName());
	}
	else
	{
		((TextVar *)pvssName)->setValue("");
	}
	return pSector ? 0 : -1;
}






/*
**	FUNCTION
**		Process call to LhcVacPvssNameOfMainPart()
**		PVSS CTRL arguments are:
**			string mainPart
**			string &pvssName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processPvssNameOfMainPart(ExecuteParamRec &param)
{
	TextVar		mainPart;
	Variable	*pvssName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "mainPart",	TEXT_VAR,	&mainPart,	NULL },
		{ "pvssName",	TEXT_VAR,	NULL,		&pvssName }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	MainPart *pMainPart = pool.findMainPartData(mainPart);
	if(pMainPart)
	{
		((TextVar *)pvssName)->setValue(pMainPart->getPvssName());
	}
	else
	{
		((TextVar *)pvssName)->setValue("");
	}
	return pMainPart ? 0 : -1;
}

/*
**	FUNCTION
**		Process call to LhcVacGetVacTypeOfSector()
**		PVSS CTRL arguments are:
**			string sector
**			int &vacType
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processVacTypeOfSector(ExecuteParamRec &param)
{
	TextVar		sector;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "sector",		TEXT_VAR,		&sector,		NULL },
		{ "result",		INTEGER_VAR,		NULL,		&result }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pool.findSectorData(sector);
	int vacType = 0;
	if(pSector)
	{
		vacType = pSector->getVacType();
	}
	((IntegerVar *)result)->setValue(vacType);
	return pSector ? 0 : -1;
}

/*
**	FUNCTION
**		Process call to LhcVacGetVacTypeOfMainPart()
**		PVSS CTRL arguments are:
**			string mainPart
**			int &vacType
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processVacTypeOfMainPart(ExecuteParamRec &param)
{
	TextVar		mainPart;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "mainPart",	TEXT_VAR,		&mainPart,	NULL },
		{ "result",		INTEGER_VAR,	NULL,		&result }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	MainPart *pMainPart = pool.findMainPartData(mainPart);
	int vacType = 0;
	if(pMainPart)
	{
		vacType = pMainPart->getVacTypeMask();
	}
	((IntegerVar *)result)->setValue(vacType);
	return pMainPart ? 0 : -1;
}

/*
**	FUNCTION
**		Process call to LhcVacDomainsOfMainPart()
**		PVSS CTRL arguments are:
**			string mainPartName
**			dyn_string &domains
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDomainsOfMainPart(ExecuteParamRec &param)
{
	TextVar		mainPartName;
	Variable	*domains, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "mainPartName",	TEXT_VAR,		&mainPartName,	NULL },
		{ "domains",		DYNTEXT_VAR,	NULL,			&domains },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	MainPart *pMainPart = pool.findMainPartData(mainPartName);
	((DynVar *)domains)->clear();
	if(!pMainPart)
	{
		char	buf[512];
		sprintf(buf, "Unknown main part <%s>", (const char *)mainPartName);
		addExceptionInfo(param, exceptionInfo, buf);
		return -1;
	}
	const QList<Sector *> &allSectors = pool.getSectors();
	for(int sectIdx = 0 ; sectIdx < allSectors.count() ; sectIdx++)
	{
		Sector *pSector = allSectors.at(sectIdx);
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			if(pMap->getMainPart() == pMainPart)
			{
				addToDynStringVar((DynVar *)domains, pMap->getDomain().toLatin1(), true);
			}
		}
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacDomainsOfSector()
**		PVSS CTRL arguments are:
**			string sectorName
**			dyn_string &domains
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDomainsOfSector(ExecuteParamRec &param)
{
	TextVar		sectorName;
	Variable	*domains, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "sectorName",		TEXT_VAR,		&sectorName,	NULL },
		{ "domains",		DYNTEXT_VAR,	NULL,			&domains },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pool.findSectorData(sectorName);
	((DynVar *)domains)->clear();
	if(!pSector)
	{
		// [VACCO-948] [VACCO-1645] checks if sectorName was passed in the format of sectorPvssName
		const QList<Sector *> &allSectors = pool.getSectors();
		for (int sectIdx = 0; sectIdx < allSectors.count(); sectIdx++){
			if (strcmp(sectorName, allSectors.at(sectIdx)->getPvssName()) == 0){
				pSector = allSectors.at(sectIdx);
				break;
			}
		}
		if (!pSector){
			char	buf[512];
			sprintf(buf, "Unknown sector <%s>", (const char *)sectorName);
			addExceptionInfo(param, exceptionInfo, buf);
			return -1;
		}
	}
	const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
	for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
	{
		MainPartMap *pMap = maps.at(mapIdx);
		addToDynStringVar((DynVar *)domains, pMap->getDomain().toLatin1(), true);
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacSetSectorSpecSynoptic()
**		PVSS CTRL arguments are:
**			string sectorName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
/* L.Kopylov 25.05.2012
int VacCtlEqpDataPvss::processSetSectorSpecSynoptic(ExecuteParamRec &param)
{
	TextVar		sectorName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "sectorName",	TEXT_VAR,	&sectorName,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pool.findSectorData(sectorName);
	if(pSector)
	{
		pSector->setSpecSynoptic(true);
		return 1;
	}
	return 0;
}
*/

/*
**	FUNCTION
**		Process call to LhcVacSetSectorSpecSynPanel()
**		PVSS CTRL arguments are:
**			string sectorName
**			string panelName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetSectorSpecSynPanel(ExecuteParamRec &param)
{
	TextVar		sectorName, panelName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "sectorName",	TEXT_VAR,	&sectorName,	NULL },
		{ "panelName",	TEXT_VAR,	&panelName,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString fileName((const char *)panelName);
	if(fileName.isEmpty())
	{
		return 0;
	}
	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pool.findSectorData(sectorName);
	if(pSector)
	{
		#ifdef Q_OS_WIN
			fileName.replace("/", "\\");
		#else
			fileName.replace("\\", "/");
		#endif
		pSector->setSpecSynPanel(fileName);
		return 1;
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacSetSectorSpecSectPanel()
**		PVSS CTRL arguments are:
**			string sectorName
**			string panelName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetSectorSpecSectPanel(ExecuteParamRec &param)
{
	TextVar		sectorName, panelName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "sectorName",	TEXT_VAR,	&sectorName,	NULL },
		{ "panelName",	TEXT_VAR,	&panelName,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString fileName((const char *)panelName);
	if(fileName.isEmpty())
	{
		return 0;
	}
	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pool.findSectorData(sectorName);
	if(pSector)
	{
		#ifdef Q_OS_WIN
			fileName.replace("/", "\\");
		#else
			fileName.replace("\\", "/");
		#endif
		pSector->setSpecSectPanel(fileName);
		return 1;
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacIsSectorInMainPart()
**		PVSS CTRL arguments are:
**			string sectorName
**			string mainPartName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processIsSectorInMainPart(ExecuteParamRec &param)
{
	TextVar		sectorName, mainPartName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "sectorName",		TEXT_VAR,	&sectorName,	NULL },
		{ "mainPartName",	TEXT_VAR,	&mainPartName,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	MainPart *pMainPart = pool.findMainPartData(mainPartName);
	if(!pMainPart)
	{
		return 0;
	}
	Sector *pSector = pool.findSectorData(sectorName);
	if(!pSector)
	{
		return 0;
	}
	return pSector->isInMainPart(pMainPart) ? 1 : 0;
}

/*
**	FUNCTION
**		Process call to LhcVacGetSectorsOfDomains()
**		PVSS CTRL arguments are:
**			dyn_string domains
**			dyn_string &sectors
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetSectorsOfDomains(ExecuteParamRec &param)
{
	DynVar		domains;
	Variable	*sectors;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "domains",	DYNTEXT_VAR,	&domains,	NULL },
		{ "sectors",	DYNTEXT_VAR,	NULL,		&sectors }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)sectors)->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &allSectors = pool.getSectors();
	for(int sectIdx = 0 ; sectIdx < allSectors.count() ; sectIdx++)
	{
		Sector *pSector = allSectors.at(sectIdx);
		if(pSector->isOuter())
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			TextVar domainName(pMap->getDomain().toLatin1());
			if(domains.isIn(domainName))
			{
				addToDynStringVar((DynVar *)sectors, pSector->getName(), true);
			}
		}
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacSetPvssFunctionalType()
**		PVSS CTRL arguments are:
**			string dpType
**			int type
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetPvssFunctionalType(ExecuteParamRec &param)
{
	TextVar		dpType;
	IntegerVar	type;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpType",			TEXT_VAR,		&dpType,	NULL },
		{ "type",			INTEGER_VAR,	&type,		NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	const QHash<QByteArray, Eqp *> &allEqp = pool.getEqpDict();
	int useType = type.getValue();
	foreach(Eqp *pEqp, allEqp)
	{
		if(pEqp->getDpType())
		{
			if(!strcmp(pEqp->getDpType(), dpType))
			{
				pEqp->setPvssFunctionalType(useType);
			}
		}
	}
	return allEqp.count();
}


/*
**	FUNCTION
**		Process call to LhcVacDpType()
**		PVSS CTRL arguments are:
**			string dpName
**			string &dpType
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDpType(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*dpType, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "dpType",			TEXT_VAR,		NULL,		&dpType },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	int coco = -1;
	if(pEqp)
	{
		if(pEqp->getDpType())
		{
			((TextVar *)dpType)->setValue(pEqp->getDpType());
			coco = 0;
		}
		else
		{
			((TextVar *)dpType)->setValue("");
			QString errMsg("No DP type for <");
			errMsg += (const char *)dpName;
			errMsg += ">";
			addExceptionInfo(param, exceptionInfo, errMsg);
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacMobileProp()
**		PVSS CTRL arguments are:
**			string dpName
**			int &mobileType
**			bool &isActive
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processMobileProp(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*mobileType, *isActive, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "mobileType",		INTEGER_VAR,	NULL,		&mobileType },
		{ "isActive",		BIT_VAR,		NULL,		&isActive },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	int coco = -1;
	if(pEqp)
	{
		((IntegerVar *)mobileType)->setValue(pEqp->getMobileType());
		((BitVar *)isActive)->setValue(pEqp->isActive(DataEnum::Online) ? PVSS_TRUE : PVSS_FALSE);
		coco = 0;
	}
	else
	{
		((IntegerVar *)mobileType)->setValue(0);
		((BitVar *)isActive)->setValue(PVSS_FALSE);
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacDisplayName()
**		PVSS CTRL arguments are:
**			string name
**			string &displayName
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDisplayName(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*displayName, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "displayName",	TEXT_VAR,		NULL,		&displayName },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	int coco = -1;
	if(pEqp)
	{
		coco = 0;
		((TextVar *)displayName)->setValue(pEqp->getVisibleName());
	}
	else
	{
		coco = -1;
		((TextVar *)displayName)->setValue((const char *)dpName);
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacDpName()
**		PVSS CTRL arguments are:
**			string displayname
**			string &dp
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDpName(ExecuteParamRec &param)
{
	TextVar		displayName;
	Variable	*dpName, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "displayName", TEXT_VAR, &displayName, NULL },
		{ "dpName", TEXT_VAR, NULL, &dpName },
		{ "exceptionInfo", DYNTEXT_VAR, NULL, &exceptionInfo }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();

	Eqp * pEqp = pool.findEqpByVisibleName(displayName);

	//Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	int coco = -1;
	if (pEqp)
	{
		coco = 0;
		((TextVar *)dpName)->setValue(pEqp->getDpName());
	}
	else
	{
		coco = -1;
		((TextVar *)dpName)->setValue((const char *)displayName);
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacDeviceLocation()
**		PVSS CTRL arguments are:
**			string dpName
**			float &coord
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDeviceLocation(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*coord, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "coord",			FLOAT_VAR,		NULL,		&coord },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	int coco = -1;
	if(pEqp)
	{
		((FloatVar *)coord)->setValue(pEqp->getStart());
		coco = 0;
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacDeviceAccessPointDistance()
**		PVSS CTRL arguments are:
**			string dpName
**			float &coord
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDeviceAccessPointDistance(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*coord, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "coord",			FLOAT_VAR,		NULL,		&coord },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	int coco = -1;
	if(pEqp)
	{
		((FloatVar *)coord)->setValue(pEqp->getAccessPointDistance());
		coco = 0;
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacDeviceVacLocation()
**		PVSS CTRL arguments are:
**			string dpName
**			int &vac
**			string &sector1
**			string &sector2
**			string &mainPart
**			bool &isBorder
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDeviceVacLocation(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*vac, *sector1, *sector2, *mainPart, *isBorder, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "vac",			INTEGER_VAR,	NULL,		&vac },
		{ "sector1",		TEXT_VAR,		NULL,		&sector1 },
		{ "sector2",		TEXT_VAR,		NULL,		&sector2 },
		{ "mainPart",		TEXT_VAR,		NULL,		&mainPart },
		{ "isBorder",		BIT_VAR,		NULL,		&isBorder },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);

	int coco = -1;
	if(pEqp)
	{
		((TextVar *)sector1)->setValue(
			pEqp->getSectorBefore() ? pEqp->getSectorBefore()->getName() : "");
		((TextVar *)sector2)->setValue(
			pEqp->getSectorAfter() ? pEqp->getSectorAfter()->getName() : "");
		((TextVar *)mainPart)->setValue(
			pEqp->getMainPart() ? pEqp->getMainPart()->getName() : "");
		((BitVar *)isBorder)->setValue(pEqp->isSectorBorder());
		((IntegerVar *)vac)->setValue(pEqp->getVacType());
		coco = 0;
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacDeviceDomain()
**		PVSS CTRL arguments are:
**			string dpName
**			dyn_string &domains
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDeviceDomain(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*domains, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "domains",		DYNTEXT_VAR,	NULL,			&domains },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)domains)->clear();

	// [VACCO-948] [VACCO-1645]
	// Added the case where the dpName is from a Sector object, not an Eqp
	Sector *pSector = findSectorDp(dpName, param, NULL);
	if (pSector)
	{
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for (int mapIdx = 0; mapIdx < maps.count(); mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			addToDynStringVar((DynVar *)domains, pMap->getDomain().toLatin1(), true);
		}
		return 0;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	if(!pEqp)
	{
		return -1;
	}
	if(!pEqp->getDomain().isEmpty())
	{
		addToDynStringVar((DynVar *)domains, pEqp->getDomain().toLatin1(), true);
	}
	else
	{
		// All domains of main part of device (if any)
		MainPart *pMainPart = pEqp->getMainPart();
		if(pMainPart)
		{
			DataPool &pool = DataPool::getInstance();
			const QList<Sector *> &allSectors = pool.getSectors();
			for(int sectIdx = 0 ; sectIdx < allSectors.count() ; sectIdx++)
			{
				Sector *pSector = allSectors.at(sectIdx);
				const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
				for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
				{
					MainPartMap *pMap = maps.at(mapIdx);
					if(pMap->getMainPart() == pMainPart)
					{
						addToDynStringVar((DynVar *)domains, pMap->getDomain().toLatin1(), true);
					}
				}
			}
		}
	}
	return 0;
}
/**
@defgroup LhcVacGetAllAttributes LhcVacGetAllAttributes()
@section s int LhcVacGetAllAttributes(string dpName, dyn_string &attrList)
@subsection ss Purpose
Get all attributes value in for_DLL file
@subsection arg WCCOA CTRL Arguments
\b dpName: 		    datapoint name SUB_DP context \n
\b attrList:	    list of attributes is returned here. The list items ordered according to rules (attributeName1, attributeValue1, attributeName2, attributeValue2,....).  At the beginning of list the some generic attributes ( Control family, Control type, Control subtype....) will be filled. \n
@subsection ret Return
Return always 0
@subsection ex Usage Example
dyn_string attrList; \n
string dpName = dpSubStr($dpName, DPSUB_DP); \n
LhcVacGetAllAttributes(dpName, attrList); \n
for(int n = 1 ; n < dynlen(attrList) ; n += 2) { \n
   DebugTN("attribute name", attrList[n], "attribute value", attrList[n+1]); \n
} \n
*/
/**
@brief GENERIC API Process call to LhcVacGetAllAttributes
*/
int VacCtlEqpDataPvss::processGetAllAttributes(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*attrList;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,		&dpName,	NULL },
		{ "attrList",	DYNTEXT_VAR,	NULL,		&attrList }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	// Execute and return output arguments
	((DynVar *)attrList)->clear();
	DataPool &pool = DataPool::getInstance();
	Eqp *pEqp = pool.findEqpByDpName(dpName);
	if(!pEqp)
	{
		return -1;
	}
	// First add some 'generic' attributes
	TextVar textVar;
	char buf[32];
	textVar.setValue("CtrlFamily");
	((DynVar *)attrList)->append(textVar);
	sprintf(buf, "%d", pEqp->getCtrlFamily());
	textVar.setValue(buf);
	((DynVar *)attrList)->append(textVar);

	textVar.setValue("CtrlType");
	((DynVar *)attrList)->append(textVar);
	sprintf(buf, "%d", pEqp->getCtrlType());
	textVar.setValue(buf);
	((DynVar *)attrList)->append(textVar);
	
	textVar.setValue("CtrlSubType");
	((DynVar *)attrList)->append(textVar);
	sprintf(buf, "%d", pEqp->getCtrlSubType());
	textVar.setValue(buf);
	((DynVar *)attrList)->append(textVar);

	if(pEqp->getDpType())
	{
		textVar.setValue("DP Type");
		((DynVar *)attrList)->append(textVar);
		textVar.setValue(pEqp->getDpType());
		((DynVar *)attrList)->append(textVar);
	}
	if(pEqp->getDpName())
	{
		textVar.setValue("DP Name");
		((DynVar *)attrList)->append(textVar);
		textVar.setValue(pEqp->getDpName());
		((DynVar *)attrList)->append(textVar);
	}
	if(pEqp->getSectorBefore())
	{
		textVar.setValue(pEqp->getSectorAfter() ? "Sector Before" : "Sector");
		((DynVar *)attrList)->append(textVar);
		textVar.setValue(pEqp->getSectorBefore()->getName());
		((DynVar *)attrList)->append(textVar);
		if(pEqp->getSectorAfter())
		{
			textVar.setValue("Sector After");
			((DynVar *)attrList)->append(textVar);
			textVar.setValue(pEqp->getSectorAfter()->getName());
			((DynVar *)attrList)->append(textVar);
		}
	}
	if(pEqp->getSurveyPart())
	{
		textVar.setValue("Survey partition");
		((DynVar *)attrList)->append(textVar);
		textVar.setValue(pEqp->getSurveyPart());
		((DynVar *)attrList)->append(textVar);
		sprintf(buf, "%f", pEqp->getSurveyPos());
		textVar.setValue("Position");
		((DynVar *)attrList)->append(textVar);
		textVar.setValue(buf);
		((DynVar *)attrList)->append(textVar);
	}
	QHashIterator<QByteArray, QString> iter(pEqp->getAttrList());
	while(iter.hasNext())
	{
		iter.next();
		textVar.setValue(iter.key());
		((DynVar *)attrList)->append(textVar);
		textVar.setValue(iter.value().toLatin1());
		((DynVar *)attrList)->append(textVar);
	}
	return ((DynVar *)attrList)->getArrayLength();
}
/**
@defgroup LhcVacGetAttribute LhcVacGetAttribute()
@section s int LhcVacGetAttribute(string dpName, string attrName, string &attrValue)
@subsection ss Purpose
Get the required attribute value in for_DLL file
@subsection arg WCCOA CTRL Arguments
\b dpName: 		    datapoint name SUB_DP context \n
\b attrName: 		name of the attribute as it is in for_DLL file \n
\b attrValue:	    value of the attribute \n
@subsection ret Return
Return 0 if OK, -1 if error
@subsection ex Usage Example
string dbVersionTs; \n
LhcVacGetAttribute(plcName, "TimeStamp", dbVersionTs); \n
strreplace( dbVersionTs, ".", "-"); \n
DebugTN("attribute name  'TimeStamp' ","attribute value", dbVersionTs); \n
*/
/**
@brief GENERIC API Process call to LhcVacGetAttribute
*/
int VacCtlEqpDataPvss::processGetAttribute(ExecuteParamRec &param)
{
	TextVar		dpName, attrName;
	Variable	*value;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,	&dpName,	NULL },
		{ "attrName",	TEXT_VAR,	&attrName,	NULL },
		{ "value",		TEXT_VAR,	NULL,		&value }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		((TextVar *)value)->setValue("");
		return -1;
	}
	const QString attrValue = pEqp->getAttrValue(attrName);
	((TextVar *)value)->setValue(!attrValue.isEmpty() ? attrValue.toLatin1() : "");
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacGetCtlAttributes",
**		PVSS CTRL arguments are:
**			string dpName
**			int &family
**			int &type
**			int &subType
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
/**
@defgroup LhcVacGetCtlAttributes LhcVacGetCtlAttributes()
@section s int LhcVacGetCtlAttributes(string dpName, int &family, int &type, int &subType)
@subsection ss Purpose
Get Control attributes family, type and subtype value for this dp in for_DLL file
@subsection arg WCCOA CTRL Arguments
\b dpName: 		datapoint name SUB_DP context \n
\b family:	    ​control family of DP \n
\b type:		control type of DP \n
\b subType:		control subType of DP \n
@subsection ret Return
Return always 0
@subsection ex Usage Example
int family, type, subType; \n
LhcVacGetCtlAttributes(dpName, family, type, subType); \n
setValue(ctrlTypeText, "text", "CtrlType=" + type); \n
*/
/**
@brief GENERIC API Process call to LhcVacGetCtlAttributes
*/
int VacCtlEqpDataPvss::processGetCtlAttributes(ExecuteParamRec &param)
{
	TextVar		dpName, attrName;
	Variable	*family, *type, *subType;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,		&dpName,	NULL },
		{ "family",		INTEGER_VAR,	NULL,	&family },
		{ "type",		INTEGER_VAR,	NULL,	&type },
		{ "subType",	INTEGER_VAR,	NULL,	&subType }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		((IntegerVar *)family)->setValue(0);
		((IntegerVar *)type)->setValue(0);
		((IntegerVar *)subType)->setValue(0);
		return -1;
	}
	((IntegerVar *)family)->setValue(pEqp->getCtrlFamily());
	((IntegerVar *)type)->setValue(pEqp->getCtrlType());
	((IntegerVar *)subType)->setValue(pEqp->getCtrlSubType());
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacGetByAttrValue()
**		PVSS CTRL arguments are:
**			string attrName
**			string value
**			dyn_string &result
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetByAttrValue(ExecuteParamRec &param)
{
	TextVar		attrName, value;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "attrName",	TEXT_VAR,		&attrName,	NULL },
		{ "value",		TEXT_VAR,		&value,		NULL },
		{ "result",		DYNTEXT_VAR,	NULL,		&result }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)result)->clear();
	const char *searchValue = value;

	// Search only equipment with DP names
	const QHash<QByteArray, Eqp *> &allEqps = DataPool::getInstance().getEqpDict();
	foreach(Eqp *pEqp, allEqps)
	{
		if(!pEqp->isActive(DataEnum::Online))
		{
			continue;
		}
		const QString attrValue = pEqp->getAttrValue(attrName);
		if(attrValue.isEmpty())
		{
			if((!searchValue) || (!*searchValue))
			{
				addToDynStringVar((DynVar *)result, pEqp->getDpName(), true);
			}
		}
		else if(attrValue == searchValue)
		{
			addToDynStringVar((DynVar *)result, pEqp->getDpName(), true);
		}
	}

	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacEqpCtlStatus",
**		PVSS CTRL arguments are:
**			string dpName
**			int mode
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpCtlStatus(ExecuteParamRec &param)
{
	TextVar		dpName;
	IntegerVar	mode;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,		&dpName,	NULL },
		{ "mode",		INTEGER_VAR,	&mode,		NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		return Eqp::Used;
	}
	return pEqp->getCtlStatus((DataEnum::DataMode)mode.getValue());
}

/*
**	FUNCTION
**		Process call to LhcVacEqpMaster()
**		PVSS CTRL arguments are:
**			string dpName
**			string &masterDp
**			int &masterChannel
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpMaster(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*masterDp, *masterChannel;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "masterDp",		TEXT_VAR,		NULL,		&masterDp },
		{ "masterChannel",	INTEGER_VAR,	NULL,		&masterChannel }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	((TextVar *)masterDp)->setValue("");
	((IntegerVar *)masterChannel)->setValue(0);
	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		return -1;
	}
	QString masterDpName;
	int masterDpChannel;
	pEqp->getMaster(masterDpName, masterDpChannel);
	((TextVar *)masterDp)->setValue(masterDpName.toLatin1());
	((IntegerVar *)masterChannel)->setValue(masterDpChannel);
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacEqpConfig()
**		PVSS CTRL arguments are:
**			string dpName
**			string &configDp
**			int &configChannel
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpConfig(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*configDp, *configChannel;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "configDp",		TEXT_VAR,		NULL,		&configDp },
		{ "configChannel",	INTEGER_VAR,	NULL,		&configChannel }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)configDp)->setValue("");
	((IntegerVar *)configChannel)->setValue(0);
	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		return -1;
	}
	QString configDpName;
	int configDpChannel;
	pEqp->getConfig(configDpName, configDpChannel);
	((TextVar *)configDp)->setValue(configDpName.toLatin1());
	((IntegerVar *)configChannel)->setValue(configDpChannel);
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacEqpSlaves()
**		PVSS CTRL arguments are:
**			string dpName
**			dyn_string &slaves
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpSlaves(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*slaves;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,		&dpName,	NULL },
		{ "slaves",		DYNTEXT_VAR,	NULL,		&slaves }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)slaves)->clear();
	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		return -1;
	}
	QStringList slaveList;
	pEqp->getSlaves(slaveList);
	foreach(QString slaveDp, slaveList)
	{
		addToDynStringVar((DynVar *)slaves, slaveDp.toLatin1());
	}
	return slaveList.count();
}
/**
	@brief Process call to LhcVacEqpTargets()
	@param[in] string dpName
	@param[out] dyn_string &slaves
*/
int VacCtlEqpDataPvss::processEqpTargets(ExecuteParamRec &param) {
	TextVar		dpName;
	Variable	*targets;
	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName", TEXT_VAR, &dpName, NULL },
		{ "targets", DYNTEXT_VAR, NULL, &targets }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0]))) {
		return -1;
	}
	((DynVar *)targets)->clear();
	Eqp *pEqp = findDp(dpName, param, NULL);
	if (!pEqp) {
		return -1;
	}
	QStringList targetList;
	pEqp->getTargets(targetList);
	foreach(QString targetDp, targetList) {
		addToDynStringVar((DynVar *)targets, targetDp.toLatin1());
	}
	return targetList.count();
}
/*
**	FUNCTION
**		Process call to LhcVacEqpMasterProcess()
**		PVSS CTRL arguments are:
**			string dpName
**			string &masterDpName
**			string &usageInMaster
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpMasterProcess(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*masterDpName, *usageInMaster;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,	&dpName,	NULL },
		{ "masterDpName",	TEXT_VAR,	NULL,		&masterDpName },
		{ "usageInMaster",	TEXT_VAR,	NULL,		&usageInMaster }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)masterDpName)->setValue("");
	((TextVar *)usageInMaster)->setValue("");
	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		return -1;
	}
	Eqp *pMaster = pEqp->getMasterProcess();
	if(pMaster)
	{
		((TextVar *)masterDpName)->setValue(pMaster->getDpName());
		QByteArray usage = pEqp->getUsageInMasterProcess();
		((TextVar *)usageInMaster)->setValue(usage.constData());
		return 1;
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacEqpStateDpes()
**		PVSS CTRL arguments are:
**			string dpName
**			dyn_string &stateDpes
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpStateDpes(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*stateDpes;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,		&dpName,	NULL },
		{ "stateDpes",	DYNTEXT_VAR,	NULL,		&stateDpes }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)stateDpes)->clear();
	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		return -1;
	}
	QStringList dpeList;
	pEqp->getStateDpes(dpeList);
	foreach(QString dpe, dpeList)
	{
		addToDynStringVar((DynVar *)stateDpes, dpe.toLatin1());
	}
	return dpeList.count();
}

/*
**	FUNCTION
**		Process call to LhcVacEqpValueDpes()
**		PVSS CTRL arguments are:
**			string dpName
**			dyn_string &valueDpes
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpValueDpes(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*valueDpes;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,		&dpName,	NULL },
		{ "valueDpes",	DYNTEXT_VAR,	NULL,		&valueDpes }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)valueDpes)->clear();
	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		return -1;
	}
	QStringList dpeList;
	pEqp->getValueDpes(dpeList);
	foreach(QString dpe, dpeList)
	{
		addToDynStringVar((DynVar *)valueDpes, dpe.toLatin1());
	}
	return dpeList.count();
}

/*
**	FUNCTION
**		Process call to LhcVacSelectEqp()
**		PVSS CTRL arguments are:
**			string dpName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSelectEqp(ExecuteParamRec &param)
{
	TextVar		dpName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,		&dpName,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	// First deselect all previously selected
	QHash<QByteArray, Eqp *> &allEqp = DataPool::getInstance().getEqpDict();
	Eqp *pEqp;
	foreach(pEqp, allEqp)
	{
		pEqp->setSelected(false, true);
	}

	// Then find required DP and select
	pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		return -1;
	}
	pEqp->setSelected(true, true);
	return 0;
}

/*
**
** FUNCTION
**		Process call to LhcVacGetDevicesAtSectors()
**		PVSS CTRL arguments are:
**			dyn_string sectors
**			dyn_string &dpNames
**			dyn_string &exceptionInfo
**
** PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
** RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetDevicesAtSectors(ExecuteParamRec &param)
{
	DynVar		sectors;
	Variable	*dpNames, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "sectors",		DYNTEXT_VAR,	&sectors,	NULL },
		{ "dpNames",		DYNTEXT_VAR,	NULL,		&dpNames },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	// Execute and return output arguments
	((DynVar *)dpNames)->clear();
	((DynVar *)exceptionInfo)->clear();

	DataPool &pool = DataPool::getInstance();
	if(!pool.getSectors().count())
	{
		addExceptionInfo(param, exceptionInfo, "Not initialized");
		return 0;
	}

	// Make search in order corresponding to order of sectors in original list
	const QList<Eqp *> &ctlList = pool.getCtlList();
	const QList<BeamLine *> &lines = pool.getLines();
	for(unsigned short idx = 0 ; idx < sectors.getNumberOfItems() ; idx ++)
	{
		TextVar *pTextVar = (TextVar *)sectors.getAt(idx);
		Sector *pSector = pool.findSectorData((const char *)pTextVar->getValue());
		if(!pSector)
		{
			continue;
		}

		// Process controllers
		for(int eqpIdx = 0 ; eqpIdx < ctlList.count() ; eqpIdx++)
		{
			Eqp *pEqp = ctlList.at(eqpIdx);
			if(!pEqp->getDpType())
			{
				continue;
			}
			if(!pEqp->isActive(DataEnum::Online))
			{
				continue;
			}
			if(pEqp->getSectorBefore() == pSector)
			{
				addToDynStringVar((DynVar *)dpNames, pEqp->getDpName(), true);
			}
			else if(pEqp->getSectorAfter() == pSector)
			{
				addToDynStringVar((DynVar *)dpNames, pEqp->getDpName(), true);
			}
		}

		// Process equipment with physical location
		for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			BeamLine *pLine = lines.at(lineIdx);
			const QList<BeamLinePart *> &parts = pLine->getParts();
			for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
			{
				BeamLinePart *pPart = parts.at(partIdx);
				Eqp	**eqpPtrs = pPart->getEqpPtrs();
				int nEqpPtrs = pPart->getNEqpPtrs();
				for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(!pEqp->getDpType())
					{
						continue;
					}
					if(!pEqp->isActive(DataEnum::Online))
					{
						continue;
					}
					if(pEqp->getSectorBefore() == pSector)
					{
						addToDynStringVar((DynVar *)dpNames, pEqp->getDpName(), true);
					}
					else if(pEqp->getSectorAfter() == pSector)
					{
						addToDynStringVar((DynVar *)dpNames, pEqp->getDpName(), true);
					}
				}
			}
		}
	}
	return 0;
}

/*
**
** FUNCTION
**		Process call to LhcVacGetDevicesOfDpTypesAtSectors()
**		PVSS CTRL arguments are:
**			dyn_string sectors
**			dyn_string dpTypes
**			dyn_string &dpNames
**			dyn_string &exceptionInfo
**
** PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
** RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetDevicesOfDpTypesAtSectors(ExecuteParamRec &param)
{
	DynVar		sectors, dpTypes;
	Variable	*dpNames, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "sectors",		DYNTEXT_VAR,	&sectors,	NULL },
		{ "dpTypes",		DYNTEXT_VAR,	&dpTypes,	NULL },
		{ "dpNames",		DYNTEXT_VAR,	NULL,		&dpNames },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	// Execute and return output arguments
	((DynVar *)dpNames)->clear();
	((DynVar *)exceptionInfo)->clear();

	DataPool &pool = DataPool::getInstance();
	if(!pool.getSectors().count())
	{
		addExceptionInfo(param, exceptionInfo, "Not initialized");
		return 0;
	}

	// Make search in order corresponding to order of sectors in original list
	const QList<Eqp *> &ctlList = pool.getCtlList();
	const QList<BeamLine *> &lines = pool.getLines();
	for(unsigned short idx = 0 ; idx < sectors.getNumberOfItems() ; idx++)
	{
		TextVar *pTextVar = (TextVar *)sectors.getAt(idx);
		Sector *pSector = pool.findSectorData((const char *)pTextVar->getValue());
		if(!pSector)
		{
			continue;
		}

		// Process controllers
		for(int eqpIdx = 0 ; eqpIdx < ctlList.count() ; eqpIdx++)
		{
			Eqp *pEqp = ctlList.at(eqpIdx);
			if(!pEqp->getDpType())
			{
				continue;
			}
			if(!pEqp->isActive(DataEnum::Online))
			{
				continue;
			}
			if((pEqp->getSectorBefore() != pSector) && (pEqp->getSectorAfter() != pSector))
			{
				continue;
			}
			for(Variable *pTypeVar = dpTypes.getFirstVar() ; pTypeVar ; pTypeVar = dpTypes.getNextVar())
			{
				if(((TextVar *)pTypeVar)->getValue())
				{
					if(!strcmp(pEqp->getDpType(), ((TextVar *)pTypeVar)->getValue()))
					{
						addToDynStringVar((DynVar *)dpNames, pEqp->getDpName(), true);
						break;
					}
				}
			}
		}

		// Process equipment with physical location
		for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			BeamLine *pLine = lines.at(lineIdx);
			const QList<BeamLinePart *> &parts = pLine->getParts();
			for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
			{
				BeamLinePart *pPart = parts.at(partIdx);
				Eqp	**eqpPtrs = pPart->getEqpPtrs();
				int nEqpPtrs = pPart->getNEqpPtrs();
				for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(!pEqp->getDpType())
					{
						continue;
					}
					if(!pEqp->isActive(DataEnum::Online))
					{
						continue;
					}
					if((pEqp->getSectorBefore() != pSector) && (pEqp->getSectorAfter() != pSector))
					{
						continue;
					}
					for(Variable *pTypeVar = dpTypes.getFirstVar() ; pTypeVar ; pTypeVar = dpTypes.getNextVar())
					{
						if(((TextVar *)pTypeVar)->getValue())
						{
							if(!strcmp(pEqp->getDpType(), ((TextVar *)pTypeVar)->getValue()))
							{
								addToDynStringVar((DynVar *)dpNames, pEqp->getDpName(), true);
								break;
							}
						}
					}
				}
			}
		}
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacGetDevicesAtMainParts()
**		PVSS CTRL arguments are:
**			dyn_string mainParts
**			dyn_string &dpNames
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetDevicesAtMainParts(ExecuteParamRec &param)
{
	DynVar		mainParts;
	Variable	*dpNames, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "mainParts",		DYNTEXT_VAR,	&mainParts,	NULL },
		{ "dpNames",		DYNTEXT_VAR,	NULL,		&dpNames },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	// Execute and return output arguments
	((DynVar *)dpNames)->clear();
	((DynVar *)exceptionInfo)->clear();

	DataPool &pool = DataPool::getInstance();
	if(!pool.getMainParts().count())
	{
		addExceptionInfo(param, exceptionInfo, "Not initialized");
		return 0;
	}

	// Make search in order corresponding to order of main parts in origial list
	const QList<Eqp *> &ctlList = pool.getCtlList();
	const QList<BeamLine *> &lines = pool.getLines();
	for(unsigned short idx = 0 ; idx < mainParts.getNumberOfItems() ; idx++)
	{
		TextVar *pTextVar = (TextVar *)mainParts.getAt(idx);
		MainPart *pMainPart = pool.findMainPartData((const char *)pTextVar->getValue());
		if(!pMainPart)
		{
			continue;
		}
		
		// Process controllers
		for(int eqpIdx = 0 ; eqpIdx < ctlList.count() ; eqpIdx++)
		{
			Eqp *pEqp = ctlList.at(eqpIdx);
			if(!pEqp->getDpType())
			{
				continue;
			}
			if(!pEqp->isActive(DataEnum::Online))
			{
				continue;
			}
			if(pEqp->getMainPart() == pMainPart)
			{
				addToDynStringVar((DynVar *)dpNames, pEqp->getDpName(), true);
			}
		}

		// Process equipment with physical location
		for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			BeamLine *pLine = lines.at(lineIdx);
			const QList<BeamLinePart *> &parts = pLine->getParts();
			for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
			{
				BeamLinePart *pPart = parts.at(partIdx);
				Eqp	**eqpPtrs = pPart->getEqpPtrs();
				int nEqpPtrs = pPart->getNEqpPtrs();
				for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(!pEqp->getDpType())
					{
						continue;
					}
					if(!pEqp->isActive(DataEnum::Online))
					{
						continue;
					}
					if(pEqp->getMainPart() == pMainPart)
					{
						addToDynStringVar((DynVar *)dpNames, pEqp->getDpName(), true);
					}
				}
			}
		}
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacGetDevicesMatchingFilter()
**		PVSS CTRL arguments are:
**			string filter
**			dyn_string &dpNames
**			dyn_string &eqpNames
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetDevicesMatchingFilter(ExecuteParamRec &param)
{
	TextVar		filter;
	Variable	*dpNames, *eqpNames;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "filter",			TEXT_VAR,		&filter,	NULL },
		{ "dpNames",		DYNTEXT_VAR,	NULL,		&dpNames },
		{ "eqpNames",		DYNTEXT_VAR,	NULL,		&eqpNames }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	// Execute and return output arguments
	((DynVar *)dpNames)->clear();
	((DynVar *)eqpNames)->clear();
	if(!filter.isTrue())
	{
		return 0;
	}

	DataPool &pool = DataPool::getInstance();
	QStringList dpNameList, eqpNameList;
	pool.findEqpMatchingMask((const char *)filter, dpNameList, eqpNameList);
	for(int idx = 0 ; idx < dpNameList.count() ; idx++)
	{
		addToDynStringVar((DynVar *)dpNames, dpNameList.at(idx).toLatin1(), false);
	}
	for(int idx = 0 ; idx < eqpNameList.count() ; idx++)
	{
		addToDynStringVar((DynVar *)eqpNames, eqpNameList.at(idx).toLatin1(), false);
	}
	return dpNameList.count();
}

/*
**	FUNCTION
**		Process call to LhcVacGetThermometersForValve()
**		PVSS CTRL arguments are:
**			string valveDp
**			dyn_string &thermometerDps
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetThermometersForValve(ExecuteParamRec &param)
{
	TextVar		valveDp;
	Variable	*thermometerDps, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{"valveDp",			TEXT_VAR,		&valveDp,	NULL},
		{"thermometerDps",	DYNTEXT_VAR,	NULL,		&thermometerDps},
		{"exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	// Execute and return output arguments
	((DynVar *)thermometerDps)->clear();
	((DynVar *)exceptionInfo)->clear();

	QStringList	thermoList;
	QString		errMsg;
	DataPool &pool = DataPool::getInstance();
	int coco = pool.findThermometersForValve(valveDp, thermoList, errMsg);
	if(coco < 0)
	{
		addExceptionInfo(param, exceptionInfo, errMsg);
	}
	else
	{
		foreach(QString dpName, thermoList)
		{
			addToDynStringVar((DynVar *)thermometerDps, dpName.toLatin1());
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacGetNeighbourValves()
**		PVSS CTRL arguments are:
**			string valveDp
**			dyn_string &valvesBefore
**			dyn_string &valvesAfter
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetNeighbourValves(ExecuteParamRec &param)
{
	TextVar		valveDp;
	Variable	*valvesBefore, *valvesAfter, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{"valveDp",			TEXT_VAR,		&valveDp,	NULL},
		{"valvesBefore",	DYNTEXT_VAR,	NULL,		&valvesBefore},
		{"valvesAfter",		DYNTEXT_VAR,	NULL,		&valvesAfter},
		{"exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo}
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	// Execute and return output arguments
	((DynVar *)valvesBefore)->clear();
	((DynVar *)valvesAfter)->clear();
	((DynVar *)exceptionInfo)->clear();

	QStringList	listBefore, listAfter;
	QString		errMsg;
	DataPool &pool = DataPool::getInstance();
	int coco = pool.findNeighbourValves(valveDp, listBefore, listAfter, errMsg);
	if(coco < 0)
	{
		addExceptionInfo(param, exceptionInfo, errMsg);
	}
	else
	{
		QString dpName;
		foreach(dpName, listBefore)
		{
			addToDynStringVar((DynVar *)valvesBefore, dpName.toLatin1(), true);
		}
		foreach(dpName, listAfter)
		{
			addToDynStringVar((DynVar *)valvesAfter, dpName.toLatin1(), true);
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacAddBakeoutWorkDP()
**		PVSS CTRL arguments are:
			string dpName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processAddBakeoutWorkDP(ExecuteParamRec &param)
{
	TextVar	dpName;

		// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",	TEXT_VAR,	&dpName,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}


	MobilePool &mobilePool = MobilePool::getInstance();
	mobilePool.addBakeoutWorkDp((const char *)dpName);
	return 1;
}

/*
**	FUNCTION
**		Process call to LhcVacSetMobileEqp()
**		PVSS CTRL arguments are:
**			dyn_string targetDps
**			dyn_string workDps
**			dyn_string sourceDps
**			dyn_bool activeFlags
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetMobileEqp(ExecuteParamRec &param)
{
	DynVar		targetDps, workDps, sourceDps, activeFlags;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "targetDps",		DYNTEXT_VAR,	&targetDps,		NULL },
		{ "workDps",		DYNTEXT_VAR,	&workDps,		NULL },
		{ "sourceDps",		DYNTEXT_VAR,	&sourceDps,		NULL },
		{ "activeFlags",	DYNBIT_VAR,		&activeFlags,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	// Number of items in both dyn arrays must be equal
	if(targetDps.getArrayLength( ) != workDps.getArrayLength())
	{
		showError(param, ErrClass::PARAMETERERROR, "Number of DPs and work DPs is different");
		return -1;
	}

	if(targetDps.getArrayLength( ) != sourceDps.getArrayLength())
	{
		showError(param, ErrClass::PARAMETERERROR, "Number of DPs and source DPs is different");
		return -1;
	}

	if(targetDps.getArrayLength( ) != activeFlags.getArrayLength())
	{
		showError(param, ErrClass::PARAMETERERROR, "Number of DPs and active flags is different");
		return -1;
	}

	DataPool &pool = DataPool::getInstance();

	// If input arrays are empty - deactivate all active DPs
	if(!targetDps.getArrayLength())
	{
		clearAllActiveMobile(DataEnum::Online);
		return 0;
	}
	// Process all DPs
	MobilePool &mobilePool = MobilePool::getInstance();
	mobilePool.startOnlineEqp();

	Variable *pTextVar = targetDps.getFirstVar();
	Variable *pWorkDp = workDps.getFirstVar();
	Variable *pSourceDp = sourceDps.getFirstVar();
	Variable *pBoolVar = activeFlags.getFirstVar();
	while(pTextVar)
	{
		bool	isActive = pBoolVar->isTrue() == PVSS_TRUE;
		Eqp	*pMobileEqp = pool.findEqpByDpName((const char *)*((TextVar *)pTextVar));
		if(pMobileEqp)
		{
			pMobileEqp->setActive(DataEnum::Online, isActive);
		}
		mobilePool.addOnlineEqp((const char *)*((TextVar *)pTextVar),
			(const char *)*((TextVar *)pWorkDp),
			(const char *)*((TextVar *)pSourceDp),
			isActive);

		pTextVar = targetDps.getNextVar();
		pWorkDp = workDps.getNextVar();
		pSourceDp = sourceDps.getNextVar();
		pBoolVar = activeFlags.getNextVar();
	}
	mobilePool.finishOnlineEqp();
	return 1;
}

/**
@brief FUNCTION Process call to LhcVacSetWireLessEqp() activate mobile instance eqp to show them on synoptics
@param[in, out]	param	List of argument(dyn_string instanceDps, dyn_string masterDps, dyn_string tcpDps)
*/
int VacCtlEqpDataPvss::processSetWireLessEqp(ExecuteParamRec &param) {
	DynVar		instanceDps, masterDps, tcpDps;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceDps", DYNTEXT_VAR, &instanceDps, NULL },
		{ "masterDps", DYNTEXT_VAR, &masterDps, NULL },
		{ "tcpDps", DYNTEXT_VAR, &tcpDps, NULL }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0]))) {
		return -1;
	}
	// variable used to store dp Name
	Variable *pInstanceDpName, *pTcpDpName;

	// Get pool of eqp
	DataPool &pool = DataPool::getInstance();

	// Deactive and deconnect from communication object
	// Get any equipment of mobile type = instance and deactivate it
	QList<Eqp *> instanceEqps = pool.getMobileTypeEqps(MobileType::Instance);
	for (int i = 0; i < instanceEqps.count(); ++i) {
		instanceEqps[i]->setActive(DataEnum::Online, false);
	}
	// Active
	// Then if in list active and connect to communication object 
	pInstanceDpName = instanceDps.getFirstVar();
	pTcpDpName = tcpDps.getFirstVar();
	while (pInstanceDpName) {
		Eqp	*pEqpToActive = pool.findEqpByDpName((const char *)*((TextVar *)pInstanceDpName));
		if (pEqpToActive) {
			pEqpToActive->setActive(DataEnum::Online, true);
			// set communication object 
			pEqpToActive->setComDpName((const char *)*((TextVar *)pTcpDpName));
			Eqp	*pComEqp = pool.findEqpByDpName((const char *)*((TextVar *)pTcpDpName));
			QObject::connect(pComEqp, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
				pEqpToActive, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
		}
		pInstanceDpName = instanceDps.getNextVar();
		pTcpDpName = tcpDps.getNextVar();
	}
	return 1;
}


/*
**	FUNCTION
**		Process call to LhcVacAllEqpWithSameVisName()
**		PVSS CTRL arguments are:
**			string dpName
**			dyn_string &result
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processAllEqpWithSameVisName(ExecuteParamRec &param)
{
	TextVar		dpName, textVar;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",	TEXT_VAR,		&dpName,	NULL },
		{ "result",	DYNTEXT_VAR,	NULL,		&result },
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{	
		return -1;
	}	

	// Execute and return output arguments
	((DynVar *)result)->clear();
	DataPool &pool = DataPool::getInstance();
	Eqp	*pInputEqp = pool.findEqpByDpName(dpName);
	if(!pInputEqp)
	{
		return -1;
	}
	const char *nameToFind = pInputEqp->getVisibleName();

	// For the time being it is only used for VRPI, so only search in controllers
	const QList<Eqp *> &ctlList = pool.getCtlList();
	for(int eqpIdx = 0 ; eqpIdx < ctlList.count() ; eqpIdx++)
	{
		Eqp *pEqp = ctlList.at(eqpIdx);
		if(!pEqp->isActive(DataEnum::Online))
		{
			continue;
		}
		if(!strcmp(nameToFind, pEqp->getVisibleName()))
		{
			addToDynStringVar((DynVar *)result, pEqp->getDpName());
		}
	}

	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacNextMobileHistoryQuery()
**		PVSS CTRL arguments are:
**			time &start
**			time &end
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processNextMobileHistoryQuery(ExecuteParamRec &param)
{
	Variable	*start, *end;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "start",		TIME_VAR,		NULL,	&start },
		{ "end",		TIME_VAR,		NULL,	&end }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	MobileHistoryQuery &pool = MobileHistoryQuery::getInstance();
	QDateTime startTime, endTime;
	int coco;
	if(pool.getNext(startTime, endTime))
	{
		((TimeVar *)start)->setSeconds(startTime.toTime_t());
		((TimeVar *)start)->setMilli(startTime.time().msec());
		((TimeVar *)end)->setSeconds(endTime.toTime_t());
		((TimeVar *)end)->setMilli(endTime.time().msec());
		coco = 1;
	}
	else
	{
		coco = 0;
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacSetMobileEqpHistory()
**		PVSS CTRL arguments are:
**			dyn_dyn_string mobileDps
**			dyn_dyn_bool activeFlags
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetMobileEqpHistory(ExecuteParamRec &param)
{
	DynVar	mobileDps, activeFlags;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "mobileDps",		DYNDYNTEXT_VAR,	&mobileDps,		NULL },
		{ "activeFlags",	DYNDYNBIT_VAR,	&activeFlags,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	if(mobileDps.getArrayLength() != activeFlags.getArrayLength())
	{
		showError(param, ErrClass::PARAMETERERROR, "Number of DPs and active flags arrays is different");
		return -1;
	}

	MobilePool &pool = MobilePool::getInstance();
	pool.startHistoryEqp();

	DynVar *pDynDps = (DynVar *)mobileDps.getFirstVar();
	DynVar *pDynFlags = (DynVar *)activeFlags.getFirstVar();
	while(pDynDps)
	{
		if(pDynDps->getArrayLength() != pDynFlags->getArrayLength())
		{
			showError(param, ErrClass::PARAMETERERROR, "Number of DPs and active flags is different, skip...");
		}
		else
		{
			TextVar *pMobileDp = (TextVar *)pDynDps->getFirstVar();
			BitVar *pFlag = (BitVar *)pDynFlags->getFirstVar();
			while(pMobileDp)
			{
				if(pFlag->getValue() == PVSS_TRUE)
				{
					pool.addHistoryEqp(pMobileDp->getValue());
				}
				pMobileDp = (TextVar *)pDynDps->getNextVar();
				pFlag = (BitVar *)pDynFlags->getNextVar();
			}
		}
		pDynDps = (DynVar *)mobileDps.getNextVar();
		pDynFlags = (DynVar *)activeFlags.getNextVar();
			
	}
	pool.finishHistoryEqp();
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacNextDpeHistoryDepthQuery()
**		PVSS CTRL arguments are:
**			string &dpe
**			time &startTime
**			int &mobileType
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processNextDpeHistoryDepthQuery(ExecuteParamRec &param)
{
	Variable	*dpe, *startTime, *mobileType;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpe",		TEXT_VAR,		NULL,	&dpe },
		{ "startTime",	TIME_VAR,		NULL,	&startTime },
		{ "MobileType",	INTEGER_VAR,	NULL,	&mobileType }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DpeHistoryDepthQuery &pool = DpeHistoryDepthQuery::getInstance();
	QByteArray dpeName;
	QDateTime start;
	int mobType, coco;
	if(pool.getNext(dpeName, start, mobType))
	{
		((TextVar *)dpe)->setValue(dpeName.constData());
		((TimeVar *)startTime)->setSeconds(start.toTime_t());
		((TimeVar *)startTime)->setMilli(start.time().msec());
		((IntegerVar *)mobileType)->setValue(mobType);
		coco = 1;
	}
	else
	{
		((TextVar *)dpe)->setValue("");
		coco = 0;
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacSetDpeHistoryDepth()
**		PVSS CTRL arguments are:
**			string dpName
**			time absStartTime
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetDpeHistoryDepth(ExecuteParamRec &param)
{
	TextVar	dpName;
	TimeVar	absStartTime;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,	&dpName,		NULL },
		{ "absStartTime",	TIME_VAR,	&absStartTime,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QDateTime ts;
	ts.setTime_t(absStartTime.getSeconds());
	ts.setTime(ts.time().addMSecs(absStartTime.getMilli()));
	return VacMainView::setDpHistoryDepth(dpName.getValue(), ts);
}

/*
**	FUNCTION
**		Process call to LhcVacEqpSetActive()
**		PVSS CTRL arguments are:
**			string dpName
**			bool flag
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpSetActive(ExecuteParamRec &param)
{
	TextVar	dpName;
	BitVar	flag;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,	&dpName,		NULL },
		{ "flag",			BIT_VAR,	&flag,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	Eqp *pEqp = pool.findEqpByDpName(dpName);
	if(!pEqp)
	{
		return 0;
	}
	if(pEqp->getMobileType() != MobileType::Fixed)
	{
		return -1;
	}
	pEqp->setActive(DataEnum::Online, flag.getValue() == PVSS_TRUE);
	return 1;
}

/*
**	FUNCTION
**		Process call to LhcVacGetEqpIconType()
**		PVSS CTRL arguments are:
**			string dpName
**			int &type
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetEqpIconType(ExecuteParamRec &param)
{
	TextVar	dpName;
	Variable *type;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,		&dpName,	NULL },
		{ "type",		INTEGER_VAR,	NULL,		&type }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	((IntegerVar *)type)->setValue(0);
	DataPool &pool = DataPool::getInstance();
	Eqp *pEqp = pool.findEqpByDpName(dpName);
	if(!pEqp)
	{
		return 0;
	}
	((IntegerVar *)type)->setValue(VacIcon::getIconType(pEqp));
	return 1;
}
/**
 @brief Process call to  LhcVacGetEqpByMasterAndChannelNb
 @param[in]		masterDpName	dp name of the master of equipment we want
 @param[in]		channelNb		channel number of equipment we want
 @param[out]	&dpName			dp name of the equipment we want
*/
int VacCtlEqpDataPvss::processGetEqpByMasterAndChannelNb(ExecuteParamRec &param)
{
	TextVar masterDpName, dpType;
	IntegerVar	channelNb;
	Variable	*dpName;
	// Parse arguments
	SPvssArg	args[] = {
		{ "masterDpName", TEXT_VAR, &masterDpName, NULL },
		{ "channelNb", INTEGER_VAR, &channelNb, NULL },
		{ "dpType", TEXT_VAR, &dpType, NULL },
		{ "dpName", TEXT_VAR, NULL, &dpName }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0]))) {
		return -1;
	}
	((TextVar *)dpName)->setValue("");
	DataPool &pool = DataPool::getInstance();
	QString parsedDpName;
	int parsedChannelNb;
	foreach (Eqp *pEqp, pool.findEqpsByMasterDp((QString)masterDpName))	{
		pEqp->getMaster(parsedDpName, parsedChannelNb);
		if ((parsedDpName == (QString)masterDpName) && (parsedChannelNb == (int)channelNb) 
													&& ((QString)pEqp->getDpType() == (QString)dpType) ) {
			((TextVar *)dpName)->setValue(pEqp->getDpName());
			return 1;
		}
	}
	return 0;
}
/*
**	FUNCTION
**		Helper method: find in data pool device with given DP name,
**		optionally add error message to exceptionInfo if not found
**
**	PARAMETERS
**		dpName			- DP name to find
**		param			- All calling and execution parametrs can be accessed
**							via this class
**		exceptionInfo	- Pointer to DynVar where error message shall be added,
**							can be NULL
**
**	RETURNS
**		Pointer to device with given DP name, or
**		NULL if such device is not found
**
**	CAUTIONS
**		None
*/
Eqp *VacCtlEqpDataPvss::findDp(const char *dpName, ExecuteParamRec &param, DynVar *exceptionInfo)
{
	DataPool &pool = DataPool::getInstance();
	Eqp *pEqp = pool.findEqpByDpName(dpName);
	if(!pEqp)
	{
		if(exceptionInfo)
		{
			QString errMsg("Unknown DP <");
			errMsg += (const char *)dpName;
			errMsg += ">";
			addExceptionInfo(param, exceptionInfo, errMsg);
		}
	}
	return pEqp;
}




/*
**	FUNCTION
**		[VACCO-948] [VACCO-1645]
**		Helper method: find in data pool device with given DP name,
**		optionally add error message to exceptionInfo if not found
**
**	PARAMETERS
**		dpName			- DP name to find
**		param			- All calling and execution parametrs can be accessed
**							via this class
**		exceptionInfo	- Pointer to DynVar where error message shall be added,
**							can be NULL
**
**	RETURNS
**		Pointer to device with given DP name, or
**		NULL if such device is not found
**
**	CAUTIONS
**		None
*/
Sector *VacCtlEqpDataPvss::findSectorDp(const char *dpName, ExecuteParamRec &param, DynVar *exceptionInfo)
{
	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pool.findSectorByDpName(dpName);
	if (!pSector)
	{
		if (exceptionInfo)
		{
			QString errMsg("Unknown DP <");
			errMsg += (const char *)dpName;
			errMsg += ">";
			addExceptionInfo(param, exceptionInfo, errMsg);
		}
		return NULL;
	}
	return pSector;
}


/*
**	FUNCTION
**		Process call to LhcVacNextDpConnect()
**		PVSS CTRL arguments are:
**			string &dpe
**			int &dataType
**			bool &connect
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processNextDpConnect(ExecuteParamRec &param)
{
	Variable	*dpe, *connect;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpe",		TEXT_VAR,		NULL,	&dpe },
		{ "connect",	BIT_VAR,		NULL,	&connect }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DpConnection &connection = DpConnection::getInstance();
	QByteArray dpeName;
	int coco;
	bool connectFlag;
	if(connection.getNext(dpeName, connectFlag))
	{
		((TextVar *)dpe)->setValue(dpeName.constData());
		((BitVar *)connect)->setValue(connectFlag ? PVSS_TRUE : PVSS_FALSE);
		coco = 1;
	}
	else
	{
		((TextVar *)dpe)->setValue("");
		coco = 0;
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacNextDpPolling()
**		PVSS CTRL arguments are:
**			string &dpe
**			bool &connect
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processNextDpPolling(ExecuteParamRec &param)
{
	Variable	*dpe, *connect;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpe",		TEXT_VAR,		NULL,	&dpe },
		{ "connect",	BIT_VAR,		NULL,	&connect }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DpPolling &connection = DpPolling::getInstance();
	QByteArray dpeName;
	int coco;
	bool connectFlag;
	if(connection.getNext(dpeName, connectFlag))
	{
		((TextVar *)dpe)->setValue(dpeName.constData());
		((BitVar *)connect)->setValue(connectFlag ? PVSS_TRUE : PVSS_FALSE);
		coco = 1;
	}
	else
	{
		((TextVar *)dpe)->setValue("");
		coco = 0;
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacNewOnlineValue()
**		PVSS CTRL arguments are:
**			string dp
**			anytype value
**			time timeStamp
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processNewOnlineValue(ExecuteParamRec &param)
{
	TextVar	dp;
	AnyTypeVar	value;
	TimeVar		timeStamp;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dp",			TEXT_VAR,		&dp,	NULL },
		{ "value",		ANYTYPE_VAR,	&value,	NULL },
		{ "timeStamp",	TIME_VAR,		&timeStamp,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	int	coco = -1;
	char *dpName, *dpeName;
	if(splitDpName(dp, &dpName, &dpeName))
	{
//printf("VacCtlEqpDataPvss::processNewOnlineValue <%s> <%s>\n", dpName, dpeName);
		Eqp *pEqp = DataPool::getInstance().findEqpByDpName(dpName);
		if(pEqp)
		{
//printf("VacCtlEqpDataPvss::processNewOnlineValue() Eqp found\n");
			QVariant *pVariant = anyTypeToVariant(param, value);
			if(pVariant)
			{
//printf("VacCtlEqpDataPvss::processNewOnlineValue() QVariant OK\n");

				// [VACCO-948] [VACCO-1645]
				// Exception when the dpName is a Sector object, not an Eqp
				Sector *pSector = findSectorDp(dpName, param, NULL);
				if (pSector){
					pSector->stateChanged(pVariant->toUInt());
				}
				else{

					QDateTime ts;
					ts.setTime_t(timeStamp.getSeconds());
					ts.setTime(ts.time().addMSecs(timeStamp.getMilli()));
					pEqp->newOnlineValue(dpeName, *pVariant, ts);
					coco = 0;
					delete pVariant;
				}
			}
		}
		else	
		{
				// It can be BAKEOUT work DP from MobilePool
				QVariant *pVariant = anyTypeToVariant(param, value);
				if (pVariant)
				{
					//printf("VacCtlEqpDataPvss::processNewOnlineValue() QVariant OK\n");
					QDateTime ts;
					ts.setTime_t(timeStamp.getSeconds());
					ts.setTime(ts.time().addMSecs(timeStamp.getMilli()));
					MobilePool::getInstance().newOnlineValue(dpName, dpeName, *pVariant, ts);
					coco = 0;
					MobilePool::getInstance().newOnlineValue(dpName, dpeName, *pVariant, ts);
					delete pVariant;
					/*
					char *pDummy = NULL;
					*pDummy = '\0';
					*/
				}
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacNewReplayValue()
**		PVSS CTRL arguments are:
**			string dpe
**			anytype value
**			time timeStamp
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processNewReplayValue(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar	dpe;
	AnyTypeVar	value;
	TimeVar		timeStamp;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpe",		TEXT_VAR,		&dpe,		NULL },
		{ "value",		ANYTYPE_VAR,	&value,		NULL },
		{ "timeStamp",	TIME_VAR,		&timeStamp,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	// Notify replay pool that DPE value has been arrived
	ReplayPool::getInstance().notifyDataArrived(dpe);

	// Then process the value
	char *dpName, *dpeName;
	if(!splitDpName(dpe, &dpName, &dpeName))
	{
		return -1;
	}
	Eqp *pEqp = DataPool::getInstance().findEqpByDpName(dpName);
	if(!pEqp)
	{
		return -1;
	}

	QVariant *pVariant = anyTypeToVariant(param, value);
	if(!pVariant)
	{
		return -1;
	}
	QDateTime ts;
	ts.setTime_t(timeStamp.getSeconds());
	ts.setTime(ts.time().addMSecs(timeStamp.getMilli()));
	pEqp->newReplayValue((const char *)dpeName, *pVariant, ts);
	delete pVariant;
	return 0;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacNextHistoryQuery()
**		PVSS CTRL arguments are:
**			int &id
**			string &dpe
**			bool &bitState
**			int &bitNbr
**			time &start
**			time &end
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processNextHistoryQuery(ExecuteParamRec &param)
{
	Variable	*id, *dpe, *bitState, *bitNbr, *start, *end;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",			INTEGER_VAR,	NULL,	&id },
		{ "dpe",		TEXT_VAR,		NULL,	&dpe },
		{ "bitState",			BIT_VAR,	NULL,	&bitState },
		{ "bitNbr",			INTEGER_VAR,	NULL,	&bitNbr },
		{ "start",		TIME_VAR,		NULL,	&start },
		{ "end",		TIME_VAR,		NULL,	&end }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DpeHistoryQuery &queue = DpeHistoryQuery::getInstance();
	int		rangeId, bitNbrPar;
	bool	bitStatePar;
	QByteArray dpeName;
	QDateTime startTime, endTime;
	int coco;
	if(queue.getNext(rangeId, dpeName, bitStatePar, bitNbrPar, startTime, endTime))
	{
		((IntegerVar *)id)->setValue(rangeId);
		((TextVar *)dpe)->setValue(dpeName.constData());
		((BitVar *)bitState)->setValue(bitStatePar);
		((IntegerVar *)bitNbr)->setValue(bitNbrPar);
		((TimeVar *)start)->setSeconds(startTime.toTime_t());
		((TimeVar *)start)->setMilli(startTime.time().msec());
		((TimeVar *)end)->setSeconds(endTime.toTime_t());
		((TimeVar *)end)->setMilli(endTime.time().msec());
		coco = 1;
	}
	else
	{
		((TextVar *)dpe)->setValue("");
		coco = 0;
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacAddHistoryValues()
**		PVSS CTRL arguments are:
**			int id
**			string dpe
**			bool &bitState
**			int &bitNbr
**			dyn_time times
**			dyn_anytype values
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processAddHistoryValues(ExecuteParamRec &param)
{
	IntegerVar	id, bitNbr;
	TextVar		dpe;
	BitVar		bitState;
	DynVar		times, values;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",				INTEGER_VAR,	&id,		NULL },
		{ "dpe",			TEXT_VAR,		&dpe,		NULL },
		{ "bitState",		INTEGER_VAR,	&bitState,		NULL },
		{ "bitNbr",			INTEGER_VAR,	&bitNbr,		NULL },
		{ "times",			DYNTIME_VAR,	&times,		NULL },
		{ "values",			DYNANYTYPE_VAR,	&values,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,	&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	if(values.getNumberOfItems() != times.getNumberOfItems())
	{
		char buf[128];
		sprintf(buf, "Number of values %d does not match number of times %d",
			(int)values.getNumberOfItems(), (int)times.getNumberOfItems());
		addExceptionInfo(param, exceptionInfo, buf);
		return -1;
	}

	int coco = 0;
	Variable *pTimeVar, *pValueVar;
	int rangeId = id.getValue();
	bool isBitState = bitState.getValue() != PVSS_FALSE;
	int bitNumber = bitNbr.getValue();
	for(pTimeVar = times.getFirstVar(), pValueVar = values.getFirstVar() ; pTimeVar ;
			pTimeVar = times.getNextVar(), pValueVar = values.getNextVar())
	{
		QDateTime ts;
		ts.setTime_t(((TimeVar *)pTimeVar)->getSeconds());
		ts.setTime(ts.time().addMSecs(((TimeVar *)pTimeVar)->getMilli()));
		QVariant *pValue = anyTypeToVariant(param, *((AnyTypeVar *)pValueVar));
		if(pValue)
		{
			bool needData = VacMainView::addHistoryValue(rangeId, dpe,
				isBitState, bitNumber, ts, *pValue);
			delete pValue;
			if(!needData)
			{
				break;
			}
		}
	}
	VacMainView::finishHistoryRange(rangeId, dpe, isBitState, bitNumber);
	return coco;
}


/*
**	FUNCTION
**		Process call to LhcVacDevListGetDpes()
**		PVSS CTRL arguments are:
**			string dp
**			dyn_string &dpes
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDevListGetDpes(ExecuteParamRec &param)
{
	TextVar		dp;
	Variable	*dpes;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dp",		TEXT_VAR,		&dp,	NULL },
		{ "dpes",	DYNTEXT_VAR,	NULL,	&dpes }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)dpes)->clear();
	Eqp *pEqp = findDp(dp, param, NULL);
	if(!pEqp)
	{
		return -1;
	}
	QStringList dpeList;
	pEqp->getDpesForDevList(dpeList);
	foreach(QString dpeName, dpeList)
	{
		addToDynStringVar((DynVar *)dpes, dpeName.toLatin1());
	}
	return dpeList.count();
}

/*
**	FUNCTION
**		Process call to LhcVacDevListCheckCrit()
**		PVSS CTRL arguments are:
**			string dp
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDevListCheckCrit(ExecuteParamRec &param)
{
	TextVar		dp;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dp",		TEXT_VAR,		&dp,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dp, param, NULL);
	if(!pEqp)
	{
		return -1;
	}
	return pEqp->checkDevListCrit();
}

/*
**	FUNCTION
**		Process call to LhcVacDevListGetHistoryParam()
**		PVSS CTRL arguments are:
**			string dp
**			string &dpe
**			time &startTime
**			time &endTime
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDevListGetHistoryParam(ExecuteParamRec &param)
{
	TextVar		dp;
	Variable	*dpe, *startTime, *endTime;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dp",			TEXT_VAR,	&dp,	NULL },
		{ "dpe",		TEXT_VAR,	NULL,	&dpe },
		{ "startTime",	TIME_VAR,	NULL,	&startTime },
		{ "endTime",	TIME_VAR,	NULL,	&endTime }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)dpe)->setValue("");
	Eqp *pEqp = findDp(dp, param, NULL);
	if(!pEqp)
	{
		return -1;
	}
	QString dpeName;
	QDateTime start, end;
	pEqp->getHistoryParamForDevList(dpeName, start, end);
	if(dpeName.isEmpty())
	{
		return 0;
	}
	((TextVar *)dpe)->setValue(dpeName.toLatin1());
	((TimeVar *)startTime)->setSeconds(start.toTime_t());
	((TimeVar *)startTime)->setMilli(start.time().msec());
	((TimeVar *)endTime)->setSeconds(end.toTime_t());
	((TimeVar *)endTime)->setMilli(end.time().msec());
	
	return 1;
}

/*
**	FUNCTION
**		Process call to LhcVacDevListCheckHistory()
**		PVSS CTRL arguments are:
**			string dp
**			dyn_anytype values
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDevListCheckHistory(ExecuteParamRec &param)
{
	TextVar		dp;
	DynVar		values;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dp",			TEXT_VAR,		&dp,		NULL },
		{ "values",		DYNANYTYPE_VAR,	&values,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dp, param, NULL);
	if(!pEqp)
	{
		return -1;
	}
	float factor = pEqp->getGrowFactorForDevList();
	if(factor == 0.0)
	{
		return 0;
	}
	
	Variable *pValueVar;
	float minValue = 1.0E5;	// somthing non-existent
	for(pValueVar = values.getFirstVar() ; pValueVar ; pValueVar = values.getNextVar())
	{
		QVariant *pValue = anyTypeToVariant(param, *((AnyTypeVar *)pValueVar));
		if(pValue)
		{
			float newValue = (float)pValue->toDouble();
			delete pValue;
			if((newValue <= 0) || (newValue >= 10000))
			{
				continue;
			}
			if(newValue <= minValue)
			{
				minValue = newValue;
			}
			else if((newValue / minValue) > factor)
			{
				return 1;
			}
		}
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to LhcVacDevListCheckValueGrow()
**		PVSS CTRL arguments are:
**			string dp
**			dyn_anytype values
**			dyn_time times
**			anytype &minValue
**			time &firstLargeValueTime
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**
**	RETURNS
**		The value to be returned by corresponding PVSS function:
**		1 = matches criteria
**		0 = not matches criteria
**		-1 = error
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDevListCheckValueGrow(ExecuteParamRec &param)
{
	TextVar		dp;
	DynVar		values, times;
	FloatVar	minValue;
	TimeVar		firstLargeValueTime;
	Variable	*pNewMinValue, *pNewFirstLargeValueTime;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dp", TEXT_VAR, &dp, NULL },
		{ "values", DYNANYTYPE_VAR, &values, NULL },
		{ "values", DYNTIME_VAR, &times, NULL },
		{ "minValue", FLOAT_VAR, &minValue, &pNewMinValue },
		{ "firstLargeValueTime", TIME_VAR, &firstLargeValueTime, &pNewFirstLargeValueTime }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	if (values.getNumberOfItems() != times.getNumberOfItems())
	{
		char buf[128];
		sprintf(buf, "Number of values %d does not match number of times %d",
			(int)values.getNumberOfItems(), (int)times.getNumberOfItems());
		showError(param, ErrClass::ILLEGAL_ARG, buf);
		return -1;
	}

	Eqp *pEqp = findDp(dp, param, NULL);
	if (!pEqp)
	{
		return -1;
	}
	float factor = pEqp->getGrowFactorForDevList();
	if (factor == 0.0)
	{
		return 0;
	}


	int spikeFilter = pEqp->getSpikeFilterForDevList();
	int coco = 0;
	Variable *pTimeVar, *pValueVar;

	for (pTimeVar = times.getFirstVar(), pValueVar = values.getFirstVar(); pTimeVar;
		pTimeVar = times.getNextVar(), pValueVar = values.getNextVar())
	{
		QDateTime ts;
		ts.setTime_t(((TimeVar *)pTimeVar)->getSeconds());
		ts.setTime(ts.time().addMSecs(((TimeVar *)pTimeVar)->getMilli()));
		QVariant *pValue = anyTypeToVariant(param, *((AnyTypeVar *)pValueVar));
		if (pValue)
		{
			float newValue = (float)pValue->toDouble();
			delete pValue;
			if ((newValue <= 0) || (newValue >= 10000))
			{
				continue;
			}
			if (newValue <= minValue.getValue())
			{
				minValue.setValue(newValue);
			}
			else if ((newValue / minValue.getValue()) > factor)
			{
				if (spikeFilter <= 0)
				{
					return 1;
				}
				if (firstLargeValueTime == TimeVar::NullTimeVar)
				{
					firstLargeValueTime = *((TimeVar *)pTimeVar);
				}
				else
				{
					int deltaTime = ((TimeVar *)pTimeVar)->getSeconds() - firstLargeValueTime.getSeconds();
					if (deltaTime > spikeFilter)
					{
						return 1;
					}
				}
			}
			else
			{
				firstLargeValueTime = TimeVar::NullTimeVar;
			}
		}
	}
	((FloatVar *)pNewMinValue)->setValue(minValue);
	((TimeVar *)pNewFirstLargeValueTime)->setValue(firstLargeValueTime);
	return 0;
}


/*
**	FUNCTION
**		Process call to LhcVacEqpMainStateString()
**		PVSS CTRL arguments are:
**			string dpName
**			int mode
**			string &result
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpMainStateString(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName;
	IntegerVar	mode;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,		&dpName,	NULL },
		{ "mode",		INTEGER_VAR,	&mode,		NULL },
		{ "result",		TEXT_VAR,		NULL,		&result }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString modeErr;
	if(!checkMode(param, mode.getValue(), modeErr))
	{
		((TextVar *)result)->setValue(modeErr.toLatin1());
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		((TextVar *)result)->setValue("Unknown DP");
		return -1;
	}
	QString state;
	pEqp->getMainStateString(state, (DataEnum::DataMode)mode.getValue());
	((TextVar *)result)->setValue(state.toLatin1());
	return 0;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacEqpPartStateString()
**		PVSS CTRL arguments are:
**			string dpName
**			int part
**			int mode
**			string &result
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpPartStateString(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName;
	IntegerVar	part, mode;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,		&dpName,	NULL },
		{ "part",		INTEGER_VAR,	&part,		NULL },
		{ "mode",		INTEGER_VAR,	&mode,		NULL },
		{ "result",		TEXT_VAR,		NULL,		&result }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString modeErr;
	if(!checkMode(param, mode.getValue(), modeErr))
	{
		((TextVar *)result)->setValue(modeErr.toLatin1());
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		((TextVar *)result)->setValue("Unknown DP");
		return -1;
	}
	QString state;
	pEqp->getPartStateString(part.getValue(), state, (DataEnum::DataMode)mode.getValue());
	((TextVar *)result)->setValue(state.toLatin1());
	return 0;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacEqpMainValueString()
**		PVSS CTRL arguments are:
**			string dpName
**			int mode
**			string &result
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpMainValueString(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName;
	IntegerVar	mode;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,		&dpName,	NULL },
		{ "mode",		INTEGER_VAR,	&mode,		NULL },
		{ "result",		TEXT_VAR,		NULL,		&result }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString modeErr;
	if(!checkMode(param, mode.getValue(), modeErr))
	{
		((TextVar *)result)->setValue(modeErr.toLatin1());
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		((TextVar *)result)->setValue("Unknown DP");
		return -1;
	}
	QString state;
	pEqp->getMainValueString(state, (DataEnum::DataMode)mode.getValue());
	((TextVar *)result)->setValue(state.toLatin1());
	return 0;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacEqpMainStateStringForMenu()
**		PVSS CTRL arguments are:
**			string dpName
**			int mode
**			string &result
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpMainStateStringForMenu(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName;
	IntegerVar	mode;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",	TEXT_VAR,		&dpName,	NULL },
		{ "mode",	INTEGER_VAR,	&mode,		NULL },
		{ "result",	TEXT_VAR,		NULL,		&result }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString modeErr;
	if(!checkMode(param, mode.getValue(), modeErr))
	{
		((TextVar *)result)->setValue(modeErr.toLatin1());
		return -1;
	}

	// [VACCO-948] [VACCO-1645]
	// Added the case where the dpName is from a Sector object, not an Eqp
	Sector *pSector = findSectorDp(dpName, param, NULL);
	if (pSector)
	{
		QString state;
		pSector->getMainStateStringForMenu(state);
		((TextVar *)result)->setValue(state.toLatin1());
		return 0;
	}

	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		((TextVar *)result)->setValue("Unknown DP");
		return -1;
	}
	else
	{
		QString state;
		pEqp->getMainStateStringForMenu(state, (DataEnum::DataMode)mode.getValue());
		((TextVar *)result)->setValue(state.toLatin1());
		return 0;
	}
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacEqpMainColor()
**		PVSS CTRL arguments are:
**			string dpName
**			int mode
**			string &result
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpMainColor(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName;
	IntegerVar	mode;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",	TEXT_VAR,		&dpName,	NULL },
		{ "mode",	INTEGER_VAR,	&mode,		NULL },
		{ "result",	TEXT_VAR,		NULL,		&result }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString modeErr;
	if(!checkMode(param, mode.getValue(), modeErr))
	{
		((TextVar *)result)->setValue(modeErr.toLatin1());
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		((TextVar *)result)->setValue("Unknown DP");
		return -1;
	}
	QColor color;
	pEqp->getMainColor(color, (DataEnum::DataMode)mode.getValue());
	char buf[256];
	sprintf(buf, "{%d,%d,%d}", color.red(), color.green(), color.blue());
	((TextVar *)result)->setValue(buf);
	return 0;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacEqpPartColor()
**		PVSS CTRL arguments are:
**			string dpName
**			int part
**			int mode
**			string &result
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpPartColor(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName;
	IntegerVar	part, mode;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",	TEXT_VAR,		&dpName,	NULL },
		{ "part",	INTEGER_VAR,	&part,		NULL },
		{ "mode",	INTEGER_VAR,	&mode,		NULL },
		{ "result",	TEXT_VAR,		NULL,		&result }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString modeErr;
	if(!checkMode(param, mode.getValue(), modeErr))
	{
		((TextVar *)result)->setValue(modeErr.toLatin1());
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		((TextVar *)result)->setValue("Unknown DP");
		return -1;
	}
	QColor color;
	pEqp->getPartColor(part.getValue(), color, (DataEnum::DataMode)mode.getValue());
	char buf[256];
	sprintf(buf, "{%d,%d,%d}", color.red(), color.green(), color.blue());
	((TextVar *)result)->setValue(buf);
	return 0;
#else
	return 0;
#endif
}

/**
@brief FUNCTION Process call to LhcVacSubEqpColor()
@param [in,out]	param	List of argument (string dpName, string subEqpType, int mode, string &result)
**/
int VacCtlEqpDataPvss::processSubEqpColor(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName, subEqpType;
	IntegerVar	mode;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName", TEXT_VAR, &dpName, NULL },
		{ "subEqpType", TEXT_VAR, &subEqpType, NULL },
		{ "mode", INTEGER_VAR, &mode, NULL },
		{ "result", TEXT_VAR, NULL, &result }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString modeErr;
	if (!checkMode(param, mode.getValue(), modeErr))
	{
		((TextVar *)result)->setValue(modeErr.toLatin1());
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, NULL);
	if (!pEqp)
	{
		((TextVar *)result)->setValue("Unknown DP");
		return -1;
	}
	QColor color;
	pEqp->getSubEqpColor((QString)subEqpType, color, (DataEnum::DataMode)mode.getValue());
	char buf[256];
	sprintf(buf, "{%d,%d,%d}", color.red(), color.green(), color.blue());
	((TextVar *)result)->setValue(buf);
	return 0;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacEqpDpeValue()
**		PVSS CTRL arguments are:
**			string dpName
**			string dpe
**			anytype &value
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpDpeValue(ExecuteParamRec &param)
{
	TextVar		dpName, dpe;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",	TEXT_VAR,		&dpName,	NULL },
		{ "dpe",	TEXT_VAR,		&dpe,		NULL },
		{ "result",	ANYTYPE_VAR,	NULL,		&result }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = DataPool::getInstance().findEqpByDpName(dpName);
	if(!pEqp)
	{
		QString errMsg = "Unknown DP <";
		errMsg += (const char *)dpName;
		errMsg += ">";
		showError(param, ErrClass::ILLEGAL_ARG, errMsg);
		return -1;
	}
	QDateTime timeStamp;
	const QVariant &value = pEqp->getDpeValue(dpe, DataEnum::Online, timeStamp);
	if(!value.isValid())
	{
		QString errMsg = "No online value for ";
		errMsg += (const char *)dpName;
		errMsg += ".";
		errMsg += (const char *)dpe;
		showError(param, ErrClass::ILLEGAL_ARG, errMsg);
		return -1;
	}
	variantToAnyType(param, value, (AnyTypeVar *)result);
	return 1;
}

/*
**	FUNCTION
**		Process call to LhcVacEqpPlcAlarm()
**		PVSS CTRL arguments are:
**			string dpName
**			bool &value
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpPlcAlarm(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*result;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",	TEXT_VAR,		&dpName,	NULL },
		{ "result",	BIT_VAR,	NULL,		&result }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = DataPool::getInstance().findEqpByDpName(dpName);
	if(!pEqp)
	{
		QString errMsg = "Unknown DP <";
		errMsg += (const char *)dpName;
		errMsg += ">";
		showError(param, ErrClass::ILLEGAL_ARG, errMsg);
		return -1;
	}
//	qDebug("VacCtlEqpDataPvss::processEqpPlcAlarm(%s) = %d\n", pEqp->getName(), (int)pEqp->isPlcAlarm(DataEnum::Online));
	((BitVar *)result)->setValue(pEqp->isPlcAlarm(DataEnum::Online));
	return 1;
}

/*
**	FUNCTION
**		Process call to LhcVacEqpGetHistoryState()
**		PVSS CTRL arguments are:
**			int id
**			string dpName
**			time ts
**			string &state
**			string &color
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processEqpGetHistoryState(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar	id;
	TextVar		dpName;
	TimeVar		ts;
	Variable	*state, *color, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",				TEXT_VAR,		&id,		NULL },
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "ts",				TIME_VAR,		&ts,		NULL },
		{ "state",			TEXT_VAR,		NULL,		&state },
		{ "color",			TEXT_VAR,		NULL,		&color },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	VacCtlStateArchive *pPool = VacCtlStateArchive::findInstance(id.getValue());
	if(!pPool)
	{
		 char errMsg[256];
		sprintf(errMsg, "State archive data instance with ID %d not found",
			(int)id.getValue());
		addExceptionInfo(param, exceptionInfo, errMsg);
		return -1;
	}
	Eqp *pEqp = findDp(dpName, param, NULL);
	if(!pEqp)
	{
		((TextVar *)state)->setValue("Unknown DP");
		return -1;
	}
	QDateTime	tsQt;
	tsQt.setTime_t(ts.getSeconds());
	tsQt.setTime(tsQt.time().addMSecs(ts.getMilli()));
	QColor 		colorQt;
	QString		stateQt;
	pEqp->getHistoryState(pPool, tsQt, stateQt, colorQt);
	((TextVar *)state)->setValue(stateQt.toLatin1());
	char buf[256];
	sprintf(buf, "{%d,%d,%d}", colorQt.red(), colorQt.green(), colorQt.blue());
	((TextVar *)color)->setValue(buf);
	return 0;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacOpenDialog()
**		PVSS CTRL arguments are:
**			int type
**			unsigned vacTypes
**			string firstSector
**			string lastSector
**			int mode
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processOpenDialog(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar	type;
	UIntegerVar	vacTypes;
	TextVar		firstSector, lastSector;
	IntegerVar	mode;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "type",			INTEGER_VAR,	&type,			NULL },
		{ "vacTypes",		UINTEGER_VAR,	&vacTypes,		NULL },
		{ "firstSector",	TEXT_VAR,		&firstSector,	NULL },
		{ "lastSector",		TEXT_VAR,		&lastSector,	NULL },
		{ "mode",			INTEGER_VAR,	&mode,			NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString modeErr;
	if(!checkMode(param, mode.getValue(), modeErr))
	{
		addExceptionInfo(param, exceptionInfo, modeErr.toLatin1());
		return -1;
	}

	QStringList	errList;
	int coco = VacMainView::openDialog(type.getValue(), vacTypes.getValue(),
			firstSector, lastSector, (DataEnum::DataMode)mode.getValue(), errList);
		addExceptionInfo(param, exceptionInfo, errList);
	
	return coco;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacOpenDialog()
**		PVSS CTRL arguments are:
**			int type
**			unsigned vacTypes
**			string firstSector
**			string lastSector
**			int mode
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processOpenDocumentationDialog(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION	
	IntegerVar	type;
	UIntegerVar	vacTypes;
	TextVar		equipmentName;
	IntegerVar	mode;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "type", INTEGER_VAR, &type, NULL },
		{ "vacTypes", UINTEGER_VAR, &vacTypes, NULL },
		{ "equipmentName", TEXT_VAR, &equipmentName, NULL },
		{ "mode", INTEGER_VAR, &mode, NULL },
		{ "exceptionInfo", DYNTEXT_VAR, NULL, &exceptionInfo }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString modeErr;
	if(!checkMode(param, mode.getValue(), modeErr))
	{
		addExceptionInfo(param, exceptionInfo, modeErr.toLatin1());
		return -1;
	}

	QString parentEqpName="";
	DataPool & pool = DataPool::getInstance();
	Eqp * pEqp = pool.findEqpByVisibleName(equipmentName);
	
	
	if (pEqp)
	{
		parentEqpName = pEqp->getAttrValue("RackControlParent");
		if (parentEqpName == NULL)
		{
			parentEqpName = "";
		}
	}

	QStringList	errList;
	int coco = -1;
	if (type.getValue() == VacMainView::DialogRackDiagram)
	{
		coco = VacMainView::openDocumentationDialog(type.getValue(), vacTypes.getValue(),
			parentEqpName.toUtf8().data(), (DataEnum::DataMode)mode.getValue(), errList);
		addExceptionInfo(param, exceptionInfo, errList);
	}
	if (type.getValue() == VacMainView::DialogInterlockSchematic)
	{
		coco = VacMainView::openDocumentationDialog(type.getValue(), vacTypes.getValue(),
			equipmentName, (DataEnum::DataMode)mode.getValue(), errList);
		addExceptionInfo(param, exceptionInfo, errList);
	}
	return coco;

#else
return 0;
#endif
}
/*
**	FUNCTION
**		Process call to LhcVacMakeDeviceVisible()
**		PVSS CTRL arguments are:
**			string dpName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processMakeDeviceVisible(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",	TEXT_VAR,	&dpName,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	Eqp *pEqp = pool.findEqpByDpName(dpName);
	int coco = 0;
	if(pEqp)
	{
		VacMainView::makeDeviceVisible(pEqp);
		coco = 1;
	}
	return coco;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacConfirmEvConnLost",
**		PVSS CTRL arguments are:
**			time connLostTime
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processConfirmEvConnLost(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TimeVar		connLostTime;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "connLostTime",	TIME_VAR,	&connLostTime,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QString text = "Connection to Event Manager has been lost at ";
	QDateTime lostTime;
	lostTime.setTime_t(connLostTime.getSeconds());
	text += lostTime.toString("dd-MM-yyyy hh:mm:ss");
	text += "\nAll data shown in opened panels may be invalid after that time.";
	text += "\nDo You want to close application ?";
	QWidget *mainView = VacMainView::getInstance();
	QMessageBox::StandardButton answer = QMessageBox::critical(mainView, "Vacuum Control UI", text, QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
	return answer == QMessageBox::Yes ? 1 : 0;
#else
	return 0;
#endif
}


/*
**	FUNCTION
**		Process call to LhcVacAddToHistoryDialog()
**		PVSS CTRL arguments are:
**			string dpName
**			int historyDepth
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processAddToHistoryDialog(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName;
	IntegerVar	historyDepth;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "historyDepth",	INTEGER_VAR,	&historyDepth,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	if(!pEqp)
	{
		return -1;
	}

	return VacMainView::addToHistory(pEqp, historyDepth.getValue());
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacAddToHistoryInstance()
**		PVSS CTRL arguments are:
**			int instanceId
**			string dpName
**			string colorString
**			int historyDepth
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processAddToHistoryInstance(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName, colorString;
	IntegerVar	instanceId, historyDepth;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",		INTEGER_VAR,	&instanceId,	NULL },
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "colorString",	TEXT_VAR,		&colorString,	NULL },
		{ "historyDepth",	INTEGER_VAR,	&historyDepth,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	if(!pEqp)
	{
		return -1;
	}

	return VacMainView::addToHistory(instanceId.getValue(), pEqp, colorString, historyDepth.getValue());
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacAddDpeToHistoryDialog()
**		PVSS CTRL arguments are:
**			string dpName: PVSS Data Point Name (Ex: "VPC_101_116_1A")
**			string dpeName: PVSS Data Point Element suffix name (Ex: "PR" or "P1PV" or "T1PV")
**			string yAxisName: Vertical Axis type name ("Pressure" or "CryoTemperature" or "Temperature" or "Bit" or "Current" or "Voltage" other string are link to the default axis)
**			int historyDepth: Depth of the History dialog (get with GetDpeHistoryDepth(string dpName, dyn_string &exceptionInfo))
**			dyn_string &exceptionInfo: Exception info message
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**  COMMENTS
**		The DP value type could not be used to select the appropriate vertical axis (several analog values DPE with different type of value), the arguments yAxisName is used to select the correct vertical axis 
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processAddDpeToHistoryDialog(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName, dpeName, yAxisName;
	IntegerVar	historyDepth;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "dpeName",			TEXT_VAR,		&dpeName,		NULL },
		{ "yAxisName",			TEXT_VAR,		&yAxisName,		NULL },
		{ "historyDepth",	INTEGER_VAR,	&historyDepth,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	if(!pEqp)
	{
		return -1;
	}

	return VacMainView::addDpeToHistory(pEqp, dpeName, yAxisName, historyDepth.getValue());
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacAddDpeToHistoryInstance()
**		PVSS CTRL arguments are:
**			int instanceId
**			string dpName: PVSS Data Point Name (Ex: "VPC_101_116_1A")
**			string dpeName: PVSS Data Point Element suffix name (Ex: "PR" or "P1PV" or "T1PV")
**			string yAxisName: Vertical Axis type name ("Pressure" or "CryoTemperature" or "Temperature" or "Bit" or "Current" or "Voltage" other string are link to the default axis)
**			string colorString
**			int historyDepth: Depth of the History dialog (get with GetDpeHistoryDepth(string dpName, dyn_string &exceptionInfo))
**			dyn_string &exceptionInfo: Exception info message
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**  COMMENTS
**		The DP value type could not be used to select the appropriate vertical axis (several analog values DPE with different type of value), the arguments yAxisName is used to select the correct vertical axis 
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processAddDpeToHistoryInstance(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName, dpeName, yAxisName, colorString;
	IntegerVar	instanceId, historyDepth;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",		INTEGER_VAR,	&instanceId,	NULL },
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "dpeName",		TEXT_VAR,		&dpeName,		NULL },
		{ "yAxisName",		TEXT_VAR,		&yAxisName,		NULL },
		{ "colorString",	TEXT_VAR,		&colorString,	NULL },
		{ "historyDepth",	INTEGER_VAR,	&historyDepth,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	if(!pEqp)
	{
		return -1;
	}

	return VacMainView::addDpeToHistory(instanceId.getValue(), pEqp, dpeName, yAxisName, colorString, historyDepth.getValue());
#else
	return 0;
#endif
}


/*
**	FUNCTION
**		Process call to LhcVacAddBitToHistoryDialog()
**		PVSS CTRL arguments are:
**			string dpName
**			string dpeName,
**			int bitNbr
**			string bitName
**			int historyDepth
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processAddBitToHistoryDialog(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName, dpeName, bitName;
	IntegerVar	bitNbr, historyDepth;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "dpeName",		TEXT_VAR,		&dpeName,		NULL },
		{ "bitNbr",			INTEGER_VAR,	&bitNbr,		NULL },
		{ "bitName",		TEXT_VAR,		&bitName,		NULL },
		{ "historyDepth",	INTEGER_VAR,	&historyDepth,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	if(!pEqp)
	{
		return -1;
	}

	return VacMainView::addToBitHistory(pEqp, dpeName, bitNbr.getValue(), bitName, historyDepth.getValue());
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacAddBitToHistoryInstance()
**		PVSS CTRL arguments are:
**			int instanceId
**			string dpName
**			string dpeName,
**			int bitNbr
**			string bitName
**			string colorString
**			int historyDepth
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processAddBitToHistoryInstance(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	TextVar		dpName, dpeName, bitName, colorString;
	IntegerVar	instanceId, bitNbr, historyDepth;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",		INTEGER_VAR,	&instanceId,	NULL },
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "dpeName",		TEXT_VAR,		&dpeName,		NULL },
		{ "bitNbr",			INTEGER_VAR,	&bitNbr,		NULL },
		{ "bitName",		TEXT_VAR,		&bitName,		NULL },
		{ "colorString",	TEXT_VAR,		&colorString,	NULL },
		{ "historyDepth",	INTEGER_VAR,	&historyDepth,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	Eqp *pEqp = findDp(dpName, param, (DynVar *)exceptionInfo);
	if(!pEqp)
	{
		return -1;
	}

	return VacMainView::addToBitHistory(instanceId.getValue(), pEqp, dpeName, bitNbr.getValue(), bitName, colorString, historyDepth.getValue());
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacCreateHistoryInstance()
**		PVSS CTRL arguments are:
**			None
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processCreateHistoryInstance(ExecuteParamRec & /* param */)
{
#ifndef PVSS_SERVER_VERSION

	return VacMainView::createHistory();
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacClearHistoryInstance()
**		PVSS CTRL arguments are:
**			int instanceId
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processClearHistoryInstance(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar	instanceId;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",		INTEGER_VAR,	&instanceId,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	VacMainView::clearHistory(instanceId.getValue());
	return 0;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacGetHistoryFilterType()
**		PVSS CTRL arguments are:
**			int instanceId
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetHistoryFilterType(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar	instanceId;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",		INTEGER_VAR,	&instanceId,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	return VacMainView::getHistoryFilterType(instanceId.getValue());
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacSetHistoryFilterType()
**		PVSS CTRL arguments are:
**			int instanceId
**			int type
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetHistoryFilterType(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar	instanceId, type;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",		INTEGER_VAR,	&instanceId,	NULL },
		{ "type",			INTEGER_VAR,	&type,			NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	return VacMainView::setHistoryFilterType(instanceId.getValue(), type.getValue());
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacGetAllHistoryItems()
**		PVSS CTRL arguments are:
**			int instanceId
**			dyn_string &dps
**			dyn_string &dpes
**			dyn_int &bitNbrs
**			dyn_string &colors
**			dyn_string &axisTypeNames
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetAllHistoryItems(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar	instanceId;
	Variable	*dps, *dpes, *bitNbrs, *colors, *axisTypeNames;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",		INTEGER_VAR,	&instanceId,	NULL },
		{ "dps",			DYNTEXT_VAR,	NULL,			&dps },
		{ "dpes",			DYNTEXT_VAR,	NULL,			&dpes },
		{ "bitNbrs",		DYNINTEGER_VAR,	NULL,			&bitNbrs },
		{ "colors",			DYNTEXT_VAR,	NULL,			&colors },
		{ "axisTypeNames",	DYNTEXT_VAR,	NULL,			&axisTypeNames }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)dps)->clear();
	((DynVar *)dpes)->clear();
	((DynVar *)bitNbrs)->clear();
	((DynVar *)colors)->clear();
	((DynVar *)axisTypeNames)->clear();

	QStringList dpNames, dpeNames, colorNames, typeNames;
	int *bitNbrArray;
	VacMainView::getAllHistoryItems(instanceId.getValue(), dpNames, dpeNames, &bitNbrArray, colorNames, typeNames);

	int count = 0;
	QString name;
	foreach(name, dpNames)
	{
		addToDynStringVar((DynVar *)dps, name.toLatin1(), false);
	}
	foreach(name, dpeNames)
	{
		addToDynStringVar((DynVar *)dpes, name.toLatin1(), false);
		addToDynIntVar((DynVar *)bitNbrs, bitNbrArray[count++], false);
	}
	foreach(name, colorNames)
	{
		addToDynStringVar((DynVar *)colors, name.toLatin1(), false);
	}
	foreach(name, typeNames)
	{
		addToDynStringVar((DynVar *)axisTypeNames, name.toLatin1(), false);
	}
	if(bitNbrArray)
	{
		free((void *)bitNbrArray);
	}
	return count;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacGetHistoryTimeScale()
**		PVSS CTRL arguments are:
**			int instanceId
**			time &start
**			time &end
**			bool &online
**			bool &logScale
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetHistoryTimeScale(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar instanceId;
	Variable	*start, *end, *online, *logScale;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",	INTEGER_VAR,	&instanceId,	NULL },
		{ "start",		TIME_VAR,		NULL,			&start },
		{ "end",		TIME_VAR,		NULL,			&end },
		{ "online",		BIT_VAR,		NULL,			&online },
		{ "logScale",	BIT_VAR,		NULL,			&logScale }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QDateTime startTime, endTime;
	bool flagOnline, flagLogScale;
	int coco = VacMainView::getHistoryTimeScaleParam(instanceId.getValue(), startTime, endTime, flagOnline, flagLogScale);
	if(!coco)	// No instance, set some 'reasonable' defaults
	{
		endTime = QDateTime::currentDateTime();
		startTime = endTime.addSecs(- 4 * 60 * 60);
		flagOnline = true;
		flagLogScale = false;
	}
	((TimeVar *)start)->setSeconds(startTime.toTime_t());
	((TimeVar *)start)->setMilli(startTime.time().msec());
	((TimeVar *)end)->setSeconds(endTime.toTime_t());
	((TimeVar *)end)->setMilli(endTime.time().msec());
	((BitVar *)online)->setValue(flagOnline ? PVSS_TRUE : PVSS_FALSE);
	((BitVar *)logScale)->setValue(flagLogScale ? PVSS_TRUE : PVSS_FALSE);
	return coco;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacSetHistoryTimeScale()
**		PVSS CTRL arguments are:
**			int instanceId
**			time start
**			time end
**			bool online
**			bool logScale
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetHistoryTimeScale(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar instanceId;
	TimeVar	start, end;
	BitVar	online, logScale;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",	INTEGER_VAR,	&instanceId,	NULL },
		{ "start",		TIME_VAR,		&start,			NULL },
		{ "end",		TIME_VAR,		&end,			NULL },
		{ "online",		BIT_VAR,		&online,		NULL },
		{ "logScale",	BIT_VAR,		&logScale,		NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QDateTime startTime;
	startTime.setTime_t(start.getSeconds());
	startTime.setTime(startTime.time().addMSecs(start.getMilli()));

	QDateTime endTime;
	endTime.setTime_t(end.getSeconds());
	endTime.setTime(endTime.time().addMSecs(end.getMilli()));

	int coco = VacMainView::setHistoryTimeScaleParam(instanceId.getValue(), startTime, endTime,
		online.getValue() == PVSS_TRUE, logScale.getValue() == PVSS_TRUE);
	return coco;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacGetHistoryVerticalScales()
**		PVSS CTRL arguments are:
**			int instanceId
**			dyn_int &types
**			dyn_bool &logs
**			dyn_int &mins
**			dyn_int &maxs
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetHistoryVerticalScales(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar	instanceId;
	Variable	*types, *logs, *mins, *maxs;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",		INTEGER_VAR,	&instanceId,	NULL },
		{ "types",			DYNINTEGER_VAR,	NULL,			&types },
		{ "logs",			DYNBIT_VAR,	NULL,			&logs },
		{ "mins",			DYNFLOAT_VAR,	NULL,			&mins },
		{ "maxs",			DYNFLOAT_VAR,	NULL,			&maxs }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)types)->clear();
	((DynVar *)logs)->clear();
	((DynVar *)mins)->clear();
	((DynVar *)maxs)->clear();

	int nScales = 0, *typeArray = NULL;
	bool *logArray = NULL;
	float *minArray = NULL, *maxArray = NULL;
	int coco = VacMainView::getVerticalScalesParams(instanceId.getValue(), nScales, &typeArray, &logArray, &minArray, &maxArray);

	for(int n = 0 ; n < nScales ; n++)
	{
		addToDynIntVar((DynVar *)types, typeArray[n], false);
		addToDynBoolVar((DynVar *)logs, logArray[n], false);
		addToDynFloatVar((DynVar *)mins, minArray[n], false);
		addToDynFloatVar((DynVar *)maxs, maxArray[n], false);
	}
	if(typeArray)
	{
		free((void *)typeArray);
	}
	if(logArray)
	{
		free((void *)logArray);
	}
	if(minArray)
	{
		free((void *)minArray);
	}
	if(maxArray)
	{
		free((void *)maxArray);
	}
	return coco;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacSetHistoryVerticalScale()
**		PVSS CTRL arguments are:
**			int instanceId
**			int type
**			bool log
**			float min
**			float max
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetHistoryVerticalScale(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar instanceId, type;
	BitVar	log;
	FloatVar min, max;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",	INTEGER_VAR,	&instanceId,	NULL },
		{ "type",		INTEGER_VAR,	&type,			NULL },
		{ "log",		BIT_VAR,		&log,			NULL },
		{ "min",		FLOAT_VAR,		&min,			NULL },
		{ "max",		FLOAT_VAR,		&max,			NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	int coco = VacMainView::setVerticalScaleParams(instanceId.getValue(), type.getValue(),
		log.getValue() == PVSS_TRUE, min.getValue(), max.getValue());
	return coco;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacGetHistoryTitle()
**		PVSS CTRL arguments are:
**			int instanceId
**			string &title
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetHistoryTitle(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar instanceId;
	Variable *title;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",	INTEGER_VAR,	&instanceId,	NULL },
		{ "title",		TEXT_VAR,		NULL,			&title }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	const QString &winTitle = VacMainView::getHistoryTitle(instanceId.getValue());
	if(winTitle.isEmpty())
	{
		((TextVar *)title)->setValue("");
	}
	else
	{
		((TextVar *)title)->setValue(winTitle.toLatin1());
	}
	return 1;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacSetHistoryTitle()
**		PVSS CTRL arguments are:
**			int instanceId
**			string title
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetHistoryTitle(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar instanceId;
	TextVar title;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",	INTEGER_VAR,	&instanceId,	NULL },
		{ "title",		TEXT_VAR,		&title,			NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	int coco = VacMainView::setHistoryTitle(instanceId.getValue(), title);
	return coco;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacGetHistoryGeometry()
**		PVSS CTRL arguments are:
**			int instanceId
**			int &x
**			int &y
**			int &w
**			int &h
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetHistoryGeometry(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar instanceId;
	Variable *x, *y, *w, *h;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",	INTEGER_VAR,	&instanceId,	NULL },
		{ "x",			INTEGER_VAR,	NULL,			&x },
		{ "y",			INTEGER_VAR,	NULL,			&y },
		{ "w",			INTEGER_VAR,	NULL,			&w },
		{ "h",			INTEGER_VAR,	NULL,			&h }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	int winX, winY, winW, winH, coco;
	coco = VacMainView::getHistoryWindowGeometry(instanceId.getValue(), winX, winY, winW, winH);
	if(coco)
	{
		((IntegerVar *)x)->setValue(winX);
		((IntegerVar *)y)->setValue(winY);
		((IntegerVar *)w)->setValue(winW);
		((IntegerVar *)h)->setValue(winH);
	}
	else
	{
		((IntegerVar *)x)->setValue(0);
		((IntegerVar *)y)->setValue(0);
		((IntegerVar *)w)->setValue(0);
		((IntegerVar *)h)->setValue(0);
	}
	return coco;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacSetHistoryGeometry()
**		PVSS CTRL arguments are:
**			int instanceId
**			int x
**			int y
**			int w
**			int h
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetHistoryGeometry(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar instanceId, x, y, w, h;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",	INTEGER_VAR,	&instanceId,	NULL },
		{ "x",			INTEGER_VAR,	&x,				NULL },
		{ "y",			INTEGER_VAR,	&y,				NULL },
		{ "w",			INTEGER_VAR,	&w,				NULL },
		{ "h",			INTEGER_VAR,	&h,				NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	return VacMainView::setHistoryWindowGeometry(instanceId.getValue(),
		x.getValue(), y.getValue(), w.getValue(), h.getValue());
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacGetAllHistoryInstances()
**		PVSS CTRL arguments are:
**			dyn_int &allIds
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetAllHistoryInstances(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	Variable *allIds;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "allIds",	DYNINTEGER_VAR,	NULL,	&allIds }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)allIds)->clear();
	QList<int> idList;
	int coco = VacMainView::getAllHistoryInstaces(idList);
	for(int idx = 0 ; idx < idList.count() ; idx++)
	{
		addToDynIntVar((DynVar *)allIds, idList.at(idx));
	}
	return coco;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacSetHistoryConfigName()
**		PVSS CTRL arguments are:
**			int instanceId
**			string name
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetHistoryConfigName(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar instanceId;
	TextVar	name;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",	INTEGER_VAR,	&instanceId,	NULL },
		{ "name",		TEXT_VAR,		&name,			NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	return VacMainView::setHistoryConfigName(instanceId.getValue(), (const char *)name);
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacGetHistoryConfigName()
**		PVSS CTRL arguments are:
**			int instanceId
**			string &name
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processGetHistoryConfigName(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar instanceId;
	Variable	*name;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",	INTEGER_VAR,	&instanceId,	NULL },
		{ "name",		TEXT_VAR,		NULL,			&name }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	const QString configName = VacMainView::getHistoryConfigName(instanceId.getValue());
	((TextVar *)name)->setValue(configName.toLatin1());
	return !configName.isEmpty();
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacGetHistoryAllComments()
**		PVSS CTRL arguments are:
**			int instanceId
**			dyn_string &comments
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::progessGetHistoryAllComments(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar	instanceId;
	Variable	*comments;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",		INTEGER_VAR,	&instanceId,	NULL },
		{ "comments",		DYNTEXT_VAR,	NULL,			&comments }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)comments)->clear();

	QStringList commentList;
	int coco = VacMainView::getHistoryAllComments(instanceId.getValue(), commentList);

	QString name;
	foreach(name, commentList)
	{
		addToDynStringVar((DynVar *)comments, name.toLatin1(), false);
	}
	return coco;
#else
	return 0;
#endif
}

/*
**	FUNCTION
**		Process call to LhcVacSetHistoryAllComments()
**		PVSS CTRL arguments are:
**			int instanceId
**			dyn_string comments
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::progessSetHistoryAllComments(ExecuteParamRec &param)
{
#ifndef PVSS_SERVER_VERSION
	IntegerVar	instanceId;
	DynVar	comments;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "instanceId",		INTEGER_VAR,	&instanceId,	NULL },
		{ "comments",		DYNTEXT_VAR,	&comments,		NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	QStringList commentList;
	for(unsigned short idx = 0 ; idx < comments.getNumberOfItems() ; idx ++)
	{
		TextVar *pTextVar = (TextVar *)comments.getAt(idx);
		commentList.append((const char *)pTextVar->getValue());
	}

	int coco = VacMainView::setHistoryAllComments(instanceId.getValue(), commentList);
	return coco;
#else
	return 0;
#endif
}


/*
**
** FUNCTION
**		Process call to LhcVacDpForMobileOnFlange()
**		PVSS CTRL arguments are:
**			string sourceDp
**			string targetDpType
**			int locationIndex
**			string &targetDp
**			dyn_string &exceptionInfo
**
** PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
** RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDpForMobileOnFlange(ExecuteParamRec &param)
{
	TextVar		sourceDp, targetDpType;
	IntegerVar	locationIndex;
	Variable	*targetDp, *exceptionInfo;

		// Parse arguments
	SPvssArg	args[] =
	{
		{ "sourceDp",		TEXT_VAR,		&sourceDp,		NULL },
		{ "targetDpType",	TEXT_VAR,		&targetDpType,	NULL },
		{ "locationIndex",	INTEGER_VAR,	&locationIndex,	NULL },
		{ "targetDp",		TEXT_VAR,		NULL,			&targetDp },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)targetDp)->setValue("");

	// Find PLC data for source DP
	PlcConfig	*pPlc;
	QString errMsg;
	int coco = findSourcePlc(sourceDp, &pPlc, errMsg);
	if( coco < 0 )
	{
		addExceptionInfo(param, exceptionInfo, errMsg);
		return coco;
	}

	// Find flange name
	const QStringList &flanges = pPlc->getFlanges();
	if((locationIndex.getValue() < 1) || (locationIndex.getValue() >= flanges.count()))
	{
		char buf[512];
		sprintf(buf, "Bad locationIndex %d for PLC <%s>: allowed [1...%d]", (int)locationIndex.getValue(),
			pPlc->getName(), flanges.count() - 1);
		addExceptionInfo(param, exceptionInfo, buf);
		return -5;
	}

	// And now search for equipment.... Criteria are:
	// 1) mobileType
	// 2) Flange name
	// 3) DP type
	Eqp	*pEqp = findMobileOnFlange(MobileType::OnFlange, flanges.at(locationIndex.getValue()).toLatin1(), targetDpType);
	if(!pEqp)
	{
		char buf[512];
		sprintf( buf, "Equipment with DP type %s on flange %s is not found", (const char *)targetDpType,
			flanges.at(locationIndex.getValue()).toLatin1().constData());
		addExceptionInfo(param, exceptionInfo, buf);
		return -10;
	}
	((TextVar *)targetDp)->setValue(pEqp->getDpName());
	return 0;
}

/*
**
** FUNCTION
**		Process call to LhcVacDpForMobileOnSector()
**		PVSS CTRL arguments are:
**			string sourceDp
**			string targetDpType
**			int locationIndex
**			int indexInSector
**			string &targetDp
**			dyn_string &exceptionInfo
**
** PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
** RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDpForMobileOnSector(ExecuteParamRec &param)
{
	TextVar		sourceDp, targetDpType;
	IntegerVar	locationIndex, indexInSector;
	Variable	*targetDp, *exceptionInfo;

		// Parse arguments
	SPvssArg	args[] =
	{
		{ "sourceDp",		TEXT_VAR,		&sourceDp,		NULL },
		{ "targetDpType",	TEXT_VAR,		&targetDpType,	NULL },
		{ "locationIndex",	INTEGER_VAR,	&locationIndex,	NULL },
		{ "indexInSector",	INTEGER_VAR,	&indexInSector,	NULL },
		{ "targetDp",		TEXT_VAR,		NULL,			&targetDp },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((TextVar *)targetDp)->setValue( "" );

	// Find PLC data for source DP
	PlcConfig	*pPlc;
	QString errMsg;
	int coco = findSourcePlc(sourceDp, &pPlc, errMsg);
	if( coco < 0 )
	{
		addExceptionInfo(param, exceptionInfo, errMsg);
		return coco;
	}

	// Find sector name
	const QStringList &sectors = pPlc->getSectors();
	if((locationIndex.getValue() < 1) || (locationIndex.getValue() >= sectors.count()))
	{
		char buf[512];
		sprintf(buf, "Bad locationIndex %d for PLC <%s>: allowed [1...%d]", (int)(locationIndex.getValue()),
			pPlc->getName(), sectors.count() - 1);
		addExceptionInfo(param, exceptionInfo, buf);
		return -5;
	}

	// Find sector
	Sector *pSector = DataPool::getInstance().findSectorData(sectors.at(locationIndex.getValue()).toLatin1());
	if(!pSector)
	{
		char buf[512];
		sprintf(buf, "Unknown sector <%s> in PLC <%s>", sectors.at(locationIndex.getValue()).toLatin1().constData(),
			pPlc->getName());
		addExceptionInfo(param, exceptionInfo, buf);
		return -6;
	}

	// And now search for equipment.... Criteria are:
	// 1) mobileType
	// 2) Sector
	// 3) DP type
	Eqp	*pEqp = findMobileOnSector(MobileType::OnSector, pSector, indexInSector.getValue(), targetDpType);
	if(!pEqp)
	{
		char buf[512];
		sprintf(buf, "Equipment with DP type %s on sector %s is not found", (const char *)targetDpType,
			sectors.at(locationIndex.getValue()).toLatin1().constData());
		addExceptionInfo(param, exceptionInfo, buf);
		return -10;
	}
	((TextVar *)targetDp)->setValue(pEqp->getDpName());
	return 0;
}

/*
**
** FUNCTION
**		Process call to LhcVacSetDebugMode()
**		PVSS CTRL arguments are:
**			string mode
**			bool flag
**
** PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
** RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processSetDebugMode(ExecuteParamRec &param)
{
	TextVar	mode;
	BitVar	flag;

		// Parse arguments
	SPvssArg	args[] =
	{
		{ "mode",	TEXT_VAR,	&mode,	NULL },
		{ "flag",	BIT_VAR,	&flag,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	int coco = 0;
	if(!strcasecmp(mode, "data"))
	{
		DebugCtl::setData(flag.getValue() == PVSS_TRUE);
		coco = 1;
	}
	else if(!strcasecmp(mode, "mainview"))
	{
		DebugCtl::setMainView(flag.getValue() == PVSS_TRUE);
		coco = 1;
	}
	else if(!strcasecmp(mode, "axis"))
	{
		DebugCtl::setAxis(flag.getValue() == PVSS_TRUE);
		coco = 1;
	}
	else if(!strcasecmp(mode, "history"))
	{
		DebugCtl::setHistory(flag.getValue() == PVSS_TRUE);
		coco = 1;
	}
	else if(!strcasecmp(mode, "synoptic"))
	{
		DebugCtl::setSynoptic(flag.getValue() == PVSS_TRUE);
		coco = 1;
	}
	else if(!strcasecmp(mode, "profile"))
	{
		DebugCtl::setProfile(flag.getValue() == PVSS_TRUE);
		coco = 1;
	}
	else if(!strcasecmp(mode, "sms"))
	{
		DebugCtl::setSms(flag.getValue() == PVSS_TRUE);
		coco = 1;
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to LhcVacGetRackEquipment
**		PVSS CTRL arguments are:
**			string inputString
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
/*int VacCtlEqpDataPvss::processGetRackEquipment(ExecuteParamRec &param)
{
	TextVar		address;
	Variable		*eqpNames;
	Variable		*equipmentTypes;
	Variable		*positionsY;
	Variable		*positionsX;
	Variable		*sizesY;
	Variable		*sizesX;
	// Parse arguments
	SPvssArg	args[] =
	{
		{ "inputString", TEXT_VAR, &address, NULL },
		{ "eqpNames", DYNTEXT_VAR, NULL, &eqpNames },
		{ "positionsY", DYNINTEGER_VAR, NULL, &positionsY },
		{ "positionsX", DYNINTEGER_VAR, NULL, &positionsX },
		{ "sizesY", DYNFLOAT_VAR, NULL, &sizesY },
		{ "sizesX", DYNFLOAT_VAR, NULL, &sizesX },
		{ "equipmentTypes", DYNTEXT_VAR, NULL, &equipmentTypes }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)eqpNames)->clear();
	
	((DynVar *)positionsY)->clear();
	((DynVar *)positionsX)->clear();
	((DynVar *)sizesY)->clear();
	((DynVar *)sizesX)->clear();
	((DynVar *)equipmentTypes)->clear();


	QString url = address.getString();
	QJsonDocument rackEquipJson = VacCtlWeb::httpGetJson(url);
	Rack newRack = DataPool::getRackFromJson(rackEquipJson);

	int numberOfEquip = newRack.getNumberOfEquipment();

	for (int i = 0; i < numberOfEquip; i++)
	{
		addToDynStringVar((DynVar *)eqpNames, newRack.getEqpNameAt(i).toStdString().c_str());
		addToDynIntVar((DynVar *)positionsX, newRack.getPositionX(i));
		addToDynIntVar((DynVar *)positionsY, newRack.getPositionY(i));
		addToDynFloatVar((DynVar *)sizesX, newRack.getSizeX(i));
		addToDynFloatVar((DynVar *)sizesY, newRack.getSizeY(i));
		addToDynStringVar((DynVar *)equipmentTypes, newRack.getEqpType(i).toStdString().c_str());
		
	}



	return numberOfEquip;
}*/


/*
**	FUNCTION
**		[VACCO-948] [VACCO-1645]
**		Process call to LhcVacChangeSectorState()
**		PVSS CTRL arguments are:
**			string sectorDpName
**			anytype value (new state)
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processChangeSectorState(ExecuteParamRec &param)
{
	TextVar	sectorDpName;
	AnyTypeVar	value;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "sectorDpName", TEXT_VAR,		&sectorDpName,	NULL },
		{ "value",		  ANYTYPE_VAR,	&value,			NULL },
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	char *dpName, *dpeName;
	QVariant *pVariant = anyTypeToVariant(param, value);
	if (splitDpName(sectorDpName, &dpName, &dpeName)){
		DataPool &pool = DataPool::getInstance();
		const QList<Sector *> &allSectors = pool.getSectors();
		Sector *pSector = NULL;
		for (int sectIdx = 0; sectIdx < allSectors.count(); sectIdx++){
			if (strcmp(dpName, allSectors.at(sectIdx)->getPvssName()) == 0){
				pSector = allSectors.at(sectIdx);
				pSector->stateChanged(pVariant->toUInt());
				break;
			}
		}
	}
	return 0;
}




/*
**	FUNCTION
**		[VACCO-948] [VACCO-1645]
**		Process call to LhcVacDisplayNameOfSector()
**		PVSS CTRL arguments are:
**			string sector
**			string &pvssName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::processDisplayNameOfSector(ExecuteParamRec &param)
{
	TextVar		dpName;
	Variable	*displayName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName", TEXT_VAR, &dpName, NULL },
		{ "displayName", TEXT_VAR, NULL, &displayName }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &allSectors = pool.getSectors();
	Sector *pSector = pool.findSectorByDpName(dpName);
	if (pSector)
	{
		((TextVar *)displayName)->setValue(pSector->getName());
	}
	else
	{
		((TextVar *)displayName)->setValue("");
	}

	return pSector ? 0 : -1;
}


/*
**	FUNCTION
**		Check data acquisition mode argument of call - must be one of
**		DataEnum::DataMode enumeration values
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**		mode	- Mode to be checked
**		errMsg	- Variable where error message will be written in case of error
**
**	RETURNS
**		true	- if mode is correct;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacCtlEqpDataPvss::checkMode(ExecuteParamRec &param, int mode, QString &errMsg)
{
	switch(mode)
	{
	case DataEnum::Online:
	case DataEnum::Replay:
	case DataEnum::Polling:
		return true;
	}
	char buf[64];
	sprintf(buf, "Unknown mode %d", mode);
	errMsg = buf;
	showError(param, ErrClass::PARAMETERERROR, buf);
	return false;
}

/*
**	FUNCTION
**		Deactivate all active mobile equipment
**
**	PARAMETERS
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlEqpDataPvss::clearAllActiveMobile(DataEnum::DataMode mode)
{
	DataPool &pool = DataPool::getInstance();
	const QHash<QByteArray, Eqp *> &allEqps = pool.getEqpDict();
	foreach(Eqp *pEqp, allEqps)
	{
		switch(pEqp->getMobileType())
		{
		case MobileType::OnFlange:
		case MobileType::OnSector:
			pEqp->setActive(mode, false);
			break;
		default:
			break;
		}
	}
	MobilePool &mobilePool = MobilePool::getInstance();
	mobilePool.startOnlineEqp();
	mobilePool.finishOnlineEqp();
}

/*
**
** FUNCTION
**		Find PLC configuration for PLC of mobile equipment container DP
**
** PARAMETERS
**		sourceDp	- Name of mobile container DP
**		ppPlc		- Pointer to found CPlcConfig is returned here
**		errMsg		- In case of error corresponding message will be
**						written to this variable
**
** RETURNS
**		0	- In case of success;
**		<0	- In case of error, more precisely:
**				-1 = source DP is not found
**				-2 = source DP is not mobile equipment container
**				-3 = PLC name for source DP is not found
**				-4 = Configuration for PLC is not found
**
** CAUTIONS
**		None
*/
int VacCtlEqpDataPvss::findSourcePlc(const char *sourceDp, PlcConfig **ppPlc, QString &errMsg)
{
	// Find source DP
	DataPool &pool = DataPool::getInstance();
	Eqp	*pSourceEqp = pool.findEqpByDpName(sourceDp);
	if(!pSourceEqp)
	{
		errMsg = "Source DP <";
		errMsg += sourceDp;
		errMsg += "> not found";
		return -1;
	}

	if(pSourceEqp->getMobileType() != MobileType::Container)
	{
		errMsg = "Source DP <";
		errMsg += sourceDp;
		errMsg += "> is not mobile equipment container";
		return -2;
	}

	// Find PLC name for source DP
	QString plcName = pSourceEqp->findPlcName();
	if(plcName.isEmpty())
	{
		errMsg = "PLC is not known for source DP <";
		errMsg += sourceDp;
		errMsg += ">";
		return -3;
	}
	// Find PLC config for given PLC name
	*ppPlc = PlcConfig::findPlc(plcName.toLatin1());
	if(!(*ppPlc))
	{
		errMsg = "No configuration for PLC <";
		errMsg += plcName;
		errMsg += ">";
		return -4;
	}
	return 0;
}

/*
**
** FUNCTION
**		Find mobile device of given DP type on given flange
**		This equipment MUST be located on one of beam lines
**
** PARAMETERS
**		mobileType	- Required mobile type of device
**		flangeName	- Required flange name
**		dpType		- Required DP type name
**
** RETURNS
**		Pointer to device found, or
**		NULL if required device was not found
**
** CAUTIONS
**		None
*/
Eqp *VacCtlEqpDataPvss::findMobileOnFlange(int mobileType, const char *flangeName, const char *dpType)
{
	DataPool &pool = DataPool::getInstance();
	const QList<BeamLine *> &lines = pool.getLines();
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		const QList<BeamLinePart *> &parts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			const QList<Eqp *> &eqpList = pPart->getEqpList();
			for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
			{
				Eqp *pEqp = eqpList.at(eqpIdx);
				if(pEqp->getMobileType() != mobileType )
				{
					continue;
				}
				if(!pEqp->getFlangeName())
				{
					continue;
				}
				if(strcmp(pEqp->getFlangeName(), flangeName))
				{
					continue;
				}
				if(strcmp(pEqp->getDpType(), dpType))
				{
					continue;
				}
				return pEqp;
			}
		}
	}
	return NULL;
}

/*
**
** FUNCTION
**		Find mobile device of given DP type on given sector
**		This equipment MUST be in controllers - it is not on any line
**
** PARAMETERS
**		mobileType		- Required mobile type of device
**		pSector			- Pointer to required sector
**		indexInSector	- Required index in sector
**		dpType			- Required DP type name
**
** RETURNS
**		Pointer to device found, or
**		NULL if required device was not found
**
** CAUTIONS
**		None
*/
Eqp *VacCtlEqpDataPvss::findMobileOnSector(int mobileType, Sector *pSector, int indexInSector, const char *dpType)
{
	const QList<Eqp *> &ctlList = DataPool::getInstance().getCtlList();
	for(int idx = ctlList.count() - 1 ; idx >= 0 ; idx--)
	{
		Eqp *pEqp = ctlList.at(idx);
		if(pEqp->getMobileType() != mobileType)
		{
			continue;
		}
		if(pEqp->getSectorBefore() != pSector)
		{
			continue;
		}
		if(pEqp->getIndexInSector() != indexInSector)
		{
			continue;
		}
		if(strcmp(pEqp->getDpType(), dpType))
		{
			continue;
		}
		return pEqp;
	}
	return NULL;
}
