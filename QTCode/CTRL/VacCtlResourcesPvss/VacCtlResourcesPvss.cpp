﻿/**
@defgroup apiResourcesFct List of Resources API functions
The newExternHdl function has a fixed interface and must be exported so that it can be called by WCCOA.\n
The keyword '_declspec(dllexport)' has to be set only under windows for exporting the function newExternHdl.Under Linux
this keyword is not necessary.\n
The newExternHdl function must allocate a class object and return the pointer to it.\n
Parameter is nextHdl.\n
It returns a pointer to new class instance.\n
Usual way to create C++LibAPI for WCCOA BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl).\n
- @ref VacResourcesParseFile
- @ref VacResourcesGetValue
*/

#include "VacCtlResourcesPvss.h"

#include <BitVar.hxx>
#include <AnyTypeVar.hxx>
#include <UIntegerVar.hxx>
#include <FloatVar.hxx>

// Data types for resources values to be returend to PVSS
enum
{
	TypeInt = 0,
	TypeUnsigned = 1,
	TypeFloat = 2,
	TypeString = 3,
	TypeBool = 4,
	TypeEqpTypeGroups = 5
};

//------------------------------------------------------------------------------

/*
BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl)
{
*/

  static FunctionListRec fnList[] =
  {
	//  Return-Value    function name        parameter list               true == thread-save
	//  -------------------------------------------------------------------------------------
	{
		ANYTYPE_VAR, "VacResourcesParseFile",
		"(string fileName, string machineName, dyn_string &exceptionInfo)",
		false
	},
	{
		ANYTYPE_VAR,
		"VacResourcesGetValue",
		"(string resourceName, int dataType, anytype defaultValue)",
		false
	},
    {
		ANYTYPE_VAR,
		"VacResourcesGetSpecificValue",
		"(string resourceName, string specificSuffix, int dataType, anytype defaultValue)",
		false
	}
  };

/*
  // this line counts the number of functions you want to implement.
  PVSSulong funcCount = sizeof(fnList) / sizeof(FunctionListRec);

  // now allocate the new instance
  return new VacCtlResourcesPvss(nextHdl, funcCount, fnList);
}
*/

CTRL_EXTENSION(VacCtlResourcesPvss, fnList)

//------------------------------------------------------------------------------

const Variable *VacCtlResourcesPvss::execute(ExecuteParamRec &param)
{
	enum
	{
		VacResourcesParseFile = 0,
		VacResourcesGetValue,
		VacResourcesGetSpecificValue
	};

	// TODO for the different return types you need corresponding static Variables
	static AnyTypeVar returnVar;

	param.thread->clearLastError();

	// Clean variable to return to make sure it will accept
	// result of any data type
	returnVar.setVar( NULL );
	switch(param.funcNum)
	{
    // ---------------------------------------------------------------------
    // TODO change to your function
    case VacResourcesParseFile:
		returnVar = executeParseFile(param);
		break;
	case VacResourcesGetValue:
		returnVar = executeGetValue(param);
		break;
	case VacResourcesGetSpecificValue:
		returnVar = executeGetSpecificValue(param);
		break;
	}
	return &returnVar;
}
/**
@defgroup VacResourcesParseFile VacResourcesParseFile()
@section s int VacResourcesParseFile(string fileName, string machineName, dyn_string &exceptionInfo)
@subsection ss Purpose
Read and parse resource file; store result of parsing in the memory.
@subsection arg WCCOA CTRL Arguments
\b fileName: 		[IN] File name to be read/parsed; the name shall be absolute path, not relative to WinCC OA project directory. \n
\b machineName: 	​[IN] Name of machine for which resources will be used. This name is used to allow resources from [machineName] 
					section of file to override resources with the same name from [All] section of file. See description of VacCtlResources.txt in EDMS document for more details. \n
\b exceptionInfo: 	[IN/OUT] 	​Standard exception handling variable. The errors can be: file does not exist/not accessible/invalid file format... 
@subsection ret Return
0 - in case of success; \n
-1 - in case of error, in this case error details can be found in exceptionInfo.
@subsection caut Caution
None.
@subsection ex Usage Example
// Read machine name from dedicated DP; result will be in global variable glAccelerator \n
dyn_string exceptionInfo; \n
\n
LhcVacGetMachineId(exceptionInfo); \n
if(dynlen(exceptionInfo) > 0) { \n
	// Process error, usually this error is considered to be fatal \n
}\n
// Read all resources \n
VacResourcesParseFile(PROJ_PATH + "/data/VacCtlResources.txt", glAccelerator, exceptionInfo); \n
if(dynlen(exceptionInfo) > 0) { \n
	// Process error \n
} 
*/
/**
@brief RESOURCES API Process call to VacResourcesParseFile()
*/
AnyTypeVar VacCtlResourcesPvss::executeParseFile(ExecuteParamRec &param)
{
	AnyTypeVar	returnVal;
	TextVar		fileName, machineName;
	Variable	*exceptionInfo;

	returnVal.setVar(NULL);

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "fileName",		TEXT_VAR,		&fileName,		NULL },
		{ "machineName",	TEXT_VAR,		&machineName,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof( args ) / sizeof(args[0])))
	{
		returnVal.setVar(new IntegerVar(-1));
		return returnVal;
	}
	ResourcePool &pool = ResourcePool::getInstance();
	QStringList errList;
	int coco = pool.parseFile(machineName, fileName, errList);
	returnVal.setVar(new IntegerVar(coco));
	addExceptionInfo(param, exceptionInfo, errList);
	return	returnVal;
}
/**
@defgroup VacResourcesGetValue VacResourcesGetValue()
@section s anytype VacResourcesGetValue(string resourceName, int dataType, anytype defaultValue)
@subsection ss Purpose
Get the value of resource with given name, converted to required data type. If resource with given name is not found - the default value will be returned.
@subsection arg WCCOA CTRL Arguments
\b resourceName: 	[IN] ​The name of resource whose value shall be found \n
\b dataType: 	​	[IN] Required data type of result; allowed values for this argument are declared in CTRL library vclResources.ctl. \n
\b defaultValue:	[IN] The value to be returned by this function if resource with given name is not found dataType
section of file to override resources with the same name from [All] section of file. See description of VacCtlResources.txt in EDMS document for more details. \n
\b exceptionInfo: 	[IN/OUT] 	​Standard exception handling variable. The errors can be: file does not exist/not accessible/invalid file format...
@subsection ret Return
Either the value of resource with given resourceName; or defaultValue if such resource is not found.
@subsection caut Caution
It is not checked internally if resource value (always of type string) can be converted to required dataType or not: the standard C++ function for conversion is called - and that's it. \n
In case of empty resourceName, or unknown dataType - the defaultValue is returned.
@subsection ex Usage Example
// Read machine name from dedicated DP; result will be in global variable glAccelerator \n
dyn_string exceptionInfo; \n
\n
LhcVacGetMachineId(exceptionInfo); \n
if(dynlen(exceptionInfo) > 0) { \n
// Process error, usually this error is considered to be fatal \n
}
// Read all resources \n
VacResourcesParseFile(PROJ_PATH + "/data/VacCtlResources.txt", glAccelerator, exceptionInfo); \n
if(dynlen(exceptionInfo) > 0) { \n
// Process error \n
} \n
\n
// Read the name of bit 13 in RR1 of given DP, empty string if there is no such resource \n
string dpName;  // The name of DP, taken from somewhere \n
string resourceName = dpTypeName(dpName) + ".RR1.Bit_13.ShortName"; \n
\n
string bitName = VacResourcesGetValue(resourceName, VAC_RESOURCE_STRING, ""); \n
\n
DebugN("The bit 13 of " + dpName + ".RR1 is" + (bitName = "" ? "unknown" : bitName)); \n
*/
/**
@brief RESOURCES API Process call to VacResourcesGetValue()
*/
AnyTypeVar VacCtlResourcesPvss::executeGetValue(ExecuteParamRec &param)
{
	AnyTypeVar	returnVal;
	TextVar		resourceName;
	IntegerVar	dataType;
	AnyTypeVar	defaultValue;

	returnVal.setVar(NULL);

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "resourceName",	TEXT_VAR,		&resourceName,	NULL },
		{ "dataType",		INTEGER_VAR,	&dataType,		NULL },
		{ "defaultValue",	ANYTYPE_VAR,	&defaultValue,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		returnVal.setVar(new TextVar("Arguments error"));
		return returnVal;
	}

	if(getResourceValue(resourceName, dataType.getValue(), returnVal) != ResourcePool::OK)
	{
		returnVal = defaultValue;
	}
	return	returnVal;
}

/*
**	FUNCTION
**		Process call to VacResourcesGetSpecificValue()
**		PVSS CTRL arguments are:
**			string resourceName
**			string specificSuffix
**			int dataType
**			anytype defaultValue
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
AnyTypeVar VacCtlResourcesPvss::executeGetSpecificValue(ExecuteParamRec &param)
{
	AnyTypeVar	returnVal;
	TextVar		resourceName, specificSuffix;
	IntegerVar	dataType;
	AnyTypeVar	defaultValue;

	returnVal.setVar(NULL);

		// Parse arguments
	SPvssArg	args[] =
	{
		{ "resourceName",	TEXT_VAR,		&resourceName,		NULL },
		{ "specificSuffix",	TEXT_VAR,		&specificSuffix,	NULL },
		{ "dataType",		INTEGER_VAR,	&dataType,			NULL },
		{ "defaultValue",	ANYTYPE_VAR,	&defaultValue,		NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		returnVal.setVar(new TextVar("Arguments error"));
		return returnVal;
	}

	if(strlen(resourceName) == 0)
	{
		returnVal = defaultValue;
		return returnVal;
	}
	// Try to find resource with suffix
	if(strlen(specificSuffix) > 0)
	{
		QString fullName((const char *)resourceName);
		fullName += ".";
		fullName += (const char *)specificSuffix;
		if(getResourceValue(fullName.toLatin1(), dataType.getValue(), returnVal) == ResourcePool::OK)
		{
			return returnVal;
		}
	}
	if(getResourceValue(resourceName, dataType.getValue(), returnVal) == ResourcePool::OK)
	{
		returnVal = defaultValue;
	}
	return	returnVal;
}

/*
**	FUNCTION
**		Find value of resource with given name, convert to required data type
**
**	PARAMETERS
**		resourceName	- Name of resource to find
**		dataType		- Required dataa type of result
**		result			- Variable where result will be put in case of success
**
**	RETURNS
**		One of enum values, defined in ResourcePool class
**
**	CAUTIONS
**		None
*/
int VacCtlResourcesPvss::getResourceValue(const char *resourceName, int dataType,
	AnyTypeVar &result)
{
	int coco = ResourcePool::UnsupportedType;
	ResourcePool &pool = ResourcePool::getInstance();
	switch(dataType)
	{
	case TypeInt:
		{
			int value;
			if((coco = pool.getIntValue(resourceName, value)) == ResourcePool::OK)
			{
				result.setVar(new IntegerVar(value));
			}
		}
		break;
	case TypeUnsigned:
		{
			unsigned value;
			if((coco = pool.getUintValue(resourceName, value)) == ResourcePool::OK)
			{
				result.setVar(new UIntegerVar(value));
			}
		}
		break;
	case TypeFloat:
		{
			float value;
			if((coco = pool.getFloatValue(resourceName, value)) == ResourcePool::OK)
			{
				result.setVar(new FloatVar(value));
			}
		}
		break;
	case TypeString:
		{
			QString value;
			if((coco = pool.getStringValue(resourceName, value)) == ResourcePool::OK)
			{
				TextVar *pTextVar = new TextVar;
				pTextVar->setValue(value.toLatin1());
				result.setVar(pTextVar);
			}
		}
		break;
	case TypeBool:
		{
			bool value;
			if((coco = pool.getBoolValue(resourceName, value)) == ResourcePool::OK)
			{
				result.setVar(new BitVar(value));
			}
		}
		break;
	case TypeEqpTypeGroups:
		{
			// TODO
		}
		break;
	}
	return coco;
}
