#ifndef SYNOPTICALLLHCEWO_H
#define	SYNOPTICALLLHCEWO_H

// EWO interface for MainLHC widget

#include <BaseExternWidget.hxx>

#include "SynopticAllLhcWidget.h"

class EWO_EXPORT SynopticAllLhcEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	SynopticAllLhcEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void dpMouseDown(int button, int mode, int x, int y, int extra, const char *dpName);
	void dpConnectChanged(void);
	void dpeHistoryQueryQueueChange(void);
	void dpPollingChanged(void);
	void pollingPeriodChanged(int period);
	void mobileHistoryQueryChange(void);
	void dpeHistoryDepthQueryChange(void);

private:
	SynopticAllLhcWidget	*baseWidget;

	QVariant invokeDataReady(const QString &name, QList<QVariant> &values,
		QString &error);
};


#endif	// SYNOPTICALLLHCEWO_H
