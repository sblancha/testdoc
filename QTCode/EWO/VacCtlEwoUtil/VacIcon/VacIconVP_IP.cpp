//	Implementation of VacIconVP_IP class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVP_IP.h"
#include "VacMainView.h"
#include "EqpVP_IP.h"
#include "EqpVA_RI.h"

#include <qpainter.h>

#include <QMouseEvent>

//#include <QDebug>


// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// COLOR_GP_ : Generic Purpose Color used here
// COLOR_GP_NOEQP
// COLOR_GP_INVALID
// COLOR_GP_OFF
// COLOR_GP_ON	
// COLOR_GP_ERROR
// COLOR_GP_WARNING


//	Main state bits to be analyzed
// Generic used:
// RR1_32B_GP_VALID
// RR1_32B_GP_ERROR
// RR1_32B_GP_WARNING
// RR1_32B_GP_ON
// RR1_32B_GP_OFF
#define	RR2_VPIP_OBJECTST_MASK	(0xFF00)
#define RR2_VPIP_OBJECTST_UR	(0x0100)

/////////////////////////////////////////////////////////////////////////////////
////////////////////	CONSTRUCTION/DESCTRUCTION
/////////////////////////////////////////////////////////////////////////////////
VacIconVP_IP::VacIconVP_IP(QWidget *parent, Qt::WindowFlags f) :
VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 21);
}

VacIconVP_IP::~VacIconVP_IP()
{
	disconnect();
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////	FUNCTIONS
/////////////////////////////////////////////////////////////////////////////////
/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVP_IP::getGeometry(const VacEqpTypeMask & /* mask */)
{
	//qDebug() << "height: getGeometry " << height();
	//qDebug() << "width: " << width();
	return VacIconGeometry(21, 21, 10, 26, -1); // was 27, 21, 10, 15, 0 - 0: appear on the beamline

}

VacIconContainer::Direction VacIconVP_IP::getSlaveDirection(void) const
{
	VacIconContainer::Direction result = VacIconContainer::Up;
	switch (direction)
	{
	case VacIconContainer::Up:
		result = VacIconContainer::Down;
		break;
	case VacIconContainer::Down:
		result = VacIconContainer::Up;
		break;
	case VacIconContainer::Left:
		result = VacIconContainer::Right;
		break;
	case VacIconContainer::Right:
		result = VacIconContainer::Left;
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVP_IP::setEqp(Eqp *pEqp)
{
	disconnect();
	if (pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVP_IP"), "VacIconVP_IP::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVP_IP *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVP_IP::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVP_IP::connect(void)
{
	if (connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("PR");
	dpes.append("RR1");
	dpes.append("RR2");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVP_IP::disconnect(void)
{
	bool result = false;
	if (connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVP_IP::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if (pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVP_IP::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0, rr2 = 0;
	bool blockedOff = false;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if (pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		rr2 = pEqp->getRR2(mode);
		blockedOff = pEqp->isBlockedOff(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	painter.save();
	painter.eraseRect(0, 0, width(), height());
	/*
	if(pEqp)
	{
	if(pEqp->isSelected())
	{
	drawSelection(painter);
	}
	}
	*/
	switch (direction)
	{
	case VacIconContainer::Up:	// No rotation
		painter.fillRect(10 - pipeWidth / 2, 0, pipeWidth ? pipeWidth : 1, 4, pipeColor);
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.fillRect(10 - pipeWidth / 2, 18, pipeWidth ? pipeWidth : 1, 4, pipeColor);
		break;
	case VacIconContainer::Left:	// 90 degrees clockwise
		painter.fillRect(0, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		break;
	case VacIconContainer::Right:	// 90 degrees conuterclockwise
		painter.fillRect(18, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		break;
	}

	//painter.save();

	// Prepare painter's coordinate system for drawing
	// Coordinate system (0,0) is center of widget
	painter.translate(10, 10); // width() / 2, height() / 2);

	// fillRect() produces not perfect result after rotation,
	// so draw rectangles before painter rotation
	// Draw pump image
	setPainterForImage(painter, ctlStatus, rr1, rr2, plcAlarm);

	// 1) Filled rectangle
	painter.drawRect(-9, -9, 18, 18);

	// Turn coordinate system according to direction
	switch (direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	// Coordinate system is ready - draw
	drawImage(painter, rr1, blockedOff, plcAlarm);

	// Draw selection ON TOP of image
	painter.restore();	// Original coordinate system - see save() above
	//qDebug() << "height: " << height();
	//qDebug() << "width: " << width();
	if (pEqp)
	{
		if (pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
}

void VacIconVP_IP::drawImage(QPainter &painter, unsigned /* rr1 */, bool blockedOff, bool /* plcAlarm */)
{
	/*
	// Draw vacuum pipe - short line
	painter.fillRect(-(pipeWidth / 2), -11, pipeWidth ? pipeWidth : 1, 3, pipeColor);

	// Draw pump image
	setPainterForImage(painter, rr1, plcAlarm);

	// 1) Filled rectangle
	QRectF rectF(-9, -9, 19, 19);
	// painter.fillRect(rectF, painter.brush());
	painter.drawRect(rectF);
	*/

	// 2) Two vertical lines
	painter.setPen(Qt::black);
	painter.drawLine(-7, -9, -7, 9);
	painter.drawLine(7, -9, 7, 9);

	// 3) Two slope lines emanating from connection point
	// pen.setWidth(2);
	painter.drawLine(0, -9, -7, 0);
	painter.drawLine(0, -9, 7, 0);

	// 4) blocked OFF symbol
	if (blockedOff)
	{
		painter.drawLine(-9, 7, 9, -1);
		painter.drawLine(-9, 8, 9, 0);
		painter.drawLine(-9, -1, 9, 7);
		painter.drawLine(-9, 0, 9, 8);
	}
	// 5) draw specific number in the middle according to channel number
	QString letter;
	letter = "u"; // "u" for unknown channel
	if (pEqp) {
		letter = QString::number(pEqp->getChannelNb());
	}
	painter.setPen(Qt::black);
	if (direction == VacIconContainer::Down) {	// 180 degrees: does not rotate channel value text
		painter.rotate(180);
		painter.drawText(-7, -11, 14, 19, Qt::AlignCenter, letter);
		painter.rotate(-180);// rotate back 180 degrees (does not use save/restore because maybe already used!)
	}
	else {
		painter.drawText(-7, -9, 14, 19, Qt::AlignCenter, letter);
	}
}


void VacIconVP_IP::setPainterForImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm)
{
	QColor fillColor(COLOR_GP_INVALID);
	QColor lineColor = Qt::black;
	switch (ctlStatus)
	{
	case Eqp::NotControl:
		fillColor.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		fillColor.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if (!plcAlarm)
		{
			if (rr1 & RR1_32B_GP_VALID)
			{
				if (rr1 & RR1_32B_GP_ERROR)
				{
					fillColor.setRgb(COLOR_GP_ERROR);
				}
				else if (rr1 & RR1_32B_GP_WARNING)
				{
					fillColor.setRgb(COLOR_GP_WARNING);
				}
				else if ((rr1 & RR1_32B_GP_ON) && (rr1 & RR1_32B_GP_OFF))
				{
					fillColor.setRgb(COLOR_GP_ERROR);
				}
				else if ((rr1 & RR1_32B_GP_ON) &&
					((rr2 & RR2_VPIP_OBJECTST_MASK) & RR2_VPIP_OBJECTST_UR)) // ObjectSt=1 -> under range
				{
					fillColor.setRgb(COLOR_GP_UR);
				}
				else if (rr1 & RR1_32B_GP_OFF)
				{
					fillColor.setRgb(COLOR_GP_OFF);
				}
				else if (rr1 & RR1_32B_GP_ON)
				{
					fillColor.setRgb(COLOR_GP_ON);
				}
			}
		}
		break;
	}
	painter.setBrush(fillColor);
	QPen pen(lineColor, lineColor == Qt::black ? 1 : 2);
	painter.setPen(pen);
}
