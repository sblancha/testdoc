//	Implementation of BakeoutWorkDp class
/////////////////////////////////////////////////////////////////////////////////

#include "BakeoutWorkDp.h"

#include "Eqp.h"

#include "DataPool.h"
#include "MobilePool.h"

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

BakeoutWorkDp::BakeoutWorkDp(const char *realDpName) : dpName(realDpName)
{
	pDpeConnections = new QList<DpeConnection *>();
	pSignalDestinations = new QList<InterfaceEqp *>();
	pEqp = pSourceEqp = NULL;
	rr1 = 0;
	plcAlarm = false;
	for(int n = 0 ; n < MAX_BAKEOUT_CHANNEL ; n++)
	{
		channelStates[n] = 0;
	}
	for(int n = 0 ; n < MAX_BAKEOUT_CTL_CHANNEL ; n++)
	{
		ctlChannelStates[n] = 0;
	}
}

BakeoutWorkDp::~BakeoutWorkDp()
{
	delete pSignalDestinations;
	delete pDpeConnections;
}

/*
**	FUNCTION
**		Set new pointer to equipment (bakeout rack)
**		TODO: NOT IMPLEMENTED YET
**
**	ARGUMENTS
**		pEqp		- Pointer to bakeout rack device
**		pSourceEqp	- Pointer to source (VMOB) device
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::setEqp(Eqp *pEqp, Eqp *pSourceEqp)
{
	if(pEqp != this->pEqp)	// Disconnect from all DPEs which are connected
	{
		int idx;
		for(idx = 0 ; idx < pDpeConnections->count() ; idx++)
		{
			DpeConnection *pConn = pDpeConnections->at(idx);
			if(pConn->getCount(DataEnum::Online) > 0)
			{
				DpConnection::disconnect(dpName.toLatin1(), pConn->getName());
			}
		}
		for(idx = 0 ; idx < pSignalDestinations->count() ; idx++)
		{
			InterfaceEqp *pDst = pSignalDestinations->at(idx);
			QObject::disconnect(this, SIGNAL(selectChanged(Eqp *)),
				pDst, SLOT(selectChange(Eqp *)));
			QObject::disconnect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
				pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
		}
		pDpeConnections->clear();
		pSignalDestinations->clear();
	}
	this->pEqp = pEqp;
	this->pSourceEqp = pSourceEqp;
}

/*
**	FUNCTION
**		Connect receiver to given DPE
**
**	ARGUMENTS
**		pDst	- Pointer to receiver
**		channel	- channel number
**		setT	- Flag indicating if temperature setting DPE is required
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::connectDpeSignal(InterfaceEqp *pDst, int channel, enum ChannelValueType valueType)
{
	if(!pEqp)
	{
		return;
	}

	QString dpeName (MobilePool::findBakeoutChannelDpeName(channel, valueType));

	// Is this new connection?
	DpeConnection *pConnect = NULL;

	// Check also if this the very first connection of this device, if yes -
	// connection to PLC will also be done if this device is controlled by PLC
	bool needPlcConnect = true;
	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pConn = pDpeConnections->at(idx);
		if(!strcmp(pConn->getName(), dpeName.toLatin1().constData()))
		{
			pConnect = pConn;
		}
		if(pConn->getCount(DataEnum::Online) > 0)
		{
			needPlcConnect = false;
		}
	}
	if(!pConnect)
	{
		pConnect = new DpeConnection(dpeName.toLatin1().constData());
		pDpeConnections->append(pConnect);
	}

	// Connect to PLC if required
	if(needPlcConnect)
	{
		// VMOB has only reference to 1 PLC
		if(pSourceEqp)
		{
			const QString plcName = pEqp->findPlcName();
			if(!plcName.isEmpty())
			{
				Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
				if(pPlc)
				{
					pPlc->connect(this, DataEnum::Online);
				}
			}
		}
	}

	// Is connection to DPE needed?
	if(!pConnect->getCount(DataEnum::Online))
	{
		DpConnection::connect(dpName.toLatin1().constData(), dpeName.toLatin1().constData());
	}
	pConnect->plusCount(DataEnum::Online);

	// If necessary - connect destination object to signals of this object
	if(!pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->append(pDst);
		QObject::connect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
		QObject::connect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
	}
}

/*
**	FUNCTION
**		Disconnect receiver from given DPE
**
**	ARGUMENTS
**		pDst	- Pointer to receiver
**		channel	- channel number
**		setT	- Flag indicating if temperature setting DPE is required
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::disconnectDpeSignal(InterfaceEqp *pDst, int channel, enum ChannelValueType valueType)
{
	if(!pEqp)
	{
		return;
	}
	QString dpeName(MobilePool::findBakeoutChannelDpeName(channel, valueType));

	// Find connection
	DpeConnection *pConnect = NULL;

	// Check also if disconnection from PLC will be needed - if this is the very last
	// DPE to disconnect on this device
	bool needPlcDisconnect = true;
	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pConn = pDpeConnections->at(idx);
		if(!strcmp(pConn->getName(), dpeName.toLatin1().constData()))
		{
			pConnect = pConn;
			if(pConn->getCount(DataEnum::Online) > 1)	// Compare to 1 because we'll decrement counter
			{
				needPlcDisconnect = false;
			}
		}
		else
		{
			if(pConn->getCount(DataEnum::Online) > 0)
			{
				needPlcDisconnect = false;
			}
		}
	}
	if(!pConnect)
	{
		return;
	}

	// Disconnect from PLC if required
	if(needPlcDisconnect)
	{
		// VMOB has only reference to 1 PLC
		if(pSourceEqp)
		{
			const QString plcName = pSourceEqp->findPlcName();
			if(!plcName.isEmpty())
			{
				Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
				if(pPlc)
				{
					pPlc->disconnect(this, DataEnum::Online);
				}
			}
		}
	}

	// Is disconnection needed?
	pConnect->minusCount(DataEnum::Online);
	if(!pConnect->getCount(DataEnum::Online))
	{
		DpConnection::disconnect(dpName.toLatin1().constData(), dpeName.toLatin1().constData());
	}

	// If necessary - disconnect destination object from signals of this object
	// Note that destination object will be disconnected from signals on FIRST
	// call to this method
	if(pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->removeAll(pDst);
		QObject::disconnect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
		QObject::disconnect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
	}
}

/*
**	FUNCTION
**		Connect receiver to all DPEs of given channel
**
**	ARGUMENTS
**		pDst	- Pointer to receiver
**		channel	- channel number
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::connectChannel(InterfaceEqp *pDst, int channel)
{
	if(!pEqp)
	{
		return;
	}

	char	dpeName[256];
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CH%02d.T", channel);
#else
	sprintf(dpeName, "CH%02d.T", channel);
#endif
	connectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CH%02d.SET", channel);
#else
	sprintf(dpeName, "CH%02d.SET", channel);
#endif
	connectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CH%02d.STATE", channel);
#else
	sprintf(dpeName, "CH%02d.STATE", channel);
#endif
	connectDpe(dpeName);

	// If necessary - connect destination object to signals of this object
	if(!pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->append(pDst);
		QObject::connect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
		QObject::connect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
	}
}

void BakeoutWorkDp::connectCtlChannel(InterfaceEqp *pDst, int channel)
{
	if(!pEqp)
	{
		return;
	}

	char	dpeName[256];
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.T", channel);
#else
	sprintf(dpeName, "CtlCH%02d.T", channel);
#endif
	connectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.Status", channel);
#else
	sprintf(dpeName, "CtlCH%02d.Status", channel);
#endif
	connectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.IntlRegChannel", channel);
#else
	sprintf(dpeName, "CtlCH%02d.IntlRegChannel", channel);
#endif
	connectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.WarningThreshold", channel);
#else
	sprintf(dpeName, "CtlCH%02d.WarningThreshold", channel);
#endif
	connectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.ErrorThreshold", channel);
#else
	sprintf(dpeName, "CtlCH%02d.ErrorThreshold", channel);
#endif
	connectDpe(dpeName);

	// If necessary - connect destination object to signals of this object
	if(!pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->append(pDst);
		QObject::connect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
		QObject::connect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
	}
}

/*
**	FUNCTION
**		Connect receiver to state DPE of given channel
**
**	ARGUMENTS
**		pDst	- Pointer to receiver
**		channel	- channel number
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::connectChannelState(InterfaceEqp *pDst, int channel)
{
	if(!pEqp)
	{
		return;
	}

	char	dpeName[256];
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CH%02d.STATE", channel);
#else
	sprintf(dpeName, "CH%02d.STATE", channel);
#endif
	connectDpe(dpeName);

	// If necessary - connect destination object to signals of this object
	if(!pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->append(pDst);
		QObject::connect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
		QObject::connect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
	}
}

/*
**	FUNCTION
**		Connect receiver to state DPE of given control channel
**
**	ARGUMENTS
**		pDst	- Pointer to receiver
**		channel	- channel number
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::connectCtlChannelState(InterfaceEqp *pDst, int channel)
{
	if(!pEqp)
	{
		return;
	}

	char	dpeName[256];
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.Status", channel);
#else
	sprintf(dpeName, "CtlCH%02d.Status", channel);
#endif
	connectDpe(dpeName);

	// If necessary - connect destination object to signals of this object
	if(!pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->append(pDst);
		QObject::connect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
		QObject::connect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
	}
}

/*
**	FUNCTION
**		Disconnect receiver from all DPEs of given channel
**
**	ARGUMENTS
**		pDst	- Pointer to receiver
**		channel	- channel number
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::disconnectChannel(InterfaceEqp *pDst, int channel)
{
	if(!pEqp)
	{
		return;
	}

	char	dpeName[256];
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.T", channel);
#else
	sprintf(dpeName, "CtlCH%02d.T", channel);
#endif
	disconnectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.Status", channel);
#else
	sprintf(dpeName, "CtlCH%02d.Status", channel);
#endif
	disconnectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.IntlRegChannel", channel);
#else
	sprintf(dpeName, "CtlCH%02d.IntlRegChannel", channel);
#endif
	disconnectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.WarningThreshold", channel);
#else
	sprintf(dpeName, "CtlCH%02d.WarningThreshold", channel);
#endif
	disconnectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.ErrorThreshold", channel);
#else
	sprintf(dpeName, "CtlCH%02d.ErrorThreshold", channel);
#endif
	disconnectDpe(dpeName);

	// If necessary - disconnect destination object from signals of this object
	// Note that destination object will be disconnected from signals on FIRST
	// call to this method
	if(pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->removeAll(pDst);
		QObject::disconnect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
		QObject::disconnect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
	}
}

void BakeoutWorkDp::disconnectCtlChannel(InterfaceEqp *pDst, int channel)
{
	if(!pEqp)
	{
		return;
	}

	char	dpeName[256];
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CH%02d.T", channel);
#else
	sprintf(dpeName, "CH%02d.T", channel);
#endif
	disconnectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CH%02d.SET", channel);
#else
	sprintf(dpeName, "CH%02d.SET", channel);
#endif
	disconnectDpe(dpeName);
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CH%02d.STATE", channel);
#else
	sprintf(dpeName, "CH%02d.STATE", channel);
#endif
	disconnectDpe(dpeName);

	// If necessary - disconnect destination object from signals of this object
	// Note that destination object will be disconnected from signals on FIRST
	// call to this method
	if(pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->removeAll(pDst);
		QObject::disconnect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
		QObject::disconnect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
	}
}

/*
**	FUNCTION
**		Disconnect receiver from state DPE of given channel
**
**	ARGUMENTS
**		pDst	- Pointer to receiver
**		channel	- channel number
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::disconnectChannelState(InterfaceEqp *pDst, int channel)
{
	if(!pEqp)
	{
		return;
	}

	char	dpeName[256];
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CH%02d.STATE", channel);
#else
	sprintf(dpeName, "CH%02d.STATE", channel);
#endif
	disconnectDpe(dpeName);

	// If necessary - disconnect destination object from signals of this object
	// Note that destination object will be disconnected from signals on FIRST
	// call to this method
	if(pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->removeAll(pDst);
		QObject::disconnect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
		QObject::disconnect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
	}
}

/*
**	FUNCTION
**		Disconnect receiver from state DPE of given channel
**
**	ARGUMENTS
**		pDst	- Pointer to receiver
**		channel	- channel number
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::disconnectCtlChannelState(InterfaceEqp *pDst, int channel)
{
	if(!pEqp)
	{
		return;
	}

	char	dpeName[256];
#ifdef Q_OS_WIN
	sprintf_s(dpeName, sizeof(dpeName) / sizeof(dpeName[0]), "CtlCH%02d.Status", channel);
#else
	sprintf(dpeName, "CtlCH%02d.Status", channel);
#endif
	disconnectDpe(dpeName);

	// If necessary - disconnect destination object from signals of this object
	// Note that destination object will be disconnected from signals on FIRST
	// call to this method
	if(pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->removeAll(pDst);
		QObject::disconnect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
		QObject::disconnect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
	}
}

/*
**	FUNCTION
**		Connect receiver to RR1 DPE of bakeout rack
**
**	ARGUMENTS
**		pDst	- Pointer to receiver
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::connectRr1(InterfaceEqp *pDst)
{
	if(!pEqp)
	{
		return;
	}

	const char	*dpeName = "RR1";

	// Is this new connection?
	DpeConnection *pConnect = NULL;

	// Check also if this the very first connection of this device, if yes -
	// connection to PLC will also be done if this device is controlled by PLC
	bool needPlcConnect = true;
	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pConn = pDpeConnections->at(idx);
		if(!strcmp(pConn->getName(), dpeName))
		{
			pConnect = pConn;
		}
		if(pConn->getCount(DataEnum::Online) > 0)
		{
			needPlcConnect = false;
		}
	}
	if(!pConnect)
	{
		pConnect = new DpeConnection(dpeName);
		pDpeConnections->append(pConnect);
	}

	// Connect to PLC if required
	if(needPlcConnect)
	{
		// VMOB has only reference to 1 PLC
		if(pSourceEqp)
		{
			const QString plcName = pEqp->findPlcName();
			if(!plcName.isEmpty())
			{
				Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
				if(pPlc)
				{
					pPlc->connect(this, DataEnum::Online);
				}
			}
		}
	}

	// Is connection to DPE needed?
	if(!pConnect->getCount(DataEnum::Online))
	{
		DpConnection::connect(dpName.toLatin1(), dpeName);
	}
	pConnect->plusCount(DataEnum::Online);

	// If necessary - connect destination object to signals of this object
	if(!pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->append(pDst);
		QObject::connect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
		QObject::connect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
	}
}

/*
**	FUNCTION
**		Disconnect receiver from RR1 DPE
**
**	ARGUMENTS
**		pDst	- Pointer to receiver
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::disconnectRr1(InterfaceEqp *pDst)
{
	if(!pEqp)
	{
		return;
	}

	const char	*dpeName = "RR1";

	// Find connection
	DpeConnection *pConnect = NULL;

	// Check also if disconnection from PLC will be needed - if this is the very last
	// DPE to disconnect on this device
	bool needPlcDisconnect = true;
	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pConn = pDpeConnections->at(idx);
		if(!strcmp(pConn->getName(), dpeName))
		{
			pConnect = pConn;
			if(pConn->getCount(DataEnum::Online) > 1)	// Compare to 1 because we'll decrement counter
			{
				needPlcDisconnect = false;
			}
		}
		else
		{
			if(pConn->getCount(DataEnum::Online) > 0)
			{
				needPlcDisconnect = false;
			}
		}
	}
	if(!pConnect)
	{
		return;
	}

	// Disconnect from PLC if required
	if(needPlcDisconnect)
	{
		// VMOB has only reference to 1 PLC
		if(pSourceEqp)
		{
			const QString plcName = pSourceEqp->findPlcName();
			if(!plcName.isEmpty())
			{
				Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
				if(pPlc)
				{
					pPlc->disconnect(this, DataEnum::Online);
				}
			}
		}
	}

	// Is disconnection needed?
	pConnect->minusCount(DataEnum::Online);
	if(!pConnect->getCount(DataEnum::Online))
	{
		DpConnection::disconnect(dpName.toLatin1(), dpeName);
	}

	// If necessary - disconnect destination object from signals of this object
	// Note that destination object will be disconnected from signals on FIRST
	// call to this method
	if(pSignalDestinations->contains(pDst))
	{
		pSignalDestinations->removeAll(pDst);
		QObject::disconnect(this, SIGNAL(selectChanged(Eqp *)),
			pDst, SLOT(selectChange(Eqp *)));
		QObject::disconnect(this, SIGNAL(dpeChanged(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)),
			pDst, SLOT(dpeChange(Eqp *, const char *, DataEnum::Source, const QVariant &, DataEnum::DataMode, const QDateTime &)));
	}
}


/*
**	FUNCTION
**		Connect receiver to DPE with given name
**
**	ARGUMENTS
**		dpeName	- DPE name
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::connectDpe(const char *dpeName)
{
	// Is this new connection?
	DpeConnection *pConnect = NULL;

	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pConn = pDpeConnections->at(idx);
		if(!strcmp(pConn->getName(), dpeName))
		{
			pConnect = pConn;
			break;
		}
	}
	if(!pConnect)
	{
		pConnect = new DpeConnection(dpeName);
		pDpeConnections->append(pConnect);
	}

	// Is connection to DPE needed?
	if(!pConnect->getCount(DataEnum::Online))
	{
		DpConnection::connect(dpName.toLatin1(), dpeName);
	}
	pConnect->plusCount(DataEnum::Online);
}

/*
**	FUNCTION
**		Disconnect receiver from DPE with given name
**
**	ARGUMENTS
**		dpeName	- DPE name
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutWorkDp::disconnectDpe(const char *dpeName)
{
	// Find connection
	DpeConnection *pConnect = NULL;

	// Check also if disconnection from PLC will be needed - if this is the very last
	// DPE to disconnect on this device
	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pConn = pDpeConnections->at(idx);
		if(!strcmp(pConn->getName(), dpeName))
		{
			pConnect = pConn;
			break;
		}
	}
	if(!pConnect)
	{
		return;
	}

	// Is disconnection needed?
	pConnect->minusCount(DataEnum::Online);
	if(!pConnect->getCount(DataEnum::Online))
	{
		DpConnection::disconnect(dpName.toLatin1(), dpeName);
	}
}

/*
**
**	FUNCTION
**		Return last known value for given DPE
**
**	PARAMETERS
**		channel	- channel number
**		setT	- Flag indicating if temperature setting DPE is required
**
**	RETURNS
**		Last known DPE value, or
**		dummy value if no last known
**
** CAUTIONS
**		None
*/
const QVariant &BakeoutWorkDp::getDpeValue(int channel, enum ChannelValueType valueType)
{
	QString dpeName(MobilePool::findBakeoutChannelDpeName(channel, valueType));
	QDateTime dummyTs;
	return getDpeValue(dpeName.toLatin1().constData(), dummyTs);
}

/*
**
**	FUNCTION
**		Return last known value for given DPE
**
**	PARAMETERS
**		channel	- channel number
**		setT	- Flag indicating if temperature setting DPE is required
**
**	RETURNS
**		Last known DPE value, or
**		dummy value if no last known
**
** CAUTIONS
**		None
*/
const QVariant &BakeoutWorkDp::getDpeValue(const char *dpeName, QDateTime &timeStamp)
{
	static QVariant dummy;

	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pDpe = pDpeConnections->at(idx);
		if(!strcmp(pDpe->getName(), dpeName))
		{
			return pDpe->getValue(DataEnum::Online, timeStamp);
		}
	}
	return dummy;
}

// Implementation for newOnlineValue() methods - just send signal
void BakeoutWorkDp::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	// First store value for future use...
	for(int idx = 0 ; idx < pDpeConnections->count() ; idx++)
	{
		DpeConnection *pDpe = pDpeConnections->at(idx);
		if(!strcmp(pDpe->getName(), dpe))
		{
			pDpe->setValue(DataEnum::Online, value, timeStamp);
		}
	}

	if(!strcmp(dpe, "RR1"))
	{
		rr1 = value.toUInt();
	}
	else
	{
		int channel = 0;
		if(sscanf(dpe, "CH%02d.STATE", &channel) == 1)
		{
			if((1 <= channel) && (channel <= MAX_BAKEOUT_CHANNEL))
			{
				channelStates[channel - 1] = value.toInt();
			}
		}
		else if(sscanf(dpe, "CtlCH%02d.Status", &channel) == 1)
		{
			if((1 <= channel) && (channel <= MAX_BAKEOUT_CTL_CHANNEL))
			{
				ctlChannelStates[channel - 1] = value.toInt();
			}
		}
	}

	// ... then emit signal
	emit dpeChanged(pEqp, dpe, DataEnum::Own, value, DataEnum::Online, timeStamp);
}

int BakeoutWorkDp::getChannelState(int channel)
{
	if((1 <= channel) && (channel <= MAX_BAKEOUT_CHANNEL))
	{
		return channelStates[channel - 1];
	}
	return 0;
}
int BakeoutWorkDp::getCtlChannelState(int channel)
{
	if((1 <= channel) && (channel <= MAX_BAKEOUT_CTL_CHANNEL))
	{
		return ctlChannelStates[channel - 1];
	}
	return 0;
}

/*
**	FUNCTION
**		Slot activated when PLC state is changed - we only connect to PLC
**
**	PARAMETERS
**		pSrc	- Pointer to object - source of signal
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void BakeoutWorkDp::dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source /* source */, const QVariant &value,
		DataEnum::DataMode /* mode */, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(DataEnum::Online);
	if(alarm != plcAlarm)
	{
		plcAlarm = alarm;
		emit dpeChanged(pEqp, dpeName, DataEnum::Plc, value, DataEnum::Online, timeStamp);
	}
}
