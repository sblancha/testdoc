#ifndef	VACICONVPC_HCCC_H
#define	VACICONVPC_HCCC_H

//	Icon for VPC_HCCC - HRS HCC Controller, Cryo Controller used in NA62


#include "VacIcon.h"

class EqpVPC_HCCC;

class VACCTLEWOUTIL_EXPORT VacIconVPC_HCCC: public VacIcon  // Sub Class of superclass VacIcon
{
	Q_OBJECT

public:
	// CONSTRUCTOR  (parent argument mandatory?)
	VacIconVPC_HCCC(QWidget *parent, Qt::WindowFlags f = 0);
	// DESCTRUCTOR
	virtual ~VacIconVPC_HCCC();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

	// Override method of VacIcon 
//	virtual void setDirection(VacIconContainer::Direction newDirection);

protected:
	// Pointer to my device
	EqpVPC_HCCC *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	void drawVertical(QPainter &painter);
	//void drawHorizontal(QPainter &painter);
	//void drawSymbols(QPainter &painter, bool vertical);//no additional symbol like 'ON', 'OFF', 'M'
	//void drawForced(QPainter &painter, bool vertical);
	//void drawManual(QPainter &painter, bool vertical);
	//void drawFullInterlock(QPainter &painter, bool vertical);
	//void drawStartInterlock(QPainter &painter, bool vertical);
	//void drawCmdOff(QPainter &painter, bool vertical);
	//void drawCmdOn(QPainter &painter, bool vertical);

	//void displayFlash(const QPolygon &points);
	//void displayBlockedOFF(bool blockedOff, bool isVertical);

	void setPainterForControl(QPainter &painter);
	void setPainterForMainImage(QPainter &painter);
		
	// Override VacIcon's reaction on mouse press
	//virtual void mousePressEvent(QMouseEvent *pEvent);
};

#endif	// VACICONVPC_HCCC_H
