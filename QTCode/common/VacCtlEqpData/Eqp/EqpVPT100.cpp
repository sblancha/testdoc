//	Implementation of EqpVPT100 class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVPT100.h"

#include "DataPool.h"
#include "FunctionalType.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0
#define COLOR_WARNING			255,255,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_ON			(0x00020000)
#define	RR1_OFF			(0x00010000)
#define RR1_AL3			(0x00008000)
#define RR1_AL2			(0x00004000)
#define RR1_AL1			(0x00002000)
#define RR1_ERROR_CODE_MASK	(0x000000FF)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVPT100::EqpVPT100(const Eqp &source) : Eqp(source)
{
	rr1Online = rr1Replay = 0;
	tOnline = tReplay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::VPT100;
}

EqpVPT100::~EqpVPT100()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPT100::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	addDpeNameToList(stateDpes, "RR1");
}

/*
**	FUNCTION
**		Return list of all DPEs, related to analog value of device. The list is used
**		to build value history
**
**	ARGUMENTS
**		valueDpes	- Variable where all value DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPT100::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	addDpeNameToList(valueDpes, "T");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPT100::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "T", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPT100::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "T", mode);
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "EqpStatus", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPT100::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "T"))
	{
		if(value.canConvert(QVariant::Int))
		{
			int newValue = value.toInt();
			if(tOnline == newValue)
			{
				return;
			}
			tOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPT100::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "T"))
	{
		if(value.canConvert(QVariant::Int))
		{
			int newValue = value.toInt();
			if(tReplay == newValue)
			{
				return;
			}
			tReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPT100::dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPT100::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, tReplay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, tOnline, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Return main value for pressure transmitter - pressure
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVPT100::getMainValue(DataEnum::DataMode mode)
{
	return mode == DataEnum::Replay ? (float)tReplay : (float)tOnline;
}
#endif

/*
**	FUNCTION
**		Check if main value for gauge (pressure) is valid or not.
**		Latest known requirement: even invalid value shall be shown,
**		so the value is always considered to be valid
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
bool EqpVPT100::isMainValueValid(DataEnum::DataMode /* mode */)
{
	return true;
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1		- Value of RR1 DPE
**		t			- Value of T DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPT100::getMainColor(unsigned rr1, int /* t */, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr1 & RR1_ERROR)
	{
		color.setRgb(COLOR_ERROR);
	}
	else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if(rr1 & RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else if(rr1 & RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPT100::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, tReplay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, tOnline, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPT100::getMainValueString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainValueString(rr1Replay, tReplay, plcAlarmReplay, string);
	}
	else
	{
		getMainValueString(rr1Online, tOnline, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		t			- Value of T DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPT100::getMainValueString(unsigned /* rr1 */, int t, bool /* plcAlarm */, QString &string)
{
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d", t);
#else
	sprintf(buf, "%d", t);
#endif
	string = buf;

	/* Always display value - see above L.Kopylov 13.10.2010
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(rr1 & RR1_VALID)
	{
		if(rr1 & RR1_ON)
		{
			if(!(rr1 & RR1_OFF))
			{
				char buf[32];
				sprintf(buf, "%d", t);
				string = buf;
			}
			else
			{
				string = "ERROR";
			}
		}
		else if(rr1 & RR1_OFF)
		{
			string = "OFF";
		}
		else
		{
			string = "Undefined";
		}		
	}
	else
	{
		string = "Not valid";
	}
	*/
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		t			- Value of T DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPT100::getMainStateString(unsigned rr1, int /* pr */, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr1 & RR1_ON)
	{
		if(rr1 & RR1_OFF)
		{
			string = "Error";
		}
		else
		{
			string = "ON";
		}
	}
	else if(rr1 & RR1_OFF)
	{
		string = "OFF";
	}
	else
	{
		string = "Undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPT100::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainValueString(valueString, mode);
	string += valueString;
}
#endif


/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPT100::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("RR1");
	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, 0, false, state);
	getMainColor(rr1, 0, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpVPT100::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	return rr1 & RR1_ERROR_CODE_MASK;
}





/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPT100::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "EqpStatus");
	addDpeNameToList(dpes, "RR1");
	addDpeNameToList(dpes, "T");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history)
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVPT100::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	if(!(rr1Online & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	unsigned rr1 = DataEnum::Replay ? rr1Replay : rr1Online;
	if((rr1 & (RR1_ON | RR1_OFF)) == RR1_ON)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if((rr1 & (RR1_ON | RR1_OFF)) == RR1_OFF)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if(rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if(criteria.isWarnings())
	{
		if(rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}
	if(criteria.isAlerts())
	{
		if(rr1 & (RR1_AL1 | RR1_AL2 | RR1_AL3))
		{
			return true;
		}
	}

	// Check pressure if needed
	if(criteria.isUseTemperatureLimit())
	{
		float t = mode == DataEnum::Replay ? tOnline : tReplay;
		if(t > criteria.getTemperatureLimit())
		{
			return 1;
		}
	}

	// Last chance - criteria on pressure grow
	if(criteria.isUseTemperatureGrowLimit())
	{
		// Can not decide right now, more info is needed
		return 2;
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Return parameters for history query, required for device list
**
**	ARGUMENTS
**		dpe		- [out] DPE, for which history is required
**		start	- [out] start time of history query
**		end		- [out] end time of history query
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPT100::getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUseTemperatureGrowLimit())
	{
		dpe = "";
		return;
	}
	dpe = dpName;
	dpe += ".PR";
	start = criteria.getTemperatureStartTime();
	if(criteria.isTemperatureEndTimeNow())
	{
		end = QDateTime::currentDateTime();
	}
	else
	{
		end = criteria.getTemperatureEndTime();
	}
}
#endif

/*
**	FUNCTION
**		Return grow factor for main value (pressure) to check if
**		device matches device list criteria
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Pressure grow factor; or
**		0 if criteria does not contain pressure grow
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVPT100::getGrowFactorForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUseTemperatureGrowLimit())
	{
		return 0.0;
	}
	return criteria.getTemperatureGrowLimit();
}

int EqpVPT100::getSpikeFilterForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUseTemperatureGrowLimit())
	{
		return 0;
	}
	return criteria.getTemperatureSpikeInterval();
}

#endif

