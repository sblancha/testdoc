//	Implementation of ProfileAllSPS class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileAllSPS.h"

#include "VacMainView.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

ProfileAllSPS::ProfileAllSPS() :
	QDialog(NULL /* VacMainView::getInstance() */, Qt::Window)
{
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle("Pressure Profile - all of SPS");

	pWidget = new ProfileAllSPSWidget(this);
	pWidget->dataReady();
	setMinimumSize(pWidget->minimumSize());
}

ProfileAllSPS::~ProfileAllSPS()
{
}


void ProfileAllSPS::resizeEvent(QResizeEvent *e)
{
	QDialog::resizeEvent(e);
	pWidget->resize(size());
}
