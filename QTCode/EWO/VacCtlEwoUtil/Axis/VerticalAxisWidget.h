#ifndef	VERTICALAXISWIDGET_H
#define	VERTICALAXISWIDGET_H

//	Vertical axis for different graphs. This class is real QWidget.

// IMPORTANT:
// It is assumed that axis widget(s) and plot area are aligned in parent
// widget in such a way that height of axis widget equals to height of
// plot area widget.

#include "DataEnum.h"

#include <QWidget>
#include <QPainter>

#include <stdio.h>

#include <QToolButton>

class VerticalAxisWidget : public QWidget
{
	Q_OBJECT

public:
	VerticalAxisWidget(DataEnum::ValueType valueType, QWidget *parent = 0, Qt::WindowFlags f = 0);
	virtual ~VerticalAxisWidget();

	bool prepare(void);
	void drawOnPlot(QPainter &painter, const QRect &plotArea);

	void getLimits(float &useMin, float &useMax);
	void setLimits(float newMin, float newMax);
	void setInitialLimits(float newMin, float newMax, int maxValues);
	void setZooming(const QRect &zoomRect);

	// Value returned by valueToScreen method
	typedef enum
	{
		BelowAxis = -1,
		WithinRange = 0,
		AboveAxis = 1
	} ValueRange;

	// Convert value to screen coordinate
	ValueRange valueToScreen(float value, int &screenY);
	ValueRange logValueToScreen(float value, int &screenY);

	// Convert screen coordinate to value
	double screenToValue(int screenY);

	// print value according to default format for this axis
	void formatValue(char *buf, float value, size_t bufLen);

	// Access
	virtual inline DataEnum::ValueType getValueType(void) const { return valueType; }
	virtual const char *getUnit(void) const;
	virtual float getValueCoeff(void);

	virtual inline bool isActive(void) const { return active; }
	virtual inline void setActive(bool flag) { active = flag; update(); }

	virtual inline bool isShowUnit(void) const { return showUnit; }
	virtual void setShowUnit(bool flag);

	virtual inline bool isGrid(void) const { return grid; }
	virtual inline void setGrid(bool flag) { grid = flag; }

	virtual inline int getGridStyle(void) const { return gridStyle; }
	virtual inline void setGridStyle(int style) { gridStyle = style; }

	virtual inline int getTickSize(void) const { return tickSize; }
	virtual inline void setTickSize(int size) { tickSize = size; }

	virtual inline bool isLogScale(void) const { return logScale; }
	virtual void setLogScale(bool flag);

	virtual inline bool isInitialLimitsSet(void) const { return initialLimitsSet; }
	virtual inline void setInitialLimitsSet(bool flag) { initialLimitsSet = flag; }

	virtual inline bool isTemporaryLinear(void) { return temporaryLinear; }

	virtual inline bool isLeftOfPlot(void) const { return leftOfPlot; }
	virtual void setLeftOfPlot(bool flag);

	virtual bool eventFilter(QObject *pObject, QEvent *pEvent);

	void dump(FILE *pFile);

signals:
	void limitsChanged(bool scrolling);

protected:
	// Type of value controllable by this axis. Pressure is always on the left hand
	// side of plot, all others - on the right hand side
	DataEnum::ValueType	valueType;

	// Scale factor label to be shown for linear axis
	QString		scaleLabel;

	QToolButton	*pUpButton;
	QToolButton	*pDownButton;

	// Grid stype
	int			gridStyle;

	// Ticks size
	int			tickSize;

	// Axis minimum - set from outside
	float		min;

	// Axis maximum - set from outside
	float		max;

	// Miltiplier for linear scale
	float		linScaleMulti;

	// Log10 of multiplier for linear scale
	float		linScaleLog;

	// Step between two labeled ticks in logarithmic scale, normally it is 1, but
	// can be more if there are too many ticks in scale range
	int			drawEvery;

	// Minimum of scale, can differ from min
	float		scaleMin;

	// Maximum of linear scale, can differ from max
	float		scaleMax;

	// Value of the very first labeled tick
	float		labelsStart;

	// Step between two labeled ticks
	float		labelsStep;

	// log10 of scaleMin in logarithmic scale
	float		logScaleMin;

	// log10 of scaleMax in logarithmic scale
	float		logScaleMax;

	// Coefficient for values to be shown, normally = 1
	float		valueCoeff;

	// Format used to print labels for linear axis
	char		linLabelFormat[32];

	// Flag indicating if this axis is active - used to distinguish one of axies
	// if several axies are used for single plot
	bool		active;

	// Flag indicating scale type: logarihtmic (true) or linear (false)
	bool		logScale;

	// Flag indicating if unit text shall be shown
	bool		showUnit;

	// Flag indicating if initial scale limits have been set based on 'reasonable'
	// amount of first values shown
	bool		initialLimitsSet;

	// Flag indicating if scale at least once was in logarithmic mode and then
	// was switched to linear mode, i.e. that logScaleMin and logScaleMax were
	// calculated/used
	bool		temporaryLinear;

	// Flag indicating if axis parameters shall be recalculated
	bool		needPrepare;

	// Flag indicating if grid shall be used
	bool		grid;

	// Flag indicating if control is opened for editing limits maximum
	bool		editMax;

	// L.Kopylov 29.11.2011 Flag indicating if axis appears on left side of plot
	bool		leftOfPlot;

	// L.Kopylov 15.06.2012 Flag indicating of SHIFT button was pressed while
	// one of UP/DOWN buttons was clicked
	bool		shiftClick;

	// Flag indicating if valueCoeff have been read from resources
	bool		gotValueCoeff;

	void prepareLogScale(void);
	void prepareLinScale(void);
	void prepareLinScaleCommon(float useMin, float useMax);
	void prepareLinScaleInteger(float useMin, float useMax);

	void drawLogScale(QPainter &painter);
	void drawOnPlotLogScale(QPainter &painter, const QRect &plotArea);
	void drawLinScale(QPainter &painter);
	void drawOnPlotLinScale(QPainter &painter, const QRect &plotArea);

	void makeLogLabel(int value, char *label, size_t bufLen);
	void makeLinLabel(float value, char *label, size_t bufLen);
	void calculateMinWidth(int maxLabelWidth);

	ValueRange valueToScreenLin(float value, int &screenY);
	ValueRange valueToScreenLog(float value, int &screenY);

	const char *getResourceSuffix(void) const;
	void setDefaultLimits(void);
	void setDefaultInitialLimits(void);
	float getAbsMin(void);
	float getAbsMax(void);

	int getLimitButtonPosX(void);

	void logMinDown(void);
	void logMinUp(void);
	void logMaxDown(void);
	void logMaxUp(void);
	void linMinDown(void);
	void linMinUp(void);
	void linMaxDown(void);
	void linMaxUp(void);
	float getLinStep(float useMin, float useMax, bool decrease);

	// Reimplemented from QWidget
	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void resizeEvent(QResizeEvent *pEvent);
	virtual void mouseMoveEvent(QMouseEvent *pEvent);
	virtual void mousePressEvent(QMouseEvent *pEvent);
	virtual void wheelEvent(QWheelEvent *pEvent);
	virtual void keyPressEvent(QKeyEvent *pEvent);
	/*
	virtual void focusInEvent(QFocusEvent *pEvent);
	virtual void focusOutEvent(QFocusEvent *pEvent);
	*/

private slots:
	void linAxisMenu(void);
	void logAxisMenu(void);

	void upButtonClick(void);
	void downButtonClick(void);

};

#endif	// VERTICALAXISWIDGET_H
