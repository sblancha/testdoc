#ifndef RACKICONVRPTP350_H
#define RACKICONVRPTP350_H

#include "RackIcon.h"

class Eqp;

class VACCTLEWOUTIL_EXPORT RackIconVRPTP350 : public RackIcon
{
	Q_OBJECT
public:
	RackIconVRPTP350(QWidget * parent, Qt::WindowFlags f = 0);
	virtual ~RackIconVRPTP350();

	virtual void setEqp(Eqp * pEqp);
	virtual bool connect(void);
	virtual bool disconnect(void);
protected:
	Eqp * pEqp; //pointer to equipment, if it exists
	virtual void paintEvent(QPaintEvent *pEvent);

};

#endif
