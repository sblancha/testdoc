#ifndef	BAKEOUTRACKDETAILSDIALOG_H
#define	BAKEOUTRACKDETAILSDIALOG_H

// Bakeout rack details - represented in table

#include "DataEnum.h"

#include <QDialog>

class BakeoutRackTable;
class BakeoutCtlTable;

class Sector;
class Eqp;

class BakeoutTree;
#include <QLabel>
class ExtendedLabel;
#include <QMenu>

class BakeoutRackDetailsDialog : public QDialog
{
	Q_OBJECT

public:
	BakeoutRackDetailsDialog();
	~BakeoutRackDetailsDialog();

	static void create(void);

signals:
	void dpMouseDown(int button, int mode, int x, int y, int extra, const char *dpName);

protected:

	// The only instances of dialog
	static BakeoutRackDetailsDialog *pInstance;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Tree of main parts, sectors and devices
	BakeoutTree			*pTree;

	// Label with selected rack name
	ExtendedLabel		*pEqpNameLabel;

	// Label for safety state indication
	QLabel				*pSafetyLabel;

	// Label for PCS (== Power Cut System) state indication
	QLabel				*pPcsLabel;

	// File menu
	QMenu		*pFileMenu;

	// Help menu
	QMenu		*pHelpMenu;

	// Regulation channels table
	BakeoutRackTable	*pTable;

	// Control channels table
	BakeoutCtlTable		*pCtlTable;

	void buildLayout(void);
	void buildFileMenu(void);
	void buildHelpMenu(void);

private slots:
	void eqpSelectChanged(Eqp *pEqp);
	void eqpStateChange(void);

	void filePrint(void);
	void help(void);
};

#endif	// BAKEOUTRACKDETAILSDIALOG_H
