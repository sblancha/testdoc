#ifndef	BAKEOUTRACKCHANNEL_H
#define	BAKEOUTRACKCHANNEL_H

// Class holding data for one bakeout rack channel (both T and SET)

#include "InterfaceEqp.h"

#include <qcolor.h>
#include <qrect.h>

class Eqp;

// Bits in status
#define	PCS_BIT	(0x200)
#define	SAF_BIT	(0x100)
#define	STATUS_MASK	(0xFF)

class BakeoutRackChannel : public InterfaceEqp
{
	Q_OBJECT

public:
	BakeoutRackChannel(Eqp *pEqp, int channel);
	~BakeoutRackChannel();

	// Access
	inline int getChannel(void) const { return channel; }
	inline float getValue(void) const { return value; }
	inline float getValueSet(void) const { return valueSet; }
	inline int getStatus(void) const { return status; }
	inline bool isValueValid(void) const { return valueValid; }
	inline bool isActive(void) const {return active; }
	void setActive(bool flag);

	inline void setNarrow(int coord) { rect.setLeft(coord); rect.setRight(coord); setRect.setLeft(coord - 1); setRect.setRight(coord + 1); }
	inline void setWide(int left, int right) { rect.setLeft(left); rect.setRight(right); setRect.setLeft(left - 1); setRect.setRight(right + 1); }
	inline void setTop(int coord) { rect.setTop(coord); }
	inline void setBottom(int coord) { rect.setBottom(coord); }
	inline void setSetCoord(int coord) { setRect.setTop(coord - 1); setRect.setBottom(coord + 1); }

	inline int getLeft(void) const { return rect.left(); }
	inline int getRight(void) const { return rect.right(); }
	inline int getTop(void) const { return rect.top(); }
	inline int getBottom(void) const { return rect.bottom(); }
	inline const QRect &getRect(void) const { return rect; }
	inline int getLeftSet(void) const { return setRect.left(); }
	inline int getRightSet(void) const { return setRect.right(); }
	inline int getTopSet(void) const { return setRect.top(); }
	inline int getBottomSet(void) const { return setRect.bottom(); }
	inline const QRect &getRectSet(void) const { return setRect; }

	inline QColor &getColor(void) { return color; }
	const QString getStatusString(void);
	const QString getShortStatusString(void);

public slots:	// Implement slots of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);
		
signals:	// Define own signals
	void eqpDataChanged(BakeoutRackChannel *pSrc, bool colorChanged);

protected:

	// Pointer to device
	Eqp			*pEqp;

	// Last known value
	int			value;

	// Last known set value
	int			valueSet;

	// Last known channel status
	int			status;

	// Name of T DPE
	QString		tDpeName;

	// Name of SET DPE
	QString		setDpeName;

	// Name of STATE DPE
	QString		statusDpeName;

	// Color used for drawing
	QColor		color;

	// Bar boundaries
	QRect		rect;

	// Set bar boundaries
	QRect		setRect;

	// Channel number
	int			channel;

	// Flag indicating if last known value is valid
	bool		valueValid;

	// Flag indicating if channel is active
	bool		active;

	void calculateColor(bool emitSignal);
};


#endif	// BAKEOUTRACKCHANNEL_H
