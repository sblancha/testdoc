#ifndef	VACICONVG_A_RO_H
#define	VACICONVG_A_RO_H

//	Icon for VG_A_RO

#include "VacIconVG.h"

class EqpVG_A_RO;

class VACCTLEWOUTIL_EXPORT VacIconVG_A_RO : public VacIconVG
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVG_A_RO(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVG_A_RO();
	virtual void setEqp(Eqp *pEqp);

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVG_A_RO_H
