//	Implementation of LhcSegment class

#include "LhcSegment.h"
#include "MainLHCImage.h"

#include "DataPool.h"
#include "BeamLinePart.h"
#include "Sector.h"

#include "FunctionalType.h"
#include "VacType.h"
#include "EqpType.h"
#include "Eqp.h"
#include "EqpVACOK.h"

#include <QPainter>
#include <QMessageBox>

#include <math.h>

#include "PlatformDef.h"
#include "DebugCtl.h"

LhcSegment::LhcSegment(MainLHCImage *owner)
{
	this->owner = owner;
	vacType = VacType::None;
	start = end = 0;
	pSector = NULL;
	startAngleSet = endAngleSet = haveStateColor = false;
	connected = false;
	pEqp = pEqp2 = pOtherLineEqp = NULL;
	connectedEqp = connectedEqp2 = false;
	interlockHidden = false;
	pNeighborEqpList = NULL;
}

LhcSegment::LhcSegment(MainLHCImage *owner, float start, float end, Sector *pSector)
{
	this->owner = owner;
	this->pSector = pSector;
	this->start = start;
	this->end = end;
	vacType = VacType::None;
	startAngleSet = endAngleSet = haveStateColor = false;
	connected = false;
	pEqp = pEqp2 = pOtherLineEqp = NULL;
	connectedEqp = connectedEqp2 = false;
	interlockHidden = false;
	pNeighborEqpList = NULL;
}

LhcSegment::LhcSegment(MainLHCImage *owner, int vacType, float start, float end, Sector *pSector)
{
	this->owner = owner;
	this->pSector = pSector;
	this->start = start;
	this->end = end;
	this->vacType = vacType;
	startAngleSet = endAngleSet = haveStateColor = false;
	connected = false;
	pEqp = pEqp2 = NULL;
	connectedEqp = connectedEqp2 = false;
	// Check if this segment contains other line connection
	pOtherLineEqp = findLineConnect();
	interlockHidden = false;
	pNeighborEqpList = NULL;
}

LhcSegment::~LhcSegment()
{
	if(connected && pSector)
	{
		switch(vacType)
		{
		case VacType::Qrl:
		case VacType::Cryo:
		case VacType::DSL:
			disconnectEqpIsol();
			break;
		default:
			disconnectEqpBeam();
			break;
		}
	}
	clear();
	resetNeighborEqp();
}

void LhcSegment::resetNeighborEqp(void)
{
	if(pNeighborEqpList != NULL)
	{
		delete pNeighborEqpList;
		pNeighborEqpList = NULL;
	}
}

/*
**	FUNCTION
**		Connect to all equipment responsible for color of this segment.
**		Connect owner to signal of this segment
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcSegment::connectEqp(void)
{
	if(connected)
	{
		return;	// To be done only once
	}
	connected = true;
	if(!pSector)
	{
		return;
	}
	switch(vacType)
	{
	case VacType::Qrl:
	case VacType::Cryo:
	case VacType::DSL:
		connectEqpIsol();
		break;
	default:
		connectEqpBeam();
		break;
	}
}

/*
**	FUNCTION
**		Connect to all equipment responsible for color of this segment - for
**		beam vacuum, equipment is valve(s).
**		Connect owner to signal of this segment
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcSegment::connectEqpBeam(void)
{
	if(!pEqp)
	{
		return;
	}
	pEqp->connect(this, DataEnum::Online);
	if(pEqp2)
	{
		pEqp2->connect(this, DataEnum::Online);
	}
	QObject::connect(this, SIGNAL(stateChanged(void)), owner, SLOT(eqpStateChanged(void)));
}

void LhcSegment::disconnectEqpBeam(void)
{
	if(!pEqp)
	{
		return;
	}
	pEqp->disconnect(this, DataEnum::Online);
	if(pEqp2)
	{
		pEqp2->disconnect(this, DataEnum::Online);
	}
}

/*
**	FUNCTION
**		Connect to all equipment responsible for color of this segment - for
**		isolation vacuum, equipment is VACOK(s).
**		Connect owner to signal of this segment
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcSegment::connectEqpIsol(void)
{
	pEqp = pEqp2 = NULL;
	connectedEqp = connectedEqp2 = false;
	bool hasMainEqp = false;

	// Find all VACOKs on this sector - must be 2 devices: NLOST & OK
	const QHash<QByteArray, Eqp *> &eqpDict = DataPool::getInstance().getEqpDict();
	foreach(Eqp *pCheckEqp, eqpDict)
	{
		if(pCheckEqp->getFunctionalType() != FunctionalType::VACOK)
		{
			continue;
		}
		const QString forCryo = pCheckEqp->getAttrValue("OutForCRYO");
		if(forCryo.isEmpty())
		{
			continue;
		}
		bool isMyEqp = false,
			isMainEqp = false;
		if(pSector == pCheckEqp->getSectorBefore())
		{
			isMyEqp = true;
			hasMainEqp = isMainEqp = true;
		}
		else
		{
			char attrName[32];
			for(int n = 2 ; n <= 4 ; n++)
			{
#ifdef Q_OS_WIN
				sprintf_s(attrName, sizeof(attrName) / sizeof(attrName[0]), "SectorIntl%d", n);
#else
				sprintf(attrName, "SectorIntl%d", n);
#endif
				const QString attrValue = pCheckEqp->getAttrValue(attrName);
				if(!attrValue.isEmpty())
				{
					if(attrValue == pSector->getName())
					{
						isMyEqp = true;
						break;
					}
				}
			}
		}

		if(isMyEqp)	// VACOK for this sector
		{
			const QString logic = pCheckEqp->getAttrValue("Logic");
			if(logic.isEmpty())
			{
				printf("LhcSegment::connectEqpIsol(): no logic for %s\n", pCheckEqp->getName());
				fflush(stdout);
				continue;
			}
			if(logic == "1")
			{
				if(pEqp)
				{
					printf("LhcSegment::connectEqpIsol(): multiple eqp for sector %s logic=1: %s and %s\n",
						pSector->getName(), pEqp->getName(), pCheckEqp->getName());
					continue;
				}
				pEqp = pCheckEqp;
				if(isMainEqp)
				{
					pEqp->connect(this, DataEnum::Online);
					connectedEqp = true;
				}
			}
			else
			{
				if(pEqp2)
				{
					printf("LhcSegment::connectEqpIsol(): multiple eqp for sector %s logic=0: %s and %s\n",
						pSector->getName(), pEqp2->getName(), pCheckEqp->getName());
					continue;
				}
				pEqp2 = pCheckEqp;
				if(isMainEqp)
				{
					pEqp2->connect(this, DataEnum::Online);
					connectedEqp2 = true;
				}
			}
		}
	}
	if(hasMainEqp)
	{
		QObject::connect(this, SIGNAL(stateChanged(void)), owner, SLOT(eqpStateChanged(void)));
	}
	haveStateColor = (pEqp != NULL) || (pEqp2 != NULL);
}
void LhcSegment::disconnectEqpIsol(void)
{
	if(pEqp && connectedEqp)
	{
		pEqp->disconnect(this, DataEnum::Online);
	}
	if(pEqp2 && connectedEqp2)
	{
		pEqp2->disconnect(this, DataEnum::Online);
	}
}


/*
**	FUNCTION
**		Slot receiving notifications from equipment, see InterfaceEqp class
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcSegment::dpeChange(Eqp * /* pSrc */, const char * /* dpeName */,
	DataEnum::Source /* source */, const QVariant & /* value */, DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	// Only online mode calls are of interest for main view
	if(mode != DataEnum::Replay)
	{
		emit stateChanged();
	}
}

/*
**
**	FUNCTION
**		Clear all angles
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void LhcSegment::clear(void)
{
	while(!angles.isEmpty())
	{
		delete angles.takeFirst();
	}
}

/*
**
**	FUNCTION
**		Build for list of points with pre-calculated values
**		of sine and cosine - they will be used for drawing.
**		Every piece of LHC ring vacuum is drawn using polyline, so it shall
**		contain small enough segments to look like part of circle.
**		Step 0.02 corresponds to ~1.15 degrees - shall be OK for reasonable
**		screen resolutions.
**
**	PARAMETERS
**		index	- Index of this segment, used for error reporting
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void LhcSegment::buildPoints(int index)
{
	angles.clear();
	char	buf[512];

	if(endAngle > 20.0)
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "segment %d type %d: angles from %f to %f",
			index, vacType, startAngle, endAngle);
#else
		sprintf(buf, "segment %d type %d: angles from %f to %f",
			index, vacType, startAngle, endAngle);
#endif
		QMessageBox::critical(owner, "ERROR 0", buf, QMessageBox::Ok, QMessageBox::NoButton);
	}
	if(startAngle > endAngle )
	{
		endAngle += 2.0 * M_PI;
	}
	double angle = startAngle;
	int nPoints = (int)ceil((endAngle - startAngle) / 0.02) + 1;
	if(nPoints < 2)
	{
		nPoints = 2;
	}
	do
	{
		if(angles.count() >= nPoints )
		{
#ifdef Q_OS_WIN
			sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "segment %d type %d: angles from %f to %f, nPoints %d nAngles %d",
				index, vacType, startAngle, endAngle, nPoints, angles.count());
#else
			sprintf(buf, "segment %d type %d: angles from %f to %f, nPoints %d nAngles %d",
				index, vacType, startAngle, endAngle, nPoints, angles.count());
#endif
			QMessageBox::critical(owner, "ERROR 1", buf, QMessageBox::Ok, QMessageBox::NoButton);
		}
		angles.append(new SineCosine(angle));
		angle += 0.02;
	} while( angle < endAngle );
	if(angles.last()->getAngle() < endAngle)	// Add end point
	{
		angles.append(new SineCosine(endAngle));
	}
}

/*
**
**	FUNCTION
**		Build list of points with pre-calculated values
**		of sine and cosine - they will be used for drawing.
**		VERSION FOR ARC SEGMENT
**		Every piece of LHC ring vacuum is drawn using polyline, so it shall
**		contain small enough segments to look like part of circle.
**		Step 0.02 corresponds to ~1.15 degrees - shall be OK for reasonable
**		screen resolutions.
**
**	PARAMETERS
**		index	- Index of this segment, used for error reporting
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void LhcSegment::buildArcPoints(int index)
{
	angles.clear();
	if(endAngle > 20.0)
	{
		char	buf[512];
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "segment %d type %d: angles from %f to %f",
			index, vacType, startAngle, endAngle );
#else
		sprintf(buf, "segment %d type %d: angles from %f to %f",
			index, vacType, startAngle, endAngle );
#endif
		QMessageBox::critical(owner, "ERROR 0", buf, QMessageBox::Ok, QMessageBox::NoButton);
	}
	if(startAngle > endAngle)
	{
		endAngle += 2.0 * M_PI;
	}
	angles.append(new SineCosine(startAngle));
	angles.append(new SineCosine(endAngle));
}

/*
**
**	FUNCTION
**		Draw selection lines for this segment.
**
**	PARAMETERS
**		painter	- Painter used for drawing
**		color	- Color used to draw selection
**		lineWidth	- Width of selection line [Pixels]
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void LhcSegment::drawSelection(QPainter &painter, QColor &color, short lineWidth)
{
	typeColor = color;
	short	oldRad = innerRad;
	innerRad = outerRad - lineWidth;
	draw(painter, true);
	innerRad = oldRad;
	oldRad = outerRad;
	outerRad = innerRad + lineWidth;
	draw(painter, true);
	outerRad = oldRad;
}

/*
**
**	FUNCTION
**		Draw one segment of vacuum system (QRL/CRYO/SELECT segments)
**		This method decides how exactly segment shall be drawn (use device
**		state color or not)
**
**	PARAMETERS
**		painter		- painter used for drawing
**		fillOnly	- true if segment interior shall be filled only, stroke is not required
**
** 	RETURNS
**		None
**
** 	CAUTIONS
**		None
**
*/
void LhcSegment::draw(QPainter &painter, bool fillOnly)
{
	if(angles.isEmpty())
	{
		return;
	}

	// Draw the whole segment using type color
	draw(painter, innerRad, outerRad, typeColor, fillOnly);

	// Draw Eqp-state dependent part
	if(haveStateColor)
	{
		int width;
		if(vacType == VacType::Qrl)
		{
			width = (outerRad - innerRad) / 4;
		}
		else
		{
			width = (outerRad - innerRad) / 12;
		}

		QColor color;
		int radIn, radOut;
		if(pEqp)
		{
			pEqp->getMainColor(color, DataEnum::Online);
			radOut = outerRad;
			radIn = radOut - width;
			draw(painter, radIn, radOut, color, fillOnly);
		}
		if(pEqp2)
		{
			pEqp2->getMainColor(color, DataEnum::Online);
			radOut = outerRad - width - 1;
			radIn = radOut - width;
			draw(painter, radIn, radOut, color, fillOnly);
		}
	}
}

/*
**
**	FUNCTION
**		Draw segment using type color and given inner and outer radii
**
**	PARAMETERS
**		painter		- painter used for drawing
**		radInner	- inner radius for drawing
**		radOuter	- outer radius for drawing
**		color		- color used to fill segment
**		fillOnly	- true if segment interior shall be filled only, stroke is not required
**
** 	RETURNS
**		None
**
** 	CAUTIONS
**		None
**
*/
void LhcSegment::draw(QPainter &painter, short radInner, short radOuter, QColor &color, bool fillOnly)
{
	QPolygon points(angles.count() * 2);
	int n = 0, idx;
	SineCosine *pAngle;
	for(idx = 0 ; idx < angles.count() ; idx++)
	{
		pAngle = angles.at(idx);
		short x = owner->getRingCenterX() + (short)rint(radOuter * pAngle->getCosValue());
		short y = owner->getRingCenterY() - (short)rint(radOuter * pAngle->getSinValue());
		points.setPoint(n++, x, y);
	}
	for(idx = angles.count() - 1 ; idx >= 0 ; idx--)
	{
		pAngle = angles.at(idx);
		short x = owner->getRingCenterX() + (short)rint(radInner * pAngle->getCosValue());
		short y = owner->getRingCenterY() - (short)rint(radInner * pAngle->getSinValue());
		points.setPoint(n++, x, y);
	}
	/* QPainter's drawPolygon() always closes the path.
	BEWARE: number of points must be correct !!!
	points.setPoint(n, points[0]);	// Close the path
	*/
	painter.setBrush(color);
	painter.drawPolygon(points);
	if(!fillOnly)
	{
		painter.setPen(Qt::black);
		painter.drawPolyline(points);
	}
}

/*
**
**	FUNCTION
**		Draw one beam vacuum segment of vacuum system
**
**	PARAMETERS
**		painter			- Painter used for drawing
**		color			- color used to fill segment
**		beamTypeMethod	- Method used for drawing line corresponding to beam type (enum)
**		beamTypeWidth	- Width of line corresponding to beam type
**
** RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void LhcSegment::drawBeamSegment(QPainter &painter, QColor &color,
	short beamTypeMethod, short beamTypeWidth, FILE *pFile)
{
	QPolygon inner, outer, middle;
	if(!buildBeamSegmentPolygones(beamTypeMethod, beamTypeWidth, inner, outer, middle, pFile))
	{
		return;
	}

	// Drawing is done differently depending on number of points 
	painter.setPen(QPen(color, 1));
	if((inner.count() > 1) || (outer.count() > 1))	// LIK 10.09.2008: MSVC used && condition
	{
		QPolygon total(outer);
		total.putPoints(outer.count(), inner.count(), inner);
		painter.setBrush(color);
		painter.drawPolygon(total);
	}
	else
	{
		painter.drawLine(inner.point(0), outer.point(outer.count()-1));
		painter.drawLine(outer.point(outer.count()-1), outer.point(0));
	}

	// Draw vacuum type color
	painter.setPen(QPen(typeColor, beamTypeWidth));
	if(inner.count() > 0)
	{
		if(pFile && DebugCtl::isMainView())
		{
			fprintf(pFile, "   Inner border: inner contains %d\n", inner.count());
		}
		if(inner.count() > 1)
		{
			painter.drawPolyline(inner);
		}
		else
		{
			painter.drawLine(inner.point(0), inner.point(0));
		}
	}

	if(outer.count() > 0)
	{
		if(pFile && DebugCtl::isMainView())
		{
			fprintf(pFile, "   Outer border: outer contains %d\n", outer.count());
		}
		if(outer.count() > 1)
		{
			painter.drawPolyline(outer);
		}
		else
		{
			painter.drawLine(outer.point(0), outer.point(0));
		}
	}

	if(middle.count() > 0)
	{
		if(pFile && DebugCtl::isMainView())
		{
			fprintf(pFile, "   Middle border: middle contains %d\n", middle.count());
		}
		painter.drawPolyline(middle);
	}
}

int LhcSegment::buildBeamSegmentPolygones(short beamTypeMethod, short beamTypeWidth, QPolygon &inner, QPolygon &outer, QPolygon &middle, FILE *pFile)
{
	if(pFile && DebugCtl::isMainView())
	{
		fprintf(pFile, "  SEGMENT: %d from %f to %f", vacType, start, end);
		if(pSector)
		{
			fprintf(pFile, " : %s", pSector->getName());
		}
		fprintf(pFile, "\n");
		if(pEqp)
		{
			QColor color;
			pEqp->getMainColor(color, DataEnum::Online);
			fprintf(pFile, "   eqp    : %s (order %d color %X)\n", pEqp->getName(),
				pEqp->getDrawOrder(), ((color.red() << 16) || (color.green() << 8) || color.blue()));
		}
		if(pEqp2)
		{
			QColor color;
			pEqp2->getMainColor(color, DataEnum::Online);
			fprintf(pFile, "   eqp2   : %s (order %d color %X)\n", pEqp2->getName(),
				pEqp2->getDrawOrder(), ((color.red() << 16) || (color.green() << 8) || color.blue()));
		}
		fprintf(pFile, "   rel    : start %f end %f\n", relStart, relEnd );
		fprintf(pFile, "   anglse : start %f end %f\n", startAngle, endAngle);
		fprintf(pFile, "   radii  : inner %d outer %d\n", innerRad, outerRad);
		fprintf(pFile, "   %d points:\n", angles.count());
	}
	if(!angles.count())
	{
		return 0;
	}

	float	middleRad = 0;
	if(beamTypeWidth > 0)
	{
		switch(beamTypeMethod)
		{
		case MainLHCImage::MiddleLine:	// Middle
			middleRad = (float)(0.5 * ((float)outerRad + (float)innerRad));
			break;
		default:	// Nothing
			break;
		}
	}

	// Outer and middle polylines
	int x, y, prevX = -100, prevY = -100;
	int	idx;
	for(idx = 0 ; idx < angles.count() ; idx++)
	{
		SineCosine *pAngle = angles.at(idx);
		x = owner->getRingCenterX() + (int)rint(outerRad * pAngle->getCosValue());
		y = owner->getRingCenterY() - (int)rint(outerRad * pAngle->getSinValue());
		if(pFile && DebugCtl::isMainView())
		{
			if(!outer.count())
			{
				fprintf(pFile, "   START FROM %d %d\n", x, y);
			}
		}
		if((x != prevX) || (y != prevY))
		{
			outer.append(QPoint(x, y));
			if(pFile && DebugCtl::isMainView())
			{
				fprintf(pFile, "     MOVE TO %d %d\n", x, y);
			}
			prevX = x;
			prevY = y;
		}
		if(middleRad > 0)
		{
			x = owner->getRingCenterX() + (int)rint(middleRad * pAngle->getCosValue());
			y = owner->getRingCenterY() - (int)rint(middleRad * pAngle->getSinValue());
			middle.append(QPoint(x, y));
		}
	}

	// Inner polyline
	for(idx = angles.count() - 1 ; idx >= 0 ; idx--)
	{
		SineCosine *pAngle = angles.at(idx);
		x = owner->getRingCenterX() + (int)rint(innerRad * pAngle->getCosValue());
		y = owner->getRingCenterY() - (int)rint(innerRad * pAngle->getSinValue());
		if((x != prevX) || (y != prevY))
		{
			inner.append(QPoint(x, y));
			if(pFile && DebugCtl::isMainView())
			{
				fprintf(pFile, "     inner MOVE TO %d %d\n", x, y);
			}
			prevX = x;
			prevY = y;
		}
	}
	if(pFile && DebugCtl::isMainView())
	{
		fprintf(pFile, "   FINISH, nInner = %d\n", inner.count());
	}
	return inner.count() + outer.count() + middle.count();
}

void LhcSegment::drawBeamEqp(QPainter &painter, const QColor &color, const QColor &borderColor, int /* beamTypeWidth */)
{
	int x, y, imageRad;
	if(!getBeamEqpGeometry(x, y, imageRad))
	{
		return;
	}
	painter.setBrush(color);
	painter.setPen(color);
	painter.drawPie(x - imageRad, y - imageRad, imageRad << 1, imageRad << 1,
		0, 5760);
	painter.setPen(QPen(borderColor, 2));
	painter.drawArc(x - imageRad, y - imageRad, imageRad << 1, imageRad << 1,
		0, 5760);
}

bool LhcSegment::getBeamEqpGeometry(int &x, int &y, int &radius)
{
	if(angles.isEmpty())
	{
		return false;
	}
	//int imageRad = (int)rint(0.5 * float(outerRad - innerRad - beamTypeWidth));
	radius = (int)rint(0.5 * float(outerRad - innerRad));
	if(radius < 1)
	{
		return false;
	}
	int middleRad = (outerRad + innerRad) >> 1;
	SineCosine *pAngle = angles.at(0);
	x = owner->getRingCenterX() + (int)rint(middleRad * pAngle->getCosValue());
	y = owner->getRingCenterY() - (int)rint(middleRad * pAngle->getSinValue());
	return true;
}

/*
**
**	FUNCTION
**		Find connection of other (injection/dump) line within given segment
**		Change 11.03.2008: only use vacType R and B, ignore other vacuum types:
**		this is done to ignore DSL line(s) connected to CRYO line
**
**	PARAMETERS
**		None
**
** RETURNS
**		Pointer to device corresponding to other line connection in this segment, or
**		NULL if there is no other line connection in this segment
**
**	CAUTIONS
**		None
**
*/
Eqp *LhcSegment::findLineConnect(void)
{
	if((vacType == VacType::Qrl) || (vacType == VacType::Cryo))
	{
		return NULL;
	}
	DataPool &pool = DataPool::getInstance();
	BeamLine	*pLine = pool.getCircularLine();
	if(!pLine)
	{
		return NULL;
	}
	const QList<BeamLinePart *> &parts = pLine->getParts();
	BeamLinePart *pPart;
	for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		pPart = parts.at(partIdx);
		const QList<Eqp *> &eqpList = pPart->getEqpList();
		Eqp *pEqp;
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			pEqp = eqpList.at(eqpIdx);
			if(pEqp->getVacType() != vacType)
			{
				continue;
			}
			if((pEqp->getType() != EqpType::OtherLineStart) &&
				(pEqp->getType() != EqpType::OtherLineEnd))
			{
				continue;
			}
			if(start < end)
			{
				if((start <= pEqp->getStart()) && (pEqp->getStart() <= end))
				{
					return pEqp;
				}
			}
			else
			{
				if((start <= pEqp->getStart()) || (pEqp->getStart() <= end))
				{
					return pEqp;
				}
			}
		}
	}
	return NULL;
}

void LhcSegment::checkForNeighborEqp(LhcSegment *pOtherSegment, const QPolygon &otherEqpPolygon)
{
	if(innerRad >= pOtherSegment->outerRad)
	{
		return;
	}
	if(outerRad <= pOtherSegment->innerRad)
	{
		return;
	}
	switch(vacType)
	{
	case VacType::RedBeam:
	case VacType::BlueBeam:
	case VacType::CrossBeam:
	case VacType::CommonBeam:
		break;
	default:
		return;
	}
	QPolygon inner, outer, middle;
	if(!buildBeamSegmentPolygones(0, 0, inner, outer, middle, NULL))
	{
		return;
	}
	QPolygon total(outer);
	total.putPoints(outer.count(), inner.count(), inner);
	QPolygon intersect = otherEqpPolygon.intersected(total);
	if(intersect.isEmpty())
	{
		return;
	}
	// qDebug("%s is neighbor for %s\n", pEqp->getDpName(), pOtherSegment->getEqp()->getDpName());
	if(pEqp)
	{
		pOtherSegment->addNeighborEqp(pEqp);
	}
	if(pEqp2)
	{
		pOtherSegment->addNeighborEqp(pEqp2);
	}
}

void LhcSegment::addNeighborEqp(Eqp *pNeighborEqp)
{
	if((pNeighborEqp == pEqp) || (pNeighborEqp == pEqp2))
	{
		return;
	}
	if(!pNeighborEqpList)
	{
		pNeighborEqpList = new QList<Eqp *>();
		pNeighborEqpList->append(pNeighborEqp);
	}
	else
	{
		if(!pNeighborEqpList->contains(pNeighborEqp))
		{
			pNeighborEqpList->append(pNeighborEqp);
		}
	}
}

/*
**
**	FUNCTION
**		Dump parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**		index	- Index of this segment in list
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void LhcSegment::dump(FILE *pFile, int index)
{
	if(!DebugCtl::isMainView())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	fprintf(pFile, " %03d: %d from %f to %f", index, vacType, start, end);
	if(pSector)
	{
		fprintf(pFile, " : <%s>", pSector->getName());
	}
	if(pOtherLineEqp)
	{
		fprintf(pFile, ", LINE CONNECTION : <%s>", pOtherLineEqp->getName());
	}
	fprintf(pFile, "\n");
	if(pEqp)
	{
		QColor color;
		pEqp->getMainColor(color, DataEnum::Online);
		fprintf(pFile, "   eqp    : %s (order %d color %X)\n", pEqp->getName(),
			pEqp->getDrawOrder(), ((color.red() << 16) || (color.green() << 8) || color.blue()));
	}
	if(pEqp2)
	{
		QColor color;
		pEqp2->getMainColor(color, DataEnum::Online);
		fprintf(pFile, "   eqp2   : %s (order %d color %X)\n", pEqp2->getName(),
			pEqp2->getDrawOrder(), ((color.red() << 16) || (color.green() << 8) || color.blue()));
	}
	fprintf(pFile, "   rel    : start %f end %f\n", relStart, relEnd);
	fprintf(pFile, "   angles : start %f end %f\n", startAngle, endAngle);
	fprintf(pFile, "   radii  : inner %d outer %d\n", innerRad, outerRad);
	fprintf(pFile, "   %d points:\n", angles.count());
	for(int n = 0 ; n < angles.count() ; n++)
	{
		SineCosine *pAngle = angles.at(n);
		pAngle->dump(pFile, n);
	}
}
