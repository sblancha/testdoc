#pragma once
#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpEXT_ALARM : public Eqp
{
	Q_OBJECT

public:
	EqpEXT_ALARM(const Eqp &source);
#ifdef Q_OS_WIN
	EqpEXT_ALARM(const EqpEXT_ALARM &src) : Eqp(src) {}
#endif
	~EqpEXT_ALARM();

	virtual void postProcess(void);
	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

#ifndef PVSS_SERVER_VERSION
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
#endif
};
