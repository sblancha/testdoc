//	Implementation of VacIconVGD_VELO class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGD_VELO.h"

#include <qpainter.h>

// We only need to implement drawSpecific() method

// Draw 'membrane' symbol in the center of icon
void VacIconVGD_VELO::drawSpecific(QPainter &painter)
{
	QPen pen(Qt::black, 3);
	painter.setPen(pen);
	painter.drawLine(5, 10, 15, 10);
}
