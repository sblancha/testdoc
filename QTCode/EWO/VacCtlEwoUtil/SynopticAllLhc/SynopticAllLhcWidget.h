#ifndef	SYNOPTICALLLHCWIDGET_H
#define	SYNOPTICALLLHCWIDGET_H

#include "VacCtlEwoUtilExport.h"

// Dialog for displaying synoptic of all cryogeinc vacuum of LHC

#include "LhcRingSelection.h"

//#include "SynLhcView.h"
#include "LhcRingSelection.h"
#include "DataEnum.h"

#include <QWidget>

#include <QLabel>

class SynopticAllLhcWidget : public QWidget, public LhcRingSelection
{
	Q_OBJECT

public:
	VACCTLEWOUTIL_EXPORT SynopticAllLhcWidget(QWidget *parent = 0, Qt::WindowFlags f = 0, DataEnum::DataMode mode = DataEnum::Online);
	VACCTLEWOUTIL_EXPORT ~SynopticAllLhcWidget();

	// Access
	VACCTLEWOUTIL_EXPORT inline int getLhcPartIndex(void) const { return lhcPartIndex; }
	VACCTLEWOUTIL_EXPORT void setLhcPartIndex(int index);

protected:

	// Label with synoptic title
	QLabel					*pTitleLabel;

	// List of synoptic views
	// QPtrList<SynLhcView>	images;

	// Index of LHC part to be shown (can be 0 or 1)
	int						lhcPartIndex;

	// Data acquisition mode
	DataEnum::DataMode		mode;

	void buildLayout();
};

#endif	// SYNOPTICALLLHCWIDGET_H
