#ifndef SECTORORDER_H
#define SECTORORDER_H

#include "VacCtlEqpDataExport.h"

// Static class holding methods to build list of sectors ordered according to geography

#include <stdio.h>

#include <qlist.h>

#include <QStringList>

class DataPool;
class BeamLine;
class BeamLinePart;
class Sector;
class Eqp;

class VACCTLEQPDATA_EXPORT SectorOrder  
{
public:
	static DataPool	*pPool;

	static void setDataPool(DataPool *pDataPool) { pPool = pDataPool; }
	static int orderSectors(QStringList &errList);

	// Access
	static inline bool isKeepManualOrderOnCircularLines(void) { return keepManualOrderOnCircularLines; }
	static inline void setKeepManualOrderOnCircularLines(bool flag) { keepManualOrderOnCircularLines = flag; }

private:
	static QString dumpFileName;
	static FILE	*pErrorFile;
	static bool keepManualOrderOnCircularLines;

	static int orderLhcSectors(QStringList &errList);
	static int orderSurveySectors(QStringList &errList);
	static void assignNewPointers(void);
	static void assignBeamOrder(Sector **sectArray, int &nNewSectors);

	static void addSectorsOfCircularLine(BeamLine *pLine, QList<Sector *> &newSectors);
	static BeamLinePart *findStartOnCircularLineManualOrder(BeamLine *pLine, int &startEqpIdx);
	static BeamLinePart *findStartOnCircularLineAutoOrder(BeamLine *pLine, int &startEqpIdx);
	static void addSectorsOfStraightLine(BeamLine *pLine, QList<Sector *> &newSectors);
	static void addSectorsOfChildLine(Eqp *pStartEqp, const char *parentRawLine,
		QList<Sector *> &newSectors, char *sectName, size_t sectNameLen);
	static bool checkForNewSect(BeamLine *pLine, Eqp *pEqp, Sector *pSect,
		QList<Sector *> &newSectors, char *sectName, size_t sectNameLen);
	static void addSectorCopy(QList<Sector *> &newSectors, Sector *pSect);
	static void addSectorAnyway(QList<Sector *> &newSectors, Sector *pSector);
	static int buildDrawParts(QStringList &errList);
	static void addOrphanLines(void);
	static void addDrawPartOnChildLine(Eqp *pConnEqp, Sector *pSector);
};

#endif	// SECTORORDER_H
