#ifndef	VACICONVP_TP_H
#define	VACICONVP_TP_H

//	Icon for VP_TP

#include "VacIcon.h"

class EqpVP_TP;

class VACCTLEWOUTIL_EXPORT VacIconVP_TP : public VacIcon
{
	Q_OBJECT

public:
	// CONSTRUCTOR  (parent argument mandatory?)
	VacIconVP_TP(QWidget *parent, Qt::WindowFlags f = 0);
	// DESCTRUCTOR
	virtual ~VacIconVP_TP();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);
	virtual void drawInterlockSrcArrow(QPainter &painter, bool before, bool after);
	
protected:
	// Pointer to my device
	EqpVP_TP *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, float rotation, bool plcAlarm);
	virtual void drawAnimation(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, float rotation, bool plcAlarm);
	virtual void drawSymbols(QPainter &painter);
	virtual void drawForced(QPainter &painter);
	virtual void drawManual(QPainter &painter);
	virtual void drawFullInterlock(QPainter &painter);
	virtual void drawStartInterlock(QPainter &painter);
	virtual void drawCmdOff(QPainter &painter);
	virtual void drawCmdOn(QPainter &painter);
};

#endif	// VACICONVP_TP_H
