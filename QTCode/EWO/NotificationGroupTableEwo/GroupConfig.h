#ifndef GROUPCONFIG_H
#define	GROUPCONFIG_H

// Data for group SMS configuration

#include "SmsConfig.h"
#include "SmsGroup.h"

#include <qcolor.h>
#include <qlist.h>
#include <qstringlist.h>

class GroupConfig : public SmsConfig
{
	Q_OBJECT

public:
	GroupConfig(const QString &groupDpName, const QStringList &simpleDpNames);
	~GroupConfig();

	SmsGroup *addGroup(const QString &dpName);
	void removeGroup(const QString &dpName);

	// Access
	virtual bool isGroup(void) const { return true; }
	virtual inline int getNumChildren(void) const { return list.count(); }
	inline int getNumErrors(void) const { return numErrors; }
	inline const QList<SmsGroup *> &getList(void) { return list; }
	virtual void setDeleted(bool flag);

protected:

	// List of simple configurations in this group
	QList<SmsGroup *>	list;

	// Error count
	int		numErrors;

	virtual void calculateState(void);

private slots:
	void configStateChange(SmsConfig *pSrc);
	void configNumMsgChange(SmsConfig *pSrc, int delta);
};

#endif	// GROUPCONFIG_H
