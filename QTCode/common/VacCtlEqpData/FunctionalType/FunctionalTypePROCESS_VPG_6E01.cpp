//	Implementation of FunctionalTypePROCESS_VPG_6E01 class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypePROCESS_VPG_6E01.h"


#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypePROCESS_VPG_6E01::FunctionalTypePROCESS_VPG_6E01(int type, const char *name, const char *description) :
	FunctionalTypePROCESS(type, name, description)
{
}

QString FunctionalTypePROCESS_VPG_6E01::getStepResourceBase(void)
{
	return QString("VPG_6E01.Step_");
}
