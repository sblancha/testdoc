#ifndef FUNCTIONALTYPEVRE_H
#define FUNCTIONALTYPEVRE_H

// Special functional type for bakeout racks - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVRE : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Connected = 1,
		BakeoutInProgress = 2,
		BakeoutStopped = 3,
		BakeoutError = 4,
		BakeoutWarning = 5
	};

	FunctionalTypeVRE(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVRE() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVRE_H


