//	Implementation of MobilePool class
/////////////////////////////////////////////////////////////////////////////////

#include "MobilePool.h"

#include "DataPool.h"
#include "Eqp.h"
#include "EqpVBAKEOUT.h"
#include "MobileType.h"

MobilePool	MobilePool::instance;

MobilePool &MobilePool::getInstance(void) { return instance; }

/*
**	FUNCTION
**		Calculate DPE name for bakeout channel
**
**	ARGUMENTS
**		pEqp	- Pointer to bakeout rack equipment
**		channel	- channel number [1...24]
**		setT	- Flag indicating if temperature setting DPE is required
**
**	RETURNS
**		Pointer to DPE name
**
**	CAUTIONS
**		None
*/
const QString &MobilePool::findBakeoutDpeName(Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType valueType)
{
	static QString result;
	result = QString("%1.%2").arg(pEqp->getDpName()).arg(findBakeoutChannelDpeName(channel, valueType));
	return result;
}

const QString &MobilePool::findBakeoutChannelDpeName(int channel, enum BakeoutWorkDp::ChannelValueType valueType) {
	static QString result;
	if (channel <= 24) {
		result = QString("CH%1.").arg(channel, 2, 10, QLatin1Char('0'));
	}
	else {
		result = QString("CtlCH%1.").arg(channel - 24, 2, 10, QLatin1Char('0'));
	}
	switch (valueType) {
	case BakeoutWorkDp::ChannelValueTypeT:
		result += "T";
		break;
	case BakeoutWorkDp::ChannelValueTypeSetT:
		result += "SET";
		break;
	case BakeoutWorkDp::ChannelValueTypeStatus:
		if (channel <= 24) {
			result += "STATE";
		}
		else {
			result += "Status";
		}
		break;
	default:	// Just to make Linux compiler happy
		break;
	}
	return result;

}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

MobilePool::MobilePool()
{
	pBakeoutList = new QList<BakeoutWorkDp *>();
	pBakeoutDict = new QHash<QByteArray, BakeoutWorkDp *>();
	pActiveListOnline = new QList<Eqp *>();
	pActiveListHistory = new QList<Eqp *>();
	pTmpListOnline = new QList<Eqp *>();
	pTmpListHistory = new QList<Eqp *>();
}

MobilePool::~MobilePool()
{
	delete pTmpListHistory;
	delete pTmpListOnline;
	delete pActiveListHistory;
	delete pActiveListOnline;
	delete pBakeoutDict;	// Added 12.01.2012 L.Kopylov
	while(!pBakeoutList->isEmpty())
	{
		delete pBakeoutList->takeFirst();
	}
	delete pBakeoutList;
}

/*
**	FUNCTION
**		Add bakeout container to data pool
**
**	ARGUMENTS
**		dpName	- Container's DP name
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobilePool::addBakeoutWorkDp(const char *dpName)
{
	if(pBakeoutDict->value(dpName))
	{
		return;
	}
	BakeoutWorkDp *pItem = new BakeoutWorkDp(dpName);
	pBakeoutList->append(pItem);
	pBakeoutDict->insert(dpName, pItem);
}

/*
**	FUNCTION
**		Start collecting active equipment list for online mode
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobilePool::startOnlineEqp(void)
{
	pTmpListOnline->clear();
}

/*
**	FUNCTION
**		Add one device active in online mode
**
**	ARGUMENTS
**		dpName			- DP name of device
**		workDpName		- Name of DP where device is activated
**		sourceDpName	- Name of source (VMOB) DP
**		active			- Flag indicating if pair dpName + workDpName is active or not
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobilePool::addOnlineEqp(const char *dpName, const char *workDpName,
	const char *sourceDpName, bool active)
{
	DataPool &pool = DataPool::getInstance();
	Eqp	*pEqp = pool.findEqpByDpName(dpName);
	if(!pEqp)
	{
		return;
	}
	if(pEqp->getMobileType() == MobileType::OnSector)	// BAKEOUT
	{
		BakeoutWorkDp *pWorkDp = pBakeoutDict->value(workDpName);
		if(!pWorkDp)
		{
			return;
		}
		Eqp *pSourceEqp = NULL;
		if(active)
		{
			pSourceEqp = pool.findEqpByDpName(sourceDpName);
		}
		pWorkDp->setEqp(active ? pEqp : NULL, pSourceEqp);
		if(pEqp->inherits("EqpVBAKEOUT"))
		{
			((EqpVBAKEOUT *)pEqp)->setWorkDp(active ? pWorkDp : NULL);
		}
	}
	if(active)
	{
		if(!pTmpListOnline->contains(pEqp))
		{
			pTmpListOnline->append(pEqp);
		}
	}
}

/*
**	FUNCTION
**		Finish collecting active equipment in online mode.
**		Notify all listeners if list of active equipment has been changed.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobilePool::finishOnlineEqp(void)
{
	bool contentChanged = (*pTmpListOnline) != (*pActiveListOnline);
	if(!contentChanged)
	{
		pTmpListOnline->clear();
		return;
	}
	for(int idx = 0 ; idx < pActiveListOnline->count() ; idx++)
	{
		Eqp *pEqp = pActiveListOnline->at(idx);
		if(!pTmpListOnline->contains(pEqp))
		{
			if(pEqp->inherits("EqpVBAKEOUT"))
			{
				((EqpVBAKEOUT *)pEqp)->setWorkDp(NULL);
			}
		}
	}
	*pActiveListOnline = *pTmpListOnline;
	pTmpListOnline->clear();
	emit activeListChanged(DataEnum::Online);
}

/*
**	FUNCTION
**		Start collecting active equipment list for online mode
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobilePool::startHistoryEqp(void)
{
	pTmpListHistory->clear();
}

/*
**	FUNCTION
**		Add one device active in history mode
**
**	ARGUMENTS
**		dpName		- DP name of device
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobilePool::addHistoryEqp(const char *dpName)
{
	DataPool &pool = DataPool::getInstance();
	Eqp	*pEqp = pool.findEqpByDpName(dpName);
	if(!pEqp)
	{
		return;
	}
	if(!pTmpListHistory->contains(pEqp))
	{
		pTmpListHistory->append(pEqp);
	}
}

/*
**	FUNCTION
**		Finish collecting active equipment in history mode.
**		Notify all listeners if list of active equipment has been changed.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobilePool::finishHistoryEqp(void)
{
	bool contentChanged = (*pTmpListHistory) != (*pActiveListHistory);
	if(!contentChanged)
	{
		pTmpListHistory->clear();
		return;
	}
	(*pActiveListHistory) = (*pTmpListHistory);
	pTmpListHistory->clear();
	emit activeListChanged(DataEnum::Replay);
}

/*
**	FUNCTION
**		Connect to DPE of bakeout channel IF that bakeout rack is active now
**
**	ARGUMENTS
**		pReceiver	- Pointer to receiver that shall be connected
**		pEqp		- Pointer to bakeout rack equipment
**		channel		- channel number [1...24]
**		setT		- Flag indicating if temperature setting DPE is required
**
**	RETURNS
**		true	- if bakeout rack is active;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool MobilePool::connectValue(InterfaceEqp *pReceiver, Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType valueType)
{
	mutex.lock();
	BakeoutWorkDp *pWorkDp = findWorkDpForEqp(pEqp);
	if(pWorkDp)
	{
		pWorkDp->connectDpeSignal(pReceiver, channel, valueType);
	}
	mutex.unlock();
	return pWorkDp != NULL;
}

/*
**	FUNCTION
**		Connect to all DPEs of bakeout channel IF that bakeout rack is active now
**
**	ARGUMENTS
**		pReceiver	- Pointer to receiver that shall be connected
**		pEqp		- Pointer to bakeout rack equipment
**		channel		- channel number [1...24]
**
**	RETURNS
**		true	- if bakeout rack is active;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool MobilePool::connectChannel(InterfaceEqp *pReceiver, Eqp *pEqp, int channel)
{
	mutex.lock();
	BakeoutWorkDp *pWorkDp = findWorkDpForEqp(pEqp);
	if(pWorkDp)
	{
		pWorkDp->connectChannel(pReceiver, channel);
	}
	mutex.unlock();
	return pWorkDp != NULL;
}

bool MobilePool::connectCtlChannel(InterfaceEqp *pReceiver, Eqp *pEqp, int channel)
{
	//qDebug("MobilePool::connectCtlChannel(%s, %d)\n", pEqp->getDpName(), channel);
	mutex.lock();
	BakeoutWorkDp *pWorkDp = findWorkDpForEqp(pEqp);
	if(pWorkDp)
	{
		//qDebug("MobilePool::connectCtlChannel(%s, %d): work DP found\n", pEqp->getDpName(), channel);
		pWorkDp->connectCtlChannel(pReceiver, channel);
	}
	mutex.unlock();
	return pWorkDp != NULL;
}


/*
**	FUNCTION
**		Connect to RR1 DPE of bakeout rack IF that bakeout rack is active now
**
**	ARGUMENTS
**		pReceiver	- Pointer to receiver that shall be connected
**		pEqp		- Pointer to bakeout rack equipment
**
**	RETURNS
**		true	- if bakeout rack is active;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool MobilePool::connectRr1(InterfaceEqp *pReceiver, Eqp *pEqp)
{
	mutex.lock();
	BakeoutWorkDp *pWorkDp = findWorkDpForEqp(pEqp);
	if(pWorkDp)
	{
		pWorkDp->connectRr1(pReceiver);
	}
	mutex.unlock();
	return pWorkDp != NULL;
}

/*
**	FUNCTION
**		Disconnect from DPE of bakeout channel
**
**	ARGUMENTS
**		pReceiver	- Pointer to receiver that shall be connected
**		pEqp		- Pointer to bakeout rack equipment
**		channel		- channel number [1...24]
**		setT		- Flag indicating if temperature setting DPE is required
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobilePool::disconnectValue(InterfaceEqp *pReceiver, Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType valueType)
{
	mutex.lock();
	BakeoutWorkDp *pWorkDp = findWorkDpForEqp(pEqp);
	if(pWorkDp)
	{
		pWorkDp->disconnectDpeSignal(pReceiver, channel, valueType);
	}
	mutex.unlock();
}

/*
**	FUNCTION
**		Disconnect from all DPEs of bakeout channel
**
**	ARGUMENTS
**		pReceiver	- Pointer to receiver that shall be connected
**		pEqp		- Pointer to bakeout rack equipment
**		channel		- channel number [1...24]
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobilePool::disconnectChannel(InterfaceEqp *pReceiver, Eqp *pEqp, int channel)
{
	mutex.lock();
	BakeoutWorkDp *pWorkDp = findWorkDpForEqp(pEqp);
	if(pWorkDp)
	{
		pWorkDp->disconnectChannel(pReceiver, channel);
	}
	mutex.unlock();
}

void MobilePool::disconnectCtlChannel(InterfaceEqp *pReceiver, Eqp *pEqp, int channel)
{
	mutex.lock();
	BakeoutWorkDp *pWorkDp = findWorkDpForEqp(pEqp);
	if(pWorkDp)
	{
		pWorkDp->disconnectCtlChannel(pReceiver, channel);
	}
	mutex.unlock();
}

/*
**	FUNCTION
**		Disconnect from RR1 DPE of bakeout rack
**
**	ARGUMENTS
**		pReceiver	- Pointer to receiver that shall be connected
**		pEqp		- Pointer to bakeout rack equipment
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobilePool::disconnectRr1(InterfaceEqp *pReceiver, Eqp *pEqp)
{
	mutex.lock();
	BakeoutWorkDp *pWorkDp = findWorkDpForEqp(pEqp);
	if(pWorkDp)
	{
		pWorkDp->disconnectRr1(pReceiver);
	}
	mutex.unlock();
}

/*
**	FUNCTION
**		Process new online value for work DP
**
**	ARGUMENTS
**		dpName		- Work DP name
**		dpeName		- DPE of work DP
**		value		- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobilePool::newOnlineValue(const char *dpName, const char *dpeName, const QVariant &value, const QDateTime &timeStamp)
{
	mutex.lock();
	BakeoutWorkDp *pWorkDp = pBakeoutDict->value(dpName);
	if(pWorkDp)
	{
		pWorkDp->newOnlineValue(dpeName, value, timeStamp);
	}
	mutex.unlock();
}

/*
**	FUNCTION
**		Return last known DPE analog value of given bakeout rack channel
**
**	ARGUMENTS
**		pEqp		- Pointer to bakeout rack equipment
**		channel		- channel number [1...24]
**		setT		- Flag indicating if temperature setting DPE is required
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
const QVariant &MobilePool::getDpeValue(Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType valueType)
{
	BakeoutWorkDp *pWorkDp = findWorkDpForEqp(pEqp);
	if(pWorkDp)
	{
		return pWorkDp->getDpeValue(channel, valueType);
	}
	static QVariant dummy;
	return dummy;
}

/*
**	FUNCTION
**		Return last known DPE value of given bakeout rack
**
**	ARGUMENTS
**		pEqp		- Pointer to bakeout rack equipment
**		dpeName		- DPE name
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
const QVariant &MobilePool::getDpeValue(Eqp *pEqp, const char *dpeName, QDateTime &timeStamp)
{
	BakeoutWorkDp *pWorkDp = findWorkDpForEqp(pEqp);
	if(pWorkDp)
	{
		return pWorkDp->getDpeValue(dpeName, timeStamp);
	}
	static QVariant dummy;
	return dummy;
}

/*
**	FUNCTION
**		Return last known PLC alarm value of given bakeout rack
**
**	ARGUMENTS
**		pEqp		- Pointer to bakeout rack equipment
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
bool MobilePool::isPlcAlarm(Eqp *pEqp)
{
	BakeoutWorkDp *pWorkDp = findWorkDpForEqp(pEqp);
	if(pWorkDp)
	{
		return pWorkDp->isPlcAlarm();
	}
	return true;	// ????????????
}

/*
**	FUNCTION
**		Find bakeout work DP that is used for given bakeout equipment
**
**	ARGUMENTS
**		dpName	- Container's DP name
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
BakeoutWorkDp *MobilePool::findWorkDpForEqp(Eqp *pEqp)
{
	for(int idx = 0 ; idx < pBakeoutList->count() ; idx++)
	{
		BakeoutWorkDp *pWorkDp = pBakeoutList->at(idx);
		if(pWorkDp->getEqp() == pEqp)
		{
			return pWorkDp;
		}
	}
	return NULL;
}

