//	Implementation of VacIconVV_PLUS_PP class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVV_PLUS_PP.h"

#include "EqpVV.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OPEN				0,255,0
#define COLOR_CLOSED			255,0,0
#define	COLOR_ERROR				255,102,255

// Colors for control and interlocks
#define COLOR_GREEN				0,255,0
#define COLOR_YELLOW			255,255,0
#define COLOR_RED				255,0,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_OPEN		(0x00020000)
#define	RR1_CLOSED		(0x00010000)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVV_PLUS_PP::VacIconVV_PLUS_PP(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(19, 41);
	setAreasGeometry();
}

VacIconVV_PLUS_PP::~VacIconVV_PLUS_PP()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for symbol areas rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVV_PLUS_PP::setAreasGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		vvr.setRect(1, 2, 19, 19);
		pump.setRect(2, 23, 17, 17);
		break;
	case VacIconContainer::Down:
		vvr.setRect(1, 20, 19, 19);
		pump.setRect(2, 1, 17, 17);
		break;
	case VacIconContainer::Left:
		vvr.setRect(20, 1, 19, 19);
		pump.setRect(1, 2, 17, 17);
		break;
	case VacIconContainer::Right:
		vvr.setRect(1, 1, 19, 19);
		pump.setRect(23, 2, 17, 17);
		break;
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVV_PLUS_PP::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		return VacIconGeometry(21, 41, 10, 42, -1);
		break;
	case VacIconContainer::Down:
		return VacIconGeometry(21, 41, 10, 42, -1);
		break;
	default:
		break;
	}
	return VacIconGeometry(41, 21, 42, 10, -1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVV_PLUS_PP::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(21);
		size.setHeight(41);
		break;
	default:
		size.setWidth(41);
		size.setHeight(21);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreasGeometry();
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVV_PLUS_PP::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVV"), "VacIconVV_PLUS_PP::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVV *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVV_PLUS_PP::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVV_PLUS_PP::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVV_PLUS_PP::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}


/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVV_PLUS_PP::getToolTip(const QPoint &pos, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		if(vvr.contains(pos))
		{
			pEqp->getToolTipString(text, mode);
			rect = vvr;
		}
		else if(pump.contains(pos))
		{
			text = pEqp->getVisibleName();
			text += " pump is not controllable";
			rect = pump;
		}
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVV_PLUS_PP::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	drawPipes(painter);
	drawVvr(painter, ctlStatus, rr1, plcAlarm);
	drawPump(painter);
}

void VacIconVV_PLUS_PP::drawPipes(QPainter &painter)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		// Line from VVR to outside
		painter.fillRect(vvr.center().x() - 1, 0, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, vvr.bottom() + 1,
			3, pump.top() - vvr.bottom() + 1, pipeColor);
		break;
	case VacIconContainer::Down:
		// Line from VVR to outside
		painter.fillRect(vvr.center().x() - 1, vvr.bottom() - 1, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, pump.bottom(),
			3, vvr.top() - pump.bottom(), pipeColor);
		// Connect VGR to connection line of pump
		break;
	case VacIconContainer::Left:
		// Line from VVR to outside
		painter.fillRect(vvr.right() - 1, vvr.center().y() - 1, 3, 4, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.right(), pump.center().y() - 1,
			vvr.left() - pump.right(), 3, pipeColor);
		break;
	case VacIconContainer::Right:
		// Line from VVR to outside
		painter.fillRect(0, vvr.center().y() - 1, 4, 3, pipeColor);
		// Connect pump to valves
		painter.fillRect(vvr.right(), pump.center().y() - 1,
			pump.left() - vvr.right(), 3, pipeColor);
		break;
	}
}


void VacIconVV_PLUS_PP::drawVvr(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	// Save painter coordinate system - interlocks will be drawn
	// in original (non-translated) coordinate system
	painter.save();

	// Prepare painter's coordinate system for image drawing
	// Coordinate system (0,0) is center of valve symbol
	painter.translate(vvr.center().x(), vvr.center().y());

	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	case VacIconContainer::Down:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Left:	// No rotation
		break;
	case VacIconContainer::Right:	// 180 degrees
		painter.rotate(180);
		break;
	}

	// Draw valve image
	setPainterForControl(painter, ctlStatus, rr1, plcAlarm);

	// 1) Rectangle connecting big rectangle on top with valve image
	QPolygon points(4);
	points.setPoint(0, -2, -6);
	points.setPoint(1, 2, -6);
	points.setPoint(2, 2, 0);
	points.setPoint(3, -2, 0);
	painter.drawPolygon(points);

	// 2) big rectangle on top
	points.setPoint(0, -4, -9);
	points.setPoint(1, 4, -9);
	points.setPoint(2, 4, -6);
	points.setPoint(3, -4, -6);
	painter.drawPolygon(points);

	// 3) Valve image
	setPainterForMainImage(painter, ctlStatus, rr1, plcAlarm);

	points.setPoint(0, -9, -9);
	points.setPoint(1, 9, 9);
	points.setPoint(2, 9, -9);
	points.setPoint(3, -9, 9);
	painter.drawPolygon(points);

	// Restore coordinate system to original
	painter.restore();
}

void VacIconVV_PLUS_PP::drawPump(QPainter &painter)
{
	QPen pen(Qt::black, 2);
	painter.setPen(pen);
	painter.setBrush(Qt::NoBrush);
	painter.drawArc(pump.left(), pump.top(), pump.width(), pump.height(), 0, 360 * 16);
	QPoint center = pump.center();
	switch(direction)
	{
	case VacIconContainer::Up:
		painter.drawLine(center.x() - 5, center.y() - 4, center.x() - 2, center.y() + 7);
		painter.drawLine(center.x() + 5, center.y() - 5, center.x() + 2, center.y() + 7);
		break;
	case VacIconContainer::Down:
		painter.drawLine(center.x() - 2, center.y() - 7, center.x() - 5, center.y() + 4);
		painter.drawLine(center.x() + 2, center.y() - 7, center.x() + 5, center.y() + 5);
		break;
	case VacIconContainer::Left:
		painter.drawLine(center.x() - 7, center.y() - 2, center.x() + 5, center.y() - 5);
		painter.drawLine(center.x() - 7, center.y() + 2, center.x() + 5, center.y() + 5);
		break;
	case VacIconContainer::Right:
		painter.drawLine(center.x() - 4, center.y() - 5, center.x() + 7, center.y() - 2);
		painter.drawLine(center.x() - 4, center.y() + 5, center.x() + 7, center.y() + 2);
		break;
	}
}


void VacIconVV_PLUS_PP::setPainterForControl(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	QColor color(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr1 & RR1_ERROR)
				{
					color.setRgb(COLOR_RED);
				}
				else if(rr1 & RR1_WARNING)
				{
					color.setRgb(COLOR_YELLOW);
				}
				else
				{
					color.setRgb(COLOR_GREEN);
				}
			}
		}
		break;
	}
	painter.setBrush(color);
	painter.setPen(Qt::black);
}

void VacIconVV_PLUS_PP::setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	QColor color(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr1 & RR1_OPEN)
				{
					if(rr1 & RR1_CLOSED)
					{
						color.setRgb(COLOR_ERROR);
					}
					else
					{
						color.setRgb(COLOR_OPEN);
					}
				}
				else if(rr1 & RR1_CLOSED)
				{
					color.setRgb(COLOR_CLOSED);
				}
				else
				{
					color.setRgb(COLOR_UNDEFINED);
				}
			}
		}
		break;
	}
	QBrush brush(color);
	painter.setBrush(brush);
	painter.setPen(Qt::black);
}
