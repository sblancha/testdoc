//	Implementation of EqpSMSMachineMode class
////////////////////////////////////////////////////////////////////////////////

#include "EqpSMSMachineMode.h"

#include "DataPool.h"
#include "FunctionalType.h"
#include "MobileType.h"
#include "EqpMsgCriteria.h"
#include "DpConnection.h"

#include "ResourcePool.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

EqpSMSMachineMode::EqpSMSMachineMode(const char *dpName, const char *visibleName, const char *dpeName)
{
	this->dpName = DataPool::getInstance().addString(dpName);
	name = DataPool::getInstance().addString(visibleName);
	this->dpeName = dpeName;
	mobileType = MobileType::Fixed;
	functionalType = FunctionalType::SMS_MACHINE_MODE;
	modeOnline = 0;
}

EqpSMSMachineMode::~EqpSMSMachineMode()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to analog value of device. The list is used
**		to build value history
**
**	ARGUMENTS
**		valueDpes	- Variable where all value DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSMSMachineMode::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	QString result(dpName);
	result += ".";
	result += dpeName;
	valueDpes.append(dpeName);
}


/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSMSMachineMode::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, dpeName, mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSMSMachineMode::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, dpeName, mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSMSMachineMode::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, dpeName.constData()))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(modeOnline == newValue)
			{
				return;
			}
			modeOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS - this device does not participate in replay
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSMSMachineMode::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Return main value for device - mode
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpSMSMachineMode::getMainValue(DataEnum::DataMode /* mode */)
{
	return modeOnline;
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSMSMachineMode::getMainColor(QColor &color, DataEnum::DataMode /* mode */)
{
	color.setRgb(255, 255, 255);	// Neutral white
}

void EqpSMSMachineMode::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	getMainValueString(string, mode);
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSMSMachineMode::getMainValueString(QString &string, DataEnum::DataMode /* mode */)
{
	getMainValueString(modeOnline, string);
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		intensity	- Value of Intensity DPE
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSMSMachineMode::getMainValueString(unsigned modeValue, QString &string)
{
	QString resourceName;
	for(int bit = 0 ; bit < 32 ; bit++)
	{
		if(modeValue == (unsigned)(1 << bit))
		{
			resourceName = "MachineMode";
			resourceName += QString::number(bit);
			if(ResourcePool::getInstance().getStringValue(resourceName, string) == ResourcePool::OK)
			{
				return;
			}
			break;
		}
	}
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "??? %d", modeValue);
#else
	sprintf(buf, "??? %d", modeValue);
#endif
	string = buf;
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSMSMachineMode::getMainStateStringForMenu(QString &string, DataEnum::DataMode /* mode */)
{
	getMainStateStringForMenu(modeOnline, string);
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		intensity	- Value of Intensity DPE
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSMSMachineMode::getMainStateStringForMenu(unsigned modeValue, QString &string)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainValueString(modeValue, valueString);
	string += valueString;
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSMSMachineMode::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	getMainStateStringForMenu(string, mode);
}
#endif




/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSMSMachineMode::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	addDpeNameToList(dpes, dpeName.constData());
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history)
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpSMSMachineMode::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}
	return 0;
}
#endif


void EqpSMSMachineMode::convertValueToString(float value, QString &result, EqpPart /* part */)
{
	getMainValueString((unsigned)rint(value), result);
}


/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSMSMachineMode::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// State is valid - decide based on criteria
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainValue:	// min-max values == range of steps
		{
			unsigned minMode = (unsigned)rint(pCriteria->getLowerLimit());
			if(modeOnline == minMode)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), true, modeOnline);
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), true, modeOnline);
			}
			return;
		}
		break;
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}

