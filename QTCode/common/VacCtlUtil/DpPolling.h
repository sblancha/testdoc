#ifndef DPPOLLING_H
#define DPPOLLING_H

#include "VacCtlUtilExport.h"

// Class buffering DP polling requests to be sent to PVSS

#include <qobject.h>
#include <qlist.h>
#include <qmutex.h>

class DpPollingItem
{
public:
	DpPollingItem(const char *dpeIn, bool toConnect) : dpe(dpeIn)
	{
		this->toConnect = toConnect;
	}
	~DpPollingItem() {}

	// Access
	inline const QByteArray &getDpe(void) const { return dpe; };
	inline bool isToConnect(void) const { return toConnect; }

private:

	// DPE name
	QByteArray dpe;

	// Flag indicating if DPE shall be connected
	bool	toConnect;
};

class VACCTLUTIL_EXPORT DpPolling : public QObject
{
	Q_OBJECT

public:
	static bool connect(const char *dp, const char *dpe);
	static bool disconnect(const char *dp, const char *dpe);
	static DpPolling &getInstance(void);

	DpPolling();
	DpPolling(const DpPolling &source);
	~DpPolling();
	DpPolling & operator = (const DpPolling &other);

	bool getNext(QByteArray &dpe, bool &connect);

signals:
	void changed(void);

protected:

	bool connect(const QByteArray &dpe);
	bool disconnect(const QByteArray &dpe);

	// The only instance of this class
	static DpPolling	instance;

	// List of DPEs pending connection
	QList<DpPollingItem *> *pDpes;

	// Mutex to protect list from parallel access
	QMutex	mutex;

	// Flag indicating if signal on content change has been sent
	bool	signalSent;
};
 
#endif	// DPPOLLING_H
