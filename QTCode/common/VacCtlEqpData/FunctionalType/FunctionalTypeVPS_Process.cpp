//	Implementation of FunctionalTypeVPS_Process class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVPS_Process.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeVPS_Process::FunctionalTypeVPS_Process(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVPS_Process::fillCriteria(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "VPS Process is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPS Process is OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPS Process is not OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPS Process is OFF in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPS Process is not OFF in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "VPS Process is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPS Process is ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPS Process is not ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPS Process is ON in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPS Process is not ON in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "VPS Process ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VPS Process in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VPS Process in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VPS Process in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VPS Process in main part {mainPart}: no error");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeVPS_Process::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case Off:
			result = "OFF";
			break;
		case On:
			result = "ON";
			break;
		default:
			break;
		}
		break;
	case EqpMsgCriteria::MainValue:
		result = "PR";
		break;
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeVPS_Process::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "OFF"))
		{
			result = Off;
		}
		else if(!strcasecmp(name, "ON"))
		{
			result = On;
		}
		break;
	case EqpMsgCriteria::MainValue:
		if(!strcasecmp(name, "PR"))
		{
			return 0;
		}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}
