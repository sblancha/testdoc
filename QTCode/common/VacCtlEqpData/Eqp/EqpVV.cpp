//	Implementation of EqpVV class
////////////////////////////////////////////////////////////////////////////////////

#include "EqpVV.h"
#include "DataPool.h"
#include "FunctionalTypeVV.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OPEN				0,255,0
#define	COLOR_OPEN_ERROR		204,255,0
#define	COLOR_OPEN_WARNING		153,255,0
#define COLOR_CLOSED			255,0,0
#define COLOR_CLOSED_ERROR		255,0,0
#define	COLOR_CLOSED_WARNING	255,0,0
#define	COLOR_ERROR				255,102,255
#define	COLOR_WARNING			255,255,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_OPEN		(0x00020000)
#define	RR1_CLOSED		(0x00010000)
#define RR1_AL3			(0x00008000)
#define RR1_AL2			(0x00004000)
#define RR1_AL1			(0x00002000)
#define RR1_ERROR_CODE_MASK	(0x000000FF)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVV::EqpVV(const Eqp &source) : Eqp(source)
{
	rr1Online = rr2Online = rr1Replay = rr2Replay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::VV;

	// Decide if this device has RR2 and timestamp DPEs
	haveRr2 = haveTs1 = haveTs2 = haveEqpStatus = true;
	switch(ctrlFamily)
	{
	case 1:	// Valves
		switch(ctrlType)
		{
		case 1:	// SVCU-controlled for SPS
			switch(ctrlSubType)
			{
			case 3:		// VVF_S
			case 259:	// VVF_S read only
				haveRr2 = false;
				haveTs2 = false;
				break;
			}
			break;
		case 2:	// VLV_STD
			haveRr2 = false;
			haveTs1 = haveTs2 = false;
			break;
		case 4:	// VLV_ANA
			haveTs1 = haveTs2 = false;
			break;
		case 6:	// VV_PULSED
			haveRr2 = haveTs1 = haveTs2 = false;
			break;
		case 7:	// VV_STD_IO
			haveRr2 = haveTs1 = haveTs2 = false;
			break;
		case 8:	// VV_AO
			haveRr2 = haveTs1 = haveTs2 = false;
			break;
		case 12:
			switch (ctrlSubType)
			{
			case 2: //VVF_S_DP
				haveRr2 = false;
				haveTs2 = false;
				break;
			}
			break;
		case 32:	// Gate valves in VELO (VVG_VELO)
		case 33:	// Process valves in VELO (VVP_VELO)
		case 34:	// Safety valves in VELO (VVS_VELO)
			haveRr2 = false;
			haveTs2 = false;
			break;
		}
		break;
	default:	// Not real valve VVS_PSB_SUM
		haveTs1 = haveTs2 = haveEqpStatus = false;
		break;
	}
}

EqpVV::~EqpVV()
{
}

/*
**	FUNCTION
**		Post-processing after all data in pool are ready.
**		Find interlock thermometers for valve in LHC. Thermometers
**		are not needed for this device - the purpose is to mark thermometers
**		as interlock sources.
**		L.Kopylov 22.10.2013 add processing of interlock sources (VGP/VPI)
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV::postProcess(void)
{
	if((ctrlFamily == 1) || (ctrlType == 3))	// VVS_LHC
	{
		QStringList	thermoList;
		QString		errMsg;
		DataPool::getInstance().findThermometersForValve(dpName, thermoList, errMsg);
	}
	// 'normal' interlock sources
	processInterlockSource("GaugeBefore", false);
	processInterlockSource("PumpBefore", false);
	processInterlockSource("GaugeAfter", true);
	processInterlockSource("PumpAfter", true);
	for(int idx = 1 ; idx <= 8 ; idx++)
	{
		QByteArray attrName("PrevInterlockSource");
		attrName += QString::number(idx);
		processInterlockSource(attrName, false);

		attrName = "NextInterlockSource";
		attrName += QString::number(idx);
		processInterlockSource(attrName, true);
	}
}

void EqpVV::processInterlockSource(const char *attrName, bool isAfter)
{
	QString sourceDp = getAttrValue(attrName);
	if(sourceDp.isEmpty())
	{
		return;
	}
	DataPool &pool = DataPool::getInstance();
	Eqp *pSourceEqp = pool.findEqpByDpName(sourceDp.toLatin1());
	if(pSourceEqp)
	{
		if(pSourceEqp->getFunctionalType() != FunctionalType::VRPI)
		{
			if(isAfter)
			{
				pSourceEqp->setIntlSourceNext(true);
			}
			else
			{
				pSourceEqp->setIntlSourcePrev(true);
			}
			return;
		}
	}
	// We are here - only if source if VRPI
	const QHash<QByteArray, Eqp *> &eqpDict = pool.getEqpDict();
	foreach(Eqp *pEqp , eqpDict)
	{
		if(pEqp->getFunctionalType() != FunctionalType::VPI)
		{
			continue;
		}
		QString masterName = pEqp->getAttrValue("MasterName");
		if(!masterName.isEmpty())
		{
			bool matches = false;
			if(pSourceEqp)
			{
				matches = masterName == sourceDp;
			}
			else
			{
				matches = masterName.startsWith(sourceDp);
			}
			if(matches)
			{
				if(isAfter)
				{
					pEqp->setIntlSourceNext(true);
				}
				else
				{
					pEqp->setIntlSourcePrev(true);
				}
			}
		}
	}
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	if(haveEqpStatus)
	{
		addDpeNameToList(stateDpes, "EqpStatus");
	}
	addDpeNameToList(stateDpes, "RR1");
	if(haveRr2)
	{
		addDpeNameToList(stateDpes, "RR2");
	}
	if(haveTs1)
	{
		addDpeNameToList(stateDpes, "TS1.YMDH");
		addDpeNameToList(stateDpes, "TS1.MSM");
	}
	if(haveTs2)
	{
		addDpeNameToList(stateDpes, "TS2.YMDH");
		addDpeNameToList(stateDpes, "TS2.MSM");
	}
}

/*
**	FUNCTION
**		Find name of PLC, controlling this device. VV overrides method of Eqp
**		because algorithm for VVS_PSB_SUM is more complex than default method
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		DP name of PLC; or
**		NULL if PLC is not found
**
**	CAUTIONS
**		None
*/
const QString EqpVV::findPlcName(void)
{
	if((ctrlFamily == 254) && (ctrlType == 7))	// VVS_PSB_SUM
	{
		DataPool &pool = DataPool::getInstance();
		for(int n = 1 ; n <= 4 ; n++)
		{
			char attrName[32];
#ifdef Q_OS_WIN
			sprintf_s(attrName, sizeof(attrName) / sizeof(attrName[0]), "RealValve%d", n);
#else
			sprintf(attrName, "RealValve%d", n);
#endif
			const QString attrValue = getAttrValue(attrName);
			if(!attrValue.isEmpty())
			{
				Eqp *pEqp = pool.findEqpByDpName(attrValue.toLatin1());
				if(pEqp)
				{
					const QString plcName = pEqp->findPlcName();
					if(!plcName.isEmpty())
					{
						return plcName;
					}
				}
			}
		}
		return NULL;
	}
	return Eqp::findPlcName();
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	if(haveEqpStatus)
	{
		connectDpe(pDst, "EqpStatus", mode);
	}
	connectDpe(pDst, "RR1", mode);
	if(haveRr2)
	{
		connectDpe(pDst, "RR2", mode);
	}
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	if(haveRr2)
	{
		disconnectDpe(pDst, "RR2", mode);
	}
	disconnectDpe(pDst, "RR1", mode);
	if(haveEqpStatus)
	{
		disconnectDpe(pDst, "EqpStatus", mode);
	}
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Online == newValue)
			{
				return;
			}
			rr2Online = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Replay == newValue)
			{
				return;
			}
			rr2Replay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Check if state of this device shall be reflected as color on
**		main view of PVSS application. All valves shall be reflected on
**		main view EXCEPT for single ring valves in PSB - for them summary
**		valves on PSB are used
**		Addition 12.07.2011: VV_PULSED also shall not influence color.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if device shall be presented on main view;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool EqpVV::isSpecColor(void)
{
	bool result = true;
	if(((ctrlType == 5) && (ctrlSubType == 2)) || ((ctrlType == 12) && (ctrlSubType == 5)))
	{
		result = false;	// single ring valve in PSB (DP included)
	}
	else if(ctrlType == 6)
	{
		result = false;	// VV_PULSED
	}
	return result;
}

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, rr2Replay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, rr2Online, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV::getMainColor(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(rr1 & RR1_ERROR)	// Still different colors for open/closed
	{
		if(rr1 & RR1_OPEN)
		{
			if(rr1 & RR1_CLOSED)
			{
				color.setRgb(COLOR_ERROR);
			}
			else
			{
				color.setRgb(COLOR_OPEN_ERROR);
			}
		}
		else if(rr1 & RR1_CLOSED)
		{
			color.setRgb(COLOR_CLOSED_ERROR);
		}
		else
		{
			color.setRgb(COLOR_ERROR);
		}
	}
	// No errors - check open/closed state
	else if((rr1 & RR1_OPEN) && (rr1 & RR1_CLOSED))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_WARNING)
	{
		if(rr1 & RR1_OPEN)
		{
			if(rr1 & RR1_CLOSED)
			{
				color.setRgb(COLOR_WARNING);
			}
			else
			{
				color.setRgb(COLOR_OPEN_WARNING);
			}
		}
		else if(rr1 & RR1_CLOSED)
		{
			color.setRgb(COLOR_CLOSED_WARNING);
		}
		else
		{
			color.setRgb(COLOR_WARNING);
		}
	}
	else if(rr1 & RR1_OPEN)
	{
		color.setRgb(COLOR_OPEN);
	}
	else if(rr1 & RR1_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate draw order for device. Method is only used for drawing
**		main views (LHC/SPS) => only online data are checked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Draw order, items with higher order are drawn on top of items
**		with low order
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVV::getDrawOrder(void)
{
	int result = 1;
	if(!plcAlarmOnline)
	{
		if(rr1Online & RR1_VALID)
		{
			if(rr1Online & RR1_CLOSED)
			{
				result = 2;
			}
			else if(rr1Online & RR1_OPEN)
			{
				result = 0;
			}
		}
	}
	return result;
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, rr2Replay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, rr2Online, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV::getMainStateString(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
		return;
	}
	if(rr1 & RR1_ERROR)	// Still different strings for open/closed
	{
		if(rr1 & RR1_OPEN)
		{
			if(rr1 & RR1_CLOSED)
			{
				string = "Error";
			}
			else
			{
				string = "Open (error)";
			}
		}
		else if(rr1 & RR1_CLOSED)
		{
			string = "Closed (error)";
		}
		else
		{
			string = "Error";
		}
	}
	// No errors - check open/closed state
	else if((rr1 & RR1_OPEN) && (rr1 & RR1_CLOSED))
	{
		string = "Error";
	}
	else if(rr1 & RR1_WARNING)
	{
		if(rr1 & RR1_OPEN)
		{
			if(rr1 & RR1_CLOSED)
			{
				string = "Error";
			}
			else
			{
				string = "Open (warning)";
			}
		}
		else if(rr1 & RR1_CLOSED)
		{
			string = "Closed (warning)";
		}
		else
		{
			string = "Undefined (warning)";
		}
	}
	else if(rr1 & RR1_OPEN)
	{
		string = "Open";
	}
	else if(rr1 & RR1_CLOSED)
	{
		string = "Closed";
	}
	else
	{
		string = "Undefined";
	}
}
#endif


/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	if(haveEqpStatus)
	{
		dpeNames.append("EqpStatus");
	}
	dpeNames.append("RR1");
	dpeNames.append("RR2");
	// Timestamps are not used when producing state, but they are needed
	// to check if some change occured at given moment
	dpeNames.append("TS1.YMDH");
	dpeNames.append("TS1.MSM");
	dpeNames.append("TS2.YMDH");
	dpeNames.append("TS2.MSM");
	QList<QVariant *> values;
	if(!pPool->getDpValues(dpName, ts, dpeNames, values))
	{
		return;
	}
	CtlStatus useStatus = Eqp::Used;
	QVariant *pVariant = NULL;
	if(haveEqpStatus)
	{
		if (values.isEmpty()) {
			return;
		}
		pVariant = values.takeFirst();
		useStatus = (CtlStatus)pVariant->toUInt();
		delete pVariant;
	}
	if(isColorByCtlStatus(useStatus, color))
	{
		isStateStringByCtlStatus(useStatus, state);
	}
	else
	{
		if (values.isEmpty()) {
			return;
		}
		pVariant = values.takeFirst();
		unsigned rr1 = pVariant->toUInt();
		delete pVariant;
		if (values.isEmpty()) {
			return;
		}
		pVariant = values.takeFirst();
		unsigned rr2 = pVariant->toUInt();
		delete pVariant;
		getMainStateString(rr1, rr2, false, state);
		getMainColor(rr1, rr2, false, color);
	}
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpVV::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	return rr1 & RR1_ERROR_CODE_MASK;
}

Eqp::MainEqpStatus EqpVV::getMainStatus(void)
{
	MainEqpStatus result = Good;
	if(plcAlarmOnline)
	{
		result = Undefined;
	}
	else if(!(rr1Online & RR1_VALID))
	{
		result = Undefined;
	}
	else
	{
		unsigned rr1Masked = rr1Online & (RR1_OPEN | RR1_CLOSED);
		if((rr1Masked != RR1_OPEN) && (rr1Masked != RR1_CLOSED))
		{
			result = Undefined;
		}
		else if(rr1Masked == RR1_OPEN)
		{
			result = Good;
		}
		else
		{
			result = Bad;
		}
	}
	return result;
}

bool EqpVV::getInterlockColor(DataEnum::DataMode mode, QColor &color, QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList)
{
	bool result = false;
	order = 0;
	if(isColorByCtlStatus(color, mode))
	{
		borderColor = color;
		return false;
	}
	getMainColor(color, mode);	// Default == main color of device
	borderColor = color;	// Default = no visible border
	switch(ctrlFamily)
	{
	case 1:	// Valves
		switch(ctrlType)
		{
		case 1:	// SVCU-controlled for SPS
			switch(ctrlSubType)
			{
			case 1:	// VVS_SPS
			case 2:	// VVS_SV
				result = getInterlockColorVVS_SPS(mode, color, borderColor, order, pNeighborList);
				break;
			}
			break;
		case 3:	// VVS_LHC
			result = getInterlockColorVVS_LHC(mode, color, borderColor, order, pNeighborList);
			break;
		case 5:	// VVS_PS
			result = getInterlockColorVVS_PS(mode, color, borderColor, order, pNeighborList);
			break;
		case 7:	// VV_STD_IO
			result = getInterlockColorVV_STD_IO(mode, color, borderColor, order, pNeighborList);
			break;
		case 12:
			switch(ctrlSubType)
			{
				case 1: //VVS_S_DP
					result = getInterlockColorVVS_SPS(mode, color, borderColor, order, pNeighborList);
					break;
				case 3:	// VVS_LHC_DP
					result = getInterlockColorVVS_LHC(mode, color, borderColor, order, pNeighborList);
					break;
				case 4: //	
					result = getInterlockColorVVS_PS(mode, color, borderColor, order, pNeighborList);
					break;
			}
			break;
		}
		break;
	case 254:
		switch(ctrlType)
		{
		case 7:	// Summary valves in PSB (2 or 4 real valves) VVS_PSB_SUM
			result = getInterlockColorVVS_PS(mode, color, borderColor, order, pNeighborList);
			break;
		}
	}
	return result;
}

bool EqpVV::getInterlockColorVVS_PS(DataEnum::DataMode mode, QColor &color, QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList)
{
	bool plcAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	if(plcAlarm)
	{
		color.setRgb(0, 0, 255);
		borderColor.setRgb(0, 0, 255);
		order = 5;
		return true;
	}
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(0, 0, 255);
		borderColor.setRgb(0, 0, 255);
		order = 5;
		return true;
	}

	// Highest level - undefined state
	unsigned rr1Masked = rr1 & (RR1_OPEN | RR1_CLOSED);
	if((rr1Masked != RR1_OPEN) && (rr1Masked != RR1_CLOSED))
	{
		color.setRgb(133, 56, 148);	// HTML #853894
		borderColor.setRgb(0, 0, 255);
		order = 9;
		return true;
	}

	// Valid data, valve is either open or closed - border color depends on OPEN/CLOSED state
	getInterlockBorderColor(borderColor, order, pNeighborList);

	// Next level - local interlocks before/after
	unsigned rr2 = mode == DataEnum::Replay ? rr2Replay : rr2Online;
	if(rr2 & 0x0300)
	{
		color.setRgb(255, 255, 0);	// Yellow
		if(order < 7)
		{
			order = 7;
		}
		return true;
	}
	// Next level - error
	if(rr1 & RR1_ERROR)
	{
		color.setRgb(212, 0, 255);	// HTML #D400FF (violet)
		if(order < 4)
		{
			order = 4;
		}
		return true;
	}

	// Next level - warning
	if(rr1 &  RR1_WARNING)
	{
		color.setRgb(255, 127, 0);	// orange
		if(order < 3)
		{
			order = 3;
		}
		return true;
	}

	// Others - 'normal' color of valve
	if(rr1Masked == RR1_OPEN)
	{
		color.setRgb(COLOR_OPEN);
	}
	else
	{
		color.setRgb(COLOR_CLOSED);
	}
	order = 0;
	return false;
}

bool EqpVV::getInterlockColorVVS_SPS(DataEnum::DataMode mode, QColor &color, QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList)
{
	bool plcAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	if(plcAlarm)
	{
		color.setRgb(0, 0, 255);
		borderColor.setRgb(0, 0, 255);
		order = 5;
		return true;
	}
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(0, 0, 255);
		borderColor.setRgb(0, 0, 255);
		order = 5;
		return true;
	}

	// Highest level - undefined state
	unsigned rr1Masked = rr1 & (RR1_OPEN | RR1_CLOSED);
	if((rr1Masked != RR1_OPEN) && (rr1Masked != RR1_CLOSED))
	{
		color.setRgb(133, 56, 148);	// HTML #853894
		borderColor.setRgb(0, 0, 255);
		order = 9;
		return true;
	}

	// Valid data, valve is either open or closed - border color depends on OPEN/CLOSED state
	getInterlockBorderColor(borderColor, order, pNeighborList);

	// Next level - local interlocks before/after
	unsigned rr2 = mode == DataEnum::Replay ? rr2Replay : rr2Online;
	if(rr2 & 0x0300)
	{
		color.setRgb(255, 255, 0);	// Yellow
		if(order < 7)
		{
			order = 7;
		}
		return true;
	}
	/* Decision by F.Antoniotty 26.06.2012: global interlocks are not needed
	// Next level - global interlocks before/after
	if(rr2 & 0x0C00)
	{
		color.setRgb(255, 255, 0);	// Yellow
		order = 6;
		return true;
	}
	*/
	// Next level - error
	if(rr1 & RR1_ERROR)
	{
		color.setRgb(212, 0, 255);	// HTML #D400FF (violet)
		if(order < 4)
		{
			order = 4;
		}
		return true;
	}

	// Next level - warning
	if(rr1 &  RR1_WARNING)
	{
		color.setRgb(255, 127, 0);	// orange
		if(order < 3)
		{
			order = 3;
		}
		return true;
	}

	// Next level - deltaP interlocks
	if(rr2 & 0x3000)
	{
		color.setRgb(255, 255, 255);	// white
		if(order < 6)
		{
			order = 6;
		}
		return true;
	}

	// Others - 'normal' color of valve
	if(rr1Masked == RR1_OPEN)
	{
		color.setRgb(COLOR_OPEN);
	}
	else
	{
		color.setRgb(COLOR_CLOSED);
	}
	order = 0;
	return false;
}

bool EqpVV::getInterlockColorVVS_LHC(DataEnum::DataMode mode, QColor &color, QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList)
{
	bool plcAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	if(plcAlarm)
	{
		color.setRgb(0, 0, 255);
		borderColor.setRgb(0, 0, 255);
		order = 5;
		return true;
	}
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(0, 0, 255);
		borderColor.setRgb(0, 0, 255);
		order = 5;
		return true;
	}

	// Highest level - undefined state
	unsigned rr1Masked = rr1 & (RR1_OPEN | RR1_CLOSED);
	if((rr1Masked != RR1_OPEN) && (rr1Masked != RR1_CLOSED))
	{
		color.setRgb(133, 56, 148);	// HTML #853894
		borderColor.setRgb(0, 0, 255);
		order = 9;
		return true;
	}

	// Valid data, valve is either open or closed - border color depends on OPEN/CLOSED state
	getInterlockBorderColor(borderColor, order, pNeighborList);

	// Next level - local pressure interlock
	unsigned rr2 = mode == DataEnum::Replay ? rr2Replay : rr2Online;
	if(rr2 & 0x0100)
	{
		color.setRgb(255, 255, 0);	// Yellow
		if(order < 8)
		{
			order = 8;
		}
		return true;
	}

	// Next level - interlock valves
	if(rr2 & 0x0C00)
	{
		color.setRgb(255, 255, 0);	// Yellow
		if(order < 7)
		{
			order = 7;
		}
		return true;
	}
		
	// Next level - temperature interlock
	if(rr2 & 0x0200)
	{
		color.setRgb(255, 255, 0);	// Yellow
		if(order < 6)
		{
			order = 6;
		}
		return true;
	}

	// Next level - error
	if(rr1 & RR1_ERROR)
	{
		color.setRgb(212, 0, 255);	// HTNL #D400FF (violet)
		if(order < 4)
		{
			order = 4;
		}
		return true;
	}

	// Next level - warning
	if(rr1 &  RR1_WARNING)
	{
		color.setRgb(255, 127, 0);	// orange
		if(order < 3)
		{
			order = 3;
		}
		return true;
	}

	// Next level - local pressures OK
	if(!(rr1 & 0x00100000))
	{
		color.setRgb(255, 255, 255);	// White
		if(order < 2)
		{
			order = 2;
		}
		return true;
	}

	// Others - 'normal' color of valve
	if(rr1Masked == RR1_OPEN)
	{
		color.setRgb(COLOR_OPEN);
	}
	else
	{
		color.setRgb(COLOR_CLOSED);
	}
	order = 0;
	return false;
}

bool EqpVV::getInterlockColorVV_STD_IO(DataEnum::DataMode mode, QColor &color, QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList)
{
	bool plcAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	if(plcAlarm)
	{
		color.setRgb(0, 0, 255);
		borderColor.setRgb(0, 0, 255);
		order = 5;
		return true;
	}
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(0, 0, 255);
		borderColor.setRgb(0, 0, 255);
		order = 5;
		return true;
	}

	// Highest level - undefined state
	unsigned rr1Masked = rr1 & (RR1_OPEN | RR1_CLOSED);
	if((rr1Masked != RR1_OPEN) && (rr1Masked != RR1_CLOSED))
	{
		color.setRgb(133, 56, 148);	// HTML #853894
		borderColor.setRgb(0, 0, 255);
		order = 9;
		return true;
	}

	// Valid data, valve is either open or closed - border color depends on OPEN/CLOSED state
	getInterlockBorderColor(borderColor, order, pNeighborList);

	// Next level - interlock(s)
	if(rr1 & 0x00200000)
	{
		color.setRgb(255, 255, 0);	// Yellow
		if(order < 8)
		{
			order = 8;
		}
		return true;
	}

	// Others - 'normal' color of valve
	if(rr1Masked == RR1_OPEN)
	{
		color.setRgb(COLOR_OPEN);
	}
	else
	{
		color.setRgb(COLOR_CLOSED);
	}
	order = 0;
	return false;
}

/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	/* DPEs are needed anyway, even if there is no criteria - to have information to be shown???
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.hasStateCriteria())
	{
		return;	// No state criteria
	}
	*/
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	if(haveEqpStatus)
	{
		addDpeNameToList(dpes, "EqpStatus");
	}
	addDpeNameToList(dpes, "RR1");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVV::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if((rr1 & (RR1_OPEN | RR1_CLOSED)) == RR1_OPEN)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if((rr1 & (RR1_OPEN | RR1_CLOSED)) == RR1_CLOSED)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if(rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if(criteria.isWarnings())
	{
		if(rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}
	if(criteria.isAlerts())
	{
		if(rr1 & (RR1_AL1 | RR1_AL2 | RR1_AL3))
		{
			return true;
		}
	}
	return 0;
}
#endif


/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if(checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	// Without PLC connection does not match any criteria
	if(plcAlarmOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// In invalid state - only matches Error/warning criteria
	if(!(rr1Online & RR1_VALID))
	{
		switch(pCriteria->getType())
		{
		case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		default:
			pCriteria->setMatch(false, false, 0);
			return;
		}
	}

	// State is valid - decide based on criteria
	unsigned maskedRr1 = rr1Online & (RR1_OPEN | RR1_CLOSED);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case FunctionalTypeVV::Closed:
			if(maskedRr1 == RR1_CLOSED)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::Open:
			if(maskedRr1 == RR1_OPEN)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::PrSourcesOK:
			if(rr1Online & 0x00100000)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::CurIntlLocalPressure:
			if(rr2Online & 0x00000100)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::CurIntlLocalTemp:
			if(rr2Online & 0x00000200)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::CurIntlPrevValve:
			if(rr2Online & 0x00000400)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::CurIntlNextValve:
			if(rr2Online & 0x00000800)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::CurIntlDeltaPBefore:
			if(rr2Online & 0x00001000)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::CurIntlDeltaPAfter:
			if(rr2Online & 0x00002000)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::CurIntlGlobalBefore:
			if(rr2Online & 0x00000400)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::CurIntlGlobalAfter:
			if(rr2Online & 0x00000800)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::CurIntlLocalBefore:
			if(rr2Online & 0x00000100)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV::CurIntlLocalAfter:
			if(rr2Online & 0x00000200)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	case EqpMsgCriteria::Error:
		if(rr1Online & RR1_ERROR)
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else
		{
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	case EqpMsgCriteria::Warning:
		if(rr1Online & (RR1_ERROR | RR1_WARNING))
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else
		{
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}

