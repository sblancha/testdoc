#ifndef VACICONVVF_S_H
#define	VACICONVVF_S_H

// Icon for VVF_S

#include "VacIcon.h"

class EqpVV;

class VACCTLEWOUTIL_EXPORT VacIconVVF_S : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVVF_S(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVVF_S();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);


protected:
	// Pointer to my device
	EqpVV *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
	
	void setPainterForControl(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1,  bool plcAlarm);
	void setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
};

#endif	// VACICONVVF_S_H
