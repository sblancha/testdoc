const int ACCESS_ZONES_CMD_NOTHING = 0;
const int ACCESS_ZONES_CMD_ON_CMW = 1;
const int ACCESS_ZONES_CMD_ON_PVSS = 2;
const int ACCESS_ZONES_CMD_OFF_CMW = 3;
const int ACCESS_ZONES_CMD_OFF_PVSS = 4;

const int ACCESS_ZONES_STATE_ALL_OPEN = 0;
const int ACCESS_ZONES_STATE_ALL_CLOSED = 1;
const int ACCESS_ZONES_STATE_OTHER = 2;

const bool ACCESS_ZONE_START = true;
const bool ACCESS_ZONE_FINISH = false;

// maximum number of access zones the valve belong to
const int ACCESS_ZONES_PER_VALVE_NUMBER= 5;

//LhcVacActionPossible_AZ:
/** Check if allows executing actions for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param dpType: in, DP type
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_ACCESS_ZONE(string dpName, string dpType, dyn_anytype act, bool &possible,
  dyn_string &exceptionInfo)
{
  possible = true;
}

//LhcVacGetAccessZoneCommandName:
/** Get readable name of command for access zone

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param command: in, command name
	@param accessZone: in, name of command
	@param commandName: out, readable name of command.
*/

void LhcVacGetAccessZoneCommandName(const int command, const string accessZone, string &commandName)
{
  if(command == ACCESS_ZONES_CMD_ON_CMW)
  {
    commandName = "Access to Zone <" + accessZone + "> (CMW)";
  }
  else if(command == ACCESS_ZONES_CMD_ON_PVSS)
  {
    commandName = "Access to Zone <" + accessZone + "> (PVSS)";
  }
  else if(command == ACCESS_ZONES_CMD_OFF_CMW)
  {
    commandName = "No Access to Zone <" + accessZone + "> (CMW)";
  }
  else if(command == ACCESS_ZONES_CMD_OFF_PVSS)
  {
    commandName = "No Access to Zone <" + accessZone + "> (PVSS)";
  }
  else
  {
    commandName = "No command  for Zone <" + accessZone + ">";
  }
     
}
//LhcVacGetAccessZoneStateUI:
/** Get readable name of state for access zone for UI

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param state: in, command name
	@param stateName: out, readable state.
*/
void LhcVacGetAccessZoneStateUI(const int state, string &stateName)
{
  if(state == ACCESS_ZONES_STATE_ALL_OPEN)
  {
    stateName = "NO ACCESS";
  }
  else if(state == ACCESS_ZONES_STATE_ALL_CLOSED)
  {
    stateName = "ACCESS";
  }
  else if(state == ACCESS_ZONES_STATE_OTHER)
  {
    stateName = "OTHER";
  }     
}
//LhcVacGetAccessZoneCommandNameUI:
/** Get readable name of command for access zone for UI

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param command: in, command name
	@param commandName: out, readable name of command.
*/
void LhcVacGetAccessZoneCommandNameUI(const int command, string &commandName)
{
  if(command == ACCESS_ZONES_CMD_ON_CMW)
  {
    commandName = "Access(CMW)";
  }
  else if(command == ACCESS_ZONES_CMD_ON_PVSS)
  {
    commandName = "Access(PVSS)";
  }
  else if(command == ACCESS_ZONES_CMD_OFF_CMW)
  {
    commandName = "NoAccess(CMW)";
  }
  else if(command == ACCESS_ZONES_CMD_OFF_PVSS)
  {
    commandName = "NoAccess(PVSS)";
  }
  else
  {
    commandName = "No command";
  }
     
}

//LhcVacDeviceAction_ACCESS_ZONE:
/** Execute action for valve. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_ACCESS_ZONE(string dpName, dyn_anytype action, dyn_string &exceptionInfo)
{
  bool	        newWr1;
  dyn_errClass	err;
  unsigned      act;

  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_OPEN:
    newWr1 =  ACCESS_ZONE_FINISH;
    break;
  case LVA_DEVICE_CLOSE:
    newWr1 =  ACCESS_ZONE_START;
    break;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }
  // Execute
  dpSetWait(dpName + ".CMD_PVSS:_original.._value", newWr1);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
}

