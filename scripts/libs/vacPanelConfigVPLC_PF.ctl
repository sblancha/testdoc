/** @defgroup vacPanelConfigVPLC_PF admin/vacPanelConfigVPLC_PF.pnl
vacPanelConfigVPLC_PF.ctl is the scope library of the panel vacPanelConfigVPLC_PF.pnl in the directory admin/. \n
vacPanelConfigVPLC_PF.pnl is a panel to initialize configuration data of fixed pumping group PLC. \n
\n 
Vacuum PLC for Pumping group Fixed(VPLC_PF) is a PLC dedicated to the control of a single fixed pumping group, 
i.e. the process and sub-devices control. \n
PLCs of type VPLC_PF are not updated such as standard PLCs. This means no code files is generated 
from vacuum database exporter and so nothing is compiled neither downloaded. These PLCs are configured 
by this script setting write register dpe and getting read register dpe. \n
\n
The configuration parameters to be initialized are specified in the Vacuum Fixed Device Communication Register document: \n
https://svnvacplc.web.cern.ch/repo/Specifications/VacFixDevCommRegisters.xlsx sheet VPLC_PF \n
\n
PLCs of type VPLC_PF have 2 communication partners (other PLCs) to get the pressure values (1VGM/1VGR/1VGP per sector)   
of the previous and next sectors and display values on local touch panel. \n
The PLCs have so 3 IP Address to be defined in the PLC hardware configuration (is own IP and the IP of the 2 partner). \n
\n
The main item of the panel is a \b table called plcTable, it has the below columns: \n
    Column Name          | Title            |Description
    ---------------------|------------------|-----------
    nb                   | #                | Number of the PLC 
    plcName              | PLC Name         | The layout name of the PLC 
    systIntegrity        | Integrity        | PLC alive status from Unicos System integrity (updated with call back) 
    ip                   | IP               | IP Address of the PLC 
    vpgName              | VPG Name         | Layout name of the associated Pumping group
    hwParUpd             | Up HW            | Is Hardware parameters need to be updated (Yes or No)
    procParUpd           | Up Pro.          | Is Process parameters need to be updated (Yes or No)
    txSt                 | Tx State         | The current config transmission state 
    pCode                | P Code           | The parameter code currently processed 
    pName                | Param Name       | The parameter name currently processed 
    pVal                 | Param Value      | Value write or read of the current parameter
    prePumpTime          | prePumpTim       | Parameter value prePumpTime [sec]
    ventCheckTime        | ventChecTim      | Parameter value ventCheckTime [sec]
    ventingTime          | ventTime         | Parameter value ventingTime [sec]
    vvtOpenDelay         | vvtOpDelay       | Parameter value vvtOpenDelay [sec]
    vvpOpenDelay         | vvpOpDelay       | Parameter value vvpOpenDelay [sec]
    errVptNOnTO          | errVptNOnTO      | Parameter value errVptNOnTO [sec]
    errAccelTO           | errAccelTO       | Parameter value errAccelTO [sec]
    errRecoverTO         | errRecovTO       | Parameter value errRecoverTO [sec]
    vvr1ForcedTO         | vvr1FoTO         | Parameter value vvr1ForcedTO [sec]
    vvr2ForcedTO         | vvr2FoTO         | Parameter value vvr2ForcedTO [sec]
    autoRestart          | auRest           | Parameter value autoRestart [0:no 1:yes]
    autoRestartMax       | auRestMax        | Parameter value autoRestartMax [number] 
    autoVent             | auVent           | Parameter value autoVent [0:no 1:yes] 
    vg2PrLimitVal        | vg2PrLimit       | Parameter value vg2PrLimitVal [mbar]

*/
#uses "VacCtlEqpDataPvss"	      // C++ CTRL Library
#uses "VacCtlImportPvss"	      // C++ CTRL Library
#uses "vclGeneric.ctl"           // WCCOA CTRL Library
#uses "vclDeviceFileParser.ctl"	// WCCOA CTRL Library
#uses "vclDevCommon.ctl"	      // WCCOA CTRL Library
#uses "vclAddressConfig.ctl"     // WCCOA CTRL Library

/// @brief DP Type of PLC Pumping group Fixed
const string PLC_DP_TYPE = "VPLC_PF";
/// @brief DP Type of Pumping group
const string VPGU_DP_TYPE = "VP_GU";

/// @brief Parameter code to Write Hardware Config VPG Name  
const int SET_vpgName_CODE = 1;
/// @brief Parameter code to Write Hardware Config VVR1 Name  
const int SET_vvr1Name_CODE = 2;                             
/// @brief Parameter code to Write Hardware Config VVR2 Name  
const int SET_VVR2Name_CODE = 3;  
/// @brief Parameter code to Set Hardware Config VG1 Type  
const int SET_vg1Type_CODE = 10;
/// @brief Parameter code to Set Hardware Config VG2 Type  
const int SET_vg2Type_CODE = 11;
/// @brief Parameter code to Set Hardware Config isPrSwitch  
const int SET_isPrSwitch_CODE = 12;

/// @brief Parameter code to Set Datablock Number of VGM in first partner PLC (previous sector) 
const int SET_vgmDbPrevSect_CODE = 13;
/// @brief Parameter code to Set Datablock Number of VGR in first partner PLC (previous sector) 
const int SET_vgrDbPrevSect_CODE = 14; 
/// @brief Parameter code to Set Datablock Number of VGP in first partner PLC (previous sector) 
const int SET_vgpDbPrevSect_CODE = 15; 

/// @brief Parameter code to Set Datablock Number of VGM in second partner PLC (next sector) 
const int SET_vgmDbNextSect_CODE = 16;
/// @brief Parameter code to Set Datablock Number of VGR in second partner PLC (next sector) 
const int SET_vgrDbNextSect_CODE = 17; 
/// @brief Parameter code to Set Datablock Number of VGP in second partner PLC (next sector) 
const int SET_vgpDbNextSect_CODE = 18;

/// @brief Parameter code to Set prePumpTime NEED FEEDBACK  
const int SET_prePumpTime_CODE = 100;
/// @brief Parameter code to Set ventCheckTime NEED FEEDBACK    
const int SET_ventCheckTime_CODE = 101;  
/// @brief Parameter code to Set ventingTime  NEED FEEDBACK  
const int SET_ventingTime_CODE = 102;  
/// @brief Parameter code to Set vvtOpenDelay  NEED FEEDBACK  
const int SET_vvtOpenDelay_CODE = 103;  
/// @brief Parameter code to Set vvpOpenDelay   NEED FEEDBACK  
const int SET_vvpOpenDelay_CODE = 104;  
/// @brief Parameter code to Set errVPTNOnTO  NEED FEEDBACK  
const int SET_errVPTNOnTO_CODE = 105;  
/// @brief Parameter code to Set errAccelTO  NEED FEEDBACK  
const int SET_errAccelTO_CODE = 106;  
/// @brief Parameter code to Set errRecoverTO  NEED FEEDBACK  
const int SET_errRecoverTO_CODE = 107;
/// @brief Parameter code to Set VVR1ForcedTO  NEED FEEDBACK  
const int SET_vvr1ForcedTO_CODE = 108;
/// @brief Parameter code to Set VVR2ForcedTO  NEED FEEDBACK  
const int SET_vvr2ForcedTO_CODE = 109;
/// @brief Parameter code to Set autoRestart  NEED FEEDBACK  
const int SET_autoRestart_CODE = 110;
/// @brief Parameter code to Set autoRestartMax NEED FEEDBACK   
const int SET_autoRestartMax_CODE = 111;
/// @brief Parameter code to Set autoVent  NEED FEEDBACK  
const int SET_autoVent_CODE = 112;
/// @brief Parameter code to Set Penning Gauge Pressure Interlock parameter code NEED FEEDBACK  
const int SET_vg2PrLimitVal_CODE = 113;
 
/// @brief Not Attribute Parameter code to Set Config file (.forDLL) Time stamp YMDH
const int SET_TS_YMDH_CODE = 150;
/// @brief Not Attribute Parameter code to Set Config file (.forDLL) Time stamp MSM
const int SET_TS_MSM_CODE = 151;

/// @brief Parameter code to Get prePumpTime  
const int GET_prePumpTime_CODE = 200;
/// @brief Parameter code to Get ventCheckTime  
const int GET_ventCheckTime_CODE = 201;  
/// @brief Parameter code to Get ventingTime  
const int GET_ventingTime_CODE = 202;  
/// @brief Parameter code to Get vvtOpenDelay  
const int GET_vvtOpenDelay_CODE = 203;  
/// @brief Parameter code to Get vvpOpenDelay   
const int GET_vvpOpenDelay_CODE = 204;  
/// @brief Parameter code to Get errVPTNOnTO  
const int GET_errVptNOnTO_CODE = 205;  
/// @brief Parameter code to Get errAccelTO  
const int GET_errAccelTO_CODE = 206;  
/// @brief Parameter code to Get errRecoverTO  
const int GET_errRecoverTO_CODE = 207;
/// @brief Parameter code to Get VVR1ForcedTO  
const int GET_vvr1ForcedTO_CODE = 208;
/// @brief Parameter code to Get VVR2ForcedTO  
const int GET_vvr2ForcedTO_CODE = 209;
/// @brief Parameter code to Get autoRestart  
const int GET_autoRestart_CODE = 210;
/// @brief Parameter code to Get autoRestartMax  
const int GET_autoRestartMax_CODE = 211;
/// @brief Parameter code to Get autoVent  
const int GET_autoVent_CODE = 212;
/// @brief Parameter code to Get Penning Gauge Pressure Interlock  
const int GET_vg2PrLimitVal_CODE = 213; 


/// @brief Transmission status meaning SENDING 
const string TX_SENDING = "Sending...";
/// @brief Transmission status meaning CHECKING 
const string TX_CHECKING = "Checking...";
/// @brief Transmission status meaning PARAMETER DONE 
const string TX_DONE = "Param. Done";
/// @brief Transmission status meaning ATTRIBUTE ERREUR 
const string TX_ATTR_ERR = "ERR:Attr Reading";
/// @brief Transmission status meaning CHECKING ERREUR 
const string TX_ATTR_ERR = "ERR:Checking";
/// @brief Transmission status meaning All VPG CONFIGURED 
const string TX_CONFIGURED = "Configured OK";

/// @brief Code to send to FE through WR1 to notify eqp is activated in SCADA 
const unsigned SCADA_ACTIVATED_WR1 = 0x0400u;

/// @brief WCCOA project system name
string systemName;

/// @brief List of PLC DPs 
dyn_string      plcDps;
/// @brief Pumping group DPs 
dyn_string	    vpgDps;
/// @brief List of VPLC_PF layout names 
dyn_string      plcNames;
/// @brief Pumping group layout names 
dyn_string	    vpgNames;
/// @brief List of VPLC_PF IP Addresses 
dyn_string      plcIps;

/// @brief Flag to enable debugging output
const bool		  isDebugFlag = true;

/**
@brief Initialisation of the panel
@details Build the PLC table
@param mainTable The PLC table of the panel
*/
void PanelInit(shape mainTable) {
  dyn_string exceptionInfo;  
  //DebugTN("This is the init");
  dynClear(plcDps);
  dynClear(vpgDps);
  dynClear(plcNames);
  dynClear(vpgNames);
  dynClear(plcIps);
  mainTable.deleteAllLines();
  dynClear(paramToSet);
  // Fill PLC DPs  
  plcDps = dpNames("*",PLC_DP_TYPE);
  systemName = strrtrim(dpSubStr(plcDps[1], DPSUB_SYS), ":");
  // Build the PLC table
  // Fill table with static data: names, ip addr...   
  for(int i = 1; i <= dynlen(plcDps); i++) {
    string layoutName, sIpAddr, vpgName;
    // Fill PLC Names     
    LhcVacDisplayName(dpSubStr(plcDps[i], DPSUB_DP), layoutName, exceptionInfo);
    dynAppend(plcNames, layoutName);
    // Fill PLC IP Addresses 
    LhcVacGetAttribute(dpSubStr(plcDps[i], DPSUB_DP), "ipAddress", sIpAddr);
    DebugTN("ipAddress:",  sIpAddr);
    dynAppend(plcIps, sIpAddr);
    // Fill VPG Name
    LhcVacGetAttribute(dpSubStr(plcDps[i], DPSUB_DP), "vpgName", vpgName);
    DebugTN("vpgName:",  vpgName);
    dynAppend(vpgNames, vpgName);
    // Editing Table with 'static' data
    mainTable.appendLine("nb", i, "plcName", layoutName, "ip", sIpAddr, "vpgName", vpgName);
  }
  // Callbacks to keep some data up to date in the table
  for(int i = 1; i <= dynlen(plcDps); i++) {
    // Manage system integrity
    string systIntegDpName = dpSubStr(plcDps[i], DPSUB_DP);
    DebugTN("PLC dp name:",  systIntegDpName);
    strchange(systIntegDpName, 0, 8, "");
    DebugTN("PLC counter dp name:",  systIntegDpName);
    systIntegDpName = "_unSystemAlarm_DPE_" + systIntegDpName; 
    //DebugTN("systIntegDpName ",systIntegDpName);
    if(dpExists(systIntegDpName)) {    
      ret = dpConnectUserData("UpdateSystIntegrityCB", mainTable, systIntegDpName + ".alarm");
      if(ret != 0) {
        mainTable.cellValueRC(i-1, "systIntegrity", "Disable");
      }
    }
    else {
      mainTable.cellValueRC(i-1, "systIntegrity", "Disable");
    }    
  }
 
}

/**
@brief Closing of the panel
*/
void PanelClose() {
	DebugTN("This is the close function");
}

/**
@brief Configuration process of fixed pumping group PLCs
@param mainTable The PLC table of the panel
*/
void ConfigProcess(shape mainTable, bool checkOnly) {
  dyn_string		exceptionInfo;
  dyn_errClass	   err;
  string          errMessage; 
  int             ret;             //return value of function (0 is OK)
  
  DebugTN("vacPanelConfigVPLC_PF.pnl: ----- Fixed Pumping group PLC configuration starting");

  // Build the array parameter code
  dyn_int hwStringParamCos = makeDynInt( 
                                   SET_vpgName_CODE,
                                   SET_vvr1Name_CODE);
  dyn_int hwStringOptParamCos = makeDynInt(
                                   SET_vvr2Name_CODE);
  dyn_int hwIntParamCos = makeDynInt(
                                   SET_isPrSwitch_CODE);
  dyn_int hwIntOptParamCos = makeDynInt(
                                   SET_vgmDbPrevSect_CODE,
                                   SET_vgrDbPrevSect_CODE,
                                   SET_vgpDbPrevSect_CODE,
                                   SET_vgmDbNextSect_CODE,
                                   SET_vgrDbNextSect_CODE,
                                   SET_vgpDbNextSect_CODE);
  dyn_int processIntParamCos = makeDynInt( 
                                   SET_prePumpTime_CODE,
                                   SET_ventCheckTime_CODE,
                                   SET_ventingTime_CODE,
                                   SET_vvtOpenDelay_CODE,
                                   SET_vvpOpenDelay_CODE,
                                   SET_errVPTNOnTO_CODE,
                                   SET_errAccelTO_CODE,
                                   SET_errRecoverTO_CODE,
                                   SET_vvr1ForcedTO_CODE,
                                   SET_vvr2ForcedTO_CODE,
                                   SET_autoRestart_CODE,
                                   SET_autoRestartMax_CODE,
                                   SET_autoVent_CODE,
                                   SET_TS_YMDH_CODE,
                                   SET_TS_MSM_CODE);
  dyn_int processFloatParamCos = makeDynInt(SET_vg2PrLimitVal_CODE);
  

  dyn_string paramName = makeDynString("vpgName",
                                           "vvr1SectName",
                                           "isPrSwitch",
                                           "vgmDbPrevSect",
                                           "vgrDbPrevSect",
                                           "vgpDbPrevSect",
                                           "vgmDbNextSect",
                                           "vgrDbNextSect",
                                           "vgpDbNextSect",
                                           SET_prePumpTime_CODE,
                                           SET_ventCheckTime_CODE,
                                           SET_ventingTime_CODE,
                                           SET_vvtOpenDelay_CODE,
                                           SET_vvpOpenDelay_CODE,
                                           SET_errVPTNOnTO_CODE,
                                           SET_errAccelTO_CODE,
                                           SET_errRecoverTO_CODE,
                                           SET_vvr1ForcedTO_CODE,
                                           SET_vvr2ForcedTO_CODE,
                                           SET_autoRestart_CODE,
                                           SET_autoRestartMax_CODE,
                                           SET_autoVent_CODE,
                                           "vg2PrLimitVal");
  dyn_int needFeedbackParamCos = makeDynInt(); 
                                           
  dyn_int optionalParamCodes = makeDynInt( SET_VVR2Name_CODE,
                                           SET_vg1Type_CODE,
                                           SET_vg2Type_CODE);  

  dyn_string optionalParamAttrName = makeDynString("vvr2SectName",
                                                   "vg1Type",
                                                   "vg2Type",
                                                   "blabla");
                                                   
                                                   
  
                                           
  anytype paramVal;
 
  for(int i = 1; i <= dynlen(plcDps); i++) {
    // VPG Name
    paramVal = "";
    paramVal = plcNames[i];
    if (paramVal == "") {
      mainTable.cellValueRC(i-1, "txSt", TX_ATTR_ERR);      
    }  
    a = makeDynAnytype(SET_vpgName_CODE,X);
  }    











}

/**
@brief Private Configuration process of fixed pumping group PLCs
@param[in] plcDpName   DP name of the PLC VPLC_PF
@param[in] pCode       Parameter code
@param[in] isValFloat  Value to set or get is a float32, if false value is integer32
@return The return read value from PLC maybe float32 or integer32  
*/
anytype _processParam(string plcDpName, int pCode, bool isValFloat) {
    
  DebugTN("vacPanelConfigVPLC_PF.pnl: read or write param");
  //reset config read register   
  //dpSetWait( 
  return 0; 
}
/**
@brief Private Get the row number from PLC number in the list (first column "nb")
*/
int _getTableRowNb(shape table, int nbPlc) {
  //DebugTN(" indexConnId ", sIndexConnId);  
  for(int i = 0; i < dynlen(plcDps); i++) {
    string sNbVal;
    getValue(table, "cellValueRC", i, "nb", sNbVal);
    //DebugTN(" connIdVal ", sConnIdVal, i);
    if( sNbVal == (string)nbPlc) {
      //DebugTN(" found i", i);      
      return i;    
    }     
  }
}
