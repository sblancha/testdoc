#ifndef	SECTLHCVIEW_H
#define	SECTLHCVIEW_H

// LHC sector view

#include "SynLhcView.h"


class SectLhcView : public SynLhcView  
{
	Q_OBJECT

public:
	SectLhcView(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	virtual ~SectLhcView();

	static SectLhcView *create(QWidget *parent, Sector *pFirstSector, Sector *pLastSector,
		DataEnum::DataMode mode);


protected:
	// The view is only built once - the flag indicating that view was built
	bool	ready;

	virtual void build(bool hideMagnets);
	virtual void buildView(unsigned vacTypeMask, VacEqpTypeMask &cryoEqpMask,
		VacEqpTypeMask &beamEqpMask);
	virtual void buildGeometry(void);
	virtual void calcVerticalPos(void);
};

#endif	// SECTLHCVIEW_H
