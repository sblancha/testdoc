#ifndef VACDPDATACONNECTION_H
#define	VACDPDATACONNECTION_H

#include "InterfaceEqp.h"

#include <qlist.h>

class Eqp;

class VacDpDataConnection : public InterfaceEqp
{
	Q_OBJECT

public:
	VacDpDataConnection();
	virtual ~VacDpDataConnection();

	void connectDpe(const char *dpe);

	// Access
	inline Eqp *getEqp(void) const { return pEqp; }
	void setEqp(Eqp *pEqp);
	inline DataEnum::DataMode getMode(void) const { return mode; }
	inline void setMode(DataEnum::DataMode mode) { this->mode = mode; }

public slots:
	// Override slots of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

signals:
	// Signal is emitted when state of one of DPE's of interest has been changed
	void dpeValueChange(const char *dpeName, int source, const QVariant &value);

protected:
	// List of requested DPE names
	QList<QByteArray *>	dpes;

	// Pointer to device being used
	Eqp			*pEqp;

	// Data acquisition mode
	DataEnum::DataMode	mode;

	void disconnectAll();
};

#endif	// VACDPDATACONNECTION_H
