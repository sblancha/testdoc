//	Implementation of HistoryTimeRangeDialog class
/////////////////////////////////////////////////////////////////////////////////

#include "HistoryTimeRangeDialog.h"

#include <qlayout.h>

////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

HistoryTimeRangeDialog::HistoryTimeRangeDialog(QWidget *parent, const QDateTime &start, const QDateTime &end, bool endTimeEditable)
	: QDialog(parent, Qt::Window)
{
	setAttribute(Qt::WA_DeleteOnClose);
	setModal(true);
	this->endTimeEditable = endTimeEditable;
	settingValue = false;

	setWindowTitle("Time Range for History");

	QGridLayout *pMainLayout = new QGridLayout(this);
	pMainLayout->setSpacing(16);
	pMainLayout->setVerticalSpacing(16);

	QLabel *pLabel = new QLabel("From", this);
	pLabel->show();
	pMainLayout->addWidget(pLabel, 0, 0);

	pStartTime = new QDateTimeEdit(start, this);
	pStartTime->setCalendarPopup(true);
	pStartTime->setDisplayFormat("dd-MM-yyyy hh:mm:ss");
	pStartTime->setMaximumDateTime(QDateTime::currentDateTime());
	pMainLayout->addWidget(pStartTime, 0, 1);
	pStartTime->show();
	connect(pStartTime, SIGNAL(dateTimeChanged(const QDateTime &)),
		this, SLOT(startTimeChanged(const QDateTime &)));

	pLabel = new QLabel("to", this);
	pLabel->show();
	pMainLayout->addWidget(pLabel, 0, 2);

	pEndTime = new QDateTimeEdit(end, this);
	pEndTime->setCalendarPopup(true);
	pEndTime->setDisplayFormat("dd-MM-yyyy hh:mm:ss");
	pStartTime->setMaximumDateTime(QDateTime::currentDateTime());
	pMainLayout->addWidget(pEndTime, 0, 3);
	pEndTime->setEnabled(endTimeEditable);
	pEndTime->show();
	connect(pEndTime, SIGNAL(dateTimeChanged(const QDateTime &)),
		this, SLOT(endTimeChanged(const QDateTime &)));

	pMinLabel = new QLabel("Min", this);
	pMinLabel->show();
	pMainLayout->addWidget(pMinLabel, 1, 1, 1, 2);

	pOkPb = new QPushButton("OK", this);
	pOkPb->show();
	pMainLayout->addWidget(pOkPb, 3, 1, 1, 1, Qt::AlignHCenter);
	connect(pOkPb, SIGNAL(clicked()), this, SLOT(okClicked()));

	pCancelPb = new QPushButton("Cancel", this);
	pCancelPb->show();
	pMainLayout->addWidget(pCancelPb, 3, 3, 1, 2, Qt::AlignHCenter);
	connect(pCancelPb, SIGNAL(clicked()), this, SLOT(cancelClicked()));

}

HistoryTimeRangeDialog::~HistoryTimeRangeDialog()
{
}

/*
**	FUNCTION
**		Set allowed time range for date+time editors
**
**	ARGUMENTS
**		start	- Minmum allowed start time
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryTimeRangeDialog::setAllowedRange(const QDateTime &start)
{
	minDateTime = start;
	QString label("min ");
	label += minDateTime.toString("dd-MM-yyyy hh:mm:ss");
	pMinLabel->setText(label);
	// pStartTime->dateEdit()->setRange(start.date(), QDate::currentDate());
	// pEndTime->dateEdit()->setRange(start.date(), QDate::currentDate());
}

/*
**	FUNCTION
**		Slot activated when start time has been changed by editor.
**		Make sure that range start...end is at least 60 sec
**
**	ARGUMENTS
**		value	- New start time set by editor
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryTimeRangeDialog::startTimeChanged(const QDateTime &value)
{
	if(settingValue)
	{
		return;
	}
	QDateTime newValue(value);
	if(newValue < minDateTime)
	{
		newValue = minDateTime;
	}
	QDateTime endTime(pEndTime->dateTime());
	int seconds = newValue.secsTo(endTime);
	if(seconds < 60)
	{
		QDateTime now(QDateTime::currentDateTime());
		endTime = newValue.addSecs(60);
		if(endTime > now)
		{
			endTime = now;
			newValue = endTime.addSecs(-60);
		}
	}
	settingValue = true;
	if(newValue != value)
	{
		pStartTime->setDateTime(newValue);
	}
	pEndTime->setDateTime(endTime);
	settingValue = false;
}

/*
**	FUNCTION
**		Slot activated when end time has been changed by editor.
**		Make sure that range start...end is at least 60 sec
**
**	ARGUMENTS
**		value	- New end time set by editor
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryTimeRangeDialog::endTimeChanged(const QDateTime &value)
{
	if(settingValue)
	{
		return;
	}
	QDateTime newValue(value);
	bool movedFromMin = false;
	if(newValue < minDateTime)
	{
		newValue = minDateTime.addSecs(60);
		movedFromMin = true;
	}
	QDateTime startTime(pStartTime->dateTime());
	int seconds = startTime.secsTo(newValue);
	settingValue = true;
	if(seconds < 60)
	{
		if(movedFromMin)
		{
			newValue = newValue.addSecs(60);
		}
		else
		{
			pStartTime->setDateTime(newValue.addSecs(-60));
		}
	}
	if(newValue != value)
	{
		pEndTime->setDateTime(newValue);
	}
	settingValue = false;
}

/*
**	FUNCTION
**		Slot activated when OK button was clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryTimeRangeDialog::okClicked(void)
{
	emit rangeChanged(pStartTime->dateTime(), pEndTime->dateTime());
	deleteLater();
}

/*
**	FUNCTION
**		Slot activated when Cancel button was clicked
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryTimeRangeDialog::cancelClicked(void)
{
	deleteLater();
}

