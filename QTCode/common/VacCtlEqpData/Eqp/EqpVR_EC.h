// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Component Name: VacCtlEqpData librairy
Class: Equipment Vacuum contRol - bakE-out Channel (VR_EC)
Language: C++

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: This file contains the defined classes for the equipment Vacuum contRol - bakE-out Channel (VR_EC)

Limitations: To be used with vacuum framework.

Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------
#ifndef EQPVR_EC_H
#define	EQPVR_EC_H

//	VR_EC - Vacuum contRol - bakE-out Channel 

#include "Eqp.h"

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_NOEQP				255,192,203		//pink
#define COLOR_INVALID			63,127,255  	//blue
#define COLOR_OFF				255,255,255		//white
#define COLOR_ON				0,204,0			//green
//#define	COLOR_ERROR				255, 0, 0	//finally red not used for error
#define	COLOR_ERROR				220, 80, 200	//darkviolet
#define	COLOR_WARNING			255,165,0		//orange

#define	COLOR_SERVICES			255,165,0		//orange

//	Main state bits to be analyzed
#define	RR1_VALID		(0x4000) // 0x40 00
#define	RR1_LOCAL		(0x1000) // 0x10 00
#define	RR1_ON			(0x0200) // 0x02 00
#define	RR1_OFF			(0x0100) // 0x01 00
#define	RR1_ERROR		(0x0080) // 0x00 80
#define	RR1_WARNING		(0x0040) // 0x00 40
#define	RR1_PCS			(0x0020) // 0x00 20
#define	RR1_SAFE		(0x0020) // 0x00 20
#define	RR1_MANUAL		(0x0400) // 0x04 00  00 00
#define	RR1_ACTIVE		(0x0200) // 0x02 00

class VACCTLEQPDATA_EXPORT EqpVR_EC : public Eqp
{
	Q_OBJECT

public:
	EqpVR_EC(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVR_EC(const EqpVR_EC &src) : Eqp(src) {}
#endif
	~EqpVR_EC();

	virtual void setActive(DataEnum::DataMode mode, bool active);

	virtual void getMaster(QString &masterDp); // no master channel , int &masterChannel);
	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isComAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? comAlarmReplay : comAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	virtual unsigned getErrorSt() const { return errorStOnline; }
	virtual unsigned getRecipeSt() const { return recipeStOnline; }
	virtual int getPowerSt() const { return powerStOnline; }
	virtual double getDegCSt() const { return degCStOnline; }
	virtual double getDegCSpSt() const { return degCSpStOnline; }

	public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE 
	unsigned	rr1Online;
	unsigned	rr1Replay;

	// Last known value of the temperature set point value
	double degCSpStOnline;
	double degCSpStReplay;

	// Last knwon state of TCP connection to mobile (true == alarm) for online mode
	bool		comAlarmOnline;
	bool		comAlarmReplay;

	// Last known value of error code status for online mode
	unsigned	errorStOnline;
	unsigned	errorStReplay;

	// Last known value of recipe number
	unsigned	recipeStOnline;

	// Last known value of the channel output power %
	int	powerStOnline;

	// Last known value of temperature process value 
	double degCStOnline;
	double degCStReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, bool comAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, double degCSt, unsigned recipe, bool comAlarm, QString &string, bool isHistoryState = false);
	
private:

#endif
};


#endif	// EQPVR_EC_H


