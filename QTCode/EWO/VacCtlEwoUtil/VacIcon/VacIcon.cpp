//	Implementation of VacIcon class
/////////////////////////////////////////////////////////////////////////////////

#include "Eqp.h"
#include "VacIcon.h"
//#include "VacIconToolTip.h"

#include "VacMainView.h"
#include "Eqp.h"
#include "FunctionalType.h"
#include "MobileType.h"

// Different icon types
#include "VacIconVA_RI.h"
#include "VacIconVVS_PS.h"
#include "VacIconVVS_S.h"
#include "VacIconVVF_S.h"
#include "VacIconVVS_LHC.h"
#include "VacIconVLV.h"
#include "VacIconVLV_ANA.h"
#include "VacIconVGM.h"
#include "VacIconVGR.h"
#include "VacIconVGP.h"
#include "VacIconVGI.h"
#include "VacIconVGF.h"
#include "VacIconVGA_VELO.h"
#include "VacIconVGD_VELO.h"
#include "VacIconVPI.h"
#include "VacIconVP_IP.h"
#include "VacIconVPG_SA.h"
#include "VacIconVPG_SA_VG.h"
#include "VacIconVPG_BP.h"
#include "VacIconVPG_STD.h"
#include "VacIconVPG_EA_SPS.h"
#include "VacIconVP_STDIO.h"
#include "VacIconCRYO_TT.h"
#include "VacIconCRYO_TT_SUM.h"
#include "VacIconVGTR.h"
#include "VacIconEXP_AREA.h"
#include "VacIconCOLDEX.h"
#include "VacIconEXT_ALARM.h"
#include "VacIconVPGMPR.h"
#include "VacIconVPGM.h"
#include "VacIconVPT100.h"
#include "VacIconVV_PLUS_PP.h"
#include "VacIconVIES.h"
#include "VacIconVVS_PS_CMW.h"
#include "VacIconVPI_PS_CMW.h"
#include "VacIconVGR_PS_CMW.h"
#include "VacIconVGP_PS_CMW.h"
#include "VacIconVGI_PS_CMW.h"
#include "VacIconVR_ER.h"
#include "VacIconVV_PULSED.h"
#include "VacIconVV_STD_IO.h"
#include "VacIconVV_PUL.h"
#include "VacIconVV_AO.h"
#include "VacIconVP_STD_IO_PP.h"
#include "VacIconVP_STD_IO_TMP.h"
#include "VacIconVP_STD_IO_C0.h"
#include "VacIconVP_TP.h"
#include "VacIconVP_GU.h"
#include "VacIconGaugeMembrane.h"
#include "VacIconGaugePirani.h"
#include "VacIconGaugePenning.h"
#include "VacIconGaugeDualRP.h"
#include "VacIconVPG_6A01.h"
#include "VacIconVPG_6A01_1VVR_NO_VG.h"
#include "VacIconVPG_6A01_1VVR_VG.h"
#include "VacIconVPG_6A01_2VVR_NO_VG.h"
#include "VacIconVPG_6A01_2VVR_VG.h"
#include "VacIconVPG_MBLK.h"
#include "VacIconBGI_6B01.h"
#include "VacIconINJ_6B02.h"
#include "VacIconVPG_6E01.h"
#include "VacIconVRJ_TC.h"
#include "VacIconVPN.h"
//#include "VacIconVG_DP.h"
#include "VacIconVG_DP_VGF_RP.h"
#include "VacIconVG_DP_VGM.h"
#include "VacIconVPS.h"

#include "VacIconVPR_VELO.h"
#include "VacIconVPT_VELO.h"

#include "VacIconVOPS_VELO.h"
#include "VacIconVINT_VELO.h"
#include "VacIconVPRO_VELO.h"

#include "VacIconMP_ACCESS.h"
#include "VacIconSECT_VPI_SUM.h"
#include "VacIconBEAM_Intens.h"
#include "VacIconVBAR.h"

#include "VacIconVGM_STD.h"
#include "VacIconVGR_STD.h"
#include "VacIconVGP_STD.h"
#include "VacIconVGI_STD.h"
#include "VacIconVGF_STD_RP.h"
#include "VacIconVGF_STD_RI.h"
#include "VacIconVG_A_RO.h"
#include "VacIconVOPS_2PS.h"

#include "VacIconVPC_HCCC.h"

#include "VacIconVG_PT_P.h"
#include "VacIconV8DI_FE.h"

#include <qapplication.h>
#include <qpainter.h>
#include <QMouseEvent>
#include <QToolTip>

/*
**	FUNCTION
**		Return instance of icon for given equipment type
**
**	ARGUMENTS
**		type	- equipment type enumeration
**		parent	- parent widget for this icon
**
**	RETURNS
**		Pointer to new instance of icon for given type, or
**		NULL if such type is not supported (yet)
**
**	CAUTIONS
**		It is responsability of caller to delete returned instance
*/
VacIcon *VacIcon::getIcon(VacIconContainer::EqpType type, QWidget *parent)
{
	VacIcon *icon = NULL;
	switch(type)
	{
	case VacIconContainer::VA_RI:
		icon = new VacIconVA_RI(parent);
		break;
	case VacIconContainer::VVS_PS:
		icon = new VacIconVVS_PS(parent);
		break;
	case VacIconContainer::VVS_SPS:
		icon = new VacIconVVS_S(parent);
		break;
	case VacIconContainer::VVF_SPS:
		icon = new VacIconVVF_S(parent);
		break;
	case VacIconContainer::VVS_LHC:
		icon = new VacIconVVS_LHC(parent);
		break;
	case VacIconContainer::VLV:
		icon = new VacIconVLV(parent);
		break;
	case VacIconContainer::VLV_ANA:
		icon = new VacIconVLV_ANA(parent);
		break;
	case VacIconContainer::VV_PULSED:
		icon = new VacIconVV_PULSED(parent);
		break;
	case VacIconContainer::VV_STD_IO:
		icon = new VacIconVV_STD_IO(parent);
		break;
	case VacIconContainer::VV_PUL:
		icon = new VacIconVV_PUL(parent);
		break;
	case VacIconContainer::VV_AO:
		icon = new VacIconVV_AO(parent);
		break;
	case VacIconContainer::VV_PLUS_PP:
		icon = new VacIconVV_PLUS_PP(parent);
		break;
	case VacIconContainer::VIES:
		icon = new VacIconVIES(parent);
		break;
	case VacIconContainer::VV_PS_CMW:
		icon = new VacIconVVS_PS_CMW(parent);
		break;
	case VacIconContainer::VGR_PS_CMW:
		icon = new VacIconVGR_PS_CMW(parent);
		break;
	case VacIconContainer::VGP_PS_CMW:
		icon = new VacIconVGP_PS_CMW(parent);
		break;
	case VacIconContainer::VPI_PS_CMW:
		icon = new VacIconVPI_PS_CMW(parent);
		break;
	case VacIconContainer::VGI_PS_CMW:
		icon = new VacIconVGI_PS_CMW(parent);
		break;

	case VacIconContainer::GaugeMembrane:
		icon = new VacIconGaugeMembrane(parent);
		break;
	case VacIconContainer::GaugePirani:
		icon = new VacIconGaugePirani(parent);
		break;
	case VacIconContainer::GaugePenning:
		icon = new VacIconGaugePenning(parent);
		break;
	case VacIconContainer::GaugeDualRP:
		icon = new VacIconGaugeDualRP(parent);
		break;

	case VacIconContainer::VGM:
		icon = new VacIconVGM(parent);
		break;

	case VacIconContainer::VGTR:
		icon = new VacIconVGTR(parent);
		break;

	case VacIconContainer::VGR:
		icon = new VacIconVGR(parent);
		break;

	case VacIconContainer::VGP:
		icon = new VacIconVGP(parent);
		break;

	case VacIconContainer::VGI:
		icon = new VacIconVGI(parent);
		break;

	case VacIconContainer::VGF:
		icon = new VacIconVGF(parent);
		break;

	case VacIconContainer::VG_A_RO:
		icon = new VacIconVG_A_RO(parent);
		break;

	case VacIconContainer::VOPS_2_PS:
		icon = new VacIconVOPS_2PS(parent);
		break;

	case VacIconContainer::VPGMPR:
		icon = new VacIconVPGMPR(parent);
		break;

	case VacIconContainer::VGA_VELO:
		icon = new VacIconVGA_VELO(parent);
		break;

	case VacIconContainer::VGD_VELO:
		icon = new VacIconVGD_VELO(parent);
		break;

	case VacIconContainer::VG_STD_VGM:
		icon = new VacIconVGM_STD(parent);
		break;
	case VacIconContainer::VG_STD_VGC:
		icon = new VacIconVGM_STD(parent);
		break;
	case VacIconContainer::VG_STD_VGR:
		icon = new VacIconVGR_STD(parent);
		break;
	case VacIconContainer::VG_STD_VGP:
		icon = new VacIconVGP_STD(parent);
		break;
	case VacIconContainer::VG_STD_VGI:
		icon = new VacIconVGI_STD(parent);
		break;
	case VacIconContainer::VG_STD_VGF_RP:
		icon = new VacIconVGF_STD_RP(parent);
		break;
	case VacIconContainer::VG_STD_VGF_RI:
		icon = new VacIconVGF_STD_RI(parent);
		break;

	case VacIconContainer::VG_DP_VGF_RP:
		icon = new VacIconVG_DP_VGF_RP(parent);
		break;
	case VacIconContainer::VG_DP_VGM:
		icon = new VacIconVG_DP_VGM(parent);
		break;

	case VacIconContainer::VG_PT_P:
		icon = new VacIconVG_PT_P(parent);
		break;

	case VacIconContainer::VP_IP:
		icon = new VacIconVP_IP(parent);
		break;

	case VacIconContainer::VP_TP:
		icon = new VacIconVP_TP(parent);
		break;

	case VacIconContainer::VPI:
		icon = new VacIconVPI(parent);
		break;

	case VacIconContainer::VPR_VELO:
		icon = new VacIconVPR_VELO(parent);
		break;

	case VacIconContainer::VPT_VELO:
		icon = new VacIconVPT_VELO(parent);
		break;

	case VacIconContainer::MP_ACCESS:
		icon = new VacIconMP_ACCESS(parent);
		break;

	case VacIconContainer::SECT_VPI_SUM:
		icon = new VacIconSECT_VPI_SUM(parent);
		break;

	case VacIconContainer::VPG_SA:
		icon = new VacIconVPG_SA(parent);
		break;
	case VacIconContainer::VPG_SA_VG:
		icon = new VacIconVPG_SA_VG(parent);
		break;

	case VacIconContainer::VPG_BP:
		icon = new VacIconVPG_BP(parent);
		break;

	case VacIconContainer::VPG_STD:
		icon = new VacIconVPG_STD(parent);
		break;
	case VacIconContainer::VPG_MBLK:
		icon = new VacIconVPG_MBLK(parent);
		break;
	case VacIconContainer::VPG_EA_SPS:
		icon = new VacIconVPG_EA_SPS(parent);
		break;

	case VacIconContainer::PROCESS_VPG_6A01:
		icon = new VacIconVPG_6A01(parent);
		break;
	case VacIconContainer::PROCESS_VPG_6A01_1VVR_NO_VG:
		icon = new VacIconVPG_6A01_1VVR_NO_VG(parent);
		break;
	case VacIconContainer::PROCESS_VPG_6A01_1VVR_VG:
		icon = new VacIconVPG_6A01_1VVR_VG(parent);
		break;
	case VacIconContainer::PROCESS_VPG_6A01_2VVR_NO_VG:
		icon = new VacIconVPG_6A01_2VVR_NO_VG(parent);
		break;
	case VacIconContainer::PROCESS_VPG_6A01_2VVR_VG:
		icon = new VacIconVPG_6A01_2VVR_VG(parent);
		break;

	case VacIconContainer::PROCESS_BGI_6B01:
		icon = new VacIconBGI_6B01(parent);
		break;
	case VacIconContainer::PROCESS_INJ_6B02:
		icon = new VacIconINJ_6B02(parent);
		break;

	case VacIconContainer::PROCESS_VPG_6E01:
		icon = new VacIconVPG_6E01(parent);
		break;

	case VacIconContainer::VP_STDIO:
		icon = new VacIconVP_STDIO(parent);
		break;

	case VacIconContainer::VP_STD_IO_PP:
		icon = new VacIconVP_STD_IO_PP(parent);
		break;

	case VacIconContainer::VP_STD_IO_TMP:
		icon = new VacIconVP_STD_IO_TMP(parent);
		break;

	case VacIconContainer::VP_STD_IO_C0:
		icon = new VacIconVP_STD_IO_C0(parent);
		break;

	case VacIconContainer::VPN:
		icon = new VacIconVPN(parent);
		break;

	case VacIconContainer::VPC_HCCC:
		icon = new VacIconVPC_HCCC(parent);
		break;

	case VacIconContainer::VPS:
		icon = new VacIconVPS(parent);
		break;

	case VacIconContainer::VPGM:
		icon = new VacIconVPGM(parent);
		break;

	case VacIconContainer::VP_GU:
		icon = new VacIconVP_GU(parent);
		break;

	case VacIconContainer::VPT100:
		icon = new VacIconVPT100(parent);
		break;
	case VacIconContainer::VRJ_TC:
		icon = new VacIconVRJ_TC(parent);
		break;
	case VacIconContainer::VR_ER:
		icon = new VacIconVR_ER(parent);
		break;

	case VacIconContainer::CRYO_TT:
		icon = new VacIconCRYO_TT(parent);
		break;

	case VacIconContainer::CRYO_TT_SUM:
		icon = new VacIconCRYO_TT_SUM(parent);
		break;

	case VacIconContainer::EXP_AREA:
		icon = new VacIconEXP_AREA(parent);
		break;
	case VacIconContainer::EXT_ALARM:
		icon = new VacIconEXT_ALARM(parent);
		break;
	case VacIconContainer::VBAR:
		icon = new VacIconVBAR(parent);
		break;
	case VacIconContainer::COLDEX:
		icon = new VacIconCOLDEX(parent);
		break;

	case VacIconContainer::BEAM_INT:
		icon = new VacIconBEAM_Intens(parent);
		break;

	case VacIconContainer::VOPS_VELO:
		icon = new VacIconVOPS_VELO(parent);
		break;

	case VacIconContainer::VINT_VELO:
		icon = new VacIconVINT_VELO(parent);
		break;

	case VacIconContainer::VPRO_VELO:
		icon = new VacIconVPRO_VELO(parent);
		break;

	case VacIconContainer::V8DI_FE:
		icon = new VacIconV8DI_FE(parent);
		break;

	default:
		break;
	}
	return icon;
}

/*
**	FUNCTION
**		Return instance of icon for given device
**
**	ARGUMENTS
**		pEqp	- Pointer to device for which icon is requested
**		parent	- parent widget for this icon
**
**	RETURNS
**		Pointer to new instance of icon for given device, or
**		NULL if such type is not supported (yet)
**
**	CAUTIONS
**		It is responsability of caller to delete returned instance
*/
VacIcon *VacIcon::getIcon(Eqp *pEqp, QWidget *parent)
{
	VacIconContainer::EqpType type = (VacIconContainer::EqpType)getIconType(pEqp);
	if(type != VacIconContainer::None)
	{
		VacIcon *pIcon = getIcon(type, parent);
		if(pIcon)
		{
			pIcon->setEqp(pEqp);
		}
		return pIcon;
	}
	return NULL;
}

/*
**	FUNCTION
**		Find icon type for given device without creating icon
**
**	ARGUMENTS
**		pEqp	- Pointer to device for which icon type is requested
***
**	RETURNS
**		Icon type for this device, can be None if device type does
**		not have icon (yet)
**
**	CAUTIONS
**		None
*/
int VacIcon::getIconType(Eqp *pEqp)
{
	VacIconContainer::EqpType type = VacIconContainer::None;
	if(pEqp->getMobileType() != MobileType::Fixed)
	{
		return getMobileIconType(pEqp);
	}
	switch(pEqp->getCtrlFamily())
	{
	case 1:	// FAMILY Valves
		switch(pEqp->getCtrlType())
		{
		case 1:	// Different valves of SPS, including COLDEX
			switch(pEqp->getCtrlSubType())
			{
			case 1:		//	VVS_S
			case 257:	// VVS_S read-only
			case 2:		// VVS_SV
			case 258:	// VVS_SV read-only
			case 12:
				type = VacIconContainer::VVS_SPS;
				break;
			case 3:		// VVF_S
			case 259:	// VVF_S read-only
				type = VacIconContainer::VVF_SPS;
				break;
			case 255:	// COLDEX TODO
				break;
			}
			break;
		case 2:	// STD Valves for Zone Nord, GIS etc., STD valves for LHC
			type = VacIconContainer::VLV;
			{
				const QString attr = pEqp->getAttrValue("SpecialHandling");
				if(!attr.isEmpty())
				{
					if(attr == "1")
					{
						type = VacIconContainer::VV_PLUS_PP;
					}
				}
			}
			break;
		case 12: // VVS with SVCU DP interface
			switch (pEqp->getCtrlSubType()) {
			case 1: // VVS_S_DP VVS with DP interface for SPS
				type = VacIconContainer::VVS_SPS;
				break;
			case 2:
				type = VacIconContainer::VVF_SPS;
				break;
			case 3:
				type = VacIconContainer::VVS_LHC;
				break;
			case 4:
			case 5:
				type = VacIconContainer::VVS_PS;
				break;
			}
			break;
		case 32:	// Gate valves in VELO (VVG_VELO)
		case 33:	// Process valves in VELO (VVP_VELO)
		case 34:	// Safety valves in VELO (VVS_VELO)
			type = VacIconContainer::VLV;
			break;
		case 3:	// VVS for LHC
			type = VacIconContainer::VVS_LHC;
			break;
		case 5:	// VVS for PS
			type = VacIconContainer::VVS_PS;
			break;
		case 4:	// Analog valve for LHC-BGI
			type = VacIconContainer::VLV_ANA;
			break;
		case 6:	// VV_PULSED for SPS
			type = VacIconContainer::VV_PULSED;
			break;
		case 7:	// Standard IO Valve (1 DO, 2 DI)
		case 10:// Standard IO Valve (1 DO, 2 DI) - special valve for VPG_6E01 in Linac2
		case 62:// High Vacuum Valve part of the HCC Controller
			type = VacIconContainer::VV_STD_IO;
			break;
		case 11:// Vacuum Valve pulsed
			type = VacIconContainer::VV_PUL;
			break;
		case 8:	// New analog valve type 8
			type = VacIconContainer::VV_AO;
			break;
		}
		break;

	case 2:	// FAMILY Ion pumps VPI
		type = VacIconContainer::VPI;
		break;

	case 3:	// FAMILY Sublimation pump VPS
		switch(pEqp->getCtrlType())
		{
		case 1:
			switch(pEqp->getCtrlSubType())
			{
			case 2:
				type = VacIconContainer::VPS;
				break;
			}
			break;
		}
		break;

	case 4:	// VP - Vacuum Pumps
		switch(pEqp->getCtrlType())
		{
		case 1:	// VP_STDIO
			switch(pEqp->getCtrlSubType())
			{
			case 1:	// PP in BGI
				type = VacIconContainer::VP_STD_IO_PP;
				break;
			case 2:	// PP + VVR in SPS
				type = VacIconContainer::VP_STDIO;
				break;
			case 3:	// Cryo pump type 0
				type = VacIconContainer::VP_STD_IO_C0;
				break;
			case 11:	// TMP in BGI
				type = VacIconContainer::VP_STD_IO_TMP;
				break;
			}
			break;
		case 2:	// VP_TP (Turbo moleculare pump with profibus interface)
			type = VacIconContainer::VP_TP;
			break;
		case 3:	// VP_PULSED_PP (variant of VP_STDIO)
			type = VacIconContainer::VP_STD_IO_PP;
			break;
		case 4:	// VP_IP (Ion pump with profibus controller)
			type = VacIconContainer::VP_IP;
			break;
		case 5:	// Vacuum Pump - Group Unified
			type = VacIconContainer::VP_GU;
			break;
		case 32:	// VPT_VELO
			type = VacIconContainer::VPT_VELO;
			break;
		case 62:	// VPC - Vacuum Pump Cryogenic
			switch(pEqp->getCtrlSubType())
			{
			case 1:	// VPC with HRS-HCC controller 
				type = VacIconContainer::VPC_HCCC;
				break;
			}
		}
		break;

	case 5:	// FAMILY Roughing pump in VELO
		type = VacIconContainer::VPR_VELO;
		break;

	case 202:	// FAMILY Power supplies for ion pump VRPI TODO
		break;

	case 9:	// FAMILY VG_PT Passive Gauge TPG300 (FB-based)
		switch(pEqp->getCtrlSubType())
		{
		case 1:	// VGP 5E-3 to 5E-9
		case 2:	// VGP 5E-3 to 1.1E-11
			type = VacIconContainer::VG_PT_P;
			break;
		case 3:	// VGR 1E+3 to 8E-4
			type = VacIconContainer::VGR;
			break;
		}
		break;

	case 10:	// FAMILY Pirani gauges VGR
		switch (pEqp->getCtrlType()) {
		case 6: // TYPE VGR_IO
			type = VacIconContainer::GaugePirani;
			break;
		default: // TYPE old VGR
			type = VacIconContainer::VGR;
			break;
		}
		break;

	case 11:	// FAMILY Penning gauges VGP
		switch (pEqp->getCtrlType()) {
		case 6: // TYPE VGP_IO
			type = VacIconContainer::GaugePenning;
			break;
		default: // TYPE old VGP
			type = VacIconContainer::VGP;
			break;
		}
		break;

	case 12:	// FAMILY Full range gauges, Hot Cathod - VGFH
	case 13:	// FAMILY Full range gauges, Cold Cathod - VGFC
		switch (pEqp->getCtrlType()) {
		case 6: // TYPE VGF_IO
			type = VacIconContainer::GaugeDualRP;
			break;
		default: // TYPE old VGF
			type = VacIconContainer::VGF;
			break;
		}
		break;

	case 14:	// FAMILY Ionization gauges VGI
		type = VacIconContainer::VGI;
		break;

	case 15:	// FAMILY Membrane gauges VGM
		switch (pEqp->getCtrlType()) {
		case 6: // TYPE VGM_IO
			type = VacIconContainer::GaugeMembrane;
			break;
		default: // TYPE old VGM
			type = VacIconContainer::VGM;
			break;
		}
		break;

	case 16: //FAMILY Standard Gauge
		switch(pEqp->getCtrlType())
		{
		case 1:	// VG_STD
			switch(pEqp->getCtrlSubType())
			{
			case 1:	// VG Standard IO (0-10V) VGM
				type = VacIconContainer::VG_STD_VGM;
				break;
			case 8: // VG Standard IO (0-10V) VGC Capacitance same widget as membrane
				type = VacIconContainer::VG_STD_VGC;
				break;
			case 2:	// VG Standard IO (0-10V) VGR
				type = VacIconContainer::VG_STD_VGR;
				break;
			case 3:	// VG Standard IO (0-10V) VGP
				type = VacIconContainer::VG_STD_VGP;
				break;
			case 4:	// VG Standard IO (0-10V) VGI
				type = VacIconContainer::VG_STD_VGI;
				break;
			case 5:	// VG Standard IO (0-10V) VGF(R+P)
				type = VacIconContainer::VG_STD_VGF_RP;
				break;
			case 7:	// VG Standard IO (0-10V) VGF(R+I)
				type = VacIconContainer::VG_STD_VGF_RI;
				break;
			}
			break;
		case 21:	// Profibus Gauges VG_DP 
			switch(pEqp->getCtrlSubType())
			{
			case 1:	// Profibus Gauges - Membrane Gauge
				type = VacIconContainer::VG_DP_VGM;
				break;
			case 5:	// Profibus Gauges - Full Range Gauge Pirani(R) + Penning (P)
				type = VacIconContainer::VG_DP_VGF_RP;
				break;
			}
			break;
		case 32:	// Gauge absolute in VELO
			type = VacIconContainer::VGA_VELO;
			break;
		}
		break;
	case 17:	// FAMILY Gauge Others
		switch(pEqp->getCtrlType())
		{
		case 32:	//VGD_VELO
			type = VacIconContainer::VGD_VELO;
			break;
		case 33:
			type = VacIconContainer::VG_A_RO;
			break;
		}
		break;
	case 18:	// FAMILY Pressure transmitter VGTR
		type = VacIconContainer::VGTR;
		break;
	
	case 30: // FAMILY Temperature sensor
		switch(pEqp->getCtrlType())
		{
		case 1:	// Thermocopule VT100
			type = VacIconContainer::VPT100;
			break;
		case 2:	// Patch panel for thermocouples
			type = VacIconContainer::VRJ_TC;
			break;
		}
		break;

	case 32: // FAMILY Velo 
		type = VacIconContainer::VOPS_VELO;
		break;
	case 40: // FAMILY Generic IO
		switch (pEqp->getCtrlType())
		{
		case 1:	// 8 DI 
			type = VacIconContainer::V8DI_FE;
			break;
		}
	case 64: // FAMILY Velo Process
		type = VacIconContainer::VPRO_VELO;
		break;

	case 100: // FAMILY Fixed pumping groups for LHC VPGF
		switch(pEqp->getCtrlType())
		{
		case 1:	// LHC bypass (VPGFA, VPGFB)
			type = VacIconContainer::VPG_BP;
			break;
		case 2:	// LHC standlone (VPGFC, VPGFD, VPGFC for UJ33)
			{
				bool haveVg = false;
				QString attr = pEqp->getAttrValue("VGR1");
				if(!attr.isEmpty())
				{
					haveVg = true;
				}
				else
				{
					attr = pEqp->getAttrValue("VGP1");
					if(!attr.isEmpty())
					{
						haveVg = true;
					}
					else
					{
						attr = pEqp->getAttrValue("VGF");
						if(!attr.isEmpty())
						{
							haveVg = true;
						}
					}
				}
				type = haveVg ? VacIconContainer::VPG_SA_VG : VacIconContainer::VPG_SA;
			}
			break;
		}
		break;

	case 101:	// FAMILY STD fixed pumping group for LHC
		type = VacIconContainer::VPG_STD;
		break;

	case 105:	// FAMILY Fixed pumping groups for SPS (North Area & CNGS)
		type = VacIconContainer::VPG_EA_SPS;
		break;

	case 106:	// FAMILY Fixed Pumping Group
		if (pEqp->getCtrlType() == 1) { // TYPE VPG 6A01
			if (pEqp->getCtrlSubType() == 1)
			{
				type = VacIconContainer::PROCESS_VPG_6A01;
			}
			else
			{
				type = findIconTypeForVPG_6A01(pEqp);
			}
		}
		else if (pEqp->getCtrlType() == 2) {
			if (pEqp->getCtrlSubType() == 1)
			{
				type = VacIconContainer::VPG_MBLK;
			}
		}
		else if (pEqp->getCtrlType() == 11) { // VPGF_LHC
			type = VacIconContainer::VP_GU;
		}
		break;

	case 107: // FAMILY  Injection process
		switch(pEqp->getCtrlType())
		{
		case 1:	// BGI_6B01
			switch(pEqp->getCtrlSubType())
			{
			case 1:
			case 2:
				type = VacIconContainer::PROCESS_BGI_6B01;
				break;
			}
			break;
		case 2:	// INJ_6B02
			type = VacIconContainer::PROCESS_INJ_6B02;
			break;
		}
		break;

	case 110:	// FAMILY VPG_6E01
		type = VacIconContainer::PROCESS_VPG_6E01;
		break;

	case 121: // FAMILY Bakeout 
		type = VacIconContainer::VR_ER;
		break;

	case 150:	// FAMILY Interlocks VACOK TODO
		switch(pEqp->getCtrlType())
		{
		case 32:	// VELO interlock
			type = VacIconContainer::VINT_VELO;
			break;
		default:
			break;
		}
		break;

	case 151: // FAMILY Vacuum Alarm
		switch (pEqp->getCtrlType()) {
		case 1: // Vacuum Alarm Relay Ion pump controller
			type = VacIconContainer::VA_RI;
			break;
		default:
			break;
		}
		break;

	case 205:	// FAMILY COLDEX
		type = VacIconContainer::COLDEX;
		break;

	case 255:	// FAMILY PLC-related
		switch(pEqp->getCtrlType())
		{
		case 1:	// Master PLC S7-400
		case 2:	// Master PLC S7-300
		case 3:	// Slave PLC S7-300
			break;
		}
		break;

	case 254:	// FAMILY Auxilliary types added by SUP TODO
		switch(pEqp->getCtrlType())
		{
		case 1:	// Vacuum barier between two sectors - no DP
			type = VacIconContainer::VBAR;
			break;
		case 2:	// Experimental Area EXP_AREA
			type = VacIconContainer::EXP_AREA;
			break;
		case 3:	// Alarms of VELO - puer alarms DP
			break;
		case 4:	// Cryo Thermometers VCRYO_TT
			type = VacIconContainer::CRYO_TT;
			break;
		case 5:	// LHC Beam Intensity
			type = VacIconContainer::BEAM_INT;
			break;
		case 6:	// Summary of Cryo Thermometers in one CRYO cell CRYO_TT_SUM
			type = VacIconContainer::CRYO_TT_SUM;
			break;
		case 7:	// Summary valves in PSB (2 or 4 real valves) VVS_PSB_SUM
			type = VacIconContainer::VVS_PS;
			break;
		case 8:	// Alarm for external target
			type = VacIconContainer::EXT_ALARM;
			break;
		case 70:	// VIES - solenoid
			type = VacIconContainer::VIES;
			break;
		case 80:	// Equipment of PSR, data read from CMW
			switch(pEqp->getCtrlSubType())
			{
			case 1:	// VVS
				type = VacIconContainer::VV_PS_CMW;
				break;
			case 2:	// VGR
				type = VacIconContainer::VGR_PS_CMW;
				break;
			case 3:	// VGP
				type = VacIconContainer::VGP_PS_CMW;
				break;
			case 4:	// VPI
				type = VacIconContainer::VPI_PS_CMW;
				break;
			case 5:	// VGI
				type = VacIconContainer::VGI_PS_CMW;
				break;
			}
			break;
		case 90:	// VPN
			type = VacIconContainer::VPN;
			break;
		}
		break;
	}
	if(type == VacIconContainer::None)	// Still can be auxilliary devices
	{
		switch(pEqp->getFunctionalType())
		{
		case FunctionalType::MP_ACCESS:
			type = VacIconContainer::MP_ACCESS;
			break;
		case FunctionalType::SECT_VPI_SUM:
			type = VacIconContainer::SECT_VPI_SUM;
			break;
		}
	}
	return type;
}

/*
**	FUNCTION
**		Find icon type for given mobile device without creating icon
**
**	ARGUMENTS
**		pEqp	- Pointer to device for which icon type is requested
***
**	RETURNS
**		Icon type for this device, can be None if device type does
**		not have icon (yet)
**
**	CAUTIONS
**		None
*/
int VacIcon::getMobileIconType(Eqp *pEqp)
{
	VacIconContainer::EqpType type = VacIconContainer::None;
	switch(pEqp->getFunctionalType())
	{
	case FunctionalType::VPGM:
		type = VacIconContainer::VPGM;
		break;
	case FunctionalType::VPGMPR:
		type = VacIconContainer::VPGMPR;
		break;
	case FunctionalType::VPG:
		type = VacIconContainer::VP_GU;
		break;
	case FunctionalType::VRE:
		type = VacIconContainer::VR_ER;
		break;
	default:
		break;
	}
	return type;
}

VacIconContainer::EqpType VacIcon::findIconTypeForVPG_6A01(Eqp *pEqp)
{
	VacIconContainer::EqpType type = VacIconContainer::None;

	// How many VVRs related to VPG
	int nVvrs = 0;
	QString attrValue = pEqp->getAttrValue("VVR1");
	if(!attrValue.isEmpty())
	{
		nVvrs++;
	}
	attrValue = pEqp->getAttrValue("VVR2");
	if(!attrValue.isEmpty())
	{
		nVvrs++;
	}

	// Are there any gauges on group?
	bool hasGauges = false;
	attrValue = pEqp->getAttrValue("VGR1");
	if(!attrValue.isEmpty())
	{
		hasGauges = true;
	}
	attrValue = pEqp->getAttrValue("VGP1");
	if(!attrValue.isEmpty())
	{
		hasGauges = true;
	}
	attrValue = pEqp->getAttrValue("VGF");
	if(!attrValue.isEmpty())
	{
		hasGauges = true;
	}

	// Decide
	if(hasGauges)
	{
		switch(nVvrs)
		{
		case 0:	// Not possible? Take default
			type = VacIconContainer::PROCESS_VPG_6A01;
			break;
		case 1:
			type = VacIconContainer::PROCESS_VPG_6A01_1VVR_VG;
			break;
		case 2:
			type = VacIconContainer::PROCESS_VPG_6A01_2VVR_VG;
			break;
		}
	}
	else
	{
		switch(nVvrs)
		{
		case 0:
			type = VacIconContainer::PROCESS_VPG_6A01;
			break;
		case 1:
			type = VacIconContainer::PROCESS_VPG_6A01_1VVR_NO_VG;
			break;
		case 2:
			type = VacIconContainer::PROCESS_VPG_6A01_2VVR_NO_VG;
			break;
		}
	}
	return type;
}

/////////////////////////////////////////////////////////////////////////////////
////////// Construction/destruction

VacIcon::VacIcon(QWidget *parent, Qt::WindowFlags f)
	: QWidget(parent, f)
{
	if(VacMainView::getInstance())
	{
		setFont(VacMainView::getInstance()->font());
	}
	direction = VacIconContainer::Up;
	beamDirection = VacIconContainer::LeftToRight;
	pipeColor.setRgb(0, 0, 255);
	pipeWidth = 3;
	mode = DataEnum::Online;
	connected = false;
	drawConnectionPipe = true;
	QObject::connect(&connection, SIGNAL(stateChanged()), this, SLOT(forceRedraw()));
	QObject::connect(&connection, SIGNAL(selectChanged()), this, SLOT(forceRedraw()));
	/*
	const QList<VacMainView *> &mainList = VacMainView::getInstanceList();
	for(int idx = 0 ; idx < mainList.count() ; idx++)
	{
		VacMainView *pInstance = mainList.at(idx);
		if(pInstance->isPvssEvents())
		{
			QObject::connect(this, SIGNAL(mouseDown(int, int, int, int, int, const char *)),
				pInstance, SIGNAL(dpIconMouseDown(int, int, int, int, int, const char *)));
		}
	}
	*/
	if(!isVirtualDevice())
	{
		VacMainView *pMainView = VacMainView::getInstance();
		if(pMainView)
		{
			QObject::connect(this, SIGNAL(mouseDown(int, int, int, int, int, const char *)),
				pMainView, SIGNAL(dpIconMouseDown(int, int, int, int, int, const char *)));
		}
	}
	//Blinking
	blinkingPeriod = 1000; //ms
	isBlinking = false;
	isBlinkVisible = true;
	pTimer = new QTimer(this);
	pTimer->setSingleShot(false);
	QObject::connect(pTimer, SIGNAL(timeout(void)), this, SLOT(timerBlinkDone(void)));
	pTimer->start(blinkingPeriod / 2);
}

VacIcon::~VacIcon()
{
	QObject::disconnect(&connection, SIGNAL(selectChanged()), this, SLOT(forceRedraw()));
	QObject::disconnect(&connection, SIGNAL(stateChanged()), this, SLOT(forceRedraw()));
	QObject::disconnect(pTimer, SIGNAL(timeout(void)), this, SLOT(timerBlinkDone(void)));
}

void VacIcon::mousePressEvent(QMouseEvent *pEvent)
{
	if(isVirtualDevice())
	{
		return;
	}
	Eqp *pEqp = getEqp();
	if(pEqp)
	{
		QPoint mousePoint(pEvent->x(), pEvent->y());
		QPoint screenPoint = mapToGlobal(mousePoint);
		int buttonNbr = 0;
		switch(pEvent->button())
		{
		case Qt::LeftButton:
			buttonNbr = 1;
			break;
		case Qt::RightButton:
			buttonNbr = 2;
			break;
		default:
			break;
		}
		if(buttonNbr)
		{
			emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), 0, pEqp->getDpName());
		}
	}
}

void VacIcon::mouseDoubleClickEvent(QMouseEvent * /* pEvent */)
{
	if(isVirtualDevice())
	{
		return;
	}
	Eqp *pEqp = getEqp();
	if(pEqp)
	{
		VacMainView *pMainView = VacMainView::getInstance();
		if(pMainView)
		{
			pMainView->emitEqpDoubleClick(pEqp->getDpName(), mode);
		}
	}
}

/*
**	FUNCTION
**		Set direction for this icon
**
**	ARGUMENTS
**		newDirection	- New icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		This implementation only stores direction in class instance
**		and forces icon to redraw. If, for example, icon geometry would
**		change as a consequence of direction change - subclass shall reimplement
**		this method making appropriate changes.
*/
void VacIcon::setDirection(VacIconContainer::Direction newDirection)
{
	direction = newDirection;
	forceRedraw();
}

/*
**	FUNCTION
**		Set beam direction for this icon
**
**	ARGUMENTS
**		newDirection	- New beam direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		This implementation only stores direction in class instance
**		and forces icon to redraw. If, for example, icon geometry would
**		change as a consequence of direction change - subclass shall reimplement
**		this method making appropriate changes.
*/
void VacIcon::setBeamDirection(VacIconContainer::BeamDirection newDirection)
{
	beamDirection = newDirection;
	forceRedraw();
}

/*
**	FUNCTION
**		Set vacuum pipe color for this icon
**
**	ARGUMENTS
**		newDirection	- New beam direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		This implementation only stores color in class instance
**		and forces icon to redraw. If, for example, icon image does not depend
**		on this color - subclass can reimplement this method. Still reimplemented
**		method shall save new color in icon instance.
*/
void VacIcon::setPipeColor(const QColor &newColor)
{
	pipeColor = newColor;
	forceRedraw();
}

/*
**	FUNCTION
**		Set vacuum pipe width for this icon
**
**	ARGUMENTS
**		width	- New vacuum pipe width
**
**	RETURNS
**		None
**
**	CAUTIONS
**		This implementation only stores width in class instance
**		and forces icon to redraw. If, for example, icon image does not depend
**		on this width - subclass can reimplement this method. Still reimplemented
**		method shall save new width in icon instance.
*/
void VacIcon::setPipeWidth(int width)
{
	pipeWidth = width;
	forceRedraw();
}

/*
**	FUNCTION
**		Force redraw of this icon if it is visible
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIcon::forceRedraw(void)
{
	if(!isVisible())
	{
		return;
	}
	/*
	QPaintEvent *pEvent = new QPaintEvent(rect(), false);
	QApplication::postEvent(this, pEvent);
	*/
	update();
}

/*
**	FUNCTION
**		Draw icon in selected state: thin rectangle on icon border
**
**	ARGUMENTS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIcon::drawSelection(QPainter &painter)
{
	/*
	painter.setPen(Qt::black);
	painter.drawRect(0, 0, width(), height());
	*/
	//painter.setRasterOp(Qt::XorROP);
	QPen pen(Qt::red, 3);
	painter.setPen(pen);
	painter.drawRect(0, 0, width() - 1, height() - 1);
}

bool VacIcon::event(QEvent *pEvent)
{
	if(pEvent->type() == QEvent::ToolTip)
	{
		QHelpEvent *pHelpEvent = static_cast<QHelpEvent *>(pEvent);
		QString toolTipText;
		QRect toolTipRect;
		if(getToolTip(pHelpEvent->pos(), toolTipText, toolTipRect))
		{
			QToolTip::showText(pHelpEvent->globalPos(), toolTipText, this, toolTipRect);
		}
		else
		{
			QToolTip::hideText();
			pEvent->ignore();
		}
		return true;
	}
	return QWidget::event(pEvent);
}

/**
@Brief SLOT Blinking behavior
*/

void VacIcon::timerBlinkDone(void) {
	isBlinkVisible = !isBlinkVisible;
	update();
}

