//	Implemenation of LineViewer class
///////////////////////////////////////////////////////////////////////////

#include "LineViewer.h"
#include "DataPool.h"
#include "Eqp.h"

#include <qpainter.h>
#include <QMouseEvent>

#include <math.h>

#define MARGIN	(5)

////////////////////////// Construction/destruction ///////////////////////////////

LineViewer::LineViewer(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	setMinimumSize(400, 400);
	setMouseTracking(true);
}

LineViewer::~LineViewer()
{
}

//////////////////////// Slots ///////////////////////////////////////////////////
void LineViewer::selectionChanged(void)
{
	update();
}


/////////////////////// Drawing /////////////////////////////////////////////////
void LineViewer::paintEvent(QPaintEvent * /* pEvent */)
{
	int	w = width(), h = height();
	if(((w - 2 * MARGIN) <= 0) || ((h - 2 * MARGIN) <= 0))
	{
		return;
	}

	DataPool	&pool = DataPool::getInstance();
	QList<BeamLine *> &lines = pool.getLines();
	if(lines.isEmpty())
	{
		return;
	}

	// Find overall geography limits
	minX = maxX = minY = maxY = 0.0;
	findLimits(lines);
	if((minX == maxX) && (minY == maxY))
	{
		return;
	}

	// Calculate coefficient for converting world coordinates to screen coordinates
	calculateCoeff(w, h);

	// Draw all visible lines
	QPainter painter(this);
	painter.setPen(Qt::black);
	drawLines(lines, painter);
}


void LineViewer::findLimits(QList<BeamLine *> &lines)
{
	BeamLine	*pLine = NULL;
	int idx = 0;
	for(idx = lines.count() - 1 ; idx >= 0 ; idx--)
	{
		pLine = lines.at(idx);
		if(!pLine->isHidden())
		{
			break;
		}
	}
	if(idx < 0)
	{
		return;
	}
	minX = pLine->getMinX();
	minY = pLine->getMinY();
	maxX = pLine->getMaxX();
	maxY = pLine->getMaxY();
	for(--idx ; idx >= 0 ; idx--)
	{
		pLine = lines.at(idx);
		if(pLine->isHidden())
		{
			continue;
		}
		if(pLine->getMinX() < minX)
		{
			minX = pLine->getMinX();
		}
		if(pLine->getMaxX() > maxX)
		{
			maxX = pLine->getMaxX();
		}
		if(pLine->getMinY() < minY)
		{
			minY = pLine->getMinY();
		}
		if(pLine->getMaxY() > maxY)
		{
			maxY = pLine->getMaxY();
		}
	}
}

void LineViewer::calculateCoeff(int w, int h)
{
	float coefX = (minX != maxX) ? (float)(w - 2 * MARGIN) / (maxX - minX) : 0.0,
			coefY = (minY != maxY) ? (float)(h - 2 * MARGIN) / (maxY - minY) : 0.0;
	if(coefX == 0.0)
	{
		coef = coefY;
	}
	else if(coefY == 0.0)
	{
		coef = coefX;
	}
	else
	{
		coef = coefX > coefY ? coefY : coefX;
	}
}

void LineViewer::drawLines(QList<BeamLine *> &lines, QPainter &painter)
{
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		BeamLine *pLine = lines.at(idx);
		if(!pLine->isHidden())
		{
			drawLine(pLine, painter);
		}
	}
}

void LineViewer::drawLine(BeamLine *pLine, QPainter &painter)
{
	int	x = -1, y = -1;

	QList<BeamLinePart *> &parts = pLine->getParts();
	for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			Eqp *pEqp = eqpList.at(eqpIdx);
			drawEqp(painter, pEqp, x, y);
		}
	}
}

void LineViewer::drawEqp(QPainter &painter, Eqp *pEqp, int &x, int &y)
{
	int	areaHeight = height();
	int	eqpX = MARGIN + (int)rint((pEqp->getStartX() - minX) * coef);
	int	eqpY = areaHeight - MARGIN - (int)rint((pEqp->getStartY() - minY) * coef);
	if((x < 0) && (y < 0))	// The very first point
	{
		x = eqpX;
		y = eqpY;
	}
	else if((x != eqpX) || (y != eqpY))
	{
		painter.drawLine(x, y, eqpX, eqpY);
		x = eqpX;
		y = eqpY;
	}
	if(pEqp->getLength() == 0.0)
	{
		return;
	}
	eqpX = MARGIN + (int)rint((pEqp->getEndX() - minX) * coef);
	eqpY = areaHeight - MARGIN - (int)rint((pEqp->getEndY() - minY) * coef);
	if((x != eqpX) || (y != eqpY))
	{
		painter.drawLine(x, y, eqpX, eqpY);
		x = eqpX;
		y = eqpY;
	}
}

/////////////////////// Mouse motion /////////////////////////////////////////////////

void LineViewer::mouseMoveEvent(QMouseEvent *pEvent)
{
	Eqp *pEqp = findClosestDevice(pEvent->x(), pEvent->y());
	emit eqpSelected(pEqp);
}

Eqp *LineViewer::findClosestDevice(int x, int y)
{
	Eqp	*pResultEqp = NULL;
	int	distance = width() + height();

	DataPool &pool = DataPool::getInstance();
	QList<BeamLine *> &lines = pool.getLines();
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		if(pLine->isHidden())
		{
			continue;
		}
		QList<BeamLinePart *> &parts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			QList<Eqp *> &eqpList = pPart->getEqpList();
			for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
			{
				Eqp *pEqp = eqpList.at(eqpIdx);
				if(checkDeviceDistance(pEqp, x, y, distance))
				{
					pResultEqp = pEqp;
				}
			}
		}
	}
	return pResultEqp;
}

bool LineViewer::checkDeviceDistance(Eqp *pEqp, int x, int y, int &distance)
{
	int	areaHeight = height();
	int	eqpX = MARGIN + (int)rint((pEqp->getStartX() - minX) * coef);
	int	eqpY = areaHeight - MARGIN - (int)rint((pEqp->getStartY() - minY) * coef);
	int eqpDistance = abs(eqpX - x) + abs(eqpY - y);
	if(eqpDistance < 4)
	{
		if(eqpDistance < distance)
		{
			distance = eqpDistance;
			return true;
		}
	}
	if(pEqp->getLength() == 0.0)
	{
		return false;
	}
	eqpX = MARGIN + (int)rint((pEqp->getEndX() - minX) * coef);
	eqpY = areaHeight - MARGIN - (int)rint((pEqp->getEndY() - minY) * coef);
	eqpDistance = abs(eqpX - x) + abs(eqpY - y);
	if(eqpDistance < 4)
	{
		if(eqpDistance < distance)
		{
			distance = eqpDistance;
			return true;
		}
	}
	return false;
}
