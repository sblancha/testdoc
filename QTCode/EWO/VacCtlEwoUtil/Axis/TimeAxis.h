#ifndef TIMEAXIS_H
#define	TIMEAXIS_H

// Horizontal time axis

#include <QDateTime>
#include <QRect>
#include <QPainter>

#include <stdio.h>

#include <QFontMetrics>

class TimeAxis
{
public:
	TimeAxis();
	~TimeAxis();

	void setGraphArea(const QRect &rect);
	void setLimits(const QDateTime &newMin, const QDateTime &newMax);
	void setLogScale(bool flag);
	void setLogAxisUnit(unsigned unit);

	// Value returned by timeToScreen method
	typedef enum
	{
		BeforeMin = -1,
		WithinRange = 0,
		AfterMax = 1
	} TimeRange;

	TimeRange timeToScreen(const QDateTime &value, int &screenX) const;
	TimeRange timeAsMsecToScreen(qint64 value, int &screenX) const;
	QDateTime screenToTime(int screenX, QString &label);
	int findLabelHeight(const QFontMetrics &fontMetrics);
	bool prepare(const QFontMetrics &fontMetrics);
	void draw(QPainter &painter);

	// Access
	inline const QDateTime &getMin(void) const { return min; }
	inline const QDateTime &getMax(void) const { return max; }
	inline const QRect &getGraphArea(void) const { return graphArea; }
	inline int getScaleLeft(void) const { return scaleLeft; }
	inline int getScaleRight(void) const { return scaleRight; }
	inline int getAxisLocation(void) const { return axisLocation; }
	inline bool isLogarithmic(void) const { return logarithmic; }
	inline unsigned getLogAxisUnit(void) const { return logAxisUnit; }
	inline bool isAutoLogAxisUnit(void) const { return autoLogAxisUnit; }
	inline void setGrid(bool flag) { grid = flag; }
	inline void setGridStyle(int style) { gridStyle = style; }
	inline void setTickSize(int size) { tickSize = size; }
	inline void setTickOutside(bool flag) { tickOutside = flag; }

	void dump(FILE *pFile);

protected:
	// Axis minimum
	QDateTime	min;

	// Axis maximum
	QDateTime	max;

	// Axis minimum as quint64 (to speed up time -> screen conversion)
	qint64		minAsMsec;

	// Axis maximum as quint64 (to speed up time -> screen conversion)
	qint64		maxAsMsec;

	// Logarithm of maximum - only used in logarithmic scale mode
	double		logMax;

	// Time of start label to draw
	QDateTime	startLabelTime;

	// Time between labels [sec]
	int			labelStep;

	// Graph area to use
	QRect		graphArea;

	// X-coordinate of left scale edge
	int			scaleLeft;

	// X-coordinate of right scale edge
	int			scaleRight;

	// Y-location of axis line
	int			axisLocation;

	// Line style for grid drawing
	int			gridStyle;

	// Size of ticks between grid lines
	int			tickSize;

	// unit of log axis - only used in logarithmic scale mode [sec]
	unsigned		logAxisUnit;

	// Flag indicating if labels shall be drawn (sometimes there can be not enough
	// space for labels)
	bool		showLabels;

	// Flag indicating if logarithmic scale for time shall be used
	bool		logarithmic;

	// Flag indicating if logAxisUnit shall be caltulated automatically
	bool		autoLogAxisUnit;

	// Flag inidcating if ticks shall be drawn outside graph area
	bool		tickOutside;

	// Flag indicating if grid shall be drawn
	bool		grid;

	// Flag indicating if axis parameters shall be recalculated
	bool		needPrepare;

	void calculateLinAxis(const QFontMetrics &fontMetrics);
	void calculateLogAxis(const QFontMetrics &fontMetrics);

	void drawLin(QPainter &painter);
	void drawLog(QPainter &painter);
	void drawLabel(QPainter &painter, QString &label, int x);

	void labelExtents(const QFontMetrics &fontMetrics, QString &label, QSize &size);
	void makeLabel(QString &label, const QDateTime &value);
	int nextLabelStep(int step);
	int nextScaleStep(int step, bool use60Base);
};

#endif	// TIMEAXIS_H
