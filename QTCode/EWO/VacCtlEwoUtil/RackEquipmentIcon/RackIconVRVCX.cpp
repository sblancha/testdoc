#include "RackIconVRVCX.h"

#include "DataPool.h"

#include "qpainter.h"

RackIconVRVCX::RackIconVRVCX(QWidget *parent, Qt::WindowFlags f) :
RackIcon(parent, f)
{
	pEqp = NULL;
	//QWidget::setAttribute(Qt::WA_TransparentForMouseEvents);
	//setFixedSize(21, 21); see about this later
}

RackIconVRVCX::~RackIconVRVCX()
{
	disconnect();
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRVCX::disconnect(void)
{
	bool result = false;
	if (connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	
	connected = false;
	
	return result;
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRVCX::connect(void)
{
	if (connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	//dpes.append("PR");
	connection.connect(pEqp, dpes, DataEnum::Online);
	connected = true;
	return true;


}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void RackIconVRVCX::setEqp(Eqp *pEqp)
{
	disconnect();
	DataPool &pool = DataPool::getInstance();
	if (pEqp != NULL)
	{
		//Q_ASSERT_X(pEqp->inherits("EqpVRPI"), "RackIconVRPIT::setEqp", pEqp->getDpName());
		//this->pEqp = (EqpVR_GT *)pEqp;
	}
	forceRedraw();
}

void RackIconVRVCX::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.scale(scale, scale);
	painter.setBrush(QColor(220, 220, 220));
	QFont calibriSmallBold("Calibri", 7, QFont::Bold);
	QFont calibriSmall("Calibri", 8);
	int textFlags;

	QPen selectionPen = QPen(Qt::red);
	selectionPen.setWidth(3);
	if (isSelected)
	{
		painter.setPen(selectionPen);
	}
	else
	{
		painter.setPen(Qt::black);
	}
	QRect equipmentRect(0, 0, this->sizeX, this->sizeY);
	//painter.setPen(Qt::black);
	painter.drawRect(equipmentRect);
	painter.setPen(Qt::black);

	painter.setFont(calibriSmallBold);
	textFlags = Qt::AlignHCenter | Qt::AlignTop | Qt::TextWrapAnywhere;
	painter.drawText(equipmentRect, textFlags, this->equipmentName);

	painter.setFont(calibriSmall);
	QString valveCablesLine = attributes.value("ValveCable1");
	QString valveCableTemp;
	for (int i = 2; i <= 4; i++)
	{
		valveCableTemp = attributes.value("ValveCable" + i);
		valveCablesLine += " - " + valveCableTemp;
	}
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 13, equipmentRect.width(), equipmentRect.height() - 13, textFlags, valveCablesLine);

	valveCablesLine = attributes.value("ValveCable5") + " - " + attributes.value("ValveCable6");
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 24, equipmentRect.width(), equipmentRect.height() - 24, textFlags, valveCablesLine);
	
}


