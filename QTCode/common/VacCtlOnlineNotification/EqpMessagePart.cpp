//	Implementation of EqpMessagePart class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpMessagePart.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

EqpMessagePart::EqpMessagePart(MessagePartType type)
{
	this->type = type;
}

EqpMessagePart::~EqpMessagePart()
{
}

/*
**	FUNCTION
**		Parse given key string, representing message part type
**
**	ARGUMENTS
**		key		- Pointer to string to be parsed
**		errMsg	- variable where error message will be written in case of parsing error
**
**	RETURNS
**		Decoded type of message part
**
**	CAUTIONS
**		None
*/
EqpMessagePart::MessagePartType EqpMessagePart::decodeType(const QString &key, QString &errMsg)
{
	QString pureKey(key.toLatin1().trimmed().toUpper());
	if(pureKey.isEmpty())
	{
		errMsg = "Empty key";
		return None;
	}
	MessagePartType result = None;
	if(pureKey == "EQP")
	{
		result = MessagePartEqpName;
	}
	else if(pureKey == "SECTOR")
	{
		result = MessagePartSectorName;
	}
	else if(pureKey == "MAINPART")
	{
		result = MessagePartMainPartName;
	}
	else if(pureKey == "VALUE")
	{
		result = MessagePartValue;
	}
	else if(pureKey == "COUNT")
	{
		result = MessagePartGroupCount;
	}
	else if(pureKey == "ERROR")
	{
		result = MessagePartErrorText;
	}
	else if (pureKey == "WARNING")
	{
		result = MessagePartWarningText;
	}
	else if(pureKey == "LOWLIMIT")
	{
		result = MessagePartLowLimit;
	}
	else if(pureKey == "HIGHLIMIT")
	{
		result = MessagePartHighLimit;
	}
	else if(pureKey == "EVENTTIME")
	{
		result = MessagePartEventTime;
	}
	else if(pureKey == "SENDTIME")
	{
		result = MessagePartEventTime;
	}
	else if(pureKey == "EVENTVALUE")
	{
		result = MessagePartEventValue;
	}
	else if(pureKey == "MINDURATION")
	{
		result = MessageTypeMinDuration;
	}
	else if (pureKey == "SLAVES")
	{
		result = MessagePartSlaves;
	}
	else
	{
		errMsg = "Unknown key {";
		errMsg += key.toLatin1().trimmed();
		errMsg += "}";
	}
	return result;
}

