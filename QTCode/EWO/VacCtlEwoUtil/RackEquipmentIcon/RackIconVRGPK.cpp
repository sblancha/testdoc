#include "RackIconVRGPK.h"

#include "DataPool.h"

#include "qpainter.h"

RackIconVRGPK::RackIconVRGPK(QWidget *parent, Qt::WindowFlags f) :
RackIcon(parent, f)
{
	pEqp = NULL;
	//setFixedSize(21, 21); see about this later
}

RackIconVRGPK::~RackIconVRGPK()
{
	disconnect();
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRGPK::disconnect(void)
{
	bool result = false;
	if (connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	
	connected = false;
	
	return result;
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRGPK::connect(void)
{
	if (connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	//dpes.append("PR");
	connection.connect(pEqp, dpes, DataEnum::Online);
	connected = true;
	return true;


}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void RackIconVRGPK::setEqp(Eqp *pEqp)
{
	disconnect();
	DataPool &pool = DataPool::getInstance();
	if (pEqp != NULL)
	{
		//Q_ASSERT_X(pEqp->inherits("EqpVRPI"), "RackIconVRPIT::setEqp", pEqp->getDpName());
		//this->pEqp = (EqpVR_GT *)pEqp;
	}
	forceRedraw();
}

void RackIconVRGPK::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.scale(scale, scale);
	QPen selectionPen = QPen(Qt::red);
	selectionPen.setWidth(3);
	QFont calibriSmallBold("Calibri", 8, QFont::Bold);
	QFont calibriExtraSmall("Calibri", 6);
	int textFlags;

	if (isSelected)
	{
		painter.setPen(selectionPen);
	}
	else
	{
		painter.setPen(Qt::black);
	}

	int lineSplitIndex = this->equipmentName.indexOf("\.", 7);
	QString firstLine = this->equipmentName.left(lineSplitIndex + 1);
	QString secondLine = this->equipmentName.right(this->equipmentName.length() - (lineSplitIndex + 1));

	painter.setBrush(QColor(220, 220, 220));
	painter.setFont(calibriSmallBold);
	textFlags = Qt::AlignHCenter | Qt::AlignTop | Qt::TextWrapAnywhere;

	QRect equipmentRect(0, 0, this->sizeX, this->sizeY);
	//painter.setPen(Qt::black);
	painter.drawRect(equipmentRect);
	painter.setPen(Qt::black);
	painter.drawText(equipmentRect, textFlags, firstLine);
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 9, equipmentRect.width(), 12, textFlags, secondLine);

	painter.setFont(calibriExtraSmall);
	textFlags = Qt::AlignCenter;

	QString attributeValue;
	//write VGI name
	attributeValue = attributes.value("VGI", "-");
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 20, equipmentRect.width(), 7, textFlags, attributeValue);
	attributeValue = attributes.value("CablePower", " - ");
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 28, equipmentRect.width(), 7, textFlags, "TCA3 " + attributeValue);
	attributeValue = attributes.value("CableControl", "-");
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 36, equipmentRect.width(), 7, textFlags, "NF12 " + attributeValue);

}


