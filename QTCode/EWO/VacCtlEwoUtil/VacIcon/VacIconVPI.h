#ifndef	VACICONVPI_H
#define	VACICONVPI_H

//	Icon for VPI

#include "VacIcon.h"

class EqpVPI;

class VACCTLEWOUTIL_EXPORT VacIconVPI : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPI(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPI();

	virtual VacIconContainer::Direction getSlaveDirection(void) const;

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);
	virtual void drawInterlockSrcArrow(QPainter &painter, bool before, bool after);

protected:
	// Pointer to my device
	EqpVPI *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, unsigned rr1, bool blockedOff, bool plcAlarm);
	virtual void setPainterForImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
};

#endif	// VACICONVPI_H
