#include "VacCtlPvssUtil.h"

#include <IdExpr.hxx>
#include <BitVar.hxx>
#include <AnyTypeVar.hxx>
#include <UIntegerVar.hxx>
#include <FloatVar.hxx>

#include <QVariant>

/*
**
**	FUNCTION
**		Parse all arguments required for DLL function call
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**		args	- Array of all required arguments description, see comments for SPvssArg data type
**		nArgs	- Number of items in array (== number of arguments to parse)
**
**	RETURNS
**		true	- In case of success, or
**		false	- if parsing has failed for at least one argument. In this case appropriate error
**					will be added to execution thread of PVSS.
**
**	CAUTIONS
**		None
*/
bool VacCtlPvssUtil::parseArgs(ExecuteParamRec &param, SPvssArgPtr args, int nArgs)
{
	for(int n = 0 ; n < nArgs ; n++)
	{
		if(args[n].pInVar && args[n].ppOutVar)	// In/out argument
		{
			CtrlExpr	*expr;
			if(!getInOutArg(param, args[n].argName, args[n].varType, n == 0, &expr, args[n].ppOutVar))
			{
				return false;
			}
			*args[n].pInVar = *(expr->evaluate(param.thread));
		}
		else if(args[n].pInVar)	// Input argument
		{
			CtrlExpr	*expr = getArg(param, args[n].argName, n == 0);
			if(!expr)
			{
				return false;
			}
			*args[n].pInVar = *(expr->evaluate(param.thread));
		}
		else if(args[n].ppOutVar)	// Output argument
		{
			if(!(*args[n].ppOutVar = getOutArg(param, args[n].argName, args[n].varType, n == 0)))
			{
				return false;
			}
		}
		else
		{
			ErrClass err(ErrClass::PRIO_SEVERE,
				ErrClass::ERR_IMPL,
				// What error occurred
				ErrClass::ILLEGAL_ARG,
				// Where did error occur
				// e.g. script name + lineNo. or
				// Graphic obj (UI) + lineNo should always be indicated
				param.thread->getScript()->getClientData()->getLocation(),
				// Name of function being called
				param.funcName,
				// specific text of error message
				"Invalid argument description in DLL implementation");
			// Output error message to stderr (if log +stderr specified)
			ErrHdl::error(err);
			// Pass error message to Ctrl thread.
			param.thread->appendLastError(&err);
			return false;
		}
	}
	return true;
}

/*
**	FUNCTION
**		Get next argument passed to DLL function call
**
**	PARAMETERS
**		param		- All calling and execution parametrs can be accessed via this class
**		argName		- Name of argument for error reporting if argument is missing
**		isFirstArg	- true if first argument shall be obtained,
**					  false if next available argument shall be obtained.
**
**	RETURNS
**		Pointer to expression for argument, or
**		NULL if no argument found
**
**	CAUTIONS
**		None
*/
CtrlExpr *VacCtlPvssUtil::getArg(ExecuteParamRec &param, const char *argName, bool isFirstArg)
{
	CtrlExpr	*result = NULL;

	if(param.args)
	{
		if(isFirstArg)
		{
			result = param.args->getFirst();
		}
		else
		{
			result = param.args->getNext();
		}
	}
	if(!result)
	{
		char	buf[256];
		sprintf(buf, "%s argument missing", argName);
		showError(param, ErrClass::ARG_MISSING, buf);
	}
	return result;
}

/*
**	FUNCTION
**		Get next argument passed to DLL function call that shall be used
**		as both input and output argument
**
**	PARAMETERS
**		param		- All calling and execution parametrs can be accessed via this class
**		argName		- Name of argument for error reporting if argument is missing
**		varType		- Required type of argument
**		isFirstArg	- true if first argument shall be obtained,
**					  false if next available argument shall be obtained.
**		pExpr		- Pointer to expression to get in value is returned here
**		pVariable	- Pointer to variable for out value is returned here
**
**	RETURNS
**		true	- in case of success;
**		false	- in case of error.
**
**	CAUTIONS
**		None
*/
bool VacCtlPvssUtil::getInOutArg(ExecuteParamRec &param, const char *argName, enum VariableType varType,
	bool isFirstArg, CtrlExpr **pExpr, Variable **pVariable)
{
	CtrlExpr	*expr;

	if(!(expr = getArg(param, argName, isFirstArg)))
	{
		return false;
	}
	// Check if argument is lvalue
	Variable *result = expr->getTarget(param.thread);
	if(!result)
	{
		char	buf[256];
		sprintf(buf, "Argument %s is not lvalue", argName);
		showError(param, ErrClass::NO_LVALUE, buf);
		return false;
	}
	if(result->isA() != varType)	// Check argument data type
	{
		showError(param, ErrClass::ILLEGAL_ARG, (const char *)((IdExpr*)expr)->getName());
		return false;
	}
	*pExpr = expr;
	*pVariable = result;
	return true;
}

/*
**	FUNCTION
**		Get next argument passed to DLL function call that shall be used
**		to return value to PVSS code
**
**	PARAMETERS
**		param		- All calling and execution parametrs can be accessed via this class
**		argName		- Name of argument for error reporting if argument is missing
**		varType		- Required type of argument
**		isFirstArg	- true if first argument shall be obtained,
**					  false if next available argument shall be obtained.
**
**	RETURNS
**		Pointer to variable, or
**		NULL if no argument found
**
**	CAUTIONS
**		None
*/
Variable *VacCtlPvssUtil::getOutArg(ExecuteParamRec &param, const char *argName, enum VariableType varType,
	bool isFirstArg)
{
	CtrlExpr	*expr;

	if(!(expr = getArg(param, argName, isFirstArg)))
	{
		return NULL;
	}

	// Check if argument is lvalue
	Variable *result = expr->getTarget(param.thread);
	if(!result)
	{
		char	buf[256];
		sprintf(buf, "Argument %s is not lvalue", argName);
		showError(param, ErrClass::NO_LVALUE, buf);
		return NULL;
	}
	if(result->isA( ) != varType)	// Check argument data type
	{
		showError(param, ErrClass::ILLEGAL_ARG, (const char *)((IdExpr*)expr)->getName());
		return NULL;
	}
	return result;
}


/*
**	FUNCTION
**		Split DP name in form
**		<DPName>.<DPEname>
**		to 2 components: pure DP name and DPE name. Split is done on first
**		occurence of '.' in input name
**
**	PARAMETERS
**		dp			- Input DP + DPE name
**		ppDpName	- Address of variable where pointer to pure DP name
**						will be returned
**		ppDpeName	- Address of variable where pointer to pure DP name
**						will be returned
**
**	RETURNS
**		true - if split was successful;
**		false - otherwise
**
**	CAUTIONS
**		Function returns pointers to static buffer, content of buffer will
**		be overwritten by next call
*/
bool VacCtlPvssUtil::splitDpName(const char *dp, char **ppDpName, char **ppDpeName)
{
	static char buf[1024];
	if(!dp)
	{
		return false;
	}
	if(!*dp)
	{
		return false;
	}
	strcpy(buf, dp);
	char *firstDot = strchr(buf, '.');
	*ppDpName = buf;
	if(firstDot)
	{
		*firstDot = '\0';
		firstDot++;
		*ppDpeName = firstDot;
	}
	else
	{
		*ppDpeName = NULL;
	}
	return true;
}

/*
**	FUNCTION
**		Set error in DLL function calling context. This error can later be
**		retrieved by PVSS code using getLastError()
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**		errCls	- Error code
**		errMsg	- String with error description
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::showError(ExecuteParamRec &param, ErrClass::ErrCode errCls, const QString &errMsg)
{
	ErrClass err(ErrClass::PRIO_WARNING,
		ErrClass::ERR_CONTROL,
		// What error occurred
		errCls,
		// Where did error occur
		// e.g. script name + lineNo. or
		// Graphic obj (UI) + lineNo should always be indicated
		param.thread->getScript()->getClientData()->getLocation(),
		// Name of function being called
		param.funcName,
		// specific text of error message
		errMsg.toLatin1());
	// Output error message to stderr (if log +stderr specified)
	ErrHdl::error(err);
	// Pass error message to Ctrl thread.
	param.thread->appendLastError(&err);
}

/*
**	FUNCTION
**		Add all strings from error list to dyn_string variable exceptionInfo in a way compatible
**		with the way FrameWork functions do.
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**		result	- Pointer to dyn_string variable where messages shall be added
**		coco	- Completion code of function that caused an error.
**				  Negative coco leads to 'ERROR', other coco values - to 'WARNING'
**		errList	- List of error messages
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::addExceptionInfo(ExecuteParamRec &param, Variable *result, QStringList &errList)
{
	foreach(QString msg, errList)
	{
		addExceptionInfo(param, result, msg.toLatin1());
	}
}

/*
**	FUNCTION
**		Add error message to dyn_string variable exceptionInfo in a way compatible
**		with the way FrameWork functions do.
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**		result	- Pointer to dyn_string variable where messages shall be added
**		coco	- Completion code of function that caused an error.
**				  Negative coco leads to 'ERROR', other coco values - to 'WARNING'
**		errMsg	- Pointer to error description
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::addExceptionInfo(ExecuteParamRec &param, Variable *result, const QString &errMsg)
{
	TextVar		newInfo;

	// Exception type
	newInfo.setValue("ERROR");
	((DynVar *)result)->append(newInfo);

	// Function name + exception text
	QString str(param.funcName);
	str += "(): ";
	str += errMsg;
	newInfo.setValue(str.toLatin1());
	((DynVar *)result)->append(newInfo);

	// Exception code
	newInfo.setValue("");
	((DynVar *)result)->append(newInfo);
}

/*
**	FUNCTION
**		Add string value to dyn_string.
**
**	PARAMETERS
**		pDynVar		- Pointer to dyn_string variable
**		value		- string to be added
**		checkUnique	- Check if value is already in list
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::addToDynStringVar(DynVar *pDynVar, const char *value, bool checkUnique)
{
	TextVar	var;
	var.setValue(value ? value : "");
	if(checkUnique)
	{
		if(pDynVar->isIn(var))
		{
			return;
		}
	}
	pDynVar->append(var);
}

/*
**	FUNCTION
**		Add integer value to dyn_int.
**
**	PARAMETERS
**		pDynVar	- Pointer to dyn_int variable
**		value	- value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::addToDynIntVar(DynVar *pDynVar, int value, bool checkUnique)
{
	IntegerVar var(value);
	if(checkUnique)
	{
		if(pDynVar->isIn(var))
		{
			return;
		}
	}
	pDynVar->append(var);
}

/*
**	FUNCTION
**		Add boolean value to dyn_bool.
**
**	PARAMETERS
**		pDynVar	- Pointer to dyn_bool variable
**		value	- value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::addToDynBoolVar(DynVar *pDynVar, bool value, bool checkUnique)
{
	BitVar var(value);
	if(checkUnique)
	{
		if(pDynVar->isIn(var))
		{
			return;
		}
	}
	pDynVar->append(var);
}

/*
**	FUNCTION
**		Add float value to dyn_float.
**
**	PARAMETERS
**		pDynVar	- Pointer to dyn_bool variable
**		value	- value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::addToDynFloatVar(DynVar *pDynVar, float value, bool checkUnique)
{
	FloatVar var(value);
	if(checkUnique)
	{
		if(pDynVar->isIn(var))
		{
			return;
		}
	}
	pDynVar->append(var);
}

/*
**	FUNCTION
**		Add string value to dyn_anytype.
**
**	PARAMETERS
**		pDynVar	- Pointer to dyn_anytype variable
**		value	- string to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::addToDynAnyVar(DynVar *pDynVar, const char *value)
{
	TextVar	*pTextVar = new TextVar();
	pTextVar->setValue(value ? value : "");
	AnyTypeVar anyTypeVar;
	anyTypeVar.setVar(pTextVar);
	pDynVar->append(anyTypeVar);
}

/*
**	FUNCTION
**		Add integer value to dyn_anytype.
**
**	PARAMETERS
**		pDynVar	- Pointer to dyn_anytype variable
**		value	- value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::addToDynAnyVar(DynVar *pDynVar, int value)
{
	IntegerVar *pIntVar = new IntegerVar(value);
	AnyTypeVar anyTypeVar;
	anyTypeVar.setVar(pIntVar);
	pDynVar->append(anyTypeVar);
}

/*
**	FUNCTION
**		Add boolean value to dyn_anytype.
**
**	PARAMETERS
**		pDynVar	- Pointer to dyn_anytype variable
**		value	- value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::addToDynAnyVar(DynVar *pDynVar, bool value)
{
	BitVar *pBitVar = new BitVar(value);
	AnyTypeVar anyTypeVar;
	anyTypeVar.setVar(pBitVar);
	pDynVar->append(anyTypeVar);
}

/*
**	FUNCTION
**		Add float value to dyn_anytype.
**
**	PARAMETERS
**		pDynVar	- Pointer to dyn_anytype variable
**		value	- value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::addToDynAnyVar(DynVar *pDynVar, float value)
{
	FloatVar *pFloatVar = new FloatVar(value);
	AnyTypeVar anyTypeVar;
	anyTypeVar.setVar(pFloatVar);
	pDynVar->append(anyTypeVar);
}


/*
**	FUNCTION
**		Convert value from AnyTypeVar to QVariant - only limited set
**		of types is supported
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**		value	- Value to be converted to QVariant	
**
**	RETURNS
**		Pointer to QVariant with the value from 'value' argument; or
**		NULL if conversion from data type is not supported
**
**	CAUTIONS
**		It is responsability of caller to delete returned QVariant
*/
QVariant *VacCtlPvssUtil::anyTypeToVariant(ExecuteParamRec &param, AnyTypeVar &value)
{
	Variable *pVar = value.getVar();
	if(!pVar)
	{
		showError(param, ErrClass::PARAMETERERROR, "Empty value");
		return NULL;
	}
	QVariant *pVariant = NULL;
	const char *pText = NULL;
	switch(pVar->isA())
	{
	case INTEGER_VAR:
		pVariant = new QVariant((int)((IntegerVar *)pVar)->getValue());
		break;
	case UINTEGER_VAR:
		pVariant = new QVariant((unsigned)((UIntegerVar *)pVar)->getValue());
		break;
	case FLOAT_VAR:
		pVariant = new QVariant((double)((FloatVar *)pVar)->getValue());
		break;
	case BIT_VAR:
		pVariant = new QVariant((bool)((BitVar *)pVar)->isTrue());
		break;
	case TEXT_VAR:	// This is not 'normal', but rather workaround for WinCC OA 3.15 bug - see https://its.cern.ch/jira/browse/ENS-19925
		pText = ((TextVar *)pVar)->getValue();
		if (pText) {
			if (*pText) {
				QString text(pText);
				bool convertOk = false;
				quint32 uValue = text.toUInt(&convertOk);
				if (!convertOk) {
					uValue = 0;
				}
				pVariant = new QVariant(uValue);
			}
		}
		break;
	default:	// Just to make compiler happy
		break;
	}
	if(!pVariant)
	{
		char buf[128];
		sprintf(buf, "Unsupported data type %d", pVar->isA());
		showError(param, ErrClass::PARAMETERERROR, buf);
	}
	return pVariant;
}

/*
**	FUNCTION
**		Convert value from QVariant to AnyTypeVar - only limited set
**		of types is supported
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class
**		value	- Variant value to be converted
**		pVar	- Pointer to variable where result will be written	
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacCtlPvssUtil::variantToAnyType(ExecuteParamRec &param, QVariant value, AnyTypeVar *pVar)
{
	switch(value.type())
	{
	case QVariant::Int:
		pVar->setVar(new IntegerVar(value.toInt()));
		break;
	case QVariant::UInt:
		pVar->setVar(new UIntegerVar(value.toUInt()));
		break;
	case QVariant::Double:
	// case QMetaType::Float:	// See https://icecontrols.its.cern.ch/jira/browse/ENS-9991
		pVar->setVar(new FloatVar(value.toDouble()));
		break;
	case QVariant::Bool:
		pVar->setVar(new BitVar(value.toBool()));
		break;
	default:
		{
			char buf[128];
			sprintf(buf, "Unsupported QVariant type %d", value.type());
			showError(param, ErrClass::PARAMETERERROR, buf);
		}
		break;
	}
}

