//	Implementation of VacIconVGR_PS_CMW class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGR_PS_CMW.h"

#include <qpainter.h>

// We only need to implement drawSpecific() method

// Draw 'Pirani' symbol in the center of icon
void VacIconVGR_PS_CMW::drawSpecific(QPainter &painter)
{
	painter.setPen(Qt::black);
	QPolygon points(8);
	points.setPoint(0, 4, 9);
	points.setPoint(1, 6, 12);
	points.setPoint(2, 7, 12);
	points.setPoint(3, 9, 7);
	points.setPoint(4, 10, 7);
	points.setPoint(5, 13, 12);
	points.setPoint(6, 14, 12);
	points.setPoint(7, 16, 9);
	painter.drawPolyline(points);
}
