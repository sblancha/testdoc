
#uses "VacCtlEqpDataPvss"	// Load DLL
#uses "vclSmsConfig.ctl"	// Load CTRL library
#uses "vclSectors.ctl"	// Load CTRL library
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "email.ctl"	// Load PVSS standard CTRL library
#uses "vclDevCommon.ctl"	// Load CTRL library
#uses "vclMobile.ctl"  // Load CTRL library


// Common configuration: name of SMTP server
string	smtpServer;

// Common configuration: name of client
string	mailClient;

// Common configuration: name of message sender
string	mailSender;

// Common configuration: recent machine mode
unsigned machineMode;


// List of known SMS configuration DP names
dyn_string  configDps;

// List of times when every config was activated
dyn_time    configActivateTimes;

// List of recipients for every configuration
dyn_dyn_string  configRecipients;

// List of machine mode masks for every configuration;
dyn_uint     configModes;

// Number of messages sent for every configuration since activation
// dyn_int          configMessageCounts;



// List of configurations for which messages which were failed to send
dyn_string    failedMsgConfigs;

// Timestamp - when message sending failed
dyn_time      failedMsgTimes;

// List of messages which were failed to send
dyn_string    failedMsgs;

// List of bodies for failed messages
dyn_string    failedBodies;

// Empty time
time	emptyTime;

// Flag enabling debugging messages - read from DP
bool debug;

main()
{
  emptyTime = makeTime(1999, 1, 1);

  // Initialize static data
  if(!InitStaticData())
  {
    return;
  }

  if(!ConnectToCommonConfig())
  {
    return;
  }

  // Connect to state of mobile equipment
  VacConnectToMobileList("");

  // Infinite loop - make processings
  int pass = 0;
  while(true)
  {
    if(pass == 0)
    {
      UpdateConfigList();
    }
    if(++pass == 20)
    {
      pass = 0;
    }
    ProcessConnections();
    ProcessMessages();
    VacSmsCheckGroupExpired();
    delay(1);
  }
}

/** ProcessMessages
Purpose:
Process pending requests to send messages

Parameters:
	- None

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux, but only tested under Linux
	. distributed system: yes.
*/
void ProcessMessages()
{
  // First try to resend messages which were failed to send
  int nFailed = dynlen(failedMsgConfigs);
  for(int n = dynlen(failedMsgConfigs) ; n > 0 ; n--)
  {
DebugTN("vcsSmsNotification.ctl: ProcessMessages(): Processing failed message");
    time now = getCurrentTime();
    if((now - failedMsgTimes[n]) > 3600)  // Was not able to send message for 1 hour
    {
      dynRemove(failedMsgTimes, n);
      dynRemove(failedMsgConfigs, n);
      dynRemove(failedMsgs, n);
      dynRemove(failedBodies, n);
    }
    else if(SendMessage(failedMsgConfigs[n], failedMsgs[n], failedBodies[n]))
    {
      dynRemove(failedMsgTimes, n);
      dynRemove(failedMsgConfigs, n);
      dynRemove(failedMsgs, n);
      dynRemove(failedBodies, n);
    }
  }

  // Then process new messages
  string config, message, body;
  VacSmsGetNextMessage(config, message, body);
  while(config != "")
  {
    if(dpExists(config + ".DpNumber"))
    {
      int id;
      dpGet(config + ".DpNumber", id);
      string idString;
      if(id < 99999)
      {
        sprintf(idString, " [%05d]", id);
      }
      else
      {
        sprintf(idString, " [%d]", id);
      }
      message += idString;
    }
DebugTN("vcsSmsNotification.ctl: ProcessMessages(): got new message for " + config + ": " + message);
    synchronized(configDps)
    {
      int idx = dynContains(configDps, config);
      if(idx > 0)
      {
        uint eventCount;
        dpGet(config + ".State.EventCount", eventCount);
        eventCount++;
        dpSet(config + ".State.EventCount", eventCount);
      }
    }
    if(!SendMessage(config, message, body))
    {
      time now = getCurrentTime();
      dynAppend(failedMsgTimes, now);
      dynAppend(failedMsgConfigs, config);
      dynAppend(failedMsgs, message);
      dynAppend(failedBodies, body);
    }
    VacSmsGetNextMessage(config, message, body);
  }
}

bool SendMessage(string config, string message, string body)
{
  dyn_string	recipients;
  bool doNotSend = false;
  synchronized(configDps)
  {
    int idx = dynContains(configDps, config);
    if(idx > 0)
    {
      if((machineMode & configModes[idx]) == 0)
      {
        doNotSend = true;
        DebugTN("Message not send machine mode " + machineMode + " not match with " + configModes[idx]);
      }
      else
      {
        for(int n = dynlen(configRecipients[idx]) ; n > 0 ; n--)
        {
          string address = configRecipients[idx][n];
          dynAppend(recipients, address);
        }
      }
    }
    else
    {
      DebugTN("vcsSmsNotification.ctl: SendMessage(): config <" + config + "> not found");
    }
  }
  if(doNotSend)
  {
    if(debug)
    {
      DebugTN("vcsSmsNotification.ctl: SendMessage(): do not send message for " + config + ": machine mode does not match");
    }
    return true;
  }
  if(dynlen(recipients) == 0)
  {
    return true;
  }
  if(smtpServer == "")
  {
    DebugTN("vcsSmsNotification.ctl: SendMessage(): empty SMTP server");
    return true;
  }
  if(mailClient == "")
  {
    DebugTN("vcsSmsNotification.ctl: SendMessage(): empty mail client (domain)");
    return true;
  }
  if(mailSender == "")
  {
    DebugTN("vcsSmsNotification.ctl: SendMessage(): empty mail sender");
    return true;
  }

  dyn_string	email = makeDynString("");
  for(int n = dynlen(recipients) ; n > 0 ; n--)
  {
    if(email[1] != "")
    {
      email[1] += ";";
    }
    email[1] += recipients[n];
  }
  email[2] = mailSender;
  email[3] = message;
  email[4] = body;
  if(debug)
  {
    DebugTN( "vcsSmsNotification.ctl: SendMessage(): message to send is '" + email[3] + "'" );
  }
  int coco;
  emSendMail(smtpServer, mailClient, email, coco);
  if(coco >= 0)
  {
    synchronized(configDps)
    {
      int idx = dynContains(configDps, config);
      if(idx > 0)
      {
        int msgCount;
        dpGet(config + ".State.MessagesSent", msgCount);
        msgCount++;
        dpSet(config + ".State.MessagesSent", msgCount);
      }
    }
  }
  return coco < 0 ? false : true;
}

/** ProcessConnections
Purpose:
Process pending requests to connect/disconnect DPs for data acquisition

Parameters:
	- None

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux, but only tested under Linux
	. distributed system: yes.
*/
void ProcessConnections()
{
  // Process connect/disconnect requests
  string dpe;
  bool connect;
  LhcVacNextDpConnect(dpe, connect);
  while(dpe != "")
  {
    if(debug)
    {
      DebugTN("vcsSmsNotification.ctl: ProcessConnections()", dpe, connect);
    }
    if(connect)
    {
      dpConnect("OnlineValueCb", dpe, dpe + ":_original.._stime");
    }
    else
    {
      dpDisconnect("OnlineValueCb", dpe, dpe + ":_original.._stime");
    }
    LhcVacNextDpConnect(dpe, connect);
  }

  // Mark configurations as initialized
  synchronized(configDps)
  {
    for(int n = dynlen(configDps) ; n > 0 ; n--)
    {
      /*
      if(debug)
      {
        DebugTN("vscSmsNotification.ctl: ProcessConnections(): checking " + configDps[n], configActivateTimes[n]);
      }
      */
      if(configActivateTimes[n] == emptyTime)
      {
        continue;
      }
      int timeDiff = getCurrentTime() - configActivateTimes[n];
      /*
      if(debug)
      {
        DebugTN("vscSmsNotification.ctl: ProcessConnections(): time diff " + timeDiff + " for " + configDps[n]);
      }
      */
      if(timeDiff < 10)
      {
        continue;
      }
      string errMsg;
      VacSmsSetConfigInitialized(configDps[n], errMsg);
      if(errMsg != "")
      {
        dpSet(configDps[n] + ".State.ErrorMessage", errMsg,
          configDps[n] + ".State.Initialized", false);
      }
      else
      {
        dpSet(configDps[n] + ".State.ErrorMessage", "",
          configDps[n] + ".State.Initialized", true);
      }
      configActivateTimes[n] = emptyTime;
    }
  }
}

void OnlineValueCb(string dpe, anytype value, string stimeDpe, time stime)
{
  string dpeName = dpSubStr(dpe, DPSUB_DP_EL);
  LhcVacNewOnlineValue(dpeName, value, stime);
}

/** UpdateConfigList
Purpose:
Update list of existing configuration DPs. Connect to new DPs, remove deleted DPs from local list

Parameters:
	- None

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux
	. distributed system: yes.
*/
void UpdateConfigList()
{
  dyn_string existingDps = dpNames("*", SMS_CONFIG_DP_TYPE);

  if(debug)
  {
    DebugTN("vscSmsNotification.ctl: UpdateConfigList(): got " + dynlen(existingDps) + " DP(s)");
  }

  for(int n = dynlen(existingDps) ; n > 0 ; n--)
  {
    existingDps[n] = dpSubStr(existingDps[n], DPSUB_DP);
    bool toBeDeleted;
    dpGet(existingDps[n] + ".ToBeDeleted", toBeDeleted);
    if(toBeDeleted)
    {
      DeleteConfigDp(existingDps[n]);
    }
    else
    {
      if(dynContains(configDps, existingDps[n]) <= 0)
      {
        AddConfigDp(existingDps[n]);
      }
    }
  }

  for(int n = dynlen(configDps) ; n > 0 ; n--)
  {
    if(dynContains(existingDps, configDps[n]) <= 0)
    {
      DeleteConfigDp(configDps[n]);
    }
  }
}

void DeleteConfigDp(string dpName)
{
  synchronized(configDps)
  {
    if(debug)
    {
      DebugTN("vscSmsNotification.ctl: DeleteConfigDp(): removing <" + dpName + ">");
    }
    string errMsg;
    VacSmsSetConfigActive(dpName, false, errMsg);
    if(errMsg != "")
    {
      DebugTN("vscSmsNotification.ctl: DeleteConfigDp(): deactivating <" + dpName + "> error: " + errMsg);
    }
    // L.Kopylov 25.04.2014 VacSmsDeleteConfig(dpName);
    int dpIdx = dynContains(configDps, dpName);
    if(dpIdx > 0)
    {
      if(dpExists(dpName))
      {
        dpDisconnect("ConfigActivateCb", dpName + ".State.Activate");
        dpDisconnect("ConfigFilteringCb",
          dpName + ".MsgFiltering.Type",
          dpName + ".MsgFiltering.DeadTime",
          dpName + ".MsgFiltering.Grouping.Type",
          dpName + ".MsgFiltering.Grouping.Interval",
          dpName + ".MsgFiltering.Grouping.Count",
          dpName + ".MsgFiltering.Grouping.Message");
        dpDisconnect("ConfigScopeCb",
          dpName + ".Scope.Type",
          dpName + ".Scope.Names");
        dpDisconnect("UsageFilterCb",
          dpName + ".Scope.UsageFilter.FunctionalTypes",
          dpName + ".Scope.UsageFilter.AttrNames",
          dpName + ".Scope.UsageFilter.ReverseFlags");
        dpDisconnect("ConfigCriteriaCb",
          dpName + ".Criteria.JoinType",
          dpName + ".Criteria.FunctionalTypes",
          dpName + ".Criteria.Types",
          dpName + ".Criteria.Subtypes",
          dpName + ".Criteria.LowerLimits",
          dpName + ".Criteria.UpperLimits",
          dpName + ".Criteria.ReverseFlags",
          dpName + ".Criteria.MinDurations");
        dpDisconnect("ConfigMessageCb", dpName + ".Message");
        dpDisconnect("ConfigRecipientsCb", dpName + ".Recipients");
        dpDisconnect("ConfigModeCb", dpName + ".Mode");

        dynRemove(configModes, dpIdx);
        dynRemove(configRecipients, dpIdx);
        dynRemove(configDps, dpIdx);
//        dynRemove(configMessageCounts, dpIdx);
      }
    }
  }
  if(dpExists(dpName))
  {
    dpDelete(dpName);
  }
}


void AddConfigDp(string dpName)
{
  if(debug)
  {
    DebugTN("vcsSmsNotification.ctl: AddConfigDp(" + dpName + ")");
  }
  synchronized(configDps)
  {
    dynAppend(configModes, 0);
    dynAppend(configRecipients, makeDynString());
    dynAppend(configDps, dpName);
    dynAppend(configActivateTimes, emptyTime);
//    dynAppend(configMessageCounts, 0);
  }
  dpConnect("ConfigModeCb", dpName + ".Mode");
  dpConnect("ConfigRecipientsCb", dpName + ".Recipients");
  dpConnect("ConfigMessageCb", dpName + ".Message");
  dpConnect("ConfigCriteriaCb",
    dpName + ".Criteria.JoinType",
    dpName + ".Criteria.FunctionalTypes",
    dpName + ".Criteria.Types",
    dpName + ".Criteria.Subtypes",
    dpName + ".Criteria.LowerLimits",
    dpName + ".Criteria.UpperLimits",
    dpName + ".Criteria.ReverseFlags",
    dpName + ".Criteria.MinDurations");
  dpConnect("UsageFilterCb",
    dpName + ".Scope.UsageFilter.FunctionalTypes",
    dpName + ".Scope.UsageFilter.AttrNames",
    dpName + ".Scope.UsageFilter.ReverseFlags");
  dpConnect("ConfigScopeCb",
    dpName + ".Scope.Type",
    dpName + ".Scope.Names");
  dpConnect("ConfigFilteringCb",
    dpName + ".MsgFiltering.Type",
    dpName + ".MsgFiltering.DeadTime",
    dpName + ".MsgFiltering.Grouping.Type",
    dpName + ".MsgFiltering.Grouping.Interval",
    dpName + ".MsgFiltering.Grouping.Count",
    dpName + ".MsgFiltering.Grouping.Message");
  dpConnect("ConfigActivateCb", dpName + ".State.Activate");
}

void ConfigModeCb(string dpe, unsigned mode)
{
  if(debug)
  {
    DebugTN("vcsSmsNotification.ctl: ConfigModeCb():", mode);
  }
  string dpName = dpSubStr(dpe, DPSUB_DP);
  synchronized(configDps)
  {
    int idx = dynContains(configDps, dpName);
    if(idx > 0)
    {
      configModes[idx] = mode;
    }
    else
    {
      DebugTN("vcsSmsNotification.ctl: ConfigModeCb(): callback from unknown DP <" + dpName + ">");
    }
  }
}

void ConfigRecipientsCb(string dpe, dyn_string recipients)
{
  if(debug)
  {
    DebugTN("vcsSmsNotification.ctl: ConfigRecipientsCb():", recipients);
  }
  string dpName = dpSubStr(dpe, DPSUB_DP);
  synchronized(configDps)
  {
    int idx = dynContains(configDps, dpName);
    if(idx > 0)
    {
      configRecipients[idx] = recipients;
    }
    else
    {
      DebugTN("vcsSmsNotification.ctl: ConfigRecipientsCb(): callback from unknown DP <" + dpName + ">");
    }
  }
}

void ConfigMessageCb(string dpe, string message)
{
  if(debug)
  {
    DebugTN("vcsSmsNotification.ctl: ConfigMessageCb():", message);
  }
  if(message == "")
  {
    return;
  }
  string errMsg;
  string dpName = dpSubStr(dpe, DPSUB_DP);
  synchronized(configDps)
  {
    int idx = dynContains(configDps, dpName);
    if(idx > 0)
    {
      VacSmsSetMessage(dpName, message, errMsg);
    }
    else
    {
      DebugTN("vcsSmsNotification.ctl: ConfigMessageCb(): callback from unknown DP <" + dpName + ">");
    }
  }
  if(errMsg != "")
  {
    dpSet(dpName + ".State.ErrorMessage", errMsg);
  }
}

void ConfigCriteriaCb(string dpe1, int joinType, string dpe2, dyn_int funcTypes,
	string dpe3, dyn_int types, string dpe4, dyn_int subTypes,
	string dpe5, dyn_float lowerLimits, string dpe6, dyn_float upperLimits,
	string dpe7, dyn_bool reverseFlags, string dpe8, dyn_int minDurations)
{
  if(debug)
  {
    DebugTN("vcsSmsNotification.ctl: ConfigCriteriaCb(): funcTypes", funcTypes);
    DebugTN("vcsSmsNotification.ctl: ConfigCriteriaCb(): types", types);
    DebugTN("vcsSmsNotification.ctl: ConfigCriteriaCb(): subTypes", subTypes);
    DebugTN("vcsSmsNotification.ctl: ConfigCriteriaCb(): lowerLimits", lowerLimits);
    DebugTN("vcsSmsNotification.ctl: ConfigCriteriaCb(): upperLimits", upperLimits);
    DebugTN("vcsSmsNotification.ctl: ConfigCriteriaCb(): reverseFlags", reverseFlags);
    DebugTN("vcsSmsNotification.ctl: ConfigCriteriaCb(): joinType", joinType);
  }
  if(dynlen(funcTypes) == 0)
  {
    return;	// Empty
  }
  string errMsg;
  string dpName = dpSubStr(dpe1, DPSUB_DP);
  synchronized(configDps)
  {
    int idx = dynContains(configDps, dpName);
    if(idx > 0)
    {
      if(dynlen(funcTypes) != dynlen(types))
      {
        errMsg = "Different number of functional types (" + dynlen(funcTypes) + ") and criteria types (" + dynlen(types) + ")";
      }
      else if(dynlen(funcTypes) != dynlen(subTypes))
      {
        errMsg = "Different number of functional types (" + dynlen(funcTypes) + ") and criteria subtypes (" + dynlen(subTypes) + ")";
      }
      else if(dynlen(funcTypes) != dynlen(lowerLimits))
      {
        errMsg = "Different number of functional types (" + dynlen(funcTypes) + ") and criteria lower limits (" + dynlen(lowerLimits) + ")";
      }
      else if(dynlen(funcTypes) != dynlen(upperLimits))
      {
        errMsg = "Different number of functional types (" + dynlen(funcTypes) + ") and criteria upper limits (" + dynlen(upperLimits) + ")";
      }
      else if(dynlen(funcTypes) != dynlen(reverseFlags))
      {
        errMsg = "Different number of functional types (" + dynlen(funcTypes) + ") and criteria reverse flags (" + dynlen(reverseFlags) + ")";
      }
      else if(dynlen(funcTypes) != dynlen(minDurations))
      {
        errMsg = "Different number of functional types (" + dynlen(funcTypes) + ") and criteria min. durations (" + dynlen(minDurations) + ")";
      }
      else
      {
        int coco = VacSmsSetCriteria(dpName, joinType, funcTypes, types, subTypes, lowerLimits, upperLimits,
                  reverseFlags, minDurations);
        if(debug)
        {
          DebugTN("vcsSmsNotification.ctl: ConfigCriteriaCb(): VacSmsSetCriteria() returned " + coco);
        }
      }
    }
    else
    {
      DebugTN("vcsSmsNotification.ctl: ConfigCriteriaCb(): callback from unknown DP <" + dpName + ">");
    }
  }
  if(errMsg != "")
  {
    dpSet(dpName + ".State.ErrorMessage", errMsg);
  }
}

void UsageFilterCb(string dpe1, dyn_int funcTypes, string dpe2, dyn_string attrNames,
                   string dpe3, dyn_bool reverseFlags)
{
  if(debug)
  {
    DebugTN("vcsSmsNotification.ctl: UsageFilterCb(): funcTypes", funcTypes);
    DebugTN("vcsSmsNotification.ctl: UsageFilterCb(): attrNames", attrNames);
    DebugTN("vcsSmsNotification.ctl: UsageFilterCb(): reserveFlags", reserveFlags);
  }
  string errMsg;
  string dpName = dpSubStr(dpe1, DPSUB_DP);
  synchronized(configDps)
  {
    int idx = dynContains(configDps, dpName);
    if(idx > 0)
    {
      if(dynlen(funcTypes) != dynlen(attrNames))
      {
        errMsg = "Different number of functional types (" + dynlen(funcTypes) + ") and attribute names (" + dynlen(attrNames) + ")";
      }
      else if(dynlen(funcTypes) != dynlen(reverseFlags))
      {
        errMsg = "Different number of functional types (" + dynlen(funcTypes) + ") and revervse flags (" + dynlen(reserveFlags) + ")";
      }
      else
      {
        int coco = VacSmsSetUsageFilter(dpName, funcTypes, attrNames, reverseFlags);
        if(debug)
        {
          DebugTN("vcsSmsNotification.ctl: UsageFilterCb(): VacSmsSetUsageFilter() returned " + coco);
        }
      }
    }
    else
    {
      DebugTN("vcsSmsNotification.ctl: UsageFilterCb(): callback from unknown DP <" + dpName + ">");
    }
  }
  if(errMsg != "")
  {
    dpSet(dpName + ".State.ErrorMessage", errMsg);
  }
}

void ConfigScopeCb(string dpe1, int type, string dpe2, dyn_string names)
{
  if(dynlen(names) == 0)
  {
    return;
  }
  if(debug)
  {
    DebugTN("vcsSmsNotification.ctl: ConfigScopeCb(): type" + type + ", names", names);
  }
  string errMsg;
  string dpName = dpSubStr(dpe1, DPSUB_DP);
  synchronized(configDps)
  {
    int idx = dynContains(configDps, dpName);
    if(idx > 0)
    {
      VacSmsSetScope(dpName, type, names, errMsg);
    }
    else
    {
      DebugTN("vcsSmsNotification.ctl: ConfigScopeCb(): callback from unknown DP <" + dpName + ">");
    }
  }
  if(errMsg != "")
  {
    dpSet(dpName + ".State.ErrorMessage", errMsg);
  }
}

void ConfigFilteringCb(string dpe1, int type, string dpe2, int deadTime,
  string dpe3, int groupType, string dpe4, int groupInterval,
  string dpe5, int groupCount, string dpe6, string groupMessage)
{
  if(debug)
  {
    DebugTN("vcsSmsNotification.ctl: ConfigFilteringCb(): type " + type + ", deadTime " + deadTime +
      ", groupType " + groupType + ", groupInterval " + groupInterval + ", groupCount " + groupCount, groupMessage);
  }
  string errMsg;
  string dpName = dpSubStr(dpe1, DPSUB_DP);
  synchronized(configDps)
  {
    int idx = dynContains(configDps, dpName);
    if(idx > 0)
    {
      switch(type)
      {
      case MSG_FILTER_TYPE_NONE:
        VacSmsClearGrouping(dpName, errMsg);
        break;
      case MSG_FILTER_TYPE_DEAD_TIME:
        VacSmsSetDeadTime(dpName, deadTime, errMsg);
        break;
      case MSG_FILTER_TYPE_GROUPING:
        VacSmsSetGrouping(dpName, groupType, groupInterval, groupCount, groupMessage, errMsg);
        break;
      default:
        errMsg = "Unknown filtering type " + type;
        break;
      }
    }
    else
    {
      DebugTN("vcsSmsNotification.ctl: ConfigFilteringCb(): callback from unknown DP <" + dpName + ">");
    }
  }
  if(errMsg != "")
  {
    dpSet(dpName + ".State.ErrorMessage", errMsg);
  }
}

void ConfigActivateCb(string dpe, bool activate)
{
  if(debug)
  {
    DebugTN("vcsSmsNotification.ctl: ConfigActivateCb(): activate " + activate);
  }
  string errMsg;
  string dpName = dpSubStr(dpe, DPSUB_DP);
  synchronized(configDps)
  {
    int idx = dynContains(configDps, dpName);
    if(idx > 0)
    {
      VacSmsSetConfigActive(dpName, activate, errMsg);
      if(errMsg == "")
      {
        bool isActive;
        VacSmsIsConfigActive(dpName, isActive);
        dpSet(dpName + ".State.Activated", isActive,
          dpName + ".State.Initialized", false);
// Do not reset message counter L.Kopylov 26.10.2012          dpName + ".State.MessagesSent", 0);
        if(isActive)
        {
          configActivateTimes[idx] = getCurrentTime();	// Queued for initializing
        }
        else
        {
          configActivateTimes[idx] = emptyTime;	// Doesn't need initializtion
        }
        // configMessageCounts[idx] = 0;
      }
    }
    else
    {
      DebugTN("vcsSmsNotification.ctl: ConfigActivateCb(): callback from unknown DP <" + dpName + ">");
    }
  }
  dpSet(dpName + ".State.ErrorMessage", errMsg);
}


/** ConnectToCommonConfig
Purpose:
Connect to common configuration

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux, but only tested under Linux
	. distributed system: yes.
*/
bool ConnectToCommonConfig()
{
  string dpName;
  LhcSmsGetCommonConfigDp(dpName);
  if(dpName == "")
  {
    DebugTN("vcsSmsNotification.ctl: ConnectToCommonConfig(): failed to get common config DP name");
    return false;
  }
  dpConnect("CommonConfigCb",
    dpName + ".server",
    dpName + ".client",
    dpName + ".sender",
    dpName + ".debug");
  dpConnect("MachineModeCb", dpName + ".Mode");
  return true;
}

void CommonConfigCb(string dpe1, string server, string dpe2, string client, string dpe3, string sender, string dpe4, bool debugFlag)
{
  smtpServer = server;
  mailClient = client;
  mailSender = sender;
  debug = debugFlag;
}

void MachineModeCb(string dpe, unsigned mode)
{
  machineMode = mode;
  if(debug)
  {
    DebugTN("vcsSmsNotification.ctl: MachineModeCb(): new machine mode is " + machineMode);
  }
}

/** InitStaticData
Purpose:
Read all machine configuration data from file(s)

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux, but only tested under Linux
	. distributed system: yes.
*/
bool InitStaticData()
{
  dyn_string	exceptionInfo;

  // Get accelerator name to be used by other components
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsSmsNotification.ctl: Failed to read machine name: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsSmsNotification.ctl: Machine is " + glAccelerator);
  bool isLhcData = glAccelerator == "LHC" ? true : false;

  // Initalize passive machine data
  int coco = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsSmsNotification.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsSmsNotification.ctl: LhcVacEqpInitPassive() done");

  // Initialzie active equipment information
  coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
    exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsSmsNotification.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsSmsNotification.ctl: InitStaticData() done" );

  VacResourcesParseFile(PROJ_PATH + "/data/VacCtlResources.txt", glAccelerator, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsSmsNotification.ctl: VacResourcesParseFile() failed: " + exceptionInfo);
  }

  // Global equipment
  string dpName;
  LhcSmsGetCommonConfigDp(dpName);
  if(dpName != "")
  {
    coco = LhcVacAddGlobalEqp(dpName, "Mode", "SMS.MachineMode", 1000, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("vcsSmsNotification.ctl: LhcVacAddGlobalEqp(" + dpName + ") coco = " + coco + ", exceptionInfo:", exceptionInfo);
    }
    else
    {
      DebugTN("vcsSmsNotification.ctl: LhcVacAddGlobalEqp(" + dpName + ") coco = " + coco);
    }
  }

  return true;
}
