
// Configuration and auxilliary functions for E-mail notifications
// about vacuum equipment problems

//#uses "vclVacSectors.ctl"	// Load CTRL library
#uses "email.ctl"	// Load standard PVSS CTRL library

// Name of DP type containing E-mail configuration
const string VAC_EMAIL_CONFIG_DP_TYPE = "_VacEmailConfig";

// Name of DP containing E-mail configuration
const string VAC_EMAIL_CONFIG_DP = "_VacEmailConfig";

// Set name of SMTP server
void VacEmailSetServer( string serverName, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	VacEmailGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dpSetWait( configDp + ".server", serverName );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailSetServer(): dpSetWait() failed", "" );
	}
}
// Set name of client computer ('domain' for emSendMail())
void VacEmailSetClient( string clientName, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	VacEmailGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dpSetWait( configDp + ".client", clientName );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailSetClient(): dpSetWait() failed", "" );
	}
}
// Set address of mail sender. It looks like sender shall be recognized
// by SMTP server
void VacEmailSetSender( string senderName, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	VacEmailGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dpSetWait( configDp + ".sender", senderName );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailSetSender(): dpSetWait() failed", "" );
		throwError( err );
	}
}
// Set address(es) of recipients to be notified about problems in all sectors
// Every item in list shall be valid E-mail address
void VacEmailSetRecipientsAll( dyn_string recipients, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	VacEmailGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dpSetWait( configDp + ".recipientsAll", recipients );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailSetSender(): dpSetWait() failed", "" );
		throwError( err );
	}
}
// Add recipient for specific vacuum sector
void VacEmailAddSectorRecipient( string sector, string recipient, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;
	dyn_string		specSectors, specRecipients;
	dynClear( exceptionInfo );
	if( sector == "" )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailAddSectorRecipient(): empty sector name", "" );
		return;
	}
	if( recipient == "" )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailAddSectorRecipient(): empty recipient address", "" );
		return;
	}
	VacEmailGetSpecRecipients( configDp, specSectors, specRecipients, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Check if given recipient for given sector is already in list
	for( int n = dynlen( specSectors ) ; n > 0 ; n-- )
	{
		if( specSectors[n] != sector ) continue;
		if( specRecipients[n] == recipient ) return;	// Already in list
	}
	// Add new sector and recipient, write to config DP
	dynAppend( specSectors, sector );
	dynAppend( specRecipients, recipient );
	dpSet( configDp + ".sectorsSpecific", specSectors,
		configDp + ".recipientsSpecific", specRecipients );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailAddSectorRecipient(): dpSetWait() failed", "" );
		throwError( err );
	}
}
// Remove recipient for specific vacuum sector
void VacEmailRemoveSectorRecipient( string sector, string recipient, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;
	dyn_string		specSectors, specRecipients;

	dynClear( exceptionInfo );
	if( sector == "" )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailRemoveSectorRecipient(): empty sector name", "" );
		return;
	}
	if( recipient == "" )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailRemoveSectorRecipient(): empty recipient address", "" );
		return;
	}
	VacEmailGetSpecRecipients( configDp, specSectors, specRecipients, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Check if given recipient for given sector is already in list
	int	n;
	for( n = dynlen( specSectors ) ; n > 0 ; n-- )
	{
		if( specSectors[n] != sector ) continue;
		if( specRecipients[n] == recipient )
		{
			dynRemove( specSectors, n );
			dynRemove( specRecipients, n );
			break;
		}
	}
	if( n <= 0 ) return;	// Specified pair sector + recipient is not in list
	// Add new sector and recipient, write to config DP
	dpSetWait( configDp + ".sectorsSpecific", specSectors,
		configDp + ".recipientsSpecific", specRecipients );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailRemoveSectorRecipient(): dpSetWait() failed", "" );
		throwError( err );
	}
}
// Switch OFF notification for vacuum sector(s)
void VacEmailDisableSector( dyn_string sectors, time endTime, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;
	dyn_string		disabledSectors;
	dyn_time			endTimes;

	dynClear( exceptionInfo );
	if( dynlen( sectors ) == 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailDisableSector(): empty sector list", "" );
		return;
	}
	VacEmailGetDisabledSectors( configDp, disabledSectors, endTimes, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Check if any changes are inserted
	bool	hasChanges = false;
	for( int n = dynlen( sectors ) ; n > 0 ; n-- )
	{
		if( sectors[n] == "" ) continue;
		int	i;
		for( i = dynlen( disabledSectors ) ; i > 0 ; i-- )
		{
			if( disabledSectors[i] != sectors[n] ) continue;
			if( endTimes[i] != endTime )
			{
				endTimes[i] = endTime;
				hasChanges = true;
				break;
			}
		}
		if( i <= 0 )
		{
			dynAppend( disabledSectors, sectors[n] );
			dynAppend( endTimes, endTime );
			hasChanges = true;
		}
	}
	if( ! hasChanges ) return;	// No changes to list content
	// Write to config DP
	dpSetWait( configDp + ".sectorsOff", disabledSectors,
		configDp + ".sectorsOffEndTime", endTimes );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailDisableSector(): dpSetWait() failed", "" );
		throwError( err );
	}
}
// Switch ON notification for vacuum sector(s)
void VacEmailEnableSector( dyn_string sectors, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;
	dyn_string		disabledSectors;
	dyn_time			endTimes;

	dynClear( exceptionInfo );
	if( dynlen( sectors ) == 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailEnableSector(): empty sector list", "" );
		return;
	}
	VacEmailGetDisabledSectors( configDp, disabledSectors, endTimes, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Check if any changes are inserted
	bool	hasChanges = false;
	for( int n = dynlen( sectors ) ; n > 0 ; n-- )
	{
		if( sectors[n] == "" ) continue;
		int	i;
		for( i = dynlen( disabledSectors ) ; i > 0 ; i-- )
		{
			if( disabledSectors[i] != sectors[n] ) continue;
			dynRemove( disabledSectors, n );
			dynRemove( endTimes, n );
			hasChanges = true;
			break;
		}
	}
	if( ! hasChanges ) return;	// No changes to list content
	// Write to config DP
	dpSetWait( configDp + ".sectorsOff", disabledSectors,
		configDp + ".sectorsOffEndTime", endTimes );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailEnableSector(): dpSetWait() failed", "" );
		throwError( err );
	}
}
// Check if DP with email config exists, if not - create such DP
void VacEmailGetConfigDp( string &configDpName, dyn_string &exceptionInfo )
{
	configDpName = "";
	if( dpExists( VAC_EMAIL_CONFIG_DP ) )
	{
		configDpName = VAC_EMAIL_CONFIG_DP;
		return;
	}
	if( dpCreate( VAC_EMAIL_CONFIG_DP, VAC_EMAIL_CONFIG_DP_TYPE ) < 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailGetConfigDp(): dpCreate() failed", "" );
		return;
	}
	if( ! dpExists( VAC_EMAIL_CONFIG_DP ) )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailGetConfigDp(): config DP not created", "" );
		return;
	}
	configDpName = VAC_EMAIL_CONFIG_DP;
}
// Read all special sectors and their recipients
void VacEmailGetSpecRecipients( string &configDp, dyn_string &specSectors,
	dyn_string &specRecipients, dyn_string &exceptionInfo )
{
	VacEmailGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Get list of sectors with specific recipients
	dpGet( configDp + ".sectorsSpecific", specSectors,
		configDp + ".recipientsSpecific", specRecipients );
	dyn_errClass err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailGetSpecRecipients(): dpGet() failed", "" );
		throwError( err );
		return;
	}
	// Two lists must have the same length
	if( dynlen( specSectors ) != dynlen( specRecipients ) )
	{
		fwException_raise(exceptionInfo, "ERROR",
			"VacEmailGetSpecRecipients(): INTERNAL: " + dynlen( specSectors ) +
			" sectors but " + dynlen( specRecipients ) + " recipients", "" );
		dynClear( specSectors );
		dynClear( specRecipients );
		dpSetWait( configDp + ".sectorsSpecific", specSectors,
			configDp + ".recipientsSpecific", specRecipients );
	}
}
// Read all sectors with disabled notification
void VacEmailGetDisabledSectors( string &configDp, dyn_string &disabledSectors,
	dyn_time &endTimes, dyn_string &exceptionInfo )
{
	VacEmailGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Get list of sectors with specific recipients
	dpGet( configDp + ".sectorsOff", disabledSectors,
		configDp + ".sectorsOffEndTime", endTimes );
	dyn_errClass err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacEmailDisabledSectors(): dpGet() failed", "" );
		throwError( err );
		return;
	}
	// Two lists must have the same length
	if( dynlen( disabledSectors ) != dynlen( endTimes ) )
	{
		fwException_raise(exceptionInfo, "ERROR",
			"VacEmailDisabledSectors(): INTERNAL: " + dynlen( disabledSectors ) +
			" sectors but " + dynlen( endTimes ) + " end times", "" );
		dynClear( specSectors );
		dynClear( specRecipients );
		dpSetWait( configDp + ".sectorsOff", disabledSectors,
			configDp + ".sectorsOffEndTime", endTimes );
	}
}
// Decide if given sector is warm beam sector
bool VacEamilIsWarmBeamSector( string sectorName )
{
	int	vacType;
	LhcVacGetVacTypeOfSector( sectorName, vacType );
	// Skip sectors for QRL and CRYO
	if( ( vacType == LVA_VACUUM_QRL ) || ( vacType == LVA_VACUUM_CRYO ) )
		return false;
	// Skip LHC ARC sectors - based on hardcoded sector name
	if( patternMatch( "ARC*", sectorName ) ) return false;
	return true;
}
