//	Implementation of VacIconVG_A_RO class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVG_A_RO.h"

#include "EqpVG_A_RO.h"

#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVG_A_RO::VacIconVG_A_RO(QWidget *parent, Qt::WindowFlags f) :
VacIconVG(parent, f) {}

VacIconVG_A_RO::~VacIconVG_A_RO()
{
}


// Connect to equipment

void VacIconVG_A_RO::setEqp(Eqp *pEqp)
{
	disconnect();
	if (pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVG_A_RO"), "VacIconVG_A_RO::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVG_A_RO *)pEqp;
	}
	forceRedraw();
}


// Draw 'Penning' symbol in the center of icon
void VacIconVG_A_RO::drawSpecific(QPainter &painter)
{
	QPen blackPen(Qt::black, 2);
	painter.setPen(blackPen);

	//painter.drawLine(3, 8, 12, 17);

	painter.setPen(Qt::black);
	QPolygon points(3);
	points.setPoint(0, 5, 8);
	points.setPoint(1, 10, 13);
	points.setPoint(2, 15, 8);
	painter.drawPolyline(points);
	
}

