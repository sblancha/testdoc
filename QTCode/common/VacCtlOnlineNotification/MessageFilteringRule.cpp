//	Implementation of MessageFilteringRule class
/////////////////////////////////////////////////////////////////////////////////

#include "MessageFilteringRule.h"
#include "FinalEqpMessage.h"
#include "MessageGroup.h"
#include "EqpMessage.h"
#include "MessageQueue.h"

#include "Eqp.h"

const char *MessageFilteringRule::filterTypeToString(int type)
{
	switch(type)
	{
	case None:
		return "None";
	case DeadTime:
		return "DeadTime";
	case Grouping:
		return "Grouping";
	default:
		break;
	}
	static char result[32];
#ifdef Q_OS_WIN
	sprintf_s(result, sizeof(result) / sizeof(result[0]), "%d", type);
#else
	sprintf(result, "%d", type);
#endif
	return result;
}

int MessageFilteringRule::filterTypeParse(const char *string, QString &errMsg)
{
	int result = 0;
	if(!string)
	{
		errMsg += "NULL filter type";
	}
	else if(!(*string))
	{
		errMsg += "Empty filter type";
	}
	else if(!strcasecmp(string, "none"))
	{
		result = None;
	}
	else if(!strcasecmp(string, "deadtime"))
	{
		result = DeadTime;
	}
	else if(!strcasecmp(string, "grouping"))
	{
		result = Grouping;
	}
	else
	{
		errMsg += "Unsupported filter type <";
		errMsg += string;
		errMsg += ">";
	}
	return result;
}

const char *MessageFilteringRule::groupTypeToString(int type)
{
	switch(type)
	{
	case GroupNone:
		return "None";
	case GroupMachine:
		return "Machine";
	case GroupMainPart:
		return "MainPart";
	case GroupSector:
		return "Sector";
	case GroupArc:
		return "Arc";
	default:
		break;
	}
	static char result[32];
#ifdef Q_OS_WIN
	sprintf_s(result, sizeof(result) / sizeof(result[0]), "%d", type);
#else
	sprintf(result, "%d", type);
#endif
	return result;
}

int MessageFilteringRule::groupTypeParse(const char *string, QString &errMsg)
{
	int result = 0;
	if(!string)
	{
		errMsg += "NULL group type";
	}
	else if(!(*string))
	{
		errMsg += "Empty group type";
	}
	else if(!strcasecmp(string, "none"))
	{
		result = GroupNone;
	}
	else if(!strcasecmp(string, "machine"))
	{
		result = GroupMachine;
	}
	else if(!strcasecmp(string, "mainpart"))
	{
		result = GroupMainPart;
	}
	else if(!strcasecmp(string, "sector"))
	{
		result = GroupSector;
	}
	else if(!strcasecmp(string, "arc"))
	{
		result = GroupArc;
	}
	else
	{
		errMsg += "Unsupported filter type <";
		errMsg += string;
		errMsg += ">";
	}
	return result;
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

MessageFilteringRule::MessageFilteringRule()
{
	type = None;
	deadTime = 0;
	groupType = GroupMachine;
	groupLimit = 0;
	groupInterval = 0;
	groupSendInitial = true;
	valid = true;
	pMsgList = new QList<FinalEqpMessage *>();
	pGroupList = new QList<MessageGroup *>();
	pGroupText = NULL;
}

MessageFilteringRule::~MessageFilteringRule()
{
	while(!pGroupList->isEmpty())
	{
		delete pGroupList->takeFirst();
	}
	delete pGroupList;
	while(!pMsgList->isEmpty())
	{
		delete pMsgList->takeFirst();
	}
	delete pMsgList;
}

/*
**	FUNCTION
**		Validate and set new filtering type
**
**	ARGUMENTS
**		newType	- Filtering type to set
**		errMsg	- Error message will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageFilteringRule::setType(int newType, QString &errMsg)
{
	switch(newType)
	{
	case None:
	case DeadTime:
	case Grouping:
		type = newType;
		valid = true;
		errMsg = "";
		break;
	default:
		errMsg = "Unknown message filtering type ";
		errMsg += QString::number(newType);
		valid = false;
		break;
	}
}

/*
**	FUNCTION
**		Validate and set new dead time
**
**	ARGUMENTS
**		newTime	- New dead time to set
**		errMsg	- Error message will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageFilteringRule::setDeadTime(int newTime, QString &errMsg)
{
	if(newTime < 0)
	{
		errMsg = "Invalid dead time ";
		errMsg += QString::number(newTime);
		if(type == DeadTime)
		{
			valid = false;
		}
		return;
	}
	deadTime = newTime;
	if(type == DeadTime)
	{
		valid = true;
	}
}

/*
**	FUNCTION
**		Validate and set new message grouping type
**
**	ARGUMENTS
**		newType	- New grouping type to set
**		errMsg	- Error message will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageFilteringRule::setGroupType(int newType, QString &errMsg)
{
	switch(newType)
	{
	case GroupMachine:
	case GroupMainPart:
	case GroupSector:
	case GroupArc:
		groupType = newType;
		errMsg = "";
		if(type == Grouping)
		{
			valid = true;
		}
		break;
	default:
		errMsg = "Unknown message grouping type ";
		errMsg += QString::number(newType);
		if(type == Grouping)
		{
			valid = false;
		}
		break;
	}
}

/*
**	FUNCTION
**		Validate and set new message count limit for grouping
**
**	ARGUMENTS
**		newLimit	- New message count limit to set
**		errMsg		- Error message will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageFilteringRule::setGroupLimit(int newLimit, QString &errMsg)
{
	if(newLimit < 0)
	{
		errMsg = "Invalid grouping count limit ";
		errMsg += QString::number(newLimit);
		if(type == Grouping)
		{
			valid = false;
		}
		return;
	}
	groupLimit = newLimit;
	errMsg = "";
	if(type == Grouping)
	{
		valid = true;
	}
}

/*
**	FUNCTION
**		Validate and set new time interval for grouping
**
**	ARGUMENTS
**		interval	- New time interval to set [sec]
**		errMsg		- Error message will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageFilteringRule::setGroupInterval(int interval, QString &errMsg)
{
	if(interval < 0)
	{
		errMsg = "Invalid grouping time interval ";
		errMsg += QString::number(interval);
		if(type == Grouping)
		{
			valid = false;
		}
		return;
	}
	groupInterval = interval;
	errMsg = "";
	if(type == Grouping)
	{
		valid = true;
	}
}

/*
**	FUNCTION
**		Validate and set new time interval for grouping
**
**	ARGUMENTS
**		text		- New text for group message
**		errMsg	- Error message will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageFilteringRule::setGroupText(const char *text, QString &errMsg)
{
	if(pGroupText)
	{
		delete pGroupText;
		pGroupText = NULL;
	}
	pGroupText = EqpMessage::create(text, errMsg);
	if(type == Grouping)
	{
		valid = pGroupText != NULL;
	}
}

/*
**	FUNCTION
**		New message has been produced for given device. Apply filtering rule
**		to the message and decide how it shall be processed
**
**	ARGUMENTS
**		pEqp		- Pointer to device that has produced the message
**		configName	- Name of E-mail configuration that produced the message
**		text		- Message text
**		messageId	- Almost the same as text, but without any values - used
**							to identify messages of the same source
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageFilteringRule::addMessage(Eqp *pEqp, const QString &configName, QString &text,
	QString &messageId)
{
	switch(type)
	{
	case DeadTime:
		addMessageWithDeadTime(configName, text, messageId);
		break;
	case Grouping:
		addMessageWithGroup(pEqp, configName, text);
		break;
	default:
		MessageQueue::addMessage(configName, text);
		break;
	}
}

/*
**	FUNCTION
**		Process new message in case when dead time filtering is used
**
**	ARGUMENTS
**		configName	- Name of E-mail configuration that produced the message
**		text		- Message text
**		messageId	- Almost the same as text, but without any values - used
**							to identify messages of the same source
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageFilteringRule::addMessageWithDeadTime(const QString &configName, QString &text, QString &messageId)
{
	for(int idx = 0 ; idx < pMsgList->count() ; idx++)
	{
		FinalEqpMessage *pMsg = pMsgList->at(idx);
		if(pMsg->getText() == messageId)
		{
			QDateTime &sendTime = pMsg->getSendTime();
			QDateTime now = QDateTime::currentDateTime();
			if(sendTime.secsTo(now) < deadTime)
			{
				return;
			}
			pMsg->setSendTimeNow();
			MessageQueue::addMessage(configName, text);
			return;
		}
	}
	pMsgList->append(new FinalEqpMessage(messageId));
	MessageQueue::addMessage(configName, text);
}

/*
**	FUNCTION
**		Process new message in case when group filtering is used
**
**	ARGUMENTS
**		pEqp		- Pointer to device that has produced the message
**		configName	- Name of E-mail configuration that produced the message
**		text		- Message text
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageFilteringRule::addMessageWithGroup(Eqp *pEqp, const QString &configName, QString &text)
{
	void *groupId = NULL;
	switch(groupType)
	{
	case GroupMainPart:
		groupId = (void *)pEqp->getMainPart();
		break;
	case GroupSector:
		groupId = (void *)pEqp->getSectorBefore();
		if(!groupId)
		{
			groupId = (void *)pEqp->getSectorAfter();
		}
		break;
	default:
		break;
	}

	// Find group with the same ID (if any)
	MessageGroup *pGroup;
	for(int idx = 0 ; idx < pGroupList->count() ; idx++)
	{
		pGroup = pGroupList->at(idx);
		if(pGroup->getGroupId() == groupId)
		{
			pGroup->addMessage(text);
			return;
		}
	}
	pGroup = new MessageGroup(groupId, configName, groupInterval);
	pGroup->addMessage(text);
	// Set parameters of 1st event in group
	pGroup->setMainPart(pEqp->getMainPart());
	if(pEqp->getSectorBefore())
	{
		pGroup->setSector(pEqp->getSectorBefore());
	}
	else
	{
		pGroup->setSector(pEqp->getSectorAfter());
	}
	pGroupList->append(pGroup);
}

/*
**	FUNCTION
**		Check if life time for message group(s) is expired. Send all individual
**		messages of groups where life time is expired
**
**	ARGUMENTS
**		now	- Current time
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MessageFilteringRule::checkGroupTimeExpired(const QDateTime &now)
{
	for(int idx = pGroupList->count() - 1 ; idx >= 0 ; idx--)
	{
		MessageGroup *pGroup = pGroupList->at(idx);
		if(!pGroup->isTimeExpired(now))
		{
			continue;
		}

		// Depending on number of messages in group - send either single group
		// message, or all individual messages in group
		QStringList &messages = pGroup->getMessages();
		QStringList::Iterator iter;
		int nMessages = messages.count();
		if((nMessages > groupLimit) && pGroupText)
		{
			QString message = pGroupText->buildMessage(pGroup);

			// Add all individual messages to E-mail body
			QString body;
			QStringList &messages = pGroup->getMessages();
			QStringList::Iterator iter;
			for(iter = messages.begin() ; iter != messages.end() ; ++iter)
			{
				body += *iter;
				body += "\n";
			}

			MessageQueue::addMessage(pGroup->getConfigName(), message, body);
		}
		else
		{
			for(iter = messages.begin() ; iter != messages.end() ; ++iter)
			{
				MessageQueue::addMessage(pGroup->getConfigName(), *iter);
			}
		}
		pGroupList->removeOne(pGroup);
	}
}

