#
# Project file for qmake utility to build SupPrintEwo EWO
#
#	13.01.2009	M.Mikheev
#		Initial version
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = SupPrint

QT += printsupport

HEADERS = SupPrint.h \
	SupPrintEwo.h

SOURCES = SupPrint.cpp \
	SupPrintEwo.cpp

