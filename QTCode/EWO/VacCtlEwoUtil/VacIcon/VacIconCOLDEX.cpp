//	Implementation of VacIconCOLDEX class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconCOLDEX.h"

#include "EqpCOLDEX.h"

#include "VacType.h"
// #include "FunctionalType.h"
#include "VacEqpTypeMask.h"

#include <qpainter.h>


// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_IN				0,255,0
#define COLOR_OUT				255,255,255
#define COLOR_AWAY				255,255,0
#define COLOR_ERROR				255,0,0

//	Main state bits to be analyzed
#define	RR1_IN			(0x01000000)
#define	RR1_OUT			(0x02000000)
#define	RR1_AWAY		(0x00080000)

static QString defaultLabel(" BENCH ");
static QString coldexLabel("COLDEX");
static QString vr_tableLabel("CRAB"); //label for subtype 2

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconCOLDEX::VacIconCOLDEX(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	// QFont myFont(font());
	// bool fontChanged = false;
	// if(myFont.pixelSize() > 0)
	// {
		// myFont.setPixelSize((int)rint(myFont.pixelSize() * 1.5));
		// fontChanged = true;
	// }
	// else if(myFont.pointSize() > 0)
	// {
		// myFont.setPointSize((int)rint(myFont.pointSize() * 1.5));
		// fontChanged = true;
	// }
	// if(fontChanged)
	// {
		// setFont(myFont);
	// }
	QRect labelRect;
	labelRect = fontMetrics().boundingRect(defaultLabel);
	setFixedSize(labelRect.width() + 6, labelRect.width() + 6);
}

VacIconCOLDEX::~VacIconCOLDEX()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		It is supposed that this method will not be used for CRYO_TT
*/
VacIconGeometry VacIconCOLDEX::getGeometry(const VacEqpTypeMask & /* mask */)
{
#ifdef Q_OS_WIN
	return VacIconGeometry(width(), height(), width() >> 1, height() >> 1, 1);
#else
	return VacIconGeometry(width(), height(), width() >> 1, (height() >> 1) - 1, 1);
#endif
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconCOLDEX::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpCOLDEX"), "VacIconCOLDEX::setEqp", pEqp->getDpName());
		this->pEqp = (EqpCOLDEX *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconCOLDEX::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconCOLDEX::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconCOLDEX::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconCOLDEX::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconCOLDEX::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	QRect myRect = rect();
	painter.eraseRect(myRect);
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	// Beam pipe connections
	QPen pen(pipeColor, pipeWidth);
	painter.setPen(pen);
	painter.drawLine(0, myRect.height() >> 1, 3, myRect.height() >> 1);
	painter.drawLine(myRect.width() - 4, myRect.height() >> 1, myRect.width(), myRect.height() >> 1);

	// Fill with color - central part with text
	setMainColor(painter);
	painter.fillRect(2, 2, myRect.width() - 4, myRect.height() - 4, painter.brush());

	// common border
	painter.setPen(Qt::gray);
	painter.setBrush(Qt::NoBrush);
	painter.drawRect(2, 2, myRect.width() - 4, myRect.height() - 4);

	// Draw label
	painter.setPen(Qt::black);
	QRect labelRect;
	switch (pEqp->getCtrlSubType())
	{
		case 2:
			labelRect = painter.fontMetrics().boundingRect(vr_tableLabel);
			break;
		default:
			labelRect = painter.fontMetrics().boundingRect(coldexLabel);
			break;
	}
	int labelY = 0;
	#ifdef Q_OS_WIN
		labelY = myRect.center().y() + (labelRect.height() >> 1) - 3;
	#else
		labelY = myRect.center().y() + (labelRect.height() >> 2);
	#endif
	switch (pEqp->getCtrlSubType())
	{
		case 2:
			painter.drawText(myRect.center().x() - (labelRect.width() >> 1) - 1, labelY, vr_tableLabel);
			break;
		default:
			painter.drawText(myRect.center().x() - (labelRect.width() >> 1) - 1, labelY, coldexLabel);
			break;
	}
}

void VacIconCOLDEX::setMainColor(QPainter &painter)
{
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
			color.setRgb(COLOR_NOT_CONTROL);
			break;
		case Eqp::NotConnected:
			color.setRgb(COLOR_NOT_CONNECTED);
			break;
		default:
			if(!pEqp->isPlcAlarm(mode))
			{
				unsigned rr1 = pEqp->getRR1(mode);
			
				if(((rr1 & RR1_IN) ? 1 : 0) + ((rr1 & RR1_OUT) ? 1 : 0) + ((rr1 & RR1_AWAY) ? 1 : 0) > 1)
				{
					color.setRgb(COLOR_ERROR);
				}
				else if(rr1 & RR1_IN)
				{
					color.setRgb(COLOR_IN);
				}
				else if(rr1 & RR1_OUT)
				{
					color.setRgb(COLOR_OUT);
				}
				else if(rr1 & RR1_AWAY)
				{
					color.setRgb(COLOR_AWAY);
				}
			}
			break;
		}
	}
	QBrush brush(color);
	painter.setBrush(brush);
}
