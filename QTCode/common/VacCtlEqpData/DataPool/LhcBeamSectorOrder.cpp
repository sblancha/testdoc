//	Implementation of LhcBeamSectorOrder class
////////////////////////////////////////////////////////////////////////////////

#include "LhcBeamSectorOrder.h"

#include "DataPool.h"
#include "Eqp.h"

#include <math.h>	// for fabs()

////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

LhcBeamSectorOrder::LhcBeamSectorOrder()
{
	pInnerList = new QList<Sector *>();
	pOuterList = new QList<Sector *>();
	pCrossList = new QList<Sector *>();
}

LhcBeamSectorOrder::~LhcBeamSectorOrder()
{
	delete pCrossList;
	delete pOuterList;
	delete pInnerList;
}

/*
**	FUNCTION
**		Add sector to list for further processing
**
**	ARGUMENTS
**		pSector	- Pointer to sector to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcBeamSectorOrder::addSector(Sector *pSector)
{

	// Decide to which of lists sector shall be added.
	switch(pSector->getVacType())
	{
	case VacType::CrossBeam:
	case VacType::CommonBeam:
		pCrossList->append(pSector);
		return;
	default:
		break;
	}

	// Red or blue beam - decide if sector is on inner or outer beam
	DataPool &pool = DataPool::getInstance();
	LhcRegion *pRegion = pool.regionAtLocation(pSector->getStart());
	if(pRegion->getType() == LhcRegion::RedOut)
	{
		if(pSector->getVacType() == VacType::RedBeam)
		{
			pOuterList->append(pSector);
		}
		else
		{
			pInnerList->append(pSector);
		}
	}
	else
	{
		if(pSector->getVacType() == VacType::BlueBeam)
		
		{
			pOuterList->append(pSector);
		}
		else
		{
			pInnerList->append(pSector);
		}
	}
}

/*
**	FUNCTION
**		Return next sector according to desired order of sectors
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Pointer to next sector, or
**		NULL if there are no more sectors
**
**	CAUTIONS
**		None
*/
Sector *LhcBeamSectorOrder::getNext(void)
{
	// Take first item of every list
	Sector *pInner = pInnerList->isEmpty() ? NULL : pInnerList->at(0);
	Sector *pOuter = pOuterList->isEmpty() ? NULL : pOuterList->at(0);
	Sector *pCross = pCrossList->isEmpty() ? NULL : pCrossList->at(0);

	Sector *pSector = NULL;
	if(pInner || pOuter || pCross)
	{
		pSector = decide(pInner, pOuter, pCross);
	}

	// Remove from list selected sector
	if(pSector)
	{
		if(pSector == pInner)
		{
			pInnerList->takeFirst();
		}
		else if(pSector == pOuter)
		{
			pOuterList->takeFirst();
		}
		else if(pSector == pCross)
		{
			pCrossList->takeFirst();
		}
	}

	return pSector;
}

Sector *LhcBeamSectorOrder::decide(Sector *pInner, Sector *pOuter, Sector *pCross)
{
/*
printf("LhcBeamSectorOrder::decide(): inner %s outer %s cross %s\n",
(pInner ? pInner->getName() : "NULL"),
(pOuter ? pOuter->getName() : "NULL"),
(pCross ? pCross->getName() : "NULL"));
fflush(stdout);
*/

	// Decision 'cross or inner/outer' is simple
	if(pCross)
	{
		if(pInner && pOuter)
		{
			if((pCross->getBeamOrder() < pInner->getBeamOrder()) &&
				(pCross->getBeamOrder() < pOuter->getBeamOrder()))
			{
				return pCross;
			}
		}
		else if(pInner)
		{
			if(pCross->getBeamOrder() < pInner->getBeamOrder())
			{
				return pCross;
			}
		}
		else if(pOuter)
		{
			if(pCross->getBeamOrder() < pOuter->getBeamOrder())
			{
				return pCross;
			}
		}
		else
		{
			return pCross;
		}
	}
/*
printf("LhcBeamSectorOrder::decide(): 1\n");
fflush(stdout);
*/

	// If only one sector is left - no choise
	if(!pInner)
	{
		return pOuter;
	}
	else if(!pOuter)
	{
		return pInner;
	}
/*
printf("LhcBeamSectorOrder::decide(): 2\n");
fflush(stdout);
*/

	// Here we have to chose between inner and outer
	// Outer is preferred unless inner is 'definitely' before the outer
	// Fortunately inner and outer beams do not pass IP1
	float innerStart = pInner->getStart(), innerEnd = pInner->getEnd();

/*
printf("LhcBeamSectorOrder::decide(): 20\n");
fflush(stdout);
*/

	float outerStart = pOuter->getStart(), outerEnd = pOuter->getEnd();

	// Let's hope LHC does not contains sectors shorter that 10 cm
	if(fabs(innerStart - outerStart) < 0.1)
	{
		innerStart = outerStart;
	}
	if(fabs(innerEnd - outerEnd) < 0.1)
	{
		innerEnd = outerEnd;
	}

/*
printf("LhcBeamSectorOrder::decide(): inner %f %f outer %f %f\n",
innerStart, innerEnd, outerStart, outerEnd);
fflush(stdout);
*/

	// Check if they are on different sides of IP1
	if(fabs(innerStart - outerStart) > 10000)
	{
		if(innerStart > 10000)
		{
			return pInner;
		}
		return pOuter;
	}
	if(innerEnd <= outerStart)
	{
		return pInner;	// Definitely before
	}

	if((outerStart > innerStart) && (outerEnd <= innerEnd))
	{
		return pOuter;	// Outer is inside inner
	}

	if(outerStart < innerStart)
	{
		return pOuter;	// Outer starts before - no further check
	}

	float common = 0, innerBefore = 0, innerAfter = 0, outerBefore = 0, outerAfter = 0;
	if(innerStart > outerStart)
	{
		outerBefore = innerStart - outerStart;
		if(innerEnd > outerEnd)
		{
			common = outerEnd - innerStart;
			innerAfter = innerEnd - outerEnd;
		}
		else
		{
			common = innerEnd - innerStart;
			outerAfter = outerEnd - innerEnd;
		}
	}
	else
	{
		innerBefore = outerStart - innerStart;
		if(innerEnd > outerEnd)
		{
			common = outerEnd - outerStart;
			innerAfter = innerEnd - outerEnd;
		}
		else
		{
			common = innerEnd - outerStart;
			outerAfter = outerEnd - innerEnd;
		}
	}

/*
printf("LhcBeamSectorOrder::decide(): before: I %f O %f, common %f, after I %f O %f\n",
innerBefore, outerBefore, common, innerAfter, outerAfter);
fflush(stdout);
*/

	if(innerBefore > 0)	// Inner start before outer
	{
		if(innerAfter > 0)
		{
			return pOuter;	// Outer is completely inside inner
		}
		/*
		if(outerAfter > innerBefore)
		{
			return pOuter;	// A5R6.B - A5R6.R
		}
		*/
		if(common < (innerBefore + outerAfter))
		{
			return pInner;
		}
	}
	else if(outerBefore > 0)
	{
		if(outerAfter > 0)
		{
			return pInner;	// Inner is completely inside outer
		}
		/*
		if(common > (outerBefore + innerAfter))
		{
			return pInner;
		}
		*/
	}

	return pOuter;
}
