
//#uses "VacEqpData"	// Load DLL 
//#uses "lhcVacEqpConfig.ctl"	// Load CTRL library
#uses "VacCtlEqpDataPvss"	// Load DLL
#uses "eqp/vclVV.ctl"	// Load CTRL library
#uses "email.ctl"	// Load PVSS standard CTRL library
#uses "vclDevCommon.ctl"	// Load CTRL library


//********************** General configuration *********************************
// SMTP server
string smtpServer;

// E-mail sender
string sender, timedDpName;

// E-mail sender client
string	client;

// Recipients for sms
dyn_string	recipients;

// The interval (in sec) to count archived events
	int					gInterval;
// The interval to restart the check procedure
	int					gRestart;
	unsigned		startTime;

main()
{
	dyn_string 	ilDp;
	dyn_string	exceptionInfo;
	dyn_int			ilValue;
	// Initialize static data
	if( ! InitStaticData() ) return;

	LhcVacValveIntrlckTimedDp(timedDpName, exceptionInfo );
	ConnectToConfig( );
	if(dynlen(exceptionInfo))
	{
		DebugTN("lhcVacValveInterlock.ctl ERROR: Processing in LhcVacValveIntrlckTimedDp");
	}
	gInterval = 0; 
	dpSet( timedDpName + ".validFrom", makeTime(2006,6,1),
      	timedDpName + ".validUntil", makeTime(2032,6,1),
        timedDpName + ".time", makeDynInt(startTime), 				
        timedDpName + ".monthDay", makeDynInt(), 				
        timedDpName + ".month", makeDynInt(), 				
        timedDpName + ".weekDay", makeDynInt(), 			
        timedDpName + ".month", makeDynInt(), 				
     		timedDpName + ".delay", 0,
      	timedDpName + ".interval", gInterval,			//Interval once per day
     		timedDpName + ".syncDay", -1,
     		timedDpName + ".syncWeekDay", -1,
    		timedDpName + ".syncTime", -1, 
     		timedDpName + ".syncMonth", -1);
	DebugTN("lhcVacValveInterlock.ctl: Check time initialized");
 	timedFunc ("MailValveInterlock", timedDpName);	
}

void MailValveInterlock()
{
	dyn_string 	ilDp;
	dyn_string	exceptionInfo;
	dyn_int			ilValue;
	int					coco;
	
	CheckLhcValveInterlock(ilDp, ilValue);
	string body = PrepareBody(ilDp, ilValue);
	coco = SendMessages(body);		
}

/** ConnectToConfig
Purpose:
Connect to DPEs containing E-mail notification configuration

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToConfig( )
{
	string				configDp;
	dyn_string		exceptionInfo;
	dyn_errClass	err;

	VacValveInterlockGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "lhcVacValveInterlock.ctl: FATAL: " + exceptionInfo );
		return false;
	}

	if( dpConnect( "ServerCb", configDp + ".server" ) < 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacValveInterlock.ctl: FATAL: dpConnect(" + configDp + ".server) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "ClientCb", configDp + ".client" ) < 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacValveInterlock.ctl: FATAL: dpConnect(" + configDp + ".client) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "SenderCb", configDp + ".sender" ) < 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacValveInterlock.ctl: FATAL: dpConnect(" + configDp + ".sender) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "RecipientsCb", configDp + ".recipients" ) < 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacValveInterlock.ctl: FATAL: dpConnect(" + configDp + ".recipients) failed" );
		throwError( err );
		return false;
	}
	if( dpConnect( "StartTimeCb", configDp + ".startTime" ) < 0 )
	{
		err = getLastError();
		DebugTN( "lhcVacValveInterlock.ctl: FATAL: dpConnect(" + configDp + ".startTime) failed" );
		throwError( err );
		return false;
	}
	return true;
}
// Callbacks from DP containing general E-mail configuration
void ServerCb( string dpe, string newServer )
{
	smtpServer = newServer;
}
void ClientCb( string dpe, string newClient )
{
	client = newClient;
}
void SenderCb( string dpe, string newSender )
{
	sender = newSender;
}
void RecipientsCb( string dpe, dyn_string newRecipients )
{
	recipients = newRecipients;
}
void StartTimeCb( string dpe, unsigned interval)
{
	startTime = interval;
  string startTimeDpe = timedDpName + ".time";
  dpSet(startTimeDpe, makeDynInt(startTime));				
DebugN("NewStartTime:", startTimeDpe, startTime);
}


/* InitStaticData
Purpose:
Read all machine configuration data from file(s)

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool InitStaticData( )
{
	dyn_string	exceptionInfo;
	long				coco;
	bool				isLhcData;

	// Set accelerator name to be used by other components
	LhcVacGetMachineId( exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "lhcVacValveInterlock.ctl: Failed to read machine name: " + exceptionInfo );
		return false;
	}
	DebugTN( "lhcVacValveInterlock.ctl: Machine is " + glAccelerator );
	isLhcData = glAccelerator == "LHC" ? true : false;
	// Initalize passive machine data
	coco = LhcVacEqpInitPassive( isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo );
	if( coco < 0 )
	{
		DebugTN( "lhcVacValveInterlock.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo );
		return false;
	}
	DebugTN( "lhcVacValveInterlock.ctl: LhcVacEqpInitPassive() done" );

	// Initialzie active equipment information
//	coco = LhcVacEqpInitActive( isLhcData, glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
//			exceptionInfo );
//	if( coco < 0 )
//	{
//		DebugTN( "lhcVacValveInterlock.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo );
//		return false;
//	}
//	DebugTN( "lhcVacValveInterlock.ctl: LhcVacEqpInitActive() done" );

//	DebugTN( "lhcVacValveInterlock.ctl: InitStaticData() done" );
	return true;
}
/** SendMessages
Purpose:
Send E-mails for all messages pending in the message queue

Parameters:
	- None

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void SendMessages_0(dyn_string ilDp, dyn_int ilValue)
{
	int					n, coco, sendFailureCounter = 0;
	string 			alertMsg;
		// Prepare and send message
		dyn_string	email = makeDynString( "" );
		if(dynlen(recipients) == 0)
			return;
		for( n = dynlen( recipients ) ; n > 0 ; n-- )
		{
			if( email[1] != "" ) email[1] += ";";
			email[1] += recipients[n];
		}
		email[2] = sender;
		email[3] = "LHC Valve Interlock List";
		for( n = dynlen( ilDp ) ; n > 0 ; n-- )
		{
			email[4] += ilDp[n] + ": ";
			if(ilValue[n] &  0x01u)
				email[4] += "| LCL_PRESS";
			if(ilValue[n] &  0x02u)
				email[4] += "| LCL_TEMP_HARDWARE";
			if(ilValue[n] &  0x04u)
				email[4] += "| VVS_BEFORE";
			if(ilValue[n] &  0x08u)
				email[4] += "| VVS_AFTER";				
			if(ilValue[n] &  0x10u)
				email[4] += "| VVS_TEMP_SOFTWARE";				
			email[4] += "\n";
		}
		DebugTN( "lhcVacValveInterlock.ctl: message to " + email[1] + " is '" + email[3] + "'" + email[4]);
		emSendMail( smtpServer, client, email, coco );
		if( coco < 0 )	// Failed, make pause then try again
		{
			if( sendFailureCounter < 3 )
			{
				sendFailureCounter++;
				return;
			}
		}
		sendFailureCounter = 0;	// Reset failures counter
}
/** SendMessages
Purpose:
Send E-mails for all messages pending in the message queue

Parameters:
	- None

Return:
	- CoCode

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
int SendMessages(string body)
{
	int					n, coco = 0, sendFailureCounter = 0;
	string 			alertMsg;
	// Prepare and send message
	dyn_string	exceptionInfo, email = makeDynString( "","","","" );
	dyn_float	posList;
	float	pos;
		if(dynlen(recipients) == 0)
			return -1;
		for( n = dynlen( recipients ) ; n > 0 ; n-- )
		{
			if( email[1] != "" ) email[1] += ";";
			email[1] += recipients[n];
		}
		email[2] = sender;
		email[3] = "LHC Valve Interlock List";
		email[4] = body;
		DebugTN( "lhcVacValveInterlock.ctl: message to " + email[1] + " is '" + email[3] + "'" + email[4]);
		int	coco;
		emSendMail( smtpServer, client, email,  coco );
		if( coco < 0 )	// Failed, make pause then try again
		{
			if( sendFailureCounter < 3 )
			{
				sendFailureCounter++;
				return coco;
			}
		}
		sendFailureCounter = 0;	// Reset failures counter
		return coco;
}
