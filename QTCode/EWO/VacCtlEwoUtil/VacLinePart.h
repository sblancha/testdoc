#ifndef VACLINEPART_H
#define	VACLINEPART_H

#include "VacCtlEwoUtilExport.h"

// Part of vacuum line - synoptic may be built of many parts

#include <stdio.h>

class Sector;

class VACCTLEWOUTIL_EXPORT VacLinePart
{
public:
	VacLinePart(int vacType, float start, float end, bool afterIp1);
	~VacLinePart();

	// Access
	inline float getStartPos(void) const { return startPos; }
	inline void setStartPos(float pos) { startPos = pos; }
	inline float getEndPos(void) const { return endPos; }
	inline void setEndPos(float pos) { endPos = pos; }
	inline float getRealStart(void) const { return realStart; }
	inline float getRealEnd(void) const { return realEnd; }
	inline Sector *getSector(void) const { return pSector; }
	inline void setSector(Sector *pNewSector) { pSector = pNewSector; }
	inline int getStartX(void) const { return startX; }
	inline void setStartX(int pos) { startX = pos; }
	inline int getEndX(void) const { return endX; }
	inline void setEndX(int pos) { endX = pos; }
	inline int getVacType(void) const { return vacType; }
	inline bool isAfterIP1(void) const { return afterIP1; }
	inline bool isStarted(void) const { return started; }
	inline void setStarted(bool flag) { started = flag; }
	inline bool isFinished(void) const { return finished; }
	inline void setFinished(bool flag) { finished = flag; }
	inline bool isStartDone(void) const { return startDone; }
	inline void setStartDone(bool flag) { startDone = flag; }
	inline bool isDrawStart(void) const { return drawStart; }
	inline void setDrawStart(bool draw) { drawStart = draw; }
	inline bool isDrawEnd(void) const { return drawEnd; }
	inline void setDrawEnd(bool draw) { drawEnd = draw; }

	inline int getStartOrder(void) const { return startOrder; }
	inline void setStartOrder(int order) { startOrder = order; }
	inline int getEndOrder(void) const { return endOrder; }
	inline void setEndOrder(int order) { endOrder = order; }
	inline int getCorrDeltaStart(void) const { return corrDeltaStart; }
	inline void setCorrDeltaStart(int delta) { corrDeltaStart = delta; }
	inline int getCorrDeltaEnd(void) const { return corrDeltaEnd; }
	inline void setCorrDeltaEnd(int delta) { corrDeltaEnd = delta; }
	void dump(FILE *pFile, int idx);

protected:
	// Start of line part in world coordinates
	float		startPos;

	// End of line part in world coordinates
	float		endPos;

	// Real start of line part in world coordinates
	float		realStart;

	// Real end of line part in world coordinates
	float		realEnd;

	// Pointer to sector data if this part corresponds to sector
	Sector		*pSector;

	// Start of line part in screen coordinates
	int			startX;

	// End of line part in screen coordinates
	int			endX;

	int			startOrder;		// Start processing order - for debugging only
	int			endOrder;		// End processing order - for debugging only
	int			corrDeltaStart;	// Correction for start screen position
	int			corrDeltaEnd;	// Correction for end screen position
	
	// Vacuum type
	int			vacType;

	// true if part of vacuum line is added after passing IP1, thus coordinates are > LHC ring length
	bool		afterIP1;

	// true if line start is within picture
	bool		started;

	// true if line finish is within picture
	bool		finished;

	// true if line start has been processed by horizontal coordinate calculations
	bool		startDone;

	// true if start of part shall be drawn
	bool		drawStart;

	// true if end of part shall be drawn
	bool		drawEnd;
};

#endif	// VACLINEPART_H
