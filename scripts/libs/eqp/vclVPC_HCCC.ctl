
/***********************************************************************************
****************** Constants for VPC_HCCC **************************
***********************************************************************************/

const unsigned
	RR1_LOCKED 		= 	0x80000000u,
	RR1_VALID 		= 	0x40000000u,
	RR1_LOCAL		=	0x10000000u,
	RR1_CRYOOK  	=	0x08000000u,
	RR1_COOLDOWN	=	0x20000000u,
	RR1_ON_NOMINAL	=	0x02000000u,	
	RR1_OFF			=	0x01000000u,	
	RR1_ERROR		=	0x00800000u,
	RR1_WARNING		=	0x00400000u,
	RR1_PREVAC		=	0x00020000u,
	RR1_REGEN		=	0x00010000u;
	
const string	
	COLOR_UNDEFINED	=	"[25,50,100]",  
	COLOR_NOT_CONNECTED	=	"[50,50,50]",  
	//COLOR_ON		=	"[0,100,0]",
	COLOR_STARTING	=	"[50,100,50]",
	COLOR_ON_ERROR	=	"[80,100,0]",
	COLOR_ON_WARNING=	"[60,100,0]",
	COLOR_OFF		=	"[100,100,100]",
	COLOR_CLOSE		=	"[100,0,0]",
	COLOR_OFF_ERROR	=	"[100,0,0]",
	COLOR_OFF_WARNING=	"[100,100,0]",
	//COLOR_ERROR		=	"[100,40,100]",
	//COLOR_WARNING	=	"[100,100,0]",
	COLOR_FORESTD	=	"[0,0,0]",
	COLOR_FOREGREY	=	"[50,50,50]",
	COLOR_BACKGREY	=	"[75,75,75]";

/***********************************************************************************
****************** Functions for VPC_HCCC **************************
***********************************************************************************/	
//LhcVacActionPossible_VPC_HCCC:
/** Check if pump is in state that allows executing actions for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, action to be executed
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_VPC_HCCC(string dpName, string dpType, const dyn_anytype act, bool &possible,
	dyn_string &exceptionInfo )
{
  dyn_errClass 	err;
  unsigned	state;
  string	dpNameAct, dpTypeAct;
		
//  LhcVacGetDpForAct(dpName, dpType, dpNameAct, dpTypeAct, exceptionInfo);
//  if(dpTypeAct == "VPC_HCCC")
  
// all actions are possible:
    possible = true;
    return;
}
	
//LhcVacIsShowMenuItem_VPC_HCCC:
/** Check if menu item has to be shown

Usage: Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param dpType: in, DP type
	@param showItem: out,  (show/hide) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacIsShowMenuItem_VPC_HCCC(string dpName, string dpType, unsigned action, bool &showItem)
{
  string masterDpName, usageInMaster;
  switch(action & LVA_ACTION_MASK)
  {
  case LVA_DEVICE_ON:
  case LVA_DEVICE_OFF:
  case LVA_DEVICE_RESET:
  case LVA_DEVICE_VPC_HCCC_REGEN:
    showItem = true;
    break;
  default:
    return;
  }  
}

//LhcVacDeviceAction_VPC_HCCC:
/** Execute action It has been checked before call that this DP type
	understands this action.

Usage: Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
int LhcVacDeviceAction_VPC_HCCC(string dpName, string dpType, dyn_anytype  action,
  dyn_string &exceptionInfo)
{
  unsigned	    newWr1, act;
  bool		      possible;
  dyn_errClass 	err;
  mapping       mConnectDpes;
  

  act = action[1] & LVA_ACTION_MASK;
  // WR1 is 16bits
  switch(act)
  {
  case LVA_DEVICE_VPC_HCCC_COOLDOWN:
	newWr1 = 0x600u;
    break;
  case LVA_DEVICE_OFF:
	newWr1 = 0x500u; 
    break;
  case LVA_DEVICE_VPC_HCCC_REGEN:
 	newWr1 = 0x401u; 
    break; 
  case LVA_DEVICE_AUTO:
 	newWr1 = 0x800u; 
    break;   
  case LVA_DEVICE_MANUAL:
  	newWr1 = 0x400u;   
    break;  	
  case LVA_DEVICE_RESET:
   	newWr1 = 0x8400u;   
	break;
  case LVA_DEVICE_VPC_HCCC_RESET_COMP:
	  newWr1 = 0x402u;
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;    
    mConnectDpes["RUNTIME"] = NO_VALUE_CHANGE;                     
    mConnectDpes["STARTCOUNTER"] = NO_VALUE_CHANGE;
    mConnectDpes["T1PV"] = NO_VALUE_CHANGE;
    mConnectDpes["T2PV"] = NO_VALUE_CHANGE;
    mConnectDpes["P1PV"] = NO_VALUE_CHANGE;
    mConnectDpes["P2PV"] = NO_VALUE_CHANGE;
    mConnectDpes["P3PV"] = NO_VALUE_CHANGE;
    mConnectDpes["P4PV"] = NO_VALUE_CHANGE;
    mConnectDpes["DIST"] = NO_VALUE_CHANGE;
    mConnectDpes["DOST"] = NO_VALUE_CHANGE;
    mConnectDpes["PARAMST"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;    
    mConnectDpes["RUNTIME"] = NO_VALUE_CHANGE;                     
    mConnectDpes["STARTCOUNTER"] = NO_VALUE_CHANGE;
    mConnectDpes["T1PV"] = NO_VALUE_CHANGE;
    mConnectDpes["T2PV"] = NO_VALUE_CHANGE;
    mConnectDpes["P1PV"] = NO_VALUE_CHANGE;
    mConnectDpes["P2PV"] = NO_VALUE_CHANGE;
    mConnectDpes["P3PV"] = NO_VALUE_CHANGE;
    mConnectDpes["P4PV"] = NO_VALUE_CHANGE;
    mConnectDpes["DIST"] = NO_VALUE_CHANGE;
    mConnectDpes["DOST"] = NO_VALUE_CHANGE;
    mConnectDpes["PARAMST"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VPC_HCCC: unknown action " + act, "");
    return LVA_FAILURE;
  }

  // Execute
  dpSetWait(dpName + ".WR1", newWr1);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return LVA_FAILURE;
  }
  else 
  {
	return LVA_OK;
  }
}



