#ifndef	VACICONVPG_6A01_H
#define	VACICONVPG_6A01_H

//	Icon for VPG_6A01 - only group, without related VVR

#include "VacIcon.h"

class EqpProcess;

class VACCTLEWOUTIL_EXPORT VacIconVPG_6A01 : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPG_6A01(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPG_6A01();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);


protected:
	// Pointer to my device
	EqpProcess *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter);
	void drawErrorWarning(QPainter &painter, unsigned rr1);
};

#endif	// VACICONVPG_6A01_H
