#include "RackIcon.h"

#include "RackIconVRPIT.h"
#include "RackIconVRGPT300.h"
#include "RackIconVRGPK.h"
#include "RackIconGeneric.h"
#include "RackIconHidden.h"
#include "RackIconVRPTP350.h"
#include "RackIconVRLRI.h"
#include "RackIconVRVCL.h"
#include "RackIconVRVCX.h"
#include "VacMainView.h"
#include <qevent.h>

/*
**	FUNCTION
**		Return instance of icon for given equipment type
**
**	ARGUMENTS
**		type	- equipment type string
**		parent	- parent widget for this icon
**
**	RETURNS
**		Pointer to new instance of icon for given type, or
**		NULL if such type is not supported (yet)
**
**	CAUTIONS
**		It is responsability of caller to delete returned instance
*/
RackIcon * RackIcon::getIcon(QString type, QWidget * parent)
{
	RackIcon * icon = Q_NULLPTR;
	switch (RackIcon::getType(type))
	{
	case RackIcon::VRPITHJA:
	case RackIcon::VRPITHJB:
	case RackIcon::VRPITCFD:
		icon = new RackIconVRPIT(parent);
		break;
	case RackIcon::VRGPT300:
		icon = new RackIconVRGPT300(parent);
		break;
	case RackIcon::VRGPK001:
		icon = new RackIconVRGPK(parent);
		break;
	case RackIcon::VRGCT001:
	case RackIcon::VRPCT001:
		icon = new RackIconHidden(parent);
		break;
	case RackIcon::VRPTP350:
	case RackIcon::VRPTP120:
		icon = new RackIconVRPTP350(parent);
		break;
	case RackIcon::VRLRI:
		icon = new RackIconVRLRI(parent);
		break;
	case RackIcon::VRVCL:
		icon = new RackIconVRVCL(parent);
		break;
	case RackIcon::VRVCX:
		icon = new RackIconVRVCX(parent);
		break;
	default:
		icon = new RackIconGeneric(parent);
		break;
	}
	return icon;
}


/*
**	FUNCTION
**		Translate string equipment type names to actual names
**
**	ARGUMENTS
**		typeName	- equipment type string
**
**	RETURNS
**		eqpType enumeration
**
**	CAUTIONS
**		any name not recognized will return "generic" type
*/
RackIcon::eqpType RackIcon::getType(QString typeName)
{
	if (typeName.compare("VRGPT300") == 0)
	{
		return RackIcon::VRGPT300;
	}
	else if (typeName.compare("VRPITHJA") == 0)
	{
		return RackIcon::VRPITHJA;
	}
	else if (typeName.compare("VRPITHJB") == 0)
	{
		return RackIcon::VRPITHJB;
	}
	else if (typeName.compare("VRPITCFD") == 0)
	{
		return RackIcon::VRPITCFD;
	}
	else if (typeName.compare("VRGPK001") == 0)
	{
		return RackIcon::VRGPK001;
	}
	else if (typeName.compare("VRGCT002") == 0)
	{
		return RackIcon::VRGCT001;
	}
	else if (typeName.compare("VRPCTA01") == 0)
	{
		return RackIcon::VRPCT001;
	}
	else if (typeName.compare("VRPTP350") == 0)
	{
		return RackIcon::VRPTP350;
	}
	else if (typeName.compare("VRPTP120") == 0)
	{
		return RackIcon::VRPTP120;
	}
	else if (typeName.compare("VRLRI001") == 0)
	{
		return RackIcon::VRLRI;
	}
	else if (typeName.compare("VRVCL001") == 0)
	{
		return RackIcon::VRVCL;
	}
	else if (typeName.compare("VRVCX001") == 0)
	{
		return RackIcon::VRVCX;
	}
	else
		return RackIcon::GENERIC;

}

//construction and destruction
RackIcon::RackIcon(QWidget *parent, Qt::WindowFlags f)
: QWidget(parent, f)
{
	this->parent = parent;
	//connect signals here - connection saying state changed or sending mouse down to main view
	QObject::connect(&connection, SIGNAL(stateChanged()), this, SLOT(forceRedraw()));
	QObject::connect(&connection, SIGNAL(selectChanged()), this, SLOT(forceRedraw()));

	QObject::connect(this, SIGNAL(selectedEqp(RackIcon *)), parent, SIGNAL(selectedEqp(RackIcon *)));
	//set defaults 
	this->sizeX = 1;
	this->sizeY = 1;
	this->scale = 1.0;
	this->positionX = 0;
	this->positionY = 0;
	connected = false;
	isSelected = false;

	VacMainView *pMainView = VacMainView::getInstance();
	if (pMainView)
	{
		QObject::connect(this, SIGNAL(mouseDown(int, int, int, int, int, const char *)),
			pMainView, SIGNAL(rackIconMouseDown(int, int, int, int, int, const char *)));
	}
}

RackIcon::~RackIcon()
{
	QObject::disconnect(&connection, SIGNAL(selectChanged()), this, SLOT(forceRedraw()));
	QObject::disconnect(&connection, SIGNAL(stateChanged()), this, SLOT(forceRedraw()));
	QObject::disconnect(this, SIGNAL(selectedEqp(RackIcon *)), parent, SIGNAL(selectedEqp(RackIcon *)));
}


/*
**	FUNCTION
**		Force redraw of this icon if it is visible
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void RackIcon::forceRedraw(void)
{

	update();
}

/*
**	FUNCTION
**		set size of icon in widget
**
**	ARGUMENTS
**		int sizeX - width
**		int sizeY - height
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void RackIcon::setSize(int sizeX, int sizeY)
{
	if (sizeX > 0)
		this->sizeX = sizeX;
	
	if (sizeY > 0)
		this->sizeY = sizeY;

	setFixedSize(this->sizeX, this->sizeY);
}

void RackIcon::setName(QString name)
{
	this->equipmentName = name;
}

/*
**	FUNCTION
**		recalculate position to account for zoom
**
**	ARGUMENTS
**	
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void RackIcon::recalculatePosition(int pixelRackWidth, int pixelRackHeight, int pixelRackFrameWidth, int pixelRackFrameHeight)
{
	int newPositionX = (pixelRackFrameWidth + (this->positionX-1)*pixelRackWidth / 100)*this->scale;
	int newPositionY = (pixelRackHeight + pixelRackFrameHeight - (this->positionY - 1)*pixelRackHeight / 45 - this->sizeY)*this->scale;
	
	this->move(newPositionX, newPositionY);
	this->setFixedSize(sizeX*scale, sizeY*scale);
}

void RackIcon::setPosition(int positionX, int positionY)
{
	if (positionX > 0 && positionX <= 100)
	{
		this->positionX = positionX;
	}
	else
		this->positionX = 0;

	if (positionY >= 1 && positionY <= 45)
	{
		this->positionY = positionY;
	}
	else
		this->positionY = 0;
}

void RackIcon::mousePressEvent(QMouseEvent *pEvent)
{

		QPoint mousePoint(pEvent->x(), pEvent->y());
		QPoint screenPoint = mapToGlobal(mousePoint);
		int buttonNbr = 0;
		switch (pEvent->button())
		{
		case Qt::LeftButton:
			buttonNbr = 1;
			break;
		case Qt::RightButton:
			buttonNbr = 2;
			break;
		default:
			break;
		}
		if (buttonNbr)
		{
			//qt signal mechanism guarantees data from local const * char .data() is copied or read before it goes out of scope
			emit mouseDown(buttonNbr, DataEnum::Online, screenPoint.x(), screenPoint.y(), 0, equipmentName.toUtf8().data());
			if (buttonNbr == 1)
			{
				emit selectedEqp(this);
			}
		}
}

void RackIcon::selectEqp()
{
	if (!isSelected)
	{
		isSelected = true;
		this->raise();
		this->forceRedraw();
	}
}

void RackIcon::deselectEqp()
{
	if (isSelected)
	{
		isSelected = false;
		this->lower();
		this->forceRedraw();
	}
}