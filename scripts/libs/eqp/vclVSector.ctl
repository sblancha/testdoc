#uses "VacCtlEqpDataPvss"
#uses "panel"

const uint VSECTOR_OPERATIONAL =  0x00000001u;
const uint VSECTOR_ONWORK =       0x00000002u;
const uint VSECTOR_VENTED =       0x00000004u;


// LhcVacDeviceAction_VSector:
/** [VACCO-948] [VACCO-1645]
    Execute action for vacuum sectors. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param action: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VSector(string dpName, string dpType, dyn_anytype action, dyn_string &exceptionInfo)
{
  unsigned      act;
  act = action[1] & LVA_ACTION_MASK;
  uint sectorStatus;
  dpGet(dpName + ".State", sectorStatus);
  switch(act & LVA_ACTION_MASK){
    case LVA_VSECTOR_OPERATIONAL:
      sectorStatus |= VSECTOR_OPERATIONAL;
      sectorStatus &= ~VSECTOR_ONWORK;
      sectorStatus &= ~VSECTOR_VENTED;
    	break;
    case LVA_VSECTOR_ONWORK:
      sectorStatus |= VSECTOR_ONWORK;
      sectorStatus &= ~VSECTOR_OPERATIONAL;
      sectorStatus &= ~VSECTOR_VENTED;
    	break;
    case LVA_VSECTOR_VENTED:
      sectorStatus |= VSECTOR_VENTED;
      sectorStatus &= ~VSECTOR_OPERATIONAL;
      sectorStatus &= ~VSECTOR_ONWORK;
    	break;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }
  dpSet(dpName + ".State", sectorStatus);
  LhcVacNewOnlineValue(dpName + ".State", sectorStatus, getCurrentTime());
}
