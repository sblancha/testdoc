#ifndef	IMAGINEITEMRECTANGLE_H
#define	IMAGINEITEMLINE_H

#include "ImageItem.h"

class VACCTLEWOUTIL_EXPORT ImageItemRectangle : public ImageItem
{
public:
	ImageItemRectangle();
	~ImageItemRectangle();

	virtual void draw(QPainter & painter);
	virtual bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	// Access
	inline int getStartX(void) const { return startX; }
	inline void setStartX(int coord) { startX = coord; }
	inline int getStartY(void) const { return startY; }
	inline void setStartY(int coord) { startY = coord; }
	inline int getSizeX(void) const { return sizeX; }
	inline void setSizeX(int coord) { sizeX = coord; }
	inline int getSizeY(void) const { return sizeY; }
	inline void setSizeY(int coord) { sizeY = coord; }
	inline bool isFilled(void) const { return filled; }
	inline void setFilled(bool flag) { filled = flag; }

protected:
	// Start/end coordinates
	int		startX;
	int		startY;
	int		sizeX;
	int		sizeY;

	// Filled of not
	bool	filled;

	virtual void parseProp(QXmlStreamReader &reader);
};

#endif	// IMAGINEITEMRECTANGLE_H
