//	Implementation of ImageItemRectangle class
/////////////////////////////////////////////////////////////////////////////////

#include "ImageItemRectangle.h"

#include <QLocale>

//////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
//////////////////////////////////////////////////////////////////////////////////

ImageItemRectangle::ImageItemRectangle() : ImageItem()
{
	startX = startY = sizeX = sizeY = 0;
	filled = false;
}

ImageItemRectangle::~ImageItemRectangle()
{
}


void ImageItemRectangle::draw(QPainter & painter)
{
	painter.setTransform(transform);
	if((backColor.alpha() > 0) && filled)
	{
		painter.setBrush(backColor);
	}
	else
	{
		painter.setBrush(Qt::NoBrush);
	}
	setForePen(painter);
	painter.drawRect(startX, startY, sizeX, sizeY);
}

bool ImageItemRectangle::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	if(!visible)
	{
		return false;
	}
	if(toolTipText.isEmpty())
	{
		return false;
	}
	QRect frameRect(startX, startY, sizeX, sizeY);
	if(!frameRect.contains(point))
	{
		return false;
	}
	text = toolTipText;
	rect = frameRect;
	return true;
}

void ImageItemRectangle::parseProp(QXmlStreamReader &reader)
{
	QStringRef propName = reader.attributes().value("name");
//printf("ImageItemRectangle::parseProp(): propName <%s>\n", propName.toString().toAscii().constData());
	if(!propName.compare("Location", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		startX = (int)rint(c.toDouble(str.section(' ', 0, 0)));
		startY = (int)rint(c.toDouble(str.section(' ', 1, 1)));
	}
	else if(!propName.compare("Size", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		sizeX = (int)rint(c.toDouble(str.section(' ', 0, 0)));
		sizeY = (int)rint(c.toDouble(str.section(' ', 1, 1)));
	}
	else if(!propName.compare("FillType", Qt::CaseInsensitive))
	{
		filled = !reader.readElementText().contains("outline", Qt::CaseInsensitive);
	}
	else
	{
		ImageItem::parseProp(reader);
		return;
	}
}

