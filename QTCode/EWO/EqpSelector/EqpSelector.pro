#
# Project file for qmake utility to build SectorSelector EWO
#
#	25.05.2010	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = EqpSelector

INCLUDEPATH += ../../common/VacCtlUtil \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/Platform \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp \
	../../common/VacCtlOnlineNotification \
	../VacCtlEwoUtil

HEADERS =	\
	QListBoxEqp.h \
	EqpSelectorControl.h \
	EqpSelector.h

SOURCES =	\
	QListBoxEqp.cpp \
	EqpSelectorControl.cpp \
	EqpSelector.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlEqpData \
	-lVacCtlHistoryData \
	-lVacCtlOnlineNotification \
	-lVacCtlUtil

