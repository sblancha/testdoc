#ifndef BAKEOUTCTLTABLE_H
#define	BAKEOUTCTLTABLE_H

// Table for displaying control channels of bakeout rack

#include "BakeoutCtlChannel.h"

#include <qtablewidget.h>
#include <qlist.h>

class BakeoutCtlTable : public QTableWidget
{
	Q_OBJECT

public:
	BakeoutCtlTable(QWidget *parent);
	~BakeoutCtlTable();

	void setEqp(Eqp *pEqp);
	inline Eqp *getEqp(void) const { return pEqp; }

	virtual QSize sizeHint(void) const { return recommendedSize; }

private slots:
	void eqpDataChange(BakeoutCtlChannel *pSrc, bool colorChanged);

protected:
	// Relative widths for all columns
	static float	colWidthCoeff[6];

	// Flag indicating if relative coulmn widths have been initialized from resources
	static bool	colWidthReady;

	// Pointer to device shown in profile
	Eqp			*pEqp;

	// List of all control channels in table
	QList<BakeoutCtlChannel *>	channels;

	virtual void resizeEvent(QResizeEvent *event);

	// Size hint
	QSize	recommendedSize;
};

#endif	// BAKEOUTCTLTABLE_H
