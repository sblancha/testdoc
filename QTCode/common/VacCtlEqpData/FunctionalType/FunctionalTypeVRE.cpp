//	Implementation of FunctionalTypeVRE class
////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVRE.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeVRE::FunctionalTypeVRE(int type, const char *name, const char *description) :
FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVRE::fillCriteria(FunctionalType *pType)
{
	// Own criteria
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Connected, "Bakeout Rack is connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is Disconnected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Bakeout Rack in sector {sector} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Bakeout Rack in sector {sector} is Disconnected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Bakeout Rack in main part {mainPart} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Bakeout Rack in main part {mainPart} is Disconnected");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, BakeoutStopped, "Bakeout is Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Bakeout Rack in sector {sector} is Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Bakeout Rack in sector {sector} is NOT Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Bakeout Rack in main part {mainPart} is Stopped");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Bakeout Rack in main part {mainPart} is NOT Stopped");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, BakeoutInProgress, "Bakeout in Progress");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} Bakeout in Progress");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} Bakeout is NOT in Progress");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Bakeout Rack in sector {sector} is Bakeout in Progress");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Bakeout Rack in sector {sector} is NOT in Progress");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Bakeout Rack in main part {mainPart} is Bakeout in Progress");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Bakeout Rack in main part {mainPart} is NOT in Progress");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "Bakeout Error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} Bakeout in Error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} Bakeout is NOT in Error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Bakeout Rack in sector {sector} in Error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Bakeout Rack in sector {sector} NOT in Error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Bakeout Rack in main part {mainPart} in Error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Bakeout Rack in main part {mainPart} NOT in Error");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Warning, 0, "Bakeout Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} Bakeout in Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} Bakeout is NOT in Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Bakeout Rack in sector {sector} in Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Bakeout Rack in sector {sector} NOT in Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Bakeout Rack in main part {mainPart} in Warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Bakeout Rack in main part {mainPart} NOT in Warning");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeVRE::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch (pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch (pCriteria->getSubType())
		{
		case Connected:
			result = "Connected";
			break;
		case BakeoutStopped:
			result = "BakeoutStopped";
			break;
		case BakeoutInProgress:
			result = "BakeoutInProgress";
			break;
		default:
			break;
		}
		break;
	default:
		break;

	}
	return result;
}

int FunctionalTypeVRE::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch (critType)
	{
	case EqpMsgCriteria::MainState:
		if (!strcasecmp(name, "Connected")) {
			result = Connected;
		}
		else if (!strcasecmp(name, "BakeoutStopped")) {
			result = BakeoutStopped;
		}
		else if (!strcasecmp(name, "BakeoutInProgress")) {
			result = BakeoutInProgress;
		}
		break;
	default:
		break;
	}
	if (result)	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}


