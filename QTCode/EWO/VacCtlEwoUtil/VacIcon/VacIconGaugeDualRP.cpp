#include "VacIconGaugeDualRP.h"
#include "EqpVG_U.h"
#include <qpainter.h>

/**
@brief FUNCTION PROTECTED draw symbol inside
@param[in]  painter	Painter object
*/
void VacIconGaugeDualRP::drawSpecific(QPainter &painter) {
	bool blockedOff = false;
	bool showAnalogVal = false;
	double pr = 0.0;
	unsigned rr1 = 0;
	if (pEqp) {
		if (pEqp->inherits("EqpVG_U")) {
			blockedOff = ((EqpVG_U *)pEqp)->isBlockedOff(mode);
			pr = ((EqpVG_U *)pEqp)->getPR(DataEnum::Online);
			rr1 = ((EqpVG_U *)pEqp)->getRR1(DataEnum::Online);
			showAnalogVal = ((EqpVG_U *)pEqp)->isAnalogValOnIcon(mode);
		}
	}
	if (showAnalogVal &&
		(mode != DataEnum::Replay) &&
		(pr > 1E-14) && (pr < 1E4) &&
		(rr1 & RR1_VALID_VGU)) {
		// If Pressure valid and online display value
		QString sPr = QString::number(pr, 'E', 0);
		drawPressureValue(painter, sPr);
	}
	painter.setPen(Qt::black);
	//Pirani
	QPolygon pirani(5);
	pirani.setPoint(0, 4, 4);
	pirani.setPoint(1, 6, 8);
	pirani.setPoint(2, 10, 2);
	pirani.setPoint(3, 14, 8);
	pirani.setPoint(4, 16, 4);
	painter.drawPolyline(pirani);
	//Penning
	QPolygon penning(4);
	penning.setPoint(0, 6, 12);
	penning.setPoint(1, 6, 16);
	penning.setPoint(2, 14, 16);
	penning.setPoint(3, 14, 12);
	painter.drawPolyline(penning);
	painter.drawLine(9, 12, 11, 12);
	if (blockedOff) {
		painter.drawLine(0, 13, 20, 20);
		painter.drawLine(0, 20, 20, 13);
	}
}











	
