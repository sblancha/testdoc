
#uses "vclFastLog.ctl"
#uses "vclMachine.ctl"

// List of session DDPs being processed
dyn_string ddpList;

// New list of session DDPs
dyn_string 	newDdpList;
bool		ddpListChanged;

// Flag indicating that thread deleting old sessions shall be stopped
bool	stopDeleteThread;
bool	deleteThreadStopped;

// Flag indicating if thread stopping session is running
bool stopThreadRunning;

main()
{
	int				n, status;
	string			ddpName;
	time			plannedStart, plannedEnd, now;
	dyn_errClass	err;

 setQueryRDBDirect(false);

	// Connect to DP containing number of fast log sessions
	if( ! dpExists( LHC_VAC_FAST_LOG_DDP_CNT_DP ) )
	{
		dpCreate( LHC_VAC_FAST_LOG_DDP_CNT_DP, LHC_VAC_FAST_LOG_DDP_CNT_DP_TYPE );
		if( ! dpExists( LHC_VAC_FAST_LOG_DDP_CNT_DP ) )
		{
			DebugTN( "FastLogProcess: Failed to create DP <" + LHC_VAC_FAST_LOG_DDP_CNT_DP +
				">, giving up..." );
			return;
		}
	}
	deleteThreadStopped = true;
	dpConnect( "SessionCntCB", LHC_VAC_FAST_LOG_DDP_CNT_DP + ".pointNum:_online.._value" );

	// Wait for events/time - infinite loop
	while( 1 )
	{
		if( ddpListChanged )
		{
			dynClear( ddpList );
			for( n = dynlen( newDdpList ) ; n > 0 ; n-- )
			{
				ddpName = newDdpList[n];
				dynAppend( ddpList, ddpName );
			}
			dynClear( newDdpList );
			ddpListChanged = false;
		}

		// Check if thread responsible for deleting old sessions is stopped,
		// if yes - restart it
		if( deleteThreadStopped )
		{
			deleteThreadStopped = false;
			startThread( "CheckSessionData" );
		}

		// Find and process sessions which shall be started/finished
		for( n = dynlen( ddpList ) ; n > 0 ; n-- )
		{
			if( dynlen( newDdpList ) > 0 ) break;
			dpGet( ddpList[n] + ".status:_online.._value", status,
				ddpList[n] + ".sessionStartTime:_online.._value", plannedStart,
				ddpList[n] + ".sessionEndTime:_online.._value", plannedEnd );
			err = getLastError( );
			if( dynlen( err ) > 0 )
			{
				FatalErrProcess( ddpList[n], true, "Failed to read session parameters" );
				throwError( err );
				continue;
			}
			switch( status )
			{
			case LHC_VAC_FAST_LOG_SESSION_READY:	// Ready to start, check if it is time to start
				now = getCurrentTime( );
				if( now >= plannedStart ) startThread( "StartLogSession", ddpList[n] );
				break;
			case LHC_VAC_FAST_LOG_SESSION_RUNNING:	// Running, check if it is time to stop
				now = getCurrentTime( );
				if( now >= plannedEnd ) startThread( "EndLogSession", ddpList[n] );
				else CheckSessionState( ddpList[n] );
				break;
			case LHC_VAC_FAST_LOG_SESSION_STOP_FAILED:	// Stop failed, try to stop once more
				if( ! stopThreadRunning )
					startThread( "EndLogSession", ddpList[n] );
				break;
			default:	// Other states are not processed by script
				break;
			}
			if( ddpListChanged ) break;
		}
		if( ! ddpListChanged ) delay( 5 );
	}
}
// Callback from DP contaning number of sessions
void SessionCntCB( string dpe, int cnt )
{
	dyn_string	exceptionInfo;

	LhcVacFastLogGetDdps( newDdpList, exceptionInfo );
	stopDeleteThread = true;
	ddpListChanged = true;
}
//******************************************************************************************
// Start fast log session for one DDP
void StartLogSession( string ddpName )
{
	dyn_errClass	err;
	bool				success;
	time				now;

DebugTN( "FastLogProcess: starting session for <" + ddpName + ">" );

	// Mark session as 'starting' to prevent session modifications
	dpSetWait( ddpName + ".status:_original.._value", LHC_VAC_FAST_LOG_SESSION_STARTING,
		ddpName + ".error:_original.._value", "" );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		FatalErrProcess( ddpName, true, "Failed to set status 'starting'" );
		throwError( err );
		return;
	}


	ProcessStartLog( ddpName, success );
DebugTN( "FastLogProcess: start session for <" + ddpName + "> " + ( success ? "OK" : "FAILED" ) );

	dpSetWait( ddpName + ".status:_original.._value",
		( success ? LHC_VAC_FAST_LOG_SESSION_RUNNING : LHC_VAC_FAST_LOG_SESSION_START_FAILED ),
		ddpName + ".health:_original.._value", LHC_VAC_FAST_LOG_SESSION_UNDEF );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		FatalErrProcess( ddpName, true, "Failed to set status after " +
			( success ? "success" : "failure" ) );
		throwError( err );
	}
	if( success )
	{
		now = getCurrentTime( );
		dpSetWait( ddpName + ".archStartTime:_original.._value", now );
		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			FatalErrProcess( ddpName, true, "Failed to set archiving start time" );
			throwError( err );
			return;
		}
	}
}
// Perform session start, return success state
void ProcessStartLog( string ddpName, bool &success )
{
	dyn_errClass	err;
	string				archName, vaDp, adpName, dpe, drvIdent;
	dyn_string		rdp, exceptionInfo;
	int						n, rdpLen, archNum, archSize, interval,
								drvNum, mode, dataType, entriesNum, vaDpNum, error;
	bool					configExists, isActive;
	dyn_anytype		config, newConfig;
	dyn_string		configs;

	success = false;

	// Get fast log parameters
	dpGet( ddpName + ".archName:_original.._value", archName,
		ddpName + ".rdpName:_original.._value", rdp,
		ddpName + ".interval:_original.._value", interval );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		FatalErrProcess( ddpName, true, "Failed to read session parameters" );
		throwError( err );
		return;
	}

	// Validate the most evident log parameters
	if( interval < LHC_VAC_FAST_LOG_MIN_INTERVAL )
	{
		FatalErrProcess( ddpName, true, "Too short interval " + interval + " (min " +
			LHC_VAC_FAST_LOG_MIN_INTERVAL + ")" );
		return;
	}
	if( ( rdpLen = dynlen( rdp ) ) == 0 )
	{
		FatalErrProcess( ddpName, true, "Empty list of real DPs" );
		return;
	}

	// Get necessary archive parameters
	LhcVacFastLogArchiveParam( archName, vaDp, archNum, archSize, entriesNum, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		FatalErrProcess( ddpName, true, "Failed to get archive parameters for <" + archName + ">" );
		return;
	}

	if( rdpLen > archSize )
	{
		FatalErrProcess( ddpName, true, rdpLen + " real DPs for session, but <" + archName +
			"> is only configured for " + archSize );
		return;
	}

	if( sscanf( dpSubStr( vaDp, DPSUB_DP), "_ValueArchive_%d", vaDpNum ) != 1 )
	{
		FatalErrProcess( ddpName, true, "Failed to extract arch. number from <" + vaDp + ">" );
		return;
	}

	// Assign address and archiving interval for ADPs
	success = true;
	configs = makeDynString( "_address", "_distrib" );
	for( n = 1 ; n <= rdpLen ; n++ )
	{
		// Validate ADP name
		adpName = LHC_VAC_FAST_LOG_ADP_TEMPLATE + archNum + "_" + n;
		if( ! dpExists( adpName ) )
		{
			FatalErrProcess( ddpName, true, "ADP <" + adpName + "> does not exist" );
			success = false;
			break;
		}
		// Validate RDP name
		if( ! dpExists( rdp[n] ) )
		{
			FatalErrProcess( ddpName, true, "Real DP <" + rdp[n] + "> does not exist" );
			success = false;
			break;
		}
		
		// Set ADP address equal to RDP address
		dpe = adpName + ".PR";
		dpCopyConfig( rdp[n] + ".PR", dpe, configs, error, vaDpNum );
		if( error != 0 )
		{
			FatalErrProcess( ddpName, true, "Failed to copy address from " + rdp[n] +
				" to " + adpName + ": code " + error );
			success = false;
			break;
		}
		// Set archiving interval for ADP
    dpSetWait( dpe + ":_archive.1._std_time", interval );
		err = getLastError( ); 
    	if( dynlen( err ) > 0 )
    	{ 
			FatalErrProcess( ddpName, true, "Could not set interval " + interval + " for <" + dpe + ">" );
			success = false;
			throwError(err); 
			break;
		}
	}
	// If session start failed - remove address config from DPs for which it was successfully set
	if( ! success )
	{
		for( n = n - 1 ; n > 0 ; n-- )
		{
			adpName = LHC_VAC_FAST_LOG_ADP_TEMPLATE + archNum + "_" + n;
			fwPeriphAddress_delete( adpName + ".PR", exceptionInfo );
		}
	}

	// Stop OPC driver used for this session - it shall be restarted automatically
	// dpSet( "_Managers.Exit:_original.._value", convManIdToInt( DRIVER_MAN, vaDpNum ) );
}
//******************************************************************************************
// Finish fast log session for one DDP
void EndLogSession( string ddpName )
{
	dyn_errClass	err;
	bool				success;
	time				now;

	stopThreadRunning = true;

DebugTN( "FastLogProcess: stopping session for <" + ddpName + ">" );

	// Mark session as 'stopping' to prevent session modifications
	dpSetWait( ddpName + ".status:_original.._value", LHC_VAC_FAST_LOG_SESSION_STOPPING );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		FatalErrProcess( ddpName, false, "Failed to set status 'stopping'" );
		throwError( err );
		stopThreadRunning = false;
		return;
	}


	ProcessEndLog( ddpName, success );
DebugTN( "FastLogProcess: stop session for <" + ddpName + "> " + ( success ? "OK" : "FAILED" ) );

	dpSetWait( ddpName + ".status:_original.._value",
		( success ? LHC_VAC_FAST_LOG_SESSION_FINISHED : LHC_VAC_FAST_LOG_SESSION_STOP_FAILED ) );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		FatalErrProcess( ddpName, false, "Failed to set status after <" +
			( success ? "success" : "failure" ) );
		throwError( err );
	}
	now = getCurrentTime( );
	dpSetWait( ddpName + ".archEndTime:_original.._value", now );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		FatalErrProcess( ddpName, false, "Failed to set archiving end time" );
		throwError( err );
		stopThreadRunning = false;
		return;
	}
	stopThreadRunning = false;
}
// Perform session finish, return success state
void ProcessEndLog( string ddpName, bool &success )
{
	dyn_errClass	err;
	string			archName, vaDp, adpName;
	dyn_string		rdp, exceptionInfo;
	int				archNum, archSize, rdpLen, n, entriesNum;

	success = false;

	// Get fast log parameters
	dpGet( ddpName + ".archName:_original.._value", archName,
		ddpName + ".rdpName:_original.._value", rdp );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		FatalErrProcess( ddpName, false, "Failed to read session parameters" );
		throwError( err );
		return;
	}
	if( ( rdpLen = dynlen( rdp ) ) == 0 )
	{
		FatalErrProcess( ddpName, false, "Empty list of real DPs" );
		return;
	}

	// Get necessary archive parameters
	LhcVacFastLogArchiveParam( archName, vaDp, archNum, archSize, entriesNum, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		FatalErrProcess( ddpName, false, "Failed to get archive parameters for <" + archName + ">" );
		return;
	}

	// Remove address for ADPs
	for( n = 1 ; n <= rdpLen ; n++ )
	{
		// Validate ADP name
		adpName = LHC_VAC_FAST_LOG_ADP_TEMPLATE + archNum + "_" + n;
		if( ! dpExists( adpName ) )
		{
			FatalErrProcess( ddpName, false, "ADP <" + adpName + " does not exist" );
			success = false;
			continue;	// Continue removing addresses from other ADPs
		}
		if( ! dpExists( adpName + ".PR:_address" ) ) continue;
//		DebugTN( n, adpName );
		fwPeriphAddress_delete( adpName + ".PR", exceptionInfo );
		if( dynlen( exceptionInfo ) > 0 )
		{
			FatalErrProcess( ddpName, false, "Failed to delete address for <" + adpName + ">" );
			DebugTN( exceptionInfo );
			success = false;
			return;
		}
	}
	success = true;
}
// Process fatal error which prevented from starting/ending session
void FatalErrProcess( string ddpName, bool isStart, string message )
{
	dpSetWait( ddpName + ".error:_original.._value", ( isStart ? "START: " : "STOP :" ) + message );
	DebugTN( "FastLogProcess: " + ( isStart ? "START: " : "STOP :" ) + message );
}
//================================================================================
// Check what time range is available for sessions in archive data,
// update archStartTime as data for session are pushed out of archive,
// delete session if all data for it are pushed out of archive
void CheckSessionData( )
{
	int				status, archNum, archSize, nEntries, n, listLen, resultLen;
	time			archStart, archEnd, now, minDataTime;
	dyn_string		rdp, exceptionInfo;
	dyn_errClass	err;
	bool			skipSession, canBeDeleted;
	string			archName, vaDp, query, adpName, timeString;
	dyn_dyn_anytype	queryResult;

DebugTN( "CheckSessionData() started" );
	while( 1 )
	{
		listLen = dynlen( ddpList );
		for( n = 1 ; n <= listLen ; n++ )
		{
			delay( 60 );	// In order not to overload server - check every minute
			if( stopDeleteThread )
			{
DebugTN( "CheckSessionData() STOPPED #1" );
				deleteThreadStopped = true;
				stopDeleteThread = false;
				return;
			}
			// Get session parameters
			dpGet( ddpList[n] + ".status:_online.._value", status,
				ddpList[n] + ".archStartTime:_online.._value", archStart,
				ddpList[n] + ".archEndTime:_online.._value", archEnd,
				ddpList[n] + ".archName:_original.._value", archName,
				ddpList[n] + ".rdpName:_online.._value", rdp );
			err = getLastError( );
			if( dynlen( err ) > 0 )
			{
				DebugTN( "CheckSessionData: Failed to read session parameters for <" + ddpList[n] +">" );
				throwError( err );
				continue;
			}
			// Get necessary archive parameters
			LhcVacFastLogArchiveParam( archName, vaDp, archNum, archSize, nEntries, exceptionInfo );
			if( dynlen( exceptionInfo ) > 0 )
			{
				DebugTN( "CheckSessionData(): Failed to get archive parameters for <" + archName + ">" );
				dynClear( exceptionInfo );
				continue;
			}
	
			// Decide if session shall be checked
			if( dynlen( rdp ) == 0 ) continue;
			canBeDeleted = true;
			now = getCurrentTime( );
			minDataTime = now;
			switch( status )
			{
			case LHC_VAC_FAST_LOG_SESSION_RUNNING:
				canBeDeleted = false;
				archEnd = now;
				// NOBREAK
			case LHC_VAC_FAST_LOG_SESSION_FINISHED:
			case LHC_VAC_FAST_LOG_SESSION_HISTORY:
				skipSession = false;
				break;
			default:
				skipSession = true;
				break;
			}
			if( skipSession ) continue;
			// Check time range for ADPs of session
			// It is, probably, enough to check time range for just first ADP in session
			// bacause address has been assigned to 1st ADP before others
			if( stopDeleteThread )
			{
DebugTN( "CheckSessionData() STOPPED #2" );
				stopDeleteThread = false;
				deleteThreadStopped = true;
				return;
			}
			// Prepare query
			timeString = " TIMERANGE(\"";
			timeString += formatTime( "%Y.%m.%d %H:%M:%S.000", archStart );
			timeString += "\",\"";
			timeString += formatTime( "%Y.%m.%d %H:%M:%S.000", now );
			timeString += "\",1,1)";

			adpName = LHC_VAC_FAST_LOG_ADP_TEMPLATE + archNum + "_1";
			query = "SELECT 'MIN(_online.._stime)' FROM '" + adpName + "' " + timeString;

			// Execute query
			dpQuery( query, queryResult );
			err = getLastError();
			if( dynlen( err ) > 0 )
			{
				DebugTN( "Query MIN for <" + adpName + "> failed: " + err , "" );
				return;
			}

			resultLen = dynlen( queryResult );
			if( resultLen != 2 )
			{
				DebugTN( "No MIN found for <" + adpName + ">" );
			}
			else
			{
				minDataTime = queryResult[2][2];
			}
			//DebugTN( "CheckSessionData(): finished check " + ddpList[n], minDataTime, archStart, archEnd );
			if( minDataTime <= archStart ) continue;	// All data are still in archive
			if( canBeDeleted )
			{
				if( minDataTime >= archEnd )	// All data are pushed out
				{
					DebugTN( "CheckSessionData(): delete session " + ddpList[n] );
					LhcVacFastLogDeleteSession( ddpList[n], exceptionInfo );
					dynClear( exceptionInfo );
				}
			}
			else
			{
				DebugTN( "CheckSessionData(): set new archStart for " + ddpList[n] );
				dpSetWait( ddpList[n] + ".archStartTime:_original.._value", minDataTime );
				err = getLastError( );
				if( dynlen( err ) > 0 )
				{
					DebugTN( "CheckSessionData(): Failed to set archiving start time for " + ddpList[n] );
					throwError( err );
				}
			}
		}
		delay( 60 );
	}
}
void CheckSessionState( const string ddpName )
{
	string			archName, vaDp, fileName, buf;
	int				archNum, archSize, nEntries, vaDpNum, i, fileSizeState, fileSize;
	dyn_string		exceptionInfo;
	dyn_errClass	err;

	// Get archive name for session
	dpGet( ddpName + ".archName:_original.._value", archName );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		throwError( err );
		fileSizeState = LHC_VAC_FAST_LOG_SESSION_UNDEF;
	}
	else
	{
		// Get value archive DP
		LhcVacFastLogArchiveParam( archName, vaDp, archNum, archSize, nEntries, exceptionInfo );
		if( dynlen( exceptionInfo ) > 0 )
		{
			fileSizeState = LHC_VAC_FAST_LOG_SESSION_UNDEF;
		}
		else
		{
			// Extract number from value archive DP, build file name
			if( sscanf( dpSubStr( vaDp, DPSUB_DP), "_ValueArchive_%d", vaDpNum ) != 1 )
			{
				fileSizeState = LHC_VAC_FAST_LOG_SESSION_UNDEF;
			}
			else
			{
				sprintf( buf, "VA_%04d/AR_%d.reqlog0", vaDpNum, vaDpNum );
				fileName = PROJ_PATH + "db/wincc_oa/" + buf;
				if( access( fileName, F_OK ) != 0 )
				{
					DebugTN( "CheckSessionState(): no file <" + fileName + "> for " + ddpName );
					fileSizeState = LHC_VAC_FAST_LOG_SESSION_UNDEF;
				}
				else
				{
					if( ( fileSize = getFileSize( fileName ) ) < 0 )
					{
						DebugTN( "CheckSessionState(): failed to get size of <" + fileName + "> for " + ddpName );
						fileSizeState = LHC_VAC_FAST_LOG_SESSION_UNDEF;
					}
					else
					{
						// Decide for state, write to DDP
						if( fileSize < 1.E6 ) fileSizeState = LHC_VAC_FAST_LOG_SESSION_OK;
						else if( fileSize < 5.E6 ) fileSizeState = LHC_VAC_FAST_LOG_SESSION_WARNING;
						else if( fileSize < 14.E6 ) fileSizeState = LHC_VAC_FAST_LOG_SESSION_BAD;
						else fileSizeState = LHC_VAC_FAST_LOG_SESSION_FAULT;
					}
				}
			}
		}
	}

	dpSetWait( ddpName + ".health:_original.._value", fileSizeState );
//DebugTN( fileName, fileSize + " bytes", fileSizeState );
}

