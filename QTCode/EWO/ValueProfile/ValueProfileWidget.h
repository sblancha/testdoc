#ifndef VALUEPROFILEWIDGET_H
#define VALUEPROFILEWIDGET_H

#include <qwidget.h>
#include <qdatetime.h>
#include <qlist.h>
#include <qvariant.h>

#include "PlatformDef.h"

class QLabel;
class QSpinBox;
class QTimer;

class ValueProfileWidget : public QWidget
{
	Q_OBJECT

	////////////////////////////////////////////////////////////////////////////
	///////////////////// Properties exposed to PVSS panel /////////////////////
	////////////////////////////////////////////////////////////////////////////
	Q_PROPERTY(QColor axisColor READ getAxisColor WRITE setAxisColor);
	Q_PROPERTY(QColor valuesColor READ getValuesColor WRITE setValuesColor);
	Q_PROPERTY(QColor barHighlightColor READ getBarHighlightColor WRITE setBarHighlightColor);
	Q_PROPERTY(int marqueeWidth READ getMarqueeWidth WRITE setMarqueeWidth);
	Q_PROPERTY(QColor marqueeColor READ getMarqueeColor WRITE setMarqueeColor);

public:
	ValueProfileWidget(QWidget *parent = NULL, Qt::WindowFlags f = 0);

	~ValueProfileWidget();

	int setFloatValues(const QVariant &valueList, QString &errMsg);
	QStringList &getValuesInMarquee(void);

	// Accesss for properties
	inline const QColor &getAxisColor(void) const { return axisColor; }
	void setAxisColor(const QColor &color);
	inline const QColor &getValuesColor(void) const { return valuesColor; }
	void setValuesColor(const QColor &color);
	inline const QColor &getBarHighlightColor(void) const { return barHighlightColor; }
	void setBarHighlightColor(const QColor &color);
	inline int getMarqueeWidth(void) const { return marqueeWidth; }
	void setMarqueeWidth(int newWidth);
	inline const QColor &getMarqueeColor(void) const { return marqueeColor; }
	void setMarqueeColor(const QColor &color);

signals:
	void barHighlight(int index, float value);
	void marqueeMove(int start, int end);

protected:

	// List of floating point values to be shown
	QList<float>	floatList;

	// Color for axis
	QColor		axisColor;

	// Color for values
	QColor		valuesColor;

	// Color for highlighting bar under mouse
	QColor		barHighlightColor;

	// Color for marquee drawing
	QColor		marqueeColor;

	QStringList valuesInMarquee;

	// Type of values shown
	enum
	{
		TypeNone = 0,
		TypeFloat = 1
	};
	int valueType;

	// Minumum value for all float values
	float min;

	// Maximum value for all float values
	float max;

	// Horizontal offset for first bar
	int horOffset;

	// Coefficient for converting bar number to horizontal coordinate
	double	horCoeff;

	// Width of single bar
	int		barWidth;

	// Zero level position
	int		zeroLevel;

	// Coefficient for converting bar value to vertical position
	double	vertCoeff;

	// X-coordinate of mouse pointer
	int		mouseX;

	// Index of highlighted bar
	int		barIndex;

	// Rectangle of highlighted bar
	QRect	highlightedBar;

	// Flag indicating if highlighted bar is drawn
	bool	highlightedBarDrawn;

	// Flag indicating if new data have arrived
	bool	newDataArrived;

	// Width of marquee rectangle (pixels)
	int		marqueeWidth;

	// Marquee rectangle
	QRect	marquee;

	// Flag indicating if we are dragging marquee rectangle
	bool	draggingMarquee;

	// Last known start bar of marquee
	int		lastMarqueeStart;

	// Last known end bar of marquee
	int		lastMarqueeEnd;

	void resizeEvent(QResizeEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void paintEvent(QPaintEvent *event);

	void makeImage(void);
	void draw(QPainter &painter);

	void drawFloat(QPainter &painter);

	void drawHighlightedBar(void);
	void drawMarquee(void);
	void moveMarquee(int newX);
	void adjustMarqueeRect(QRect &rect);
	void signalMarquueMove(void);

	int barAtPosition(int x);
};

#endif	// VALUEPROFILEWIDGET_H
