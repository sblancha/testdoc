// BeamLine.cpp: implementation of the BeamLine class.
//
//////////////////////////////////////////////////////////////////////

#include "PlatformDef.h"
#include "FunctionalType.h"
#include "EqpMsgCriteria.h"

#include "BeamLine.h"
#include "DataPool.h"
#include "Eqp.h"
#include "EqpType.h"
#include "Sector.h"
#include "DebugCtl.h"

#include <math.h>

#include <QString>
#include <QList>
#include <QStringList>

#include <QColor>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

// For debugging
static int nCalls = 0, nBefore = 0, nAfter = 0;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BeamLine::BeamLine(char *name, bool isCircle)
{
	pParts = new QList<BeamLinePart *>();
	this->name = DataPool::getInstance().addString(name);
	this->circle = isCircle;
	dataPart = NULL;
	startType = endType = BeamLineConnType::None;
	startVacType = endVacType = VacType::Beam;
	startLine = endLine = NULL;
	notUsed = hidden = turning = resizing = containSectorBorder = virtualLine = false;
	turnAngle = 0.0;
	resizeCoef = 1.0;
	minX = minY = maxX = maxY = 0.0;
	autoTag = false;
	tagStartManual = tagEndManual = "";
	/*tagStart = tagEnd = "";
	childStart = childEnd = false;*/
	verticalPos = 0;
	allowedVacType = VacType::None;
	refType = RefNone;
	testEqp = false;
	hideOnMainView = false;
}

BeamLine::~BeamLine()
{
	while(!pParts->isEmpty())
	{
		delete pParts->takeFirst();
	}
	delete pParts;
}

/*
**
**	FUNCTION
**		Turn equipment in this line if necessary
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BeamLine::turn(void)
{
	if(turnAngle == 0.0)
	{
		return;
	}
	float	x, y, delta;
	if(refForTransform(x, y))
	{
		delta = (float)(turnAngle * M_PI / 180.0);
		turn(x, y, delta);
	}
}

/*
**
**	FUNCTION
**		Resize this line if necessary
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BeamLine::resize(void)
{
	if(resizeCoef == 1.0)
	{
		return;
	}
	float	x, y;
	if(refForTransform(x, y))
	{
		resize(x, y, resizeCoef);
	}
}

/*
**	FUNCTION
**		Build locations for start(s) and end(s) of every beam vacuum
**		sector on 3 lines: RED, BLUE, CROSS. Only sectors on main LHC
**		ring are processed, sectors on injection/dump lines are processed
**		in another way.
**
**	EXTERNAL
**
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None.
**
**	CAUTIONS
**		The method shall only be called for LHC machine and ring line.
*/
#ifndef PVSS_SERVER_VERSION
void BeamLine::buildBeamSectorBorders(void)
{
	for(int idx = 0 ; idx < pParts->count() ; idx++)
	{
		pParts->at(idx)->buildBeamSectorBorders();
	}
}
#endif

/*
**	FUNCTION
**		Find missing sector border for sector within given LHC ring region when
**		sector border is known. Sector border can be either sector border equipment
**		(if found), or start/end of region if equipment at missing sector border is
**		not found.
**
**	EXTERNAL
**
**
**	PARAMETERS
**		pStartEqp			- Pointer to equipment which is one (known) border of sector
**		pRegion				- Pointer to LHC ring region to which search shall be limited
**		vacType				- Required vacuum type for border equipment
**		direction			- direction for search (-1=bakward, 1=forward)
**		ppOtherBorderEqp	- Address of pointer to equipment at opposite sector border,
**								if such equipment will be found - pointer to it will be written here
**		borderCoord			- Found coordinate of opposite sector border will be written here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void BeamLine::findNeighbourSectorBorder(Eqp *pStartEqp, LhcRegion *pRegion, int vacType,
	int direction, Eqp **ppOtherBorderEqp, float &borderCoord)
{
	BeamLinePart	*pPart = NULL;
	int				eqpIdx, partIdx;

	// Find start equipment in line
	for(partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		BeamLinePart *pEqpPart = pParts->at(partIdx);
		if((eqpIdx = pEqpPart->getEqpIdx(pStartEqp)))
		{
			pPart = pEqpPart;
			break;
		}
	}
	// Try to find neighbour sector border if start equipment was found
	if(pPart)
	{
		neighbourEqp(&pPart, eqpIdx, direction);
		Eqp	*pEqp = pPart->getEqpPtrs()[eqpIdx];
		while((pEqp = pPart->getEqpPtrs()[eqpIdx]) != pStartEqp)
		{
			// Stop if we are out of region coordinates range
			if(!pRegion->isCoordInside(pEqp->getStart()))
			{
				break;
			}
			// Is equipment sector border on required vacuum type?
			if(pEqp->getVacType() == vacType)
			{
				if(pEqp->isSectorBorder())
				{
					// Neighbour sector border found!!!
					*ppOtherBorderEqp = pEqp;
					borderCoord = pEqp->getStart();
					return;
				}
			}
			neighbourEqp(&pPart, eqpIdx, direction);
		}
	}
	// Failed to find neighbour sector border within the same region, use start or end of region
	// depending on direction
	borderCoord = direction > 0 ? pRegion->getEnd() : pRegion->getStart();
}
#endif

/*
**	FUNCTION
**		Find indices (pointer to line part and index of equipment) for equipment
**		neighbour to given equipment on circular line.
**
**	EXTERNAL
**
**
**	PARAMETERS
**		ppPart		- Address of pointer to beam line part to start from,
**						 value can be update by this method
**		eqpIdx		- Index of equipment in line part to start from, value can be updated
**						by this method
**		direction	- Direction for neighbour search (-1=backward, 1=forward)
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall only be called for circular lines
**
** FUNCTION DEFINITION
*/
#ifndef PVSS_SERVER_VERSION
void BeamLine::neighbourEqp(BeamLinePart **ppPart, int &eqpIdx, int direction)
{
	int idx;
	eqpIdx += direction;
	if(eqpIdx < 0)
	{
		idx = pParts->indexOf(*ppPart) - 1;
		if((0 <= idx) && (idx < pParts->count()))
		{
			*ppPart = pParts->at(idx);
		}
		else
		{
			*ppPart = pParts->at(pParts->count() - 1);
		}
		eqpIdx = (*ppPart)->getNEqpPtrs() - 1;
	}
	else if(eqpIdx >= (*ppPart)->getNEqpPtrs())
	{
		idx = pParts->indexOf(*ppPart) + 1;
		if((0 <= idx) && (idx < pParts->count()))
		{
			*ppPart = pParts->at(idx);
		}
		else
		{
			*ppPart = pParts->at(0);
		}
		eqpIdx = 0;
	}
}
#endif

/*
**
** FUNCTION
**		Process tokens of single line read from input file
**
** PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error messages shall be written
**					in case of errors.
**
** RETURNS
**		1	- tokens have been processed successfully, more lines are
**				expected for this equipment.
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
** CAUTIONS
**		None
*/
int BeamLine::processLineTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	const QString & /* longValue */, QStringList &errList)
{
	// Data part name for this beam line
	if(!strcasecmp(tokens[0], "DATA_PART"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "BeamLine::ProcessLineTokens");
		dataPart = DataPool::getInstance().addString(tokens[1]);
		if(DataPool::getInstance().getDataPart())
		{
			if(strcmp(dataPart, DataPool::getInstance().getDataPart()))
			{
				notUsed = hidden = virtualLine = true;
			}
		}
		return 1;
	}
	// Start of BEAM_LINE in another beam line
	else if(!strcasecmp(tokens[0], "START"))
	{
		CHECK_MIN_TOKEN_COUNT(nTokens, 4, lineNo, "BeamLine::ProcessLineTokens");
		if(startLine)
		{
			errList.append(QString("Line %1: multiple STARTs for line").arg(lineNo));
			return -1;
		}
		startLine = DataPool::getInstance().addString(tokens[1]);
		if(sscanf(tokens[2], "%f", &startCoord) != 1)
		{
			errList.append(QString("Line %1: bad START coordinate format <%2>").arg(lineNo).arg(tokens[2]));
			return -1;
		}
		if((startType = BeamLineConnType::decode(tokens[3], lineNo, errList)) == BeamLineConnType::None)
		{
			return -1;
		}
		if(nTokens > 4)	// Vacuum type of start line
		{
			if((startVacType = VacType::decodeConn(tokens[4])) == VacType::None)
			{
				errList.append(QString("Line %1: unknown vacuum type <%2>").arg(lineNo).arg(tokens[4]));
				return -1;
			}
		}
		return 1;
	}
	// Finish of BEAM_LINE in another beam line
	else if(!strcasecmp( tokens[0], "FINISH"))
	{
		CHECK_MIN_TOKEN_COUNT(nTokens, 4, lineNo, "BeamLine::ProcessLineTokens");
		if(endLine)
		{
			errList.append(QString("Line %1: multiple FINISHs for line").arg(lineNo));
			return -1;
		}
		endLine = DataPool::getInstance().addString(tokens[1]);
		if(sscanf(tokens[2], "%f", &endCoord) != 1)
		{
			errList.append(QString("Line %1: bad FINISH coordinate format <%2>").arg(lineNo).arg(tokens[2]));
			return -1;
		}
		if((endType = BeamLineConnType::decode(tokens[3], lineNo, errList)) == BeamLineConnType::None)
		{
			return -1;
		}
		if(nTokens > 4)	// Vacuum type of end line
		{
			if((endVacType = VacType::decodeConn(tokens[4])) == VacType::None)
			{
				errList.append(QString("Line %1: unknown vacuum type <%2>").arg(lineNo).arg(tokens[4]));
				return -1;
			}
		}
		return 1;
	}
 	// Elements of BEAM_LINE
	else if(!strcasecmp(tokens[0], "ELEMENTS"))
	{
		CHECK_MIN_TOKEN_COUNT(nTokens, 4, lineNo, "BeamLine::ProcessLineTokens");
		BeamLinePart *pPart = BeamLinePart::create(
				DataPool::getInstance().addString(tokens[1]),
				tokens[2], tokens[3], lineNo, errList);
		if(!pPart)
		{
			return -1;
		}
		pParts->append(pPart);
		if(nTokens == 5)
		{
			if(!strcasecmp(tokens[4], "R"))
			{
				pPart->setReverseBeamDirection(true);
			}
		}
		return 1;
	}
	// The angle to turn the whole line
	else if(!strcasecmp(tokens[0], "turn_angle"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "BeamLine::ProcessLineTokens");
 		if(sscanf(tokens[1], "%f", &turnAngle) != 1)
		{
			errList.append(QString("Line %1: bad turn angle format <%2>").arg(lineNo).arg(tokens[1]));
			return -1;
		}
		return 1;
 	}
	// Resize coefficient to be applied to beam line
	else if(!strcasecmp(tokens[0], "resize"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "BeamLine::ProcessLineTokens");
 		if(sscanf(tokens[1], "%f", &resizeCoef) != 1)
		{
			errList.append(QString("Line %1: bad resize format <%2>").arg(lineNo).arg(tokens[1]));
			return -1;
		}
 		return 1;
	}
	// Manually forced vertical position
	else if(!strcasecmp(tokens[0], "vertical_pos"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "CBeamLine::ProcessLineTokens");
 		if(sscanf(tokens[1], "%d", &verticalPos) != 1)
		{
			errList.append(QString("Line %1: bad VERTICAL_POS format <%2>").arg(lineNo).arg(tokens[1]));
			return -1;
		}
		if(verticalPos < 1)
		{
			errList.append(QString("Line %1: bad VERTICAL_POS value %2, must be >= 1").arg(lineNo).arg(verticalPos));
			return -1;
		}
 		return 1;
	}
	// The only allowed vacuum type of active equipment
	else if(!strcasecmp(tokens[0], "vac_type"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, "BeamLine::ProcessLineTokens");
 		if((allowedVacType = VacType::decodeConn(tokens[1])) == VacType::None)
		{
			errList.append(QString("Line %1: unknown vacuum type <%2>").arg(lineNo).arg(tokens[1]));
			return -1;
		}
 		return 1;
	}
	// Flag indicating if this line shall be hidden on main view (no parameters)
	else if(!strcasecmp(tokens[0], "hide_on_main_view"))
	{
		hideOnMainView = true;
 		return 1;
	}
	// [VACCO-527] Flag to define if start and end tags should be automatically added to the synoptics when possible
	else if(!strcasecmp(tokens[0], "automatic_tag"))
	{
		if (!strcasecmp(tokens[1], "on"))
		{
			autoTag = true;
			return 1;
		}
		else if(!strcasecmp(tokens[1], "off"))
		{
			autoTag = false;
			return 1;
		}
		return -1;
 		
	}
	// [VACCO-527] Parsing of start and end tags
	else if(!strcasecmp(tokens[0], "start_tag"))
	{
		tagStartManual = tokens[1];
 		return 1;
	}
	else if(!strcasecmp(tokens[0], "end_tag"))
	{
		tagEndManual = tokens[1];
 		return 1;
	}
	// [VACCO-527] End of beam line
	else if(!strcasecmp(tokens[0], "end_beam_line"))
	{
		if(pParts->isEmpty())
		{
			errList.append(QString("Line %1: no beam line parts in <%2>").arg(lineNo).arg(name));
			return -1;
		}

		// Decide for type of reference point. L.Kopylov 21.07.2011
		if(circle)	// Circle line is rotated around it's center
		{
			refType = RefCenter;
		}
		else if(startLine)
		{
			refType = RefStart;
		}
		else if(endLine)
		{
			refType = RefEnd;
		}

		return 0;
	}
	PARSING_ERR2("Line %d: unexpected <%s>", lineNo, tokens[0]);
}

/*
**
** FUNCTION
**		Check if equipment shall be added to one of parts of this beam line,
**		if yes - add equipment.
**
** PARAMETERS
**		pEqp		- Pointer to equipment to be added
**		pureSurvey	- true if equipment shall be added to 'pure survey' line,
**						i.e. line without ELEMENTS
**
** RETURNS
**		true if equipment has been added to line;
**		false otherwise
**
** CAUTIONS
**		None
*/
bool BeamLine::addEqp(Eqp *pEqp, bool pureSurvey)
{
	bool	result = false;
	int		eqpType = pEqp->getType();
	if(eqpType == EqpType::Passive)
	{
		result = addPassiveEqp(pEqp, pureSurvey);
	}
	else if(eqpType == EqpType::VacDevice)
	{
		result = addActiveEqp(pEqp);
	}
	return result;
}

/*
**
** FUNCTION
**		Calculate (X,Y) coordinates for all equipment in beam line,
**		method is used for LHC where (X,Y) do not come from DB.
**
** PARAMETERS
**		None
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void BeamLine::makeXY(void)
{
	Eqp	*pEqp;
	double	radius, angle, dcumEnd = 0.0;
	int partIdx;

	if(!circle)
	{
		return;
	}

	BeamLinePart *pPart;
	for(partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		pPart = pParts->at(partIdx);
		if(pPart->getEnd() > dcumEnd)
		{
			dcumEnd = pPart->getEnd();
		}
	}
	if(dcumEnd == 0.0)
	{
		return;
	}
	radius = dcumEnd / 2. / M_PI;
	for(partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		pPart = pParts->at(partIdx);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			pEqp = eqpList.at(eqpIdx);
			angle = pEqp->getStart() / dcumEnd * 2. * M_PI - M_PI / 2.;
			pEqp->setStartX((float)(radius * cos(angle)));
			pEqp->setStartY((float)(radius * sin(angle)));
			if(pEqp->getLength() > 0.0)
			{
				angle = (pEqp->getStart() + pEqp->getLength()) / dcumEnd * 2 * M_PI - M_PI / 2.;
				pEqp->setEndX((float)(radius * cos(angle)));
				pEqp->setEndY((float)(radius * sin(angle)));
			}
			else
			{
				pEqp->setEndX(pEqp->getStartX());
				pEqp->setEndY(pEqp->getStartY());
			}
		}
	}
}
#endif

/*
**
** FUNCTION
**		Calculate all beam line parameters after all passive equipment
**		have been added to line.
**
** PARAMETERS
**		errList	- Variable where error message shall be written
**					in case of errors.
**
** RETURNS
**		true in case of success;
**		false otherwise
**
** CAUTIONS
**		None
*/
bool BeamLine::finish(QStringList &errList)
{
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("BeamLine::finish(%s) start\n", name);
#else
		printf("BeamLine::finish(%s) start\n", name);
		fflush(stdout);
#endif
	}
	// Verify that at least some equipment exists for beam line
	BeamLinePart	*pPart = NULL;
	int partIdx;
	for(partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		BeamLinePart *pCheckPart = pParts->at(partIdx);
		const QList<Eqp *> &eqpList = pCheckPart->getEqpList();
		if(!eqpList.isEmpty())
		{
			pPart = pCheckPart;
			break;
		}
	}
	if(!pPart)
	{
		if(DebugCtl::isData())
		{
#ifdef Q_OS_WIN
			qDebug("BeamLine::finish(%s) all parts empty\n", name);
#else
			printf("BeamLine::finish(%s) all parts empty\n", name);
			fflush(stdout);
#endif
		}
		errList.append(QString("Equipment for line <%1> is not found in raw data").arg(name));
		return false;
	}

	// Assign coordinates to start of line
	float	x, y;
	Eqp	*pEqp, *pPrevEqp = NULL;
	if(startLine)
	{
		if(DebugCtl::isData())
		{
#ifdef Q_OS_WIN
			qDebug("BeamLine::finish(%s) startLine %s\n", name, startLine);
#else
			printf("BeamLine::finish(%s) startLine %s\n", name, startLine);
			fflush(stdout);
#endif
		}
		if(!DataPool::getInstance().coordInLine(startLine, name, startVacType, startCoord, true, x, y))
		{
			if(DebugCtl::isData())
			{
#ifdef Q_OS_WIN
				qDebug("BeamLine::finish(%s) startLine %s NOT FOUND\n", name, startLine);
#else
				printf("BeamLine::finish(%s) startLine %s NOT FOUND\n", name, startLine);
				fflush(stdout);
#endif
			}
			errList.append(QString("Start line <%1> of <%2> is not found").arg(startLine).arg(name));
			return false;
		}
		pEqp = new Eqp(EqpType::LineStart);
		pEqp->setName(startLine);
		bool partIsReverse = false;
		// Add before the very first device
		for(partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
		{
			pPart = pParts->at(partIdx);
			QList<Eqp *> &eqpList = pPart->getEqpList();
			if(!eqpList.isEmpty())
			{
				partIsReverse = pPart->isReverse();
				if(partIsReverse)
				{
					// First or last eqp - only use passive
					pPrevEqp = eqpList.last();
					for(int idx = eqpList.count() - 1 ; idx >= 0 ; idx--)
					{
						Eqp *pEqp = eqpList.at(idx);
						if(pEqp->getType() == EqpType::Passive)
						{
							pPrevEqp = pEqp;
							break;
						}
					}
					eqpList.append(pEqp);
				}
				else
				{
					// First or last eqp - only use passive
					pPrevEqp = eqpList.first();
					for(int idx = 0 ; idx < eqpList.count() ; idx++)
					{
						Eqp *pEqp = eqpList.at(idx);
						if(pEqp->getType() == EqpType::Passive)
						{
							pPrevEqp = pEqp;
							break;
						}
					}
					eqpList.prepend(pEqp);
				}
				break;
			}
		}
		pEqp->setStartX(x);
		pEqp->setEndX(x);
		pEqp->setStartY(y);
		pEqp->setEndY(y);
		if(!DataPool::getInstance().isLhcFormat())	// Calculate real distance
		{
			if(partIsReverse)
			{
				pEqp->setStart(pPrevEqp->getStart() + (float)sqrt(
					(x - pPrevEqp->getStartX()) * (x - pPrevEqp->getStartX()) +
					(y - pPrevEqp->getStartY()) * (y - pPrevEqp->getStartY())));
			}
			else
			{
				pEqp->setStart(pPrevEqp->getStart() - (float)sqrt(
					(x - pPrevEqp->getStartX()) * (x - pPrevEqp->getStartX()) +
					(y - pPrevEqp->getStartY()) * (y - pPrevEqp->getStartY())));
			}
		}
		else	// LHC drawing is so abstract that real distance does not matter
		{
			if(partIsReverse)
			{
				pEqp->setStart((float)(pPrevEqp->getStart() + 0.001));
			}
			else
			{
				pEqp->setStart((float)(pPrevEqp->getStart() - 0.001));
			}
		}
		if(DebugCtl::isData())
		{
#ifdef Q_OS_WIN
			qDebug("BeamLine::finish(%s) startLine %s done\n", name, startLine);
#else
			printf("BeamLine::finish(%s) startLine %s done\n", name, startLine);
			fflush(stdout);
#endif
		}
	}

	// Assign coordinates to end of line
	if(endLine)
	{
		if(DebugCtl::isData())
		{
#ifdef Q_OS_WIN
			qDebug("BeamLine::finish(%s) endLine %s\n", name, endLine);
#else
			printf("BeamLine::finish(%s) endLine %s\n", name, endLine);
			fflush(stdout);
#endif
		}
		if(!DataPool::getInstance().coordInLine(endLine, name, endVacType, endCoord, false, x, y))
		{
			if(DebugCtl::isData())
			{
#ifdef Q_OS_WIN
				qDebug("BeamLine::finish(%s) endLine %s NOT FOUND\n", name, endLine);
#else
				printf("BeamLine::finish(%s) endLine %s NOT FOUND\n", name, endLine);
				fflush(stdout);
#endif
			}
			errList.append(QString("End line <%1> of <%2> is not found").arg(endLine).arg(name));
			return false;
		}
		pEqp = new Eqp(EqpType::LineEnd);
		pEqp->setName(endLine);
		// Add new equipment after the very last, i.e. at the end of list
		bool partIsReverse = false;
		for(partIdx = pParts->count() - 1; partIdx >= 0 ; partIdx--)
		{
			pPart = pParts->at(partIdx);
			QList<Eqp *> &eqpList = pPart->getEqpList();
			if(!eqpList.isEmpty())
			{
				partIsReverse = pPart->isReverse();
				if(partIsReverse)
				{
					// First or last eqp - only use passive
					pPrevEqp = eqpList.first();
					for(int idx = 0 ; idx < eqpList.count() ; idx++)
					{
						Eqp *pEqp = eqpList.at(idx);
						if(pEqp->getType() == EqpType::Passive)
						{
							pPrevEqp = pEqp;
							break;
						}
					}
					eqpList.prepend(pEqp);
				}
				else
				{
					// First or last eqp - only use passive
					pPrevEqp = eqpList.last();
					for(int idx = eqpList.count() - 1 ; idx >= 0 ; idx--)
					{
						Eqp *pEqp = eqpList.at(idx);
						if(pEqp->getType() == EqpType::Passive)
						{
							pPrevEqp = pEqp;
							break;
						}
					}
					eqpList.append(pEqp);
				}
				break;
			}
		}
		pEqp->setStartX(x);
		pEqp->setEndX(x);
		pEqp->setStartY(y);
		pEqp->setEndY(y);
		if(!DataPool::getInstance().isLhcFormat())	// Calculate real distance
		{
			if(partIsReverse)
			{
				pEqp->setStart(pPrevEqp->getStart() - (float)sqrt(
					(x - pPrevEqp->getStartX()) * (x - pPrevEqp->getStartX()) +
					(y - pPrevEqp->getStartY()) * (y - pPrevEqp->getStartY())));
			}
			else
			{
				pEqp->setStart(pPrevEqp->getStart() + (float)sqrt(
					(x - pPrevEqp->getStartX()) * (x - pPrevEqp->getStartX()) +
					(y - pPrevEqp->getStartY()) * (y - pPrevEqp->getStartY())));
			}
		}
		else	// LHC drawing is so abstract that real distance does not matter
		{
			if(partIsReverse)
			{
				pEqp->setStart((float)(pPrevEqp->getStart() + pPrevEqp->getLength() - 0.001));
			}
			else
			{
				pEqp->setStart((float)(pPrevEqp->getStart() + pPrevEqp->getLength() + 0.001));
			}
		}
		if(DebugCtl::isData())
		{
#ifdef Q_OS_WIN
			qDebug("BeamLine::finish(%s) endLine %s done\n", name, endLine);
#else
			printf("BeamLine::finish(%s) endLine %s done\n", name, endLine);
			fflush(stdout);
#endif
		}
	}

	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("BeamLine::finish(%s) finish\n", name);
#else
		printf("BeamLine::finish(%s) finish\n", name);
		fflush(stdout);
#endif
	}
	return true;
}

/*
**
** FUNCTION
**    Find (X,Y) coordinates of point to be used as a reference for beam line
**    coordinates transformation (resize, turn). The reference point is either
**    start of line or end of line.
**
** PARAMETERS
**    refX   - Resulting X coordinate of reference point is returned here.
**    refY   - Resulting Y coordinate of reference point is returned here.
**
** RETURNS
**    true  - In case of success
**    false - In case of error (beam line has no start or end).
**
** CAUTIONS
**    None
*/
bool BeamLine::refForTransform(float &refX, float &refY)
{
	Eqp	*pEqp = NULL;
	int partIdx, eqpIdx;

	if(circle)	// Circle line is rotated around it's center
	{
		float	xMin = 0.0, yMin = 0.0, xMax = 0.0, yMax = 0.0;
		bool	isFirst = true;
		for(partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
		{
			BeamLinePart *pPart = pParts->at(partIdx);
			QList<Eqp *> &eqpList = pPart->getEqpList();
			for(eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
			{
				pEqp = eqpList.at(eqpIdx);
				if(isFirst)
				{
					isFirst = false;
					xMin = xMax = pEqp->getStartX();
					yMin = yMax = pEqp->getStartY();
				}
				else
				{
					if(pEqp->getStartX() < xMin)
					{
						xMin = pEqp->getStartX();
					}
					else if(pEqp->getStartX() > xMax)
					{
						xMax = pEqp->getStartX();
					}
					if(pEqp->getStartY() < yMin)
					{
						yMin = pEqp->getStartY();
					}
					else if(pEqp->getStartY() > yMax)
					{
						yMax = pEqp->getStartY();
					}
				}
				if(pEqp->getEndX() < xMin)
				{
					xMin = pEqp->getEndX();
				}
				else if(pEqp->getEndX() > xMax)
				{
					xMax = pEqp->getEndX();
				}
				if(pEqp->getEndY() < yMin)
				{
					yMin = pEqp->getEndY();
				}
				else if(pEqp->getEndY() > yMax)
				{
					yMax = pEqp->getEndY();
				}
			}
		}
		refX = (float)((xMin + xMax) * 0.5);
		refY = (float)((yMin + yMax) * 0.5);
	}
	else	// Non-circle line is rotated around it's start or end, start is preferred
	{
		bool	useStart = true;
		if(startLine)
		{
			useStart = true;
		}
		else if(endLine)
		{
			useStart = false;
		}
		/* Use start even though there is no start or end - for FT16. LIK 24.11.2008
		else
		{
			return false;
		}
		*/

		if(useStart)
		{
			for(partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
			{
				BeamLinePart *pPart = pParts->at(partIdx);
				QList<Eqp *> &eqpList = pPart->getEqpList();
				if(!eqpList.isEmpty())
				{
					pEqp = eqpList.at(0);
					break;
				}
			}
			refX = pEqp->getStartX();
			refY = pEqp->getStartY();
		}
		else
		{
			for(partIdx = pParts->count() - 1 ; partIdx >= 0 ; partIdx--)
			{
				BeamLinePart *pPart = pParts->at(partIdx);
				QList<Eqp *> &eqpList = pPart->getEqpList();
				if(!eqpList.isEmpty())
				{
					pEqp = eqpList.last();
					break;
				}
			}
			refX = pEqp->getEndX();
			refY = pEqp->getEndY();
		}
	}
	return true;
}

/*
**
** FUNCTION
**    Turn beam line around reference point,
**    move child lines (if any) to keep line connections.
**
** PARAMETERS
**    centerX  - X coordinate of reference point for turning.
**    centerY  - Y coordinate of reference point for turning.
**    delta    - Angle for turn (-PI ... PI).
**
** RETURNS
**    None.
**
** CAUTIONS
**    None
*/
void BeamLine::turn(float centerX, float centerY, float delta)
{
	if(turning)
	{
		return;	// To prevent infifnite call stack
	}

	int partIdx, eqpIdx;
	BeamLinePart *pPart;
	Eqp	*pEqp;
	for(partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		pPart = pParts->at(partIdx);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			pEqp = eqpList.at(eqpIdx);
			turnPoint(centerX, centerY, delta, pEqp);
		}
	}

	// Turn all child lines
	turning = true;
	DataPool &pool = DataPool::getInstance();
	for(partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		pPart = pParts->at(partIdx);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			pEqp = eqpList.at(eqpIdx);
			if((pEqp->getType() != EqpType::OtherLineStart) &&
				(pEqp->getType() != EqpType::OtherLineEnd))
			{
				continue;
			}
			BeamLine *pChild = pool.getLine(pEqp->getName());
			pChild->turn(centerX, centerY, delta);
		}
	}
	turning = false;
}

/*
**
** FUNCTION
**    Resize beam line - make it longer or shorter,
**    move child lines (if any) to keep line connections.
**
** PARAMETERS
**    centerX  - X coordinate of reference point for resizing.
**    centerY  - Y coordinate of reference point for resizing.
**    delta    - Factor for resizing (<1 to make line shorter, >1 to make line longer).
**
** RETURNS
**    None.
**
** CAUTIONS
**    None
*/
void BeamLine::resize(float centerX, float centerY, float delta)
{
	float	xCoord, yCoord;

	for(int partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		BeamLinePart *pPart = pParts->at(partIdx);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			Eqp *pEqp = eqpList.at(eqpIdx);
			xCoord = pEqp->getStartX();
			yCoord = pEqp->getStartY();
			resizePoint(centerX, centerY, delta, xCoord, yCoord);
			if((pEqp->getType() == EqpType::OtherLineStart) ||
				(pEqp->getType() == EqpType::OtherLineEnd))
			{
				moveChildLines(pEqp->getName(), xCoord - pEqp->getStartX(), yCoord - pEqp->getStartY());
			}
			else if((pEqp->getType() == EqpType::LineEnd) && (refType == RefStart))
			{
				moveChildLines(endLine, xCoord - pEqp->getStartX(), yCoord - pEqp->getStartY());
			}
			pEqp->setStartX(xCoord);
			pEqp->setStartY(yCoord);
			xCoord = pEqp->getEndX();
			yCoord = pEqp->getEndY();
			resizePoint(centerX, centerY, delta, xCoord, yCoord);
			pEqp->setEndX(xCoord);
			pEqp->setEndY(yCoord);
		}
	}
}

/*
**
** FUNCTION
**    Calculate (X,Y) limits for all equipment in beam line.
**
** PARAMETERS
**    None
**
** RETURNS
**    None.
**
** CAUTIONS
**    None
*/
void BeamLine::findGeoLimits(void)
{
	bool	isFirst = true;

	for(int partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		BeamLinePart *pPart = pParts->at(partIdx);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		if(eqpList.isEmpty())
		{
			continue;
		}
		pPart->calculateGeoLimits();
		if(isFirst)
		{
			minX = pPart->getMinX();
			minY = pPart->getMinY();
			maxX = pPart->getMaxX();
			maxY = pPart->getMaxY();
			isFirst = false;
		}
		else
		{
			if(pPart->getMinX() < minX)
			{
				minX = pPart->getMinX();
			}
			if(pPart->getMinY() < minY)
			{
				minY = pPart->getMinY();
			}
			if(pPart->getMaxX() > maxX)
			{
				maxX = pPart->getMaxX();
			}
			if(pPart->getMaxY() > maxY)
			{
				maxY = pPart->getMaxY();
			}
		}
	}
}

/*
**
** FUNCTION
**		Find (X,Y) coordinates of given position in raw line data by
**		interpolating coordinates of neighbour elements. Used to find
**		coordinates of line start/end in another line, also to find
**		cordinates of controllable equipment.
**
** PARAMETERS
**		pName			- Name of beam line where coordinates are needed.
**		pForLine		- Name of beam line who needs these coordinates, or
**							NULL if this is call to get coordinates for
**							controllable equipment.
**		connectVacType	- Vacuum type in parent line, see VacType class
**		coord			- Position of start/end in raw line.
**		isStart			- true if equipment is start of another line, false if it is finish of another line
**		resultX			- Resulting X coordinate is returned here.
**		resultY			- Resulting Y coordinate is returned here.
**
** RETURNS
**		true if location has been found;
**		false otherwise.
**
** CAUTIONS
**		None
*/
bool BeamLine::coordInLine(const char *pName, char *pForLine, int connectVacType, float coord, bool isStart,
	float &resultX, float &resultY)
{
	for(int partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		BeamLinePart *pPart = pParts->at(partIdx);
		if(strcmp(pPart->getName(), pName))
		{
			continue;
		}
		// Check if EQP coordinates are within coordinates range of part
		if(pPart->getStart() < pPart->getEnd())
		{
			if(coord < pPart->getStart())
			{
				continue;
			}
			if(coord > pPart->getEnd())
			{
				continue;
			}
		}
		else
		{
			if(coord < pPart->getEnd())
			{
				continue;
			}
			if(coord > pPart->getStart())
			{
				continue;
			}
		}
		nCalls++;
		Eqp *pEqp;
		int coco = pPart->coordInPart(coord, resultX, resultY, &pEqp, testEqp);
		if (coco > 5) {	// Method returns 10 if no equipment in part; normally return -1|0|1
			continue;
		}
/*
if(testEqp)
{
printf("BeamLine::coordInLine(%8.3f) returned %d (%8.3f %8.3f)\n", coord, coco, resultX, resultY);
fflush(stdout);
}
*/
		if(coco < 0)	// Before 1st device in part
		{
			if(!coordBeforeFirst(pPart, pEqp, coord, resultX, resultY))
			{
				return false;
			}
		}
		else if(coco > 0)	// After last device in part
		{
			if(!coordAfterLast(pPart, pEqp, coord, resultX, resultY))
			{
				return false;
			}
		}
		if(!pForLine)
		{
			return true;	// That's all for equipment coordinate
		}

		// If this coordinates are for line start/end - add equipment to parent line
		pEqp = new Eqp(isStart ? EqpType::OtherLineStart : EqpType::OtherLineEnd);
		pEqp->setName(pForLine);
		pEqp->setVacType(connectVacType);
		pEqp->setStart(coord);
		pEqp->setStartX(resultX);
		pEqp->setEndX(resultX);
		pEqp->setStartY(resultY);
		pEqp->setEndY(resultY);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		if(pEqp->getStart() > eqpList.last()->getStart())
		{
			eqpList.append(pEqp);
		}
		else if(pEqp->getStart() < eqpList.first()->getStart())
		{
			eqpList.prepend(pEqp);
		}
		else	// Add neither first nor last - exact order does no matter becaues
				// later ordered array will be built - so just add after the first item
		{
			eqpList.insert(1, pEqp);
		}
		return true;
	}
	return false;
}

/*
**
** FUNCTION
**		Find (X,Y) coordinates of given position in raw line data by
**		interpolating coordinates of neighbour elements - process the case
**		when required coordinate is before coordinate of the very first
**		passive device in given beam line part.
**
** PARAMETERS
**		pPart		- Pointer to beam line part where searching for coordinate
**		pFirstEqp	- Pointer to first useful equipment in this part
**		coord		- Position of start/end in raw line.
**		resultX		- Resulting X coordinate is returned here.
**		resultY		- Resulting Y coordinate is returned here.
**
** RETURNS
**		true if location has been found;
**		false otherwise.
**
** CAUTIONS
**		None
*/
bool BeamLine::coordBeforeFirst(BeamLinePart *pPart, Eqp *pFirstEqp, float coord, float &resultX, float &resultY)
{
	Eqp	*pPrevEqp, *pNextEqp;
	float	distance;
nBefore++;
	// Find previous device in beam line that can be used for calculations
	if((pPrevEqp = prevEqp(pPart, pFirstEqp)))
	{
		distance = (float)sqrt(
			(pFirstEqp->getStartX() - pPrevEqp->getEndX()) * (pFirstEqp->getStartX() - pPrevEqp->getEndX()) +
			(pFirstEqp->getStartY() - pPrevEqp->getEndY()) * (pFirstEqp->getStartY() - pPrevEqp->getEndY()));
		resultX = pPrevEqp->getEndX() + (pFirstEqp->getStartX() - pPrevEqp->getEndX()) /
			distance * (pFirstEqp->getStart() - coord);
		resultY = pPrevEqp->getEndY() + (pFirstEqp->getStartY() - pPrevEqp->getEndY()) /
			distance * (pFirstEqp->getStart() - coord);
		return true;
	}
	// Use first eqp if it has non-zero length
	if(pFirstEqp->getLength())
	{
		resultX = pFirstEqp->getStartX() + (pFirstEqp->getEndX() - pFirstEqp->getStartX()) /
			pFirstEqp->getLength() * (coord - pFirstEqp->getStart());
		resultY = pFirstEqp->getStartY() + (pFirstEqp->getEndY() - pFirstEqp->getStartY()) /
			pFirstEqp->getLength() * (coord - pFirstEqp->getStart());
		return true;
	}
	else	// Otherwise use next equipment after first one
	{
		if((pNextEqp = nextEqp(pPart, pFirstEqp)))
		{
			distance = (float)sqrt(
				(pNextEqp->getStartX() - pFirstEqp->getEndX()) * (pNextEqp->getStartX() - pFirstEqp->getEndX()) +
				(pNextEqp->getStartY() - pFirstEqp->getEndY()) * (pNextEqp->getStartY() - pFirstEqp->getEndY()));
			resultX = pFirstEqp->getStartX() + (pNextEqp->getStartX() - pFirstEqp->getStartX()) /
				distance * (coord - pFirstEqp->getStart());
			resultY = pFirstEqp->getStartY() + (pNextEqp->getStartY() - pFirstEqp->getStartY()) /
				distance * (coord - pFirstEqp->getStart());
			return true;
		}
	}
	return false;
}

/*
**
** FUNCTION
**		Find (X,Y) coordinates of given position in raw line data by
**		interpolating coordinates of neighbour elements - process the case
**		when required coordinate is after coordinate of the very last
**		passive device in given beam line part.
**
** PARAMETERS
**		pPart		- Pointer to beam line part where searching for coordinate
**		pFirstEqp	- Pointer to last useful equipment in this part
**		coord		- Position of start/end in raw line.
**		resultX		- Resulting X coordinate is returned here.
**		resultY		- Resulting Y coordinate is returned here.
**
** RETURNS
**		true if location has been found;
**		false otherwise.
**
** CAUTIONS
**		None
*/
bool BeamLine::coordAfterLast(BeamLinePart *pPart, Eqp *pEqp, float coord, float &resultX, float &resultY)
{
	Eqp	*pNextEqp, *pPrevEqp;
	float	distance;
nAfter++;

	// Find next device in beam line that can be used for calculations
	if((pNextEqp = nextEqp(pPart, pEqp)))
	{
		distance = (float)sqrt(
			(pNextEqp->getStartX() - pEqp->getEndX()) * (pNextEqp->getStartX() - pEqp->getEndX()) +
			(pNextEqp->getStartY() - pEqp->getEndY()) * (pNextEqp->getStartY() - pEqp->getEndY()));
		resultX = pEqp->getEndX() + (pNextEqp->getStartX() - pEqp->getEndX()) / distance *
			(coord - pEqp->getStart());
		resultY = pEqp->getEndY() + (pNextEqp->getStartY() - pEqp->getEndY()) / distance *
			(coord - pEqp->getStart());
		return true;
	}
	// No next devive - try to find previous one that can be used
	if((pPrevEqp = prevEqp(pPart, pEqp)))
	{
		distance = (float)sqrt(
			(pEqp->getStartX() - pPrevEqp->getEndX()) * (pEqp->getStartX() - pPrevEqp->getEndX()) +
			(pEqp->getStartY() - pPrevEqp->getEndY()) * (pEqp->getStartY() - pPrevEqp->getEndY()));
		resultX = pPrevEqp->getEndX() + (pEqp->getStartX() - pPrevEqp->getEndX()) / distance *
			(pEqp->getStart() - coord);
		resultY = pPrevEqp->getEndY() + (pEqp->getStartY() - pPrevEqp->getEndY()) / distance *
			(pEqp->getStart() - coord);
		return true;
	}
	// No pervious or next device - try to use this one
	if(pEqp->getLength())
	{
		resultX = pEqp->getStartX() + (pEqp->getEndX() - pEqp->getStartX()) /
			pEqp->getLength() * (coord - pEqp->getStart());
		resultY = pEqp->getStartY() + (pEqp->getEndY() - pEqp->getStartY()) /
			pEqp->getLength() * (coord - pEqp->getStart());
		return true;
	}
	return false;
}

/*
**
** FUNCTION
**		Decide if moving along line in position increasing direction
**		is clockwise or counterclockwise.
**
** PARAMETERS
**		None
**
** RETURNS
**		true if circle line is clockwise,
**		false otherwise
**
** CAUTIONS
**		None
*/
bool BeamLine::isClockWise(void)
{
	double	xCenter, yCenter,
		startAngle = 0.0,
		angle = 0.0,
		xStart = 0.0,
		yStart = 0.0;
	int	eqpIdx = -1;

	if(!circle)
	{
		return false;
	}

	xCenter = (minX + maxX) / 2.0;
	yCenter = (minY + maxY) / 2.0;

	BeamLinePart *pPart;
	Eqp **eqpPtrs;
	int partIdx, nEqpPtrs;
	for(partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		pPart = pParts->at(partIdx);
		eqpPtrs = pPart->getEqpPtrs();
		nEqpPtrs = pPart->getNEqpPtrs();
		for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
		{
			if(eqpPtrs[eqpIdx]->getType() != EqpType::Passive)
			{
				continue;
			}
			if(eqpPtrs[eqpIdx]->getStartX() == xCenter)
			{
				continue;
			}
			if(eqpPtrs[eqpIdx]->getStartY() == yCenter)
			{
				continue;
			}
			xStart = eqpPtrs[eqpIdx]->getStartX();
			yStart = eqpPtrs[eqpIdx]->getStartY();
			startAngle = atan2(xStart - xCenter, yStart - yCenter);
			break;
		}
		if(eqpIdx < nEqpPtrs)
		{
			break;
		}
	}

	for( ; partIdx < pParts->count() ; partIdx++)
	{
		pPart = pParts->at(partIdx);
		eqpPtrs = pPart->getEqpPtrs();
		nEqpPtrs = pPart->getNEqpPtrs();
		for( ; eqpIdx < nEqpPtrs ; eqpIdx++)
		{
			if(eqpPtrs[eqpIdx]->getType() != EqpType::Passive)
			{
				continue;
			}
			if(eqpPtrs[eqpIdx]->getStartX() == xCenter)
			{
				continue;
			}
			if(eqpPtrs[eqpIdx]->getStartY() == yCenter)
			{
				continue;
			}
			if(eqpPtrs[eqpIdx]->getStartX() == xStart)
			{
				continue;
			}
			if(eqpPtrs[eqpIdx]->getStartY() == yStart)
			{
				continue;
			}
			angle = atan2(eqpPtrs[eqpIdx]->getStartX() - xCenter,
				eqpPtrs[eqpIdx]->getStartY() - yCenter);
			break;
		}
		if(eqpIdx < nEqpPtrs)
		{
			break;
		}
		eqpIdx = 0;
	}

	// Compare two angles to decide
	startAngle += 2. * 3.1415926;
	angle += 2. * 3.1415926;
	return angle > startAngle;
}

/*
**
** FUNCTION
**		Get coordinates of two end points of beam line
**
** PARAMETERS
**		xStart	- Pointer to variable where X-coordinate of start will be written
**		yStart	- Pointer to variable where Y-coordinate of start will be written
**		xEnd	- Pointer to variable where X-coordinate of end will be written
**		yEnd	- Pointer to variable where Y-coordinate of end will be written
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void BeamLine::getEndPoints(float *xStart, float *yStart, float *xEnd, float *yEnd)
{
	int	eqpIdx;

	// Start point
	BeamLinePart *pPart;
	int partIdx;
	for(partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		pPart = pParts->at(partIdx);
		Eqp **eqpPtrs = pPart->getEqpPtrs();
		int nEqpPtrs = pPart->getNEqpPtrs();
		for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
		{
			if(eqpPtrs[eqpIdx]->getType() != EqpType::Passive)
			{
				continue;
			}
			*xStart = eqpPtrs[eqpIdx]->getStartX();
			*yStart = eqpPtrs[eqpIdx]->getStartY();
			break;
		}
		if(eqpIdx < nEqpPtrs)
		{
			break;
		}
	}

	// End point
	for(partIdx = pParts->count() - 1 ; partIdx >= 0 ; partIdx--)
	{
		pPart = pParts->at(partIdx);
		Eqp **eqpPtrs = pPart->getEqpPtrs();
		int nEqpPtrs = pPart->getNEqpPtrs();
		for(eqpIdx = nEqpPtrs - 1 ; eqpIdx >= 0 ; eqpIdx--)
		{
			if(eqpPtrs[eqpIdx]->getType() != EqpType::Passive)
			{
				continue;
			}
			*xEnd = eqpPtrs[eqpIdx]->getEndX();
			*yEnd = eqpPtrs[eqpIdx]->getEndY();
			break;
		}
		if(eqpIdx >= 0)
		{
			break;
		}
	}
}
#endif

/*
**
** FUNCTION
**		Build ordered arrays of equipment on all line parts
**
** PARAMETERS
**		None
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void BeamLine::buildOrderedEqp(void)
{
	for(int partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		BeamLinePart *pPart = pParts->at(partIdx);
		pPart->buildOrderedEqp();
	}
}

/*
**
** FUNCTION
**		Remove all active equipment from beam line
**
** PARAMETERS
**		None
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void BeamLine::clearActive(void)
{
	for(int partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		BeamLinePart *pPart = pParts->at(partIdx);
		pPart->clearActive();
	}
}

/*
**
** FUNCTION
**		Check if passive equipment shall be added to one of parts of this beam line,
**		if yes - add equipment.
**
** PARAMETERS
**		pEqp		- Pointer to equipment to be added
**		pureSurvey	- true if equipment shall be added to 'pure survey' line,
**						i.e. line without ELEMENTS
**
** RETURNS
**		true if equipment has been added to line;
**		false otherwise
**
** CAUTIONS
**		None
*/
bool BeamLine::addPassiveEqp(Eqp *pEqp, bool pureSurvey)
{
	BeamLinePart	*pPart;

	if(pureSurvey)
	{
		if(strcmp(name, pEqp->getSurveyPart()))
		{
			return false;
		}
		if(!pParts->isEmpty())
		{
			pPart = pParts->first();
		}
		else
		{
			pPart = BeamLinePart::create(name, pEqp->getStart(), pEqp->getStart() + pEqp->getLength());
			pParts->append(pPart);
		}
		pPart->addPureSurveyEqp(pEqp);
		return true;
	}
	else
	{
		for(int partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
		{
			pPart = pParts->at(partIdx);
			if(pPart->addPassiveEqp(pEqp))
			{
				return true;
			}
		}
	}
	return false;
}

/*
**
** FUNCTION
**		Check if active equipment shall be added to one of parts of this beam line,
**		if yes - add equipment.
**
** PARAMETERS
**		pEqp	- Pointer to equipment to be added
**
** RETURNS
**		true if equipment has been added to line;
**		false otherwise
**
** CAUTIONS
**		None
*/
bool BeamLine::addActiveEqp(Eqp *pEqp)
{
	// Check added for DSL line(s)
	// This check prevents entering .R, .B etc equipment into DSL line
	if(allowedVacType != VacType::None)
	{
		if(pEqp->getVacType() != allowedVacType)
		{
			return false;
		}
	}

	// Check if equipment is located in one of parts
	DataPool &pool = DataPool::getInstance();
	for(int partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		BeamLinePart *pPart = pParts->at(partIdx);
		if(strcmp(pPart->getName(), pEqp->getSurveyPart()))
		{
			continue;
		}
		// Check if EQP coordinates are within coordinates range of part
		if(pPart->getStart() < pPart->getEnd())
		{
			if(pEqp->getStart() < pPart->getStart())
			{
				continue;
			}
			if(pEqp->getStart() > pPart->getEnd())
			{
				continue;
			}
		}
		else
		{
			if(pEqp->getStart() < pPart->getEnd())
			{
				continue;
			}
			if(pEqp->getStart() > pPart->getStart())
			{
				continue;
			}
		}
		QList<Eqp *> &eqpList = pPart->getEqpList();
		eqpList.prepend(pEqp);
		if(pPart->isReverseBeamDirection())
		{
			pEqp->setReverseBeamDirection(true);
		}
#ifndef PVSS_SERVER_VERSION
		// (X,Y) coordinates are only required for client
		// Furthermore, they are not required for LHC where main view is
		// drawn not based on equipment (X,Y) coordinates
/*
if(!strcmp(pEqp->getName(), "ETP.VGP10"))
{
printf("BeamLine::addActiveEqp(%s) to %s\n", pEqp->getName(), name);
fflush(stdout);
//if(!strcmp(name, "DE"))
{
	testEqp = true;
}
}
*/
		if(!pool.isLhcFormat())
		{
			float	x, y;
			if(coordInLine(pEqp->getSurveyPart(), NULL, pEqp->getVacType(), pEqp->getSurveyPos(), false, x, y))
			{
				pEqp->setStartX(x);
				pEqp->setEndX(x);
				pEqp->setStartY(y);
				pEqp->setEndY(y);
			}
		}
// testEqp = false;
#endif
		return true;
	}
	return false;
}

/*
**
**	FUNCTION
**		Calculate new coordinate for one point (X,Y) after applying turning the
**		whole beam line by given angle
**
**	PARAMETERS
**		centerX	- X coordinate of reference point for resizing.
**		centerY	- Y coordinate of reference point for resizing.
**		delta	- Angle to turn beam line (-PI ... PI).
**		pEqp	- Pointer to device being rotated
**
**	RETURNS
**		None.
**
**	CAUTIONS
**		None
*/
void BeamLine::turnPoint(float centerX, float centerY, float delta, Eqp *pEqp)
{
	float	xOffset, yOffset, radius, angle;

	xOffset = pEqp->getStartX() - centerX;
	yOffset = pEqp->getStartY() - centerY;
	if((xOffset != 0.0) || (yOffset != 0.0))
	{
		angle = (float)atan2(yOffset, xOffset) + delta;
		radius = (float)sqrt(xOffset * xOffset + yOffset * yOffset);
		pEqp->setStartX(centerX + radius * (float)cos(angle));
		pEqp->setStartY(centerY + radius * (float)sin(angle));
	}
	xOffset = pEqp->getEndX() - centerX;
	yOffset = pEqp->getEndY() - centerY;
	if((xOffset != 0.0) || (yOffset != 0.0))
	{
		angle = (float)atan2(yOffset, xOffset) + delta;
		radius = (float)sqrt(xOffset * xOffset + yOffset * yOffset);
		pEqp->setEndX(centerX + radius * (float)cos(angle));
		pEqp->setEndY(centerY + radius * (float)sin(angle));
	}
}

/*
**
**	FUNCTION
**		Calculate new coordinate for one point (X,Y) after applying resize
**		factor for beam line.
**
**	PARAMETERS
**		centerX  - X coordinate of reference point for resizing.
**		centerY  - Y coordinate of reference point for resizing.
**		delta    - Factor for resizing (<1 to make line shorter, >1 to make line longer).
**		coordX   - Input: X coordinate of element before resizing,
**                	output: X coordinate of element after resizing.
**		coordY   - Input: Y coordinate of element before resizing,
**                	output: Y coordinate of element after resizing.
**
**	RETURNS
**		None.
**
**	CAUTIONS
**		None
*/
void BeamLine::resizePoint(float centerX, float centerY, float delta, float &coordX, float &coordY)
{
	float	xOffset, yOffset, radius, angle;

	xOffset = coordX - centerX;
	yOffset = coordY - centerY;
	if((xOffset == 0.0) && (yOffset == 0.0))
	{
		return;
	}
	angle = (float)atan2(yOffset, xOffset);
	radius = delta * (float)sqrt(xOffset * xOffset + yOffset * yOffset);
	coordX = centerX + radius * (float)cos(angle);
	coordY = centerY + radius * (float)sin(angle);
}

/*
**
** FUNCTION
**    Move all positions in child line by a given values in (X,Y).
**
** PARAMETERS
**    childName		- Name of beam line to be moved.
**    deltaX		- X coordinate offset to apply.
**    deltaY		- Y coordinate offset to apply.
**
** RETURNS
**    None.
**
** CAUTIONS
**    None
*/
void BeamLine::moveChildLines(const char *childName, float deltaX, float deltaY)
{
	BeamLine	*pChild;
	Eqp		*pEqp;
/*
printf("BeamLine::moveChildLines(%s, %9.3f, %9.3f): line is %s\n", childName, deltaX, deltaY, name);
fflush(stdout);
*/
	if(!(pChild = DataPool::getInstance().getLine(childName)))
	{
		return;
	}
	/* L.Kopylov 21.07.2011 for AD ring
	if(pChild->isCircle())
	{
		return;
	}
	*/

/*
printf("BeamLine::moveChildLines(): child found\n");
fflush(stdout);
*/

	if(pChild->resizing)
	{
		return;	// To prevent infifnite call stack
	}
/*
printf("BeamLine::moveChildLines(): child is not resizing\n");
fflush(stdout);
*/

	resizing = true;	// Do not move this line itself L.Kopylov 21.07.2011

	for(int partIdx = 0 ; partIdx < pChild->pParts->count() ; partIdx++)
	{
		BeamLinePart *pPart = pChild->pParts->at(partIdx);
		QList<Eqp *> &childEqpList = pPart->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < childEqpList.count() ; eqpIdx++)
		{
			pEqp = childEqpList.at(eqpIdx);
			pEqp->setStartX(pEqp->getStartX() + deltaX);
			pEqp->setStartY(pEqp->getStartY() + deltaY);
			if((pEqp->getType() == EqpType::OtherLineStart) ||
				(pEqp->getType() == EqpType::OtherLineEnd))
			{
				pChild->resizing = true;
				pChild->moveChildLines(pEqp->getName(), deltaX, deltaY);
				pChild->resizing = false;
			}
			else if((pEqp->getType() == EqpType::LineEnd) && (pChild->refType == RefStart))
			{
				pChild->resizing = true;
				pChild->moveChildLines(pEqp->getName(), deltaX, deltaY);
				pChild->resizing = false;
			}
			// L.Kopylov 22.07.2011 else
			{
				pEqp->setEndX(pEqp->getEndX() + deltaX);
				pEqp->setEndY(pEqp->getEndY() + deltaY);
			}
		}
	}
	resizing = false;	// L.Kopylov 21.07.2011
}

/*
**
**	FUNCTION
**		Find pointer to passive device in this beam line which is:
**		a) located before given (target) equipment, and
**		b) can be used together with target equipment for (X,Y)
**			coordinates interpolation.
**
**	PARAMETERS
**		pStartPart	- Pointer to beam line part where searching for coordinates
**		pTargetEqp	- Pointer to target equipment
**
**	RETURNS
**		Pointer to previous device;
**		NULL if there is no appropriate previous device
**
**	CAUTIONS
**		None
*/
Eqp *BeamLine::prevEqp(BeamLinePart *pStartPart, Eqp *pTargetEqp)
{
	Eqp				*pEqp, *pPrevEqp = NULL;
	BeamLinePart	*pPart = pStartPart;

	while(true)
	{
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			pEqp = eqpList.at(eqpIdx);
			if(pEqp->getType() != EqpType::Passive)
			{
				continue;	// Only use real equipment
			}
			if(pEqp == pTargetEqp)
			{
				break;
			}
			if((pEqp->getEndX() == pTargetEqp->getStartX()) && (pEqp->getEndY() == pTargetEqp->getStartY()))
			{
				continue;
			}
			pPrevEqp = pEqp;
			// For 'normal' neighbour part the last found device shall be used,
			// for 'reverse' neighbour part the first found device shall be used
			if((pPart != pStartPart) && pPart->isReverse())
			{
				break;
			}
		}
		if(pPrevEqp)
		{
			return pPrevEqp;
		}
		int partIdx = pParts->indexOf(pPart);
		if(partIdx > 0)
		{
			pPart = pParts->at(partIdx - 1);
		}
		else
		{
			if(circle)
			{
				pPart = pParts->last();
			}
			else
			{
				break;
			}
		}
		if(pPart == pStartPart)
		{
			break;
		}
	}
	return pPrevEqp;
}

/*
**
**	FUNCTION
**		Find pointer to passive device in this beam line which is:
**		a) located after given (target) equipment, and
**		b) can be used together with target equipment for (X,Y)
**			coordinates interpolation.
**
**	PARAMETERS
**		pStartPart	- Pointer to beam line part where searching for coordinates
**		pTargetEqp	- Pointer to target equipment
**
**	RETURNS
**		Pointer to next device;
**		NULL if there is no appropriate next device
**
**	CAUTIONS
**		None
*/
Eqp *BeamLine::nextEqp(BeamLinePart *pStartPart, Eqp *pTargetEqp)
{
	BeamLinePart	*pPart = pStartPart;
	Eqp				*pEqp, *pNextEqp = NULL;
	int				eqpIdx;

	while(true)
	{
		QList<Eqp *>	&eqpList = pPart->getEqpList();
		if(pPart == pStartPart)
		{
			eqpIdx = eqpList.indexOf(pTargetEqp);
		}
		else
		{
			eqpIdx = 0;
		}
		if((0 <= eqpIdx) && (eqpIdx < eqpList.count()))
		{
			for( ; eqpIdx < eqpList.count() ; eqpIdx++)
			{
				pEqp = eqpList.at(eqpIdx);
				if(pEqp->getType() != EqpType::Passive)
				{
					continue;	// Only use real equipment
				}
				if((pEqp->getEndX() != pTargetEqp->getStartX()) &&
					(pEqp->getEndY() != pTargetEqp->getStartY()))
				{
					pNextEqp = pEqp;
				}
				// For 'normal' neighbour part return the first device found,
				// for 'reverse' neighbour part return the last device found
				if((pPart == pStartPart) || (!pPart->isReverse()))
				{
					break;
				}
			}
		}
		if(pNextEqp)
		{
			return pNextEqp;
		}
		int partIdx = pParts->indexOf(pPart) + 1;
		if(partIdx < pParts->count())
		{
			pPart = pParts->at(partIdx);
		}
		else
		{
			if(circle)
			{
				pPart = pParts->first();
			}
			else
			{
				return NULL;
			}
		}
		if(pPart == pStartPart)
		{
			break;
		}
	}
	return NULL;
}

/*
**
**	FUNCTION
**		Find maximum drawing order for beam line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Maximum drawing order of beam line.
**
** CAUTIONS
**		None
**
*/
#ifndef PVSS_SERVER_VERSION
int BeamLine::maxDrawOrder(void)
{
	int	result = 0;

	for(int partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		BeamLinePart *pPart = pParts->at(partIdx);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)	// Coordinate order is not important
		{
			Eqp *pEqp = eqpList.at(eqpIdx);
			if(pEqp->isSpecColor())
			{
				if(pEqp->getDrawOrder() > result)
				{
					result = pEqp->getDrawOrder();
				}
			}
		}
	}
	return result;
}
#endif

#ifndef PVSS_SERVER_VERSION
int BeamLine::maxDrawEqpOrder(void)
{
	int	result = 0;

	for(int partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		BeamLinePart *pPart = pParts->at(partIdx);
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)	// Coordinate order is not important
		{
			Eqp *pEqp = eqpList.at(eqpIdx);
			if(pEqp->isSpecColor())
			{
				QColor color, borderColor;
				int order = 0;
				if(pEqp->getInterlockColor(DataEnum::Online, color, borderColor, order, NULL))
				{
					if(order > result)
					{
						result = order;
					}
				}
			}
		}
	}
	return result;
}
#endif

/*
**	FUNCTION
**		Find part of this line lying after given part
**
**	PARAMETERS
**		pStartPart	- Pointer to start part
**
**	RETURNS
**		Pointer to next line part; or
**		NULL if there is no next line part
**
**	CAUTIONS
**		For circular line method wraps over list of line parts
*/
BeamLinePart *BeamLine::nextPart(BeamLinePart *pStartPart)
{
	int partIdx = pParts->indexOf(pStartPart);
	if(partIdx < 0)
	{
		return NULL;
	}
	partIdx++;
	BeamLinePart *pPart = NULL;
	if(partIdx >= pParts->count())
	{
		if(circle)
		{
			pPart = pParts->first();
		}
	}
	else
	{
		pPart = pParts->at(partIdx);
	}
	return pPart;
}

/*
**	FUNCTION
**		Find start and end equipment on this beam line in given sector range
**
**	PARAMETERS
**		pStartSector	- Pointer to first sector in range
**		pEndSector		- Pointer last sector in range
**		ppStartPart		- Pointer to variable where pointer to start beam line part
**							will be written
**		ppEndPart		- Pointer to variable where pointer to end beam line part
**							will be written
**		startEqpIdx		- Variable where index of first device on start beam line
**							part will be written
**		endEqpIdx		- Variable where index of last device on end beam line part
**							will be writtem
**
**	RETURNS
**		true if at least some of sectors in range are on given beam line,
**		false otherwise.
**
**	CAUTIONS
**		None
*/
bool BeamLine::findEqpRange(Sector *pStartSector, Sector *pEndSector,
	BeamLinePart **ppStartPart, BeamLinePart **ppEndPart, int &startEqpIdx, int &endEqpIdx)
{
	// Range of equipment is built differently for circle and straight lines
	if(circle)
	{
		return findEqpRangeOnCircle(pStartSector, pEndSector,
				ppStartPart, ppEndPart, startEqpIdx, endEqpIdx);
	}
	return findEqpRangeOnStraight(pStartSector, pEndSector,
				ppStartPart, ppEndPart, startEqpIdx, endEqpIdx);
}

/*
**	FUNCTION
**		Find start and end equipment on this beam line in given sector range
**		The version for circular beam line shall take into account sector order
**		because it can pass start of line. On the other hand, sector range on circle
**		line may not have other sectors between sectors of original range
**
**	PARAMETERS
**		pStartSector	- Pointer to first sector in range
**		pEndSector		- Pointer last sector in range
**		ppStartPart		- Pointer to variable where pointer to start beam line part
**							will be written
**		ppEndPart		- Pointer to variable where pointer to end beam line part
**							will be written
**		startEqpIdx		- Variable where index of first device on start beam line
**							part will be written
**		endEqpIdx		- Variable where index of last device on end beam line part
**							will be writtem
**
**	RETURNS
**		true if at least some of sectors in range are on given beam line,
**		false otherwise.
**
**	CAUTIONS
**		None
*/
bool BeamLine::findEqpRangeOnCircle(Sector *pStartSector, Sector *pEndSector,
	BeamLinePart **ppStartPart, BeamLinePart **ppEndPart, int &startEqpIdx, int &endEqpIdx)
{
	bool	result = false;
	DataPool &pool = DataPool::getInstance();
	int endPartIdx = -1;
	BeamLinePart	*pStartPart = NULL, *pEndPart = NULL;
	int	startEqp = -1, endEqp = -1; 
	Sector *pSector = pStartSector;
	do
	{
		// Find equipment range for all draw parts on required beam line
		bool startFound = false;
		QList<SectorDrawPart *> &drawParts = pSector->getDrawParts();
		for(int drawPartIdx = 0 ; drawPartIdx < drawParts.count() ; drawPartIdx++)
		{
			SectorDrawPart *pPart = drawParts.at(drawPartIdx);
			if(pPart->getLine() != this)
			{
				continue;
			}
			if(!startFound)
			{
				pStartPart = pPart->getStartPart();
				startEqp = pPart->getStartEqpIdx();
				pEndPart = pPart->getEndPart();
				endEqp = pPart->getEndEqpIdx();
				endPartIdx = findPartIdx(pEndPart);
				startFound = true;
			}
			else
			{
				if(pPart->isCircleClose())
				{
					pEndPart = pPart->getEndPart();
					endEqp = pPart->getEndEqpIdx();
				}
				else
				{
					int newEndPartIdx = findPartIdx(pPart->getEndPart());
					if(newEndPartIdx > endPartIdx)
					{
						pEndPart = pPart->getEndPart();
						endEqp = pPart->getEndEqpIdx();
						endPartIdx = newEndPartIdx;
					}
					else if((pPart->getEndPart() == pEndPart) && (pPart->getEndEqpIdx() > endEqpIdx))
					{
						endEqp = pPart->getEndEqpIdx();
					}
				}
			}
		}

		// Apply found equipment range of sector
		if(startFound)	// at least one draw part is on required beam line
		{
			if(!result)	// First sector on this beam line
			{
				*ppStartPart = pStartPart;
				startEqpIdx = startEqp;
				*ppEndPart = pEndPart;
				endEqpIdx = endEqp;
				result = true;
			}
			else
			{
				*ppEndPart = pEndPart;
				endEqpIdx = endEqp;
			}
		}
		if(pSector == pEndSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);
	return result;
}

/*
**	FUNCTION
**		Find start and end equipment on this beam line in given sector range
**		The version for straight beam line shall NOT take into account sector order,
**		but it must take all equipment in sectors, even if there are other sectors
**		(not in original range) between sectors in original range.
**
** PARAMETERS
**		pStartSector	- Pointer to first sector in range
**		pEndSector		- Pointer last sector in range
**		ppStartPart		- Pointer to variable where pointer to start beam line part
**							will be written
**		ppEndPart		- Pointer to variable where pointer to end beam line part
**							will be written
**		startEqpIdx		- Variable where index of first device on start beam line
**							part will be written
**		endEqpIdx		- Variable where index of last device on end beam line part
**							will be writtem
**
**	RETURNS
**		true if at least some of sectors in range are on given beam line,
**		false otherwise.
**
**	CAUTIONS
**		None
*/
bool BeamLine::findEqpRangeOnStraight(Sector *pStartSector, Sector *pEndSector,
	BeamLinePart **ppStartPart, BeamLinePart **ppEndPart, int &startEqpIdx, int &endEqpIdx)
{
	DataPool	&pool = DataPool::getInstance();
	bool		startFound = false;
	Sector		*pSector = pStartSector;

	// Every draw part on this line can just extend previous range
	int	startPartIdx = 0, endPartIdx = 0;
	do
	{
		// Find equipment range for all draw parts on this beam line
		QList<SectorDrawPart *> &drawParts = pSector->getDrawParts();
		for(int drawPartIdx = 0 ; drawPartIdx < drawParts.count() ; drawPartIdx++)
		{
			SectorDrawPart *pPart = drawParts.at(drawPartIdx);
			if(pPart->getLine() != this)
			{
				continue;
			}
			if(!startFound)
			{
				// The very first draw part for this line - takes all limits of draw part
				*ppStartPart = pPart->getStartPart();
				startEqpIdx = pPart->getStartEqpIdx();
				startPartIdx = findPartIdx(*ppStartPart);
				*ppEndPart = pPart->getEndPart();
				endEqpIdx = pPart->getEndEqpIdx();
				endPartIdx = findPartIdx(*ppEndPart);
				startFound = true;
			}
			else	// Not first draw part on this line
			{
				// check if start of range shall be extended
				int newPartIdx = findPartIdx(pPart->getStartPart());
				if(newPartIdx < startPartIdx)
				{
					*ppStartPart = pPart->getStartPart();
					startEqpIdx = pPart->getStartEqpIdx();
					startPartIdx = newPartIdx;
				}
				else if(pPart->getStartPart() == *ppStartPart)
				{
					if(pPart->getStartEqpIdx() < startEqpIdx)
					{
						startEqpIdx = pPart->getStartEqpIdx();
					}
				}

				// Check if end of range shall be extended
				newPartIdx = findPartIdx(pPart->getEndPart());
				if(newPartIdx > endPartIdx)
				{
					*ppEndPart = pPart->getEndPart();
					endEqpIdx = pPart->getEndEqpIdx();
					endPartIdx = newPartIdx;
				}
				else if(pPart->getEndPart() == *ppEndPart)
				{
					if(pPart->getEndEqpIdx() > endEqpIdx)
					{
						endEqpIdx = pPart->getEndEqpIdx();
					}
				}
			}
		}
		if(pSector == pEndSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);
	return startFound;
}



/*
**	FUNCTION
**		Find index of given beam line part within this line
**
**	PARAMETERS
**		pLinePart	- Pointer to beam line part to find
**
**	RETURNS
**		Index of given beam line part;
**		-1 if part is not found
**
**	CAUTIONS
**		None
*/
int BeamLine::findPartIdx(BeamLinePart *pLinePart)
{
	return pParts->indexOf(pLinePart);
}


/*
** FUNCTION
**		Make sure all sectors (more precise - all areas between valves) are
**		not shorter than specified minimum length. This is done in order to
**		make sure state of every individual valve is visible on main view.
**		Originally request came for SPS - and first implementation is for
**		SPS ring ONLY.
**
** PARAMETERS
**		minLength	- required minimum length of sector [m]
**
** RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void BeamLine::setMinSectorLength(int minLength)
{
	if(minLength <= 0)
	{
		return;
	}

	// Algorithm for SPS only
	if(strcmp(name, "SPS"))
	{
		return;
	}

	// Set zero length for all devices with small length - they will not be visible on synoptic anyway
	// This can help to get more space for extension
	for(int partIdx = 0 ; partIdx < pParts->count() ; partIdx++)
	{
		pParts->at(partIdx)->shortenEqp();
	}

	// There are a number of devices which can not be moved:
	// - valves between two main parts
	// - start/end of child lines
	// So, all changes shall be done in range between two fixed points
	int startFixedPartIdx = -1, endFixedPartIdx = -1, startFixedEqpIdx = -1, endFixedEqpIdx = -1;
	while(true)
	{
		startFixedPartIdx = endFixedPartIdx;
		startFixedEqpIdx = endFixedEqpIdx;
		nextFixedEqp(endFixedPartIdx, endFixedEqpIdx);

		processAreaBetweenFixedPoints(minLength, startFixedPartIdx, startFixedEqpIdx, endFixedPartIdx, endFixedEqpIdx);
		if(endFixedPartIdx < 0)
		{
			break;
		}
	}
}

/*
** FUNCTION
**		Extend sectors in area between two fixed points
**
** PARAMETERS
**		minLength			- Minimum allowed sector length
**		startFixedPartIdx	- Index of beam line part where start fixed device is located
**		startFixedEqpIdx	- Index of start fixed device in beam line part
**		endFixedPartIdx	- Index of beam line part where end fixed device is located
**		endFixedEqpIdx		- Index of end fixed device in beam line part
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void BeamLine::processAreaBetweenFixedPoints(int minLength, int startFixedPartIdx, int startFixedEqpIdx, int endFixedPartIdx, int endFixedEqpIdx)
{
	// Processing is different depending on types of fixed devices
	Eqp *pStartFixedEqp = NULL;
	if(startFixedPartIdx >= 0)
	{
		BeamLinePart *pPart = pParts->at(startFixedPartIdx);
		pStartFixedEqp = pPart->getEqpPtrs()[startFixedEqpIdx];
	}
	Eqp *pEndFixedEqp = NULL;
	if(endFixedPartIdx >= 0)
	{
		BeamLinePart *pPart = pParts->at(endFixedPartIdx);
		pEndFixedEqp = pPart->getEqpPtrs()[endFixedEqpIdx];
	}

	if(pStartFixedEqp)
	{
		if(pEndFixedEqp)
		{
			if(pStartFixedEqp->getType() == EqpType::VacDevice)
			{
				// Area between two valves shall be processed from center towards start and end
				if(pEndFixedEqp->getType() == EqpType::VacDevice)
				{
					processAreaFromCenter(minLength, startFixedPartIdx, startFixedEqpIdx, endFixedPartIdx, endFixedEqpIdx);
				}
				else
				{
					processAreaFromEnd(minLength, startFixedPartIdx, startFixedEqpIdx, endFixedPartIdx, endFixedEqpIdx);
				}
			}
			else if(pEndFixedEqp->getType() == EqpType::VacDevice)
			{
				processAreaFromStart(minLength, startFixedPartIdx, startFixedEqpIdx, endFixedPartIdx, endFixedEqpIdx);
			}
			else
			{
				processAreaFromCenter(minLength, startFixedPartIdx, startFixedEqpIdx, endFixedPartIdx, endFixedEqpIdx);
			}
		}
		else
		{
			processAreaFromEnd(minLength, startFixedPartIdx, startFixedEqpIdx, endFixedPartIdx, endFixedEqpIdx);
		}
	}
	else if(pEndFixedEqp)
	{
		// Do not process the very first few devices of SPS - they at the end of long sector
		/*
		if(pEndFixedEqp->getType() == EqpType::VacDevice)
		{
			processAreaFromStart(minLength, startFixedPartIdx, startFixedEqpIdx, endFixedPartIdx, endFixedEqpIdx);
		}
		*/
	}
}

/*
** FUNCTION
**		Extend short sectors in area between two fixed points - sectors are extended in direction
**		from start to end of region
**
** PARAMETERS
**		minLength			- Minimum allowed sector length
**		startFixedPartIdx	- Index of beam line part where start fixed device is located
**		startFixedEqpIdx	- Index of start fixed device in beam line part
**		endFixedPartIdx	- Index of beam line part where end fixed device is located
**		endFixedEqpIdx		- Index of end fixed device in beam line part
**
** RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void BeamLine::processAreaFromStart(int minLength, int startFixedPartIdx, int startFixedEqpIdx, int endFixedPartIdx, int endFixedEqpIdx)
{
	if(startFixedPartIdx < 0)
	{
		startFixedPartIdx = 0;
		startFixedEqpIdx = 0;
	}
	if(endFixedPartIdx < 0)
	{
		endFixedPartIdx = pParts->count() - 1;
		BeamLinePart *pPart = pParts->at(endFixedPartIdx);
		endFixedEqpIdx = pPart->getNEqpPtrs() - 1;
	}

	// Find total amount all sectors shall be extended
	float totalHoles;
	float totalExtend = totalSectorsToExtend(minLength, startFixedPartIdx, startFixedEqpIdx, endFixedPartIdx, endFixedEqpIdx, totalHoles);
	if(totalExtend <= 0)
	{
		return;	// No sectors to extend
	}
	if(totalHoles <= 0)
	{
		return;	// Not possible to extend
	}
	if(totalHoles < totalExtend)
	{
		minLength = (int)((float)minLength * totalHoles / totalExtend);
	}
	int partIdx = startFixedPartIdx, eqpIdx = startFixedEqpIdx;
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp *pValve = pPart->getEqpPtrs()[eqpIdx];
	if(pValve->getFunctionalType() != FunctionalType::VV)	// This is not a valve, try to use previous valve (if any)
	{
		int prevPartIdx, prevEqpIdx;
		pValve = findPreviousValve(partIdx, eqpIdx, prevPartIdx, prevEqpIdx);
		if(!pValve)
		{
			pValve = pPart->getEqpPtrs()[eqpIdx];
		}
	}
	while(true)
	{
		int nextPartIdx, nextEqpIdx;
		Eqp *pNextValve = findNextValve(partIdx, eqpIdx, nextPartIdx, nextEqpIdx);
		if(!pNextValve)
		{
			return;
		}
		if((nextPartIdx != endFixedPartIdx) || (nextEqpIdx >= endFixedEqpIdx))
		{
			return;
		}
		partIdx = nextPartIdx;
		eqpIdx = nextEqpIdx;
		float sectorLength = pNextValve->getStart() - pValve->getStart();
		if(sectorLength < minLength)
		{
			makeSectorShorterForward(nextPartIdx, nextEqpIdx, endFixedPartIdx, endFixedEqpIdx, minLength - sectorLength, minLength);
		}
		pValve = pNextValve;
	}
}

/*
** FUNCTION
**		Extend short sectors in area between two fixed points - sectors are extended in direction
**		from center of area towards start to end of area. This approach works for
**		SPS sextants where every sextant has long sector at start and end and
**		a number of sjorter sectors in the middile.
**
** PARAMETERS
**		minLength			- Minimum allowed sector length
**		startFixedPartIdx	- Index of beam line part where start fixed device is located
**		startFixedEqpIdx	- Index of start fixed device in beam line part
**		endFixedPartIdx	- Index of beam line part where end fixed device is located
**		endFixedEqpIdx		- Index of end fixed device in beam line part
**
** RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void BeamLine::processAreaFromCenter(int minLength, int startFixedPartIdx, int startFixedEqpIdx, int endFixedPartIdx, int endFixedEqpIdx)
{
	if(startFixedPartIdx < 0)
	{
		startFixedPartIdx = 0;
		startFixedEqpIdx = 0;
	}
	if(endFixedPartIdx < 0)
	{
		endFixedPartIdx = pParts->count() - 1;
		BeamLinePart *pPart = pParts->at(endFixedPartIdx);
		endFixedEqpIdx = pPart->getNEqpPtrs() - 1;
	}

	BeamLinePart *pPart = pParts->at(startFixedPartIdx);
	Eqp *pStartEqp = pPart->getEqpPtrs()[startFixedEqpIdx];
	pPart = pParts->at(endFixedPartIdx);
	Eqp *pEndEqp = pPart->getEqpPtrs()[endFixedEqpIdx];
	float areaLength = pEndEqp->getStart() - pStartEqp->getStart();
	if(areaLength < minLength)
	{
		return;	// No chance to do something useful
	}

	// Try to find valve in the center of area
	float areaCenter = (pStartEqp->getStart() + pEndEqp->getStart()) / 2;
	int bestValvePartIdx = -1, bestValveEqpIdx = -1;
	float bestDistance = areaLength;
	int partIdx = startFixedPartIdx, eqpIdx = startFixedEqpIdx;
	while(true)
	{
		Eqp *pValve = findNextValve(partIdx, eqpIdx, partIdx, eqpIdx);
		if(!pValve)
		{
			break;
		}
		float valveDistance = (float)fabs(pValve->getStart() - areaCenter);
		if(valveDistance < bestDistance)
		{
			bestDistance = valveDistance;
			bestValvePartIdx = partIdx;
			bestValveEqpIdx = eqpIdx;
		}
		else
		{
			break;	// When distance start increasing
		}
	}
	if(bestValvePartIdx >= 0)
	{
		// Process from valve to both ends
		processAreaFromEnd(minLength, startFixedPartIdx, startFixedEqpIdx, bestValvePartIdx, bestValveEqpIdx);
		processAreaFromStart(minLength, bestValvePartIdx, bestValveEqpIdx, endFixedPartIdx, endFixedEqpIdx);
	}
}

/*
** FUNCTION
**		Extend sectors in area between two fixed points - sectors are extended in direction
**		from end to start of region
**
** PARAMETERS
**		minLength			- Minimum allowed sector length
**		startFixedPartIdx	- Index of beam line part where start fixed device is located
**		startFixedEqpIdx	- Index of start fixed device in beam line part
**		endFixedPartIdx	- Index of beam line part where end fixed device is located
**		endFixedEqpIdx		- Index of end fixed device in beam line part
**
** RETURNS
**		None
**
** CAUTIONS
**		None
**
*/
void BeamLine::processAreaFromEnd(int minLength, int startFixedPartIdx, int startFixedEqpIdx, int endFixedPartIdx, int endFixedEqpIdx)
{
	if(startFixedPartIdx < 0)
	{
		startFixedPartIdx = 0;
		startFixedEqpIdx = 0;
	}
	if(endFixedPartIdx < 0)
	{
		endFixedPartIdx = pParts->count() - 1;
		BeamLinePart *pPart = pParts->at(endFixedPartIdx);
		endFixedEqpIdx = pPart->getNEqpPtrs() - 1;
	}

	// Find total amount all sectors shall be extended
	float totalHoles;
	float totalExtend = totalSectorsToExtend(minLength, startFixedPartIdx, startFixedEqpIdx, endFixedPartIdx, endFixedEqpIdx, totalHoles);
	if(totalExtend <= 0)
	{
		return;	// No sectors to extend
	}
	if(totalHoles <= 0)
	{
		return;	// Not possible to extend
	}
	if(totalHoles < totalExtend)
	{
		minLength = (int)((float)minLength * totalHoles / totalExtend);
	}
	int partIdx = endFixedPartIdx, eqpIdx = endFixedEqpIdx;
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp *pValve = pPart->getEqpPtrs()[eqpIdx];
	if(pValve->getFunctionalType() != FunctionalType::VV)	// This is not a valve, try to use previous valve (if any)
	{
		int nextPartIdx, nextEqpIdx;
		pValve = findNextValve(partIdx, eqpIdx, nextPartIdx, nextEqpIdx);
		if(!pValve)
		{
			pValve = pPart->getEqpPtrs()[eqpIdx];
		}
	}
	while(true)
	{
		int prevPartIdx, prevEqpIdx;
		Eqp *pPrevValve = findPreviousValve(partIdx, eqpIdx, prevPartIdx, prevEqpIdx);
		if(!pPrevValve)
		{
			return;
		}
		if((prevPartIdx != startFixedPartIdx) || (prevEqpIdx <= startFixedEqpIdx))
		{
			return;
		}
		partIdx = prevPartIdx;
		eqpIdx = prevEqpIdx;
		float sectorLength = pValve->getStart() - pPrevValve->getStart();
		if(sectorLength < minLength)
		{
			makeSectorShorterBackward(prevPartIdx, prevEqpIdx, startFixedPartIdx, startFixedEqpIdx, minLength - sectorLength, minLength);
		}
		pValve = pPrevValve;
	}
}


/*
** FUNCTION
**		Move start of sector in forward direction by given distance.
**		If sector is not long enough to move it's start by required distance -
**		try to move start of next sector (recursively).
**
** PARAMETERS
**		partIdx				- Index of beam line part where first device of this sector is located
**		eqpIdx				- Index of first device of this sector in it' line part
**		endFixedPartIdx	- Index of beam line part where motion shall stop
**		endFixedEqpIdx		- Index of device where motion shall stop in it's line part
**		delta					- Required distance to move sector start
**		minLength			- Minimum allowed length of sector
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void BeamLine::makeSectorShorterForward(int partIdx, int eqpIdx, int endFixedPartIdx, int endFixedEqpIdx, float delta, int minLength)
{
	if((partIdx == endFixedPartIdx) && (eqpIdx == endFixedEqpIdx))
	{
		return;
	}
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp *pValve = pPart->getEqpPtrs()[eqpIdx];

	// Check if this sector may be made shorter. First find next valve after start of sector,
	// search shall not go behind end fixed device
	int nextValvePartIdx = partIdx;
	int nextValveEqpIdx = eqpIdx;
	Eqp *pNextValve = findNextValve(partIdx, eqpIdx, nextValvePartIdx, nextValveEqpIdx);
	if((nextValvePartIdx != endFixedPartIdx) || (nextValveEqpIdx >= endFixedEqpIdx))
	{
		nextValvePartIdx = endFixedPartIdx;
		nextValveEqpIdx = endFixedEqpIdx;
	}
	if(nextValvePartIdx < 0)
	{
		nextValvePartIdx = pParts->count() - 1;
		pPart = pParts->at(nextValvePartIdx);
		nextValveEqpIdx = pPart->getNEqpPtrs() - 1;
	}
	pPart = pParts->at(nextValvePartIdx);
	pNextValve = pPart->getEqpPtrs()[nextValveEqpIdx];

	// May this sector be made shorter?
	float sectorLength = pNextValve->getStart() - pValve->getStart();
	float mayShortenThis = 0;
	if(sectorLength > (minLength + delta))
	{
		// Check if there are enough holes between elements to move devices
		mayShortenThis = totalHolesInArea(partIdx, eqpIdx, nextValvePartIdx, nextValveEqpIdx);
	}

	if(mayShortenThis < delta)
	{
		makeSectorShorterForward(nextValvePartIdx, nextValveEqpIdx, endFixedPartIdx, endFixedEqpIdx, delta - mayShortenThis, minLength);
		// Check if there are enough holes between elements to move devices
		mayShortenThis = totalHolesInArea(partIdx, eqpIdx, nextValvePartIdx, nextValveEqpIdx);
	}
	if(mayShortenThis > 0)
	{
		moveEqpForward(partIdx, eqpIdx, nextValvePartIdx, nextValveEqpIdx, mayShortenThis > delta ? delta : mayShortenThis);
	}
}

/*
** FUNCTION
**		Move given device forward by given distance
**
** PARAMETERS
**		partIdx				- Index of beam line part where device to be moved is located
**		eqpIdx				- Index of device to be moved in it' line part
**		endFixedPartIdx	- Index of beam line part where motion shall stop
**		endFixedEqpIdx		- Index of device where motion shall stop in it's line part
**		delta					- Required distance to move device
**
** RETURNS
**		How far this device was actually moved
**
** CAUTIONS
**		None
*/
float BeamLine::moveEqpForward(int partIdx, int eqpIdx, int endPartIdx, int endEqpIdx, float delta)
{
	if((partIdx == endPartIdx) && (eqpIdx == endEqpIdx))
	{
		return 0;	// reached end device
	}
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp *pEqp = pPart->getEqpPtrs()[eqpIdx];

	float result = 0;
	switch(pEqp->getType())
	{
	case EqpType::Passive:
		result = moveEqpForwardPassive(partIdx, eqpIdx, endPartIdx, endEqpIdx, delta);
		break;
	case EqpType::VacDevice:
		result = moveEqpForwardActive(partIdx, eqpIdx, endPartIdx, endEqpIdx, delta);
		break;
	default:
		result = moveEqpForwardActive(partIdx, eqpIdx, endPartIdx, endEqpIdx, delta);
		break;
	}
	return result;
}

/*
** FUNCTION
**		Move active device by given distance in forward direction. If there is
**		not enough space to move this device - try to move next one (recursively)
**
** PARAMETERS
**		partIdx		- Index of beam line where the equipment to move is located
**		eqpIdx		- Index of equipment to move in it's beam line part
**		endPartIdx	- Index of beam line part where the very last device of area being processed is located
**		endEqpIdx	- Index of the very last device of area being processed in it's beam line part
**		delta			- Required distance increase
**
** RETURNS
**		How far this device was actually moved
**
** CAUTIONS
**		None
*/
float BeamLine::moveEqpForwardActive(int partIdx, int eqpIdx, int endPartIdx, int endEqpIdx, float delta)
{
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp *pEqp = pPart->getEqpPtrs()[eqpIdx];

	// Device can be part of 'group' (several devices at the same location).
	// It is necessary to move group as a whole
	bool eqpInGroup = false;
	int nextPartIdx = partIdx;
	int nextEqpIdx = eqpIdx;
	Eqp *pNextEqp = NULL;
	BeamLinePart *pNextPart = pPart;
	while(true)
	{
		if(!nextDeviceIndices(nextPartIdx, nextEqpIdx, false))
		{
			return 0;
		}
		pNextPart = pParts->at(nextPartIdx);
		pNextEqp = pNextPart->getEqpPtrs()[nextEqpIdx];
		if((nextPartIdx != endPartIdx) || (nextEqpIdx >= endEqpIdx))
		{
			break;
		}
		if(pNextEqp->getStart() > pEqp->getStart())
		{
			break;
		}
		eqpInGroup = true;
	}

	// How far can we move this device without moving next one
	float mayMoveThis = pNextEqp->getStart() - (pEqp->getStart() + pEqp->getLength()) - MIN_HOLE;
	if(mayMoveThis >= delta)
	{
		mayMoveThis = delta;
	}
	else
	{
		// Moving just this device is not enough, let's try to move next one
		if(mayMoveThis < 0)
		{
			mayMoveThis = 0;
		}
		mayMoveThis += moveEqpForward(nextPartIdx, nextEqpIdx, endPartIdx, endEqpIdx, delta - mayMoveThis);
	}
	if(mayMoveThis > 0)
	{
		float x, y;
		if(coordInLine(pPart->getName(), NULL, VacType::None, pEqp->getStart() + mayMoveThis, true, x, y))
		{
			if(eqpInGroup)
			{
				int groupPartIdx = partIdx;
				int groupEqpIdx = eqpIdx;
				pNextPart = pPart;
				while(true)
				{
					// Apply changes to device
					pEqp = pNextPart->getEqpPtrs()[groupEqpIdx];
					if(pEqp->getLength() == 0)
					{
						pEqp->setStart(pEqp->getStart() + mayMoveThis);
						pEqp->setStartX(x);
						pEqp->setEndX(x);
						pEqp->setStartY(y);
						pEqp->setEndY(y);
					}
					else
					{
						float endX, endY;
						if(coordInLine(pPart->getName(), NULL, VacType::None, pEqp->getStart() + pEqp->getLength() + mayMoveThis, true, endX, endY))
						{
							pEqp->setStart(pEqp->getStart() + mayMoveThis);
							pEqp->setStartX(x);
							pEqp->setEndX(endX);
							pEqp->setStartY(y);
							pEqp->setEndY(endY);
						}
						else
						{
							// Not very elegant, but this shall not happen
							pEqp->setStart(pEqp->getStart() + mayMoveThis);
							pEqp->setStartX(x);
							pEqp->setEndX(x);
							pEqp->setStartY(y);
							pEqp->setEndY(y);
						}
					}

					// Move to next device in group
					if(!nextDeviceIndices(groupPartIdx, groupEqpIdx, false))
					{
						break;	// This shall not happen
					}
					pNextPart = pParts->at(groupPartIdx);
					if((groupPartIdx == nextPartIdx) && (groupEqpIdx == nextEqpIdx))
					{
						break;
					}
				}
			}
			else
			{
				if(pEqp->getLength() == 0)
				{
						pEqp->setStart(pEqp->getStart() + mayMoveThis);
						pEqp->setStartX(x);
						pEqp->setEndX(x);
						pEqp->setStartY(y);
						pEqp->setEndY(y);
				}
				else
				{
					float endX, endY;
					if(coordInLine(pPart->getName(), NULL, VacType::None, pEqp->getStart() + pEqp->getLength() + mayMoveThis, true, endX, endY))
					{
						pEqp->setStart(pEqp->getStart() + mayMoveThis);
						pEqp->setStartX(x);
						pEqp->setEndX(endX);
						pEqp->setStartY(y);
						pEqp->setEndY(endY);
					}
					else
					{
						mayMoveThis = 0;
					}
				}
			}
		}
		else
		{
			mayMoveThis = 0;
		}
	}
	else
	{
		mayMoveThis = 0;
	}

	if(mayMoveThis >= delta)
	{
		return delta;
	}

	return mayMoveThis;
}

/*
**
** FUNCTION
**		Move passive device by specified distance in forward direction
**
** PARAMETERS
**		partIdx		- Index of beam line where the equipment to move is located
**		eqpIdx		- Index of equipment to move in it's beam line part
**		endPartIdx	- Index of beam line part where the very last device of area being processed is located
**		endEqpIdx	- Index of the very last device of area being processed in it's beam line part
**		delta			- Required distance increase
**
** RETURNS
**		How far this device was actually moved
**
** CAUTIONS
**		None
**
*/
float BeamLine::moveEqpForwardPassive(int partIdx, int eqpIdx, int endPartIdx, int endEqpIdx, float delta)
{
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp *pEqp = pPart->getEqpPtrs()[eqpIdx];

	// Passive device may be long enough to cover one (or more) active devices after it.
	// After extension they still shall remain 'within' this passive device. So, we'll try
	// to move next device which is not within this passive
	int nextPartIdx = partIdx;
	int nextEqpIdx = eqpIdx;
	Eqp *pNextEqp = NULL;
	BeamLinePart *pNextPart = pPart;
	while(true)
	{
		if(!nextDeviceIndices(nextPartIdx, nextEqpIdx, false))
		{
			return 0;
		}
		pNextPart = pParts->at(nextPartIdx);
		pNextEqp = pNextPart->getEqpPtrs()[nextEqpIdx];
		if((nextPartIdx != partIdx) || (nextEqpIdx >= endEqpIdx))
		{
			break;
		}
		if(pNextEqp->getStart() > (pEqp->getStart() + pEqp->getLength()))
		{
			break;
		}
	}

	// How far can we move this device without moving next one
	float mayMoveThis = pNextEqp->getStart() - (pEqp->getStart() + pEqp->getLength()) - MIN_HOLE;
	if(mayMoveThis >= delta)
	{
		mayMoveThis = delta;
	}
	else
	{
		// Moving just this device is not enough, let's try to move next one
		mayMoveThis += moveEqpForward(nextPartIdx, nextEqpIdx, endPartIdx, endEqpIdx, delta - mayMoveThis);
	}
	if(mayMoveThis > 0)
	{
		float x, y;
		if(coordInLine(pPart->getName(), NULL, VacType::None, pEqp->getStart() + mayMoveThis, true, x, y))
		{
			int groupPartIdx = partIdx;
			int groupEqpIdx = eqpIdx;
			pNextPart = pPart;
			while(true)
			{
					// Apply changes to device
				pEqp = pNextPart->getEqpPtrs()[groupEqpIdx];
				if(pEqp->getLength() == 0)
				{
					pEqp->setStart(pEqp->getStart() + mayMoveThis);
					pEqp->setStartX(x);
					pEqp->setEndX(x);
					pEqp->setStartY(y);
					pEqp->setEndY(y);
				}
				else
				{
					float endX, endY;
					if(coordInLine(pPart->getName(), NULL, VacType::None, pEqp->getStart() + pEqp->getLength() + mayMoveThis, true, endX, endY))
					{
						pEqp->setStart(pEqp->getStart() + mayMoveThis);
						pEqp->setStartX(x);
						pEqp->setEndX(endX);
						pEqp->setStartY(y);
						pEqp->setEndY(endY);
					}
					else
					{
						// Not very elegant, but this shall not happen
						pEqp->setStart(pEqp->getStart() + mayMoveThis);
						pEqp->setStartX(x);
						pEqp->setEndX(x);
						pEqp->setStartY(y);
						pEqp->setEndY(y);
					}
				}
				// Move to next device in group
				if(!nextDeviceIndices(groupPartIdx, groupEqpIdx, false))
				{
					break;	// This shall not happen
				}
				pNextPart = pParts->at(groupPartIdx);
				if((groupPartIdx == nextPartIdx) && (groupEqpIdx == nextEqpIdx))
				{
					break;
				}
			}
		}
	}
	else
	{
		mayMoveThis = 0;
	}

	if(mayMoveThis >= delta)
	{
		return delta;
	}

	return mayMoveThis;
}


/*
** FUNCTION
**		Move end of sector in backward direction by given distance.
**		If sector is not long enough to move it's end by required distance -
**		try to move end of previous sector (recursively).
**
** PARAMETERS
**		partIdx				- Index of beam line part where first device of this sector is located
**		eqpIdx				- Index of first device of this sector in it' line part
**		startFixedPartIdx	- Index of beam line part where motion shall stop
**		startFixedEqpIdx	- Index of device where motion shall stop in it's line part
**		delta					- Required distance to move sector end
**		minLength			- Minimum allowed length of sector
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void BeamLine::makeSectorShorterBackward(int partIdx, int eqpIdx, int startFixedPartIdx, int startFixedEqpIdx, float delta, int minLength)
{
	if((partIdx == startFixedPartIdx) && (eqpIdx == startFixedEqpIdx))
	{
		return;
	}
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp *pValve = pPart->getEqpPtrs()[eqpIdx];

	// Check if this sector may be made shorter. First find previous valve before end of sector,
	// search shall not go behind start fixed device
	int prevValvePartIdx = partIdx;
	int prevValveEqpIdx = eqpIdx;
	Eqp *pPrevValve = findPreviousValve(partIdx, eqpIdx, prevValvePartIdx, prevValveEqpIdx);
	if((prevValvePartIdx != startFixedPartIdx) || (prevValveEqpIdx <= startFixedEqpIdx))
	{
		prevValvePartIdx = startFixedPartIdx;
		prevValveEqpIdx = startFixedEqpIdx;
	}
	if(prevValvePartIdx < 0)
	{
		prevValvePartIdx = 0;
		pPart = pParts->at(prevValvePartIdx);
		prevValveEqpIdx = 0;
	}
	pPart = pParts->at(prevValvePartIdx);
	pPrevValve = pPart->getEqpPtrs()[prevValveEqpIdx];

	// May this sector be made shorter?
	float sectorLength = pValve->getStart() - pPrevValve->getStart();
	float mayShortenThis = 0;
	if(sectorLength > (minLength + delta))
	{
		// Check if there are enough holes between elements to move devices
		mayShortenThis = totalHolesInArea(prevValvePartIdx, prevValveEqpIdx, partIdx, eqpIdx);
	}

	if(mayShortenThis < delta)
	{
		makeSectorShorterBackward(prevValvePartIdx, prevValveEqpIdx, startFixedPartIdx, startFixedEqpIdx, delta - mayShortenThis, minLength);
		// Check if there are enough holes between elements to move devices
		mayShortenThis = totalHolesInArea(prevValvePartIdx, prevValveEqpIdx, partIdx, eqpIdx);
	}
	if(mayShortenThis > 0)
	{
		moveEqpBackward(partIdx, eqpIdx, prevValvePartIdx, prevValveEqpIdx, mayShortenThis > delta ? delta : mayShortenThis);
	}
}

/*
** FUNCTION
**		Move given device backward by given distance
**
** PARAMETERS
**		partIdx				- Index of beam line part where device to be moved is located
**		eqpIdx				- Index of device to be moved in it' line part
**		startFixedPartIdx	- Index of beam line part where motion shall stop
**		startFixedEqpIdx	- Index of device where motion shall stop in it's line part
**		delta					- Required distance to move device
**
** RETURNS
**		How far this device was actually moved
**
** CAUTIONS
**		None
*/
float BeamLine::moveEqpBackward(int partIdx, int eqpIdx, int startPartIdx, int startEqpIdx, float delta)
{
	if((partIdx == startPartIdx) && (eqpIdx == startEqpIdx))
	{
		return 0;	// reached end device
	}
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp *pEqp = pPart->getEqpPtrs()[eqpIdx];

	float result = 0;
	switch(pEqp->getType())
	{
	case EqpType::Passive:
		result = moveEqpBackwardPassive(partIdx, eqpIdx, startPartIdx, startEqpIdx, delta);
		break;
	case EqpType::VacDevice:
		result = moveEqpBackwardActive(partIdx, eqpIdx, startPartIdx, startEqpIdx, delta);
		break;
	default:
		result = moveEqpBackwardActive(partIdx, eqpIdx, startPartIdx, startEqpIdx, delta);
		break;
	}
	return result;
}

/*
** FUNCTION
**		Move active device by given distance in backward direction
**
** PARAMETERS
**		partIdx			- Index of beam line where the equipment to move is located
**		eqpIdx			- Index of equipment to move in it's beam line part
**		startPartIdx	- Index of beam line part where the very first device of area being processed is located
**		startEqpIdx		- Index of the very first device of area being processed in it's beam line part
**		delta			- Required distance increase
**
** RETURNS
**		How far this device was actually moved
**
** CAUTIONS
**		None
*/
float BeamLine::moveEqpBackwardActive(int partIdx, int eqpIdx, int startPartIdx, int startEqpIdx, float delta)
{
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp *pEqp = pPart->getEqpPtrs()[eqpIdx];

	// Device can be part of 'group' (several devices at the same location).
	// It is necessary to move group as a whole
	bool eqpInGroup = false;
	int prevPartIdx = partIdx;
	int prevEqpIdx = eqpIdx;
	Eqp *pPrevEqp = NULL;
	BeamLinePart *pPrevPart = pPart;
	while(true)
	{
		if(!previousDeviceIndices(prevPartIdx, prevEqpIdx, false))
		{
			return 0;
		}
		pPrevPart = pParts->at(prevPartIdx);
		pPrevEqp = pPrevPart->getEqpPtrs()[prevEqpIdx];
		if((prevPartIdx != startPartIdx) || (prevEqpIdx <= startEqpIdx))
		{
			break;
		}
		if(pPrevEqp->getStart() < pEqp->getStart())
		{
			break;
		}
		eqpInGroup = true;
	}

	// How far can we move this device without moving next one
	// Here another precaution shall be taken: if this device is not within long passive device -
	// it shall not move so far that it will become inside
	int prevPasPartIdx = partIdx, prevPasEqpIdx = eqpIdx;
	Eqp *pPrevPasEqp = NULL;
	while(true)
	{
		if(!previousDeviceIndices(prevPasPartIdx, prevPasEqpIdx, false))
		{
			return 0;
		}
		pPrevPart = pParts->at(prevPasPartIdx);
		pPrevPasEqp = pPrevPart->getEqpPtrs()[prevPasEqpIdx];
		if((prevPasPartIdx != startPartIdx) || (prevPasEqpIdx <= startEqpIdx))
		{
			break;
		}
		if(pPrevPasEqp->getLength() > 0)
		{
			break;
		}
	}
	float mayMoveThis = pEqp->getStart() - (pPrevEqp->getStart() + pPrevEqp->getLength()) - MIN_HOLE;
	if(pPrevPasEqp)
	{
		if((pPrevPasEqp->getStart() + pPrevPasEqp->getLength()) < pEqp->getStart())	// This device is NOT within previous passive
		{
			float mayMoveToPas = pEqp->getStart() - (pPrevPasEqp->getStart() + pPrevPasEqp->getLength()) - MIN_HOLE;
			if(mayMoveToPas < mayMoveThis)
			{
				mayMoveThis = mayMoveToPas;
			}
		}
	}


	if(mayMoveThis >= delta)
	{
		mayMoveThis = delta;
	}
	else
	{
		// Moving just this device is not enough, let's try to move next one
		if(mayMoveThis < 0)
		{
			mayMoveThis = 0;
		}
		float toMovePrevious = delta - mayMoveThis;
		mayMoveThis += moveEqpBackward(prevPartIdx, prevEqpIdx, startPartIdx, startEqpIdx, toMovePrevious);
	}
	if(mayMoveThis > 0)
	{
		float x, y;
		if(coordInLine(pPart->getName(), NULL, VacType::None, pEqp->getStart() - mayMoveThis, true, x, y))
		{
			if(eqpInGroup)
			{
				int groupPartIdx = partIdx;
				int groupEqpIdx = eqpIdx;
				pPrevPart = pPart;
				while(true)
				{
					// Apply changes to device
					pEqp = pPrevPart->getEqpPtrs()[groupEqpIdx];
					if(pEqp->getLength() == 0)
					{
						pEqp->setStart(pEqp->getStart() - mayMoveThis);
						pEqp->setStartX(x);
						pEqp->setEndX(x);
						pEqp->setStartY(y);
						pEqp->setEndY(y);
					}
					else
					{
						float endX, endY;
						if(coordInLine(pPart->getName(), NULL, VacType::None, pEqp->getStart() + pEqp->getLength() - mayMoveThis, true, endX, endY))
						{
							pEqp->setStart(pEqp->getStart() - mayMoveThis);
							pEqp->setStartX(x);
							pEqp->setEndX(endX);
							pEqp->setStartY(y);
							pEqp->setEndY(endY);
						}
						else
						{
							// Not very elegant, but this shall not happen
							pEqp->setStart(pEqp->getStart() - mayMoveThis);
							pEqp->setStartX(x);
							pEqp->setEndX(x);
							pEqp->setStartY(y);
							pEqp->setEndY(y);
						}
					}

					// Move to next device in group
					if(!previousDeviceIndices(groupPartIdx, groupEqpIdx, false))
					{
						break;	// This shall not happen
					}
					pPrevPart = pParts->at(groupPartIdx);
					if((groupPartIdx == prevPartIdx) && (groupEqpIdx == prevEqpIdx))
					{
						break;
					}
				}
			}
			else
			{
				if(pEqp->getLength() == 0)
				{
					pEqp->setStart(pEqp->getStart() - mayMoveThis);
					pEqp->setStartX(x);
					pEqp->setEndX(x);
					pEqp->setStartY(y);
					pEqp->setEndY(y);
				}
				else
				{
					float endX, endY;
					if(coordInLine(pPart->getName(), NULL, VacType::None, pEqp->getStart() + pEqp->getLength() - mayMoveThis, true, endX, endY))
					{
						pEqp->setStart(pEqp->getStart() - mayMoveThis);
						pEqp->setStartX(x);
						pEqp->setEndX(endX);
						pEqp->setStartY(y);
						pEqp->setEndY(endY);
					}
					else
					{
						mayMoveThis = 0;
					}
				}
			}
		}
		else
		{
			mayMoveThis = 0;
		}
	}
	else
	{
		mayMoveThis = 0;
	}

	if(mayMoveThis >= delta)
	{
		return delta;
	}

	return mayMoveThis;
}

/*
** FUNCTION
**		Move passive device by specified distance in backward direction
**
** PARAMETERS
**		partIdx			- Index of beam line where the equipment to move is located
**		eqpIdx			- Index of equipment to move in it's beam line part
**		startPartIdx	- Index of beam line part where the very first device of area being processed is located
**		startEqpIdx		- Index of the very first device of area being processed in it's beam line part
**		delta			- Required distance increase
**
** RETURNS
**		How far this device was actually moved
**
** CAUTIONS
**		None
*/
float BeamLine::moveEqpBackwardPassive(int partIdx, int eqpIdx, int startPartIdx, int startEqpIdx, float delta)
{
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp *pEqp = pPart->getEqpPtrs()[eqpIdx];

	// No special care about active devices which are within this one: they shall care themselves
	int prevPartIdx = partIdx;
	int prevEqpIdx = eqpIdx;
	if(!previousDeviceIndices(prevPartIdx, prevEqpIdx, false))
	{
		return 0;
	}
	BeamLinePart *pPrevPart = pParts->at(prevPartIdx);
	Eqp *pPrevEqp = pPrevPart->getEqpPtrs()[prevEqpIdx];

	// How far can we move this device without moving previous one
	// Here another precaution shall be taken: this device shall not move so far that it will become
	// inside another long passive
	int prevPasPartIdx = partIdx, prevPasEqpIdx = eqpIdx;
	Eqp *pPrevPasEqp = NULL;
	while(true)
	{
		if(!previousDeviceIndices(prevPasPartIdx, prevPasEqpIdx, false))
		{
			return 0;
		}
		pPrevPart = pParts->at(prevPasPartIdx);
		pPrevPasEqp = pPrevPart->getEqpPtrs()[prevPasEqpIdx];
		if((prevPasPartIdx != startPartIdx) || (prevPasEqpIdx <= startEqpIdx))
		{
			break;
		}
		if(pPrevPasEqp->getLength() > 0)
		{
			break;
		}
	}
	float mayMoveThis = pEqp->getStart() - (pPrevEqp->getStart() + pPrevEqp->getLength()) - MIN_HOLE;
	if(pPrevPasEqp)
	{
		if((pPrevPasEqp->getStart() + pPrevPasEqp->getLength()) < pEqp->getStart())	// This device is NOT within previous passive
		{
			float mayMoveToPas = pEqp->getStart() - (pPrevPasEqp->getStart() + pPrevPasEqp->getLength()) - MIN_HOLE;
			if(mayMoveToPas < mayMoveThis)
			{
				mayMoveThis = mayMoveToPas;
			}
		}
	}

	if(mayMoveThis >= delta)
	{
		mayMoveThis = delta;
	}
	else
	{
		// Moving just this device is not enough, let's try to move next one
		mayMoveThis += moveEqpBackward(prevPartIdx, prevEqpIdx, startPartIdx, startEqpIdx, delta - mayMoveThis);
	}
	if(mayMoveThis > 0)
	{
		float x, y;
		if(coordInLine(pPart->getName(), NULL, VacType::None, pEqp->getStart() - mayMoveThis, true, x, y))
		{
			if(pEqp->getLength() == 0)
			{
				pEqp->setStart(pEqp->getStart() - mayMoveThis);
				pEqp->setStartX(x);
				pEqp->setEndX(x);
				pEqp->setStartY(y);
				pEqp->setEndY(y);
			}
			else
			{
				float endX, endY;
				if(coordInLine(pPart->getName(), NULL, VacType::None, pEqp->getStart() + pEqp->getLength() - mayMoveThis, true, endX, endY))
				{
					pEqp->setStart(pEqp->getStart() - mayMoveThis);
					pEqp->setStartX(x);
					pEqp->setEndX(endX);
					pEqp->setStartY(y);
					pEqp->setEndY(endY);
				}
				else
				{
					// Not very elegant, but this shall not happen
					pEqp->setStart(pEqp->getStart() - mayMoveThis);
					pEqp->setStartX(x);
					pEqp->setEndX(x);
					pEqp->setStartY(y);
					pEqp->setEndY(y);
				}
			}
		}
	}
	else
	{
		mayMoveThis = 0;
	}

	if(mayMoveThis >= delta)
	{
		return delta;
	}

	return mayMoveThis;
}



/*
** FUNCTION
**		Find total distance all short sectors in area between two fixed points
**		shall be extended. Find out also how much holes can be found in
**		long sectors of this area to give this space to short sectors.
**
** PARAMETERS
**		minLength		- Minimum allowed sector length
**		startPartIdx	- Index of beam line part where start of area is located
**		startEqpIdx		- Index of area start device in it's line part
**		endPartIdx		- Index of beam line part where end of area is located
**		endEqpIdx		- Index of area end device in it's line part
**		totalHoles		- Total distancee that can be borrowed from long sectors
**								is returned here
**
** RETURNS
**		Total distance that shall be given to short sectors in this area
**
** CAUTIONS
**		None
**
*/
float BeamLine::totalSectorsToExtend(int minLength, int startPartIdx, int startEqpIdx, int endPartIdx, int endEqpIdx, float &totalHoles)
{
	float result = 0;
	totalHoles = 0;

	int partIdx = startPartIdx, eqpIdx = startEqpIdx;
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp *pValve = pPart->getEqpPtrs()[eqpIdx];
	if(pValve->getFunctionalType() != FunctionalType::VV)	// This is not a valve, try to use previous valve (if any)
	{
		int prevPartIdx, prevEqpIdx;
		pValve = findPreviousValve(partIdx, eqpIdx, prevPartIdx, prevEqpIdx);
		if(!pValve)
		{
			pValve = pPart->getEqpPtrs()[eqpIdx];
		}
	}
	while(true)
	{
		int nextPartIdx, nextEqpIdx;
		Eqp *pNextValve = findNextValve(partIdx, eqpIdx, nextPartIdx, nextEqpIdx);
		if(!pNextValve)
		{
			break;
		}
		float sectorLength = pNextValve->getStart() - pValve->getStart();
		if(sectorLength < minLength)
		{
			result += (float)minLength - sectorLength;
		}
		else if(sectorLength > minLength)
		{
			if((nextPartIdx != endPartIdx) || (nextEqpIdx > endEqpIdx))
			{
				// Even if next valve is after end of area - calculate up to end of area
				nextPartIdx = endPartIdx;
				nextEqpIdx = endEqpIdx;
				pPart = pParts->at(nextPartIdx);
			}
			float total = totalHolesInArea(partIdx, eqpIdx, nextPartIdx, nextEqpIdx);
			if(total > 0)
			{
				if((sectorLength - minLength) < total)
				{
					totalHoles += sectorLength - minLength;
				}
				else
				{
					totalHoles += total;
				}
			}
		}
		partIdx = nextPartIdx;
		eqpIdx = nextEqpIdx;
		pValve = pNextValve;
		if((partIdx == endPartIdx) && (eqpIdx == endEqpIdx))
		{
			break;
		}
	}
	return result;
}

/*
** FUNCTION
**		Find total length of all 'holes' in area between two devices,
**		i.e. how much shorter this area can be done in principle
**
** PARAMETERS
**		startPartIdx	- Index of beam line part where start of area is located
**		startEqpIdx		- Index of area start device in it's line part
**		endPartIdx		- Index of beam line part where end of area is located
**		endEqpIdx		- Index of area end device in it's line part
**
** RETURNS
**		Total holes in area
**
** CAUTIONS
**		None
**
*/
float BeamLine::totalHolesInArea(int startPartIdx, int startEqpIdx, int endPartIdx, int endEqpIdx)
{
	float result = 0;
	BeamLinePart *pPart = pParts->at(startPartIdx);
	Eqp *pEqp = pPart->getEqpPtrs()[startEqpIdx];
	int partIdx = startPartIdx, eqpIdx = startEqpIdx;
	float lastEqpEnd = pEqp->getStart() + pEqp->getLength();
	while(true)
	{
		if(!nextDeviceIndices(partIdx, eqpIdx, false))
		{
			break;
		}
		pPart = pParts->at(partIdx);
		pEqp = pPart->getEqpPtrs()[eqpIdx];
		if(pEqp->getStart() > (lastEqpEnd + MIN_HOLE))
		{
			result += pEqp->getStart() - (lastEqpEnd + MIN_HOLE);
		}
		if(pEqp->getStart() > lastEqpEnd)
		{
			lastEqpEnd = pEqp->getStart() + pEqp->getLength();
		}
		if((partIdx != endPartIdx) || (eqpIdx >= endEqpIdx))
		{
			break;
		}
	}
	return result;
}


/*
** FUNCTION
**		Find next device that can not be moved
**
** PARAMETERS
**		partIdx	- Index of beam line part where previous fixed device was found, can be -1 if no previous
**		eqpIdx	- Index of previous fixed device in it' line part, can be -1 if no previous
**
** RETURNS
**		true	- if next fixed device was found;
**		false	- otherwise
**
** CAUTIONS
**		None
**
*/
bool BeamLine::nextFixedEqp(int &partIdx, int &eqpIdx)
{
	if(partIdx < 0)
	{
		partIdx = 0;
		eqpIdx = 0;
	}
	while(nextDeviceIndices(partIdx, eqpIdx, false))
	{
		BeamLinePart *pPart = pParts->at(partIdx);
		Eqp *pEqp = pPart->getEqpPtrs()[eqpIdx];
		switch(pEqp->getType())
		{
		case EqpType::LineStart:
		case EqpType::LineEnd:
		case EqpType::OtherLineStart:
		case EqpType::OtherLineEnd:
			return true;
			break;
		case EqpType::VacDevice:	// Add sector borders between 2 main parts (SPS specific)
			if(!pEqp->isSectorBorder())
			{
				break;
			}
			if(pEqp->getSectorBefore()->hasCommonMainPart(pEqp->getSectorAfter()))
			{
				break;
			}
			return true;
			break;
		default:
			break;
		}
	}
	partIdx = -1;
	eqpIdx = -1;
	return false;
}

/*
** FUNCTION
**		Find part and device indices for device after given one
**
** PARAMETERS
**		partIdx			- Index of beam line part where start device is located
**		eqpIdx			- Index of start device in it's beam line part
**		turnAround		- true if it is allowed to turn from end to start on
**							circular line
**
** RETURNS
**		true	- if next device has been found;
**		false	- otherwise
**
** CAUTIONS
**		None
**
*/
bool BeamLine::nextDeviceIndices(int &partIdx, int &eqpIdx, bool turnAround)
{
	eqpIdx++;
	BeamLinePart *pPart = pParts->at(partIdx);
	if(eqpIdx >= pPart->getNEqpPtrs())
	{
		partIdx++;
		if(partIdx >= (int)pParts->count())
		{
			if(turnAround && circle)
			{
				partIdx = 0;
			}
			else
			{
				return false;
			}
		}
		eqpIdx = 0;
	}
	return true;
}

/*
** FUNCTION
**		Find part and device indices for device before given one
**
** PARAMETERS
**		partIdx			- Index of beam line part where start device is located
**		eqpIdx			- Index of start device in it's beam line part
**		turnAround		- true if it is allowed to turn from end to start on
**							circular line
**
** RETURNS
**		true	- if next device has been found;
**		false	- otherwise
**
** CAUTIONS
**		None
**
*/
bool BeamLine::previousDeviceIndices(int &partIdx, int &eqpIdx, bool turnAround)
{
	eqpIdx--;
	BeamLinePart *pPart = pParts->at(partIdx);
	if(eqpIdx < 0)
	{
		partIdx--;
		if(partIdx < 0)
		{
			if(turnAround && circle)
			{
				partIdx = pParts->count() - 1;
			}
			else
			{
				return false;
			}
		}
		pPart = pParts->at(partIdx);
		eqpIdx = pPart->getNEqpPtrs() - 1;
	}
	return true;
}

/*
** FUNCTION
**		Find device in this line which is sector border and is located
**		after given device
**
** PARAMETERS
**		startPartIdx	- Index of beam line part where start device is located
**		startEqpIdx		- Index of start device in beam line part
**		partIdx			- Index of beam line part for next valve is returned here
**		eqpIdx			- Index of next valve in next beam line part is returned here
**
** RETURNS
**		Pointer to next sector border; or
**		NULL if next sector border is not found
**
** CAUTIONS
**		None
*/
Eqp *BeamLine::findNextValve(int startPartIdx, int startEqpIdx, int &partIdx, int &eqpIdx)
{
	partIdx = startPartIdx;
	eqpIdx = startEqpIdx;
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	while(true)
	{
		eqpIdx++;
		if(eqpIdx >= nEqpPtrs)
		{
			/* L.Kopylov 26.01.2010 do not turn over zero position
			eqpIdx = 0;
			partIdx++;
			if(partIdx >= nParts)
			{
				partIdx = 0;
			}
			pPart = &parts[partIdx];
			eqpPtrs = pPart->eqpPtrs;
			nEqpPtrs = pPart->nEqpPtrs;
			*/
			eqpIdx = nEqpPtrs - 1;
			if(eqpIdx == startEqpIdx)
			{
				return NULL;
			}
			return eqpPtrs[eqpIdx];	// Even if this is not a valve
		}
		if((partIdx == startPartIdx) && (eqpIdx == startEqpIdx))
		{
			break;
		}
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(pEqp->getFunctionalType() == FunctionalType::VV)
		{
			return pEqp;
		}
	}
	return NULL;
}

/*
** FUNCTION
**		Find device in this line which is sector border and is located
**		before given device
**
** PARAMETERS
**		startPartIdx	- Index of beam line part where start device is located
**		startEqpIdx		- Index of start device in beam line part
**		partIdx			- Index of beam line part for previous valve is returned here
**		eqpIdx			- Index of previous valve in next beam line part is returned here
**
** RETURNS
**		Pointer to next sector border; or
**		NULL if next sector border is not found
**
** CAUTIONS
**		None
*/
Eqp *BeamLine::findPreviousValve(int startPartIdx, int startEqpIdx, int &partIdx, int &eqpIdx)
{
	partIdx = startPartIdx;
	eqpIdx = startEqpIdx;
	BeamLinePart *pPart = pParts->at(partIdx);
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	while(true)
	{
		eqpIdx--;
		if(eqpIdx < 0)
		{
			partIdx--;
			if(partIdx < 0)
			{
				partIdx = pParts->count() - 1;
			}
			pPart = pParts->at(partIdx);
			eqpPtrs = pPart->getEqpPtrs();
			nEqpPtrs = pPart->getNEqpPtrs();
			eqpIdx = nEqpPtrs - 1;
		}
		if((partIdx == startPartIdx) && (eqpIdx == startEqpIdx))
		{
			break;
		}
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(pEqp->getFunctionalType() == FunctionalType::VV)
		{
			return pEqp;
		}
	}
	return NULL;
}





/*
**	FUNCTION
**		Write beam line content to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**		lineIdx	- Index of this line
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BeamLine::dump(FILE *pFile, int lineIdx)
{
	if(!DebugCtl::isData())
	{
		return;
	}
	fprintf(pFile, "############ Line %d <%s> %s%s%s%s\n", lineIdx, name, (circle ? "CIRCLE" : ""),
		(virtualLine ? " VIRTUAL" : ""), (hidden ? " HIDDEN" : ""), (notUsed ? " NOT_USED" : ""));
	fprintf(pFile, "  turnAngle %f, resizeCoef %f\n", turnAngle, resizeCoef);
	fprintf(pFile, "  allowedVacType = %X\n", allowedVacType);
	fprintf(pFile, "  Limits: (%f, %f) - (%f, %f)\n", minX, minY, maxX, maxY);
	fprintf(pFile, "  IsClockWise = %d\n", isClockWise());
	fprintf(pFile, "   total calls %d before %d after %d\n", nCalls, nBefore, nAfter);
	if(startLine)
	{
		fprintf(pFile, "  Start <%s> at %f type %d", startLine, startCoord, startType);
	}
	if(endLine)
	{
		fprintf(pFile, "  End <%s> atr %f type %d", endLine, endCoord, endType);
	}
	fprintf(pFile, "  Consists of %d parts:\n", pParts->count());
	for(int idx = 0 ; idx < pParts->count() ; idx++)
	{
		BeamLinePart *pPart = pParts->at(idx);
		pPart->dump(pFile, idx);
	}
}