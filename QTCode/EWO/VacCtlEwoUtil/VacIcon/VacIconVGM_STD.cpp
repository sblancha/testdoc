//	Implementation of VacIconVGM_STD class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGM_STD.h"

#include <qpainter.h>

// We only need to implement drawSpecific() method

// Draw 'membrane' symbol in the center of icon
void VacIconVGM_STD::drawSpecific(QPainter &painter)
{
	painter.setPen(Qt::black);
	painter.drawLine(1, 10, 20, 10);
}
