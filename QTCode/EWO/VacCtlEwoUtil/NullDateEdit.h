#ifndef NULLDATEEDIT_H
#define	NULLDATEEDIT_H

// Attempt to modify QDateEdit to allow for empty dates

#include <qdatetimeedit.h>

class NullDateEdit : public QDateEdit
{
	Q_OBJECT

public:
	NullDateEdit(QWidget *parent = 0, const char *name = 0)
		: QDateEdit(parent, name)
	{
	}
	NullDateEdit(const QDate &date, QWidget *parent = 0, const char *name = 0)
		: QDateEdit(date, parent, name)
	{
	}

protected:
	virtual void fix(void);
};

#endif	// NULLDATEEDIT_H
