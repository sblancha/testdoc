#ifndef	VACICONVGR_STD_H
#define	VACICONVGR_STD_H

//	Icon for VG_STD - VGR

#include "VacIconVG_STD.h"

class VACCTLEWOUTIL_EXPORT VacIconVGR_STD : public VacIconVG_STD
{
public:
	VacIconVGR_STD(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG_STD(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGR_STD_H
