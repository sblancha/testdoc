//	Implementation of VacIconVV_AO class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVV_AO.h"

#include "EqpVV_AO.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OPEN				0,255,0
#define COLOR_CLOSED			255,0,0
#define	COLOR_ERROR				255,102,255

// Colors for control and interlocks
#define COLOR_GREEN				0,255,0
#define COLOR_YELLOW			255,255,0
#define COLOR_RED				255,0,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_MANUAL		(0x04000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_OPEN		(0x02000000)
#define	RR1_CLOSED		(0x01000000)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVV_AO::VacIconVV_AO(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 27);
}

VacIconVV_AO::~VacIconVV_AO()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVV_AO::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Left:
	case VacIconContainer::Right:
		return VacIconGeometry(27, 21, 10, 15, 0);
	default:
		break;
	}
	return VacIconGeometry(21, 27, 10, 10, 0);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVV_AO::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(21);
		size.setHeight(27);
		break;
	default:
		size.setWidth(27);
		size.setHeight(21);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
}


/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVV_AO::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVV_AO"), "VacIconVV_AO::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVV_AO *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVV_AO::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVV_AO::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("SetPointR");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVV_AO::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVV_AO::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVV_AO::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
	case VacIconContainer::Down:
		drawHorizontal(painter);
		break;
	case VacIconContainer::Left:	// 90 degrees
	case VacIconContainer::Right:	// 90 degrees clockwise
		drawVertical(painter);
		break;
	}
}

void VacIconVV_AO::drawHorizontal(QPainter &painter)
{
	// Draw vacuum pipe - two short lines
	painter.fillRect(0, 14, 5, 3, pipeColor);
	painter.fillRect(18, 14, 5, 3, pipeColor);
	
	// Draw valve image
	setPainterForControl(painter);

	// 1) Rectangle connecting big rectangle on top with valve image
	QPolygon points(4);
	points.setPoint(0, 8, 9);
	points.setPoint(1, 12, 9);
	points.setPoint(2, 12, 15);
	points.setPoint(3, 8, 15);
	painter.drawPolygon(points);

	// 2) big rectangle on top
	points.setPoint(0, 6, 6);
	points.setPoint(1, 14, 6);
	points.setPoint(2, 14, 9);
	points.setPoint(3, 6, 9);
	painter.drawPolygon(points);

	// 3) Valve image
	int openPercent = setPainterForMainImage(painter);

	points.setPoint(0, 1, 6);
	points.setPoint(1, 19, 25);
	points.setPoint(2, 19, 6);
	points.setPoint(3, 1, 25);
	painter.drawPolygon(points);

	// 4) 'partially open' part of image
	if(openPercent)
	{
		int openPart = 2;
		if(openPercent < 200)
		{
			openPart = 2;
		}
		else if(openPercent < 500)
		{
			openPart = 3;
		}
		else if(openPercent < 800)
		{
			openPart = 4;
		}
		else
		{
			openPart = 5;
		}
		points.setPoint(0, 1, 15 - openPart);
		points.setPoint(1, 19, 15 + openPart);
		points.setPoint(2, 19, 15 - openPart);
		points.setPoint(3, 1, 15 + openPart);
		painter.setPen(Qt::NoPen);
		painter.setBrush(QColor(COLOR_OPEN));
		painter.drawPolygon(points);
	}

	// 5) Arrow symbol on top of valve
	QPen arrowPen(Qt::black, 2);
	painter.setPen(arrowPen);
	painter.drawLine(6, 25, 14, 6);
	painter.drawLine(14, 6, 12, 8);
	painter.drawLine(14, 6, 14, 10);

	
	drawSymbols(painter, false);
}

void VacIconVV_AO::drawVertical(QPainter &painter)
{
	// Draw vacuum pipe - two short lines
	painter.fillRect(9, 0, 3, 5, pipeColor);
	painter.fillRect(9, 19, 3, 5, pipeColor);
	
	// Draw valve image
	setPainterForControl(painter);

	// 1) Rectangle connecting big rectangle on top with valve image
	QPolygon points(4);
	points.setPoint(0, 10, 8);
	points.setPoint(1, 16, 8);
	points.setPoint(2, 16, 12);
	points.setPoint(3, 10, 12);
	painter.drawPolygon(points);

	// 2) big rectangle on top
	points.setPoint(0, 16, 6);
	points.setPoint(1, 19, 6);
	points.setPoint(2, 19, 14);
	points.setPoint(3, 16, 12);
	painter.drawPolygon(points);

	// 3) Valve image
	int openPercent = setPainterForMainImage(painter);

	points.setPoint(0, 1, 1);
	points.setPoint(1, 19, 19);
	points.setPoint(2, 1, 19);
	points.setPoint(3, 19, 1);
	painter.drawPolygon(points);

	// 4) 'partially open' part of image
	if(openPercent)
	{
		int openPart = 2;
		if(openPercent < 200)
		{
			openPart = 2;
		}
		else if(openPercent < 500)
		{
			openPart = 3;
		}
		else if(openPercent < 800)
		{
			openPart = 4;
		}
		else
		{
			openPart = 5;
		}
		points.setPoint(0, 10 - openPart, 1);
		points.setPoint(1, 10 + openPart, 19);
		points.setPoint(2, 10 - openPart, 19);
		points.setPoint(3, 10 + openPart, 1);
		painter.setPen(Qt::NoPen);
		painter.setBrush(QColor(COLOR_OPEN));
		painter.drawPolygon(points);
	}

	// 5) Arrow symbol on top of valve
	QPen arrowPen(Qt::black, 2);
	painter.setPen(arrowPen);
	painter.drawLine(1, 6, 19, 14);
	painter.drawLine(19, 14, 15, 14);
	painter.drawLine(19, 14, 17, 7);

	
	drawSymbols(painter, true);
}

void VacIconVV_AO::drawSymbols(QPainter &painter, bool vertical)
{
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			return;
		default:
			if(pEqp->isPlcAlarm(mode))
			{
				return;	// Invalid
			}
			unsigned rr1 = pEqp->getRR1(mode);
			if(!(rr1 & RR1_VALID))
			{
				return;	// Invalid
			}

			if(rr1 & RR1_MANUAL)
			{
				drawManual(painter, vertical);
			}
			break;
		}
	}
	else
	{
		drawManual(painter, vertical);
	}
}

void VacIconVV_AO::drawManual(QPainter &painter, bool vertical)
{
	painter.setPen(Qt::black);
	if(vertical)
	{
		painter.drawLine(21, 1, 21, 5);
		painter.drawLine(25, 1, 25, 5);
		painter.drawLine(23, 3, 23, 4);
		painter.drawLine(22, 2, 22, 2);
		painter.drawLine(24, 2, 24, 2);
	}
	else
	{
		painter.drawLine(1, 1, 1, 5);
		painter.drawLine(5, 1, 5, 5);
		painter.drawLine(3, 3, 3, 4);
		painter.drawLine(2, 2, 2, 2);
		painter.drawLine(4, 2, 4, 2);
	}
}


void VacIconVV_AO::setPainterForControl(QPainter &painter)
{
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
			color.setRgb(COLOR_NOT_CONTROL);
			break;
		case Eqp::NotConnected:
			color.setRgb(COLOR_NOT_CONNECTED);
			break;
		default:
			if(!pEqp->isPlcAlarm(mode))
			{
				unsigned rr1 = pEqp->getRR1(mode);
				if(rr1 & RR1_VALID)
				{
					if(rr1 & RR1_ERROR)
					{
						color.setRgb(COLOR_RED);
					}
					else if(rr1 & RR1_WARNING)
					{
						color.setRgb(COLOR_YELLOW);
					}
					else
					{
						color.setRgb(COLOR_GREEN);
					}
				}
			}
			break;
		}
	}
	painter.setBrush(color);
	painter.setPen(Qt::black);
}

int VacIconVV_AO::setPainterForMainImage(QPainter &painter)
{
	int	openPercent = 0;

	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
			color.setRgb(COLOR_NOT_CONTROL);
			break;
		case Eqp::NotConnected:
			color.setRgb(COLOR_NOT_CONNECTED);
			break;
		default:
			if(!pEqp->isPlcAlarm(mode))
			{
				unsigned rr1 = pEqp->getRR1(mode);
				if(rr1 & RR1_VALID)
				{
					if(rr1 & RR1_OPEN)
					{
						if(rr1 & RR1_CLOSED)
						{
							color.setRgb(COLOR_ERROR);
						}
						else
						{
							color.setRgb(COLOR_OPEN);
						}
					}
					else if(rr1 & RR1_CLOSED)
					{
						color.setRgb(COLOR_CLOSED);
					}
					else
					{
						// Either partially open, or really undefined
						openPercent = pEqp->getSetPoint(mode);
						if((50 < openPercent) && (openPercent < 950))
						{
							color.setRgb(COLOR_CLOSED);
						}
						else
						{
							openPercent = 0;
							color.setRgb(COLOR_UNDEFINED);
						}
					}
				}
			}
			break;
		}
	}
	QBrush brush(color);
	painter.setBrush(brush);
	painter.setPen(Qt::black);
	return openPercent;
}
