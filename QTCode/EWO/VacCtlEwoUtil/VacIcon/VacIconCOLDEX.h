#ifndef	VACICONCOLDEX_H
#define	VACICONCOLDEX_H

// Icon for COLDEX

#include "VacIcon.h"

class EqpCOLDEX;

class VACCTLEWOUTIL_EXPORT VacIconCOLDEX : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconCOLDEX(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconCOLDEX();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpCOLDEX *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void setMainColor(QPainter &painter);
};

#endif	// VACICONCOLDEX_H
