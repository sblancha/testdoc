#ifndef _VACCTLHISTORYDATAPVSS_H_
#define _VACCTLHISTORYDATAPVSS_H_

# ifdef _WIN32
#   define  PVSS_EXPORT  __declspec(dllexport)
# else
#   define  PVSS_EXPORT
# endif

#include "VacCtlPvssUtil.h"

class VacCtlStateArchive;

#include <QVariant>

class AnyTypeVar;

class VacCtlHistoryDataPvss : public VacCtlPvssUtil
{
public:
	VacCtlHistoryDataPvss(BaseExternHdl *nextHdl, PVSSulong funcCount, FunctionListRec fnList[])
		: VacCtlPvssUtil(nextHdl, funcCount, fnList) {}

	virtual const Variable *execute(ExecuteParamRec &param);

protected:
	int processProcess(ExecuteParamRec &param);
	int processSetValues(ExecuteParamRec &param);
	int processGetAllTs(ExecuteParamRec &param);
	int processGetValues(ExecuteParamRec &param);
	int processGetPreviousTs(ExecuteParamRec &param);
	int processGetNextTs(ExecuteParamRec &param);
	int processNeighbourIndexes(ExecuteParamRec &param);
	int processClear(ExecuteParamRec &param);
	int processDelete(ExecuteParamRec &param);

	VacCtlStateArchive *findStatePool(ExecuteParamRec &param, IntegerVar &id,
		Variable *exceptionInfo);

	int processReplayNextDpe(ExecuteParamRec &param);
	int processReplayNextDpes(ExecuteParamRec &param);
	int processReplaySetValues(ExecuteParamRec &param);
	int processReplayProcess(ExecuteParamRec &param);
	int processReplayGetNextEventData(ExecuteParamRec &param);
	int processReplayGetPreviousEventData(ExecuteParamRec &param);
	int processReplaySetPlayTime(ExecuteParamRec &param);
	int processReplayGetPlayData(ExecuteParamRec &param);
	int processReplayClear(ExecuteParamRec &param);

};

#endif	// _VACCTLHISTORYDATAPVSS_H_
