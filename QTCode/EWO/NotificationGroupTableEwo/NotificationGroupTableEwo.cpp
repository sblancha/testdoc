/**
@defgroup NotificationGroupTableEwo NotificationGroupTableEwo
NotificationGroupTableEwo is a 'true' EWO (not QDialogs) dedicated to be inserted in WCCOA Panel vision\sms\NotificationGroupList.pnl.\n
It is table implemented as an EWO to reused the development of the EWO SmsConfigTable.\n
\n
< b> The base widget is the class GroupTable. < /b> \n 
@section signal Signals
List of signal this EWO can emit to WCCOA panel:\n
- selectionChanged()
- connectRequest()
- tableDoubleClick(int row, int col)
@section method Methods
- void clear()
- void addGroup(string dpName)
- void deleteGroup(string dpName)
\n

- void setGroupName(string dpName, string name)
- void setGroupOwner(string dpName, string owner)
- void setGroupId(string dpName, ulong id)
- void setGroupType(string dpName, int type);
- void setGroupScopes(string dpName, dyn_string scopes);
- void setGroupRecipients(string dpName, dyn_string recipients); 

- void setGroupFlags(string dpName, bool active, bool activated, bool initialized)
- void setGroupNumMsg(string dpName, int numMsg)
- void setGroupNumEvents(string dpName, int numEvents)
\n
- string nextDisconnectRequest()
- string nextConnectRequest()
- string getSelectedGroup()
- int getNumRows()
- string getGroupAtRow(int row)

*/
#include "NotificationGroupTableEwo.h"

EWO_PLUGIN(NotificationGroupTableEwo)

NotificationGroupTableEwo::NotificationGroupTableEwo(QWidget *parent) : BaseExternWidget(parent) {
	baseWidget = new GroupTable(parent);
	// Connect to signals of base widget
	connect(baseWidget->selectionModel(),
		SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
		this,
		SLOT(selectionChange(const QItemSelection &, const QItemSelection &)));
	connect(baseWidget->getModel(), SIGNAL(connectRequest(void)), this, SLOT(connectRequest(void)));
	connect(baseWidget, SIGNAL(doubleClicked(const QModelIndex &)),
		this, SLOT(doubleClick(const QModelIndex &)));
}
/**
@brief List of signals this EWO can emit to WCCOA panel
*/
QStringList NotificationGroupTableEwo::signalList(void) const {
	QStringList list;
	list.append("selectionChanged()");
	list.append("connectRequest()");
	list.append("tableDoubleClick(int row, int col)");
	return list;
}
/**
@brief List of methods which can be called by WCCOA panel
*/
QStringList NotificationGroupTableEwo::methodList(void) const
{
	QStringList list;
	list.append("void clear()");
	list.append("void addGroup(string dpName)");
	list.append("void deleteGroup(string dpName)");

	list.append("void setGroupName(string dpName, string name)");
	list.append("void setGroupOwner(string dpName, string owner)");
	list.append("void setGroupId(string dpName, ulong id)");
	list.append("void setGroupType(string dpName, int type)");
	list.append("void setGroupScopes(string dpName, dyn_string scopes)");
	list.append("void setGroupRecipients(string dpName, dyn_string recipients)");

	list.append("void setGroupFlags(string dpName, bool active, bool activated, bool initialized)");
	list.append("void setGroupNumMsg(string dpName, int numMsg)");
	list.append("void setGroupNumEvents(string dpName, int numEvents)");

	list.append("string nextDisconnectRequest()");
	list.append("string nextConnectRequest()");
	list.append("string getSelectedGroup()");
	list.append("int getNumRows()");
	list.append("string getGroupAtRow(int row)");
	return list;
}
/**
@brief Method is called when one of this EWO's methods is called from WCCOA
@param[in]		name	WCCOA method name - one of names we returned by methodList() method
@param[in][out]	values	Arguments from WCCOA
@param[in][out]	error	Variable where error message shall be written in case of fatal error
@return Value to be returned to WCCOA
*/
QVariant NotificationGroupTableEwo::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "clear")
	{
		baseWidget->getModel()->clear();
		return QVariant();
	}
	else if(name == "addGroup")
	{
		return invokeAddGroup(name, values, error);
	}
	else if(name == "deleteGroup")
	{
		return invokeDeleteGroup(name, values, error);
	}
	else if(name == "setGroupOwner")
	{
		return invokeSetGroupOwner(name, values, error);
	}
	else if(name == "setGroupName")
	{
		return invokeSetGroupName(name, values, error);
	}
	else if (name == "setGroupId")
	{
		return invokeSetGroupId(name, values, error);
	}
	else if (name == "setGroupType")
	{
		return invokeSetGroupType(name, values, error);
	}
	else if (name == "setGroupScopes")
	{
		return invokeSetGroupScopes(name, values, error);
	}
	else if (name == "setGroupRecipients")
	{
		return invokeSetGroupRecipients(name, values, error);
	}
	else if(name == "setGroupFlags")
	{
		return invokeSetGroupFlags(name, values, error);
	}
	else if(name == "setGroupNumMsg")
	{
		return invokeSetGroupNumMsg(name, values, error);
	}
	else if(name == "setGroupNumEvents")
	{
		return invokeSetGroupNumEvents(name, values, error);
	}
	else if(name == "nextDisconnectRequest")
	{
		return baseWidget->getModel()->nextDisconnectRequest();
	}
	else if(name == "nextConnectRequest")
	{
		return baseWidget->getModel()->nextConnectRequest();
	}
	else if(name == "getSelectedGroup")
	{
		return baseWidget->getSelectedGroup();
	}
	else if(name == "getNumRows")
	{
		return baseWidget->getModel()->getNumRows();
	}
	else
	{
		error = "Uknown method SmsConfigTable.";
		error += name;
	}
	return QVariant();
}
QVariant NotificationGroupTableEwo::invokeAddGroup(const QString &name, QList<QVariant> &values, QString &error) {
	if(!hasNumArgs(name, values, 1, error))	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String)) {
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	baseWidget->getModel()->addGroup(values[0].toString());
	return QVariant();
}
QVariant NotificationGroupTableEwo::invokeDeleteGroup(const QString &name, QList<QVariant> &values, QString &error) {
	if(!hasNumArgs(name, values, 1, error)) {
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String)) {
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	baseWidget->getModel()->deleteGroup(values[0].toString());
	return QVariant();
}
QVariant NotificationGroupTableEwo::invokeSetGroupOwner(const QString &name, QList<QVariant> &values, QString &error) {
	if(!hasNumArgs(name, values, 2, error)) {
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String)) {
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::String)) {
		error = "argument 2 (owner) is not string";
		return QVariant();
	}
	baseWidget->getModel()->setGroupOwner(values[0].toString(), values[1].toString());
	return QVariant();
}
QVariant NotificationGroupTableEwo::invokeSetGroupName(const QString &name, QList<QVariant> &values, QString &error) {
	if(!hasNumArgs(name, values, 2, error)) {
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String)) {
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::String)) {
		error = "argument 2 (name) is not string";
		return QVariant();
	}
	baseWidget->getModel()->setGroupName(values[0].toString(), values[1].toString());
	return QVariant();
}
QVariant NotificationGroupTableEwo::invokeSetGroupId(const QString &name, QList<QVariant> &values, QString &error) {
	if (!hasNumArgs(name, values, 2, error)) {
		return QVariant();
	}
	if (!values[0].canConvert(QVariant::String)) {
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if (!values[1].canConvert(QVariant::LongLong)) {
		error = "argument 2 (id) is not ulong";
		return QVariant();
	}
	baseWidget->getModel()->setGroupId(values[0].toString(), values[1].toLongLong());
	return QVariant();
}
QVariant NotificationGroupTableEwo::invokeSetGroupType(const QString &name, QList<QVariant> &values, QString &error) {
	if (!hasNumArgs(name, values, 2, error)) {
		return QVariant();
	}
	if (!values[0].canConvert(QVariant::String)) {
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if (!values[1].canConvert(QVariant::Int)) {
		error = "argument 2 (id) is not int";
		return QVariant();
	}
	baseWidget->getModel()->setGroupType(values[0].toString(), values[1].toLongLong());
	return QVariant();
}
QVariant NotificationGroupTableEwo::invokeSetGroupScopes(const QString &name, QList<QVariant> &values, QString &error) {
	if (!hasNumArgs(name, values, 2, error)) {
		return QVariant();
	}
	if (!values[0].canConvert(QVariant::String)) {
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if (!values[1].canConvert(QVariant::StringList)) {
		error = "argument 2 (name) is not dyn_string";
		return QVariant();
	}
	baseWidget->getModel()->setGroupScopes(values[0].toString(), values[1].toStringList());
	return QVariant();
}
QVariant NotificationGroupTableEwo::invokeSetGroupRecipients(const QString &name, QList<QVariant> &values, QString &error) {
	if (!hasNumArgs(name, values, 2, error)) {
		return QVariant();
	}
	if (!values[0].canConvert(QVariant::String)) {
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if (!values[1].canConvert(QVariant::StringList)) {
		error = "argument 2 (name) is not dyn_string";
		return QVariant();
	}
	baseWidget->getModel()->setGroupRecipients(values[0].toString(), values[1].toStringList());
	return QVariant();
}
QVariant NotificationGroupTableEwo::invokeSetGroupFlags(const QString &name, QList<QVariant> &values, QString &error) {
	if(!hasNumArgs(name, values, 4, error)) {
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String)) {
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::Bool)) {
		error = "argument 2 (active) is not bool";
		return QVariant();
	}
	if(!values[2].canConvert(QVariant::Bool)) {
		error = "argument 3 (actived) is not bool";
		return QVariant();
	}
	if(!values[3].canConvert(QVariant::Bool)) {
		error = "argument 4 (initialized) is not bool";
		return QVariant();
	}
	baseWidget->getModel()->setGroupFlags(values[0].toString(), values[1].toBool(), values[2].toBool(), values[3].toBool());
	return QVariant();
}
QVariant NotificationGroupTableEwo::invokeSetGroupNumMsg(const QString &name, QList<QVariant> &values, QString &error) {
	if(!hasNumArgs(name, values, 2, error)) {
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String)) {
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::Int)) {
		error = "argument 2 (numMsg) is not int";
		return QVariant();
	}
	baseWidget->getModel()->setGroupNumMsg(values[0].toString(), values[1].toInt());
	return QVariant();
}
QVariant NotificationGroupTableEwo::invokeSetGroupNumEvents(const QString &name, QList<QVariant> &values, QString &error) {
	if(!hasNumArgs(name, values, 2, error)) {
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String)) {
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::UInt)) {
		error = "argument 2 (numMsg) is not uint";
		return QVariant();
	}
	baseWidget->getModel()->setGroupNumEvents(values[0].toString(), values[1].toUInt());
	return QVariant();
}
/**
@brief Slot receiving selectionChanged signal of ConfigTable, emit signal 'selectionChanged' to WCCOA
*/
void NotificationGroupTableEwo::selectionChange(const QItemSelection & /* selected */,
												const QItemSelection & /* deselected */) {
	emit signal("selectionChanged");
}
/**
@brief Slot receiving connectRequest signal of ConfigTable, emit signal 'connectRequest' to WCCOA
*/
void NotificationGroupTableEwo::connectRequest(void) {
	emit signal("connectRequest");
}
/**
@brief Slot receiving doubleClicked signal of ConfigTable, emit signal 'doubleClick' to WCCOA
@param[in]	row			table row
@param[in]	col			table column
@param[in]	button		mouse button
@param[in]	mousePos	Mouse position
*/
void NotificationGroupTableEwo::doubleClick(const QModelIndex &index) {
	QList<QVariant> args;
	args.append(QVariant(index.row()));
	args.append(QVariant(index.column()));
	emit signal("tableDoubleClick", args);
}

