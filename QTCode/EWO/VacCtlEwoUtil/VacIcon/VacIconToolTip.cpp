//	Implementation of VacIconToolTip class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconToolTip.h"

#include "VacIcon.h"

VacIconToolTip::VacIconToolTip(QWidget *parent) : QToolTip(parent)
{
}

void VacIconToolTip::maybeTip(const QPoint &pos)
{
	if(!parentWidget()->inherits("VacIcon"))
	{
		return;
	}
	QRect rect;
	QString	text;
	if(!((VacIcon *)parentWidget())->getToolTip(pos, text, rect))
	{
		return;
	}
	tip(rect, text);	
}
