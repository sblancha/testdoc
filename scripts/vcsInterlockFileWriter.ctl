
main()
{
	bool hasMore = true;
	int pageNumber = 0;
	file writeFile = fopen(getPath(DATA_REL_PATH) + "LHC.for_Interlocks", "w");
	int err = ferror(writeFile); //Output possible errors
	
	if ( err )
	{
	 DebugTN("Error no. ",err," occurred while opening file");
	 return;
	}
		
	while(hasMore)
	{
		int result;
		string url = "http://vacmonscadai:8081/api/interlockInfo?versionId=-57725&size=25&page=" + pageNumber;
		mapping mapResult;
		result = netGet(url, mapResult);
		dyn_anytype jSonResult = jsonDecode(mapResult["content"]);
		
		dyn_anytype parentList;
		dyn_anytype detailList;
		string equipmentName, parentName, connectedTo, connectedOn, cable;
		bool firstChainStarted = false;
		//dyn_anytype topObjects = mappingKeys(jSonResult);	
		if(dynlen(jSonResult ) ==0)
		{
			hasMore = false;
		}
		for(int i = 1; i <= dynlen(jSonResult); i++)
		{
			//get data
			equipmentName = "";
			equipmentName = jSonResult[i]["name"];
			
			string type = substr(equipmentName, 0, 2);
			
			if(type == "VV")
			{
				if(firstChainStarted)
				{
					fputs("EndChain\n\n", writeFile);
				}
				
				fputs("StartChain=" + equipmentName + "\n\n", writeFile);
				firstChainStarted = true;
				continue;
			}
			
			fputs("#================ "+ equipmentName+" =====================\n",writeFile);
			fputs("EqpName=" + equipmentName + "\n", writeFile);
			
			parentList = jSonResult[i]["parentList"];

			for(int j= 1; j <= dynlen(parentList); j++)
			{
				parentName = connectedOn = connectedTo = cable = "";
				parentName = parentList[j]["parentName"];
				strreplace(parentName, " ", "");
				connectedOn = parentList[j]["connectedOn"];
				strreplace(connectedOn, " ", "");
				connectedTo = parentList[j]["connectedTo"];
				strreplace(connectedTo, " ", "");
				cable = parentList[j]["cable"];
				
				fputs("Parent=" + parentName + "\n", writeFile);
				fputs("ConnectedOn=" + connectedOn + "\n", writeFile);
				fputs("ConnectedTo=" + connectedTo + "\n", writeFile);
				fputs("Cable=" + cable + "\n", writeFile);
			}
			
			fputs("\n", writeFile);
			detailList = jSonResult[i]["detailList"];
			
			for(int j = 1; j<=dynlen(detailList);j++)
			{
				string detailValue = detailList[j]["detailValue"];
				strreplace(detailValue, " ", "");
				fputs(detailList[j]["detailName"] + "=" + detailValue + "\n", writeFile);
			}
			
			fputs("\n", writeFile);
			fputs("EndEqp\n", writeFile);
			fputs("\n", writeFile);

			
		}
		fputs("EndChain\n\n", writeFile);
		pageNumber++;
	}
	fclose(writeFile);
}
