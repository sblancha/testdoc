//	Implementation of EqpSECT_VPI_SUM class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpSECT_VPI_SUM.h"

#include "EqpVPI.h"
#include "DataPool.h"

#include "FunctionalType.h"
#include "EqpMsgCriteria.h"

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_ALL_ON	0,255,0
#define	COLOR_ALL_OFF 	255,0,0
#define COLOR_SOME_OFF	255,255,0
#define	COLOR_UNDEFINED	63,127,255

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define	RR1_ON			(0x00020000)
#define	RR1_OFF			(0x00010000)

/*
**	FUNCTION
**		Calculate DP name for sector summary device in given sector
**
**	ARGUMENTS
**		pSector	- Pointer to sector
**
**	RETURNS
**		DP name; or
**		empty string if there is no DP for such sector
**
**	CAUTIONS
**		None
*/
QString EqpSECT_VPI_SUM::getDpNameForSector(Sector *pSector)
{
	QString result;
	if(pSector->getVacType() & (VacType::Qrl | VacType::Cryo | VacType::DSL))
	{
		return result;
	}
	result = "VSECT_VPI_SUM_";
	result += pSector->getPvssName();
	return result;
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

EqpSECT_VPI_SUM::EqpSECT_VPI_SUM(Sector *pSector) : Eqp()
{
	this->pSector = pSector;
	pPlcs = new QList<Eqp *>();
	QString sectDpName = getDpNameForSector(pSector);
	dpName = (char *)malloc(strlen(sectDpName.toLatin1()) + 1);
#ifdef Q_OS_WIN
	strcpy_s(dpName, strlen(sectDpName.toLatin1()) + 1, sectDpName.toLatin1());
#else
	strcpy(dpName, sectDpName.toLatin1());
#endif
	name = (char *)malloc(strlen(pSector->getName()) + 1);
#ifdef Q_OS_WIN
	strcpy_s(name, strlen(pSector->getName()) + 1, pSector->getName());
#else
	strcpy(name, pSector->getName());
#endif
	functionalType = FunctionalType::SECT_VPI_SUM;
	plcSearchDone = false;
	nVpis = nVrpis = 0;
	rrcOnline = rrcReplay = 0;
}

EqpSECT_VPI_SUM::~EqpSECT_VPI_SUM()
{
	free((void *)name);
	free((void *)dpName);
	delete pPlcs;
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSECT_VPI_SUM::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	findAllPlcs();

	// First add DPEs of all PLCs
	for(int idx = 0 ; idx < pPlcs->count() ; idx++)
	{
		Eqp *pPlc = pPlcs->at(idx);
		QStringList plcStateDpes;
		pPlc->getStateDpes(plcStateDpes);
		stateDpes << plcStateDpes;
	}
	// Finally add the only DPE of this device - only makes sense if at least
	// one PLC was found
	if(pPlcs->count())
	{
		addDpeNameToList(stateDpes, "RRC");
	}
}

/*
**	FUNCTION
**		Return list of all PLCs, used by this device
**
**	ARGUMENTS
**		list	- Variable where list of PLCs will be written
**
**	RETURNS
**		true	- to identify this device may require more than one PLC
**
**	CAUTIONS
**		None
*/
bool EqpSECT_VPI_SUM::getPlcList(QList<Eqp *> &list)
{
	findAllPlcs();
	list = *pPlcs;
	return true;
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSECT_VPI_SUM::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "RRC", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSECT_VPI_SUM::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "RRC", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSECT_VPI_SUM::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RRC"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rrcOnline == newValue)
			{
				return;
			}
			rrcOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSECT_VPI_SUM::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RRC"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rrcReplay == newValue)
			{
				return;
			}
			rrcReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC(s), so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSECT_VPI_SUM::dpeChange(Eqp * /* pSrc */, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSECT_VPI_SUM::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	for(int idx = 0 ; idx < pPlcs->count() ; idx++)
	{
		Eqp *pPlc = pPlcs->at(idx);
		if(!pPlc->isAlive(mode))
		{
			color.setRgb(COLOR_UNDEFINED);
			return;
		}
	}
	unsigned rrc = mode == DataEnum::Replay ? rrcReplay : rrcOnline;
	unsigned statusOr = rrc & 0xFFFF0000u;
	unsigned statusAnd = rrc << 16;
	
	if(statusOr & RR1_ON)	// At least some VRPIs are ON
	{
		if(!(statusAnd & RR1_ON))	// Some VRPIs are not ON
		{
			color.setRgb(COLOR_SOME_OFF);
		}
		else if(statusOr & RR1_OFF)	// Some VRPIs are OFF
		{
			color.setRgb(COLOR_SOME_OFF);
		}
		else if(!(statusAnd & RR1_VALID))	// Not all are valid
		{
			color.setRgb(COLOR_UNDEFINED);
		}
		else	// All VPIs are valid and ON
		{
			color.setRgb(COLOR_ALL_ON);
		}
	}
	else	// None of VPIs are ON
	{
		if(statusAnd & RR1_OFF)	// All VRPIs are OFF
		{
			color.setRgb(COLOR_ALL_OFF);
		}
		else
		{
			color.setRgb(COLOR_UNDEFINED);
		}
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSECT_VPI_SUM::getMainStateString(QString & string, DataEnum::DataMode mode)
{
	char buf[256];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%s %d VPI(s) %d VRPI(s)", pSector->getName(), nVpis, nVrpis);
#else
	sprintf(buf, "%s %d VPI(s) %d VRPI(s)", pSector->getName(), nVpis, nVrpis);
#endif
	string = buf;

	for(int idx = 0 ; idx < pPlcs->count() ; idx++)
	{
		Eqp *pPlc = pPlcs->at(idx);
		if(!pPlc->isAlive(mode))
		{
			string += ": PLC error";
			return;
		}
	}
	unsigned rrc = mode == DataEnum::Replay ? rrcReplay : rrcOnline;
	unsigned statusOr = rrc & 0xFFFF0000u;
	unsigned statusAnd = rrc << 16;
	
	if(statusOr & RR1_ON)	// At least some VRPIs are ON
	{
		if(!(statusAnd & RR1_ON))	// Some VRPIs are not ON
		{
			string += ": some OFF";
		}
		else if(statusOr & RR1_OFF)	// Some VRPIs are OFF
		{
			string += ": some OFF";
		}
		else if(!(statusAnd & RR1_VALID))	// Not all are valid
		{
			string += ": some NOT valid";
		}
		else	// All VPIs are valid and ON
		{
			string += ": all ON";
		}
	}
	else	// None of VPIs are ON
	{
		if(statusAnd & RR1_OFF)	// All VRPIs are OFF
		{
			string += ": all OFF";
		}
		else
		{
			string += ": undefined";
		}
	}
	
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSECT_VPI_SUM::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	getMainStateString(string, mode);
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpSECT_VPI_SUM::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	getMainStateString(string, mode);
}
#endif

/*
**	FUNCTION
**		Check if there is alarm for at least one PLC
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
bool EqpSECT_VPI_SUM::isPlcAlarm(DataEnum::DataMode mode) const
{
	for(int idx = 0 ; idx < pPlcs->count() ; idx++)
	{
		Eqp *pPlc = pPlcs->at(idx);
		if(!pPlc->isAlive(mode))
		{
			return true;
		}
	}
	return false;
}
#endif

/*
**	FUNCTION
**		Find all PLCs, controlling VRPIs of VPIs of this sector.
**		Count also total number of all VPIs and VRPIs in this sector
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpSECT_VPI_SUM::findAllPlcs(void)
{
	if(plcSearchDone)
	{
		return;	// Search is done only once
	}
	plcSearchDone = true;

	QList<Eqp *> vrpis;	// In order not to count the same VRPI more than once
	const QList<BeamLine *> &lines = DataPool::getInstance().getLines();
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		const QList<BeamLinePart *> &parts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			const QList<Eqp *> &eqps = pPart->getEqpList();
			for(int eqpIdx = 0 ; eqpIdx < eqps.count() ; eqpIdx++)
			{
				Eqp *pEqp = eqps.at(eqpIdx);
				if(pEqp->getFunctionalType() != FunctionalType::VPI)
				{
					continue;
				}
				if(pEqp->getSectorBefore() != pSector)
				{
					continue;
				}
				nVpis++;
				Eqp *pVrpi = pEqp->getMaster();
				if(!pVrpi)
				{
					continue;
				}
				if(vrpis.contains(pVrpi))
				{
					continue;
				}
				vrpis.append(pVrpi);
				nVrpis++;
				const QString plcName = pVrpi->findPlcName();
				if(!plcName.isEmpty())
				{
					Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
					if(pPlc)
					{
						if(!pPlcs->contains(pPlc))
						{
							pPlcs->append(pPlc);
						}
					}
				}
			}
		}
	}
}

