// Actions for VG_A_RO 

void LhcVacDeviceAction_VG_A_RO(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  mapping         mConnectDpes;
  unsigned        act;
  
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
   default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }
}
