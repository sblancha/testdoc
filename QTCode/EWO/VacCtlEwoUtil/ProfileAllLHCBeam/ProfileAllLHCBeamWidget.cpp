//	Implementation of ProfileAllLHCBeamWidget class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileAllLHCBeamWidget.h"

#include "ExtendedLabel.h"


#include "ProfileLhcSingleBeamView.h"

#include "VacMainView.h"
#include "ResourcePool.h"
#include "DataPool.h"

#include <qlayout.h>
#include <qdesktopwidget.h>
#include <qapplication.h>
#include <qmenu.h>
#include <qcursor.h>
#include <qinputdialog.h>
#include <QMouseEvent>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

ProfileAllLHCBeamWidget::ProfileAllLHCBeamWidget(QWidget *parent, Qt::WindowFlags f) :
	QWidget(parent, f)
{
	setWindowTitle("Pressure Profile - all of LHC by beam type");

	// At creation time index of LHC part is not known, will be set later
	// (after initializang static data) by setLhcPartIndex()
	lhcPartIndex = -1;

}

ProfileAllLHCBeamWidget::~ProfileAllLHCBeamWidget()
{
}

/*
**	FUNCTION
**		Set index of LHC part shown in this dialog. Initialize
**		all contrls according to given index
**
**	ARGUMENTS
**		index	- LHC part index to set [0 or 1]
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Method shall be called after all static data are initialized
*/
void ProfileAllLHCBeamWidget::setLhcPartIndex(int index)
{
	if(lhcPartIndex >= 0)
	{
		return;	// Already initialized, can not be changed at run time
	}
	if((index != 0) && (index != 1))
	{
		return;	// Wrong index
	}
	lhcPartIndex = index;

	buildInitialEqpMask();
	buildLayout();

	/*
	const QList<VacMainView *> &mainList = VacMainView::getInstanceList();
	for(int idx = 0 ; idx < mainList.count() ; idx++)
	{
		VacMainView *pInstance = mainList.at(idx);
		if(pInstance->isPvssEvents())
		{
			QObject::connect(this, SIGNAL(profileDpDown(int, int, int, int, int, const char *)),
				pInstance, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
		}
	}
	*/
	VacMainView *pMainView = VacMainView::getInstance();
	if(pMainView)
	{
		QObject::connect(this, SIGNAL(profileDpDown(int, int, int, int, int, const char *)),
			pMainView, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
	}
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileAllLHCBeamWidget::buildLayout(void)
{
	// 1) Main layout - 1 label and 8 profile images
	QVBoxLayout *pMainBox = new QVBoxLayout(this);
	pMainBox->setContentsMargins(0, 0, 0, 0);
	pMainBox->setSpacing(0);

	// 2) Common label
	int vacType = lhcPartIndex ? VacType::RedBeam : VacType::BlueBeam;
	QString vacName = lhcPartIndex ? "Red Beam" : "Blue Beam";
	ExtendedLabel *pLabel = new ExtendedLabel(vacName, this);
	pLabel->setLabelType(ExtendedLabel::Active);
	pMainBox->addWidget(pLabel);
	pLabel->setAlignment(Qt::AlignHCenter);
	QObject::connect(pLabel, SIGNAL(mousePressed(QMouseEvent *)),
		this, SLOT(labelPress(QMouseEvent *)));

	// 3) For every main part - profile image
	// Set limits for new profile
	int beamMin = -12, beamMax = -8;
	ResourcePool &resourcePool = ResourcePool::getInstance();
	if(resourcePool.getIntValue("AllProfile.BeamVacuumScaleMin", beamMin) == ResourcePool::NotFound)
	{
		beamMin = -12;
	}
	if(resourcePool.getIntValue("AllProfile.BeamVacuumScaleMax", beamMax) == ResourcePool::NotFound)
	{
		beamMax = -8;
	}

	int tempMin = 0, tempMax = 20;
	if(resourcePool.getIntValue("Profile.TemperatureScaleMin", tempMin) == ResourcePool::NotFound)
	{
		tempMin = 0;
	}
	if(resourcePool.getIntValue("Profile.TemperatureScaleMax", tempMax) == ResourcePool::NotFound)
	{
		tempMax = 20;
	}

	VacEqpTypeMask dummyEqpMask;
	DataPool &pool = DataPool::getInstance();
	for(int n = 1 ; n <= 8 ; n++)
	{
		QString mpName = "LSSV" + QString::number(n);
		MainPart *pMainPart = pool.findMainPartData(mpName.toLatin1());
		if(!pMainPart)
		{
			continue;
		}
		Sector *pFirstSector = NULL;
		Sector *pLastSector = NULL;
		if(!buildSectorRange(pMainPart, &pFirstSector, &pLastSector))
		{
			return;
		}

		// Main part and sector range have been found - build widgets
		QString errMsg;
		ProfileLhcSingleBeamView *pView = ProfileLhcSingleBeamView::create(this, vacType, pMainPart,
			pFirstSector, pLastSector);
		if(!pView)
		{
			// TODO: show error message
			continue;
		}
		pView->setMinimumSize(400, 100);
		pMainBox->addWidget(pView, 10);	// Image will consume all free space
		images.append(pView);

		// Prepare and show view
		pView->init(allEqpMask);
		pView->applyMask(vacType, dummyEqpMask, eqpMask);
		pView->setBeamLimits(beamMin, beamMax);
		pView->setLinLimits(tempMin, tempMax);
		pView->show();

		// Connect to essential signals of view
		QObject::connect(pView, SIGNAL(beamLimitsChanged(int, int)),
			this, SLOT(beamLimitsChange(int, int)));
		QObject::connect(pView, SIGNAL(linLimitsChanged(int, int)),
			this, SLOT(linLimitsChange(int, int)));
		QObject::connect(pView, SIGNAL(dpMouseDown(int, int, int, int, const char *)),
			this, SLOT(dpMouseDown(int, int, int, int, const char *)));
	}
}

/*
**	FUNCTION
**		Find first and last sector for given main part
**
**	ARGUMENTS
**		pMp				- Pointer to main part
**		ppFirstSector	- Pointer to variable where pointer to first sector
**							will be written
**		ppLastSector	- Pointer to variable where pointer to last sector
**							will be written
**
**	RETURNS
**		true	- if sectors have been found;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool ProfileAllLHCBeamWidget::buildSectorRange(MainPart *pMp, Sector **ppFirstSector, Sector **ppLastSector)
{
	*ppFirstSector = *ppLastSector = NULL;

	QList<MainPart *> mpList;
	mpList.append(pMp);
	mainVacMask = vacMask = VacType::None;
	QStringList sectorList;
	buildRangeFromMps(mpList, sectorList);
	DataPool &pool = DataPool::getInstance();

	// DSL sector shall not be used as start or end sector
	foreach(QString sectName, sectorList)
	{
		Sector *pSector = pool.findSectorData(sectName.toLatin1());
		if(pSector->getVacType() == VacType::DSL)
		{
			continue;
		}
		if(!*ppFirstSector)
		{
			*ppFirstSector = pSector;
		}
		*ppLastSector = pSector;
	}
	return *ppFirstSector && *ppLastSector;
}

/*
**	FUNCTION
**		Build initial equipment type mask
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileAllLHCBeamWidget::buildInitialEqpMask(void)
{
	// First build mask containing all possible types for profile
	allEqpMask.append(FunctionalType::VGM);
	allEqpMask.append(FunctionalType::VGR);
	allEqpMask.append(FunctionalType::VGP);
	allEqpMask.append(FunctionalType::VGI);
	allEqpMask.append(FunctionalType::VGF);
	allEqpMask.append(FunctionalType::VPI);
	allEqpMask.append(FunctionalType::VPGMPR);
	allEqpMask.append(FunctionalType::VGTR);
	allEqpMask.append(FunctionalType::CRYO_TT);
	allEqpMask.append(FunctionalType::CRYO_TT_EXT);

	// Then read default settings from resource
	eqpMask = allEqpMask;
	ResourcePool &pool = ResourcePool::getInstance();
	QString resourceName = "AllProfile.BeamVacuumEqpTypes";
	if(pool.getEqpTypesMask(resourceName, eqpMask) == ResourcePool::NotFound)
	{
		eqpMask = allEqpMask;
	}
}

/*
**	FUNCTION
**		Slot activated when vertical limits for beam pressure have been changed
**		in one of graphs
**
**	ARGUMENTS
**		min	- New minimum value
**		max	- New maximum value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeamWidget::beamLimitsChange(int min, int max)
{
	setBeamLimits(min, max);
	emit beamLimitsChanged(min, max);
}

/*
**	FUNCTION
**		Set vertical limits for beam pressure in all graphs
**
**	ARGUMENTS
**		min	- New minimum value
**		max	- New maximum value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeamWidget::setBeamLimits(int min, int max)
{
	for(int idx = 0 ; idx < images.count() ; idx++)
	{
		ProfileLhcView *pImage = images.at(idx);
		pImage->setBeamLimits(min, max);
	}
}

/*
**	FUNCTION
**		Slot activated when vertical limits for temperature have been changed
**		in one of graphs
**
**	ARGUMENTS
**		min	- New minimum value
**		max	- New maximum value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeamWidget::linLimitsChange(int min, int max)
{
	setLinLimits(min, max);
	emit linLimitsChanged(min, max);
}

/*
**	FUNCTION
**		Set vertical limits for temperature in all graphs
**
**	ARGUMENTS
**		min	- New minimum value
**		max	- New maximum value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeamWidget::setLinLimits(int min, int max)
{
	for(int idx = 0 ; idx < images.count() ; idx++)
	{
		ProfileLhcView *pImage = images.at(idx);
		pImage->setLinLimits(min, max);
	}
}

/*
**	FUNCTION
**		Slot activated when mouse pointer was pressed on one of devices in profile
**
**	ARGUMENTS
**		button	- Mouse button number
**		mode	- Data acquisition mode
**		x		- Mouse X-coordinate in screen coordinate system
**		y		- Mouse Y-coordinate in screen coordinate system
**		dpName	- Name of DP under mouse pointer
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeamWidget::dpMouseDown(int button, int mode, int x, int y, const char *dpName)
{
	emit profileDpDown(button, mode, x, y, 0, dpName);
}

/*
**	FUNCTION
**		Analyze which equipment types are available in graphs of this dialog
**
**	ARGUMENTS
**		allMpMask	- Variable where resulting types will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeamWidget::analyzeTypes(VacEqpTypeMask &allMpMask)
{
	unsigned vacMask;
	VacEqpTypeMask beamMask, cryoMask;
	for(int idx = 0 ; idx < images.count() ; idx++)
	{
		ProfileLhcView *pView = images.at(idx);
		beamMask.clear();
		pView->analyzeTypes(vacMask, cryoMask, beamMask);
		const QList<FunctionalType *> &types = beamMask.getList();
		for(int typeIdx = 0 ; typeIdx < types.count() ; typeIdx++)
		{
			FunctionalType *pType = types.at(typeIdx);
			if(allEqpMask.contains(pType->getType()))
			{
				allMpMask.append(pType->getType());
			}
		}
	}
}

/*
**	FUNCTION
**		Slot activated when one of MP 'labels' is pressed
**
**	ARGUMENTS
**		e	- Pointer to mouse event that caused signal to emit
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeamWidget::labelPress(QMouseEvent *e)
{
	if(e->button() != Qt::RightButton)
	{
		return;
	}
	emit labelPressed(e);
}

/*
**	FUNCTION
**		Apply new equipment type mask to all graphs of this dialog
**
**	ARGUMENTS
**		newEqpMask	- New equipment type mask.
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileAllLHCBeamWidget::applyMask(VacEqpTypeMask &newEqpMask)
{
	eqpMask = newEqpMask;
	VacEqpTypeMask dummyMask;
	for(int idx = 0 ; idx < images.count() ; idx++)
	{
		ProfileLhcView *pView = images.at(idx);
		pView->applyMask(VacType::BlueBeam | VacType::RedBeam, dummyMask, eqpMask);
	}
}
