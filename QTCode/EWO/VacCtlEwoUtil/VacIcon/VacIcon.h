#ifndef VACICON_H
#define VACICON_H

#include "VacCtlEwoUtilExport.h"

//	Superclass for all vacuum equipment icons.

#include "VacIconGeometry.h"
#include "VacIconContainer.h"
#include "VacIconConnection.h"
#include "EqpMsgCriteria.h"

#include "Eqp.h"	// For Eqp::CtlStatus

#include <qwidget.h>
#include <qtimer.h>

class VacEqpTypeMask;

class Eqp;

class VACCTLEWOUTIL_EXPORT VacIcon : public QWidget
{
	Q_OBJECT

signals:
	void mouseDown(int button, int mode, int x, int y, int extra, const char *dpName);
	
public:
	// Note parent argument is mandatory in constructor
	VacIcon(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIcon();

	// Return icon instance for given device
	static VacIcon *getIcon(Eqp *pEqp, QWidget *parent);

	// Return icon instance for given device type
	static VacIcon *getIcon(VacIconContainer::EqpType type, QWidget *parent);

	// Return icon type for given device (WITHOUT creating icon)
	static int getIconType(Eqp *pEqp);

	// Size and normal position of icon
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask) = 0;

	// Size and normal position of icon - very special method for LHC where
	// geometry can also depend on what vacumm is drawn (beam/isolation).
	// For the time being the only eqp. type who needs this specific is
	// cryo thermometer
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask, bool /* forBeamVac */)
	{
		return getGeometry(mask);	// cryo thermometers shall override this implementation
	}

	// Assign equipment to icon - after that icon can take device's data
	virtual void setEqp(Eqp *pEqp) = 0;

	// Get equipment used by icon
	virtual Eqp *getEqp(void) = 0;

	// Connect to device's DPEs
	virtual bool connect(void) = 0;

	// Disconnect from device's DPEs
	virtual bool disconnect(void) = 0;

	// Calculate text and rectangle for tooltip
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect) = 0;

	// Some icons shall always be located 'opposite to other icons' in vertical
	// direction. The only known example at the moment is VIES, but more may come...
	virtual inline bool isAlwaysOpposite(void) const { return false; }

	// Icon itself shall be able to draw arrow, indicating that this device is
	// interlock source for valve (VGP & VPI). The arrow is drawn outside the icon,
	// but icon's knowledge of own geometry is needed.
	virtual void drawInterlockSrcArrow(QPainter & /* painter */, bool /* before */, bool /* after */) {}

	// Usually icon is connected to vacuum pipe, but there are exceptions (VRJ_TC, for example)
	virtual inline bool isConnectedToVacuum(void) { return true; }

	// Access
	inline VacIconContainer::Direction getDirection(void) const { return direction; }
	virtual void setDirection(VacIconContainer::Direction newDirection);
	inline VacIconContainer::BeamDirection getBeamDirection(void) const { return beamDirection; }
	virtual void setBeamDirection(VacIconContainer::BeamDirection newDirection);
	inline const QColor &getPipeColor(void) const { return pipeColor; }
	virtual void setPipeColor(const QColor &newColor);
	inline int getPipeWidth(void) const { return pipeWidth; }
	virtual void setPipeWidth(int newWidth);
	inline bool isDrawConnectionPipe(void) const { return drawConnectionPipe; }
	inline void setDrawConnectionPipe(bool flag) { drawConnectionPipe = flag; }
	inline DataEnum::DataMode getMode(void) const { return mode; }
	virtual void setMode(DataEnum::DataMode newMode) = 0;	// Must reconnect
	// Direction for 'slave' icons. The only known example for the time being is:
	// alarms for external targets
	virtual VacIconContainer::Direction getSlaveDirection(void) const { return direction; }

public slots:
	// Force redraw of this icon
	virtual void forceRedraw(void);


protected:
	// Connection to device
	VacIconConnection				connection;

	// Color for vacuum pipe drawing (used if pipe is drawn within icon)
	QColor							pipeColor;

	// Width for vacuum pipe drawing (used if pipe is drawn within icon)
	int								pipeWidth;

	// Direction for icon
	VacIconContainer::Direction		direction;

	// Beam direction in this icon
	VacIconContainer::BeamDirection	beamDirection;

	// Data acquisition mode
	DataEnum::DataMode				mode;

	// Flag indicating if this icon is connected to source device
	bool							connected;

	// In rare cases (for example - VGF_STD in VPG) connection pipe shall not be drawn
	bool							drawConnectionPipe;

	// Blinking behavior
	bool							isBlinking;
	bool							isBlinkVisible;
	int								blinkingPeriod;
	QTimer							*pTimer;
	
	// Return icon type for given mobile device (WITHOUT creating icon)
	static int getMobileIconType(Eqp *pEqp);

	static VacIconContainer::EqpType findIconTypeForVPG_6A01(Eqp *pEqp);

	// Check if icon is for 'virtual' device (device without DP, example - VBAR)
	virtual inline bool isVirtualDevice(void) const { return false; }

	// Draw icon's selection
	virtual void drawSelection(QPainter &painter);

	// React on mouse press
	virtual void mousePressEvent(QMouseEvent *pEvent);

	// React on mouse double click
	virtual void mouseDoubleClickEvent(QMouseEvent *pEvent);

	// Tooltip processing
	virtual bool event(QEvent *pEvent);

protected slots:
	virtual void timerBlinkDone(void);
};

#endif	// VACICON_H
