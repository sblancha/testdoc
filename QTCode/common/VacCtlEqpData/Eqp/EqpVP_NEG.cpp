//	Implementation of EqpVP_NEG class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVP_NEG.h"

#include "EqpVPN.h"
#include "EqpVPNMUX.h"

#include "DataPool.h"
#include "FunctionalTypeVP_NEG.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0
#define COLOR_WARNING			255,255,0
#define COLOR_UNDERRANGE		0,180,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define	RR1_ON			(0x02000000)
#define	RR1_OFF			(0x01000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define RR1_ERROR_CODE_MASK	(0x000000FF)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVP_NEG::EqpVP_NEG(const Eqp &source) : Eqp(source)
{
	pMaster = NULL;
	rr1Online = rr1Replay = 0;
	rr2Online = rr2Replay = 0;
	currentOnline = currentReplay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::VP_NEG;
}

EqpVP_NEG::~EqpVP_NEG()
{
	slaveEqps.clear();
}

void EqpVP_NEG::postProcess(void)
{
	const QString masterDp = getAttrValue("MasterName");
	if(!masterDp.isEmpty())
	{
		DataPool &pool = DataPool::getInstance();
		Eqp *pEqp = pool.findEqpByDpName(masterDp.toLatin1());
		if(pEqp)
		{
			Q_ASSERT_X(pEqp->inherits("EqpVPNMUX"), "EqpVP_NEG::postProcess", pEqp->getDpName());
			pMaster = (EqpVPNMUX *)pEqp;
			pMaster->addSlave(this);

			// In order to stay PERMANENTLY connected to master's selection
			/* Is it needed here? L.Kopylov 11.06.2014
			selected = pMaster->isSelected();
			QObject::connect(pMaster, SIGNAL(selectChanged(Eqp *)), this, SLOT(masterSelectChange(Eqp *)));
			*/
		}
	}
}

/*
**	FUNCTION
**		Find master DP and master channel for this pump.
**
**	ARGUMENTS
**		masterDP		- Variable where master DP name will be written
**		masterChannel	- Variable where channel number in master will
**							be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_NEG::getMaster(QString &masterDp, int &masterChannel)
{
	masterDp = getAttrValue("MasterName");
	if(masterDp.isEmpty())
	{
		masterChannel = 0;
		return;
	}
	masterChannel = getAttrValue("MasterChannel").toInt();
}

/*
**
**	FUNCTION
**		Set selected state of this device.
**
**	PARAMETERS
**		selected	- New selection state
**		isFromPvss	- Flag indicating if this is original call from PVSS
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void EqpVP_NEG::setSelected(bool selected, bool isFromPvss)
{
	Eqp::setSelected(selected, isFromPvss);
}

/*
**	FUNCTION
**		Find slave DPs for this power supply.
**
**	ARGUMENTS
**		slaves		- Variable where slave DP names will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_NEG::getSlaves(QStringList &slaves)
{
	slaves.clear();
	foreach(EqpVPN *pEqp , slaveEqps)
	{
		slaves.append(pEqp->getDpName());
	}
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_NEG::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "RR2");
}

void EqpVP_NEG::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	addDpeNameToList(valueDpes, "STATE.I_Value");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_NEG::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "RR2", mode);
	connectDpe(pDst, "STATE.I_Value", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_NEG::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "STATE.I_Value", mode);
	disconnectDpe(pDst, "RR2", mode);
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "EqpStatus", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_NEG::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Online == newValue)
			{
				return;
			}
			rr2Online = newValue;
		}
	}
	else if(!strcmp(dpe, "STATE.I_Value"))
	{
		if(value.canConvert(QVariant::Int))
		{
			int newValue = value.toInt();
			float newCurrent = (float)newValue;
			if(currentOnline == newCurrent)
			{
				return;
			}
			currentOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_NEG::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "RR2"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr2Replay == newValue)
			{
				return;
			}
			rr2Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "STATE.I_Value"))
	{
		if(value.canConvert(QVariant::Int))
		{
			int newValue = value.toUInt();
			float newCurrent = (float)newValue;
			if(currentReplay == newCurrent)
			{
				return;
			}
			currentReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

bool EqpVP_NEG::convertValue(const QVariant &value, float &result, EqpPart /* part */)
{
	bool coco = false;
	result = 0;
	if(value.canConvert(QVariant::Int))
	{
		result = value.toInt();
		result *= (float)0.001;		// mA -> A; mV -> V
		coco = true;
	}
	return coco;
}


/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_NEG::dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_NEG::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, rr2Replay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, rr2Online, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_NEG::getMainColor(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr1 & RR1_ERROR)
	{
		color.setRgb(COLOR_ERROR);
	}
	else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if(rr1 & RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else if(rr1 & RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_NEG::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, rr2Replay, currentReplay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, rr2Online, currentOnline, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		current		- Value of STATE.I_Value DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_NEG::getMainStateString(unsigned rr1, unsigned rr2, float current, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
	}
	else if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if(rr1 & RR1_ON)
	{
		if(rr1 & RR1_OFF)
		{
			string = "Error";
		}
		else
		{
			string = "ON, ";
			QString modeString;
			getModeString(rr2, modeString);
			string += modeString;
			string += ", I=";
			string += QString::number(0.001 * (double)current, 'f', 3);
			string += " A";
		}
	}
	else if(rr1 & RR1_OFF)
	{
		string = "OFF, ";
		QString modeString;
		getModeString(rr2, modeString);
		string += modeString;
	}
	else
	{
		string = "Undefined";
	}
}

void EqpVP_NEG::getModeString(unsigned rr2, QString &result)
{
	unsigned mode = (rr2 >> 8) & 0xFF;
	switch(mode)
	{
	case 1:
		result = "Activation";
		break;
	case 2:
		result = "Degassing";
		break;
	case 3:
		result = "Manual";
		break;
	default:
		result = "Mode ";
		result += QString::number(mode);
		break;
	}
}

#endif


/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_NEG::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("RR1");
	dpeNames.append("RR2");
	dpeNames.append("STATE.I_Value");
	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	if (values.isEmpty()) {
		return;
	}
	pVariant = values.takeFirst();
	unsigned rr2 = pVariant->toUInt();
	delete pVariant;
	if (values.isEmpty()) {
		return;
	}
	pVariant = values.takeFirst();
	float current = pVariant->toFloat();
	delete pVariant;
	getMainStateString(rr1, rr2, current, false, state);
	getMainColor(rr1, rr2, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpVP_NEG::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	return rr1 & RR1_ERROR_CODE_MASK;
}




/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVP_NEG::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "EqpStatus");
	addDpeNameToList(dpes, "RR1");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVP_NEG::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if((rr1 & (RR1_ON | RR1_OFF)) == RR1_ON)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if((rr1 & (RR1_ON | RR1_OFF)) == RR1_OFF)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if(rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if(criteria.isWarnings())
	{
		if(rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}

	return 0;
}
#endif



/*
**	FUNCTION
**		Check if current value is valid or not. The method was initially
**		introduced for SMS sending facility
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		true	- If pressure value is valid;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool EqpVP_NEG::isMainValueValid(DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		return isValueValid(plcAlarmReplay, rr1Replay);
	}
	return isValueValid(plcAlarmOnline, rr1Online);
}

bool EqpVP_NEG::isValueValid(bool plcAlarm, unsigned rr1)
{
	if(plcAlarm)
	{
		return false;
	}
	if((rr1 & (RR1_VALID)) != (RR1_VALID))
	{
		return false;
	}
	return true;
}

float EqpVP_NEG::getMainValue(DataEnum::DataMode mode)
{
	return mode == DataEnum::Replay ? currentReplay : currentOnline;
}

/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVP_NEG::checkCriteria(EqpMsgCriteria *pCriteria)
{
	checkCriteria(pCriteria, currentOnline);
}

void EqpVP_NEG::checkCriteria(EqpMsgCriteria *pCriteria, float /* current */)
{
	// Only accept criteria for this functional type
	if((pCriteria->getFunctionalType() != functionalType) &&
		(pCriteria->getFunctionalType() != FunctionalType::VPN))
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if(checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	// Without PLC connection does not match any criteria
	if(plcAlarmOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// In invalid state - only matches Error/warning criteria
	if(!(rr1Online & RR1_VALID))
	{
		switch(pCriteria->getType())
		{
		case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		default:
			pCriteria->setMatch(false, false, 0);
			return;
		}
	}

	// State is valid - decide based on criteria
	unsigned maskedRr1 = rr1Online & (RR1_ON | RR1_OFF);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case FunctionalTypeVP_NEG::Off:
			if(maskedRr1 == RR1_OFF)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVP_NEG::On:
			if(maskedRr1 == RR1_ON)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
	default:
		break;
	}
}

