//	Implementation of EqpTypeSelectorWidget class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpTypeSelectorWidget.h"

#include "DataPool.h"
#include "Eqp.h"

#include <qlayout.h>
#include <qgroupbox.h>
#include <qtablewidget.h>
#include <QHeaderView>

#define	SELECTED_TEXT	"YES"
#define UNSELECTED_TEXT	""

VacEqpTypeMask	EqpTypeSelectorWidget::storedEqpMask;


EqpTypeSelectorWidget::EqpTypeSelectorWidget(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	// Build layout. Layout is simple: SectorSelectorWidget on the left and
	// table with equipment types on the right
	QHBoxLayout *mainBox = new QHBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);

	pSectorSelector = new SectorSelectorWidget(this);
	connect(pSectorSelector, SIGNAL(selectionChanged(void)),
		this, SLOT(sectorSelectionChange(void)));
	mainBox->addWidget(pSectorSelector, 1);

	QGroupBox *tableBox = new QGroupBox("Equipment Types", this);
	QVBoxLayout *pTableBoxLayout = new QVBoxLayout(tableBox);
	pTableBoxLayout->setContentsMargins(0, 0, 0, 0);
	pTableBoxLayout->setSpacing(0);

	pTable = new QTableWidget(tableBox);
//	pTable->setReadOnly(true);
//	pTable->setLeftMargin(0);
	pTable->setColumnCount(4);
	pTable->hideColumn(0);	// Type ID
	QStringList labels;
	labels.append("");
	labels.append("Eqp Type");
	labels.append("Amount");
	labels.append("Use");
	pTable->setHorizontalHeaderLabels(labels);

	pTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
	pTable->setColumnWidth(2, 60);
	pTable->setColumnWidth(3, 40);
	connect(pTable, SIGNAL(itemClicked(QTableWidgetItem *)),
		this, SLOT(tableCellClicked(QTableWidgetItem *)));
//	pTableBoxLayout->addWidget(pTable);
	pTableBoxLayout->addWidget(pTable);
	mainBox->addWidget(tableBox, 2);
}

EqpTypeSelectorWidget::~EqpTypeSelectorWidget()
{
}

void EqpTypeSelectorWidget::initSectorContent(void)
{
	pSectorSelector->initContent();
	eqpMask.clear();
	displaySelection();
}

void EqpTypeSelectorWidget::setSelectedSectors(const QStringList &sectorList)
{
	pSectorSelector->setSelectedSectors(sectorList);
}

QList<QVariant> EqpTypeSelectorWidget::getSelectedEqpTypes(void)
{
	QList<QVariant> result;
	const QList<FunctionalType *> &types = eqpMask.getList();
	for(int idx = 0 ; idx < types.count() ; idx++)
	{
		FunctionalType *pType = types.at(idx);
		result.append(QVariant(pType->getType()));
	}
	return result;
}

void EqpTypeSelectorWidget::setSelectedEqpTypes(QList<QVariant> &types)
{
	eqpMask.clear();
	for(int idx = 0 ; idx < types.count() ; idx++)
	{
		int type = types.at(idx).toInt();
		eqpMask.append(type);
	}
	displaySelection();
}

/*
**	FUNCTION
**		Check if scope selection is empty
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- scope is empty;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool EqpTypeSelectorWidget::isEmpty(void)
{
	// Scope is not empty if there is at least one equipment type in table
	// with 'YES' in 3rd column
	for(int row = pTable->rowCount() - 1 ; row >= 0 ; row--)
	{
		if(pTable->item(row, 3)->text() == SELECTED_TEXT)
		{
			return false;
		}
	}
	return true;
}

/*
**	FUNCTION
**		Restore selected equipment types from stored equipment type mask
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpTypeSelectorWidget::restoreSelectedTypes(void)
{
	eqpMask.clear();
	displaySelection();
	eqpMask = storedEqpMask;
	displaySelection();
	emit selectionChanged();
}

/*
**	FUNCTION
**		Select equipment type(s) corresponding to given DP type
**
**	ARGUMENTS
**		dpTypeName	- Name of DP type that shall be selected
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpTypeSelectorWidget::selectDpType(const char *dpTypeName)
{
	DataPool &pool = DataPool::getInstance();
	const QHash<QByteArray, Eqp *> &eqpDict = pool.getEqpDict();
	bool changed = false;
	foreach(Eqp *pEqp, eqpDict)
	{
		if(pEqp->getDpType())
		{
			if(!strcmp(pEqp->getDpType(), dpTypeName))
			{
				if(!eqpMask.contains(pEqp->getFunctionalType()))
				{
					changed = true;
					eqpMask.append(pEqp->getFunctionalType());
					selectTypeInTable(pEqp->getFunctionalType(), true);
				}
			}
		}
	}
	if(changed)
	{
		emit selectionChanged();
	}
}

/*
**	FUNCTION
**		Select given functional type
**
**	ARGUMENTS
**		type	- Functional type that shall be selected
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpTypeSelectorWidget::selectFunctionalType(int type)
{
	if(!eqpMask.contains(type))
	{
		eqpMask.append(type);
		selectTypeInTable(type, true);
		emit selectionChanged();
	}
}

/*
**	FUNCTION
**		Select given functional type(s) corresponding to given PVSS functional type
**
**	ARGUMENTS
**		type	- PVSS functional type that shall be selected
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpTypeSelectorWidget::selectPvssFunctionalType(int type)
{
	DataPool &pool = DataPool::getInstance();
	const QHash<QByteArray, Eqp *> &eqpDict = pool.getEqpDict();
	bool changed = false;
	foreach(Eqp *pEqp, eqpDict)
	{
		if(pEqp->getPvssFunctionalType() == type)
		{
			if(!eqpMask.contains(pEqp->getFunctionalType()))
			{
				changed = true;
				eqpMask.append(pEqp->getFunctionalType());
				selectTypeInTable(pEqp->getFunctionalType(), true);
			}
		}
	}
	if(changed)
	{
		emit selectionChanged();
	}
}

/*
**	FUNCTION
**		Build list of DP names corresponding to current sector and functional
**		type selection. List of DP names shall contain names in 'natural' order
**
**	ARGUMENTS
**		result	- variable where resulting DP names list will be returned
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpTypeSelectorWidget::getDpNames(QStringList &result)
{
	result.clear();

	// Get list of selected sectors
	DataPool &pool = DataPool::getInstance();
	QStringList sectorNames;
	pSectorSelector->getSelectedSectors(sectorNames);
//	const QList<BeamLine *> &lines = pool.getLines();

	// Non-vacuum devices - before devices in sectors
	if(pSectorSelector->isNonVacSelected())
	{
		const QHash<QByteArray, Eqp *> &eqpDict = pool.getEqpDict();
		foreach(Eqp *pEqp, eqpDict)
		{
			if(!pEqp->getDpName())
			{
				continue;
			}
			if(pEqp->getSectorBefore() || pEqp->getSectorAfter())	// 12.10.2012 L.Kopylov
			{
				continue;
			}
			if(!eqpMask.contains(pEqp->getFunctionalType()))
			{
				continue;
			}
			if(!pEqp->isActive(DataEnum::Online))
			{
				continue;
			}
			if(result.indexOf(pEqp->getDpName()) >= 0)
			{
				continue;	// DP name is already in list
			}
			result.append(pEqp->getDpName());
		}
	}
	if(result.count() > 0)
	{
		result.sort();	// Non-vacuum devices have no natural order
	}

	// Build list of DP names	
	foreach(QString sectName, sectorNames)
	{
		Sector *pSector = pool.findSectorData(sectName.toLatin1());
		if(!pSector)
		{
			continue;
		}

		const QList<Eqp *> &eqpList = pSector->getEqpList();
		for(int idx = 0 ; idx < eqpList.count() ; idx++)
		{
			Eqp *pEqp = eqpList.at(idx);
			if(!pEqp->getDpName())
			{
				continue;
			}
			if(!eqpMask.contains(pEqp->getFunctionalType()))
			{
				continue;
			}
			/* if (!pEqp->isActive(DataEnum::Online))
			{
				continue;
			}*/ // to use device list with wireless mobile report even dp not active 
			if(result.indexOf(pEqp->getDpName()) >= 0)
			{
				continue;	// DP name is already in list
			}
			result.append(pEqp->getDpName());
		}

		/*
		for(BeamLine *pLine = lineIter.toFirst() ; pLine ; pLine = ++lineIter)
		{
			QPtrListIterator<BeamLinePart> partIter(pLine->getParts());
			for(BeamLinePart *pPart = partIter.toFirst() ; pPart ; pPart = ++partIter)
			{
				Eqp	**eqpPtrs = pPart->getEqpPtrs();
				int nEqpPtrs = pPart->getNEqpPtrs();
				for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(!pEqp->getDpName())
					{
						continue;
					}
					if(!eqpMask.contains(pEqp->getFunctionalType()))
					{
						continue;
					}
					if(result.find(pEqp->getDpName()) != result.end())
					{
						continue;	// DP name is already in list
					}
					if(!pEqp->isActive(DataEnum::Online))
					{
						continue;
					}
					if(pEqp->getSectorBefore() == pSector)
					{
						result.append(pEqp->getDpName());
					}
					else if(pEqp->getSectorAfter() == pSector)
					{
						result.append(pEqp->getDpName());
					}
				}
			}
		}
		*/
	}
}

/*
**	FUNCTION
**		Mark all functional types as not selected in table
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpTypeSelectorWidget::clearTableSelection(void)
{
	for(int row = pTable->rowCount() - 1 ; row >= 0 ; row--)
	{
		pTable->item(row, 3)->setText(UNSELECTED_TEXT);
	}
}

/*
**	FUNCTION
**		Display current state of eqp selection mask in table
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpTypeSelectorWidget::displaySelection(void)
{
	for(int row = pTable->rowCount() - 1 ; row >= 0 ; row--)
	{
		QString typeText = pTable->item(row, 0)->text();
		pTable->item(row, 3)->setText(eqpMask.contains(typeText.toInt()) ? SELECTED_TEXT : UNSELECTED_TEXT);
	}
}

/*
**	FUNCTION
**		Set selection of given functional type in table
**
**	ARGUMENTS
**		typeId	- ID of functional type whose selection in table shall be changed
**		set		- Flag indicating if type shall be selected (true) or
**					unselected (false) in table
**
**	RETURNS
**		true	- if selection state of functional type in table has been changed;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool EqpTypeSelectorWidget::selectTypeInTable(int typeId, bool set)
{
	for(int row = pTable->rowCount() - 1 ; row >= 0 ; row--)
	{
		QString typeText = pTable->item(row, 0)->text();
		if(typeText.toInt() == typeId)
		{
			QString selectText = pTable->item(row, 3)->text();
			if(selectText == SELECTED_TEXT)
			{
				if(set)
				{
					return false;
				}
			}
			else if(!set)
			{
				return false;
			}
			pTable->item(row, 3)->setText(set ? SELECTED_TEXT : UNSELECTED_TEXT);
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Slot activated when sector selection has been changed.
**		Update content of equipment types table according to devices
**		located in selected sectors.
**		Update equipment mask accoding to new set of equipment types
**		in table.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpTypeSelectorWidget::sectorSelectionChange(void)
{
	pTable->setRowCount(0);

	// Build list of selected sectors
	QStringList	sectorNames;
	pSectorSelector->getSelectedSectors(sectorNames);
	QList<Sector *> sectorList;
	DataPool &pool = DataPool::getInstance();
	foreach(QString sectName, sectorNames)
	{
		Sector *pSector = pool.findSectorData(sectName.toLatin1());
		if(pSector)
		{
			sectorList.append(pSector);
		}
	}
	bool notVac = pSectorSelector->isNonVacSelected();
	if((!sectorList.count()) && (!notVac))
	{
		return;
	}

	// Two arrays: functional types and number of devices of such type
	// in selected sectors
	const QList<FunctionalType *> &allTypes = FunctionalType::getTypes();
	int nTypes = allTypes.count();
	int *types = (int *)calloc(nTypes, sizeof(int));
	int *counts = (int *)calloc(nTypes, sizeof(int));

	int n;
	for(n = 0 ; n < nTypes ; n++)
	{
		FunctionalType *pType = allTypes.at(n);
		types[n] = pType->getType();
	}

	const QHash<QByteArray, Eqp *> &eqpDict = pool.getEqpDict();
	foreach(Eqp *pEqp, eqpDict)
	{
		if(!pEqp->getFunctionalType())
		{
			continue;
		}
		// Ignore sector VPI summary
		if(pEqp->getFunctionalType() == FunctionalType::SECT_VPI_SUM)
		{
			continue;
		}
		// Ignore real valves which are part of SUMMARY valves
		if((pEqp->getCtrlFamily() == 1) && (pEqp->getCtrlType() == 5) && (pEqp->getCtrlSubType() == 2))
		{
			continue;
		}
		// [VACCO-278] Not ignoring VPNs, NEGs are now on the device list
		/*if(pEqp->getFunctionalType() == FunctionalType::VPN)
		{
			continue;
		}*/
		bool inSectors = false;
		if(pEqp->getSectorBefore())
		{
			if(sectorList.indexOf(pEqp->getSectorBefore()) >= 0)
			{
				inSectors = true;
			}
		}
		if(!inSectors)
		{
			if(pEqp->getSectorAfter())
			{
				if(sectorList.indexOf(pEqp->getSectorAfter()) >= 0)
				{
					inSectors = true;
				}
			}
		}
		if(!inSectors)
		{
			const QList<Sector *> &extraSectors = pEqp->getExtraSectors();
			for(int extraIdx = 0 ; extraIdx < extraSectors.count() ; extraIdx++)
			{
				Sector *pSector = extraSectors.at(extraIdx);
				if(sectorList.indexOf(pSector) >= 0)
				{
					inSectors = true;
				}
			}
		}
		if(notVac)
		{
			if((!pEqp->getSectorBefore()) && (!pEqp->getSectorAfter()) && (!pEqp->getMainPart()))
			{
				switch(pEqp->getFunctionalType())	// Some types are ignored
				{
				case FunctionalType::VRPI:
					break;
				default:
					{
						QStringList devListDpeNames;
						pEqp->getDpesForDevList(devListDpeNames);
						inSectors = devListDpeNames.count() > 0;
					}
					break;
				}
			}
		}
		if(inSectors)
		{
			for(int n = nTypes - 1 ; n >= 0 ; n--)
			{
				if(types[n] == pEqp->getFunctionalType())
				{
					counts[n]++;
					break;
				}
			}
		}
	}

	// Fill in table
	for(n = 0 ; n < nTypes ; n++)
	{
		if(!counts[n])
		{
			continue;
		}
		FunctionalType *pType = allTypes.at(n);
		if(!pType)
		{
			continue;
		}
		int row = pTable->rowCount();
		pTable->setRowCount(row + 1);
		QTableWidgetItem *pItem = new QTableWidgetItem(QString::number(types[n]));
		pItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		pTable->setItem(row, 0, pItem);
		pItem = new QTableWidgetItem(pType->getDescription());
		pItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		pTable->setItem(row, 1, pItem);
		pItem = new QTableWidgetItem(QString::number(counts[n]));
		pItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		pTable->setItem(row, 2, pItem);
		pItem = new QTableWidgetItem(eqpMask.contains(types[n]) ? SELECTED_TEXT : UNSELECTED_TEXT);
		pItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		pTable->setItem(row, 3, pItem);
	}
	pTable->resizeRowsToContents();

	free((void *)counts);
	free((void *)types);
	emit selectionChanged();
}

/*
**	FUNCTION
**		Slot activated when mouse has been clicked in table. If click
**		occured in 3rd column - reverse equipment type selection
**
**	ARGUMENTS
**		row			- Table row
**		column		- Table column
**		button		- Button state
**		mousePos	- Mouse pointer position
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpTypeSelectorWidget::tableCellClicked(QTableWidgetItem *pItem)
{
	if(pItem->column() != 3)
	{
		return;
	}
	if(pItem->row() >= pTable->rowCount())
	{
		return;
	}
	QString typeText = pTable->item(pItem->row(), 0)->text();
	int type = typeText.toInt();
	if(eqpMask.contains(type))
	{
		eqpMask.remove(type);
		pTable->item(pItem->row(), 3)->setText(UNSELECTED_TEXT);
	}
	else
	{
		eqpMask.append(type);
		pTable->item(pItem->row(), 3)->setText(SELECTED_TEXT);
	}
	emit selectionChanged();
}



