#ifndef	VACICONVG_STD_H
#define	VACICONVG_STD_H

//	Superclass for simple VG_STD icons (VGM/VGR/VGP). It looks like the most (or even all) of VG
//	icons use the same rules for colors and ALSMOST the same image, the
//	difference in image is just small type-specific part of image drawn
//	inside colored circle with neutral (black) color.
//	So, all gaugesd which follow this rule may inherit this class and they
//	shall just implement drawSpecific() method - the method is called AFTER
//	circle and other parts are drawn.

#include "VacIcon.h"

class EqpVG_STD;

class VACCTLEWOUTIL_EXPORT VacIconVG_STD : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVG_STD(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVG_STD();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVG_STD *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter);

	virtual void drawSpecific(QPainter &painter) = 0;

	void setPainterForMainImage(QPainter &painter);

};

#endif	// VACICONVG_STD_H
