#include "VacCtlHistoryDataPvss.h"

#include "VacCtlStateArchive.h"
#include "ReplayPool.h"

#include <UIntegerVar.hxx>
#include <AnyTypeVar.hxx>
#include <BitVar.hxx>
#include <TimeVar.hxx>

#include <QVariant>

#ifndef PVSS_SERVER_VERSION
#include <QColor>
#endif

#include <unistd.h>

#ifndef WIN32
#include <pwd.h>
#endif

	static FunctionListRec fnList[] =
	{
		// Every structure in array contains:
		// Return-Value    function name
		// parameter list
		// true == thread-save

		//---------------------------------- State history ----------------------
		{
			INTEGER_VAR, "StateArchiveCreate",
			"()",
			false
		},
		{
			INTEGER_VAR, "StateArchiveSetValues",
			"(int id, string dpName, string dpeName, dyn_ulong values, dyn_time times, dyn_string &exceptionInfo)",
			false
		},
    	{
			INTEGER_VAR, "StateArchiveProcess",
			"(int id, dyn_string &exceptionInfo)",
			false
		},
		{
			INTEGER_VAR,
			"StateArchiveGetAllTs",
			"(int id, dyn_time &times, dyn_string &exceptionInfo)",
			false
		},
    	{
			INTEGER_VAR, "StateArchiveGetValues",
			"(int id, string dpName, time ts, dyn_string dpeNames, dyn_uint &dpeValues, dyn_string &exceptionInfo)",
			false
		},
    	{
			INTEGER_VAR,
			"StateArchiveGetPreviousTs",
			"(int id, string dpName, time ts, time &prevTs, dyn_string &exceptionInfo)",
			false
		},
    	{
			INTEGER_VAR, "StateArchiveGetNextTs",
			"(int id, string dpName, time ts, time *nextTs, dyn_string &exceptionInfo)",
			false
		},
    	{
			INTEGER_VAR, "StateArchiveNeighbourIndexes",
			"(int id, string dpName, time ts, int &prevIdx, int &nextIdx, dyn_string &exceptionInfo)",
			false
		},
    	{
			INTEGER_VAR, "StateArchiveClear",
			"(int id, dyn_string &exceptionInfo)",
			false
		},
    	{
			INTEGER_VAR, "StateArchiveDelete",
			"(int id, dyn_string &exceptionInfo)",
			false
		},

		//---------------------------------- Replay -----------------------------
    	{
			INTEGER_VAR,
			"VacReplayNextDpe",
			"(string &dpeName)",
			false
		},
    	{
			INTEGER_VAR,
			"VacReplayNextDpes",
			"(dyn_string &dpeNames)",
			false
		},
		{
			INTEGER_VAR,
			"VacReplaySetValues",
			"(string dpeName, bool useFirstValue, dyn_anytype values, dyn_time times, dyn_string &exceptionInfo)",
			false
		},
    	{
			INTEGER_VAR,
			"VacReplayProcess",
			"()",
			false
		},
		{
			INTEGER_VAR,
			"VacReplayGetNextEventData",
			"(string &dpeName, time &ts, anytype &value)",
			false
		},
		{
			INTEGER_VAR,
			"VacReplayGetPreviousEventData",
			"(string &dpeName, time &ts, anytype &value)",
			false
		},
		{
			INTEGER_VAR,
			"VacReplaySetPlayTime",
			"(time ts, bool forward)",
			false
		},
		{
			INTEGER_VAR,
			"VacReplayGetPlayData",
			"(string &dpeName, anytype &value)",
			false
		},
    	{
			INTEGER_VAR,
			"VacReplayClear",
			"()",
			false
		},
		
	};

CTRL_EXTENSION(VacCtlHistoryDataPvss, fnList)

/*
**
** FUNCTION
**		The following description has been borrowed from PVSS help:
**
**		The newExternHdl function has a fixed interface and must be
**		exported so that it can be called by the PVSS_II Manager.
**		The keyword '_declspec(dllexport)' has to be set only under
**		windows for exporting the function newExternHdl. Under Linux
**		this keyword is not necessary. 
**		The newExternHdl function must allocate a class object and return
**		the pointer to it.
**
** PARAMETERS
**		nextHdl	
**
** RETURNS
**		Pointer to new class instance
**
** CAUTIONS
**		None
*/
/*
BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl)
{

	// this line counts the number of functions you want to implement.
  	PVSSulong funcCount = sizeof(fnList) / sizeof(FunctionListRec);

	// now allocate the new instance
	return new VacCtlHistoryDataPvss(nextHdl, funcCount, fnList);
}
*/

/*
**
** FUNCTION
**		The following description has been borrowed from PVSS help:
**
**		This function is called every time we use one of the above
**		function names in a Ctrl script.
**		The argument is an aggregaton of function name, function number, 
**		the arguments passed to the Ctrl function, the "thread" context 
**		and user defined data.
**
** PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
** RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
const Variable *VacCtlHistoryDataPvss::execute(ExecuteParamRec &param)
{
	enum
	{
		StateArchiveCreate = 0,
		StateArchiveSetValues,
		StateArchiveProcess,
		StateArchiveGetAllTs,
		StateArchiveGetValues,
		StateArchiveGetPreviousTs,
		StateArchiveGetNextTs,
		StateArchiveNeighbourIndexes,
		StateArchiveClear,
		StateArchiveDelete,

		VacReplayNextDpe,
		VacReplayNextDpes,
		VacReplaySetValues,
		VacReplayProcess,
		VacReplayGetNextEventData,
		VacReplayGetPreviousEventData,
		VacReplaySetPlayTime,
		VacReplayGetPlayData,
		VacReplayClear
	};

	// TODO for the different return types you need corresponding static Variables
	static IntegerVar	integerVar;
	int					coco = 0;

	integerVar.setValue(0);
	param.thread->clearLastError();
	switch(param.funcNum)
	{
	case StateArchiveCreate:
		coco = VacCtlStateArchive::create();
		break;
	case StateArchiveSetValues:
		coco = processSetValues(param);
		break;
	case StateArchiveProcess:
		coco = processProcess(param);
		break;
	case StateArchiveGetAllTs:
		coco = processGetAllTs(param);
		break;
	case StateArchiveGetValues:
		coco = processGetValues(param);
		break;
	case StateArchiveGetPreviousTs:
		coco = processGetPreviousTs(param);
		break;
	case StateArchiveGetNextTs:
		coco = processGetNextTs(param);
		break;
	case StateArchiveNeighbourIndexes:
		coco = processNeighbourIndexes(param);
		break;
	case StateArchiveClear:
		processClear(param);
		break;
	case StateArchiveDelete:
		processDelete(param);
		break;

	case VacReplayNextDpe:
		coco = processReplayNextDpe(param);
		break;
	case VacReplayNextDpes:
		coco = processReplayNextDpes(param);
		break;
	case VacReplaySetValues:
		coco = processReplaySetValues(param);
		break;
	case VacReplayProcess:
		coco = processReplayProcess(param);
		break;
	case VacReplayGetNextEventData:
		coco = processReplayGetNextEventData(param);
		break;
	case VacReplayGetPreviousEventData:
		coco = processReplayGetPreviousEventData(param);
		break;
	case VacReplaySetPlayTime:
		coco = processReplaySetPlayTime(param);
		break;
	case VacReplayGetPlayData:
		coco = processReplayGetPlayData(param);
		break;
	case VacReplayClear:
		coco = processReplayClear(param);
		break;
	default:
		coco = -1;
		break;
	}
	integerVar.setValue(coco);
	return &integerVar;
}

/*
**	FUNCTION
**		Process call to StateArchiveSetValues()
**		PVSS CTRL arguments are:
**			int id
**			string dpName
**			string dpeName
**			dyn_uint values
**			dyn_time times
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processSetValues(ExecuteParamRec &param)
{
	IntegerVar	id;
	TextVar		dpName, dpeName;
	DynVar		values, times;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",				INTEGER_VAR,		&id,			NULL },
		{ "dpName",			TEXT_VAR,			&dpName,		NULL },
		{ "dpeName",		TEXT_VAR,			&dpeName,		NULL },
		{ "values",			DYNUINTEGER_VAR,	&values,		NULL },
		{ "times",			DYNTIME_VAR,		&times,			NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,		NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	VacCtlStateArchive *pPool = findStatePool(param, id, exceptionInfo);
	if(!pPool)
	{
		return -1;
	}

	((DynVar *)exceptionInfo)->clear();
	if(values.getArrayLength() != times.getArrayLength())
	{
		char errMsg[256];
		sprintf(errMsg, "Number of timestamps %d does not match number of values %d",
			times.getArrayLength(), values.getArrayLength());
		addExceptionInfo(param, exceptionInfo, errMsg);
		return -1;
	}
	Variable *pTimeVar, *pValueVar;
	char *pureDpName, *pureDpeName;
	if(!splitDpName(dpeName, &pureDpName, &pureDpeName))
	{
		QString errMsg("Failed to split <");
		errMsg += dpeName;
		errMsg += "> to DP and DPE names";
		addExceptionInfo(param, exceptionInfo, errMsg);
		return -1;
	}
	for(pTimeVar = times.getFirstVar(), pValueVar = values.getFirstVar() ; pTimeVar ;
			pTimeVar = times.getNextVar(), pValueVar = values.getNextVar())
	{
		QDateTime ts;
		ts.setTime_t(((TimeVar *)pTimeVar)->getSeconds());
		ts.setTime(ts.time().addMSecs(((TimeVar *)pTimeVar)->getMilli()));
		QVariant value((unsigned)((UIntegerVar *)pValueVar)->getValue());
		pPool->append(dpName, pureDpeName, ts, value);
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to StateArchivePRocess()
**		PVSS CTRL arguments are:
**			int id
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processProcess(ExecuteParamRec &param)
{
	IntegerVar	id;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",				INTEGER_VAR,	&id,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,	&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	VacCtlStateArchive *pPool = findStatePool(param, id, exceptionInfo);
	if(!pPool)
	{
		return -1;
	}
	pPool->sort();
	return 0;
}

/*
**	FUNCTION
**		Process call to StateArchiveGetAllTs()
**		PVSS CTRL arguments are:
**			int id
**			dyn_time &times
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processGetAllTs(ExecuteParamRec &param)
{
	IntegerVar	id;
	Variable	*times, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",				INTEGER_VAR,	&id,	NULL },
		{ "times",			DYNTIME_VAR,	NULL,	&times },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,	&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)times)->clear();
	VacCtlStateArchive *pPool = findStatePool(param, id, exceptionInfo);
	if(!pPool)
	{
		return -1;
	}
	int nAllTs;
	const QDateTime **allTs = pPool->getAllTs(nAllTs);
	for(int n = 0 ; n < nAllTs ; n++)
	{
		TimeVar ts(allTs[n]->toTime_t(), allTs[n]->time().msec());
		((DynVar *)times)->append(ts);
	}
	return 0;
}

/*
**	FUNCTION
**		Process call to StateArchiveGetValues()
**		PVSS CTRL arguments are:
**			int id
**			string dpName
**			time ts
**			dyn_string dpeNames
**			dyn_uint &dpeValues
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processGetValues(ExecuteParamRec &param)
{
	IntegerVar	id;
	TextVar		dpName;
	TimeVar		ts;
	DynVar		dpeNames;
	Variable	*dpeValues, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",				INTEGER_VAR,		&id,		NULL },
		{ "dpName",			TEXT_VAR,			&dpName,	NULL },
		{ "ts",				TIME_VAR,			&ts,		NULL },
		{ "dpeNames",		DYNTEXT_VAR,		&dpeNames,	NULL },
		{ "dpeValues",		DYNUINTEGER_VAR,	NULL,		&dpeValues },
		{ "exceptionInfo",	DYNTEXT_VAR,		NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)dpeValues)->clear();
	((DynVar *)exceptionInfo)->clear();

	VacCtlStateArchive *pPool = findStatePool(param, id, exceptionInfo);
	if(!pPool)
	{
		return -1;
	}

	// Convert list of DPE names to Qt-understandable format
	QList<QByteArray> dpeList;
	for(Variable *pDpeVar = dpeNames.getFirstVar() ; pDpeVar ; pDpeVar = dpeNames.getNextVar())
	{
		char *pureDpName, *pureDpeName;
		if(!splitDpName((const char *)((TextVar *)pDpeVar)->getValue(), &pureDpName, &pureDpeName))
		{
			QString errMsg("Failed to split <");
			errMsg += (const char *)((TextVar *)pDpeVar)->getValue();
			errMsg += "> to DP and DPE names";
			addExceptionInfo(param, exceptionInfo, errMsg);
			return -1;
		}
		dpeList.append(pureDpeName);
	}
	if(dpeList.isEmpty())
	{
		addExceptionInfo(param, exceptionInfo, "Empty list of DPEs");
		return -1;
	}

	// Get values
	QDateTime tsQt;
	tsQt.setTime_t(ts.getSeconds());
	tsQt.setTime(tsQt.time().addMSecs(ts.getMilli()));
	QList<QVariant *> values;
	bool coco = pPool->getDpValues(dpName, tsQt, dpeList, values);
	if(!coco)
	{
		return 0;
	}
	// Convert values to PVSS dyn_uint
	while(!values.isEmpty())
	{
		QVariant *pVariant = values.takeFirst();
		UIntegerVar var(pVariant->toUInt());
		((DynVar *)dpeValues)->append(var);
		delete pVariant;
	}
	return 1;
}

/*
**	FUNCTION
**		Process call to StateArchiveGetPreviousTs()
**		PVSS CTRL arguments are:
**			int id
**			string dpName
**			time ts
**			time &prevTs
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processGetPreviousTs(ExecuteParamRec &param)
{
	IntegerVar	id;
	TextVar		dpName;
	TimeVar		ts;
	Variable	*prevTs, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",				INTEGER_VAR,		&id,		NULL },
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "ts",				TIME_VAR,		&ts,		NULL },
		{ "prevTs",			TIME_VAR,		NULL,		&prevTs },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	VacCtlStateArchive *pPool = findStatePool(param, id, exceptionInfo);
	if(!pPool)
	{
		return -1;
	}
	QDateTime tsQt;
	tsQt.setTime_t(ts.getSeconds());
	tsQt.setTime(tsQt.time().addMSecs(ts.getMilli()));
	QDateTime prevTsQt;
	QString errMsg;
	int coco = pPool->getPreviousTs(dpName, tsQt, prevTsQt, errMsg);
	if(coco < 0)
	{
		addExceptionInfo(param, exceptionInfo, errMsg);
	}
	else if(coco > 0)	// previous found
	{
		((TimeVar *)prevTs)->setSeconds(prevTsQt.toTime_t());
		((TimeVar *)prevTs)->setMilli(prevTsQt.time().msec());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to StateArchiveGetNextTs()
**		PVSS CTRL arguments are:
**			int id
**			string dpName
**			time ts
**			time &nextTs
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processGetNextTs(ExecuteParamRec &param)
{
	IntegerVar	id;
	TextVar		dpName;
	TimeVar		ts;
	Variable	*nextTs, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",				INTEGER_VAR,		&id,		NULL },
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "ts",				TIME_VAR,		&ts,		NULL },
		{ "nextTs",			TIME_VAR,		NULL,		&nextTs },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	VacCtlStateArchive *pPool = findStatePool(param, id, exceptionInfo);
	if(!pPool)
	{
		return -1;
	}
	QDateTime tsQt;
	tsQt.setTime_t(ts.getSeconds());
	tsQt.setTime(tsQt.time().addMSecs(ts.getMilli()));
	QDateTime nextTsQt;
	QString errMsg;
	int coco = pPool->getNextTs(dpName, tsQt, nextTsQt, errMsg);
	if(coco < 0)
	{
		addExceptionInfo(param, exceptionInfo, errMsg);
	}
	else if(coco > 0)	// previous found
	{
		((TimeVar *)nextTs)->setSeconds(nextTsQt.toTime_t());
		((TimeVar *)nextTs)->setMilli(nextTsQt.time().msec());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to StateArchiveNeighbourIndexes()
**		PVSS CTRL arguments are:
**			int id
**			string dpName
**			time ts
**			int &prevIdx
**			int &nextIdx
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processNeighbourIndexes(ExecuteParamRec &param)
{
	IntegerVar	id;
	TextVar		dpName;
	TimeVar		ts;
	Variable	*prevIdx, *nextIdx, *exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",				INTEGER_VAR,		&id,		NULL },
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "ts",				TIME_VAR,		&ts,		NULL },
		{ "prevIdx",		INTEGER_VAR,	NULL,		&prevIdx },
		{ "nextIdx",		INTEGER_VAR,	NULL,		&nextIdx },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	VacCtlStateArchive *pPool = findStatePool(param, id, exceptionInfo);
	if(!pPool)
	{
		return -1;
	}
	QDateTime tsQt;
	tsQt.setTime_t(ts.getSeconds());
	tsQt.setTime(tsQt.time().addMSecs(ts.getMilli()));
	int prev, next;
	QString errMsg;
	int coco = pPool->getNeighbourIndexes(dpName, tsQt, prev, next, errMsg);
	if(coco < 0)
	{
		((IntegerVar *)prevIdx)->setValue(0);
		((IntegerVar *)nextIdx)->setValue(0);
		addExceptionInfo(param, exceptionInfo, errMsg);
	}
	else
	{
		((IntegerVar *)prevIdx)->setValue(prev);
		((IntegerVar *)nextIdx)->setValue(next);
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to StateArchiveClear()
**		PVSS CTRL arguments are:
**			int id
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processClear(ExecuteParamRec &param)
{
	IntegerVar	id;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",				INTEGER_VAR,	&id,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,	&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	VacCtlStateArchive *pPool = findStatePool(param, id, exceptionInfo);
	if(!pPool)
	{
		return -1;
	}
	pPool->clear();
	return 0;
}

/*
**	FUNCTION
**		Process call to StateArchiveDelete()
**		PVSS CTRL arguments are:
**			int id
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processDelete(ExecuteParamRec &param)
{
	IntegerVar	id;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "id",				INTEGER_VAR,	&id,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,	&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	if(!VacCtlStateArchive::deleteInstance(id.getValue()))
	{
		char errMsg[256];
		sprintf(errMsg, "State archive data instance with ID %d not found",
			(int)id.getValue());
		addExceptionInfo(param, exceptionInfo, errMsg);
		return -1;
	}
	return 0;
}

/*
**	FUNCTION
**		Find state archive data pool instance with given ID, if not found - add
**		error message to exceptionInfo
**
**	PARAMETERS
**		param			- All calling and execution parametrs can be accessed via this class
**		id				- Required ID of data pool
**		exceptionInfo	- Variable where error message shall be added if pool
**							with given ID was not found
**
**	RETURNS
**		Pointer to pool with required ID; or
**		NULL if such instance was not found
**
**	CAUTIONS
**		None
*/
VacCtlStateArchive *VacCtlHistoryDataPvss::findStatePool(ExecuteParamRec &param,
	IntegerVar &id, Variable *exceptionInfo)
{
	((DynVar *)exceptionInfo)->clear();
	VacCtlStateArchive *pPool = VacCtlStateArchive::findInstance(id.getValue());
	if(pPool)
	{
		return pPool;
	}
	char errMsg[256];
	sprintf(errMsg, "State archive data instance with ID %d not found",
		(int)id.getValue());
	addExceptionInfo(param, exceptionInfo, errMsg);
	return NULL;
}


/*
**	FUNCTION
**		Process call to VacReplayNextDpe()
**		PVSS CTRL arguments are:
**			string 	&dpeName
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processReplayNextDpe(ExecuteParamRec &param)
{
	Variable	*dpeName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpeName",	TEXT_VAR,	NULL,	&dpeName },
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	ReplayPool &pool = ReplayPool::getInstance();
	QByteArray dpe;
	int coco = pool.nextDpe(dpe);
	((TextVar *)dpeName)->setValue(dpe.constData());
	return coco;
}

/*
**	FUNCTION
**		Process call to VacReplayNextDpes()
**		PVSS CTRL arguments are:
**			dyn_string 	&dpeNames
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processReplayNextDpes(ExecuteParamRec &param)
{
	Variable	*dpeNames;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpeNames",	DYNTEXT_VAR,	NULL,	&dpeNames },
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	((DynVar *)dpeNames)->clear();
	ReplayPool &pool = ReplayPool::getInstance();
	QList<QByteArray> dpes;
	int coco = pool.nextDpes(dpes);
	for(int n = 0 ; n < dpes.count() ; n++)
	{
		addToDynStringVar((DynVar *)dpeNames, dpes.at(n).constData());
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacReplaySetValues()
**		PVSS CTRL arguments are:
**			string		dpeName
**			bool		useFirstValue
**			dyn_anytype values
**			dyn_time	times
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processReplaySetValues(ExecuteParamRec &param)
{
	TextVar		dpeName;
	DynVar		values, times;
	Variable	*exceptionInfo;
	BitVar		useFirstValue;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpeName",		TEXT_VAR,			&dpeName,		NULL },
		{ "useFirstValue",	BIT_VAR,			&useFirstValue,NULL },
		{ "values",			DYNANYTYPE_VAR,		&values,		NULL },
		{ "times",			DYNTIME_VAR,		&times,			NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,		NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	if(values.getNumberOfItems() != times.getNumberOfItems())
	{
		char buf[128];
		sprintf(buf, "Number of values %d does not match number of times %d",
			(int)values.getNumberOfItems(), (int)times.getNumberOfItems());
		addExceptionInfo(param, exceptionInfo, buf);
		return -1;
	}

	ReplayPool &pool = ReplayPool::getInstance();

	int coco = 0;
	Variable *pTimeVar, *pValueVar;
	for(pTimeVar = times.getFirstVar(), pValueVar = values.getFirstVar() ; pTimeVar ;
			pTimeVar = times.getNextVar(), pValueVar = values.getNextVar())
	{
		QDateTime ts;
		ts.setTime_t(((TimeVar *)pTimeVar)->getSeconds());
		ts.setTime(ts.time().addMSecs(((TimeVar *)pTimeVar)->getMilli()));
		QVariant *pValue = anyTypeToVariant(param, *((AnyTypeVar *)pValueVar));
		if(pValue)
		{
			coco = pool.setValue(dpeName, ts, *pValue, useFirstValue.isTrue());
			delete pValue;
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacReplayProcess()
**		PVSS CTRL arguments are:
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
** CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processReplayProcess(ExecuteParamRec &param)
{
	ReplayPool &pool = ReplayPool::getInstance();
	pool.process();
	return 0;
}

/*
**	FUNCTION
**		Process call to VacReplayGetNextEventData()
**		PVSS CTRL arguments are:
**			string		&dpeName
**			time		&ts
**			anytype		&value
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processReplayGetNextEventData(ExecuteParamRec &param)
{
	Variable	*dpeName, *ts, *value;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpeName",		TEXT_VAR,		NULL,		&dpeName },
		{ "ts",				TIME_VAR,		NULL,		&ts },
		{ "value",			ANYTYPE_VAR,	NULL,		&value }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	ReplayPool &pool = ReplayPool::getInstance();
	QByteArray dpeNameQt;
	QDateTime tsQt;
	QVariant valueQt;
	
	int coco = pool.getNextEventData(dpeNameQt, tsQt, valueQt);
	((TextVar *)dpeName)->setValue(dpeNameQt);
	((TimeVar *)ts)->setSeconds(tsQt.toTime_t());
	((TimeVar *)ts)->setMilli(tsQt.time().msec());
	if(!dpeNameQt.isEmpty())
	{
		variantToAnyType(param, valueQt, (AnyTypeVar *)value);
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacReplayGetPreviousEventData()
**		PVSS CTRL arguments are:
**			string		&dpeName
**			time		&ts
**			anytype		&value
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processReplayGetPreviousEventData(ExecuteParamRec &param)
{
	Variable	*dpeName, *ts, *value;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpeName",		TEXT_VAR,		NULL,		&dpeName },
		{ "ts",				TIME_VAR,		NULL,		&ts },
		{ "value",			ANYTYPE_VAR,	NULL,		&value }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	ReplayPool &pool = ReplayPool::getInstance();
	QByteArray dpeNameQt;
	QDateTime tsQt;
	QVariant valueQt;
	
	int coco = pool.getPreviousEventData(dpeNameQt, tsQt, valueQt);
	((TextVar *)dpeName)->setValue(dpeNameQt);
	((TimeVar *)ts)->setSeconds(tsQt.toTime_t());
	((TimeVar *)ts)->setMilli(tsQt.time().msec());
	if(!dpeNameQt.isEmpty())
	{
		variantToAnyType(param, valueQt, (AnyTypeVar *)value);
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacReplaySetPlayTime()
**		PVSS CTRL arguments are:
**			time		&ts
**			bool		&forward
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processReplaySetPlayTime(ExecuteParamRec &param)
{
	TimeVar		ts;
	BitVar		forward;

		// Parse arguments
	SPvssArg	args[] =
	{
		{ "ts",				TIME_VAR,		&ts,		NULL },
		{ "forward",		BIT_VAR,		&forward,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	ReplayPool &pool = ReplayPool::getInstance();
	QDateTime tsQt;
	tsQt.setTime_t(ts.getSeconds());
	tsQt.setTime(tsQt.time().addMSecs(ts.getMilli()));
	int coco = pool.setPlayTime(tsQt, forward.isTrue() == PVSS_TRUE);
	return coco;
}

/*
**	FUNCTION
**		Process call to VacReplayGetPlayData()
**		PVSS CTRL arguments are:
**			string		&dpeName
**			anytype		&value
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processReplayGetPlayData(ExecuteParamRec &param)
{
	Variable	*dpeName, *value;

		// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpeName",		TEXT_VAR,		NULL,		&dpeName },
		{ "value",			ANYTYPE_VAR,	NULL,		&value }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	ReplayPool &pool = ReplayPool::getInstance();
	QByteArray dpeNameQt;
	QVariant valueQt;
	int coco = pool.getPlayData(dpeNameQt, valueQt);
	((TextVar *)dpeName)->setValue(dpeNameQt);
	if(!dpeNameQt.isEmpty())
	{
		variantToAnyType(param, valueQt, (AnyTypeVar *)value);
	}
	return coco;
}

/*
**	FUNCTION
**		Process call to VacReplayClear()
**		PVSS CTRL arguments are:
**			None
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlHistoryDataPvss::processReplayClear(ExecuteParamRec &param)
{
	ReplayPool &pool = ReplayPool::getInstance();
	int coco = pool.reset();
	return coco;
}

