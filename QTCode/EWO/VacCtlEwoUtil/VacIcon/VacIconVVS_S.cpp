//	Implementation of VacIconVVS_S class
/////////////////////////////////////////////////////////////////////////////////


#include "VacIconVVS_S.h"

#include "EqpVV.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OPEN				0,255,0
#define COLOR_CLOSED			255,0,0
#define	COLOR_ERROR				255,102,255

// Colors for control and interlocks
#define COLOR_GREEN				0,255,0
#define COLOR_YELLOW			255,255,0
#define COLOR_RED				255,0,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define	RR1_OPEN		(0x00020000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_CLOSED		(0x00010000)

#define RR2_CIR_DP_AFTER	(0x2000)
#define	RR2_CIR_DP_BEFORE	(0x1000)
#define RR2_CIR_GBL_AFTER	(0x0800)
#define RR2_CIR_GBL_BEFORE	(0x0400)
#define RR2_CIR_LCL_AFTER	(0x0200)
#define RR2_CIR_LCL_BEFORE	(0x0100)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVVS_S::VacIconVVS_S(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(29, 21);
	setInterlockGeometry();
}

VacIconVVS_S::~VacIconVVS_S()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for all interlock rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVS_S::setInterlockGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		if(beamDirection == VacIconContainer::LeftToRight)
		{
			gblBefore.setRect(1, 1, 4, 6);
			dpBefore.setRect(1, 7, 4, 7);
			lclBefore.setRect(1, 14, 4, 6);
			gblAfter.setRect(24, 1, 4, 6);
			dpAfter.setRect(24, 7, 4, 7);
			lclAfter.setRect(24, 14, 4, 6);
		}
		else
		{
			gblAfter.setRect(1, 1, 4, 6);
			dpAfter.setRect(1, 7, 4, 7);
			lclAfter.setRect(1, 14, 4, 6);
			gblBefore.setRect(24, 1, 4, 6);
			dpBefore.setRect(24, 7, 4, 7);
			lclBefore.setRect(24, 14, 4, 6);
		}
		break;
	case VacIconContainer::Down:
		if(beamDirection == VacIconContainer::LeftToRight)
		{
			gblAfter.setRect(1, 1, 4, 6);
			dpAfter.setRect(1, 7, 4, 7);
			lclAfter.setRect(1, 14, 4, 6);
			gblBefore.setRect(24, 1, 4, 6);
			dpBefore.setRect(24, 7, 4, 7);
			lclBefore.setRect(24, 14, 4, 6);
		}
		else
		{
			gblBefore.setRect(1, 1, 4, 6);
			dpBefore.setRect(1, 7, 4, 7);
			lclBefore.setRect(1, 14, 4, 6);
			gblAfter.setRect(24, 1, 4, 6);
			dpAfter.setRect(24, 7, 4, 7);
			lclAfter.setRect(24, 14, 4, 6);
		}
		break;
	case VacIconContainer::Left:
		if(beamDirection == VacIconContainer::LeftToRight)
		{
			gblBefore.setRect(1, 25, 6, 4);
			dpBefore.setRect(7, 25, 7, 4);
			lclBefore.setRect(15, 25, 6, 4);
			gblAfter.setRect(1, 0, 6, 4);
			dpAfter.setRect(7, 0, 7, 4);
			lclAfter.setRect(14, 0, 6, 4);
		}
		else
		{
			gblAfter.setRect(1, 25, 6, 4);
			dpAfter.setRect(7, 25, 7, 4);
			lclAfter.setRect(15, 25, 6, 4);
			gblBefore.setRect(1, 0, 6, 4);
			dpBefore.setRect(7, 0, 7, 4);
			lclBefore.setRect(14, 0, 6, 4);
		}
		break;
	case VacIconContainer::Right:
		if(beamDirection == VacIconContainer::LeftToRight)
		{
			gblAfter.setRect(1, 25, 6, 4);
			dpAfter.setRect(7, 25, 7, 4);
			lclAfter.setRect(15, 25, 6, 4);
			gblBefore.setRect(1, 0, 6, 4);
			dpBefore.setRect(7, 0, 7, 4);
			lclBefore.setRect(14, 0, 6, 4);
		}
		else
		{
			gblBefore.setRect(1, 25, 6, 4);
			dpBefore.setRect(7, 25, 7, 4);
			lclBefore.setRect(15, 25, 6, 4);
			gblAfter.setRect(1, 0, 6, 4);
			dpAfter.setRect(7, 0, 7, 4);
			lclAfter.setRect(14, 0, 6, 4);
		}
		break;
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVVS_S::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		return VacIconGeometry(29, 21, 14, 10, 0);
		break;
	default:
		break;
	}
	return VacIconGeometry(21, 29, 10, 14, 0);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVS_S::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(29);
		size.setHeight(21);
		break;
	default:
		size.setWidth(21);
		size.setHeight(29);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setInterlockGeometry();
}

/*
**	FUNCTION
**		Override method of VacIcon: interlocks geometry of this icon
**		depends on beam direction
**
**	ARGUMENTS
**		newDirection	- new beam direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVS_S::setBeamDirection(VacIconContainer::BeamDirection newDirection)
{
	VacIcon::setBeamDirection(newDirection);
	setInterlockGeometry();
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVS_S::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVV"), "VacIconVVS_S::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVV *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVS_S::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVVS_S::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("RR2");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVVS_S::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVVS_S::getToolTip(const QPoint &pos, QString &text, QRect &rect)
{
	if(pEqp && connected)
	{
		// Where is the point? If in one of interlocks - show interlock text,
		// otherwise show valve name and state
		if(gblBefore.contains(pos))
		{
			text = pEqp->getVisibleName();
			text += ": Global before";
			rect = gblBefore;
		}
		else if(dpBefore.contains(pos))
		{
			text = pEqp->getVisibleName();
			text += ": dP before";
			rect = dpBefore;
		}
		else if(lclBefore.contains(pos))
		{
			text = pEqp->getVisibleName();
			text += ": Local before";
			rect = lclBefore;
		}
		else if(gblAfter.contains(pos))
		{
			text = pEqp->getVisibleName();
			text += ": Global after";
			rect = gblAfter;
		}
		else if(dpAfter.contains(pos))
		{
			text = pEqp->getVisibleName();
			text += ": dP after";
			rect = dpAfter;
		}
		else if(lclAfter.contains(pos))
		{
			text = pEqp->getVisibleName();
			text += ": Local after";
			rect = lclAfter;
		}
		else
		{
			pEqp->getToolTipString(text, mode);
			rect.setRect((width() / 2) - 9, (height() / 2) - 9, 19, 19);
		}
	}
	else
	{
		rect = this->rect();
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVVS_S::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0, rr2 = 0;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		rr2 = pEqp->getRR2(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	/*
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
	*/

	// Save painter coordinate system - interlocks will be drawn
	// in original (non-translated) coordinate system
	painter.save();

	// Prepare painter's coordinate system for image drawing
	// Coordinate system (0,0) is center of widget
	painter.translate(width() / 2, height() / 2);
	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	// Coordinate system is ready - draw
	drawImage(painter, ctlStatus, rr1, rr2, plcAlarm);

	// Restore coordinate system to original and draw interlocks
	painter.restore();
	drawInterlocks(painter, ctlStatus, rr1, rr2, plcAlarm);

	// Draw selection ON TOP of image
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
}

void VacIconVVS_S::drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm)
{
	// Draw vacuum pipe - two short lines
	QPen pen(pipeColor, pipeWidth);
	painter.setPen(pen);
	painter.drawLine(-14, 0, -9, 0);
	painter.drawLine(9, 0, 15, 0);	// FlatCapStyly does not cover the end point of line,
									// hence end point is 14+1=15
	
	// Draw valve image
	setPainterForControl(painter);

	// 1) Rectangle connecting big rectangle on top with valve image
	QPolygon points(4);
	points.setPoint(0, -2, -6);
	points.setPoint(1, 2, -6);
	points.setPoint(2, 2, 0);
	points.setPoint(3, -2, 0);
	painter.drawPolygon(points);

	// 2) big rectangle on top
	points.setPoint(0, -4, -9);
	points.setPoint(1, 4, -9);
	points.setPoint(2, 4, -6);
	points.setPoint(3, -4, -6);
	painter.drawPolygon(points);

	// 3) Valve image
	setPainterForMainImage(painter, ctlStatus, rr1, rr2, plcAlarm);

	points.setPoint(0, -9, -9);
	points.setPoint(1, 9, 9);
	points.setPoint(2, 9, -9);
	points.setPoint(3, -9, 9);
	painter.drawPolygon(points);
}

void VacIconVVS_S::drawInterlocks(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm)
{
	// Interlocks before valve
	QColor color = colorForInterlock(ctlStatus, rr1, rr2, plcAlarm, RR2_CIR_GBL_BEFORE);
	painter.fillRect(gblBefore, color);

	color = colorForInterlock(ctlStatus, rr1, rr2, plcAlarm, RR2_CIR_DP_BEFORE);
	painter.fillRect(dpBefore, color);
	
	color = colorForInterlock(ctlStatus, rr1, rr2, plcAlarm, RR2_CIR_LCL_BEFORE);
	painter.fillRect(lclBefore, color);

	
	// Interlocks after valve
	color = colorForInterlock(ctlStatus, rr1, rr2, plcAlarm, RR2_CIR_GBL_AFTER);
	painter.fillRect(gblAfter, color);

	color = colorForInterlock(ctlStatus, rr1, rr2, plcAlarm, RR2_CIR_DP_AFTER);
	painter.fillRect(dpAfter, color);

	color = colorForInterlock(ctlStatus, rr1, rr2, plcAlarm, RR2_CIR_LCL_AFTER);
	painter.fillRect(lclAfter, color);
}

void VacIconVVS_S::setPainterForControl(QPainter &painter)
{
	QColor color(COLOR_UNDEFINED), borderColor;
	if(pEqp)
	{
		int order;
		pEqp->getInterlockColor(mode, color, borderColor, order, NULL);
	}
	painter.setBrush(color);
	painter.setPen(Qt::black);
}

void VacIconVVS_S::setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned /* rr2 */, bool plcAlarm)
{
	QColor color(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr1 & RR1_OPEN)
				{
					if(rr1 & RR1_CLOSED)
					{
						color.setRgb(COLOR_ERROR);
					}
					else
					{
						color.setRgb(COLOR_OPEN);
					}
				}
				else if(rr1 & RR1_CLOSED)
				{
					color.setRgb(COLOR_CLOSED);
				}
				else
				{
					color.setRgb(COLOR_UNDEFINED);
				}
			}
		}
		break;
	}
	QBrush brush(color);
	painter.setBrush(brush);
	painter.setPen(Qt::black);
}

QColor VacIconVVS_S::colorForInterlock(Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm, unsigned bit)
{
	QColor color(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr2 & bit)
				{
					color.setRgb(COLOR_RED);
				}
				else
				{
					color.setRgb(COLOR_GREEN);
				}
			}
		}
		break;
	}
	return color;
}
