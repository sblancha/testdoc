#ifndef	FALSHINGRECT
#define	FALSHINGRECT

// Flashing rectangle - part of VVS_LHC icon to show current pressure interlock

#include <qwidget.h>
#include <qtimer.h>

class FlashingRect : public QWidget
{
	Q_OBJECT

public:
	FlashingRect(QWidget *parent, Qt::WindowFlags f = 0);
	~FlashingRect();

	// Access
	inline const QColor &getColor(void) const { return color; }
	inline void setColor(const QColor &value) { color = value; }
protected:
	// Timer for flashing
	QTimer	*pTimer;

	// Half-period for flashing
	int		halfPeriod;

	// Color for flashing
	QColor	color;

	// Flag indicationg if interior shall be filled with color
	bool	fillInterior;

	virtual void paintEvent(QPaintEvent *pEvent);

private slots:
	void timerDone(void);
};

#endif	// FALSHINGRECT
