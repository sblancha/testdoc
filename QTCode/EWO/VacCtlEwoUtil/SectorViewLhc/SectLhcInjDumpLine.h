#ifndef	SECTLHCINJDUMPLINE_H
#define	SECTLHCINJDUMPLINE_H

// Injection/dump line for LHC sector view

#include "SectLine.h"

class SectLhcInjDumpLine : public SectLine
{
	Q_OBJECT

public:
	SectLhcInjDumpLine();
	virtual ~SectLhcInjDumpLine();

	static SectLhcInjDumpLine *create(QWidget *parent, BeamLine *pLine, int parentVacType);

	// Access
	inline int getConnPoint(void) const { return connPoint; }
	inline void setConnPoint(int value) { connPoint = value; }

protected:
	// Vacuum type of parent line (hence, vacuum type of this line too)
	int		vacType;

	// X-coordinate of connection point in parent line
	int		connPoint;

	virtual void checkForReverse(void);
};

#endif	// SECTLHCINJDUMPLINE_H
