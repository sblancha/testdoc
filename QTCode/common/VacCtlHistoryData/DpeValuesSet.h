#ifndef	DPEVALUESSET_H
#define	DPEVALUESSET_H

#include "VacCtlHistoryDataExport.h"

//	Class holding all values for single DPE

#include "DpeValue.h"

#include <qstring.h>
#include <qlist.h>

class VACCTLHISTORYDATA_EXPORT DpeValuesSet
{
public:
	DpeValuesSet(const char *name);
	DpeValuesSet(const DpeValuesSet &source);
	~DpeValuesSet();
	DpeValuesSet & operator = (const DpeValuesSet &other);

	void append(const QDateTime &ts, const QVariant &value);
	int addValue(const QDateTime &ts, const QVariant &value, bool useFirstValue);
	void sort(void);
	void clear(void);
	DpeValue *findValueAt(const QDateTime &ts);
	DpeValue *findPreviousValue(const QDateTime &ts);
	DpeValue *findPreviousValue(const QDateTime &ts, int &valueIdx);
	DpeValue *findNextValue(const QDateTime &ts);
	DpeValue *findNextValue(const QDateTime &ts, int &valueIdx);

	const DpeValue *getValueAt(int index) const;
	void setIndex(const QDateTime &ts, bool forward);
	inline const DpeValue *getCurrent(void) const { return getValueAt(index); }
	inline const DpeValue *getNext(void) const { return getValueAt(index + 1); }
	inline const DpeValue *getPrevious() const { return getValueAt(index - 1); }

	// Access
	inline const QByteArray &getName(void) const { return name; }
	inline int getIndex(void) const { return index; }
	inline void setIndex(int value) { index = value; }
	inline int getRefCount(void) { return refCount; }
	inline void setRefCount(int count) { refCount = count; }
	inline bool isReady(void) { return ready; }
	inline void setReady(bool ready) { this->ready = ready; }
	inline bool isNotReported(void) { return notReported; }
	inline void setNotReported(bool value) { notReported = value; }

protected:
	// Name of this DPE
	QByteArray			name;

	// List of all values for this DPE
	QList<DpeValue *>	*pValues;

	// Recent index, used by VacCtlStateArchive class, this class only keeps
	// the value
	int					index;

	// Reference counter for this DPE, used by ReplayPool
	int	refCount;

	// Flag indicating if DPE is being processed by PVSS (used by ReplayPool)
	bool		ready;

	// Flag indicating if next/previous value for this DPE was NOT played by PVSS
	// (used by ReplayPool)
	bool		notReported;
};

#endif	// DPEVALUESSET_H
