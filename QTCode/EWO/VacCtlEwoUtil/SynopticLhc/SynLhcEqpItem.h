#ifndef	SYNLHCEQPITEM_H
#define	SYNLHCEQPITEM_H

// Equipment items (both passive and active) of LHC synoptic and sector views
#include "ValveInterlockSource.h"

#include "DataEnum.h"

#include "VacIcon.h"

#include <QRect>
#include <QColor>

#include <stdio.h>

class Eqp;
//class VacIcon;
class VacEqpTypeMask;

#include <QWidget>

class SynLhcEqpItem
{
public:
	SynLhcEqpItem(Eqp *pEqp);
	~SynLhcEqpItem();

	void initIcon(QWidget *parent, bool forBeamVacuum, VacEqpTypeMask &eqpMask, bool eqpAboveLine);
	void setPipeColor(QColor &color);

	bool isCryoThermometer(void);

	const char *getName(void) const;
	const char *getNameBefore(bool reverse) const;
	const char *getNameAfter(bool reverse) const;
	void moveHor(int delta);
	void moveVert(int delta);
	void moveIcon(void);
	void deleteIcon(void);
	void nullIcon(void);

	// Access
	inline QRect &getRect(void) { return rect; }
	void setRect(QRect &rect);
	inline QPoint &getConnPoint(void) { return connPoint; }
	inline void setConnPoint(QPoint point) { connPoint = point; }
	inline Eqp *getEqp(void) const { return pEqp; }
	inline float getStart(void) const { return start; }
	inline void setStart(float coord) { start = coord; }
	inline VacIcon *getIcon(void) const { return pIcon; }
	inline void setIcon(VacIcon *pIcon) { this->pIcon = pIcon; }
	inline void setMode(DataEnum::DataMode newMode) { if(pIcon) { pIcon->setMode(newMode); } }
	int getIconHeight(void);
	inline int getType(void) const { return type; }
	inline void setType(int type) { this->type = type; }
	inline int getIconY(void) const { return iconY; }
	inline int getGroupId(void) const { return groupId; }
	inline void setGroupId(int id) { groupId = id; }
	inline int getVerticalPos(void) const { return verticalPos; }
	inline void setVerticalPos(int pos) { verticalPos = pos; }
	inline bool isSectorBorder(void) const { return sectorBorder; }
	inline void setSectorBorder(bool border) { sectorBorder = border; }
	inline bool isCommonReady(void) const { return commonReady; }
	inline void setCommonReady(bool ready) { commonReady = ready; }
	inline bool isStartReady(void) const { return startReady; }
	inline void setStartReady(bool flag) { startReady = flag; }
	inline bool isEndReady(void) const { return endReady; }
	inline void setEndReady(bool flag) { endReady = flag; }
	inline bool isOuterEqp(void) const { return outerEqp; }
	inline void setOuterEqp(bool flag) { outerEqp = flag; }
	inline bool isIntlSourceForward(void) { return intlSourceForward; }
	inline void setIntlSourceForward(bool flag) { intlSourceForward = flag; }
	inline bool isIntlSourceBackward(void) { return intlSourceBackward; }
	inline void setIntlSourceBackward(bool flag) { intlSourceBackward = flag; }

	inline int getStartOrder(void) const { return startOrder; }
	inline void setStartOrder(int order) { startOrder = order; }
	inline int getEndOrder(void) const { return endOrder; }
	inline void setEndOrder(int order) { endOrder = order; }
	inline int getCorrDelta(void) { return corrDelta; }
	inline void setCorrDelta(int delta) { corrDelta = delta; }
	void dump(FILE *pFile, int idx);

protected:
	// Geometry of equipment image on unlimited drawing area
	QRect		rect;

	// Connection point
	QPoint		connPoint;

	// Pointer to equipment
	Eqp			*pEqp;

	// Pointer to icon of this item (active equipment only)
	VacIcon		*pIcon;

	// Start coordinate of device used for geometry calculations
	float		start;

	// Offset of icon X coordinate from connection point
	int			iconY;

	// Group ID - used for processing multiple connection at the same coordinate
	int			groupId;

	// Item type - see EqpType.h
	int			type;

	// Used for processing multiple connection at the same coordinate
	int			verticalPos;

	int			startOrder;		// Start processing order - for debugging only
	int			endOrder;		// End processing order - for debugging only
	int			corrDelta;		// Correction of screen position

	// true if device is sector border
	bool		sectorBorder;

	// true if common connection lines have been drawn
	bool		commonReady;

	// true if equipment is 'outer', i.e.: it is added to this line,
	// all geometry is calculated in this line, but it will be drawn
	// in another line using another rules. This is used for devices
	// of vacuum type C located in sectors C with common vacuum volume
	bool		outerEqp;

	// Flag indicating if start of long passive equipment has got screen coordinate
	bool		startReady;
	
	// Flag indicating if end of long passive equipment has got screen coordinate
	bool		endReady;

	// L.Kopylov 28.01.2010 addition to identify interlock sources for valves on synoptic

	// Flag to identify if this device is interlock source for valve which is after this device on view
	bool		intlSourceForward;

	// Flag to identify if this device is interlock source for valve which is before this device on view
	bool		intlSourceBackward;
};

#endif	// SYNLHCEQPITEM_H
