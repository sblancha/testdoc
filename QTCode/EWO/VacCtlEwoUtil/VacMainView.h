#ifndef VACMAINVIEW_H
#define	VACMAINVIEW_H

#include "VacCtlEwoUtilExport.h"

// Superclass for Qt controls on main views of PVSS applications.
// The class defines common signals to be emitted by all main view
// widgets and provides THE ONLY instance of Qt widget on main view
// which is used as parent by other views (if any).
//
// The class also serves as static holder of drawing properties for
// other views (dialogs)

#include <QWidget>
#include <QPrinter>
#include <QList>

#include "DataEnum.h"	// for DataMode enum

class Eqp;
#include <QDateTime>
#include <QVariant>

class VACCTLEWOUTIL_EXPORT VacMainView : public QWidget
{
	Q_OBJECT

public:
	VacMainView(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~VacMainView();

	static VacMainView *getInstance(void);
	static const QList<VacMainView *> &getInstanceList(void) { return instanceList; }

	// Dialog opening
	enum
	{
		DialogSynoptic = 1,
		DialogProfile = 2,
		DialogSector = 3,
		DialogProfileAllSPS = 4,
		DialogSynopticLHC = 5,
		DialogProfileLHC = 6,
		DialogProfileAllLhcInOut = 7,
		DialogProfileAllLhcBeam = 8,
		DialogSynopticAllLhc = 9,
		DialogSectorLhc = 10,
		DialogMobileHistory = 11,
		DialogBakeoutRackProfile = 12,
		DialogBakeoutRackDetails = 13,
		DialogRackDiagram = 14,
		DialogInterlockSchematic = 15
	};

	static int openDialog(int type, unsigned vacTypes, const char *firstSector,
			const char *lastSector, DataEnum::DataMode mode, QStringList &errList);
	static int openDocumentationDialog(int type, unsigned vacTypes, const char *equipmentName,
		DataEnum::DataMode mode, QStringList &errList);
	static void makeDeviceVisible(Eqp *pEqp);

	static void printWidget(QWidget *pWidget);

	static int getAllHistoryInstaces(QList<int> &idList);
	static int createHistory(void);
	static int getHistoryFilterType(int instanceId);
	static int setHistoryFilterType(int instanceId, int type);
	static void clearHistory(int instanceId);
	static int addToHistory(Eqp *pEqp, int historyDepth);
	static int addToHistory(int instanceId, Eqp *pEqp, const char *colorString, int historyDepth);
	static int addDpeToHistory(Eqp *pEqp, const char *dpeName, const char *yAxisName, int historyDepth);
	static int addDpeToHistory(int instanceId, Eqp *pEqp, const char *dpeName, const char *yAxisName, const char *colorString, int historyDepth);
	static int addToBitHistory(Eqp *pEqp, const char *dpe, int bitNbr, const char *bitName, int historyDepth);
	static int addToBitHistory(int instanceId, Eqp *pEqp, const char *dpe, int bitNbr, const char *bitName, const char *colorString, int historyDepth);
	static bool addHistoryValue(int id, const char *dpe, bool bitState, int bitNbr, const QDateTime &time, const QVariant &value);
	static void finishHistoryRange(int id, const char *dpe, bool bitState, int bitNbr);

	static void getAllHistoryItems(int instanceId, QStringList &dps, QStringList &dpes, int **bitNbrs, QStringList &colors, QStringList &axisTypeNames);
	static int getHistoryTimeScaleParam(int instanceId, QDateTime &start, QDateTime &end, bool &online, bool &logScale);
	static int setHistoryTimeScaleParam(int instanceId, const QDateTime &start, const QDateTime &end, bool online, bool logScale);
	static int getVerticalScalesParams(int id, int &nScales, int **ppTypes, bool **ppLogs, float **ppMins, float **ppMaxs);
	static int setVerticalScaleParams(int id, int type, bool log, float min, float max);
	static const QString &getHistoryTitle(int id);
	static int setHistoryTitle(int id, const char *title);
	static int getHistoryWindowGeometry(int id, int &x, int &y, int &w, int &h);
	static int setHistoryWindowGeometry(int id, int x, int y, int w, int h);
	static int setHistoryConfigName(int id, const QString &name);
	static const QString &getHistoryConfigName(int id);
	static int getHistoryAllComments(int id, QStringList &comments);
	static int setHistoryAllComments(int id, QStringList comments);
	

	static void replayDialogOpened(QWidget *dialog);
	static void replayDialogDeleted(QWidget *dialog);
	static void deleteReplayDialogs(void);

	static int setDpHistoryDepth(const char *dpName, const QDateTime &absStartTime);

	static void setSynLineColor(int vacType, QColor &color);

	// Methods which shall be implemented by subclasses
	virtual void dialogRequest(int type, unsigned vacTypes, const QStringList &sectorList,
			DataEnum::DataMode mode) = 0;

	// Resource access
	static const QColor &getSynPipeColor(void);
	static void setSynPipeColor(const QColor &color);

	static const QColor &getSynAltPipeColor(void);
	static void setSynAltPipeColor(const QColor &color);

	static const QColor &getSynPipePartColor(void);
	static void setSynPipePartColor(const QColor &color);

	static const QColor &getSynPassiveLineColor(void);
	static void setSynPassiveLineColor(const QColor &color);

	static const QColor &getSynPassiveFillColor(void);
	static void setSynPassiveFillColor(const QColor &color);

	static const QColor &getSynSectorColor(void);
	static void setSynSectorColor(const QColor &color);

	static int getSynPipeWidth(void);
	static void setSynPipeWidth(int width);

	static QColor &getGraphAxisColor(void);
	static void setGraphAxisColor(const QColor &color);

	static int getProfileBarWidth(void);
	static void setProfileBarWidth(int width);

	static QColor &getProfileAltSectorColor(void);
	static void setProfileAltSectorColor(const QColor &color);

	static QColor &getProfileAltBarColor(void);
	static void setProfileAltBarColor(const QColor &color);

	static int getProfileRedrawDelay(void);
	static void setProfileRedrawDelay(int delay);

	static QColor &getHistoryNegativeColor(void);
	static void setHistoryNegativeColor(const QColor &color);

	static int getHistorySelectWidth(void);
	static void setHistorySelectWidth(int width);

	static int getProfileSectorDrawMethod(void);
	static void setProfileSectorDrawMethod(int method);

	static QColor &getProfileBackColorRedBeam(void);
	static void setProfileBackColorRedBeam(const QColor &color);

	static QColor &getProfileBackColorBlueBeam(void);
	static void setProfileBackColorBlueBeam(const QColor &color);

	static QColor &getProfileBackColorCrossBeam(void);
	static void setProfileBackColorCrossBeam(const QColor &color);

	static QColor &getProfileBackColorQrl(void);
	static void setProfileBackColorQrl(const QColor &color);

	static QColor &getProfileBackColorCryo(void);
	static void setProfileBackColorCryo(const QColor &color);

	static QColor &getProfileTempQrlBColor(void);
	static void setProfileTempQrlBColor(const QColor &color);

	static QColor &getProfileTempQrlCColor(void);
	static void setProfileTempQrlCColor(const QColor &color);

	static QColor &getProfileTempCmColor(void);
	static void setProfileTempCmColor(const QColor &color);

	static QColor &getProfileTempRfColor(void);
	static void setProfileTempRfColor(const QColor &color);

	static QColor &getProfileTempBsColor(void);
	static void setProfileTempBsColor(const QColor &color);

	static int getPollingPeriod(void);
	static void setPollingPeriod(int period);

	static inline const QString &getFilePath(void) { return filePath; }
	static inline void setFilePath(const QString &path) { filePath = path; }

	inline bool isPvssEvents(void) const { return pvssEvents; }
	void setPvssEvents(bool flag);

	void emitSectorDoubleClick(const QString &sector, int mode)
	{
		emit sectorDoubleClick(sector, mode);
	}
	void emitEqpDoubleClick(const QString &dpName, int mode)
	{
		emit eqpDoubleClick(dpName, mode);
	}

signals:
	void selectionChanged(void);
	void sectorMouseDown(int button, int x, int y, const char *sectorName);
	void dpMouseDown(int button, int mode, int x, int y, int extra, const char *dpName);
	void dpIconMouseDown(int button, int mode, int x, int y, int extra, const char *dpName);
	void rackIconMouseDown(int button, int mode, int x, int y, int extra, const char *eqpName);
	void pollingPeriodChanged(int period);
	void historySaveRequest(int id);
	void historySaveMultiRequest(int id);
	void historyLoadRequest(int id);
	void historyFillNumberRequest(int id);
	void sectorDoubleClick(const QString &sector, int mode);
	void eqpDoubleClick(const QString &dpName, int mode);
	void synSectorMouseDown(int button, int mode,int x, int y, const char *sectorName, bool selected); // [VACCO-929]

protected:
	// The only instance of main view widget
	static VacMainView	*pMainView;

	// List of instances, pMainView is not not 'the only', but primary
	static QList<VacMainView *>	instanceList;

	// Path for saving file(s)
	static QString		filePath;

	// List of dialog instances opened in Replay mode
	static QList<QWidget *>	replayDialogs;

	// Pipe color for synoptic drawing
	static QColor		synPipeColor;

	// Alternative pipe color for synoptoc drawing
	static QColor		synAltPipeColor;

	// Color for drawing part of vacuum pipe on synoptic with another color
	static QColor		synPipePartColor;

	// Color for outline of passive elements on synoptic
	static QColor		synPassiveLineColor;

	// Color for filling passive elements on synoptic
	static QColor		synPassiveFillColor;

	// Color for drawing sector border and sector names on synoptic
	static QColor		synSectorColor;

	// Width of line for drawing vacuum pipe on synoptic
	static int			synPipeWidth;

	// Color used for all axies in graphs
	static QColor		graphAxisColor;

	// Relative width of one bar on profile graph [0...100]
	static int			profileBarWidth;

	// Color for alternate sector area filling on profile graph
	static QColor		profileAltSectorColor;

	// Alternative color for flashing bars in profile graph
	static QColor		profileAltBarColor;

	// Delay for redrawing profile graph
	static int			profileRedrawDelay;

	// Color for negative values in history
	static QColor		historyNegativeColor;

	// Line width for selected device in history
	static int			historySelectWidth;

	// Method for drawing sectors in LHC profile view (1=borders, 2=hatchfill);
	static int			profileSectorDrawMethod;

	// Background color for red beam sector in LHC profile
	static QColor		profileBackColorRedBeam;

	// Background color for blue beam sector in LHC profile
	static QColor		profileBackColorBlueBeam;

	// Background color for cross beam sector in LHC profile
	static QColor		profileBackColorCrossBeam;

	// Background color for QRL sector in LHC profile
	static QColor		profileBackColorQrl;

	// Background color for CRYO sector in LHC profile
	static QColor		profileBackColorCryo;

	// Color for thermometer on QRL line B in profile
	static QColor		profileTempQrlBColor;

	// Color for thermometer on QRL line C in profile
	static QColor		profileTempQrlCColor;

	// Color for thermometer on magnet cold mass in profile
	static QColor		profileTempCmColor;

	// Color for thermometer on RF in profile
	static QColor		profileTempRfColor;

	// Color for thermometer on beam screen in profile
	static QColor		profileTempBsColor;

	// QPrinter is used by all QDialogs to print themselves
	static QPrinter	*pPrinter;

	// Period for polling
	static int			pollingPeriod;

	// Normally only one main view is expected, but sometimes
	// second one can be created (MainViewTabled in one of panels)
	// This flag allows to distinguish secondary main view
	bool					secondary;

	// Flag indicating if this instance shall produce events for PVSS.
	// By default only 1st instance produces events
	bool					pvssEvents;

	// For secondary main view: flag indicating if this instance is connected to
	// dpMouseDown() signal of primary main view
	bool					connectedToPrimary;

	static int openSectorView(unsigned vacTypes, const char *firstSector,
		const char *lastSector, DataEnum::DataMode mode, QStringList &errList);
	static int openSynoptic(unsigned vacTypes, const char *firstSector,
		const char *lastSector, DataEnum::DataMode mode, QStringList &errList);
	static int openProfile(unsigned vacTypes, const char *firstSector,
		const char *lastSector, DataEnum::DataMode mode, QStringList &errList);
	static int openProfileAllSPS(unsigned vacTypes, const char *firstSector,
		const char *lastSector, DataEnum::DataMode mode, QStringList &errList);
	static int openSynopticLhc(unsigned vacTypes, const char *firstSector,
		const char *lastSector, DataEnum::DataMode mode, QStringList &errList);
	static int openProfileLhc(unsigned vacTypes, const char *firstSector,
		const char *lastSector, DataEnum::DataMode mode, QStringList &errList);
	static int openProfileAllLhcInOut(QStringList &errList);
	static int openProfileAllLhcBeam(QStringList &errList);
	static int openSynopticAllLhc(DataEnum::DataMode mode, QStringList &errList);
	static int openSectorViewLhc(const char *firstSector,
		const char *lastSector, DataEnum::DataMode mode, QStringList &errList);
	static int openMobileHistory(QStringList &errList);
	static int openBakeoutRackProfile(QStringList &errList);
	static int openBakeoutRackDetails(QStringList &errList);
	static int openRackDiagram(const char * equipmentName, QStringList &errList);
	static int openInterlockSchematic(const char * equipmentName, QStringList &errList);

	void notifyPollingPeriod(int period);
};

#endif	// VACMAINVIEW_H
