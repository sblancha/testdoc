
// Constants/functions related to 'control status' of devices.
// Creation of this functionality is caused by request to have
// devices in very special statuses - 'NotControl' and 'NotConnected'
//  See VTL cases 2761 and 2673
//
//  Requirement is realized by adding new DPE 'EqpStatus' to many (though
//  not all) DP types.


// Possible values for EpqStatus DPE, values repeat enum Eqp::CtlStatus, see Eqp.h
const int EQP_CTL_STATUS_USE = 0;
const int EQP_CTL_STATUS_NOT_USED = 1;
const int EQP_CTL_STATUS_TEMP_REMOVE = 2;
const int EQP_CTL_STATUS_PERM_REMOVE = 3;
const int EQP_CTL_STATUS_NOT_CONTROL = 4;
const int EQP_CTL_STATUS_NOT_CONNECTED = 5;

// IDs for items in popup menu to execute actions 'set not connected'/'set connected'
// PUSH_BUTTON,<action_name>,<ID>,1
const int MENU_ID_NOT_CONNECTED = 104;
const int MENU_ID_CONNECTED = 105;

// Special value to be passed in mapping to LhcVacSetDeviceNotConnected() if value
// of DPE shall NOT be set when setting to NotConnected
const anytype NO_VALUE_CHANGE;
const float PR_VALUE_OVERRANGE;


// Check if item for changing EqpStatus shall be added to popup menu
// for given device.
string LhcVacMenuItemForEqpCtlStatus(const string dpName, dyn_string &exceptionInfo)
{
  int eqpCtlStatus = LhcVacEqpCtlStatus(dpName, DIALOG_MODE_ONLINE);
  unsigned actToFind = 0;
  switch(eqpCtlStatus)
  {
  case EQP_CTL_STATUS_NOT_CONTROL:
    return "";  // Nothing can be done with non-controlled device
  case EQP_CTL_STATUS_NOT_CONNECTED:
    actToFind = LVA_DEVICE_SET_CONNECTED;
    break;
  default:
    actToFind = LVA_DEVICE_SET_NOT_CONNECTED;
    break;
  }

  // Does the type of device support such action?
  string dpType;
  int typeIdx = LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo);
  if(typeIdx <= 0)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacPopupSymbolMenu: unsupported DP type <" + dpType + ">", "");
    return result;
  }
  int actIdx = dynContains(glDevAllowedActs[typeIdx], actToFind);
  if(actIdx <= 0)
  {
    return "";
  }
  // Is current user allowed to execute such action?
  bool allowed = false;
  LhcVacEnoughPrivilegeForAction(dpName, dpType, makeDynAnytype(actToFind), false, allowed, exceptionInfo);
  if(!allowed)
  {
    return "";
  }

  // Prepare result
  string result = "PUSH_BUTTON,";
  switch(actToFind)
  {
  case LVA_DEVICE_SET_CONNECTED:
    result += "Set Connected," + MENU_ID_CONNECTED + ",1";
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    result += "Set Not Connected," + MENU_ID_NOT_CONNECTED + ",1";
    break;
  }
  return result;
}

void LhcVacSetDeviceNotConnected(string dpName, mapping dpesToDeactivate,
                                 dyn_string &exceptionInfo)
{
  if(mappinglen(dpesToDeactivate) < 1)
  {
    return;
  }

  // Deactivate addresses
  dyn_string dpeNames;
  dyn_bool   flags;
  for(int idx = 1 ; idx <= mappinglen(dpesToDeactivate) ; idx++)
  {
    string dpeName = dpName + "." + mappingGetKey(dpesToDeactivate, idx);
    if(dpExists(dpeName))
    {
      bool configExists = false, isActive = false;
      dyn_anytype config;
      dyn_string localExceptionInfo;
      fwPeriphAddress_get(dpeName, configExists, config, isActive, localExceptionInfo);
      if(configExists)
      {
        dpeName += ":_address.._active";
        dynAppend(dpeNames, dpeName);
        dynAppend(flags, false);
      }
    }
  }
  if(dynlen(dpeNames) > 0)
  {
    if(dpSet(dpeNames, flags) < 0)
    {
      dyn_errClass err = getLastError();
      fwException_raise(exceptionInfo, "ERROR", "LhcVacSetDeviceNotConnected(): deactivate address(es) failed: " +
                        err, "");
      return;
    }
  }

  // Set special values to DPEs
  dynClear(dpeNames);
  dyn_anytype values;
  for(int idx = 1 ; idx <= mappinglen(dpesToDeactivate) ; idx++)
  {
    string dpeName = dpName + "." + mappingGetKey(dpesToDeactivate, idx);
    int valueType = getType(mappingGetValue(dpesToDeactivate, idx));
    if(dpExists(dpeName) && (valueType != ANYTYPE_VAR) && (valueType != MIXED_VAR))
    {
      dynAppend(dpeNames, dpeName);
      dynAppend(values, mappingGetValue(dpesToDeactivate, idx));
    }
  }
  if(dynlen(dpeNames) > 0)
  {
    if(dpSet(dpeNames, values) < 0)
    {
      dyn_errClass err = getLastError();
      fwException_raise(exceptionInfo, "ERROR", "LhcVacSetDeviceNotConnected(): set special value(s) failed: " +
                        err, "");
      return;
    }
  }

  // Finally - set EqpStatus
  if(dpSet(dpName + ".EqpStatus", EQP_CTL_STATUS_NOT_CONNECTED) < 0)
  {
    dyn_errClass err = getLastError();
    fwException_raise(exceptionInfo, "ERROR", "LhcVacSetDeviceNotConnected(): set status NotConnected failed: " +
                      err, "");
  }
}

void LhcVacSetDeviceConnected(string dpName, mapping dpesToActivate,
                              dyn_string &exceptionInfo)
{
  if(mappinglen(dpesToActivate) < 1)
  {
    return;
  }

  // Deactivate addresses
  dyn_string dpeNames;
  dyn_bool   flags;
  for(int idx = 1 ; idx <= mappinglen(dpesToActivate) ; idx++)
  {
    string dpeName = dpName + "." + mappingGetKey(dpesToActivate, idx);
    if(dpExists(dpeName))
    {
      bool configExists = false, isActive = false;
      dyn_anytype config;
      dyn_string localExceptionInfo;
      fwPeriphAddress_get(dpeName, configExists, config, isActive, localExceptionInfo);
      if(configExists)
      {
        dpeName += ":_address.._active";
        dynAppend(dpeNames, dpeName);
        dynAppend(flags, true);
      }
    }
  }
  if(dynlen(dpeNames) > 0)
  {
    if(dpSet(dpeNames, flags) < 0)
    {
      dyn_errClass err = getLastError();
      fwException_raise(exceptionInfo, "ERROR", "LhcVacSetDeviceConnected(): activate address(es) failed: " +
                        err, "");
      return;
    }
  }

  // Finally - set EqpStatus
  if(dpSet(dpName + ".EqpStatus", EQP_CTL_STATUS_USE) < 0)
  {
    dyn_errClass err = getLastError();
    fwException_raise(exceptionInfo, "ERROR", "LhcVacSetDeviceNotConnected(): set status NotConnected failed: " +
                      err, "");
  }
}
