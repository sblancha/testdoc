#ifndef	VACICONVGA_VELO_H
#define	VACICONVGA_VELO_H

//	Icon for absolue gauge in VELO

#include "VacIconVG.h"

class VACCTLEWOUTIL_EXPORT VacIconVGA_VELO : public VacIconVG
{
public:
	VacIconVGA_VELO(QWidget *parent, Qt::WindowFlags f = 0);

protected:
	virtual void drawSpecific(QPainter &painter);
};


#endif	// VACICONVGA_VELO_H
