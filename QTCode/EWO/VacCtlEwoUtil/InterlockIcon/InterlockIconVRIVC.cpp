#include "InterlockIconVRIVC.h"

#include "qpainter.h"

InterlockIconVRIVC::InterlockIconVRIVC(QWidget * parent) :
InterlockIcon(parent)
{
	

}

InterlockIconVRIVC::~InterlockIconVRIVC()
{

}

void InterlockIconVRIVC::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	QFont calibri("Calibri", 10, QFont::Bold);
	QFont calibriBig("Calibri", 12);
	//draw title
	QPen whitePen(Qt::white);

	


	painter.setBrush(this->beamColor);
	painter.setPen(whitePen);
	painter.setFont(calibri);

	painter.drawRect(0, 5, 100, 50);
	painter.drawText(15, 5, 70, 50, Qt::AlignCenter | Qt::TextWrapAnywhere, this->name);
	
	painter.setPen(Qt::black);
	painter.setBrush(Qt::white);

	painter.drawRect(100 + 5, 0, 500, 60);
	painter.drawLine(100 + 5, 30, 600 + 5, 30);

	for (int i = 1; i <= 4; i++)
	{
		painter.drawLine(100 + 5 + 100 * i, 0, 105 + 100 * i, 60);
	}

	painter.setFont(calibriBig);

	painter.drawText(105, 0, 100, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "IN A");
	painter.drawText(205, 0, 100, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "IN B");
	painter.drawText(305, 0, 100, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "IN C");
	painter.drawText(405, 0, 100, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "IN D");

	painter.drawText(105, 30, 100, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "OUT A");
	painter.drawText(205, 30, 100, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "OUT B");
	painter.drawText(305, 30, 100, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "OUT C");
	painter.drawText(405, 30, 100, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "OUT D");

	painter.drawText(505, 0, 100, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "PLC 1");
	painter.drawText(505, 30, 100, 30, Qt::AlignCenter | Qt::TextWrapAnywhere, "PLC 2");


}

void InterlockIconVRIVC::moveIcon(int x, int y)
{
	this->move(x, y - 60 / 2);
}

QPoint InterlockIconVRIVC::getConnectionPoint(QString connectorName)
{
	int baseX = this->x();
	int baseY = this->y();

	if (QString::compare(connectorName, "InputA") == 0)
	{
		baseX += 155;
	}
	else if (QString::compare(connectorName, "InputB") == 0)
	{
		baseX += 255;
	}
	else if (QString::compare(connectorName, "InputC") == 0)
	{
		baseX += 355;
	}
	else if (QString::compare(connectorName, "InputD") == 0)
	{
		baseX += 455;
	}
	else if (QString::compare(connectorName, "PLC1") == 0)
	{
		baseX += 605;
		baseY += 15;
	}
	else if (QString::compare(connectorName, "PLC2") == 0)
	{
		baseX += 605;
		baseY += 45;
	}
	else if (QString::compare(connectorName, "OutputA") == 0)
	{
		baseX += 155;
		baseY += 60;
	}
	else if (QString::compare(connectorName, "OutputB") == 0)
	{
		baseX += 255;
		baseY += 60;
	}
	else if (QString::compare(connectorName, "OutputC") == 0)
	{
		baseX += 355;
		baseY += 60;
	}
	else if (QString::compare(connectorName, "OutputD") == 0)
	{
		baseX += 455;
		baseY += 60;
	}
	return QPoint(baseX, baseY);	
	
}

void InterlockIconVRIVC::getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber)
{
	if (connectorName.contains("In"))
	{
		direction = InterlockIconConnection::Up;
		offset = 50;
	}
	else if (connectorName.contains("PLC"))
	{
		direction = InterlockIconConnection::Right;
		offset = 80;
	}
	else if (connectorName.contains("Out"))
	{
		direction = InterlockIconConnection::Down;
		offset = 50;	
	}
	else
	{
		direction = InterlockIconConnection::Undefined;
		offset = 0;
	}
	writeCableNumber = false;
}

QRect InterlockIconVRIVC::getBoundingRect()
{
	QPoint topLeft = QPoint(this->x(), this->y());
	QRect boundingRect(topLeft, topLeft + QPoint(605, 60));

	return boundingRect;
}