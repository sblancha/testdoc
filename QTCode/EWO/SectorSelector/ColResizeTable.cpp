//	Implementation of ColResizeTable class
/////////////////////////////////////////////////////////////////////////////////

#include "ColResizeTable.h"

void ColResizeTable::resizeEvent(QResizeEvent *pEvent)
{
	QTableWidget::resizeEvent(pEvent);
	int nCols = columnCount();
	if(!nCols)
	{
		return;
	}
	int colWidth = width() / nCols;
	for(int col = 0 ; col < nCols ; col++)
	{
		setColumnWidth(col, colWidth);
	}
}

