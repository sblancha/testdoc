#ifndef SYNVIEW_H
#define SYNVIEW_H

// Synoptic view for simple machines like PS, SPS...

#include "SynLine.h"
#include "SynopticWidget.h"
#include "SynLhcSectorLabel.h" //name says LHC but class works for all machines

#include "VacEqpTypeMask.h"
#include "Eqp.h"
#include "EqpMsgCriteria.h"

#include <qwidget.h>
#include <qlist.h>

#include <stdio.h>

class SynView : public SynopticWidget
{
	Q_OBJECT

public:
	SynView(QWidget *parent = NULL, Qt::WindowFlags f = 0);
	~SynView();

	static SynView *create(QWidget *parent, Sector *pFirstSector, Sector *pLastSector,
		DataEnum::DataMode mode);

	virtual void analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allCryoEqpMask,
		VacEqpTypeMask &allBeamEqpMask);
	virtual int applyMask(VacEqpTypeMask &mask);

	// The following method shall be implemented by SectView - to be used instead of applyMask()
//	virtual void buildSectorView(void) { Q_ASSERT(0); }

	virtual bool findDevicePosition(Eqp *pEqp, int &position, bool &visible);

	virtual void setViewStart(int viewStart, int viewWidth, bool dragging);

	// Implement abstract method of SynopticWidget
	virtual bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	virtual void dump(const char *fileName);

	inline QList<SynLine *> getLines(void){ return lines; };


	virtual void showMobilesNotConnected(void);
	virtual void hideMobilesNotConnected(void);


	void connectSectorSelection(SynLine * pLine); 
	void disconnectSectorSelection(SynLine * pLine); 
	SynLhcSectorLabel * getSectorFromXY(int mouseX, int mouseY); 

signals:
	void mouseDown(int button, int mode, int localX, int localY, int globalX, int globalY, int extra); 
	void synSectorMouseDown(int button, int mode, int x, int y, const char *sectorName, bool selected); 


public slots:
	bool synSectorClicked(int button, int mode, int localX, int localY, int globalX, int globalY, int extra); 
	void forceRedraw(); 
	
protected:
	// List of synoptic lines
	QList<SynLine *>	lines;

	// Required width of picture on unlimited draw area
	int					totalWidth;

	// Required height of picture on unlimited draw area
	int					totalHeight;

	// Last known start of visible area
	int					viewStart;

	// Main equipment type mask
	VacEqpTypeMask		eqpMask;

	// Pointer to first line for vertical interline connection (LTE&LTL sector)
	SynLine				*pFirstConnLine;

	// Pointer second line for vertical interline connection 
	SynLine				*pSecondConnLine;

	// X-coordinate for interline connection
	int					interLineX;

	// Data acquisition mode for this view
	DataEnum::DataMode	mode;

	// Flag indicating if redrawing is required after mobile equipment state change
	bool				redrawRequiredForMobile;

	virtual void buildView(VacEqpTypeMask &mask);
	void buildGeometry(void);
	void buildVertRelations(void);
	void manualVertRelations(void);
	void buildHorRelations(void);
	void calcHorRelation(SynLine *pLine);
	bool calcConnectGeometry(SynLine *pLine, SynLine *pParentLine, int parentEqpIdx,
			bool isStart);
	void checkForOverlap(void);
	void moveLineAndChildren(SynLine *pParent, SynLine *pTopMost, int eqpIdx, int distance);
	void moveChildLines(SynLine *pParent, long delta, QList<ChildSynLine *> *pChildren);
	SynLine *getParentLine(SynLine *pChild, const Eqp *pConnEqp, int &parentEqpIdx);

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void mousePressEvent(QMouseEvent *pEvent);

private slots:
	void coldexStateChange(bool isIn, bool isOut);
};

#endif	// SYNVIEW_H
