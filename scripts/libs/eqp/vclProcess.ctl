
/***********************************************************************************
****************** Constants & functions for process driven by sequencer ***********
***********************************************************************************/
// for 32-bit wr1
const uint PROCESS_W32_AUTO =        0x08000000u;
const uint PROCESS_W32_MANUAL =      0x04000000u;
const uint PROCESS_W32_ON =          0x02000000u;
const uint PROCESS_W32_OFF =         0x01000000u;
const uint PROCESS_W32_PART_OPEN =   0x00010000u;
const uint PROCESS_W32_PART_CLOSE =  0x00020000u;
const uint PROCESS_W32_LOCAL =       0x10000000u;
const uint PROCESS_W32_REMOTE =      0x20000000u;
const uint PROCESS_W32_FORCE =       0x40000000u;
const uint PROCESS_W32_RESET =       0x80000000u;
// for 16-bit wr1
const uint PROCESS_W16_AUTO =        0x0800u;
const uint PROCESS_W16_MANUAL =      0x0400u;
const uint PROCESS_W16_ON =          0x0200u;
const uint PROCESS_W16_OFF =         0x0100u;
const uint PROCESS_W16_LOCAL =       0x1000u;
const uint PROCESS_W16_REMOTE =      0x2000u;
const uint PROCESS_W16_FORCE =       0x4000u;
const uint PROCESS_W16_RESET =       0x8000u;
const uint PROCESS_W16_BYPASS =      0x4000u;

const unsigned PROCESS_VV_W_REMOTE = 0x2000u,
	PROCESS_VV_W_OPEN =    0x0200u,
	PROCESS_VV_W_CLOSE =   0x0100u,
  PROCESS_VV_W_MANUAL =  0x0400u;


void LhcVacActionPossible_Process(string dpName, string dpType, dyn_anytype act, bool &possible,
  dyn_string &exceptionInfo)
{
  if(dpType == "VPG_6A01")
  {  
    string modeDpe = dpName + ".RR2";
    unsigned rr2; 
    dpGet(modeDpe, rr2);
    int currentMode = (rr2 & 0xFF00) >> 8;
  
    dyn_string exceptionInfo;
    int family,type,subType; 
    int mode;
    LhcVacGetCtlAttributes(dpName, family, type, subType);
    string enabled = VacResourcesGetValue(dpType + ".Subtype_" + subType + ".Transition_" + currentMode, VAC_RESOURCE_STRING, "");
    if(enabled == "")
    {
      possible = true;
      return;
    }
    
    possible =false;
// DebugN("subType", subType, currentMode);   
    if((act[1] & LVA_DEVICE_MASK) != 0)
    {
      mode = -1;
      if(act[1] == (LVA_DEVICE_MASK | LVA_VPG_PUMP))
      {
        mode = 2;
      }
      else if(act[1] == (LVA_DEVICE_MASK | LVA_VPG_VENT))
      {
        mode = 4;      
      }
      else if(act[1] == (LVA_DEVICE_MASK | LVA_VPG_VENT_TURBO))
      {
        mode = 3;      
      }
      if(mode > 0)
      { 
        if(enabled[mode -1] == 'Y')
        {
           possible = true;
        }

      }
    } 
//    DebugN("Result;", enabled, mode, possible);
  }
  else if(dpType == "VPG_6E01")
  {

    string modeDpe = dpName + ".RR2";
    unsigned rr2; 
    dpGet(modeDpe, rr2);
    int currentMode = (rr2 & 0xFF00) >> 8;
  
    dyn_string exceptionInfo;
    int family,type,subType; 
    int mode;
    LhcVacGetCtlAttributes(dpName, family, type, subType);
    string enabled = VacResourcesGetValue(dpType + ".Subtype_" + subType + ".Transition_" + currentMode, VAC_RESOURCE_STRING, "");
    possible =false;
// DebugN("subType", subType, currentMode);   
    if((act[1] & LVA_DEVICE_MASK) != 0)
    {
      mode = -1;
      if(act[1] == (LVA_DEVICE_MASK | LVA_VPG_PUMP))
      {
        mode = 2;
      }
      else if(act[1] == (LVA_DEVICE_MASK | LVA_DEVICE_STOP_VPG))
      {
        mode = 1;      
      }
      if(mode > 0)
      { 
        if(enabled[mode -1] == 'Y')
        {
           possible = true;
        }

      }
    }     
  }
  else
  {
    possible = true;
  }
}

// for VPG_6A01 (6A) WR1 - 16 bit register
int LhcVacDeviceAction_ProcessVPG_6A(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  unsigned  newWr1;
  string    vvr1, vvr2, vvr;  
  mapping   mConnectDpes;
  
//DebugN("LhcVacDeviceAction_Process", action);  
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
    newWr1 = PROCESS_W16_MANUAL | PROCESS_W16_REMOTE | PROCESS_W16_ON | (2 & 0xFF); //0x2600u;
    break;
  case LVA_DEVICE_OFF:
    newWr1 = PROCESS_W16_MANUAL | PROCESS_W16_REMOTE | PROCESS_W16_OFF; //0x2500u;
    break;
  case LVA_DEVICE_MANUAL:
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_MANUAL;
    break;
  case LVA_DEVICE_AUTO:
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_AUTO;
    break;
  case LVA_DEVICE_RESET:
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_RESET;
    break;
  case LVA_DEVICE_FORCE: 
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_FORCE;
    break;
  case LVA_DEVICE_PROC_SET_MODE:
    newWr1 = PROCESS_W16_REMOTE | (action[2] & 0x3F);  // Bits 7 and 6 are PS protection commands
 //   dpSetWait(dpName + ".ModeSet", action[2] & 0xFF);
    break;
  case LVA_DEVICE_PS_PROT_ON:
    newWr1 = PROCESS_W16_REMOTE | 0x0080u;
    break;
  case LVA_DEVICE_PS_PROT_OFF:
    newWr1 = PROCESS_W16_REMOTE | 0x0040u;
    break;
  case LVA_DEVICE_VVR1_OPEN:
    LhcVacGetAttribute(dpName, "VVR1", vvr1);
    newWr1 = PROCESS_VV_W_REMOTE | PROCESS_VV_W_OPEN;
    if(vvr1 != "")
    {
      dpName = vvr1;
    }  
    else
    {
      DebugTN("LhcVacDeviceAction_ProcessVPG_6A: no VVR1 found for " + dpName);
      return LVA_OK;;
    }  
    break;
  case LVA_DEVICE_VVR2_OPEN:
    LhcVacGetAttribute(dpName, "VVR2", vvr2);
    newWr1 = PROCESS_VV_W_REMOTE | PROCESS_VV_W_OPEN;
    if(vvr2 != "")
    {
      dpName = vvr2;
    }  
    else
    {
      DebugTN("LhcVacDeviceAction_ProcessVPG_6A: no VVR2 found for " + dpName);
      return LVA_OK;;
    }  
    break;
  case LVA_DEVICE_VVR1_CLOSE:
    LhcVacGetAttribute(dpName, "VVR1", vvr1);
    newWr1 = PROCESS_VV_W_REMOTE | PROCESS_VV_W_CLOSE;
    if(vvr1 != "")
    {
      dpName = vvr1;
    }  
    else
    {
      DebugTN("LhcVacDeviceAction_ProcessVPG_6A: no VVR1 found for " + dpName);
      return LVA_OK;
    }  
    break;
  case LVA_DEVICE_VVR2_CLOSE:
    LhcVacGetAttribute(dpName, "VVR2", vvr2);
    newWr1 = PROCESS_VV_W_REMOTE | PROCESS_VV_W_CLOSE;
    if(vvr2 != "")
    {
      dpName = vvr2;
    }  
    else
    {
      DebugTN("LhcVacDeviceAction_ProcessVPG_6A: no VVR2 found for " + dpName);
      return LVA_OK;
    }  
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  case LVA_DEVICE_VVR1_MANUAL:
    LhcVacGetAttribute(dpName, "VVR1", vvr1);
    newWr1 = PROCESS_VV_W_REMOTE | PROCESS_VV_W_MANUAL;
    if(vvr1 != "")
    {
      dpName = vvr1;
    }  
    else
    {
      DebugTN("LhcVacDeviceAction_ProcessVPG_6A: no VVR1 found for " + dpName);
      return LVA_OK;
    }  
    break;
  case LVA_DEVICE_VVR2_MANUAL:
    LhcVacGetAttribute(dpName, "VVR2", vvr2);
    newWr1 = PROCESS_VV_W_REMOTE | PROCESS_VV_W_MANUAL;
    if(vvr2 != "")
    {
      dpName = vvr2;
    }  
    else
    {
      DebugTN("LhcVacDeviceAction_ProcessVPG_6A: no VVR2 found for " + dpName);
      return LVA_OK;
    }  
    break;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_ProcessVPG_6A: unknown action " + act, "");
    return LVA_FAILURE;
  }
  dpSetWait(dpName + ".WR1", newWr1);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return LVA_FAILURE;
  }
  return LVA_OK;
}
// for VPG_6E01 ) WR1 - 16 bit register
int LhcVacDeviceAction_ProcessVPG_6E(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  unsigned  newWr1;
  mapping   mConnectDpes;
//DebugN("LhcVacDeviceAction_Process", action);  
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
    newWr1 = PROCESS_W16_MANUAL | PROCESS_W16_REMOTE | PROCESS_W16_ON; //0x2600u;
    break;
  case LVA_DEVICE_OFF:
    newWr1 = PROCESS_W16_MANUAL | PROCESS_W16_REMOTE | PROCESS_W16_OFF; //0x2500u;
    break;
  case LVA_DEVICE_MANUAL:
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_MANUAL;
    break;
  case LVA_DEVICE_RESET:
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_RESET;
    break;
  case LVA_DEVICE_BYPASS:
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_BYPASS;
    break;
  case LVA_DEVICE_PROC_SET_MODE:
    newWr1 = PROCESS_W16_REMOTE | (action[2] & 0xFF);
 //   dpSetWait(dpName + ".ModeSet", action[2] & 0xFF);
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_ProcessVPG_6E: unknown action " + act, "");
    return LVA_FAILURE;
  }

  dpSetWait(dpName + ".WR1", newWr1);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return LVA_FAILURE;
  }
  return LVA_OK;
}

// for BGI (6B) WR1 - 32 bit register
int LhcVacDeviceAction_ProcessBGI_6B(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  unsigned  newWr1;
  mapping   mConnectDpes;
//DebugN("LhcVacDeviceAction_Process", action);  
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
    newWr1 = PROCESS_W32_MANUAL | PROCESS_W32_REMOTE | PROCESS_W32_ON; //0x26000000;
    break;
  case LVA_DEVICE_OFF:
    newWr1 = PROCESS_W32_MANUAL | PROCESS_W32_REMOTE | PROCESS_W32_OFF; // 0x25000000;
    break;
  case LVA_DEVICE_PART_OPEN:
    newWr1 = PROCESS_W32_PART_OPEN | PROCESS_W32_REMOTE;
    break;
  case LVA_DEVICE_PART_CLOSE:
    newWr1 = PROCESS_W32_PART_CLOSE | PROCESS_W32_REMOTE;
    break;
  case LVA_DEVICE_MANUAL:
    newWr1 = PROCESS_W32_REMOTE | PROCESS_W32_MANUAL;
    break;
  case LVA_DEVICE_AUTO:
    newWr1 = PROCESS_W32_REMOTE | PROCESS_W32_AUTO;
    break;
  case LVA_DEVICE_RESET:
    newWr1 = PROCESS_W32_REMOTE | PROCESS_W32_RESET;
    break;
  case LVA_DEVICE_FORCE: 
    newWr1 = PROCESS_W32_REMOTE | PROCESS_W32_FORCE;
    break;
  case LVA_DEVICE_PROC_SET_MODE:
    newWr1 = PROCESS_W32_REMOTE | (action[2] & 0xFF);
//    dpSetWait(dpName + ".ModeSet", action[2] & 0xFF);
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["InjCountdown"] = NO_VALUE_CHANGE;
    mConnectDpes["RR4"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["InjCountdown"] = NO_VALUE_CHANGE;
    mConnectDpes["RR4"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_ProcessBGI_6B: unknown action " + act, "");
    return LVA_FAILURE;
  }

  dpSetWait(dpName + ".WR1", newWr1);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return LVA_FAILURE;
  }
  return LVA_OK;
}  

// for VINJ_6B02 WR1 - 16 bit register
int LhcVacDeviceAction_ProcessINJ_6B02(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  unsigned  newWr1;
  mapping   mConnectDpes;
//DebugN("LhcVacDeviceAction_Process", action);  
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
    newWr1 = PROCESS_W16_MANUAL | PROCESS_W16_REMOTE | PROCESS_W16_ON; //0x2600u;
    break;
  case LVA_DEVICE_OFF:
    newWr1 = PROCESS_W16_MANUAL | PROCESS_W16_REMOTE | PROCESS_W16_OFF; //0x2500u;
    break;
  case LVA_DEVICE_MANUAL:
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_MANUAL;
    break;
  case LVA_DEVICE_AUTO:
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_AUTO;
    break;
  case LVA_DEVICE_RESET:
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_RESET;
    break;
  case LVA_DEVICE_FORCE: 
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_FORCE;
    break;
  case LVA_DEVICE_PROC_SET_MODE:
    newWr1 = PROCESS_W16_REMOTE | (action[2] & 0xFF);
 //   dpSetWait(dpName + ".ModeSet", action[2] & 0xFF);
    break;
  case LVA_DEVICE_PART_OPEN:
    newWr1 = PROCESS_W16_REMOTE | PROCESS_W16_MANUAL;
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["SetPointR"] = NO_VALUE_CHANGE;
    mConnectDpes["CountDownR"] = NO_VALUE_CHANGE;
    mConnectDpes["SetPointW"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["SetPointR"] = NO_VALUE_CHANGE;
    mConnectDpes["CountDownR"] = NO_VALUE_CHANGE;
    mConnectDpes["SetPointW"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return LVA_OK;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_ProcessINJ_6B02: unknown action " + act, "");
    return LVA_FAILURE;
  }

  if((act == LVA_DEVICE_PART_OPEN) && (dynlen(action) > 1))
  {
    dpSetWait(dpName + ".WR1", newWr1,
              dpName + ".SetPointW", action[2]);
  }
  else
  {
    dpSetWait(dpName + ".WR1", newWr1);
  }
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return LVA_FAILURE;
  }
  return LVA_OK;
}


void  LhcVacGetRequiredActPriv_Process(string dpName, dyn_anytype act, int &privRequired, dyn_string &exceptionInfo)
{
  int typeIndex, code;
  string dpType;
  switch (act[1])
  {
    case LVA_DEVICE_ON:
    case LVA_DEVICE_OFF:
      dpType = dpTypeName(dpName);
      if(dpType = "VPG_6A01")      
      {
      //LHC #1619: [VPG6A] Labelling and Access Privilege change request for SCADA command - VPG familly 6A type 01     
        int family,type,subType; 
        int mode;
        LhcVacGetCtlAttributes(dpName, family, type, subType);
        if(subType == 3 )
        {
          privRequired = 2;
        }
      }
      break;
    case LVA_DEVICE_AUTO:
    case LVA_DEVICE_RESET:
    case LVA_DEVICE_FORCE:
    case LVA_DEVICE_PART_OPEN:
    case LVA_DEVICE_PART_CLOSE:
      break;
    case LVA_DEVICE_PROC_SET_MODE:
      if((typeIndex = LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo)) < 1)
      {
        errMsg = "LhcVacModeStatusForCode: unsupported DP type <" + dpType + "> of <" + dpName + ">";
        return;
      }
      int code = (act[2] & 0xFF);  // Mode status code
      int newPriv = VacResourcesGetValue(dpType + ".ModeSetPrivelege_" + code , VAC_RESOURCE_INT, privRequired);
      if(newPriv > privRequired)
      {
        privRequired = newPriv;
      }
      break;
    }
}

