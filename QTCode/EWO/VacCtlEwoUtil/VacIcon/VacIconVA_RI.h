#pragma once
#include "VacIcon.h"

class EqpVA_RI;

class VACCTLEWOUTIL_EXPORT VacIconVA_RI : public VacIcon
{
	Q_OBJECT

public:
	VacIconVA_RI(QWidget *parent, Qt::WindowFlags f = 0);
	~VacIconVA_RI();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);
	virtual void setMode(DataEnum::DataMode /* newMode */);
	virtual bool connect(void);
	virtual bool disconnect(void);

protected:
	// Pointer to my device
	EqpVA_RI *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
};
