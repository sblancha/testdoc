#uses "VacCtlEqpDataPvss"      // Load DLL or so
#uses "vclDevCommon.ctl"   // Load CTRL library
#uses "vclDIPConfig.ctl"   // Load CTRL library
#uses "eqp/vclAccessMode.ctl"  // Load CTRL library
#uses "vclResources.ctl"   // Load CTRL library

// parse resource and create DPs to control access mode

// List of all main parts
dyn_string mainParts;

main()
{
  DebugTN("vcsAccessMode.ctl: Starting");
  if(!InitStaticData())
  {
    return;
  }

  // 1st step: get string containing mainparts using in access mode facility
  dyn_string exceptionInfo;
  VacResourcesParseFile(PROJ_PATH + "/data/VacCtlResources.txt", glAccelerator, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsAccessMode.ctl: VacResourcesParseFile() failed:", exceptionInfo);
    return;
  }
  string mainPartsString = VacResourcesGetValue("AccessMode.MainParts", VAC_RESOURCE_STRING, "");

  // 2nd step: parse mainPartsString to build list of main parts 
  strreplace (mainPartsString, "\t", " ");
  dyn_string accessMps = strsplit(mainPartsString, " ");

  // Get names of all existing DPs of required type
  dyn_string dpList = dpNames("*", "_VacAccessMode");
  for(int i = dynlen(dpList) ; i > 0 ; i--)
  {
    dpList[i] = dpSubStr(dpList[i], DPSUB_DP);
  }

  // 3th step: create DP(s), publish them in DIP
  for(int i = dynlen(accessMps); i > 0 ; i--)
  {
    string dpName, dpeName;
    LhcVacGetDpeForAccessMode(accessMps[i], dpName, dpeName);
    if(dpName == "")
    {
      DebugTN("vcsAccessMode.ctl: Failed to get DP name for  <" + accessMps[i] +
        ">, skipping..." );
      continue;
    }
    int index = dynContains(dpList, dpName);
    string itemName = LhcVacGetPublishingName(accessMps[i]);
    if(index > 0)  // DP already exists
    {
      dynRemove(dpList, index );
    }
    else
    {
      dpCreate(dpName, "_VacAccessMode");
      if(!dpExists(dpName))
      {
        DebugTN("vcsAccessMode.ctl: Failed to create DP <" + dpName +
          ">, giving up..." );
        return;
      }
    }
    ProcessDpePublishing(dpeName, itemName, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("vcsAccessMode.ctl: ProcessDpePublishing(" + dpeName + "," +
        itemName + ") failed", exceptionInfo);
      dynClear(exceptionInfo);
      continue;
    }
    // DIP manager does not publish DPE until it's value is set
    dpSetWait(dpName + ".MODE", 0);
  }

  // Remove DP(s) remaining in dpList, unpublish them
  for(int i = dynlen(dpList) ; i > 0 ; i--)
  {
    string mainPart;
    LhcVacGetMainPartForAccessDp(dpList[i], mainPart, exceptionInfo);
    if(mainPart == "")
    {
      continue;
    }
    string itemName = LhcVacGetPublishingName(mainPart);
    fwDIP_unpublish(itemName, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("vcsAccessMode.ctl: fwDIP_unpublish(" + itemName + ") failed:",
        exceptionInfo);
    }
    dpDelete(dpList[i] );
  }
  DebugTN("vcsAccessMode.ctl: Fihished");

}
bool InitStaticData()
{
  dyn_string	exceptionInfo;

  // Get accelerator name
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN( "vcsAccessMode.ctl: Failed to read machine name:", exceptionInfo);
    return false;
  }
  bool isLhcData = glAccelerator == "LHC" ? true : false;

  // Initalize passive machine data
  int coco = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
  if( coco < 0 )
  {
    DebugTN("vcsAccessMode.ctl: LhcVacEqpInitPassive() failed:", exceptionInfo);
    return false;
  }
  DebugTN("vcsAccessMode.ctl: LhcVacEqpInitPassive() done");

  // Initialzie active equipment information
  coco = LhcVacEqpInitActive(glAccelerator,
    PROJ_PATH + "data/" + glAccelerator + ".for_DLL", exceptionInfo);
  if(coco < 0)
  {
    DebugTN( "vcsAccessMode.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo );
    return false;
  }
  DebugTN("vcsAccessMode.ctl: LhcVacEqpInitActive() done");
  LhcVacInitDevFunc();
  if(LhcVacGetAllMainParts(mainParts) < 0)
  {
    DebugTN("vcsAccessMode.ctl: LhcVacGetAllMainParts() failed");
    return false;
  }
  if(dynlen(mainParts) == 0)
  {
    DebugTN("vcsAccessMode.ctl: No main parts");
    return false;
  }
  return true;
}	


