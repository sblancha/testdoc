/////////////////////////////////////////////////////////////////////////////////
//	TITLE: Vacuum Cryo Pump Controller HRS-HCC EWO Icon
//  DESCRIPTION: Implementation of VacIconVPC_HCCC class
//	COMMENT: get the animation "convention" from VacIconVV_STD_IO 
//  CREATION DATE: 2014-07-29
//  MODIFICATION DATE: 2014-07-29
//  VERSION: 0.1 
//	AUTHOR: CERN Vacuum Controls Team (TE/VSC/ICM)
//	LOG: 
//		V0.1: Creation
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPC_HCCC.h"

#include "EqpVPC_HCCC.h"

#include <qpainter.h>
#include <QMouseEvent>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for cryo pump animation
#define COLOR_UNDEFINED			63,127,255  
#define COLOR_ON				0,255,0
#define COLOR_STARTING			127,255,127
#define	COLOR_ON_ERROR			204,255,0
#define	COLOR_ON_WARNING		153,255,0
#define COLOR_OFF				255,255,255
#define COLOR_OFF_ERROR			255,0,0
#define	COLOR_OFF_WARNING		255,255,0
#define	COLOR_ERROR				255,102,255
#define	COLOR_WARNING			255,255,0


// Colors for control and interlocks
/*
#define COLOR_GREEN				0,255,0
#define COLOR_YELLOW			255,255,0
#define COLOR_RED				255,0,0
#define	COLOR_WHITE				255,255,255
*/
#define COLOR_BLACK				0,0,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_COOLDOWN	(0x20000000)
#define RR1_LOCAL		(0x10000000)
#define RR1_CRYOOK		(0x08000000)
#define	RR1_ON_NOMINAL	(0x02000000)	
#define	RR1_OFF			(0x01000000)	
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_PREVAC		(0x00020000)
#define	RR1_REGEN		(0x00010000)

/////////////////////////////////////////////////////////////////////////////////
////////////////////	CONSTRUCTION/DESCTRUCTION
/////////////////////////////////////////////////////////////////////////////////
VacIconVPC_HCCC::VacIconVPC_HCCC(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 27);
}

VacIconVPC_HCCC::~VacIconVPC_HCCC()
{
	disconnect();
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////	FUNCTIONS
/////////////////////////////////////////////////////////////////////////////////

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPC_HCCC::getGeometry(const VacEqpTypeMask & /* mask */)
{
	/*
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		return VacIconGeometry(27, 21, 10, 15, 0);
		break;
	default:
		break;
	}
	*/
	return VacIconGeometry(27, 21, 10, 15, 0);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPC_HCCC::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVPC_HCCC"), "VacIconVPC_HCCC::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVPC_HCCC *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPC_HCCC::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPC_HCCC::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPC_HCCC::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPC_HCCC::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/*
**	FUNCTION
**		Override VacIcon's method in order to have different menus for different
**		parts of icon
**
**	ARGUMENTS
**		pEvent	- Pointer to mouse event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
/*void VacIconVPC_HCCC::mousePressEvent(QMouseEvent *pEvent)
{
	if(pEqp)
	{
		QPoint mousePoint(pEvent->x(), pEvent->y());
		QPoint screenPoint = mapToGlobal(mousePoint);
		int buttonNbr = 0;
		switch(pEvent->button())
		{
		case Qt::LeftButton:
			buttonNbr = 1;
			break;
		case Qt::RightButton:
			buttonNbr = 2;
			break;
		default:
			break;
		}
		if(buttonNbr)
		{
			//if(valve.contains(mousePoint))
			//{
			//	emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), Eqp::EqpPartVVG, pEqp->getDpName());
			//}
			//else
			{
				emit mouseDown(buttonNbr, mode, screenPoint.x(), screenPoint.y(), 0, pEqp->getDpName());
			}
		}
	}
}
*/

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPC_HCCC::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	//painter.eraseRect(0, 0, width(), height());
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	drawVertical(painter);
}

void VacIconVPC_HCCC::drawVertical(QPainter &painter)
{
    // Draw vacuum pipe - two short lines
	painter.fillRect(9, 0, 3, 5, pipeColor);
	painter.fillRect(9, 19, 3, 8, pipeColor);
	
	// Draw Cryo pump image
	setPainterForMainImage(painter);
	//QPen pen(Qt::black, 2);
	//painter.setPen(pen);
	//QBrush brush(Qt::darkGreen);
	//painter.setBrush(brush);
	
	// 1) big rectangle 
	QPolygon bigRectangle(4);
	bigRectangle.setPoint(0, 3, 3);
	bigRectangle.setPoint(1, 19, 3);
	bigRectangle.setPoint(2, 19, 19);
	bigRectangle.setPoint(3, 3, 19);
	painter.drawPolygon(bigRectangle);

	// 2) the 90DegRot "M" in the left Middle
	setPainterForControl(painter);
	
	
	QPolygon M90Deg(10);
	M90Deg.setPoint(0, 1, 5);
	M90Deg.setPoint(1, 11, 5);
	M90Deg.setPoint(2, 7, 10);
	M90Deg.setPoint(3, 11, 16);
	M90Deg.setPoint(4, 1, 16);
	M90Deg.setPoint(5, 1, 14);
	M90Deg.setPoint(6, 8, 14);
	M90Deg.setPoint(7, 5, 10);
	M90Deg.setPoint(8, 8, 7);
	M90Deg.setPoint(9, 1, 7);
	painter.drawPolygon(M90Deg);

//	drawSymbols(painter, true); //no additional symbol like 'ON', 'OFF', 'M'
}


void VacIconVPC_HCCC::setPainterForMainImage(QPainter &painter)
{
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		pEqp->getMainColor(color, mode);
	}
	QBrush brush(color);
	painter.setBrush(brush);
	painter.setPen(Qt::black);
}

void VacIconVPC_HCCC::setPainterForControl(QPainter &painter)
{
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		int order;
		QColor borderColor;
		pEqp->getInterlockColor(mode, color, borderColor, order, NULL);
	}
	painter.setBrush(color);
	painter.setPen(Qt::black);
}
