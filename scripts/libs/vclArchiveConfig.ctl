// CTRL Library
// created from lhcVacArchiveConfig.ctl for QT
// @SAA 19may10
#uses "vclDevCommon.ctl"	// Load CTRL library
// Archive states
const int
	UNKNOWN_STATE = -1,
	ONLINE_STATE  = 1,
	STOPPED_STATE = 0,
	DELETED_STATE = 3;

// Smooth types
const int SMOOTH_TYPE_NONE = 0;
const int SMOOTH_TYPE_TIME = 1;
const int SMOOTH_TYPE_VALUE = 2;
const int SMOOTH_TYPE_TIME_AND_VALUE = 3;
const int SMOOTH_TYPE_TIME_OR_VALUE = 4;
const int SMOOTH_TYPE_REL_VALUE = 5;
const int SMOOTH_TYPE_TIME_AND_REL_VALUE = 6;

// Flag - use RDB archiving, if false - use file archiving
global bool gImportUseRDBArchving = false;
// RDB archive group class name - used when gImportUseRDBArchving = true
global string gImportUseRdbClass;

// pointers to archives and its dps used by IMPORT
global dyn_string	importUsedArchiveNamesL;
global dyn_string	importUsedArchiveDpsL;
global int				numberOfImportUsedArchivesL;
global dyn_string		glUsedList;
//ArchivesUsedByImport:
/**
	Put/Get archives used by import.
	Put is used on Start Import steep.
	Get is used on archives switch steep.

	@param importUsedArchiveNames: in/out, array contained archive names
	@param importUsedArchiveDps: in/out, array contained archive dps
	@param put_get: in,
		if put_get == FLAG_PUT then put data here
		if put_get == FLAG_GET then get data from here

	@return number of archives
	@author ASA
*/
//
//
int ArchivesUsedByImport( dyn_string &importUsedArchiveNames, dyn_string &importUsedArchiveDps, int put_get )
{
	// get
	if( put_get == FLAG_GET )
	{
		importUsedArchiveNames = importUsedArchiveNamesL;
		importUsedArchiveDps = importUsedArchiveDpsL;
		return( numberOfImportUsedArchivesL );
	}
	// put
	dynClear( importUsedArchiveNamesL );	// clear library
	dynClear( importUsedArchiveDpsL );		// clear library
	importUsedArchiveDpsL = importUsedArchiveDps;
	importUsedArchiveNamesL = importUsedArchiveNames;
	numberOfImportUsedArchivesL = dynlen( importUsedArchiveNamesL );
	return( numberOfImportUsedArchivesL );
}

//GetArchiveNumForImport:
//
//
int GetArchiveNumForImport( string archiveDp )
{
	int num;
	dpGet( archiveDp + ".arNr:_online.._value", num );
	return num;
}

//IsArchiveDeleted:
//
//
bool IsArchiveDeleted( string archiveDp )
{
	int archiveState;
	dpGet( archiveDp + ".state:_online.._value", archiveState );
	if( archiveState == DELETED_STATE )
		return true;
	else
		return false;
}

//DeleteArchiveForImport:
//
//
bool archiveDeleted;
CheckDeleteCB( string archiveDp, int state ) 
{ 
  bool bRedu = (host2 != "" && host1 != host2); 
 
  if (state == DELETED_STATE && (!bRedu || (state_2 == DELETED_STATE && bRedu)))// wenn wirklich gel�scht 
  { 
		archiveDeleted = true;
//		DebugN( "CheckDeleteCB: DELETED", archiveDp );
  } 
}
void DeleteArchiveForImport( string archiveDp )
{
//	DebugN( "DeleteArchiveForImport: archive DP for delete", archiveDp );

	archiveDeleted = false;

	dpSet( archiveDp + ".state:_original.._value", DELETED_STATE );   // L�schen setzen !!!
	if( host2 != "" && host1 != host2 )          // falls redundantes System  !!!! 
		dpSet( archiveDp + "_2.state:_original.._value", DELETED_STATE );   // L�schen setzen !!!
				
  dpConnect( "CheckDeleteCB", archiveDp + ".state:_online.._value" ); // may be need callback function to be sure that archive is deleted
	while( ! archiveDeleted ) delay( 1 );
	dpDisconnect( "CheckDeleteCB", archiveDp + ".state:_online.._value" );
}

//DeleteArchivesForImport:
//
//
void DeleteArchivesForImport( dyn_string archiveDps )
{
	int len, num;
	//DebugN( "DeleteArchivesForImport: archive DPs for delete", archiveDps );
			
	for( len = dynlen( archiveDps ); len > 0; len-- )
	{
		DeleteArchiveForImport( archiveDps[len] );
	}
}

//GetVacUnusedArchiveClasses:
//
//	Get archives and corresponded dps from PVSS DB which not need for
//	import after analyse parsedArchives.
//	This archives can be deleted by IMPORT.
//	"ValueArchive_0000" must be in system and not be placed into result.
//
//	PARAMETERS:
//
//	parsedArchives - archives used by import from parse library
//	vacUnusedArchiveClasses - archives which not need for import - can be deleted
//	vacUnusedArchiveDps - dps corresponded with vacUnusedArchiveClasses
//	exceptionInfo - contain errors if exist
//
//	@ASA
//
void GetVacUnusedArchiveClasses( dyn_string parsedArchives,
	dyn_string &vacUnusedArchiveClasses, dyn_string &vacUnusedArchiveDps,
	dyn_string &exceptionInfo )
{
	dyn_string 	readFromSystems;
	dyn_string 	allArchiveClasses, allArchiveDps;
	int					len, element_ValueArchive_0000;

	dynClear( vacUnusedArchiveClasses );
	dynClear( vacUnusedArchiveDps );
	
	fwArchive_getAllValueArchiveClasses( readFromSystems,
		allArchiveClasses, allArchiveDps, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;

	// remove ValueArchive_0000 from research
	element_ValueArchive_0000 = dynContains( allArchiveClasses, "ValueArchive_0000" );
	if( element_ValueArchive_0000 < 1 )
	{
		fwException_raise( exceptionInfo, "ERROR",
			"Can not find archive <ValueArchive_0000> in PVSS.",
			"" );
		return;
	}
	dynRemove( allArchiveClasses, element_ValueArchive_0000 );
	dynRemove( allArchiveDps, element_ValueArchive_0000 );
	
	// check for unused archive classes not exist
	if( dynlen( allArchiveClasses ) < 1 ) return;
	// check for inputed array not contain any archive classes
	if( dynlen( parsedArchives ) < 1 ) 
	{
		vacUnusedArchiveClasses = allArchiveClasses;
		vacUnusedArchiveDps = allArchiveDps;
		return;
	}
	
	// create archive classes be unused by import for future delete ( after confirmation )
	for( len = dynlen( allArchiveClasses ); len > 0; len-- )
	{
		if( dynContains( parsedArchives, allArchiveClasses[len] ) > 0 ) continue;
		dynAppend( vacUnusedArchiveClasses, allArchiveClasses[len] );
		dynAppend( vacUnusedArchiveDps, allArchiveDps[len] );
	}
}

//CheckArchiveClass:
//
//	Functions get archive values:
//	
//	@param archiveDpElements: out
//	@param archiveState: out
//	@param archiveStateMessage: out
//
//
void CheckArchiveClass( string archiveClass, string &archiveDp, int &num,
	int &archiveDpElements, int &archiveState, string &archiveStateMessage,
	dyn_string &exceptionInfo )
{
	int					i;
	dyn_string 	readFromSystems;
	dyn_string 	archiveClasses;
	dyn_string 	archiveClassDps;

	dynClear( exceptionInfo );
  num = -1;

	fwArchive_getAllValueArchiveClasses( readFromSystems,
		archiveClasses, archiveClassDps,
		exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;

	for( i = dynlen( archiveClasses ) ; i > 0 ; i-- )
  {
		if(  archiveClass == archiveClasses[i] ) break;
  } 
  if( i < 1 )
	{
		fwException_raise( exceptionInfo, "WARNING",
			"Non-existent archive class " + archiveClass,
			"" );
		return;
	}    	
  //DebugN( "CheckArchiveClass: archiveClass archiveClassDp ", archiveClasses[i], archiveClassDps[i] );
	archiveDp = archiveClassDps[i];
	dpGet(	archiveDp + ".size.maxDpElGet:_online.._value", archiveDpElements,
			archiveDp + ".state:_online.._value", archiveState,
			archiveDp + ".arNr:_online.._value", num );
	archiveStateMessage = getCatStr( "va", "state" + archiveState );
	// If archive is deleted - it is the same as 'archive does not exist'
	if( archiveState == DELETED_STATE )
	{
		archiveDp = "";
		fwException_raise( exceptionInfo, "WARNING",
			"Non-existent (deleted) archive class " + archiveClass,
			"" );
		num = -1;
	}
}

//SetArchiveMaxDpEl:
//	Set archive DP elements + 5%
//
// PARAMETERS:
//	archiveDp	- archive dp
//	archiveDpElements - dp elements be setted for archive, new value be returned as parameter value
//
void SetArchiveMaxDpEl( string archiveDp, int &archiveDpElements )
{
	int	spare = archiveDpElements * 0.05;	// Plus 5%
	if( spare < 10 ) spare = 10;
	archiveDpElements = archiveDpElements + spare;
	// DebugN( "SetArchiveMaxDpEl:", archiveDp, archiveDpElements );
	dpSet( archiveDp + ".size.maxDpElSet:_original.._value", archiveDpElements );
}

//SetArchiveFileCompressions:
//	Change archive properties - file compression ...
//
// PARAMETERS
//	archiveDp - archive DP
//
void SetArchiveFileCompressions( string archiveDp )
{
	string 	autoStartAfterSwitchSet;
	int		packCountSet, switchTimeInterval;

	dpGet( archiveDp + ".setMgmt.fileSwitch.switchTimeGet:_online.._value", switchTimeInterval );
	if( switchTimeInterval > 0 )
	{
		dpSet( archiveDp + ".setMgmt.fileSwitch.switchTimeSet:_online.._value", 0 );
	}

	//DebugN( "SetArchiveFileCompressions:", archiveDp );
	dpGet(	archiveDp + ".setMgmt.fileCompression.autoStartAfterSwitchGet:_online.._value", autoStartAfterSwitchSet,
			archiveDp + ".setMgmt.fileCompression.packCountGet:_online.._value", packCountSet );
	//DebugN( "SetArchiveFileCompressions: autoStartAfterSwitchSet, packCountSet", autoStartAfterSwitchSet, packCountSet );		
	if( autoStartAfterSwitchSet != "TRUE" )
	{
		//DebugN( "SetArchiveFileCompressions: Set Compression Level - ON for archive", archiveDp );
		dpSet( archiveDp + ".setMgmt.fileCompression.autoStartAfterSwitchSet:_original.._value", "TRUE" );
	}
	if( packCountSet != 1 )
	{
		//DebugN( "SetArchiveFileCompressions: Set Compression Level II - ON for archive", archiveDp );
		dpSet( archiveDp + ".setMgmt.fileCompression.packCountSet:_original.._value", 1 );
	}
}

//SetArchiveStorage:
// Set archive storage properies.
// ( Do not remember about secret decrease button for SUP - look function IsReducedMaxNumberValueEntries )
// return: true - if properies was changed, false another case
//
bool SetArchiveStorage( string archiveDp, int nDpes, int totalDepth, int interval, dyn_string &exceptionInfo )
{
	int	nTotalValues = totalDepth * 24 * 60 * 60;
	if( interval > 0 ) nTotalValues /= interval;

	//DebugN( "SetArchiveStorage(): archiveDp, nDpes, totalDepth, interval, nTotalValues", archiveDp, nDpes, totalDepth, interval, nTotalValues );

	int	nFiles, nFilesGet, timePerFile, valuesPerFile, valuesPerFileGet;
        int    archiveSize, archiveSizeLimit;
        archiveSizeLimit = 2000; // @SAA 2048 before 18june2010
	timePerFile = 7 * 24 * 60 * 60;	// Start with one week per file
	while( true )
	{
		valuesPerFile = timePerFile;
		if( interval > 0 ) valuesPerFile /= interval;
		CalculateSpaceOfArchiveFile( archiveDp, nDpes, valuesPerFile, archiveSize );
		if( archiveSize < archiveSizeLimit )
		{
			//DebugN( "SetArchiveStorage(): ", archiveDp, "timePerFile " + timePerFile, "nDeps ", nDpes, "valuesPerFile " + valuesPerFile, "archiveSize " + archiveSize );
			break;
		}
		//timePerFile -= 24 * 60 * 60;	// Minus one day
		timePerFile -= 60 * 60;	// Minus one hour 21aug08 @SAA
		if( timePerFile <= 0 )
		{
			//DebugTN(  "SetArchiveStorage() ERROR: ", archiveDp, "timePerFile " + timePerFile, "nDeps ", nDpes, "valuesPerFile " + valuesPerFile, "archiveSize " + archiveSize );
			fwException_raise( exceptionInfo, "ERROR",
				"Archive file size is too large for " + archiveDp,
				"" );
			return false;
		}
	}

	if( IsReducedMaxNumberValueEntries( ) ) valuesPerFile = valuesPerFile / 10; // only for SUP for increase speed file switch
	if( valuesPerFile < 500 ) valuesPerFile = 500; // why minimum value is 500 look into modify archive parameter dialogue
	else if(valuesPerFile > 99999)
	{
		valuesPerFile = 99999;	// LIK 25.08.2008
	}

	int nFiles = nTotalValues / valuesPerFile;
	if( nFiles < 5 ) nFiles = 5;
	
	//DebugN( "SetArchiveStorage():", archiveDp, "Need To Archive: " + nFiles + " files, " + valuesPerFile + " values in each file" );
	dpGet( archiveDp + ".size.maxValuesSet:_original.._value", valuesPerFileGet,
				 archiveDp + ".setMgmt.fileDeletion.keepCountSet:_original.._value", nFilesGet );
	//DebugN( "SetArchiveStorage():", archiveDp, "Archive Settings: " + nFilesGet + " files, " + valuesPerFileGet + " values in each file" );
	
	if( valuesPerFileGet == valuesPerFile && nFilesGet == nFiles )
	{
		//DebugN( "SetArchiveStorage():", archiveDp, "Do not need to change archive storage properies." );
		return false;
	}
				 
	// Change Archive Storage Properies, after it file change must be maked
	//DebugN( "SetArchiveStorage(): archiveDp, valuesPerFile, nFiles", archiveDp, valuesPerFile, nFiles );
	dpSet( archiveDp + ".size.maxValuesSet:_original.._value", valuesPerFile,
				 archiveDp + ".setMgmt.fileDeletion.keepCountSet:_original.._value", nFiles );
	return true;
}

//IsReducedMaxNumberValueEntries:
//ReduceArchiveMaxNumberValueEntries:
// 
// SUP DEBUG PERPOSE FUNCTIONS for increase file switch speed
//
global bool	isReducedMaxNumberValueEntriesL = false;
bool IsReducedMaxNumberValueEntries()
{
	return isReducedMaxNumberValueEntriesL;
}
void ReduceArchiveMaxNumberValueEntries( bool whatToDo )
{
	if( whatToDo )
		isReducedMaxNumberValueEntriesL = true;
	else
		isReducedMaxNumberValueEntriesL = false;
}

//GetArchiveParams: 
/*	
 Get old archiving parameters for given DPE. For the time being function duplicates
 functionality of fwArchive_get() because support for relative value change is missing.

	@param	dpe:	in, name of DPE
	@param isArchived: out, true: archive config existing, 
                   false: archive config is not existing
	@param archiveClass: out, archive class
	@param smoothKind: out, see SMOOTH_TYPE_xx constants
	@param deadband: out, archive deadband 
	@param timeInterval: out, archive time interval
*/
void GetArchiveParams( string dpe, bool &isArchived, string &archiveClass, int &smoothKind,
	float &deadband, float &timeInterval )
{
  bool configExists, isActive;
  int smoothType, smoothProcedure;
  dyn_string exceptionInfo;

  //DebugN( "GetArchiveParams: dpe, class", dpe );	

  archiveClass = "";
  smoothKind = 0;
  deadband = 0.0;
  timeInterval = 0.0;

/* old variant ...
 	if( dpExists( archiveClass + ".general.arName" ) )
 	{
		isArchived = true; 
		dpGet( archiveClass + ".general.arName:_online.._value", archiveClass );
	}
	else
	{
		isArchived = false; 
		return;
	}
*/

	// now fwArchive_get function used
  fwArchive_get( dpe, configExists, archiveClass, smoothType, smoothProcedure, deadband, timeInterval, isActive, exceptionInfo);
	if( dynlen( exceptionInfo ) > 0 )
		isArchived = false;
	else if( archiveClass == "" )
		isArchived = false;
	else
		isArchived = configExists && isActive;
	if( !isArchived ) return;


  // Vacuum equipment uses own smoothing procedures
  if( smoothType == DPATTR_ARCH_PROC_VALARCH )
  {
  	smoothKind = SMOOTH_TYPE_NONE;
  }
  else
  {
  	switch( smoothProcedure )
  	{
		case DPATTR_VALUE_SMOOTH:							// for all data types except bolean
		case DPATTR_COMPARE_OLD_NEW:					// for boolean data type
			smoothKind = SMOOTH_TYPE_VALUE;
			break;
		case DPATTR_TIME_SMOOTH:							// for all data types and for boolean data type
 			smoothKind = SMOOTH_TYPE_TIME;
			break;
		case DPATTR_TIME_AND_VALUE_SMOOTH:		// for all data types except bolean
		case DPATTR_OLD_NEW_AND_TIME_SMOOTH:	// for boolean data type
 			smoothKind = SMOOTH_TYPE_TIME_AND_VALUE;
			break;
		case DPATTR_TIME_OR_VALUE_SMOOTH:			// for all data types except bolean
		case DPATTR_OLD_NEW_OR_TIME_SMOOTH:		// for boolean data type
 			smoothKind = SMOOTH_TYPE_TIME_OR_VALUE;
			break;
		case DPATTR_VALUE_REL_SMOOTH:					// for all data types except bolean
			smoothKind = SMOOTH_TYPE_REL_VALUE;
			break;
                case DPATTR_TIME_AND_VALUE_REL_SMOOTH:
                  smoothKind = SMOOTH_TYPE_TIME_AND_REL_VALUE;
                  break;
		default:
			smoothKind = SMOOTH_TYPE_NONE;
			break;
  	}
  }
}

/*	SetArchiveParamsForArchiveClassName

 Set new archiving parameters for given DPE. For the time being function duplicates
 functionality of fwArchive_set() except the smoothKind is own for vacuum.

@param	dpe:	in, name of DPE
@param archiveClassName: in, archive name
@param smoothKind: in, see SMOOTH_TYPE_xx constants
@param deadband: in, archive deadband 
@param timeInterval: in, archive time interval
@param exceptionInfo: out, details of errors detected will be written here

*/
int SmoothForAllConvertVacToPVSS( int smoothKind, int &coco ) // for all data type except boolean
{
	switch( smoothKind )
	{
	case SMOOTH_TYPE_VALUE:
		//DebugN( "SmoothForAllConvertVacToPVSS()", "SMOOTH_TYPE_VALUE->DPATTR_VALUE_SMOOTH" );
		return DPATTR_VALUE_SMOOTH;
	case SMOOTH_TYPE_TIME_AND_VALUE:
		//DebugN( "SmoothForAllConvertVacToPVSS()", "SMOOTH_TYPE_TIME_AND_VALUE->DPATTR_TIME_AND_VALUE_SMOOTH" );
		return DPATTR_TIME_AND_VALUE_SMOOTH;
	case SMOOTH_TYPE_TIME_OR_VALUE:
		//DebugN( "SmoothForAllConvertVacToPVSS()", "SMOOTH_TYPE_TIME_OR_VALUE->DPATTR_TIME_OR_VALUE_SMOOTH" );
		return DPATTR_TIME_OR_VALUE_SMOOTH;
	case SMOOTH_TYPE_REL_VALUE:
		//DebugN( "SmoothForAllConvertVacToPVSS()", "SMOOTH_TYPE_REL_VALUE->DPATTR_VALUE_REL_SMOOTH" );
		return DPATTR_VALUE_REL_SMOOTH;
        case SMOOTH_TYPE_TIME_AND_REL_VALUE:
          return DPATTR_TIME_AND_VALUE_REL_SMOOTH;
	default:
		coco = FAILURE;
		return FAILURE;
	}
}
int SmoothForBoolConvertVacToPVSS( int smoothKind, int &coco ) // for boolean data type
{
	switch( smoothKind )
	{
	case SMOOTH_TYPE_VALUE:
		//DebugN( "SmoothForBoolConvertVacToPVSS()", "boolean data type SMOOTH_TYPE_VALUE->DPATTR_COMPARE_OLD_NEW" );
		return DPATTR_COMPARE_OLD_NEW;
	case SMOOTH_TYPE_TIME_AND_VALUE:
		//DebugN( "SmoothForBoolConvertVacToPVSS()", "boolean data type SMOOTH_TYPE_TIME_AND_VALUE->DPATTR_OLD_NEW_AND_TIME_SMOOTH" );
		return DPATTR_OLD_NEW_AND_TIME_SMOOTH;
	case SMOOTH_TYPE_TIME_OR_VALUE:
		//DebugN( "SmoothForBoolConvertVacToPVSS()", "boolean data type SMOOTH_TYPE_TIME_OR_VALUE->DPATTR_OLD_NEW_OR_TIME_SMOOTH" );
		return DPATTR_OLD_NEW_OR_TIME_SMOOTH;
	default:
		coco = FAILURE;
		return FAILURE;
	}
}
void SetArchiveParamsForArchiveClassName( string dpe, string archiveClassName,
	int smoothKind, float deadband, float timeInterval, dyn_string &exceptionInfo )
{
	int	archiveType = DPATTR_ARCH_PROC_SIMPLESM;
	//	archiveType	can be:
	//		DPATTR_ARCH_PROC_VALARCH: no smoothing,  
  //		DPATTR_ARCH_PROC_SIMPLESM: value dependent + deadband/old-new comparison smoothing
	int	smoothProcedure;
	int coco = OK;
	dynClear( exceptionInfo );	
	switch( smoothKind )
	{
	case SMOOTH_TYPE_NONE:
		//DebugN( "SetArchiveParamsForArchiveClassName()", "SMOOTH_TYPE_NONE->DPATTR_ARCH_PROC_VALARCH no smoothing" );
		archiveType = DPATTR_ARCH_PROC_VALARCH;
		break;
	case SMOOTH_TYPE_TIME:
		//DebugN( "SetArchiveParamsForArchiveClassName()", "SMOOTH_TYPE_TIME->DPATTR_TIME_SMOOTH" );
		smoothProcedure = DPATTR_TIME_SMOOTH;
		break;
	default:
		if( dpElementType(dpe) == DPEL_BOOL ) 
			smoothProcedure = SmoothForBoolConvertVacToPVSS( smoothKind, coco );
		else
			smoothProcedure = SmoothForAllConvertVacToPVSS( smoothKind, coco );
		if( coco == FAILURE )
		{
			fwException_raise( exceptionInfo, "ERROR",
				"SetArchiveParamsForArchiveClassName(): unsupported smoothKind " + smoothKind + " for " + dpe,
				"" );
			return;
		}
		break;
	}
  if(gImportUseRDBArchving)
  {
    archiveClassName = gImportUseRdbClass;
  }
  fwArchive_set( dpe, archiveClassName, archiveType, smoothProcedure,
  		deadband, timeInterval, exceptionInfo, false );
}

/*	SetArchiveParams

 Set new archiving parameters for given DPE. For the time being function duplicates
 functionality of fwArchive_set() because support for relative value change is missing.

@param	dpe:	in, name of DPE
@param archiveDp: in, archive DP
@param smoothKind: in, see SMOOTH_TYPE_xx constants
@param deadband: in, archive deadband 
@param timeInterval: in, archive time interval
@param exceptionInfo: out, details of errors detected will be written here

void SetArchiveParams( string dpe, string archiveDp,
	int smoothKind, float deadband, float timeInterval, dyn_string &exceptionInfo )
{
	int						smoothType, smoothProcedure, i;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	
	switch( smoothKind )
	{
	case SMOOTH_TYPE_VALUE:
		smoothProcedure = DPATTR_VALUE_SMOOTH;
		break;
	case SMOOTH_TYPE_TIME:
		smoothProcedure = DPATTR_TIME_SMOOTH;
		break;
	case SMOOTH_TYPE_TIME_AND_VALUE:
		smoothProcedure = DPATTR_TIME_AND_VALUE_SMOOTH;
		break;
	case SMOOTH_TYPE_TIME_OR_VALUE:
		smoothProcedure = DPATTR_TIME_OR_VALUE_SMOOTH;
		break;
	case SMOOTH_TYPE_REL_VALUE:
		smoothProcedure = DPATTR_VALUE_REL_SMOOTH;
		break;
	default:
		fwException_raise( exceptionInfo, "ERROR",
			"SetArchiveParams(): unsupported smoothKind " + smoothKind + " for " + dpe,
			"" );
		return;
	}

	dpSetWait(
		dpe + ":_archive.._type", DPCONFIG_DB_ARCHIVEINFO,
		dpe + ":_archive.._archive", true,
		dpe + ":_archive.1._class", archiveDp,
		dpe + ":_archive.1._type", 3 );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		fwException_raise( exceptionInfo, "ERROR",
			"SetArchiveParams(): Could not create archiving config for " + dpe + "; " + StdErrorText( err ),
			"" );
		throwError( err );
		return;
	}
	switch( smoothProcedure )
	{
	case DPATTR_VALUE_SMOOTH:
	case DPATTR_VALUE_REL_SMOOTH:
		dpSetWait( dpe + ":_archive.1._std_type", smoothProcedure,
			dpe + ":_archive.1._std_tol", deadband );
		break;
	case DPATTR_TIME_SMOOTH:
		dpSetWait( dpe + ":_archive.1._std_type", smoothProcedure,
			dpe + ":_archive.1._std_time", timeInterval );
		break;
	case DPATTR_TIME_AND_VALUE_SMOOTH:
	case DPATTR_TIME_OR_VALUE_SMOOTH:
		dpSetWait( dpe + ":_archive.1._std_type", smoothProcedure,
			dpe + ":_archive.1._std_tol", deadband,
			dpe + ":_archive.1._std_time", timeInterval );
		break;
	}
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		fwException_raise( exceptionInfo, "ERROR",
			"SetArchiveParams(): Could set smoothing parameters for " + dpe + "; " + StdErrorText( err ),
			"" );
		throwError( err );
		return;
	}

}
*/

//_vaCreateArchiveImport:
/**
	Modified function from scripts/libs/va.ctl
	_vaCreateArchive( sArchivDp, archiveClass ); 
	Used for create archive where callbacks for update table removed (changed to inputed callback function ???)
*/
void _vaCreateArchiveImport( string dp, langString ls ) 
{  
 if (dpExists(dp))                              // Nummer < 10 oder DP schon vorhanden, aber gel�scht !! 
 { 
    dpSet(dp+".state:_original.._value",0);     // als "stop" markieren 
    if (host2 != "" && host1 != host2)          // falls redundantes System !!!! 
    { 
      dpSet(dp+"_2.state:_original.._value",0); // als "stop" markieren 
    } 
 } 
 else 
 { 
    dpCreate(dp, "_ValueArchive");              // anlegen 
    dyn_errClass err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "_vaCreateArchiveImport(): failed to create DP <" + dp + ":" + err , "");
      fwExceptionHandling_display(exceptionInfo);
      return;	 
    }        
    dpCreate(dp+"_2", "_ValueArchive");      // redundanter DP anlegen 
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "_vaCreateArchiveImport(): failed to create DP <" + dp + "_2:" + err , "");
      fwExceptionHandling_display(exceptionInfo);
      return;	 
    }        
  } 
 
  dpSet(dpSubStr(dp,DPSUB_SYS_DP)+".general.arName:_original.._value",ls); 
  _vaCreateDpSet(dp); 
  dpSet(dpSubStr(dp+"_2",DPSUB_SYS_DP)+".general.arName:_original.._value",ls); 
  _vaCreateDpSet(dp+"_2"); 
  dpSetWait( dp+".files.startTime:_original.._value",makeDynTime(makeTime(2000,1,1)),  // Im Redundanzfall ist die Starttime 1.1.2000 
             dp+"_2.files.startTime:_original.._value",makeDynTime(makeTime(2000,1,1))); 

  //dpConnect( cbFunc, dp + ".general.arName:_online.._value" ); // may be need callback function to be sure that archive is created

} 

//CreateNonExistentArchiveClass:
// Created from function in archive_select.pnl
//
void CreateNonExistentArchiveClass( string archiveClass, int &num )
{
	int 		i, state;
	string 		sArchivDp, mySystem, emptyDp, defaultDp = "_ValueArchive_";
	string 		sNumber, tempDp;
	dyn_string	ds;

	ds = dpNames( "*","_ValueArchive" );				// alle DP's holen
	if( dynlen( ds ) > 0 )
		mySystem = dpSubStr(  ds[1],DPSUB_SYS );	// System feststellen
	else
		emptyDp = defaultDp;						// falls nicht feststellbar -> kein DP definiert
			
	i = 0;
	while( defaultDp != emptyDp )
	{
		sprintf( sNumber, "%d", i );
		tempDp = mySystem + defaultDp + sNumber;
		if( dynContains( ds, tempDp) == 0 )	// leeren DP gefunden
            emptyDp = defaultDp;
		else								// testen ob dieser DP als gel�scht markiert ist...
        {
	    	if( host2 != "" && host1 != host2 && host2 == hostname )          // falls redundantes System und am 2. Rechner !!!! 
     			dpGet( tempDp +"_2.state:_online.._value", state );             // Status vom redundanten DP holen
           	 else
     			dpGet( tempDp +".state:_online.._value", state );               // Status holen

            if ( state == 3 )	                                             // einen leeren (gel�schten) dp finden
				emptyDp = defaultDp;
            else
				i++;		                                             // ansonsten probier' den n�chsten aus
		}
	}

 	sArchivDp = emptyDp + i;
	//DebugN( "Non-existent archive class", archiveClass, sArchivDp );
	_vaCreateArchiveImport( sArchivDp, archiveClass ); // defined in scripts/libs/va.ctl

	num = i;
	return;
}

//CalculateSpaceArchiveFile:
//
// This get from void _vaCalculateSpace()  - scripts/libs/va.ctl
//
 // // size =fdpel* fdpcount ;   
 //
 // size = (256 + fdpcount*48)/1024;   // add Archiveheader IM 58636
 //
 // size =fdpel* fdpcount/1024*48 + size;        // Anz der Elemente berechnen [kB] 
 // size = size+64 ;              // 64 KB Header dazu   
 // if (size < 6400) size = 6400;     // Stripsize 6,4 MB 
 // size=ceil(size/64,0)*64;      // aufrunden auf n�chste 64 KB Grenze 
 // size = size / 1024;       // KB -> MB umrechnen 
 //
 // setValue("dp_size","text",size);   
 // if (size > 2048)
 // {
 //		...
 // }
//
//	
void CalculateSpaceOfArchiveFile( string archiveDp, int fdpel, int fdpcount, int &archiveSize )
{ 
	float size;
	
	// size =fdpel* fdpcount ;   
 
 	size = (256 + fdpcount*48)/1024;   // add Archiveheader IM 58636
 
	size =fdpel* fdpcount/1024*48 + size;        // Anz der Elemente berechnen [kB] 
	size = size+64 ;              // 64 KB Header dazu   
	if (size < 6400) size = 6400;     // Stripsize 6,4 MB 
	size=ceil(size/64,0)*64;      // aufrunden auf n�chste 64 KB Grenze 
	size = size / 1024;       // KB -> MB umrechnen 
	archiveSize = ceil( size );
 
	//if (size > 2048)
	//{
	//	DebugN( "CalculateSpaceOfArchiveFile(): Archive file size is bigger than 2048 MB archiveDp, size, ceil(size)", archiveDp, size, archiveSize );
	//}
 
} 

// BuildDpeListForDpType
// @ASA
//Look scripts\lhcVacMissingArchValue.ctl
//
// Parameters:
//  dpType - (in) datapoint type
//  dllDpeNames - (out) array with dpe names
//  exceptionInfo - contain errors if exist
//
// MDF:
// feb 2011 @ASA
//
void BuildDpeListForDpType( string dpType, dyn_string &allDpeNames, dyn_string &exceptionInfo )
{
	dyn_dyn_string	names;
	dyn_dyn_int			types;
	dyn_string			dpeTree, localExceptionInfo;

	dynClear( allDpeNames );
	dynClear( exceptionInfo );

	// Get DP type structure for working DP
	if( dpTypeGet( dpType, names, types ) < 0 )
	{
		dyn_errClass err = getLastError( );
		DebugTN( "lhcVacMobileEqp.ctl: ERROR: dpTypeGet(" + dpType + ") failed: " + StdErrorText( err ) );
		DebugTN( err );
		fwException_raise( exceptionInfo, "ERROR", "BuildDpeListForDpType(): dpTypeGet(" + dpType + ") failed", "");
		return;
	}
	int dpeIdx, typeIdx, type, nDpes = dynlen( names );
	for( dpeIdx = 1 ; dpeIdx <= nDpes ; dpeIdx++ )
	{
		typeIdx = dynlen( types[dpeIdx] );
		type = types[dpeIdx][typeIdx];
		if( ( type == DPEL_STRUCT ) ||	// Structure
			(type == DPEL_BIT32_STRUCT) || (type == DPEL_BLOB_STRUCT) || (type == DPEL_BOOL_STRUCT) ||
			(type == DPEL_CHAR_STRUCT) || (type == DPEL_DPID_STRUCT) || (type == DPEL_DYN_BIT32_STRUCT) ||
			(type == DPEL_DYN_BLOB_STRUCT) || (type == DPEL_DYN_BOOL_STRUCT) || (type == DPEL_DYN_CHAR_STRUCT) ||
			(type == DPEL_DYN_DPID_STRUCT) || (type == DPEL_DYN_FLOAT_STRUCT) || (type == DPEL_DYN_INT_STRUCT) ||
			(type == DPEL_DYN_LANGSTRING_STRUCT) || (type == DPEL_DYN_STRING_STRUCT) || (type == DPEL_DYN_TIME_STRUCT) ||
			(type == DPEL_DYN_UINT_STRUCT) || (type == DPEL_FLOAT_STRUCT) || (type == DPEL_INT_STRUCT) ||
			(type == DPEL_LANGSTRING_STRUCT) || (type == DPEL_STRING_STRUCT) || (type == DPEL_TIME_STRUCT) ||
			(type == DPEL_UINT_STRUCT) )
		{
			if( dynlen( dpeTree ) >= typeIdx )
				dpeTree[typeIdx] = names[dpeIdx][typeIdx];
			else
				dynAppend( dpeTree, names[dpeIdx][typeIdx] );
			continue; 
		}
		if( dynlen( dpeTree ) == 0 ) continue;	// 'root' DPE
		string dpeName = "";
		for( int n = 2 ; n < typeIdx ; n++ )	// Start from 2 because 1 is DP type name
		{
			if( dpeName != "" ) dpeName += ".";
			dpeName += dpeTree[n];
		}
		if( dpeName != "" ) dpeName += ".";
		dpeName += names[dpeIdx][typeIdx];
		dynAppend( allDpeNames, dpeName );
	}
}

//DeleteArchiveConfigForDp:
// @ASA feb11
//
//  Parameters:
//  dp - (in) datapoint
//  dpType - (in) datapoint type
// 
void DeleteArchiveConfigForDp( string dp, string dpType )
{
  dyn_string allDpeNames, exceptionInfo;
  string     dpe, archiveClass;
  int        num, smoothKind, smoothType, smoothProcedure;
  float      deadband, timeInterval;
  bool       configExists, isActive;    
  
  // get all dpe(s) for device be deleted
  BuildDpeListForDpType( dpType, allDpeNames, exceptionInfo );
  //DebugN( "DeleteArchiveConfigForDp() dpType, allDpeNames", dpType, allDpeNames );
  if( dynlen( exceptionInfo ) > 0 )
  {
    DebugN( num, "DeleteArchiveConfigForDp() failed" );
    return;			
  }
  
  for( num = dynlen( allDpeNames ) ; num > 0 ; num-- ) // for all dpe in datapoint
  {
    dpe = UseDpeName( dpType, dp, allDpeNames[num] );
    
    // try search archives for dpe... see func GetArchiveParams();
    fwArchive_get( dpe, configExists,
                   archiveClass, smoothType, smoothProcedure, deadband, timeInterval, isActive,
                   exceptionInfo);
    if( dynlen( exceptionInfo ) > 0 )    
    {
      DebugN( num, "DeleteArchiveConfigForDp().fwArchive_get(): failed for dp, dpType, dpe, archiveClass",
              dp, dpType, dpe, archiveClass );
      dynClear( exceptionInfo );
      continue;
    }
    if( archiveClass == "" ) continue;
    fwArchive_delete( dpe, exceptionInfo );
    if( dynlen( exceptionInfo ) > 0 )    
    {
      DebugN( num, "DeleteArchiveConfigForDp().fwArchive_delete(): failed for dp, dpType, dpe, archiveClass",
              dp, dpType, dpe, archiveClass );
      dynClear( exceptionInfo );
      continue;
    }
  } // forend
  return;
}

