#uses "vclArchiveConfig.ctl"  // Load CTRL lib



// Enums for fast log session state
const int LHC_VAC_ARCHIVE_SESSION_NONE = 0;
const int LHC_VAC_ARCHIVE_SESSION_CONFIG = 1;
const int LHC_VAC_ARCHIVE_SESSION_READY = 2;
const int LHC_VAC_ARCHIVE_SESSION_STARTING = 3;
const int LHC_VAC_ARCHIVE_SESSION_START_FAILED = 4;
const int LHC_VAC_ARCHIVE_SESSION_RUNNING = 5;
const int LHC_VAC_ARCHIVE_SESSION_STOPPING = 6;
const int LHC_VAC_ARCHIVE_SESSION_STOP_FAILED = 7;
const int LHC_VAC_ARCHIVE_SESSION_FINISHED = 8;

// Name of archive DP
const string LHC_VAC_ARCHIVE_SESSION_DP = "_VacArchiveSession";

// Name of archive DP
const string LHC_VAC_ARCHIVE_SESSION_NUMBER = "_VacArchiveSessionNumber";

//LhcVacArchiveSessionDelete:
/** Delete session contained in given DDP

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param sessionDpName: in, name DDP to be deleted
	@param exceptionInfo: out, details of any exceptions are returned here

	@return None.
	@author S.Merker
*/
void LhcVacArchiveSessionDelete(string sessionDpName, dyn_string &exceptionInfo)
{
  // Check if archiveDpName exists
	 if(sessionDpName == "")
	 {
		  fwException_raise(exceptionInfo, 
			   "ERROR", "LhcVacArchiveSessionDelete(): Empty session DP name", "");
		  return;
	 }
	 if(! dpExists(sessionDpName))
	 {
		  fwException_raise(exceptionInfo, 
			   "ERROR", "LhcVacArchiveSessionDelete(): Session DP <" + sessionDpName + "> does not exist", "");
		  return;
	 }

  // Check for status: running session can not be deleted
	 int				    status;
	 dpGet(sessionDpName + ".Status:_original.._value", status);
	 dyn_errClass err = getLastError();
	 if(dynlen(err) > 0)
	 {
		  fwException_raise(exceptionInfo, 
			   "ERROR", "LhcVacArchiveSessionDelete(): Failed to get session status for <" + sessionDpName + ">", "");
		  throwError(err);
		  return;
	 }
  string statusStr;
	 switch(status)
	 {
	 case LHC_VAC_ARCHIVE_SESSION_STARTING:
	 case LHC_VAC_ARCHIVE_SESSION_RUNNING:
	 case LHC_VAC_ARCHIVE_SESSION_STOPPING:
		  LhcVacArchiveSessionGetStateString(status, statusStr, exceptionInfo);
		  fwException_raise(exceptionInfo, 
			   "ERROR", "LhcVacArchiveSessionDelete(): Can't delete " + statusStr + "session for <" +
			   sessionDpName + ">", "");
		  return;
 	}

	 // Delete DP
	 dpDelete(sessionDpName);
	 err = getLastError();
	 if(dynlen(err) > 0)
	 {
		  fwException_raise(exceptionInfo, 
			   "ERROR", "LhcVacArchiveSessionDelete(): Failed to delete session for DP " + sessionDpName, "");
		  throwError(err);
	 }
  // Update counter of existing sessions
	dyn_string dpList = dpNames("*", LHC_VAC_ARCHIVE_SESSION_DP);
	dpSetWait( LHC_VAC_ARCHIVE_SESSION_NUMBER + ".SessionNumber:_original.._value", dynlen(dpList ) );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacArchiveSessionDelete(): Failed to set new session counter " + dynlen(dpList ), "" );
		throwError( err );
		return;
	}
}

//LhcVacArchiveSessionCreateDp:
/** Create new Dp type of "_VacArchiveSession", assign new unique name

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param newDpName: in, name of new created session DP
	@param copyFromDp: in,name of another session DP from where session parameters shall be copied.
	@param exceptionInfo: out, details of any exceptions are returned here
	@return None.
  @author S.Merker
*/
void LhcVacArchiveSessionCreateDp(string &newDpName, string copyFromDp, dyn_string &exceptionInfo)
{
  // Find number for new session
  int num, maxNum;
  dyn_string   dpList = dpNames("*", LHC_VAC_ARCHIVE_SESSION_DP), exceptionInfo;
  dyn_errClass err;
  float        smoothDeadBand,smoothTime;
  int          smoothType;
  	// Check if source DP for copy exists
	if( copyFromDp != "" )
	{
		if( ! dpExists( copyFromDp ) )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacArchiveSessionCreateDp(): DP <" + copyFrom + "> to copy from does not exist", "" );
			return;
		}
	}
  for(int n = dynlen(dpList) ; n > 0 ; n--)
	 {
		  if(sscanf(dpSubStr(dpList[n], DPSUB_DP), LHC_VAC_ARCHIVE_SESSION_DP + "%d", num) != 1)
		  {
			   fwException_raise(exceptionInfo, 
				    "ERROR", "LhcVacArchiveSessionCreateDp(): Bad Archive DP name <" + dpList[n] + ">", "");
			   return;
		  }
		  if(num > maxNum)
    {
      maxNum = num;
    }
	 }
	 //maxNum = maxNum+1;
   maxNum++;
	 // Create new archive DP
  newDpName = LHC_VAC_ARCHIVE_SESSION_DP + maxNum;
  dpCreate(newDpName, LHC_VAC_ARCHIVE_SESSION_DP);
  if(! dpExists(newDpName))
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacArchiveSessionCreateDp(): Failed to create DP <" + newDpName + ">", "");
    return;
  }

  // Copy real DPs from another session
  if(copyFromDp != "")
  {    
    dpGet( copyFromDp + ".DpList:_original.._value", dpList,
      copyFromDp + ".SmoothTime:_online.._value", smoothTime,
      copyFromDp + ".SmoothDeadBand:_online.._value", smoothDeadBand,
      copyFromDp + ".SmoothType:_online.._value", smoothType);
		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacArchiveSessionCreateDp(): Failed to get original session attributes <" +
				copyFromDp + ">", "" );
			throwError( err );
			dpDelete( newDpName );
			return;
		}
		dpSetWait( newDpName + ".DpList:_original.._value", dpList,
      newDpName + ".SmoothTime:_original.._value", smoothTime,
      newDpName + ".SmoothDeadBand:_original.._value", smoothDeadBand,
      newDpName + ".SmoothType:_original.._value", smoothType);
		err = getLastError( );
		if( dynlen( err ) > 0 )
		{
			fwException_raise( exceptionInfo, 
				"ERROR", "LhcVacFastLogNewSession(): Failed to copy original session attributes to <" + newDpName + ">", "" );
			throwError( err );
			dpDelete( newDpName );
			return;
		}
  }
  // Update counter of existing sessions
	dpSetWait(LHC_VAC_ARCHIVE_SESSION_NUMBER + ".SessionNumber:_original.._value", dynlen(dpList ) );
	err = getLastError( );
	if( dynlen( err ) > 0 )
	{
		fwException_raise( exceptionInfo, 
			"ERROR", "LhcVacArchiveSessionCreateDp(): Failed to set new session counter " + dynlen(dpList ), "" );
		throwError( err );
		return;
	}
}
//LhcVacArchiveSessionGetStateString:
/** Converts status enum into human readable string to be shown

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param status: in, session status
	@param statusStr: out, result string
	@param exceptionInfo: out, details of any exceptions are returned here

	@return None.
	@author S.Merker
*/
void LhcVacArchiveSessionGetStateString(int status, string &statusStr, dyn_string &exceptionInfo)
{
	 switch(status)
	 {
	 case LHC_VAC_ARCHIVE_SESSION_NONE:
		  statusStr = "Free";
		  break;
	 case LHC_VAC_ARCHIVE_SESSION_CONFIG:
		  statusStr = "Configuration";
		  break;
	 case LHC_VAC_ARCHIVE_SESSION_READY:
		  statusStr = "Ready";
		  break;
	 case LHC_VAC_ARCHIVE_SESSION_STARTING:
		  statusStr = "Starting";
		  break;
	 case LHC_VAC_ARCHIVE_SESSION_START_FAILED:
		  statusStr = "Start failed";
		  break;
	 case LHC_VAC_ARCHIVE_SESSION_RUNNING:
		  statusStr = "Running";
		  break;
	 case LHC_VAC_ARCHIVE_SESSION_STOPPING:
		  statusStr = "Stopping";
		  break;
	 case LHC_VAC_ARCHIVE_SESSION_STOP_FAILED:
		  statusStr = "Stop failed";
		 break;	
	 case LHC_VAC_ARCHIVE_SESSION_FINISHED:
		  statusStr = "Finished";
		  break;	
 	default:
		  fwException_raise(exceptionInfo,
		 	  "ERROR", "LhcVacArchiveSessionGetStateString: unsupported status <" + status + ">", "");
	 }
}
