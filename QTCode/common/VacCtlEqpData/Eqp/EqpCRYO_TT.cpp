//	Implementation of EqpCRYO_TT class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpCRYO_TT.h"

#include "DataPool.h"
#include "FunctionalTypeCRYO_TT.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			153,153,255
#define COLOR_GREEN				153,255,153
#define COLOR_YELLOW			255,255,153
#define COLOR_RED				255,153,153

//	Main state bits to be analyzed
#define	RR1_INVALID		(0x1)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpCRYO_TT::EqpCRYO_TT(const Eqp &source) : Eqp(source)
{
	rr1Online = rr1Replay = RR1_INVALID;
	tOnline = tReplay = 0;
	valveInterlock = false;
	switch(ctrlSubType)
	{
	case CmAvg:	// Avg
	case CmMax:	// Max
		functionalType = FunctionalType::CRYO_TT_EXT;
		break;
	default:
		functionalType = FunctionalType::CRYO_TT;
		break;
	}
	nominalT = 0;
	const QString attrValue = getAttrValue("NominalT");
	if(!attrValue.isEmpty())
	{
		bool ok;
		nominalT = attrValue.toInt(&ok);
		if(!ok)
		{
			nominalT = 0;
		}
		else if((nominalT != 2) && (nominalT != 4))
		{
			nominalT = 0;
		}
	}
}

EqpCRYO_TT::~EqpCRYO_TT()
{
}

/*
**	FUNCTION
**		Return vacuum type for this device. Thermometers do not have
**		vacuum type coded in the name - they are not related to any
**		vacuum.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Vacuum type of device
**
**	CAUTIONS
**		None
*/
int EqpCRYO_TT::getVacType(void) const
{
	int result = vacType;
	switch(ctrlSubType)
	{
	case QrlLineB:
	case QrlLineC:
		result = VacType::Qrl;
		break;
	default:
		result = VacType::Cryo | VacType::RedBeam | VacType::RedBeam | VacType::BlueBeam |
			VacType::CrossBeam | VacType::CommonBeam;
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpCRYO_TT::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	addDpeNameToList(stateDpes, "RR1");
}

/*
**	FUNCTION
**		Return list of all DPEs, related to analog value of device. The list is used
**		to build value history
**
**	ARGUMENTS
**		valueDpes	- Variable where all value DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpCRYO_TT::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	addDpeNameToList(valueDpes, "T");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpCRYO_TT::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "T", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpCRYO_TT::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "T", mode);
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "EqpStatus", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpCRYO_TT::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "T"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(tOnline == newValue)
			{
				return;
			}
			tOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "T"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(tReplay == newValue)
			{
				return;
			}
			tReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, tReplay, color);
	}
	else
	{
		getMainColor(rr1Online, tOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Return main value for thermometer - temperature
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpCRYO_TT::getMainValue(DataEnum::DataMode mode)
{
	return mode == DataEnum::Replay ? tReplay : tOnline;
}
#endif

/*
**	FUNCTION
**		Check if main value for thermometer (temperature) is valid or not.
**		Latest known requirement: even invalid value shall be shown,
**		so the value is always considered to be valid
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
bool EqpCRYO_TT::isMainValueValid(DataEnum::DataMode /* mode */)
{
	return true;
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		t			- Value of T DPE
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT::getMainColor(unsigned rr1, float t, QColor &color)
{
	if(rr1 & RR1_INVALID)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(t < 5)
	{
		color.setRgb(COLOR_GREEN);
	}
	else if(t < 40)
	{
		color.setRgb(COLOR_YELLOW);
	}
	else
	{
		color.setRgb(COLOR_RED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	getMainValueString(string, mode);
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT::getMainValueString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainValueString(rr1Replay, tReplay, string);
	}
	else
	{
		getMainValueString(rr1Online, tOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		t			- Value of T DPE
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT::getMainValueString(unsigned /* rr1 */, float t, QString &string)
{
	char buf[32];
	if(t > 99.9)
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d", (int)t);
#else
		sprintf(buf, "%d", (int)t);
#endif
	}
	else if(t > 9.99)
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%.1f", t);
#else
		sprintf(buf, "%.1f", t);
#endif
	}
	else
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%.2f", t);
#else
		sprintf(buf, "%.2f", t);
#endif
	}
	string = buf;
}
#endif

/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("RR1");
	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	getMainValueString(rr1, false, state);
	getMainColor(rr1, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif



/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	addDpeNameToList(dpes, "EqpStatus");
	addDpeNameToList(dpes, "RR1");
	addDpeNameToList(dpes, "T");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history)
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpCRYO_TT::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	if(rr1 & RR1_INVALID)
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Check temperature if needed
	if(criteria.isUseTemperatureLimit())
	{
		float t = mode == DataEnum::Replay ? tReplay : tOnline;
		if(t > criteria.getTemperatureLimit())
		{
			return 1;
		}
	}

	// Last chance - criteria on temperature grow
	if(criteria.isUseTemperatureGrowLimit())
	{
		// Can not decide right now, more info is needed
		return 2;
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Return parameters for history query, required for device list
**
**	ARGUMENTS
**		dpe		- [out] DPE, for which history is required
**		start	- [out] start time of history query
**		end		- [out] end time of history query
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT::getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUseTemperatureGrowLimit())
	{
		dpe = "";
		return;
	}
	dpe = dpName;
	dpe += ".T";
	start = criteria.getTemperatureStartTime();
	if(criteria.isTemperatureEndTimeNow())
	{
		end = QDateTime::currentDateTime();
	}
	else
	{
		end = criteria.getTemperatureEndTime();
	}
}

#endif

/*
**	FUNCTION
**		Return grow factor for main value (pressure) to check if
**		device matches device list criteria
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Pressure grow factor; or
**		0 if criteria does not contain pressure grow
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpCRYO_TT::getGrowFactorForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUseTemperatureGrowLimit())
	{
		return 0.0;
	}
	return criteria.getTemperatureGrowLimit();
}

int EqpCRYO_TT::getSpikeFilterForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUseTemperatureGrowLimit())
	{
		return 0;
	}
	return criteria.getTemperatureSpikeInterval();
}

#endif


/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpCRYO_TT::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if((pCriteria->getFunctionalType() != FunctionalType::CRYO_TT_EXT)
		&& (pCriteria->getFunctionalType() != FunctionalType::CRYO_TT))
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if(checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	switch(pCriteria->getSubType())
	{
	case FunctionalTypeCRYO_TT::NominalTemp_2:
		if(nominalT != 2)
		{
			return;
		}
		break;
	case FunctionalTypeCRYO_TT::NominalTemp_4:
		if(nominalT != 4)
		{
			return;
		}
		break;
	default:
		break;
	}

	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case FunctionalTypeCRYO_TT::Valid:
			if(!(rr1Online & RR1_INVALID))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	case EqpMsgCriteria::MainValue:
		if(!(rr1Online & RR1_INVALID))
		{
			if(pCriteria->isReverse())
			{
				if(tOnline > pCriteria->getUpperLimit())
				{
					pCriteria->setMatch(false, true, tOnline);
					return;
				}
				else if(tOnline < pCriteria->getLowerLimit())
				{
					pCriteria->setMatch(true, true, tOnline);
					return;
				}
				// Between lower and upper limit - no change
				return;
			}
			else
			{
				if(tOnline > pCriteria->getUpperLimit())
				{
					pCriteria->setMatch(true, true, tOnline);
					return;
				}
				else if(tOnline < pCriteria->getLowerLimit())
				{
					pCriteria->setMatch(false, true, tOnline);
					return;
				}
				// Between lower and upper limit - no change
				return;
			}
		}
		break;
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}

