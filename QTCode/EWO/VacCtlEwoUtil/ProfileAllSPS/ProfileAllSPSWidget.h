#ifndef PROFILEALLSPSWIDGET_H
#define	PROFILEALLSPSWIDGET_H

#include "VacCtlEwoUtilExport.h"

// Dialog displaying pressure profile of all sextants of SPS ring

#include "DataEnum.h"
#include "VacEqpTypeMask.h"
#include "EqpMsgCriteria.h"

#include "ExtendedLabel.h"
#include "ProfileImage.h"

#include <qwidget.h>

class MainPart;

class ProfileAllSPSWidget : public QWidget
{
	Q_OBJECT

public:
	VACCTLEWOUTIL_EXPORT ProfileAllSPSWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);
	VACCTLEWOUTIL_EXPORT ~ProfileAllSPSWidget();

	VACCTLEWOUTIL_EXPORT void dataReady(void);

signals:
	void profileDpDown(int button, int mode, int x, int y, int dummy, const char *dpName);

protected:
	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// List of profile images
	QList<ProfileImage *>	images;

	// List of main part labels
	QList<ExtendedLabel *>	mpLabels;

	// Equipment type mask for all possible equipment types
	VacEqpTypeMask			allEqpMask;

	// Equipment type mask
	VacEqpTypeMask			eqpMask;

	// Pointer to main part to which popup menu is related
	MainPart				*pMainPart;

	// ID of 'Synoptic' item in popup menu
	QAction 				*pSynopticMenuAction;

	void buildInitialEqpMask(void);
	void buildLayout(void);

private slots:
	void limitsChange(int min, int max);
	void dpMouseDown(int button, int mode, int x, int y, const char *dpName);
	void labelPressed(QMouseEvent *e);
	void tsMenu(QAction * pAction);
};

#endif	// PROFILEALLSPSWIDGET_H
