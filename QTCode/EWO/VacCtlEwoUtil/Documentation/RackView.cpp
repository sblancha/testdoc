#include "RackView.h"

#include "qpainter.h"
#include <QWheelEvent>
#include "RackWidget.h"
#include "Rack.h"
#include "RackIcon.h"
#include "DataPool.h"

#define RACK_WIDTH 0.483
#define RACK_HEIGHT_UNIT 0.0445
#define RACK_PIXEL_HEIGHT 675
#define RACK_PIXEL_WIDTH 200

RackView::RackView(QWidget *parent, Qt::WindowFlags f) :
QWidget(parent, f)
{
	QSize mySize(1400, 900);
	setFixedSize(mySize);
	scale = 1;
	buildingName = "";
	//rackWidgets.append( new RackWidget(this));

//	for (int i = 0; i < rackWidgets.length(); i++)
//	{
	//	rackWidgets.at(i)->setRackIndex(i);
	//}
}

RackView::~RackView()
{
	for (int i = 0; i < rackWidgets.length(); i++)
	{
		delete rackWidgets.at(i);
	}
}

RackView * RackView::create(QWidget *parent, QString buildingName)
{
	RackView * pView = NULL;

	pView = new RackView(parent);
	pView->buildingName = buildingName;
	//pView->rack = rack;
	//pView->addWidget(rack);
	return pView;
}

void RackView::showAllRacks()
{
	//remove old widgets
	int oldNumberOfWidgets = rackWidgets.length();
	for (int i = oldNumberOfWidgets - 1; i >= 0; i--)
	{
		delete rackWidgets.at(i);
		rackWidgets.removeLast();
	}

	DataPool &pool = DataPool::getInstance();
	QList<Rack *> &rackList = pool.getRackList();
	QString rackName, rackBuildingName;

	for (int i = 0; i < rackList.length();i++)
	{
		rackName = rackList.at(i)->getRackName();
		rackBuildingName = rackName.right(rackName.length() - rackName.indexOf("=") - 1);
		if (rackBuildingName.compare(buildingName) == 0)
		{
			addWidget(rackList.at(i));
		}
	}

}
void RackView::addWidget(Rack * rack)
{
	RackWidget * newWidget = new RackWidget(this);
	newWidget->setRackName(rack->getRackName());
	newWidget->setRackIndex(rackWidgets.length());
	newWidget->makeEquipmentList(*rack);
	rackWidgets.append(newWidget);

	QSize mySize(220*rackWidgets.length()*scale + 100, 900);
	setFixedSize(mySize);

}

void RackView::paintEvent(QPaintEvent *pEvent)
{

}

void RackView::wheelEvent(QWheelEvent *pEvent)
{
	
	QPoint delta = pEvent->angleDelta();
	scale += delta.ry() / 2400.0;
	if (scale < 0.1)
		scale = 0.1;

	for (int i = 0; i < rackWidgets.length(); i++)
	{
		rackWidgets.at(i)->setScale(scale);
		rackWidgets.at(i)->recalculatePosition();
	}

	int sizeX = 300 * rackWidgets.length()*scale;
	if (sizeX < 900) sizeX = 900;
	
	int sizeY = 900 * scale;
	if (sizeY < 900) sizeY = 900;

	QSize mySize(sizeX,sizeY);
	setFixedSize(mySize);

	pEvent->accept();
	this->update();
}

void RackView::setViewStart(int viewStart, int viewWidth, bool dragging){

}

void RackView::selectionChanged(RackIcon * rackIcon)
{
	for (int i = 0; i < rackWidgets.length(); i++)
	{
		rackWidgets.at(i)->clearSelection();
	}

	rackIcon->selectEqp();
}

void RackView::selectEquipment(QString equipmentName)
{
	for (int i = 0; i < rackWidgets.length(); i++)
	{
		rackWidgets.at(i)->clearSelection();
		RackIcon * pRackIcon = rackWidgets.at(i)->getRackIcon(equipmentName);
		if (pRackIcon != NULL)
			pRackIcon->selectEqp();
	}
}

int RackView::renderWidget(QPainter &painter, int index)
{
	if (index < 0 || index >= rackWidgets.count())
	{
		return -1;
	}
	
	qreal oldScale = rackWidgets.at(index)->getScale();
	rackWidgets.at(index)->setScale(1.0);
	rackWidgets.at(index)->recalculatePosition();
	rackWidgets.at(index)->render(&painter, QPoint(), QRegion(), QWidget::DrawChildren);
	rackWidgets.at(index)->setScale(oldScale);
	rackWidgets.at(index)->recalculatePosition();
	return 1;
}

int RackView::getWidgetHeight()
{
	if (rackWidgets.count() > 0)
	{
		return rackWidgets.at(0)->getRackPixelHeight();
	}
	else
		return -1;
}