#ifndef	VACICONVPGMPR_H
#define	VACICONVPGMPR_H

#include "VacIcon.h"

class EqpVPGMPR;

class VACCTLEWOUTIL_EXPORT VacIconVPGMPR : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPGMPR(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPGMPR();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	void setDirection(VacIconContainer::Direction newDirection);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVPGMPR *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, unsigned rr1, float pr, bool plcAlarm);

	void setPainterForMainImage(QPainter &painter, unsigned rr1, float pr, bool plcAlarm);
};

#endif	// VACICONVPGMPR_H
