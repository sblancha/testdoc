//	Implementation of ImageItem class
/////////////////////////////////////////////////////////////////////////////////

#include "ImageItem.h"

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

ImageItem::ImageItem() : backColor(Qt::white), foreColor(Qt::black)
{
	// Default values = default PVSS values for line
	lineWidth = 1;
	penStyle = Qt::SolidLine;
	capStyle = Qt::FlatCap;
	joinStyle = Qt::BevelJoin;
	visible = true;
}

ImageItem::~ImageItem()
{
}

void ImageItem::parse(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement())
		{
			break;
		}
		if(reader.isStartElement())
		{
			if(!reader.name().compare("properties", Qt::CaseInsensitive))
			{
				parseProperties(reader);
			}
			else
			{
				parseUnknown(reader);
			}
		}
	}
}

void ImageItem::setForePen(QPainter &painter)
{
	QPen pen(foreColor);
	pen.setWidth(lineWidth);
	pen.setStyle(penStyle);
	pen.setCapStyle(capStyle);
	pen.setJoinStyle(joinStyle);
	painter.setPen(pen);
}

void ImageItem::parseProperties(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement()) // && (!reader.name().compare("shape", Qt::CaseInsensitive)))
		{
#ifdef XML_PANEL_PARSE
//			printf("ImageItem::parseProperties(): isEndElement %s at %d\n",
//				reader.name().toString().toLatin1().constData(), (int)reader.lineNumber());
#endif
			break;
		}
		if(reader.isStartElement())
		{
#ifdef XML_PANEL_PARSE
//			printf("ImageItem::parseProperties(): isStartElement %s at %d\n",
//				reader.name().toString().toLatin1().constData(), (int)reader.lineNumber());
#endif
			if(!reader.name().compare("prop", Qt::CaseInsensitive))
			{
				parseProp(reader);
			}
			else if(!reader.name().compare("extended", Qt::CaseInsensitive))
			{
				parseProperties(reader);
			}
			else
			{
				parseUnknown(reader);
			}
		}
	}
}

void ImageItem::parseProp(QXmlStreamReader &reader)
{
	QStringRef propName = reader.attributes().value("name");
	if(!propName.compare("ForeColor", Qt::CaseInsensitive))
	{
		setForeColor(parseColor(reader.readElementText(), Qt::black));
	}
	else if(!propName.compare("BackColor", Qt::CaseInsensitive))
	{
		setBackColor(parseColor(reader.readElementText(), Qt::white));
	}
	else if(!propName.compare("Visible", Qt::CaseInsensitive))
	{
		visible = !reader.readElementText().compare("True", Qt::CaseInsensitive);
	}
	else if(!propName.compare("LineType", Qt::CaseInsensitive))
	{
		// Throw away surrounding []
		QString string = reader.readElementText();
		QString truncated = string.mid(1, string.length() - 2);
//printf("LineType <%s>\n", truncated.toLatin1().constData());

		// 0 = Pen style
		QString part = truncated.section(',', 0, 0);
//printf("Part 0: <%s>\n", part.toLatin1().constData());
		if(!part.compare("solid", Qt::CaseInsensitive))
		{
			setPenStyle(Qt::SolidLine);
		}
		else if(!part.compare("dashed", Qt::CaseInsensitive))
		{
			setPenStyle(Qt::DashLine);
		}
		else if(!part.compare("dotted", Qt::CaseInsensitive))
		{
			setPenStyle(Qt::DotLine);
		}
		else if(!part.compare("dash_dot", Qt::CaseInsensitive))
		{
			setPenStyle(Qt::DashDotLine);
		}
		else if(!part.compare("dash_dot_dot", Qt::CaseInsensitive))
		{
			setPenStyle(Qt::DashDotDotLine);
		}

		// 1 = oneColor ??? not clear

		// 2 = JoinStyle
		part = truncated.section(',', 2, 2);
//printf("Part 2: <%s>\n", part.toLatin1().constData());
		if(!part.compare("JoinMiter", Qt::CaseInsensitive))
		{
			setJoinStyle(Qt::MiterJoin);
		}
		else if(!part.compare("JoinBevel", Qt::CaseInsensitive))
		{
			setJoinStyle(Qt::BevelJoin);
		}
		else if(!part.compare("JoinRound", Qt::CaseInsensitive))
		{
			setJoinStyle(Qt::RoundJoin);
		}

		// 3 = CapStyle
		part = truncated.section(',', 3, 3);
//printf("Part 3: <%s>\n", part.toLatin1().constData());
		if(!part.compare("CapButt", Qt::CaseInsensitive))
		{
			setCapStyle(Qt::FlatCap);
		}
		else if(!part.compare("CapRound", Qt::CaseInsensitive))
		{
			setCapStyle(Qt::RoundCap);
		}
		else if(!part.compare("CapProjecting", Qt::CaseInsensitive))
		{
			setCapStyle(Qt::SquareCap);
		}
		
		// 3 = line width
		part = truncated.section(',', 4, 4);
//printf("Part 4: <%s>\n", part.toLatin1().constData());
		setLineWidth(part.toShort());
	}
	else if(!propName.compare("Geometry", Qt::CaseInsensitive))
	{
		float v1, v2, v3, v4, v5, v6;
		if(sscanf(reader.readElementText().toLatin1().constData(), "%f %f %f %f %f %f",
			&v1, &v2, &v3, &v4, &v5, &v6) == 6)
		{
			QTransform newTransform(v1, v2, v3, v4, v5, v6);
			transform = newTransform;
		}
	}
	else if(!propName.compare("ToolTipText", Qt::CaseInsensitive))
	{
		parseToolTipText(reader);
	}
	else
	{
		parseUnknown(reader);
	}
}

void ImageItem::parseToolTipText(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement())
		{
#ifdef XML_PANEL_PARSE
			printf("ImageItem::parseToolTipText(): stop at %d: end of %s\n",
				(int)reader.lineNumber(), reader.name().toString().toLatin1().constData());
#endif
			break;
		}
		if(reader.isStartElement())
		{
			if(!reader.name().compare("prop", Qt::CaseInsensitive))
			{
				QStringRef propName = reader.attributes().value("name");
				if(!propName.compare("en_US.iso88591", Qt::CaseInsensitive))
				{
					toolTipText = reader.readElementText();
					continue;
				}
			}
			parseUnknown(reader);
		}
	}
}

void ImageItem::parseUnknown(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement())
		{
			break;
		}
		if(reader.isStartElement())
		{
			parseUnknown(reader);
		}
	}
}

QColor ImageItem::parseColor(const QString &string, const QColor &defaultColor)
{
	if(string == "_Transparent")
	{
		return QColor(0, 0, 0, 0);
	}
	else
	{
		int a, r, g, b;
		if (sscanf(string.toLatin1().constData(), "{%d,%d,%d,%d}", &r, &g, &b, &a) == 4)
		{
			return QColor(r, g, b, a);
		}
		else if (sscanf(string.toLatin1().constData(), "{%d,%d,%d}", &r, &g, &b) == 3)
		{
			return QColor(r, g, b);
		}
	}
	return QColor(defaultColor);
}
