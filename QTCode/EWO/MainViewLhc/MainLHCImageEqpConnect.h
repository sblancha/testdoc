#ifndef MAINLHCIMAGEEQPCONNECT_H
#define MAINLHCIMAGEEQPCONNECT_H

//	Class provides equipment connection functionality for
//	MainLHCView. MainLHCView itself can not derive from
//	InterfaceEqp directly because both InterfaceEqp and QWidget
//	are derived from QObject - and such situation can not be
//	handled by Qt correctly. Message from one of Qt forums:
//
//	Your class inherits from multiple QObject based classes, normally
//	in C++ this could be handled by virtual inheritance, but some parts
//	of QObject is not made compativle with this, so that is no go.

#include "InterfaceEqp.h"

class MainLHCImageEqpConnect : public InterfaceEqp
{
	Q_OBJECT

public:
	MainLHCImageEqpConnect() { connected = false; }
	virtual ~MainLHCImageEqpConnect() {}

	void connect(void);

public slots:
	// Override slot of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

signals:
	// Signal is emitted when state of one of devices has been changed
	void stateChanged(void);

protected:
	// Flag indicating if connection to equipment was made - connection
	// shall only be done ONCE
	bool	connected;
};

#endif	// MAINLHCIMAGEEQPCONNECT_H

