
// Configuration and auxilliary functions for sms notifications
// about vacuum interlock activation

//#uses "lhcVacSectors.ctl"	// Load CTRL library
#uses "email.ctl"					// Load standard PVSS CTRL library

// Name of DP type containing E-mail configuration
const string VAC_SMS_CONFIG_DP_TYPE = "_VacOkSmsConfig";

// Name of DP containing E-mail configuration
const string VAC_SMS_CONFIG_DP = "_VacOkSmsConfig";
// Set name of SMTP server
void VacSmsSetServer( string serverName, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	VacSmsGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dpSetWait( configDp + ".server", serverName );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsSetServer(): dpSetWait() failed", "" );
	}
}
// Set name of client computer ('domain' for emSendMail())
void VacSmsSetClient( string clientName, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	VacSmsGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dpSetWait( configDp + ".client", clientName );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsSetClient(): dpSetWait() failed", "" );
	}
}
// Set silence time
void VacOkSmsSetDeadTime( int dTime, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	VacSmsGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dpSetWait( configDp + ".deadTime", dTime );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsSetDeadTime(): dpSetWait() failed", "" );
	}
}
// Get silence time
void VacSmsGetDeadTime( int &dTime, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	VacSmsGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dpGetWait( configDp + ".deadTime", dTime );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsSetDeadTime(): dpGet() failed", "" );
	}
}

// Set address of mail sender. It looks like sender shall be recognized
// by SMTP server
void VacSmsSetSender( string senderName, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	VacSmsGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dpSetWait( configDp + ".sender", senderName );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsSetSender(): dpSetWait() failed", "" );
		throwError( err );
	}
}
void VacSmsGetRecipientsAll( dyn_string &recipients, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	VacSmsGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dpGet( configDp + ".recipientsAll", recipients );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsGetRecipientsAll(): dpGet() failed", "" );
		throwError( err );
	}
}
// Set address(es) of recipients to be notified about problems in all sectors
// Every item in list shall be valid E-mail address
void VacSmsSetRecipientsAll( dyn_string recipients, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;

	dynClear( exceptionInfo );
	VacSmsGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dpSetWait( configDp + ".recipientsAll", recipients );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsSetSender(): dpSetWait() failed", "" );
		throwError( err );
	}
}
// Add recipient for specific vacuum sector
void VacSmsAddArcRecipient( string arc, string recipient, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;
	dyn_string		specArc, specRecipients;

	dynClear( exceptionInfo );
	if( arc == "" )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsAddSectorRecipient(): empty sector name", "" );
		return;
	}
	if( recipient == "" )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsAddSectorRecipient(): empty recipient address", "" );
		return;
	}
	VacSmsGetSpecRecipients( configDp, specArc, specRecipients, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Check if given recipient for given sector is already in list
	for( int n = dynlen( specArc ) ; n > 0 ; n-- )
	{
		if( specArc[n] != arc ) continue;
		if( specRecipients[n] == recipient ) return;	// Already in list
	}
	// Add new sector and recipient, write to config DP
	dynAppend( specArc, arc );
	dynAppend( specRecipients, recipient );
	dpSetWait( configDp + ".arcsSpecific", specArc,
		configDp + ".recipientsSpecific", specRecipients );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsAddArcRecipient(): dpSetWait() failed", "" );
		throwError( err );
	}
}
// Remove recipient for specific vacuum sector
void VacSmsRemoveArcRecipient( string arc, string recipient, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;
	dyn_string		specArc, specRecipients;

	dynClear( exceptionInfo );
	if( arc == "" )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsRemoveArcRecipient(): empty sector name", "" );
		return;
	}
	if( recipient == "" )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsRemoveArcRecipient(): empty recipient address", "" );
		return;
	}
	VacSmsGetSpecRecipients( configDp, specArc, specRecipients, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Check if given recipient for given sector is already in list
	int	n;
	for( n = dynlen( specArc ) ; n > 0 ; n-- )
	{
		if( specArc[n] != arc ) continue;
		if( specRecipients[n] == recipient )
		{
			dynRemove( specArc, n );
			dynRemove( specRecipients, n );
			break;
		}
	}
	if( n <= 0 ) return;	// Specified pair sector + recipient is not in list
	// Add new sector and recipient, write to config DP
	dpSetWait( configDp + ".arcsSpecific", specArc,
		configDp + ".recipientsSpecific", specRecipients );
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsRemoveArcRecipient(): dpSetWait() failed", "" );
		throwError( err );
	}
}
// Switch OFF notification for vacuum sector(s)
void VacSmsDisableSector( string sector, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;
	dyn_string		disabledSectors;

	dynClear( exceptionInfo );
	VacSmsGetDisabledSectors( configDp, disabledSectors, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Check if any changes are inserted
	bool	hasChanges = false;
	if (dynContains(disabledSectors, sector) > 0)
		return;
	dynAppend( disabledSectors, sector);
	// Write to config DP
	dpSetWait( configDp + ".sectorsOff", disabledSectors);
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsDisableSector(): dpSetWait() failed", "" );
		throwError( err );
	}
}
// Switch ON notification for vacuum sector(s)
void VacSmsEnableSector( dyn_string sectors, dyn_string &exceptionInfo )
{
	string				configDp;
	dyn_errClass	err;
	dyn_string		disabledSectors;
	dyn_time			endTimes;

	dynClear( exceptionInfo );
	if( dynlen( sectors ) == 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsEnableSector(): empty sector list", "" );
		return;
	}
	VacSmsGetDisabledSectors( configDp, disabledSectors, endTimes, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Check if any changes are inserted
	bool	hasChanges = false;
	for( int n = dynlen( sectors ) ; n > 0 ; n-- )
	{
		if( sectors[n] == "" ) continue;
		int	i;
		for( i = dynlen( disabledSectors ) ; i > 0 ; i-- )
		{
			if( disabledSectors[i] != sectors[n] ) continue;
			dynRemove( disabledSectors, n );
			dynRemove( endTimes, n );
			hasChanges = true;
			break;
		}
	}
	if( ! hasChanges ) return;	// No changes to list content
	// Write to config DP
	dpSetWait( configDp + ".sectorsOff", disabledSectors);
	err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsEnableSector(): dpSetWait() failed", "" );
		throwError( err );
	}
}
// Check if DP with email config exists, if not - create such DP
void VacSmsGetConfigDp( string &configDpName, dyn_string &exceptionInfo )
{
	configDpName = "";
	if( dpExists( VAC_SMS_CONFIG_DP ) )
	{
		configDpName = VAC_SMS_CONFIG_DP;
		return;
	}
	if( dpCreate( VAC_SMS_CONFIG_DP, VAC_SMS_CONFIG_DP_TYPE ) < 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsGetConfigDp(): dpCreate() failed", "" );
		return;
	}
	if( ! dpExists( VAC_SMS_CONFIG_DP ) )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsGetConfigDp(): config DP not created", "" );
		return;
	}
	configDpName = VAC_SMS_CONFIG_DP;
	dpSetWait(configDpName + ".server", "cernmx.cern.ch");
	dpSetWait(configDpName + ".client", "cern.ch");
	dpSetWait(configDpName + ".deadTime", 30);
	dpSetWait(configDpName + ".sender", "vacin@cern.ch");
}
// Read all recipients for specified arc
void VacSmsGetSpecRecipientsForArc( string &configDp, string arc,
	dyn_string &specRecipients, dyn_string &exceptionInfo )
{
	dyn_string allArc, allRecipients;
	VacSmsGetSpecRecipients( configDp, allArc, allRecipients, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	dynClear(specRecipients);
	int	i;
	for( i = dynlen( allArc ) ; i > 0 ; i-- )
		if(allArc[i] == arc)
			dynAppend(specRecipients, allRecipients[i]);
}
// Read all special sectors and their recipients
void VacSmsGetSpecRecipients( string &configDp, dyn_string &specSectors,
	dyn_string &specRecipients, dyn_string &exceptionInfo )
{
	VacSmsGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Get list of sectors with specific recipients
	dpGet( configDp + ".arcsSpecific", specSectors,
		configDp + ".recipientsSpecific", specRecipients );
	dyn_errClass err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacSmsGetSpecRecipients(): dpGet() failed", "" );
		throwError( err );
		return;
	}
	// Two lists must have the same length
	if( dynlen( specSectors ) != dynlen( specRecipients ) )
	{
		fwException_raise(exceptionInfo, "ERROR",
			"VacSmsGetSpecRecipients(): INTERNAL: " + dynlen( specSectors ) +
			" sectors but " + dynlen( specRecipients ) + " recipients", "" );
		dynClear( specSectors );
		dynClear( specRecipients );
		dpSetWait( configDp + ".arcsSpecific", specSectors,
			configDp + ".recipientsSpecific", specRecipients );
	}
}
// Read all sectors with disabled notification
void VacSmsGetDisabledSectors( string &configDp, dyn_string &disabledSectors, dyn_string &exceptionInfo )
{
	VacSmsGetConfigDp( configDp, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 ) return;
	// Get list of sectors with specific recipients
	dpGet( configDp + ".sectorsOff", disabledSectors);
	dyn_errClass err = getLastError();
	if( dynlen( err ) > 0 )
	{
		fwException_raise(exceptionInfo, "ERROR", "VacOkSmsGetDisabledSectors(): dpGet() failed", "" );
		throwError( err );
		return;
	}
}
// Decide if given sector is warm beam sector
bool VacOkSmsIsCryoSector( string sectorName )
{
	int	vacType;
	LhcVacGetVacTypeOfSector( sectorName, vacType );
//DebugN("VacOkSmsIsCryoSector:", sectorName, vacType);
	// Accept sectors for QRL and CRYO
	if( ( vacType == LVA_VACUUM_QRL ) || ( vacType == LVA_VACUUM_CRYO ) )
		return true;
	return false;
}
// Find corresponding Arc name for the sector name
// No generality - !!! convention is hardcoded !!!
bool VacOkFindArcForSector(string sectorName, string &arcName )
{
int 		sufPos;
string 	numStr;
//first, is it Q or M
	if((sufPos = strpos(sectorName, ".Q")) > 0 )
	{ 
//it is easy - just two figures from thr name on a proper position
		arcName = substr(sectorName, sufPos - 2, 2);
		return true;
	}
	else if((sufPos = strpos(sectorName, ".M")) > 0 )
	{
		string lr = substr(sectorName, sufPos - 2, 1);
		int num = substr(sectorName, sufPos - 1, 1);
		numStr = num;
//more complex if L -> then arc =(n-1)n, else n(n+1)
		if(lr == "L")
		{
			if (num > 1) 
				arcName = (num - 1) + numStr ;
			else
				arcName = "81";
		}
		else	//"R"
		{
			if (num <= 7)
				arcName = numStr + (num + 1);
			else
				arcName = "81";
		}
		return true;
	}
	else
	{
		arcName = sectorName;
		return true;
	}
}
void GetSpecificArcName(dyn_string &specArc)
{
	bool	isBorder;
	int 		n, coco, listLen, vacType;
	string		sectName, dpName, dpType, sector1, sector2, mainPart, arcName;
	dyn_string	exceptionInfo, deviceList, sectorList, err, mainParts;
   dyn_dyn_string sectors, domains; 
   sectorList = GetSectorList();
    
//DebugN("lhcVacOkSmsConfig.ctl::GetSpecificArcName:AllSectorList", dynlen(sectorList));
	LhcVacGetDevicesAtSectors( sectorList, deviceList, exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		fwExceptionHandling_display( exceptionInfo );
		return;
	}

	listLen = dynlen( deviceList );
//DebugN("ConnectDeviceList::deviceList:", listLen);
	dynClear(specArc); 
	for( n = 1 ; n <= listLen ; n++ )
	{
		dpName =  deviceList[n];
		dynClear( exceptionInfo );
		LhcVacDpType( dpName, dpType, exceptionInfo );
		if( dynlen( exceptionInfo ) > 0 )
		{
			fwExceptionHandling_display( exceptionInfo );
			continue;
		}
		switch( dpType )
		{
		case "CRYO_VACOK_RO":
		case "CRYO_VACOK_PLC":
		case "CRYO_VACOK":
			LhcVacDeviceVacLocation( dpName, vacType, sector1, sector2, mainPart, isBorder, exceptionInfo);
			sectName = sector1;
			VacOkFindArcForSector(sectName, arcName);
			
			if(dynContains(specArc, arcName) > 0)
				break;
			dynAppend(specArc, arcName);	
			break;
		}
	}
	return;

}
dyn_string 	GetSectorList()
{
	dyn_string	sectorList, mainParts, exceptionInfo;
  dyn_dyn_string sectors, domains;
  if(LhcVacGetAllSectors(mainParts, sectors, domains, exceptionInfo) < 0)
  {
    DebugTN("emailAlarmConfig.pnl: LhcVacGetAllSectors() failed: " + exceptionInfo);
    return false;
  }
  int nMp, nMpSec, nSectors;
  nMp = dynlen( sectors);
  for( int n = 1 ; n <= nMp ; n++ )
  {
    nMpSec = dynlen(sectors[n]);
    for( int k = 1 ; k <= nMpSec ; k++ )
    {
        dynAppend(sectorList, sectors[n][k]);
     }  
  }
  nSectors = dynlen(sectorList);   
  return(sectorList);
  
}

main()
{

}
