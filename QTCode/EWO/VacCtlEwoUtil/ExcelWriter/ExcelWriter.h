#ifndef	EXCELWRITER_H
#define	EXCELWRITER_H

#include "VacCtlEwoUtilExport.h"

// Class providing basic functionality to write file, that can be read
// by MS Excel. First version just generates XML file in format that can
// be understand by MS Excel

#include "ExcelWriterRow.h"

class ExcelWriterColumnFormat
{
public:
	ExcelWriterColumnFormat(int column) { this->column = column; }
	~ExcelWriterColumnFormat() {}

	// Access
	inline int getColumn(void) const { return column; }
	inline const QString &getTitle(void) const { return title; }
	inline void setTitle(const QString &title) { this->title = title; }
	inline int getWidth(void) const { return width; }
	inline void setWidth(int width) { this->width = width; }

protected:
	// Column number
	int 		column;

	// Column title
	QString		title;

	// Column width
	int			width;
};

class VACCTLEWOUTIL_EXPORT ExcelWriter
{
public:

	ExcelWriter();
	~ExcelWriter();

	void clear(void);
	int addRow(void);
	void setCellFormat(int row, int column, ExcelWriterCell::Format format);
	void setCellColor(int row, int column, const QColor &color);
	void setCellValue(int row, int column, const QString &string);
	void setCellValue(int row, int column, const QDateTime &dateTime, bool withMsec);
	void setCellValue(int row, int column, float value);
	void setCellValue(int row, int column, int value);
	void setCellComment(int row, int column, const QString &comment);

	void setColumnTitle(int column, const QString &title);
	void setColumnWidth(int column, int width);

	QString write(FILE *pFile);
	QString writeCsv(FILE *pFile);

protected:
	// List of all rows
	QList<ExcelWriterRow *> 			*pRowList;

	// List of column formats
	QList<ExcelWriterColumnFormat *>	*pColumnFormatList;


	ExcelWriterRow *findRow(int row);
	ExcelWriterColumnFormat *findColumnFormat(int column);

	QString writeHeader(FILE *pFile);
	QString writeStyles(FILE *pFile, QList<ExcelCellFormat *> &formats);
	int writeColoredStyle(FILE *pFile, ExcelCellFormat *pFormat, unsigned index);
	int writeColorlessStyle(FILE *pFile, ExcelCellFormat *pFormat, unsigned index);
	int addDataFormat(FILE *pFile, ExcelCellFormat *pFormat);
	QString startSheet(FILE *pFile, int sheetNumber);
	QString finishSheet(FILE *pFile);
	QString startSheetCsv(FILE *pFile);
};

#endif	// EXCELWRITER_H
