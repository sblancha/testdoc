#ifndef	VACICONVIES_H
#define	VACICONVIES_H

//	Icon for VIES (solenoid)

#include "VacIcon.h"

class EqpVIES;

class VACCTLEWOUTIL_EXPORT VacIconVIES : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVIES(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVIES();

	virtual inline bool isAlwaysOpposite(void) const { return true; }

	virtual inline bool isConnectedToVacuum(void) { return false; }

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVIES *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter);
	virtual void setPainterForImage(QPainter &painter);
};

#endif	// VACICONVIES_H
