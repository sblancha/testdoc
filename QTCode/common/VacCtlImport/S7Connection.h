#ifndef S7CONNECTION_H
#define S7CONNECTION_H

#include "VacCtlImportExport.h"

//	Class holding S7 connection data

#include "FileParser.h"

class ImportPool;

class VACCTLIMPORT_EXPORT S7Connection : public FileParser
{
public:
	~S7Connection() {}

	static void setImportPool(ImportPool *pImportPool);
	static S7Connection *createFromFile(FILE *pFile, int &lineNo, const char *name,
		QStringList &errList);

	void dump(FILE *pFile);

	// Access
	inline const char *getName(void) { return name; }
	inline const char *getIpAddress(void) { return ipAddress; }
	inline int getType(void) { return type; }
	inline int getRack(void) { return rack; }
	inline int getSlot(void) { return slot; };
	inline int getLineNo(void) { return lineNo; }
	inline bool isIpAddressValid(void) { return ipAddressValid; }

protected:
	S7Connection(const char *name, int lineNo);

	// Reference to ImportPool instance
	static ImportPool	*pPool;

	// Name of connection
	char					*name;

	// IP address
	char					*ipAddress;

	// Connection type
	int						type;

	// Rack number
	int						rack;

	// Slot number
	int						slot;

	// Line number in input file where this connection was defined
	int						lineNo;

	// Flag indicating if IP address seems to be valid
	bool					ipAddressValid;

	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);

private:
	bool checkIpAddress(const char *address);
};

#endif	// S7CONNECTION_H
