// Major modes for dialog opening - must match DataEnum::DataMode in C++ code
const int DIALOG_MODE_ONLINE = 0,
  DIALOG_MODE_REPLAY = 1,
  DIALOG_MODE_POLLING = 2;

// Modes of replay
const int REPLAY_MODE_NONE = 0,
  REPLAY_MODE_DRAG = 1,
  REPLAY_MODE_PLAY = 2;

// Background color for panels opened in replay mode
const string REPLAY_MODE_BACKGROUND = "{51,255,255}";

// special internal DP to support replay facility
// If dp isn't exist, it will be created
void LhcVacGetReplayControlDpName(string &replayDpName)
{
  string  dpName = "_VacReplayControl_" + myManNum();
  // Create DP if needed
  if(!dpExists(dpName))
  {
    dpCreate(dpName, "_VacReplayControl");
    dyn_errClass err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacGetReplayControlDpName(): failed to create DP <" + dpName + ":" + err , "");
      fwExceptionHandling_display(exceptionInfo);
      replayDpName = "";
      return;	 
    }
  }
  replayDpName = dpName;
}
