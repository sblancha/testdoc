//	Implementation of VacIconVVS_LHC class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVVS_LHC.h"
#include "FlashingRect.h"

#include "EqpVV.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OPEN				0,255,0
#define COLOR_CLOSED			255,0,0
#define COLOR_ERROR				255,102,255
#define COLOR_TEMPERATURE 63,127,127

// Colors for control and interlocks
#define COLOR_GREEN				0,255,0
#define COLOR_YELLOW			255,255,0
#define COLOR_RED				255,0,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_TST_PIN		(0x10000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define RR1_PR_SRC_OK	(0x00100000)
#define	RR1_OPEN		(0x00020000)
#define	RR1_CLOSED		(0x00010000)

#define RR2_CIR_VVS_AFTER	(0x0800)
#define	RR2_CIR_VVS_BEFORE	(0x0400)
#define RR2_CIR_TEMP		(0x0200)
#define RR2_CIR_PRESS		(0x0100)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVVS_LHC::VacIconVVS_LHC(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(27, 21);
	pFlash = NULL;
	setInterlockGeometry();
}

VacIconVVS_LHC::~VacIconVVS_LHC()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for all interlock rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVS_LHC::setInterlockGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		if(beamDirection == VacIconContainer::LeftToRight)
		{
			vvBefore.setRect(1, 1, 3, 19);
			vvAfter.setRect(23, 1, 3, 19);
		}
		else
		{
			vvBefore.setRect(23, 1, 3, 19);
			vvAfter.setRect(1, 1, 3, 19);
		}
		break;
	case VacIconContainer::Down:
		if(beamDirection == VacIconContainer::LeftToRight)
		{
			vvAfter.setRect(1, 1, 3, 19);
			vvBefore.setRect(23, 1, 3, 19);
		}
		else
		{
			vvBefore.setRect(1, 1, 3, 19);
			vvAfter.setRect(23, 1, 3, 19);
		}
		break;
	case VacIconContainer::Left:
		if(beamDirection == VacIconContainer::LeftToRight)
		{
			vvBefore.setRect(1, 23, 19, 3);
			vvAfter.setRect(1, 1, 19, 3);
		}
		else
		{
			vvAfter.setRect(1, 23, 19, 3);
			vvBefore.setRect(1, 1, 19, 3);
		}
		break;
	case VacIconContainer::Right:
		if(beamDirection == VacIconContainer::LeftToRight)
		{
			vvAfter.setRect(1, 23, 19, 3);
			vvBefore.setRect(1, 1, 19, 3);
		}
		else
		{
			vvBefore.setRect(1, 23, 19, 3);
			vvAfter.setRect(1, 1, 19, 3);
		}
		break;
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVVS_LHC::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		return VacIconGeometry(27, 21, 13, 10, 0);
		break;
	default:
		break;
	}
	return VacIconGeometry(21, 27, 10, 13, 0);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVS_LHC::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(27);
		size.setHeight(21);
		break;
	default:
		size.setWidth(21);
		size.setHeight(27);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setInterlockGeometry();
}

/*
**	FUNCTION
**		Override method of VacIcon: interlocks geometry of this icon
**		depends on beam direction
**
**	ARGUMENTS
**		newDirection	- new beam direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVS_LHC::setBeamDirection(VacIconContainer::BeamDirection newDirection)
{
	VacIcon::setBeamDirection(newDirection);
	setInterlockGeometry();
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVS_LHC::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVV"), "VacIconVVS_LHC::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVV *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVVS_LHC::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVVS_LHC::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("RR2");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVVS_LHC::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVVS_LHC::getToolTip(const QPoint &pos, QString &text, QRect &rect)
{
	if(pEqp && connected)
	{
		// Where is the point? If in one of interlocks - show interlock text,
		// otherwise show valve name and state
		if(vvBefore.contains(pos))
		{
			text = pEqp->getVisibleName();
			text += ": VVS before";
			rect = vvBefore;
		}
		else if(vvAfter.contains(pos))
		{
			text = pEqp->getVisibleName();
			text += ": VVS after";
			rect = vvAfter;
		}
		else
		{
			pEqp->getToolTipString(text, mode);
			rect.setRect((width() / 2) - 9, (height() / 2) - 9, 19, 19);
		}
	}
	else
	{
		rect = this->rect();
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVVS_LHC::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0, rr2 = 0;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::NotControl;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		rr2 = pEqp->getRR2(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	/*
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
	*/

	// Save painter coordinate system - interlocks will be drawn
	// in original (non-translated) coordinate system
	painter.save();

	// Prepare painter's coordinate system for drawing
	// Coordinate system (0,0) is center of widget
	painter.translate(width() / 2, height() / 2);
	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	// Coordinate system is ready - draw
	drawImage(painter, ctlStatus, rr1, rr2, plcAlarm);

	// Restore coordinate system to original and draw interlocks
	painter.restore();
	drawInterlocks(painter, ctlStatus, rr1, rr2, plcAlarm);

	// Draw selection ON TOP of image
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
}

void VacIconVVS_LHC::drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm)
{
	// Draw vacuum pipe - two short lines
	QPen pen(pipeColor, pipeWidth);
	painter.setPen(pen);
	painter.drawLine(-14, 0, -9, 0);
	painter.drawLine(9, 0, 15, 0);	// FlatCapStyly does not cover the end point of line,
									// hence end point is 14+1=15
	
	// Draw valve image

	// 1) Rectangle connecting big rectangle on top with valve image
	setPainterForMainImage(painter, ctlStatus, rr1, rr2, plcAlarm);
	QPolygon points(4);
	points.setPoint(0, -2, -6);
	points.setPoint(1, 2, -6);
	points.setPoint(2, 2, 0);
	points.setPoint(3, -2, 0);
	painter.drawPolygon(points);

	// 2) big rectangle on top
	QBrush brushSave(painter.brush());
	QPen penSave(painter.pen());
	setPainterForControl(painter, ctlStatus, rr1, rr2, plcAlarm);
	points.setPoint(0, -4, -9);
	points.setPoint(1, 4, -9);
	points.setPoint(2, 4, -6);
	points.setPoint(3, -4, -6);
	painter.drawPolygon(points);

	// 3) Valve image
	painter.setBrush(brushSave);
	painter.setPen(penSave);
	points.setPoint(0, -9, -9);
	points.setPoint(1, 9, 9);
	points.setPoint(2, 9, -9);
	points.setPoint(3, -9, 9);
	painter.drawPolygon(points);

	if((ctlStatus != Eqp::NotControl) && (ctlStatus != Eqp::NotConnected))
	{
		// On top of image: symbol showing that valve can not be opened
		bool openEnable = true;
		if(!((rr1 & RR1_TST_PIN) || (rr1 & RR1_PR_SRC_OK)))	// Open NOT enabled
		{
			openEnable = false;
		}
		else if((rr2 & RR2_CIR_VVS_AFTER) || (rr2 & RR2_CIR_VVS_BEFORE)
			|| (rr2 & RR2_CIR_TEMP) || (rr2 & RR2_CIR_PRESS))
		{
			openEnable = false;
		}
		if(!openEnable)
		{
			QRect disableRect(-1, -6, 3, 13);
			painter.fillRect(disableRect, Qt::black); 
		}

		// On top of image: temperature interlock
		if(rr2 & RR2_CIR_TEMP)
		{
			QRect tempRect(-9, -2, 19, 5);
			QColor tempColor(COLOR_TEMPERATURE);
			painter.setBrush(tempColor);
			painter.setPen(Qt::black);
			painter.drawRect(tempRect);
		}
	}
}

void VacIconVVS_LHC::drawInterlocks(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm)
{
	// Interlock before valve
	QColor color = colorForInterlock(ctlStatus, rr1, rr2, plcAlarm, RR2_CIR_VVS_BEFORE);
	painter.fillRect(vvBefore, color);

	// Interlock after valve
	color = colorForInterlock(ctlStatus, rr1, rr2, plcAlarm, RR2_CIR_VVS_AFTER);
	painter.fillRect(vvAfter, color);

	switch(ctlStatus)
	{
	case Eqp::NotControl:
	case Eqp::NotConnected:
		if(pFlash)
		{
			delete pFlash;
			pFlash = NULL;
		}
		break;
	default:
		{
			// Current pressure interlock - separate flashing control
			if(rr2 & RR2_CIR_PRESS)
			{
				if(!pFlash)
				{
					pFlash = new FlashingRect(this);
					switch(direction)
					{
					case VacIconContainer::Up:	// No rotation
					case VacIconContainer::Down:	// 180 degrees
						pFlash->setGeometry(5, 8, 17, 5);
						break;
					case VacIconContainer::Left:	// 90 degrees counterclockwise
					case VacIconContainer::Right:	// 90 degrees clockwise
						pFlash->setGeometry(8, 5, 5, 17);
						break;
					}
					pFlash->show();
				}
			}
			else if(pFlash)
			{
				delete pFlash;
				pFlash = NULL;
			}
		}
		break;
	}
}

void VacIconVVS_LHC::setPainterForControl(QPainter &painter, Eqp::CtlStatus /* ctlStatus */, unsigned rr1, unsigned /* rr2 */, bool plcAlarm)
{
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		int order;
		QColor borderColor;
		pEqp->getInterlockColor(mode, color, borderColor, order, NULL);
	}
	else if(!plcAlarm)
	{
		if(rr1 & RR1_VALID)
		{
			if(rr1 & RR1_ERROR)
			{
				color.setRgb(COLOR_RED);
			}
			else if(rr1 & RR1_WARNING)
			{
				color.setRgb(COLOR_YELLOW);
			}
			else
			{
				color.setRgb(COLOR_GREEN);
			}
		}
	}
	painter.setBrush(color);
	painter.setPen(Qt::black);
}

void VacIconVVS_LHC::setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned /* rr2 */, bool plcAlarm)
{
	QColor color(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr1 & RR1_OPEN)
				{
					if(rr1 & RR1_CLOSED)
					{
						color.setRgb(COLOR_ERROR);
					}
					else
					{
						color.setRgb(COLOR_OPEN);
					}
				}
				else if(rr1 & RR1_CLOSED)
				{
					color.setRgb(COLOR_CLOSED);
				}
				else
				{
					color.setRgb(COLOR_UNDEFINED);
				}
			}
		}
		break;
	}
	QBrush brush(color);
	painter.setBrush(brush);
	painter.setPen(Qt::black);
}

QColor VacIconVVS_LHC::colorForInterlock(Eqp::CtlStatus ctlStatus, unsigned rr1, unsigned rr2, bool plcAlarm, unsigned bit)
{
	QColor color(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		return color;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		return color;
	default:
	if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr2 & bit)
				{
					color.setRgb(COLOR_RED);
				}
				else
				{
					color.setRgb(COLOR_GREEN);
				}
			}
		}
		break;
	}
	return color;
}
