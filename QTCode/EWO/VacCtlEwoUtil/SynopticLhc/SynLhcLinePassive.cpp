//	Implementation of SynLhcLinePassive class
/////////////////////////////////////////////////////////////////////////////////

#include "SynLhcLinePassive.h"

#include "VacLinePart.h"

#include "Eqp.h"
#include "EqpType.h"
#include "EqpMsgCriteria.h"

#include "DataPool.h"
#include "DebugCtl.h"

#include <qwidget.h>
#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction


SynLhcLinePassive::SynLhcLinePassive(QWidget *parent, int type, float start, float end) :
	SynLhcLine(parent, type, start, end)
{
	curEqp = true;		// Parts are never processed in this line
}

SynLhcLinePassive::~SynLhcLinePassive()
{
}

/*
**	FUNCTION
**		Analyze which equipment types will be shown in view
**
**	PARAMETERS
**		allVacTypeMask	- OR'ed bit mask for all vacuum types
**		allCryoEqpMask	- Total equipment type mask for all equipment on isolation vacuum
**							to be shown in this view
**		allBeamEqpMask	- Total equipment type mask for all equipment on beam vacuum
**							to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLinePassive::analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask & /* allCryoEqpMask */, VacEqpTypeMask &allBeamEqpMask)
{
	SynLhcLine::analyzeTypes(allVacTypeMask, allBeamEqpMask);
}

/*
**	FUNCTION
**		Check if tooltip shall be shown for given mouse pointer position
**
**	PARAMETERS
**		point	- Mouse pointer position
**		text	- Variable where tool tip text shall be written
**		rect	- Variable where boundaries for returned tooltip text shall
**					be written
**
**	RETURNS
**		true	- If tooltip text shall be shown for given mouse position;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SynLhcLinePassive::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		if(pItem->getRect().contains(point))
		{
			text = pItem->getName();
			rect = pItem->getRect();
			return true;
		}
	}
	return false;
}


/*
**	FUNCTION
**		Check if given device shall appear in this synoptic line. Coordinate
**		of device is NOT checked.
**
**	PARAMETERS
**		pEqp	- Pointer to device in question
**		partIdx	- Index of line part where equipment is located will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
bool SynLhcLinePassive::isMyEqp(Eqp *pEqp, int &partIdx)
{
	if(hideMagnets)
	{
		return false;
	}
	if(pEqp->isSkipOnSynoptic())
	{
		return false;
	}
	int functionalType = pEqp->getFunctionalType();
	if(pEqp->getType() == EqpType::Passive)
	{
		if(functionalType == FunctionalType::None)
		{
			return false;
		}
	}
	else
	{
		if(!(pEqp->isCryoThermometer() || (functionalType == FunctionalType::VRJ_TC)))
		{
			return false;
		}
		if(!eqpMask.contains(functionalType))
		{
			return false;
		}
		if((pEqp->getVacType() & VacType::Qrl))
		{
			return false;
		}
		// Part index must be found - LIK 05.06.2008 return true;
	}

	// Find in which part equipment is located
	partIdx = findPartIdx(pEqp);

	// LIK 05.06.2008 take all thermometers in the arc
	if(pEqp->isCryoThermometer() || (functionalType == FunctionalType::VRJ_TC))
	{
		return true;
	}
	return ! DataPool::getInstance().isEqpInTheArc(pEqp);
}

/*
** FUNCTION
**		Find next position of element to be processed when
**		calculating horizontal geometry.
**		Implementation is more complex compared to other line types
**		because it can be that one (or more) thermometers are within
**		coordinates of magnet.
**
** PARAMETERS
**		None
**
** RETURNS
**		Position of next element to process, or
**		huge value if no more elements shall be processed
**
** CAUTIONS
**		None
*/
float SynLhcLinePassive::nextPosition(void)
{
	float	result = HUGE_COORDINATE;

	curEqpIdx = eqps.count();
	for(int n = 0 ; n < eqps.count() ; n++)
	{
		SynLhcEqpItem *pItem = eqps.at(n);
		if(pItem->isCryoThermometer() || (pItem->getEqp()->getFunctionalType() == FunctionalType::VRJ_TC))
		{
			if(!pItem->isStartReady())
			{
				if(pItem->getStart() < result)
				{
					result = pItem->getStart();
					curEqpIdx = n;
				}
			}
		}
		else
		{
			if(!pItem->isStartReady())
			{
				if(pItem->getStart() < result)
				{
					result = pItem->getStart();
					curEqpIdx = n;
				}
			}
			else if(!pItem->isEndReady())
			{
				if((pItem->getStart() + pItem->getEqp()->getLength()) < result)
				{
					result = pItem->getStart() + pItem->getEqp()->getLength();
					curEqpIdx = n;
				}
			}
		}
	}
	return result;
}

/*
**	FUNCTION
**		Calculate screen position for next element to be processed
**
**	PARAMETERS
**		order		- Order of this element in processing sequence
**		coord		- Last processed world coordinate
**		connPos		- Last used connection point in screen coordinates
**		endCoord	- Last used end coordinate of element
**		endConnPos	- Last used and connection point
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLinePassive::placeItem(int order, float &coord, int &connPos, float &endCoord, int &endConnPos)
{
	SynLhcEqpItem *pItem = eqps.at(curEqpIdx);
	if(!pItem)
	{
		return;	// Can not happen, just protection
	}

	if(pItem->isCryoThermometer() || (pItem->getEqp()->getFunctionalType() == FunctionalType::VRJ_TC))
	{
		SynLhcLine::placeItem(order, coord, connPos, endCoord, endConnPos);
		return;
	}

	if(!pItem->isStartReady())
	{
		pItem->setStartOrder(order);
	}
	else
	{
		pItem->setEndOrder(order);
	}

	float useCoord = pItem->getStart();
	if(pItem->isStartReady())
	{
		useCoord += pItem->getEqp()->getLength();
	}

	// Connect point location must grow with coordinate
	if(useCoord > coord)
	{
		coord = useCoord;
		connPos += MIN_PASSIVE_SPACING;
	}

	int minIconLeft = 0;
	if(!pItem->isStartReady())	// Icon shall not overlap with previous icon on the same line
	{
		/* Conditions changed - see below LIK 05.06.2008
		if((endCoord > 0.0) && (pItem->start > endCoord) && (connPos <= endConnPos))
		{
			connPos = endConnPos + MIN_ACTIVE_SPACING;
		}
		*/
		if((endCoord > 0.0) && (connPos <= endConnPos))
		{
			if(pItem->getStart() > endCoord)
			{
				connPos = endConnPos + MIN_ACTIVE_SPACING;
			}
			else if(pItem->getStart() == endCoord)
			{
				connPos = endConnPos;
			}
		}
		if(curEqpIdx > 0)
		{
			SynLhcEqpItem *pPrev = eqps.at(curEqpIdx - 1);
			minIconLeft = pPrev->getRect().right() + MIN_PASSIVE_SPACING;
		}
		else
		{
			minIconLeft = 0;
		}
		pItem->getRect().moveLeft(connPos > minIconLeft ? connPos : minIconLeft);
		pItem->getConnPoint().setX(connPos > minIconLeft ? connPos : minIconLeft);
		pItem->setStartReady(true);
	}

	else	// Calculate sizes of 'icon'
	{
		const char	*label = pItem->getEqp()->makeShortName();
		QRect labelRect = parent->fontMetrics().boundingRect(label);
		QRect &rect = pItem->getRect();
		rect.setWidth(labelRect.width() + 4);
		if(rect.right() < connPos)
		{
			rect.setWidth(connPos - rect.left());
		}
		else if(connPos < rect.right())
		{
			connPos = rect.right();
		}
		rect.setHeight(labelRect.height() + 4);
		rect.moveTop(rect.top() - rect.height());	// LIK 04.06.2008
		if((pItem->getStart() + pItem->getEqp()->getLength()) > endCoord)
		{
			endCoord = pItem->getStart() + pItem->getEqp()->getLength();
			endConnPos = rect.right();
		}
		pItem->setEndReady(true);
		curEqpIdx++;
	}
	if(connPos > endX)
	{
		endX = connPos;
	}
}

/*
**	FUNCTION
**		Calculate vertical limits for line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLinePassive::calcVerticalLimits(void)
{
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		int yPos = pItem->getRect().top();
		if(yPos < 0)
		{
			if(abs(yPos) > ascent)
			{
				ascent = abs(yPos);
			}
		}
		else
		{
			if(abs(yPos) > descent)
			{
				descent = abs(yPos);
			}
		}
		yPos = pItem->getRect().bottom();
		if(yPos < 0)
		{
			if(abs(yPos) > ascent)
			{
				ascent = abs(yPos);
			}
		}
		else
		{
			if(abs(yPos) > descent)
			{
				descent = abs(yPos);
			}
		}
	}
}

/*
**	FUNCTION
**		Draw non-controllable equipment
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		viewStart	- Start X-coordinate of visible area
**		viewEnd		- End X-coordinate of visible area
**
**	RETURNS
**		always 1
**
**	CAUTIONS
**		None
*/
int SynLhcLinePassive::draw(QPainter &painter, int viewStart, int viewEnd)
{
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		QRect &rect = pItem->getRect();
		if(rect.left() > viewEnd)
		{
			continue;
		}
		if(rect.right() < viewStart)
		{
			continue;
		}
		if(pItem->isCryoThermometer() || (pItem->getEqp()->getFunctionalType() == FunctionalType::VRJ_TC))
		{
			SynLhcLine::drawActiveElement(pItem, painter);
			continue;
		}
		// Prepare drawing parameters
		int functionalType = pItem->getEqp()->getFunctionalType();
		QColor color;
		switch(functionalType)
		{
		case FunctionalType::ColdDipole:
			color.setRgb(0, 0, 255);
			break;
		case FunctionalType::ColdQuadrupole:
			color.setRgb(0, 0, 255);
			break;
		case FunctionalType::WarmDipole:
			color.setRgb(255, 0, 0);
			break;
		case FunctionalType::WarmQuadrupole:
			color.setRgb(255, 0, 0);
			break;
		case FunctionalType::TDICollimator:
			color.setRgb(150, 75, 0);
			break;
		default:
			break;
		}
		painter.setPen(color);

		// Draw rectangle
		painter.drawRect(rect);

		// Draw text
		const char	*label = pItem->getEqp()->makeShortName();
		QRect labelRect = parent->fontMetrics().boundingRect(label);
		painter.drawText(rect.center().x() - (labelRect.width() >> 1),
			rect.center().y() + (labelRect.height() >> 1), label);
	}
	return 1;
}

/*
**	FUNCTION
**		Write all parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLinePassive::dump(FILE *pFile)
{
	if(!DebugCtl::isSynoptic())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}

	fprintf(pFile, "\n\n++++++++++++++ Line %d (%s) nParts %d nEqp %d\n", type,
		getLineName(), pPartList->count(), eqps.count());
	fprintf(pFile, " lineY %d ascent %d descent %d EQP IS %s LINE\n", y, ascent, descent,
		(eqpAboveLine ? "ABOVE" : "BELOW"));
	fprintf(pFile, " start %f end %f\n", start, end);
	fprintf(pFile, " PARTS:\n");
	int n;
	for(n = 0 ; n < pPartList->count() ; n++)
	{
		VacLinePart *pPart = pPartList->at(n);
		fprintf(pFile, "  %02d: [%d..%d] start %f end %f (real %f %f) type %d %s %s\n", n,
			pPart->getStartOrder(), pPart->getEndOrder(), pPart->getStartPos(),
			pPart->getEndPos(), pPart->getRealStart(), pPart->getRealEnd(),
			pPart->getVacType(), (pPart->getSector() ? pPart->getSector()->getName() : ""),
			(pPart->isAfterIP1() ? "(AFTER IP1)" : "()"));
		fprintf(pFile, "   startX %d endX %d isStarted %d isFinished %d\n",
			pPart->getStartX(), pPart->getEndX(), pPart->isStarted(), pPart->isFinished());
		fprintf(pFile, "   drawStart %d drawEnd %d\n", pPart->isDrawStart(), pPart->isDrawEnd());
		fprintf(pFile, "   correct: start %d end %d\n", pPart->getCorrDeltaStart(),
			pPart->getCorrDeltaEnd());
	}
	fprintf(pFile, " EQUIPMENT:\n");
	for(n = 0 ; n < eqps.count() ; n++)
	{
		SynLhcEqpItem *pItem = eqps.at(n);
		QRect &rect = pItem->getRect();
		QPoint &connPoint = pItem->getConnPoint();
		fprintf(pFile, "  %04d: [%d...%d] <%s> (type %d) %f+%f (%f) -> ( %d %d ) ( %d x %d ) conn %d %d\n", n,
			pItem->getStartOrder(), pItem->getEndOrder(), pItem->getEqp()->getName(),
			pItem->getEqp()->getFunctionalType(),
			pItem->getEqp()->getStart(), pItem->getEqp()->getLength(), pItem->getStart(),
			rect.left(), rect.top(), rect.width(), rect.height(),
			connPoint.x(), connPoint.y());
		fprintf(pFile, "      correct: %d\n", pItem->getCorrDelta());
	}
}
