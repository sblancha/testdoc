#ifndef SURVEYVIEWER_H
#define SURVEYVIEWER_H

// GUI of survey viewer application

#include "LineViewer.h"

#include <qmainwindow.h>
#include <qtablewidget.h>
#include <qpushbutton.h>
#include <qlabel.h>


class SurveyViewer : public QMainWindow
{
	Q_OBJECT

public:
	SurveyViewer(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~SurveyViewer();

private slots:
	void newFile(void);
	void lineChecked(QTableWidgetItem *pItem);
	void eqpSelected(Eqp *pEqp);

private:
	LineViewer	*pLineViewer;
	QTableWidget		*pLineTable;
	QPushButton	*pFileButton;
	QLabel		*pLabel;
};

#endif	// SURVEYVIEWER_H
