// Simple test for data pool

#include "DataPool.h"
#include <qstrlist.h>

int main(int argc, char **argv)
{
	DataPool &pool = DataPool::getInstance();
	QStrList errList;
	int coco = pool.initPassive(true,
		"/home/kopylov/PVSS_projects/vac_lik_lhc_1/vac_lik_lhc_1/data",
		"LHC", errList);
	printf("initPassive(): coco = %d\n", coco);
	for(char *msg = errList.first() ; msg ; msg = errList.next())
	{
		printf("%s\n", msg);
	}
	if(coco <= 0)
	{
		return 0;
	}
	coco = pool.initActive("LHC",
		"/home/kopylov/PVSS_projects/vac_lik_lhc_1/vac_lik_lhc_1/data/LHC.for_DLL",
		errList);
	printf("initActive(): coco = %d\n", coco);
	for(char *msg = errList.first() ; msg ; msg = errList.next())
	{
		printf("%s\n", msg);
	}
	return 0;
}
