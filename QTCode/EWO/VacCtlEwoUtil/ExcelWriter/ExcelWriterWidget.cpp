//	Implementation of ExcelWriterWidget class
/////////////////////////////////////////////////////////////////////////////////

#include "ExcelWriterWidget.h"

#include "VacMainView.h"

#include "DataPool.h"
#include "Eqp.h"

#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>

#include <QMouseEvent>
#include <QMenu>

#include <errno.h>

/////////////////////////////////////////////////////////////////////////////////
//	Construction/destruction

ExcelWriterWidget::ExcelWriterWidget(QWidget *parent) :
		QPushButton(parent)
{
	writeCsv = false;
	connect(this, SIGNAL(clicked(void)), this, SLOT(buttonClick(void)));
}

ExcelWriterWidget::ExcelWriterWidget(const QString &text, QWidget *parent) :
		QPushButton(text, parent)
{
	writeCsv = false;
	connect(this, SIGNAL(clicked(void)), this, SLOT(buttonClick(void)));
}

ExcelWriterWidget::ExcelWriterWidget(const QIcon &icon, const QString &text, QWidget *parent) :
		QPushButton(icon, text, parent)
{
	writeCsv = false;
	connect(this, SIGNAL(clicked(void)), this, SLOT(buttonClick(void)));
}

void ExcelWriterWidget::mousePressEvent(QMouseEvent *pEvent)
{
	if(pEvent->button() != Qt::RightButton)
	{
		QPushButton::mousePressEvent(pEvent);
		return;
	}
	QMenu *pMenu = new QMenu();
	QAction *pAction = pMenu->addAction("Format XML", this, SLOT(selectXmlFormat()));
	pAction->setCheckable(true);
	pAction->setChecked(!writeCsv);
	pAction = pMenu->addAction("Format CSV", this, SLOT(selectCsvFormat()));
	pAction->setCheckable(true);
	pAction->setChecked(writeCsv);
	pMenu->exec(pEvent->globalPos());
}

void ExcelWriterWidget::selectXmlFormat(void)
{
	writeCsv = false;
}

void ExcelWriterWidget::selectCsvFormat(void)
{
	writeCsv = true;
}

/*
**	FUNCTION
**		Slot receiving 'clicked' signal of button. Open dialog for
**		selecting output file. If file was selected - emits 'contentRequested'
**		signal to provide content for file.
**
**	ARGUMENTS
**		None
**
**	RETURNS
*		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterWidget::buttonClick(void)
{
	QString newFile = QFileDialog::getSaveFileName(this,
		"Choose a filename to save under",
		VacMainView::getFilePath(),	// QString::null,
		writeCsv ? "CSV files (*.txt)" : "Excel files (*.xls)");
	if(newFile.isEmpty())
	{
		return;
	}
	QFileInfo info(newFile);
	if(writeCsv)
	{
		if(info.suffix() != "txt")
		{
			newFile += ".txt";
			info.setFile(newFile);
		}
	}
	else
	{
		if(info.suffix() != "xls")
		{
			newFile += ".xls";
			info.setFile(newFile);
		}
	}
	VacMainView::setFilePath(info.path());
	if(info.exists())
	{
		int answer = QMessageBox::warning(this,
			"File exists",
			"Selected file exists - overwrite it ?",
			QMessageBox::Yes, QMessageBox::No);
		if(answer != QMessageBox::Yes)
		{
			return;
		}
	}
	fileName = newFile;
	emit contentRequested();
}

/*
**	FUNCTION
**		Write prepared content to output file
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriterWidget::write(void)
{
	if(fileName.isEmpty())
	{
		return "No output file name";
	}
#ifdef Q_OS_WIN
	FILE *pFile = NULL;
	if(fopen_s(&pFile, fileName.toLatin1().constData(), "w"))
	{
		pFile = NULL;
	}
#else
	FILE *pFile = fopen(fileName.toLatin1().constData(), "w");
#endif
	if(!pFile)
	{
		return QString("File open failed: errno %1").arg(errno);
	}
	QString error;
	if(writeCsv)
	{
		error = writer.writeCsv(pFile);
	}
	else
	{
		error = writer.write(pFile);
	}
	fclose(pFile);
	return error;
}

/*
**	FUNCTION
**		Add one history value to history values processor
**
**	ARGUMENTS
**		dp		- DP name
**		ts		- Timestamp of value
**		value	- value to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ExcelWriterWidget::addHistoryValue(const char *dp, const QDateTime &ts, float value)
{
	Eqp *pEqp = DataPool::getInstance().findEqpByDpName(dp);
	if(pEqp)
	{
		history.addValue(pEqp, "", 0, "", ts, value);
	}
}

/*
**	FUNCTION
**		Write prepared history content to output file
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Empty string in case of success;
**		string with error description in case of error
**
**	CAUTIONS
**		None
*/
QString ExcelWriterWidget::writeHistory(void)
{
	if(fileName.isEmpty())
	{
		return "No output file name";
	}
#ifdef Q_OS_WIN
	FILE *pFile = NULL;
	if(fopen_s(&pFile, fileName.toLatin1().constData(), "w"))
	{
		pFile = NULL;
	}
#else
	FILE *pFile = fopen(fileName.toLatin1().constData(), "w");
#endif
	if(!pFile)
	{
		return QString("File open failed: errno %1").arg(errno);
	}
	QString error = history.write(pFile, &writer);
	fclose(pFile);
	history.clear();
	return error;
}


