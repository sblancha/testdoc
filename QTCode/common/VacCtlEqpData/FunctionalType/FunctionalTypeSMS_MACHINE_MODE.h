#ifndef FUNCTIONALTYPESMSMACHINEMODE_H
#define FUNCTIONALTYPESMSMACHINEMODE_H

// Global device - machine mode for SMS

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeSMS_MACHINE_MODE : public FunctionalType
{
public:

	// Main value criteria subtypes
	enum
	{
		Unknown = 0,
		Mode = 1
	};

	FunctionalTypeSMS_MACHINE_MODE(int type, const char *name, const char *description);
	virtual ~FunctionalTypeSMS_MACHINE_MODE() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);
	virtual void getCriteriaValues(int critType, int critSubType, QList<int> &values, QList<QByteArray> &labels);
};

#endif	// FUNCTIONALTYPESMSMACHINEMODE_H
