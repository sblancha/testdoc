//	Implementation of LhcMainViewer class
/////////////////////////////////////////////////////////////////////////////

#include "LhcMainViewer.h"

#include <qtoolbar.h>
#include <qtoolbutton.h>


LhcMainViewer::LhcMainViewer(QWidget *parent, const char *name, WFlags flags)
	: QMainWindow(parent, name, flags)
{
	pView = new MainLHC(this, "LHC main view control");
	QFont font("Helvetica", 8, QFont::Normal);
	pView->setFont(font);
	setCentralWidget(pView);

	QToolBar *testTools = new QToolBar(this, "test tool bar");
	QToolButton	*readDataPb = new QToolButton(testTools, "read button");
	readDataPb->setTextLabel("Read data");
	connect(readDataPb, SIGNAL(clicked()), this, SLOT(readData()));
}

LhcMainViewer::~LhcMainViewer()
{
	delete pView;
}


void LhcMainViewer::readData(void)
{
	printf("readData()\n");
	QStrList	errList;
	int coco = pView->initData(
		"/home/kopylov/PVSS_projects/test/test36/data", "LHC",
		"/home/kopylov/PVSS_projects/test/test36/data/LHC.for_DLL",
		 errList);
	printf("initData(): %d\n", coco);
	QStrListIterator iter(errList);
	for(char *msg = iter.toFirst() ; msg ; msg = ++iter)
	{
		printf("%s\n", msg);
	}
}
