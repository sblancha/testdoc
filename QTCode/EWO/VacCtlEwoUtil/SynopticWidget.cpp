//	Implementation of SynopticWidget class
/////////////////////////////////////////////////////////////////////////////////

#include "SynopticWidget.h"

#include "VacMainView.h"

#include <QToolTip>
#include <QEvent>
#include <QHelpEvent>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

SynopticWidget::SynopticWidget(QWidget *parent, Qt::WindowFlags f) :
	QWidget(parent, f)
{
	setMouseTracking(true);
	if(VacMainView::getInstance())
	{
		setFont(VacMainView::getInstance()->font());
	}
}

SynopticWidget::~SynopticWidget()
{
}

bool SynopticWidget::event(QEvent *pEvent)
{
	if(pEvent->type() == QEvent::ToolTip)
	{
		QHelpEvent *pHelpEvent = static_cast<QHelpEvent *>(pEvent);
		QString text;
		QRect rect;
		if(getToolTip(pHelpEvent->pos(), text, rect))
		{
			QToolTip::showText(pHelpEvent->globalPos(), text, this, rect);
		}
		else
		{
			QToolTip::hideText();
			pEvent->ignore();
		}
		return true;
	}
	return QWidget::event(pEvent);
}

