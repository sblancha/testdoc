//	Implementation of HistoryEqpData class
/////////////////////////////////////////////////////////////////////////////////

#include "HistoryEqpData.h"

#include "ScreenPoint.h"
#include "TimeAxis.h"
#include "TimeAxisWidget.h"
#include "VerticalAxisWidget.h"

#include "HistoryProcessor.h"

#include "VacMainView.h"
#include "DpeHistoryQuery.h"
#include "DebugCtl.h"

#include <QPainter>

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

HistoryEqpData::HistoryEqpData()
{
	pOnlineList = new QList<HistoryDataRange *>();
	pOfflineList = new QList<HistoryDataRange *>();
	initMembers();
}

HistoryEqpData::HistoryEqpData(Eqp *pEqp, const QDateTime &absStartTime)
{
	pOnlineList = new QList<HistoryDataRange *>();
	pOfflineList = new QList<HistoryDataRange *>();
	initMembers();
	this->pEqp = pEqp;
	this->absStartTime = absStartTime;
	valueType = pEqp->getMainValueType();

	// Calculate DPE name and connect to device
	QStringList dpes;
	pEqp->getValueDpes(dpes);
	Q_ASSERT(dpes.count() > 0);

	dpeName = dpes.first().toLatin1();
	char *pureDpName = NULL, *pureDpeName = NULL;
	bool splitSuccess = splitDpName(dpeName, &pureDpName, &pureDpeName);
	Q_ASSERT(splitSuccess);
	pEqp->connectDpe(this, pureDpeName, DataEnum::Online);

	// Get and apply immediately last known value ???
	QDateTime firstTs;
	const QVariant &value = pEqp->getDpeValue(pureDpeName, DataEnum::Online, firstTs);
	addOnlineValue(value, firstTs);
}

HistoryEqpData::HistoryEqpData(Eqp *pEqp, const char *dpeSuffixName, DataEnum::ValueType dpeValType, const QString &yAxisTypeName, const QDateTime &absStartTime) :
	axisTypeName(yAxisTypeName)
{
	pOnlineList = new QList<HistoryDataRange *>();
	pOfflineList = new QList<HistoryDataRange *>();
	initMembers();
	this->pEqp = pEqp;
	this->absStartTime = absStartTime;
	pureDpeName = dpeSuffixName;
	dpeName = pEqp->getDpName();
	dpeName += ".";
	dpeName += dpeSuffixName;
	valueType = dpeValType;

	// Connect to device
	pEqp->connectDpe(this, dpeSuffixName, DataEnum::Online);

	// Get and apply immediately last known value ???
	QDateTime firstTs;
	const QVariant &value = pEqp->getDpeValue(dpeSuffixName, DataEnum::Online, firstTs);
	addOnlineValue(value, firstTs);
}

HistoryEqpData::HistoryEqpData(Eqp *pEqp, const char *dpe, int bitNbr, const char *bitVisibleName, const QDateTime &absStartTime)
{
	pOnlineList = new QList<HistoryDataRange *>();
	pOfflineList = new QList<HistoryDataRange *>();
	initMembers();
	this->pEqp = pEqp;
	pureDpeName = dpe;
	this->bitNbr = bitNbr;
	this->bitName = bitVisibleName;
	this->absStartTime = absStartTime;
	bitState = true;
	this->bitNbr = bitNbr;
	dpeName = pEqp->getDpName();
	dpeName += ".";
	dpeName += dpe;
	valueType = DataEnum::BitState;

	// Connect to device
	pEqp->connectDpe(this, dpe, DataEnum::Online);

	// Get and apply immediately last known value ???
	QDateTime firstTs;
	const QVariant &value = pEqp->getDpeValue(dpe, DataEnum::Online, firstTs);
	addOnlineValue(value, firstTs);
}

void HistoryEqpData::initMembers(void)
{
	bitState = false;
	bitNbr = 0;
	created = QDateTime::currentDateTime();
	pCurrentRange = NULL;
	pEqp = NULL;
	valueType = DataEnum::ValueTypeNone;
	filterType = HistoryDataRange::FilterTypeMinMax;
	valueCoeff = 1;
}

HistoryEqpData::~HistoryEqpData()
{
	// Calculate DPE name and disconnect from device
	if(pEqp)
	{
		char *pureDpName = NULL, *pureDpeName = NULL;
		bool splitSuccess = splitDpName(dpeName, &pureDpName, &pureDpeName);
		Q_ASSERT(splitSuccess);
		pEqp->disconnectDpe(this, pureDpeName, DataEnum::Online);
	}

	// Remove any pending queries for history data
	DpeHistoryQuery &pQueryPool = DpeHistoryQuery::getInstance();
	QDateTime startTime(QDateTime::currentDateTime());
	QDateTime endTime(QDateTime::currentDateTime());
	for (int idx = pOfflineList->count() - 1; idx >= 0; idx--)
	{
		HistoryDataRange *pRange = pOfflineList->at(idx);
		startTime.setMSecsSinceEpoch(pRange->getStart());
		endTime.setMSecsSinceEpoch(pRange->getEnd());
		pQueryPool.remove(pRange->getId(), (const char *)dpeName,
			bitState, bitNbr, startTime, endTime);
	}
	while(!pOfflineList->isEmpty())
	{
		delete pOfflineList->takeFirst();
	}
	delete pOfflineList;
	while(!pOnlineList->isEmpty())
	{
		delete pOnlineList->takeFirst();
	}
	delete pOnlineList;
}

/*
**	FUNCTION
**		Method is called when new display time limits are set.
**		It shall check if there are enough offline data ranges
**		covering required time interval, if necessary - create
**		new data ranges and add requests for data in these ranges.
**
**		It must be safe enough to query data till end of today - range
**		for today is created first immediately when new device is added,
**		so PVSS archive at the momentof query just shall no contain values
**		in the future.
**
**	PARAMETERS
**		start	- Start of display time
**		end		- End of display time
**
**	RETURNS
**		None
**
**	CAUTIONS
**		It is responsability of caller to make sure both start and
**		end time do not overlap with online data received by history
**		view - in order not to get the same data twice.
*/
void HistoryEqpData::setTimeLimits(const QDateTime &start, const QDateTime &end)
{
	// Every range shall cover one day
	QDateTime dateTime(start);
	QTime time(0, 0, 0);
	dateTime.setTime(time);
	if(dateTime < absStartTime)
	{
		dateTime = absStartTime;
	}

	while(dateTime < end)
	{
		HistoryDataRange *pNewRange = NULL;
		QDateTime endDateTime(dateTime);
		QTime endTime(23, 59, 59, 999);
		endDateTime.setTime(endTime);
		/*
		if(endDateTime > end)
		{
			endDateTime = end;
		}
		*/

		if(!pOfflineList->count())	// Just add new range
		{
/*
printf("HistoryEqpData::setTimeLimits() new first range\n");
fflush(stdout);
*/
			pNewRange = new HistoryDataRange(dateTime, endDateTime);
			pNewRange->setFilterType(filterType);
			pOfflineList->append(pNewRange);
		}
		else	// Check if range exists, if not - create new range and add to list
		{
			bool rangeExists = false;
			QDateTime rangeDateTime(QDateTime::currentDateTime());
			for(int idx = 0 ; idx < pOfflineList->count() ; idx++)
			{
				HistoryDataRange *pRange = pOfflineList->at(idx);
				rangeDateTime.setMSecsSinceEpoch(pRange->getStart());
				QDate rangeDate = rangeDateTime.date();
				if(rangeDate == dateTime.date())
				{
					rangeExists = true;
					// If history request has not been sent for this range yet -
					// we have to send it now
					if(!pRange->isRequestSent())
					{
						pNewRange = pRange;
					}
					break;
				}
				else if(rangeDate > dateTime.date())
				{
/*
printf("HistoryEqpData::setTimeLimits() new range for time %s\n",
	dateTime.toString("dd-MM-yyyy hh:mm:ss.zzz").ascii());
fflush(stdout);
*/
					pNewRange = new HistoryDataRange(dateTime, endDateTime);
					pNewRange->setFilterType(filterType);
					pOfflineList->insert(idx, pNewRange);
					rangeExists = true;
					break;
				}
			}
			Q_ASSERT(rangeExists);
		}
		if(pNewRange)
		{
			pNewRange->setRequestSent(true);
			QDateTime startTime(QDateTime::currentDateTime());
			QDateTime endTime(startTime);
			startTime.setMSecsSinceEpoch(pNewRange->getStart());
			endTime.setMSecsSinceEpoch(pNewRange->getEnd());
			DpeHistoryQuery::getInstance().add(pNewRange->getId(), (const char *)dpeName,
				bitState, bitNbr, startTime, endTime);
		}
		dateTime = dateTime.addDays(1);
		// Set time to 00:00:00 - it could be different ifwe started from the very first archived time
		dateTime.setTime(time);
	}
}

/*
**	FUNCTION
**		Add history value for data range with given ID
**
**	PARAMETERS
**		id		- Range id
**		time	- Timestamp for value
**		value	- Value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryEqpData::addHistoryValue(int id, const QDateTime &time, const QVariant &value)
{
	float result;
	if(bitState)
	{
		if(!value.canConvert(QVariant::UInt))
		{
			return;
		}
		unsigned u = value.toUInt();
		result = u & (1<<bitNbr) ? 1 : 0;
	}
	else	// Pure analog value
	{
		if(!pEqp->convertValue(value, result))
		{
			return;
		}
		result *= valueCoeff;
	}
	/* Change algorithm to: use not ID, but time range. L.Kopylov 23.05.2011
	if(pCurrentRange)
	{
		if(pCurrentRange->getId() == id)
		{
			pCurrentRange->addValue(time, result);
			return;
		}
	}
	QPtrListIterator<HistoryDataRange> iter(*pOfflineList);
	for(pCurrentRange = iter.toFirst() ; pCurrentRange ; pCurrentRange = ++iter)
	{
		if(pCurrentRange->getId() == id)
		{
			pCurrentRange->addValue(time, result);
			return;
		}
	}
	*/
	qint64 timeAsMsec = time.toMSecsSinceEpoch();
	if(pCurrentRange)
	{
		if((pCurrentRange->getStart() <= timeAsMsec) && (timeAsMsec <= pCurrentRange->getEnd()))
		{
			pCurrentRange->addValue(id, time, result);
			return;
		}
	}
	// Find range. it is possible that NEW range shall be allocated
	for(int idx = 0 ; idx < pOfflineList->count() ; idx++)
	{
		pCurrentRange = pOfflineList->at(idx);

		// Consider range start as start of day always
		QDateTime rangeStart(QDateTime::currentDateTime());
		rangeStart.setMSecsSinceEpoch(pCurrentRange->getStart());
		QTime startTime(0, 0, 0);
		rangeStart.setTime(startTime);
		// If range start is before absolute start time - move it a bit into the past
		// in order to be sure even times few msec before still will be in this range
		if(rangeStart <= absStartTime)
		{
			rangeStart = rangeStart.addYears(-1);
		}
		qint64 rangeStartAsMsec = rangeStart.toMSecsSinceEpoch();
		if((rangeStartAsMsec <= timeAsMsec) && (timeAsMsec <= pCurrentRange->getEnd()))
		{
			pCurrentRange->addValue(id, time, result);
			return;
		}
		else if(pCurrentRange->getStart() > timeAsMsec)	// New range is required
		{
			QDateTime startDateTime(time);
			startDateTime.setTime(startTime);
			if(startDateTime < absStartTime)
			{
				startDateTime = absStartTime;
			}
			QDateTime endDateTime(time);
			QTime endTime(23, 59, 59, 999);
			endDateTime.setTime(endTime);
/*
printf("HistoryEqpData::addHistoryValue() new range for time %s\n",
	time.toString("dd-MM-yyyy hh:mm:ss.zzz").ascii());
fflush(stdout);
*/
			HistoryDataRange *pNewRange = new HistoryDataRange(startDateTime, endDateTime);
			pNewRange->setFilterType(filterType);
			pOfflineList->insert(idx, pNewRange);
			pCurrentRange = pNewRange;
			pCurrentRange->addValue(id, time, result);
			return;
		}
	}
}

/*
**	FUNCTION
**		Notification that all history values for one range have
**		been arrived. Check if timestamps in this range are duplicated
**		in neighbour ranges, remove duplicates if necessary
**
**	PARAMETERS
**		id		- Range id
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryEqpData::finishHistoryRange(int /* id */)
{
	// Nothing to do because range identified by times, not by ID. L.Kopylov 23.05.2011
	return;
	/*
	QPtrListIterator<HistoryDataRange> iter(*pOfflineList);
	for(pCurrentRange = iter.toFirst() ; pCurrentRange ; pCurrentRange = ++iter)
	{
		if(pCurrentRange->getId() == id)
		{
			break;
		}
	}
	if(!pCurrentRange)
	{
		return;
	}

	// Check if timestamps of this range are duplicated in next range,
	// remove duplicates from THIS range if found. Iterator is now positioned
	// at CURRENT range
	HistoryDataRange *pRange = ++iter;
	if(pRange)
	{
		pCurrentRange->checkForDuplicates(pRange);
	}

	// Check if timestamps of this range are duplicated in previous range,
	// remove duplicates from PREVIOUS range if found. Iterator is now positioned
	// at NEXT range
	pRange = --iter;
	if(pRange)
	{
		pRange = --iter;
		if(pRange)
		{
			pRange->checkForDuplicates(pCurrentRange);
		}
	}
	*/
}

void HistoryEqpData::setFilterType(HistoryDataRange::FilterType newType)
{
	if(filterType == newType)
	{
		return;
	}
	filterType = newType;
	HistoryDataRange *pRange;
	int idx;
	for(idx = pOfflineList->count() - 1 ; idx >= 0 ; idx--)
	{
		pRange = pOfflineList->at(idx);
		pRange->setFilterType(newType);
	}
	for(idx = pOnlineList->count() - 1 ; idx >= 0 ; idx--)
	{
		pRange = pOnlineList->at(idx);
		pRange->setFilterType(newType);
	}
}

/*
**	FUNCTION
**		Find min and max over all ranges of this device
**
**	PARAMETERS
**		min		- Variable where minimum value will be written
**		max		- Variable where max value will be written
**
**	RETURNS
**		Number of values checked
**
**	CAUTIONS
**		None
*/
int HistoryEqpData::findLimits(float &min, float &max)
{
	if(bitState)
	{
		min = 0;
		max = 1;
		return 1000;	// does not matter how many values to check - result is always min=0, max=1
	}
	min = HISTORY_HIGH_VALUE;
	max = HISTORY_LOW_VALUE;
	int count = 0, idx;
	HistoryDataRange *pRange;
	for(idx = pOfflineList->count() - 1 ; idx >= 0 ; idx--)
	{
		pRange = pOfflineList->at(idx);
		float rangeMin, rangeMax;
		count += pRange->findLimits(rangeMin, rangeMax);
		if(rangeMin < min)
		{
			min = rangeMin;
		}
		if(rangeMax > max)
		{
			max = rangeMax;
		}
	}
	for(idx = pOnlineList->count() - 1 ; idx >= 0 ; idx--)
	{
		pRange = pOnlineList->at(idx);
		float rangeMin, rangeMax;
		count += pRange->findLimits(rangeMin, rangeMax);
		if(rangeMin < min)
		{
			min = rangeMin;
		}
		if(rangeMax > max)
		{
			max = rangeMax;
		}
	}
	return count;
}

/*
**	FUNCTION
**		Find value of this device at given time (or before that time)
**
**	PARAMETERS
**		time	- Time when value is needed
**		value	- Variable where found value will be returned
**
**	RETURNS
**		true	- If value was found;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool HistoryEqpData::getValueAt(QDateTime &time, float &value)
{
	bool		valueFound = false;
	
	HistoryDataRange *pRange;
	int idx;
	for(idx = 0 ; idx < pOfflineList->count() ; idx++)
	{
		pRange = pOfflineList->at(idx);
		if(pRange->getValueAt(time, value))
		{
/*
printf("HistoryEqpData::getValueAt(%s): found in range %s to %s\n",
	time.toString("dd-MM-yyyy hh:mm:ss.zzz").ascii(),
	pRange->getStart().toString("dd-MM-yyyy hh:mm:ss.zzz").ascii(),
	pRange->getEnd().toString("dd-MM-yyyy hh:mm:ss.zzz").ascii());
fflush(stdout);
*/
			valueFound = true;
		}
		/* L.Kopylov 09.06.2011 Range without values at all can exist
		else if(valueFound)	// Found in previous range. L.Kopylov 24.08.2010
		{
			return valueFound;
		}
		*/
	}
	for(idx = 0 ; idx < pOnlineList->count() ; idx++)
	{
		pRange = pOnlineList->at(idx);
		if(pRange->getValueAt(time, value))
		{
			valueFound = true;
		}
		/* L.Kopylov 09.06.2011 Range without values at all can exist
		else if(valueFound)	// Found in previous range. L.Kopylov 24.08.2010
		{
			return valueFound;
		}
		*/
	}
	return valueFound;
}

/*
**	FUNCTION
**		Add all values in given time range to class that will export them
**		to Excel file
**
**	PARAMETERS
**		minTime		- Start time of interval to be exported
**		maxTime		- End time of interval to be exported
**		processor	- Processor where we shall add values
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryEqpData::addExportValues(const QDateTime &minTime, const QDateTime &maxTime, HistoryProcessor &processor)
{
	int idx;
	HistoryDataRange *pRange;
	qint64 minTimeAsMsec = minTime.toMSecsSinceEpoch();
	qint64 maxTimeAsMsec = maxTime.toMSecsSinceEpoch();
	for(idx = 0 ; idx < pOfflineList->count() ; idx++)
	{
		pRange = pOfflineList->at(idx);
		if(pRange->getEnd() < minTimeAsMsec)
		{
			pRange->setLastAsPreviousExportValue(pEqp, pureDpeName, bitNbr, bitName, minTime, processor);
			continue;
		}
		if(pRange->getStart() > maxTimeAsMsec)
		{
			return;
		}
		pRange->addExportValues(pEqp, pureDpeName, bitNbr, bitName, minTime, maxTime, processor);
	}
	for(idx = 0 ; idx < pOnlineList->count() ; idx++)
	{
		pRange = pOnlineList->at(idx);
		pRange->addExportValues(pEqp, pureDpeName, bitNbr, bitName, minTime, maxTime, processor);
	}
}

/*
**	FUNCTION
**		Slot notified when online value of my DPE is changed. Add new
**		value to list of values, notify that new data arrived.
**
**	PARAMETERS
**		pSrc		- Pointer to object - source of signal
**		srcDpeName	- DPE name
**		source		- DPE change source
**		value		- New value for DPE
**		mode		- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryEqpData::dpeChange(Eqp * /* pSrc */, const char * srcDpeName, DataEnum::Source source,
	const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	// Until now NO situations known where value comes from parent/PLC
	if(source != DataEnum::Own)
	{
		return;
	}
	if(mode != DataEnum::Online)
	{
		return;
	}
	QString fullDpeName(pEqp->getDpName());
	fullDpeName += ".";
	fullDpeName += srcDpeName;
	if(fullDpeName == dpeName)
	{
		addOnlineValue(value, timeStamp);
	}
}

void HistoryEqpData::addOnlineValue(const QVariant &value, const QDateTime &timeStamp)
{
	float newValue;
	if(bitState)
	{
		if(!value.canConvert(QVariant::UInt))
		{
			return;
		}
		unsigned u = value.toUInt();
		newValue = u & (1 << bitNbr) ? 1 : 0;
	}
	else
	{
		if(!pEqp->convertValue(value, newValue))
		{
			return;
		}
		newValue *= valueCoeff;
	}
	/*
	if(!value.canCast(QVariant::Double))
	{
		return;
	}
	float newValue = (float)value.toDouble();
	*/
	HistoryDataRange *pRange = pOnlineList->isEmpty() ? NULL : pOnlineList->last();
	if(!pRange)
	{
		pRange = new HistoryDataRange();
		pRange->setFilterType(filterType);
		pOnlineList->append(pRange);
	}
	if(!pRange->canAddValue())
	{
		pRange = new HistoryDataRange();
		pRange->setFilterType(filterType);
		pOnlineList->append(pRange);
	}
	pRange->addOnlineValue(newValue, timeStamp);
	emit onlineValueArrived();
}

/*
**	FUNCTION
**		Draw device's data
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		pTimeAxis	- Pointer to time (horizontal) axis
**		pValueAxis	- Pointer to value (vertical) axis
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryEqpData::draw(QPainter &painter, TimeAxis *pTimeAxis, VerticalAxis *pValueAxis)
{
	ScreenPoint sp;
	int lineWidth = isSelected() ? VacMainView::getHistorySelectWidth() : 0, idx;
	HistoryDataRange *pRange;
	for(idx = 0 ; idx < pOfflineList->count() ; idx++)
	{
		pRange = pOfflineList->at(idx);
		if(pRange->draw(painter, pTimeAxis, pValueAxis, color, lineWidth,
			pEqp->isMainValueNegative(), sp))
		{
			return;
		}
	}
	for(idx = 0 ; idx < pOnlineList->count() ; idx++)
	{
		pRange = pOnlineList->at(idx);
		if(pRange->draw(painter, pTimeAxis, pValueAxis, color, lineWidth,
			pEqp->isMainValueNegative(), sp))
		{
			break;
		}
	}
	// For online range: check if last drawn point is extended till
	// right side of axis, if not - draw horizontal line till the end
	// of time axis
	// LIK 11.03,2009 if(sp.getTimeRange() == TimeAxis::BeforeMin)
	// SBLANCHA 03.07.2018 if no new value draw a dash line till the end of time axis
	if(sp.getX() < pTimeAxis->getScaleRight())
	{
		QColor useColor;
		if(sp.isNegative())
		{
			useColor = VacMainView::getHistoryNegativeColor();
		}
		else
		{
			useColor = color;
		}
		QPen pen(useColor, lineWidth, Qt::DotLine);
		painter.setPen(pen);
		painter.drawLine(sp.getX(), sp.getY(), pTimeAxis->getScaleRight(), sp.getY());
	}
}

/*
**	FUNCTION
**		Draw device's data - the same as above, but uses another classes foe axies
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		pTimeAxis	- Pointer to time (horizontal) axis
**		pValueAxis	- Pointer to value (vertical) axis
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryEqpData::draw(QPainter &painter, TimeAxisWidget *pTimeAxis, VerticalAxisWidget *pValueAxis)
{
	ScreenPoint sp;
	int lineWidth = isSelected() ? VacMainView::getHistorySelectWidth() : 0, idx;
	HistoryDataRange *pRange;
	for(idx = 0 ; idx < pOfflineList->count() ; idx++)
	{
		pRange = pOfflineList->at(idx);
		if(pRange->draw(painter, pTimeAxis, pValueAxis, color, lineWidth,
			pEqp->isMainValueNegative(), sp))
		{
			return;
		}
	}
	for(idx = 0 ; idx < pOnlineList->count() ; idx++)
	{
		pRange = pOnlineList->at(idx);
		if(pRange->draw(painter, pTimeAxis, pValueAxis, color, lineWidth,
			pEqp->isMainValueNegative(), sp))
		{
			break;
		}
	}
	// For online range: check if last drawn point is extended till
	// right side of axis, if not - draw horizontal line till the end
	// of time axis
	// LIK 11.03.2009 if(sp.getTimeRange() == TimeAxis::BeforeMin)
	// SBLANCHA 03.07.2018 if no new value draw a dash line till the end of time axis
	if(sp.getX() < pTimeAxis->getScaleRight())
	{
		QColor useColor;
		if(sp.isNegative())
		{
			useColor = VacMainView::getHistoryNegativeColor();
		}
		else
		{
			useColor = color;
		}
		QPen pen(useColor, lineWidth, Qt::DotLine);
		painter.setPen(pen);
		painter.drawLine(sp.getX(), sp.getY(), pTimeAxis->getScaleRight(), sp.getY());
	}
}

/*
**	FUNCTION
**		Split DP name in form
**		<DPName>.<DPEname>
**		to 2 components: pure DP name and DPE name. Split is done on first
**		occurence of '.' in input name
**
**	PARAMETERS
**		dp			- Input DP + DPE name
**		ppDpName	- Address of variable where pointer to pure DP name
**						will be returned
**		ppDpeName	- Address of variable where pointer to pure DP name
**						will be returned
**
**	RETURNS
**		true - if split was successful;
**		false - otherwise
**
**	CAUTIONS
**		Function returns pointers to static buffer, content of buffer will
**		be overwritten by next call
*/
bool HistoryEqpData::splitDpName(const char *dp, char **ppDpName, char **ppDpeName)
{
	static char buf[1024];
	if(!dp)
	{
		return false;
	}
	if(!*dp)
	{
		return false;
	}
#ifdef Q_OS_WIN
	strcpy_s(buf, sizeof(buf) / sizeof(buf[0]), dp);
#else
	strcpy(buf, dp);
#endif
	char *firstDot = strchr(buf, '.');
	*ppDpName = buf;
	if(firstDot)
	{
		*firstDot = '\0';
		firstDot++;
		*ppDpeName = firstDot;
	}
	else
	{
		*ppDpeName = NULL;
	}
	return true;
}

void HistoryEqpData::dump(FILE *pFile)
{
	if(!DebugCtl::isHistory())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	fprintf(pFile, " Eqp data for %s %s\n", pEqp->getVisibleName(),
		(bitState ? "BIT" : ""));
	HistoryDataRange *pItem = NULL;
	fprintf(pFile, "  %d offline ranges:\n", pOfflineList->count());
	for(int idx = 0 ; idx < pOfflineList->count() ; idx++)
	{
		pItem = pOfflineList->at(idx);
		pItem->dump(pFile);
	}
	fprintf(pFile, "  %d online ranges:\n", pOnlineList->count());
	for(int idx = 0 ; idx < pOnlineList->count() ; idx++)
	{
		pItem = pOnlineList->at(idx);
		pItem->dump(pFile);
	}
}
