#ifndef	EQPVP_STDIO_H
#define	EQPVP_STDIO_H

//	EqpVP_STDIO	- Primary pump with manual valve in TDC2 of SPS

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVP_STDIO : public Eqp
{
	Q_OBJECT

public:
	EqpVP_STDIO(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVP_STDIO(const EqpVP_STDIO &src) : Eqp(src) {}
#endif
	~EqpVP_STDIO();

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	virtual int getErrorCode(DataEnum::DataMode mode);
	bool isStarting(unsigned rr1) const;	// Criteria is rather complex - use dedicated function
	inline bool isStarting(DataEnum::DataMode mode) const { return  mode == DataEnum::Replay ? isStarting(rr1Replay) : isStarting(rr1Online); }
	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of 
	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, bool plcAlarm, QString &string);
#endif
};

#endif	// EQPVP_STDIO_H
