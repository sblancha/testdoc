#ifndef	FUNCTIONALTYPEVACOK_H
#define	FUNCTIONALTYPEVACOK_H

// Special functional type for CRYO alarms - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVACOK : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		OK = 1
	};

	FunctionalTypeVACOK(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVACOK() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVACOK_H

