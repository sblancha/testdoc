#ifndef	BAKEOUTEQPTREEITEM_H
#define	BAKEOUTEQPTREEITEM_H

// Item of tree view representing bakeout rack

#include <QTreeWidget>

#include "BakeoutWorkDp.h"

#include "MobileTreeItemTypes.h"

class	Eqp;
class MobileHistoryImage;

class BakeoutEqpTreeItem : public QTreeWidgetItem
{
public:
	BakeoutEqpTreeItem(MobileHistoryImage *pImage, QTreeWidgetItem *parent, Eqp *pEqp);
	virtual ~BakeoutEqpTreeItem();

	virtual void setExpanded(bool open);

	// Access
	inline Eqp *getEqp(void) { return pEqp; }

protected:
	// Pointer to device for this item
	Eqp	*pEqp;

	void addChannelItem(MobileHistoryImage *pImage, int channel, enum BakeoutWorkDp::ChannelValueType valueType);
};

#endif	// BAKEOUTEQPTREEITEM_H
