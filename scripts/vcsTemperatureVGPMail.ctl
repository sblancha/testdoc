
// E-mail notification for problems during LHC operation:
//	1) VGP pressure above limit
//	2) Max temperature grows too fast

#uses "VacCtlEqpDataPvss"	// Load DLL or so
#uses "vclSectors.ctl"	// Load CTRL library
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "email.ctl"	// Load PVSS standard CTRL library
#uses "vclDevCommon.ctl"	// Load CTRL library
#uses "vclTemperatureVGPEmailConfig.ctl"	// Load CTRL library

//********************** General configuration *********************************

// SMTP server
string smtpServer;

// E-mail sender
string sender;

// E-mail sender client
string	client;

// Recipients for problems in all sectors
dyn_string	recipientsAll;

// Recipients for specific LSS (8 LSSs)
dyn_dyn_string	recipientsLss;

// Flag indicating if all messages are disabled
bool	disabled;

// Minimum interval between messages [minutes]
int	deadTime;

// Pressure limit for VGP
float	prLimit;

// Temperature limit for thermometers with nominal 1.9K
float	tLimit2;

// Temperature limit for thermometers with nominal 4.5K
float	tLimit4;


// The following parameters are in principle available in DLL.
// But, taking into account that:
//	1) DLL does not have functions to read these data
//	2) LHC geometry is VERY unlikely to change
// it was decided to put constants here instead of adding extra
// functions to DLL

// Coordinates of 8 IPs (9th IP = 1st IP)
dyn_float ipCoords;



// Constants: indices in dyn_anytype common for VGP and T
const int ALARM = 1;	// Alarm condition is true/false
const int MESSAGES_SENT = 2;
const int LAST_MESSAGE_TIME = 3;
const int LSS_NUMBER = 4;
const int VALUE = 5;

// List of all VGP DP names under control
dyn_string	vgpNames;

// Smoothing parameter for VGP pressures: the pressure is calculated
// as average over last measurements
const int P_AVERAGING = 5;

// Parameters for every VGP in vgpNames. For every VGP list contains
// the following items:
//	1 - Flag indicating if alarm condition is true for VGP [bool]
//	2 - How many messages have been sent already [int]
//	3 - Time when last message was sent [time]
//	4 - Index of LSS where device is located [int] [1...8]
//	5 - Last calculated pressure [float]
//	6... Raw pressure values, P_AVERAGING values of [float]
dyn_anytype	vgpValues;

// Start index of first raw value for VGP
const int FIRST_RAW_PR = 6;


// List of all thermometers DP names under control
dyn_string	thermoNames;

// Smoothing parameter for temperatures: the pressure is calculated
// as average over last measurements
const int T_AVERAGING = 5;

// Parameters for every thermometer in thermoNames. For every thermometer
// list contains the following items:
//	1 - Flag indicating if alarm condition is true for thermometer [bool]
//	2 - How many messages have been sent already [int]
//	3 - Time when last message was sent [time]
//	4 - Index of LSS where device is located [int] [1...8]
//	5 - Last calculated temperature [float]
//	6 - Nominal temperature [int]
//	7... Raw temperature values, T_AVERAGING values of [float]
dyn_anytype	thermoValues;

// Index of nominal temperature in thermometers data
const int NOMINAL_T = 6;

// Start index of first raw value for thermometer
const int FIRST_RAW_T = 7;



// Flag to enable debugging messages
bool debug = false;

void main()
{
	// Initialize static data
	if(!InitStaticData())
	{
		return;
	}

	ipCoords = makeDynFloat(0.0, 3332.3604, 6664.7208, 9997.0812,
							13329.4416, 16661.802, 19994.1624, 23315.3028, 26658.8832);

	// Connect to E-mail configuration DPEs
	if(!ConnectToConfig())
	{
		return;
	}

	// Connect to VPGs on beam vacuum
	if(!ConnectToGauges())
	{
		return;
	}

	// Connect to thermometers under control
	if(!ConnectToThermometers())
	{
		return;
	}

	// Wait forever, check for alarm conditions from time to time
	while(true)
	{
		delay(25);
		SendMessages();
	}
}

/** SendMessages
Purpose:
Check if messages for device(s) shall be sent, if yes - format and send
messages

Parameters:
	- None

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.1
	. operating system: WXP and Linux.
	. distributed system: yes.
*/

void SendMessages()
{
	if(disabled)
	{
		return;
	}

	// VGPs
	for(int n = dynlen(vgpNames) ; n > 0 ; n--)
	{
		if(!vgpValues[n][ALARM])
		{
			continue;
		}
		if(vgpValues[n][MESSAGES_SENT] > 2)	// Do not send too many messages
		{
			continue;
		}
		time now = getCurrentTime();
		int delta = now - vgpValues[n][LAST_MESSAGE_TIME];
		if(delta < (deadTime * 60))
		{
			continue;
		}
		if(SendMessageVGP(vgpNames[n], vgpValues[n][VALUE], vgpValues[n][LSS_NUMBER]))
		{
			vgpValues[n][LAST_MESSAGE_TIME] = now;
			vgpValues[n][MESSAGES_SENT]++;
		}
	}

	// Thermometers
	for(int n = dynlen(thermoNames) ; n > 0 ; n--)
	{
		if(!thermoValues[n][ALARM])
		{
			continue;
		}
		if(thermoValues[n][MESSAGES_SENT] > 2)	// Do not send too many messages
		{
			continue;
		}
		time now = getCurrentTime();
		int delta = now - thermoValues[n][LAST_MESSAGE_TIME];
		if(delta < (deadTime * 60))
		{
			continue;
		}
		if(SendMessageThermometer(thermoNames[n], thermoValues[n][VALUE], thermoValues[n][LSS_NUMBER]))
		{
			thermoValues[n][LAST_MESSAGE_TIME] = now;
			thermoValues[n][MESSAGES_SENT]++;
		}
	}
}

bool SendMessageVGP(string dpName, float value, int lssNumber)
{
	// Find list of recipients
	dyn_string	recipients = FindRecipients(dpName, lssNumber);
	if(dynlen(recipients) == 0)
	{
		return false;	// Message was NOT sent
	}

	// Find sector for message
	int					vacType;
	string			sector1, sector2, mainPart;
	bool				isBorder;
	dyn_string	exceptionInfo;

	LhcVacDeviceVacLocation(dpName, vacType, sector1, sector2, mainPart, isBorder, exceptionInfo);
	if(dynlen(exceptionInfo) > 0)
	{
		DebugTN("vcsTemperatureVGPMail.ctl: LhcVacDeviceVacLocation(" + dpName + ") failed");
		return 0;
	}

	string devName;
	LhcVacDisplayName(dpName, devName, exceptionInfo);
	if(devName == "")
	{
		devName = dpName;
	}
	string buf;
	sprintf(buf, "%6.2E", value);
	string message = "Alarm " + devName + " in sector " + sector1 + ": " + buf;
	return SendMessage(message, recipients);
}

bool SendMessageThermometer(string dpName, float value, int lssNumber)
{
	// Find list of recipients
	dyn_string	recipients = FindRecipients(dpName, lssNumber);
	if(dynlen(recipients) == 0)
	{
		return false;	// Message was NOT sent
	}

	string devName;
	dyn_string	exceptionInfo;
	LhcVacDisplayName(dpName, devName, exceptionInfo);
	if(devName == "")
	{
		devName = dpName;
	}
	string buf;
	sprintf(buf, "%.2f", value);
	string message = "Alarm " + devName + ": T = " + buf;
	return SendMessage(message, recipients);
}

bool SendMessage(string message, dyn_string recipients)
{
	// Prepare and send message
	dyn_string	email = makeDynString( "" );
	for(int n = dynlen(recipients) ; n > 0 ; n--)
	{
		if(email[1] != "")
		{
			email[1] += ";";
		}
		email[1] += recipients[n];
	}
	email[2] = sender;
	email[3] = message;
	email[4] = "";
	if(debug)
	{
		DebugTN( "vcsTemperatureVGPMail.ctl: message to send is '" + email[3] + "'" );
	}
	int coco;
	emSendMail(smtpServer, client, email, coco);
	return coco < 0 ? false : true;
}

dyn_string FindRecipients(string dpName, int lssNumber)
{
	dyn_string result;
	if((client == "") || (smtpServer == "") || (sender == ""))
	{
		return result;
	}
	for(int n = dynlen(recipientsAll) ; n > 0 ; n--)
	{
		string recipient = recipientsAll[n];
		dynAppend(result, recipient);
	}
	for(int n = dynlen(recipientsLss[lssNumber]) ; n > 0 ; n--)
	{
		string recipient = recipientsLss[lssNumber][n];
		if(dynContains(result, recipient) <= 0)
		{
			dynAppend(result, recipient);
		}
	}
	return result;
}

/** ConnectToGauges
Purpose:
Connect to all controllable gauges, fill in initial content of gauges data

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.1
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToGauges()
{
	dyn_string	vgpTypes = makeDynString("VGP_C", "VGP_C0", "VGP_T");
	for(int n = dynlen(vgpTypes) ; n > 0 ; n--)
	{
		dyn_string gaugeNames = dpNames("**", vgpTypes[n]);
		int	count = 0;
		for(int i = dynlen(gaugeNames) ; i > 0 ; i--)
		{
			int	coco = ConnectToGauge(dpSubStr(gaugeNames[i], DPSUB_DP));
			switch(coco)
			{
			case 0:	// Gauge is not used
				break;
			case 1:	// Gauge is used
				count++;
				break;
			case -1:	// Fatal error
				return false;
			}
		}
		DebugTN("vcsTemperatureVGPMail.ctl: connected to " + count + " of type " + vgpTypes[n]);
	}
	return true;
}

/** ConnectToGauge
Purpose:
Check if given VGP shall be controlled, if yes - add gauge data to local
cache and connect to DPEs of gauge

Parameters:
	- dpName	- [in] DP name of gauge to be checked

Return:
	- 1 = gauge is controlled;
		0 = gauge is not controlled;
		-1 = fatal error

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.1
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
int ConnectToGauge(string dpName)
{
	int					vacType;
	string			sector1, sector2, mainPart;
	bool				isBorder;
	dyn_string	exceptionInfo;

	LhcVacDeviceVacLocation(dpName, vacType, sector1, sector2, mainPart, isBorder, exceptionInfo);
	if(dynlen(exceptionInfo) > 0)
	{
		DebugTN("vcsTemperatureVGPMail.ctl: LhcVacDeviceVacLocation(" + dpName + ") failed");
		return 0;
	}
	switch(vacType)
	{
	case LVA_VACUUM_RED_BEAM:
	case LVA_VACUUM_BLUE_BEAM:
	case LVA_VACUUM_CROSS_BEAM:
	case LVA_VACUUM_BEAMS_COMMON:
		break;
	default:
		return 0;
	}

	// Vacuum type is OK, get LSS number for device
	int	lssNumber;
	if(!GetLssNumber(dpName, lssNumber))
	{
		return -1;
	}
	if(lssNumber == 0)
	{
		return 0;
	}

	// Add gauge information and name to list of values
	dyn_anytype eqpData = makeDynAnytype(
		(bool)false,	// alarm condition
		(int)0,				// how many messages have been sent
		(time)makeTime(1980, 01, 01, 01, 00, 00),	// Time when message was sent
		(int)lssNumber,	// LSS number for device
		(float)10000.0		// Last average value
		);
	for(int n = P_AVERAGING ; n > 0 ; n--)
	{
		dynAppend(eqpData, (float)0.0);
	}
	int idx = dynAppend(vgpNames, dpName);
	vgpValues[idx] = eqpData;

	// Connect to VGP
	string	plcDp, alarmDp;
	LhcVacDevicePlc(dpName, plcDp, alarmDp, exceptionInfo);
	if(alarmDp != "")
	{
		dpConnect("VgpCbPLC",
			dpName + ".RR1",
			dpName + ".PR",
			alarmDp + ".alarm");
	}
	else
	{
		dpConnect("VgpCbNoPLC", dpName + ".RR1",
			dpName + ".PR");
	}
	return 1;
}
void VgpCbPLC(string dpe1, unsigned rr1, string dpe2, float value, string dpe3, int alarm)
{
	ProcessVgpData(dpSubStr(dpe1, DPSUB_DP), rr1, value, alarm);
}
void VgpCbNoPLC(string dpe1, unsigned rr1, string dpe2, float value)
{
	ProcessVgpData(dpSubStr(dpe1, DPSUB_DP), rr1, value, 0);
}

void ProcessVgpData(string dpName, unsigned rr1, float value, int alarm)
{
	int idx = dynContains(vgpNames, dpName);
	if(idx < 0)
	{
		DebugTN("vcsTemperatureVGPMail.ctl: ProcessVgpData(): unknown DP <" + dpName + ">");
		return;
	}
	if(alarm != 0)	// Bad PLC state
	{
		SetVgpInvalid(idx);
		return;
	}
	if((rr1 & (VG_SPS_R_VALID | VG_SPS_R_PRESS_VALID)) != (VG_SPS_R_VALID | VG_SPS_R_PRESS_VALID))
	{
		SetVgpInvalid(idx);
		return;
	}

	// Add to raw values
	for(int n = 1 ; n < P_AVERAGING ; n++)
	{
		vgpValues[idx][FIRST_RAW_PR + n - 1] = vgpValues[idx][FIRST_RAW_PR + n];
	}
	vgpValues[idx][FIRST_RAW_PR + P_AVERAGING - 1] = value;

	// Calculate average
	float average = 0;
	for(int n = 0 ; n < P_AVERAGING ; n++)
	{
		if(vgpValues[idx][FIRST_RAW_PR + n] == 0.0)
		{
			return;	// Not enough values for averaging
		}
		average += vgpValues[idx][FIRST_RAW_PR + n];
	}
	average /= P_AVERAGING;
	vgpValues[idx][VALUE] = average;
	vgpValues[idx][ALARM] = average > prLimit;
	if(!vgpValues[idx][ALARM])
	{
		vgpValues[idx][MESSAGES_SENT] = 0;
	}
	if(debug && vgpValues[idx][ALARM])
	{
		string buf;
		sprintf(buf, "%6.2E", average);
		DebugTN("vcsTemperatureVGPMail.ctl: " + dpName + ", value " + buf +
			", alarm " + vgpValues[idx][ALARM]);
	}
}
void SetVgpInvalid(int idx)
{
	vgpValues[idx][ALARM] = false;
	vgpValues[idx][VALUE] = 0.0;
	for(int n = 0 ; n < P_AVERAGING ; n++)
	{
		vgpValues[idx][FIRST_RAW_PR + n] = 0.0;
	}
}

/** ConnectToThermometers
Purpose:
Connect to all controllable thermometers, fill in initial content of gauges data

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.1
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToThermometers()
{
	dyn_string	thermoTypes = makeDynString("VCRYO_TT", "VCRYO_TT_Ext");
	for(int n = dynlen(thermoTypes) ; n > 0 ; n--)
	{
		dyn_string tNames = dpNames("**", thermoTypes[n]);
		int	count = 0;
		for(int i = dynlen(tNames) ; i > 0 ; i--)
		{
			int	coco = ConnectToThermometer(dpSubStr(tNames[i], DPSUB_DP));
			switch(coco)
			{
			case 0:	// Thermometer is not used
				break;
			case 1:	// Thermometer is used
				count++;
				break;
			case -1:	// Fatal error
				return false;
			}
		}
		DebugTN("vcsTemperatureVGPMail.ctl: connected to " + count + " of type " + thermoTypes[n]);
	}
	return true;
}

/** ConnectToThermometer
Purpose:
Check if given thermometer shall be controlled, if yes - add thermometer data to local
cache and connect to DPEs of thermometer. Decision is based on presence of
attribute "NominalT"

Parameters:
	- dpName	- [in] DP name of thermometer to be checked

Return:
	- 1 = thermometer is controlled;
		0 = thermometer is not controlled;
		-1 = fatal error

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.1
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
int ConnectToThermometer(string dpName)
{
	string	attrValue;
	LhcVacGetAttribute(dpName, "NominalT", attrValue);
	if(attrValue == "")
	{
		return 0;
	}
	int	nominalT;
	if(sscanf(attrValue, "%d", nominalT) != 1)
	{
		return 0;
	}
	if((nominalT != 2) && (nominalT != 4))
	{
		return 0;
	}

	// Nominal T is OK, get LSS number for device
	int	lssNumber;
	if(!GetLssNumber(dpName, lssNumber))
	{
		return -1;
	}
	if(lssNumber == 0)
	{
		return 0;
	}

	// Add thermoemeter information and name to list of values
	dyn_anytype eqpData = makeDynAnytype(
		(bool)false,	// alarm condition
		(int)0,				// how many messages have been sent
		(time)makeTime(1980, 01, 01, 01, 00, 00),	// Time when message was sent
		(int)lssNumber,	// LSS number for device
		(float)300.0,		// Last average value
		(int)nominalT	// Nominal temperature
		);
	for(int n = T_AVERAGING ; n > 0 ; n--)
	{
		dynAppend(eqpData, (float)0.0);
	}
	int idx = dynAppend(thermoNames, dpName);
	thermoValues[idx] = eqpData;

	// Connect to thermometer
	dpConnect("ThermoCb",
		dpName + ".RR1",
		dpName + ".T");
	return 1;
}
void ThermoCb(string dpe1, unsigned rr1, string dpe2, float value)
{
	string dpName = dpSubStr(dpe1, DPSUB_DP);
	int idx = dynContains(thermoNames, dpName);
	if(idx < 0)
	{
		DebugTN("vcsTemperatureVGPMail.ctl: ThermoCb(): unknown DP <" + dpName + ">");
		return;
	}
	if((rr1 & VCRYO_TT_R_INVALID) != 0)	// Value is invalid
	{
		thermoValues[idx][ALARM] = false;
		thermoValues[idx][VALUE] = 0.0;
		for(int n = 0 ; n < T_AVERAGING ; n++)
		{
			thermoValues[idx][FIRST_RAW_T + n] = 0.0;
		}
		return;
	}

	// Add to raw values
	for(int n = 1 ; n < T_AVERAGING ; n++)
	{
		thermoValues[idx][FIRST_RAW_T + n - 1] = thermoValues[idx][FIRST_RAW_T + n];
	}
	thermoValues[idx][FIRST_RAW_T + T_AVERAGING - 1] = value;

	// Calculate average
	float average = 0;
	for(int n = 0 ; n < T_AVERAGING ; n++)
	{
		if(thermoValues[idx][FIRST_RAW_T + n] == 0.0)
		{
			return;	// Not enough values for averaging
		}
		average += thermoValues[idx][FIRST_RAW_T + n];
	}
	average /= T_AVERAGING;
	thermoValues[idx][VALUE] = average;
	switch(thermoValues[idx][NOMINAL_T])
	{
	case 2:
		thermoValues[idx][ALARM] = average > tLimit2;
		break;
	case 4:
		thermoValues[idx][ALARM] = average > tLimit2;
		break;
	}	
	if(!thermoValues[idx][ALARM])
	{
		thermoValues[idx][MESSAGES_SENT] = 0;
	}
	if(debug && thermoValues[idx][ALARM])
	{
		string buf;
		sprintf(buf, "%6.2E", average);
		DebugTN("vcsTemperatureVGPMail.ctl: " + dpName + ", value " + buf +
			", nominal " + thermoValues[idx][NOMINAL_T] + ",alarm " +
			thermoValues[idx][ALARM]);
	}
}

/** GetLssNumber
Purpose:
Find LSS number for given DP name. Decision is taken based on closest IP

Parameters:
	- dpName	- [in] Name of DP
		lssNumber	- [out] LSS number for this DP, or 0 if not found

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.1
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool GetLssNumber(string dpName, int &lssNumber)
{
	lssNumber = 0;
	float	coord;
	dyn_string	exceptionInfo;
	LhcVacDeviceLocation(dpName, coord, exceptionInfo);
	if(dynlen(exceptionInfo) > 0)
	{
		DebugTN("vcsTemperatureVGPMail.ctl: LhcVacDeviceLocation(" + dpName + ") failed");
		return false;
	}
	float bestDistance = 1.0E10;	// very high value
	for(int n = dynlen(ipCoords) ; n > 0 ; n--)
	{
		float distance = ipCoords[n] - coord;
		if(distance < 0.0)
		{
			distance = -distance;
		}
		if(distance < bestDistance)
		{
			bestDistance = distance;
			lssNumber = n;
		}
	}
	if(lssNumber == 9)
	{
		lssNumber = 1;
	}
	return true;
}


/** ConnectToConfig
Purpose:
Connect to all configuration DPEs

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.1
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToConfig()
{
	string			configDp;
	dyn_string	exceptionInfo;

	VacVgpEmailGetConfigDp(configDp, exceptionInfo);
	if(dynlen(exceptionInfo) > 0)
	{
		DebugTN("vcsTemperatureVGPMail.ctl: VacVgpEmailGetConfigDp() failed", exceptionInfo);
		return false;
	}
	dpConnect("ClientCb", configDp + ".client");
	dpConnect("ServerCb", configDp + ".server");
	dpConnect("SenderCb", configDp + ".sender");
	dpConnect("DisabledCb", configDp + ".disabled");
	dpConnect("DeadTimeCb", configDp + ".deadTime");
	dpConnect("PrLimitCb", configDp + ".prLimit");
	dpConnect("TLimit2Cb", configDp + ".tLimit2");
	dpConnect("TLimit4Cb", configDp + ".tLimit4");
	dpConnect("RecipientsAllCb", configDp + ".recipientsAll");
	dpConnect("RecipientsLss1Cb", configDp + ".recipientsLss1");
	dpConnect("RecipientsLss2Cb", configDp + ".recipientsLss2");
	dpConnect("RecipientsLss3Cb", configDp + ".recipientsLss3");
	dpConnect("RecipientsLss4Cb", configDp + ".recipientsLss4");
	dpConnect("RecipientsLss5Cb", configDp + ".recipientsLss5");
	dpConnect("RecipientsLss6Cb", configDp + ".recipientsLss6");
	dpConnect("RecipientsLss7Cb", configDp + ".recipientsLss7");
	dpConnect("RecipientsLss8Cb", configDp + ".recipientsLss8");
	return true;
}
void ClientCb(string dpe, string value)
{
	client = value;
}
void ServerCb(string dpe, string value)
{
	smtpServer = value;
}
void SenderCb(string dpe, string value)
{
	sender = value;
}
void DisabledCb(string dpe, bool value)
{
	disabled = value;
}
void DeadTimeCb(string dpe, int value)
{
	deadTime = value;
}
void PrLimitCb(string dpe, float value)
{
	prLimit = value;
}
void TLimit2Cb(string dpe, float value)
{
	tLimit2 = value;
}
void TLimit4Cb(string dpe, float value)
{
	tLimit4 = value;
}
void RecipientsAllCb(string dpe, dyn_string value)
{
	recipientsAll = value;
}
void RecipientsLss1Cb(string dpe, dyn_string value)
{
	recipientsLss[1] = value;
}
void RecipientsLss2Cb(string dpe, dyn_string value)
{
	recipientsLss[2] = value;
}
void RecipientsLss3Cb(string dpe, dyn_string value)
{
	recipientsLss[3] = value;
}
void RecipientsLss4Cb(string dpe, dyn_string value)
{
	recipientsLss[4] = value;
}
void RecipientsLss5Cb(string dpe, dyn_string value)
{
	recipientsLss[5] = value;
}
void RecipientsLss6Cb(string dpe, dyn_string value)
{
	recipientsLss[6] = value;
}
void RecipientsLss7Cb(string dpe, dyn_string value)
{
	recipientsLss[7] = value;
}
void RecipientsLss8Cb(string dpe, dyn_string value)
{
	recipientsLss[8] = value;
}

/** InitStaticData
Purpose:
Read all machine configuration data from file(s)

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.1
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool InitStaticData()
{
	dyn_string	exceptionInfo;
	long				coco;
	bool				isLhcData;

	// Get accelerator name to be used by other components
	LhcVacGetMachineId(exceptionInfo);
	if(dynlen(exceptionInfo) > 0)
	{
		DebugTN("vcsTemperatureVGPMail.ctl: Failed to read machine name: " + exceptionInfo);
		return false;
	}
	DebugTN("vcsTemperatureVGPMail.ctl: Machine is " + glAccelerator);
	isLhcData = glAccelerator == "LHC" ? true : false;
	if(!isLhcData)
	{
		DebugTN("vcsTemperatureVGPMail.ctl: machine is not LHC, exiting...");
		return false;
	}

	// Initalize passive machine data
	coco = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
	if(coco < 0)
	{
		DebugTN("vcsTemperatureVGPMail.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo);
		return false;
	}
	DebugTN("vcsTemperatureVGPMail.ctl: LhcVacEqpInitPassive() done");

	// Initialzie active equipment information
	coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
			exceptionInfo);
	if(coco < 0)
	{
		DebugTN("vcsTemperatureVGPMail.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo);
		return false;
	}
	DebugTN( "vcsTemperatureVGPMail.ctl: LhcVacEqpInitActive() done" );

	DebugTN( "vcsTemperatureVGPMail.ctl: InitStaticData() done" );
	return true;
}



