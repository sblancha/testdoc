#ifndef FUNCTIONALTYPEPROCESS_VPG_6E01_H
#define FUNCTIONALTYPEPROCESS_VPG_6E01_H

// Process - pumping group 6E01

#include "FunctionalTypePROCESS.h"

class VACCTLEQPDATA_EXPORT FunctionalTypePROCESS_VPG_6E01 : public FunctionalTypePROCESS
{
public:
	FunctionalTypePROCESS_VPG_6E01(int type, const char *name, const char *description);
	virtual ~FunctionalTypePROCESS_VPG_6E01() {}

protected:
	virtual QString getStepResourceBase(void);
};

#endif	// FUNCTIONALTYPEPROCESS_VPG_6E01_H
