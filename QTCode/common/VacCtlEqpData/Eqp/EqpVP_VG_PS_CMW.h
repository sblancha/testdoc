#ifndef	EQPVP_VG_PS_CMW_H
#define EQPVP_VG_PS_CMW_H

//	Ion pumps and gauges for PSR - data from CMW

#include "Eqp.h"


class VACCTLEQPDATA_EXPORT EqpVP_VG_PS_CMW : public Eqp
{
	Q_OBJECT

public:
	EqpVP_VG_PS_CMW(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVP_VG_PS_CMW(const EqpVP_VG_PS_CMW &src) : Eqp(src) {}
#endif
	~EqpVP_VG_PS_CMW();

	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::Pressure; }
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode /* mode */) { return true; }
	virtual bool isMainValueNegative(void) const { return false; }
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
	virtual void getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end);
	virtual float getGrowFactorForDevList(void);
	virtual int getSpikeFilterForDevList(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline unsigned getRR1(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? rr1Replay : rr1Online;
	}
	inline float getPR(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? prReplay : prOnline;
	}
	inline bool isDriverOK(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? driverOkReplay : driverOkOnline;
	}

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of PR DPE for online mode
	float	prOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of PR DPE for replay mode
	float	prReplay;

	// Last known value of DriverOK DPE for online mode
	bool		driverOkOnline;

	// Last known value of DriverOK DPE for replay mode
	bool		driverOkReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(bool driverOk, unsigned rr1, float pr, QColor &color);
	virtual void getMainStateString(bool driverOk, unsigned rr1, float pr, QString &string);
	virtual void getMainValueString(bool driverOk, unsigned rr1, float pr, QString &string);
	virtual void getMainStateStringForMenu(bool driverOk, unsigned rr1, float pr, QString &string);
	virtual void getToolTipString(bool driverOk, unsigned rr1, float pr, QString &string);
#endif

};

#endif	// EQPVP_VG_PS_CMW_H
