//	Implementation of FlexTable class
/////////////////////////////////////////////////////////////////////////////////

#include "FlexTable.h"

#include "PlatformDef.h"

#include <QHeaderView>
#include <stdio.h>

EWO_PLUGIN(FlexTable)

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

FlexTable::FlexTable(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new QTableWidget(parent);
	baseWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);	// all cells are read-only
	connect(baseWidget, SIGNAL(cellClicked(int, int)), this, SLOT(tableCellClicked(int, int)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList FlexTable::signalList(void) const
{
	QStringList list;
	list.append("CellClicked(int row, int column)");
	return list;
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList FlexTable::methodList(void) const
{
	QStringList list;
	list.append("int setCellText(int row, int col, string text)");
	list.append("int setCellColor(int row, int col, string color)");
	list.append("int showRow(int row)");
	list.append("int hideRow(int row)");
	list.append("int showColumn(int col)");
	list.append("int hideColumn(int col)");
	list.append("int setColumnHeader(int col, string text)");
	list.append("int setColumnHeaderVisible(bool flag)");
	list.append("int setRowHeader(int row, string text)");
	list.append("int setRowHeaderVisible(bool flag)");
	list.append("int resizeRowsToContents()");
	list.append("int resizeColumnsToContents()");
	list.append("int clear()");
	list.append("int columnWidth(int column)");
	list.append("int setColumnWidth(int column, int width)");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant FlexTable::invokeMethod(const QString &name, QList<QVariant> &values, QString &error)
{
	if(name == "setCellText")
	{
		return processSetCellText(name, values, error);
	}
	else if(name == "setCellColor")
	{
		return processSetCellColor(name, values, error);
	}
	else if(name == "showRow")
	{
		return processShowRow(name, values, error);
	}
	else if(name == "hideRow")
	{
		return processHideRow(name, values, error);
	}
	else if(name == "showColumn")
	{
		return processShowColumn(name, values, error);
	}
	else if(name == "hideColumn")
	{
		return processHideColumn(name, values, error);
	}
	else if(name == "setColumnHeader")
	{
		return processSetColumnHeader(name, values, error);
	}
	else if(name == "setColumnHeaderVisible")
	{
		return processSetColumnHeaderVisible(name, values, error);
	}
	else if(name == "setRowHeader")
	{
		return processSetRowHeader(name, values, error);
	}
	else if(name == "setRowHeaderVisible")
	{
		return processSetRowHeaderVisible(name, values, error);
	}
	else if(name == "resizeRowsToContents")
	{
		baseWidget->resizeRowsToContents();
		return QVariant(0);
	}
	else if(name == "resizeColumnsToContents")
	{
		baseWidget->resizeColumnsToContents();
		return QVariant(0);
	}
	else if(name == "clear")
	{
		baseWidget->setRowCount(0);
		baseWidget->setColumnCount(0);
		return QVariant(0);
	}
	else if(name == "columnWidth")
	{
		return processColumnWidth(name, values, error);
	}
	else if(name == "setColumnWidth")
	{
		return processSetColumnWidth(name, values, error);
	}
	else
	{
		error = "Uknown method FlexTable.";
		error += name;
	}
	return QVariant(-1);
}

QVariant FlexTable::processSetCellText(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not int";
		return QVariant();
	}
	int row = values[0].toInt();
	if(!checkRow(row, error))
	{
		return QVariant(-1);
	}

	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (column) is not int";
		return QVariant();
	}
	int column = values[1].toInt();
	if(!checkColumn(column, error))
	{
		return QVariant(-1);
	}

	QString text = values[2].toString();
	QTableWidgetItem *pItem = baseWidget->item(row, column);
	if(!pItem)
	{
		pItem = new QTableWidgetItem(text);
		baseWidget->setItem(row, column, pItem);
	}
	pItem->setText(text);

	return QVariant(1);
}

QVariant FlexTable::processSetCellColor(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not int";
		return QVariant();
	}
	int row = values[0].toInt();
	if(!checkRow(row, error))
	{
		return QVariant(-1);
	}

	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (column) is not int";
		return QVariant();
	}
	int column = values[1].toInt();
	if(!checkColumn(column, error))
	{
		return QVariant(-1);
	}

	QString text = values[2].toString();
	int red, green, blue;
	if(sscanf(text.toLatin1().constData(), "{%d,%d,%d}", &red, &green, &blue) != 3)
	{
		if(sscanf(text.toLatin1().constData(), "[%d,%d,%d]", &red, &green, &blue) != 3)
		{
			error = "arument 3 (color): can not convert to color ";
			error += text;
			return QVariant(-1);
		}
		red = (int)rint(red * 2.55);
		green = (int)rint(green * 2.55);
		blue = (int)rint(blue * 2.55);
	}
	QColor color(red, green, blue);
	QTableWidgetItem *pItem = baseWidget->item(row, column);
	if(!pItem)
	{
		pItem = new QTableWidgetItem(text);
		baseWidget->setItem(row, column, pItem);
	}
	pItem->setBackground(color);

	return QVariant(1);
}

QVariant FlexTable::processShowRow(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not int";
		return QVariant();
	}
	int row = values[0].toInt();
	if(!checkRow(row, error))
	{
		return QVariant(-1);
	}
	baseWidget->showRow(row);
	return QVariant(1);
}

QVariant FlexTable::processHideRow(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not int";
		return QVariant();
	}
	int row = values[0].toInt();
	if(!checkRow(row, error))
	{
		return QVariant(-1);
	}
	baseWidget->hideRow(row);
	return QVariant(1);
}

QVariant FlexTable::processShowColumn(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (column) is not int";
		return QVariant();
	}
	int col = values[0].toInt();
	if(!checkColumn(col, error))
	{
		return QVariant(-1);
	}
	baseWidget->showColumn(col);
	return QVariant(1);
}

QVariant FlexTable::processHideColumn(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (column) is not int";
		return QVariant();
	}
	int col = values[0].toInt();
	if(!checkColumn(col, error))
	{
		return QVariant(-1);
	}
	baseWidget->hideColumn(col);
	return QVariant(1);
}

QVariant FlexTable::processSetColumnHeader(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (column) is not int";
		return QVariant();
	}
	int col = values[0].toInt();
	if(!checkColumn(col, error))
	{
		return QVariant(-1);
	}
	QString text = values[1].toString();
	QTableWidgetItem *pItem = baseWidget->horizontalHeaderItem(col);
	if(!pItem)
	{
		pItem = new QTableWidgetItem(text);
		baseWidget->setHorizontalHeaderItem(col, pItem);
	}
	pItem->setText(text);

	return QVariant(1);
}

QVariant FlexTable::processSetColumnHeaderVisible(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Bool))
	{
		error = "argument 1 (flag) is not boolean";
		return QVariant();
	}
	if(values[0].toBool())
	{
		baseWidget->horizontalHeader()->show();
	}
	else
	{
		baseWidget->horizontalHeader()->hide();
	}
	return QVariant(1);
}

QVariant FlexTable::processSetRowHeader(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not int";
		return QVariant();
	}
	int row = values[0].toInt();
	if(!checkRow(row, error))
	{
		return QVariant(-1);
	}
	QString text = values[1].toString();
	QTableWidgetItem *pItem = baseWidget->verticalHeaderItem(row);
	if(!pItem)
	{
		pItem = new QTableWidgetItem(text);
		baseWidget->setVerticalHeaderItem(row, pItem);
	}
	pItem->setText(text);

	return QVariant(1);
}

QVariant FlexTable::processSetRowHeaderVisible(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Bool))
	{
		error = "argument 1 (flag) is not boolean";
		return QVariant();
	}
	if(values[0].toBool())
	{
		baseWidget->verticalHeader()->show();
	}
	else
	{
		baseWidget->verticalHeader()->hide();
	}
	return QVariant(1);
}

QVariant FlexTable::processColumnWidth(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (column) is not int";
		return QVariant();
	}
	int column = values[0].toInt();
	if(!checkColumn(column, error))
	{
		return QVariant(-1);
	}
	return QVariant(baseWidget->columnWidth(column));
}

QVariant FlexTable::processSetColumnWidth(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant(-1);
	}

	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (column) is not int";
		return QVariant();
	}
	int column = values[0].toInt();
	if(!checkColumn(column, error))
	{
		return QVariant(-1);
	}
	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (width) is not int";
		return QVariant();
	}
	int width = values[1].toInt();
	baseWidget->setColumnWidth(column, width);
	return QVariant(baseWidget->columnWidth(column));
}


bool FlexTable::checkRow(int row, QString &error)
{
	if((0 <= row) && (row < baseWidget->rowCount()))
	{
		return true;
	}
	error = "invalid row ";
	error += QString::number(row);
	error += ", valid row must be in range 0...";
	error += QString::number(baseWidget->rowCount() - 1);
	return false;
}

bool FlexTable::checkColumn(int column, QString &error)
{
	if((0 <= column) && (column < baseWidget->columnCount()))
	{
		return true;
	}
	error = "invalid column ";
	error += QString::number(column);
	error += ", valid column must be in range 0...";
	error += QString::number(baseWidget->columnCount() - 1);
	return false;
}

void FlexTable::tableCellClicked(int row, int column)
{
	QList<QVariant> args;
	args.append(QVariant(row));
	args.append(QVariant(column));
	emit signal("CellClicked", args);
}

