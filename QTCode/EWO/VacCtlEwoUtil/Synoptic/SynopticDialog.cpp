//	Implementation of SynopticDialog class
/////////////////////////////////////////////////////////////////////////////////

#include "SynopticDialog.h"
#include "SynView.h"
#include "SynViewXml.h"

#include "VacMainView.h"
#include "Sector.h"
#include "DataPool.h"
#include "ResourcePool.h"
#include "EqpMsgCriteria.h"
#include "Eqp.h"

#include <qpushbutton.h>
#include <QMenu>
#include <qlabel.h>
#include <qcombobox.h>
#include <QScrollArea>
#include <QScrollBar>
#include <qlayout.h>
#include <qdesktopwidget.h>
#include <qapplication.h>

// Base name for equipment mask resource
#define RESOURCE_BASE	"Synoptic.BeamVacuumEqpTypes"

SynopticDialog	*SynopticDialog::pLastInstance = NULL;

void SynopticDialog::makeDeviceVisible(Eqp *pEqp)
{
	if(pLastInstance)
	{
		pLastInstance->ensureDeviceVisible(pEqp);
		pLastInstance = NULL;	// Only one call is accepted
	}
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

SynopticDialog::SynopticDialog(Sector *pFirstSector, Sector *pLastSector,
	DataEnum::DataMode mode) :
	QDialog(NULL /* VacMainView::getInstance() */, Qt::Window)
{
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle("Synoptic");
	this->pFirstSector = pFirstSector;
	this->pLastSector = pLastSector;
	this->mode = mode;
	pView = NULL;

	buildLayout();
	setTitle();
	buildInitialEqpMask();
	buildView(true);
	if(mode == DataEnum::Replay)
	{
		VacMainView::replayDialogOpened(this);
	}
	pLastInstance = this;
	ConnectEqpActiveChange();
	ConnectEqpPositionChange();
}

SynopticDialog::~SynopticDialog()
{
	if(pLastInstance == this)
	{
		pLastInstance = NULL;
	}
	if(mode == DataEnum::Replay)
	{
		VacMainView::replayDialogDeleted(this);
	}
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticDialog::buildLayout(void)
{
	// 1) Main layout - menu, labels and combobox on top, scrolled view on bottom
	QVBoxLayout *mainBox = new QVBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);

	// 2) Horizontal layout with
	//	- buttons for activating popup menus
	//	- title label
	//	- ComboBox with main part selection
	QHBoxLayout *topBox = new QHBoxLayout();
	topBox->setSpacing(0);
	mainBox->addLayout(topBox);

	// 2.1 all popup menus
	pFilePb = new QPushButton("&File", this);
	pFilePb->setFlat(true);
	QSize size = pFilePb->fontMetrics().size(Qt::TextSingleLine, "File");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pFilePb->setFixedSize(size);
	buildFileMenu();
	pFilePb->setMenu(pFileMenu);
	topBox->addWidget(pFilePb);

	pViewPb = new QPushButton("&View", this);
	pViewPb->setFlat(true);
	size = pViewPb->fontMetrics().size(Qt::TextSingleLine, "View");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pViewPb->setFixedSize(size);
	buildViewMenu();
	pViewPb->setMenu(pViewMenu);
	topBox->addWidget(pViewPb);

	pHelpPb = new QPushButton("&Help", this);
	pHelpPb->setFlat(true);
	size = pHelpPb->fontMetrics().size(Qt::TextSingleLine, "Help");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pHelpPb->setFixedSize(size);
	buildHelpMenu();
	pHelpPb->setMenu(pHelpMenu);
	topBox->addWidget(pHelpPb);

	
	// 2.2 synoptic title
	pTitleLabel = new QLabel("Synoptic", this);
	topBox->addWidget(pTitleLabel, 10);	// The only resizable widget on top of dialog
	pTitleLabel->setAlignment(Qt::AlignHCenter);

	// 2.3 Combo box for main part selection
	buildMainPartCombo();
	topBox->addWidget(pCombo);

	// 3 Scroll view where synoptic will lie
	pScroll = new QScrollArea(this);
	mainBox->addWidget(pScroll, 10);	// Scroll view will consume all free space
	connect(pScroll->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(viewMove(int)));

	/* Huge label for scroll testing
	QLabel *dummy = new QLabel("Some dummy content", pScroll->viewport());
	QSize bigSize(10000, 800);
	dummy->setFixedSize(bigSize);
	pScroll->addChild(dummy);
	*/
}

/*
**	FUNCTION
**		Build 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticDialog::buildFileMenu(void)
{
	pFileMenu = new QMenu(this);
	pFileMenu->addAction("Print...", this, SLOT(filePrint()));
	pFileMenu->addSeparator();
	pFileMenu->addAction("Close", this, SLOT(deleteLater()));
}

/*
**	FUNCTION
**		Build 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticDialog::buildViewMenu(void)
{
	// First build mask containing all possible types for synoptic
	allEqpMask.append(FunctionalType::VGM);
	allEqpMask.append(FunctionalType::VGR);
	allEqpMask.append(FunctionalType::VGP);
	allEqpMask.append(FunctionalType::VGI);
	allEqpMask.append(FunctionalType::VGF);
	allEqpMask.append(FunctionalType::VPI);
	allEqpMask.append(FunctionalType::VP_STDIO);
	allEqpMask.append(FunctionalType::VPGF);
	allEqpMask.append(FunctionalType::PROCESS_VPG_6A01);
	allEqpMask.append(FunctionalType::PROCESS_VPG_6E01);
	allEqpMask.append(FunctionalType::PROCESS_BGI_6B01);
	allEqpMask.append(FunctionalType::VPG);
	allEqpMask.append(FunctionalType::VPGM);
	allEqpMask.append(FunctionalType::VGTR);
	allEqpMask.append(FunctionalType::VPT100);
	allEqpMask.append(FunctionalType::VRJ_TC);
	allEqpMask.append(FunctionalType::VRE); //Bakeout rack
	/* Use std. VGR/VGP/VPI
	allEqpMask.append(FunctionalType::VGR_PS_CMW);
	allEqpMask.append(FunctionalType::VGP_PS_CMW);
	allEqpMask.append(FunctionalType::VPI_PS_CMW);
	*/
	allEqpMask.append(FunctionalType::VIES);
	allEqpMask.append(FunctionalType::VPN);
	allEqpMask.append(FunctionalType::VPS);

	pViewMenu = new QMenu(this);
	pEqpSubMenu = pViewMenu->addMenu("Equipment");
	const QList<FunctionalType *> &types = allEqpMask.getList();
	for(int idx = 0 ; idx < types.count() ; idx++) {
		FunctionalType *pItem = types.at(idx);
		// L.Kopylov 11.04.2014 Only 1 menu item for PROCESS_VPG_xxx
		QAction *pAction = NULL;
		switch(pItem->getType()) {
		case FunctionalType::PROCESS_VPG_6A01:
			pAction = pEqpSubMenu->addAction("VPG Process");
			break;
		case FunctionalType::PROCESS_VPG_6E01:
			break;	// No separate items
		default:
			pAction = pEqpSubMenu->addAction(pItem->getDescription());
			break;
		}
		if(pAction) {
			pAction->setCheckable(true);
			pAction->setData(pItem->getType());
			pAction->setChecked(eqpMask.contains(pItem->getType()));
			typeActions.append(pAction);
		}
	}
	pViewMenu->addSeparator();

	// Add Show mobiles not connected
	QAction *pActShowMobs = NULL;
	pActShowMobs = pViewMenu->addAction("Show mobiles not connected", this, SLOT(showMobilesNC()));
	
	// Add Hide mobiles not connected
	QAction *pActHideMobs = NULL;
	pActHideMobs = pViewMenu->addAction("Hide mobiles not connected", this, SLOT(hideMobilesNC()));

	// Other actions
	pViewMenu->addSeparator();
	pViewMenu->addAction("Sector...", this, SLOT(viewSector()));
	pViewMenu->addAction("Pressure Profile...", this, SLOT(viewProfile()));
	
	// Finally connect eqpType filter action
	connect(pViewMenu, SIGNAL(triggered(QAction *)), this, SLOT(eqpTypeMenu(QAction *)));
}

/*
**	FUNCTION
**		Build 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticDialog::buildHelpMenu(void)
{
	pHelpMenu = new QMenu(this);
	pHelpMenu->addAction("User manual...", this, SLOT(help()));
}

/*
**	FUNCTION
**		Build main part combo box, insert main part names which can be selected
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticDialog::buildMainPartCombo(void)
{
	pCombo = new QComboBox(this);
	pCombo->setEditable(false);
	pCombo->clear();
	DataPool &pool = DataPool::getInstance();
	QStringList strList;
	pool.findMainPartNames(strList, true);
	QList<Sector *> &sectors = pool.getSectors();
	foreach(QString mpName, strList)
	{
		MainPart *pMainPart = pool.findMainPartData(mpName.toLatin1());
		unsigned vacType = pMainPart->getVacTypeMask();
		if(!VacType::isSingleBeam(vacType))
		{
			continue;
		}
		bool skip = false;
		/* L.Kopylov 25.05.2012
		for(int idx = 0 ; idx < sectors.count() ; idx++)
		{
			Sector *pSector = sectors.at(idx);
			if(!pSector->isSpecSynoptic())
			{
				continue;
			}
			if(pSector->isOuter())
			{
				continue;
			}
			if(pSector->isInMainPart(pMainPart))
			{
				skip = true;
				break;
			}
		}
		*/
		if(!skip)
		{
			if(pool.areSectorsOfMainPartContinuous(pMainPart))
			{
				pCombo->addItem(mpName);
			}
			else
			{
				printf("skip %s: sectors are not continuous\n", pMainPart->getName());
				fflush(stdout);
				for(int idx = 0 ; idx < sectors.count() ; idx++)
				{
					Sector *pSector = sectors.at(idx);
					if(!pSector->isInMainPart(pMainPart))
					{
						continue;
					}
					if(!pSector->getSpecSynPanel().isEmpty())
					{
printf("Add special %s\n", mpName.toLatin1().constData());
fflush(stdout);
						pCombo->addItem(mpName);
						break;
					}
				}
			}
		}
	}
	selectInitialMp();
	connect(pCombo, SIGNAL(activated(const QString &)),
		this, SLOT(comboActivated(const QString &)));
}

/*
**	FUNCTION
**		Build initial equipment type mask
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticDialog::buildInitialEqpMask(void)
{
	// Put to eqpMask default subset for this instance
	// TODO: get from resource file
	eqpMask = allEqpMask;
	ResourcePool &pool = ResourcePool::getInstance();
	QString resourceName = RESOURCE_BASE;
	resourceName += ".";
	resourceName += pCombo->currentText();
	if(pool.getEqpTypesMask(resourceName, eqpMask) == ResourcePool::NotFound)
	{
		resourceName = RESOURCE_BASE;
		eqpMask = allEqpMask;
		if(pool.getEqpTypesMask(resourceName, eqpMask) == ResourcePool::NotFound)
		{
			eqpMask = allEqpMask;
		}
	}

	// Set items checked in menu by default
	const QList<FunctionalType *> &types = allEqpMask.getList();
	for(int idx = 0 ; idx < types.count() ; idx++)
	{
		FunctionalType *pItem = types.at(idx);
		for(int actIdx = 0 ; actIdx < typeActions.count() ; actIdx++)
		{
			QAction *pAction = typeActions.at(actIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				bool contains = eqpMask.contains(pItem->getType());
				pAction->setChecked(contains);
				// Add all VPG processes if one is selected
				if(contains)
				{
					switch(pItem->getType())
					{
					case FunctionalType::PROCESS_VPG_6A01:
						eqpMask.append(FunctionalType::PROCESS_VPG_6E01);
						break;
					}
				}
				break;
			}
		}
	}
	// Add experimental area to beam vacuum equipment mask
	eqpMask.append(FunctionalType::EXP_AREA);
}


/*
**	FUNCTION
**		Set initial selection in main part combo box
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void SynopticDialog::selectInitialMp(void)
{
	DataPool &pool = DataPool::getInstance();
	int nMps = pCombo->count();
	int *mps = (int *)calloc(nMps, sizeof(int));
	Sector *pSector = pFirstSector;
	do
	{
		for(int n = 0 ; n < nMps ; n++)
		{
			MainPart *pMainPart = pool.findMainPartData(pCombo->itemText(n).toLatin1());
			if(pSector->isInMainPart(pMainPart))
			{
				mps[n]++;
			}
		}
		if(pSector == pLastSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);

	int bestIdx = -1, bestWeight = 0;
	for(int n = 0 ; n < nMps ; n++)
	{
		if(mps[n] > bestWeight)
		{
			bestWeight = mps[n];
			bestIdx = n;
		}
	}
	free((void *)mps);
	pCombo->setCurrentIndex(bestIdx);
}

/*
**	FUNCTION
**		Set text and color for title label widget
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticDialog::setTitle(void)
{
	if(mode == DataEnum::Replay)
	{
		QPalette palette = pTitleLabel->palette();
		palette.setColor(QPalette::Window, Qt::cyan);
		pTitleLabel->setPalette(palette);
		pTitleLabel->setAutoFillBackground(true);
	}
	QString title = "Synoptic, sector";
	if(pFirstSector == pLastSector)
	{
		title += " ";
		title += pFirstSector->getName();
	}
	else
	{
		title += "s ";
		title += pFirstSector->getName();
		title += " ... ";
		title += pLastSector->getName();
	}
	pTitleLabel->setText(title);
}

/*
**	FUNCTION
**		Build synoptic view according to current selection
**
**	ARGUMENTS
**		init	- Flag indicating if method is called from constructor
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticDialog::buildView(bool init)
{
	if(pView)
	{
		delete pView;
		pView = NULL;
	}
	// Create instance of synoptic view
	if(pFirstSector->getSpecSynPanel().isEmpty())
	{
		pView = SynView::create(pScroll->viewport(), pFirstSector, pLastSector, mode);
	}
	else
	{
		pView = SynViewXml::create(pScroll->viewport(), pFirstSector->getSpecSynPanel(), mode);
	}

	// Analyze equipment types in view, set menu accordingly
	unsigned vacMask;
	VacEqpTypeMask cryoMask, beamMask;
	pView->analyzeTypes(vacMask, cryoMask, beamMask);
	const QList<FunctionalType *> &types = allEqpMask.getList();
	for(int idx = 0 ; idx < types.count() ; idx++)
	{
		FunctionalType *pItem = types.at(idx);
		for(int actIdx = 0 ; actIdx < typeActions.count() ; actIdx++)
		{
			QAction *pAction = typeActions.at(actIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				pAction->setEnabled(beamMask.contains(pItem->getType()));
				if(!beamMask.contains(pItem->getType()))
				{
					pAction->setChecked(false);
				}
				break;
			}
		}
	}

	// Build content of synoptic view with current selection mask
	pView->applyMask(eqpMask);

	// Add view to scroller
	pView->show();
	pScroll->setWidget(pView);

	setMyWidth(init);
	pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
}

void SynopticDialog::ensureDeviceVisible(Eqp *pEqp)
{
	if(!pView)
	{
		return;
	}
	int position;
	bool visible;
	if(!pView->findDevicePosition(pEqp, position, visible))
	{
		return;	// No such device in view
	}
	if(!visible)	// Make it visible - device type menus, vacuum menu already must correspond to device
	{
		int	funcType = pEqp->getFunctionalType();
		// Decision on where to apply mask is made on vacuum type for synoptic, not for device: some CRYO_TT are on both beam and cryo vacuums
		if(eqpMask.contains(pEqp->getFunctionalType()))
		{
			qDebug("SynopticDialog::ensureDeviceVisible(%s): must be visible, but it is not\n", pEqp->getDpName());
			return;
		}
		//qDebug("SynopticDialog::ensureDeviceVisible(%s): eqpMask before %s\n", pEqp->getDpName(), eqpMask.toString().toLatin1().constData());
		if(!allEqpMask.contains(funcType))
		{
			return;
		}
		for(int typeIdx = 0 ; typeIdx < typeActions.count() ; typeIdx++)
		{
			QAction *pAction = typeActions.at(typeIdx);
			if(pAction->data().toInt() == funcType)
			{
				pAction->setChecked(true);
				eqpMask.append(funcType);
				break;
			}
		}
		//qDebug("SynopticLhcDialog::ensureDeviceVisible(%s): cryoEqpMask after %s\n", pEqp->getDpName(), cryoEqpMask.toString().toLatin1().constData());
		//qDebug("SynopticLhcDialog::ensureDeviceVisible(%s): beamEqpMask after %s\n", pEqp->getDpName(), beamEqpMask.toString().toLatin1().constData());
		pView->applyMask(eqpMask);
		setMyWidth(false);
		pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
	}
	if(!pView->findDevicePosition(pEqp, position, visible))
	{
		qDebug("SynopticDialog::ensureDeviceVisible(%s): not added to view after mask change\n", pEqp->getDpName());
		return;	// No such device in view
	}
	if(!visible)
	{
		qDebug("SynopticDialog::ensureDeviceVisible(%s): not visible after mask change\n", pEqp->getDpName());
		return;	// Still not visible
	}
	if((pScroll->horizontalScrollBar()->value() < position) && (position < (pScroll->horizontalScrollBar()->value() + pScroll->width())))
	{
		return;	// Must be visible already
	}
	int value = position - pScroll->width() / 2;
	if(value < 0)
	{
		value = 0;
	}
	pScroll->horizontalScrollBar()->setValue(value);
}

/*
**	FUNCTION
**		Set width of this dialog to best fit to synoptic content
**
**	ARGUMENTS
**		init	- true if method is called during initialization
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticDialog::setMyWidth(bool init)
{
	if(!pView)
	{
		return;
	}
	// Set own height according to size of synoptic view
	int scrollHeight;
	if(init)
	{
		scrollHeight = pView->minimumHeight() + pScroll->horizontalScrollBar()->height() / 2;
		scrollHeight += 2;
		pScroll->setMinimumHeight(scrollHeight);
	}
	else
	{
		int minHeight = pView->minimumHeight();
		int realHeight = pScroll->viewport()->height();
		scrollHeight = pView->minimumHeight() + pScroll->horizontalScrollBar()->height();
		scrollHeight += 2;
		pScroll->setMinimumHeight(scrollHeight);
		// resize(width(), height() + minHeight - realHeight);
		setMinimumHeight(height() + minHeight - realHeight);
	}

	// Adjust own width
	int viewWidth = pView->minimumWidth();
	QDesktopWidget *pDesktop = QApplication::desktop();
	const QRect &screen = pDesktop->screenGeometry(pDesktop->screenNumber(this));
	// LIK 26.04.2009 if(init)
	{
		if(viewWidth > screen.width())
		{
			resize(screen.width() - 50, scrollHeight + pCombo->height());
		}
		else
		{
			resize(viewWidth + 10, scrollHeight + pCombo->height());
		}
	}
}

/*
**	FUNCTION
**		Slot activated when 'Print' item is selected in 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticDialog::filePrint(void)
{
	VacMainView::printWidget(this);
}

/*
**	FUNCTION
**		Slot activated when 'Sector' item is selected in 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticDialog::viewSector(void)
{
	makeDialogRequest(VacMainView::DialogSector);
}

/*
**	FUNCTION
**		Slot activated when 'Profile' item is selected in 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticDialog::viewProfile(void)
{
	makeDialogRequest(VacMainView::DialogProfile);
}

/*
**	FUNCTION
**		Make request to main view to show another dialog for data
**		shown in this dialog
**
**	ARGUMENTS
**		type	- Type of dialog to be opened, see enum in VacMainView class
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticDialog::makeDialogRequest(int type)
{
	QStringList sectorList;
	unsigned vacType = 0;
	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pFirstSector;
	do
	{
		vacType |= pSector->getVacType();
		sectorList.append(pSector->getName());
		if(pSector == pLastSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);
	VacMainView *pMainView = VacMainView::getInstance();
	pMainView->dialogRequest(type, vacType, sectorList, mode);
}


/*
**	FUNCTION
**		Slot activated when 'User manual' item is selected in 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticDialog::help(void)
{
}

/*
**	FUNCTION
**		Slot activated when one of equipment type items is selected
**		in 'Equipment' submenu of 'View' menu
**
**	ARGUMENTS
**		id	- Functional type corresponding to selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticDialog::eqpTypeMenu(QAction *pAction)
{
	if(!pAction->data().isValid())
	{
		return;
	}
	int id = pAction->data().toInt();
	if(eqpMask.contains(id))
	{
		eqpMask.remove(id);
		// L.Kopylov 11.04.2014 1 menu item switches all VPG processes
		if(id == FunctionalType::PROCESS_VPG_6A01)
		{
			eqpMask.remove(FunctionalType::PROCESS_VPG_6E01);
		}
		pAction->setChecked(false);
	}
	else
	{
		eqpMask.append(id);
		// L.Kopylov 11.04.2014 1 menu item switches all VPG processes
		if(id == FunctionalType::PROCESS_VPG_6A01)
		{
			eqpMask.append(FunctionalType::PROCESS_VPG_6E01);
		}
		pAction->setChecked(true);
	}
	/* int start = */ pView->applyMask(eqpMask);
	setMyWidth(false);
	/* TODO: the idea of calculating recommended start position does not work as
		expected: it looks like maximum of scroll bar will be set later, so
		setting value here does no make sense
	pScroll->horizontalScrollBar()->setValue(start);
	pView->setViewStart(start, pScroll->width(), false);
	*/
	pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
}

/*
**	FUNCTION
**		Slot activated when item is selected in main part combo box
**
**	ARGUMENTS
**		name	- name of selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticDialog::comboActivated(const QString &name)
{
	DataPool &pool = DataPool::getInstance();
	QStringList sectList;
	pool.findSectorsInMainPart(name.toLatin1(), sectList, true);
	if(!sectList.count())
	{
		bool specPanel = false;
		pool.findSectorsInMainPart(name.toLatin1(), sectList, false);
		for(int idx = 0 ; idx < sectList.count() ; idx++)
		{
			QString sectName = sectList.at(idx);
			Sector *pSector = pool.findSectorData(sectName.toLatin1());
			if(pSector)
			{
				if(!pSector->getSpecSynPanel().isEmpty())
				{
					specPanel = true;
					break;
				}
			}
		}
		if(!specPanel)
		{
			selectInitialMp();
			return;
		}
	}
	if (sectList.isEmpty()) {
		selectInitialMp();
		return;
	}
	Sector *pNewStart = pool.findSectorData(sectList.first().toLatin1()),
			*pNewEnd = pool.findSectorData(sectList.last().toLatin1());
	if((!pNewStart) || (!pNewEnd))
	{
		selectInitialMp();
		return;
	}
	pFirstSector = pNewStart;
	pLastSector = pNewEnd;
	buildView(false);
	setTitle();
}

/*
**	FUNCTION
**		Slot activated when scrollbar(s) are used to scroll synoptic view
**
**	ARGUMENTS
**		x	- New X-coordinate of visible part
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynopticDialog::viewMove(int x)
{
	if(pView)
	{
		pView->setViewStart(x, pScroll->width(),
			pScroll->horizontalScrollBar()->isSliderDown());
	}
}

void SynopticDialog::resizeEvent(QResizeEvent * /*pEvent */)
{
	if(pView)
	{
		pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
	}
}

/**
@brief Connect mobile active change to rebuilt in case it changes
*/
void SynopticDialog::ConnectEqpActiveChange(void) {
	
	QList<SynLine *> listLines = pView->getLines();
	
	for (int i = 0; i<listLines.count(); ++i) {
		QList<Eqp *> Eqps = listLines[i]->getListEqpsActiveMayChange();
		for (int n = 0; n < Eqps.count(); ++n) {
			Eqp * pE = Eqps[n];
			connect(pE, SIGNAL(mobileConnectionChanged(Eqp *, DataEnum::DataMode)),
				this, SLOT(eqpActiveChange(Eqp *, DataEnum::DataMode)));
		}
	}
}

/**
@brief Connect epp position change to rebuilt in case it changes
*/
void SynopticDialog::ConnectEqpPositionChange(void) {

	QList<SynLine *> listLines = pView->getLines();

	for (int i = 0; i<listLines.count(); ++i) {
		QList<Eqp *> Eqps = listLines[i]->getListEqpsPositionMayChange();
		for (int n = 0; n < Eqps.count(); ++n) {
			Eqp * pE = Eqps[n];
			connect(pE, SIGNAL(positionChanged(Eqp *, DataEnum::DataMode)),
				this, SLOT(eqpActiveChange(Eqp *, DataEnum::DataMode)));
		}
	}
}

/**
@brief SLOT Connect mobile active change to rebuilt in case it changes
*/
void SynopticDialog::eqpActiveChange(Eqp * pEqp, DataEnum::DataMode mode) {
	if (pEqp) {
		if (mode != DataEnum::Replay) {
			buildView(false);
		}
	}
}

/**
@brief SLOT Show mobiles not connected
*/
void SynopticDialog::showMobilesNC(void) {
	if (pView) {
		pView->showMobilesNotConnected();
		// Force redraw of the view with the current mask
		pView->applyMask(eqpMask); 
		setMyWidth(false);
		pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
	}
	
}

/**
@brief SLOT Hide mobiles not connected
*/
void SynopticDialog::hideMobilesNC(void) {
	if (pView) {
		pView->hideMobilesNotConnected();
		// Force redraw of the view with the current mask
		pView->applyMask(eqpMask);
		setMyWidth(false);
		pView->setViewStart(pScroll->horizontalScrollBar()->value(), pScroll->width(), false);
	}
}

































