//	Implementation of SynLhcLine class
/////////////////////////////////////////////////////////////////////////////////

#include "SynLhcLine.h"
#include "SynLhcLineBeam.h"
#include "VacLinePart.h"
#include "SynopticLhcDialog.h"

#include "DataPool.h"
#include "Eqp.h"
#include "EqpVP_GU.h"
#include "EqpType.h"
#include "EqpMsgCriteria.h"

#include "VacIcon.h"
#include "VacMainView.h"
#include "DebugCtl.h"

#include <QPainter>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

SynLhcLine::SynLhcLine(QWidget *parent, int type, float start, float end)
	: QObject(), LhcLineParts(start, end)
{
	this->parent = parent;
	this->type = type;
	decideForEqpLocation();
	vacTypeMask = 0;
	doNotAddEqp = false;
	hideMagnets = false;
	redrawRequiredForMobile = false;
	mobileListReady = false;
	alwaysIncludeSectorBorder = true;
	isShowMobiles = false;
	clear();
}

SynLhcLine::~SynLhcLine()
{
	clear();
	foreach(Eqp *pEqp, mobileList)
	{
		disconnect(pEqp, SIGNAL(mobileStateChanged(Eqp *, DataEnum::DataMode)),
			this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
	}
}

/*
** FUNCTION
**		Clear all content of line
**
** PARAMETERS
**		None
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void SynLhcLine::clear(void)
{
	LhcLineParts::clear();
	while(!sectors.isEmpty())
	{
		delete sectors.takeFirst();
	}
	while(!eqps.isEmpty())
	{
		delete eqps.takeFirst();
	}
	maxSectHeight = 0;
	startX = endX = 0;
	y = ascent = descent = 0;
	minPassiveLength = DataPool::getInstance().getLhcRingLength();
	maxPassiveLength = 0.0;
	haveInterlockSources = false;
	// clear list for mobile
	listEqpsActiveMayChange.clear();
	// clear list for Relay hardware arrow or other eqp position may change
	listEqpsPositionMayChange.clear();
}

/*
**	FUNCTION
**		Connect to activity signals of all mobile equipment located on this line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::connectToMobile(void)
{
	if(mobileListReady)
	{
		return;
	}
	mobileListReady = true;

	// Find LHC circular line
	BeamLine	*pLine = DataPool::getInstance().getCircularLine();
	if(!pLine)
	{
		return;
	}

	// LHC ring consists of one part only
	const QList<BeamLinePart *> &lineParts = pLine->getParts();
	if (lineParts.isEmpty()) {
		return;
	}
	BeamLinePart *pPart = lineParts.first();
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	int eqpIdx = 0;

	if(!pPartList->count())
	{
		buildLineParts();
	}

	// Find first device to add - only coordinate is considered for decision
	for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if((pEqp->getStart() + pEqp->getLength()) >= start)
		{
			break;
		}
	}

	// Analyze first portion of devices
	for( ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(start < end)
		{
			if(pEqp->getStart() > end)
			{
				break;
			}
		}
		if(pEqp->getStart() < start)
		{
			continue;
		}
		if(isMyMobile(pEqp))
		{
			connect(pEqp, SIGNAL(mobileStateChanged(Eqp *, DataEnum::DataMode)),
				this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
			mobileList.append(pEqp);
		}
	}
	if(start < end)
	{
		return;
	}

	// Analyze equipment after IP1
	for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(pEqp->getStart() > end)
		{
			break;
		}
		if(isMyMobile(pEqp))
		{
			connect(pEqp, SIGNAL(mobileStateChanged(Eqp *, DataEnum::DataMode)),
				this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
			mobileList.append(pEqp);
		}
	}
}
/*
**	@brief PUBLIC function Connect to activity signals of all wireless mobile equipment located on this line also to connect eqp that position may change such as VA_R
*/
void SynLhcLine::connectToWirelessMobile(void)
{
	// Find LHC circular line
	BeamLine	*pLine = DataPool::getInstance().getCircularLine();
	if (!pLine)	{
		return;
	}
	// LHC ring consists of one part only
	const QList<BeamLinePart *> &lineParts = pLine->getParts();
	if (lineParts.isEmpty()) {
		return;
	}
	BeamLinePart *pPart = lineParts.first();
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	int eqpIdx = 0;
	if (!pPartList->count()) {
		buildLineParts();
	}
	// Find first device to add - only coordinate is considered for decision
	for (eqpIdx = 0; eqpIdx < nEqpPtrs; eqpIdx++) {
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if ((pEqp->getStart() + pEqp->getLength()) >= start) {
			break;
		}
	}
	// Analyze first portion of devices
	for (; eqpIdx < nEqpPtrs; eqpIdx++) {
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if (start < end) {
			if (pEqp->getStart() > end) {
				break;
			}
		}
		if (pEqp->getStart() < start) {
			continue;
		}
		// For wireless mobile
		if (pEqp->isActiveMayChange()) {
			listEqpsActiveMayChange.append(pEqp);
			connect(pEqp, SIGNAL(mobileConnectionChanged(Eqp *, DataEnum::DataMode)),
				this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
		}
		// For Alarm relay arrows VA_R
		if (pEqp->isPositionMayChange()) {
			listEqpsPositionMayChange.append(pEqp);
			connect(pEqp, SIGNAL(positionChanged(Eqp *, DataEnum::DataMode)),
				this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode))); // use the redraw of wireless mobile for VA_R
		}
	}
	if (start < end) {
		return;
	}
	// Analyze equipment after IP1
	for (eqpIdx = 0; eqpIdx < nEqpPtrs; eqpIdx++) {
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if (pEqp->getStart() > end) {
			break;
		}
		// For wireless mobile
		if (pEqp->isActiveMayChange()) {
			listEqpsActiveMayChange.append(pEqp);
			connect(pEqp, SIGNAL(mobileConnectionChanged(Eqp *, DataEnum::DataMode)),
				this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
		}
	}
}
/*
**	FUNCTION
**		Check if change of activity for given (mobile) device is of interest
**		for this line
**
**	PARAMETERS
**		pEqp	- Pointer to device to be checked
**
**	RETURNS
**		true	- If device activity if of interest for this line;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SynLhcLine::isMyMobile(Eqp *pEqp)
{
	if(pEqp->getFunctionalType() != FunctionalType::VPGM)
	{
		return false;
	}
	int vacType = pEqp->getVacType();
	switch(type)
	{
	case Qrl:
		return vacType == VacType::Qrl;
	case Cryo:
		return vacType == VacType::Cryo;
	case CrossBeam:
		return vacType == VacType::CrossBeam;
	case OuterBeam:
	case InnerBeam:
		{
			int partIdx = findPartIdx(pEqp);
			if((partIdx >= 0) && (partIdx < pPartList->count()))
			{
				return pPartList->at(partIdx)->getVacType() == vacType;
			}
			else
			{
				// printf("type %d: partIdx %d for %s\n", type, partIdx, pEqp->getName());
			}
		}
		break;
	default:
		break;
	}
	return false;
}

/*
**	FUNCTION
**		Slot activated for activity of mobile device has been changed
**
**	PARAMETERS
**		pSrc	- Pointer to device - source of signal
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::mobileStateChange(Eqp *pSrc, DataEnum::DataMode mode)
{
	if(mode != this->mode)
	{
		return;
	}
	redrawRequiredForMobile = true;
	int vacType = pSrc->getVacType();
	switch(vacType)
	{
	case VacType::Qrl:
	case VacType::Cryo:
		if(!(vacType & vacTypeMask))
		{
			redrawRequiredForMobile = false;
		}
		break;
	default:
		if(!(vacTypeMask & (VacType::RedBeam | VacType::BlueBeam)))
		{
			redrawRequiredForMobile = false;
		}
		break;
	}
	if(!eqpMask.contains(pSrc->getFunctionalType()))
	{
		redrawRequiredForMobile = false;
	}
	emit mobileStateChanged();
}

/*
**	FUNCTION
**		Decide for equipment location relative to vacuum line (above/below)
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::decideForEqpLocation(void)
{
	switch(type)
	{
	case Qrl:
	case OuterBeam:
	case CommonBeam:
		eqpAboveLine = true;
		break;
	case Cryo:
	case Pas:
	case CrossBeam:
	case InnerBeam:
		eqpAboveLine = false;
		break;
	}
}

/*
**	FUNCTION
**		Check if content of line shall be rebuilt
**
**	PARAMETERS
**		vacTypeMask	- Bit mask for vacuum type selection
**		eqpMask		- Mask for equipment selection
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
bool SynLhcLine::isRebuildRequired(unsigned vacTypeMask, VacEqpTypeMask &eqpMask)
{
	return (vacTypeMask != this->vacTypeMask) || (!eqpMask.equals(this->eqpMask)) || redrawRequiredForMobile;
}

/*
**	FUNCTION
**		Find name of item under mouse pointer.
**
**	PARAMETERS
**		mouseX		- X coordinate of mouse pointer location
**		mouseY		- Y coordinate of mouse pointer location
**
**	RETURNS
**		Name of equipment whose icon is under mouse pointer,
**		NULL if no device under pointer.
**
**	CAUTIONS
**		None
*/
const char *SynLhcLine::closestItem(int mouseX, int mouseY)
{
	const char	*result = NULL;

	if(mouseY < (y - ascent))
	{
		return NULL;
	}
	if(mouseY > (y + descent))
	{
		return NULL;
	}
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		QRect rect(pItem->getRect());
		if(rect.left() > mouseX)
		{
			continue;	// May be break ???? LIK 11.02.2005
		}
		if((rect.right()) < mouseX)
		{
			continue;
		}
		if(rect.top() > mouseY)
		{
			continue;
		}
		if(rect.bottom() < mouseY)
		{
			continue;
		}
		result = pItem->getName();
		break;
	}
	return result;
}

bool SynLhcLine::findDevicePosition(Eqp *pEqpToFind, int &eqpPosition, bool &eqpVisible)
{
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		if(pItem->getEqp() == pEqpToFind)
		{
			eqpPosition = pItem->getRect().x();
			eqpVisible = true;
			return true;
		}
	}

	// Not found... equipment may be hidden by equipment type mask
	eqpVisible = false;
	eqpPosition = 0;

	// Find LHC circular line
	BeamLine	*pLine = DataPool::getInstance().getCircularLine();
	if(!pLine)
	{
		return false;
	}

	// LHC ring consists of one part only
	if (pLine->getParts().isEmpty()) {
		return false;
	}
	BeamLinePart *pPart = pLine->getParts().first();
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	int eqpIdx = 0;

	// Find first device to chec - only coordinate is considered for decision
	for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if((pEqp->getStart() + pEqp->getLength()) >= start)
		{
			break;
		}
	}

	// Save old equipment mask
	VacEqpTypeMask oldEqpMask = eqpMask;
	// Set new masks which include all types
	eqpMask = VacEqpTypeMask::getAllTypes();

	bool result = false;
	int partIdx = 0;

	// Analyze first portion of devices
	for( ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(start < end)
		{
			if(pEqp->getStart() > end)
			{
				break;
			}
		}
		if(pEqp == pEqpToFind)
		{
			if(pEqp->getStart() < start)
			{
				eqpMask = oldEqpMask;
				return false;
			}
			result = isMyEqp(pEqp, partIdx);
			eqpMask = oldEqpMask;
			return result;
		}
	}
	if(!includesIP1)
	{
		eqpMask = oldEqpMask;
		return false;
	}

	// Check equipment after IP1
	for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(pEqp->getStart() > end)
		{
			break;
		}
		if(pEqp == pEqpToFind)
		{
			result = isMyEqp(pEqp, partIdx);
			eqpMask = oldEqpMask;
			return result;
		}
	}
	eqpMask = oldEqpMask;
	return false;
}

/*
**	FUNCTION
**		Build all equipment to be shown on this line
**
**	PARAMETERS
**		vacTypeMask	- Bit mask for vacuum type selection
**		eqpMask		- Bit mask for equipment selection
**
** RETURNS
**		Number of devices to be shown in this line
**
** CAUTIONS
**		None
*/
int SynLhcLine::build(unsigned vacTypeMask, VacEqpTypeMask &eqpMask)
{
	clear();
	redrawRequiredForMobile = false;
	this->vacTypeMask = vacTypeMask;
	this->eqpMask = eqpMask;
	buildLineParts();
	if(!pPartList->count())
	{
		return 0;
	}
	buildEquipment();
	buildEqpGroups();

	// L.Kopylov 28.01.2010 addition to mark interlok sources
	markInterlockSources();
	return eqps.count();
}

/*
**	FUNCTION
**		Analyze which equipment types will be shown in view
**
**	PARAMETERS
**		allVacTypeMask	- OR'ed bit mask for all vacuum types
**		allEqpMask		- Type mask for all equipment to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allEqpMask)
{
	// Find LHC circular line
	BeamLine	*pLine = DataPool::getInstance().getCircularLine();
	if(!pLine)
	{
		return;
	}

	// LHC ring consists of one part only
	if (pLine->getParts().isEmpty()) {
		return;
	}
	BeamLinePart *pPart = pLine->getParts().first();
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	int eqpIdx = 0;

	if(!pPartList->count())
	{
		buildLineParts();
	}

	// Save old masks
	unsigned oldVacTypeMask = vacTypeMask;
	VacEqpTypeMask oldEqpMask = eqpMask;

	// Set new masks which include all types
	vacTypeMask = 0xFFFFFFFF;
	eqpMask = VacEqpTypeMask::getAllTypes();
	doNotAddEqp = true;

	// Find first device to add - only coordinate is considered for decision
	for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if((pEqp->getStart() + pEqp->getLength()) >= start)
		{
			break;
		}
	}

	int partIdx = 0;

	// Analyze first portion of devices
	for( ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(start < end)
		{
			if(pEqp->getStart() > end)
			{
				break;
			}
		}
		if(pEqp->getStart() < start)
		{
			continue;
		}
		if(isMyEqp(pEqp, partIdx))
		{
			allVacTypeMask |= pEqp->getVacType();
			allEqpMask.append(pEqp->getFunctionalType());
		}

	}
	if(start < end)
	{
		vacTypeMask = oldVacTypeMask;
		eqpMask = oldEqpMask;
		doNotAddEqp = false;
		return;
	}

	// Analyze equipment after IP1
	for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(pEqp->getStart() > end)
		{
			break;
		}
		if(isMyEqp(pEqp, partIdx))
		{
			allVacTypeMask |= pEqp->getVacType();
			allEqpMask.append(pEqp->getFunctionalType());
		}
	}
	vacTypeMask = oldVacTypeMask;
	eqpMask = oldEqpMask;
	doNotAddEqp = false;
}


/*
**	FUNCTION
**		Build list of equipment for this line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::buildEquipment(void)
{
	int	partIdx = 0;

	// Find LHC circular line
	BeamLine	*pLine = DataPool::getInstance().getCircularLine();
	if(!pLine)
	{
		return;
	}

	// LHC ring consists of one part only
	if (pLine->getParts().isEmpty()) {
		return;
	}
	BeamLinePart *pPart = pLine->getParts().first();
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	int eqpIdx = 0;

	// Find first device to add - only coordinate is considered for decision
	for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if((pEqp->getStart() + pEqp->getLength()) >= start)
		{
			break;
		}
	}

	// Add first portion of devices
	for( ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(start < end)
		{
			if(pEqp->getStart() > end)
			{
				break;
			}
		}
		if(pEqp->getStart() < start)
		{
			continue;
		}
		if(isMyEqp(pEqp, partIdx))
		{
			addEqp(pEqp, partIdx);
		}
	}
	if(!includesIP1)
	{
		return;
	}

	// Add equipment after IP1
	for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		if(pEqp->getStart() > end)
		{
			break;
		}
		if(isMyEqp(pEqp, partIdx))
		{
			addEqp(pEqp, partIdx);
		}
	}
}

/*
**	FUNCTION
**		Join controllable equipment to groups. Note that line
**		where this method is called only contains controllable
**		equipment.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::buildEqpGroups()
{
	int				groupId = 1;
	VacEqpTypeMask	dummyMask;

	/* LIK 02.06.2009
	for(SynLhcEqpItem *pItem = iter.toFirst() ; pItem ; pItem = ++iter)
	{
		QPoint &connPoint = pItem->getConnPoint();
		if(pItem->getIcon())
		{
			connPoint.setY(pItem->getIcon()->getGeometry(dummyMask).getY());
		}
	}
	*/
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		// CRYO thermometers do not participate in groups
		if(pItem->isCryoThermometer())
		{
			continue;
		}
		if(pItem->getVerticalPos() && (!pItem->getGroupId()))
		{
			bool	found = false;
			for(int nextIdx = idx + 1 ; nextIdx < eqps.count() ; nextIdx++)
			{
				SynLhcEqpItem *pNext = eqps.at(nextIdx);
				// CRYO thermometers do not participate in groups
				if(pNext->isCryoThermometer())
				{
					continue;
				}
				if(pNext->getStart() > pItem->getStart())
				{
					break;
				}
				if((pNext->getStart() == pItem->getStart()) &&
					(pNext->getVerticalPos() == pItem->getVerticalPos()))
				{
					found = true;
					pNext->setGroupId(groupId);
					QRect &rect = pNext->getRect();
					QPoint &connPoint = pNext->getConnPoint();
					if(eqpAboveLine)
					{
						rect.moveTop(rect.top() - COMMON_ACTIVE_OFFSET);
						connPoint.setY(connPoint.y() - COMMON_ACTIVE_OFFSET);
					}
					else
					{
						rect.moveTop(rect.top() + COMMON_ACTIVE_OFFSET);
						connPoint.setY(connPoint.y() + COMMON_ACTIVE_OFFSET);
					}
				}
			}
			if(found)
			{
				pItem->setGroupId(groupId++);
				QRect &rect = pItem->getRect();
				QPoint &connPoint = pItem->getConnPoint();
				if(eqpAboveLine)
				{
					rect.moveTop(rect.top() - COMMON_ACTIVE_OFFSET);
					connPoint.setY(connPoint.y() - COMMON_ACTIVE_OFFSET);
				}
				else
				{
					rect.moveTop(rect.top() + COMMON_ACTIVE_OFFSET);
					connPoint.setY(connPoint.y() + COMMON_ACTIVE_OFFSET);
				}
			}
		}
	}
}

/*
** FUNCTION
**		Mark devices which are sources of interlock for valves.
**
** PARAMETERS
**		None
**
** RETURNS
**		None
**
** CAUTIONS
**		Only valves which are in this view are considered
*/
void SynLhcLine::markInterlockSources(void)
{
	haveInterlockSources = false;

	// L.Kopylov 17.01.2014 DataPool &pool = DataPool::getInstance();

	int eqpIdx = 0;
	for(eqpIdx = 0 ; eqpIdx < eqps.count() ; eqpIdx++)
	{
		SynLhcEqpItem *pItem = eqps.at(eqpIdx);
		if(pItem->getType() != EqpType::VacDevice)
		{
			continue;
		}
		Eqp *pEqp = pItem->getEqp();
		if(pEqp->isIntlSourcePrev())
		{
			pItem->setIntlSourceForward(true);
			haveInterlockSources = true;
		}
		if(pEqp->isIntlSourceNext())
		{
			pItem->setIntlSourceBackward(true);
			haveInterlockSources = true;
		}
	}
}

/*
**	FUNCTION
**		Add device to list of devices to be shown in this line
**
**	PARAMETERS
**		pEqp	- Pointer to device to be added
**		partIdx	- Index of line part where device is located
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::addEqp(Eqp *pEqp, int partIdx)
{
	SynLhcEqpItem	*pItem = new SynLhcEqpItem(pEqp);
	eqps.append(pItem);

	// Correct start position for consecutive icons placement
	VacLinePart *pPart = pPartList->at(partIdx);
	float lhcRingLength = DataPool::getInstance().getLhcRingLength();
	if((pPart->getStartPos() > pPart->getEndPos()) || (pPart->getEndPos() > lhcRingLength))
	{
		if(pEqp->getStart() < pPart->getStartPos())
		{
			pItem->setStart(pEqp->getStart() + lhcRingLength);
		}
	}
	else if(pPart->isAfterIP1())
	{
		pItem->setStart(pEqp->getStart() + lhcRingLength);
	}
	QColor color;
	switch(pItem->getType())
	{
	case EqpType::VacDevice:	// Get sizes of icon
		pItem->initIcon(parent, isBeamVacuum(), eqpMask, eqpAboveLine);
		setLineColor(pEqp->getVacType(), color);
		pItem->setPipeColor(color);
		pItem->setMode(mode);
		break;
	case EqpType::Passive:	// Passive elements
		pItem->getRect().setWidth(PASSIVE_ELEM_WIDTH);
		if(pEqp->getLength() < minPassiveLength)
		{
			minPassiveLength = pEqp->getLength();
		}
		if(pEqp->getLength() > maxPassiveLength)
		{
			maxPassiveLength = pEqp->getLength();
		}
		break;
	}
}

/*
**	FUNCTION
**		Find index of line part where equipment is located
**
**	PARAMETERS
**		pEqp	- Pointer to device
**
**	RETURNS
**		Index of line part
**
**	CAUTIONS
**		None
*/
int SynLhcLine::findPartIdx(Eqp *pEqp)
{
	int partIdx;
	for(partIdx = 0 ; partIdx < pPartList->count() ; partIdx++)
	{
		VacLinePart *pPart = pPartList->at(partIdx);
		if(pPart->getRealStart() < pPart->getRealEnd())	// part not including IP1
		{
			if((pPart->getRealStart() <= pEqp->getStart()) && (pEqp->getStart() <= pPart->getRealEnd()))
			{
				break;
			}
		}
		else	// Part including IP1
		{
			if((pPart->getRealStart() <= pEqp->getStart()) || (pEqp->getStart() <= pPart->getRealEnd()))
			{
				break;
			}
		}
	}
	return partIdx;
}

/*
**	FUNCTION
**		Check if this line is for beam vacuum or not
**
**	PARAMETERS
**		None
**
**	RETURNS
**		true	- if this line is for beam vacuum;
**		false	- if this line is for isolation vacuum
**
**	CAUTIONS
**		None
*/
bool SynLhcLine::isBeamVacuum(void)
{
	return (type != Qrl) && (type != Cryo);
}

/*
**	FUNCTION
**		Intialize synoptic line BEFORE building line geometry
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::init(void)
{
	curEqpIdx = endX = curPartIdx = maxSectHeight = 0;
	y = ascent = descent = 0;
}

/*
**	FUNCTION
**		Find next position of element to be processed when
**		calculating horizontal geometry
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Position of next element to process, or
**		huge value if no more elements shall be processed
**
** CAUTIONS
**		None
*/
float SynLhcLine::nextPosition(void)
{
	float	result = HUGE_COORDINATE;

	// Make sure start of line part will be processed before first
	// equipment in this part and end of line part will be processed
	// after last equipment in that part
	VacLinePart *pPart = NULL;
	if((0 <= curPartIdx) && (curPartIdx < pPartList->count()))
	{
		pPart = pPartList->at(curPartIdx);
	}
	if(pPart)
	{
		if(!pPart->isStartDone())
		{
			if(pPart->getStartPos() < result)
			{
				result = pPart->getStartPos();
				curEqp = false;
			}
		}
		SynLhcEqpItem *pItem = NULL;
		if((0 <= curEqpIdx) && (curEqpIdx < eqps.count()))
		{
			pItem = eqps.at(curEqpIdx);
		}
		if(pItem)
		{
			if(pItem->getStart() < result)
			{
				result = pItem->getStart();
				curEqp = true;
			}
		}
		if(pPart->isStartDone() && pPart->isFinished())
		{
			if(pPart->getEndPos() < result)
			{
				result = pPart->getEndPos();
				curEqp = false;
			}
		}
	}
	return result;
}

/*
**	FUNCTION
**		Calculate screen position for next element to be processed
**
**	PARAMETERS
**		order		- Order of this element in processing sequence
**		coord		- Last processed world coordinate
**		connPos		- Last used connection point in screen coordinates
**		endCoord	- Last used end coordinate of element
**		endConnPos	- Last used and connection point
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::placeItem(int order, float &coord, int &connPos,
	float &endCoord, int &endConnPos)
{
	if(curEqp)	// Place equipment
	{
		SynLhcEqpItem *pItem = eqps.at(curEqpIdx);
		pItem->setStartOrder(order);
		pItem->setEndOrder(order);
		placeEqp(coord, connPos, endCoord, endConnPos);
	}
	else	// Place line start or end
	{
		VacLinePart *pPart = pPartList->at(curPartIdx);
		if(!pPart->isStartDone())
		{
			pPart->setStartOrder(order);
			placePartStart(coord, connPos, endCoord, endConnPos);
		}
		else
		{
			pPart->setEndOrder(order);
			placePartEnd(coord, connPos, endCoord, endConnPos);
		}
	}
}

/*
**	FUNCTION
**		Calculate screen position for next device to be processed
**
**	PARAMETERS
**		coord		- Last processed world coordinate
**		connPos		- Last used connection point in screen coordinates
**		endCoord	- Last used end coordinate of element
**		endConnPos	- Last used and connection point
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::placeEqp(float &coord, int &connPos, float &endCoord, int &endConnPos)
{
	SynLhcEqpItem	*pItem = eqps.at(curEqpIdx);
	pItem->setStartReady(true);	// Especially for CRYO thermometers

	int minSpacing = pItem->getType() == EqpType::VacDevice ? MIN_ACTIVE_SPACING : MIN_PASSIVE_SPACING;

	// Connect point location must grow with coordinate
	if(pItem->getStart() > coord)
	{
		coord = pItem->getStart();
		connPos += minSpacing;
	}
	if((endCoord >= 0.0) && (pItem->getStart() > endCoord))
	{
		if((endConnPos + minSpacing) > connPos)
		{
			connPos = endConnPos + minSpacing;
		}
	}

	// Icon shall not overlap with previous icon on the same line
	// After inroducing CRYO thermometers - there are several 'levels' of icons,
	// so vertical position and size of previous icons shall also be taken into account
	// This ONLY applies to thermometers 22.05.2008 L.Kopylov
	//
	// Valves on isolation vacuum shall take into account connection point
	// of previous device even if previous device is on another vertical
	// position and icons do not overlap 10.06.2009 L.Kopylov
	int minIconLeft = 0;
	bool isValveOnIsolVacuum = false;
	if(!isBeamVacuum())
	{
		if(pItem->getType() == EqpType::VacDevice)
		{
			if(pItem->getEqp()->getFunctionalType() == FunctionalType::VV)
			{
				isValveOnIsolVacuum = true;
				const QString attr = pItem->getEqp()->getAttrValue("SpecialHandling");
				if(attr == "1")
				{
					isValveOnIsolVacuum = false;
				}
			}
		}
	}
	for(int idx = curEqpIdx - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pPrev = eqps.at(idx);
		if(isValveOnIsolVacuum)
		{
			int prevConn = pPrev->getConnPoint().x();
			if(minIconLeft < prevConn)
			{
				minIconLeft = prevConn;
			}
		}
		QRect &rect = pPrev->getRect();

		// L.Kopylov 29.01.2010 to have min/avg/max thermometers on one vertical line
		if(pItem->isCryoThermometer())
		{
			if(pPrev->getType() == EqpType::VacDevice)
			{
				if(pPrev->getEqp()->getFunctionalType() == FunctionalType::CRYO_TT_SUM)
				{
					int prevMinLeft = rect.right() + minSpacing;
					if(minIconLeft < prevMinLeft)
					{
						minIconLeft = prevMinLeft;
					}
					continue;
				}
			}
		}

		// L.Kopylov 11.11.2011: if device is sector border - do not take into
		// account difference in vertical positions, otherwise VG/VPI on beam line
		// may appear very close to sector valves
		bool checkVerticalPos = false;
		if(isBeamVacuum() && (!pItem->isSectorBorder()) && (!pPrev->isSectorBorder()))
		{
			checkVerticalPos = true;
		}
		// L.Kopylov 11.11.2011: Another change: vertical position shall not be
		// checked for solenoids (VIES). The result is too wide in some cases, but
		// it is better than possible icon overlap
		if(checkVerticalPos)
		{
			if(pItem->getType() == EqpType::VacDevice)
			{
				if(pItem->getEqp()->getFunctionalType() == FunctionalType::VIES)
				{
					checkVerticalPos = false;
				}
			}
		}
		if(checkVerticalPos)
		{
			if(pPrev->getType() == EqpType::VacDevice)
			{
				if(pPrev->getEqp()->getFunctionalType() == FunctionalType::VIES)
				{
					checkVerticalPos = false;
				}
			}
		}

		if(checkVerticalPos)
		{
			if(rect.bottom() < pItem->getRect().top())
			{
				continue;
			}
			if(rect.top() > pItem->getRect().bottom())
			{
				continue;
			}
		}
		int prevMinLeft = rect.right() + minSpacing;
		if(minIconLeft < prevMinLeft)
		{
			minIconLeft = prevMinLeft;
		}
	}

	// Icon shall not overlap with start of current line part
	VacLinePart *pPart = NULL;
	if((0 <= curPartIdx) && (curPartIdx < pPartList->count()))
	{
		pPart = pPartList->at(curPartIdx);
	}
	if(pPart)
	{
		if(pPart->isStartDone())
		{
			if(minIconLeft < (pPart->getStartX() + MIN_ACTIVE_SPACING))
			{
				minIconLeft = pPart->getStartX() + MIN_ACTIVE_SPACING;
			}
		}
	}

	if((connPos - pItem->getRect().left()) < minIconLeft)
	{
		connPos = minIconLeft + pItem->getRect().left();
	}

	// If this is sector border - there shall be enough space before device for sector name
	if(pItem->isSectorBorder())
	{
		connPos = calcSectorBorderPosition(pItem, connPos);
	}

	// Single equipment and group of equipments are processed differently
	if(!pItem->getGroupId())	// Signle equipment
	{
		pItem->getConnPoint().setX(connPos);
		QRect &rect = pItem->getRect();
		rect.moveLeft(connPos - rect.left());
		if(pItem->getEqp()->getLength() &&
			((pItem->getStart() + pItem->getEqp()->getLength()) > endCoord))
		{
			endCoord = pItem->getStart() + pItem->getEqp()->getLength();
			endConnPos = rect.right();
		}
		if(rect.right() > endX)
		{
			endX = rect.right();
		}
		curEqpIdx++;
	}

	else	// Group of equipments
	{
		// Place all elements of group starting from connect point = 0
		int	groupConn = 0, iconLeft = 0, groupStart = -1, groupEnd = 0,
			borderConn = 0;

		// If group contains valve (equipment with verticalPos == 0) - group
		// connection point can not be before connection point of the very first
		// valve in group
		int	valveConn = -1, idx;
		SynLhcEqpItem *pNext;
		for(idx = curEqpIdx ; idx < eqps.count() ; idx++)
		{
			pNext = eqps.at(idx);
			if(pNext->getGroupId() != pItem->getGroupId())
			{
				break;
			}
			pNext->setStartOrder(pItem->getStartOrder());
			pNext->setEndOrder(pItem->getStartOrder());
			if((pNext != pItem) && pNext->isSectorBorder())
			{
				borderConn = calcSectorBorderPosition(pNext, groupConn);
			}
			QRect &rect = pNext->getRect();
			QPoint &connPoint = pNext->getConnPoint();
			connPoint.setX(groupConn);
			rect.moveLeft(groupConn - rect.x());
			if(rect.left() < iconLeft)
			{
				connPoint.setX(connPoint.x() + iconLeft - rect.x());
				rect.moveLeft(iconLeft);
			}
			// L.Kopylov 11.11.2011 Solenoids (VIES) do not participate in
			// calculation of group's connection point
			bool isSolenoid = false;
			if(pNext->getType() == EqpType::VacDevice)
			{
				if(pNext->getEqp()->getFunctionalType() == FunctionalType::VIES)
				{
					isSolenoid = true;
				}
			}
			if(!isSolenoid)
			{
				if(groupStart < 0)
				{
					groupStart = connPoint.x();
				}
				groupEnd = connPoint.x();
			}
			if((pNext->getVerticalPos() == 0) && (valveConn < 0))
			{
				valveConn = connPoint.x();
			}
			iconLeft = rect.right();
			if(iconLeft < connPoint.x())
			{
				iconLeft = connPoint.x();
			}
			iconLeft += MIN_ACTIVE_SPACING;
			groupConn = iconLeft;
		}

		// Find connect point of group, move the whole group if needed
		groupConn = (groupEnd + groupStart) / 2;
		if((valveConn >= 0) && (valveConn < groupConn))
		{
			groupConn = valveConn;
		}
		int	delta = minIconLeft;
		if((pItem->getRect().x() + delta) < minIconLeft)
		{
			delta += minIconLeft - pItem->getRect().x();
		}
		if((delta + groupConn) < connPos)
		{
			delta = connPos - groupConn;
		}
		else
		{
			connPos = delta + groupConn;
		}
		// Added 12.06.2009 LIK
		if((valveConn > 0) && (borderConn > 0) && (borderConn > (valveConn + delta)))
		{
			delta += borderConn - (valveConn + delta);
		}
		valveConn = -1;
		float valveCoord = -1;
		for(idx = curEqpIdx ; idx < eqps.count() ; idx++)
		{
			pNext = eqps.at(idx);
			if(pNext->getGroupId() != pItem->getGroupId())
			{
				break;
			}
			QRect &rect = pNext->getRect();
			QPoint &connPoint = pNext->getConnPoint();
			connPoint.setX(connPoint.x() + delta);
			rect.moveLeft(rect.left() + delta);
			if(rect.right() > endX)
			{
				endX = rect.right();
			}
			if((pNext->getVerticalPos() == 0) && (valveConn < 0))
			{
				valveConn = connPoint.x();
				valveCoord = pNext->getStart();
			}
			if(pNext->getStart() > endCoord)
			{
				endCoord = pNext->getStart();
				endConnPos = connPoint.x();
			}
		}
		if(valveConn > 0)	// LIK 06.06.2008
		{
			endCoord = valveCoord;
			endConnPos = valveConn;
		}
		curEqpIdx = idx;	// The whole group has been processed ????????
	}
	if(connPos > endX)
	{
		endX = connPos;
	}
}

/*
**	FUNCTION
**		Calculate screen position for start of line part
**
**	PARAMETERS
**		coord		- Last processed world coordinate
**		connPos		- Last used connection point in screen coordinates
**		endCoord	- Last used end coordinate of element
**		endConnPos	- Last used and connection point
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::placePartStart(float &coord, int &connPos, float &endCoord, int &endConnPos)
{
	VacLinePart *pPart = pPartList->at(curPartIdx);

	if(pPart->getStartPos() > coord)
	{
		coord = pPart->getStartPos();
		connPos += MIN_PASSIVE_SPACING;
	}
	if((endCoord > 0.0) && (pPart->getStartPos() > endCoord) && (connPos <= endConnPos))
	{
		connPos = endConnPos + MIN_ACTIVE_SPACING;
	}
	if(curPartIdx > 0)
	{
		VacLinePart *pPrev = pPartList->at(curPartIdx - 1);
		if(pPart->getStartPos() == pPrev->getEndPos())
		{
			pPrev->setEndX(connPos);
			pPart->setStartX(connPos);
		}
		else	// QRL and CRYO line pieces shall have a gap from end of previous part
		{
			if((pPrev->getEndX() + VAC_LINE_SPACING) > connPos)
			{
				connPos = pPrev->getEndX() + VAC_LINE_SPACING;
			}
			pPart->setStartX(connPos);
		}
	}
	else
	{
		pPart->setStartX(connPos);
	}
	if(pPart->getStartX() > endX)
	{
		endX = pPart->getStartX();
	}
	pPart->setStartDone(true);
	// If end of part is outside of synoptic area and line part is a sector - add
	// sector label
	if((pPart->getEndPos() > end) && pPart->getSector())
	{
		addSector(pPart->getSector(), false, pPart->getStartX(), 0, pPart->getEndPos(), pPart->getStartOrder());
	}
}

/*
**	FUNCTION
**		Calculate screen position for end of line part
**
**	PARAMETERS
**		coord		- Last processed world coordinate
**		connPos		- Last used connection point in screen coordinates
**		endCoord	- Last used end coordinate of element
**		endConnPos	- Last used and connection point
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::placePartEnd(float &coord, int &connPos, float &endCoord, int &endConnPos)
{
	VacLinePart *pPart = NULL;
	if((0 <= curPartIdx) && (curPartIdx < pPartList->count()))
	{
		pPart = pPartList->at(curPartIdx);
	}
	if(!pPart)
	{
		return;	// Can not happen, just protection
	}
	if(pPart->isFinished())
	{
		if(pPart->getEndPos() > coord)
		{
			coord = pPart->getEndPos();
			connPos += MIN_PASSIVE_SPACING;
		}
		if((endCoord >= 0.0) && (pPart->getEndPos() > endCoord) && (connPos <= endConnPos))
		{
			connPos = endConnPos + MIN_PASSIVE_SPACING;
		}
	}

	// Make sure line part covers all equipment in it
	for(int idx = curEqpIdx - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		if(!pItem->isSectorBorder())
		{
			if(pItem->getRect().right() > connPos)
			{
				connPos = pItem->getRect().right();
			}
		}
		break;
	}

	// Make sure there is enough space to put sector name in this line
	Sector	*pSector = NULL;
	int sectStart = 0;
	if(pPart->getSector())	// Take sector name from part
	{
		pSector = pPart->getSector();
		sectStart = pPart->getStartX();
	}
	else	// Find sector name from previous sector border
	{
		for(int idx = curEqpIdx - 1 ; idx >= 0 ; idx--)
		{
			SynLhcEqpItem *pItem = eqps.at(idx);
			if(pItem->getStart() < pPart->getStartPos())
			{
				break;
			}
			if(!pItem->isSectorBorder())
			{
				continue;
			}

			// Do not add sector after the last one
			if(pItem->getEqp()->getSectorBefore() != pEndSector)
			{
				pSector = pItem->getEqp()->getSectorAfter();
				sectStart = pItem->getConnPoint().x();
				break;
			}
		}
	}
	if(!pSector) // No sector border within line part - check if there is another sector reference
	{
		for(int idx = curEqpIdx - 1 ; idx >= 0 ; idx--)
		{
			SynLhcEqpItem *pItem = eqps.at(idx);
			if(pItem->getStart() < pPart->getStartPos())
			{
				break;
			}
			if(pItem->getEqp()->getSectorBefore())
			{
				pSector = pItem->getEqp()->getSectorBefore();
				break;
			}
		}
		sectStart = pPart->getStartX();
	}
 	if(pSector)
	{
		QRect nameRect = parent->fontMetrics().boundingRect(pSector->getName());
		if((sectStart + nameRect.width() + 2 * MIN_ACTIVE_SPACING) > connPos)
		{
			connPos = sectStart + nameRect.width() + 2 * MIN_ACTIVE_SPACING;
		}
		if(nameRect.height() > maxSectHeight)
		{
			maxSectHeight = nameRect.height();
		}
		addSector(pSector, false, sectStart, connPos, coord, pPart->getEndOrder());
	}
	// Make sure line part has no zero length
	if((pPart->getStartX() + 2 * PASSIVE_ELEM_WIDTH) > connPos)
	{
		connPos = pPart->getStartX() + 2 * PASSIVE_ELEM_WIDTH;
	}
	pPart->setEndX(connPos);
	if(pPart->getEndX() > endX)
	{
		endX = pPart->getEndX();
	}
	curPartIdx++;
}

/*
**	FUNCTION
**		Correct last assigned screen position. The purpose is to have
**		the same screen coordinate for elements on different lines having
**		the same physical coordinates. When lines are processed one after
**		other - screen position on the next line may become more than
**		screen position on previous line(s)
**
**	PARAMETERS
**		coord		- Last processed world coordinate
**		connPos		- Last used connection point in screen coordinates
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::correctLastPosition(float coord, int connPos, int /* order */)
{
	// Equipment
	for(int idx = curEqpIdx - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		int delta = 0;
		if(pItem->getStart() < coord)
		{
			break;
		}
		// Single equipment and groups are processed differently
		if(pItem->getGroupId() > 0)	// Group
		{
			int	groupMinConn = pItem->getConnPoint().x(),
				groupMaxConn = pItem->getConnPoint().x();
			SynLhcEqpItem *pPrev;
			int prevIdx;
			for(prevIdx = idx - 1 ; prevIdx >= 0 ; prevIdx--)
			{
				pPrev = eqps.at(prevIdx);
				if(pPrev->getGroupId() != pItem->getGroupId())
				{
					break;
				}
				// Even within one group elements may have different coordinates, so take
				// into account coordinate of individual elements with group LIK 05.06.2008
				if(pPrev->getStart() < coord)
				{
					break;
				}
				groupMinConn = pPrev->getConnPoint().x();
			}
			if(!delta)
			{
				delta = connPos - ((groupMinConn + groupMaxConn) / 2);
			}
			if(delta > 0)
			{
				for(prevIdx = idx /* L.Kopylov 11.11.2011 remove - 1 */ ; prevIdx >= 0 ; prevIdx--)
				{
					pPrev = eqps.at(prevIdx);
					if(pPrev->getGroupId() != pItem->getGroupId())
					{
						break;
					}
					pPrev->moveHor(delta);
					pPrev->setCorrDelta(delta);
				}
			}
			else
			{
				break;
			}
			// Set list position to previous before processed group
			if(pPrev)
			{
				idx = eqps.indexOf(pPrev);
			}
			else
			{
				idx = 0;
			}
		}
		else if(pItem->getType() == EqpType::VacDevice)	// Single device
		{
			if(!delta)
			{
				delta = connPos - pItem->getConnPoint().x();
			}
			if(delta > 0)
			{
				pItem->moveHor(delta);
				pItem->setCorrDelta(delta);
			}
			else
			{
				break;
			}
		}
	}

	// Line parts
	if(pPartList->count())
	{
		int partIdx = -1;
		if((0 <= curPartIdx) && (curPartIdx < pPartList->count()))
		{
			partIdx = curPartIdx;
		}
		bool checkLastPart = false;
		if(partIdx < 0)
		{
			partIdx = pPartList->count() - 1;
			checkLastPart = true;
		}
		VacLinePart *pCurPart = pPartList->at(partIdx);
		for( ; partIdx >= 0 ; partIdx--)
		{
			VacLinePart *pPart = pPartList->at(partIdx);
			if(checkLastPart || (pPart != pCurPart))	// Check end position
			{
				if(pPart->getEndPos() < coord)
				{
					break;
				}
				int delta = connPos - pPart->getEndX();
				if(delta > 0)
				{
					pPart->setEndX(pPart->getEndX() + delta);
					pPart->setCorrDeltaEnd(delta);
				}
			}
			if(pPart->isStartDone())	// Check start position
			{
				if(pPart->getStartPos() < coord)
				{
					break;
				}
				int delta = connPos - pPart->getStartX();
				if(delta > 0)
				{
					pPart->setStartX(pPart->getStartX() + delta);
					pPart->setCorrDeltaStart(delta);
				}
			}
		}
	}

	// Sectors
	for(int sectIdx = sectors.count() - 1 ; sectIdx >= 0 ; sectIdx--)
	{
		SynLhcSectorLabel *pLabel = sectors.at(sectIdx);
		if(pLabel->getEndPos() < coord)
		{
			break;
		}
		int delta = connPos - pLabel->getEnd();
		if(delta > 0)
		{
			pLabel->setEnd(pLabel->getEnd() + delta);
			pLabel->setCorrDelta(delta);
		}
	}
}

/*
**	FUNCTION
**		Finish line geomtery after total width of synoptic view has been found
**
**	PARAMETERS
**		viewWidth	- Total width of synoptic view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::finishPartsSectors(int viewWidth)
{
	for(int idx = pPartList->count() - 1 ; idx >= 0 ; idx--)
	{
		VacLinePart *pPart = pPartList->at(idx);
		if(!pPart->isFinished())
		{
			pPart->setEndX(viewWidth);
			pPart->setEndPos(end);
		}
	}

	// The very last sector can be without end coordinate - set it to total width
	if(!sectors.isEmpty())
	{
		SynLhcSectorLabel *pLabel = sectors.last();
		if(!pLabel->getEnd())
		{
			pLabel->setEnd(viewWidth);
			pLabel->setDrawEnd(false);
		}
	}
}

/*
**	FUNCTION
**		Add sector geometry information. Before adding - verify if such
**		sector already present: sectors can come from both equipment at
**		sector border and from line parts
**
**	PARAMETERS
**		pSector	- Pointer to sector to add
**		isByEqp	- true if sector is added by device processing,
**					false - by line part processing
**		start	- Start screen coordinate of sector
**		end		- End screen coordinate of sector
**		endPos	- Physical coordinate of sector end
**		order	- Order of item processing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::addSector(Sector *pSector, bool isByEqp, int start, int end, float endPos, int order)
{
	SynLhcSectorLabel *pSect = NULL;
	if(!sectors.isEmpty())
	{
		pSect = sectors.last();
		if(pSect->getSector() == pSector)
		{
			if(pSect->getEnd() < end)
			{
				if(!pSect->isByEqp())	// Correct coordinates for corresponding part
				{
					if(pSect->getEndPos() < endPos)	// LIK 30.10.2006
					{
						pSect->setEndPos(endPos);
					}
					VacLinePart *pNextPart = NULL;
					for(int idx = pPartList->count() - 1 ; idx >= 0 ; idx--)
					{
						VacLinePart *pPart = pPartList->at(idx);
						if((pPart->getStartX() == pSect->getStart()) &&
							(pPart->getEndX() == pSect->getEnd()))
						{
							pPart->setEndX(end);
							if(pNextPart)
							{
								if(pNextPart->getStartPos() == pPart->getEndPos())
								{
									pNextPart->setStartX(end);
								}
							}
							break;
						}
						pNextPart = pPart;
					}
				}
				pSect->setEnd(end);
				pSect->setEndOrder(order);
			}
			return;
		}
	}
	// New sector
	pSect = new SynLhcSectorLabel(pSector);
	sectors.append(pSect);
	pSect->setByEqp(isByEqp);
	pSect->setStartOrder(order);
	pSect->setEndOrder(order);
	pSect->setStart(start);
	pSect->setEndPos(endPos);
	pSect->setEnd(end);
}

/*
**	FUNCTION
**		Check if vacuum type (of equipment) matches vacuum type mask
**
**	PARAMETERS
**		vacType	- Vacuum type to check
**
**	RETURNS
**		true if vacType matches vacTypeMask;
**		false - otherwise.
**
**	CAUTIONS
**		None
*/
bool SynLhcLine::vacTypeMatchesMask(unsigned vacType)
{
	// In order to prevent appearance of CRYO thermometers on line which is masked out
	switch(type)
	{
	case Qrl:
	case Cryo:
		vacType &= ~(VacType::RedBeam | VacType::BlueBeam | VacType::CrossBeam | VacType::CommonBeam);
		break;
	case Pas:
	case OuterBeam:
	case CrossBeam:
	case InnerBeam:
	case CommonBeam:
		vacType &= ~(VacType::Qrl | VacType::Cryo | VacType::DSL);
		break;
	}

	// Now ready to compare
	if(vacType & vacTypeMask)
	{
		return true;
	}
	if(vacType & (VacType::CrossBeam | VacType::CommonBeam))
	{
		if(vacTypeMask & (VacType::RedBeam | VacType::BlueBeam))
		{
			return true;
		}
	}
	else if(vacType & VacType::DSL)
	{
		if(vacTypeMask & VacType::Cryo)
		{
			return true;
		}
	}
	return false;
}

void SynLhcLine::addAlarmIcons(void)
{
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		if(!pItem->getIcon())
		{
			continue;
		}
		Eqp *pEqp = pItem->getEqp();
		if(!pEqp)
		{
			continue;
		}
		const QList<Eqp *> &targets = pEqp->getExtTargets();
		if(targets.isEmpty())
		{
			continue;
		}
		int extY = 0;
		VacIconContainer::Direction direction = pItem->getIcon()->getSlaveDirection();
		switch(direction)
		{
		case VacIconContainer::Up:	// Above line
			extY = pItem->getRect().top() - (pItem->isIntlSourceBackward() || (pItem->isIntlSourceForward()) ? 9 : 1);
			break;
		case VacIconContainer::Down:	// Below line
			extY = pItem->getRect().bottom() + (pItem->isIntlSourceBackward() || (pItem->isIntlSourceForward()) ? 9 : 1);
			break;
		default:	// to make gcc happy
			break;
		}
		int centerX = pItem->getRect().center().x();
		VacIcon *pIcon = NULL;
		SynLhcEqpItem *pNewItem = NULL;
		VacEqpTypeMask	dummyMask;
		for(int targetIdx = 0 ; targetIdx < targets.count() ; targetIdx++)
		{
			pEqp = targets.at(targetIdx);
			pIcon = VacIcon::getIcon(pEqp, parent);
			if(!pIcon)
			{
				continue;
			}
			pIcon->setDirection(direction);
			VacIconGeometry geometry = pIcon->getGeometry(dummyMask);
			// Finally - add new device to list
			pNewItem = new SynLhcEqpItem(pEqp);
			pNewItem->setOuterEqp(pItem->isOuterEqp());
			pNewItem->setIcon(pIcon);
			QColor color;
			setLineColor(pEqp->getVacType(), color);
			pNewItem->setPipeColor(color);
			pNewItem->setType(EqpType::VacDevice);
			if((direction == VacIconContainer::Up) && (!targetIdx))	// Add height for 1st icon above
			{
				extY -= geometry.getHeight();
			}
			QRect rect(centerX - (geometry.getWidth() >> 1), extY, geometry.getWidth(), geometry.getHeight());
			pNewItem->setRect(rect);
			if(direction == VacIconContainer::Up)
			{
				extY -= geometry.getHeight() + 1;
			}
			else
			{
				extY += geometry.getHeight() + 1;
			}
			pNewItem->setConnPoint(rect.center());
			pNewItem->setVerticalPos(pItem->getVerticalPos());
			pNewItem->setSectorBorder(false);
			eqps.append(pNewItem);
			pIcon->move(rect.x(), rect.y());
		}
		/*
		if(pNewItem)	// Last created icon - on maximum distance from beam line
		{
			QRect rect(pNewItem->getRect());
			if((- rect.y()) > ascent)
			{
				ascent = - rect.y();
			}
			if(rect.bottom() > descent)
			{
				descent = rect.bottom();
			}
		}
		*/
	}
}

/*
**	FUNCTION
**		Calculate vertical limits for line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::calcVerticalLimits(void)
{
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		// LIK 10.06.2009 only take into account equipment on right side of line
		// in order not to take into account valves on isolation vacuum
		int yPos = pItem->getRect().top();
		if(yPos < 0)
		{
			if(abs(yPos) > ascent)
			{
				ascent = abs(yPos);
			}
		}
		else
		{
			if(abs(yPos) > descent)
			{
				descent = abs(yPos);
			}
		}
		yPos = pItem->getConnPoint().y();
		if(yPos < 0)
		{
			if(abs(yPos) > ascent)
			{
				ascent = abs(yPos);
			}
		}
		else
		{
			if(abs(yPos) > descent)
			{
				descent = abs(yPos);
			}
		}
		yPos = pItem->getRect().bottom();
		if(yPos < 0)
		{
			if(abs(yPos) > ascent)
			{
				ascent = abs(yPos);
			}
		}
		else
		{
			if(abs(yPos) > descent)
			{
				descent = abs(yPos);
			}
		}
	}
	// Add vertical space for sector labels
	if(eqpAboveLine)
	{
		// if( yDescent < ( maxSectHeight + 4 ) ) yDescent = maxSectHeight + 4;
		if(maxSectHeight)
		{
			descent += maxSectHeight + 4;
		}
	}
	else
	{
		// if( yAscent < ( maxSectHeight + 4 ) ) yAscent = maxSectHeight + 4;
		if(maxSectHeight)
		{
			ascent += maxSectHeight + 4;
		}
	}

	// L.Kopylov 27.01.2010 Add vertical space for interlock sources arrow below line
	if(haveInterlockSources)
	{
		if(eqpAboveLine)
		{
			ascent += 5;
		}
		else
		{
			descent += 5;
		}
	}
}

/*
**	FUNCTION
**		Move all elements in line in vertical direction
**
**	PARAMETERS
**		delta	- The offset for movement
**
**	RETURNS
**		None
**
** CAUTIONS
**		None
*/
void SynLhcLine::moveVertically(int &delta)
{
	delta += ascent;
	y += delta;
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		pItem->getRect().moveTop(pItem->getRect().top() + delta);
		pItem->getConnPoint().setY(pItem->getConnPoint().y() + delta);
	}
	delta += descent;
}

/*
**	FUNCTION
**		Notify synoptic line that geometry calculation has been finished,
**		line shall move all active icons to calculated positions.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::finishGeometry(void)
{
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		pItem->moveIcon();
	}
}

/*
**	FUNCTION
**		Draw line image: beam lines, active elements, sector labels etc...
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		viewStart	- Start of visible range [Pixels]
**		viewEnd		- End of visible range [Pixels]
**
** RETURNS
**		always 1
**
** CAUTIONS
**		None
*/
int SynLhcLine::draw(QPainter &painter, int viewStart, int viewEnd)
{
	drawParts(painter, viewStart, viewEnd);
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		pItem->setCommonReady(false);
	}
	drawActive(painter, viewStart, viewEnd);
	drawSectLabels(painter, viewStart, viewEnd);
	drawSectSelection(painter); // [VACCO-929] Draws selection line around the selected sectors
	return 1;
}

/*
**	FUNCTION
**		Draw all vacuum line parts.
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		viewStart	- Start of visible range [Pixels]
**		viewEnd		- End of visible range [Pixels]
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::drawParts(QPainter &painter, int viewStart, int viewEnd)
{
	for(int idx = 0 ; idx < pPartList->count() ; idx++)
	{
		VacLinePart *pPart = pPartList->at(idx);
		if((pPart->getStartX() > viewEnd) || (viewStart > pPart->getEndX()))
		{
			continue;
		}
		int lineStart = pPart->getStartX();
		int lineEnd = pPart->getEndX();

		setPainterForLine(painter, pPart->getVacType());
		painter.drawLine(lineStart, y, lineEnd, y);

		if(pPart->isStarted() && pPart->isDrawStart())
		{
			drawPartStart(painter, lineStart);
		}
		if(pPart->isFinished() && pPart->isDrawEnd())
		{
			drawPartEnd(painter, lineEnd);
		}
	}
}



/*
**	FUNCTION
**		Draw all controllable equipment
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		viewStart	- Start of visible range [Pixels]
**		viewEnd		- End of visible range [Pixels]
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::drawActive(QPainter &painter, int viewStart, int viewEnd)
{
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		if(pItem->isOuterEqp())
		{
			continue;
		}
		QRect &rect = pItem->getRect();

		// Draw sector borders anyway - labels look bad when dragging
		if(!pItem->isSectorBorder())
		{
			if((rect.right() < viewStart) || (rect.left() > viewEnd))
			{
				continue;
			}
		}
		switch(pItem->getType())
		{
		case EqpType::VacDevice:
			drawActiveElement(pItem, painter);
			break;
		case EqpType::SectorBefore:
		case EqpType::SectorAfter:
			drawSectorElement(pItem, painter);
			break;
		}
	}
}


/*
**	FUNCTION
**		Draw one controllable equipment
**
**	PARAMETERS
**		pItem		- Pointer to equipment item to draw
**		painter		- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::drawActiveElement(SynLhcEqpItem *pItem, QPainter &painter)
{
	QRect &rect = pItem->getRect();
	QPoint &connPoint = pItem->getConnPoint();

	// Just for debugging - draw outline of equipment icon
	if(DebugCtl::isSynoptic())
	{
		painter.setPen(Qt::black);
		painter.drawRect(rect);
	}

	// Calculate center of element
	int xCenter = 0, yCenter = 0;
	bool isConnInside = false;
	if((connPoint.x() >= rect.left()) && (connPoint.x() <= rect.right()))
	{
		xCenter = connPoint.x();
		isConnInside = true;
	}
	else
	{
		xCenter = rect.x() + (rect.width() >> 1);
		isConnInside = false;
	}
	yCenter = rect.top() + (rect.height() >> 1);

	// If Item is VPG Unified (VP_GU) with 2 vvrs (vvr1 and vvr2) overwrite xcenter to vvr1 center and add 2nd pipe to vvr2 center
	if (pItem->getType() == EqpType::VacDevice) {
		if ((pItem->getEqp()->getCtrlFamily() == 106) && (pItem->getEqp()->getCtrlType() == 11))  { // Control type VPGF_L (VP_GU)
			Eqp *pVEqp = pItem->getEqp();
			Q_ASSERT_X(pVEqp->inherits("EqpVP_GU"), "SynLhcLine::drawActiveElement", pVEqp->getDpName());
			EqpVP_GU *pVpgEqp = (EqpVP_GU *)pVEqp;
			if (pVpgEqp->isVvr2()) {
				isConnInside = false;
				// additional pipes
				setPainterForLine(painter, pItem->getEqp()->getVacType());
				// pipe to vvr2 center
				painter.drawLine(rect.x() + rect.width() - 10, y, rect.x() + rect.width() - 10, rect.y());
				// pipe to vvr1 center
				painter.drawLine(rect.x() + 9, y, rect.x() +9, rect.y());
			}
		}
	}
	// For valves on isolation vacuum draw extra lines connecting valve with base line
	bool isValveOnIsolVacuum = false;
	if(!isBeamVacuum())
	{
		if(pItem->getType() == EqpType::VacDevice)
		{
			if(pItem->getEqp()->getFunctionalType() == FunctionalType::VV)
			{
				isValveOnIsolVacuum = true;
				const QString attr = pItem->getEqp()->getAttrValue("SpecialHandling");
				if(attr == "1")
				{
					isValveOnIsolVacuum = false;
				}
			}
		}
	}
	if(isValveOnIsolVacuum)
	{
		setPainterForLine(painter, pItem->getEqp()->getVacType());
		painter.drawLine(rect.left() - 2, y, rect.left() - 2, rect.center().y() - 1);
		painter.drawLine(rect.right() + 2, y, rect.right() + 2, rect.center().y() - 1);
	}

	// CRYO thermometers do not need connection lines drawn
	// Solenoid icons (VIES) also do not need connection line. L.Kopylov 26.04.2011
	// VRJ_TC also do not need connection, but more common mechanism has been introduced. L.Kopylov 03.04.2014
	else if((!pItem->isCryoThermometer()) && pItem->getIcon()->isConnectedToVacuum())
	{
		setPainterForLine(painter, pItem->getEqp()->getVacType());

		// If device is part of group - draw connection line common for all devices in group.
		if(pItem->getGroupId() && (!pItem->isCommonReady()))
		{
			drawCommonConn(pItem, painter);
		}

		// Draw connecting line from center of element, 3 variants are possible:
		// 1) connX is inside element width - just straight line
		// 2) connX is before element - two lines on left of element
		// 3) connX is after element - two lines on the right of element
		if(isConnInside)
		{
			painter.drawLine(xCenter, yCenter, xCenter, connPoint.y());
		}
		else
		{
			painter.drawLine(xCenter, yCenter, connPoint.x(), yCenter);
			painter.drawLine(connPoint.x(), yCenter, connPoint.x(), connPoint.y());
		}
	}

	// Draw sector border and sector labels for device at sector border
	// This only works on beam vacuum
	if(pItem->isSectorBorder())
	{
		drawEqpSectorBorder(pItem->getEqp(), xCenter, painter);
	}
	// L.Kopylov 28.01.2010 show interlock source for valve
	else if(pItem->isIntlSourceBackward() || pItem->isIntlSourceForward())
	{
		painter.setPen(VacMainView::getSynPassiveLineColor());
		int arrowY = rect.top();
		if(eqpAboveLine)
		{
			arrowY -= 5;
		}
		else
		{
			arrowY = rect.bottom() + 5;
		}
		int arrowStart = rect.left() + 2, arrowEnd = rect.right() - 2;
		painter.drawLine(arrowStart, arrowY, arrowEnd, arrowY);
		if(pItem->isIntlSourceForward())
		{
			painter.drawLine(arrowEnd - 3, arrowY - 3, arrowEnd, arrowY);
			painter.drawLine(arrowEnd - 3, arrowY + 3, arrowEnd, arrowY);
		}
		if(pItem->isIntlSourceBackward())
		{
			painter.drawLine(arrowStart + 3, arrowY - 3, arrowStart, arrowY);
			painter.drawLine(arrowStart + 3, arrowY + 3, arrowStart, arrowY);
		}
	}

	// Add new icon to be drawn on synoptic
	/* TODO: mobile on EXP_AREA !!!!!!
	if((!pParam->bgndOnly) && pItem->pEqp->GetIconGeometry(true, eqpMask))	// Vac type does not matter
	{
		// For EXP_AREA add VPGM connected in that area HARDCODED!!!
		if(pItem->pEqp->dpType)
		{
			if(!strcmp(pItem->pEqp->dpType, "EXP_AREA"))
			{
				char *flangeName = pItem->pEqp->GetAttrValue("VPGM_Flange");
				if(flangeName)	// Find ACTIVE mobile device sitting on that flange
				{
					CEqp *pEqp = pPool->FindActiveEqpOnFlange(flangeName);
				}
			}
		}
	}
	*/
}

/*
**	FUNCTION
**		Draw common connection line for all devices in group
**
**	PARAMETERS
**		pItem	- Pointer to first item in group
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		It is assumed that pen was already set in painter before calling this method
*/
void SynLhcLine::drawCommonConn(SynLhcEqpItem *pItem, QPainter &painter)
{
	int	connXstart = endX,
		connXend = startX,
		connY = pItem->getConnPoint().y(),
		groupId = pItem->getGroupId();
	bool	hasZeroVerticalPos = false;

	// Search forward from current device
	int startIdx = eqps.indexOf(pItem), idx;
	for(idx = startIdx ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		if(pItem->getGroupId() == groupId)
		{
			if(!pItem->getVerticalPos())
			{
				hasZeroVerticalPos = true;
			}
			pItem->setCommonReady(true);
			// L.Kopylov 11.11.2011 Solenoids (VIES) do not participate in
			// common connection line
			if(pItem->getEqp()->getFunctionalType() == FunctionalType::VIES)
			{
				continue;
			}
			QPoint &connPoint = pItem->getConnPoint();
			if(connXstart > connPoint.x())
			{
				connXstart = connPoint.x();
			}
			if(connXend < connPoint.x())
			{
				connXend = connPoint.x();
			}
			if((connY == y) && (connPoint.y() != y))
			{
				connY = connPoint.y();
			}
		}
		else if(pItem->getGroupId()) // Another group
		{
			break;
		}
	}

	// Search backward from current device
	for(idx = startIdx ; idx >= 0 ; idx--)
	{
		pItem = eqps.at(idx);
		if(pItem->getGroupId() == groupId)
		{
			if(!pItem->getVerticalPos())
			{
				hasZeroVerticalPos = true;
			}
			pItem->setCommonReady(true);
			// L.Kopylov 11.11.2011 Solenoids (VIES) do not participate in
			// common connection line
			if(pItem->getEqp()->getFunctionalType() == FunctionalType::VIES)
			{
				continue;
			}
			QPoint &connPoint = pItem->getConnPoint();
			if(connXstart > connPoint.x())
			{
				connXstart = connPoint.x();
			}
			if(connXend < connPoint.x())
			{
				connXend = connPoint.x();
			}
			if((connY == y) && (connPoint.y() != y))
			{
				connY = connPoint.y();
			}
		}
		else if(pItem->getGroupId()) // Another group
		{
			break;
		}
	}
	painter.drawLine(connXstart, connY, connXend, connY);

	if(!hasZeroVerticalPos)
	{
		connXstart = (connXstart + connXend) >> 1;
		painter.drawLine(connXstart, connY, connXstart, y);
	}
}

/*
**	FUNCTION
**		Activate icons which are visible, deactivate icons which are not visible
**
**	PARAMETERS
**		viewStart	- Start position of visible part
**		viewWidth	- width of visible part
**		dragging	- true if this method is called from scroll bar dragging
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::setViewStart(int viewStart, int viewWidth, bool /* dragging */)
{
	int endPos = viewStart + viewWidth;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		VacIcon *pIcon = pItem->getIcon();
		if(!pIcon)
		{
			continue;
		}
		QRect &rect = pItem->getRect();
		if(rect.x() > endPos)
		{
			pIcon->disconnect();
		}
		else if((rect.x() + rect.width()) < viewStart)
		{
			pIcon->disconnect();
		}
		else
		{
			pIcon->connect();
		}
	}
}

/*
**	FUNCTION
**		Set painter for drawing main line and eqp connection lines
**
**	PARAMETERS
**		painter		- Painter to set up
**		vacType		- Vacuum type of part to be drawn
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::setPainterForLine(QPainter &painter, int vacType)
{
	QColor color;
	setLineColor(vacType, color);
	painter.setPen(QPen(color, VacMainView::getSynPipeWidth()));
}


/*
**	FUNCTION
**		[VACCO-929] [VACCO-948] [VACCO-1645]
**		If a sector is selected, draws the selection lines around it.
**
**	PARAMETERS
**		painter	- Painter used for background drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::drawSectSelection(QPainter &painter)
{
	QColor colorSelected = QColor(255, 127, 80);
	QColor colorOnWork1 = QColor(0, 0, 0);
	QColor colorOnWork2 = QColor(255, 222, 0);
	QColor colorVented1 = QColor(0, 255, 255);
	QColor colorVented2 = QColor(204, 0, 204);
	int verticalUpperY = 3;
	int verticalLowerY = 2;

	for (int idx = 0; idx < sectors.count(); idx++){
		SynLhcSectorLabel *sectLabel = sectors.at(idx);
		Sector *sect = sectLabel->getSector();

		QList<QLine *> lines = sectLabel->getSectorGraph();
		for (int idx2 = 0; idx2 < lines.count(); idx2++){
			if (!sect->isSelected() & sect->isOperational()) continue;
			int x1 = lines.at(idx2)->x1();
			int x2 = lines.at(idx2)->x2();
			int y1 = lines.at(idx2)->y1();
			int y2 = lines.at(idx2)->y2();

			if ((sect->isSelected()) && (sect->isOperational())){
				painter.setPen(QPen(colorSelected, 2, Qt::SolidLine));
				painter.drawLine(x1, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1, y1 + verticalUpperY, x2, y2 + verticalUpperY);
			} else if (!(sect->isSelected()) && (sect->isOnWork())){
				painter.setPen(QPen(colorOnWork1, 2, Qt::DotLine));
				painter.drawLine(x1, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1, y1 + verticalUpperY, x2, y2 + verticalUpperY);

				painter.setPen(QPen(colorOnWork2, 2, Qt::DotLine));
				painter.drawLine(x1 + 2, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1 + 2, y1 + verticalUpperY, x2, y2 + verticalUpperY);
			} else if ((sect->isSelected()) && (sect->isOnWork())){
				painter.setPen(QPen(colorSelected, 2, Qt::DotLine));
				painter.drawLine(x1, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1, y1 + verticalUpperY, x2, y2 + verticalUpperY);

				painter.setPen(QPen(colorOnWork2, 2, Qt::DotLine));
				painter.drawLine(x1 + 2, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1 + 2, y1 + verticalUpperY, x2, y2 + verticalUpperY);
			} else if (!(sect->isSelected()) && (sect->isVented())){
				painter.setPen(QPen(colorVented1, 2, Qt::DotLine));
				 painter.drawLine(x1, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				 painter.drawLine(x1, y1 + verticalUpperY, x2, y2 + verticalUpperY);

				 painter.setPen(QPen(colorVented2, 2, Qt::DotLine));
				 painter.drawLine(x1 + 2, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				 painter.drawLine(x1 + 2, y1 + verticalUpperY, x2, y2 + verticalUpperY);
			} else if ((sect->isSelected()) && (sect->isVented())){
				 painter.setPen(QPen(colorSelected, 2, Qt::DotLine));
				 painter.drawLine(x1, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				 painter.drawLine(x1, y1 + verticalUpperY, x2, y2 + verticalUpperY);

				 painter.setPen(QPen(colorVented2, 2, Qt::DotLine));
				 painter.drawLine(x1 + 2, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				 painter.drawLine(x1 + 2, y1 + verticalUpperY, x2, y2 + verticalUpperY);
			}
		}
	}
}


/*
**	FUNCTION
**		Find color to be used for drawing main line and eqp connection lines
**
**	PARAMETERS
**		vacType	- vacuum type of line/equipment
**		color	- Variable where resulting color will be set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::setLineColor(int vacType, QColor &color)
{
	VacMainView::setSynLineColor(vacType, color);
}

/*
**	FUNCTION
**		Return name of this line type to be written to output file for debugging
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Pointer to string with line type name
**
**	CAUTIONS
**		Pointer to static string is returned, content will be overwritten by next call
*/
const char *SynLhcLine::getLineName(void)
{
	switch(type)
	{
	case Qrl:
		return "QRL";
	case Cryo:
		return "CRYO";
	case Pas:
		return "PASSIVE";
	case OuterBeam:
		return "OUTER BEAM";
	case CrossBeam:
		return "CROSS BEAM";
	case InnerBeam:
		return "INNER BEAM";
	case CommonBeam:
		return "COMMON BEAM";
	default:
		break;
	}
	static char result[32];
#ifdef Q_OS_WIN
	sprintf_s(result, sizeof(result) / sizeof(result[0]), "??? %d", type);
#else
	sprintf(result, "??? %d", type);
#endif
	return result;
}

/*
**	FUNCTION
**		Write all parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLine::dump(FILE *pFile)
{
	if(!DebugCtl::isSynoptic())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}

	fprintf(pFile, "\n\n++++++++++++++ Line %d (%s) nParts %d nEqp %d\n", type,
		getLineName(), pPartList->count(), eqps.count());
	fprintf(pFile, " VacMask %X\n", vacTypeMask);
	fprintf(pFile, " eqpMask:\n");
	eqpMask.dump(pFile);
	fprintf(pFile, " lineY %d ascent %d descent %d EQP IS %s LINE\n", y, ascent, descent,
		(eqpAboveLine ? "ABOVE" : "BELOW"));
	fprintf(pFile, " start %f end %f\n", start, end);
	fprintf(pFile, " PARTS:\n");
	int n = 0;
	for(n = 0 ; n < pPartList->count() ; n++)
	{
		VacLinePart *pPart = pPartList->at(n);
		pPart->dump(pFile, n);
	}
	fprintf(pFile, " SECTORS:\n");
	for(n = 0 ; n < sectors.count() ; n++)
	{
		SynLhcSectorLabel *pLabel = sectors.at(n);
		pLabel->dump(pFile, n);
	}
	fprintf(pFile, " EQUIPMENT:\n");
	for(n = 0 ; n < eqps.count() ; n++)
	{
		SynLhcEqpItem *pItem = eqps.at(n);
		pItem->dump(pFile, n);
	}
	
	
	fprintf(pFile, "\n MOBILE EQUIPMENT:\n");
	for(n = 0 ; n < mobileList.count() ; n++)
	{
		Eqp *pEqp = mobileList.at(n);
		fprintf(pFile, "   %03d: %s @ %f\n", n, pEqp->getName(), pEqp->getStart());
	}
}


/*
**	FUNCTION
**		[VACCO-929]
**		Checks if any sector is represented in the XY point (relative to the synoptic panel)
**
**	PARAMETERS
**		coordX	- panel x coordinate
**		coordY	- panel y coordinate
**
**	RETURNS
**		Pointer to the equivalent SynLhcSectorLabel, if any exists given the XY coords.
**		NULL if no sector is located in the region.
**
**	CAUTIONS
**		None
*/
SynLhcSectorLabel* SynLhcLine::getSectorFromXY(int coordX, int coordY)
{
	int verticalTolerance = 4;
	SynLhcSectorLabel *sectorSelected = NULL;
	for (int idx = 0; idx < sectors.count(); idx++) {
		SynLhcSectorLabel *pSectLabel = sectors.at(idx);
		if (pSectLabel->isPointOnSector(coordX, coordY, verticalTolerance)) {
			sectorSelected = pSectLabel;
			break;
		}
	}
	return sectorSelected;
}


/*
**	FUNCTION
**		[VACCO-929]
**		Auxiliary function, only used for SynLhcLineBeam.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Pointer to list of SynLhcInjDumpLine (empty if not for SynLhcLineBeam)
**
**	CAUTIONS
**		None
*/
QList<SynLhcInjDumpLine *> SynLhcLine::getChildLines(void)
{
	QList<SynLhcInjDumpLine *> pChildLines;
	pChildLines.clear();
	return pChildLines;
}