#ifndef	RACKWIDGET_H
#define	RACKWIDGET_H

#include "qwidget.h"
#include "qpainter.h"

class Rack;
class RackIcon;

class RackWidget : public QWidget
{
	Q_OBJECT
public:
	RackWidget(QWidget *parent = NULL);
	~RackWidget();

	void draw(QPainter &painter, int startX, int startY, int rackIndex);
	void setRackIndex(int rackIndex);
	void setScale(qreal newScale);
	inline qreal getScale(){ return scale; };
	inline void setRackName(QString newName){ this->rackName = newName; };
	void makeEquipmentList(Rack &rack);
	void recalculatePosition();
	void clearSelection(void);
	RackIcon * getRackIcon(QString equipmentName);
	int getRackPixelHeight();
signals:
	void selectedEqp(RackIcon * pIcon);
protected:
	QWidget * parent;
	qreal scale;
	int rackIndex;
	QString rackName;
	QList<QRect *> equipment;
	QList<QString> equipNames;
	QList<QString> equipTypes;
	QList<RackIcon * > equipIcons;

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif