#ifndef SINGLEEQPSEELCTORWIDGET_H
#define SINGLEEQPSEELCTORWIDGET_H

// Widget for single device selection - serach devices by
// visible name filter with wildcards

#include <QWidget>

#include <QLineEdit>
#include <QPushButton>
#include <QTableWidget>

class SingleEqpSelectorWidget : public QWidget
{
	Q_OBJECT

public:
	SingleEqpSelectorWidget(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~SingleEqpSelectorWidget();

	const QString getNextSelectedDp(void);

signals:
	void addEqpRequest(void);
	void removeSelected(void);
	void removeAll(void);

protected:
	// Editor for eqp name filter
	QLineEdit	*pFilterText;

	// Button to find devices based on filter text and populate table with names found
	QPushButton	*pFilterButton;

	// Table with names of devices found
	QTableWidget		*pTable;

	// Button to accept selection in table
	QPushButton	*pTableButton;

	// Index of last row in table where DP name was returned by getNextSelectedDp()
	int			lastRow;

	virtual void resizeEvent(QResizeEvent *event);

private slots:
	void filterButtonClicked(void);
	void tableSelectionChanged(void);
	void tableButtonClicked(void);
};

#endif	// SINGLEEQPSEELCTORWIDGET_H
