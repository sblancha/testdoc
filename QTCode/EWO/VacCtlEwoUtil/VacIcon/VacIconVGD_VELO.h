#ifndef	VACICONVGD_VELO_H
#define	VACICONVGD_VELO_H

//	Icon for differential gauge in VELO

#include "VacIconVG.h"

class VACCTLEWOUTIL_EXPORT VacIconVGD_VELO : public VacIconVG
{
public:
	VacIconVGD_VELO(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};


#endif	// VACICONVGD_VELO_H
