//	Implementation of PollGroup class
/////////////////////////////////////////////////////////////////////////////

#include "PollGroup.h"
#include "ImportPool.h"

#include "PlatformDef.h"

#include <QStringList>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

ImportPool	*PollGroup::pPool = NULL;

void PollGroup::setImportPool(ImportPool *pImportPool)
{
	pPool = pImportPool;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

PollGroup::PollGroup(const char *name, int lineNo)
{
	this->name = pPool->addString(name);
	this->lineNo = lineNo;
	period = EMPTY_INT_VALUE;
}

/*
**
**	FUNCTION
**		Create new PollGroup instance from data read in file.
**
**	PARAMETERS
**		pFile		- Pointer to file being read.
**		lineNo		- Line number in file.
**		name		- name of new group
**		errList		- List where error message shall be added
**						in case of errors.
**
**	RETURNS
**		Pointer to new PollGroup instance created from data in file;
**		NULL in case of error.
**
**	CAUTIONS
**		It is responsability of caller to destroy returned instance using 'delete'
**		operator.
*/
PollGroup *PollGroup::createFromFile(FILE *pFile, int &lineNo, const char *name,
	QStringList &errList)
{
	PollGroup	*pNew = new PollGroup(name, lineNo);
	if(pNew->parseFile(pFile, lineNo, errList) < 0)
	{
		delete pNew;
		return NULL;
	}
	
	if(pNew->getPeriod() == EMPTY_INT_VALUE)
	{
		PARSING_WARNING1("'PollPeriod' isn't specified for S7 Poll Group <%s>", pNew->getName());
	}
	return(pNew);
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int PollGroup::processLineTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	const QString & /* longValue */, QStringList &errList)
{
	const char *funcName = "PollGroup::processLineTokens()";
	if(!strcasecmp(tokens[0], "PollPeriod"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &period) != 1)
		{
			PARSING_WARNING2("Line %d: bad 'PollPeriod' format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "EndS7PollGroup"))
	{
		return 0;
	}
	PARSING_ERR2("Line %d: unexpected <%s>", lineNo, tokens[0]);
}

/*
**
**	FUNCTION
**		Dump instance content to output file for debugging
**
**	PARAMETERS
**		pFile		- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void PollGroup::dump(FILE *pFile)
{
	if(!ImportPool::isDebugMode())
	{
		return;
	}
	fprintf(pFile, "================ Poll Group <%s>: period %d lineNo %d\n",
		name, period, lineNo);
}
