//	Implementation of DevListCriteriaEditorWidget class
/////////////////////////////////////////////////////////////////////////////////

#include "DevListCriteriaEditorWidget.h"

#include <QCheckBox>
#include <QLineEdit>
#include <QLabel>
#include <QSpinBox>
#include <QLayout>
#include <QGroupBox>
#include <QTabWidget>

#include <QVariant>

#include <stdio.h>

DevListCriteriaEditorWidget::DevListCriteriaEditorWidget(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	valid = true;
	setting = false;

	// Build control layout
	DevListCriteria &criteria = DevListCriteria::getInstance();

	// 1) Vertical layout holding all groups of controls
	QVBoxLayout *mainBox = new QVBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);

	// 2) Grid layout with a number of checkboxes
	QGridLayout	*checkBoxGrid = new QGridLayout();
	mainBox->addLayout(checkBoxGrid);

	pNormalCb = new QCheckBox("ON, OPEN...", this);
	pNormalCb->setChecked(criteria.isNormal());
	checkBoxGrid->addWidget(pNormalCb, 0, 0);
	connect(pNormalCb, SIGNAL(toggled(bool)), this, SLOT(normalChanged(bool)));

	pNotNormalCb = new QCheckBox("not ON, not OPEN...", this);
	pNotNormalCb->setChecked(criteria.isNotNormal());
	checkBoxGrid->addWidget(pNotNormalCb, 0, 1);
	connect(pNotNormalCb, SIGNAL(toggled(bool)), this, SLOT(notNormalChanged(bool)));

	pAbnormalCb = new QCheckBox("OFF, CLOSED...", this);
	pAbnormalCb->setChecked(criteria.isAbnormal());
	checkBoxGrid->addWidget(pAbnormalCb, 1, 0);
	connect(pAbnormalCb, SIGNAL(toggled(bool)), this, SLOT(abnormalChanged(bool)));

	pNotAbnormalCb = new QCheckBox("not OFF, not CLOSED...", this);
	pNotAbnormalCb->setChecked(criteria.isNotAbnormal());
	checkBoxGrid->addWidget(pNotAbnormalCb, 1, 1);
	connect(pNotAbnormalCb, SIGNAL(toggled(bool)), this, SLOT(notAbnormalChanged(bool)));

	pErrorsCb = new QCheckBox("Errors", this);
	pErrorsCb->setChecked(criteria.isErrors());
	checkBoxGrid->addWidget(pErrorsCb, 2, 0);
	connect(pErrorsCb, SIGNAL(toggled(bool)), this, SLOT(errorsChanged(bool)));

	pWarningsCb = new QCheckBox("Warnings", this);
	pWarningsCb->setChecked(criteria.isWarnings());
	checkBoxGrid->addWidget(pWarningsCb, 2, 1);
	connect(pWarningsCb, SIGNAL(toggled(bool)), this, SLOT(warningsChanged(bool)));

	pAlertsCb = new QCheckBox("Alerts", this);
	pAlertsCb->setChecked(criteria.isAlerts());
	checkBoxGrid->addWidget(pAlertsCb, 3, 0);
	connect(pAlertsCb, SIGNAL(toggled(bool)), this, SLOT(alertsChanged(bool)));

	pAccessCb = new QCheckBox("Access Errors", this);
	pAccessCb->setChecked(criteria.isAccess());
	checkBoxGrid->addWidget(pAccessCb, 3, 1);
	connect(pAccessCb, SIGNAL(toggled(bool)), this, SLOT(accessChanged(bool)));

	pNotConnectedCb = new QCheckBox("Not Connected(fixed equipment)", this);
	pNotConnectedCb->setChecked(criteria.isNotConnected());
	checkBoxGrid->addWidget(pNotConnectedCb, 4, 1);
	connect(pNotConnectedCb, SIGNAL(toggled(bool)), this, SLOT(notConnectedChanged(bool)));

	pNotActiveCb = new QCheckBox("Not Active(wireless not connected)", this);
	pNotActiveCb->setChecked(criteria.isNotActive());
	checkBoxGrid->addWidget(pNotActiveCb, 4, 0);
	connect(pNotActiveCb, SIGNAL(toggled(bool)), this, SLOT(notActiveChanged(bool)));

	// 3) Tab with analog limits for pressure, temperature...
	QTabWidget *pTab = new QTabWidget(this);
	QWidget*pPage = buildPressureLimitsGroup(pTab, criteria);
	pTab->addTab(pPage, "Pressure");
	pPage = buildTemperatureLimitsGroup(pTab, criteria);
	pTab->addTab(pPage, "Temperature");
	mainBox->addWidget(pTab);

}

QWidget * DevListCriteriaEditorWidget::buildPressureLimitsGroup(QWidget *pParent, DevListCriteria &criteria)
{
	QWidget *pResult = new QWidget(pParent);
	QGridLayout	*pGrid = new QGridLayout(pResult);

	// Line 1 - pressure limit
	pUsePressureLimitCb = new QCheckBox("Limit", pResult);
	pUsePressureLimitCb->setChecked(criteria.isUsePressureLimit());
	pGrid->addWidget(pUsePressureLimitCb, 0, 0);
	connect(pUsePressureLimitCb, SIGNAL(toggled(bool)), this, SLOT(usePressureLimitChanged(bool)));

	pPressureLimitText = new QLineEdit(pResult);
	QRect textSize = pPressureLimitText->fontMetrics().boundingRect("9.99E+99");
	pPressureLimitText->setMinimumWidth(textSize.width() + 4);
	pGrid->addWidget(pPressureLimitText, 0, 1);
	connect(pPressureLimitText, SIGNAL(textChanged(const QString &)),
		this, SLOT(pressureLimitChanged(const QString &)));
	showPrLimit();

	// Line 1 - pressure grow parameters
	pUsePressureGrowLimitCb = new QCheckBox("Grow by", pResult);
	pUsePressureGrowLimitCb->setChecked(criteria.isUsePressureGrowLimit());
	pGrid->addWidget(pUsePressureGrowLimitCb, 1, 0);
	connect(pUsePressureGrowLimitCb, SIGNAL(toggled(bool)),
		this, SLOT(usePressureGrowLimitChanged(bool)));

	pPressureGrowLimitText = new QLineEdit(pResult);
	pPressureGrowLimitText->setMinimumWidth(textSize.width() + 4);
	pGrid->addWidget(pPressureGrowLimitText, 1, 1);
	connect(pPressureGrowLimitText, SIGNAL(textChanged(const QString &)),
		this, SLOT(pressureGrowLimitChanged(const QString &)));

	QLabel *pLab = new QLabel("from", pResult);
	pGrid->addWidget(pLab, 1, 2);

	pPressureStartTimeEdit = new QDateTimeEdit(criteria.getPressureStartTime(), pResult);
	pPressureStartTimeEdit->setDisplayFormat("dd-MM-yyyy HH:mm");
	pGrid->addWidget(pPressureStartTimeEdit, 1, 3);
	connect(pPressureStartTimeEdit, SIGNAL(dateTimeChanged(const QDateTime &)),
		this, SLOT(pressureStartTimeChanged(const QDateTime &)));

	pLab = new QLabel("to", pResult);
	pGrid->addWidget(pLab, 1, 4);

	pPressureEndTimeEdit = new QDateTimeEdit(criteria.getPressureEndTime(), pResult);
	pPressureEndTimeEdit->setDisplayFormat("dd-MM-yyyy HH:mm");
	pGrid->addWidget(pPressureEndTimeEdit, 1, 5);
	connect(pPressureEndTimeEdit, SIGNAL(dateTimeChanged(const QDateTime &)),
		this, SLOT(pressureEndTimeChanged(const QDateTime &)));

	pPressureEndTimeNowCb = new QCheckBox("Now", pResult);
	pPressureEndTimeNowCb->setChecked(criteria.isPressureEndTimeNow());
	pGrid->addWidget(pPressureEndTimeNowCb, 1, 6);
	connect(pPressureEndTimeNowCb, SIGNAL(toggled(bool)),
		this, SLOT(pressureEndTimeNowChanged(bool)));

	pLab = new QLabel(" Spike filter", pResult);
	pGrid->addWidget(pLab, 1, 7);

	pPressureSpikeIntervalSb = new QSpinBox(pResult);
	pPressureSpikeIntervalSb->setRange(0, 600);
	pPressureSpikeIntervalSb->setSingleStep(1);
	pPressureSpikeIntervalSb->setWrapping(true);
	pPressureSpikeIntervalSb->setSuffix(" sec");
	pGrid->addWidget(pPressureSpikeIntervalSb, 1, 8);
	connect(pPressureSpikeIntervalSb, SIGNAL(valueChanged(int)),
		this, SLOT(pressureSpikeIntervalChanged(int)));
	pPressureSpikeIntervalSb->setValue(criteria.getPressureSpikeInterval());

	showPrGrowLimit();

	return pResult;
}

QWidget *DevListCriteriaEditorWidget::buildTemperatureLimitsGroup(QWidget *pParent, DevListCriteria &criteria)
{
	QWidget *pResult = new QWidget(pParent);
	QGridLayout	*pGrid = new QGridLayout(pResult);

	// Line 1 - value limit
	pUseTemperatureLimitCb = new QCheckBox("Limit", pResult);
	pUseTemperatureLimitCb->setChecked(criteria.isUseTemperatureLimit());
	pGrid->addWidget(pUseTemperatureLimitCb, 0, 0);
	connect(pUseTemperatureLimitCb, SIGNAL(toggled(bool)), this, SLOT(useTemperatureLimitChanged(bool)));

	pTemperatureLimitText = new QLineEdit(pResult);
	QRect textSize = pTemperatureLimitText->fontMetrics().boundingRect("9.99E+99");
	pTemperatureLimitText->setMinimumWidth(textSize.width() + 4);
	pGrid->addWidget(pTemperatureLimitText, 0, 1);
	connect(pTemperatureLimitText, SIGNAL(textChanged(const QString &)),
		this, SLOT(temperatureLimitChanged(const QString &)));
	showTempLimit();

	// Line 2 - value grow criteria
	pUseTemperatureGrowLimitCb = new QCheckBox("Grow by", pResult);
	pUseTemperatureGrowLimitCb->setChecked(criteria.isUseTemperatureGrowLimit());
	pGrid->addWidget(pUseTemperatureGrowLimitCb, 1, 0);
	connect(pUseTemperatureGrowLimitCb, SIGNAL(toggled(bool)),
		this, SLOT(useTemperatureGrowLimitChanged(bool)));

	pTemperatureGrowLimitText = new QLineEdit(pResult);
	pTemperatureGrowLimitText->setMinimumWidth(textSize.width() + 4);
	pGrid->addWidget(pTemperatureGrowLimitText, 1, 1);
	connect(pTemperatureGrowLimitText, SIGNAL(textChanged(const QString &)),
		this, SLOT(temperatureGrowLimitChanged(const QString &)));

	QLabel *pLab = new QLabel("from", pResult);
	pGrid->addWidget(pLab, 1, 2);

	pTemperatureStartTimeEdit = new QDateTimeEdit(criteria.getTemperatureStartTime(), pResult);
	pTemperatureStartTimeEdit->setDisplayFormat("dd-MM-yyyy HH:mm");
	pGrid->addWidget(pTemperatureStartTimeEdit, 1, 3);
	connect(pTemperatureStartTimeEdit, SIGNAL(dateTimeChanged(const QDateTime &)),
		this, SLOT(temperatureStartTimeChanged(const QDateTime &)));

	pLab = new QLabel("to", pResult);
	pGrid->addWidget(pLab, 1, 4);

	pTemperatureEndTimeEdit = new QDateTimeEdit(criteria.getTemperatureEndTime(), pResult);
	pTemperatureEndTimeEdit->setDisplayFormat("dd-MM-yyyy HH:mm");
	pGrid->addWidget(pTemperatureEndTimeEdit, 1, 5);
	connect(pTemperatureEndTimeEdit, SIGNAL(dateTimeChanged(const QDateTime &)),
		this, SLOT(temperatureEndTimeChanged(const QDateTime &)));

	pTemperatureEndTimeNowCb = new QCheckBox("Now", pResult);
	pTemperatureEndTimeNowCb->setChecked(criteria.isTemperatureEndTimeNow());
	pGrid->addWidget(pTemperatureEndTimeNowCb, 1, 6);
	connect(pTemperatureEndTimeNowCb, SIGNAL(toggled(bool)),
		this, SLOT(temperatureEndTimeNowChanged(bool)));

	pLab = new QLabel(" Spike filter", pResult);
	pGrid->addWidget(pLab, 1, 7);

	pTemperatureSpikeIntervalSb = new QSpinBox(pResult);
	pTemperatureSpikeIntervalSb->setRange(0, 600);
	pTemperatureSpikeIntervalSb->setSingleStep(1);
	pTemperatureSpikeIntervalSb->setWrapping(true);
	pTemperatureSpikeIntervalSb->setSuffix(" sec");
	pGrid->addWidget(pTemperatureSpikeIntervalSb, 1, 8);
	connect(pTemperatureSpikeIntervalSb, SIGNAL(valueChanged(int)),
		this, SLOT(temperatureSpikeIntervalChanged(int)));
	pTemperatureSpikeIntervalSb->setValue(criteria.getTemperatureSpikeInterval());

	showTempGrowLimit();

	return pResult;
}

QList<QVariant> DevListCriteriaEditorWidget::getCriteriaFlags(void) const
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	QList<QVariant> result;
	result.append(QVariant(criteria.isNormal()));					// 0
	result.append(QVariant(criteria.isNotNormal()));				// 1
	result.append(QVariant(criteria.isAbnormal()));					// 2
	result.append(QVariant(criteria.isNotAbnormal()));				// 3
	result.append(QVariant(criteria.isErrors()));					// 4
	result.append(QVariant(criteria.isWarnings()));					// 5
	result.append(QVariant(criteria.isAlerts()));					// 6
	result.append(QVariant(criteria.isAccess()));					// 7
	result.append(QVariant(criteria.isUsePressureLimit()));			// 8
	result.append(QVariant(criteria.isUsePressureGrowLimit()));		// 9
	result.append(QVariant(criteria.isUseTemperatureLimit()));		// 10
	result.append(QVariant(criteria.isUseTemperatureGrowLimit()));	// 11
	result.append(QVariant(criteria.isNotConnected()));				// 12
	result.append(QVariant(criteria.isPressureEndTimeNow()));		// 13
	result.append(QVariant(criteria.isTemperatureEndTimeNow()));	// 14

	return result;
}

void DevListCriteriaEditorWidget::setCriteriaFlags(const QList<QVariant> &flags)
{
	setting = true;
	DevListCriteria &criteria = DevListCriteria::getInstance();
	for(int idx = 0 ; idx < flags.count() ; idx++)
	{
		if(!flags.at(idx).canConvert(QVariant::Bool))
		{
			continue;
		}
		bool flag = flags.at(idx).toBool();
		switch(idx)	// See order of bits in getCriteriaFlags
		{
		case 0:
			criteria.setNormal(flag);
			pNormalCb->setChecked(flag);
			break;
		case 1:
			criteria.setNotNormal(flag);
			pNotNormalCb->setChecked(flag);
			break;
		case 2:
			criteria.setAbnormal(flag);
			pAbnormalCb->setChecked(flag);
			break;
		case 3:
			criteria.setNotAbnormal(flag);
			pNotAbnormalCb->setChecked(flag);
			break;
		case 4:
			criteria.setErrors(flag);
			pErrorsCb->setChecked(flag);
			break;
		case 5:
			criteria.setWarnings(flag);
			pWarningsCb->setChecked(flag);
		case 6:
			criteria.setAlerts(flag);
			pAlertsCb->setChecked(flag);
			break;
		case 7:
			criteria.setAccess(flag);
			pAccessCb->setChecked(flag);
			break;
		case 8:
			criteria.setUsePressureLimit(flag);
			pUsePressureLimitCb->setChecked(flag);
			break;
		case 9:
			criteria.setUsePressureGrowLimit(flag);
			pUsePressureGrowLimitCb->setChecked(flag);
			break;
		case 10:
			criteria.setUseTemperatureLimit(flag);
			pUseTemperatureLimitCb->setChecked(flag);
			break;
		case 11:
			criteria.setUseTemperatureGrowLimit(flag);
			pUseTemperatureGrowLimitCb->setChecked(flag);
			break;
		case 12:
			criteria.setNotConnected(flag);
			pNotConnectedCb->setChecked(flag);
			break;
		case 13:
			criteria.setPressureEndTimeNow(flag);
			pPressureEndTimeNowCb->setChecked(flag);
			break;
		case 14:
			criteria.setTemperatureEndTimeNow(flag);
			pTemperatureEndTimeNowCb->setChecked(flag);
			break;
		}
	}

	// Some flags are mutually exclusive, leave just one of pair
	if(criteria.isNormal() && criteria.isNotNormal())
	{
		criteria.setNormal(false);
		pNormalCb->setChecked(false);
	}
	if(criteria.isAbnormal() && criteria.isNotAbnormal())
	{
		criteria.setNotAbnormal(false);
		pNotAbnormalCb->setChecked(false);
	}
	if(criteria.isErrors() && criteria.isWarnings())
	{
		criteria.setWarnings(false);
		pWarningsCb->setChecked(false);
	}

	// Flags may change sensitivity of limits text fields
	pPressureLimitText->setEnabled(criteria.isUsePressureLimit());
	bool enabled = criteria.isUsePressureGrowLimit();
	pPressureGrowLimitText->setEnabled(enabled);
	pPressureStartTimeEdit->setEnabled(enabled);
	pPressureEndTimeEdit->setEnabled(enabled && (!criteria.isPressureEndTimeNow()));
	pPressureEndTimeNowCb->setEnabled(enabled);
	pPressureSpikeIntervalSb->setEnabled(enabled);

	pTemperatureLimitText->setEnabled(criteria.isUseTemperatureLimit());
	enabled = criteria.isUsePressureGrowLimit();
	pTemperatureGrowLimitText->setEnabled(enabled);
	pTemperatureStartTimeEdit->setEnabled(enabled);
	pTemperatureEndTimeEdit->setEnabled(enabled && (!criteria.isTemperatureEndTimeNow()));
	pTemperatureEndTimeNowCb->setEnabled(enabled);
	pTemperatureSpikeIntervalSb->setEnabled(enabled);

	// And finally...
	setting = false;
	emit changed();
}

QList<QVariant> DevListCriteriaEditorWidget::getCriteriaValues(void) const
{
	QList<QVariant> result;
	DevListCriteria &criteria = DevListCriteria::getInstance();
	result.append(QVariant((double)criteria.getPressureLimit()));					// 0
	result.append(QVariant((double)criteria.getPressureGrowLimit()));				// 1
	result.append(QVariant((double)criteria.getTemperatureLimit()));				// 2
	result.append(QVariant((double)criteria.getTemperatureGrowLimit()));			// 3
	result.append(QVariant((double)criteria.getPressureSpikeInterval()));	// 4
	result.append(QVariant((double)criteria.getTemperatureSpikeInterval()));	// 5
	return result;
}

void DevListCriteriaEditorWidget::setCriteriaValues(const QList<QVariant> &values)
{
	setting = true;
	DevListCriteria &criteria = DevListCriteria::getInstance();
	for(int idx = 0 ; idx < values.count() ; idx++)
	{
		if(!values.at(idx).canConvert(QVariant::Double))
		{
			continue;
		}
		float value = (float)values.at(idx).toDouble();
		switch(idx)	// see order in getCriteriaValues()
		{
		case 0:
			criteria.setPressureLimit(value);
			showPrLimit();
			break;
		case 1:
			criteria.setPressureGrowLimit(value);
			showPrGrowLimit();
			break;
		case 2:
			criteria.setTemperatureLimit(value);
			showTempLimit();
			break;
		case 3:
			criteria.setTemperatureGrowLimit(value);
			showTempGrowLimit();
			break;
		case 4:
			criteria.setPressureSpikeInterval(rint(value));
			pPressureSpikeIntervalSb->setValue(rint(value));
			break;
		case 5:
			criteria.setTemperatureSpikeInterval(rint(value));
			pTemperatureSpikeIntervalSb->setValue(rint(value));
			break;
		}
	}
	setting = false;
	emit changed();
}

QList<QVariant> DevListCriteriaEditorWidget::getCriteriaTimes(void) const
{
	QList<QVariant> result;
	DevListCriteria &criteria = DevListCriteria::getInstance();
	result.append(QVariant(criteria.getPressureStartTime()));		// 0
	result.append(QVariant(criteria.getPressureEndTime()));			// 1
	result.append(QVariant(criteria.getTemperatureStartTime()));	// 2
	result.append(QVariant(criteria.getTemperatureEndTime()));		// 3
	return result;
}

void DevListCriteriaEditorWidget::setCriteriaTimes(const QList<QVariant> &times)
{
	setting = true;
	DevListCriteria &criteria = DevListCriteria::getInstance();
	for(int idx = 0 ; idx < times.count() ; idx++)
	{
		if(!times.at(idx).canConvert(QVariant::DateTime))
		{
			continue;
		}
		QDateTime value = times.at(idx).toDateTime();
		switch(idx)	// See order in getCriteriaTimes()
		{
		case 0:
			criteria.setPressureStartTime(value);
			break;
		case 1:
			criteria.setPressureEndTime(value);
			break;
		case 2:
			criteria.setTemperatureStartTime(value);
			break;
		case 3:
			criteria.setTemperatureEndTime(value);
			break;
		}
	}

	// Adjust for current time if needed
	if(criteria.isPressureEndTimeNow())
	{
		int interval = criteria.getPressureStartTime().secsTo(criteria.getPressureEndTime());
		if(interval <= 0)
		{
			interval = 24 * 60 * 60;	// Use some default
		}
		criteria.setPressureEndTime(QDateTime::currentDateTime());
		criteria.setPressureStartTime(criteria.getPressureEndTime().addSecs(-interval));
	}
	if(criteria.isTemperatureEndTimeNow())
	{
		int interval = criteria.getTemperatureStartTime().secsTo(criteria.getTemperatureEndTime());
		if(interval <= 0)
		{
			interval = 24 * 60 * 60;	// Use some default
		}
		criteria.setTemperatureEndTime(QDateTime::currentDateTime());
		criteria.setTemperatureStartTime(criteria.getTemperatureEndTime().addSecs(-interval));
	}

	// Display values
	pPressureStartTimeEdit->setDateTime(criteria.getPressureStartTime());
	pPressureEndTimeEdit->setDateTime(criteria.getPressureEndTime());
	pTemperatureStartTimeEdit->setDateTime(criteria.getTemperatureStartTime());
	pTemperatureEndTimeEdit->setDateTime(criteria.getTemperatureEndTime());

	setting = false;
	emit changed();
}

void DevListCriteriaEditorWidget::showPrLimit(void)
{
	char	buf[32];
	DevListCriteria &criteria = DevListCriteria::getInstance();
	sprintf(buf, "%7.1E", criteria.getPressureLimit());
	setting = true;
	pPressureLimitText->setText(buf);
	pPressureLimitText->setEnabled(criteria.isUsePressureLimit());
	setting = false;
}

void DevListCriteriaEditorWidget::showPrGrowLimit(void)
{
	char	buf[32];
	DevListCriteria &criteria = DevListCriteria::getInstance();
	sprintf(buf, "%4.1f", criteria.getPressureGrowLimit());
	setting = true;
	pPressureGrowLimitText->setText(buf);
	bool enabled = criteria.isUsePressureGrowLimit();
	pPressureGrowLimitText->setEnabled(enabled);
	pPressureStartTimeEdit->setEnabled(enabled);
	pPressureEndTimeEdit->setEnabled(enabled && (!criteria.isPressureEndTimeNow()));
	pPressureEndTimeNowCb->setEnabled(enabled);
	pPressureSpikeIntervalSb->setEnabled(enabled);
	adjustPressureTimes();
	setting = false;
}

void DevListCriteriaEditorWidget::adjustPressureTimes(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isUsePressureGrowLimit() && criteria.isPressureEndTimeNow())
	{
		int interval = criteria.getPressureStartTime().secsTo(criteria.getPressureEndTime());
		if(interval <= 0)
		{
			interval = 24 * 60 * 60;
		}
		pPressureEndTimeEdit->setDateTime(QDateTime::currentDateTime());
		pPressureStartTimeEdit->setDateTime(pPressureEndTimeEdit->dateTime().addSecs(-interval));
		pPressureEndTimeEdit->setEnabled(false);
	}
	else if(criteria.isUsePressureGrowLimit())
	{
		pPressureEndTimeEdit->setEnabled(true);
	}
}

void DevListCriteriaEditorWidget::showTempLimit(void)
{
	char	buf[32];
	DevListCriteria &criteria = DevListCriteria::getInstance();
	sprintf(buf, "%7.2f", criteria.getTemperatureLimit());
	setting = true;
	pTemperatureLimitText->setText(buf);
	pTemperatureLimitText->setEnabled(criteria.isUseTemperatureLimit());
	setting = false;
}

void DevListCriteriaEditorWidget::showTempGrowLimit(void)
{
	char	buf[32];
	DevListCriteria &criteria = DevListCriteria::getInstance();
	sprintf(buf, "%5.2f", criteria.getTemperatureGrowLimit());
	setting = true;
	pTemperatureGrowLimitText->setText(buf);
	bool enabled = criteria.isUseTemperatureGrowLimit();
	pTemperatureGrowLimitText->setEnabled(enabled);
	pTemperatureStartTimeEdit->setEnabled(enabled);
	pTemperatureEndTimeEdit->setEnabled(enabled && (!criteria.isTemperatureEndTimeNow()));
	pTemperatureEndTimeNowCb->setEnabled(enabled);
	pTemperatureSpikeIntervalSb->setEnabled(enabled);
	adjustTemperatureTimes();
	setting = false;
}

void DevListCriteriaEditorWidget::adjustTemperatureTimes(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isUseTemperatureGrowLimit() && criteria.isTemperatureEndTimeNow())
	{
		int interval = criteria.getTemperatureStartTime().secsTo(criteria.getTemperatureEndTime());
		if(interval <= 0)
		{
			interval = 24 * 60 * 60;
		}
		pTemperatureEndTimeEdit->setDateTime(QDateTime::currentDateTime());
		pTemperatureStartTimeEdit->setDateTime(pTemperatureEndTimeEdit->dateTime().addSecs(-interval));
		pTemperatureEndTimeEdit->setEnabled(false);
	}
	else if(criteria.isUseTemperatureGrowLimit())
	{
		pTemperatureEndTimeEdit->setEnabled(true);
	}
}

void DevListCriteriaEditorWidget::normalChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setNormal(use);
	if(use && criteria.isNotNormal())
	{
		criteria.setNotNormal(false);
		setting = true;
		pNotNormalCb->setChecked(false);
		setting = false;
	}
	emit changed();
}

void DevListCriteriaEditorWidget::notNormalChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setNotNormal(use);
	if(use && criteria.isNormal())
	{
		criteria.setNormal(false);
		setting = true;
		pNormalCb->setChecked(false);
		setting = false;
	}
	emit changed();
}

void DevListCriteriaEditorWidget::abnormalChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setAbnormal(use);
	if(use && criteria.isNotAbnormal())
	{
		criteria.setNotAbnormal(false);
		setting = true;
		pNotAbnormalCb->setChecked(false);
		setting = false;
	}
	emit changed();
}

void DevListCriteriaEditorWidget::notAbnormalChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setNotAbnormal(use);
	if(use && criteria.isAbnormal())
	{
		criteria.setAbnormal(false);
		setting = true;
		pAbnormalCb->setChecked(false);
		setting = false;
	}
	emit changed();
}

void DevListCriteriaEditorWidget::errorsChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setErrors(use);
	if(use && criteria.isWarnings())
	{
		criteria.setWarnings(false);
		setting = true;
		pWarningsCb->setChecked(false);
		setting = false;
	}
	emit changed();
}

void DevListCriteriaEditorWidget::warningsChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setWarnings(use);
	if(use && criteria.isErrors())
	{
		criteria.setErrors(false);
		setting = true;
		pErrorsCb->setChecked(false);
		setting = false;
	}
	emit changed();
}

void DevListCriteriaEditorWidget::alertsChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setAlerts(use);
	emit changed();
}

void DevListCriteriaEditorWidget::accessChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setAccess(use);
	emit changed();
}

void DevListCriteriaEditorWidget::notConnectedChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setNotConnected(use);
	emit changed();
}

void DevListCriteriaEditorWidget::notActiveChanged(bool use)
{
	if (setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setNotActive(use);
	emit changed();
}

void DevListCriteriaEditorWidget::usePressureLimitChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setUsePressureLimit(use);
	showPrLimit();
	emit changed();
}

void DevListCriteriaEditorWidget::usePressureGrowLimitChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setUsePressureGrowLimit(use);
	showPrGrowLimit();
	emit changed();
}

void DevListCriteriaEditorWidget::useTemperatureLimitChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setUseTemperatureLimit(use);
	showTempLimit();
	emit changed();
}

void DevListCriteriaEditorWidget::useTemperatureGrowLimitChanged(bool use)
{
	if(setting)
	{
		return;
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setUseTemperatureGrowLimit(use);
	showTempGrowLimit();
	emit changed();
}


void DevListCriteriaEditorWidget::pressureLimitChanged(const QString &text)
{
	if(setting)
	{
		return;
	}
	float value = text.toFloat(&valid);
	if(valid)
	{
		valid = value > 0.0;
	}
	if(valid)
	{
		DevListCriteria &criteria = DevListCriteria::getInstance();
		criteria.setPressureLimit(value);
		setWidgetBackground(pPressureLimitText, Qt::white);
	}
	else
	{
		setWidgetBackground(pPressureLimitText, Qt::red);
	}
	emit changed();
}

void DevListCriteriaEditorWidget::pressureGrowLimitChanged(const QString &text)
{
	if(setting)
	{
		return;
	}
	float value = text.toFloat(&valid);
	if(valid)
	{
		valid = value > 0.0;
	}
	if(valid)
	{
		DevListCriteria &criteria = DevListCriteria::getInstance();
		criteria.setPressureGrowLimit(value);
		setWidgetBackground(pPressureGrowLimitText, Qt::white);
	}
	else
	{
		setWidgetBackground(pPressureGrowLimitText, Qt::red);
	}
	emit changed();
}

void DevListCriteriaEditorWidget::temperatureLimitChanged(const QString &text)
{
	if(setting)
	{
		return;
	}
	float value = text.toFloat(&valid);
	if(valid)
	{
		valid = value > 0.0;
	}
	if(valid)
	{
		DevListCriteria &criteria = DevListCriteria::getInstance();
		criteria.setTemperatureLimit(value);
		setWidgetBackground(pTemperatureLimitText, Qt::white);
	}
	else
	{
		setWidgetBackground(pTemperatureLimitText, Qt::red);
	}
	emit changed();
}

void DevListCriteriaEditorWidget::temperatureGrowLimitChanged(const QString &text)
{
	if(setting)
	{
		return;
	}
	float value = text.toFloat(&valid);
	if(valid)
	{
		valid = value > 0.0;
	}
	if(valid)
	{
		DevListCriteria &criteria = DevListCriteria::getInstance();
		criteria.setTemperatureGrowLimit(value);
		setWidgetBackground(pTemperatureGrowLimitText, Qt::white);
	}
	else
	{
		setWidgetBackground(pTemperatureGrowLimitText, Qt::red);
	}
	emit changed();
}

void DevListCriteriaEditorWidget::pressureStartTimeChanged(const QDateTime &value)
{
	DevListCriteria::getInstance().setPressureStartTime(value);
	emit changed();
}

void DevListCriteriaEditorWidget::pressureEndTimeChanged(const QDateTime &value)
{
	DevListCriteria::getInstance().setPressureEndTime(value);
	emit changed();
}

void DevListCriteriaEditorWidget::pressureEndTimeNowChanged(bool value)
{
	DevListCriteria::getInstance().setPressureEndTimeNow(value);
	pPressureEndTimeEdit->setEnabled(!value);
	if(value)
	{
		adjustPressureTimes();
	}
	emit changed();
}


void DevListCriteriaEditorWidget::pressureSpikeIntervalChanged(int value)
{
	DevListCriteria::getInstance().setPressureSpikeInterval(value);
	emit changed();
}

void DevListCriteriaEditorWidget::temperatureStartTimeChanged(const QDateTime &value)
{
	DevListCriteria::getInstance().setTemperatureStartTime(value);
	emit changed();
}

void DevListCriteriaEditorWidget::temperatureEndTimeChanged(const QDateTime &value)
{
	DevListCriteria::getInstance().setTemperatureEndTime(value);
	emit changed();
}

void DevListCriteriaEditorWidget::temperatureEndTimeNowChanged(bool value)
{
	DevListCriteria::getInstance().setTemperatureEndTimeNow(value);
	pTemperatureEndTimeEdit->setEnabled(!value);
	if(value)
	{
		adjustTemperatureTimes();
	}
	emit changed();
}


void DevListCriteriaEditorWidget::temperatureSpikeIntervalChanged(int value)
{
	DevListCriteria::getInstance().setTemperatureSpikeInterval(value);
	emit changed();
}

void DevListCriteriaEditorWidget::setWidgetBackground(QWidget *pWidget, const QColor &color)
{
	QPalette palette(pWidget->palette());
	palette.setColor(QPalette::Window, color);
	pWidget->setPalette(palette);
}
