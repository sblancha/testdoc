//	Implementation of EqpState class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpState.h"

#include "EqpMsgCriteria.h"

#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

EqpState::EqpState(Eqp *pEqp, const ComplexCriteria &sourceCriteria) :
	criteria(sourceCriteria)
{
	pEqpList = new QList<Eqp *>();
	pEqpList->append(pEqp);
	match = false;
	activated = false;
}


EqpState::~EqpState()
{
	deactivate();
	delete pEqpList;
}

/*
**	FUNCTION
**		Connect to all required devices
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpState::activate(void)
{
/*
printf("EqpState::activate(), activated %d\n", (int)activated);
fflush(stdout);
*/
	if(activated)
	{
		return;
	}
	for(int idx = 0 ; idx < pEqpList->count() ; idx++)
	{
		Eqp *pEqp = pEqpList->at(idx);
/*
printf("EqpState::activate(), connecting to %s\n", pEqp->getName());
fflush(stdout);
*/
		pEqp->connect(this, DataEnum::Online);
		QObject::connect(pEqp, SIGNAL(mobileStateChanged(Eqp *, DataEnum::DataMode)),
			this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
		criteria.checkDevice(pEqp);
	}
	activated = true;
	match = criteria.matches();
}

/*
**	FUNCTION
**		Disconnect from all devices
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpState::deactivate(void)
{
/*
printf("EqpState::deactivate(), activated %d\n", (int)activated);
fflush(stdout);
*/
	if(!activated)
	{
		return;
	}
	for(int idx = 0 ; idx < pEqpList->count() ; idx++)
	{
		Eqp *pEqp = pEqpList->at(idx);
/*
printf("EqpState::deactivate(), disconnecting from %s\n", pEqp->getName());
fflush(stdout);
*/
		QObject::disconnect(pEqp, SIGNAL(mobileStateChanged(Eqp *, DataEnum::DataMode)),
			this, SLOT(mobileStateChange(Eqp *, DataEnum::DataMode)));
		pEqp->disconnect(this, DataEnum::Online);
	}
	activated = false;
}

/*
**	FUNCTION
**		Add device to list of devices to be checked
**
**	ARGUMENTS
**		pEqp	- Pointer to device to add
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpState::addEqp(Eqp *pEqp)
{
	pEqpList->append(pEqp);
}

/*
**	FUNCTION
**		Slot to be activated when one of equipment's DPEs have been changed.
**		Calculate new 'matching state' of this instance, emit signal if that
**		state has changed.
**
**	ARGUMENTS
**		pSrc	- Pointer to device - source of signal
**		dpeName	- DPE name
**		source	- DPE change source
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpState::dpeChange(Eqp *pSrc, const char * /* dpeName */,
		DataEnum::Source source, const QVariant & /* value */,
		DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	if(mode != DataEnum::Online)
	{
		return;
	}

	if(source == DataEnum::Plc)
	{
		return;	// Do not react on PLC state change
	}

	criteria.checkDevice(pSrc);
	bool newMatch = criteria.matches();
/*
printf("EqpState::dpeChange(%s.%s): match %d newMatch %d criteria %X\n",
pSrc->getName(), dpeName, (int)match, (int)newMatch, (unsigned)&criteria);
fflush(stdout);
*/

	if(newMatch != match)
	{
		match = newMatch;
/*
printf("EqpState::dpeChange(%s): emit match %d\n", pSrc->getName(), match);
fflush(stdout);
*/
		emit stateChanged(this, match);
	}
}

void EqpState::mobileStateChange(Eqp *pSrc, DataEnum::DataMode mode)
{
	if(mode != DataEnum::Online)
	{
		return;
	}

	criteria.checkDevice(pSrc);
	bool newMatch = criteria.matches();

	if(newMatch != match)
	{
		match = newMatch;
		emit stateChanged(this, match);
	}
}

/*
**	FUNCTION
**		Reset pending match state for all criteria, used to avoid sending
**		a lot of messages shortly after notification config is initialized
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpState::resetPendingMatch(void)
{
	criteria.resetPendingMatch();
	match = criteria.matches();
}

/*
**	FUNCTION
**		Check if pending match for this state has turned into real one.
**		Emit signal if this happened
**
**	ARGUMENTS
**		now	- current date+time
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpState::checkPendingMatch(const QDateTime &now)
{
	if(match)
	{
		return;
	}
	if(!criteria.checkPendingMatch(now))
	{
		return;
	}
	if(criteria.matches())
	{
		match = true;
		emit stateChanged(this, match);
	}		
}


