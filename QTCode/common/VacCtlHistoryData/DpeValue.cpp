//	Implementation of DpeValve class
/////////////////////////////////////////////////////////////////////////////////

#include "DpeValue.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/decsruction

DpeValue::DpeValue()
{
}

DpeValue::DpeValue(QDateTime tsIn, QVariant value) : ts(tsIn)
{
	this->value = value;
}

DpeValue::~DpeValue()
{
}

DpeValue & DpeValue::operator = (const DpeValue &other)
{
	ts = other.ts;
	value = other.value;
	return *this;
}

bool DpeValue::operator < (const DpeValue &other)
{
	return ts < other.ts;
}

