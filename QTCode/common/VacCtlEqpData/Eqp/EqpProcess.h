#ifndef	EQPPROCESS_H
#define	EQPPROCESS_H

//	Process equipment - driven by sequencer

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpProcess : public Eqp
{
	Q_OBJECT

public:
	EqpProcess(const Eqp &source);
#ifdef Q_OS_WIN
	EqpProcess(const EqpProcess &src) : Eqp(src) {}
#endif
	~EqpProcess();

	virtual void postProcess(void);
	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual float getMainValue(DataEnum::DataMode mode);
	virtual void convertValueToString(float value, QString &result, EqpPart part = EqpPartNone);
	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline unsigned getRR2(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr2Replay : rr2Online; }
	inline unsigned getRR4(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr4Replay : rr4Online; }
	virtual int getErrorCode(DataEnum::DataMode mode);

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR2 DPE for online mode
	unsigned	rr2Online;

	// Last known value of RR4 DPE for online mode
	unsigned	rr4Online;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for replay mode
	unsigned	rr2Replay;

	// Last known value of RR4 DPE for replay mode
	unsigned	rr4Replay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

	// Flag indicating if this device have RR4 DPE
	bool		haveRR4;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
#endif

	virtual void getStepName(int step, QString &result);
};

#endif	// EQPPROCESS_H
