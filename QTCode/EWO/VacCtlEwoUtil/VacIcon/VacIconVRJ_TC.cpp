//	Implementation of VacIconVRJ_TC class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVRJ_TC.h"

#include "EqpVRJ_TC.h"

#include "VacType.h"
// #include "FunctionalType.h"
#include "VacEqpTypeMask.h"

#include <QPainter>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_RED			255,0,0
#define	COLOR_YELLOW		255,255,0
#define	COLOR_BLUE			0,0,255

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVRJ_TC::VacIconVRJ_TC(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	nSlaves = 0;
	/*
#ifdef Q_OS_WIN
	QRect labelRect = fontMetrics().boundingRect("16*t�");
#else
	QRect labelRect = fontMetrics().boundingRect("16*t�");
#endif
	setFixedSize(labelRect.width() + 2, labelRect.height() + 2);
	*/
	setFixedSize(17, 17);
}

VacIconVRJ_TC::~VacIconVRJ_TC()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		It is supposed that this method will not be used for CRYO_TT
*/
VacIconGeometry VacIconVRJ_TC::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(width(), height(), width() >> 1, height() + 2, 1);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVRJ_TC::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVRJ_TC"), "VacIconVRJ_TC::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVRJ_TC *)pEqp;
		QStringList slaveNames;
		pEqp->getSlaves(slaveNames);
		nSlaves = slaveNames.count();
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVRJ_TC::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVRJ_TC::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("Status");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVRJ_TC::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVRJ_TC::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVRJ_TC::paintEvent(QPaintEvent * /* pEvent */)
{
	/*
	QString	label = "???";
	if(pEqp)
	{
		label = QString::number(nSlaves);
		label += "*t�";
	}
	*/
	QPainter painter(this);
	QRect myRect = rect();
	QColor color;
	if(pEqp)
	{
		pEqp->getMainColor(color, mode);
	}
	else
	{
		color.setRgb(COLOR_BLUE);
	}
	painter.setPen(color);
	painter.fillRect(myRect, color);
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
	painter.setPen(Qt::black);
	/*
	QRect labelRect = painter.fontMetrics().boundingRect(label);
	painter.drawText(myRect.center().x() - (labelRect.width() >> 1),
		myRect.center().y() + (labelRect.height() >> 1),
		label);
	*/
	painter.setBrush(Qt::black);
	painter.fillRect(6, 1, 2, 13, Qt::SolidPattern);
	/*
	painter.fillRect(7, 2, 4, 2, Qt::SolidPattern);
	painter.fillRect(7, 5, 4, 2, Qt::SolidPattern);
	painter.fillRect(7, 8, 4, 2, Qt::SolidPattern);
	*/
	painter.drawLine(8, 2, 10, 2);
	painter.drawLine(8, 5, 10, 5);
	painter.drawLine(8, 8, 10, 8);
	painter.drawEllipse(4, 10, 5, 5);
}

