#ifndef	PROFILETHERMOMETER_H
#define	PROFILETHERMOMETER_H

// Class holding information for one CRYO thermometer in profile line
// Thermometers are drawn differently compared to vacuum equipment and they
// need another set of data

#include "InterfaceEqp.h"

#include <qpainter.h>
#include <qcolor.h>
#include <qpoint.h>

#include <stdio.h>

class	Eqp;

class ProfileThermometer : public InterfaceEqp
{
	Q_OBJECT

public:
	ProfileThermometer(Eqp *pEqp, DataEnum::DataMode mode);
	~ProfileThermometer();

	void show(void);
	void hide(void);
	void draw(QPainter &painter);
	QRect getRect(void);

	void dump(FILE *pFile, int idx);

	// Access
	inline Eqp *getEqp(void) const { return pEqp; }
	inline float getStart(void) const { return start; }
	inline void setStart(float coord) { start = coord; }
	inline float getRealStart(void) const { return realStart; }
	inline QPoint &getPoint(void) { return point; }
	inline int getSubType(void) const { return subType; }
	inline bool isHidden(void) const { return hidden; }
	inline float getValue(void) const { return value; }
	inline bool isValid(void) const { return valid; }

public slots:	// Implement slots of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);
	virtual void selectChange(Eqp *pSrc);

signals:	// Define own signals
	void eqpDataChanged(ProfileThermometer *pSrc);
	void eqpSelectChanged(bool linear);

protected:
	// Pointer to device
	Eqp		*pEqp;

	// Coordinates for temperature value in line
	QPoint	point;

	// Last known color of device
	QColor	color;

	// Start coordinate of temperature area (== sector) for calculations
	float	start;

	// Real start coordinate in DCUM coordinate system
	float		realStart;

	// Temperature value
	float		value;

	// Data acquisition mode
	// (in fact, it is the same value for all items in one graph, but
	// this item needs to know the mode
	DataEnum::DataMode		mode;

	// Subtype of device - different thermometers
	int			subType;

	// Flag for value validity
	bool		valid;

	// true if device is hidden
	bool		hidden;

	// true if device is selected
	bool		selected;
};

#endif	// PROFILETHERMOMETER_H
