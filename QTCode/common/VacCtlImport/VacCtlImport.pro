#
# Project file for qmake utility to build Makefile for VacImport DLL
#
#	02.10.2008	L.Kopylov
#		Initial version
#
#	13.12.2008	L.Kopylov
#		Rename target library
#
#
#	02.02.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	10.02.2010	L.Kopylov
#		Changes for DLL on Windows
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

TEMPLATE = lib

TARGET = VacCtlImport

unix:MAKEFILE = GNUmakefile

unix:VERSION = 1.0.1

# To avoid having version numbers in .so file name
unix:CONFIG += plugin

DESTDIR = ../../../bin

OBJECTS_DIR = obj

unix:CONFIG += qt thread release warn_on
win32:CONFIG += qt dll thread release

# To produce PDB file together with DLL
win32:QMAKE_CXXFLAGS+=/Zi
win32:QMAKE_LFLAGS+= /INCREMENTAL:NO /Debug


DEFINES += BUILDING_VACCTLIMPORT

#DEFINES += _VACDEBUG

INCLUDEPATH += ../VacCtlUtil \
				../Platform

HEADERS = ../Platform/PlatformDef.h \
	../VacCtlUtil/NamePool.h \
	../VacCtlUtil/FileParser.h \
	VacCtlImportExport.h \
	ImportConstants.h \
	Archive.h \
	S7Connection.h \
	PollGroup.h \
	DpeData.h \
	DpData.h \
	DriverRef.h \
	ImportPool.h


SOURCES = Archive.cpp \
	S7Connection.cpp \
	PollGroup.cpp \
	DpeData.cpp \
	DpData.cpp \
	DriverRef.cpp \
	ImportPool.cpp


LIBS += -L../../../bin \
	-lVacCtlUtil
