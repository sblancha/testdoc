//	Implementation of SectLhcLinePassive class
/////////////////////////////////////////////////////////////////////////////////

#include "SectLhcLinePassive.h"

#include "DataPool.h"
#include "Eqp.h"
#include "EqpType.h"

/*
**	FUNCTION
**		Check if given device shall appear in this synoptic line. Coordinate
**		of device is NOT checked.
**
**	PARAMETERS
**		pEqp	- Pointer to device in question
**		partIdx	- Index of line part, where equipment is located, will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
bool SectLhcLinePassive::isMyEqp(Eqp *pEqp, int &partIdx)
{
	if(hideMagnets)
	{
		return false;
	}
	if(pEqp->isSkipOnSynoptic())
	{
		return false;
	}
	if(pEqp->getType() == EqpType::Passive)
	{
		if(pEqp->getFunctionalType() == FunctionalType::None)
		{
			return false;
		}
	}
	else
	{
		if(!pEqp->isValveInterlock())
		{
			return false;
		}
	}

	// Find in which part equipment is located
	partIdx = findPartIdx(pEqp);
	if(pEqp->isValveInterlock())
	{
		return true;
	}
	return ! DataPool::getInstance().isEqpInTheArc(pEqp);
}
