#ifndef	VACICONVG_DP_VGM_H
#define	VACICONVG_DP_VGM_H

//	Icon for VG_DP_VGM - very similar to one for VG_STD - VGM

#include "VacIconVG_STD.h"

class VACCTLEWOUTIL_EXPORT VacIconVG_DP_VGM : public VacIconVG_STD
{
//	Q_OBJECT

public:
		VacIconVG_DP_VGM(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG_STD(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVG_DP_VGM_H
