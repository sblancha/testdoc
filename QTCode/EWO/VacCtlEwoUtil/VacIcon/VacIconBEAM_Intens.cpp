//	Implementation of VacIconBEAM_Intens class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconBEAM_Intens.h"

#include "EqpBEAM_Intens.h"

#include "VacType.h"
// #include "FunctionalType.h"
#include "VacEqpTypeMask.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_RED			255,0,0
#define	COLOR_BLUE			0,0,255

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconBEAM_Intens::VacIconBEAM_Intens(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
#ifdef Q_OS_WIN
	QRect labelRect = fontMetrics().boundingRect("9.99E+009");
#else
	QRect labelRect = fontMetrics().boundingRect("9.99E+09");
#endif
	setFixedSize(labelRect.width() + 2, labelRect.height() + 2);
}

VacIconBEAM_Intens::~VacIconBEAM_Intens()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		It is supposed that this method will not be used for CRYO_TT
*/
VacIconGeometry VacIconBEAM_Intens::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(width(), height(), width() >> 1, height() >> 1, 1);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconBEAM_Intens::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpBEAM_Intens"), "VacIconBEAM_Intens::setEqp", pEqp->getDpName());
		this->pEqp = (EqpBEAM_Intens *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconBEAM_Intens::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconBEAM_Intens::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("Intensity");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconBEAM_Intens::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconBEAM_Intens::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconBEAM_Intens::paintEvent(QPaintEvent * /* pEvent */)
{
	QString	label = "???";
	if(pEqp)
	{
		pEqp->getMainStateString(label, mode);
	}
	QPainter painter(this);
	QRect myRect = rect();
	painter.eraseRect(myRect);
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}

	QColor color;
	if(pEqp)
	{
		switch(pEqp->getVacType())
		{
		case VacType::BlueBeam:
			color.setRgb(COLOR_BLUE);
			break;
		case VacType::RedBeam:
			color.setRgb(COLOR_RED);
			break;
		default:
			break;
		}
	}
	painter.setPen(color);
	QRect labelRect = painter.fontMetrics().boundingRect(label);
	painter.drawText(myRect.center().x() - (labelRect.width() >> 1),
		myRect.center().y() + (labelRect.height() >> 1),
		label);
}

