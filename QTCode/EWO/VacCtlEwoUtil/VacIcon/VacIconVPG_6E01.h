#ifndef	VACICONVPG_6E01_H
#define	VACICONVPG_6E01_H

// Icon for VPG_6E01

#include "VacIcon.h"

class EqpProcess;

class VACCTLEWOUTIL_EXPORT VacIconVPG_6E01 : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPG_6E01(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPG_6E01();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual void setPipeColor(const QColor &newColor);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint & /* pos */, QString & /* text */, QRect & /* rect */) { return false; }	// no tooltip

	// Override method of VacIcon 
	virtual void setDirection(VacIconContainer::Direction newDirection);

protected:
	// Pointer to my device
	EqpProcess *pEqp;

	// Areas for pump, valve and gauge symbols. Drawing can be easy using QPainter
	// coordinate transformations, but finding areas for tooltips is not so
	// evident. May be it is more convenient just to keep in meory coordinates
	// of all distinct areas

	QRect	pump;
	QRect	vvr1;
	QRect	vvr2;
	QRect	vgr;
	QRect	vgp;
	QRect	vgf;

	VacIcon	*pIconVPG;
	VacIcon *pIconVVR1;
	VacIcon *pIconVVR2;
	VacIcon *pIconVGR;
	VacIcon *pIconVGP;
	VacIcon	*pIconVGF;

	VacIconContainer::Direction calculateVvrDirection(void);
	VacIconContainer::Direction calculateVgrDirection(void);
	VacIconContainer::Direction calculateVgpDirection(void);
	void setAreaGeometry(void);

	virtual void paintEvent(QPaintEvent *pEvent);

	// Override VacIcon's reaction on mouse press
	virtual void mousePressEvent(QMouseEvent *pEvent);
};

#endif	// VACICONVPG_6E01_H
