/**@name SCRIPT: vcsValveStateReport.ctl

@author: Mikhail Mikheev (IHEP, Protvino, Russia)

Creation Date: 2/11/2010

version 1.0

Modification History:
Purpose:  Valve state report script
Dedicated to analyze valve state changes and prapare the list to be shown on a web
*/
#uses "VacCtlEqpDataPvss"	// Load DLL
#uses "eqp/vclVV.ctl"	// Load CTRL library
#uses "vclSectors.ctl"	// Load CTRL library
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "vclDevCommon.ctl"	// Load CTRL library
#uses "vclResources.ctl"   // Load CTRL library
#uses "vclMachine.ctl"		// Load CTRL library

dyn_string     valvesList;
dyn_uint       valvesStateList; 
const unsigned  VLV_UNDEF =       0x0400u,
                VLV_VALID =       0x0200u,
                VLV_ERRORS =      0x0100u,
                VLV_WARNINGS =    0x0080u,
                VLV_INTRRLOCK_DPA = 0x0040u,
                VLV_INTRRLOCK_DPB = 0x0020u,
                VLV_INTRRLOCK_LA = 0x0010u,
                VLV_INTRRLOCK_LB = 0x0008u,
                VLV_INTRRLOCK_GA = 0x0004u,
                VLV_INTRRLOCK_GB = 0x0002u,
                VLV_CLOSED =      0x0001u;

const string  VLV_VALID_MSG =         "NVAL", 
              VLV_ERRORS_MSG =      "ERR",
	            VLV_WARNINGS_MSG =    "WARN",
	            VLV_INTRRLOCK_GA_MSG = "IN_GA",
	            VLV_INTRRLOCK_GB_MSG = "IN_GB",
	            VLV_INTRRLOCK_LA_MSG = "IN_LA",
	            VLV_INTRRLOCK_LB_MSG = "IN_LB",
	            VLV_CIL_MSG =         "CINT",
	            VLV_CLOSED_MSG =      "CLSD", 
	            VLV_UNDEF_MSG =       "UDEF"; 
int processingInterval = 10; // 60
// Name of DP type containing time configuration
  const string VLV_REPORT_TIMED_DP_TYPE = "_TimedFunc";

// Name of DP containing time configuration
  const string VLV_REPORT_TIMED_DP = "_VLVReportTd";


main()
{
  dyn_string exceptionInfo; 
	string timedDpName = "";
	if( !dpExists( VLV_REPORT_TIMED_DP ) )
	{
	  if( dpCreate( VLV_REPORT_TIMED_DP, VLV_REPORT_TIMED_DP_TYPE ) < 0 )
	  {
		  fwException_raise(exceptionInfo, "vcsSpsValvaStateWebReport.ctl::main() ERROR", " Time config DP dpCreate() failed", "" );
		  return;
	  }
	  if( ! dpExists( VLV_REPORT_TIMED_DP ) )
	  {
		  fwException_raise(exceptionInfo, "vcsSpsValvaStateWebReport.ctl::main() ERROR", "Time config DP not exist", "" );
		  return;
	  }
	}
	timedDpName = VLV_REPORT_TIMED_DP;
Debug("timedDpName", timedDpName);  
//Configure  timed dp to perform calculation with processingInterval interval
  dpSet( timedDpName + ".validFrom", makeTime(2006,6,1),
      	timedDpName + ".validUntil", makeTime(2032,6,1),
         timedDpName + ".time", makeDynInt(), 				
         timedDpName + ".monthDay", makeDynInt(), 				
         timedDpName + ".month", makeDynInt(), 				
         timedDpName + ".weekDay", makeDynInt(), 			
         timedDpName + ".month", makeDynInt(), 				
     		timedDpName + ".delay", 0,
      	timedDpName + ".interval", processingInterval,			//Interval  in sec
     		timedDpName + ".syncDay", -1,
     		timedDpName + ".syncWeekDay", -1,
    		timedDpName + ".syncTime", -1, 
     		timedDpName + ".syncMonth", -1);
   InitStaticData( );     
// 	timedFunc ("CheckValeState", timedDpName);	
  DebugTN("vcsSpsValvaStateWebReport.ctl::main():Init completed");        
  CheckValeState();
  }

void CheckValeState()
{
  unsigned rr1, rr2, state = 0, state1, state2;
  string dpeRr1Name, dpeRr2Name, dpName, valveName;
  dyn_string mpList, allValveList, exceptionInfo;
    LhcVacGetAllMainParts(mpList);
    int nMps = dynlen(mpList);
    for(int mpIdx = 1 ; mpIdx <= nMps ; mpIdx++)
    {
      dyn_string sectList;
      LhcVacGetSectorsOfMainPart(mpList[mpIdx], sectList);
      int nSectors = dynlen(sectList);
//DebugN("nSectors", nSectors);   
      for(int sectIdx = 1 ; sectIdx <= nSectors ; sectIdx++)
      {
        string sectName = sectList[sectIdx];
        dyn_string valveDps, exceptionInfo;
        
        LhcVacGetDevicesOfDpTypesAtSectors(makeDynString(sectName), makeDynString("VVS_S","VVS_SV"),
        valveDps, exceptionInfo);
        int nValves = dynlen(valveDps);
        for(int eqpIdx = 1 ; eqpIdx <=  nValves ; eqpIdx++)
        {
          string dpName = valveDps[eqpIdx], eqpName;
          if(dynContains(allValveList, dpName) > 0)
          {
            continue;  // Already processed
          }
          dynAppend(allValveList, dpName);
          dpGet(dpName + ".RR1", rr1);
          dpGet(dpName + ".RR2", rr2);
        }
      }  
    }
    int nValves = dynlen(allValveList);
 DebugN("nValves", nValves);
 dynClear(valvesList);
 dynClear(valvesStateList);

  for(int i = 1; i <= nValves; i++)
  {
    state = 0;
    dpName = dpSubStr(allValveList[i], DPSUB_DP);
    dpeRr1Name = dpName + ".RR1";
    dpeRr2Name = dpName + ".RR2";
    dpGet(dpeRr1Name, rr1, dpeRr2Name, rr2);
    if ((!(rr1 & SVCU_SPS_R_CLOSED) && !(rr1 & SVCU_SPS_R_OPEN)) ||
               ((rr1 & SVCU_SPS_R_CLOSED) && (rr1 & SVCU_SPS_R_OPEN)))
      state += VLV_UNDEF;
    if((((rr1 & SVCU_SPS_R_CLOSED) && !(rr1 & SVCU_SPS_R_OPEN)) ))
      state += VLV_CLOSED;
 unsigned state1 = rr1; 
 unsigned state2 = rr2 &	
  SVCU_SPS_R_CIR_DP_AFTER +
	SVCU_SPS_R_CIR_DP_BEFORE +
	SVCU_SPS_R_CIR_GBL_AFTER  +
	SVCU_SPS_R_CIR_GBL_BEFORE +
	SVCU_SPS_R_CIR_LCL_AFTER  +
	SVCU_SPS_R_CIR_LCL_BEFORE;
   

  if(state2  &	SVCU_SPS_R_CIR_LCL_BEFORE ) state += VLV_INTRRLOCK_LB;
  if(state2  &	SVCU_SPS_R_CIR_LCL_AFTER) state += VLV_INTRRLOCK_LA;
  if(state2  &	SVCU_SPS_R_CIR_GBL_AFTER) state += VLV_INTRRLOCK_GB;
  if(state2  &	SVCU_SPS_R_CIR_GBL_BEFORE) state += VLV_INTRRLOCK_GA;
  if(state2  &	SVCU_SPS_R_CIR_DP_BEFORE) state += VLV_INTRRLOCK_DPB;
  if(state2  &	SVCU_SPS_R_CIR_DP_BEFORE) state += VLV_INTRRLOCK_DPA;
  if(rr1 & SVCU_SPS_R_WARNINGS) state += VLV_WARNINGS;
  if(rr1 & SVCU_SPS_R_ERRORS) state += VLV_ERRORS;
  if(!(rr1 & SVCU_SPS_R_VALID)) state += VLV_VALID;
  if(state)
  {
    LhcVacDisplayName(dpSubStr(dpName, DPSUB_DP), valveName, exceptionInfo);
    dynAppend(valvesList, valveName);    
    dynAppend(valvesStateList, state);    
DebugN("StateToReport:", valveName, state);
    }
 }
 PrepareReport();
 dynClear(valvesList);
 dynClear(valvesStateList);
}
PrepareReport()
{
	int			err;
	string 		fileName;
	file    		fileToPrint;
	string		buf,  strToPrint;
	time			curTime;
   dyn_dyn_string report;
   int nValves = dynlen(valvesList);
   string valveMsg;
   for(int i = 1; i <= nValves; i++)
   {
     int nLevel = 0;
     valveMsg = valvesList[i];
 	  if(valvesStateList[i] & VLV_UNDEF)
     {       
       valveMsg += " ," + VLV_UNDEF_MSG;     
          nLevel = 9;   
     }
     else
       valveMsg += " , XXX";            
     if(valvesStateList[i] & VLV_INTRRLOCK_LB)
     {       
       valveMsg += " ," + VLV_INTRRLOCK_LB_MSG;
       nLevel = 8;   
     }
     else
       valveMsg += " , XXX";            
	  if(valvesStateList[i] & VLV_INTRRLOCK_LA)
     {       
       valveMsg += " ," + VLV_INTRRLOCK_LA_MSG;     
       if(nLevel == 0) 
          nLevel = 7;   
     }
     else
       valveMsg += " , XXX";            
     if(valvesStateList[i] & VLV_INTRRLOCK_GB)
     {       
       valveMsg += " ," + VLV_INTRRLOCK_GB_MSG;     
       if(nLevel == 0) 
          nLevel = 6;   
     }
     else
       valveMsg += " , XXX";            
     if(valvesStateList[i] & VLV_VALID)
     {       
       valveMsg += " ," + VLV_VALID_MSG;     
       if(nLevel == 0) 
          nLevel = 5;   
     }
     else
       valveMsg += " , XXX";            
     if(valvesStateList[i] & VLV_ERRORS)
     {       
       valveMsg += " ," + VLV_ERRORS_MSG;     
       if(nLevel == 0) 
          nLevel = 4;   
     }
     else
       valveMsg += " , XXX";            
	  if(valvesStateList[i] & VLV_WARNINGS)
     {       
       valveMsg += " ," + VLV_WARNINGS_MSG;     
       if(nLevel == 0) 
          nLevel = 3;   
     }
     else
       valveMsg += " , XXX";            
 	  if(valvesStateList[i] & VLV_CLOSED)
     {       
       valveMsg += " ," + VLV_CLOSED_MSG;     
       if(nLevel == 0) 
          nLevel = 1;   
     }
     else
       valveMsg += " , XXX";            
//DebugN(valveMsg);
     dynAppend(report[nLevel+1], valveMsg);     
  }  

   fileName = "C:\\vacWebMonitoring\\Screenshots\\Tables\\vlvSpsList.txt";  
//   fileName = "C:\\tmp\\vlvSpsList.txt";  

   fileToPrint = fopen (fileName, "w+" );
	err = ferror(fileToPrint); // export error
	if (err!=0)
	{
		DebugN("vcsSpsValveStateReport.ctl::Open file: Error no. ",err," occurred");
		return;
	}
	curTime = getCurrentTime();
	buf = "List of valve states created at ";
	buf += formatTime("%Y/%m/%d  %H:%M:%S", curTime) + "\n";
//DebugN(report);
   fputs(buf, fileToPrint); 
   for(int i = dynlen(report); i >= 1; i--)
   {
     int rLen = dynlen(report[i]);
     for(int nn = 1; nn <= rLen; nn++)
     {
	    fputs(report[i][nn], fileToPrint); 
	    fputs("\n", fileToPrint); 
     }
   }  
   fclose(fileToPrint); // close file
}
   
/**InitStaticData
Purpose:
Read all machine configuration data from file(s)

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool InitStaticData( )
{
	dyn_string	exceptionInfo, mainParts;
	long		coco;
	bool		isLhcData;
	// Set accelerator name to be used by other components
	LhcVacGetMachineId( exceptionInfo );
	if( dynlen( exceptionInfo ) > 0 )
	{
		DebugTN( "vscSpsVavleStateWebReport.ctl: Failed to read machine name: " + exceptionInfo );
		return false;
	}
	DebugTN( "vscSpsVavleStateWebReport.ctl: Machine is " + glAccelerator );
	VacResourcesParseFile( PROJ_PATH + "/data/VacResources.txt", glAccelerator, exceptionInfo );
	isLhcData = glAccelerator == "LHC" ? true : false;
	// Initalize passive machine data
	coco = LhcVacEqpInitPassive( isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo );
	if( coco < 0 )
	{
		DebugTN( "vscSpsVavleStateWebReport.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo );
		return false;
	}
	DebugTN( "vscSpsVavleStateWebReport.ctl: LhcVacEqpInitPassive() done" );

	// Initialzie active equipment information
	coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
			exceptionInfo );
	if( coco < 0 )
	{
		DebugTN( "vscSpsVavleStateWebReport.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo );
		return false;
	}
	DebugTN( "vscSpsVavleStateWebReport.ctl: LhcVacEqpInitActive() done" );

	DebugTN( "vscSpsVavleStateWebReport.ctl: InitStaticData() done" );
	LhcVacInitDevFunc();
	return true;
}
