#
# Project file for qmake utility to build Makefile for VacCtlOnlineNotification DLL
#
#	13.09.2009	L.Kopylov
#		Initial version
#
#	02.02.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	10.02.2010	L.Kopylov
#		Changes for DLL on Windows
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

TEMPLATE = lib

TARGET = VacCtlOnlineNotification

unix:MAKEFILE = GNUmakefile

unix:VERSION = 1.0.1

# To avoid having version numbers in .so file name
unix:CONFIG += plugin

DESTDIR = ../../../bin

OBJECTS_DIR = obj

unix:CONFIG += qt thread release warn_on
win32:CONFIG += qt dll thread release

# To produce PDB file together with DLL
win32:QMAKE_CXXFLAGS+=/Zi
win32:QMAKE_LFLAGS+= /INCREMENTAL:NO /Debug


DEFINES += BUILDING_VACCTLONLINENOTIFICATION

#DEFINES += _VACDEBUG

INCLUDEPATH += ../Platform \
	../VacCtlUtil \
	../VacCtlEqpData \
	../VacCtlEqpData/FunctionalType \
	../VacCtlEqpData/DataPool \
	../VacCtlEqpData/ResourcePool \
	../VacCtlEqpData/Eqp

HEADERS = VacCtlOnlineNotificationExport.h \
		ComplexCriteria.h \
		EqpState.h \
		EqpMessagePart.h \
		EqpMessage.h \
		FinalEqpMessage.h \
		EqpUsageFilterItem.h \
		EqpUsageFilter.h \
		MessageQueueItem.h \
		MessageQueue.h \
		MessageGroup.h \
		MessageFilteringRule.h \
		NotificationConfig.h \
		SmsConfig.h \
		SmsConfigParser.h


SOURCES = ComplexCriteria.cpp \
		EqpState.cpp \
		EqpMessagePart.cpp \
		EqpMessage.cpp \
		FinalEqpMessage.cpp \
		EqpUsageFilterItem.cpp \
		EqpUsageFilter.cpp \
		MessageQueueItem.cpp \
		MessageQueue.cpp \
		MessageGroup.cpp \
		MessageFilteringRule.cpp \
		NotificationConfig.cpp \
		SmsConfig.cpp \
		SmsConfigParser.cpp


LIBS += -L../../../bin \
	-lVacCtlEqpData \
	-lVacCtlHistoryData \
	-lVacCtlUtil
