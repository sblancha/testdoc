//	Implementation of ComplexCriteria class
/////////////////////////////////////////////////////////////////////////////////

#include "ComplexCriteria.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "Eqp.h"

const char *ComplexCriteria::joinTypeToString(int type)
{
	switch(type)
	{
	case None:
		return "None";
	case And:
		return "AND";
		break;
	case Or:
		return "OR";
		break;
	default:
		break;
	}
	static char result[32];
#ifdef Q_OS_WIN
	sprintf_s(result, sizeof(result) / sizeof(result[0]), "%d", type);
#else
	sprintf(result, "%d", type);
#endif
	return result;
}

int ComplexCriteria::joinTypeParse(const char *string, QString &errMsg)
{
	int result = 0;
	if(!string)
	{
		errMsg += "NULL join type";
	}
	else if(!(*string))
	{
		errMsg += "Empty join type";
	}
	else if(!strcasecmp(string, "none"))
	{
		result = None;
	}
	else if(!strcasecmp(string, "and"))
	{
		result = And;
	}
	else if(!strcasecmp(string, "or"))
	{
		result = Or;
	}
	else
	{
		errMsg += "Unsupported join type <";
		errMsg += string;
		errMsg += ">";
	}
	return result;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

ComplexCriteria::ComplexCriteria()
{
	joinType = None;
	pSimpleCriteriaList = new QList<EqpMsgCriteria *>();
	pComplexCriteriaList = new QList<ComplexCriteria *>();
}

ComplexCriteria::ComplexCriteria(int type)
{
	this->joinType = type;
	pSimpleCriteriaList = new QList<EqpMsgCriteria *>();
	pComplexCriteriaList = new QList<ComplexCriteria *>();
}

ComplexCriteria::ComplexCriteria(const ComplexCriteria &source)
{
	joinType = source.joinType;
	pSimpleCriteriaList = new QList<EqpMsgCriteria *>();
	pComplexCriteriaList = new QList<ComplexCriteria *>();
	int idx;
	for(idx = 0 ; idx < source.pSimpleCriteriaList->count() ; idx++)
	{
		pSimpleCriteriaList->append(new EqpMsgCriteria(*(source.pSimpleCriteriaList->at(idx))));
	}
	for(idx = 0 ; idx < source.pComplexCriteriaList->count() ; idx++)
	{
		pComplexCriteriaList->append(new ComplexCriteria(*(source.pComplexCriteriaList->at(idx))));
	}
}

ComplexCriteria::~ComplexCriteria()
{
	while(!pComplexCriteriaList->isEmpty())
	{
		delete pComplexCriteriaList->takeFirst();
	}
	delete pComplexCriteriaList;
	while(!pSimpleCriteriaList->isEmpty())
	{
		delete pSimpleCriteriaList->takeFirst();
	}
	delete pSimpleCriteriaList;
}

ComplexCriteria & ComplexCriteria::operator = (const ComplexCriteria &source)
{
	joinType = source.joinType;
	while(!pSimpleCriteriaList->isEmpty())
	{
		delete pSimpleCriteriaList->takeFirst();
	}
	while(!pComplexCriteriaList->isEmpty())
	{
		delete pComplexCriteriaList->takeFirst();
	}
	int idx;
	for(idx = 0 ; idx < source.pSimpleCriteriaList->count() ; idx++)
	{
		pSimpleCriteriaList->append(new EqpMsgCriteria(*(source.pSimpleCriteriaList->at(idx))));
	}
	for(idx = 0 ; idx < source.pComplexCriteriaList->count() ; idx++)
	{
		pComplexCriteriaList->append(new ComplexCriteria(*(source.pComplexCriteriaList->at(idx))));
	}
	return *this;
}


/*
**	FUNCTION
**		Write to array all functional types which are used in this crtiria
**
**	ARGUMENTS
**		typeArray	- Address of pointer to array of functional types,
**						may be updated by this method
**		nTypes		- Number of types in array, may be updated by this method
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ComplexCriteria::getFunctionalTypes(int **typeArray, int &nTypes)
{
	int idx;
	for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
	{
		EqpMsgCriteria *pEqpCriteria = pSimpleCriteriaList->at(idx);
		addFunctionalTypeToArray(pEqpCriteria->getFunctionalType(), typeArray, nTypes);
	}
	for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
	{
		ComplexCriteria *pComplexCriteria = pComplexCriteriaList->at(idx);
		pComplexCriteria->getFunctionalTypes(typeArray, nTypes);
	}
}

/*
**	FUNCTION
**		Find criteria for value - in order to use limit(s) as part of final
**		message to be sent
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Pointer to first value criteria; or
**		NULL if value criteria is not found
**
**	CAUTIONS
**		None
*/
EqpMsgCriteria *ComplexCriteria::getValueCriteria(void)
{
	int idx;
	EqpMsgCriteria *pEqpCriteria;
	for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
	{
		pEqpCriteria = pSimpleCriteriaList->at(idx);
		if(pEqpCriteria->getType() == EqpMsgCriteria::MainValue)
		{
			return pEqpCriteria;
		}
	}
	for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
	{
		ComplexCriteria *pComplexCriteria = pComplexCriteriaList->at(idx);
		pEqpCriteria = pComplexCriteria->getValueCriteria();
		if(pEqpCriteria)
		{
			return pEqpCriteria;
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Check if given functional type is in array of types, if not - add it
**		to array
**
**	ARGUMENTS
**		type		- functional type to check/add
**		typeArray	- Address of pointer to array of functional types,
**						may be updated by this method
**		nTypes		- Number of types in array, may be updated by this method
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ComplexCriteria::addFunctionalTypeToArray(int type, int **typeArray, int &nTypes)
{
	for(int n = nTypes - 1 ; n >= 0 ; n--)
	{
		if((*typeArray)[n] == type)
		{
			return;
		}
	}
	*typeArray = (int *)realloc((void *)*typeArray, (nTypes + 1) * sizeof(int));
	(*typeArray)[nTypes++] = type;
}


/*
**	FUNCTION
**		Check if given device matches this criteria
**
**	ARGUMENTS
**		pEqp				- Pointer to device to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ComplexCriteria::checkDevice(Eqp *pEqp)
{
	int idx;
	for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
	{
		EqpMsgCriteria *pEqpCriteria = pSimpleCriteriaList->at(idx);
		pEqp->checkCriteria(pEqpCriteria);
	}
	for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
	{
		ComplexCriteria *pComplexCriteria = pComplexCriteriaList->at(idx);
		pComplexCriteria->checkDevice(pEqp);
	}
}

/*
**	FUNCTION
**		Check if the whole criteria is fulfilled
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- criteria is fulfilled'
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool ComplexCriteria::matches(void)
{
	bool criteriaMatches = false;
	int idx;
	for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
	{
		EqpMsgCriteria *pEqpCriteria = pSimpleCriteriaList->at(idx);
/*
printf("ComplexCriteria::matches(): name %s matches %d\n", pEqpCriteria->getName().ascii(), (int)pEqpCriteria->isMatch());
fflush(stdout);
*/
		criteriaMatches = pEqpCriteria->isMatch();
		if(criteriaMatches)
		{
			if(joinType != And)
			{
				return true;
			}
		}
		else if(joinType == And)
		{
			return false;
		}
	}

	for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
	{
		ComplexCriteria *pComplexCriteria = pComplexCriteriaList->at(idx);
		criteriaMatches = pComplexCriteria->matches();
		if(criteriaMatches)
		{
			if(joinType != And)
			{
				return true;
			}
		}
		else if(joinType == And)
		{
			return false;
		}
	}

	return criteriaMatches;
}

/*
**	FUNCTION
**		Reset pending match state for all criteria, used to avoid sending
**		a lot of messages shortly after notification config is initialized
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ComplexCriteria::resetPendingMatch(void)
{
	int idx;
	for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
	{
		EqpMsgCriteria *pEqpCriteria = pSimpleCriteriaList->at(idx);
		pEqpCriteria->resetPendingMatch();
	}

	for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
	{
		ComplexCriteria *pComplexCriteria = pComplexCriteriaList->at(idx);
		pComplexCriteria->resetPendingMatch();
	}
}

/*
**	FUNCTION
**		Check if pending 'matches' state for at least one of criteria has been
**		turned into real 'matches' state
**
**	ARGUMENTS
**		now	- current date + time
**
**	RETURNS
**		true	- at least one of pending matches has been turned into real one
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool ComplexCriteria::checkPendingMatch(const QDateTime &now)
{
	bool result = false;
	int idx;
	for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
	{
		EqpMsgCriteria *pEqpCriteria = pSimpleCriteriaList->at(idx);
		if(pEqpCriteria->checkPendingMatch(now))
		{
			result = true;
		}
	}

	for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
	{
		ComplexCriteria *pComplexCriteria = pComplexCriteriaList->at(idx);
		if(pComplexCriteria->checkPendingMatch(now))
		{
			result = true;
		}
	}

	return result;
}

const QDateTime ComplexCriteria::getEventTime(void) const
{
/*
printf("ComplexCriteria::getEventTime(): this %X \n", (unsigned)this);
fflush(stdout);
*/
	QDateTime result;
	EqpMsgCriteria *pEqpCriteria;
	ComplexCriteria *pComplexCriteria;
	int idx;
	if(joinType == And)	// Take the latest timestamp
	{
		result = QDateTime::currentDateTime().addYears(-10);	// Far in the past
		for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
		{
			pEqpCriteria = pSimpleCriteriaList->at(idx);
/*
printf("ComplexCriteria::getEventTime(): simple AND %X matchByValue %d eventTime %s\n",
(unsigned)pEqpCriteria, (int)pEqpCriteria->isMatchByValue(), pEqpCriteria->getEventTime().toString("yyyy-MM-dd hh:mm:ss.zzz").ascii());
fflush(stdout);
*/
			if(pEqpCriteria->getEventTime() > result)
			{
				result = pEqpCriteria->getEventTime();
			}
		}

		for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
		{
			pComplexCriteria = pComplexCriteriaList->at(idx);
			if(pComplexCriteria->getEventTime() > result)
			{
				result = pComplexCriteria->getEventTime();
			}
		}
	}
	else	// take the earliest timestamp
	{
		result = QDateTime::currentDateTime().addYears(10);	// Far in the future
		for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
		{
			pEqpCriteria = pSimpleCriteriaList->at(idx);
			if(pEqpCriteria->isMatch())
			{
				if(pEqpCriteria->getEventTime() < result)
				{
					result = pEqpCriteria->getEventTime();
				}
			}
		}

		for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
		{
			pComplexCriteria = pComplexCriteriaList->at(idx);
			if(pComplexCriteria->matches())
			{
				if(pComplexCriteria->getEventTime() < result)
				{
					result = pComplexCriteria->getEventTime();
				}
			}
		}
	}
	return QDateTime(result);
}

float ComplexCriteria::getEventValue(void) const
{
/*
printf("ComplexCriteria::getEventValue(): this %X \n", (unsigned)this);
fflush(stdout);
*/
	float result = 0;
	QDateTime eventTime;
	EqpMsgCriteria *pEqpCriteria;
	ComplexCriteria *pComplexCriteria;
	int idx;
	if(joinType == And)	// Take the value of latest timestamp
	{
		eventTime = QDateTime::currentDateTime().addYears(-10);	// Far in the past
		for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
		{
			pEqpCriteria = pSimpleCriteriaList->at(idx);
/*
printf("ComplexCriteria::getEventValue(): simple AND %X matchByValue %d eventTime %s\n",
(unsigned)pEqpCriteria, (int)pEqpCriteria->isMatchByValue(), pEqpCriteria->getEventTime().toString("yyyy-MM-dd hh:mm:ss.zzz").ascii());
fflush(stdout);
*/
			if((pEqpCriteria->getEventTime() > eventTime) && pEqpCriteria->isMatchByValue())
			{
				eventTime = pEqpCriteria->getEventTime();
				result = pEqpCriteria->getEventValue();
/*
printf("    ComplexCriteria::getEventValue(): take value %g\n", result);
fflush(stdout);
*/
			}
		}

		for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
		{
			pComplexCriteria = pComplexCriteriaList->at(idx);
/*
printf("ComplexCriteria::getEventValue(): complex AND matchByValue %d eventTime %s\n",
(int)pComplexCriteria->isMatchByValue(), pComplexCriteria->getEventTime().toString("yyyy-MM-dd hh:mm:ss.zzz").ascii());
fflush(stdout);
*/
			if((pComplexCriteria->getEventTime() > eventTime) && pComplexCriteria->isMatchByValue())
			{
				eventTime = pComplexCriteria->getEventTime();
				result = pComplexCriteria->getEventValue();
/*
printf("    ComplexCriteria::getEventValue(): take value %g\n", result);
fflush(stdout);
*/
			}
		}
	}
	else	// take the earliest timestamp
	{
		eventTime = QDateTime::currentDateTime().addYears(10);	// Far in the future
		for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
		{
			pEqpCriteria = pSimpleCriteriaList->at(idx);
			if(pEqpCriteria->isMatch())
			{
/*
printf("ComplexCriteria::getEventValue(): simple OR matchByValue %d eventTime %s\n",
(int)pEqpCriteria->isMatchByValue(), pEqpCriteria->getEventTime().toString("yyyy-MM-dd hh:mm:ss.zzz").ascii());
fflush(stdout);
*/
				if((pEqpCriteria->getEventTime() < eventTime) && pEqpCriteria->isMatchByValue())
				{
					eventTime = pEqpCriteria->getEventTime();
					result = pEqpCriteria->getEventValue();
/*
printf("    ComplexCriteria::getEventValue(): take value %g\n", result);
fflush(stdout);
*/
				}
			}
		}

		for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
		{
			pComplexCriteria = pComplexCriteriaList->at(idx);
			if(pComplexCriteria->matches())
			{
/*
printf("ComplexCriteria::getEventValue(): complex OR matchByValue %d eventTime %s\n",
(int)pComplexCriteria->isMatchByValue(), pComplexCriteria->getEventTime().toString("yyyy-MM-dd hh:mm:ss.zzz").ascii());
fflush(stdout);
*/
				if((pComplexCriteria->getEventTime() < eventTime) && pComplexCriteria->isMatchByValue())
				{
					eventTime = pComplexCriteria->getEventTime();
					result = pComplexCriteria->getEventValue();
/*
printf("    ComplexCriteria::getEventValue(): take value %g\n", result);
fflush(stdout);
*/
				}
			}
		}
	}
	return result;
}

bool ComplexCriteria::isMatchByValue(void) const
{
	int idx;
	for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
	{
		EqpMsgCriteria *pEqpCriteria = pSimpleCriteriaList->at(idx);
		if(pEqpCriteria->isMatch() && pEqpCriteria->isMatchByValue())
		{
			return true;
		}
	}

	for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
	{
		ComplexCriteria *pComplexCriteria = pComplexCriteriaList->at(idx);
		if(pComplexCriteria->matches() && pComplexCriteria->isMatchByValue())
		{
			return true;
		}
	}
	return false;
}

int ComplexCriteria::getMinDuration(void) const
{
	int result = 0;
	EqpMsgCriteria *pEqpCriteria;
	ComplexCriteria *pComplexCriteria;
	int idx;
	if(joinType == And)	// Take the longest minDuration
	{
		result = 0;
		for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
		{
			pEqpCriteria = pSimpleCriteriaList->at(idx);
			if(pEqpCriteria->getMinDuration() > result)
			{
				result = pEqpCriteria->getMinDuration();
			}
		}

		for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
		{
			pComplexCriteria = pComplexCriteriaList->at(idx);
			if(pComplexCriteria->getMinDuration() > result)
			{
				result = pComplexCriteria->getMinDuration();
			}
		}
	}
	else	// take the earliest timestamp
	{
		result = 0x7FFFFFFF;	// Huge value
		for(idx = 0 ; idx < pSimpleCriteriaList->count() ; idx++)
		{
			pEqpCriteria = pSimpleCriteriaList->at(idx);
			if(pEqpCriteria->isMatch())
			{
				if(pEqpCriteria->getMinDuration() < result)
				{
					result = pEqpCriteria->getMinDuration();
				}
			}
		}

		for(idx = 0 ; idx < pComplexCriteriaList->count() ; idx++)
		{
			pComplexCriteria = pComplexCriteriaList->at(idx);
			if(pComplexCriteria->matches())
			{
				if(pComplexCriteria->getMinDuration() < result)
				{
					result = pComplexCriteria->getMinDuration();
				}
			}
		}
	}
	return result;
}
