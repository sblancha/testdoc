#ifndef	PROFILELHCSINGLEBEAMLINE_H
#define	PROFILELHCSINGLEBEAMLINE_H

// Single beam line in LHC profile - used for profile all LHC

#include "ProfileLhcLineBeam.h"


class ProfileLhcSingleBeamLine : public ProfileLhcLineBeam
{
public:
	ProfileLhcSingleBeamLine(ProfileLhcView *parent, int vacType, MainPart *pMainPart);
	virtual ~ProfileLhcSingleBeamLine();

	virtual void calcBarGeometry(int &maxBarIdx);

	virtual void dump(FILE *pFile);

	// access
	MainPart *getMainPart(void) const { return pMainPart; }

protected:
	// Vacuum type used by this line
	int			vacType;

	// Pointer to main part of this line
	MainPart	*pMainPart;

	virtual bool isMyEqp(Eqp  *pEqp, int &partIdx);

	// Implementation of LhcLineParts abstract methods
	virtual int vacTypeOfRegion(int regionType);
	virtual LhcLineParts::BuildType linePartMethod(void) { return LhcLineParts::TypeBeam; }
};

#endif	// PROFILELHCSINGLEBEAMLINE_H
