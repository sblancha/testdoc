#ifndef DATEEDITWIDGET_H
#define	DATEEDITWIDGET_H

// Class slightly extending functionality of QDateEdit: this widget
// signals change as QDateTime - start or end of day depending on
// setting

#include <qdatetimeedit.h>

class DateEditWidget : public QDateEdit
{
	Q_OBJECT

	////////////////////////////////////////////////////////////////////////////
	///////////////////// Properties exposed to PVSS panel /////////////////////
	////////////////////////////////////////////////////////////////////////////
	Q_PROPERTY(QDateTime dateTime READ getDateTime WRITE setDateTime);
	Q_PROPERTY(TimeOfDay mode READ getMode WRITE setMode);

	Q_ENUMS(TimeOfDay);

public:
	// Mode of data acquistion - copy of enum from DataEnum
	enum TimeOfDay
	{
		StartOfDay = 0,
		EndOfDay = 1
	};

	 // Accessors for properties above
	inline const QDateTime &getDateTime(void) const { return dateTime; }
	void setDateTime(QDateTime &value) { dateTime = value; }
	inline TimeOfDay getMode(void) const { return mode; }
	void setMode(TimeOfDay newMode);

	// Construction/destruction
	DateEditWidget(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~DateEditWidget() {}

signals:
	void dateTimeChanged(const QDateTime &dateTime);

protected:
	// Current date+time value
	QDateTime	dateTime;

	// Current time part mode
	TimeOfDay	mode;

private slots:
	void dateChange(const QDate &date);
};

#endif	// DATEEDITWIDGET_H
