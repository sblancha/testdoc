V 14
1
LANG:1 22 Configuration Criteria
PANEL,-1 -1 815 772 N "_3DFace" 1
"$dpName"
"main()
{
  dpName = $dpName;
  if(!dpExists(dpName + \".VisibleName\"))
  {
    PanelOff();
    return;
  }

  // Check if criteia is editable
  bool activate;
  dpGet(dpName + \".State.Activate\", activate);
  isEditable = (!activate) && VacSmsIsMyConfiguration(dpName);

  // Prepare interface items
  CritTable.sortOnClick(false);
  CritTable.tableMode(TABLE_SELECT_BROWSE);

  // Build list of available equipment types
  EqpTypeCombo.appendItem(\"\");
  VacSmsGetFuncTypes(funcTypeIds, funcTypeNames);
  int nTypes = dynlen(funcTypeIds);
  for(int n = 1 ; n <= nTypes ; n++)
  {
    EqpTypeCombo.appendItem(funcTypeNames[n]);
  }

  // Set sensitivity of interface elements
  EqpTypeCombo.enabled = isEditable;
//  CritTable.enabled = isEditable;
  JoinTypeCombo.enabled = isEditable;
  MsgSourceCombo.enabled = isEditable;
  OkPb.enabled = isEditable;
  

  // Connect to criteria DPEs
  dpConnect(\"CriteriaCb\",
    dpName + \".Criteria.FunctionalTypes\",
    dpName + \".Criteria.Types\",
    dpName + \".Criteria.Subtypes\",
    dpName + \".Criteria.LowerLimits\",
    dpName + \".Criteria.UpperLimits\",
    dpName + \".Criteria.ReverseFlags\",
    dpName + \".Criteria.MinDurations\");

  dpConnect(\"JoinTypeCb\",
    dpName + \".Criteria.JoinType\");

  dpConnect(\"UsageFilterCb\",
    dpName + \".Scope.UsageFilter.FunctionalTypes\",
    dpName + \".Scope.UsageFilter.AttrNames\",
    dpName + \".Scope.UsageFilter.ReverseFlags\");
}" 0
 E E E E 1 0 0 0  10 10
""0  1
E "
// Name of configuration DP
string dpName;

// Flag indicating if criteria is editable
bool isEditable;

// List of functional type IDs
dyn_int funcTypeIds;

// List of corresponding functional type names
dyn_string funcTypeNames;

void FillCriteriaForFuncType(int funcType)
{
  CritTable.deleteAllLines();
  int typeIdx = dynContains(funcTypeIds, funcType);
  if(typeIdx <= 0)
  {
    EqpTypeCombo.selectedPos = 1;	// Empty
    return;
  }
  EqpTypeCombo.selectedPos = typeIdx + 1;
  BuildUsedByTable(funcType);

  dyn_int critFuncTypes;
  dyn_int critTypes;
  dyn_int critSubTypes;
  dyn_string critNames;
  VacSmsGetFuncTypeCriteria(funcType, critFuncTypes, critTypes, critSubTypes, critNames);
  bool haveEnumValues = false;
  int nCrit = dynlen(critTypes);
  for(int n = 1 ; n <= nCrit ; n++)
  {
    dyn_int values;
    dyn_string labels;
    CritTable.appendLine(\"funcType\", critFuncTypes[n],
      \"critType\", critTypes[n],
      \"critSubType\", critSubTypes[n],
      \"critName\", critNames[n],
      \"use\", false,
      \"reverse\", false,
      \"lowerLimit\", 0.00,
      \"upperLimit\", 0.00,
      \"minDuration\", 0);
    if(VacSmsGetCriteriaValues(critFuncTypes[n], critTypes[n], critSubTypes[n], values, labels) == 0)
    {
      CritTable.cellValueRC(n - 1, \"useEnum\", false);
    }
    else
    {
      /*
      DebugTN(\"FuncType \" + critFuncTypes[n] + \", type \" + critTypes[n] + \", subType \" + critSubTypes[n],
        values, labels);
      */
      dynInsertAt(labels, false, 1);
      CritTable.cellWidgetRC(n - 1, \"lowerLimit\", \"ComboBox\", labels);
      CritTable.cellValueRC(n - 1, \"lowerLimit\", labels[2]);
      CritTable.cellWidgetRC(n - 1, \"upperLimit\", \"ComboBox\", labels);
      CritTable.cellValueRC(n - 1, \"upperLimit\", labels[2]);
      CritTable.cellValueRC(n - 1, \"useEnum\", true);
      haveEnumValues = true;
    }
  }

  for(int colIdx = CritTable.columnCount - 1 ; colIdx >= 0 ; colIdx--)
  {
    string colName = CritTable.columnToName(colIdx);
    if(colName == \"lowerLimit\")
    {
      CritTable.columnHeader(colIdx, haveEnumValues ? \"Lower Limit / Step\" : \"Lower Limit\");
    }
    else if(colName == \"upperLimit\")
    {
      CritTable.columnHeader(colIdx, haveEnumValues ? \"Upper Limit / Step\" : \"Upper Limit\");
    }
  }
}

void BuildUsedByTable(int funcType)
{
  dyn_int usedByTypeIds;
  dyn_string usedByTypeNames, attrNames;
  UsageTable.deleteAllLines();
  if(dynContains(funcTypeIds, funcType) > 0)
  {
//    DebugTN(\"Calling usage\");
    int coco = VacSmsConfigGetFuncTypeUsage(funcType, usedByTypeIds, usedByTypeNames, attrNames);
//    DebugTN(\"VacSmsConfigGetFuncTypeUsage() = \" + coco, usedByTypeIds, usedByTypeNames, attrNames);
  }
  int nItems = dynlen(usedByTypeIds);
  for(int idx = 1 ; idx <= nItems ; idx++)
  {
    UsageTable.appendLine(\"filter\", false,
      \"reverse\", false,
      \"usedById\", usedByTypeIds[idx],
      \"usedByName\", usedByTypeNames[idx],
      \"attrName\", attrNames[idx]);
  }
  UsageTable.sort(true, \"usedByName\", \"attrName\");
}

void UpdateDefaultMessage()
{
  int sourceType = MsgSourceCombo.selectedPos;
  int nRows = CritTable.lineCount;
  int funcType, critType, critSubType;
  bool reverse;
  for(int row = 0 ; row < nRows ; row++)
  {
    bool use;
    getValue(\"CritTable\", \"cellValueRC\", row, \"use\", use);
    if(use)
    {
      getValue(\"CritTable\", \"cellValueRC\", row, \"funcType\", funcType);
      getValue(\"CritTable\", \"cellValueRC\", row, \"critType\", critType);
      getValue(\"CritTable\", \"cellValueRC\", row, \"critSubType\", critSubType);
      getValue(\"CritTable\", \"cellValueRC\", row, \"reverse\", reverse);
//DebugTN(\"Got 1st in table:\", funcType, critType, critSubType);
      break;
    }
  }
  if(funcType > 0)
  {
    string message;
    VacSmsGetStdCriteriaMessage(funcType, critType, critSubType, sourceType, reverse, message);
    StdMessageText.text = message;
  }
  else
  {
    StdMessageText.text = \"\";
  }
}

void CriteriaCb(string dpe1, dyn_int funcTypes, string dpe2, dyn_int types,
  string dpe3, dyn_int subTypes, string dpe4, dyn_float lowerLimits,
  string dpe5, dyn_float upperLimits, string dpe6, dyn_bool reverseFlags,
  string dpe7, dyn_int minDurations)
{
  if(dynlen(funcTypes) == 0)
  {
    EqpTypeCombo.selectedPos = 1;	// Empty
    CritTable.deleteAllLines();
    return;
  }
  FillCriteriaForFuncType(funcTypes[1]);

  // Display criteria
  for(int n = dynlen(funcTypes) ; n > 0 ; n--)
  {
    if(n > dynlen(types))
    {
      continue;
    }
    if(n > dynlen(subTypes))
    {
      continue;
    }
    int row = FindCriteriaRow(funcTypes[n], types[n], subTypes[n]);
    if(n < 0)
    {
      continue;
    }
    bool useEnum;
    getValue(\"CritTable\", \"cellValueRC\", row, \"useEnum\", useEnum);
    dyn_int values;
    dyn_string labels;
    if(useEnum)
    {
      VacSmsGetCriteriaValues(funcTypes[n], types[n], subTypes[n], values, labels);
    }
    
    CritTable.cellValueRC(row, \"use\", true);
    if(n <= dynlen(reverseFlags))
    {
      CritTable.cellValueRC(row, \"reverse\", reverseFlags[n]);
    }
    if(n <= dynlen(lowerLimits))
    {
      if(useEnum)
      {
        int idx = lowerLimits[n];
        if((0 < idx) && (idx <= dynlen(labels)))
        {
          CritTable.cellValueRC(row, \"lowerLimit\", labels[idx]);
        }
        else
        {
          CritTable.cellValueRC(row, \"lowerLimit\", \"??? \" + idx);
        }
      }
      else
      {
        CritTable.cellValueRC(row, \"lowerLimit\", lowerLimits[n]);
      }
    }
    if(n <= dynlen(upperLimits))
    {
      if(useEnum)
      {
        int idx = upperLimits[n];
        if((0 < idx) && (idx <= dynlen(labels)))
        {
          CritTable.cellValueRC(row, \"upperLimit\", labels[idx]);
        }
        else
        {
          CritTable.cellValueRC(row, \"upperLimit\", \"??? \" + idx);
        }
      }
      else
      {
        CritTable.cellValueRC(row, \"upperLimit\", upperLimits[n]);
      }
    }
    if(n <= dynlen(minDurations))
    {
      CritTable.cellValueRC(row, \"minDuration\", minDurations[n]);
    }
  }
  UpdateDefaultMessage();
}

void JoinTypeCb(string dpe1, int joinType)
{
  JoinTypeCombo.selectedPos = joinType;
}

int FindCriteriaRow(int funcTypeId, int type, int subType)
{
  int row;
  for(row = CritTable.lineCount - 1 ; row >= 0 ; row--)
  {
    int funcType, critType, critSubType;
    getValue(\"CritTable\", \"cellValueRC\", row, \"funcType\", funcType);
    if(funcType != funcTypeId)
    {
      continue;
    }
    getValue(\"CritTable\", \"cellValueRC\", row, \"critType\", critType);
    if(critType != type)
    {
      continue;
    }
    getValue(\"CritTable\", \"cellValueRC\", row, \"critSubType\", critSubType);
    if(critSubType == subType)
    {
      break;
    }
  }
  return row;
}

void UsageFilterCb(string dpe1, dyn_int funcTypes, string dpe2, dyn_string attrNames,
                   string dpe3, dyn_bool reverseFlags)
{
  for(int row = 0 ; row < UsageTable.lineCount ; row++)
  {
    bool use = false, reverse = false;
    int typeId;
    getValue(\"UsageTable\", \"cellValueRC\", row, \"usedById\", typeId);
    for(int idx = dynlen(funcTypes) ; idx > 0 ; idx--)
    {
      if(funcTypes[idx] == typeId)
      {
        if((0 < idx) && (idx <= dynlen(attrNames)))
        {
          string attrName;
          getValue(\"UsageTable\", \"cellValueRC\", row, \"attrName\", attrName);
          if(attrName == attrNames[idx])
          {
            if(idx <= dynlen(reverseFlags))
            {
              use = true;
              reverse = reverseFlags[idx];
              break;
            }
          }
        }
      }
    }
    UsageTable.cellValueRC(row, \"filter\", use);
    UsageTable.cellValueRC(row, \"reverse\", reverse);
  }
}
" 0
 3
"CBRef" "1"
"EClose" E
"dpi" "120"
0 0 0
""
DISPLAY_LAYER, 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0
LAYER, 0 
1
LANG:1 6 Layer1
2 0
"EqpTypeText"
""
1 43 3 E E E 1 E 1 E N "_WindowText" E N "_Transparent" E E
 E E
1 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 45 5 145 21
0 2 0 "0s" 0 0 0 64 0 0  45 5 1
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 1
LANG:1 15 Eqpuipment type
2 3
"JoinTypeText"
""
1 4 543 E E E 1 E 1 E N "_WindowText" E N "_Transparent" E E
 E E
4 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 6 545 132 561
0 2 0 "0s" 0 0 0 64 0 0  6 545 1
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 1
LANG:1 23 Criteria are joined by:
2 7
"Text1"
""
1 372 544 E E E 1 E 1 E N "_WindowText" E N "_Transparent" E E
 E E
7 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 374 546 500 562
0 2 0 "0s" 0 0 0 64 0 0  374 546 1
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 1
LANG:1 20 Produce message for:
22 1
"EqpTypeCombo"
""
1 14 3 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
2 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  150 0 553 27
0

E
"main()
{
  int pos = this.selectedPos - 1;
  if((pos < 1) || (pos > dynlen(funcTypeIds)))
  {
    FillCriteriaForFuncType(0);
  }
  else
  {
    FillCriteriaForFuncType(funcTypeIds[pos]);
  }
}" 0

E
 0 0
25 2
"CritTable"
""
1 13 297 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
3 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  11 295 805 539
E"main(int row, string column, string value)
{
  if(!isEditable)
  {
    return;
  }
  if(column = \"use\")
  {
    UpdateDefaultMessage();
  }
}" 0
 1 0 1 10 1 "funcType" 10 0 0 "s" 1
LANG:1 8 funcType
E
1
LANG:1 0 

128 "critType" 10 0 0 "s" 1
LANG:1 8 critType
E
1
LANG:1 0 

128 "critSubType" 10 0 0 "s" 1
LANG:1 11 critSubType
E
1
LANG:1 0 

128 "use" 2 1 1 "s" 1
LANG:1 3 Use
E
1
LANG:1 17 Use this criteria

36 "reverse" 4 1 1 "s" 1
LANG:1 7 Reverse
E
1
LANG:1 20 Use reverse criteria

60 "critName" 21 1 0 "s" 1
LANG:1 8 Criteria
E
1
LANG:1 16 Criteria meaning

240 "lowerLimit" 14 1 1 "s" 1
LANG:1 11 Lower Limit
E
1
LANG:1 11 Lower limit

170 "upperLimit" 14 1 1 "s" 1
LANG:1 11 Upper Limit
E
1
LANG:1 11 Upper limit

170 "minDuration" 7 1 1 "s" 1
LANG:1 13 Min. duration
E
1
LANG:1 41 Min. event duration [sec] to send message

95 "useEnum" 1 0 0 "s" 1
LANG:1 0 
E
1
LANG:1 41 Min. event duration [sec] to send message

20 
26 26 "" 1 1
LANG:1 2 #1
8 30
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 0 1 1 1 7
1 0
22 4
"JoinTypeCombo"
""
1 144 543 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
5 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  142 541 226 568
2
1
LANG:1 3 AND

0
1
LANG:1 2 OR

0

E
E
E
 0 0
13 5
"OkPb"
""
1 90 743 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
6 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  88 741 162 769

T 
1
LANG:1 2 OK
"main()
{
  int nLines = CritTable.lineCount;
  int joinType;
  dyn_int funcTypes;
  dyn_int types;
  dyn_int subTypes;
  dyn_float lowerLimits;
  dyn_float upperLimits;
  dyn_bool reverseFlags;
  dyn_int minDurations;
  for(int row = 0 ; row < nLines ; row++)
  {
    bool use;
    getValue(\"CritTable\", \"cellValueRC\", row, \"use\", use);
    if(!use)
    {
      continue;
    }
    int funcType, type, subType;
    getValue(\"CritTable\", \"cellValueRC\", row, \"funcType\", funcType);
    getValue(\"CritTable\", \"cellValueRC\", row, \"critType\", type);
    getValue(\"CritTable\", \"cellValueRC\", row, \"critSubType\", subType);
    bool useEnum;
    getValue(\"CritTable\", \"cellValueRC\", row, \"useEnum\", useEnum);
    float lowerLimit, upperLimit;
// L.Kopylov 26.10.2012    if(type == CRIT_TYPE_MAIN_VALUE)
    {
      if(useEnum)
      {
        dyn_int values;
        dyn_string labels;
        VacSmsGetCriteriaValues(funcType, type, subType, values, labels);
        string label = \"\";
        getValue(\"CritTable\", \"cellValueRC\", row, \"lowerLimit\", label);
        int idx = dynContains(labels, label);
        if(idx <= 0)
        {
          if(label == \"0\")
          {
            lowerLimit = 0;
          }
          else
          {
            dyn_string exceptionInfo;
            fwException_raise(exceptionInfo, \"ERROR\", \"ApplyCriteria(): line \" + (row + 1) + \": unknown enum \" + label + \" for lower limit\", \"\");
            fwExceptionHandling_display(exceptionInfo);
            return;
          }
        }
        lowerLimit = values[idx];

        string label = \"\";
        getValue(\"CritTable\", \"cellValueRC\", row, \"upperLimit\", label);
        int idx = dynContains(labels, label);
        if(idx <= 0)
        {
          if(label == \"0\")
          {
            upperLimit = 0;
          }
          else
          {
            dyn_string exceptionInfo;
            fwException_raise(exceptionInfo, \"ERROR\", \"ApplyCriteria(): line \" + (row + 1) + \": unknown enum \" + label + \" for upper limit\", \"\");
            fwExceptionHandling_display(exceptionInfo);
            return;
          }
        }
        upperLimit = values[idx];        
        if(lowerLimit > upperLimit)
        {
          dyn_string exceptionInfo;
          fwException_raise(exceptionInfo, \"ERROR\", \"ApplyCriteria(): line \" + (row + 1) + \": lower limit \" + lowerLimit +
                            \" can not be above upper limit \" + upperLimit, \"\");
          fwExceptionHandling_display(exceptionInfo);
          return;
        }
      }
      else
      {
        getValue(\"CritTable\", \"cellValueRC\", row, \"lowerLimit\", lowerLimit);
        getValue(\"CritTable\", \"cellValueRC\", row, \"upperLimit\", upperLimit);
        if((lowerLimit > upperLimit) && (type == CRIT_TYPE_MAIN_VALUE))
        {
          dyn_string exceptionInfo;
          fwException_raise(exceptionInfo, \"ERROR\", \"ApplyCriteria(): line \" + (row + 1) + \": lower limit can not be above upper limit\", \"\");
          fwExceptionHandling_display(exceptionInfo);
          return;
        }
      }
    }

    bool reverseFlag;
    getValue(\"CritTable\", \"cellValueRC\", row, \"reverse\", reverseFlag);

    int minDuration;
    getValue(\"CritTable\", \"cellValueRC\", row, \"minDuration\", minDuration);

    dynAppend(funcTypes, funcType);
    dynAppend(types, type);
    dynAppend(subTypes, subType);
    dynAppend(lowerLimits, lowerLimit);
    dynAppend(upperLimits, upperLimit);
    dynAppend(reverseFlags, reverseFlag);
    dynAppend(minDurations, minDuration);
  }

  if(dynlen(funcTypes) > 0)
  {
    // The very first criteria must be for type selected, if not - add dummy criteria at first position
    int pos = EqpTypeCombo.selectedPos;
    int funcType = funcTypeIds[pos - 1];
    if(funcTypes[1] != funcType)
    {
      dynInsertAt(funcTypes, funcType, 1);
      dynInsertAt(types, 0, 1);
      dynInsertAt(subTypes, 0, 1);
      dynInsertAt(lowerLimits, 0, 1);
      dynInsertAt(upperLimits, 0, 1);
      dynInsertAt(reverseFlags, 0, 1);
      dynInsertAt(minDurations, 0, 1);
    }
  }

  int joinType = JoinTypeCombo.selectedPos;

  // Add filtering by usage
  dyn_int usedByIds;
  dyn_string attrNames;
  dyn_bool useReverse;
  for(int row = 0 ; row < UsageTable.lineCount ; row++)
  {
    bool filter = false;
    getValue(\"UsageTable\", \"cellValueRC\", row, \"filter\", filter);
    if(!filter)
    {
      continue;
    }
    int usedById;
    getValue(\"UsageTable\", \"cellValueRC\", row, \"usedById\", usedById);
    dynAppend(usedByIds, usedById);
    string attrName;
    getValue(\"UsageTable\", \"cellValueRC\", row, \"attrName\", attrName);
    dynAppend(attrNames, attrName);
    bool reverse;
    getValue(\"UsageTable\", \"cellValueRC\", row, \"reverse\", reverse);
    dynAppend(useReverse, reverse);
  }
  dpSet(dpName + \".Scope.UsageFilter.FunctionalTypes\", usedByIds,
    dpName + \".Scope.UsageFilter.AttrNames\", attrNames,
    dpName + \".Scope.UsageFilter.ReverseFlags\", useReverse);

  dpSet(dpName + \".Criteria.JoinType\", joinType,
    dpName + \".Criteria.FunctionalTypes\", funcTypes,
    dpName + \".Criteria.Types\", types,
    dpName + \".Criteria.Subtypes\", subTypes,
    dpName + \".Criteria.LowerLimits\", lowerLimits,
    dpName + \".Criteria.UpperLimits\", upperLimits,
    dpName + \".Criteria.ReverseFlags\", reverseFlags,
    dpName + \".Criteria.MinDurations\", minDurations);

  dpSet(dpName + \".Message\", StdMessageText.text);
  PanelOff();
}" 0
 E E E
13 6
"CancelPb"
""
1 611 742 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
7 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  609 740 683 768

T 
1
LANG:1 6 Cancel
"main()
{
	PanelOff();
}" 0
 E E E
22 8
"MsgSourceCombo"
""
1 502 544 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
8 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  500 542 716 569
3
1
LANG:1 13 Single Device

1
1
LANG:1 13 Vacuum Sector

0
1
LANG:1 16 Vacuum Main Part

0

E
"main()
{
	UpdateDefaultMessage();
}" 0

E
 0 0
14 9
"StdMessageText"
""
1 9 582 E E E 1 E 1 E N "_WindowText" E N "_3DFace" E E
 E E
9 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  7 580 810 607
2 "0s" 0 0 0 0 0 -1  E E E
30 15
"UsageFrame"
""
1 2 27 E E E 1 E 1 E N "_WindowText" E N {0,0,0} E E
 E E
15 0 0 0 0 0
E E E
1
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 0 1 0 1 E 1 0 1.015686274509804 0 -4.423529411764702 0 E 2 27 812 283
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0 1
LANG:1 21 Extra Filter on Usage
25 16
"UsageTable"
""
1 9 41 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
16 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignCenter"
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0  7 39 637 274
EE 1 0 1 5 0 "filter" 3 1 1 "s" 1
LANG:1 6 Filter
E
1
LANG:1 0 

50 "reverse" 4 1 1 "s" 1
LANG:1 7 Reverse
E
1
LANG:1 0 

60 "usedByName" 22 1 0 "s" 1
LANG:1 12 Used by Type
E
1
LANG:1 0 

250 "attrName" 22 1 0 "s" 1
LANG:1 7 Used as
E
1
LANG:1 0 

250 "usedById" 22 0 0 "s" 1
LANG:1 7 Used ID
E
1
LANG:1 0 

250 
20 20 10 10
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0 0 1 1 1 7
1 0
30 17
"CritFrame"
""
1 2 281 E E E 1 E 1 E N "_WindowText" E N {0,0,0} E E
 E E
17 0 0 0 0 0
E E E
1
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 0 1 0 1 E 1.00123609394314 0 1 -0.501854140914729 0 0 E 2 281 811 572
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0 1
LANG:1 18 Notification State
2 18
"Legend1"
""
1 644 79 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
18 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 2 1 E U  0 E 644 79 795 100
0 2 2 "0s" 0 0 0 64 0 0  644 79 1
1
LANG:1 26 Arial,-1,13,5,75,0,0,0,0,0
0 1
LANG:1 23 Reverse - joined by AND
2 19
"Legend2"
""
1 645 104 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
19 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 2 1 E U  0 E 645 104 796 125
0 2 2 "0s" 0 0 0 64 0 0  645 104 1
1
LANG:1 26 Arial,-1,13,5,75,0,0,0,0,0
0 1
LANG:1 21 Normal - joined by OR
29 22
"TEXT_EDIT1"
""
1 9 619.9999999999998 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
22 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"text" "string Attention: The addition of all the criteria selected above define a notification state. Only transition from \"other\" state to notification state trigger an event. The panel does not prevent to any wrong settings. Using both opposite binary criteria may result a wrong definition. 
E.g. most of the devices are either Off or On; for such devices if you use both On and Off criteria with \"OR\" join it means there is no \"other\" state and with \"AND\" join it means there is no notification state; in both case event will never be triggered."
"alignment" "enum 8"
"layoutAlignment" "AlignNone"
1
LANG:1 35 MS Shell Dlg 2,-1,13,5,75,0,0,0,0,0
0  9 619.9999999999998 800.0000000000001 729.9999999999998
15 PVSS.TextEdit.1
0
E0
LAYER, 1 
1
LANG:1 6 Layer2
0
LAYER, 2 
1
LANG:1 6 Layer3
0
LAYER, 3 
1
LANG:1 6 Layer4
0
LAYER, 4 
1
LANG:1 6 Layer5
0
LAYER, 5 
1
LANG:1 6 Layer6
0
LAYER, 6 
1
LANG:1 6 Layer7
0
LAYER, 7 
1
LANG:1 6 Layer8
0
0
