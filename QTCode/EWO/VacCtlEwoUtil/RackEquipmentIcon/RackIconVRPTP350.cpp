#include "RackIconVRPTP350.h"

#include "DataPool.h"

#include "qpainter.h"

RackIconVRPTP350::RackIconVRPTP350(QWidget *parent, Qt::WindowFlags f) :
RackIcon(parent, f)
{
	pEqp = NULL;
	//QWidget::setAttribute(Qt::WA_TransparentForMouseEvents);
	//setFixedSize(21, 21); see about this later
}

RackIconVRPTP350::~RackIconVRPTP350()
{
	disconnect();
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRPTP350::disconnect(void)
{
	bool result = false;
	if (connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	
	connected = false;
	
	return result;
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRPTP350::connect(void)
{
	if (connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	//dpes.append("PR");
	connection.connect(pEqp, dpes, DataEnum::Online);
	connected = true;
	return true;


}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void RackIconVRPTP350::setEqp(Eqp *pEqp)
{
	disconnect();
	DataPool &pool = DataPool::getInstance();
	if (pEqp != NULL)
	{
		//Q_ASSERT_X(pEqp->inherits("EqpVRPI"), "RackIconVRPIT::setEqp", pEqp->getDpName());
		//this->pEqp = (EqpVR_GT *)pEqp;
	}
	forceRedraw();
}

void RackIconVRPTP350::paintEvent(QPaintEvent *pEvent)
{
	
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.scale(scale, scale);
	painter.setBrush(Qt::white);

	QFont calibriExtraSmallBold("Calibri", 6, QFont::Bold);
	QFont calibriExtraSmall("Calibri", 6);
	int textFlags;
	
	QPen selectionPen = QPen(Qt::red);
	selectionPen.setWidth(3);	
	if (isSelected)
	{
		painter.setPen(selectionPen);
	}
	else
	{
		painter.setPen(Qt::black);
	}
	QRect equipmentRect(0, 0, this->sizeX, this->sizeY);
	//painter.setPen(Qt::black);
	painter.drawRect(equipmentRect);
	
	painter.setPen(Qt::black);
	painter.setFont(calibriExtraSmallBold);
	painter.setBrush(QColor(220, 220, 220));
	textFlags = Qt::AlignHCenter | Qt::AlignTop | Qt::TextWrapAnywhere;

	//split in lines
	int lineSplitIndex = this->equipmentName.indexOf("\.", 7);
	QString firstLine = this->equipmentName.left(lineSplitIndex + 1);
	QString secondLine = this->equipmentName.right(this->equipmentName.length() - (lineSplitIndex + 1));

	painter.drawRect(equipmentRect);
	painter.drawText(equipmentRect, textFlags, firstLine);
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 8, equipmentRect.width(), equipmentRect.height() - 8, textFlags, secondLine);

	QString attributeValue;
	attributeValue = attributes.value("CrateSlot");
	painter.setFont(calibriExtraSmall);
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 18, equipmentRect.width(), equipmentRect.height() - 18, textFlags, "TMPU " + attributeValue);

	attributeValue = attributes.value("CableNF12");
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 26, equipmentRect.width(), equipmentRect.height() - 26, textFlags, "NF12 " + attributeValue);
	attributeValue = attributes.value("CableNG18");
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 34, equipmentRect.width(), equipmentRect.height() - 34, textFlags, "NG18 " + attributeValue); 
}


