#ifndef TIMEAXISWIDGET_H
#define	TIMEAXISWIDGET_H

// Horizontal time axis widget

#include <qwidget.h>
#include <qdatetime.h>
#include <qrect.h>

#include <stdio.h>

class TimeAxisWidget : public QWidget
{
	Q_OBJECT

public:
	TimeAxisWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);
	~TimeAxisWidget();

	void setGraphArea(const QRect &rect);
	void setLimits(const QDateTime &newMin, const QDateTime &newMax);
	void setLogScale(bool flag);
	void setLogAxisUnit(unsigned unit);

	// Value returned by timeToScreen method
	typedef enum
	{
		BeforeMin = -1,
		WithinRange = 0,
		AfterMax = 1
	} TimeRange;

	TimeRange timeToScreen(const QDateTime &value, int &screenX) const;
	TimeRange timeAsMsecToScreen(qint64 value, int &screenX) const;
	QDateTime screenToTime(int screenX, QString &label);
	int findLabelHeight(const QFontMetrics &fontMetrics);
	bool prepare(const QFontMetrics &fontMetrics);
	void drawOnPlotArea(QPainter &painter);

	// Access
	inline const QDateTime &getMin(void) const { return min; }
	inline const QDateTime &getMax(void) const { return max; }
	inline qint64 getMinAsMsec(void) const { return minAsMsec;  }
	inline qint64 getMaxAsMsec(void) const { return maxAsMsec;  }
	inline const QRect &getGraphArea(void) const { return graphArea; }
	inline int getScaleLeft(void) const { return scaleLeft; }
	inline int getScaleRight(void) const { return scaleRight; }
	inline bool isLogScale(void) const { return logScale; }
	inline unsigned getLogAxisUnit(void) const { return logAxisUnit; }
	inline bool isAutoLogAxisUnit(void) const { return autoLogAxisUnit; }
	inline void setGrid(bool flag) { grid = flag; }
	inline void setGridStyle(int style) { gridStyle = style; }
	inline void setTickSize(int size) { tickSize = size; }
	inline void setTickOutside(bool flag) { tickOutside = flag; }

	void dump(FILE *pFile);

signals:
	void limitsChanged(bool dragging);

protected:
	// Axis minimum : 
	QDateTime	min;

	// Axis maximum
	QDateTime	max;

	// Axis minimum as quint64 (to speed up time -> screen conversion)
	qint64		minAsMsec;

	// Axis maximum as quint64 (to speed up time -> screen conversion)
	qint64		maxAsMsec;

	// Logarithm of maximum - only used in logarithmic scale mode
	double		logMax;

	// Time of start label to draw
	QDateTime	startLabelTime;

	// Time between labels [sec]
	int			labelStep;

	// Graph area to use
	QRect		graphArea;

	// X-coordinate of left scale edge
	int			scaleLeft;

	// X-coordinate of right scale edge
	int			scaleRight;

	// Line style for grid drawing
	int			gridStyle;

	// Size of ticks between grid lines
	int			tickSize;

	// unit of log axis - only used in logarithmic scale mode [sec]
	unsigned		logAxisUnit;

	// Flag indicating if logarithmic scale for time shall be used
	bool		logScale;

	// Flag indicating if logAxisUnit shall be caltulated automatically
	bool		autoLogAxisUnit;

	// Flag inidcating if ticks shall be drawn outside graph area
	bool		tickOutside;

	// Flag indicating if grid shall be drawn
	bool		grid;

	// Flag indicating if axis parameters shall be recalculated
	bool		needPrepare;

	void calculateLinAxis(const QFontMetrics &fontMetrics);
	void calculateLogAxis(const QFontMetrics &fontMetrics);

	void draw(QPainter &painter);
	void drawLin(QPainter &painter, const QFontMetrics &fm);
	void drawLog(QPainter &painter, const QFontMetrics &fm);
	void drawLabel(QPainter &painter, const QFontMetrics &fm, QString &label, int x);
	void drawLinOnPlotArea(QPainter &painter);
	void drawLogOnPlotArea(QPainter &painter);

	void labelExtents(const QFontMetrics &fontMetrics, QString &label, QSize &size);
	void makeLabel(QString &label, const QDateTime &value);
	int nextLabelStep(int step);
	int nextScaleStep(int step, bool use60Base);

	void showTimeScaleMenu(void);

	// overwrite QWidget's methods
	virtual void mousePressEvent(QMouseEvent *pEvent);
	virtual void paintEvent(QPaintEvent *pEvent);

private slots:
	void axisMenu(QAction *pAction);
	
};

#endif	// TIMEAXISWIDGET_H
