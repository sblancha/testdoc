#
# Project file for qmake utility to build Makefile for simple DataPool test
#
#	15.08.2008	L.Kopylov
#		Initial version
#
#	13.12.2008	L.Kopylov
#		Renaming libraries and directories

TEMPLATE = app

TARGET = testDataPool

INCLUDEPATH += ../../VacCtlUtil \
				../DataPool


unix:LIBS += -L../../../bin -lVacCtlEqpData -lVacCtlUtil \
	-lVacCtlEwoUtil

CONFIG += qt thread release warn_on

DEFINES += _VACDEBUG

unix:SOURCES = testDataPool.cpp




