//	Implementation of VacIconVG_PT_P class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVG_PT_P.h"

#include "EqpVG_PT.h"

#include <qpainter.h>

#define	RR1_VALID		(0x40000000)
#define	RR1_ON			(0x02000000)
#define	RR1_OFF			(0x01000000)
#define RR1_FORCED		(0x00010000)

// We only need to implement drawSpecific() method

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVG_PT_P::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVG_PT"), "VacIconVG_PT_P::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVG_PT *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVG_PT_P::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("PR");
	dpes.append("BlockedOFF");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

// Draw 'Penning' symbol in the center of icon
void VacIconVG_PT_P::drawSpecific(QPainter &painter)
{
	QPen blackPen(Qt::black, 2);
	painter.setPen(blackPen);
	/*
	painter.drawArc(9, 8, 3, 3, 0, 360 * 16);
	*/
	painter.drawLine(10, 8, 12, 8);

	bool blockedOff = false;
	bool forcedOn = false;
	bool forcedOnLocal = false;		// TODO: Forcled local can not be detected with recent implementation of VG_PT

	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			break;
		default:
			{
				// Special painting for AUTO and ForcedON modes
				bool plcAlarm = pEqp->isPlcAlarm(mode);
				if(!plcAlarm)
				{
					unsigned rr1 = ((EqpVG_PT *)pEqp)->getRR1(mode);
					if(rr1 & RR1_VALID)
					{
						if((rr1 & RR1_ON) && (rr1 & RR1_FORCED))
						{
							forcedOn = true;
							/* TODO
							if(!(rr1 & RR1_REMOTE))
							{
								forcedOnLocal = true;
							}
							*/
						}
					}
				}
				blockedOff = ((EqpVG_PT *)pEqp)->isBlockedOff(mode);
			}
			break;
		}
	}

	painter.setPen(Qt::black);
	QPolygon points(4);
	points.setPoint(0, 5, 8);
	points.setPoint(1, 5, 13);
	points.setPoint(2, 15, 13);
	points.setPoint(3, 15, 8);
	painter.drawPolyline(points);
	if(forcedOn)
	{
		painter.drawLine(5, 11, 15, 11);
		if(forcedOnLocal)
		{
			painter.drawLine(5, 14, 5, 15);
			painter.drawLine(15, 14, 15, 15);
			painter.drawLine(5, 15, 15, 15);
		}
	}

	if(blockedOff)
	{
		painter.setPen(blackPen);
		painter.drawLine(0, 13, 20, 20);
		painter.drawLine(0, 20, 20, 13);
	}
}
