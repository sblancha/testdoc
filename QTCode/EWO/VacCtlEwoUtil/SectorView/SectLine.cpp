//	Implementation of SectLine class
/////////////////////////////////////////////////////////////////////////////////

#include "SectLine.h"

#include "VacEqpTypeMask.h"

#include "DataPool.h"
#include "BeamLine.h"
#include "BeamLinePart.h"
#include "Eqp.h"
#include "EqpCOLDEX.h"
#include "EqpType.h"
#include "VacIcon.h"

#include "VacMainView.h"

#include <qpainter.h>

/*
** FUNCTION
**		Check if range of sectors given by pointers to start and end sector
**		shall include (part of) given beam line. If so - create new instance
**		of CSynLine for this beam line and return it. Note that new instance
**		has only limits equipment selection, NOT equipment itself - equipment
**		and geometry will be built by method build().
**
** PARAMETERS
**		parent			- QWidget where this line will be drawn
**		pLine			- Pointer to beam line for which synoptic line shall be built
**		pStartSector	- Pointer to first sector in range
**		pEndSector		- Pointer to last sector in range
**
** RETURNS
**		Pointer to new instance ofCSynLine if given beam line shall be at least
**			partially presented in synoptic;
**		NULL if given beam line shall not be presented in synoptic
**
** CAUTIONS
**		None
*/
SectLine *SectLine::create(QWidget *parent, BeamLine *pLine, Sector *pStartSector, Sector *pEndSector)
{
	BeamLinePart	*pStartPart, *pEndPart;
	int				startEqpIdx, endEqpIdx;

	DataPool &pool = DataPool::getInstance();
	if(!pLine->findEqpRange(pStartSector, pEndSector,
		&pStartPart, &pEndPart, startEqpIdx, endEqpIdx))
	{
		return NULL;
	}

	SectLine *pSectLine = new SectLine();
	pSectLine->parent = parent;
	pSectLine->pBeamLine = pLine;
	pSectLine->pStartPart = pStartPart;
	pSectLine->pEndPart = pEndPart;
	pSectLine->startEqpIdx = startEqpIdx;
	pSectLine->endEqpIdx = endEqpIdx;
	pSectLine->metersToScreen = 1.0 / pool.getMinPassiveEqpVisibleWidth();
	return pSectLine;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SectLine::SectLine() : SynLine()
{
}

SectLine::~SectLine()
{
}

/*
**	FUNCTION
**		Build all geometry of this line
**
**	PARAMETERS
**		sectorUsage	- List of all sectors which were already used in other
**						line(s)
**
**	RETURNS
**		Number of devices in resulting beam line;
**		-1 - in case of error
**
**	CAUTIONS
**		None
*/
int SectLine::build(QList<Sector *> &sectorUsage)
{
	clear();

	QList<BeamLinePart *> &lineParts = pBeamLine->getParts();
	const BeamLinePart *pPart = pStartPart;
	int partIdx;
	for(partIdx = 0 ; partIdx < lineParts.count() ; partIdx++)
	{
		BeamLinePart *pSearchPart = lineParts.at(partIdx);
		if(pSearchPart == pPart)
		{
			break;
		}
	}
	int eqpIdx = startEqpIdx;
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	bool isFirstEqp = true, isFirstBorder = true;

	while(true)
	{
		do
		{
			Eqp *pEqp = eqpPtrs[eqpIdx];
			switch(pEqp->getType())
			{
			case EqpType::OtherLineStart:
			case EqpType::OtherLineEnd:
				addDevice(pPart, eqpIdx, pEqp, pEqp->getType());
				break;
			case EqpType::VacDevice:
				if(pEqp->isActive(mode))
				{
					if(pEqp->isSkipOnSynoptic())
					{
						continue;
					}
					// Add sector before
					Sector *pSector = pEqp->getSectorBefore();
					if(pSector)
					{
						// In special case of circular line AND the whole circle included:
						// do not put the sector before the very first border because it
						// will appear later between two other sector borders
						if((!isFirstBorder) || (!pBeamLine->isCircle()) ||
							(pStartPart != pEndPart) || (startEqpIdx != endEqpIdx))
						{
							if(!sectorUsage.contains(pSector))
							{
								addDevice(pPart, eqpIdx, pEqp, EqpType::SectorBefore);
								sectorUsage.append(pSector);
							}
							else
							{
								
							}
						}
					}

					// Add device itself under the following conditions:
					// either device must appear on sector view
					// or device is sector border
					if(pEqp->isForSectorView() || pEqp->isSectorBorder())
					{
						addDevice(pPart, eqpIdx, pEqp, pEqp->getType());
					}
					if(pEqp->getFunctionalType() == FunctionalType::COLDEX && !pColdex)
					{
						pColdex = (EqpCOLDEX *)pEqp;
						pColdex->connect(this, mode);
						QDateTime now = QDateTime::currentDateTime();
						dpeChange(pColdex, "RRC", DataEnum::Own, QVariant(pColdex->getRR1(mode)), mode, now);
					}

					// Add sector after device
					if(pEqp->isSectorBorder())
					{
						isFirstBorder = false;
						if(!sectorUsage.contains(pEqp->getSectorAfter()))
						{
							addDevice(pPart, eqpIdx, pEqp, EqpType::SectorAfter);
							sectorUsage.append(pEqp->getSectorAfter());
						}
					}
				}
				break;
			}
			if((pPart == pEndPart) && (eqpIdx == endEqpIdx))
			{
				// special case: the whole circular line, i.e. start and end eqp are the same
				if(pBeamLine->isCircle())
				{
					if(!isFirstEqp)
					{
						break;
					}
				}
				else
				{
					break;
				}
			}
			isFirstEqp = false;
		} while(++eqpIdx < nEqpPtrs);
		if((pPart == pEndPart) && (eqpIdx == endEqpIdx))
		{
			break;
		}
		partIdx++;
		if(partIdx >= lineParts.count())
		{
			if(pBeamLine->isCircle())
			{
				partIdx = 0;
			}
			else
			{
				break;
			}
		}
		pPart = lineParts.at(partIdx);
		eqpIdx = 0;
		eqpPtrs = pPart->getEqpPtrs();
		nEqpPtrs = pPart->getNEqpPtrs();
	}

	if(reverse)
	{
		reverseEqp();
	}

	// Finally it is possible that line does not contain equipment at all
	if(!eqps.count())
	{
		// nEqp = -1;	// !!!!!!!!!!!!!!!! ??????????????????????
		startX = 0;
		endX = 50;
		ascent = 5;
		descent = 5;
		y = 0;
		return 1;
	}

	// Build geometry of this line
	buildGeometry();
	addSectorNameSpace(false);
	return eqps.count();
}

/*
** FUNCTION
**		Draw border between two sectors and two sectors names of this border
**		Override method of SynLine: Sector labels are only drawn above line
**
**	PARAMETERS
**		painter	- Painter used for drawing
**		pItem	- Pointer to equipment item of sector border
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLine::drawSectorBorder(QPainter &painter, SynEqpItem *pItem)
{
	painter.setPen(VacMainView::getSynSectorColor());
	int centerX = pItem->getConnPoint().x();
	painter.drawLine(centerX, y - ascent, centerX, y + descent);

	// Name of sector before border is drawn between two sector borders, if no
	// sector border before this one - draw just before this border
	// If line is draw in reverse order - take sector after instead of sector before
	const char *pName = pItem->getNameBefore(reverse);
	int textY = y - ascent + pItem->getTextHeight();

	// Find previous device which is sector border
	SynEqpItem *pOther = NULL;
	int idx;
	for(idx = eqps.count() - 1 ; idx >= 0 ; --idx)
	{
		SynEqpItem *pPrev = eqps.at(idx);
		if(pPrev == pItem)
		{
			break;
		}
	}
	for(--idx ; idx >= 0 ; --idx)
	{
		SynEqpItem *pPrev = eqps.at(idx);
		if(pPrev->isSectorBorder())
		{
			pOther = pPrev;
			break;
		}
	}
	int textStartX = 0;
	int textWidth = painter.fontMetrics().boundingRect(pName).width();
	if(pOther)
	{
		textStartX = pOther->getConnPoint().x() + (centerX - pOther->getConnPoint().x()) / 2 -
			textWidth / 2;
	}
	else
	{
		textStartX = centerX - textWidth - 3;
	}
	painter.drawText(textStartX, textY, pName);

	// Name of sector after border is only drawn if there is no another
	// border after this one, otherwise name will appear as 'sector before'
	// of next border
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynEqpItem *pPrev = eqps.at(idx);
		if(pPrev == pItem)
		{
			break;
		}
	}
	pOther = NULL;
	for(++idx ; idx < eqps.count() ; ++idx)
	{
		pOther = eqps.at(idx);
		if(pOther->isSectorBorder())
		{
			break;
		}
	}
	if(!pOther)
	{
		pName = pItem->getNameAfter(reverse);
		painter.drawText(centerX + 3, textY, pName);
	}
}

/*
**	FUNCTION
**		Build geometry of sector icons
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLine::buildSectorIcons(void)
{
	int	iconStart, iconEnd, idx, idx2;

	SynEqpItem *pPrevBorder = NULL, *pItem;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		if(pItem->isSectorBorder())
		{
			pPrevBorder = pItem;
		}
		if((pItem->getType() != EqpType::SectorBefore) && (pItem->getType() != EqpType::SectorAfter))
		{
			continue;
		}
		// Find start of icon
		if(pPrevBorder)
		{
			iconStart = pPrevBorder->getConnPoint().x() + 2;
		}
		else
		{
			iconStart = startX + 2;
		}
		// Find end of icon
		SynEqpItem *pNextBorder = NULL;
		for(idx2 = eqps.count() - 1 ; idx2 >= 0 ; idx2--)
		{
			SynEqpItem *pNextItem = eqps.at(idx2);
			if(pNextItem == pItem)
			{
				break;
			}
			if(pNextItem->isSectorBorder())
			{
				pNextBorder = pNextItem;
			}
		}
		if(pNextBorder)
		{
			iconEnd = pNextBorder->getConnPoint().x() - 2;
		}
		else
		{
			iconEnd = endX - 2;
		}
		// Assign all parameters
		QRect iconRect(iconStart, y + descent,
			(iconEnd - iconStart), pItem->getIconHeight());
		pItem->setRect(iconRect);
		QPoint connPoint((pItem->getRect().x() + pItem->getRect().width()) / 2,
			(pItem->getRect().y() + pItem->getRect().height()) / 2);
		pItem->setConnPoint(connPoint);
	}

	// Update line limits if necessary
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		QRect iconRect = pItem->getRect();
		if((y - iconRect.y()) > ascent)
		{
			ascent = y - iconRect.y();
		}
		if((iconRect.y() + iconRect.height() - y) > descent)
		{
			descent = iconRect.y() + iconRect.height() - y;
		}
		/* L.Kopylov 21.04.2009 - it looks like X limits do not need correction at this point
		QPoint connPoint = pItem->getConnPoint();
		int minX = (iconRect.x() < connPoint.x() ? iconRect.x() : connPoint.x()) - startX;
printf("SectLine::buildSectorIcons(): %s: startX %d minX %d for %s\n", pBeamLine->getName(),
startX, minX, pItem->getName());
		if(minX < 0)
		{
			startX += minX;
		}
		int maxX = ((iconRect.x() + iconRect.width()) > connPoint.x() ?
				iconRect.x() + iconRect.width() : connPoint.x()) - startX;
		if(maxX > (endX - startX))
		{
			endX = maxX + startX;
		}
		*/
	}
}

