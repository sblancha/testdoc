//	Implementation of FunctionalTypeVBAKEOUT class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVBAKEOUT.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeVBAKEOUT::FunctionalTypeVBAKEOUT(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVBAKEOUT::fillCriteria(FunctionalType *pType)
{
	// Own criteria
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Connected, "Bakeout Rack is connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is Disconnected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Bakeout Rack in sector {sector} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Bakeout Rack in sector {sector} is Disconnected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Bakeout Rack in main part {mainPart} is Connected");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Bakeout Rack in main part {mainPart} is Disconnected");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Warning, "Bakeout Rack warning");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} warning ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} warning OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Warning ON for Bakeout Rack in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Warning OFF for Bakeout Rack in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Warning ON for Bakeout Rack in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Warning OFF for Bakeout Rack in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Error, "Bakeout Rack ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} error ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} error OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Error ON for Bakeout Rack in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Error OFF for Bakeout Rack in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Error ON for Bakeout Rack in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Error OFF for Bakeout Rack in main part {mainPart}");
}


/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeVBAKEOUT::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case Connected:
			result = "Connected";
			break;
		case Warning:
			result = "WARNING";
			break;
		case Error:
			result = "ERROR";
			break;
		default:
			break;
		}
		break;
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeVBAKEOUT::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "CONNECTED"))
		{
			result = Connected;
		}
		else if(!strcasecmp(name, "WARNING"))
		{
			result = Warning;
		}
		else if(!strcasecmp(name, "ERROR"))
		{
			result = Error;
		}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}

