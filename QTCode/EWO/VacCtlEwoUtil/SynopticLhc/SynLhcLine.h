#ifndef	SYNLHCLINE_H
#define	SYNLHCLINE_H

// Common functionalities for all LHC synoptic/sector lines

#include "LhcLineParts.h"
#include "SynLhcEqpItem.h"
#include "SynLhcSectorLabel.h"
#include "SynLine.h"
#include "SynLhcInjDumpLine.h"

#include "VacEqpTypeMask.h"
#include "DataEnum.h"
#include "Eqp.h"
#include "EqpMsgCriteria.h"

#include "VacType.h"

#include <QObject>
#include <QList>
#include <QColor>
#include <QPainter>

#include <stdio.h>

class BeamLine;
class BeamLinePart;
class Sector;

#include <QWidget>


// Free space on left side of view
#define	LEFT_MARGIN				(5)

// Free space on right side of view
#define	RIGHT_MARGIN			(5)

// Free space on top side of view
#define	TOP_MARGIN				(5)

// Free space on bottom side of view
#define	BOTTOM_MARGIN			(5)

// Spacing between EQL and CRYO lines
#define	QRL_CRYO_LINE_OFFSET	(8)

// Spacing around line with passive elements
#define	PASSIVE_VERT_OFFSET		(8)

// Spacing between two beam lines
#define	BEAM_LINE_OFFSET		(16)

// Width of symbol for non-controllable equipment
#define	PASSIVE_ELEM_WIDTH		(3)

// Minimum spacing between two symbols for non-controllable devices
#define	MIN_PASSIVE_SPACING		(1)		// L.Kopylov 01.06.2009 - was 3

// Minimum spacing between two icons for controllable device
#define	MIN_ACTIVE_SPACING		(1)		// L.Kopylov 01.06.2009 - was 3

// Minimum height of symbol for non-controllable equipment
#define	MIN_NON_CTL_HEIGHT		(3)

// Minimum height of symbol for non-controllable equipment
#define	MAX_NON_CTL_HEIGHT		(32)

// Common vertical offset for all devices of one group
#define	COMMON_ACTIVE_OFFSET	(5)		// L.Kopylov 22.05.2008 - was 6

// Minimum space between two line parts (for QRL and CRYO)
#define	VAC_LINE_SPACING		(5)

// Half height of line start/end symbols
#define LINE_START_END_HEIGHT	(5)

class SynLhcLine :  public QObject, public LhcLineParts
{
	Q_OBJECT

public:

	// Type of vacuum from synoptic point of view
	typedef enum
	{
		Qrl = 0,
		Cryo = 1,
		Pas = 2,
		OuterBeam = 3,
		CrossBeam = 4,
		InnerBeam = 5,
		CommonBeam = 6,
		Max = 7
	} SynVacTypeEnum;

public:
	SynLhcLine(QWidget *parent, int type, float start, float end);
	virtual ~SynLhcLine();

	virtual void connectToMobile(void);
	virtual void connectToWirelessMobile(void);

	virtual void clear(void);
	virtual void analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allEqpMask);
	virtual bool isRebuildRequired(unsigned vacTypeMask, VacEqpTypeMask &eqpMask);
	virtual int build(unsigned vacTypeMask, VacEqpTypeMask &eqpMask);
	void init(void);
	virtual float nextPosition(void);
	virtual void placeItem(int order, float &coord, int &connPos,
		float &endCoord, int &endConnPos);
	virtual void correctLastPosition(float coord, int connPos, int order);
	virtual void finishPartsSectors(int viewWidth);

	virtual void addAlarmIcons(void);
	virtual void calcVerticalLimits(void);
	virtual void moveVertically(int &delta);

	virtual void finishGeometry(void);

	virtual int draw(QPainter &painter, int viewStart, int viewEnd);\

	virtual void drawSectSelection(QPainter &painter); // [VACCO-929]
	virtual void setSectorGeometry(void) {}; // [VACCO-929] Dummy method, to be implemented in subclasses because the logic for BEAM and ISOL is different
	virtual QList<VacLinePart *> getPartList(void) { return *pPartList; } // [VACCO - 929]
	virtual QList<SynLhcSectorLabel *> getSectorLabelList(void) { return sectors; } // [VACCO-929]
	virtual SynLhcSectorLabel* getSectorFromXY(int sectx, int secty); // [VACCO-929]
	virtual QList<SynLhcInjDumpLine *>	getChildLines(void); // [VACCO-929]

	QPoint getEqpCoord(Eqp pEqp);

	virtual void setViewStart(int viewStart, int viewWidth, bool dragging);

	virtual const char *closestItem(int mouseX, int mouseY);

	virtual void dump(FILE *pFile);
	virtual const char *getLineName(void);

	virtual bool findDevicePosition(Eqp *pEqp, int &position, bool &visible);

	// The following methods shall be implemented in subclasses (when necessary)
	virtual void analyzeTypes(unsigned & /* allVacTypeMask */, VacEqpTypeMask & /*allCryoEqpMask */, VacEqpTypeMask & /* allBeamEqpMask */) {}
	virtual void finalSectorIconsGeometry(void) {}	// For sector view lines
	virtual void decideForSectorBorders(void) {}
	virtual void copyEqp(SynLhcLine * /* pSrcLine */, bool /* isUpperLine */) {}

	virtual bool getToolTip(const QPoint & /* point */, QString & /* text */, QRect & /* rect */) { return false; }

	// Access
	inline int getType(void) const { return type; }
	inline int getEndX(void) const { return endX; }
	inline int getY(void) const { return y; }
	inline int getAscent(void) const { return ascent; }
	inline int getDescent(void) const { return descent; }
	inline int getCrossLineY(void) const { return crossLineY; }
	inline void setCrossLineY(int coord) { crossLineY = coord; }
	inline const QList<SynLhcEqpItem *> &getEqps(void) const { return eqps; }
	inline void setStartSector(Sector *pSector) { pStartSector = pSector; }
	inline void setEndSector(Sector *pSector) { pEndSector = pSector; }
	virtual void setMode(DataEnum::DataMode newMode) { mode = newMode; }
	inline void setHideMagnets(bool flag) { hideMagnets = flag; }
	virtual bool isAlwaysIncludeSectorBorder(void) { return alwaysIncludeSectorBorder;  }
	virtual void setAlwaysIncludeSectorBorder(bool flag) { alwaysIncludeSectorBorder = flag;  }

	virtual inline void showMobilesNotConnectedInLine(void) { isShowMobiles = true; }
	virtual inline void hideMobilesNotConnectedInLine(void) { isShowMobiles = false; }

signals:
	void mobileStateChanged(void);

protected:
	// Widget where line will be drawn
	QWidget					*parent;

	// Pointer to start sector
	Sector					*pStartSector;

	// Pointer to end sector
	Sector					*pEndSector;

	// List of devices to be drawn
	QList<SynLhcEqpItem *>	eqps;

	// Sector labels in this line
	QList<SynLhcSectorLabel *>	sectors;

	// List of mobile devices which can potentially appear in this line
	QList<Eqp *>			mobileList;

	// Type of this line from synoptic point of view
	int						type;

	// Vacuum type mask
	unsigned				vacTypeMask;

	// Equipment type mask for this line
	VacEqpTypeMask 			eqpMask;

	// Minimum length of non-controllable equipment
	float					minPassiveLength;

	// Maximum length of non-controllable equipment
	float					maxPassiveLength;

	// Maximum height of sector name
	int						maxSectHeight;


	// Start coordinate of this line's image on unlimited drawing area
	int						startX;

	// End coordinate of this line's image on unlimited drawing area
	int						endX;

	// Y-coordinate of this line's image on unlimited drawing area - base line
	// Line is drwan horizontally, so there is just one coordinate
	int						y;

	// Ascent of this line's image above base line
	int						ascent;

	// Descent of this line's image below base line
	int						descent;

	// Vertical position of cross beam line - used by blue/red beam lines
	int						crossLineY;


	// Current index of line part
	int						curPartIdx;

	// Current index of equipment
	int						curEqpIdx;

	// Flag indicating if equipment or line partshall be processed
	bool					curEqp;


	
	// Flag indicating if sector border devices shall always be included
	bool					alwaysIncludeSectorBorder;

	// Flag indicating if equipment icons are placed above vacuum line
	bool					eqpAboveLine;

	// Flag indicating if equipment shall not be added to line - used for analysis
	bool					doNotAddEqp;


	// Flag indicating if passive equipment shall not be drawn
	bool					hideMagnets;


	// Flag indicating if mobile device list has been initialized
	// and this line has connected to activity signals of all mobile devices
	bool					mobileListReady;

	// Flag indicating if redrawing is required after changing active state
	// of mobile equipment
	bool					redrawRequiredForMobile;

	// Flag to show mobiles not connected on synoptic
	bool				isShowMobiles;

	// When applying new equipment type mask - it is required to keep scroll position
	// at position it was before applying new mask (== 'as close as possible to old
	// position). In order to achieve this - line shall fix last invisible device
	// and first visible device BEFORE applying new mask, making sure that these
	// devices will also be in line AFTER applying the mask

	// Last invisible device before applying new mask
	Eqp					*pLastInvisible;

	// First visible device before applying new mask
	Eqp					*pFirstVisible;

	// Data acquisition mode for this line
	DataEnum::DataMode	mode;

	// Flag indicating if this line contains interlock sources
	bool			haveInterlockSources;

	// List of equipments used to rebuilt qdialog when active state of Eqp changed
	QList<Eqp*> listEqpsActiveMayChange;

	// List of equipments used to rebuilt qdialog when position changed
	QList<Eqp*> listEqpsPositionMayChange;
	
	virtual void decideForEqpLocation(void);
	virtual void buildEquipment(void);
	virtual void buildEqpGroups();

	virtual void markInterlockSources(void);

	virtual void addEqp(Eqp *pEqp, int partIdx);
	virtual int findPartIdx(Eqp *pEqp);
	virtual bool isBeamVacuum(void);
	virtual void placeEqp(float &coord, int &connPos, float &endCoord, int &endConnPos);
	virtual void placePartStart(float &coord, int &connPos, float &endCoord, int &endConnPos);
	virtual void placePartEnd(float &coord, int &connPos, float &endCoord, int &endConnPos);
	void addSector(Sector *pSector, bool isByEqp, int start, int end, float endPos, int order);
	virtual bool vacTypeMatchesMask(unsigned vacType);

	virtual void drawParts(QPainter &painter, int viewStart, int viewEnd);
	void drawActive(QPainter &painter, int viewStart, int viewEnd);
	void drawActiveElement(SynLhcEqpItem *pItem, QPainter &painter);
	void drawCommonConn(SynLhcEqpItem *pItem, QPainter &painter);
	void setPainterForLine(QPainter &painter, int vacType);
	virtual void setLineColor(int vacType, QColor &color);


	virtual bool isMyMobile(Eqp *pEqp);

	// Methods which shall be reimplemented by subclasses
	virtual bool isMyEqp(Eqp * /* pEqp */, int & /*partIdx */) { return false; }
	virtual int vacTypeOfRegion(int /* regionType */) { return VacType::None; }

	virtual int calcSectorBorderPosition(SynLhcEqpItem * /* pItem */, int connPos) { return connPos; }
	virtual void drawPartStart(QPainter & /* painter */, int /* coord */) {}
	virtual void drawPartEnd(QPainter & /* painter */, int /* coord */) {}
	virtual void drawEqpSectorBorder(Eqp * /* pEqp */, int /* xCenter */, QPainter & /* painter */) {}
	virtual void drawSectLabels(QPainter & /* painter */, int /* viewStart */, int /* viewEnd */) {}
	virtual void drawSectorElement(SynLhcEqpItem * /* pItem */, QPainter & /* painter */) {}

private slots:
	void mobileStateChange(Eqp *pSrc, DataEnum::DataMode mode);
};

#endif	// SYNLHCLINE_H
