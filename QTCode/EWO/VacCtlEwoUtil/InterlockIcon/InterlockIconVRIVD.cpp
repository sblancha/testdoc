#include "InterlockIconVRIVD.h"

#include "qpainter.h"

InterlockIconVRIVD::InterlockIconVRIVD(QWidget * parent) :
InterlockIcon(parent)
{
	

}

InterlockIconVRIVD::~InterlockIconVRIVD()
{

}

void InterlockIconVRIVD::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	QFont calibri("Calibri", 10, QFont::Bold);
	QFont calibriSmall("Calibri", 8);
	//draw title
	QPen whitePen(Qt::white);
	QBrush blackBrush(Qt::black);

	painter.setBrush(blackBrush);
	painter.setPen(whitePen);
	painter.setFont(calibri);

	painter.drawRect(0, 0, 200, 20);
	painter.drawText(0, 0, 200, 20, Qt::AlignCenter | Qt::TextWrapAnywhere, this->name);


	painter.setPen(Qt::black);
	painter.setBrush(Qt::white);

	painter.drawRect(0, 20, 200, 180);
	painter.drawLine(0, 65, 165, 65);
	painter.drawLine(0, 110, 165, 110);
	painter.drawLine(0, 155, 165, 155);

	painter.drawLine(165, 20, 165, 200);

	painter.drawText(10, 20, 155, 20, Qt::AlignLeft | Qt::AlignVCenter, "INTCK1");
	painter.drawText(10, 65, 155, 20, Qt::AlignLeft | Qt::AlignVCenter, "INTCK2");
	painter.drawText(10, 110, 155, 20, Qt::AlignLeft | Qt::AlignVCenter, "INTCK3");
	painter.drawText(10, 155, 155, 20, Qt::AlignLeft | Qt::AlignVCenter, "INTCK4");

	painter.setFont(calibriSmall);
	painter.drawText(10, 40, 155, 20, Qt::AlignLeft | Qt::AlignVCenter, attributes.value("INTCK1", ""));
	painter.drawText(10, 85, 155, 20, Qt::AlignLeft | Qt::AlignVCenter, attributes.value("INTCK2", ""));
	painter.drawText(10, 130, 155, 20, Qt::AlignLeft | Qt::AlignVCenter, attributes.value("INTCK3", ""));
	painter.drawText(10, 175, 155, 20, Qt::AlignLeft | Qt::AlignVCenter, attributes.value("INTCK4", ""));
	

	painter.drawText(165, 20, 35, 180, Qt::AlignCenter, "OUT");


}


void InterlockIconVRIVD::moveIcon(int x, int y)
{
	this->move(x, y - 200 / 2);
}

QPoint InterlockIconVRIVD::getConnectionPoint(QString connectorName)
{
	int baseX = this->x();
	int baseY = this->y();

	if (QString::compare(connectorName, "InterlockOut") == 0)
	{
		baseX += 200;
		baseY += 110;
	}
	else if (QString::compare(connectorName, "Interlock1") == 0)
	{
		baseY += 45;
	}
	else if (QString::compare(connectorName, "Interlock2") == 0)
	{
		baseY += 90;
	}
	else if (QString::compare(connectorName, "Interlock3") == 0)
	{
		baseY += 135;
	}
	else if (QString::compare(connectorName, "Interlock4") == 0)
	{
		baseY += 180;
	}

	return QPoint(baseX, baseY);
}

void InterlockIconVRIVD::getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber)
{
	
	if (connectorName.contains("Out"))
	{
		direction = InterlockIconConnection::Right;
		offset = 0;
	}
	else if (connectorName.contains("Int"))
	{
		direction = InterlockIconConnection::Left;
		offset = 50;
	}
	else
	{
		direction = InterlockIconConnection::Undefined;
		offset = 0;
	}
	writeCableNumber = false;
}

QRect InterlockIconVRIVD::getBoundingRect()
{
	QPoint topLeft = QPoint(this->x(), this->y());
	QRect boundingRect(topLeft, topLeft + QPoint(200, 200));

	return boundingRect;
}