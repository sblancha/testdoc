#ifndef	VACICONVP_STD_IO_H
#define	VACICONVP_STD_IO_H

//	Icon for VP_STDIO - common for PP, TMP... Specific is realized by
// subclasses - subclasses must override drawType() method

#include "VacIcon.h"

class EqpVP_STDIO;

class VACCTLEWOUTIL_EXPORT VacIconVP_STD_IO : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVP_STD_IO(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVP_STD_IO();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);


protected:
	// Pointer to my device
	EqpVP_STDIO *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter);
	virtual void drawSymbols(QPainter &painter);
	virtual void drawForced(QPainter &painter);
	virtual void drawManual(QPainter &painter);
	virtual void drawFullInterlock(QPainter &painter);
	virtual void drawStartInterlock(QPainter &painter);
	virtual void drawCmdOff(QPainter &painter);
	virtual void drawCmdOn(QPainter &painter);
	virtual void drawType(QPainter &painter) = 0;
	
	void setPainterForMainImage(QPainter &painter);
};

#endif	// VACICONVP_STD_IO_H
