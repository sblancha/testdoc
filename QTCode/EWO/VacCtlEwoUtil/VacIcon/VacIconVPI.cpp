//	Implementation of VacIconVPI class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPI.h"
#include "VacMainView.h"

#include "EqpVPI.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0
#define COLOR_WARNING			255,255,0
#define COLOR_UNDERRANGE		0,180,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_ON			(0x00020000)
#define	RR1_OFF			(0x00010000)
#define	RR1_UNDERRANGE	(0x00000200)
#define	RR1_OTHER_WARN	(0x00001C00)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVPI::VacIconVPI(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 21);
}

VacIconVPI::~VacIconVPI()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPI::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(21, 21, 10, 26, -1);
}

VacIconContainer::Direction VacIconVPI::getSlaveDirection(void) const
{
	VacIconContainer::Direction result = VacIconContainer::Up;
	switch(direction)
	{
	case VacIconContainer::Up:
		result = VacIconContainer::Down;
		break;
	case VacIconContainer::Down:
		result = VacIconContainer::Up;
		break;
	case VacIconContainer::Left:
		result = VacIconContainer::Right;
		break;
	case VacIconContainer::Right:
		result = VacIconContainer::Left;
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPI::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVPI"), "VacIconVPI::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVPI *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPI::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPI::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("PR");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPI::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPI::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPI::paintEvent(QPaintEvent * /* pEvent */)
{
	unsigned rr1 = 0;
	bool blockedOff = false;
	bool plcAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		blockedOff = pEqp->isBlockedOff(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	/*
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
	*/
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		painter.fillRect(10 - pipeWidth / 2, 0, pipeWidth ? pipeWidth : 1, 4, pipeColor);
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.fillRect(10 - pipeWidth / 2, 18, pipeWidth ? pipeWidth : 1, 4, pipeColor);
		break;
	case VacIconContainer::Left:	// 90 degrees clockwise
		painter.fillRect(0, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		break;
	case VacIconContainer::Right:	// 90 degrees conuterclockwise
		painter.fillRect(18, 10 - pipeWidth / 2, 4, pipeWidth ? pipeWidth : 1, pipeColor);
		break;
	}

	painter.save();

	// Prepare painter's coordinate system for drawing
	// Coordinate system (0,0) is center of widget
	painter.translate(10, 10); // width() / 2, height() / 2);

	// fillRect() produces not perfect result after rotation,
	// so draw rectangles before painter rotation
	// Draw pump image
	setPainterForImage(painter, ctlStatus, rr1, plcAlarm);

	// 1) Filled rectangle
	painter.drawRect(-9, -9, 18, 18);

	// Turn coordinate system according to direction
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	// Coordinate system is ready - draw
	drawImage(painter, rr1, blockedOff, plcAlarm);

	// Draw selection ON TOP of image
	painter.restore();	// Original coordinate system - see save() above
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
}

void VacIconVPI::drawImage(QPainter &painter, unsigned /* rr1 */, bool blockedOff, bool /* plcAlarm */)
{
	/*
	// Draw vacuum pipe - short line
	painter.fillRect(-(pipeWidth / 2), -11, pipeWidth ? pipeWidth : 1, 3, pipeColor);
	
	// Draw pump image
	setPainterForImage(painter, rr1, plcAlarm);

	// 1) Filled rectangle
	QRectF rectF(-9, -9, 19, 19);
	// painter.fillRect(rectF, painter.brush());
	painter.drawRect(rectF);
	*/

	// 2) Two vertical lines
	painter.setPen(Qt::black);
	painter.drawLine(-7, -9, -7, 9);
	painter.drawLine(7, -9, 7, 9);

	// 3) Two slope lines emanating from connection point
	// pen.setWidth(2);
	painter.drawLine(0, -9, -7, 0);
	painter.drawLine(0, -9, 7, 0);

	// 4) blocked OFF symbol
	if(blockedOff)
	{
		painter.drawLine(-9, 7, 9, -1);
		painter.drawLine(-9, 8, 9, 0);
		painter.drawLine(-9, -1, 9, 7);
		painter.drawLine(-9, 0, 9, 8);
	}
}


void VacIconVPI::setPainterForImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm)
{
	QColor	color(COLOR_UNDEFINED);
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(!plcAlarm)
		{
			if(rr1 & RR1_VALID)
			{
				if(rr1 & RR1_ERROR)
				{
					color.setRgb(COLOR_ERROR);
				}
				else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))
				{
					color.setRgb(COLOR_ERROR);
				}
				else if(rr1 & RR1_WARNING)
				{
					// Warnins 'underrange' and 'self-protection' are shown in special color
					// when gauge is ON and no other warnings
					if((rr1 & RR1_ON) &&
						((rr1 & RR1_UNDERRANGE) && (!(rr1 & RR1_OTHER_WARN))))
					{
						color.setRgb(COLOR_UNDERRANGE);
					}
					else
					{
						color.setRgb(COLOR_WARNING);
					}
				}
				else if(rr1 & RR1_OFF)
				{
					color.setRgb(COLOR_OFF);
				}
				else if(rr1 & RR1_ON)
				{
					color.setRgb(COLOR_ON);
				}
			}
		}
		break;
	}
	painter.setBrush(color);
	// L.Kopylov 04.02.2014 painter.setPen(color);
	painter.setPen(Qt::gray);
}

void VacIconVPI::drawInterlockSrcArrow(QPainter &painter, bool before, bool after)
{
	if(!(before || after))
	{
		return;
	}
	painter.resetTransform();
	painter.setPen(VacMainView::getSynPassiveLineColor());
	int start, end, pos;
	switch(direction)
	{
	case VacIconContainer::Up:
		start = x();
		end = start + width();
		pos = y() + height() + 5;
		painter.drawLine(start, pos, end, pos);
		if(before)
		{
			painter.drawLine(start, pos, start + 3, pos - 3);
			painter.drawLine(start, pos, start + 3, pos + 3);
		}
		if(after)
		{
			painter.drawLine(end, pos, end - 3, pos - 3);
			painter.drawLine(end, pos, end - 3, pos + 3);
		}
		break;
	case VacIconContainer::Down:	// 180 degrees
		start = x();
		end = start + width();
		pos = y() - 5;
		painter.drawLine(start, pos, end, pos);
		if(before)
		{
			painter.drawLine(start, pos, start + 3, pos - 3);
			painter.drawLine(start, pos, start + 3, pos + 3);
		}
		if(after)
		{
			painter.drawLine(end, pos, end - 3, pos - 3);
			painter.drawLine(end, pos, end - 3, pos + 3);
		}
		break;
	case VacIconContainer::Left:	// 90 degrees clockwise
		start = y();
		end = start + height();
		pos = x() + width() + 5;
		painter.drawLine(pos, start, pos, end);
		if(before)
		{
			painter.drawLine(pos, start, pos + 3, start + 3);
			painter.drawLine(pos, start, pos - 3, start + 3);
		}
		if(after)
		{
			painter.drawLine(pos, end, pos + 3, end - 3);
			painter.drawLine(pos, end, pos - 3, end - 3);
		}
		break;
	case VacIconContainer::Right:	// 90 degrees conuterclockwise
		start = y();
		end = start + height();
		pos = x() - 5;
		painter.drawLine(pos, start, pos, end);
		if(before)
		{
			painter.drawLine(pos, start, pos + 3, start + 3);
			painter.drawLine(pos, start, pos - 3, start + 3);
		}
		if(after)
		{
			painter.drawLine(pos, end, pos + 3, end - 3);
			painter.drawLine(pos, end, pos - 3, end - 3);
		}
		break;
	}
}

