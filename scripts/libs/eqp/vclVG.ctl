const string
	COLOR_ON = "{51,204,0}", //green
	COLOR_BAD_FOREGROUND = "{255,255,255}", //white
	COLOR_OK_FOREGROUND = "{76,89,93}", //dark grey 
   COLOR_OK_BACKGROUND = "{255,255,255}", //white
	COLOR_ERROR = "{255,0,255}", //violet
	COLOR_WARNING = "{255,255,0}"; //yellow
const unsigned	
  VG_SPS_R_AUTO = 0x08000000u,
  VG_SPS_R_OFF = 0x00010000u,
  VG_SPS_R_ON = 0x00020000u,
  VG_SPS_R_PRESS_VALID = 0x00040000u,
  VG_SPS_R_PROTECT = 0x00080000u,
//begin VGI specific
  VG_SPS_R_RESID_PRESS_VALID = 0x00080000u,
  VG_SPS_R_DEGAS_VALID = 0x00100000u,
  VG_SPS_R_INTERLOCKS = 0x00200000u,
//end VGI specific
  VG_SPS_R_WARNINGS = 0x00400000u,
  VG_SPS_R_ERRORS = 0x00800000u,
  VG_SPS_R_REMOTE = 0x20000000u,
  VG_SPS_R_VALID = 0x40000000u,
  VG_SPS_R_LOCKED = 0x80000000u,
  VG_SPS_R_PIRANI = 0x00100000u,
  VG_SPS_R_UNDERRANGE = 0x200u,
  VG_SPS_R_SELFPROT = 0x100u,
  VG_SPS_R_REST_WARNING = 0x1c00u,
//begin VG_STD
  VG_STD_SPS_R_HW_INTERLOCK = 0x20000000u,  
  VG_STD_SPS_R_LOCAL = 0x10000000u,
  VG_STD_SPS_R_MANUAL = 0x4000000u,
  VG_STD_SPS_R_INTERLOCK = 0x200000u,
  VG_STD_SPS_R_START_INTERLOCK= 0x100000u,
  VG_STD_SPS_R_AUTO_ON_ORDER= 0x80000u,
  VG_STD_SPS_R_MAN_ON_ORDER = 0x40000u,
  VG_STD_SPS_R_START_ON_DO = 0x20000u,
  VG_STD_SPS_R_FORCED = 0x10000u,
//end VG_STD
//begin VGI specific
				
  VG_SPS_R_DEGAS_OFF = 0x100u,
  VG_SPS_R_DEGAS_ON = 0x200u,
  VG_SPS_R_MODUL_OFF = 0x400u,
  VG_SPS_R_MODUL_ON = 0x800u,
  VG_SPS_R_FILAMENT_2 = 0x1000u,
  VG_SPS_R_FILAMENT_1 = 0x2000u,
  VG_SPS_R_MEASURE_OFF = 0x4000u,
  VG_SPS_R_MEASURE_ON = 0x8000u,
//end VGI specific
//begin VG_PT specific  
  VG_PT_SPS_R_THRESHOLD_ON = 0x00100000u,
  VG_PT_SPS_R_FORCE_ORDER = 0x00080000u,
  VG_PT_SPS_R_ON_ORDER = 0x00040000u,
  VG_PT_SPS_R_PROTECTED = 0x00020000u,
  VG_PT_SPS_R_FORCED = 0x00010000u,
// RR2
	VG_PT_SPS_R_AL3 = 0x8000u,
	VG_PT_SPS_R_AL2 = 0x4000u,
	VG_PT_SPS_R_AL1 = 0x2000u;

//end VG_PT specific	


const unsigned VG_SPS_W_REMOTE = 0x2000u,
  VG_SPS_W_AUTO = 0x0800u,
  VG_SPS_W_MANUAL = 0x0400u,
  VG_SPS_W_ON = 0x0200u,
  VG_SPS_W_OFF = 0x0100u,
  VG_SPS_W_DEGAS_OFF = 0x0001u,
  VG_SPS_W_DEGAS_ON = 0x0002u,
  VG_SPS_W_MODUL_OFF = 0x0004u,
  VG_SPS_W_MODUL_ON = 0x0008u,
  VG_SPS_W_FILAMENT_1 = 0x0010u,
  VG_SPS_W_FILAMENT_2 = 0x0020u,
  VG_SPS_W_MEASURE_OFF = 0x0040u,
  VG_SPS_W_MEASURE_ON = 0x0080u,
// VG_STD specific
  VG_SPS_W_FORCE = 0x4000u,
  VG_SPS_W_RESET = 0x8000u,
  VG_SPS_W_LOCAL = 0x1000u,
// VG_PT specific
  VG_SPS_W_VG_PT_OFF = 0x0100u,
  VG_SPS_W_VG_PT_ON_PROTECT = 0x0200u,
  VG_SPS_W_VG_PT_ON_FORCED = 0x0400u;
  
// PS_CMW specific
const int VG_PS_CMW_W_ON = 10,
  VG_PS_CMW_W_OFF = 11;
const int VG_PS_CMW_R_ON = 10,
  VG_PS_CMW_R_OFF = 11,
  VG_PS_CMW_R_VALID = 1;

// Constants for members of dyn_anytype, containing TPG300 parameters
private const int VG_DP_NAME = 1;          // DP name of gauge [string]
private const int TPG_DP_NAME = 2;         // 'DP' name of TPG300 where gauge is connected [string]
private const int TPG_CHANNEL = 3;         // Channel of gauge within TPG300 [int]
private const int TPG_CONFIG_DP_NAME = 4;  // DP name of TPG config in PLC [string]
private const int TPG_CONFIG_CHANNEL = 5;  // Channel No of TPG config in PLC [int]
private const int READ_RR1 = 6;            // Value of RR1 DPE from callback [unsigned]
private const int READ_RR2 = 7;            // Value of RR2 DPE from callback [unsigned]
private const int READ_RR3 = 8;            // Value of RR3 DPE from callback [unsigned]

// Config data for TPG300 settings - see constants above
private dyn_anytype configData;

// Max number of tries for writing to TPG300
private const int	TRIES_NUMBER = 4;

// Timeout factor for quering one value [sec]
private const int	TIMEOUT_FACTOR = 4;

// Bit mask - communicator is busy processing request
private const unsigned	TPG_CONFIG_BUSY = 0x1000000u;

// Value of RR1 for gauge - used during switch OFF procedure
private unsigned gaugeRR1;

//LhcVacActionPossible_VG:
/** Check if gauge is in state that allows executing actions for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, action to be executed
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_VG(string dpName, string dpType, dyn_anytype act, bool &possible,
  dyn_string &exceptionInfo) {
  dyn_errClass 	err;
  unsigned	state;
  switch(dpType) {
  case "VGM_C0":
  case "VGR_C0":
  case "VGR_T0":
  case "VGP_VELO":
  case "VGP_VELO_DETECTOR":
  case "VGP_VELO_BEAM":
  case "VGR_VELO":
  case "VGR_VELO_DETECTOR":
  case "VGR_VELO_BEAM":
  case "VGA_VELO":
  case "VGD_VELO":
  case "VGFH_C":
  case "VGR_PS_CMW":
  case "VGP_PS_CMW":
  case "VGI_PS_CMW":
  case "VG_PT":
  case "VG_UC":
  case "VG_UF":  
    possible = true;
    return;
  }
  // Actions enable/disable TPG300 hot start are possible independently on LOCKED state
  unsigned action = act[1] & LVA_ACTION_MASK;
  switch(action)
  {
  case LVA_TPG300_HOT_START_DISABLE:
  case LVA_TPG300_HOT_START_ENABLE:
    possible = true;
    return;
  }
  dpGet(dpSubStr(dpName, DPSUB_DP) + ".RR1", state);
  err = getLastError();
  if (dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacActionPossible_VG: Read RR1 for <" + dpName + "> failed: " + err, "");
    possible = false;
    return;
  }
  if(state & VG_SPS_R_LOCKED)
  {
    possible = FALSE;
  }
  else
  {
    possible = TRUE;
  }
}


//LhcVacDeviceAction_VG:
/** Execute action for gauge. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VG(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  unsigned	newWr1;
  mapping   mConnectDpes;
  
  if((dpType == "VGR_PS_CMW") || (dpType == "VGP_PS_CMW"))
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Equipment of this type is read-only in PVSS. Control using WorkingSet", "");
  }
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_FORCED_ON:
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_ON | VG_SPS_W_MANUAL;
    break;
  case LVA_DEVICE_ON:
    if((dpType == "VGR_PS_CMW") || (dpType == "VGP_PS_CMW"))
    {
      newWr1 = VG_PS_CMW_W_ON;
    }
    else
    {
      newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_ON | VG_SPS_W_MANUAL;
    }
    break;
  case LVA_DEVICE_ON_AUTO:	// Special case for Penning controlled via TPG300
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_ON | VG_SPS_W_AUTO | VG_SPS_W_MANUAL;
    break;	
  case LVA_DEVICE_OFF:
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_OFF | VG_SPS_W_MANUAL;
    break;
  case LVA_DEVICE_AUTO:
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_AUTO;
    break;
  case LVA_DEVICE_SET_BLOCKED_OFF:
  case LVA_DEVICE_CLEAR_BLOCKED_OFF:
    ProcessBlockedOFF(dpName, dpType, act, exceptionInfo);
    return;
    break;
  case LVA_VG_STD_REMOTE:
    newWr1 = VG_SPS_W_REMOTE;
    break;
  case LVA_VG_STD_FORCE:
    newWr1 = VG_SPS_W_FORCE;
    break;
  case LVA_DEVICE_RESET:
    newWr1 = VG_SPS_W_RESET;
    break;
  case LVA_VG_STD_MANUAL:
    newWr1 = VG_SPS_W_MANUAL;
    break;
  case LVA_VG_STD_LOCAL:
    newWr1 = VG_SPS_W_LOCAL;
    break;
  case LVA_DEVICE_VG_PT_ON_FORCED:
    newWr1 = VG_SPS_W_VG_PT_ON_FORCED;
    break;
  case LVA_DEVICE_VG_PT_ON_PROTECT:
    newWr1 = VG_SPS_W_VG_PT_ON_PROTECT;
    break;    
  case LVA_DEVICE_VG_PT_OFF:
    newWr1 = VG_SPS_W_VG_PT_OFF;
    break;    
  case LVA_TPG300_HOT_START_DISABLE:
    LhcVacVgDisableTpg300HotStart(dpName, (dynlen(action) > 1 ? action[2] : ""), exceptionInfo);
    return;
  case LVA_TPG300_HOT_START_ENABLE:
    LhcVacVgEnableTpg300HotStart(dpName, (dynlen(action) > 1 ? action[2] : ""), exceptionInfo);
    return;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["PR"] = PR_VALUE_OVERRANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;    
    mConnectDpes["AL1"] = false;
    mConnectDpes["BlockedOFF"] = false;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["PR"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;    
    mConnectDpes["AL1"] = NO_VALUE_CHANGE;
    mConnectDpes["BlockedOFF"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    // Other actions may be VGI-specific
    if(dpType == "VGI") {
      LhcVacDeviceAction_VGI(dpName, dpType, action, exceptionInfo);
    }
    else {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacDeviceAction_VG: unknown action " + act, "");
    }
    return;
  }

  // Check if action is possible
  bool		possible;
  LhcVacActionPossible_VG(dpName, dpType, action, possible, exceptionInfo);
  if(dynlen(exceptionInfo) != 0) return;
  if(! possible)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VG: Action forbidden for  <" + dpName + ">", "");
    return;
  }	

  // Execute
  LhcVacSetActionDpeValue(dpName + ".WR1", newWr1, exceptionInfo);
}

//LhcVacDeviceAction_VGI:
/** Execute action for VGI. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VGI(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  mapping       mConnectDpes;
  LhcVacActionPossible_VGI(dpName, action, exceptionInfo);
  if(dynlen(exceptionInfo) != 0)
  {
    return;
  }
  unsigned newWr1 = 0, newWr2 = 0;
  string valueDpe;  // Where analog value shall be written, the value itself must be in action[2]
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
// set degasing
  case LVA_DEVICE_DEGAS_ON:
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_DEGAS_ON;
    break;
  case LVA_DEVICE_DEGAS_OFF:
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_DEGAS_OFF;
    break;
// set modulation
  case LVA_DEVICE_MODUL_ON:
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_MODUL_ON;
    break;
  case LVA_DEVICE_MODUL_OFF:
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_MODUL_OFF;
    break;
// set filament
  case LVA_DEVICE_FILAMENT_1:
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_FILAMENT_1;
    break;
  case LVA_DEVICE_FILAMENT_2:
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_FILAMENT_2;
    break;
// set measuring
  case LVA_DEVICE_MEASURE_ON:
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_MEASURE_ON;
    break;
  case LVA_DEVICE_MEASURE_OFF:
    newWr1 = VG_SPS_W_REMOTE | VG_SPS_W_MEASURE_OFF;
    break;
  case LVA_DEVICE_RESET:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x80000000u;
    break;
  case LVA_VGI_SET_EMISSION_CURRENT:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x01000000u;
    valueDpe = "WINT";
    break;
  case LVA_VGI_SET_RESIDUAL_PRESSURE:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x02000000u;
    valueDpe = "WFLOAT";
    break;
  case LVA_VGI_SET_MODULATION_FACTOR:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x04000000u;
    valueDpe = "WINT";
    break;
  case LVA_VGI_SET_SENSIBILITY:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x08000000u;
    valueDpe = "WINT";
    break;
  case LVA_VGI_SET_GAIN:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x10000000u;
    valueDpe = "WINT";
    break;
  case LVA_VGI_SET_DEGAS_TIME:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x20000000u;
    valueDpe = "WINT";
    break;
  case LVA_VGI_SET_MODULATION_TIME:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x40000000u;
    valueDpe = "WINT";
    break;
  case LVA_VGI_SET_DEGAS_CURRENT:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x10000u;
    valueDpe = "WINT";
    break;
  case LVA_VGI_SET_GRID_VOLTAGE:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x040000u;
    valueDpe = "WINT";
    break;
  case LVA_VGI_SET_ANALOG_OUT_OFFSET:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x00080000u;
    valueDpe = "WINT";
    break;
  case LVA_VGI_SET_OVER_PANGE_PRESSURE:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x020000u;
    valueDpe = "WFLOAT";
    break;
  case LVA_VGI_ENABLE_LOCAL:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x00001000u;
    break;
  case LVA_VGI_DISABLE_LOCAL:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x00000800u;
    break;
  case LVA_VGI_AUTO_GAIN:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x00000400u;
    break;
  case LVA_VGI_DEFAULT_PARAM:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x00000200u;
    break;
  case LVA_VGI_MBAR_UNIT:
    newWr1 = VG_SPS_W_REMOTE;   
    newWr2 = 0x00000100u;
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["PR"] = PR_VALUE_OVERRANGE;
    mConnectDpes["EmissionCurrent"] = NO_VALUE_CHANGE;
    mConnectDpes["ResidualPR"] = NO_VALUE_CHANGE;
    mConnectDpes["CollectorCurrent"] = NO_VALUE_CHANGE;
    mConnectDpes["ModulationFactor"] = NO_VALUE_CHANGE;
    mConnectDpes["Sensibility"] = NO_VALUE_CHANGE;
    mConnectDpes["Gain"] = NO_VALUE_CHANGE;
    mConnectDpes["DegasTime"] = NO_VALUE_CHANGE;
    mConnectDpes["ModulationTime"] = NO_VALUE_CHANGE;
    mConnectDpes["FilamentDegasCurrent"] = NO_VALUE_CHANGE;
    mConnectDpes["GridVoltage"] = NO_VALUE_CHANGE;
    mConnectDpes["AnalogOutOffset"] = NO_VALUE_CHANGE;
    mConnectDpes["OverRangePR"] = NO_VALUE_CHANGE;
    mConnectDpes["PressureUnit"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WR2"] = NO_VALUE_CHANGE;
    mConnectDpes["WINT"] = NO_VALUE_CHANGE;
    mConnectDpes["WFLOAT"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = false;
    mConnectDpes["AL2"] = false;
    mConnectDpes["AL3"] = false;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["PR"] = NO_VALUE_CHANGE;
    mConnectDpes["EmissionCurrent"] = NO_VALUE_CHANGE;
    mConnectDpes["ResidualPR"] = NO_VALUE_CHANGE;
    mConnectDpes["CollectorCurrent"] = NO_VALUE_CHANGE;
    mConnectDpes["ModulationFactor"] = NO_VALUE_CHANGE;
    mConnectDpes["Sensibility"] = NO_VALUE_CHANGE;
    mConnectDpes["Gain"] = NO_VALUE_CHANGE;
    mConnectDpes["DegasTime"] = NO_VALUE_CHANGE;
    mConnectDpes["ModulationTime"] = NO_VALUE_CHANGE;
    mConnectDpes["FilamentDegasCurrent"] = NO_VALUE_CHANGE;
    mConnectDpes["GridVoltage"] = NO_VALUE_CHANGE;
    mConnectDpes["AnalogOutOffset"] = NO_VALUE_CHANGE;
    mConnectDpes["OverRangePR"] = NO_VALUE_CHANGE;
    mConnectDpes["PressureUnit"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WR2"] = NO_VALUE_CHANGE;
    mConnectDpes["WINT"] = NO_VALUE_CHANGE;
    mConnectDpes["WFLOAT"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = NO_VALUE_CHANGE;
    mConnectDpes["AL2"] = NO_VALUE_CHANGE;
    mConnectDpes["AL3"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VG: unknown action " + act, "");
    return;
  }
//120302mms  Change order of WR1-WR2 setting: First should be WR2
//DebugN("New Value", dpName + "." + valueDpe, newWr1, newWr2, action[2]);
  if(newWr2 != 0)
  {
    if(valueDpe != "")
    {
      if(dynlen(action) < 2)
      {
        fwException_raise(exceptionInfo, 
          "ERROR", "LhcVacDeviceAction_VGI: missing value for action " + act, "");
        return;
      }
      LhcVacSetActionDpeValue(dpName + "." + valueDpe, action[2], exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        return;
      }
      delay(0, 100);  // To be sure value arrives to PLC before WR2
    }
    LhcVacSetActionDpeValue(dpName + ".WR2", newWr2, exceptionInfo);
  }
  // Write new value(s) to DPE(s)
  if(newWr1 != 0)
  {
    LhcVacSetActionDpeValue(dpName + ".WR1", newWr1, exceptionInfo);
  }

}
void LhcVacDeviceAction_VG_DP(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  anytype	targetValue;
  string targetDpe = ".WR1";
  mapping   mConnectDpes;
        
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
    targetValue = 0x0600u;  // MANUAL + ON(Manual)
    break;
  case LVA_DEVICE_OFF:
    targetValue = 0x0500u;  // MANUAL + OFF(Manual)
    break;
  case LVA_DEVICE_AUTO:
    targetValue = 0x0800u;  // AUTO
    break;
  case LVA_DEVICE_MANUAL:
    targetValue = 0x0400u;  // MANUAL
    break;
  case LVA_DEVICE_ENABLE_PENNING:  // Enable Penning Gauge
    targetValue = 0x0002u;
    break;
  case LVA_DEVICE_DISABLE_PENNING:  // Disable Penning Gauge
    targetValue = 0x0001u;
    break;
  case LVA_DEVICE_SET_LOW_THRESHOLD:
    if(dynlen(action) != 2)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacDeviceAction_VG_DP: missing LowThreshold value", "");
      return;
    }
    targetDpe = ".WR_LowThreshold";
    targetValue = (float)action[2];
    break;
  case LVA_DEVICE_SET_HIGH_THRESHOLD:
    if(dynlen(action) != 2)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacDeviceAction_VG_DP: missing HighThreshold value", "");
      return;
    }
    targetDpe = ".WR_HighThreshold";
    targetValue = (float)action[2];
    break;
  case LVA_DEVICE_SET_BLOCKED_OFF:
  case LVA_DEVICE_CLEAR_BLOCKED_OFF:
    ProcessBlockedOFF(dpName, dpType, act, exceptionInfo);
    return;
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["StartCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["Status"] = NO_VALUE_CHANGE;
    mConnectDpes["PR"] = PR_VALUE_OVERRANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_LowThreshold"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_HighThreshold"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["StartCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["Status"] = NO_VALUE_CHANGE;
    mConnectDpes["PR"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_LowThreshold"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_HighThreshold"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacDeviceAction_VG_DP: unknown action " + act, "");
    return;
  }

  // Check if action is possible
  bool		possible;
  LhcVacActionPossible_VG(dpName, dpType, action, possible, exceptionInfo);
  if(dynlen(exceptionInfo) != 0) return;
  if(! possible)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction_VG_DP: Action forbidden for  <" + dpName + ">", "");
    return;
  }	

  // Execute
  LhcVacSetActionDpeValue(dpName + targetDpe, targetValue, exceptionInfo);
}
/**
@brief Check VGI specific conditions to perform action
@param[in]  dpName         DP name
@param[in]  action         enum
@param[out] exceptionInfo  details of any exceptions are returned here
*/
void LhcVacActionPossible_VGI(const string dpName, const dyn_anytype act,
	dyn_string &exceptionInfo)
{
  unsigned      stateRR2;
  dyn_errClass 	err;

  dpGet(dpName + ".RR2:_online.._value",stateRR2);
  err = getLastError();
  if (dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacActionPossible_VGI: Read VGI(RR2) info for <" + dpName + "> failed: " + err, "");
    return;
  }
  unsigned action = act[1];
  switch(action)
  {
  case LVA_DEVICE_MEASURE_ON:
    if((stateRR2 & VG_SPS_R_DEGAS_ON) || (stateRR2 & VG_SPS_R_MODUL_ON))
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacActionPossible_VGI: Action (measuring) forbidden for  <" + dpName + "> (modulation or degasing is ON)", "");
      return;
    }
    break;
  case LVA_DEVICE_MODUL_ON:
    if(stateRR2 & VG_SPS_R_MEASURE_OFF)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacActionPossible_VGI: Action (modulation) forbidden for  <" + dpName + "> (measuring is OFF)", "");
      return;
    }
    break;
  case LVA_DEVICE_DEGAS_ON:
    if(stateRR2 & VG_SPS_R_MEASURE_ON)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacActionPossible_VGI: Action (degasing) forbidden for  <" + dpName + "> (measuring is ON)", "");
      return;
    }
    break;
  case LVA_DEVICE_FILAMENT_1:
  case LVA_DEVICE_FILAMENT_2:
    if((stateRR2 & VG_SPS_R_MEASURE_ON) || (stateRR2 & VG_SPS_R_DEGAS_ON) || (stateRR2 & VG_SPS_R_MODUL_ON))
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacActionPossible_VGI: Action (filament) forbidden for  <" + dpName + "> (gauge is ON)", "");
      return;
    }
    break;
  }
}
//LhcVacGetRequiredActPriv_VGR_P:
/** Check if gauge is in a domain that need special privelege to execute action for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param defaultPriv: in/out default privilige for action/necessary privilege for action.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacGetRequiredActPriv_VGR_P(string dpName, const dyn_anytype action, int &defaultPriv, dyn_string &exceptionInfo)
{
  dyn_string domains; 
  LhcVacDeviceDomain(dpName, domains, exceptionInfo);

  if(dynContains(domains, "BGI") != 0)
  {
    switch(action[1] & LVA_ACTION_MASK)
    {
    case LVA_DEVICE_ON:
    case LVA_DEVICE_OFF:
    case LVA_DEVICE_AUTO:
    case VG_SPS_W_VG_PT_ON_FORCED:
    case VG_SPS_W_VG_PT_ON_PROTECT:
    case VG_SPS_W_VG_PT_OFF:
     if(defaultPriv < 3)
      {
        defaultPriv = 3;
      }
      break;
    }
    return;
  }

  // Switching OFF gauges in group VPG_6E01 requires Expert privilege
  switch(action[1] & LVA_ACTION_MASK)
  {
  case LVA_DEVICE_OFF:
    if(VacIsDeviceUsedInProcess(dpName, "VPG_6E01", makeDynString()))
    {
      if(defaultPriv < 3)
      {
        defaultPriv = 3;
      }
    }
    break;
  }
  
  // For gauges on beam vacuum: switching OFF when beam is present requires admin privilege
  // L.Kopylov after discussion with F.Antoniotti - 2 changes:
  // 1) not only OFF, but also ON commands require check
  // 2) expert privilege is enough for both actions
  switch(action[1] & LVA_ACTION_MASK)
  {
    case LVA_DEVICE_ON:
    case LVA_DEVICE_OFF:
    case LVA_DEVICE_AUTO:
    case LVA_DEVICE_ON_AUTO:
    case VG_SPS_W_VG_PT_ON_FORCED:
    case VG_SPS_W_VG_PT_ON_PROTECT:
    case VG_SPS_W_VG_PT_OFF:
      break;
    default:
      return;
  }
  string modeDpName;
  LhcVacGetMachineModeDp(modeDpName);
  if(modeDpName == "")
  {
    return;
  }
  string	mainPart, sector1, sector2;
  bool		isBorder;
  int		devVacuum;
  LhcVacDeviceVacLocation(dpName, devVacuum, sector1, sector2, mainPart, isBorder, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacGetRequiredActPriv_VGR_P: LhcVacDeviceVacLocation(" + dpName + ") failed", "");
    return;
  }

  int		mode;
  dyn_errClass 	err;
  if((devVacuum == LVA_VACUUM_BLUE_BEAM) || (devVacuum == LVA_VACUUM_CROSS_BEAM) || (devVacuum == LVA_VACUUM_BEAMS_COMMON))
  {	
    dpGet(modeDpName + ".Beam_B.Beam", mode);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacGetRequiredActPriv_VGR_P: Read <" + modeDpName + ".Beam_B.Beam> failed: " + err, "");
      return;
    }
    if(mode == 1)  // expert privillege is required
    {
      dynAppend(glActSpecialMessages, "BLUE BEAM MODE ON");
      if(defaultPriv < 3)
      {
        defaultPriv = 3;
      }
      return;
    }
  }
  if((devVacuum == LVA_VACUUM_RED_BEAM) || (devVacuum == LVA_VACUUM_CROSS_BEAM) || (devVacuum == LVA_VACUUM_BEAMS_COMMON))
  {	
    dpGet(modeDpName + ".Beam_R.Beam", mode);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacGetRequiredActPriv_VGR_P: Read <" + modeDpName + ".Beam_R.Beam> failed: " + err, "");
      return;
    }
    if(mode == 1)  // expert privillege is required
    {
      dynAppend(glActSpecialMessages, "RED BEAM MODE ON");
      if(defaultPriv < 3)
      {
        defaultPriv = 3;
      }
     }
  }
}
//LhcVacGetRequiredActPriv_VG_STD:
/** Check if valve is in state that allows executing actions for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param defaultPriv: in/out default privilige for action/necessary privilege for action.
	@param exceptionInfo: out, details of any exceptions are returned here
*/

void LhcVacGetRequiredActPriv_VG_STD(string dpName, const dyn_anytype action, int &defaultPriv, dyn_string &exceptionInfo)
{
  string dpType = dpTypeName(dpName);
  switch(dpType)
  {
  case "VG_STD":  
    CheckCommandForManualMode_VG(dpName, action, defaultPriv, exceptionInfo);
    break;
  default: //all VRPI + VPI
    return;
  }
}
// make compound confirm message for ALL std objects. S. Blanchard request 29.08.2013
void CheckCommandForManualMode_VG(string dpName, const dyn_anytype action, int &defaultPriv, dyn_string &exceptionInfo)
{
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
    case LVA_DEVICE_ON:
      break;
    case LVA_DEVICE_OFF:
      if(VacIsDeviceUsedInProcess(dpName, "VPG_6E01", makeDynString()))
      {
        if(defaultPriv < 3)
        {
          defaultPriv = 3;
        }
      }
      break;
      break;
    default:
      return;
  }
  dynAppend(glActSpecialMessages, "ATTENTION: MANUAL MODE COMMAND ");
}
//LhcVacGetRequiredActPriv_VGI:
/** Check if valve is in state that allows executing actions for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param defaultPriv: in/out default privilige for action/necessary privilege for action.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacGetRequiredActPriv_VGI(string dpName, const dyn_anytype action, int &defaultPriv, dyn_string &exceptionInfo)
{
  dyn_errClass 	err;
  int		mode;
  string        modeDpName;
  string	mainPart, sector1, sector2, checkDevSector1, checkDevSector2;
  bool		isBorder;
  int		devVacuum;

  if(glAccelerator != "LHC")
  {
    return;
  }
  unsigned act = action[1];
  switch(act & LVA_ACTION_MASK)
  {
  case LVA_DEVICE_ON:
    // VGI cannot be switched on at atmospheric pressure, check it
    LhcVacDeviceVacLocation(dpName, devVacuum, checkDevSector1, checkDevSector2, mainPart, isBorder, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("LhcVacGetRequiredActPriv_VGI.ctl: Call LhcVacDeviceVacLocation(" + dpName + ") FAILED");
      dynClear(exceptionInfo);
      checkDevSector1 = checkDevSector2 = "";
    }

    // Check all DPs of type VVS_LHC
    dyn_string allDpNames = dpNames("*", "VVS_LHC");
    for(int n = dynlen(allDpNames) ; n > 0 ; n--)
    {
      valveDpName = dpSubStr(allDpNames[n], DPSUB_DP);
      // check type of vacuum the selected valves are belong to
      LhcVacDeviceVacLocation(valveDpName, devVacuum, sector1, sector2, mainPart, isBorder, exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        DebugTN("LhcVacGetRequiredActPriv_VGI.ctl: Call LhcVacDeviceVacLocation(" + valveDpName + ") FAILED");
        dynClear(exceptionInfo);
        sector1 = sector2 = "";
      }
      if((checkDevSector1 != sector1) && (checkDevSector1 != sector2))
      {
        continue;
      }
      dpGet(valveDpName + ".RR1", stateRR1);
      err = getLastError();
      if(dynlen(err) > 0)
      {
        fwException_raise(exceptionInfo, 
          "ERROR", "LhcVacGetRequiredActPriv_VGI: Read RR1 for <" + valveDpName + "> failed: " + err, "");
        return;
      }
      // check state for this valve, if OPEN ENABLE is not set
      // the admin privillege is required
      if(((stateRR1 & VG_SPS_R_VALID) != 0) && ((stateRR1 & SVCU_SPS_R_OPEN_ENABLE) == 0))
      {
        string valveName;
        LhcVacDisplayName(valveDpName, valveName, exceptionInfo);
        if(valveName == "")
        {
          valveName = valveDpName;
        }
        dynAppend(glActSpecialMessages, valveName + ": OPEN DISABLE");
        if(defaultPriv < 4)
        {
          defaultPriv = 4;
        }
      }
    }
    break;
  default:
    // exist privillege is enough
    break;
  }
}

private void LhcVacVgDisableTpg300HotStart(string dpName, string progressLabel, dyn_string &exceptionInfo)
{
  if(!InitTpg300ConfigData(dpName, exceptionInfo))
  {
    return;
  }

  // Switch OFF all gauges of this TPG300
  dyn_string slaveDpNames;
  LhcVacEqpSlaves(configData[TPG_DP_NAME], slaveDpNames);
  for(int n = 1 ; n <= dynlen(slaveDpNames) ; n++)
  {
    if(!SwitchGaugeOFF(slaveDpNames[n], progressLabel, exceptionInfo))
    {
      return;
    }
  }

  // Save TPG300 config with all gauges OFF
  if(!SaveTpg300Config(progressLabel, exceptionInfo))
  {
    //return;
  }

  // Success - fix action result
  FixTpg300ActionExecution(LVA_TPG300_HOT_START_DISABLE);
}

private bool SwitchGaugeOFF(string dpName, string &progressLabel, dyn_string &exceptionInfo)
{
  string eqpName;
  LhcVacDisplayName(dpName, eqpName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return false;
  }
  if(progressLabel != "")
  {
    setValue(progressLabel, "text", "Switching OFF " + eqpName);
  }
  // Send command once independently on VG's recent state
  string tpgEqpName;
  LhcVacDisplayName(configData[TPG_DP_NAME], tpgEqpName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return false;
  }
  LhcVacDeviceAction(makeDynAnytype(LVA_DEVICE_OFF), makeDynString(), makeDynString(),
                     dpName, 0, exceptionInfo, "Disable Hot Start for TPG300 " + tpgEqpName);
  if(dynlen(exceptionInfo) > 0)
  {
    return false;
  }

  // Wait until VG is really OFF
  gaugeRR1 = 0;
  dpConnect("GaugeRr1Cb", true, dpName + ".RR1");
  bool success = false;
  for(int tries = 1; (tries <= TRIES_NUMBER) && (!success) ; tries++)
  {
    time startTime = getCurrentTime();
    while(true)
    {
      if((gaugeRR1 & VG_SPS_R_VALID) != 0)
      {
        if((gaugeRR1 & (VG_SPS_R_OFF | VG_SPS_R_ON)) == VG_SPS_R_OFF)
        {
          success = true;
          break;
        }
      }
      delay(0, 200);
      time now = getCurrentTime();
      if((period(now) - period(startTime)) > (tries * TIMEOUT_FACTOR))
      {
        break;
      }
    }
  }
  dpDisconnect("GaugeRr1Cb", dpName + ".RR1");
  if((!success) && (dynlen(exceptionInfo) == 0))
  {
    if((gaugeRR1 & VG_SPS_R_VALID) == 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "SwitchGaugeOFF(): " + eqpName + " state is not valid", "");
    }
    else
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "SwitchGaugeOFF(): not able to switch OFF " + eqpName, "");
    }
  }
  return success;
}

private void GaugeRr1Cb(string dpe, unsigned rr1)
{
  gaugeRR1 = rr1;
}

private void LhcVacVgEnableTpg300HotStart(string dpName, string progressLabel, dyn_string &exceptionInfo)
{
  if(!InitTpg300ConfigData(dpName, exceptionInfo))
  {
    return;
  }

  // Save TPG300 config with recent ON/OFF state of all gauges
  if(!SaveTpg300Config(progressLabel, exceptionInfo))
  {
    //return;
  }

  // Success - fix action result
  FixTpg300ActionExecution(LVA_TPG300_HOT_START_ENABLE);
}

private bool InitTpg300ConfigData(string dpName, dyn_string &exceptionInfo)
{
  dynClear(configData);
  dynAppend(configData, dpName);
  // Find master (TPG300) name and channel
  string masterDp = "";
  int masterChannel = 0;
  LhcVacEqpMaster(dpName, masterDp, masterChannel);
  if(masterDp == "")
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "FindTpg300Config(): no master for <" + dpName + ">", "");
    return false;
  }
  dynAppend(configData, masterDp);
  dynAppend(configData, masterChannel);

  // Find TPG config DP name
  string configDp = "";
  int configChannel;
  LhcVacEqpConfig(dpName, configDp, configChannel);
  if(configDp == "")
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "FindTpg300Config(): no config DP for <" + dpName + ">", "");
    return false;
  }
  dynAppend(configData, configDp);
  dynAppend(configData, configChannel);

  // Empty holders for DPE values
  dynAppend(configData, (unsigned)0);  // READ_RR1
  dynAppend(configData, (unsigned)TPG_CONFIG_BUSY);  // READ_RR2, to wait for 1st callback
  dynAppend(configData, (unsigned)0);  // READ_RR3
  //DebugTN("Config for " + dpName + " is ", configData);
  return true;
}

private bool SaveTpg300Config(string progressLabel, dyn_string &exceptionInfo)
{
  bool success = false;

  // Values to be written
  unsigned wr1 = (configData[TPG_CONFIG_CHANNEL] << 16) | (8 << 8) | 0x2u;
  unsigned wr2 = 0x00040000u;
  unsigned wr3 = (0x2u << 8);

  // Connect to RR1+RR2+RR3
  dpConnect("TpgConfigDataCb", true,
            configData[TPG_CONFIG_DP_NAME] + ".RR1",
            configData[TPG_CONFIG_DP_NAME] + ".RR2",
            configData[TPG_CONFIG_DP_NAME] + ".RR3");

  // Execute...
  for(int tries = 1; (tries <= TRIES_NUMBER) && (!success) ; tries++)
  {
    // Step 1 - wait for free communcation channel, make sure RR1 is not equal
    // to WR1 we are going to write
    time startTime = getCurrentTime();
    while(true)
    {
      if(progressLabel != "")
      {
        setValue(progressLabel, "text", "Waiting for comm. channel...");
      }
      unsigned writeWR1;
      dpGet(configData[TPG_CONFIG_DP_NAME] + ".WR1", writeWR1);
      if(((configData[READ_RR2] & TPG_CONFIG_BUSY) == 0)
        && ((configData[READ_RR1] == writeWR1) || (configData[READ_RR1] == 0)))
      {
        /*
        {
          string buf;
          sprintf(buf, "%d: RR2 %X busy %X RR1 %x writeRR1 %X", tries,
                 configData[READ_RR2], configData[READ_RR2] & TPG_CONFIG_BUSY,
                 configData[READ_RR1], writeWR1);
          DebugTN("Ready to write:", buf);
        }
        */
        if(wr1 == configData[READ_RR1])
        {
          // Write some dummy (incorrect) command - just to change RR1
          unsigned dummyWr1 = wr1 & 0xFFFF;  // Channel = 0
          dpSet(configData[TPG_CONFIG_DP_NAME] + ".WR1", dummyWr1,
              configData[TPG_CONFIG_DP_NAME] + ".WR2", wr2,
              configData[TPG_CONFIG_DP_NAME] + ".WR3", wr3);
          delay(0, 200);
        }
        else
        {
          // Write real command that is required
          dpSet(configData[TPG_CONFIG_DP_NAME] + ".WR1", wr1,
              configData[TPG_CONFIG_DP_NAME] + ".WR2", wr2,
              configData[TPG_CONFIG_DP_NAME] + ".WR3", wr3);
          success = true;
        }
        break;
      }
      delay(0, 200);
      time now = getCurrentTime();
      if((period(now) - period(startTime)) > (tries * TIMEOUT_FACTOR))
      {
        break;
      }
    }
    
    if(!success)
    {
      continue;  // Waiting for free communication channel
    }

    // Step 2: written successfully, wait for confirmation
    success = false;
    while(true)
    {
      if(progressLabel != "")
      {
        setValue(progressLabel, "text", "Waiting for comm. responce...");
      }
      /*
      {
        string buf;
        sprintf(buf, "%d: RR2 %X busy %X RR1 %x wr1 %X", tries,
               configData[READ_RR2], configData[READ_RR2] & TPG_CONFIG_BUSY,
               configData[READ_RR1], wr1);
        DebugTN("Waiting for responce:", buf);
      }
      */
      if(((configData[READ_RR2] & TPG_CONFIG_BUSY) == 0)
        && (configData[READ_RR1] == wr1)
        && ((configData[READ_RR2] & 0xFF0000) == 0))
      {
        /*
        {
          string buf;
          sprintf(buf, "%d: RR2 %X busy %X RR1 %x wr1 %X", tries,
                 configData[READ_RR2], configData[READ_RR2] & TPG_CONFIG_BUSY,
                 configData[READ_RR1], wr1);
          DebugTN("Got responce:", buf);
        }
        */
        success = true;
        break;
      }
      time now = getCurrentTime();
      if((period(now) - period(startTime)) > (tries * TIMEOUT_FACTOR))
      {
         break;
      }
      delay(0, 200);
    }
  }

  // Disconnect from RR1+RR2+RR3
  dpDisconnect("TpgConfigDataCb",
            configData[TPG_CONFIG_DP_NAME] + ".RR1",
            configData[TPG_CONFIG_DP_NAME] + ".RR2",
            configData[TPG_CONFIG_DP_NAME] + ".RR3");

  if(!success)
  {
    if((configData[READ_RR2] & 0xFF0000) == 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "SaveTpg300Config(): communcation timeout", "");
    }
    else
    {
      int errCode = ((configData[READ_RR2] & 0xFF0000) >> 16) & 0xFF;
      string errMsg, dpType;
      dyn_string localExceptionInfo;
      LhcVacErrMsgForCode(configData[TPG_CONFIG_DP_NAME], dpType, errCode, errMsg, localExceptionInfo);
      fwException_raise(exceptionInfo, 
        "ERROR", "SaveTpg300Config(): " + errMsg, "");
    }
  }
  return success;
}

// Calcback for TPG config DPEs
private void TpgConfigDataCb(string dp1, unsigned rr1, string dp2, unsigned rr2,
                  string dp3, unsigned rr3)
{
  configData[READ_RR1] = rr1;
  configData[READ_RR2] = rr2;
  configData[READ_RR3] = rr3;
}

private void FixTpg300ActionExecution(unsigned act)
{
  dyn_string vgDps;
  LhcVacEqpSlaves(configData[TPG_DP_NAME], vgDps);
  if(dynlen(vgDps) == 0)
  {
    DebugTN("FixTpg300ActionExecution(): No slaves for " + configData[TPG_DP_NAME]);
    return;
  }
  dyn_uint actList;
  dyn_string dpeList;
  for(int n = dynlen(vgDps) ; n > 0 ; n--)
  {
    dynAppend(dpeList, vgDps[n] + ".TpgHotStart.LastAction");
    dynAppend(actList, act);
  }
  dpSet(dpeList, actList);
}
