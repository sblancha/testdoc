#ifndef	EQPBEAM_INTENS_H
#define	EQPBEAM_INTENS_H

// BEAM_Intens - beam intensity measurement in LHC

#include "Eqp.h"


class VACCTLEQPDATA_EXPORT EqpBEAM_Intens : public Eqp
{
	Q_OBJECT

public:
	EqpBEAM_Intens(const Eqp &source);
#ifdef Q_OS_WIN
	EqpBEAM_Intens(const EqpBEAM_Intens &src) : Eqp(src) {}
#endif
	~EqpBEAM_Intens();

	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::Intensity; }
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode /* mode */) { return true; }
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);

	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	// Access for essential values
	inline float getIntensity(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? intensityReplay : intensityOnline; }
	
protected:
	// Last known value of Intensity DPE for online mode
	float	intensityOnline;

	// Last known value of Intensity DPE for replay mode
	float	intensityReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainValueString(float intensity, QString &string);
	virtual void getMainStateStringForMenu(float intensity, QString &string);
#endif
};

#endif	// EQPBEAM_INTENS_H
