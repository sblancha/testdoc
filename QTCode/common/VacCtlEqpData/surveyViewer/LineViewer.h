#ifndef LINEVIEWER_H
#define LINEVIEWER_H

// Class providing drawing of beam lines

#include <qwidget.h>

class BeamLine;
class Eqp;

class LineViewer : public QWidget
{
	Q_OBJECT

public:
	LineViewer(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	virtual ~LineViewer();

public slots:
	void selectionChanged(void);

signals:
	void eqpSelected(Eqp *pEqp);

protected:
	float	minX;
	float	minY;
	float	maxX;
	float	maxY;
	float	coef;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void mouseMoveEvent(QMouseEvent *pEvent);

	void findLimits(QList<BeamLine *> &lines);
	void calculateCoeff(int w, int h);
	void drawLines(QList<BeamLine *> &lines, QPainter &painter);
	void drawLine(BeamLine *pLine, QPainter &painter);
	void drawEqp(QPainter &painter, Eqp *pEqp, int &x, int &y);

	Eqp *findClosestDevice(int x, int y);
	bool checkDeviceDistance(Eqp *pEqp, int x, int y, int &distance);

};

#endif	// LINEVIEWER_H
