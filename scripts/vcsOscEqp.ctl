#uses "VacCtlEqpDataPvss"	// Load DLL
#uses "vclSectors.ctl"	// Load CTRL library
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "email.ctl"	// Load PVSS standard CTRL library
#uses "vclDevCommon.ctl"	// Load CTRL library
#uses "vclResources.ctl"   // Load CTRL library
#uses "vclMachine.ctl"		// Load CTRL library
//#uses "lhcVacArchiveConfig.ctl"
//#uses "lhcAlarmMaskConfig.ctl"
#uses "vclOscEqp.ctl"

#uses "vclArchiveConfig.ctl"  // L.Kopylov 07.11.2012
#uses "vclArchiveHistory.ctl"  // L.Kopylov 07.11.2012

#uses "vclArchiveSession.ctl"

	bool debugFlag = false;
  bool debugSkipFlag = false;
  
	string		configDp, timedDpName;
// List devices to block the archiving
	dyn_string	arList;
// List devices to notify about oscillation
	dyn_string	nList;
// List devices to block the alarm
//Threshold value to notify/block archiving/issue an alarm
	int nThresh, arThresh, alThresh;
// The interval (in sec) to count archived events
	int gInterval;
// The interval to restart the check procedure
	int gRestart;
	time checkStartTime;
//	bool debug = true;
// SMTP server
string smtpServer;
bool    messageHasSent=false;
bool    loopIsActive = false;

// Enable archiving for all - to be executed once per day
bool   enableCheckDone = false;

// E-mail sender
string sender;

// E-mail sender client
string	client;

// Recipients for alarm mask in all sectors
dyn_string	recipients;
main()
{
  dyn_string	exceptionInfo;
  int					coco;

  if(!InitStaticData())
  {
    return;
  }

  LhcVacOscEqpDp(configDp, exceptionInfo);
  if(dynlen(exceptionInfo))
  {
    DebugTN("vcsOscEqp.ctl ERROR: Processing in LhcVacOscEqpDp");
  }	
  coco = ConnectToConfigDp();
  if(coco == false)
  {
    DebugTN("vcsOscEqp.ctl: ConnectToConfigDp() FATAL_ERROR ");
  }
  coco = ConnectToEmailConfig();
  if (coco == false)
  {
    DebugTN("vcsOscEqp.ctl: ConnectToEmailConfig() FATAL_ERROR ");
  }
  LhcVacOscEqpTimedDp(timedDpName, exceptionInfo);
  if(dynlen(exceptionInfo))
  {
    DebugTN("vcsOscEqp.ctl ERROR: Processing in vcsOscEqpTimedDp");
  }
  dpSet(timedDpName + ".validFrom", makeTime(2006,6,1),
        timedDpName + ".validUntil", makeTime(2032,6,1),
        timedDpName + ".time", makeDynInt(), 				
        timedDpName + ".monthDay", makeDynInt(), 				
        timedDpName + ".month", makeDynInt(), 				
        timedDpName + ".weekDay", makeDynInt(), 			
        timedDpName + ".month", makeDynInt(), 				
        timedDpName + ".delay", 0,
        timedDpName + ".interval", gRestart *60,			//Interval  in sec
        timedDpName + ".syncDay", -1,
        timedDpName + ".syncWeekDay", -1,
        timedDpName + ".syncTime", -1, 
        timedDpName + ".syncMonth", -1);
        
  checkStartTime = getCurrentTime ();
// For debug use direct sbroutine call
	CheckForOscillation(timedDpName, getCurrentTime () - 10000, checkStartTime);
  timedFunc("CheckForOscillation", timedDpName);	
}
void	CheckForOscillation(string timedD, time before, time now)
{
  dyn_string	exceptionInfo, localExceptionInfo;
  dyn_string  dpNs, arMail;
  int	i, n, k, nTypes, nDp, coco, nTotal = 0, nMax = 0, nOsc = 0;
  dyn_time  	timeList;
  bool 				hasRR2;
  dyn_string	allDpeNames;
  int 				checkTime;
  string 			devName;
	
  checkStartTime = getCurrentTime ();
  if(loopIsActive)
  {
    DebugTN("vcsOscEqp::CheckForOscillation Is still active---start postponed");
    return;	
  }
  loopIsActive = true;
  // L.Kopylov 03.05.2012 - enable archiving back once per day
  time now = getCurrentTime();
  if(hour(now) == 2)
  {
    if(!enableCheckDone)
    {
      CheckArchiveEnable();
      loopIsActive = false;
      return;
    }
  }
  else
  {
    enableCheckDone = false;
  }
  
  dynClear(nList);
  dynClear(arList);
// Found all dpTypes
  nTypes = dynlen(glLhcVacDpTypes);
  // Archiving for some DP types shall not be touched
  dyn_string dpTypesToSkip = makeDynString(
      "VBEAM_Intens",
      "VBeamParamFloat",
      "VBeamParamFloatCMW",
      "VBeamParamInt",
      "VBeamParamDynBool",
      "VBeamParamDynBoolCMW",
      "VBeamParamString",
      "VBeamParamStringCMW",
      "VBeamParamDynFloat",
      "VBeamParamDynFloatCMW");

  DebugTN("vcsOscEqp::CheckForOscillation STARTED", nTypes, timedD, before, now);
  for(k = 1; k <= nTypes; k++)
  {
//DebugTN("vcsOscEqp:: start DP type " + glLhcVacDpTypes[k]);
    dynClear(allDpeNames);
    dynClear(dpNs);
    //For debug purpose it is possible to limit the search for desirable device types
    //		if((glLhcVacDpTypes[k] != "VGP_VELO") &&
    //		 	(glLhcVacDpTypes[k] != "VGP_T"))
    //			continue;
    if(glLhcVacDpTypes[k] == "_VacMobileList")
    {
      continue;
    }
    BuildDpeListForDpType(glLhcVacDpTypes[k], allDpeNames, exceptionInfo);
    dpNs = dpNames("*", glLhcVacDpTypes[k]);
    if(dynlen(dpNs) == 0)
    {
      continue;
    }
    if(dynlen(allDpeNames) == 0)
    {
      continue;
    }
    if(debugFlag)
    {
      DebugTN("vcsOscEqp::deviceS", glLhcVacDpTypes[k], dynlen(dpNs), dynlen(allDpeNames));
    }
    for(i = 1; i<= dynlen(dpNs); i++)
    {
      // Do not adjust beam parameters archiving
      string dpType = dpTypeName(dpNs[i]);
      if(dynContains(dpTypesToSkip, dpType) > 0)
      {
        continue;
      }

      dyn_string	localExceptionInfo;
      //To decrease the overall load of processor
      //	delay(0,300);
      for(int n = dynlen(allDpeNames) ; n > 0 ; n--)
      {
        // L.Kopylov 29.01.2013 Skip DPEs for XPOC
        if(allDpeNames[n] == "PR_XPOC")
        {
          continue;
        }
        if(UsedInArchiveSession(dpNs[i], allDpeNames[n]))
        {
          //DebugTN("vcsOscEqp.ctl: skip " + dpNs[i] + "." + allDpeNames[n] + " because of archive session");
          continue;
        }

        dynClear(timeList);

        // Get archiving  parameters for DPE
        string archiveClass;
        int archiveType, smoothProcedure, isActive;
        float deadband;
        float timeInterval;
        bool		isArchived;
        bool configExists;
        // GetArchiveParams(dpNs[i] + "." + allDpeNames[n], isArchived, archiveClass, smoothKind, deadBand, timeInterval);
        fwArchive_get(dpNs[i] + "." + allDpeNames[n], configExists, archiveClass, archiveType, smoothProcedure,
          deadband, timeInterval, isActive, exceptionInfo); 
        if(dynlen(exceptionInfo) > 0)
        {
          isArchived = false;
        }
        else if(archiveClass == "")
        {
          isArchived = false;
        }
        else
        {
          isArchived = configExists && isActive;
          if(configExists && (!isActive))
          {
            dynAppend(arList,  dpSubStr (dpNs[i], DPSUB_DP) + "." + allDpeNames[n]);
            if(debugFlag)
            {
              DebugTN("DpBlocked:", dpNs[i] +  "." + allDpeNames[n]);
            }
          }
        }
        if(!isArchived)
        {
          continue;
        }
        if(archiveType == DPATTR_ARCH_PROC_VALARCH)
        {
          if(debugSkipFlag)
          {
            DebugTN("vcsOscEqp.ctl: Skip " + dpNs[i] + "." + allDpeNames[n] + " - no smoothing used");
          }
          continue;
        }
        bool skipDpe = false;
        switch(smoothProcedure)
        {
        case DPATTR_VALUE_SMOOTH:
        case DPATTR_COMPARE_OLD_NEW:  // The same as value - for bool DPE
        case DPATTR_TIME_SMOOTH:
        case DPATTR_TIME_AND_VALUE_SMOOTH:
        case DPATTR_OLD_NEW_AND_TIME_SMOOTH:  // The same as time_and_value - for bool DPE
        case DPATTR_VALUE_REL_SMOOTH:
        case DPATTR_TIME_AND_VALUE_REL_SMOOTH:
          break;
        default:
          skipDpe = true;
          break;
        }
        if(skipDpe)
        {
          if(debugSkipFlag)
          {
            DebugTN("vcsOscEqp: skip " + dpNs[i] + "." + allDpeNames[n] + ", smoothProcedure " + smoothProcedure);
          }
          continue;
        }
        /*
        if((smoothProcedure != DPATTR_VALUE_SMOOTH) && (smoothProcedure != DPATTR_COMPARE_OLD_NEW))
        {
          continue;
        }
        */
        // Archiving without time smoothing is set for DPE - subject to check
        if(IsDynDpeType(dpNs[i] +  "." + allDpeNames[n]))
        {
          dyn_dyn_anytype dynValueList;
          coco = dpGetPeriod(now - gInterval, now, 1, dpNs[i] + "." + allDpeNames[n], dynValueList, timeList);
        }
        else
        {
          dyn_anytype valueList;
          coco = dpGetPeriod(now - gInterval, now, 1, dpNs[i] + "." + allDpeNames[n], valueList, timeList);
        }
        if(debugFlag)
        {
          DebugTN("vcsOscEqp.ctl: dpGetPeriod(" + dpNs[i] +  "." + allDpeNames[n] + ") returned " + dynlen(timeList) +
                  ", smoothProcedure " + smoothProcedure);
        }
        nOsc = dynlen(timeList);
        if(nOsc > nThresh)
        {
          //LhcVacDisplayName(dpSubStr (dpNs[i], DPSUB_DP), devName, exceptionInfo);
          dynAppend(nList, dpSubStr (dpNs[i], DPSUB_DP) + "." + allDpeNames[n] + "       " + nOsc);
        }
        if(nOsc > arThresh)
        {
          dynAppend(arList, dpSubStr (dpNs[i], DPSUB_DP) + "." + allDpeNames[n]);
          LhcVacDisplayName(dpSubStr (dpNs[i], DPSUB_DP), devName, exceptionInfo);
          dynAppend(arMail, devName);
          // L.Kopylov 02.11.2012 New algorithm: not necessary stop archiving, for some
          // smooth type just increasing deadband/time could help
          bool keepArchiving = true;
          switch(smoothProcedure)
          {
          case DPATTR_VALUE_SMOOTH:
            if(deadband == 0)
            {
              keepArchiving = false;
              DebugTN("vcsOscEqp.ctl: stop archiving for " +
                      dpNs[i] +  "." + allDpeNames[n]);
              fwArchive_stop(dpNs[i] + "." + allDpeNames[n], exceptionInfo);
              if(dynlen(exceptionInfo) > 0)
              {
                DebugTN("vcsOscEqp.ctl: fwArchive_stop(" + dpNs[i] + "." + allDpeNames[n] +
                    ") failed:", exceptionInfo);
              }
              break;
            }
            // NOBREAK
          case DPATTR_TIME_AND_VALUE_SMOOTH:
          case DPATTR_VALUE_REL_SMOOTH:
          case DPATTR_TIME_AND_VALUE_REL_SMOOTH:
            deadband *= 2;
            DebugTN("vcsOscEqp.ctl: increase deadband to " + deadband + " for " +
                      dpNs[i] +  "." + allDpeNames[n]);
            fwArchive_set(dpNs[i] + "." + allDpeNames[n], archiveClass, archiveType,
                        smoothProcedure, deadband, timeInterval, exceptionInfo, false);
            if(dynlen(exceptionInfo) > 0)
            {
              DebugTN("vcsOscEqp.ctl: fwArchive_set(" + dpNs[i] + "." + allDpeNames[n] +
                    ") to increase deadband to " + deadband + " failed:", exceptionInfo);
            }
            break;
          case DPATTR_TIME_SMOOTH:
          case DPATTR_OLD_NEW_AND_TIME_SMOOTH:  // The same as time_and_value - for bool DPE
            timeInterval *= 2;
            if(timeInterval <= 0)
            {
              timeInterval = 1;
            }
            DebugTN("vcsOscEqp.ctl: increase timeInterval to " + timeInterval + " for " +
                      dpNs[i] +  "." + allDpeNames[n]);
            fwArchive_set(dpNs[i] + "." + allDpeNames[n], archiveClass, archiveType,
                        smoothProcedure, deadband, timeInterval, exceptionInfo, false);
            if(dynlen(exceptionInfo) > 0)
            {
              DebugTN("vcsOscEqp.ctl: fwArchive_set(" + dpNs[i] + "." + allDpeNames[n] +
                    ") to increase timeInterval to " + timeInterval + " failed:", exceptionInfo);
            }
            break;
          case DPATTR_COMPARE_OLD_NEW:
            keepArchiving = false;
            DebugTN("vcsOscEqp.ctl: stop archiving for " +
                      dpNs[i] +  "." + allDpeNames[n]);
            fwArchive_stop(dpNs[i] + "." + allDpeNames[n], exceptionInfo);
            if(dynlen(exceptionInfo) > 0)
            {
              DebugTN("vcsOscEqp.ctl: fwArchive_stop(" + dpNs[i] + "." + allDpeNames[n] +
                    ") failed:", exceptionInfo);
            }
            break;
          default:
            skipDpe = true;
            break;
          }
          if(!skipDpe)
          {
            WriteArchiveConfigChange(dpNs[i] +  "." + allDpeNames[n], archiveClass,
                                     keepArchiving, archiveType, smoothProcedure, deadband,
                                     timeInterval);
          }
          if(debugFlag)
          {
            DebugN("OscFound:", devName, nOsc);
          }
        }
        nTotal += nOsc;
        if (nOsc > nMax)
        {
          nMax = nOsc;
        }
        if(debugFlag)
        {
          if(nOsc > 1)
          {
            if(debugFlag)
            {
              DebugTN("dpGetPeriod:", dpNs[i] +  "." + allDpeNames[n], dynlen(timeList));
            }
          }
        }
      }
    }
  }

  DebugTN("CheckForOscillation COMPLETED", checkTime, nTotal, nMax);
  loopIsActive = false;
  //Device lists are prepared. Now order them by position

  LhcVacOrderDevListByPosition(arList, false);
  LhcVacOrderDevListByPosition(nList, true);

  //
  //	Convert notify list to "readable" format
  dyn_string	split, nEvent;
  string  		devName;
  int nameMaxLen = 0, nMax = dynlen(nList);
  for(i = 1; i <= nMax; i++)
  {
    split = strsplit(nList[i], " ");
    LhcVacDisplayName(dpSubStr (split[1], DPSUB_DP), devName, exceptionInfo);
    string dpElement = strltrim(dpSubStr (split[1],
        DPSUB_DP_EL), dpSubStr (split[1], DPSUB_DP));
    if(strlen(devName) > nameMaxLen)
    {
      nameMaxLen = strlen(devName);
    }
    dynAppend(nEvent, split[dynlen(split)]);
    nList[i] = devName + dpElement;
  }

  //aligne numbers in message
  for(i = 1; i <= nMax; i++)
  {
    int nSpace = 7 + nameMaxLen - strlen(nList[i]);
    for(int k = 0; k < nSpace; k++)
    {
      nList[i] += " ";
    }
    nList[i] += nEvent[i];
  }
  checkTime = getCurrentTime() - checkStartTime;

  dpSet(configDp + ".oscList", nList);
  dpSet(configDp + ".archStopList", arList);
  dpSet(configDp + ".lastCheck", getCurrentTime());
  if(debugFlag)
  {
    DebugTN("Save new Oscillation data COMPLETED", checkTime, nTotal, nMax);
  }

  time currentTime = getCurrentTime();
  if(!messageHasSent  &&  hour(currentTime)  == 9)
  {
    messageHasSent = true;           
    if(dynlen(nList) > 0)
    {	
      bool flag;
      dpGet(configDp + ".sendNotif", flag);
      if(flag)
      {
        SendMessages("NEW Device Oscillations found", nList);
      }
    }
    if(dynlen(arMail) > 0)
    {
      SendMessages("Archiving blocked for oscillating devices", arMail);
    }
  }

  if(hour (currentTime)  > 9)
  {
    messageHasSent = false;
  }
}

//Order device list according its position
//LhcVacOrderDevListByPosition(dyn_string devList)

LhcVacOrderDevListByPosition(dyn_string &devList, bool isComplex)
{
  string 			name, typeName;
  dyn_string 	newList, split, slaves, exceptionInfo;
  dyn_float 	posList, newPos;
  float				pos, mValue;	
  int					i, iFound, nMax = dynlen(devList);

  for(i = 1; i <= nMax; i++)
  {
    if(isComplex)
    {
      split = strsplit(devList[i], " ");
      name = dpSubStr (split[1], DPSUB_DP);
    }
    else
    {
      name =  dpSubStr (devList[i], DPSUB_DP);
    }
    typeName =  dpTypeName (name);
    if((typeName == "VRPI_L") ||
      (typeName == "VRPI_VELO") ||
      (typeName == "VRPI_W"))
    {
      dynClear(exceptionInfo);
      LhcVacEqpSlaves(name, slaves);
      if(dynlen(slaves) == 0)
      {
        DebugTN("vcsOscEqp.ctl: No slaves for " + name);
        dynAppend(posList, 0.0);
        continue;
      }
      else
      {
        name = slaves[1];
      }
    }
    dynClear(exceptionInfo);
    LhcVacDeviceLocation(name, pos, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("vcsOscEqp.ctl: LhcVacDeviceLocation():", exceptionInfo);
      pos = 0;
    }
    dynAppend(posList, pos);
  }

  for(i = 1; i < nMax; i++)
  {
    mValue = dynMin(posList);
    iFound = dynContains(posList, mValue);
    dynAppend(newList, devList[iFound]);
    if(debugFlag)
    {
      DebugN("FindArchivePos: ", devList[iFound], posList[iFound]);
    }
    dynRemove(devList, iFound);
    dynRemove(posList, iFound);
  }
  dynClear(devList);
  devList = newList;
}

/**InitStaticData
Purpose:
Read all machine configuration data from file(s)

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool InitStaticData()
{
  dyn_string	exceptionInfo, mainParts;
  long		coco;
  bool		isLhcData;

  // Set accelerator name to be used by other components
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsOscEqp.ctl: Failed to read machine name: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsOscEqp.ctl: Machine is " + glAccelerator);
  VacResourcesParseFile(PROJ_PATH + "/data/VacResources.txt", glAccelerator, exceptionInfo);
  isLhcData = glAccelerator == "LHC" ? true : false;

  // Initalize passive machine data
  coco = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsOscEqp.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsOscEqp.ctl: LhcVacEqpInitPassive() done");

  // Initialzie active equipment information
  coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
    exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsOscEqp.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsOscEqp.ctl: LhcVacEqpInitActive() done");

  DebugTN("vcsOscEqp.ctl: InitStaticData() done");
  LhcVacInitDevFunc();
  return true;
}

void SendMessages(string title, dyn_string dpeList)
{
  int					n, coco;
  if(dynlen(dpeList) == 0)
  {
    DebugTN("vcsOscEqp.ctl: List of dpe is empty, no message to be send");
    return;
  }
  // Prepare and send message
  dyn_string	email = makeDynString("");
  if(dynlen(recipients) == 0)
  {
    return;
  }
  for(n = dynlen(recipients) ; n > 0 ; n--)
  {
    if(email[1] != "")
    {
      email[1] += ";";
    }
    email[1] += recipients[n];
  }
  email[2] = sender;
  email[3] = getHostname() + ":  " + title;
  email[4] = "Device name:";
  for(n = dynlen(dpeList) ; n > 0 ; n--)
  {
    email[4] += "\n";
    email[4] += dpeList[n];
  }
  if(debugFlag)
  {
    DebugTN("vcsOscEqp.ctl: message:" + email);
  }
  emSendMail(smtpServer, client, email, coco);
  if(coco < 0)	// Failed, make pause then try again
  {
    DebugTN("vcsOscEqp.ctl: FAILURE TO SEND MESSAGE", email[1]);
  }
}

/** ConnectToConfig
Purpose:
Connect to DPEs containing eqp oscillating configuration

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToConfigDp()
{
  dyn_string		exceptionInfo;
  dyn_errClass	err;
  if(debugFlag)
  {
    DebugN("configDp", configDp);
  }
  if(dpConnect("IntervalCb", configDp + ".interval") < 0)
  {
    err = getLastError();
    DebugTN("vcsOscEqp.ctl: FATAL: dpConnect(" + configDp + ".interval) failed");
    throwError(err);
    return false;
  }
  if(dpConnect("RestartCb", configDp + ".restart") < 0)
  {
    err = getLastError();
    DebugTN("vcsOscEqp.ctl: FATAL: dpConnect(" + configDp + ".restart) failed");
    throwError(err);
    return false;
  }
  if(dpConnect("BThreshCb", configDp + ".arThresh") < 0)
  {
    err = getLastError();
    DebugTN("vcsOscEqp.ctl: FATAL: dpConnect(" + configDp + ".arThresh) failed");
    throwError(err);
    return false;
  }
  if(dpConnect("AThreshCb", configDp + ".alThresh") < 0)
  {
    err = getLastError();
    DebugTN("vcsOscEqp.ctl: FATAL: dpConnect(" + configDp + ".alThresh) failed");
    throwError(err);
    return false;
  }
  if(dpConnect("NThreshCb", configDp + ".nThresh") < 0)
  {
    err = getLastError();
    DebugTN("vcsOscEqp.ctl: FATAL: dpConnect(" + configDp + ".nThresh) failed");
    throwError(err);
    return false;
  }
  return true;
}

/** ConnectToConfig
Purpose:
Connect to DPEs containing E-mail notification configuration

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool ConnectToEmailConfig()
{
  dyn_string		exceptionInfo;
  dyn_errClass	err;

  if(dpConnect("ServerCb", configDp + ".server") < 0)
  {
    err = getLastError();
    DebugTN("vcsOscEqp.ctl: FATAL: dpConnect(" + configDp + ".server) failed");
    throwError(err);
    return false;
  }
  if(dpConnect("ClientCb", configDp + ".client") < 0)
  {
    err = getLastError();
    DebugTN("vcsOscEqp.ctl: FATAL: dpConnect(" + configDp + ".client) failed");
    throwError(err);
    return false;
  }
  if(dpConnect("SenderCb", configDp + ".sender") < 0)
  {
    err = getLastError();
    DebugTN("vcsOscEqp.ctl: FATAL: dpConnect(" + configDp + ".sender) failed");
    throwError(err);
    return false;
  }
  if(dpConnect("RecipientsCb", configDp + ".recipients") < 0)
  {
    err = getLastError();
    DebugTN("vcsOscEqp.ctl: FATAL: dpConnect(" + configDp + ".recipientsAll) failed");
    throwError(err);
    return false;
  }
  DebugTN("vcsOscEqp.ctl: ConnectToEmailConfig() done");
  return true;
}
// BuildDpeListForDpType
/**
Purpose:
Build full names of all DPEs of given DP. Every resulting DPE names contains the whole
path WITHOUT input DP name

Parameters:
	- dpName, string, input, Name of DP for which all DPE names are required
	- allDpeNames, dyn_string, output, List of all DPE names for given DP will be returned here
	- exceptionInfo, dyn_string, output, details of exception will be written here

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void BuildDpeListForDpType(string dpType, dyn_string &allDpeNames, dyn_string &exceptionInfo)
{
  dyn_dyn_string	names;
  dyn_dyn_int			types;
  dyn_string			dpeTree, localExceptionInfo;

  dynClear(allDpeNames);
  dynClear(exceptionInfo);

  // Get DP type structure for working DP
  if(debugFlag)
  {
    DebugN("dpType",dpType, names, types);
  }
  if(dpTypeGet(dpType, names, types) < 0)
  {
    dyn_errClass err = getLastError();
    DebugTN("vcsOscEqp.ctl: ERROR: dpTypeGet(" + dpType + ") failed: " + StdErrorText(err));
    DebugTN(err);
    fwException_raise(exceptionInfo, "ERROR", "BuildDpeListForDpType(): dpTypeGet(" + dpType + ") failed", "");
    return;
  }
  int dpeIdx, typeIdx, type, nDpes = dynlen(names);
  for(dpeIdx = 1 ; dpeIdx <= nDpes ; dpeIdx++)
  {
    typeIdx = dynlen(types[dpeIdx]);
    type = types[dpeIdx][typeIdx];
    if((type == DPEL_STRUCT) ||	// Structure
      (type == DPEL_BIT32_STRUCT) || (type == DPEL_BLOB_STRUCT) || (type == DPEL_BOOL_STRUCT) ||
      (type == DPEL_CHAR_STRUCT) || (type == DPEL_DPID_STRUCT) || (type == DPEL_DYN_BIT32_STRUCT) ||
      (type == DPEL_DYN_BLOB_STRUCT) || (type == DPEL_DYN_BOOL_STRUCT) || (type == DPEL_DYN_CHAR_STRUCT) ||
      (type == DPEL_DYN_DPID_STRUCT) || (type == DPEL_DYN_FLOAT_STRUCT) || (type == DPEL_DYN_INT_STRUCT) ||
      (type == DPEL_DYN_LANGSTRING_STRUCT) || (type == DPEL_DYN_STRING_STRUCT) || (type == DPEL_DYN_TIME_STRUCT) ||
      (type == DPEL_DYN_UINT_STRUCT) || (type == DPEL_FLOAT_STRUCT) || (type == DPEL_INT_STRUCT) ||
      (type == DPEL_LANGSTRING_STRUCT) || (type == DPEL_STRING_STRUCT) || (type == DPEL_TIME_STRUCT) ||
      (type == DPEL_UINT_STRUCT))
    {
      if(dynlen(dpeTree) >= typeIdx)
      {
        dpeTree[typeIdx] = names[dpeIdx][typeIdx];
      }
      else
      {
        dynAppend(dpeTree, names[dpeIdx][typeIdx]);
      }
      continue; 
    }
    if(dynlen(dpeTree) == 0)
    {
      continue;	// 'root' DPE
    }
    string dpeName = "";
    for(int n = 2 ; n < typeIdx ; n++)	// Start from 2 because 1 is DP type name
    {
      if(dpeName != "")
      {
        dpeName += ".";
      }
      dpeName += dpeTree[n];
    }
    if(dpeName != "")
    {
      dpeName += ".";
    }
    dpeName += names[dpeIdx][typeIdx];
    dynAppend(allDpeNames, dpeName);
  }
}

// GetDpArchiveUsage
/**
Purpose:
Add to list of used PVSS archive names all PVSS archives used by DPEs of given DP

Parameters:
	- dpName, string, input, Name of DP whose DPEs shall be analyzed
	- archives, dyn_string, input/output, Resulting list of all used PVSS archives, names of
		PVSS archives used by this DP will be added to list if they do not appear in the list yet.
	- exceptionInfo, dyn_string, output, details of exception will be written here

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void GetDpArchiveUsage(string dpName, dyn_string &archives, int &smoothType, int &smoothProcedure, dyn_string &exceptionInfo)
{
  dyn_string	allDpeNames;
  dyn_string	localExceptionInfo;

  BuildDpeListForDp(dpName, allDpeNames, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  for(int n = dynlen(allDpeNames) ; n > 0 ; n--)
  {
    dynClear(localExceptionInfo);

    // Get archiving parameters for DPE
    bool		configExists, isActive;
    int 		smoothType, smoothProcedure;
    string	archiveClass;
    float		deadBand, timeInterval;

    fwArchive_get(dpName + "." + allDpeNames[n], configExists, archiveClass, smoothType, smoothProcedure, deadBand,
        timeInterval, isActive, localExceptionInfo);
    if(dynlen(localExceptionInfo) > 0)
    {
      continue;
    }
    if(archiveClass == "")
    {
      continue;
    }
    if(! (configExists && isActive))
    {
      continue;
    }
    // Archiving is set for DPE, add archive class to resulting list if not there yet
    if(dynContains(archives, archiveClass) <= 0)
    {
      dynAppend(archives, archiveClass);
    }
  }
  // If there is a parent of device - check also archiving of parent
  string	masterDpName;
  int			masterChannel;
  LhcVacEqpMaster(dpName, masterDpName, masterChannel);
  if(masterDpName != "")
  {
    GetDpArchiveUsage(masterDpName, archives, exceptionInfo);
  }
}
// Callbacks from DP containing general E-mail configuration & parameters for srchiving check
void ServerCb(string dpe, string newServer)
{
//DebugN("ServerCb");
  smtpServer = newServer;
}
void ClientCb(string dpe, string newClient)
{
//DebugN("ClientCb");
  client = newClient;
}
void SenderCb(string dpe, string newSender)
{
//DebugN("SenderCb");
  sender = newSender;
}
void RecipientsCb(string dpe, dyn_string newRecipients)
{
//DebugN("RecipientsCb");
  recipients = newRecipients;
}
void IntervalCb(string dpe, int newInterval)
{
//DebugN("IntervalCb");
  gInterval = newInterval*60;
}
void RestartCb(string dpe, int newRestart)
{
//DebugN("RestartCb");
  gRestart = newRestart;
}
void BThreshCb(string dpe, int newBThesh)
{
//DebugN("BThreshCb");
  arThresh  = newBThesh;
}
void AThreshCb(string dpe, int newAThesh)
{
//DebugN("AThreshCb");
  alThresh  = newAThesh;
}
void NThreshCb(string dpe, int newNThesh)
{
//DebugN("NThreshCb");
  nThresh  = newNThesh;
}

// L.Kopylov 03.05.2012 - enable archiving back once per day
void CheckArchiveEnable()
{
  dyn_string blockedDpes;
  dpGet(configDp + ".archStopList", blockedDpes);
  DebugTN("vcsOscEqp.ctl: CheckArchiveEnable() - start, " + dynlen(blockedDpes) + " DPE(s) to check");
  for(int n = dynlen(blockedDpes) ; n > 0 ; n--)
  {
    if(ReleaseArchiveBlock(blockedDpes[n]))
    {
      dynRemove(blockedDpes, n);
    }
  }
  dpSet(configDp + ".archStopList", blockedDpes);
  enableCheckDone = true;
  DebugTN("vcsOscEqp.ctl: CheckArchiveEnable() - finish, " + dynlen(blockedDpes) + " DPE(s) left");
}

bool ReleaseArchiveBlock(string dpe)
{
  string archiveClass;
  int archiveType, smoothProcedure, isActive;
  float deadband, timeInterval;
  bool		isArchived, configExists;
  dyn_string exceptionInfo;

  if(debugFlag)
  {
    DebugTN("vcsOscEqp.ctl: ReleaseArchiveBlock(" + dpe + ") - start");
  }

  fwArchive_get(dpe, configExists, archiveClass, archiveType, smoothProcedure,
    deadband, timeInterval, isActive, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsOscEqp.ctl: fwArchive_get(" + dpe + ") ERROR: ", exceptionInfo);
    return false;
  }
  if(archiveClass == "")
  {
    DebugTN("vcsOscEqp.ctl: fwArchive_get(" + dpe + "): archiveClass is empty");
    return false;
  }

  if(configExists && (!isActive))
  {
    if(debugFlag)
    {
      DebugTN("vcsOscEqp.ctl: ReleaseArchiveBlock(" + dpe + ") - start archiving");
    }
    fwArchive_start(dpe, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("vcsOscEqp.ctl: fwArchive_start(" + dpe + ") FAILED:", exceptionInfo);
      return false;
    }
  }
  return true;
}

void WriteArchiveConfigChange(string dpe, string archiveClass,
  bool isArchived, int archiveType, int smoothProcedure, float deadband, float timeInterval)
{
  int smoothType;
  if(archiveType == DPATTR_ARCH_PROC_VALARCH)
  {
    smoothType = SMOOTH_TYPE_NONE;
  }
  else
  {
    switch(smoothProcedure)
    {
    case DPATTR_VALUE_SMOOTH:
    case DPATTR_COMPARE_OLD_NEW:  // The same as value - for bool DPE
      smoothType = SMOOTH_TYPE_VALUE;
      break;
    case DPATTR_TIME_AND_VALUE_SMOOTH:
    case DPATTR_OLD_NEW_AND_TIME_SMOOTH:  // The same as time_and_value - for bool DPE
      smoothType = SMOOTH_TYPE_TIME_AND_VALUE;
      break;
    case DPATTR_VALUE_REL_SMOOTH:
      smoothType = SMOOTH_TYPE_REL_VALUE;
      break;
    case DPATTR_TIME_AND_VALUE_REL_SMOOTH:
      smoothType = SMOOTH_TYPE_TIME_AND_REL_VALUE;
      break;
    case DPATTR_TIME_SMOOTH:
      smoothType = SMOOTH_TYPE_TIME;
      break;
    default:
      DebugTN("vcsOscEqp.ctl: WriteArchiveConfigChange(" + dpe + "): unexpected smoothProcedure " +
              smoothProcedure);
      return;
    }
  }
  dyn_string exceptionInfo;
  WriteArchiveHistoryRecord(dpe, isArchived, smoothType, timeInterval, deadband, SOURCE_SCRIPT, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsOscEqp.ctl: WriteArchiveConfigChange(" + dpe + "): WriteArchiveHistoryRecord() failed:",
      exceptionInfo);
  }
}

bool IsDynDpeType(string dpe)
{
  int type = dpElementType(dpe);
  switch(type)
  {
  case DPEL_DYN_BIT32:
  case DPEL_DYN_BLOB:
  case DPEL_DYN_BOOL:
  case DPEL_DYN_CHAR:
  case DPEL_DYN_DPID:
  case DPEL_DYN_FLOAT:
  case DPEL_DYN_INT:
  case DPEL_DYN_LANGSTRING:
  case DPEL_DYN_STRING:
  case DPEL_DYN_TIME:
  case DPEL_DYN_UINT:
    return true;
  }
  return false;
}

bool UsedInArchiveSession(string sDpName, string sDpeName)
{
  if(sDpeName != "PR")
  {
    return false;  // Only PR DPEs can be used
  }

  // Check if DP appears in one of active, pre-active or post-finished sesions
  dyn_string dsAllDpNames = dpNames("*", LHC_VAC_ARCHIVE_SESSION_DP);
  for(int iDpIdx = dynlen(dsAllDpNames) ; iDpIdx > 0 ; iDpIdx--)
  {
    if(!SessionNearActiveState(dsAllDpNames[iDpIdx]))
    {
      continue;
    }
    dyn_string dsArchDpNames;
    dpGet(dsAllDpNames[iDpIdx] + ".DpList", dsArchDpNames);
    return dynContains(dsArchDpNames, dpSubStr(sDpName, DPSUB_DP)) > 0;
  }
  return false;
}

bool SessionNearActiveState(string sSessionDpName)
{
  uint uiStatus = LHC_VAC_ARCHIVE_SESSION_NONE;
  time tStartTime, tEndTime;
  dpGet(sSessionDpName + ".Status", uiStatus,
        sSessionDpName + ".ScheduledStartTime", tStartTime,
        sSessionDpName + ".ScheduledEndTime", tEndTime);

  time now = getCurrentTime();
  switch(uiStatus)
  {
  case LHC_VAC_ARCHIVE_SESSION_READY:
    return (tStartTime + gInterval) > now;
  case LHC_VAC_ARCHIVE_SESSION_STARTING:
  case LHC_VAC_ARCHIVE_SESSION_START_FAILED:
  case LHC_VAC_ARCHIVE_SESSION_RUNNING:
  case LHC_VAC_ARCHIVE_SESSION_STOPPING:
  case LHC_VAC_ARCHIVE_SESSION_STOP_FAILED:
    return true;
  case LHC_VAC_ARCHIVE_SESSION_FINISHED:
    return (now - gInterval) < tEndTime;
  }
  return false;
}
