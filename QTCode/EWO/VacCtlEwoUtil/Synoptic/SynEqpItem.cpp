//	Implementation of SynEqpItem class
/////////////////////////////////////////////////////////////////////////////////

#include "SynEqpItem.h"

#include "VacIcon.h"
#include "EqpType.h"
#include "Eqp.h"
#include "Sector.h"

/////////////////////////////////////////////////////////////////////////////////
//////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////
SynEqpItem::SynEqpItem()
{
	pEqp = NULL;
	pIcon = NULL;
	pBeamLinePart = NULL;
	type = EqpType::None;
	eqpIdx = -1;
	groupId = 0;
	verticalPos = 0;
	textStart = textEnd = textHeight = 0;
	sectorBorder = startFixed = commonReady = partReverse = false;
	intlSourceForward = intlSourceBackward = false;
}

SynEqpItem::~SynEqpItem()
{
	if(pIcon)
	{
		delete pIcon;
		pIcon = NULL;
	}
}

/*
**	FUNCTION
**		Calculate name of this item
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Pointer to string with resulting name
**
**	CAUTIONS
**		None
*/
const char *SynEqpItem::getName(void) const
{
	static const char	*nullName = "<null>";
	const char	*result = nullName;

	switch(type)
	{
	case EqpType::SectorBefore:
		if(pEqp)
		{
			result = pEqp->getSectorBefore()->getName();
		}
		break;
	case EqpType::SectorAfter:
		if(pEqp)
		{
			result = pEqp->getSectorAfter()->getName();
		}
		break;
	case EqpType::Aux:
		result = nullName;
		break;
	default:
		if(pEqp)
		{
			result = pEqp->getName();
		}
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Find name of sector to be drawn BEFORE this element
**
**	PARAMETERS
**		reverse	- true if equipment ine line is built in reverse order
**
**	RETURNS
**		Pointer to string with resulting name
**
**	CAUTIONS
**		It is supposed that method will only be called for elements with
**		sectorBorder = true
*/
const char *SynEqpItem::getNameBefore(bool reverse) const
{
	/* L.Kopylov 03.03.2014 - see below
	if(reverse)
	{
		return pEqp->getSectorAfter()->getName();
	}
	return pEqp->getSectorBefore()->getName();
	*/
	if(reverse)
	{
		return pEqp->getLabelAfter();
	}
	return pEqp->getLabelBefore();
}

/*
**	FUNCTION
**		Find name of sector to be drawn AFTER this element
**
**	PARAMETERS
**		reverse	- true if equipment ine line is built in reverse order
**
**	RETURNS
**		Pointer to string with resulting name
**
**	CAUTIONS
**		It is supposed that method will only be called for elements with
**		sectorBorder = true
*/
const char *SynEqpItem::getNameAfter(bool reverse) const
{
	/* L.Kopylov 03.03.2014 - see below
	if(reverse)
	{
		return pEqp->getSectorBefore()->getName();
	}
	return pEqp->getSectorAfter()->getName();
	*/
	if(reverse)
	{
		return pEqp->getLabelBefore();
	}
	return pEqp->getLabelAfter();
}

/*
**	FUNCTION
**		Move active icon to calculated position
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynEqpItem::moveIcon(void)
{
	if(pIcon)
	{
		pIcon->move(rect.x(), rect.y());
		pIcon->resize(rect.width(), rect.height());
		pIcon->show();
	}
}

/*
**	FUNCTION
**		Set pipe color for icon
**
**	PARAMETERS
**		color	- New pipe color
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynEqpItem::setPipeColor(QColor &color)
{
	if(pIcon)
	{
		pIcon->setPipeColor(color);
	}
}

/*
**	FUNCTION
**		Set beam direction for icon
**
**	PARAMETERS
**		direction	- New beam direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynEqpItem::setBeamDirection(int direction)
{
	if(pIcon)
	{
		pIcon->setBeamDirection((VacIconContainer::BeamDirection)direction);
	}
}

void SynEqpItem::setRect(QRect &rect)
{
	this->rect = rect;
	if(pIcon)
	{
		pIcon->resize(rect.width(), rect.height());
	}
}

int SynEqpItem::getIconHeight(void)
{
	return pIcon ? pIcon->height() : 0;
}
