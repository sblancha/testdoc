#include "InterlockChain.h"

InterlockChain::InterlockChain(QString valveName)
{
	this->valveName = valveName;
	isChainLoaded = false;
}

InterlockChain::~InterlockChain()
{
	qDeleteAll(listOfEquipment);
	listOfEquipment.clear();
	listOfConnections.clear();
	visibilityMasks.clear();
}

/*===================================================================================
* Function addConnection
*
* Adds connections to the list, but only if the equipment to be connected already exists
* in the list of equipment of the interlock chain.
* Otherwise it returns -1 (missing parent equipment) or -2 (missing child equipment)
*/
int InterlockChain::addConnection(InterlockEqp * pParentEqp, QString parentConnector, InterlockEqp * pChildEqp, QString childConnector, QString cable)
{
	if (!listOfEquipment.contains(pParentEqp))
	{
		return -1;
	}
	if (!listOfEquipment.contains(pChildEqp))
	{
		return -2;
	}
	
	InterlockConnection interlockConnection;
	interlockConnection.parentEquipment = pParentEqp;
	interlockConnection.parentConnector = parentConnector;
	interlockConnection.childEquipment = pChildEqp;
	interlockConnection.childConnector = childConnector;
	interlockConnection.cable = cable;

	listOfConnections.append(interlockConnection);

	return 1;
}

int InterlockChain::addConnection(InterlockConnection connection)
{
	if (!listOfEquipment.contains(connection.parentEquipment))
	{
		return -1;
	}
	if (!listOfEquipment.contains(connection.childEquipment))
	{
		return -2;
	}

	InterlockConnection interlockConnection;
	interlockConnection.parentEquipment = connection.parentEquipment;
	interlockConnection.parentConnector = connection.parentConnector;
	interlockConnection.childEquipment = connection.childEquipment;
	interlockConnection.childConnector = connection.childConnector;
	interlockConnection.cable = connection.cable;

	listOfConnections.append(interlockConnection);

	return 1;
}

InterlockEqp * InterlockChain::getEquipmentPtr(QString equipmentName)
{
	for (int i = 0; i < listOfEquipment.length(); i++)
	{
		if (QString::compare(listOfEquipment.at(i)->getName(), equipmentName) == 0)
		{
			return listOfEquipment.at(i);
		}
	}

	return NULL;
}

InterlockEqp * InterlockChain::getEquipmentPtrAt(int index)
{
	if (index < 0 || index >= listOfEquipment.length())
	{
		return NULL;
	}

	return listOfEquipment.at(index);
}

/* Function for sorting connections
*
* ASSUMPTIONS: all the connections in the list are for the same parent device
*			   connectors on the parent device have a sortable name (prefix + letter or number)
*
*/
bool InterlockChain::connectionSort(InterlockConnection  &conn1, InterlockConnection &conn2)
{
	if (conn1.childEquipment == NULL)
		return true;

	if (conn2.childEquipment == NULL)
		return false;


	QString parentConnector1 = conn1.parentConnector;
	QString  parentConnector2 = conn2.parentConnector;
	
	int compareConnectors = QString::compare(parentConnector1, parentConnector2);


	if (compareConnectors < 0)
	{
		return true;
	}
	else
		return false;
	
}

QList<InterlockConnection> InterlockChain::getSortedChildConnections(QString parentName)
{
	QList<InterlockConnection> childList;
	for (int i = 0; i < listOfConnections.length(); i++)
	{
		if (QString::compare(parentName, listOfConnections.at(i).parentEquipment->getName()) == 0)
		{
			childList.append(listOfConnections.at(i));
		}

	}

	qSort(childList.begin(), childList.end(), connectionSort);
	return childList;
}

void InterlockChain::setVisibilityMasks()
{
	visibilityMasks.clear();
	for (int i = 0; i < listOfEquipment.length(); i++)
	{
		InterlockEqp * pEqp = listOfEquipment.at(i);
		QList<InterlockConnection> connectionsFound;
		for (int j = 0; j < listOfConnections.size(); j++)
		{
			InterlockConnection connection = listOfConnections.at(j);
			if (connection.childEquipment == pEqp)
			{
				connectionsFound.append(connection);
			}
		}

		if (connectionsFound.length() == 0)
		{
			visibilityMasks.append(true);
			continue;
		}
		else
		{
			bool eqpVisibility = false;
			for (int j = 0; j < connectionsFound.length(); j++)
			{
				InterlockConnection connection = connectionsFound.at(j);
				if (connection.parentEquipment->getName().startsWith("VRIVC") && pEqp->getName().startsWith("VRIVC"))
				{
					continue;
				}
				int parentIndex = listOfEquipment.indexOf(connection.parentEquipment);

				if (parentIndex >= 0 && parentIndex < visibilityMasks.length())
				{
					eqpVisibility = eqpVisibility || visibilityMasks.at(parentIndex);
				}
			}
			visibilityMasks.append(eqpVisibility);
		}
	}


	
}

bool InterlockChain::isEqpAtIndexVisible(int index)
{
	if (index < 0 || index > visibilityMasks.length())
	{
		return false;
	}

	return visibilityMasks.at(index);
}