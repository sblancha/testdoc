#ifndef	BAKEOUTWORKDP_H
#define	BAKEOUTWORKDP_H

#include "VacCtlEqpDataExport.h"

// Set of online data for one bakeout rack

#define	MAX_BAKEOUT_CHANNEL		(24)
#define MAX_BAKEOUT_CTL_CHANNEL	(8)

#include "InterfaceEqp.h"

#include <QString>

class Eqp;

class VACCTLEQPDATA_EXPORT BakeoutWorkDp : public InterfaceEqp
{
	Q_OBJECT

public:
	enum ChannelValueType
	{
		ChannelValueTypeNone = 0,
		ChannelValueTypeT = 1,
		ChannelValueTypeSetT = 2,
		ChannelValueTypeStatus = 3
	};

public:
	BakeoutWorkDp(const char *realDpName);
	~BakeoutWorkDp();

	void setEqp(Eqp *pEqp, Eqp *pSourceEqp);

	void connectDpeSignal(InterfaceEqp *pReceiver, int channel, enum ChannelValueType valueType);
	void disconnectDpeSignal(InterfaceEqp *pReceiver, int channel, enum ChannelValueType valueType);
	void connectChannel(InterfaceEqp *pReceiver, int channel);
	void connectCtlChannel(InterfaceEqp *pReceiver, int channel);
	void connectChannelState(InterfaceEqp *pReceiver, int channel);
	void connectCtlChannelState(InterfaceEqp *pDst, int channel);
	void disconnectChannel(InterfaceEqp *pReceiver, int channel);
	void disconnectCtlChannel(InterfaceEqp *pReceiver, int channel);
	void disconnectChannelState(InterfaceEqp *pReceiver, int channel);
	void disconnectCtlChannelState(InterfaceEqp *pReceiver, int channel);
	void connectRr1(InterfaceEqp *pReceiver);
	void disconnectRr1(InterfaceEqp *pReceiver);
	const QVariant &getDpeValue(int channel, enum ChannelValueType valueType);
	const QVariant &getDpeValue(const char *dpeName, QDateTime &timeStamp);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	// Access
	inline const QString &getDpName(void) const { return dpName; }
	inline Eqp *getEqp(void) const { return pEqp; }
	inline Eqp *getSourceEqp(void) const { return pSourceEqp; }
	inline unsigned getRr1(void) const { return rr1; }
	inline bool isPlcAlarm(void) const { return plcAlarm; }
	int getChannelState(int channel);
	int getCtlChannelState(int channel);

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value,
		DataEnum::DataMode mode, const QDateTime &timeStamp);


protected:
	// DP name (BAKEOUT_1, BAKEOUT_2 etc.)
	QString		dpName;

	// Pointer to 'real' device, with name, sector etc. NULL if this DP is not active
	Eqp			*pEqp;

	// Pointer to source DP (DP type = VMOB), used to access PLC name
	Eqp			*pSourceEqp;

	// List of connected DPEs. It is expected that number of DPEs is small enough,
	// so we don't need to use hash tables for search (QASciiDict or similar storage)
	QList<DpeConnection *>	*pDpeConnections;

	// List of destination objects who connected to signals of this object.
	// Destination may issue several calls connectDpe(), but this does not mean
	// they wants to be informed several times about every DPE change
	QList<InterfaceEqp *>	*pSignalDestinations;



	// Online value for RR1 DPE
	unsigned	rr1;

	// Online values for channel states
	int			channelStates[MAX_BAKEOUT_CHANNEL];
	
	// Online values for control channel states
	int			ctlChannelStates[MAX_BAKEOUT_CTL_CHANNEL];
	
	// Online values for channel settings
//	float		channelSets[MAX_BAKEOUT_CHANNEL];

	// Online values for channel measurements
//	float		channelTs[MAX_BAKEOUT_CHANNEL];

	// Online flag for plc alarm
	bool		plcAlarm;

	void connectDpe(const char *dpeName);
	void disconnectDpe(const char *dpeName);
};

#endif	// BAKEOUTWORKDP_H
