//	Implementation of DpeHistoryDepthQuery class
/////////////////////////////////////////////////////////////////////////////////

#include "DpeHistoryDepthQuery.h"

DpeHistoryDepthQuery	DpeHistoryDepthQuery::instance;

DpeHistoryDepthQuery &DpeHistoryDepthQuery::getInstance(void)
{
	return instance;
}

int DpeHistoryDepthQuery::getPendingCount(void)
{
	return instance.pItems->count();
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction
DpeHistoryDepthQuery::DpeHistoryDepthQuery()
{
	signalSent = false;
	pItems = new QList<DpeHistoryDepthQueryItem *>();
}

DpeHistoryDepthQuery::DpeHistoryDepthQuery(const DpeHistoryDepthQuery &source) : QObject()
{
	signalSent = false;
	pItems = new QList<DpeHistoryDepthQueryItem *>(*(source.pItems));
}


DpeHistoryDepthQuery::~DpeHistoryDepthQuery()
{
	while(!pItems->isEmpty())
	{
		delete pItems->takeFirst();
	}
	delete pItems;
}

DpeHistoryDepthQuery & DpeHistoryDepthQuery::operator = (const DpeHistoryDepthQuery &other)
{
	signalSent = false;
	*pItems = *(other.pItems);
	return *this;
}


/*
**	FUNCTION
**		Add DPE query to list of pending queries.
**
**	ARGUMENTS
**		dpe			- Full DPE name (DP + DPE)
**		start		- Start time of 'default' visible range
**		mobileType	- Mobile type of device
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeHistoryDepthQuery::add(const char *dpe, const QDateTime &start, int mobileType)
{
	mutex.lock();
	pItems->append(new DpeHistoryDepthQueryItem(dpe, start, mobileType));
	mutex.unlock();
	if(!signalSent)
	{
		signalSent = true;
		emit changed();
	}
}

/*
**	FUNCTION
**		Clear pool of pending requests, normally method is called when
**		mobile history dialog is closed
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeHistoryDepthQuery::clear(void)
{
	mutex.lock();
	pItems->clear();
	signalSent = false;
	mutex.unlock();
}

/*
**	FUNCTION
**		Get parameters of next query pending in the queue, remove that
**		query from the queue
**
**	ARGUMENTS
**		dpe			- Variable where DPE name of pending request will be written
**		start		- Variable where start time of default visible range will be written
**		mobileType	- Variable where mobile type of pending request will be written
**
**	RETURNS
**		true	- if next pending request has been found;
**		false	- if queue is empty
**
**	CAUTIONS
**		None
*/
bool DpeHistoryDepthQuery::getNext(QByteArray &dpe, QDateTime &start, int &mobileType)
{
	mutex.lock();
	bool result;
	if(pItems->isEmpty())
	{
		result = false;
		signalSent = false;
	}
	else
	{
		result = true;
		DpeHistoryDepthQueryItem *pItem = pItems->takeFirst();
		dpe = pItem->getDpe();
		start = pItem->getStart();
		mobileType = pItem->getMobileType();
		delete pItem;
	}
	mutex.unlock();
	return result;
}

