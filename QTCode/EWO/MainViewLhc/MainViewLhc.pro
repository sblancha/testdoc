#
# Project file for qmake utility to build MainLHC EWO
#
#	16.10.2008	L.Kopylov
#		Initial version
#
#	14.12.2008	L.Kopylov
#		Rename target library and change destination directory
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = MainViewLhc

QT += printsupport

INCLUDEPATH += ../VacCtlEwoUtil \
	../../common/VacCtlUtil \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/Platform \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp

HEADERS =	SineCosine.h \
		LhcSegment.h \
		LhcConnectLine.h \
		MainLHCImage.h \
#		MainLHCImageEqpConnect.h \
		MainLHC.h \
		MainLhcEwo.h

SOURCES = SineCosine.cpp \
		LhcSegment.cpp \
		LhcConnectLine.cpp \
		MainLHCImage.cpp \
#		MainLHCImageEqpConnect.cpp \
		MainLHC.cpp \
		MainLhcEwo.cpp

LIBS += -L../../../bin  \
	-lVacCtlEwoUtil \
	-lVacCtlHistoryData \
	-lVacCtlEqpData \
	-lVacCtlUtil

