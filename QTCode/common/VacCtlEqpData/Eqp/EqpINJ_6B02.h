#ifndef	EQPINJ_6B02_H
#define	EQPINJ_6B02_H

//	EqpINJ_6B02	- Gas injection device in LINAC4

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpINJ_6B02 : public Eqp
{
	Q_OBJECT

public:
	EqpINJ_6B02(const Eqp &source);
#ifdef Q_OS_WIN
	EqpINJ_6B02(const EqpINJ_6B02 &src) : Eqp(src) {}
#endif
	~EqpINJ_6B02();

	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::Pressure; }

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
	virtual void getPartColor(int part, QColor &color, DataEnum::DataMode mode);
	virtual void getPartStateString(int part, QString &string, DataEnum::DataMode mode);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline unsigned getRR2(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr2Replay : rr2Online; }
	inline float getSetPointR(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? setPointReplay : setPointOnline; }
	virtual int getErrorCode(DataEnum::DataMode mode);
	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR2 DPE for online mode
	unsigned	rr2Online;

	// Last known value of SetPointR DPE for online mode
	float		setPointOnline;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for replay mode
	unsigned	rr2Replay;

	// Last known value of SetPointR DPE for replay mode
	float		setPointReplay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, unsigned rr2, float setPoint, bool plcAlarm, QString &string);
	virtual void getModeName(int mode, QString &result);
	virtual void getPartColorPump(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartColorVVG(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color);
	virtual void getPartStateStringPump(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string);
	virtual void getPartStateStringVVG(unsigned rr1, unsigned rr2, float setPoint, bool plcAlarm, QString &string);
#endif
};

#endif	// EQPINJ_6B02_H
