
/***********************************************************************************
****************** Constants for VPVELO **************************
***********************************************************************************/
const unsigned
	VPVELO_R_VALID = 0x40000000u,
	VPVELO_R_ERRORS = 0x00800000u,
	VPVELO_R_WARNINGS = 0x00400000u,
	VPVELO_R_ON = 0x00020000u,
	VPVELO_R_OFF = 0x00010000u;


void LhcVacDeviceAction_VPT_VELO(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  mapping         mConnectDpes;
  unsigned        act;
  
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["SPEED"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = false;
    mConnectDpes["AL2"] = false;
    mConnectDpes["AL3"] = false;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["SPEED"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = false;
    mConnectDpes["AL2"] = false;
    mConnectDpes["AL3"] = false;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
   default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }
}
