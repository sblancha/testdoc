#include "SurveyViewer.h"

#include <qapplication.h>

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	SurveyViewer viewer(NULL);
//	app.setMainWidget(&viewer);
	viewer.show();
	return app.exec();
}
