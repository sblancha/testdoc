#ifndef _RESOURCEPOOL_H
#define _RESOURCEPOOL_H

#include "VacCtlEqpDataExport.h"

// Class holding application resource values read from file.
//	Only ONE instance of such class shall exists per client

#include "ResourceEqpMask.h"

#include "FileParser.h"
// L.Kopylov 10.02.2010 #include "FunctionalType.h"
#include "VacEqpTypeMask.h"

#include <qstring.h>
#include <qhash.h>
#include <qlist.h>

// Auxilliary class holding resource value read from input file
class VACCTLEQPDATA_EXPORT ResourceValue
{
public:
	ResourceValue(const char *value, int type)
	{
		this->value = QString::fromLatin1(value);
		this->type = type;
	}
	~ResourceValue() {}

	// Access
	QString &getValue(void) { return value; }
	int getType(void) { return type; }

protected:
	// Resource value
	QString	value;

	// Type of section where resource was read
	int		type;
};

class VACCTLEQPDATA_EXPORT ResourcePool : public FileParser
{
public:
	// Result of resource search
	enum
	{
		OK = 0,
		NotFound = 1,
		UnsupportedType = 2
	};

	// Data types for resources


	static ResourcePool &getInstance(void);

	int parseFile(const char *machineName, const char *fileName, QStringList &errList);

	inline bool isReportResourceNotFound(void) { return reportResourceNotFound; }

	int getIntValue(const QString &name, int &result);
	int getUintValue(const QString &name, unsigned &result);
	int getFloatValue(const QString &name, float &result);
	int getStringValue(const QString &name, QString &result);
	int getBoolValue(const QString &name, bool &result);
	int getEqpTypesMask(const QString &name, VacEqpTypeMask &result);

	ResourcePool();
	~ResourcePool();

protected:
	// The only instance of resource pool
	static ResourcePool	instance;

	// Flag indicating if error shall be reported if resource is not found
	bool	reportResourceNotFound;

	// Type of section being read - used during file parsing
	int		sectionType;

	// Name of machine for which reasources are read - used during file parsing
	QString	machineName;

	// List of resource values (dictionary for hash search)
	QHash<QString, ResourceValue *>	*pResources;

	// List of all equipment type masks read from resource file
	QList<ResourceEqpMask *>	*pEqpMasks;

	// Implementation of FileParser method
	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);

	int parseSectionStart(int lineNo, char *token, QStringList &errList);
};

#endif	// _RESOURCEPOOL_H
