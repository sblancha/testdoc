#ifndef REPLAYPOOL_H
#define	REPLAYPOOL_H

#include "VacCtlHistoryDataExport.h"

//	Data pool for replay

#include "DpeValuesSet.h"

#include <qobject.h>
#include <qmutex.h>
#include <qhash.h>

#include <QVariant>

#ifdef Q_OS_WIN
#include <QMutex>
#endif

class VACCTLHISTORYDATA_EXPORT ReplayPool : public QObject
{
	Q_OBJECT

public:
	enum PlayMode
	{
		None = 0,
		Forward = 1,
		Backward = 2
	};

	ReplayPool();
	ReplayPool(const ReplayPool &source);
	~ReplayPool();
	ReplayPool & operator = (const ReplayPool &other);

	static ReplayPool &getInstance(void);
	static bool addDpe(const char *dpName, const char *dpeName);
	static bool removeDpe(const char *dpName, const char *dpeName);
	static void notifyDataArrived(const char *dpName, const char *dpeName);

	bool addDpe(const char *dpeName);
	bool removeDpe(const char *dpeName);
	inline int getDpeCount(void) { return pDpes->count(); }
	void notifyDataArrived(const char *dpeName);

	int reset(void);
	int nextDpe(QByteArray &dpeName);
	int nextDpes(QList<QByteArray> &dpeNames);

	int setValue(const char *dpeName, const QDateTime &ts, const QVariant &value,
		bool useFirstValue);
	void process(void);
	int getNextEventData(QByteArray &dpeName, QDateTime &ts, QVariant &value);
	int getPreviousEventData(QByteArray &dpeName, QDateTime &ts, QVariant &value);
	int setPlayTime(const QDateTime &ts, bool forward);
	int getPlayData(QByteArray &dpeName, QVariant &value);

	inline int getDpeCount(void) const { return pDpes->count(); }

signals:
	void dpeListChanged(void);

protected:
	// The only instance of replay data pool
	// static ReplayPool	instance;

	static QByteArray makeDpeName(const char *dpName, const char *dpeName);


	// Mutex to synchronize access to pool
	QMutex				mutex;

	// all DPE data
	QHash<QByteArray, DpeValuesSet *>	*pDpes;

	// Current play mode
	PlayMode			mode;

	// Current play time
	QDateTime			playTime;

	// Pointer to last played DPE
	const DpeValuesSet	*pLastPlayedDpe;

	// Pointer to last played value
	const DpeValue		*pLastPlayedValue;

	void setLastPlayed(const DpeValuesSet *pDpeData, const DpeValue *pValue)
	{
		pLastPlayedDpe = pDpeData;
		pLastPlayedValue = pValue;
	}
	void checkLastPlayed(int totalRefCount, QByteArray &dpeName, QDateTime &ts, QVariant &value);
};

#endif	// REPLAYPOOL_H
