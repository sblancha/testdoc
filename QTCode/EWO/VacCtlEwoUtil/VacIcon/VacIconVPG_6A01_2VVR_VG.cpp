//	Implementation of VacIconVPG_6A01_2VVR_VG class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPG_6A01_2VVR_VG.h"
#include "VacIconVPG_6A01.h"

#include "EqpProcess.h"

#include "DataPool.h"

#include <qpainter.h>
#include <QMouseEvent>


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
VacIconVPG_6A01_2VVR_VG::VacIconVPG_6A01_2VVR_VG(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	pIconVPG = pIconVVR1 = pIconVVR2 = pIconVGR = pIconVGP = pIconVGF = NULL;
	setFixedSize(54, 70);
	setAreaGeometry();
}

VacIconVPG_6A01_2VVR_VG::~VacIconVPG_6A01_2VVR_VG()
{
	disconnect();
}

/*
**	FUNCTION
**		Calculate geometry for symbol areas rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_VG::setAreaGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		vvr1.setRect(0, 2, 27, 21);
		vvr2.setRect(27, 2, 27, 21);
		vgr.setRect(0, 28, 21, 21);
		vgp.setRect(26, 28, 21, 21);
		pump.setRect(13, 49, 21, 21);
		break;
	case VacIconContainer::Down:
		vvr1.setRect(0, 47, 27, 21);
		vvr2.setRect(27, 47, 27, 21);
		vgr.setRect(0, 21, 21, 21);
		vgp.setRect(26, 21, 21, 21);
		pump.setRect(13, 0, 21, 21);
		break;
	case VacIconContainer::Left:
		vvr1.setRect(47, 27, 21, 27);
		vvr2.setRect(47, 0, 21, 27);
		vgr.setRect(21, 33, 21, 21);
		vgp.setRect(21, 7, 21, 21);
		pump.setRect(0, 20, 21, 21);
		break;
	case VacIconContainer::Right:
		vvr1.setRect(2, 27, 21, 27);
		vvr2.setRect(2, 0, 21, 27);
		vgr.setRect(28, 33, 21, 21);
		vgp.setRect(28, 7, 21, 21);
		pump.setRect(49, 20, 21, 21);
		break;
	}
	if(pIconVVR1)
	{
		pIconVVR1->move(vvr1.left(), vvr1.top());
	}
	if(pIconVVR2)
	{
		pIconVVR2->move(vvr2.left(), vvr2.top());
	}
	if(pIconVGR)
	{
		pIconVGR->move(vgr.left(), vgr.top());
	}
	if(pIconVGP)
	{
		pIconVGP->move(vgp.left(), vgp.top());
	}
	if(pIconVPG)
	{
		pIconVPG->move(pump.left(), pump.top());
	}
	if(pIconVGF)
	{
		switch(direction)
		{
		case VacIconContainer::Up:
		case VacIconContainer::Down:
			pIconVGF->move(vgr.left() + 3, vgr.top());
			break;
		case VacIconContainer::Left:
		case VacIconContainer::Right:
			pIconVGF->move(vgr.left(), vgr.top() + 3);
			break;
		}
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPG_6A01_2VVR_VG::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		return VacIconGeometry(54, 70, 27, 71, -1);
		break;
	case VacIconContainer::Down:
		return VacIconGeometry(54, 70, 26, 71, -1);
		break;
	default:
		break;
	}
	return VacIconGeometry(70, 54, 27, 71, -1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_VG::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(54);
		size.setHeight(70);
		break;
	default:
		size.setWidth(70);
		size.setHeight(54);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreaGeometry();
	if(pIconVPG)
	{
		pIconVPG->setDirection(newDirection);
	}
	if(pIconVVR1)
	{
		pIconVVR1->setDirection(calculateVvrDirection());
	}
	if(pIconVVR2)
	{
		pIconVVR2->setDirection(calculateVvrDirection());
	}
	if(pIconVGR)
	{
		pIconVGR->setDirection(calculateVgrDirection());
	}
	if(pIconVGP)
	{
		pIconVGP->setDirection(calculateVgpDirection());
	}
	if(pIconVGF)
	{
		pIconVGF->setDirection(calculateVgrDirection());
	}
}

VacIconContainer::Direction VacIconVPG_6A01_2VVR_VG::calculateVvrDirection(void)
{
	VacIconContainer::Direction result = direction;
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		result = VacIconContainer::Left;
		break;
	default:
		result = VacIconContainer::Up;
		break;
	}
	return result;
}

VacIconContainer::Direction VacIconVPG_6A01_2VVR_VG::calculateVgrDirection(void)
{
	VacIconContainer::Direction result = direction;
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		result = VacIconContainer::Right;
		break;
	default:
		result = VacIconContainer::Up;
		break;
	}
	return result;
}

VacIconContainer::Direction VacIconVPG_6A01_2VVR_VG::calculateVgpDirection(void)
{
	VacIconContainer::Direction result = direction;
	switch(direction)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		result = VacIconContainer::Left;
		break;
	default:
		result = VacIconContainer::Up;
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_VG::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pIconVPG)
	{
		delete pIconVPG;
		pIconVPG = NULL;
	}
	if(pIconVVR1)
	{
		delete pIconVVR1;
		pIconVVR1 = NULL;
	}
	if(pIconVVR2)
	{
		delete pIconVVR2;
		pIconVVR2 = NULL;
	}
	if(pIconVGR)
	{
		delete pIconVGR;
		pIconVGR = NULL;
	}
	if(pIconVGP)
	{
		delete pIconVGP;
		pIconVGP = NULL;
	}
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpProcess"), "VacIconVPG_6A01_2VVR_VG::setEqp", pEqp->getDpName());
		this->pEqp = (EqpProcess *)pEqp;
		pIconVPG = new VacIconVPG_6A01(this);
		pIconVPG->setEqp(pEqp);
		pIconVPG->move(pump.left(), pump.top());
		pIconVPG->setMode(mode);
		pIconVPG->setPipeColor(pipeColor);
		pIconVPG->setDirection(direction);
		if(connected)
		{
			pIconVPG->connect();
		}

		QString attrName = pEqp->getAttrValue("VVR1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVVR1 = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVVR1)
		{
			pIconVVR1->move(vvr1.left(), vvr1.top());
			pIconVVR1->setMode(mode);
			pIconVVR1->setPipeColor(pipeColor);
			pIconVVR1->setDirection(calculateVvrDirection());
			if(connected)
			{
				pIconVVR1->connect();
			}
		}

		attrName = pEqp->getAttrValue("VVR2");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVVR2 = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVVR2)
		{
			pIconVVR2->move(vvr2.left(), vvr2.top());
			pIconVVR2->setMode(mode);
			pIconVVR2->setPipeColor(pipeColor);
			pIconVVR2->setDirection(calculateVvrDirection());
			if(connected)
			{
				pIconVVR2->connect();
			}
		}

		attrName = pEqp->getAttrValue("VGR1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGR = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGR)
		{
			pIconVGR->move(vgr.left(), vgr.top());
			pIconVGR->setMode(mode);
			pIconVGR->setPipeColor(pipeColor);
			pIconVGR->setDirection(calculateVgrDirection());
			if(connected)
			{
				pIconVGR->connect();
			}
		}

		attrName = pEqp->getAttrValue("VGP1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGP = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGP)
		{
			pIconVGP->move(vgp.left(), vgp.top());
			pIconVGP->setMode(mode);
			pIconVGP->setPipeColor(pipeColor);
			pIconVGP->setDirection(calculateVgpDirection());
			if(connected)
			{
				pIconVGP->connect();
			}
		}

		attrName = pEqp->getAttrValue("VGF");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGF = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGF)
		{
			pIconVGF->setDrawConnectionPipe(false);
			switch(direction)
			{
			case VacIconContainer::Up:
			case VacIconContainer::Down:
				pIconVGF->move(vgr.left() + 3, vgr.top());
				break;
			case VacIconContainer::Left:
			case VacIconContainer::Right:
				pIconVGF->move(vgr.left(), vgr.top() + 3);
				break;
			}
			pIconVGF->setMode(mode);
			pIconVGF->setPipeColor(pipeColor);
			pIconVGF->setDirection(calculateVgrDirection());
			if(connected)
			{
				pIconVGF->connect();
			}
		}
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_VG::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	if(pIconVPG)
	{
		pIconVPG->setMode(newMode);
	}
	if(pIconVVR1)
	{
		pIconVVR1->setMode(newMode);
	}
	if(pIconVVR2)
	{
		pIconVVR2->setMode(newMode);
	}
	if(pIconVGR)
	{
		pIconVGR->setMode(newMode);
	}
	if(pIconVGP)
	{
		pIconVGP->setMode(newMode);
	}
	if(pIconVGF)
	{
		pIconVGF->setMode(newMode);
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set color used for pipe lines drawing
**
**	ARGUMENTS
**		newColor	- Color used for pipe drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_VG::setPipeColor(const QColor &newColor)
{
	if(pIconVPG)
	{
		pIconVPG->setPipeColor(newColor);
	}
	if(pIconVVR1)
	{
		pIconVVR1->setPipeColor(newColor);
	}
	if(pIconVVR2)
	{
		pIconVVR2->setPipeColor(newColor);
	}
	if(pIconVGR)
	{
		pIconVGR->setPipeColor(newColor);
	}
	if(pIconVGP)
	{
		pIconVGP->setPipeColor(newColor);
	}
	if(pIconVGF)
	{
		pIconVGF->setPipeColor(newColor);
	}
	VacIcon::setPipeColor(newColor);
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6A01_2VVR_VG::connect(void)
{
	if(pIconVPG)
	{
		pIconVPG->connect();
	}
	if(pIconVVR1)
	{
		pIconVVR1->connect();
	}
	if(pIconVVR2)
	{
		pIconVVR2->connect();
	}
	if(pIconVGR)
	{
		pIconVGR->connect();
	}
	if(pIconVGP)
	{
		pIconVGP->connect();
	}
	if(pIconVGF)
	{
		pIconVGF->connect();
	}
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_6A01_2VVR_VG::disconnect(void)
{
	if(pIconVPG)
	{
		pIconVPG->disconnect();
	}
	if(pIconVVR1)
	{
		pIconVVR1->disconnect();
	}
	if(pIconVVR2)
	{
		pIconVVR2->disconnect();
	}
	if(pIconVGR)
	{
		pIconVGR->disconnect();
	}
	if(pIconVGP)
	{
		pIconVGP->disconnect();
	}
	if(pIconVGF)
	{
		pIconVGF->disconnect();
	}
	return false;
}

/*
**	FUNCTION
**		Override VacIcon's method in order to have different menus for different
**		parts of icon
**
**	ARGUMENTS
**		pEvent	- Pointer to mouse event
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_6A01_2VVR_VG::mousePressEvent(QMouseEvent * /* pEvent */)
{
	// Do nothing
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPG_6A01_2VVR_VG::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
//	painter.eraseRect(0, 0, width(), height());

	switch(direction)
	{
	case VacIconContainer::Up:
		// Line from VVR1 to outside
		painter.fillRect(9, 0, 3, vvr1.height() + 5, pipeColor);
		// Line from VVR2 to outside
		painter.fillRect(36, 0, 3, vvr2.height() + 5, pipeColor);
		// Line connecting 2 valves
		painter.fillRect(9, vvr1.bottom() + 2, vvr2.center().x() - vvr1.center().x() + 2, 3, pipeColor);
		// Connect pump to center of valve connection line
		painter.fillRect(pump.center().x() - 1, vvr1.bottom() + 4, 3, pump.top() - vvr1.bottom() - 3, pipeColor);
		// Line connecting 2 gauges
		painter.fillRect(vgr.right() - 1, vgr.center().y() - 1, 9, 3, pipeColor);
		break;
	case VacIconContainer::Down:
		// Line from VVR1 to outside
		painter.fillRect(9, vvr1.top() - 2, 3, vvr1.height() + 5, pipeColor);
		// Line from VVR2 to outside
		painter.fillRect(36, vvr2.top() - 2, 3, vvr2.height() + 5, pipeColor);
		// Line connecting 2 valves
		painter.fillRect(9, vvr1.top() - 4, vvr2.center().x() - vvr1.center().x() + 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, pump.bottom(), 3, vvr1.top() - pump.bottom() - 3, pipeColor);
		// Line connecting 2 gauges
		painter.fillRect(vgr.right() - 1, vgr.center().y() - 1, 9, 3, pipeColor);
		break;
	case VacIconContainer::Left:
		// Line from VVR1 to outside
		painter.fillRect(vvr1.left() - 4, 42, vvr1.width() + 5, 3, pipeColor);
		// Line from VVR2 to outside
		painter.fillRect(vvr2.left() - 4, 15, vvr2.width() + 5, 3, pipeColor);
		// Line connecting 2 valves
		painter.fillRect(vvr1.left() - 4, 15, 3, vvr1.center().y() - vvr2.center().y() + 2, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.right(), pump.center().y() - 1, vvr1.left() - pump.right() - 3, 3, pipeColor);
		// Line connecting 2 gauges
		painter.fillRect(vgp.center().x() - 1, vgp.bottom() - 1, 3, 9, pipeColor);
		break;
	case VacIconContainer::Right:
		// Line from VVR1 to outside
		painter.fillRect(0, 42, vvr1.width() + 5, 3, pipeColor);
		// Line from VVR2 to outside
		painter.fillRect(0, 15, vvr2.width() + 5, 3, pipeColor);
		// Line connecting 2 valves
		painter.fillRect(vvr1.right() + 2, 15, 3, vvr1.center().y() - vvr2.center().y() + 2, pipeColor);
		// Connect pump to valves
		painter.fillRect(vvr1.right() + 3, pump.center().y() - 1, pump.left() - vvr1.right() - 2, 3, pipeColor);
		// Line connecting 2 gauges
		painter.fillRect(vgp.center().x() - 1, vgp.bottom() - 1, 3, 9, pipeColor);
		break;
	}

	if(!pEqp)
	{
		painter.setPen(Qt::black);
		painter.drawRect(pump.left(), pump.top(), pump.width() - 1, pump.height() - 1);
		painter.drawRect(vgr.left(), vgr.top(), vgr.width() - 1, vgr.height() - 1);
		painter.drawRect(vgp.left(), vgp.top(), vgp.width() - 1, vgp.height() - 1);
		painter.drawRect(vvr1.left(), vvr1.top(), vvr1.width() - 1, vvr1.height() - 1);
		painter.drawRect(vvr2.left(), vvr2.top(), vvr2.width() - 1, vvr2.height() - 1);
	}
}
