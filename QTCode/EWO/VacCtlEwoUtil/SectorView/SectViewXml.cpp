//	Implementation of SectViewXml class
/////////////////////////////////////////////////////////////////////////////////

#include "SectViewXml.h"

SectViewXml *SectViewXml::create(QWidget *parent, const QString &fileName, DataEnum::DataMode mode)
{
	SectViewXml *pView = new SectViewXml(parent);
	pView->pSynoptic = XmlSynoptic::createFromFile(pView, fileName, mode, pView->errMsg);
	if(pView->pSynoptic)
	{
		pView->setFixedWidth(pView->pSynoptic->getWidth());
		pView->setFixedHeight(pView->pSynoptic->getHeight());
	}
	else
	{
		if(pView->errMsg.isEmpty())
		{
			pView->errMsg = "No synoptic data";
		}
		QRect errRect = pView->fontMetrics().boundingRect(pView->errMsg);
		pView->setMinimumWidth(errRect.width() + 50);
		pView->setMinimumHeight(errRect.height() + 50);
	}
	return pView;
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

SectViewXml::SectViewXml(QWidget *parent, Qt::WindowFlags f) : SectView(parent, f)
{
	pSynoptic = NULL;
}

SectViewXml::~SectViewXml()
{
	if(pSynoptic)
	{
		delete pSynoptic;
	}
}

void SectViewXml::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	if(pSynoptic)
	{
		pSynoptic->draw(painter);
		QTransform noTransform;
		painter.setTransform(noTransform);
		painter.resetTransform();
		return;
	}
	painter.drawText(rect(), Qt::AlignHCenter | Qt::AlignVCenter, errMsg);
}

