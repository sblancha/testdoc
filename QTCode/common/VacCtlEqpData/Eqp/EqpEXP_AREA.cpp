//	Implementation of EqpEXP_AREA class
/////////////////////////////////////////////////////////////////////////////////


#include "EqpEXP_AREA.h"

#include "FunctionalType.h"
#include "EqpMsgCriteria.h"

#ifndef Q_OS_WIN
#include "ResourcePool.h"
#endif

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

EqpEXP_AREA::EqpEXP_AREA(const Eqp &source) : Eqp(source)
{
	functionalType = FunctionalType::EXP_AREA;

	// Load description (tooltip)
#ifndef Q_OS_WIN
	QString resourceName("EXP_AREA.");
	resourceName += dpName;
	resourceName += ".Description";
	ResourcePool::getInstance().getStringValue(resourceName.toLatin1(), description);
#endif

}

EqpEXP_AREA::~EqpEXP_AREA()
{
}

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpEXP_AREA::getMainStateStringForMenu(QString &string, DataEnum::DataMode /* mode */)
{
	if(description.isEmpty())
	{
		string = getVisibleName();
	}
	else
	{
		string = description;
	}
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpEXP_AREA::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	getMainStateStringForMenu(string, mode);
}
#endif

