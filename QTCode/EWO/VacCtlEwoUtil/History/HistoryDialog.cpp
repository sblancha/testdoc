/////////////////////////////////////////////////////////////////////////////////
//	Implementation of HistoryDialog class

#include "HistoryDialog.h"
#include "HistoryImage.h"
#include "HistoryTimeRangeDialog.h"

#include "HistoryProcessor.h"

#include "DpeHistoryQuery.h"
#include "VacMainView.h"

#include "DataPool.h"
#include "Eqp.h"

#include <QScrollBar>
#include <QTableWidget>
#include <QHeaderView>
#include <QLabel>
#include <QGroupBox>
#include <QCheckBox>
#include <QPushButton>
#include <QMenu>
#include <QSpinBox>
#include <QLayout>
#include <QCursor>
#include <QFileDialog>
#include <QMessageBox>
#include <QColorDialog>
#include <QToolTip>
#include <QApplication>
#include <QDesktopWidget>
#include <QInputDialog>
#include <QMouseEvent>
#include <QComboBox>

#include <errno.h>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

QList<HistoryDialog *>	HistoryDialog::instanceList;
int HistoryDialog::lastInstanceId = 0;

static QString emptyString;

void HistoryDialog::instanceActivated(HistoryDialog *pActiveInstance)
{
	for(int idx = 0 ; idx < instanceList.count() ; idx++)
	{
		HistoryDialog *pInstance = instanceList.at(idx);
		pInstance->activated++;
	}
	pActiveInstance->activated = 0;
}
HistoryDialog *HistoryDialog::findActive(void)
{
	int min = 0x7FFFFFFF;
	HistoryDialog *pResult = NULL;
	for(int idx = 0 ; idx < instanceList.count() ; idx++)
	{
		HistoryDialog *pInstance = instanceList.at(idx);
		if(pInstance->activated < min)
		{
			min = pInstance->activated;
			pResult = pInstance;
		}
	}
	return pResult;
}

/*
**	FUNCTION
**		Build list of all opened instances
**
**	ARGUMENTS
**		idList	- Variable where ID of all instances will be added
**
**	RETURNS
**		Number of instances
**
**	CAUTIONS
**		None
*/
int HistoryDialog::getAllInstances(QList<int> &idList)
{
	idList.clear();
	for(int idx = 0 ; idx < instanceList.count() ; idx++)
	{
		HistoryDialog *pInstance = instanceList.at(idx);
		idList.append(pInstance->getId());
	}
	return idList.count();
}

/*
**	FUNCTION
**		Find instance of dialog with given ID
**
**	ARGUMENTS
**		id	- Required ID of instance to find. id == 0 is interpreted as:
**				any instance is required.
**
**	RETURNS
**		Pointer to instance with given ID; or
**		NULL if such instance is not found
**
**	CAUTIONS
**		None
*/
HistoryDialog *HistoryDialog::findInstance(int id)
{
	if(!id)
	{
		return findActive();
	}
	for(int idx = 0 ; idx < instanceList.count() ; idx++)
	{
		HistoryDialog *pInstance = instanceList.at(idx);
		if(pInstance->getId() == id)
		{
			return pInstance;
		}
	}
	return NULL;
}

/*
**	FUNCTION
**		Create new instance
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		ID of new instance
**
**	CAUTIONS
**		None
*/
int HistoryDialog::create(void)
{
	HistoryDialog *pInstance = new HistoryDialog();
	pInstance->show();
	return pInstance->getId();
}

int HistoryDialog::getFilterType(int instanceId)
{
	HistoryDialog *pInstance = findInstance(instanceId);
	if(pInstance)
	{
		return pInstance->getFilterType();
	}
	return 0;
}

int HistoryDialog::setFilterType(int instanceId, int type)
{
	HistoryDialog *pInstance = findInstance(instanceId);
	if(pInstance)
	{
		pInstance->setFilterType(type);
		return 1;
	}
	return 0;
}

/*
**	FUNCTION
**		Clear instance with given ID
**
**	ARGUMENTS
**		instanceId	- ID of instance to be cleared
**
**	RETURNS
**		1 = Instance found;
**		0 = instance not found
**
**	CAUTIONS
**		None
*/
int HistoryDialog::clear(int instanceId)
{
	HistoryDialog *pInstance = findInstance(instanceId);
	if(pInstance)
	{
		pInstance->clear();
		return 1;
	}
	return 0;
}

/*
**	FUNCTION
**		Add device to history dialog, create and open dialog if it does
**		not exist yet.
**
**	ARGUMENTS
**		pEqp			- Pointer to device to be added to history
**		historyDepth	- Depth of available history [seconds back from now]
**
**	RETURNS
**		1 = device has been added;
**		0 = device was not added (for example, it does not have value DPE)
**
**	CAUTIONS
**		None
*/
int HistoryDialog::addDevice(Eqp *pEqp, int historyDepth)
{
	// Are there value DPEs in this device?
	QStringList valueDpes;
	pEqp->getValueDpes(valueDpes);
	if(valueDpes.isEmpty())
	{
		return 0;
	}

	// Create dialog instance if it does not exist yet
	HistoryDialog *pInstance = findInstance();
	if(!pInstance)
	{
		pInstance = new HistoryDialog();
		pInstance->show();
	}
	return pInstance->addEqp(pEqp, historyDepth);
}

/*
**	FUNCTION
**		Add device to history dialog with given ID.
**
**	ARGUMENTS
**		instanceId		- Required dialog instance ID
**		pEqp			- Pointer to device to be added to history
**		colorString		- Color for new device (PVSS color format {R,G,B})
**		historyDepth	- Depth of available history [seconds back from now]
**
**	RETURNS
**		1 = device has been added;
**		0 = device was not added (for example, it does not have value DPE, or dialog
**			with given ID is not opened)
**
**	CAUTIONS
**		None
*/
int HistoryDialog::addDevice(int instanceId, Eqp *pEqp, const char *colorString, int historyDepth)
{
	// Are there value DPEs in this device?
	QStringList valueDpes;
	pEqp->getValueDpes(valueDpes);
	if(valueDpes.isEmpty())
	{
		return 0;
	}

	// Create dialog instance if it does not exist yet
	HistoryDialog *pInstance = findInstance(instanceId);
	if(!pInstance)
	{
		return 0;
	}
	QColor color;
	stringToColor(colorString, color);
	return pInstance->addEqp(pEqp, color, historyDepth);
}


/*
**	FUNCTION
**		Add dpe of a device to history dialog, create and open dialog if it does
**		not exist yet.
**
**	ARGUMENTS
**		pEqp			- Pointer to device to be added to history
**		dpeName			- Name of the Dpe extension to be displayed in history
**		yAxisName		- Name of the y-Axis associated to the dpe
**		historyDepth	- Depth of available history [seconds back from now]
**
**	RETURNS
**		1 = device has been added;
**		0 = device was not added (for example, it does not have value DPE)
**
**	CAUTIONS
**		None
*/
int HistoryDialog::addDeviceDpe(Eqp *pEqp, const char *dpeName,  const char *yAxisName, int historyDepth)
{
	
	/* Check removed, getValueDpes is not relevant for this method
	// Are there value DPEs in this device?
	QStringList valueDpes;
	pEqp->getValueDpes(valueDpes);
	if(valueDpes.isEmpty())
	{
		return 0;
	}*/
	

	// Create dialog instance if it does not exist yet
	HistoryDialog *pInstance = findInstance();
	if(!pInstance)
	{
		pInstance = new HistoryDialog();
		pInstance->show();
	}
	return pInstance->addDpe(pEqp, dpeName, yAxisName, historyDepth);
}

/*
**	FUNCTION
**		Add DPE of a device to history dialog with given ID
**
**	ARGUMENTS
**		instanceId		- Required dialog instance ID
**		pEqp			- Pointer to device to be added to history
**		dpeName			- Name of the Dpe extension to be displayed in history
**		yAxisName		- Name of the y-Axis associated to the dpe
**		colorString		- Color for new device (PVSS color format {R,G,B})
**		historyDepth	- Depth of available history [seconds back from now]
**
**	RETURNS
**		1 = device has been added;
**		0 = device was not added (for example, it does not have value DPE)
**
**	CAUTIONS
**		None
*/
int HistoryDialog::addDeviceDpe(int instanceId, Eqp *pEqp, const char *dpeName, const char *yAxisName, const char *colorString, int historyDepth)
{
	// Find dialog instance
	HistoryDialog *pInstance = findInstance(instanceId);
	if(!pInstance)
	{
		return 0;
	}
	QColor color;
	stringToColor(colorString, color);
	return pInstance->addDpe(pEqp, dpeName, yAxisName, color, historyDepth);
}

/*
**	FUNCTION
**		Add device to history dialog, create and open dialog if it does
**		not exist yet.
**
**	ARGUMENTS
**		pEqp			- Pointer to device to be added to history
**		dpe				- Name of DPE where to take bit
**		bitNbr			- Bit number to take from dpe
**		name			- Name of this bit for history
**		historyDepth	- Depth of available history [seconds back from now]
**
**	RETURNS
**		1 = device has been added;
**		0 = device was not added (for example, it does not have value DPE, or dialog
**			with given ID is not opened)
**
**	CAUTIONS
**		None
*/
int HistoryDialog::addDeviceBit(Eqp *pEqp, const char *dpe, int bitNbr, const char *name, int historyDepth)
{
	// Create dialog instance if it does not exist yet
	HistoryDialog *pInstance = findInstance();
	if(!pInstance)
	{
		pInstance = new HistoryDialog();
		pInstance->show();
	}
	return pInstance->addEqpBit(pEqp, dpe, bitNbr, name, historyDepth);
}

/*
**	FUNCTION
**		Add device to history dialog instance with given ID
**
**	ARGUMENTS
**		instanceId		- Required dialog instance ID
**		pEqp			- Pointer to device to be added to history
**		dpe				- Name of DPE where to take bit
**		bitNbr			- Bit number to take from dpe
**		name			- Name of this bit for history
**		colorString		- Color for new device (PVSS color format {R,G,B})
**		historyDepth	- Depth of available history [seconds back from now]
**
**	RETURNS
**		1 = device has been added;
**		0 = device was not added (for example, it does not have value DPE
**
**	CAUTIONS
**		None
*/
int HistoryDialog::addDeviceBit(int instanceId, Eqp *pEqp, const char *dpe, int bitNbr, const char *name, const char *colorString, int historyDepth)
{
	// Create dialog instance if it does not exist yet
	HistoryDialog *pInstance = findInstance(instanceId);
	if(!pInstance)
	{
		return 0;
	}
	QColor color;
	stringToColor(colorString, color);
	return pInstance->addEqpBit(pEqp, dpe, bitNbr, name, color, historyDepth);
}


/*
**	FUNCTION
**		Add history of one bit for given device.
**
**	ARGUMENTS
**		id		- Device data range ID
**		dpe		- DPE name for device
**		bitState	- Flag indicating if this is value for bit state DPE
**		bitNbr	- Bit number for bit state history
***		time	- Timestamp for value
**		value	- Value
**
**	RETURNS
**		true	- If history dialog instance exists;
**		false	- if history dialog instance does not exist, hence further
**					history data processing makes no sence
**
**	CAUTIONS
**		None
*/
bool HistoryDialog::addHistoryValue(int id, const char *dpe, bool bitState, int bitNbr, const QDateTime &time, const QVariant &value)
{
	bool result = false;
	for(int idx = 0 ; idx < instanceList.count() ; idx++)
	{
		HistoryDialog *pInstance = instanceList.at(idx);
		if(pInstance->pImage->addHistoryValue(id, dpe, bitState, bitNbr, time, value))
		{
			result = true;
		}
	}
	return result;
}

/*
**	FUNCTION
**		All history values for one range of one DPE arrived - redraw image
**
**	ARGUMENTS
**		id		- Device data range ID
**		dpe		- DPE name for device
**		bitState	- Flag indicating if this is value for bit state DPE
**		bitNbr	- Bit number for bit state history
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDialog::finishHistoryRange(int id, const char *dpe, bool bitState, int bitNbr)
{
	for(int idx = 0 ; idx < instanceList.count() ; idx++)
	{
		HistoryDialog *pInstance = instanceList.at(idx);
		pInstance->pImage->finishHistoryRange(id, dpe, bitState, bitNbr);
		pInstance->pImage->rebuildImage(20);
	}
}

bool HistoryDialog::stringToColor(const char *string, QColor &color)
{
	int r, g, b;
	if(string)
	{
		if(sscanf(string, "{%d,%d,%d}", &r, &g, &b) == 3)
		{
			color.setRgb(r, g, b);
			return true;
		}
	}
	color.setRgb(0, 0, 0);
	return false;
}

void HistoryDialog::colorToString(const QColor &color, QString &string)
{
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "{%d,%d,%d}", color.red(), color.green(), color.blue());
#else
	sprintf(buf, "{%d,%d,%d}", color.red(), color.green(), color.blue());
#endif
	string = buf;
}

void HistoryDialog::getAllItems(int id, QStringList &dps, QStringList &dpes, int **bitNbrs, QStringList &colors, QStringList &axisTypeNames)
{
	dps.clear();
	dpes.clear();
	*bitNbrs = NULL;
	colors.clear();
	axisTypeNames.clear();
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		pInstance->getAllItems(dps, dpes, bitNbrs, colors, axisTypeNames);
	}
}

int HistoryDialog::getTimeScaleParam(int id, QDateTime &start, QDateTime &end, bool &online, bool &logScale)
{
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		pInstance->getTimeScaleParam(start, end, online, logScale);
	}
	return pInstance != NULL;
}

int HistoryDialog::setTimeScaleParam(int id, const QDateTime &start, const QDateTime &end, bool online, bool logScale)
{
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		pInstance->setTimeScaleParam(start, end, online, logScale);
	}
	return pInstance != NULL;
}

int HistoryDialog::getVerticalScalesParams(int id, int &nScales, int **ppTypes, bool **ppLogs, float **ppMins, float **ppMaxs)
{
	nScales = 0;
	*ppTypes = NULL;
	*ppLogs = NULL;
	*ppMins = NULL;
	*ppMaxs = NULL;
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		pInstance->pImage->getVerticalScalesParams(nScales, ppTypes, ppLogs, ppMins, ppMaxs);
	}
	return pInstance != NULL;
}
	
int HistoryDialog::setVerticalScaleParams(int id, int type, bool log, float min, float max)
{
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		pInstance->pImage->setVerticalScaleParams(type, log, min, max);
	}
	return pInstance != NULL;
}

int HistoryDialog::getAllComments(int id, QStringList &comments)
{
	comments.clear();
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		pInstance->getAllComments(comments);
	}
	return pInstance != NULL;
}

int HistoryDialog::setAllComments(int id, QStringList comments)
{
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		pInstance->setAllComments(comments);
	}
	return pInstance != NULL;
}

const QString &HistoryDialog::getTitle(int id)
{
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		return pInstance->getTitle();
	}
	return emptyString;
}

int HistoryDialog::setTitle(int id, const char *title)
{
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		pInstance->setTitle(title);
	}
	return pInstance != NULL;
}

int HistoryDialog::getWindowGeometry(int id, int &x, int &y, int &w, int &h)
{
	HistoryDialog *pInstance = findInstance(id);
	x = y = 0;
	w = h = 300;
	if(pInstance)
	{
		QRect window = pInstance->geometry();
		x = window.x();
		y = window.y();
		w = window.width();
		h = window.height();
	}
	return pInstance != NULL;
}

int HistoryDialog::setWindowGeometry(int id, int x, int y, int w, int h)
{
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		pInstance->setWindowGeometry(x, y, w, h);
	}
	return pInstance != NULL;
}

const QString &HistoryDialog::getConfigName(int id)
{
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		return pInstance->getConfigName();
	}
	return emptyString;
}

int HistoryDialog::setConfigName(int id, const QString &name)
{
	HistoryDialog *pInstance = findInstance(id);
	if(pInstance)
	{
		pInstance->setConfigName(name);
	}
	return pInstance != NULL;
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

HistoryDialog::HistoryDialog() :
	QDialog(NULL /* VacMainView::getInstance() */, Qt::Window)
{
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle("History");
	selectionRow = -1;
	activated = 0;

	settingSpinBoxes = settingScrollBar = settingOnline = false;
	buildLayout();
	connect(pImage, SIGNAL(imageUpdated(void)), this, SLOT(updateScrollBar(void)));
	connect(pImage, SIGNAL(valueAtPointerChanged(const QString &, const QString &)),
		this, SLOT(valueAtPointerChanged(const QString &, const QString &)));
	connect(pImage, SIGNAL(scalesChanged(void)), this, SLOT(imageZoomed(void)));
	showTimeRange();
	/*
	const QList<VacMainView *> &mainList = VacMainView::getInstanceList();
	for(int idx = 0 ; idx < mainList.count() ; idx++)
	{
		VacMainView *pInstance = mainList.at(idx);
		if(pInstance->isPvssEvents())
		{
			QObject::connect(this, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)),
				pInstance, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
		}
	}
	*/
	VacMainView *pMainView = VacMainView::getInstance();
	if(pMainView)
	{
		QObject::connect(this, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)),
			pMainView, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
		connect(this, SIGNAL(saveRequest(int)),
			pMainView, SIGNAL(historySaveRequest(int)));
		connect(this, SIGNAL(saveMultiRequest(int)),
			pMainView, SIGNAL(historySaveMultiRequest(int)));
		connect(this, SIGNAL(loadRequest(int)),
			pMainView, SIGNAL(historyLoadRequest(int)));
		connect(this, SIGNAL(fillNumberRangeRequest(int)),
			pMainView, SIGNAL(historyFillNumberRequest(int)));
	}

	// Assign ID, add itself to list of instances
	id = ++lastInstanceId;
	instanceList.append(this);
}

HistoryDialog::~HistoryDialog()
{
	instanceList.removeOne(this);
}

void HistoryDialog::setTitle(const QString &newTitle)
{
	title = newTitle;
	QString newCaption("History");
	if(!title.isEmpty())
	{
		newCaption += ": ";
		newCaption += title;
	}
	setWindowTitle(newCaption);
}

void HistoryDialog::setWindowGeometry(int x, int y, int w, int h)
{
	if(w < 200)
	{
		w = 200;
	}
	if(h < 200)
	{
		h = 200;
	}
	QRect window(x, y, w, h);
	QRect screen = QApplication::desktop()->rect();
	if(window.width() > screen.width())
	{
		window.setWidth(screen.width());
	}
	if(window.height() > screen.height())
	{
		window.setHeight(screen.height());
	}
	int delta = screen.left() - window.left();
	if(delta > 0)
	{
		window.moveLeft(screen.left());
	}
	delta = screen.top() - window.top();
	if(delta > 0)
	{
		window.moveTop(screen.top());
	}
	delta = window.right() - screen.right();
	if(delta > 0)
	{
		window.moveRight(screen.right());
	}
	delta = window.bottom() - screen.bottom();
	if(delta > 0)
	{
		window.moveBottom(screen.bottom());
	}

	setGeometry(window);
}

void HistoryDialog::getAllComments(QStringList &comments)
{
	comments.clear();
	for(int idx = 0 ; idx < pTable->rowCount() ; idx++)
	{
		QTableWidgetItem *pItem = pTable->item(idx, 3);
		if(pItem)
		{
			comments.append(pItem->text());
		}
		else
		{
			comments.append("");
		}
	}
}

void HistoryDialog::setAllComments(QStringList comments)
{
	for(int idx = 0 ; idx < pTable->rowCount() ; idx++)
	{
		if(idx >= comments.size())
		{
			break;
		}
		QTableWidgetItem *pItem = new QTableWidgetItem(comments.at(idx));
		pTable->setItem(idx, 3, pItem);
	}
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void HistoryDialog::buildLayout(void)
{
	// 1) Main layout - image and scroll bar on the left, table and control on the right
	QHBoxLayout *pMainBox = new QHBoxLayout(this);
	pMainBox->setContentsMargins(0, 0, 0, 0);
	pMainBox->setSpacing(0);

	// 2) Vertical layout with image on top and scroll bar on bottom
	QVBoxLayout *pLeftBox = new QVBoxLayout();
	pLeftBox->setContentsMargins(0, 0, 0, 0);
	pLeftBox->setSpacing(0);
	pMainBox->addLayout(pLeftBox, 10);
	pMainBox->setStretchFactor(pLeftBox, 10);

	pImage = new HistoryImage(this);
	pLeftBox->addWidget(pImage, 10);

	pScrollBar = new QScrollBar(Qt::Horizontal, this);
	pLeftBox->addWidget(pScrollBar);
	pScrollBar->setEnabled(false);
	connect(pScrollBar, SIGNAL(valueChanged(int)), this, SLOT(scrollBarValueChange(int)));
	connect(pScrollBar, SIGNAL(sliderReleased(void)), this, SLOT(scrollBarSliderRelease(void)));

	// Vertical layout with table and all controls
	QVBoxLayout *pRightBox = new QVBoxLayout();
	pMainBox->addLayout(pRightBox);

	// Column 0 = selection marker
	// Column 1 = color
	// Column 2 = device name (+ bit name if bit history is shown)
	// Column 3 = user's comment, cell is editable
	pTable = new QTableWidget(0, 4, this);
	pTable->verticalHeader()->hide();
	pTable->horizontalHeader()->hide();
	/* LIK 17.10.2011 -> Qt 4
	pTable->setLeftMargin(0);
	pTable->setTopMargin(0);
	pTable->setColumnReadOnly(0, true);
	pTable->setColumnReadOnly(1, true);
	pTable->setColumnReadOnly(2, true);
	pTable->setColumnReadOnly(3, true);
	*/
	pTable->setColumnWidth(0, 12);
	pTable->setColumnWidth(1, 12);
	pRightBox->addWidget(pTable, 10);

	connect(pTable, SIGNAL(cellPressed(int, int)),
		this, SLOT(tableMousePress(int, int)));
	connect(pTable, SIGNAL(cellDoubleClicked(int, int)),
		this, SLOT(tableDoubleClick(int, int)));
	connect(pTable, SIGNAL(cellChanged(int, int)),
		this, SLOT(tableCellChanged(int, int)));

	pTable->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(pTable, SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(tableContextMenuRequest(const QPoint &)));

	QGroupBox *pValueBox = new QGroupBox("Time and Value", this);
//	pValueBox->setInsideMargin(2);
	QVBoxLayout *pTimeValueLayout = new QVBoxLayout(pValueBox);
	pTimeValueLayout->setSpacing(0);
	pTimeValueLayout->setContentsMargins(0, 0, 0, 0);
	pTimeLabel = new QLabel(pValueBox);
	pTimeValueLayout->addWidget(pTimeLabel);
	pValueLabel = new QLabel(pValueBox);
	pTimeValueLayout->addWidget(pValueLabel);
	pRightBox->addWidget(pValueBox);

	QGroupBox *pModeBox = new QGroupBox("Mode", this);
//	pModeBox->setInsideMargin(2);

	QHBoxLayout *pModeLayout = new QHBoxLayout(pModeBox);
	pModeLayout->setSpacing(1);
	pModeLayout->setContentsMargins(0, 0, 0, 0);
	pOnlineCheckBox = new QCheckBox("Online", pModeBox);
	pOnlineCheckBox->setChecked(true);
	pModeLayout->addWidget(pOnlineCheckBox);
	connect(pOnlineCheckBox, SIGNAL(toggled(bool)), this, SLOT(onlineChange(bool)));

	pFilterTypeCombo = new QComboBox(pModeBox);
	pFilterTypeCombo->addItem("No filter");
	pFilterTypeCombo->addItem("Filter MIN");
	pModeLayout->addWidget(pFilterTypeCombo);
	connect(pFilterTypeCombo, SIGNAL(activated(int)), this, SLOT(filterTypeComboActivated(int)));
	/*
	pLogTimeCheckBox = new QCheckBox("Log(t)", pModeBox1, "History logT CheckBox");
	pLogTimeCheckBox->setChecked(false);
	connect(pLogTimeCheckBox, SIGNAL(toggled(bool)), this, SLOT(logTimeChange(bool)));

	QHBox *pModeBox2 = new QHBox(pModeBox, "Mode box 2");
	pModeBox2->setSpacing(1);
	pRpLinearCheckBox = new QCheckBox("Lin (PR)", pModeBox2, "Linear PR CheckBox");
	pRpLinearCheckBox->setChecked(false);
	connect(pRpLinearCheckBox, SIGNAL(toggled(bool)), this, SLOT(linearPrChange(bool)));
	pIntensLinearCheckBox = new QCheckBox("Lin(I)", pModeBox2, "Linear I CheckBox");
	pIntensLinearCheckBox->setChecked(false);
	connect(pIntensLinearCheckBox, SIGNAL(toggled(bool)), this, SLOT(linearIntensChange(bool)));
	*/

	pRightBox->addWidget(pModeBox);

	pRangeBox = new QGroupBox("Time Interval", this);
//	pRangeBox->setInsideMargin(2);
	QHBoxLayout *pRangeLayout = new QHBoxLayout(pRangeBox);
	pRangeLayout->setSpacing(1);
	pRangeLayout->setContentsMargins(0, 0, 0, 0);
	pMinSpinBox = new QSpinBox(pRangeBox);
	pMinSpinBox->setRange(1, 59);
	pMinSpinBox->setSingleStep(1);
	pMinSpinBox->setSuffix(" min");
	pRangeLayout->addWidget(pMinSpinBox);
	connect(pMinSpinBox, SIGNAL(valueChanged(int)), this, SLOT(minChange(int)));

	pHourSpinBox = new QSpinBox(pRangeBox);
	pHourSpinBox->setRange(0, 23);
	pHourSpinBox->setSingleStep(1);
	pHourSpinBox->setSuffix(" h");
	pRangeLayout->addWidget(pHourSpinBox);
	connect(pHourSpinBox, SIGNAL(valueChanged(int)), this, SLOT(hourChange(int)));

	pDaySpinBox = new QSpinBox(pRangeBox);
	pDaySpinBox->setRange(0, 99);
	pDaySpinBox->setSingleStep(1);
	pDaySpinBox->setSuffix(" day");
	pRangeLayout->addWidget(pDaySpinBox);
	connect(pDaySpinBox, SIGNAL(valueChanged(int)), this, SLOT(dayChange(int)));

	pRangePb = new QPushButton("...", this);
	connect(pRangePb, SIGNAL(clicked()), this, SLOT(rangeButtonClicked()));
	pRangePb->resize(20, 16);
	pRangePb->setToolTip("Select time range for history");
	pRangePb->show();

	// Fill number push button - only for LHC
	DataPool &pool = DataPool::getInstance();
	if(pool.isLhcFormat())
	{
		pFillNumberPb = new QPushButton("FN", this);
		connect(pFillNumberPb, SIGNAL(clicked()), this, SLOT(fillNumberButtonClicked()));
		QFontMetrics fm = pFillNumberPb->fontMetrics();
		QRect rect = fm.boundingRect("FN");
		int widthPb = rect.width() + 12;
		if(widthPb < 16)
		{
			widthPb = 16;
		}
		int heightPb = rect.height() + 4;
		if(heightPb < 16)
		{
			heightPb = 16;
		}
		pFillNumberPb->resize(widthPb, heightPb);
		pFillNumberPb->setToolTip("Select LHC fill number for history");
		pFillNumberPb->show();
	}
	else
	{
		pFillNumberPb = NULL;
	}
	
	pRightBox->addWidget(pRangeBox);

	QHBoxLayout *pButtonLayout = new QHBoxLayout();
	pRightBox->addLayout(pButtonLayout);
	pFilePb = new QPushButton("&File", this);
	pFilePb->setFlat(true);
	QSize size = pFilePb->fontMetrics().size(Qt::TextSingleLine, "File");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pFilePb->setFixedSize(size);

	pFileMenu = new QMenu(this);
	pFileMenu->addAction("Set Title...", this, SLOT(fileTitle()));
	pFileMenu->addSeparator();
	pFileMenu->addAction("Print Panel...", this, SLOT(filePrint()));
	pFileMenu->addAction("Export to Excel...", this, SLOT(fileExport()));
	pFileMenu->addAction("Export to CSV...", this, SLOT(fileExportCsv()));
	pFileMenu->addAction("Save image to PNG...", this, SLOT(fileSavePng()));
	pFileMenu->addAction("Save all open images to PNG...", this, SLOT(fileSaveAllPng()));
	pFileMenu->addSeparator();
	pFileMenu->addAction("Save Configuration...", this, SLOT(fileSave()));
	pFileMenu->addAction("Save Multi Configuration...", this, SLOT(fileSaveMulti()));
	pFileMenu->addAction("Load Configuration...", this, SLOT(fileLoad()));
	pFilePb->setMenu(pFileMenu);
	pButtonLayout->addWidget(pFilePb);

	pClosePb = new QPushButton("&Close", this);
	pClosePb->setFlat(true);
	size = pClosePb->fontMetrics().size(Qt::TextSingleLine, "Close");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pClosePb->setFixedSize(size);
	connect(pClosePb, SIGNAL(clicked(void)), this, SLOT(closePress(void)));
	pButtonLayout->addWidget(pClosePb);

	pHelpPb = new QPushButton("&Help", this);
	pHelpPb->setFlat(true);
	size = pHelpPb->fontMetrics().size(Qt::TextSingleLine, "Help");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pHelpPb->setFixedSize(size);
	connect(pHelpPb, SIGNAL(clicked(void)), this, SLOT(helpPress(void)));
	pButtonLayout->addWidget(pHelpPb);
}

/*
**	FUNCTION
**		Remove all items from history
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDialog::clear(void)
{
	for(int row = pTable->rowCount() - 1 ; row >= 0 ; row--)
	{
		const Eqp *pEqp = pImage->removeItem(row);
		pTable->removeRow(row);
		if(pEqp)
		{
			if(pImage->getEqpIndex(pEqp) < 0)	// The very last instance
			{
				disconnect(pEqp, SIGNAL(selectChanged(Eqp *)), this, SLOT(eqpSelectChanged(Eqp *)));
			}
		}
	}
	pImage->rebuildImage(21);
}

void HistoryDialog::setFilterType(int type)
{
	HistoryDataRange::FilterType newType = HistoryDataRange::FilterTypeMinMax;
	switch(type)
	{
	case HistoryDataRange::FilterTypeMin:
		newType = HistoryDataRange::FilterTypeMin;
		break;
	default:
		break;
	}
	if(pImage->getFilterType() == newType)
	{
		return;
	}
	pImage->setFilterType(newType);
	pFilterTypeCombo->setCurrentIndex(newType);
}

/*
**	FUNCTION
**		Return information for all items shown in this instance
**
**	ARGUMENTS
**		dps		- List of all DP names will be returned here
**		dpes	- List of all DPE names (for bit history) will be returned here
**		bitNbrs	- Pointer to array of all bit numbers (for bit history) will be rturned here
**		colors	- List of all item colors will be returned here
**		axisTypeNames	- List of all axis type names will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		It is responsability of caller to free memory allocated for bitNbrs
*/
void HistoryDialog::getAllItems(QStringList &dps, QStringList &dpes, int **bitNbrs, QStringList &colors, QStringList &axisTypeNames)
{
	QList<HistoryEqpData *> &items = pImage->getEqps();
	for(int idx = 0 ; idx < items.count() ; idx++)
	{
		HistoryEqpData *pItem = items.at(idx);
		int count = dps.count();
		if((count % 0xF) == 0)
		{
			*bitNbrs = (int *)realloc((void *)*bitNbrs, (count + 16 ) * sizeof(int));
		}
		dps.append(pItem->getEqp()->getDpName());
		dpes.append(pItem->getPureDpeName());
		if(pItem->isBitState())
		{
			(*bitNbrs)[count] = pItem->getBitNbr();
		}
		else
		{
			(*bitNbrs)[count] = -1;
		}
		QString colorString;
		colorToString(pItem->getColor(), colorString);
		colors.append(colorString);
		axisTypeNames.append(pItem->getAxisTypeName());
	}
}

/*
**	FUNCTION
**		Return parameters on recent settings of time scale
**
**	ARGUMENTS
**		start	- Start time of scale is returned here
**		end		- End time of scale is returned here
**		online	- True is returned here if history is in online mode
**		logScale	- True is returned here if time scale is in logarithmic mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDialog::getTimeScaleParam(QDateTime &start, QDateTime &end, bool &online, bool &logScale)
{
	start = pImage->getTimeMin();
	end = pImage->getTimeMax();
	online = pImage->isOnline();
	logScale = pImage->isTimeLogScale();
}

/*
**	FUNCTION
**		Set parameters of time scale
**
**	ARGUMENTS
**		start	- Start time of scale
**		end		- End time of scale
**		online	- True if history is in online mode. If in online mode - difference between
**					start and end is used to calculate time interval shown
**		logScale	- True if time scale shall be in logarithmic mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDialog::setTimeScaleParam(const QDateTime &start, const QDateTime &end, bool online, bool logScale)
{
	if(pImage->isOnline() != online)
	{
		pOnlineCheckBox->setChecked(online);
	}
	if(online)
	{
		int depth = start.secsTo(end) / 60;
		if(depth < 1)
		{
			depth = 1;
		}
		if(depth > pImage->getMaximumDepth() / 60)
		{
			depth = pImage->getMaximumDepth() / 60;
		}
		pImage->setDepth(depth);
		showTimeRange();
	}
	else
	{
		pImage->setTimeLimits(start, end);
		showTimeRange();
	}
	if(pImage->isTimeLogScale() != logScale)
	{
		pImage->setTimeLogScale(logScale);
	}
}

/*
**	FUNCTION
**		Add device to history view.
**
**	ARGUMENTS
**		pEqp			- Pointer to device to be added
**		historyDepth	- History depth for device [sec]
**
**	RETURNS
**		1	= Device has been added successfully;
**		0	= Device is already in view
**
**	CAUTIONS
**		It shall be checked before calling this method that
**		device has analog value DPE
*/
int HistoryDialog::addEqp(Eqp *pEqp, int historyDepth)
{
	// Check if there is already instance of this Eqp in image
	int eqpIdx = pImage->getEqpIndex(pEqp);

	// Add device to image
	QColor	color;
	int coco = pImage->addEqp(pEqp, historyDepth, color);
	if(!coco)
	{
		return coco;
	}
	// Add visible name to table
	addToTable(pEqp, color, NULL);

	// Connect to selection signal of device
	if(eqpIdx < 0)
	{
		connect(pEqp, SIGNAL(selectChanged(Eqp *)), this, SLOT(eqpSelectChanged(Eqp *)));
	}
	return coco;
}

/*
**	FUNCTION
**		Add device to history view.
**
**	ARGUMENTS
**		pEqp			- Pointer to device to be added
**		useColor		- Use this color for device
**		historyDepth	- History depth for device [sec]
**
**	RETURNS
**		1	= Device has been added successfully;
**		0	= Device is already in view
**
**	CAUTIONS
**		It shall be checked before calling this method that
**		device has analog value DPE
*/
int HistoryDialog::addEqp(Eqp *pEqp, const QColor &useColor, int historyDepth)
{
	// Check if there is already instance of this Eqp in image
	int eqpIdx = pImage->getEqpIndex(pEqp);

	// Add device to image
	QColor	color(useColor);
	int coco = pImage->addEqp(pEqp, historyDepth, color, true);
	if(!coco)
	{
		return coco;
	}
	// Add visible name to table
	addToTable(pEqp, color, NULL);

	// Connect to selection signal of device
	if(eqpIdx < 0)
	{
		connect(pEqp, SIGNAL(selectChanged(Eqp *)), this, SLOT(eqpSelectChanged(Eqp *)));
	}
	return coco;
}

/*
**	FUNCTION
**		Add Dpe of a device to history view.
**
**	ARGUMENTS
**		pEqp			- Pointer to device to be added
**		dpeName			- Extension name of Dpe
**		historyDepth	- History depth for device [sec]
**
**	RETURNS
**		1	= Device has been added successfully;
**		0	= Device is already in view
**
**	CAUTIONS
**		It shall be checked before calling this method that
**		device has analog value DPE
*/
int HistoryDialog::addDpe(Eqp *pEqp, const char *dpeName, const char *yAxisName, int historyDepth)
{
	// Check if there is already instance of this Eqp in image
	int eqpIdx = pImage->getEqpIndex(pEqp);

	// Add device to image
	QColor	color;
	int coco = pImage->addDpe(pEqp, dpeName, yAxisName, historyDepth, color, false);
	if(!coco)
	{
		return coco;
	}
	// Add visible name to table
	addToTable(pEqp, color, dpeName);
	// Connect to selection signal of device
	if(eqpIdx < 0)
	{
		connect(pEqp, SIGNAL(selectChanged(Eqp *)), this, SLOT(eqpSelectChanged(Eqp *)));
	}
	return coco;
}

/*
**	FUNCTION
**		Add device to history view.
**
**	ARGUMENTS
**		pEqp			- Pointer to device to be added
**		dpeName			- extension name of the Dpe
**		useColor		- Use this color for device
**		historyDepth	- History depth for device [sec]
**
**	RETURNS
**		1	= Device has been added successfully;
**		0	= Device is already in view
**
**	CAUTIONS
**		It shall be checked before calling this method that
**		device has analog value DPE
*/
int HistoryDialog::addDpe(Eqp *pEqp, const char *dpeName, const char *yAxisName, const QColor &useColor, int historyDepth)
{
	// Check if there is already instance of this Eqp in image
	int eqpIdx = pImage->getEqpIndex(pEqp);

	// Add device to image
	QColor	color(useColor);
	int coco = pImage->addDpe(pEqp, dpeName, yAxisName,  historyDepth, color, true);
	if(!coco)
	{
		return coco;
	}
	// Add visible name to table
	addToTable(pEqp, color, dpeName);

	// Connect to selection signal of device
	if(eqpIdx < 0)
	{
		connect(pEqp, SIGNAL(selectChanged(Eqp *)), this, SLOT(eqpSelectChanged(Eqp *)));
	}
	return coco;
}


/*
**	FUNCTION
**		Add history of one bit for given device.
**
**	ARGUMENTS
**		id				- Device data range ID
**		dpe				- DPE name for device
**		bitNbr			- Bit number in DPE
**		bitName			- Visible name of bit
**		historyDepth	- History depth for device [sec]
**
**	RETURNS
**		true	- If history dialog instance exists;
**		false	- if history dialog instance does not exist, hence further
**					history data processing makes no sence
**
**	CAUTIONS
**		None
*/
int HistoryDialog::addEqpBit(Eqp *pEqp, const char *dpe, int bitNbr, const char *bitName, int historyDepth)
{
	// Check if there is already instance of this Eqp in image
	int eqpIdx = pImage->getEqpIndex(pEqp);

	// Add device to image
	QColor	color;
	int coco = pImage->addEqpBit(pEqp, dpe, bitNbr, bitName, historyDepth, color);
//qDebug("HistoryDialog::addEqpBit(%s, %s, %d): coco = %d\n", pEqp->getDpName(), dpe, bitNbr, coco);
	if(!coco)
	{
		return coco;
	}
	// Add visible name to table
	addToTable(pEqp, color, bitName);

	// Connect to selection signal of device
	if(eqpIdx < 0)
	{
		connect(pEqp, SIGNAL(selectChanged(Eqp *)), this, SLOT(eqpSelectChanged(Eqp *)));
	}
	return coco;
}

/*
**	FUNCTION
**		Add history of one bit for given device.
**
**	ARGUMENTS
**		id				- Device data range ID
**		dpe				- DPE name for device
**		bitNbr			- Bit number in DPE
**		bitName			- Visible name of bit
**		useColor		- color to be used for this plot in graph
**		historyDepth	- History depth for device [sec]
**
**	RETURNS
**		true	- If history dialog instance exists;
**		false	- if history dialog instance does not exist, hence further
**					history data processing makes no sence
**
**	CAUTIONS
**		None
*/
int HistoryDialog::addEqpBit(Eqp *pEqp, const char *dpe, int bitNbr, const char *bitName, const QColor &useColor, int historyDepth)
{
	// Check if there is already instance of this Eqp in image
	int eqpIdx = pImage->getEqpIndex(pEqp);

	// Add device to image
	QColor	color(useColor);
	int coco = pImage->addEqpBit(pEqp, dpe, bitNbr, bitName, historyDepth, color, true);
	if(!coco)
	{
		return coco;
	}
	// Add visible name to table
	addToTable(pEqp, color, bitName);

	// Connect to selection signal of device
	if(eqpIdx < 0)
	{
		connect(pEqp, SIGNAL(selectChanged(Eqp *)), this, SLOT(eqpSelectChanged(Eqp *)));
	}
	return coco;
}

void HistoryDialog::addToTable(Eqp *pEqp, const QColor &color, const char *suffixName)
{
	int rowIdx = pTable->rowCount();
	pTable->setRowCount(rowIdx + 1);
	QString itemName(pEqp->getVisibleName());
	if(suffixName)
	{
		itemName += ".";
		itemName += suffixName;
	}
	QTableWidgetItem *pItem = new QTableWidgetItem(itemName);
	pItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
	pTable->setItem(rowIdx, 2, pItem);
	pItem = new QTableWidgetItem();
	pItem->setBackground(color);
	pItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
	pTable->setItem(rowIdx, 1, pItem);
	pTable->resizeColumnToContents(2);
	/*
	if(suffixName)
	{
		pItem = new QTableWidgetItem(suffixName);
		pTable->setItem(rowIdx, 3, pItem);
		pTable->resizeColumnToContents(3);
	}
	*/
	pTable->resizeRowsToContents();
	pTable->setColumnWidth(0, 12);
	pTable->setColumnWidth(1, 12);
	selectionRow = rowIdx;
	QList<HistoryEqpData *> &eqps = pImage->getEqps();
	for(int idx = 0 ; idx < rowIdx ; idx++)
	{
		HistoryEqpData *pItem = eqps.at(idx);
		if(pItem->getEqp() == pEqp)
		{
			setSelectionInTable(idx, pEqp);
		}
	}
	setSelectionInTable(rowIdx, pEqp);
	selectionRow = -1;
}

/*
**	FUNCTION
**		Set selection mark in table
**
**	ARGUMENTS
**		row		- Table row
**		pEqp	- Pointer to device shown in that row
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDialog::setSelectionInTable(int row, Eqp *pEqp)
{
	bool selected = false;
	if(selectionRow >= 0)
	{
		if((row == selectionRow) && (pEqp->isSelected()))
		{
			selected = true;
		}
	}
	else
	{
		selected = pEqp->isSelected();
		if(selected)
		{
			selectionRow = row;
		}
	}
//	qDebug("selectionRow %d row %d eqp %s: selected %d\n", selectionRow, row, pEqp->getVisibleName(), (int)selected);
	pImage->getEqps().at(row)->setSelected(selected);
	setRowSelectionInTable(row, selected);
}

void HistoryDialog::setRowSelectionInTable(int row, bool isSelected)
{
	QTableWidgetItem *pItem = pTable->item(row, 0);
	if(isSelected)
	{
		if(!pItem)
		{
			pItem = new QTableWidgetItem("*");
			pItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			pTable->setItem(row, 0, pItem);
		}
		else
		{
			pItem->setText("*");
		}
	}
	else
	{
		if(pItem)
		{
			pItem->setText("");
		}
	}
}


/*
**	FUNCTION
**		Display current time range shown in history
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDialog::showTimeRange(void)
{
	// Show range in spin boxes
	settingSpinBoxes = true;
	int secs = pImage->getTimeMin().secsTo(pImage->getTimeMax());
	int totalMins = secs / 60;
	int days = totalMins / (60 * 24);
	pDaySpinBox->setValue(days);
	int hours = (totalMins - days * (60 * 24)) / 60;
	pHourSpinBox->setValue(hours);
	int mins = totalMins - days * (60 * 24) - hours * 60;
	pMinSpinBox->setValue(mins);
	settingSpinBoxes = false;
}

/*
**	FUNCTION
**		Update scroll bar parameters according to totally available
**		and currently shown time ranges
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDialog::updateScrollBar(void)
{
	if(pScrollBar->isSliderDown())
	{
		return;
	}
	settingScrollBar = true;
	int maxDepth = pImage->getMaximumDepth();
	pScrollBar->setMinimum(-maxDepth);
	pScrollBar->setMaximum(0);
	int timeRange = pImage->getDepth() * 60;
	pScrollBar->setPageStep(timeRange);
	pScrollBar->setSingleStep(timeRange / 6);
	int start = QDateTime::currentDateTime().secsTo(pImage->getTimeMax());
	pScrollBar->setValue(start);
	settingScrollBar = false;
}

/*
**	FUNCTION
**		Set new time range as a result of value change in spin box(es)
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryDialog::setRangeFromSpinBoxes(void)
{
	int totalMins = pMinSpinBox->value() +
		pHourSpinBox->value() * 60 +
		pDaySpinBox->value() * 60 * 24;
	if(totalMins < 1)
	{
		totalMins = 1;
	}
	if(totalMins > pImage->getMaximumDepth() / 60)
	{
		totalMins = pImage->getMaximumDepth() / 60;
	}

	pImage->setDepth(totalMins);
	showTimeRange();
}

void HistoryDialog::scrollBarValueChange(int value)
{
	if(settingScrollBar)
	{
		return;
	}
	QDateTime end(QDateTime::currentDateTime().addSecs(value));
	QDateTime start(end.addSecs(-pImage->getDepth() * 60));
	if(pScrollBar->isSliderDown())
	{
		pImage->showNextRange(start, end);
	}
	else
	{
		pImage->hideNextRange();
		pImage->setTimeLimits(start, end);
	}
}

void HistoryDialog::scrollBarSliderRelease(void)
{
	QDateTime end(QDateTime::currentDateTime().addSecs(pScrollBar->value()));
	QDateTime start(end.addSecs(-pImage->getDepth() * 60));
	pImage->hideNextRange();
	pImage->setTimeLimits(start, end);
}

void HistoryDialog::onlineChange(bool on)
{
	if(!settingOnline)
	{
		pImage->setOnline(on);
	}
	pScrollBar->setEnabled(!on);
}
/*
void HistoryDialog::logTimeChange(bool on)
{
	pImage->setLogTimeScale(on);
}

void HistoryDialog::linearPrChange(bool on)
{
	pImage->switchPressureScaleType(!on);
}

void HistoryDialog::linearIntensChange(bool on)
{
	pImage->switchIntensityScaleType(!on);
}
*/
void HistoryDialog::minChange(int /* value */)
{
	if(!settingSpinBoxes)
	{
		setRangeFromSpinBoxes();
	}
}
void HistoryDialog::hourChange(int /* value */)
{
	if(!settingSpinBoxes)
	{
		setRangeFromSpinBoxes();
	}
}
void HistoryDialog::dayChange(int /* value */)
{
	if(!settingSpinBoxes)
	{
		setRangeFromSpinBoxes();
	}
}

void HistoryDialog::tableCellChanged(int /* row */, int column)
{
	pTable->resizeColumnToContents(column);
	pTable->setColumnWidth(0, 12);
	pTable->setColumnWidth(1, 12);
}

void HistoryDialog::fileTitle(void)
{
	bool ok;
	QString text = QInputDialog::getText(this, "History Title", "Enter Title for this Window",
		QLineEdit::Normal, title, &ok);
	if(ok)
	{
		setTitle(text);
	}
}
void HistoryDialog::filePrint(void)
{
	VacMainView::printWidget(this);
}

void HistoryDialog::fileExport(void)
{
	writeToFile(false);
}

void HistoryDialog::fileExportCsv(void)
{
	writeToFile(true);
}

void HistoryDialog::writeToFile(bool writeCsv)
{
	if(!pTable->rowCount())
	{
		return;
	}
	QString newFile = QFileDialog::getSaveFileName(this,
		"Select a filename to save",
		VacMainView::getFilePath(),	// QString::null,
		writeCsv ? "Text files (*.txt)" : "Excel files (*.xls)");
	if(newFile.isEmpty())
	{
		return;
	}
	QFileInfo info(newFile);
	QString suffix(writeCsv ? "txt" : "xls");
	if(info.suffix() != suffix)
	{
		newFile += ".";
		newFile += suffix;
		info.setFile(newFile);
	}
	VacMainView::setFilePath(info.path());
	/* L.Kopylov 26.10.2012
	if(info.exists())
	{
		int answer = QMessageBox::warning(this,
			"File exists",
			"Selected file exists - overwrite it ?",
			QMessageBox::Yes, QMessageBox::No);
		if(answer != QMessageBox::Yes)
		{
			return;
		}
	}
	*/
	HistoryProcessor processor;
	pImage->addExportValues(processor);
#ifdef Q_OS_WIN
	FILE *pFile = NULL;
	if(fopen_s(&pFile, newFile.toLatin1(), "w"))
	{
		pFile = NULL;
	}
#else
	FILE *pFile = fopen(newFile.toLatin1(), "w");
#endif
	if(!pFile)
	{
		QMessageBox::warning(this, "WARNING", QString("Failed to open file for writing, errno %1").arg(errno));
		return;
	}
	QString error = processor.write(pFile, writeCsv);
	fclose(pFile);
	if(!error.isEmpty())
	{
		QMessageBox::warning(this, "WARNING", error);
	}
}

void HistoryDialog::fileSavePng(void)
{
	if(!pTable->rowCount())
	{
		return;
	}
	QString newFile = VacMainView::getFilePath();
#ifdef Q_OS_WIN
	newFile += "\\";
#else
	newFile += "/";
#endif
	if(title.isEmpty())
	{
		newFile += "history_no_title.png";
	}
	else
	{
		newFile += title;
		newFile += ".png";
	}
	newFile = QFileDialog::getSaveFileName(this,
		"Select a filename to save image",
		newFile,	// QString::null,
		"Image files (*.png)");
	/* This code allows for initial selection of file name (== dialog title), BUT it uses QT dialog, not native one
	**	1) Qt dialog looks differently
	**	2) Qt dialog appears slower
	** To be checked with users. L.Kopylov 04.09.2012
	QFileDialog dialog(this, "Select filename to save image", VacMainView::getFilePath(), "Image files (*.png)");
	// dialog.setNameFilter("Image files (*.png)");
	QString newFile;	// = VacMainView::getFilePath();
	if(title.isEmpty())
	{
		newFile += "history_no_title.png";
	}
	else
	{
		newFile += title;
		newFile += ".png";
	}
	//dialog.setDirectory(VacMainView::getFilePath());
	dialog.selectFile(newFile);
	//dialog.setLabelText(QFileDialog::Accept, "Save");
	dialog.setAcceptMode(QFileDialog::AcceptSave);
	if(!dialog.exec())
	{
		return;
	}
	QStringList selFiles = dialog.selectedFiles();
	if(selFiles.count() == 0)
	{
		return;
	}
	newFile = selFiles.at(0);
	*/

	if(newFile.isEmpty())
	{
		return;
	}
	QFileInfo info(newFile);
	QString suffix("png");
	if(info.suffix() != suffix)
	{
		newFile += ".";
		newFile += suffix;
		info.setFile(newFile);
	}
	VacMainView::setFilePath(info.path());
	/*
	if(info.exists())
	{
		int answer = QMessageBox::warning(this,
			"File exists",
			"Selected file exists - overwrite it ?",
			QMessageBox::Yes, QMessageBox::No);
		if(answer != QMessageBox::Yes)
		{
			return;
		}
	}
	*/
	QPixmap pix = QPixmap::grabWidget(this);
	pix.save(newFile, "PNG");
}

void HistoryDialog::fileSaveAllPng(void)
{
	if(!pTable->rowCount())
	{
		return;
	}
	QFileDialog dialog(this);
	dialog.setFileMode(QFileDialog::Directory);
	dialog.setOption(QFileDialog::ShowDirsOnly, true);
	dialog.setDirectory(VacMainView::getFilePath());
	if(!dialog.exec())
	{
		return;
	}
	QString pathName = dialog.directory().absolutePath();
	VacMainView::setFilePath(pathName);
	for(int idx = 0 ; idx < instanceList.count() ; idx++)
	{
		if(!instanceList[idx]->saveImageToPng(pathName))
		{
			break;
		}
	}
}

bool HistoryDialog::saveImageToPng(const QString &pathName)
{
	QString fileName(pathName);
#ifdef Q_OS_WIN
	fileName += "\\";
#else
	fileName += "/";
#endif
	if(title.isEmpty())
	{
		fileName += "history_no_title";
	}
	else
	{
		fileName += title;
	}
	fileName += ".png";
	QFile file(fileName);
	if(file.exists())
	{
		QString message = "File exists:\n";
		message += fileName;
		message += "\nDo you want to overwrite the file ?";
		if(QMessageBox::question(this, "Image file exists", message) != QMessageBox::Yes)
		{
			return false;
		}
	}
	QPixmap pix = QPixmap::grabWidget(this);
	pix.save(fileName, "PNG");
	return true;
}


void HistoryDialog::fileSave(void)
{
	if(!pTable->rowCount())
	{
		return;
	}
	emit saveRequest(id);
}

void HistoryDialog::fileSaveMulti(void)
{
	if(!pTable->rowCount())
	{
		return;
	}
	emit saveMultiRequest(id);
}

void HistoryDialog::fileLoad(void)
{
	emit loadRequest(id);
}


void HistoryDialog::closePress(void)
{
	deleteLater();
}
void HistoryDialog::helpPress(void)
{
}

void HistoryDialog::tableMousePress(int row, int /* col */)
{
	const char *dpName = pImage->getDpName(row);
	if(!dpName)
	{
		return;
	}
	// Emit signal for DP selection
	selectionRow = row;
	QPoint screenPoint = QCursor::pos();
	emit dpMouseDown(1, DataEnum::Online, screenPoint.x(), screenPoint.y(), 0, dpName);
// qDebug("HistoryDialog::tableMousePress(row %d)\n", row);
}

void HistoryDialog::tableContextMenuRequest(const QPoint &point)
{
	QTableWidgetItem *pItem = pTable->itemAt(point);
	if(!pItem)
	{
		return;
	}
	int row = pItem->row();
	QMenu *pMenu = new QMenu(pTable);
	QString caption(pTable->item(row, 2)->text());
	pMenu->addAction(caption);
	pMenu->addSeparator();
	pMenu->addAction("Remove from history", this, SLOT(removeItem()));
	menuRow = row;
	pMenu->exec(QCursor::pos());
	delete pMenu;
}

void HistoryDialog::tableDoubleClick(int row, int col)
{
	QPoint screenPoint = QCursor::pos();
	const char *dpName = pImage->getDpName(row);
	if(!dpName)
	{
		return;
	}
	// Emit signal for DP selection
	selectionRow = row;
	emit dpMouseDown(1, DataEnum::Online, screenPoint.x(), screenPoint.y(), 0, dpName);

	// For 1s tmouse button in colored column - show color selection
	if(col != 1)
	{
		return;
	}
	QTableWidgetItem *pItem = pTable->item(row, col);
	QColor color(pItem->background().color());
	QColor newColor = QColorDialog::getColor(color, this);
	if(newColor.isValid())
	{
		if(newColor != color)
		{
			pItem->setBackground(newColor);
			pTable->update();
			pImage->setItemColor(row, newColor);
		}
	}
}

void HistoryDialog::removeItem(void)
{
	const Eqp *pEqp = pImage->removeItem(menuRow);
	pTable->removeRow(menuRow);
	if(pEqp)
	{
		if(pImage->getEqpIndex(pEqp) < 0)	// The very last instance
		{
			disconnect(pEqp, SIGNAL(selectChanged(Eqp *)), this, SLOT(eqpSelectChanged(Eqp *)));
		}
	}
	pImage->rebuildImage(22);
}

void HistoryDialog::eqpSelectChanged(Eqp *pEqp)
{
// qDebug("HistoryDialog::eqpSelectChanged(%s, %d) selectionRow %d\n", pEqp->getVisibleName(), (int)pEqp->isSelected(), selectionRow);
	bool found = false;
	QList<HistoryEqpData *> &eqps = pImage->getEqps();
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		HistoryEqpData *pItem = eqps.at(idx);
		if(pItem->getEqp() == pEqp)
		{
			setSelectionInTable(idx, pEqp);
			found = true;
		}
	}
	if(pEqp->isSelected())
	{
		selectionRow = -1;
	}
	if(found)
	{
		pImage->rebuildImage(23);
	}
}

void HistoryDialog::valueAtPointerChanged(const QString &timeStr, const QString &valueStr)
{
	pTimeLabel->setText(timeStr);
	pValueLabel->setText(valueStr);
}

void HistoryDialog::imageZoomed(void)
{
	// Update state of 'online' flag
	if(pImage->isOnline() != pOnlineCheckBox->isChecked())
	{
		settingOnline = true;
		pOnlineCheckBox->setChecked(pImage->isOnline());
		settingOnline = false;
	}

	// Update spin boxes and scroll bar
	showTimeRange();
	updateScrollBar();
}

void HistoryDialog::resizeEvent(QResizeEvent *e)
{
	QDialog::resizeEvent(e);
	QRect area(rect());
//	printf("Dialog: %d %d (%d x %d)\n", area.left(), area.top(), area.width(), area.height());
	QRect rect(pRangeBox->geometry());
	pRangePb->move(rect.right() - pRangePb->width() - 4, rect.top());
	if(pFillNumberPb)
	{
		pFillNumberPb->move(rect.right() - pRangePb->width() - pFillNumberPb->width() - 8, rect.top());
	}
}

bool HistoryDialog::event(QEvent *e)
{
	if(e->type() == QEvent::WindowActivate)
	{
		instanceActivated(this);
	}
	return QDialog::event(e);
}


void HistoryDialog::rangeButtonClicked(void)
{
	HistoryTimeRangeDialog *pDialog = new HistoryTimeRangeDialog(this,
		pImage->getTimeMin(), pImage->getTimeMax(), ! pImage->isOnline());
	int maxDepth = pImage->getMaximumDepth();
	QDateTime maxInPast(QDateTime::currentDateTime().addSecs(-maxDepth));
	pDialog->setAllowedRange(maxInPast);
	connect(pDialog, SIGNAL(rangeChanged(const QDateTime &, const QDateTime &)),
		this, SLOT(rangeChange(const QDateTime &, const QDateTime &)));
	pDialog->show();
}

void HistoryDialog::rangeChange(const QDateTime &start, const QDateTime &end)
{
	pImage->setTimeLimits(start, end);
	showTimeRange();
	updateScrollBar();
}

void HistoryDialog::fillNumberButtonClicked(void)
{
	emit fillNumberRangeRequest(id);
}

void HistoryDialog::filterTypeComboActivated(int index)
{
	if(index == pImage->getFilterType())
	{
		return;
	}
	pImage->setFilterType((HistoryDataRange::FilterType)index);
}
