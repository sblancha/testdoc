// FileParser.cpp: implementation of the FileParser class.
//
//////////////////////////////////////////////////////////////////////

#include "FileParser.h"

#include <locale.h>
#include <string.h>
#include <ctype.h>

#include <QString>
#include <QStringList>

// Separators used to split text string to individual tokens
static const char separators[] = "\n\r\t =";

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

FileParser::FileParser()
{
	mode = ParseNormal;
	tokens = NULL;
	maxTokenSize = NULL;
	nTokens = maxTokens = 0;
}

FileParser::~FileParser()
{
	freeTokens();
}

/*
**
** FUNCTION
**		Returns version of specified file (DLL)
**
** PARAMETERS
**		pFileName  - Pointer to string (file name) for which version info is required
**
** RETURNS
**		Converted string
**
** CAUTIONS
**		Method returns pointer to static buffer, content of buffer will be overwritten
**		by next method call
*/
const char *FileParser::fileVersion(const char * /* pFileName */)
{
#ifdef Q_OS_WIN
	/* L.Kopylov 29.01.2010 on Windows
	static char result[128];
	char	*realFileName = FileParser::pathToSystem(pFileName);
	DWORD	handle;
	DWORD	versionInfoSize = GetFileVersionInfoSize(realFileName, &handle);
	if(!versionInfoSize)
	{
		free((void *)realFileName);
		DWORD errCode = GetLastError();
		if(errCode == ERROR_FILE_NOT_FOUND)
		{
			return "File not found";
		}
		return "No version info";
	}

	void	*pVersionInfo = _alloca(versionInfoSize);
	BOOL success = GetFileVersionInfo(realFileName, handle, versionInfoSize, pVersionInfo);
	free((void *)realFileName);
	if(!success)
	{
		return "Failed to get version";
	}

	VS_FIXEDFILEINFO *pFixedInfo;
	unsigned fixedSize;
	if(!VerQueryValue(pVersionInfo, "\\", (void **)&pFixedInfo, &fixedSize))
	{
		return "Version query failed";
	}
	if(fixedSize < sizeof(VS_FIXEDFILEINFO))
	{
		return "Invalid size by version query";
	}
	sprintf(result, "%d,%d,%ld",
		(int)(((pFixedInfo->dwFileVersionMS & 0xFFFF0000) >> 16) & 0xFFFF),
		(int)(pFixedInfo->dwFileVersionMS & 0xFFFF),
		pFixedInfo->dwFileVersionLS ;
	return result;
	*/
	return "Not Yet Implemented";
#else
	return "Not Yet Implemented";
#endif
}

/*
**
** FUNCTION
**		Convert pathname to system-style path:
**		replace all '/' by '\' - for Windows;
**		replace all '\' by '/' - for Unix
**
** PARAMETERS
**		pFileName  - Pointer to string (file name) to be converted
**
** RETURNS
**		Converted string
**
** CAUTIONS
**		It is responsability of caller to free memory allcoated for returned string
**		using free()
*/
char *FileParser::pathToSystem(const char *pFileName)
{
	char		*result, *dst;
	const char	*src;

	result = (char *)malloc(strlen(pFileName) + 1);
	for(dst = result, src = pFileName ; *src != '\0' ; src++, dst++)
	{
		#ifdef Q_OS_WIN
		*dst = *src == '/' ? '\\' : *src;
		#else
		*dst = *src == '\\' ? '/' : *src;
		#endif
	}
	*dst = '\0';
	return result;
}

/*
** FUNCTION
**		Read and parse information from text file.
**
** PARAMETERS
**		pFileName	Pointer to name of text file.
**		errList		Variable where error messages will be
**					written in case of error.
**
** RETURNS
**		The value returned by ProcessLineTokens().
**
** CAUTIONS
**		None
*/
int FileParser::parseFile(const char *pFileName, QStringList &errList)
{
	FILE	*pFile = NULL;

	char *realFileName = pathToSystem(pFileName);
#ifdef Q_OS_WIN
	if(fopen_s(&pFile, pFileName, "r"))
#else
	if(!(pFile = fopen(pFileName, "r")))
#endif
	{
		QString errMsg;
		errMsg = "Failed to open file <";
		errMsg += realFileName;
		errMsg += ">";
		free((void *)realFileName);
		errList.append(errMsg);
		return -1;
	}
	free((void *)realFileName);
	int lineNo = 0;
	int coco = parseFile(pFile, lineNo, errList);
	fclose(pFile);
	return coco;
}

/*
** FUNCTION
**		Read and parse information from text file.
**
** PARAMETERS
**		pFile		Pointer to text file opened for reading.
**		lineNo		Currently processed line number in input file.
**		errList		Variable where error messages will be
**					written in case of error.
**
** RETURNS
**		The value returned by ProcessLineTokens().
**
** CAUTIONS
**		None
*/
int FileParser::parseFile(FILE *pFile, int &lineNo, QStringList &errList)
{
	int		coco = 0;

	setlocale(LC_ALL, "english-us");
	while(nextFileLine(pFile, lineNo))
	{
		if((coco = processLineTokens(pFile, lineNo, tokens, nTokens, attrLongValue, errList)) <= 0)
		{
			break;
		}
	}
	freeTokens();
	return coco;
}

/*
** FUNCTION
**		Read file line by line, split every line to tokens.
**		Skip empty lines and lines with comment (recognized by
**		first '#' or '!' character).
**		When line which is neither empty nor comment has been
**		found - return tokens of line.
**
** PARAMETERS
**		pFile		Pointer to text file opened for reading.
**		lineNo		Currently processed line number in input file.
**
** RETURNS
**		true if there were tokens read from file;
**		false if there is no more usefulf lines in file.
**
** CAUTIONS
**		None
*/
bool FileParser::nextFileLine(FILE *pFile, int &lineNo)
{
	char	buf[1024];

	while(fgets(buf, 1023, pFile))
	{
		lineNo++;
		if (!splitLine(buf))
		{
			continue;
		}
		if((tokens[0][0] == '!') || (tokens[0][0] == '#'))
		{
			continue;
		}
		return true;
	}
	return false;
}

/*
**	FUNCTION
**		Split text string to tokens.
**
**	PARAMETERS
**		pText		Pointer to NULL-terminated string to be splitted.
**
**	RETURNS
**		true if there were tokens read from file;
**		false if there is no more usefulf lines in file.
**
**	CAUTIONS
**		None.
*/
bool FileParser::splitLine(char *pText)
{
	char	buf[2048];
	bool	coco;
	switch(mode)
	{
	case ParseResources:
		coco = splitLineResource(pText);
		break;
	default:
#ifdef Q_OS_WIN
		strcpy_s(buf, sizeof(buf) / sizeof(buf[0]), pText);
#else
		strcpy(buf, pText);
#endif
		attrLongValue = "";
		splitLineResource(buf);
		if(nTokens > 1)
		{
			attrLongValue = QString::fromLatin1(tokens[1]);
		}
		coco = splitLineNormal(pText);
		break;
	}
	return coco;
}

/*
**	FUNCTION
**		Split text string to individual tokens - used for 'Normal' mode
**
**	PARAMETERS
**		pText		Pointer to NULL-terminated string to be splitted.
**
**	RETURNS
**		true if there were tokens read from file;
**		false if there is no more usefulf lines in file.
**
**	CAUTIONS
**		None.
*/
bool FileParser::splitLineNormal(char *pText)
{
	nTokens = 0;
#ifdef Q_OS_WIN
	char *nextToken = NULL;
	char *token = strtok_s(pText, separators, &nextToken);
#else
	char *token = strtok(pText, separators);
#endif
	while(token)
	{
		addToken(token);
#ifdef Q_OS_WIN
		token = strtok_s(NULL, separators, &nextToken);
#else
		token = strtok(NULL, separators);
#endif
	}
	return nTokens ? true : false;
}

/*
**	FUNCTION
**		Split text string to individual tokens - used for 'Resources' mode.
**		Every line is split to at most two parts:
**		1) up to 1st '=' character
**		2) the rest of string
**
**	PARAMETERS
**		pText		Pointer to NULL-terminated string to be splitted.
**
**	RETURNS
**		true if there were tokens read from file;
**		false if there is no more usefulf lines in file.
**
**	CAUTIONS
**		None.
*/
bool FileParser::splitLineResource(char *pText)
{
	nTokens = 0;
	pText = trim(pText);
	if(!*pText)
	{
		return false;
	}
	if((pText[0] == '!') || (pText[0] == '#'))
	{
		return false;
	}
	char *sep = strchr(pText, '=');
	if(!sep)
	{
		addToken(pText);
		return true;
	}
	// Truncate input line at location of '=' character
	*sep = '\0';
	addToken(trim(pText));

	// Trim name - everything before first '='
	char *name = trim(++sep);
	if(!*name)
	{
		return false;
	}
	addToken(name);
	return true;
}

void FileParser::addToken(const char *token)
{
	if(nTokens >= maxTokens)
	{
		tokens = (char **)realloc((void *)tokens, (maxTokens + 1) * sizeof(char *));
		tokens[maxTokens] = NULL;
		maxTokenSize = (short *)realloc((void *)maxTokenSize, (maxTokens + 1) * sizeof(short));
		maxTokenSize[maxTokens++] = 0;
	}
	int tokenLen = (int)strlen(token) + 1;
	if(maxTokenSize[nTokens] < tokenLen)
	{
		tokenLen += 16;
		tokens[nTokens] = (char *)realloc((void *)tokens[nTokens], tokenLen * sizeof(char));
		maxTokenSize[nTokens] = tokenLen;
	}
#ifdef Q_OS_WIN
	strcpy_s(tokens[nTokens++], tokenLen, token);
#else
	strcpy(tokens[nTokens++], token);
#endif
}

/*
**
**	FUNCTION
**		Remove space characters at the beginning and end of 0-terminated string.
**
**	PARAMETERS
**		inName	- Pointer to 0-terminated string
**
**	RETURNS
**		Pointer to trimmed string
**
**	CAUTIONS
**		Content of input string may be destroyed
*/
char *FileParser::trim(char *inName)
{
	int	strLen = (int)strlen(inName);
	switch(strLen)
	{
	case 0:	// Empty line
		return inName;
	case 1:	// Just 1 character - further algorithm will not work
		if(isspace(*inName))
		{
			*inName = '\0';
		}
		return inName;
	}

	char	*c;
	// Remove spaces at the end of line
	for(c = &inName[strLen-1] ; c ; c--)
	{
		if(!isspace(*c))
		{
			break;
		}
		*c = '\0';
		if(c == inName)
		{
			break;
		}
	}
	if(c == inName)
	{
		return c;
	}

	// Remove spaces at the beginning of line
	for(c = inName ; *c ; c++)
	{
		if(!isspace(*c))
		{
			break;
		}
	}
	return c;
}

/*
** FUNCTION
**		Free memory previously allocated for tokens resulting from split of
**		single text line.
**
** PARAMETERS
**		None
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void FileParser::freeTokens(void)
{
	for(short n = maxTokens - 1 ; n >= 0 ; n--)
	{
		if(tokens[n])
		{
			free( (void *)tokens[n] );
		}
	}
	if(tokens)
	{
		free( (void *)tokens );
	}
	tokens = NULL;
	if(maxTokenSize)
	{
		free( (void *)maxTokenSize );
	}
	maxTokenSize = NULL;
	nTokens = maxTokens = 0;
}

/*
** FUNCTION
**		Get number of tokens currently in memory
**
** PARAMETERS
**		None
**
** RETURNS
**		int - number of tokens 
**
** CAUTIONS
**		None
*/
int FileParser::getNumberOfTokens(){
	return nTokens;
}

/*
** FUNCTION
**		Get a copy of a token
**
** PARAMETERS
**		int - index of token
**
** RETURNS
**		QString - token content
**
** CAUTIONS
**		None
*/
QString FileParser::getCurrentToken(int index)
{
	if (index >= 0 && index < this->nTokens)
	{
		QString currentToken = QString(tokens[index]);
		return currentToken;
	}
	else
	{
		return Q_NULLPTR;
	}
		
}