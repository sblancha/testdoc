//	Implementation of BakeoutCtlTable class
////////////////////////////////////////////////////////////////////////////////

#include "BakeoutCtlTable.h"

#include "VacMainView.h"

#include "PlatformDef.h"
#include "ResourcePool.h"

#include <QHeaderView>

float	BakeoutCtlTable::colWidthCoeff[6];
bool	BakeoutCtlTable::colWidthReady = false;

// Column indices
#define	COL_CHANNEL		(0)
#define	COL_T			(1)
#define	COL_WARNING_TH	(2)
#define	COL_ERROR_TH	(3)
#define	COL_REG_CHANNEL	(4)
/*
#define	COL_PCS			(5)
#define	COL_SAF			(6)
*/
#define	COL_STATUS		(5)

BakeoutCtlTable::BakeoutCtlTable(QWidget *parent) : QTableWidget(parent)
{
	if(VacMainView::getInstance())
	{
		setFont(VacMainView::getInstance()->font());
	}

	// Table initialization
	setColumnCount(6);
	QString colTitle;
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.CC.Table.ColCH.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "Ctl CH";
	}
	setHorizontalHeaderItem(COL_CHANNEL, new QTableWidgetItem(colTitle));
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.CC.Table.ColT.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "T";
	}
	setHorizontalHeaderItem(COL_T, new QTableWidgetItem(colTitle));
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.CC.Table.ColWarn.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "Warn";
	}
	setHorizontalHeaderItem(COL_WARNING_TH, new QTableWidgetItem(colTitle));
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.CC.Table.ColErr.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "Err";
	}
	setHorizontalHeaderItem(COL_ERROR_TH, new QTableWidgetItem(colTitle));
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.CC.Table.ColRC.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "Reg. CH";
	}
	setHorizontalHeaderItem(COL_REG_CHANNEL, new QTableWidgetItem(colTitle));
	/*
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.CC.Table.ColSAF.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "SAF";
	}
	setHorizontalHeaderItem(COL_SAF, new QTableWidgetItem(colTitle));
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.CC.Table.ColPCS.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "PCS";
	}
	setHorizontalHeaderItem(COL_PCS, new QTableWidgetItem(colTitle));
	*/
	if(ResourcePool::getInstance().getStringValue("VBAKEOUT.CC.Table.ColStatus.Title", colTitle) != ResourcePool::OK)
	{
		colTitle = "Status";
	}
	setHorizontalHeaderItem(COL_STATUS, new QTableWidgetItem(colTitle));
	horizontalHeader()->show();
	verticalHeader()->hide();

	// Some reasonable default height
	QRect rect = fontMetrics().boundingRect("Channel");
	//setMinimumSize(QSize(rect.width() * 8, rect.height() * 8));
	recommendedSize.setWidth(rect.width() * 8);
	recommendedSize.setHeight(rect.height() * 12);
}

BakeoutCtlTable::~BakeoutCtlTable()
{
	while(!channels.isEmpty())
	{
		delete channels.takeFirst();
	}
}

void BakeoutCtlTable::resizeEvent(QResizeEvent *pEvent)
{
	QTableWidget::resizeEvent(pEvent);

	if(!colWidthReady)
	{
		int colWidth[8];
		int wd;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.CC.Table.ColCH.Width", wd) != ResourcePool::OK)
		{
			wd = 100;
		}
		colWidth[COL_CHANNEL] = wd > 0 ? wd : 100;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.CC.Table.ColT.Width", wd) != ResourcePool::OK)
		{
			wd = 100;
		}
		colWidth[COL_T] = wd > 0 ? wd : 100;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.CC.Table.ColWarn.Width", wd) != ResourcePool::OK)
		{
			wd = 100;
		}
		colWidth[COL_WARNING_TH] = wd > 0 ? wd : 100;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.CC.Table.ColErr.Width", wd) != ResourcePool::OK)
		{
			wd = 100;
		}
		colWidth[COL_ERROR_TH] = wd > 0 ? wd : 100;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.CC.Table.ColRC.Width", wd) != ResourcePool::OK)
		{
			wd = 100;
		}
		colWidth[COL_REG_CHANNEL] = wd > 0 ? wd : 100;
		/*
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.CC.Table.ColSAF.Width", wd) != ResourcePool::OK)
		{
			wd = 90;
		}
		colWidth[COL_SAF] = wd > 0 ? wd : 90;
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.CC.Table.ColPCS.Width", wd) != ResourcePool::OK)
		{
			wd = 90;
		}
		colWidth[COL_PCS] = wd > 0 ? wd : 90;
		*/
		if(ResourcePool::getInstance().getIntValue("VBAKEOUT.CC.Table.ColStatus.Width", wd) != ResourcePool::OK)
		{
			wd = 500;
		}
		colWidth[COL_STATUS] = wd > 0 ? wd : 500;
		float total = 0;
		for(int col = 0 ; col < 6 ; col++)
		{
			total += colWidth[col];
		}
		for(int col = 0 ; col < 6 ; col++)
		{
			colWidthCoeff[col] = (float)colWidth[col] / total;
		}
		colWidthReady = true;
	}
	int totalWidth = width();
	int wd = (int)rint(totalWidth * colWidthCoeff[COL_CHANNEL]);
	setColumnWidth(COL_CHANNEL, wd);
	wd = (int)rint(totalWidth * colWidthCoeff[COL_T]);
	setColumnWidth(COL_T, wd);
	wd = (int)rint(totalWidth * colWidthCoeff[COL_WARNING_TH]);
	setColumnWidth(COL_WARNING_TH, wd);
	wd = (int)rint(totalWidth * colWidthCoeff[COL_ERROR_TH]);
	setColumnWidth(COL_ERROR_TH, wd);
	wd = (int)rint(totalWidth * colWidthCoeff[COL_REG_CHANNEL]);
	setColumnWidth(COL_REG_CHANNEL, wd);
	/*
	wd = (int)rint(totalWidth * colWidthCoeff[COL_SAF]);
	setColumnWidth(COL_SAF, wd);
	wd = (int)rint(totalWidth * colWidthCoeff[COL_PCS]);
	setColumnWidth(COL_PCS, wd);
	*/
	wd = (int)rint(totalWidth * colWidthCoeff[COL_STATUS]);
	setColumnWidth(COL_STATUS, wd);
}

/*
**	FUNCTION
**		Set device to be shown in view
**
**	PARAMETERS
**		pEqp	- Pointer to device to be shown
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutCtlTable::setEqp(Eqp *pEqp)
{
	if(pEqp == this->pEqp)
	{
		return;
	}
	while(!channels.isEmpty())
	{
		delete channels.takeFirst();
	}
	setRowCount(0);
	this->pEqp = pEqp;
	if(pEqp)
	{
		setRowCount(8);
		for(int n = 1 ; n <= 8 ; n++)
		{
			setItem(n - 1, COL_CHANNEL, new QTableWidgetItem(QString::number(n + 24)));	// Sebastien's request
			item(n - 1, COL_CHANNEL)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			setItem(n - 1, COL_T, new QTableWidgetItem());
			item(n - 1, COL_T)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			setItem(n - 1, COL_WARNING_TH, new QTableWidgetItem());
			item(n - 1, COL_WARNING_TH)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			setItem(n - 1, COL_ERROR_TH, new QTableWidgetItem());
			item(n - 1, COL_ERROR_TH)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			setItem(n - 1, COL_REG_CHANNEL, new QTableWidgetItem());
			item(n - 1, COL_REG_CHANNEL)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			/*
			setItem(n - 1, COL_SAF, new QTableWidgetItem());
			item(n - 1, COL_SAF)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			setItem(n - 1, COL_PCS, new QTableWidgetItem());
			item(n - 1, COL_PCS)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			*/
			setItem(n - 1, COL_STATUS, new QTableWidgetItem());
			item(n - 1, COL_STATUS)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
	
			BakeoutCtlChannel *pChannel = new BakeoutCtlChannel(pEqp, n);
			channels.append(pChannel);
			connect(pChannel, SIGNAL(eqpDataChanged(BakeoutCtlChannel *, bool)),
				this, SLOT(eqpDataChange(BakeoutCtlChannel *, bool)));
			eqpDataChange(pChannel, false);
		}
		resizeRowsToContents();
	}
}

void BakeoutCtlTable::eqpDataChange(BakeoutCtlChannel *pSrc, bool /* colorChanged */)
{
	int row = pSrc->getChannel() - 1;
	QTableWidgetItem *pItem = item(row, COL_T);
	QColor bgColor = Qt::white;
	pItem->setBackground(bgColor);
	pItem->setText(QString::number(pSrc->getValue()));
	QModelIndex startIdx = indexFromItem(pItem);

	pItem = item(row, COL_WARNING_TH);
	int value = pSrc->getWarningThreshold();
	if(value)
	{
		bgColor = Qt::white;
	}
	else
	{
		bgColor.setRgb(196, 196, 196);	// Grey
	}
	pItem->setBackground(bgColor);
	pItem->setText(value ? QString::number(value) : "");
	
	pItem = item(row, COL_ERROR_TH);
	value = pSrc->getErrorThreshold();
	if(value)
	{
		bgColor = Qt::white;
	}
	else
	{
		bgColor.setRgb(196, 196, 196);	// Grey
	}
	pItem->setBackground(bgColor);
	pItem->setText(value ? QString::number(value) : "");

	pItem = item(row, COL_REG_CHANNEL);
	value = pSrc->getRegChannel();
	if(value)
	{
		bgColor = Qt::white;
	}
	else
	{
		bgColor.setRgb(196, 196, 196);	// Grey
	}
	pItem->setBackground(bgColor);
	pItem->setText(value ? QString::number(value) : "");

	/*
	int status = pSrc->getStatus();
	pItem = item(row, COL_SAF);
	pItem->setText(status & SAF_BIT ? "ON" : "");

	pItem = item(row, COL_PCS);
	pItem->setText(status & PCS_BIT ? "ON" : "");
	*/

	pItem = item(row, COL_STATUS);
	pItem->setBackground(pSrc->getColor());
	pItem->setText(pSrc->getShortStateString());

	QModelIndex endIdx = indexFromItem(pItem);
	dataChanged(startIdx, endIdx);	// Force table update
}

