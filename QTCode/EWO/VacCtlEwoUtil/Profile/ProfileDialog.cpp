//	Implementation of ProfileDialog class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileDialog.h"
#include "ProfileImage.h"

#include "VacMainView.h"
#include "Sector.h"
#include "Eqp.h"
#include "DataPool.h"
#include "ResourcePool.h"

#include <qpushbutton.h>
#include <qmenu.h>
#include <qlabel.h>
#include <qcombobox.h>
#include <qlayout.h>
#include <qdesktopwidget.h>
#include <qapplication.h>

// Base names for resources used by profile
#define RESOURCE_TYPES_BASE	"Profile.BeamVacuumEqpTypes"
#define RESOURCE_MIN_BASE 	"Profile.BeamVacuumScaleMin"
#define RESOURCE_MAX_BASE	"Profile.BeamVacuumScaleMax"

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

ProfileDialog::ProfileDialog(Sector *pFirstSector, Sector *pLastSector, DataEnum::DataMode mode) :
	QDialog(NULL /* VacMainView::getInstance() */, Qt::Window)
{
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle("Pressure Profile");
	this->pFirstSector = pFirstSector;
	this->pLastSector = pLastSector;
	this->mode = mode;
	pView = NULL;
	beamVacLimitsChanged = false;

	/*
	const QList<VacMainView *> &mainList = VacMainView::getInstanceList();
	for(int idx = 0 ; idx < mainList.count() ; idx++)
	{
		VacMainView *pInstance = mainList.at(idx);
		if(pInstance->isPvssEvents())
		{
			QObject::connect(this, SIGNAL(profileDpDown(int, int, int, int, int, const char *)),
				pInstance, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
		}
	}
	*/
	VacMainView *pMainView = VacMainView::getInstance();
	if(pMainView)
	{
		QObject::connect(this, SIGNAL(profileDpDown(int, int, int, int, int, const char *)),
			pMainView, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
	}

	buildLayout();
	setTitle();
	buildInitialEqpMask();
	buildView(true);
	if(mode == DataEnum::Replay)
	{
		VacMainView::replayDialogOpened(this);
	}
}

ProfileDialog::~ProfileDialog()
{
	if(mode == DataEnum::Replay)
	{
		VacMainView::replayDialogDeleted(this);
	}
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileDialog::buildLayout(void)
{
	// 1) Main layout - menu, labels and combobox on top, scrolled view on bottom
	pMainBox = new QVBoxLayout(this);
	pMainBox->setContentsMargins(0, 0, 0, 0);
	pMainBox->setSpacing(0);

	// 2) Horizontal layout with
	//	- buttons for activating popup menus
	//	- title label
	//	- ComboBox with main part selection
	QHBoxLayout *pTopBox = new QHBoxLayout();
	pTopBox->setSpacing(0);
	pMainBox->addLayout(pTopBox);

	// 2.1 all popup menus
	pFilePb = new QPushButton("&File", this);
	pFilePb->setFlat(true);
	QSize size = pFilePb->fontMetrics().size(Qt::TextSingleLine, "File");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pFilePb->setFixedSize(size);
	buildFileMenu();
	pFilePb->setMenu(pFileMenu);
	pTopBox->addWidget(pFilePb);

	pViewPb = new QPushButton("&View", this);
	pViewPb->setFlat(true);
	size = pViewPb->fontMetrics().size(Qt::TextSingleLine, "View");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pViewPb->setFixedSize(size);
	buildViewMenu();
	pViewPb->setMenu(pViewMenu);
	pTopBox->addWidget(pViewPb);

	pHelpPb = new QPushButton("&Help", this);
	pHelpPb->setFlat(true);
	size = pHelpPb->fontMetrics().size(Qt::TextSingleLine, "Help");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pHelpPb->setFixedSize(size);
	buildHelpMenu();
	pHelpPb->setMenu(pHelpMenu);
	pTopBox->addWidget(pHelpPb);

	
	// 2.2 Profile title
	pTitleLabel = new QLabel("Profile", this);
	pTopBox->addWidget(pTitleLabel, 10);	// The only resizable widget on top of dialog
	pTitleLabel->setAlignment(Qt::AlignHCenter);

	// 2.3 Combo box for main part selection
	buildMainPartCombo();
	pTopBox->addWidget(pCombo);
}

/*
**	FUNCTION
**		Build 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileDialog::buildFileMenu(void)
{
	pFileMenu = new QMenu(this);
	pFileMenu->addAction("Print...", this, SLOT(filePrint()));
	pFileMenu->addSeparator();
	pFileMenu->addAction("Close", this, SLOT(deleteLater()));
}

/*
**	FUNCTION
**		Build 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileDialog::buildViewMenu(void)
{
	// First build mask containing all possible types for profile
	allEqpMask.append(FunctionalType::VGM);
	allEqpMask.append(FunctionalType::VGR);
	allEqpMask.append(FunctionalType::VGP);
	allEqpMask.append(FunctionalType::VGI);
	allEqpMask.append(FunctionalType::VGF);
	allEqpMask.append(FunctionalType::VPI);
	allEqpMask.append(FunctionalType::VPGMPR);
	allEqpMask.append(FunctionalType::VGTR);
	/* Use std. VGR/VGP/VPI
	allEqpMask.append(FunctionalType::VGR_PS_CMW);
	allEqpMask.append(FunctionalType::VGP_PS_CMW);
	allEqpMask.append(FunctionalType::VPI_PS_CMW);
	*/

	pViewMenu = new QMenu(this);
	pEqpSubMenu = pViewMenu->addMenu("Equipment");
	const QList<FunctionalType *> &types = allEqpMask.getList();
	for(int idx = 0 ; idx < types.count() ; idx++)
	{
		FunctionalType *pItem = types.at(idx);
		QAction *pAction = pEqpSubMenu->addAction(pItem->getDescription());
		pAction->setData(pItem->getType());
		pAction->setCheckable(true);
		pAction->setChecked(eqpMask.contains(pItem->getType()));
		typeActions.append(pAction);
	}
	connect(pEqpSubMenu, SIGNAL(triggered(QAction *)), this, SLOT(eqpTypeMenu(QAction *)));

	// Item to show/hide sector names
	pViewMenu->addSeparator();
	pSectNamesAction = pViewMenu->addAction("Sector Names");
	pSectNamesAction->setData(111);
	pSectNamesAction->setCheckable(true);
	ResourcePool &pool = ResourcePool::getInstance();
	QString resourceName = "Profile.ShowSectorNames";
	bool flag = true;
	pool.getBoolValue(resourceName, flag);
	pSectNamesAction->setChecked(flag);
	connect(pSectNamesAction, SIGNAL(toggled(bool)), this, SLOT(sectNamesToggled(bool)));

	// Items to open other views
	pViewMenu->addSeparator();
	pViewMenu->addAction("Sector...", this, SLOT(viewSector()));
	pViewMenu->addAction("Synoptic...", this, SLOT(viewSynoptic()));
}

/*
**	FUNCTION
**		Build 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileDialog::buildHelpMenu(void)
{
	pHelpMenu = new QMenu(this);
	pHelpMenu->addAction("User manual...", this, SLOT(help()));
}

/*
**	FUNCTION
**		Build main part combo box, insert main part names which can be selected
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileDialog::buildMainPartCombo(void)
{
	pCombo = new QComboBox(this);
	pCombo->setEditable(false);
	pCombo->clear();
	DataPool &pool = DataPool::getInstance();
	QStringList mpList;
	pool.findMainPartNames(mpList, true);
	QList<Sector *> &sectList = pool.getSectors();
	foreach(QString mpName, mpList)
	{
		int nInnerSectors = 0, nOuterSectors = 0;
		MainPart *pMainPart = pool.findMainPartData(mpName.toLatin1());
		unsigned vacType = pMainPart->getVacTypeMask();
		if(!VacType::isSingleBeam(vacType))
		{
			continue;
		}
		for(int idx = 0 ; idx < sectList.count() ; idx++)
		{
			Sector *pSector = sectList.at(idx);
			if(pSector->isInMainPart(pMainPart))
			{
				if(pSector->isOuter())
				{
					nOuterSectors++;
				}
				else
				{
					nInnerSectors++;
				}
			}
		}
		if(nInnerSectors)
		{
			pCombo->addItem(mpName);
		}
	}
	selectInitialMp();
	connect(pCombo, SIGNAL(activated(const QString &)),
		this, SLOT(comboActivated(const QString &)));
}

/*
**	FUNCTION
**		Build initial equipment type mask
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileDialog::buildInitialEqpMask(void)
{
	eqpMask = allEqpMask;
	ResourcePool &pool = ResourcePool::getInstance();
	QString resourceName = RESOURCE_TYPES_BASE;
	resourceName += ".";
	resourceName += pCombo->currentText();
	if(pool.getEqpTypesMask(resourceName, eqpMask) == ResourcePool::NotFound)
	{
		resourceName = RESOURCE_TYPES_BASE;
		eqpMask = allEqpMask;
		if(pool.getEqpTypesMask(resourceName, eqpMask) == ResourcePool::NotFound)
		{
			eqpMask = allEqpMask;
		}
	}

	// Set items checked in menu by default
	const QList<FunctionalType *> &types = allEqpMask.getList();
	for(int idx = 0 ; idx < types.count() ; idx++)
	{
		FunctionalType *pItem = types.at(idx);
		for(int actIdx = typeActions.count() - 1 ; actIdx >= 0 ; actIdx--)
		{
			QAction *pAction = typeActions.at(actIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				pAction->setChecked(eqpMask.contains(pItem->getType()));
				break;
			}
		}
	}
}

/*
**	FUNCTION
**		Set initial selection in main part combo box
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void ProfileDialog::selectInitialMp(void)
{
	DataPool &pool = DataPool::getInstance();
	int nMps = pCombo->count();
	int *mps = (int *)calloc(nMps, sizeof(int));
	Sector *pSector = pFirstSector;
	do
	{
		for(int n = 0 ; n < nMps ; n++)
		{
			MainPart *pMainPart = pool.findMainPartData(pCombo->itemText(n).toLatin1());
			if(pSector->isInMainPart(pMainPart))
			{
				mps[n]++;
			}
		}
		if(pSector == pLastSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);

	int bestIdx = -1, bestWeight = 0;
	for(int n = 0 ; n < nMps ; n++)
	{
		if(mps[n] > bestWeight)
		{
			bestWeight = mps[n];
			bestIdx = n;
		}
	}
	free((void *)mps);
	pCombo->setCurrentIndex(bestIdx);
}

/*
**	FUNCTION
**		Set text and color for title label widget
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileDialog::setTitle(void)
{
	if(mode == DataEnum::Replay)
	{
		QPalette palette = pTitleLabel->palette();
		palette.setColor(QPalette::Window, Qt::cyan);
		pTitleLabel->setPalette(palette);
		pTitleLabel->setAutoFillBackground(true);
	}
	QString title = "Profile, sector";
	if(pFirstSector == pLastSector)
	{
		title += " ";
		title += pFirstSector->getName();
	}
	else
	{
		title += "s ";
		title += pFirstSector->getName();
		title += " ... ";
		title += pLastSector->getName();
	}
	pTitleLabel->setText(title);
}

/*
**	FUNCTION
**		Build profile view according to current selection
**
**	ARGUMENTS
**		init	- Flag indicating if method is called from constructor
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileDialog::buildView(bool init)
{
	if(pView)
	{
		delete pView;
		pView = NULL;
	}
	// Create instance of profile view
	QString errMsg;
	pView = ProfileImage::create(this, pFirstSector, pLastSector, mode, allEqpMask, errMsg);
	if(!pView)
	{
		// TODO: show error message
		return;
	}
	pMainBox->addWidget(pView, 10);	// Image will consume all free space

	// Analyze equipment types in view, set menu accordingly
	unsigned vacMask;
	VacEqpTypeMask beamMask;
	pView->analyzeTypes(vacMask, beamMask);
	const QList<FunctionalType *> &types = allEqpMask.getList();
	for(int idx = 0 ; idx < types.count() ; idx++)
	{
		FunctionalType *pItem = types.at(idx);
		for(int actIdx = 0 ; actIdx < typeActions.count() ; actIdx++)
		{
			QAction *pAction = typeActions.at(actIdx);
			if(pAction->data().toInt() == pItem->getType())
			{
				pAction->setEnabled(beamMask.contains(pItem->getType()));
				if(!beamMask.contains(pItem->getType()))
				{
					pAction->setChecked(false);
				}
				break;
			}
		}
	}

	connect(pView, SIGNAL(limitsChanged(int, int)),
		this, SLOT(limitsChange(int, int)));

	// Set limits for new profile
	int min = -10, max = -6;
	if(beamVacLimitsChanged)
	{
		min = beamVacMin;
		max = beamVacMax;
	}
	else
	{
		ResourcePool &pool = ResourcePool::getInstance();
		QString resourceName = RESOURCE_MIN_BASE;
		resourceName += ".";
		resourceName += pCombo->currentText();
		if(pool.getIntValue(resourceName, min) == ResourcePool::NotFound)
		{
			resourceName = RESOURCE_MIN_BASE;
			if(pool.getIntValue(resourceName, min) == ResourcePool::NotFound)
			{
				min = -10;
			}
		}
		resourceName = RESOURCE_MAX_BASE;
		resourceName += ".";
		resourceName += pCombo->currentText();
		if(pool.getIntValue(resourceName, max) == ResourcePool::NotFound)
		{
			resourceName = RESOURCE_MAX_BASE;
			if(pool.getIntValue(resourceName, max) == ResourcePool::NotFound)
			{
				max = -6;
			}
		}
	}
	pView->setLimits(min, max);
	pView->setShowSectorNames(pSectNamesAction->isChecked());
	
	// Build content of profile view with current selection mask
	pView->applyMask(eqpMask);
	pView->show();
	QObject::connect(pView, SIGNAL(dpMouseDown(int, int, int, int, const char *)),
		this, SLOT(dpMouseDown(int, int, int, int, const char *)));

	// Set own size
	if(init)
	{
		QDesktopWidget *pDesktop = QApplication::desktop();
		const QRect &screen = pDesktop->screenGeometry(pDesktop->screenNumber(this));
		resize(screen.width() - 100, screen.height() / 4);
	}
}

/*
**	FUNCTION
**		Slot activated when 'Print' item is selected in 'File' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileDialog::filePrint(void)
{
	VacMainView::printWidget(this);
}

/*
**	FUNCTION
**		Slot activated when 'Sector' item is selected in 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileDialog::viewSector(void)
{
	makeDialogRequest(VacMainView::DialogSector);
}

/*
**	FUNCTION
**		Slot activated when 'Synoptic' item is selected in 'View' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileDialog::viewSynoptic(void)
{
	makeDialogRequest(VacMainView::DialogSynoptic);
}

/*
**	FUNCTION
**		Make request to main view to show another dialog for data
**		shown in this dialog
**
**	ARGUMENTS
**		type	- Type of dialog to be opened, see enum in VacMainView class
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileDialog::makeDialogRequest(int type)
{
	QStringList sectorList;
	unsigned vacType = 0;
	DataPool &pool = DataPool::getInstance();
	Sector *pSector = pFirstSector;
	do
	{
		vacType |= pSector->getVacType();
		sectorList.append(pSector->getName());
		if(pSector == pLastSector)
		{
			break;
		}
		pSector = pool.findNeighbourSector(pSector, NULL, 1);
	} while(pSector);
	VacMainView *pMainView = VacMainView::getInstance();
	pMainView->dialogRequest(type, vacType, sectorList, mode);
}

/*
**	FUNCTION
**		Slot activated when 'User manual' item is selected in 'Help' menu
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileDialog::help(void)
{
}

void ProfileDialog::sectNamesToggled(bool checked)
{
	//qDebug("Action from show sectors menu, checked = %d\n", checked);
	pView->setShowSectorNames(checked);
}

/*
**	FUNCTION
**		Slot activated when one of equipment type items is selected
**		in 'Equipment' submenu of 'View' menu
**
**	ARGUMENTS
**		id	- Functional type corresponding to selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileDialog::eqpTypeMenu(QAction *pAction)
{
	if(!pAction->data().isValid())
	{
		return;
	}
	int id = pAction->data().toInt();
	if(eqpMask.contains(id))
	{
		eqpMask.remove(id);
		pAction->setChecked(false);
	}
	else
	{
		eqpMask.append(id);
		pAction->setChecked(true);
	}
	pView->applyMask(eqpMask);
}

/*
**	FUNCTION
**		Slot activated when item is selected in main part combo box
**
**	ARGUMENTS
**		name	- name of selected item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileDialog::comboActivated(const QString &name)
{
	DataPool &pool = DataPool::getInstance();
	QStringList sectList;
	pool.findSectorsInMainPart(name.toLatin1(), sectList, true);
	if(!sectList.count())
	{
		selectInitialMp();
		return;
	}
	Sector *pNewStart = pool.findSectorData(sectList.first().toLatin1()),
			*pNewEnd = pool.findSectorData(sectList.last().toLatin1());
	if((!pNewStart) || (!pNewEnd))
	{
		selectInitialMp();
		return;
	}
	pFirstSector = pNewStart;
	pLastSector = pNewEnd;
	buildView(false);
	setTitle();
}

/*
**	FUNCTION
**		Slot activated when limits for beam vacuum have been changed in profile.
**		Save limits for future reuse.
**
**	ARGUMENTS
**		min	- New axis minimum
**		max	- New axis maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileDialog::limitsChange(int min, int max)
{
	beamVacMin = min;
	beamVacMax = max;
	beamVacLimitsChanged = true;
}

/*
**	FUNCTION
**		Slot activated when mouse pointer was pressed on one of devices in profile
**
**	ARGUMENTS
**		button	- Mouse button number
**		mode	- Data acquisition mode
**		x		- Mouse X-coordinate in screen coordinate system
**		y		- Mouse Y-coordinate in screen coordinate system
**		dpName	- Name of DP under mouse pointer
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileDialog::dpMouseDown(int button, int mode, int x, int y, const char *dpName)
{
	emit profileDpDown(button, mode, x, y, 0, dpName);
}
