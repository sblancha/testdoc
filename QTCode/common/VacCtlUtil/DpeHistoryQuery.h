#ifndef DPEHISTORYQUERY_H
#define DPEHISTORYQUERY_H

#include "VacCtlUtilExport.h"

// Class buffering DPE history requests to be sent to PVSS

#include <qobject.h>
#include <qlist.h>
#include <qmutex.h>
#include <qdatetime.h>

class DpeHistoryQueryItem
{
public:
	DpeHistoryQueryItem(int id, const char *dpeIn, bool bitState, int bitNbr, const QDateTime &startTime, const QDateTime &endTime)
		: dpe(dpeIn), start(startTime), end(endTime)
	{
		this->id = id;
		this->bitState = bitState;
		this->bitNbr = bitNbr;
	}
	~DpeHistoryQueryItem() {}

	// Access
	inline const QByteArray &getDpe(void) const { return dpe; }
	inline bool isBitState(void) const { return bitState; }
	inline int getBitNbr(void) const { return bitNbr; }
	inline int getId(void) const { return id; }
	inline const QDateTime &getStart(void) const { return start; }
	inline const QDateTime &getEnd(void) const { return end; }

protected:
	// DPE name
	QByteArray	dpe;

	// Start time of history query time range
	QDateTime	start;

	// End time of history query time range
	QDateTime	end;

	// Internal identifier of data range in history
	int			id;

	// Bit number for bit state history
	int			bitNbr;

	// Flag indicating if this is request for bit state history
	bool			bitState;
};


class VACCTLUTIL_EXPORT DpeHistoryQuery : public QObject
{
	Q_OBJECT

public:
	static DpeHistoryQuery &getInstance(void);

	void add(int id, const char *dpe, bool bitState, int bitNbr, const QDateTime &start, const QDateTime &end);
	void remove(int id, const char *dpe, bool bitState, int bitNbr, const QDateTime &start, const QDateTime &end);
	void clear(void);
	static int getPendingCount(void);

	bool getNext(int &id, QByteArray &dpe, bool &bitState, int &bitNbr, QDateTime &start, QDateTime &end);

	DpeHistoryQuery();
	DpeHistoryQuery(const DpeHistoryQuery &source);
	virtual ~DpeHistoryQuery();
	DpeHistoryQuery & operator = (const DpeHistoryQuery &other);

signals:
	void changed(void);

protected:

	// The only instance of this class
	static DpeHistoryQuery instance;

	// List of pending requests
	QList<DpeHistoryQueryItem *>	*pItems;

	// Mutex to protect list from parallel access
	QMutex	mutex;

	// Flag indicating if signal on content change has been sent
	bool	signalSent;
};

#endif	// DPEHISTORYQUERY_H
