#include "RackIconVRLRI.h"

#include "DataPool.h"

#include "qpainter.h"

RackIconVRLRI::RackIconVRLRI(QWidget *parent, Qt::WindowFlags f) :
RackIcon(parent, f)
{
	pEqp = NULL;
	//QWidget::setAttribute(Qt::WA_TransparentForMouseEvents);
	//setFixedSize(21, 21); see about this later
}

RackIconVRLRI::~RackIconVRLRI()
{
	disconnect();
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRLRI::disconnect(void)
{
	bool result = false;
	if (connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	
	connected = false;
	
	return result;
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool RackIconVRLRI::connect(void)
{
	if (connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	//dpes.append("PR");
	connection.connect(pEqp, dpes, DataEnum::Online);
	connected = true;
	return true;


}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void RackIconVRLRI::setEqp(Eqp *pEqp)
{
	disconnect();
	DataPool &pool = DataPool::getInstance();
	if (pEqp != NULL)
	{
		//Q_ASSERT_X(pEqp->inherits("EqpVRPI"), "RackIconVRPIT::setEqp", pEqp->getDpName());
		//this->pEqp = (EqpVR_GT *)pEqp;
	}
	forceRedraw();
}

void RackIconVRLRI::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.scale(scale, scale);
	painter.setBrush(Qt::white);

	QFont calibri("Calibri", 12, QFont::Bold);
	QFont calibriSmall("Calibri", 8);
	int textFlags;
	painter.setFont(calibri);

	QPen selectionPen = QPen(Qt::red);
	selectionPen.setWidth(3);	
	if (isSelected)
	{
		painter.setPen(selectionPen);
	}
	else
	{
		painter.setPen(Qt::black);
	}
	QRect equipmentRect(0, 0, this->sizeX, this->sizeY);
	//painter.setPen(Qt::black);
	painter.drawRect(equipmentRect);
	painter.setPen(Qt::black);

	

	textFlags = Qt::AlignCenter | Qt::TextWrapAnywhere;
	painter.drawText(equipmentRect.left(), equipmentRect.top(), equipmentRect.width(), equipmentRect.height() - 7, textFlags, this->equipmentName);

	//write dp information
	painter.setFont(calibriSmall);
	QString attributeValue = attributes.value("ProfibusAddress");
	painter.drawText(equipmentRect.left(), equipmentRect.top() + 45, equipmentRect.width(), 10, textFlags, "DP address: " + attributeValue);
	
}


