//	Implementation of ProfileLhcSingleBeamView class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileLhcSingleBeamView.h"

#include "ProfileLhcSingleBeamLine.h"

#include "DataPool.h"

/*
**	FUNCTION
**		Create new instance of LHC profile view
**
**	PARAMETERS
**		parent			- Pointer to parent widget
**		vacType			- Vacuum type for this view
**		pMainPart		- Pointer to main part of this view
**		pFirstSector	- Pointer to first sector
**		pLsstSector		- Pointer to last sector
**
**	RETURNS
**		Pointer to new instance of profile view,
**		NULL in case of error.
**
**	CAUTIONS
**		None
*/
ProfileLhcSingleBeamView *ProfileLhcSingleBeamView::create(QWidget *parent, int vacType, MainPart *pMainPart,
	Sector *pFirstSector, Sector *pLastSector)
{
	float	start = pFirstSector->getStart(),
			end = pLastSector->getEnd();
	if(start == end)
	{
		return NULL;
	}

	// Build new instance
	ProfileLhcSingleBeamView *pView = new ProfileLhcSingleBeamView(parent);
	pView->pStartSector = pFirstSector;
	pView->pEndSector = pLastSector;
	pView->start = start;
	pView->end = end;
	pView->mode = DataEnum::Polling;
	pView->pMainPart = pMainPart;
	pView->vacTypeMask = vacType;

	return pView;
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction


ProfileLhcSingleBeamView::ProfileLhcSingleBeamView(QWidget *parent, Qt::WindowFlags f) :
	ProfileLhcView(parent, f)
{
	vacTypeMask = VacType::None;
	setMinimumSize(400, 150);
}

ProfileLhcSingleBeamView::~ProfileLhcSingleBeamView()
{
}

/*
**	FUNCTION
**		Initialize all lines in view
**
**	PARAMETERS
**		beamMask	- Mask containing all allowed eqp. types for beam vacuum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileLhcSingleBeamView::init(const VacEqpTypeMask &beamMask)
{
	allowedBeamEqpMask = beamMask;
	lines.append(new ProfileLhcSingleBeamLine(this, vacTypeMask, pMainPart));

	// Finally...
	for(int idx = 0 ; idx < lines.count() ; idx++)
	{
		ProfileLhcLine *pLine = lines.at(idx);
		pLine->setStart(start);
		pLine->setEnd(end);
		pLine->build(allowedBeamEqpMask);
		connect(pLine, SIGNAL(eqpDataChanged()), this, SLOT(eqpDataChange()));
		connect(pLine, SIGNAL(eqpSelectChanged(bool)), this, SLOT(eqpSelectChange(bool)));
	}
	rebuildImage();
}

