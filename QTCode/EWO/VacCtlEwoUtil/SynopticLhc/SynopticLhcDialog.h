#ifndef	SYNOPTICLHCDIALOG_H
#define	SYNOPTICLHCDIALOG_H

// Dialog for displaying LHC synoptic view

#include "DataEnum.h"
#include "VacEqpTypeMask.h"
#include "LhcRingSelection.h"
#include "EqpMsgCriteria.h"

#include <QDialog>

class Sector;
class SynLhcView;

#include <QPushButton>
#include <QMenu>
#include <QLabel>
#include <QComboBox>
#include <QScrollArea>

class SynopticLhcDialog : public QDialog, public LhcRingSelection
{
	Q_OBJECT

public:
	SynopticLhcDialog(unsigned vacTypes, Sector *pFirstSector, Sector *pLastSector, DataEnum::DataMode mode);
	~SynopticLhcDialog();

	static void makeDeviceVisible(Eqp *pEqp);

public slots:
	void eqpActiveChange(Eqp * pEqp, DataEnum::DataMode mode);

protected:
	// Last created instance - used for 'ensure eqp visible' functionality
	static	SynopticLhcDialog *pLastInstance;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Button for displaying 'File' menu
	QPushButton	*pFilePb;

	// Button for displaying 'View' menu
	QPushButton	*pViewPb;

	// Button for displaying 'Help' menu
	QPushButton	*pHelpPb;

	// 'File' menu
	QMenu	*pFileMenu;

	// 'View' menu
	QMenu	*pViewMenu;

	// 'Vacuum type' submenu of 'View' menu
	QMenu	*pVacSubMenu;

	// 'Equipment on isolation vacuum' submenu of 'View' menu
	QMenu	*pCryoEqpSubMenu;

	// 'Equipment on beam vacuum' submenu of 'View' menu
	QMenu	*pBeamEqpSubMenu;

	// 'Help' menu
	QMenu	*pHelpMenu;

	// Label with synoptic title
	QLabel		*pTitleLabel;

	// Combo box for main part selection
	QComboBox	*pCombo;

	// Scroll view holding synoptic image and icons
	QScrollArea	*pScroll;

	// Synoptic view instance providing real synoptic drawing
	SynLhcView	*pView;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Data to be shown in dialog //////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Vacuum type mask
	unsigned		vacTypeMask;

	// Equipment type mask for all possible equipment types on isolation vacuum
	VacEqpTypeMask	allCryoEqpMask;

	// Equipment type mask for all possible equipment types on beam vacuum
	VacEqpTypeMask	allBeamEqpMask;

	// Equipment type mask on isolation vacuum
	VacEqpTypeMask	cryoEqpMask;

	// Equipment type mask on beam vacuum
	VacEqpTypeMask	beamEqpMask;

	// Pointer to first sector to be shown
	Sector			*pFirstSector;

	// Pointer to last sector to be shown
	Sector			*pLastSector;

	// Data acquisition mode
	DataEnum::DataMode		mode;

	// Actions of vacuum types menu
	QList<QAction *>	vacTypeActions;

	// Actions of CRYO equipment types menu
	QList<QAction *>	cryoTypeActions;

	// Actions of BEAM equipment types menu
	QList<QAction *>	beamTypeActions;

	void buildInitialEqpMask(void);

	void buildLayout(void);
	void buildFileMenu(void);
	void buildViewMenu(void);
	void buildHelpMenu(void);
	void buildMainPartCombo(void);
	void selectInitialMp(void);

	void setTitle(void);
	void buildView(bool init);
	void setMyWidth(bool init);

	void makeDialogRequest(int type);
	void setTypesMenu(void);

	void ensureDeviceVisible(Eqp *pEqp);

	virtual void resizeEvent(QResizeEvent *pEvent);

private slots:
	void filePrint(void);

	void typeMenu(QAction *pAction);
	void vacTypeMenu(QAction *pAction);
	void cryoEqpTypeMenu(QAction *pAction);
	void beamEqpTypeMenu(QAction *pAction);
	void viewSector(void);
	void viewProfile(void);
	void help(void);
	void comboActivated(const QString &name);

	void viewMove(int x);

	void mobileStateChange(void);
	void showMobilesNC(void);
	void hideMobilesNC(void);
};

#endif	// SYNOPTICLHCDIALOG_H
