#ifndef	MOBILETREEITEMTYPES_H
#define	MOBILETREEITEMTYPES_H

// Definition of mobile tree item types

typedef enum
{
	MobileTreeItemMpSector = 10000,
	MobileTreeItemEqp = 10001,
	MobileTreeItemBakeoutRack = 10002,
	MobileTreeItemBakeoutChannel = 10003
} MobileTreeItemType;

#endif	// MOBILETREEITEMTYPES_H
