#include "InterlockIconVRPIT.h"

#include "qpainter.h"

InterlockIconVRPIT::InterlockIconVRPIT(QWidget * parent) :
InterlockIcon(parent)
{
	

}

InterlockIconVRPIT::~InterlockIconVRPIT()
{

}

void InterlockIconVRPIT::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	QFont calibri("Calibri", 10, QFont::Bold);
	QFont calibriBig("Calibri", 12);
	//draw title
	QPen whitePen(Qt::white);
	QBrush blackBrush(Qt::black);
	
	
	painter.setBrush(blackBrush);
	painter.setPen(whitePen);
	painter.setFont(calibri);

	painter.drawRect(0,0, 100, 50);
	painter.drawText(15, 0, 70, 50, Qt::AlignCenter | Qt::TextWrapAnywhere, this->name);
	
	painter.setPen(Qt::black);
	painter.setBrush(Qt::white);

	painter.drawRect(0, 55, 100, 150);
	painter.drawLine(0, 115, 100, 115);
	painter.drawLine(0, 160, 100, 160);

	painter.drawText(5, 55, 95, 30, Qt::AlignLeft | Qt::AlignVCenter, "ET200: " + attributes.value("ET200", "-"));
	painter.drawText(5, 85, 95, 30, Qt::AlignLeft | Qt::AlignVCenter, "MCA8:" + attributes.value("MCA8", "-"));
	painter.drawText(5, 115, 95, 45, Qt::AlignLeft | Qt::AlignVCenter, "SVA3:" + attributes.value("SVA3", "-"));
	painter.drawText(5, 160, 95, 45, Qt::AlignLeft | Qt::AlignVCenter, "Level 1: " + attributes.value("Level1", "-") + "\n(mv)");

}

void InterlockIconVRPIT::moveIcon(int x, int y)
{
	this->move(x - 100 / 2, y);
}

QPoint InterlockIconVRPIT::getConnectionPoint(QString connectorName)
{
	int baseX = this->x() + 100;
	int baseY = this->y() + 182;

	return QPoint(baseX, baseY);
}

void InterlockIconVRPIT::getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber)
{
	direction = InterlockIconConnection::Right;
	offset = 25;
	writeCableNumber = false;
}

QRect InterlockIconVRPIT::getBoundingRect()
{
	QPoint topLeft = QPoint(this->x(), this->y());
	QRect boundingRect(topLeft, topLeft + QPoint(100, 255));

	return boundingRect;
}