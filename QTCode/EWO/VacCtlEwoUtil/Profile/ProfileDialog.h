#ifndef	PROFILEDIALOG_H
#define	PROFILEDIALOG_H


//	Pressure profile dialog - version for non-LHC lines

#include "DataEnum.h"
#include "VacEqpTypeMask.h"
#include "EqpMsgCriteria.h"
#include "ProfileImage.h"

#include <QDialog>

class Sector;

#include <QVBoxLayout>
#include <QPushButton>
#include <QMenu>
#include <QLabel>
#include <QComboBox>

class ProfileDialog : public QDialog
{
	Q_OBJECT

public:
	ProfileDialog(Sector *pFirstSector, Sector *pLastSector, DataEnum::DataMode mode);
	~ProfileDialog();

signals:
	void profileDpDown(int button, int mode, int x, int y, int dummy, const char *dpName);

protected:
	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Box layout where profile image is added
	QVBoxLayout		*pMainBox;

	// Button for displaying 'File' menu
	QPushButton		*pFilePb;

	// Button for displaying 'View' menu
	QPushButton		*pViewPb;

	// Button for displaying 'Help' menu
	QPushButton		*pHelpPb;

	// 'File' menu
	QMenu		*pFileMenu;

	// 'View' menu
	QMenu		*pViewMenu;

	// 'Equipment' submenu of 'View' menu
	QMenu		*pEqpSubMenu;

	// Actions in pEqpSubMenu
	QList<QAction *>	typeActions;

	// Action to show/hide sector names
	QAction				*pSectNamesAction;

	// 'Help' menu
	QMenu		*pHelpMenu;

	// Label with synoptic title
	QLabel			*pTitleLabel;

	// Combo box for main part selection
	QComboBox		*pCombo;

	// Profile image instance providing real profile drawing
	ProfileImage	*pView;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Data to be shown in dialog //////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Equipment type mask for all possible equipment types
	VacEqpTypeMask	allEqpMask;

	// Equipment type mask
	VacEqpTypeMask	eqpMask;

	// Pointer to first sector to be shown
	Sector			*pFirstSector;

	// Pointer to last sector to be shown
	Sector			*pLastSector;

	// Data acquisition mode
	DataEnum::DataMode	mode;

	// Limits for beam vacuum profile + flag indicating limits were changed
	int			beamVacMin;
	int			beamVacMax;
	bool		beamVacLimitsChanged;


	void buildInitialEqpMask(void);

	void buildLayout(void);
	void buildFileMenu(void);
	void buildViewMenu(void);
	void buildHelpMenu(void);
	void buildMainPartCombo(void);
	void selectInitialMp(void);

	void setTitle(void);
	void buildView(bool init);

	void makeDialogRequest(int type);

private slots:
	void filePrint(void);

	void eqpTypeMenu(QAction *pAction);
	void sectNamesToggled(bool checked);
	void viewSector(void);
	void viewSynoptic(void);
	void help(void);
	void comboActivated(const QString &name);

	void dpMouseDown(int button, int mode, int x, int y, const char *dpName);

	void limitsChange(int min, int max);

};

#endif	// PROFILEDIALOG_H
