#ifndef DEVLISTCRITERIAEDITOR_H
#define DEVLISTCRITERIAEDITOR_H

// EWO interface for DevListCriteriaEditor widget

#include <BaseExternWidget.hxx>

#include "DevListSelectorWidget.h"
#include "EqpMsgCriteria.h"

class EWO_EXPORT DevListCriteriaEditor : public BaseExternWidget
{
	Q_OBJECT

public:
	DevListCriteriaEditor(QWidget *parent);
	~DevListCriteriaEditor();
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

	// Access
	inline int getMode(void) const { return baseWidget->getMode(); }
	inline void setMode(int mode) { baseWidget->setMode(mode); }

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void criteriaChange(void);
	void scopeChange(void);
	void addEqpRequest(void);
	void modeChanged(int mode);
	void removeSelected(void);
	void removeAll(void);
	void dataModeChanged(void);

private:
	DevListSelectorWidget	*baseWidget;
};

#endif	// DEVLISTCRITERIAEDITOR_H
