#include "ValveInterlockSource.h"

ValveInterlockSource::ValveInterlockSource(Eqp *pSourceEqp, bool isPreviousInterlock)
{
	pEqp = pSourceEqp;
	previous = isPreviousInterlock;
}
ValveInterlockSource::ValveInterlockSource(const QString &dpName, Eqp*pMasterEqp, bool isPreviousInterlock)
{
	pEqp = pMasterEqp;
	this->dpName = dpName;
	previous = isPreviousInterlock;
}

ValveInterlockSource::~ValveInterlockSource(void)
{
}

void ValveInterlockSource::getData(bool &before, Eqp **ppEqp, QString &dpName)
{
	before = previous;
	*ppEqp = pEqp;
	dpName = this->dpName;
}
