
#uses "unSystemIntegrity.ctl"		// Load CTRL library
#uses "vclImportEquipment.ctl" // Load CTRL library

global mapping gDpsMapping;



// mapping to be returned as a file parcing result
// gDpsMapping[dpName] = dpes;
// dpes[dpeName] = dpeDetails;

// dyn_anytype contains records for every affected Dpe(s)
// dpeDetails[1]  = sourceDpeName
// dpeDetails[2]  = functionExpression
// dpeDetails[3]  = plcHealthControl( true/false)
// dpeDetails[4]   = DO NOT write(true/false)

int     lineNum = 0;





// ReadDpFunctionConfigFile
/**
Purpose:
Read configuration file, generated by database export, to get list
of DP functions attributes

Parameters:
	- None

Return:
	- mapping of dpFunctions attributes. The description of this compound mapping is located at the top of file

Usage: Internal

PVSS manager usage: CTRL
Read config file
Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void ReadDpFunctionConfigFile(dyn_string &exceptionInfo)
{
  // file parsing
  string fileName = DATA_PATH + "/" + glAccelerator + "_DpFunc.config";
  if(access(fileName, F_OK) != 0)
  {
    // File does not exist - not an error? L.Kopylov 08.05.2015 fwException_raise(exceptionInfo, "ERROR", "vclDpFunction.ctl: file <" + fileName + "> does not exist, exiting...", "");
    return;
  }
  file fp = fopen(fileName, "r");
  if(fp == 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "vclDpFunction.ctl: failed to open file <" + fileName + "> for reading, exiting...", "");
    return;
  }
  string  		line;
  string      currentDp = "";
  mapping     dpes;
  int         count = 0;
  while(fgets(line, 512, fp) != 0)
  {
    lineNum++;
    dyn_string dpString = strsplit(line, "=");
    if( dynlen(dpString) !=2)
    {
      continue;
    }
    // first item (mandatory) - DPe name, 2nd (mandatory) - value
    string keyWord = strltrim(strrtrim(dpString[1]));
    if(keyWord == "Dpe")
    {
      string fullDpeName = strltrim(strrtrim(dpString[2]));
      string dpName = GetDpName(fullDpeName);
      string dpeName = GetDpeName(fullDpeName);

      if(dpName == "")
      {
        DebugTN("vclDpFunction.ctl: line " + lineNum + ", incorrect DPE <" + fullDpeName + "> from config file");
        continue;
      }
      if((dynlen(dpString)) > 2)
      {
        DebugTN("vclDpFunction.ctl: line " + lineNum + ", config file - bad consistency");
        continue;
      }
      if(currentDp != dpName)
      {        
        // new DP
        mappingClear(dpes); 
        gDpsMapping[dpName] = dpes; 
        currentDp = dpName;
        count++;
      }
      ProcessDpeData(fp, dpeName, gDpsMapping[dpName]);
    }
  }
  fclose(fp);
}
// dpeDetails[1]  = sourceDpeName
// dpeDetails[2]  = functionExpression
// dpeDetails[3]  = plcHealthControl( true/false)
// dpeDetails[4]  = DO NOT write(true/false)
private void ProcessDpeData(file &fp, string dpe, mapping &dpes)
{
  dyn_anytype dpeAttributes = makeDynAnytype("", "", false, true); 
  string  		line;
  while(fgets(line, 512, fp) != 0)
  {
    string keyWord;
    keyWord = strltrim(strrtrim(line));
    lineNum++;
    if(keyWord == "") // empty string
    {
      continue;
    }
    else if(keyWord == "EndDpe")
    {
      dpes[dpe] = dpeAttributes;
      return;
    }
    int firstEqual = strtok(line , "=");
    string keyWord = strltrim(strrtrim(substr(line, 0, firstEqual)));
    string value = strltrim(strrtrim(substr(line, firstEqual + 1))); 
    if((keyWord == "") || (value == "") )
    {
      DebugTN("vclDpFunction.ctl: line " + lineNum + ", config file - bad dpe consistency");
      continue;
    }
    int index = 0;
    if(keyWord == "Source")
    {
      index = 1; 
    }
    else if(keyWord == "PlcHealthControl")
    {
      index = 3; 
    }
    else if(keyWord == "Function")
    {
      index = 2; 
    }
    else
    {
      DebugTN("vclDpFunction.ctl: line " + lineNum + " Incorrect keyword");
      continue;
    }
    dpeAttributes[index] = value;
  } 
}
void GetDpFunctionList(mapping &dpFunctionList)
{
  dpFunctionList =  gDpsMapping;  
}

// dpSubStr(dpName, DPSUB_DP) is no used here because this function checks whether this Dp exists in system
// if Dp doesn't exist above mentionrd fuction reurn ""
private string GetDpName(string fullDpeName)
{
//  return(dpSubStr(dpeName, DPSUB_DP)); 
  dyn_string dpString = strsplit(fullDpeName, ".");
  if( dynlen(dpString) !=2)
  {
      return "";
  }
  else
  {
    return dpString[1];
  }
}
// dpSubStr(dpName, DPSUB_DP) is no used here because this function checks whether this Dp exists in system
// if Dp doesn't exist above mentionrd fuction reurn ""
private string GetDpeName(string fullDpeName)
{   
//   string dpElement = strltrim(dpSubStr (dpeName, DPSUB_DP_EL), dpSubStr (dpeName, DPSUB_DP));
//   return(dpElement);
  dyn_string dpString = strsplit(fullDpeName, ".");
  if( dynlen(dpString) !=2)
  {
      return "";
  }
  else
  {
    return "." + dpString[2];
  }
}
void ProcessDpFunctionsListForChanges()
{
  dyn_anytype  keyList = mappingKeys(gDpsMapping);
  int keyLen = dynlen(keyList);
  for(int n = 1; n <= keyLen; n++)
  {
    CheckDpFunctionForChanges(keyList[n]);
  }
}
bool CheckDpFunctionForChanges(string dpName)    
{
  if( !mappingHasKey(gDpsMapping, dpName))
  {
    return false;
  }
  dyn_anytype  dpeList = mappingKeys(gDpsMapping[dpName]);
  bool         hasToWrite = false;  
  int keyLen = dynlen(dpeList);
  for(int n = 1; n <= keyLen; n++)
  {
    if(!dpExists(dpName))
    {
      DebugN("DP does not exist ", dpName);
      // gDpsMapping[dpName][dpeList[n]][4] = true;
      hasToWrite = true;
      continue;
    }
    else // check descreapance between old and actual dpFunction expression
    {
      dyn_string params;  
      string     dpFunction, plcDp;
      if(gDpsMapping[dpName][dpeList[n]][3])
      {
        plcDp = GetPlcDp(dpName);
      }
      else
      {
        plcDp = "";
      }

      // check number and value of parameters and DpFunction expression
      dyn_string params;
      string     function;
      
      dpGet(dpName + dpeList[n] + ":_dp_fct.._param", params,
        dpName + dpeList[n] + ":_dp_fct.._fct", function);
      for(int i = 1; i<=dynlen(params); i++) // remove system name
      {
        params[i] = dpSubStr(params[i], DPSUB_DP_EL_CONF_DET_ATT);
      }
      dyn_string newParamList;
      string     newFunction;
      ProduceDpFunctionAttributes(dpName, plcDp, gDpsMapping[dpName][dpeList[n]], newFunction, newParamList);
      if(dynlen(newParamList) != dynlen(params))
      {
        DebugN("Different number of params", dpName, dpeList[n]);
        // gDpsMapping[dpName][dpeList[n]][4] = true;
        hasToWrite = true;
        continue;
      }
      for(int i = 1; i<=dynlen(newParamList); i++)
      {
        if(params[i] != newParamList[i])
        {
          DebugN("Param " + newParamList[i] + " mismatch", dpName, dpeList[n], params[i]);
          // gDpsMapping[dpName][dpeList[n]][4] = true;
          hasToWrite = true;
          continue;
        }
      }
      if(newFunction != function)
      {
        DebugN("Function differs", dpName, dpeList[n], newFunction, function);
        // gDpsMapping[dpName][dpeList[n]][4] = true;
        hasToWrite = true;
        continue;
      }     
      gDpsMapping[dpName][dpeList[n]][4] = false;
    }
  }
  return hasToWrite;
}
private string GetPlcDp(string dpName)
{
  dyn_string exceptionInfo;
  string alarmDp = "", plcDp;
  LhcVacDevicePlc(dpName, plcDp, alarmDp, exceptionInfo);
  return alarmDp; 
}
void UpdateDpFunctionList(dyn_string  &exceptionInfo)
{
  dyn_anytype  keyList = mappingKeys(gDpsMapping);
  int keyLen = dynlen(keyList);
  for(int n = 1; n <= keyLen; n++)
  {
    SetDpFunctionAttributes(keyList[n], exceptionInfo);
  }   
}
// SetDpFunctionAttributes
/**
Purpose:
Set Dp Function attributes

Parameters:
	dpName - name of dp
	exceptionInfo - the error will be returned here
Return:
	- none

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void SetDpFunctionAttributes(string dpName, dyn_string  &exceptionInfo)
{
  if( !mappingHasKey(gDpsMapping, dpName))
  {
    return;
  }
  string      plcDp = "";
  dyn_string  params;
  if(!dpExists(dpName))
  {
    dynAppend(exceptionInfo, "Dp <" + dpName + "> doesn't exist");
    return;
  }
  dyn_anytype  keyList = mappingKeys(gDpsMapping[dpName]);
  int keyLen = dynlen(keyList);
  for(int n = 1; n <= keyLen; n++)
  {  
    if(!gDpsMapping[dpName][keyList[n]][4])
    {
      continue;
    }
    if(gDpsMapping[dpName][keyList[n]][3])
    {
      plcDp =  GetPlcDp(dpName);
    }
    string newFunction;
    dyn_string paramList;
    ProduceDpFunctionAttributes(dpName, plcDp, gDpsMapping[dpName][keyList[n]],
      newFunction, paramList);
    dpSetWait(dpName + keyList[n] +":_dp_fct.._type", DPCONFIG_DP_FUNCTION);
    dyn_errClass err = getLastError(); 
    if(dynlen(err) > 0)
    { 
      fwException_raise(exceptionInfo, "ERROR", "Failed to set Dp function: SetDpFunctionAttributes() could not create dp function config for <" +
        dpName + keyList[n] + ">", "");
      continue;
    }
    // calculate dpFunction expression    
    dpSetWait(dpName + keyList[n] + ":_dp_fct.._param", paramList,
      dpName + keyList[n] + ":_dp_fct.._fct", newFunction);
    err = getLastError(); 
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, "ERROR", "Failed to set Dp function: SetDpFunctionAttributes() could not configure dp function for <"
        +  dpName + keyList[n] + ">", "");
    }
  }
  return;
}
// Build Dp Function and list of parameters according to PLC control Dp (if any)
// dpeDetails[1]  = sourceDpeName
// dpeDetails[2]  = functionExpression
// dpeDetails[3]  = plcHealthControl( true/false)
// dpeDetails[4]  = shall to write(true/false)
private void ProduceDpFunctionAttributes(const string dpName, const string plcDp, dyn_anytype dpFunctionAttributes,
  string &newFunction, dyn_string &paramList)
{
  if(plcDp != "")
  {
    newFunction =   "(~(p2*0xFFFFFFFFu)) & ( " + dpFunctionAttributes[2] + ")";
    paramList = makeDynString(dpFunctionAttributes[1] + ":_online.._value", dpSubStr(plcDp, DPSUB_DP) + ".alarm:_online.._value");
  }
  else
  {
    newFunction =  dpFunctionAttributes[2];
    paramList = makeDynString(dpFunctionAttributes[1] + ":_online.._value");    
  }
}
