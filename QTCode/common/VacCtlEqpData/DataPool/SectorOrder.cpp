//		Implemenation of static SectorOrder static class
/////////////////////////////////////////////////////////////////////////////////////////

#include "SectorOrder.h"
#include "DataPool.h"
#include "Eqp.h"
#include "EqpType.h"

#include "PlatformDef.h"
#include "DebugCtl.h"
#include "LhcBeamSectorOrder.h"

#include <QList>
#include <QDir>

DataPool	*SectorOrder::pPool;
QString		SectorOrder::dumpFileName;
FILE		*SectorOrder::pErrorFile = NULL;
bool		SectorOrder::keepManualOrderOnCircularLines = false;

// Function to be called by qsort() only
//static int compareLhcSectors(const void *p1, const void *p2);

/*
**
**	FUNCTION
**		Order sectors in data pool according to geography.
**
**	PARAMETERS
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**		0 - in case of success;
*		-1 - in case of error.
**
**	CAUTIONS
**		None.
**
**	CAUTIONS
**		None
*/
int SectorOrder::orderSectors(QStringList &errList)
{
	int	coco;
	if(pPool->getSectors().isEmpty())
	{
		return 0;
	}

	if(pPool->isLhcFormat())
	{
		coco = orderLhcSectors(errList);
	}
	else
	{
		coco = orderSurveySectors(errList);
	}
	if(pErrorFile)
	{
		errList.append("Geometry error(s) detected, see");
		errList.append(dumpFileName);
		fclose(pErrorFile);
		pErrorFile = NULL;
	}
	return coco;
}

/*
**
**	FUNCTION
**		Order sectors in data pool according to geography, geography data
**		came from SURVEY database.
**
**	PARAMETERS
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**		0 - in case of success;
*		-1 - in case of error.
**
**	CAUTIONS
**		None.
**
*/
int SectorOrder::orderSurveySectors(QStringList &errList)
{
	QList<BeamLine *>	&lines = pPool->getLines();
	QList<Sector *>	newSectors;

	// First process circular lines, then other lines
	BeamLine *pLine;
	foreach(pLine, lines)
	{
		if(pLine->isCircle())
		{
			addSectorsOfCircularLine(pLine, newSectors);
		}
	}
	foreach(pLine, lines)
	{
		if(!pLine->isCircle())
		{
			addSectorsOfStraightLine(pLine, newSectors);
		}
	}
	QList<Sector *> &sectors = pPool->getSectors();
	if(sectors.count() != newSectors.count())
	{
		if(newSectors.count() < sectors.count())
		{
			foreach(Sector *pSect, sectors)
			{
				bool found = false;
				foreach(Sector *pNewSect, newSectors)
				{
					if(!strcmp(pNewSect->getName(), pSect->getName()))
					{
						found = true;
						break;
					}
				}
				if(!found)
				{
					errList.append(QString("No equipment in sector <%1>").arg(pSect->getName()));
					// Make this sector anyway if there is at least one device in it
					addSectorAnyway(newSectors, pSect);
				}
			}
		}
	}
	assignNewPointers();
	// sectors = newSectors;
	pPool->replaceSectors(newSectors);

	// Build draw parts for all sectors
	buildDrawParts(errList);

	// Find 'main' beam line for main parts, i.e. beam line where most of
	// sectors for this beam line are located
	int *lineWeight = (int *)alloca(lines.count() * sizeof(int));
	QList<MainPart *> &mainParts = pPool->getMainParts();
	foreach(MainPart *pMainPart, mainParts)
	{
		memset((void *)lineWeight, 0, lines.count() * sizeof(int));
		foreach(Sector *pSect, sectors)
		{
			if(!pSect->isInMainPart(pMainPart))
			{
				continue;
			}
			QList<SectorDrawPart *> &drawParts = pSect->getDrawParts();
			foreach(SectorDrawPart *pPart, drawParts)
			{
				int lineIdx = lines.indexOf(pPart->getLine());
				if(lineIdx >= 0)
				{
					lineWeight[lineIdx]++;
				}
			}
		}
		int bestWeight = 0;
		BeamLine *pBestLine = NULL;
		for(int n = lines.count() - 1 ; n >= 0 ; n--)
		{
			if(lineWeight[n] >= bestWeight)
			{
				bestWeight = lineWeight[n];
				pBestLine = lines.at(n);
			}
		}
		pMainPart->setMainLine(pBestLine);
	}
	return 0;
}

/*
**
**	FUNCTION
**		After new list of sectors has been built - change sector reference
**		in all equipment to pointers in new list of sectors.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None.
**
**	CAUTIONS
**		None
*/
void SectorOrder::assignNewPointers(void)
{
	// Assign new sector indices to equipment
	QList<BeamLine *> &lines = pPool->getLines();
	foreach(BeamLine *pLine, lines)
	{
		QList<BeamLinePart *> &parts = pLine->getParts();
		foreach(BeamLinePart *pPart, parts)
		{
			QList<Eqp *> &eqps = pPart->getEqpList();
			foreach(Eqp *pEqp, eqps)
			{
				if(pEqp->getType() == EqpType::VacDevice)
				{
					pEqp->setNewSectors();
				}
			}
		}
	}
	QList<Eqp *> &eqpList = pPool->getCtlList();
	foreach(Eqp *pEqp, eqpList)
	{
		pEqp->setNewSectors();
	}
}

/*
**
**	FUNCTION
**		Order sectors in data pool according to geography, geography data
**		came from LHC database.
**
**	PARAMETERS
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**		0 - in case of success;
*		-1 - in case of error.
**
**	CAUTIONS
**		None.
**
**	CAUTIONS
**		None
*/
int SectorOrder::orderLhcSectors(QStringList &errList)
{
	// Let's try simple approach: order all sectors by start coordinate

	QList<Sector *>	&sectors = pPool->getSectors();
	
	
	// First build array containing sectors of LHC ring and order them
	Sector	**sectArray = (Sector **)calloc(sectors.count(), sizeof(Sector *));
	int nNewSectors = 0;

	// Set new attribute beamOrder for beam vacuum sectors on LHC ring such
	// that 1st sector of LSS1 is a first sector. L.Kopylov 22.10.2010
	assignBeamOrder(sectArray, nNewSectors);
/*
	for(Sector *pSector = sectors.first() ; pSector ; pSector = sectors.next())
	{
		switch(pSector->getVacType())
		{
		case VacType::RedBeam:
		case VacType::BlueBeam:
		case VacType::CrossBeam:
		case VacType::CommonBeam:
		case VacType::Qrl:
		case VacType::Cryo:
		// LIK 13.03.2008 case VacType::DSL:
			break;
		default:
			continue;
		}
		sectArray[nNewSectors++] = pSector->clone();
	}
	// Order new array of sectors
	qsort((void *)sectArray, nNewSectors, sizeof(Sector *), compareLhcSectors);
*/

	// Build new sectors list
	QList<Sector *> newSectors;
	for(int n = 0 ; n < nNewSectors ; n++)
	{
		newSectors.append(sectArray[n]);
	}
	free((void *)sectArray);

	// Now add sectors of jnkection/dump lines
	QList<BeamLine *> &lines = pPool->getLines();
	foreach(BeamLine *pLine, lines)
	{
		if(!pLine->isCircle())
		{
			addSectorsOfStraightLine(pLine, newSectors);
		}
	}

	if(sectors.count() != newSectors.count())
	{
		if(newSectors.count() < sectors.count())
		{
			foreach(Sector *pSect, sectors)
			{
				bool found = false;
				foreach(Sector *pNewSect, newSectors)
				{
					if(!strcmp(pNewSect->getName(), pSect->getName()))
					{
						found = true;
						break;
					}
				}
				if(!found)
				{
					errList.append(QString("No equipment in sector <%1>").arg(pSect->getName()));
					// Make this sector anyway if there is at least one device in it
					addSectorAnyway(newSectors, pSect);
				}
			}
		}
	}

	// Assign new sector indices to old sectors. Still number of new sectors may differ
	// from number of old sectors
	foreach(Sector *pNewSect, newSectors)
	{
		foreach(Sector *pSect, sectors)
		{
			if(pNewSect->getName() == pSect->getName())
			{
				pSect->setNewSector(pNewSect);
				break;
			}
		}
	}
	assignNewPointers();

	// Replace old sectors with new
	// sectors = newSectors;
	pPool->replaceSectors(newSectors);

	// Build draw parts for all sectors
	buildDrawParts(errList);

	// Find 'main' beam line for main parts, i.e. beam line where most of
	// sectors for this beam line are located
	int *lineWeight = (int *)alloca(lines.count() * sizeof(int));
	QList<MainPart *> &mainParts = pPool->getMainParts();
	foreach(MainPart *pMainPart, mainParts)
	{
		memset((void *)lineWeight, 0, lines.count() * sizeof(int));
		bool vacTypeBeam = true;
		foreach(Sector *pSect, sectors)
		{
			if(!pSect->isInMainPart(pMainPart))
			{
				continue;
			}
			if(pSect->getVacType() != VacType::Beam)
			{
				vacTypeBeam = false;
				break;	// All other sectors are on LHC ring
			}
			QList<SectorDrawPart *> &parts = pSect->getDrawParts();
			foreach(SectorDrawPart *pPart, parts)
			{
				int lineIdx = lines.indexOf(pPart->getLine());
				if(lineIdx >= 0)
				{
					lineWeight[lineIdx]++;
				}
			}
		}
		if(!vacTypeBeam)	// QRL/CRYO/Red/Blue/Cross
		{
			pMainPart->setMainLine(pPool->getCircularLine());
			continue;
		}
		// For other main parts - find the best line
		int bestWeight = 0;
		BeamLine *pBestLine = NULL;
		for(int n = lines.count() - 1 ; n >= 0 ; n--)
		{
			if(lineWeight[n] >= bestWeight)
			{
				bestWeight = lineWeight[n];
				pBestLine = lines.at(n);
			}
		}
		pMainPart->setMainLine(pBestLine);
	}

	return 0;
}

/*
**
**	FUNCTION
**		Set for all beam sectors on LHC ring attribute beamOrder
**		such that first sector of LSS1 has the minimum value,
**		further sectors have value increasing with coordinate.
**
**		New logic for ordering beam sectors is introduced after descussion
**		on 25.10.2010 (P.Gomes, G.Bregliozzi, ??, L.Kopylov).
**		The main decisions are:
**		1) sectors are ordered per LSS (main part)
**		2) ordering is done by traversing the sectors starting from IP
**			in forward and backward direction
**		3) Sector names cane be used for ordering - they are built according
**			to strict rules. Difference in DCUM is not as important as names.
**		4) For choosing between red/blue sectors at the same position:
**			in resulting sector list sector on outer beam must be before
**			sector on inner beam.
**		5) Of course, IP1.X must be in the middle of list for LSS1
**
**	PARAMETERS
**		sectArray - Array where pointers to new sectors shall be put
**		nNewSectors - variable where number of sectors added to array
**							will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None.
*/
void SectorOrder::assignBeamOrder(Sector **sectArray, int &nNewSectors)
{
	DataPool &pool = DataPool::getInstance();
	BeamLine *pLine = pool.getCircularLine();
	if(!pLine)
	{
		return;	// This shall not happen
	}
	QList<BeamLinePart *> &parts = pLine->getParts();
	if (parts.isEmpty()) {
		return;	// This shall not happen
	}
	BeamLinePart *pPart = parts.first();
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();

	// Search in backward direction until main part will be found
	int eqpIdx = nEqpPtrs - 1;
	MainPart *pMainPart = NULL;
	for( ; eqpIdx >= 0 ; eqpIdx--)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		switch(pEqp->getVacType())
		{
		case VacType::RedBeam:
		case VacType::BlueBeam:
		case VacType::CrossBeam:
		case VacType::CommonBeam:
			break;
		default:
			continue;
		}
		if(!pEqp->getSectorAfter())
		{
			continue;
		}
		const QList<MainPartMap *> &mapList = pEqp->getSectorAfter()->getMainPartMaps();
		if (!mapList.isEmpty()) {
			MainPartMap *pMap = pEqp->getSectorAfter()->getMainPartMaps().first();
			if((pMainPart = pMap->getMainPart()))
			{
				break;	// Main part found
			}
		}
	}
	if(!pMainPart)
	{
		return;	// This shall not happen
	}

	// Search further backward until sector of another main part will be found
	for( ; eqpIdx >= 0 ; eqpIdx--)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		switch(pEqp->getVacType())
		{
		case VacType::RedBeam:
		case VacType::BlueBeam:
		case VacType::CrossBeam:
		case VacType::CommonBeam:
			break;
		default:
			continue;
		}
		if(!pEqp->getSectorAfter())
		{
			continue;
		}
		const QList<MainPartMap *> &mapList = pEqp->getSectorAfter()->getMainPartMaps();
		if (!mapList.isEmpty()) {
			MainPartMap *pMap = pEqp->getSectorAfter()->getMainPartMaps().first();
			if(pMap->getMainPart() && (pMap->getMainPart() != pMainPart))
			{
				break;	// Another main part found
			}
		}
	}

	// Now move in forward direction, assign beamOrder to all beam vacuum sectors
	int startIdx = eqpIdx + 1;
	int beamOrder = 1;
	Sector *pSector = NULL;
	bool firstCheck = true;
	LhcBeamSectorOrder lhcOrder;
	while(true)
	{
		eqpIdx++;
		if(eqpIdx >= nEqpPtrs)
		{
			eqpIdx = 0;
		}
		if(firstCheck)
		{
			firstCheck = false;
		}
		else if(eqpIdx == startIdx)
		{
			break;
		}
		Eqp *pEqp = eqpPtrs[eqpIdx];
		switch(pEqp->getVacType())
		{
		case VacType::RedBeam:
		case VacType::BlueBeam:
		case VacType::CrossBeam:
		case VacType::CommonBeam:
			pSector = pEqp->getSectorAfter();
			break;
		default:
			continue;
		}

		if(!pSector)
		{
			continue;
		}
		if(!pSector->getBeamOrder())	// Not assigned yet
		{
			pSector->setBeamOrder(beamOrder++);
			lhcOrder.addSector(pSector);
/*
printf("SectorOrder::assignBeamOrder(): %d = %s\n",
	pSector->getBeamOrder(), pSector->getName());
fflush(stdout);
*/
		}
	}

	// Final ordering
	beamOrder = 1;
	while((pSector = lhcOrder.getNext()))
	{
/*
printf("SectorOrder::assignBeamOrder(): %d = %s (old %d)\n",
beamOrder, pSector->getName(), pSector->getBeamOrder());
fflush(stdout);
*/
		pSector->setBeamOrder(beamOrder++);
		sectArray[nNewSectors++] = pSector->clone();
	}
		
	// Set beam order for all isolation sectors
	for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
	{
		Eqp *pEqp = eqpPtrs[eqpIdx];
		switch(pEqp->getVacType())
		{
		case VacType::Qrl:
		case VacType::Cryo:
			pSector = pEqp->getSectorBefore();
			break;
		default:
			continue;
		}

		if(!pSector)
		{
			continue;
		}
		if(!pSector->getBeamOrder())	// Not assigned yet
		{
			pSector->setBeamOrder(beamOrder++);
			sectArray[nNewSectors++] = pSector->clone();
/*
printf("SectorOrder::assignBeamOrder(): %d = %s\n",
	pSector->getBeamOrder(), pSector->getName());
fflush(stdout);
*/
		}
	}
}

/*
**
**	FUNCTION
**		Add sectors of circular line to new array of sectors
**
**	PARAMETERS
**		pLine		- Pointer to beam line being processed
**		newSectors	- List of new sectors
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None.
*/
void SectorOrder::addSectorsOfCircularLine(BeamLine *pLine, QList<Sector *> &newSectors)
{
	// For circular line check if main part crosses first equipment of circle, if yes - start search
	// from first equipment in another main part

	QList<BeamLinePart *> &parts = pLine->getParts();
	if(parts.isEmpty()) {
		return;
	}
	BeamLinePart *pStartPart = NULL;
	int	startEqpIdx = 0;

	// Find first sector on circular line
	if(keepManualOrderOnCircularLines) {
		pStartPart = findStartOnCircularLineManualOrder(pLine, startEqpIdx);
	}
	else {
		pStartPart = findStartOnCircularLineAutoOrder(pLine, startEqpIdx);
	}
	if(!pStartPart) {
		return;	// No sectors on this line
	}

	// Process equipment of line
	char	sectName[256];
	BeamLinePart *pPart = pStartPart;
	int eqpIdx = startEqpIdx;
	Eqp	*pEqp, **eqpPtrs;
	sectName[0] = '\0';
	if(DebugCtl::isData())
	{
#ifdef Q_OS_WIN
		qDebug("SectorOrder::addSectorsOfCircularLine(%s): start from part %s eqp %d\n", pLine->getName(), pPart->getName(), eqpIdx);
#else
		printf("SectorOrder::addSectorsOfCircularLine(%s): start from part %s eqp %d\n", pLine->getName(), pPart->getName(), eqpIdx);
		fflush(stdout);
#endif
	}
	while(true)
	{
		while(true)
		{
			eqpPtrs = pPart->getEqpPtrs();
			if(eqpPtrs)
			{
				pEqp = eqpPtrs[eqpIdx];
				// If we are within sector - check if sectors of 'child' line
				// shall be added (only if they belong to the same main part)
				if(sectName[0] &&
					((pEqp->getType() == EqpType::OtherLineStart) ||
					 (pEqp->getType() == EqpType::OtherLineEnd)))
				{
					addSectorsOfChildLine(pEqp, pPart->getName(), newSectors, sectName, sizeof(sectName) / sizeof(sectName[0]));
					if(DebugCtl::isData())
					{
#ifdef Q_OS_WIN
						qDebug("SectorOrder::addSectorsOfCircularLine(%s): addSectorsOfChildLine(%s) eqp %d sect %s\n", pLine->getName(), pEqp->getName(), eqpIdx, sectName);
#else
						printf("SectorOrder::addSectorsOfCircularLine(%s): addSectorsOfChildLine(%s) eqp %d sect %s\n", pLine->getName(), pEqp->getName(), eqpIdx, sectName);
						fflush(stdout);
#endif
					}
				}
				if(pEqp->getType() == EqpType::VacDevice)
				{
					checkForNewSect(pLine, pEqp, NULL, newSectors, sectName, sizeof(sectName) / sizeof(sectName[0]));
					if(DebugCtl::isData())
					{
#ifdef Q_OS_WIN
						qDebug("SectorOrder::addSectorsOfCircularLine(%s): eqp %s (idx %d): sectName %s\n", pLine->getName(), pEqp->getName(), eqpIdx, sectName);
#else
						printf("SectorOrder::addSectorsOfCircularLine(%s): eqp %s (idx %d): sectName %s\n", pLine->getName(), pEqp->getName(), eqpIdx, sectName);
						fflush(stdout);
#endif
					}
				}
			}
			eqpIdx++;
			if(eqpIdx >= pPart->getNEqpPtrs())
			{
				break;
			}
			if((eqpIdx == startEqpIdx) && (pPart == pStartPart))
			{
				break;
			}
		}
		if((eqpIdx == startEqpIdx) && (pPart == pStartPart))
		{
			break;
		}
		int searchIdx = parts.indexOf(pPart);
		pPart = NULL;
		if(searchIdx >= 0)
		{
			searchIdx++;
			if(searchIdx < parts.count())
			{
				pPart = parts.at(searchIdx);
			}
		}
		eqpIdx = 0;
		if(!pPart)
		{
			pPart = parts.first();
			if((eqpIdx == startEqpIdx) && (pPart == pStartPart))
			{
				break;
			}
		}
	}
}


BeamLinePart *SectorOrder::findStartOnCircularLineManualOrder(BeamLine *pLine, int &startEqpIdx)
{
	Sector *pBestSector = NULL;
	const QList<BeamLinePart *> &parts = pLine->getParts();
	const QList<Sector *> &allSectors = pPool->getSectors();
	int bestSectorIdx = allSectors.count();
	BeamLinePart *pBestPart = pLine->getParts().first();
	foreach(BeamLinePart *pPart, parts) {
		Eqp **eqpPtrs = pPart->getEqpPtrs();
		int nEqpPtrs = pPart->getNEqpPtrs();
		for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++) {
			Eqp *pEqp = eqpPtrs[eqpIdx];
			if((pEqp->getType() == EqpType::VacDevice) && pEqp->getSectorBefore()) {
				int sectorIdx = allSectors.indexOf(pEqp->getSectorBefore());
				if(pBestSector) {
					if(sectorIdx < bestSectorIdx) {
						pBestSector = pEqp->getSectorBefore();
						bestSectorIdx = sectorIdx;
						pBestPart = pPart;
						startEqpIdx = eqpIdx;
					}
				}
				else {
						pBestSector = pEqp->getSectorBefore();
						bestSectorIdx = sectorIdx;
						pBestPart = pPart;
						startEqpIdx = eqpIdx;
				}
			}
		}
	}
	return pBestPart;
}

BeamLinePart *SectorOrder::findStartOnCircularLineAutoOrder(BeamLine *pLine, int &startEqpIdx)
{
	Sector *pFirstSector = NULL;
	const QList<BeamLinePart *> &parts = pLine->getParts();

	foreach(BeamLinePart *pPart, parts) {
		Eqp **eqpPtrs = pPart->getEqpPtrs();
		int nEqpPtrs = pPart->getNEqpPtrs();
		for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++) {
			Eqp *pEqp = eqpPtrs[eqpIdx];
			if((pEqp->getType() == EqpType::VacDevice) && pEqp->getSectorBefore()) {
				pFirstSector = pEqp->getSectorBefore();
				break;
			}
		}
	}
	if(!pFirstSector) {
		return NULL;	// No active equipment on this line
	}

	// Check if sector (may be another then the one found, but having at least one
	// main part in common with just found one) appears at the end of line's equipment
	BeamLinePart *pStartPart = NULL;
	Eqp **eqpPtrs;
	int nEqpPtrs, eqpIdx;
	bool	found = false, otherMpFound = false;
	for(int partIdx = parts.count() - 1 ; partIdx >= 0 ; partIdx--) {
		pStartPart = parts.at(partIdx);
		eqpPtrs = pStartPart->getEqpPtrs();
		nEqpPtrs = pStartPart->getNEqpPtrs();
		for(eqpIdx = nEqpPtrs - 1 ; eqpIdx >= 0 ; eqpIdx--) {
			Eqp *pEqp = eqpPtrs[eqpIdx];
			if(pEqp->getType() != EqpType::VacDevice) {
				continue;
			}
			if(pEqp->getSectorBefore()) {
				found = pFirstSector->hasCommonMainPart(pEqp->getSectorBefore());
				otherMpFound = ! found;
				break;
			}
			if(pEqp->getSectorAfter()) {
				found = pFirstSector->hasCommonMainPart(pEqp->getSectorAfter());
				otherMpFound = ! found;
				break;
			}
		}
		if(found || otherMpFound) {
			break;
		}
	}
	if(!found) { // First main part does not appear at the end, start from first equipment
		pStartPart = parts.first();
		startEqpIdx = 0;
	}
	else {	// Find first occurence of sector for other main part
		found = false;
		foreach(pStartPart, parts) {
			eqpPtrs = pStartPart->getEqpPtrs();
			nEqpPtrs = pStartPart->getNEqpPtrs();
			for(eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++) {
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if(pEqp->getType() != EqpType::VacDevice) {
					continue;
				}
				if(!pEqp->getSectorBefore()) {
					continue;
				}
				if(!pFirstSector->hasCommonMainPart(pEqp->getSectorBefore())) {
					found = true;
					break;
				}
			}
			if(found) {
				break;
			}
		}
	}
	if(found) {
		startEqpIdx = eqpIdx;
	}
	else {
		pStartPart = parts.first();
		startEqpIdx = 0;
	}

	return pStartPart;
}

/*
**
**	FUNCTION
**		Add sectors of straight line to new array of sectors
**
**	PARAMETERS
**		pLine       - Pointer to beam line to be processed
**		newSectors  - List of new sectors
***
**	RETURNS
**		None
**
**	CAUTIONS
**		None.
*/
void SectorOrder::addSectorsOfStraightLine(BeamLine *pLine, QList<Sector *> &newSectors)
{
	char	sectName[256];

	sectName[0] = '\0';
	QList<BeamLinePart *> &parts = pLine->getParts();
	foreach(BeamLinePart *pPart, parts)
	{
		Eqp	**eqpPtrs = pPart->getEqpPtrs();
		int	nEqpPtrs = pPart->getNEqpPtrs();
		for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
		{
			Eqp *pEqp = eqpPtrs[eqpIdx];
			// If we are within sector - check if sectors of 'child' line
			// shall be added (only if they belong to the same main part)
			if(sectName[0] &&
				((pEqp->getType() == EqpType::OtherLineStart) ||
				 (pEqp->getType() == EqpType::OtherLineEnd)))
			{
				addSectorsOfChildLine(pEqp, pPart->getName(), newSectors, sectName, sizeof(sectName) / sizeof(sectName[0]));
			}
			if(pEqp->getType() == EqpType::VacDevice)
			{
				checkForNewSect(pLine, pEqp, NULL, newSectors, sectName, sizeof(sectName) / sizeof(sectName[0]));
			}
		}
	}
}

/*
**
**	FUNCTION
**		Add vacuum sector(s) (if any) of child beam line, i.e. beam line
**		connected within given vacuum sector on parent line.
**
**	PARAMETERS
**		pStartEqp      - Pointer to equipment corresponding to connection point in parent line.
**		parentRawLine  - Name of raw line corresponding to parent beam line
**		newSectors     - List of new sectors
**		sectName       - Name of sector being processed
**
**	RETURNS
**    None
**
**	CAUTIONS
**    None
*/
void SectorOrder::addSectorsOfChildLine(Eqp *pStartEqp, const char *parentRawLine,
	QList<Sector *> &newSectors, char *sectName, size_t sectNameLen)
{
	bool				forward = false;
	Sector				*pSect = NULL;

	BeamLine *pLine = pPool->getLine(pStartEqp->getName());
	if(pLine->getStartLine() && (!strcmp(pLine->getStartLine(), parentRawLine)))
	{
		forward = true;
	}
	else if(pLine->getEndLine() && (!strcmp(pLine->getEndLine(), parentRawLine)))
	{
		forward = false;
	}
	else
	{
		Q_ASSERT(0);
	}

	if(sectName[0])
	{
		for(int sectIdx = newSectors.count() - 1 ; sectIdx >= 0 ; sectIdx--)
		{
			pSect = newSectors.at(sectIdx);
			if(!strcmp(pSect->getName(), sectName))
			{
				break;
			}
		}
	}
	else
	{
		pSect = NULL;
	}

	// DSL in P3 consists of UJ33 and LHCLAYOUT parts. The trick is: coordinates
	// for LHCALYOUT part INCLUDE coordinate of 'DSL_P3' connection point.
	// This configuration leads to infinite calls and stack overflow.
	// By this reason check of name of connected line has been added to recursive calls
	QList<BeamLinePart *> parts = pLine->getParts();
	Eqp	**eqpPtrs;
	int	nEqpPtrs;
	if(forward)
	{
		foreach(BeamLinePart *pPart, parts)
		{
			eqpPtrs = pPart->getEqpPtrs();
			nEqpPtrs = pPart->getNEqpPtrs();
			for(int i = 0 ; i < nEqpPtrs ; i++)
			{
				Eqp *pEqp = eqpPtrs[i];
				// If we are within sector - check if sectors of 'child' line
				// shall be added (only if they belong to the same main part)
				if((sectName[0]) &&
					((pEqp->getType() == EqpType::OtherLineStart) ||
					 (pEqp->getType() == EqpType::OtherLineEnd)))
				{
					// Child lines only make sense for beam vacuums
					if((!(pEqp->getVacType() & VacType::Qrl)) &&
						 (!(pEqp->getVacType() & VacType::Cryo)))
					{
						if(strcmp(pEqp->getName(), pLine->getName()))	// To avoid infinite recursion
						{
							addSectorsOfChildLine(pEqp, pPart->getName(), newSectors, sectName, sectNameLen);
						}
					}
				}
				if(pEqp->getType() == EqpType::VacDevice)
				{
					// L.Kopylov 24.01.2013: stop search if another main part is reached
					if(!checkForNewSect(pLine, pEqp, pSect, newSectors, sectName, sectNameLen))
					{
						return;
					}
				}
			}
		}
	}
	else
	{
		for(int partIdx = parts.count() - 1 ; partIdx >= 0 ; partIdx--)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			eqpPtrs = pPart->getEqpPtrs();
			nEqpPtrs = pPart->getNEqpPtrs();
			for(int i = nEqpPtrs - 1 ; i >= 0 ; i--)
			{
				Eqp *pEqp = eqpPtrs[i];
				// If we are within sector - check if sectors of 'child' line
				// shall be added (only if they belong to the same main part)
				if((sectName[0]) &&
					((pEqp->getType() == EqpType::OtherLineStart) ||
					 (pEqp->getType() == EqpType::OtherLineEnd)))
				{
					// Child lines only make sense for beam vacuums
					if((!(pEqp->getVacType() & VacType::Qrl)) &&
						 (!(pEqp->getVacType() & VacType::Cryo)))
					{
						if(strcmp(pEqp->getName(), pLine->getName()))	// To avoid infinite recursion
						{
							addSectorsOfChildLine(pEqp, pPart->getName(), newSectors, sectName, sectNameLen);
						}
					}
				}
				if(pEqp->getType() == EqpType::VacDevice)
				{
					// L.Kopylov 24.01.2013: stop search if another main part is reached
					if(!checkForNewSect(pLine, pEqp, pSect, newSectors, sectName, sectNameLen))
					{
						return;
					}
				}
			}
		}
	}
}

/*
**
**	FUNCTION
**		Check if new vacuum sector shall be started
**
**	PARAMETERS
**		pLine		- Pointer to beam line being processed
**		pEqp		- Pointer to vacuum equipment to be checked
**		pSect		- Pointer to vacuum sector being processed,
**						or NULL if there is no current sector.
**		newSectors	- List of new sectors
**		sectName	- Name of sector being processed, will be updated by this
**						function if new sector shall be started.
**
**	RETURNS
**		TRUE	- if we are still within main part where current sector is
**		FALSE	- otherwise
**
**	CAUTIONS
**		None
*/
bool SectorOrder::checkForNewSect(BeamLine *pLine, Eqp *pEqp, Sector *pSect,
	QList<Sector *> &newSectors, char *sectName,
#ifdef Q_OS_WIN
	size_t sectNameLen)
#else
	size_t /* sectNameLen */)
#endif
{
	Sector	*pSect2;

	if(!pLine->isCircle())
	{
		if((pSect2 = pEqp->getSectorBefore()))
		{
			if(strcmp(pSect2->getName(), sectName))
			{
				if(!pSect2->hasCommonMainPart(pSect))
				{
					return false;
				}
				bool	found = false;
				for(int sectIdx = newSectors.count() - 1 ; sectIdx >= 0 ; sectIdx--)
				{
					Sector *pNewSect = newSectors.at(sectIdx);
					if(!strcmp(pSect2->getName(), pNewSect->getName()))
					{
						found = true;
						break;
					}
				}
				if(!found)		// New sector
				{
					addSectorCopy(newSectors, pSect2);
				}
#ifdef Q_OS_WIN
				strcpy_s(sectName, sectNameLen, pSect2->getName());
#else
				strcpy(sectName, pSect2->getName());
#endif
			}
		}
	}
	if(!(pSect2 = pEqp->getSectorAfter()))
	{
		if(!(pSect2 = pEqp->getSectorBefore()))
		{
			return true;	// ???
		}
	}
	if(!strcmp(pSect2->getName(), sectName))
	{
		return true;
	}
	if(!pSect2->hasCommonMainPart(pSect))
	{
		return false;
	}
	bool	found = false;
	for(int sectIdx = newSectors.count() - 1 ; sectIdx >= 0 ; sectIdx--)
	{
		Sector *pNewSect = newSectors.at(sectIdx);
		if(!strcmp(pSect2->getName(), pNewSect->getName()))
		{
			found = true;
			break;
		}
	}
	if(!found)		// New sector
	{
		addSectorCopy(newSectors, pSect2);
	}
#ifdef Q_OS_WIN
	strcpy_s(sectName, sectNameLen, pSect2->getName());
#else
	strcpy(sectName, pSect2->getName());
#endif
	return true;
}

/*
**
**	FUNCTION
**		Add copy of old sector to new list of sectors
**
**	PARAMETERS
**		newSectors		- List of new sectors
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorOrder::addSectorCopy(QList<Sector *> &newSectors, Sector *pSect)
{
	newSectors.append(pSect->clone());
	pSect->setNewSector(newSectors.last());
}

/*
**
**	FUNCTION
**		Check if there is at least one device in given sector, if yes - add
**		the sector to new sectors
**
**	PARAMETERS
**		newSectors	- List of new sectors
**		pSector		- Pointer to sector to be added if required
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorOrder::addSectorAnyway(QList<Sector *> &newSectors, Sector *pSector)
{
	bool				hasEqp = false;

	// Search in controllers
	QList<Eqp *> &eqpList = pPool->getCtlList();
	Eqp *pEqp;
	foreach(pEqp, eqpList)
	{
		if(!pEqp->getDpType())
		{
			continue;
		}
		if(pEqp->getSectorBefore() == pSector)
		{
			hasEqp = true;
			break;
		}
		else if(pEqp->getSectorAfter() == pSector)
		{
			hasEqp = true;
			break;
		}
	}

	// Process equipment with physical location
	BeamLine *pLine;
	BeamLinePart *pPart;
	if(!hasEqp)
	{
		QList<BeamLine *> &lines = pPool->getLines();
		foreach(pLine, lines)
		{
			QList<BeamLinePart *> &parts = pLine->getParts();
			foreach(pPart, parts)
			{
				QList<Eqp *> &eqps = pPart->getEqpList();
				foreach(pEqp, eqps)
				{
					if(!pEqp->getDpType())
					{
						continue;
					}
					if(pEqp->getSectorBefore() == pSector)
					{
						hasEqp = true;
						break;
					}
					else if(pEqp->getSectorAfter() == pSector)
					{
						hasEqp = true;
						break;
					}
				}
				if(hasEqp)
				{
					break;
				}
			}
			if(hasEqp)
			{
				break;
			}
		}
	}
	if(!hasEqp)
	{
		return;
	}
	addSectorCopy(newSectors, pSector);

	// Set new sector reference for equipment in this sector
	// Process controllers
	foreach(pEqp, eqpList)
	{
		if(!pEqp->getDpType())
		{
			continue;
		}
		if(pEqp->getSectorBefore() == pSector)
		{
			pEqp->setNewSectors();
		}
		else if(pEqp->getSectorAfter() == pSector)
		{
			pEqp->setNewSectors();
		}
	}

	// Process equipment with physical location
	QList<BeamLine *> &lines = pPool->getLines();
	foreach(pLine, lines)
	{
		QList<BeamLinePart *> &parts = pLine->getParts();
		foreach(pPart, parts)
		{
			QList<Eqp *> &eqps = pPart->getEqpList();
			foreach(pEqp, eqps)
			{
				if(!pEqp->getDpType())
				{
					continue;
				}
				if(pEqp->getSectorBefore() == pSector)
				{
					pEqp->setNewSectors();
				}
				if(pEqp->getSectorAfter() == pSector)
				{
					pEqp->setNewSectors();
				}
			}
		}
	}
}

/*
**
**	FUNCTION
**		Build draw parts for all sectors in data pool. It is assumed that all sectors and all
**		vacuum equipment (the most important are sector borders) are already known.
**		The goal is to find range of equipment in every beam line corresponding to every sector.
**
**	PARAMETERS
**		errList		- Variable where error message shall be added
**						in case of errors.
**
**	RETURNS
**		0  - In case of success
**		-1 - In case of error
**
**	CAUTIONS
**    None
*/
int SectorOrder::buildDrawParts(QStringList & /* errList */)
{
	bool			writeDumpFile = false, found = false;
	int 			startEqpIdx = -1, lastEqpIdx;
	Sector			*pCurSector = NULL;
	BeamLinePart	*pStartPart = NULL, *pLastPart = NULL;

	QList<BeamLine *> &lines = pPool->getLines();
	foreach(BeamLine *pLine, lines)
	{
		QList<BeamLinePart *> &parts = pLine->getParts();
		if(pLine->isCircle())
		{
			if(pPool->isLhcFormat())
			{
				continue;	// LHC ring is procesed in a different way
			}
			// Check if sector crosses start equipment index
			foreach(BeamLinePart *pPart, parts)
			{
				Eqp **eqpPtrs = pPart->getEqpPtrs();
				int nEqpPtrs = pPart->getNEqpPtrs();
				for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(pEqp->getSectorBefore())
					{
						pCurSector = pEqp->getSectorBefore();
						break;
					}
				}
				if(pCurSector)
				{
					break;
				}
			}
			if(!pCurSector)
			{
				pStartPart = parts.first();
				startEqpIdx = 0;
			}
			else
			{
				found = false;
				foreach(BeamLinePart *pPart, parts)
				{
					Eqp **eqpPtrs = pPart->getEqpPtrs();
					int nEqpPtrs = pPart->getNEqpPtrs();
					for(int eqpIdx = nEqpPtrs - 1 ; eqpIdx >= 0 ; eqpIdx--)
					{
						Eqp *pEqp = eqpPtrs[eqpIdx];
						// L.Kopylov 13.11.2013: this if() does not take into account device with sectorBefore == sectorAfter
						//if(pEqp->getSectorAfter() == pCurSector)	// Start of my sector
						if(pEqp->getSectorBefore() && (pEqp->getSectorAfter() == pCurSector) && (pEqp->getSectorBefore() != pEqp->getSectorAfter()))	// Start of my sector
						{
							pStartPart = pPart;
							startEqpIdx = eqpIdx;
							found = true;
							break;
						}
					}
					if(found)
					{
						break;
					}
				}
				if(!found)
				{
					pStartPart = parts.first();
					startEqpIdx = 0;
				}
			}
			pLastPart = NULL;
			lastEqpIdx = -1;
		}
		else	// Straight line
		{
			pStartPart = pLastPart = parts.first();
			startEqpIdx = lastEqpIdx = 0;
		}

		// Process elements of line
		BeamLinePart *pPart = pStartPart;
		int eqpIdx = startEqpIdx;
		pCurSector = NULL;
		float			posFromDev = 0.0;
		const char		*sectFromDev = NULL;
		while(true)
		{
			while(true)
			{
				Eqp **eqpPtrs = pPart->getEqpPtrs();
				int	nEqpPtrs = pPart->getNEqpPtrs();
				if(eqpPtrs)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(pEqp->getType() == EqpType::VacDevice)
					{
						if(pEqp->getSectorBefore())
						{
							if(pCurSector && (pEqp->getSectorBefore() != pCurSector))
							{
								if(!pErrorFile)
								{
									dumpFileName = QDir::tempPath();
#ifdef Q_OS_WIN
									dumpFileName += "\\PVSS_SectorGeometryProblems.txt";
									if(fopen_s(&pErrorFile, dumpFileName.toLatin1().constData(), "w"))
									{
										pErrorFile = NULL;
									}
#else
									dumpFileName += "/PVSS_SectorGeometryProblems.txt";
									pErrorFile = fopen(dumpFileName.toLatin1().constData(), "w");
#endif
								}
								if(pErrorFile)	// fopen() can fail
								{
									fprintf(pErrorFile,
										"Line %s part %s: %s @ %f (sector %s) appears in the same sector %s as %s @ %f\n",
										pLine->getName(), pPart->getName(), pEqp->getName(), pEqp->getStart(),
										pEqp->getSectorBefore()->getName(),
										pCurSector->getName(), sectFromDev, posFromDev);
								}
							}
							pCurSector = pEqp->getSectorBefore();
							sectFromDev = pEqp->getName();
							posFromDev = pEqp->getStart();
							if(pEqp->getSectorAfter() && (pEqp->getSectorAfter() != pEqp->getSectorBefore()))
							{
								pLine->setContainSectorBorder(true);
								if(pLastPart)
								{
									pEqp->getSectorBefore()->addDrawPart(pLine,
										pLastPart, lastEqpIdx, pPart, eqpIdx, found);
									found = false;
								}
								pCurSector = pEqp->getSectorAfter();
								pLastPart = pPart;
								lastEqpIdx = eqpIdx;
								sectFromDev = pEqp->getName();
								posFromDev = pEqp->getStart();
							}
						}
					}
				}
				eqpIdx++;
				if(eqpIdx >= nEqpPtrs)
				{
					break;
				}
				if((pPart == pStartPart) && (eqpIdx == startEqpIdx))
				{
					break;
				}
			}
			if((pPart == pStartPart) && (eqpIdx == startEqpIdx))
			{
				break;
			}
			int partIdx = parts.indexOf(pPart);
			pPart = NULL;
			if(partIdx >= 0)
			{
				partIdx++;
				if(partIdx < parts.count())
				{
					pPart = parts.at(partIdx);
				}
			}
			eqpIdx = 0;
			if(!pPart)
			{
				pPart = parts.first();
				if((pPart == pStartPart) && (eqpIdx == startEqpIdx))
				{
					break;
				}
			}
		}

		// Check if last part of beam line shall be added to draw parts: cycle above only
		// adds draw parts before sector borders, but one can also have draw part after
		// last sector border in beam line
		if(pLine->isCircle())
		{
			if(pCurSector &&
				((pLastPart != pStartPart) || (lastEqpIdx != startEqpIdx)))
			{
				pCurSector->addDrawPart(pLine, pLastPart, lastEqpIdx, pStartPart, startEqpIdx, false);
			}
		}
		else	// Straight line
		{
			if(pCurSector &&
				((pLastPart != parts.last()) || (lastEqpIdx < (parts.last()->getNEqpPtrs() - 1))))
			{
				pCurSector->addDrawPart(pLine, pLastPart, lastEqpIdx,
					parts.last(), parts.last()->getNEqpPtrs() - 1, false);
				// TODO: LIK 13.08.2008 - not used about last eqp idx above...
			}
		}
	}

	addOrphanLines();

	// Mark as 'outer' sectors located on beam lines which are marked 'not used'
	QList<Sector *> &sectors = pPool->getSectors();
	foreach(Sector *pSect, sectors)
	{
		QList<SectorDrawPart *> &drawParts = pSect->getDrawParts();
		foreach(SectorDrawPart *pDrawPart, drawParts)
		{
			if(pDrawPart->getLine()->isNotUsed())
			{
				pSect->setOuter(true);
				break;
			}
		}
	}

	if(pErrorFile)
	{
		writeDumpFile = true;
	}
	if(DebugCtl::isData())
	{
		writeDumpFile = true;
	}

	if(writeDumpFile)
	{
#ifdef Q_OS_WIN
			FILE *pFile = NULL;
			if(fopen_s(&pFile, "C:\\DrawParts.txt", "w"))
			{
				pFile = NULL;
			}
#else
			FILE *pFile = fopen("/home/kopylov/DrawParts.txt", "w");
#endif
		if(pFile)
		{
			int n = 0;
			foreach(Sector *pSect, sectors)
			{
				pSect->dump(n, pFile);
				n++;
			}
			fclose(pFile);
		}
	}
	return 0;
}

/*
**
**	FUNCTION
**		Add to draw parts of sectors those beam lines which have no sector
**		borders in them: they just continue appropriate sector of parent
**		beam line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorOrder::addOrphanLines(void)
{
	QList<Sector *> &sectors = pPool->getSectors();
	foreach(Sector *pSect, sectors)
	{
		QList<SectorDrawPart *> &drawParts = pSect->getDrawParts();
		foreach(SectorDrawPart *pDrawPart, drawParts)
		{
			BeamLine *pLine = pDrawPart->getLine();
			BeamLinePart *pPart = pDrawPart->getStartPart();
			int eqpIdx = pDrawPart->getStartEqpIdx();
			bool finish = false;
			QList<BeamLinePart *> &parts = pLine->getParts();
			foreach(BeamLinePart *pSearch, parts)
			{
				if(pSearch == pPart)
				{
					break;
				}
			}
			while(true)
			{
				Eqp **eqpPtrs = pPart->getEqpPtrs();
				int nEqpPtrs = pPart->getNEqpPtrs();
				while(true)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if((pEqp->getType() == EqpType::OtherLineStart) ||
						(pEqp->getType() == EqpType::OtherLineEnd))
					{
						// To avoid infinite recursive calls for DSL
						if(strcmp(pEqp->getName(), pLine->getName()))
						{
							addDrawPartOnChildLine(pEqp, pSect);
						}
					}
					if((pPart == pDrawPart->getEndPart()) && (eqpIdx == pDrawPart->getEndEqpIdx()))
					{
						finish = true;
						break;
					}
					eqpIdx++;
					if(eqpIdx >= nEqpPtrs)
					{
						break;
					}
				}
				if(finish)
				{
					break;
				}
				int partIdx = parts.indexOf(pPart);
				pPart = NULL;
				if(partIdx >= 0)
				{
					partIdx++;
					if(partIdx < parts.count())
					{
						pPart = parts.at(partIdx);
					}
					else if(pLine->isCircle())
					{
						pPart = parts.first();
					}
					else
					{
						break;
					}
				}
				eqpIdx = 0;
			}
		}
	}
}

/*
**
**	FUNCTION
**		Add draw part of sector on connected line (i.e. process the case when
**		there is another line connection within vacuum sector).
**
**	PARAMETERS
**		pConnEqp	- Pointer to equipment for line connection
**		pSector		- Pointer to sector being processed
**
**	RETURNS
**    None
**
**	CAUTIONS
**    None
*/
void SectorOrder::addDrawPartOnChildLine(Eqp *pConnEqp, Sector *pSector)
{
	BeamLine *pLine = pPool->getLine(pConnEqp->getName());
	if(!pLine)
	{
		return;
	}
	if(pLine->isContainSectorBorder())
	{
		return;	// Will be drawn due to sector borders
	}
	QList<BeamLinePart *> &parts = pLine->getParts();
	pSector->addDrawPart(pLine, parts.first(), 0, parts.last(),
		parts.last()->getNEqpPtrs() - 1, false);

	// Add equipment of child lines
	foreach(BeamLinePart *pPart , parts)
	{
		QList<Eqp *> &eqpList = pPart->getEqpList();
		foreach(Eqp *pEqp, eqpList)
		{
			if((pEqp->getType() == EqpType::OtherLineStart) ||
				(pEqp->getType() == EqpType::OtherLineEnd))
			{
				// To avoid infinite recursive calls on DSL line
				if(strcmp(pEqp->getName(), pLine->getName()))
				{
					addDrawPartOnChildLine(pEqp, pSector);
				}
			}
		}
	}
}
