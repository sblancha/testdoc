#ifndef PANELVIEWER_H
#define PANELVIEWER_H

// GUI of panel viewer application

#include "XmlSynViewer.h"
#include "XmlSynoptic.h"

#include <qmainwindow.h>


class PanelViewer : public QMainWindow
{
	Q_OBJECT

public:
	PanelViewer(QWidget *parent, const QString &fileName);
	~PanelViewer();


private:
	XmlSynViewer	*pSynoptic;
};

#endif	// PANELVIEWER_H
