#ifndef	EQPVBEAMPARAMINT_H
#define	EQPVBEAMPARAMINT_H

//	VBeamParamInt - different beam parameters of type integer

#include "Eqp.h"


class VACCTLEQPDATA_EXPORT EqpVBeamParamInt : public Eqp
{
	Q_OBJECT

public:
	EqpVBeamParamInt(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVBeamParamInt(const EqpVBeamParamInt &src) : Eqp(src) {}
#endif
	~EqpVBeamParamInt();

	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual DataEnum::ValueType getMainValueType(void) const;
	virtual float getMainValue(DataEnum::DataMode mode);
	virtual bool isMainValueValid(DataEnum::DataMode /* mode */) { return true; }
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainValueString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
//	virtual void getDpesForDevList(QStringList &dpes);
//	virtual int checkDevListCrit(void);
//	virtual void getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end);
//	virtual float getGrowFactorForDevList(void);
#endif

//	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline float getPR(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? valueReplay : valueOnline;
	}

protected:
	// Last known value of Value DPE for online mode
	int	valueOnline;

	// Last known value of Value DPE for replay mode
	int	valueReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainValueString(int value, QString &string);
	virtual void getMainStateStringForMenu(int value, QString &string);
#endif

};

#endif	// EQPVBEAMPARAMINT_H
