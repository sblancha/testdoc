#uses "VacCtlEqpDataPvss"	  // Load DLL or so
#uses "vclDevCommon.ctl"    // Load CTRL library
#uses "vclDIPConfig.ctl"		// Load CTRL library 

bool TEST_MODE = FALSE;

string TTO_STRING = ".Actuation.thresholdToOpen";
string TTC_STRING = ".Actuation.thresholdToClose";

// TEST: Temporary test values
float TTO_TH = 5.0; // seconds
float TTC_TH = 5.0; // seconds


  //Get all valve DPs
  dyn_string getValveStateDPs() {
      dyn_string allDpNames = dpNames("*", "VVS_LHC");
      return allDpNames;
  }
  
  
  //TEST: sets thresholds for valves
  int setThresholdDev(dyn_string dpsList){
	  for(int i = 1; i <= dynlen(dpsList); i++) {
		  dpSet(dpsList[i] + TTO_STRING, TTO_TH);
		  dpSet(dpsList[i] + TTC_STRING, TTC_TH);
	  }
  }
  
  
  main() {
	DebugTN("[Valve Monitoring] STARTED injecting threshold times to VVS_LHC");
	setThresholdDev(getValveStateDPs());
	DebugTN("[Valve Monitoring] FINISHED injecting threshold times to VVS_LHC");
  }