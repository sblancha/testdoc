//	Implementation of VacIconEXP_AREA class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconEXP_AREA.h"

#include "EqpEXP_AREA.h"

#include "DataPool.h"
#include "ResourcePool.h"

#include <qpainter.h>
#include <qpixmap.h>

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconEXP_AREA::VacIconEXP_AREA(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(54, 43);
	setAutoFillBackground(true);
}

VacIconEXP_AREA::~VacIconEXP_AREA()
{
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconEXP_AREA::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(54, 43, 27, 22, 0);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconEXP_AREA::setEqp(Eqp *pEqp)
{
	QPixmap pixmap;
	QPalette palette;
	palette.setBrush(backgroundRole(), QBrush(pixmap));
	setPalette(palette);
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpEXP_AREA"), "VacIconEXP_AREA::setEqp", pEqp->getDpName());
		this->pEqp = (EqpEXP_AREA *)pEqp;

		// Load and set pixmap for this device
		QString resourceName("EXP_AREA.");
		resourceName += pEqp->getDpName();
		resourceName += ".Image";
		QString fileName;
		ResourcePool::getInstance().getStringValue(resourceName, fileName);
		if(!fileName.isEmpty())
		{
			QString fullFileName(DataPool::getInstance().getPicturePath());
			fullFileName += fileName;
			if(pixmap.load(fullFileName))
			{
				palette.setBrush(backgroundRole(), QBrush(pixmap));
				setPalette(palette);
			}
		}
	}
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconEXP_AREA::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp)
	{
		pEqp->getToolTipString(text, DataEnum::Online);
	}
	else
	{
		text = "No eqp";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconEXP_AREA::paintEvent(QPaintEvent *pEvent)
{
	QBrush brush(palette().brush(backgroundRole()));
	if(!brush.texture().isNull())
	{
		QWidget::paintEvent(pEvent);
		return;
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	painter.drawRect(rect());
	QRect labelRect = painter.fontMetrics().boundingRect("???");
	painter.drawText(width() / 2 - labelRect.width() / 2, height() / 2 + labelRect.height() / 2,
		"???");
}
