//	Implementation of MobileHistoryDialog class
/////////////////////////////////////////////////////////////////////////////////

#include "MobileHistoryDialog.h"
#include "MobileEqpTree.h"
#include "MobileHistoryImage.h"
#include "MainPartSectorTreeItem.h"
#include "EqpTreeItem.h"
#include "BakeoutEqpTreeItem.h"
#include "BakeoutChannelTreeItem.h"
#include "MobileColorList.h"

#include "MobilePool.h"

#include "DataPool.h"
#include "BeamLine.h"
#include "MobileType.h"

#include "HistoryProcessor.h"

#include "FunctionalType.h"
#include "VacMainView.h"
#include "MobileHistoryQuery.h"
#include "DpeHistoryDepthQuery.h"

#include <QSplitter>
#include <QDateTime>
#include <QDateTimeEdit>
#include <QHeaderView>
#include <QScrollBar>
#include <QLabel>
#include <QGroupBox>
#include <QCheckBox>
#include <QPushButton>
#include <QMenu>
#include <QSpinBox>
#include <QLayout>
#include <QCursor>
#include <QFileDialog>
#include <QMessageBox>

#include <errno.h>

MobileHistoryDialog *MobileHistoryDialog::pInstance = NULL;

/*
**	FUNCTION
**		Create and show new instance of dialog
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::create(void)
{
	if(pInstance)
	{
		pInstance->raise();
		return;
	}
	pInstance = new MobileHistoryDialog();
	pInstance->show();
}

/*
**	FUNCTION
**		Add history value for given device.
**
**	ARGUMENTS
**		rangeId		- Device data range ID
**		dpe			- DPE name for device
**		time		- Timestamp for value
**		value		- Value
**
**	RETURNS
**		true	- If history dialog instance exists;
**		false	- if history dialog instance does not exist, hence further
**					history data processing makes no sence
**
**	CAUTIONS
**		None
*/
bool MobileHistoryDialog::addHistoryValue(int rangeId, const char *dpe, const QDateTime &time, const QVariant &value)
{
	if(pInstance)
	{
		return pInstance->pImage->addHistoryValue(rangeId, dpe, time, value);
	}
	return false;
}

/*
**	FUNCTION
**		All history values for one range of one DPE arrived - redraw image
**
**	ARGUMENTS
**		rangeId		- Device data range ID
**		dpe			- DPE name for device
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::finishHistoryRange(int rangeId, const char *dpe)
{
	if(pInstance)
	{
		if(pInstance->pImage->finishHistoryRange(rangeId, dpe))
		{
			pInstance->pImage->rebuildImage();
		}
	}
}

/*
**	FUNCTION
**		Set absolute history start time for given DP
**
**	ARGUMENTS
**		dpName			- DP name for device
**		absStartTime	- Start time for archive data
**
**	RETURNS
**		1	- dialog instance exists = more data can be needed
**		0	- dialog instance does not exist = no more data needed
**
**	CAUTIONS
**		None
*/
int MobileHistoryDialog::setDpHistoryDepth(const char *dpName, const QDateTime &absStartTime)
{
	if(!pInstance)
	{
		return 0;
	}
	Eqp *pEqp = DataPool::getInstance().findEqpByDpName(dpName);
	if(!pEqp)
	{
		return 1;
	}
	pInstance->pImage->setAbsStartTime(pEqp, absStartTime);
	return 1;
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

MobileHistoryDialog::MobileHistoryDialog() :
	QDialog(NULL /* VacMainView::getInstance() */, Qt::Window)
{
	// Initialize itself
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle("Mobile Equipment");
	settingSpinBoxes = settingScrollBar = settingOnline  = false;
	buildLayout();
	connect(pImage, SIGNAL(imageUpdated(void)), this, SLOT(updateScrollBar(void)));
	connect(pImage, SIGNAL(valueAtPointerChanged(const char *, const char *)),
		this, SLOT(valueAtPointerChanged(const char *, const char *)));
	connect(pImage, SIGNAL(scalesChanged(void)), this, SLOT(imageZoomed(void)));

	MobilePool &pool = MobilePool::getInstance();
	connect(&pool, SIGNAL(activeListChanged(DataEnum::DataMode)),
		this, SLOT(eqpListChanged(DataEnum::DataMode)));

	showTimeRange();

	/*
	const QList<VacMainView *> &mainList = VacMainView::getInstanceList();
	for(int idx = 0 ; idx < mainList.count() ; idx++)
	{
		VacMainView *pInstance = mainList.at(idx);
		if(pInstance->isPvssEvents())
		{
			QObject::connect(this, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)),
				pInstance, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
		}
	}
	*/
	VacMainView *pMainView = VacMainView::getInstance();
	if(pMainView)
	{
		QObject::connect(this, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)),
			pMainView, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)));
	}

	rebuildTreeContent(DataEnum::Online);
}

MobileHistoryDialog::~MobileHistoryDialog()
{
	pInstance = NULL;
}

/*
**	FUNCTION
**		Create all widgets, build layout of control
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
void MobileHistoryDialog::buildLayout(void)
{
	// 0.0) Splitter
	QHBoxLayout *pMyLayout = new QHBoxLayout(this);
	pMyLayout->setSpacing(0);
	pMyLayout->setContentsMargins(0, 0, 0, 0);
	QSplitter *pSplitter = new QSplitter(this);
	pMyLayout->addWidget(pSplitter);
	

	// 0.1: Widget holding all control on left side of splitter
	QWidget	*pLeftWidget = new QWidget(pSplitter);

	// 1) Vertical layout with tree control on top (fixed size) and tree on bottom (resizable)
	QVBoxLayout *pTreeBox = new QVBoxLayout(pLeftWidget);
	pTreeBox->setSpacing(0);
	pTreeBox->setContentsMargins(0, 0, 0, 0);

	// 1.0) 'Menu' entries: File and help in horizontal layout
	QHBoxLayout *pMenuLayout = new QHBoxLayout();
	pMenuLayout->setSpacing(0);
	pMenuLayout->setContentsMargins(0, 0, 0, 0);

	QPushButton *pFilePb = new QPushButton("&File", pLeftWidget);
	pFilePb->setFlat(true);
	QSize size = pFilePb->fontMetrics().size(Qt::TextSingleLine, "File");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pFilePb->setFixedSize(size);
	QMenu *pFileMenu = new QMenu(this);
	pFileMenu->addAction("Print Panel...", this, SLOT(filePrint()));
	pFileMenu->addAction("Export to Excel...", this, SLOT(fileExport()));
	pFileMenu->addSeparator();
	pFileMenu->addAction("Close", this, SLOT(closePress()));
	pFilePb->setMenu(pFileMenu);
	pMenuLayout->addWidget(pFilePb);

	QPushButton *pHelpPb = new QPushButton("&Help", pLeftWidget);
	pHelpPb->setFlat(true);
	size = pHelpPb->fontMetrics().size(Qt::TextSingleLine, "Help");
	size.setWidth(size.width() + 20);
	size.setHeight(size.height() + 4);
	pHelpPb->setFixedSize(size);
	QMenu *pHelpMenu = new QMenu(this);
	pHelpMenu->addAction("User Manual...", this, SLOT(helpPress()));
	pHelpPb->setMenu(pHelpMenu);
	pMenuLayout->addWidget(pHelpPb);

	pMenuLayout->addStretch(10);

	pTreeBox->addLayout(pMenuLayout);
	pTreeBox->addSpacing(8);

	// 1.1) vertical group box for tree content control
	QGroupBox *pTreeCtlBox = new QGroupBox("Search for MobileEquipment", pLeftWidget);
	QVBoxLayout *pTreeCtlBoxLayoutLayout = new QVBoxLayout(pTreeCtlBox);
	pTreeCtlBoxLayoutLayout->setContentsMargins(2, 2, 2, 2);
	pTreeCtlBoxLayoutLayout->setSpacing(2);

	// 1.1.1) Check box - online/offline tree state
	pTreeStateCheckBox = new QCheckBox("Online (recent system state)", pTreeCtlBox);
	pTreeStateCheckBox->setChecked(true);
	pTreeCtlBoxLayoutLayout->addWidget(pTreeStateCheckBox);
	connect(pTreeStateCheckBox, SIGNAL(toggled(bool)), this, SLOT(treeCtlStateChange(bool)));

	// 1.1.2) Widget with label and date editor
	QWidget *pDateEditContainer = new QWidget(pTreeCtlBox);
	pTreeCtlBoxLayoutLayout->addWidget(pDateEditContainer);
	QHBoxLayout *pDateEditContainerBox = new QHBoxLayout(pDateEditContainer);
	pDateEditContainerBox->setContentsMargins(2, 2, 2, 2);
	pDateEditContainerBox->setSpacing(4);

	QLabel *pDateEditLabel = new QLabel("One week from:", pDateEditContainer);
	pDateEditContainerBox->addWidget(pDateEditLabel);

	pDateEdit = new QDateEdit(QDate::currentDate(), pDateEditContainer);
	pDateEdit->setMinimumDate(QDate::currentDate().addDays(-730));
	pDateEdit->setMaximumDate(QDate::currentDate());
	pDateEdit->setDisplayFormat("dd-MM-yyyy");
	pDateEdit->setCalendarPopup(true);
	pDateEdit->setEnabled(false);
	pDateEditContainerBox->addWidget(pDateEdit);
	connect(pDateEdit, SIGNAL(dateChanged(const QDate &)), this, SLOT(treeCtlDateChange(const QDate &)));

	pTreeBox->addWidget(pTreeCtlBox);

	// 1.2) Main part/sector/equipment tree
	pTree = new MobileEqpTree(pLeftWidget);
	QTreeWidgetItem *pHead = new QTreeWidgetItem();
	pHead->setText(0, "Mobile Eqp");
	pTree->setHeaderItem(pHead);
	pTreeBox->addWidget(pTree, 10);
	connect(pTree, SIGNAL(checkBoxPressed(QTreeWidgetItem *)),
		this, SLOT(treeCheckBoxPressed(QTreeWidgetItem *)));
	connect(pTree, SIGNAL(itemSelectionChanged()),
		this, SLOT(treeSelectionChanged()));



	// 0.2 Right widgte holding all controls on right side of splitter
	QWidget	*pRightWidget = new QWidget(pSplitter);

	// 2) Vertical layout with image on top, scroll bar in the middle and
	// image controls on bottom
	QVBoxLayout *pImageBox = new QVBoxLayout(pRightWidget);
	pImageBox->setSpacing(0);
	pImageBox->setContentsMargins(0, 0, 0, 0);

	// 2.1) Graph image - occupies all available space
	pImage = new MobileHistoryImage(pRightWidget);
	pImageBox->addWidget(pImage, 10);

	// 2.2) scroll bar below graph image
	pScrollBar = new QScrollBar(Qt::Horizontal, pRightWidget);
	pImageBox->addWidget(pScrollBar);
	pScrollBar->setEnabled(false);
	connect(pScrollBar, SIGNAL(valueChanged(int)), this, SLOT(scrollBarValueChange(int)));
	connect(pScrollBar, SIGNAL(sliderReleased(void)), this, SLOT(scrollBarSliderRelease(void)));
	
	// 2.3) Horizontal layout with graph controls
	QHBoxLayout *pImageCtlBox = new QHBoxLayout();
	pImageCtlBox->setSpacing(4);
	pImageCtlBox->setContentsMargins(0, 0, 0, 0);
	pImageBox->addLayout(pImageCtlBox);
	pImageCtlBox->addStretch(10);

	// 2.3.1) Vertical group box with time and value labels
	QGroupBox *pValueBox = new QGroupBox("Time and Value", pRightWidget);
	QHBoxLayout *pTimeValueLayout = new QHBoxLayout(pValueBox);
	pTimeValueLayout->setSpacing(8);
	pTimeValueLayout->setContentsMargins(0, 0, 0, 0);
	pTimeLabel = new QLabel(pValueBox);
	pTimeLabel->setText("2011-09-29 29:59:5999");
	pTimeLabel->adjustSize();
	QSize labelSize(pTimeLabel->size());
	pTimeLabel->setFixedSize(labelSize);
	pTimeValueLayout->addWidget(pTimeLabel);
	pValueLabel = new QLabel(pValueBox);
	labelSize.setWidth(labelSize.width() >> 1);
	pValueLabel->setFixedSize(labelSize);
	pTimeValueLayout->addWidget(pValueLabel);
	pImageCtlBox->addWidget(pValueBox);

	// 2.3.2) horizontal group box with 'online' and 'logT' checkboxes
	QGroupBox *pModeBox = new QGroupBox("Mode", pRightWidget);
	QHBoxLayout *pModeBoxLayout = new QHBoxLayout(pModeBox);
	pModeBoxLayout->setSpacing(0);
	pModeBoxLayout->setContentsMargins(0, 0, 0, 0);
	pOnlineCheckBox = new QCheckBox("Online", pModeBox);
	pOnlineCheckBox->setChecked(true);
	connect(pOnlineCheckBox, SIGNAL(toggled(bool)), this, SLOT(onlineChange(bool)));
	pModeBoxLayout->addWidget(pOnlineCheckBox);
	pImageCtlBox->addWidget(pModeBox);

	// 2.3.3) horizontal group box with spinboxes to change time range
	QGroupBox *pRangeBox = new QGroupBox("Time Interval", pRightWidget);
	QHBoxLayout *pRangeBoxLayout = new QHBoxLayout(pRangeBox);
	pRangeBoxLayout->setSpacing(4);
	pModeBoxLayout->setContentsMargins(0, 0, 0, 0);
	pMinSpinBox = new QSpinBox(pRangeBox);
	pMinSpinBox->setRange(0, 59);
	pMinSpinBox->setSingleStep(1);
	pMinSpinBox->setSuffix(" min");
	pRangeBoxLayout->addWidget(pMinSpinBox);
	connect(pMinSpinBox, SIGNAL(valueChanged(int)), this, SLOT(minChange(int)));

	pHourSpinBox = new QSpinBox(pRangeBox);
	pHourSpinBox->setRange(0, 23);
	pHourSpinBox->setSingleStep(1);
	pHourSpinBox->setSuffix(" h");
	pRangeBoxLayout->addWidget(pHourSpinBox);
	connect(pHourSpinBox, SIGNAL(valueChanged(int)), this, SLOT(hourChange(int)));

	pDaySpinBox = new QSpinBox(pRangeBox);
	pDaySpinBox->setRange(0, 99);
	pDaySpinBox->setSingleStep(1);
	pDaySpinBox->setSuffix(" day");
	pRangeBoxLayout->addWidget(pDaySpinBox);
	connect(pDaySpinBox, SIGNAL(valueChanged(int)), this, SLOT(dayChange(int)));
	pImageCtlBox->addWidget(pRangeBox);
}

/*
**	FUNCTION
**		Slot activated when list of active mobile equipment has been changed
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::eqpListChanged(DataEnum::DataMode mode)
{
	if(mode == DataEnum::Online)
	{
		if(pTreeStateCheckBox->checkState() != Qt::Checked)
		{
			return;
		}
	}
	else
	{
		if(pTreeStateCheckBox->checkState() == Qt::Checked)
		{
			return;
		}
	}
	rebuildTreeContent(mode);
}

/*
**	FUNCTION
**		Rebuild content of main parts/sectors/equipment tree for given mode.
**		Before rebuilding tree content - preserve current selection in tree
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::rebuildTreeContent(DataEnum::DataMode mode)
{
	pTree->clear();
	pImage->clearItemsUsed();

	QList<Eqp *> &activeList = MobilePool::getInstance().getActive(mode);
	if(!activeList.count())
	{
		pImage->removeAllItems();
		return;
	}

	// Build list of sectors used
	QList<Sector *> sectorList;
	for(int idx = 0 ; idx < activeList.count() ; idx++)
	{
		Eqp *pEqp = activeList.at(idx);
		Sector *pSector = pEqp->getSectorBefore();
		if(pSector)
		{
			if(sectorList.indexOf(pSector) < 0)
			{
				sectorList.append(pSector);
			}
		}
	}

	// Build content
	QList<Sector *> &allSectors = DataPool::getInstance().getSectors();
	QTreeWidgetItem *pMpItem = NULL;
	int eqpIndex = 0;
	// Every next item is added to the beginning of tree, so process in reverse direction
	for(int sectIdx = allSectors.count() - 1 ; sectIdx >= 0 ; sectIdx--)
	{
		Sector *pSector = allSectors.at(sectIdx);
		if(sectorList.indexOf(pSector) < 0)
		{
			continue;
		}

		QTreeWidgetItem *pSectItem = addSectorToTree(pSector, &pMpItem);
		addSectorEqp(pSectItem, pSector, activeList, eqpIndex);
	}
	pImage->removeUnusedItems();
	pImage->rebuildImage();
}


/*
**	FUNCTION
**		Add sector item to tree
**
**	ARGUMENTS
**		pSector		- Pointer to sector to be added
**		ppMpItem	- Address of pointer to last used main part item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
QTreeWidgetItem *MobileHistoryDialog::addSectorToTree(Sector *pSector, QTreeWidgetItem **ppMpItem)
{
	// Add (or find in tree) main part
	const QList<MainPartMap *> &mapList = pSector->getMainPartMaps();
	if (!mapList.isEmpty()) {
		MainPart *pMainPart = mapList.first()->getMainPart();
		QString mpName(pMainPart->getName());
		if(*ppMpItem)
		{
			if((*ppMpItem)->text(0) != mpName)
			{
				*ppMpItem = NULL;
			}
		}
		if(!(*ppMpItem))
		{
			for(int idx = 0 ; idx < pTree->topLevelItemCount() ; idx++)
			{
				QTreeWidgetItem *pMpItem = pTree->topLevelItem(idx);
				if(pMpItem->text(0) == mpName)
				{
					*ppMpItem = pMpItem;
					break;
				}
			}
		}
		if(!(*ppMpItem))
		{
			*ppMpItem = (QTreeWidgetItem *)(new MainPartSectorTreeItem(pTree, pMainPart->getName(), pMainPart->getVacTypeMask()));
		}
	}
	else
	{
		*ppMpItem = NULL;
	}

	// Add sector to either main part or to tree itself
	MainPartSectorTreeItem *pSectItem = NULL;
	if(*ppMpItem)
	{
		pSectItem = new MainPartSectorTreeItem(*ppMpItem, pSector->getName(), pSector->getVacType());
	}
	else
	{
		pSectItem = new MainPartSectorTreeItem(pTree, pSector->getName(), pSector->getVacType());
	}
	return (QTreeWidgetItem *)pSectItem;
}

/*
**	FUNCTION
**		Add equipment of given sector to tree
**
**	ARGUMENTS
**		pSectItem		- Pointer to sector item in tree
**		pSector			- Pointer to sector
**		activeEqpIter	- Iterator for list of all active mobile equipment
**		eqpIndex		- Index of device in tree
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::addSectorEqp(QTreeWidgetItem *pSectItem, Sector *pSector, const QList<Eqp *> &activeList,
	int &eqpIndex)
{
	// New items are added to parent at the beginning, so process in reverse order
	// Add other equipment
	EqpTreeItem *pEqpItem;
	int itemIndex;
	const QList<BeamLine *> &lines = DataPool::getInstance().getLines();
	for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
	{
		BeamLine *pLine = lines.at(lineIdx);
		const QList<BeamLinePart *> &parts = pLine->getParts();
		for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
		{
			BeamLinePart *pPart = parts.at(partIdx);
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			for(int eqpIdx = nEqpPtrs - 1 ; eqpIdx >= 0 ; eqpIdx--)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if(pEqp->getSectorBefore() != pSector)
				{
					continue;
				}
				Eqp *pActiveEqp = NULL;
				switch(pEqp->getFunctionalType())
				{
				case FunctionalType::VGM:
				case FunctionalType::VGR:
				case FunctionalType::VGP:
				case FunctionalType::VGI:
				case FunctionalType::VGF:
					pEqpItem = new EqpTreeItem(pSectItem, pEqp, eqpIndex++);
					pEqpItem->setFlags(pEqpItem->flags() | Qt::ItemIsUserCheckable);
					if((itemIndex = pImage->getEqpIndex(pEqp)) >= 0)
					{
						pEqpItem->setCheckState(0, Qt::Checked);
						pImage->setItemUsed(itemIndex);
					}
					else
					{
						pEqpItem->setCheckState(0, Qt::Unchecked);
					}
					break;
				case FunctionalType::VPGM:
					for(int idx = 0 ; idx < activeList.count() ; idx++)
					{
						pActiveEqp = activeList.at(idx);
						if(pActiveEqp == pEqp)
						{
							const QString attrName = pEqp->getAttrValue("MasterName");
							if(!attrName.isEmpty())
							{
								Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
								if(pChild)
								{
									pEqpItem = new EqpTreeItem(pSectItem, pChild, eqpIndex++);
									pEqpItem->setFlags(pEqpItem->flags() | Qt::ItemIsUserCheckable);
									if((itemIndex = pImage->getEqpIndex(pChild)) >= 0)
									{
										pEqpItem->setCheckState(0, Qt::Checked);
										pImage->setItemUsed(itemIndex);
									}
									else
									{
										pEqpItem->setCheckState(0, Qt::Unchecked);
									}
								}
							}
							break;
						}
					}
					break;
				default:
					break;
				}
			}
		}
	}

	// Last add bakeout racks (if any) - and they will appear at the beginning
	for(int idx = 0 ; idx < activeList.count() ; idx++)
	{
		Eqp *pEqp = activeList.at(idx);
		if(pEqp->getSectorBefore() != pSector)
		{
			continue;
		}
		if(pEqp->getMobileType() != MobileType::OnSector)
		{
			continue;
		}
		new BakeoutEqpTreeItem(pImage, pSectItem, pEqp);
	}
}

/*
**	FUNCTION
**		Slot activated when tree state check box (online/offline) state has
**		been changed
**
**	ARGUMENTS
**		on	- Check box state
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::treeCtlStateChange(bool on)
{
	if(on)
	{
		rebuildTreeContent(DataEnum::Online);
		pDateEdit->setEnabled(false);
	}
	else
	{
		pDateEdit->setEnabled(true);
		QDateTime startTime(pDateEdit->date());
		MobileHistoryQuery::getInstance().add(startTime, startTime.addSecs(7 * 24 * 60 * 60));
	}
}

/*
**	FUNCTION
**		Slot activated when start date of interval has been changed
**
**	ARGUMENTS
**		startDate	- Date selected in date editor
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::treeCtlDateChange(const QDate &startDate)
{
	QDateTime startTime(startDate);
	MobileHistoryQuery::getInstance().add(startTime, startTime.addSecs(7 * 24 * 60 * 60));
}

/*
**	FUNCTION
**		Display current time range shown in history
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::updateTreeCheckBoxes(QTreeWidgetItem *pItem)
{
	if(!pItem)
	{
		return;
	}
	switch(pItem->type())
	{
	case MobileTreeItemMpSector:
		break;
	case MobileTreeItemEqp:
		updateEqpCheckBoxes(pItem);
		break;
	case MobileTreeItemBakeoutRack:
		updateRackCheckBoxes(pItem);
		break;
	case MobileTreeItemBakeoutChannel:
		updateRackChannelCheckBoxes(pItem);
		break;
	}
}

void MobileHistoryDialog::updateEqpCheckBoxes(QTreeWidgetItem *pItem)
{
	EqpTreeItem *pEqpItem = (EqpTreeItem *)pItem;
/*
printf("MobileHistoryDialog::updateEqpCheckBoxes(): <%s> is %d\n",
pEqpItem->getEqp()->getVisibleName(), pEqpItem->checkState(0));
fflush(stdout);
*/

	setEqpSelection(pItem);

	// Add/remove device from graph
	int index = pImage->getEqpIndex(pEqpItem->getEqp());
	if(pEqpItem->checkState(0) == Qt::Checked)
	{
		if(index >= 0)
		{
			return;	// already in list
		}
		QColor color(*(MobileColorList::getPrColor(pEqpItem->getIndex())));

		// Item is added with some 'default' absolute depth (30 days before start time),
		// at the same time query for real depth is issued
		QDateTime absStart = QDateTime::currentDateTime().addDays(-30);
		if(pTreeStateCheckBox->checkState() != Qt::Checked)
		{
			absStart.setDate(pDateEdit->date());
			absStart = absStart.addDays(-30);
		}
		int depth = absStart.secsTo(QDateTime::currentDateTime());
		HistoryEqpData *pItem = pImage->addEqp(pEqpItem->getEqp(), depth, color);
		DpeHistoryDepthQuery::getInstance().add(pItem->getDpeName(), absStart, pEqpItem->getEqp()->getMobileType());

		// Connect to selection signal of device
		connect(pEqpItem->getEqp(), SIGNAL(selectChanged(Eqp *)), this, SLOT(eqpSelectChanged(Eqp *)));
	}
	else
	{
		if(index >= 0)
		{
			const Eqp *pEqp = pImage->removeItem(index);
			if(pEqp)
			{
				disconnect(pEqp, SIGNAL(selectChanged(Eqp *)), this, SLOT(eqpSelectChanged(Eqp *)));
			}
			pImage->rebuildImage();
		}
	}
}

void MobileHistoryDialog::updateRackCheckBoxes(QTreeWidgetItem *pItem)
{
	Qt::CheckState state = pItem->checkState(0);
	for(int idx = 0 ; idx < pItem->childCount() ; idx++)
	{
		BakeoutChannelTreeItem *pEqpItem = (BakeoutChannelTreeItem *)pItem->child(idx);
		pEqpItem->setCheckState(0, state);
		updateRackChannelAppearance(pEqpItem);
	}
}

void MobileHistoryDialog::updateRackChannelCheckBoxes(QTreeWidgetItem *pItem)
{
	BakeoutChannelTreeItem *pEqpItem = (BakeoutChannelTreeItem *)pItem;
/*
printf("MobileHistoryDialog::updateRackChannelCheckBoxes(): %d.%d is %d\n",
pEqpItem->getChannel(), pEqpItem->isSetT(), pEqpItem->checkState(0));
fflush(stdout);
*/

	// Add/remove device from graph
	updateRackChannelAppearance(pEqpItem);

	// Update check state of parent (rack)
	QTreeWidgetItem *pParent = pItem->parent();
	int nChecked = 0, nUnchecked = 0;
	for(int idx = 0 ; idx < pParent->childCount() ; idx++)
	{
		QTreeWidgetItem *pChild = pParent->child(idx);
		if(pChild->checkState(0) == Qt::Checked)
		{
			nChecked++;
		}
		else
		{
			nUnchecked++;
		}
	}
	Qt::CheckState state = Qt::Unchecked;
	if(nChecked)
	{
		state = nUnchecked ? Qt::PartiallyChecked : Qt::Checked;
	}
	pParent->setCheckState(0, state);
}

void MobileHistoryDialog::updateRackChannelAppearance(BakeoutChannelTreeItem *pEqpItem)
{
	int index = pImage->getEqpIndex(pEqpItem->getEqp(), pEqpItem->getChannel(), pEqpItem->getValueType());
	if(pEqpItem->checkState(0) == Qt::Checked)
	{
		if(index >= 0)
		{
			return;	// already in list
		}
		QColor color(*(MobileColorList::getBakeoutColor(pEqpItem->getChannel())));

		// Item is added with some 'default' absolute depth (30 days before start time),
		// at the same time query for real depth is issued
		QDateTime absStart = QDateTime::currentDateTime().addDays(-30);
		if(pTreeStateCheckBox->checkState() != Qt::Checked)
		{
			absStart.setDate(pDateEdit->date());
			absStart = absStart.addDays(-30);
		}
		int depth = absStart.secsTo(QDateTime::currentDateTime());
		HistoryEqpData *pItem = pImage->addEqp(pEqpItem->getEqp(), pEqpItem->getChannel(), pEqpItem->getValueType(), depth, color);
		const QString dpe = pItem->getDpeName();
		QDateTime viewStart;
		if(pTreeStateCheckBox->isChecked())
		{
			viewStart = QDateTime::currentDateTime();
		}
		else
		{
			viewStart.setDate(pDateEdit->date());
		}
		DpeHistoryDepthQuery::getInstance().add(dpe.toLatin1(), viewStart, pEqpItem->getEqp()->getMobileType());
		setRackChannelSelection(pEqpItem);
	}
	else
	{
		if(index >= 0)
		{
			pImage->removeItem(index);
			pImage->rebuildImage();
		}
	}
}

/*
**	FUNCTION
**		Display current time range shown in history
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::showTimeRange(void)
{
	// Show range in spin boxes
	settingSpinBoxes = true;
	int secs = pImage->getTimeMin().secsTo(pImage->getTimeMax());
	int totalMins = secs / 60;
	int days = totalMins / (60 * 24);
	pDaySpinBox->setValue(days);
	int hours = (totalMins - days * (60 * 24)) / 60;
	pHourSpinBox->setValue(hours);
	int mins = totalMins - days * (60 * 24) - hours * 60;
	pMinSpinBox->setValue(mins);
	settingSpinBoxes = false;
}

/*
**	FUNCTION
**		Update scroll bar parameters according to totally available
**		and currently shown time ranges
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::updateScrollBar(void)
{
	settingScrollBar = true;
	int maxDepth = pImage->getMaximumDepth();
	pScrollBar->setRange(-maxDepth, 0);
	pScrollBar->setPageStep(pImage->getDepth() * 60);
	int start = QDateTime::currentDateTime().secsTo(pImage->getTimeMax());
	pScrollBar->setValue(start);
	settingScrollBar = false;
}

/*
**	FUNCTION
**		Set new time range as a result of value change in spin box(es)
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::setRangeFromSpinBoxes(void)
{
	int totalMins = pMinSpinBox->value() +
		pHourSpinBox->value() * 60 +
		pDaySpinBox->value() * 60 * 24;
	if(totalMins < 1)
	{
		totalMins = 1;
	}

	pImage->setDepth(totalMins);
	showTimeRange();
}

void MobileHistoryDialog::minChange(int /* value */)
{
	if(!settingSpinBoxes)
	{
		setRangeFromSpinBoxes();
	}
}
void MobileHistoryDialog::hourChange(int /* value */)
{
	if(!settingSpinBoxes)
	{
		setRangeFromSpinBoxes();
	}
}
void MobileHistoryDialog::dayChange(int /* value */)
{
	if(!settingSpinBoxes)
	{
		setRangeFromSpinBoxes();
	}
}

void MobileHistoryDialog::scrollBarValueChange(int value)
{
	if(settingScrollBar)
	{
		return;
	}
	QDateTime end(QDateTime::currentDateTime().addSecs(value));
	QDateTime start(end.addSecs(-pImage->getDepth() * 60));
	if(pScrollBar->isSliderDown())
	{
		pImage->showNextRange(start, end);
	}
	else
	{
		pImage->hideNextRange();
		pImage->setTimeLimits(start, end);
	}
}

void MobileHistoryDialog::scrollBarSliderRelease(void)
{
	QDateTime end(QDateTime::currentDateTime().addSecs(pScrollBar->value()));
	QDateTime start(end.addSecs(-pImage->getDepth() * 60));
	pImage->hideNextRange();
	pImage->setTimeLimits(start, end);
}


/*
**	FUNCTION
**		Slot activated when another item in tree has been selected
**
**	ARGUMENTS
**		pItem	- Pointer to tree item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::treeSelectionChanged(void)
{
	QList<QTreeWidgetItem *> selList = pTree->selectedItems();
	if(selList.isEmpty())
	{
		return;
	}
	QTreeWidgetItem *pItem = selList.first();
	switch(pItem->type())
	{
	case MobileTreeItemMpSector:
		break;
	case MobileTreeItemEqp:
		setEqpSelection(pItem);
		break;
	case MobileTreeItemBakeoutChannel:
		setRackChannelSelection(pItem);
		break;
	}
}

/*
**	FUNCTION
**		Select device corresponding to tree item - pressure device
**
**	ARGUMENTS
**		pItem	- Pointer to tree item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::setEqpSelection(QTreeWidgetItem *pItem)
{
	pImage->unselectAllRacks();
	EqpTreeItem *pEqpItem = (EqpTreeItem *)pItem;
	QRect itemRect = pTree->visualItemRect(pItem);
	QPoint point(itemRect.left(), itemRect.top());
	QPoint screenPoint = mapToGlobal(point);
	emit dpMouseDown(1, DataEnum::Online, screenPoint.x(), screenPoint.y(), 0,
		pEqpItem->getEqp()->getDpName());
}

/*
**	FUNCTION
**		Select device corresponding to tree item - bakeout rack channel
**
**	ARGUMENTS
**		pItem	- Pointer to tree item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::setRackChannelSelection(QTreeWidgetItem *pItem)
{
	settingSelection = true;

	BakeoutChannelTreeItem *pChannel = (BakeoutChannelTreeItem *)pItem;
	pImage->selectEqp(pChannel->getEqp(), pChannel->getChannel(), pChannel->getValueType());

	settingSelection = false;
	pImage->rebuildImage();
}

/*
**	FUNCTION
**		Slot activated when item in tree has been pressed
**
**	ARGUMENTS
**		pItem	- Pointer to tree item
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MobileHistoryDialog::treeCheckBoxPressed(QTreeWidgetItem *pItem)
{
	updateTreeCheckBoxes(pItem);
}

void MobileHistoryDialog::valueAtPointerChanged(const char *timeStr, const char *valueStr)
{
	pTimeLabel->setText(timeStr ? timeStr : "");
	pValueLabel->setText(valueStr ? valueStr : "");
}

void MobileHistoryDialog::eqpSelectChanged(Eqp *pEqp)
{
	if(settingSelection)
	{
		return;
	}
	int index = pImage->getEqpIndex(pEqp);
/*
printf("MobileHistoryDialog::eqpSelectChanged(%s): index %d\n", pEqp->getName(), index);
fflush(stdout);
*/
	if(index >= 0)
	{
		// setSelectionInTable(index, pEqp);
		pImage->rebuildImage();
	}
}


void MobileHistoryDialog::imageZoomed(void)
{
	// Update state of 'online' flag
	if(pImage->isOnline() != pOnlineCheckBox->isChecked())
	{
		settingOnline = true;
		pOnlineCheckBox->setChecked(pImage->isOnline());
		settingOnline = false;
	}

	// Update spin boxes and scroll bar
	showTimeRange();
	updateScrollBar();
}

void MobileHistoryDialog::onlineChange(bool on)
{
	if(!settingOnline)
	{
		pImage->setOnline(on);
	}
	pScrollBar->setEnabled(!on);
}

void MobileHistoryDialog::filePrint(void)
{
	VacMainView::printWidget(this);
}

void MobileHistoryDialog::fileExport(void)
{
	if(pImage->isEmpty())
	{
		return;
	}
	QString newFile = QFileDialog::getSaveFileName(
		this,
		"Select a filename to save",
		VacMainView::getFilePath(),	// QString(),
		"Excel files (*.xls)");
	if(newFile.isEmpty())
	{
		return;
	}
	QFileInfo info(newFile);
	if(info.suffix() != "xls")
	{
		newFile += ".xls";
		info.setFile(newFile);
	}
	VacMainView::setFilePath(info.path());
	/* L.Kopylov 26.10.2012
	if(info.exists())
	{
		int answer = QMessageBox::warning(this,
			"File exists",
			"Selected file exists - overwrite it ?",
			QMessageBox::Yes, QMessageBox::No);
		if(answer != QMessageBox::Yes)
		{
			return;
		}
	}
	*/
	HistoryProcessor processor;
	pImage->addExportValues(processor);
	FILE	*pFile;
#ifdef Q_OS_WIN
	if(fopen_s(&pFile, newFile.toLatin1(), "w"))
	{
		return;
	}
#else
	if(!(pFile = fopen(newFile.toLatin1(), "w")))
	{
		return;
	}
#endif
	if(!pFile)
	{
		QMessageBox::warning(this, "WARNING", QString("Failed to open file for writing, errno %1").arg(errno));
		return;
	}
	QString error = processor.write(pFile, false);
	fclose(pFile);
	if(!error.isEmpty())
	{
		QMessageBox::warning(this, "WARNING", error);
	}
}

void MobileHistoryDialog::closePress(void)
{
	deleteLater();
}

void MobileHistoryDialog::helpPress(void)
{
}


