//	Implementation of EqpPLC class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpPLC.h"
#include "DataPool.h"
#include "FunctionalTypePLC.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_OK	0,255,0
#define COLOR_BAD	255,0,0


////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpPLC::EqpPLC(const Eqp &source) : Eqp(source)
{
	alarmOnline = alarmReplay = 0;
	functionalType = FunctionalType::PLC;
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpPLC::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	addDpeNameToList(stateDpes, "AL1");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpPLC::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "AL1", mode);
	// Emit signal immediately - this is major difference from other types
	QVariant value(mode == DataEnum::Replay ? alarmReplay : alarmOnline);
	emit dpeChanged(this, "AL1", DataEnum::Own, value, mode, mode == DataEnum::Replay ? timeStampReplay : timeStampOnline);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpPLC::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "AL1", mode);
	disconnectDpe(pDst, "EqpStatus", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpPLC::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "AL1"))
	{
		if(value.canConvert(QVariant::Bool))
		{
			bool newValue = value.toBool();
			if(alarmOnline == newValue)
			{
				return;
			}
			alarmOnline = newValue;
			timeStampOnline = timeStamp;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}
/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpPLC::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "AL1"))
	{
		if(value.canConvert(QVariant::Bool))
		{
			bool newValue = value.toBool();
			if(alarmReplay == newValue)
			{
				return;
			}
			alarmReplay = newValue;
			timeStampReplay = timeStamp;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif


/*
**	FUNCTION
**		Check if this PLC is alive or not
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		true	- if PLC is alive;
**		false	- if PLC is dead
**
**	CAUTIONS
**		None
*/
bool EqpPLC::isAlive(DataEnum::DataMode mode) const
{
	return mode == DataEnum::Replay? (alarmReplay == 0) : (alarmOnline == 0);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpPLC::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(alarmReplay, color);
	}
	else
	{
		getMainColor(alarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpPLC::getMainColor(int alarm, QColor &color)
{
	if(alarm)
	{
		color.setRgb(COLOR_BAD);
	}
	else
	{
		color.setRgb(COLOR_OK);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpPLC::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(alarmReplay, string);
	}
	else
	{
		getMainStateString(alarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		alarm	- Value of PLC alarm
**		color	- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpPLC::getMainStateString(int alarm, QString &string)
{
	if(alarm)
	{
		string = "DEAD";
	}
	else
	{
		string = "OK";
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpPLC::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("AL1");
	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	bool alarm = pVariant->toBool();
	delete pVariant;
	getMainStateString(alarm, state);
	getMainColor(alarm, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpPLC::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if(checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case FunctionalTypePLC::OK:
			if(!alarmOnline)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}

