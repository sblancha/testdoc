#ifndef	EQPVV_AO_H
#define	EQPVV_AO_H

//	Analog valve for LHC BGI - new version, type 8

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVV_AO : public Eqp
{
	Q_OBJECT

public:
	EqpVV_AO(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVV_AO(const EqpVV_AO &src) : Eqp(src) {}
#endif
	~EqpVV_AO();

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline unsigned getSetPoint(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? setPointReplay : setPointOnline; }
	virtual int getErrorCode(DataEnum::DataMode mode);
	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of SetPointR DPE for online mode
	unsigned	setPointOnline;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for replay mode
	unsigned	setPointReplay;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned setPoint, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, unsigned setPoint, bool plcAlarm, QString &string);
#endif
	
};

#endif	// EQPVV_AO_H
