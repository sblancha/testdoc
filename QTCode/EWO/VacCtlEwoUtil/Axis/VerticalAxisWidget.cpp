//	Implementation of VerticalAxisWidget class
/////////////////////////////////////////////////////////////////////////////////
#include "VerticalAxisWidget.h"

#include "VacMainView.h"

#include "PlatformDef.h"
#include "DebugCtl.h"
#include "ResourcePool.h"
#include "EqpMsgCriteria.h"

#include <QMenu>
#include <QLabel>
#include <QCursor>

#include <QToolButton>
#include <QResizeEvent>

#include <math.h>

#define maxOf2(a,b) ((a) > (b) ? (a) : (b))

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VerticalAxisWidget::VerticalAxisWidget(DataEnum::ValueType valueType, QWidget *parent, Qt::WindowFlags f)
	: QWidget(parent, f)
{
	if(VacMainView::getInstance())
	{
		setFont(VacMainView::getInstance()->font());
	}
	this->valueType = valueType;
	grid = true;
	gridStyle = Qt::SolidLine;
	tickSize = 4;
	drawEvery = 1;
	initialLimitsSet = temporaryLinear = false;
	setDefaultLimits();
	linScaleMulti = linScaleLog = 0;
	linLabelFormat[0] = '\0';
	needPrepare = true;
	active = false;
	showUnit = true;
	valueCoeff = 1;
	gotValueCoeff = false;

	const char *unit = getUnit();
	if(strlen(unit))
	{
		QRect rect = fontMetrics().boundingRect(unit);
		setFixedWidth(rect.width());
	}
	else
	{
		setFixedWidth(16);	// Some default
	}
	leftOfPlot = (valueType == DataEnum::Pressure)||(valueType == DataEnum::HighPressure);

	pUpButton = new QToolButton(this);
	pUpButton->setArrowType(Qt::UpArrow);
	pUpButton->setFixedSize(QSize(13, 13));
	pUpButton->hide();
	pUpButton->setCursor(Qt::ArrowCursor);
	pUpButton->installEventFilter(this);
	QObject::connect(pUpButton, SIGNAL(clicked(void)), this, SLOT(upButtonClick(void)));

	pDownButton = new QToolButton(this);
	pDownButton->setArrowType(Qt::DownArrow);
	pDownButton->setFixedSize(QSize(13, 13));
	pDownButton->hide();
	pDownButton->setCursor(Qt::ArrowCursor);
	pDownButton->installEventFilter(this);
	QObject::connect(pDownButton, SIGNAL(clicked(void)), this, SLOT(downButtonClick(void)));

	setMouseTracking(true);
	setFocusPolicy(Qt::StrongFocus);
	shiftClick = false;
}

VerticalAxisWidget::~VerticalAxisWidget()
{
}

void VerticalAxisWidget::resizeEvent(QResizeEvent *pEvent)
{
	if(pEvent->oldSize().height() != height())
	{
		needPrepare = true;
		emit limitsChanged(false);
	}
}

/*
**	FUNCTION
**		Prepare all axis drawing parameters
**
**	PARAMETERS
**		None
**
**	RETURNS
**		true if axis parameters have been changed,
**		false otherwise
**
**	CAUTIONS
**		None
*/
bool VerticalAxisWidget::prepare(void)
{
	bool	result = needPrepare;

	needPrepare = false;

	if(!result)
	{
		return result;
	}

	if(logScale)
	{
		prepareLogScale();
	}
	else
	{
		prepareLinScale();
	}
	return result;
}

/*
**	FUNCTION
**		Process paint event - redraw scale itself, NOT ticks/lines on plot area
**
**	PARAMETERS
**		pEvent	- Pointer to paint event, not used here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::paintEvent(QPaintEvent * /* pEvent */)
{
	if(needPrepare)
	{
		prepare();
	}
	QPainter painter(this);
	if(logScale)
	{
		drawLogScale(painter);
	}
	else
	{
		drawLinScale(painter);
	}

	// Units + label
	int posY = 0;
	QFontMetrics fm = painter.fontMetrics();
	if(!scaleLabel.isEmpty())
	{
		QRect rect = fm.boundingRect(scaleLabel);
		painter.fillRect(0, 0, width(), rect.height() + 1, Qt::white);
		int posX = (width() - rect.width()) >> 1;
		painter.drawText(posX, rect.height() - fm.descent(), scaleLabel);
		posY = rect.height();
	}
	const char *unit = getUnit();
	if(unit[0])
	{
		QRect rect = fm.boundingRect(unit);
		painter.fillRect(0, posY, width(),
			posY ? rect.height() + 2 : rect.height(), Qt::white);
		int posX = (width() - rect.width()) >> 1;
		painter.drawText(posX, posY + rect.height() - fm.descent(), unit);
		posY = rect.height();
	}
}

/*
**	FUNCTION
**		Redraw decorations (ticks, lines) on plot area. Only active axis
**		draws all these decorations
**
**	PARAMETERS
**		painter	- Painter used for drawing
**		plotArea	- geometry of plot area
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Color and font for axis shall be set in painter BEFORE calling this method.
**		Drawing area shall be cleared before calling this method.
*/
void VerticalAxisWidget::drawOnPlot(QPainter &painter, const QRect &plotArea)
{
	if(!active)
	{
		return;
	}

	if(needPrepare)
	{
		prepare();
	}

	// Draw axis labels, ticks and subticks
	if(logScale)
	{
		drawOnPlotLogScale(painter, plotArea);
	}
	else
	{
		drawOnPlotLinScale(painter, plotArea);
	}
}

/*
**	FUNCTION
**		Get minimum and maximum of axis for drawing.
**
**	PARAMETERS
**		useMin	- Value to be used as minimum will be returned here
**		useMax	- Value to be used as maximum will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::getLimits(float &useMin, float &useMax)
{
	if((useMin = min ) > (useMax = max))
	{
		useMin = max;
		useMax = min;
	}
	else if(min == max)
	{
		if(min == 0)
		{
			useMin = (float)(min - 1.0);
			useMax = (float)(max + 1.0);
		}
		else
		{
			useMin = (float)(min / 1.1);
			useMax = (float)(min * 1.1);
		}
	}
}

/*
**	FUNCTION
**		Set minimum and maximum of axis for drawing.
**
**	PARAMETERS
**		newMin	- New scale minimum
**		newMax	- New scale maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::setLimits(float newMin, float newMax)
{
	if(valueType == DataEnum::BitState)
	{
		return;	// Always 0/1
	}
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("VerticalAxisWidget::setLimits(%g, %g)\n", newMin, newMax);
	#else
		printf("VerticalAxisWidget::setLimits(%g, %g) needPrepare %d\n", newMin, newMax, needPrepare);
		printf("VerticalAxisWidget::setLimits(old %g, %g)\n", min, max);
		printf("VerticalAxisWidget::setLimits(equ %d, %d)\n", min == newMin, max == newMax);
		fflush(stdout);
	#endif
	}

	bool changed = false;
	if(min != newMin)
	{
		changed = true;
	}
	min = newMin;
	if(max != newMax)
	{
		changed = true;
	}
	max = newMax;
	if(changed)
	{
		needPrepare = true;
		update();
		emit limitsChanged(false);
	}
}

/*
**	FUNCTION
**		Set initial values for axis limits based on values shown
**
**	PARAMETERS
**		newMin	- New scale minimum
**		newMax	- New scale maximum
**		maxValues	- Maximum number of values per device
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::setInitialLimits(float newMin, float newMax, int maxValues)
{
	if(valueType == DataEnum::BitState)
	{
		return;	// Always 0/1
	}
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("VerticalAxisWidget::setInitialLimits(%g, %g, %d)\n", newMin, newMax, maxValues);
#else
		printf("VerticalAxisWidget::setInitialLimits(%g, %g, %d)\n", newMin, newMax, maxValues);
		fflush(stdout);
#endif
	}

	float absMin = getAbsMin();
	if(newMin <= absMin)
	{
		newMin = absMin;
		if(newMin > newMax)
		{
			return;
		}
	}
	float absMax = getAbsMax();
	if(newMax > absMax)
	{
		newMax = absMax;
	}

	// Some range has been found - the procedure will not be repeated anymore
	// for this value type
	initialLimitsSet = maxValues > 1;	// 1 online + 1 history
	if(logScale)
	{
		newMin = (float)pow(10, floor(log10(newMin)));
		newMax = (float)pow(10, ceil(log10(newMax)));
	}
	else
	{
		if(newMin == newMax)
		{
			if(newMin)
			{
				newMin -= 0.05 * newMin;
				newMax += 0.05 * newMax;
			}
			else
			{
				newMax = 1;
			}
		}
	}
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("VerticalAxisWidget::setInitialLimits(): setting %g, %g\n", newMin, newMax);
#else
		printf("VerticalAxisWidget::setInitialLimits(): setting %g, %g\n", newMin, newMax);
		fflush(stdout);
#endif
	}
	setLimits(newMin, newMax);
}

void VerticalAxisWidget::setZooming(const QRect &zoomRect)
{
	if(!zoomRect.height())
	{
		return;
	}
	float maxValue = (float)screenToValue(zoomRect.top()),
		minValue = (float)screenToValue(zoomRect.bottom());
	if(logScale)
	{
		maxValue = (float)pow(10, ceil(log10(maxValue)));
		minValue = (float)pow(10, floor(log10(minValue)));
	}
	setLimits(minValue, maxValue);
}


/*
**	FUNCTION
**		Convert value to screen Y-coordinate
**
**	PARAMETERS
**		value	- value to be converted
**		screenY	- resulting Y-coordinate will be returned here
**
**	RETURNS
**		enumeration - position of value with respect to axis limits
**
**	CAUTIONS
**		None
*/
VerticalAxisWidget::ValueRange VerticalAxisWidget::valueToScreen(float value, int &screenY)
{
	if(logScale)
	{
		return valueToScreenLog(value, screenY);
	}
	return valueToScreenLin(value, screenY);
}

/*
**	FUNCTION
**		Convert log10(value) to screen Y-coordinate
**
**	PARAMETERS
**		value	- log10(value) to be converted
**		screenY	- resulting Y-coordinate will be returned here
**
**	RETURNS
**		enumeration - position of value with respect to axis limits
**
**	CAUTIONS
**		Only works in logarithmic mode
*/
VerticalAxisWidget::ValueRange VerticalAxisWidget::logValueToScreen(float value, int &screenY)
{
	if(!logScale)
	{
		return BelowAxis;
	}
	int scaleBottom = height() - 1;
	if(value < logScaleMin)
	{
		screenY = scaleBottom;
		return BelowAxis;
	}
	else if(value > logScaleMax)
	{
		screenY = 0;
		return AboveAxis;
	}
	screenY = scaleBottom - (int)rint((double)(scaleBottom) * (value - logScaleMin) /
		(logScaleMax - logScaleMin));
	return WithinRange;
}

/*
** FUNCTION
**		Convert screen Y-coordinate to value
**
** PARAMETERS
**		screenY	- Y-coordinate on the screen
**
** RETURNS
**		Value corresponding to screen coordinate
**
** CAUTIONS
**		None
*/
double VerticalAxisWidget::screenToValue(int screenY)
{
	double	result;

	if(logScale)
	{
		double	valueLog = logScaleMin + (double)(height() - screenY) / (double)(height()) *
			(logScaleMax - logScaleMin);
		result = pow(10.0, valueLog);
	}
	else
	{
		result = scaleMin + (double)(height() - screenY) / (double)(height()) *
			(scaleMax - scaleMin);
		result *= linScaleMulti;
	}
	return result;
}


/*
**	FUNCTION
**		Prepare all axis drawing parameters for logarithmic scale
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::prepareLogScale(void)
{
	float useMin, useMax;
	getLimits(useMin, useMax);

	// Count how many labeled ticks can be put in graph area

	// Some space can be taken away:
	// - on top - untis + topmost label value
	// - on bottom - just to make sure label is not only partially visible
	const char *typicalLabel = "1E-12";
	QRect typicalRect = fontMetrics().boundingRect(typicalLabel);
	int usedForUnit = 0;
	if(showUnit)
	{
		const char *unit = getUnit();
		if(strlen(unit))
		{
			QRect rect = fontMetrics().boundingRect(unit);
			usedForUnit = rect.height() + 2;
		}
	}		

	// Label for maximum - space is required for units label and 1/2 of label itself
	int maxLabelOffset = usedForUnit + typicalRect.height() / 2;

	// Label for minimum - space is required for 1/2 of label height
	int minLabelOffset = typicalRect.height() / 2;

	// As a result - space for scale itself is reduced, or range is extended
	int axisHeight = height();
	scaleMin = useMin;
	scaleMax = useMax;
	double scaleHeight = log10(scaleMax) - log10(scaleMin);
	double valuePerPixel = scaleHeight / axisHeight;
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("prepareLogScale(): useMin %g useMax %g scaleMin %g scaleMax %g\n",
			useMin, useMax, scaleMin, scaleMax);
#else
		printf("prepareLogScale(): useMin %g useMax %g scaleMin %g scaleMax %g\n",
			useMin, useMax, scaleMin, scaleMax);
		fflush(stdout);
#endif
	}

	// Move scale max and min
	scaleMax = pow(10.0, log10(useMax) + valuePerPixel * maxLabelOffset);
	scaleMin = pow(10.0, log10(useMin) - valuePerPixel * minLabelOffset);

	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("prepareLogScale() moved: scaleMin %g scaleMax %g scaleHieght %g valuePerPixel %g\n",
			scaleMin, scaleMax, scaleHeight, valuePerPixel);
#else
		printf("prepareLogScale() moved: scaleMin %g scaleMax %g scaleHieght %g valuePerPixel %g\n",
			scaleMin, scaleMax, scaleHeight, valuePerPixel);
		fflush(stdout);
#endif
	}

	logScaleMin = log10(scaleMin);
	logScaleMax = log10(scaleMax);
	
	drawEvery = 0;
	int totalLabelsHeight,
		labelNbr,
		nLabels,
		maxLabelWidth,
		labelMin = (int)rint(log10(useMin)),
		labelMax = (int)rint(log10(useMax));
	do
	{
		drawEvery++;
		totalLabelsHeight = labelNbr = nLabels = maxLabelWidth = 0;
		for(int value = labelMin ; value <= labelMax ; value++)
		{
			if(drawEvery > 1)
			{
				if(++labelNbr != drawEvery)
				{
					continue;
				}
				labelNbr = 0;
			}
			char label[64];
			makeLogLabel(value, label, sizeof(label) / sizeof(label[0]));
			QRect rect = fontMetrics().boundingRect(label);
			// Count total vertical space to be consumed by all labels
			totalLabelsHeight += rect.height();

			// Calculated maximum width of one label
			if(rect.width() > maxLabelWidth)
			{
				maxLabelWidth = rect.width();
			}
			nLabels++;
		}
		if(nLabels < 2)
		{
			break;
		}
	} while(totalLabelsHeight >= axisHeight);

	calculateMinWidth(maxLabelWidth);
}

/*
**	FUNCTION
**		Prepare all axis drawing parameters for linear scale
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::prepareLinScale(void)
{
	float	useMin, useMax;
	getLimits(useMin, useMax);
	switch(valueType)
	{
	case DataEnum::BitState:	// Very simple for bit state history
		scaleMin = (float)-0.05;
		scaleMax = (float)1.1;
		labelsStart = 0;
		labelsStep = 1;
#ifdef Q_OS_WIN
		strcpy_s(linLabelFormat, sizeof(linLabelFormat) / sizeof(linLabelFormat[0]), "%2.0f");
#else
		strcpy(linLabelFormat, "%2.0f");
#endif
		linScaleMulti = 1;
		linScaleLog = 0;
		break;
	case DataEnum::FillNumber:	// Also relatively simple
		prepareLinScaleInteger(useMin, useMax);
		break;
	default:	// More complex
		prepareLinScaleCommon(useMin, useMax);
		break;
	}

	// Calculate screen space to be consumed by labels
	float value = labelsStart,
		labelsEnd = scaleMax;	// * pow(10.0, scaleMax);

	drawEvery = 1;	// TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	int maxLabelWidth = 0;
	while(value <= labelsEnd)
	{
		char	label[128];
		makeLinLabel(value, label, sizeof(label) / sizeof(label[0]));
		QRect rect = fontMetrics().boundingRect(label);
		if(rect.width() > maxLabelWidth)
		{
			maxLabelWidth = rect.width();
		}
		value += labelsStep;
	}
	calculateMinWidth(maxLabelWidth);
}

void VerticalAxisWidget::calculateMinWidth(int maxLabelWidth)
{
	int minWidth = maxLabelWidth + 4;
	scaleLabel = "";
	if(!logScale)
	{
		if(linScaleLog)
		{
			char buf[32];
#ifdef Q_OS_WIN
			sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "E%+d ", (int)rint(linScaleLog));
#else
			sprintf(buf, "E%+d ", (int)rint(linScaleLog));
#endif
			scaleLabel = buf;
			QRect rect = fontMetrics().boundingRect(scaleLabel);
			if(rect.width() > minWidth)
			{
				minWidth = rect.width();
			}
		}
	}
	if(showUnit)
	{
		const char *unit = getUnit();
		if(strlen(unit))
		{
			QRect rect = fontMetrics().boundingRect(unit);
			if(rect.width() > minWidth)
			{
				minWidth = rect.width();
			}
		}
	}
	int buttonWidth = pUpButton->width() + 4;
	if(minWidth < buttonWidth)
	{
		minWidth = buttonWidth;
	}

	// Set new minimum width if different from old one
	bool newWidth = false;
	QSize recentSize(sizeHint());
	if(recentSize.isValid())
	{
		if(minWidth != recentSize.width())
		{
			newWidth = true;
		}
	}
	else
	{
		newWidth = true;
	}
	if(newWidth)
	{
		setFixedWidth(minWidth);
		updateGeometry();
	}
}

/*
**	FUNCTION
**		Prepare min/max/formats for linear scale - common case
**
**	PARAMETERS
**		useMin	- Minimum value to use during calculation
**		useMax	- Maximum value to use during calculation
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::prepareLinScaleCommon(float useMin, float useMax)
{
	//////////////////////////////////////////////////////////////////////
	// 1st step - move max/min values to range 1...10
	float maxLog = -100;
	if(useMax)
	{
		maxLog = (float)floor(log10(fabs(useMax)));
	}
	float minLog = -100;
	if(useMin)
	{
		minLog = (float)floor(log10(fabs(useMin)));
	}
	float useLog = maxOf2(minLog, maxLog);
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("VerticalAxisWidget::prepareLinScale(): useMin %g useMax %g useLog %f\n", useMin, useMax, useLog);
#else
		printf("VerticalAxisWidget::prepareLinScale(): useMin %g useMax %g useLog %f\n", useMin, useMax, useLog);
		fflush(stdout);
#endif
	}
	if((useLog < 3) && (useLog > -3))
	{
		linScaleMulti = 1;
		linScaleLog = 0;
	}
	else
	{
		linScaleLog = useLog;
		linScaleMulti = (float)pow(10, useLog);
		useMin /= linScaleMulti;
		useMax /= linScaleMulti;
	}
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("VerticalAxisWidget::prepareLinScale(): Intermediate: useMin %g useMax %g linScaleLog %f\n", useMin, useMax, linScaleLog);
#else
		printf("VerticalAxisWidget::prepareLinScale(): Intermediate: useMin %g useMax %g linScaleLog %f\n", useMin, useMax, linScaleLog);
		fflush(stdout);
#endif
	}
	
	// Move the whole values range to range 10...100 - then it's easy
	// to decide on scale parameters
	float full = useMax - useMin;
	float valueShift = 0;
	while(full > 100.)
	{
		full /= 10.0;
		valueShift += 1.0;
	}
	while(full < 10.)
	{
		full *= 10.0;
		valueShift -= 1.0;
	}

	// Decide what is the step for labeled ticks
	if(full < 15.0)
	{
		labelsStep = 2.0;
	}
	else if(full < 39.0)
	{
		labelsStep = 5.0;
	}
	else if(full < 80.0)
	{
		labelsStep = 10.0;
	}
	else
	{
		labelsStep = 10.0;
	}

	// Calculate final parameters for scale
	int nMin = (int)floor(useMin / pow((float)10.0, valueShift));
	int nMax = (int)ceil(useMax / pow((float)10.0, valueShift));
	if((nMax * pow((float)10.0, valueShift)) < useMax)
	{
		nMax++;
	}

	scaleMin = labelsStart = (float)nMin;
	scaleMax = (float)nMax;

	while(fmod(labelsStart, labelsStep) >= 1.0)
	{
		labelsStart += 1.0;
	}

	// Convert calculated values back to original range
	scaleMin *= (float)pow((float)10.0, valueShift);
	scaleMax *= (float)pow((float)10.0, valueShift);
	labelsStep *= (float)pow((float)10.0, valueShift);
	labelsStart *= (float)pow((float)10.0, valueShift);

	// Calculate format for labels.
	float nDig = (float)log10(labelsStep);
	int nBeforeDot = 0, nAfterDot = 0;
	if(labelsStart != 0)
	{
		nBeforeDot = (int)log10(fabs(labelsStart));
		if(labelsStart < 0)
		{
			nBeforeDot++;
		}
	}

	int maxPow = (int)log10(fabs(scaleMax));	// / linScaleMulti);
	if(maxPow > nBeforeDot)
	{
		nBeforeDot = maxPow;
	}
	if(nDig < 0.0)
	{
		nAfterDot = - (int)floor(nDig);
	}
#ifdef Q_OS_WIN
	sprintf_s(linLabelFormat, sizeof(linLabelFormat) / sizeof(linLabelFormat[0]), "%%%d.%df", (nBeforeDot + nAfterDot + 1), nAfterDot);
#else
	sprintf(linLabelFormat, "%%%d.%df", (nBeforeDot + nAfterDot + 1), nAfterDot);
#endif
	/*
	labelsStart *= (float)pow(10.0, valueShift);
	labelsStep *= (float)pow(10.0, valueShift);
	*/

	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("labelsStart %g labelsStep %g scaleMin %g scaleMax %g: nDig %f nBeforeDot %d nAfterDot %d: format <%s>\n",
			labelsStart,labelsStep, scaleMin, scaleMax, nDig, nBeforeDot, nAfterDot, linLabelFormat);
#else
		printf("labelsStart %g labelsStep %g scaleMin %g scaleMax %g: nDig %f nBeforeDot %d nAfterDot %d: format <%s>\n",
			labelsStart,labelsStep, scaleMin, scaleMax, nDig, nBeforeDot, nAfterDot, linLabelFormat);
		fflush(stdout);
#endif
	}
}

/*
**	FUNCTION
**		Prepare min/max/formats for linear scale - value is integer by it's nature,
**		for example - FillNumber in LHC
**
**	PARAMETERS
**		useMin	- Minimum value to use during calculation
**		useMax	- Maximum value to use during calculation
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::prepareLinScaleInteger(float useMin, float useMax)
{
#ifdef Q_OS_WIN
	strcpy_s(linLabelFormat, sizeof(linLabelFormat) / sizeof(linLabelFormat[0]), "%.0f");
#else
	strcpy(linLabelFormat, "%.0f");
#endif
	linScaleMulti = 1;
	linScaleLog = 0;
	if(useMin == useMax)
	{
		scaleMin = useMin - 1;
		scaleMax = useMin + 1;
		labelsStart = scaleMin;
		labelsStep = 1;
	}
	else
	{
		// Move the whole values range to range 10...100 - then it's easy
		// to decide on scale parameters
		float full = useMax - useMin;
		float valueShift = 0;
		while(full > 100.)
		{
			full /= 10.0;
			valueShift += 1.0;
		}
		while(full < 10.)
		{
			full *= 10.0;
			valueShift -= 1.0;
		}

		// Decide what is the step for labeled ticks
		if(full < 15.0)
		{
			labelsStep = 2.0;
		}
		else if(full < 39.0)
		{
			labelsStep = 5.0;
		}
		else if(full < 80.0)
		{
			labelsStep = 10.0;
		}
		else
		{
			labelsStep = 10.0;
		}

		// Calculate final parameters for scale
		int nMin = (int)floor(useMin / pow((float)10.0, valueShift));
		int nMax = (int)ceil(useMax / pow((float)10.0, valueShift));
		if((nMax * pow((float)10.0, valueShift)) < useMax)
		{
			nMax++;
		}

		scaleMin = labelsStart = (float)nMin;
		scaleMax = (float)nMax;

		// Convert calculated values back to original range
		scaleMin *= (float)pow((float)10.0, valueShift);
		scaleMin = floor(scaleMin);
		scaleMax *= (float)pow((float)10.0, valueShift);
		scaleMax = ceil(scaleMax);
		labelsStep *= (float)pow((float)10.0, valueShift);
		if(labelsStep < 1)
		{
			labelsStep = 1;
		}
		labelsStart *= (float)pow((float)10.0, valueShift);
		labelsStart = floor(labelsStart);

		while(fmod(labelsStart, labelsStep) >= 1.0)
		{
			labelsStart += 1.0;
		}
		/*
		printf("labelsStart %g labelsStep %g scaleMin %g scaleMax %g\n",
			labelsStart,labelsStep, scaleMin, scaleMax);
		fflush(stdout);
		*/
	}
}

/*
**	FUNCTION
**		Draw logarithmic scale
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::drawLogScale(QPainter &painter)
{
	int tickNbr = 0,
		labelMin = (int)ceil(log10(scaleMin)),
		labelMax = (int)floor(log10(scaleMax));
/*
printf("drawLog(): labelMin %d labelMax %d\n", labelMin, labelMax);
fflush(stdout);
*/
	painter.setPen(active ? Qt::black : Qt::darkGray);
	QFontMetrics fm = painter.fontMetrics();
	for(int value = labelMin ; value <= labelMax ; value++)
	{
		int tickY;
		logValueToScreen(value, tickY);
/*
printf("    value %d tickY %d\n", value, tickY);
fflush(stdout);
*/
		// Pressure scale is always on the left, other scales - on the right
		if(leftOfPlot)
		{
			painter.drawLine(width() - 4, tickY, width(), tickY);
		}
		else
		{
			painter.drawLine(0, tickY, 4, tickY);
		}
		// Draw label if required
		bool drawLabel = true;
		if(drawEvery > 1)
		{
			if(++tickNbr == drawEvery)
			{
				tickNbr = 0;
			}
			else
			{
				drawLabel = false;
			}
		}
		if(drawLabel)
		{
			char label[64];
			makeLogLabel(value, label, sizeof(label) / sizeof(label[0]));
			QRect rect = fm.boundingRect(label);
			if(leftOfPlot)
			{
				painter.drawText(0, tickY + (rect.height() >> 1)  - fm.descent(), label);
			}
			else
			{
				painter.drawText(4, tickY + (rect.height() >> 1) - fm.descent(), label);
			}
		}
		// Draw subticks - no labels, no grid lines
		for(int subTic = 2 ; subTic < 10 ; subTic++)
		{
			double subTickValue = log10(subTic * pow(10., value));
			int subTickY;
			ValueRange result = logValueToScreen(subTickValue, subTickY);
/*
printf("subTickValue %g result %d subTickY %d\n", subTickValue, result, subTickY);
fflush(stdout);
*/
			if(result == WithinRange)
			{
				/*
				if(fabs(subTickY - tickY) < 3)
				{
					break;	// too close to each other
				}
				*/
				if(leftOfPlot)
				{
					painter.drawLine(width() - 2, subTickY, width(), subTickY);
				}
				else
				{
					painter.drawLine(0, subTickY, 3, subTickY);
				}
			}
			tickY = subTickY;
		}
	}
}

/*
**	FUNCTION
**		Draw logarithmic scale on plot area
**
**	PARAMETERS
**		painter	- Painter used for drawing
**		plotArea	- Area available for drawing on plot
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::drawOnPlotLogScale(QPainter &painter, const QRect &plotArea)
{
	painter.setPen(Qt::black);
	int labelMin = (int)ceil(log10(scaleMin)),
		labelMax = (int)floor(log10(scaleMax));
	for(int value = labelMin ; value <= labelMax ; value++)
	{
		int tickY;
		logValueToScreen(value, tickY);

		painter.drawLine(0, tickY, plotArea.width(), tickY);
		// Draw subticks - no labels, no grid lines
		for(int subTic = 2 ; subTic < 10 ; subTic++)
		{
			double subTickValue = log10(subTic * pow(10., value));
			int subTickY;
			ValueRange result = logValueToScreen(subTickValue, subTickY);
			if(result == WithinRange)
			{
				/*
				if(fabs(subTickY - tickY) < 3)
				{
					break;	// too close to each other
				}
				*/
				painter.drawLine(0, subTickY, 2, subTickY);
				painter.drawLine(plotArea.width() - 3, subTickY, plotArea.width() - 1, subTickY);
			}
			tickY = subTickY;
		}
	}
}

/*
**	FUNCTION
**		Draw linear scale
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::drawLinScale(QPainter &painter)
{
	painter.setPen(active ? Qt::black : Qt::darkGray);

	int tickNbr = 0;
	float subStep = labelsStep / 5;
	QFontMetrics fm = painter.fontMetrics();
	for(float value = labelsStart ; value <= scaleMax ; value += labelsStep)
	{
		int tickY;
		float valueForPos = value * linScaleMulti;
		
		valueToScreen(valueForPos, tickY);
		if(DebugCtl::isAxis())
		{
#ifdef Q_OS_WIN
			qDebug("VerticalAxisWidget::drawLinScale(): next value %g Y %d\n", valueForPos, tickY);
#else
			printf("VerticalAxisWidget::drawLinScale(): next value %g Y %d\n", valueForPos, tickY);
			fflush(stdout);
#endif
		}

		// Draw tick outside of graph area
		if(leftOfPlot)
		{
			painter.drawLine(width() - 4, tickY, width(), tickY);
		}
		else
		{
			painter.drawLine(0, tickY, 4, tickY);
		}
		// Draw label
		bool drawLabel = true;
		if(drawEvery > 1)
		{
			if(++tickNbr == drawEvery)
			{
				tickNbr = 0;
			}
			else
			{
				drawLabel = false;
			}
		}
		if(drawLabel)
		{
			char label[64];
			makeLinLabel(value, label, sizeof(label) / sizeof(label[0]));
			QRect rect = fm.boundingRect(label);
			if(leftOfPlot)
			{
				painter.drawText(width() - 4 - rect.width(), tickY + (rect.height() >> 1) - fm.descent(), label);
			}
			else
			{
				painter.drawText(4, tickY + (rect.height() >> 1) - fm.descent(), label);
			}
		}

		// Subticks - 5 per tick
		// Not needed for bit state
		if(valueType != DataEnum::BitState)
		{
			for(int n = 1 ; n < 5 ; n++)
			{
				float subValue = value + n * subStep;
				if(subValue > scaleMax)
				{
					break;
				}
				valueForPos = subValue * linScaleMulti;
		
				valueToScreen(valueForPos, tickY);

				if(leftOfPlot)
				{
					painter.drawLine(width() - 2, tickY, width(), tickY);
				}
				else
				{
					painter.drawLine(0, tickY, 2, tickY);
				}
			}
		}
	}
}

/*
**	FUNCTION
**		Draw linear scale on plot area
**
**	PARAMETERS
**		painter	- Painter used for drawing
**		plotArea	- Area available for drawing on plot
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::drawOnPlotLinScale(QPainter &painter, const QRect &plotArea)
{
	painter.setPen(Qt::black);

	float subStep = labelsStep / 5;
	for(float value = labelsStart ; value <= scaleMax ; value += labelsStep)
	{
		int tickY;
		float valueForPos = value * linScaleMulti;
		valueToScreen(valueForPos, tickY);
		painter.drawLine(0, tickY, plotArea.width() - 1, tickY);

		// Subticks - 5 per tick
		for(int n = 1 ; n < 5 ; n++)
		{
			float subValue = value + n * subStep;
			if(subValue > scaleMax)
			{
				break;
			}
			valueForPos = subValue * linScaleMulti;
		
			valueToScreen(valueForPos, tickY);

			painter.drawLine(0, tickY, 2, tickY);
			painter.drawLine(plotArea.width() - 3, tickY, plotArea.width() - 1, tickY);
		}
	}
}

/*
**	FUNCTION
**		Convert value to screen Y-coordinate for logarithmic scale
**
**	PARAMETERS
**		value	- value to be converted
**		screenY	- resulting Y-coordinate will be returned here
**
**	RETURNS
**		enumeration - position of value with respect to axis limits
**
**	CAUTIONS
**		None
*/
VerticalAxisWidget::ValueRange VerticalAxisWidget::valueToScreenLog(float value, int &screenY)
{
	if(value <= 0)
	{
		screenY = height() - 1;
		return BelowAxis;
	}
	double valueLog = log10(value);
	if(valueLog < logScaleMin)
	{
		screenY = height() - 1;
		return BelowAxis;
	}
	else if(valueLog > logScaleMax)
	{
		screenY = 0;
		return AboveAxis;
	}
	screenY = height() - 1 - (int)rint((double)height() * (valueLog - logScaleMin) /
		(logScaleMax - logScaleMin));
	return WithinRange;
}

/*
**	FUNCTION
**		Convert value to screen Y-coordinate for linear scale
**
**	PARAMETERS
**		value	- value to be converted
**		screenY	- resulting Y-coordinate will be returned here
**
**	RETURNS
**		enumeration - position of value with respect to axis limits
**
**	CAUTIONS
**		None
*/
VerticalAxisWidget::ValueRange VerticalAxisWidget::valueToScreenLin(float value, int &screenY)
{
	if(linScaleLog)
	{
		value /= linScaleMulti;
	}
	if(value < scaleMin)
	{
		screenY = height() - 1;
		return BelowAxis;
	}
	else if(value > scaleMax)
	{
		screenY = 0;
		return AboveAxis;
	}
	screenY = height() - 1 - (int)rint((double)height() * (value - scaleMin) /
		(scaleMax - scaleMin));
	return WithinRange;
}

/*
**	FUNCTION
**		Prepare string representation of given logarithmic axis value.
**
**	PARAMETERS
**		value	- Value to be shown on axis
**		buf		- Pointer to character buffer where this method will put result
**
**	RETURNS
**		None
**
**	CAUTIONS
**		It is responsability of caller to allocate large enough memory block for buf.
*/
void VerticalAxisWidget::makeLogLabel(int value, char *buf,
#ifdef Q_OS_WIN
	size_t bufLen)
#else
	size_t /* bufLen */)
#endif
{
	if((value < 0) || (value > 2))
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, bufLen, "1E%d", value);
#else
		sprintf(buf, "1E%d", value);
#endif
	}
	else
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, bufLen, "%.0f", (float)pow(10., (double)value));
#else
		sprintf(buf, "%.0f", (float)pow(10., (double)value));
#endif
	}
}

/*
**	FUNCTION
**		Prepare string representation of given linear axis value.
**
**	PARAMETERS
**		value	- Value to be shown on axis
**		buf		- Pointer to character buffer where this method will put result
**
** RETURNS
**		None
**
** CAUTIONS
**		It is responsability of caller to allocate large enough memory block for buf.
*/
void VerticalAxisWidget::makeLinLabel(float value,  char *buf,
#ifdef Q_OS_WIN
	size_t bufLen)
#else
	size_t /* bufLen */)
#endif

{
#ifdef Q_OS_WIN
	sprintf_s(buf, bufLen, linLabelFormat, value);
#else
	sprintf(buf, linLabelFormat, value);
#endif
}

/*
**	FUNCTION
**		Return suffix for resource name
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Pointer to string with resource suffix, or
**		NULL if value type is not supported
**
**	CAUTIONS
**		None
*/
const char *VerticalAxisWidget::getResourceSuffix(void) const
{
	switch(valueType)
	{
	case DataEnum::Pressure:
		return ".Pressure";
	case DataEnum::HighPressure:
		return ".HighPressure";
	case DataEnum::CryoTemperature:
		return ".CryoTemperature";
	case DataEnum::Intensity:
		return ".Intensity";
	case DataEnum::Temperature:
		return ".Temperature";
	case DataEnum::Energy:
		return ".Energy";
	case DataEnum::BeamCurrent:
		return ".BeamCurrent";
	case DataEnum::CritEnergy:
		return ".CritEnergy";
	case DataEnum::PhF:
		return ".PhF";
	case DataEnum::PhDm:
		return ".PhDm";
	case DataEnum::PhDmA:
		return ".PhDmA";
	case DataEnum::BitState:
		return ".BitState";
	case DataEnum::CurrentVRPM:
		return ".CurrentVRPM";
	case DataEnum::VoltageVRPM:
		return ".VoltageVRPM";
	case DataEnum::HighVoltage:
		return ".HighVoltage";
	case DataEnum::FillNumber:
		return ".FillNumber";
	case DataEnum::VvSetPoint:
		return ".VvSetPoint";
	case DataEnum::Percentage:
		return ".Percentage";
	case DataEnum::LowCurrent:
		return ".LowCurrent";
	case DataEnum::Status:
		return ".Status";
	case DataEnum::mA:
	    return ".mA";
	default:
		break;
	}
	return NULL;
}

/*
**	FUNCTION
**		Return name of units for this axis
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Pointer to string with name of units
**
**	CAUTIONS
**		None
*/
const char *VerticalAxisWidget::getUnit(void) const
{
	static  char result[256];
	if(valueType == DataEnum::BitState)
	{
		return "bit";
	}
	QString resourceName("VerticalAxis.Unit");
	const char *suffix = getResourceSuffix();
	if(suffix)
	{
		resourceName += suffix;
	}
	ResourcePool &pool = ResourcePool::getInstance();
	QString value;
	if(pool.getStringValue(resourceName, value) == ResourcePool::OK)
	{
#ifdef Q_OS_WIN
		strcpy_s(result, sizeof(result) / sizeof(result[0]), value.toLatin1().constData());
#else
		strcpy(result, value.toLatin1().constData());
#endif
	}
	else
	{
		result[0] = '\0';
	}
	return result;
}

float VerticalAxisWidget::getValueCoeff(void)
{
	if(gotValueCoeff)
	{
		return valueCoeff;
	}
	float result = 1;
	QString resourceName("VerticalAxis.Coeff");
	const char *suffix = getResourceSuffix();
	if(suffix)
	{
		resourceName += suffix;
	}
	ResourcePool &pool = ResourcePool::getInstance();
	if(pool.getFloatValue(resourceName, result) == ResourcePool::OK)
	{
		valueCoeff = result;
	}
	gotValueCoeff = true;
	return valueCoeff;
}

/*
**	FUNCTION
**		Set default axis type (lin/log) and limits
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::setDefaultLimits(void)
{
	setDefaultInitialLimits();
	if(initialLimitsSet)
	{
		return;
	}

	else
	{
		logScale = true;
		logScaleMin = logScaleMax = 0;

		QString resourceName("VerticalAxis.LogScale");
		const char *suffix = getResourceSuffix();
		if(suffix)
		{
			resourceName += suffix;
		}
		ResourcePool &pool = ResourcePool::getInstance();
		if(pool.getBoolValue(resourceName, logScale) != ResourcePool::OK)
		{
			logScale = true;
		}

		min = getAbsMin();
		max = getAbsMax();
	}
}

/*
**	FUNCTION
**		Set default axis type (lin/log) and initial limits. For some scale
**		types it is more natural to have some 'good' initial limits instead
**		of setting them based on first (few) data. One example is beam energy
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VerticalAxisWidget::setDefaultInitialLimits(void)
{
	// For bit state - [0..1] is the only reasonable range
	if(valueType == DataEnum::BitState)
	{
		logScale = false;
		min = (float)-0.01;
		max = (float)1.01;
		initialLimitsSet = true;
		return;
	}

	// For other scale types initial limits shall come from resource file
	QString resourceName("VerticalAxis.LogScale");
	const char *suffix = getResourceSuffix();
	if(suffix)
	{
		resourceName += suffix;
	}
	ResourcePool &pool = ResourcePool::getInstance();
	if(pool.getBoolValue(resourceName, logScale) != ResourcePool::OK)
	{
		logScale = true;
	}

	resourceName = "VerticalAxis.InitialLimitsFixed";
	if(suffix)
	{
		resourceName += suffix;
	}
	bool initialFixed = false;
	if(pool.getBoolValue(resourceName, initialFixed) != ResourcePool::OK)
	{
		return;
	}
	if(!initialFixed)
	{
		return;
	}

	// The fact that initial limits are fixed is NOT enough: in addition
	// resource file shall conatins values for both min and max in fixed mode
	resourceName = "VerticalAxis.InitialMin";
	if(suffix)
	{
		resourceName += suffix;
	}
	float initialMin = 0;
	if(pool.getFloatValue(resourceName, initialMin) != ResourcePool::OK)
	{
		return;
	}
	
	resourceName = "VerticalAxis.InitialMax";
	if(suffix)
	{
		resourceName += suffix;
	}
	float initialMax = 0;
	if(pool.getFloatValue(resourceName, initialMax) != ResourcePool::OK)
	{
		return;
	}
	if(initialMin >= initialMax)
	{
		return;
	}
	setLimits(initialMin, initialMax);
	initialLimitsSet = true;
}

/*
**	FUNCTION
**		Return 'absolute' minimum for given value type
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Absolute minimum value
**
**	CAUTIONS
**		None
*/
float VerticalAxisWidget::getAbsMin(void)
{
	float result = 0;
	QString resourceName("VerticalAxis.AbsMin");
	if(logScale)
	{
		resourceName += ".Log";
	}
	else
	{
		resourceName += ".Lin";
	}
	const char *suffix = getResourceSuffix();
	if(suffix)
	{
		resourceName += suffix;
	}
	ResourcePool &pool = ResourcePool::getInstance();
	if(pool.getFloatValue(resourceName, result) != ResourcePool::OK)
	{
		result = logScale ? 1E-13 : 0;
	}
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("VerticalAxisWidget::getAbsMin() =  %g\n", result);
#else
		printf("MIN: VerticalAxisWidget::getAbsMin() =  %g\n", result);
		fflush(stdout);
#endif
	}
	return result;
}

/*
**	FUNCTION
**		Return 'absolute' maximum for given value type
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Absolute maximum value
**
**	CAUTIONS
**		None
*/
float VerticalAxisWidget::getAbsMax(void)
{
	float result = 0;
	QString resourceName("VerticalAxis.AbsMax");
	if(logScale)
	{
		resourceName += ".Log";
	}
	else
	{
		resourceName += ".Lin";
	}
	const char *suffix = getResourceSuffix();
	if(suffix)
	{
		resourceName += suffix;
	}
	ResourcePool &pool = ResourcePool::getInstance();
	if(pool.getFloatValue(resourceName, result) != ResourcePool::OK)
	{
		result = (float)1E30;
	}
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("VerticalAxisWidget::getAbsMax() =  %g\n", result);
#else
		printf("MIN: VerticalAxisWidget::getAbsMax() =  %g\n", result);
		fflush(stdout);
#endif
	}
	return result;
}

void VerticalAxisWidget::formatValue(char *buf, float value,
#ifdef Q_OS_WIN
	size_t bufLen)
#else
	size_t /* bufLen */)
#endif

{
	const char *valueFmt;
	switch(valueType)
	{
	case DataEnum::Pressure:
	case DataEnum::HighPressure:
	case DataEnum::Intensity:
	case DataEnum::BeamCurrent:
	case DataEnum::LowCurrent:
		valueFmt = "%8.2E";
		break;
	case DataEnum::CryoTemperature:
	case DataEnum::Temperature:
	case DataEnum::Energy:
		valueFmt = "%8.2f";
		break;
	case DataEnum::CritEnergy:
		valueFmt = "%7.6f";
		break;
	case DataEnum::PhF:
	case DataEnum::PhDm:
	case DataEnum::PhDmA:
		valueFmt = "%8.2E";
		break;
	case DataEnum::BitState:
		//valueFmt = "%2.0f";
		//break;
	case DataEnum::Percentage:
		valueFmt = "%2.0f";
		break;
	case DataEnum::CurrentVRPM:
		valueFmt = "%8.3E";
		break;
	case DataEnum::VoltageVRPM:
		valueFmt = "%8.2E";
		break;
	case DataEnum::FillNumber:
		valueFmt = "%9.0f";
		break;
	case DataEnum::VvSetPoint:
		valueFmt = "%7.2f";
		break;
	case DataEnum::Status:
		valueFmt = "%d";
		break;
	default:
		valueFmt = "%g";
		break;
	}
	char format[128];
#ifdef Q_OS_WIN
	sprintf_s(format, sizeof(format) / sizeof(format[0]), "%s %s", valueFmt, getUnit());
	sprintf_s(buf, bufLen, format, value);
#else
	sprintf(format, "%s %s", valueFmt, getUnit());
	sprintf(buf, format, value);
#endif
}


void VerticalAxisWidget::mousePressEvent(QMouseEvent *pEvent)
{
	if(valueType == DataEnum::BitState)
	{
		return;	// No menu
	}
	if((pEvent->button() & Qt::MouseButtonMask) !=  Qt::RightButton)
	{
		return;
	}

	QMenu *pMenu = new QMenu(this);
	const char *unit = getUnit();
	QString title(unit);
	if(unit[0])
	{
		title += " scale mode";
	}
	else
	{
		title = "Scale mode";
	}
	pMenu->addAction(title);
	pMenu->addSeparator();

	QAction *pAction = pMenu->addAction("Logarithmic", this, SLOT(logAxisMenu()));
	pAction->setData(1);
	pAction->setCheckable(true);
	pAction->setChecked(logScale);
	pAction = pMenu->addAction("Linear", this, SLOT(linAxisMenu()));
	pAction->setData(0);
	pAction->setCheckable(true);
	pAction->setChecked(!logScale);

	pMenu->exec(QCursor::pos());
	delete pMenu;
}

void VerticalAxisWidget::linAxisMenu(void)
{
	setLogScale(false);
}

void VerticalAxisWidget::logAxisMenu(void)
{
	setLogScale(true);
}

void VerticalAxisWidget::mouseMoveEvent(QMouseEvent *pEvent)
{
	if(valueType == DataEnum::BitState)
	{
		return;	// No limits change
	}
	if(pEvent->y() < (height() >> 3))	// Possibility to control scale max
	{
		setCursor(Qt::SizeVerCursor);
		editMax = true;
		int posX = getLimitButtonPosX();
		if(posX >= 0)
		{
			int posY = 0;
			if(!scaleLabel.isEmpty())
			{
				QRect rect = fontMetrics().boundingRect(scaleLabel);
				posY = rect.height();
			}
			const char *unit = getUnit();
			if(unit[0])
			{
				QRect rect = fontMetrics().boundingRect(unit);
				posY += rect.height();
			}
			pUpButton->move(posX, posY);
			pDownButton->move(posX, posY + pUpButton->height());
			pUpButton->show();
			pDownButton->show();
		}
		else
		{
			pUpButton->hide();
			pDownButton->hide();
		}
	}
	else if(pEvent->y() > (height() - (height() >> 3)))	// Possibility to control scale min
	{
		editMax = false;
		setCursor(Qt::SizeVerCursor);
		int posX = getLimitButtonPosX();
		if(posX >= 0)
		{
			int posY = height() - pDownButton->height() - 1;
			pDownButton->move(posX, posY);
			pUpButton->move(posX, posY - pDownButton->height());
			pUpButton->show();
			pDownButton->show();
		}
		else
		{
			pUpButton->hide();
			pDownButton->hide();
		}
	}
	else
	{
		unsetCursor();
		if(!pUpButton->isHidden())
		{
			pUpButton->hide();
			pDownButton->hide();
		}
	}
}

int VerticalAxisWidget::getLimitButtonPosX(void)
{
	int posX = 0;
	switch(valueType)
	{
	case DataEnum::BitState:
		posX = -1;
		break;
	default:
		posX = leftOfPlot ? 0 : width() - pUpButton->width() - 1;
		break;
	}
	return posX;
}

void VerticalAxisWidget::wheelEvent(QWheelEvent *pEvent)
{
	QPoint pos = mapFromGlobal(QCursor::pos());
	if(!rect().contains(pos))
	{
		return;
	}

	shiftClick = (pEvent->modifiers() & Qt::ShiftModifier) != 0;
	if(pos.y() < (height() >> 3))	// Edit maximum
	{
		if(DebugCtl::isAxis())
		{
#ifdef Q_OS_WIN
			qDebug("MAX: delta %d\n", pEvent->delta());
#else
			printf("MAX: delta %d\n", pEvent->delta());
			fflush(stdout);
#endif
		}
		pEvent->accept();
		editMax = true;
		if(pEvent->delta() > 0)
		{
			upButtonClick();
		}
		else
		{
			downButtonClick();
		}
	}
	else if(pos.y() > (height() - (height() >> 3)))	// Edit minimum
	{
		if(DebugCtl::isAxis())
		{
#ifdef Q_OS_WIN
			qDebug("MIN: delta %d\n", pEvent->delta());
#else
			printf("MIN: delta %d\n", pEvent->delta());
			fflush(stdout);
#endif
		}
		pEvent->accept();
		editMax = false;
		if(pEvent->delta() > 0)
		{
			upButtonClick();
		}
		else
		{
			downButtonClick();
		}
	}
	else
	{
		pEvent->ignore();
	}
}

void VerticalAxisWidget::upButtonClick(void)
{
	if(logScale)
	{
		if(editMax)
		{
			logMaxUp();
		}
		else
		{
			logMinUp();
		}
	}
	else
	{
		if(editMax)
		{
			linMaxUp();
		}
		else
		{
			linMinUp();
		}
	}
}

void VerticalAxisWidget::downButtonClick(void)
{
	if(logScale)
	{
		if(editMax)
		{
			logMaxDown();
		}
		else
		{
			logMinDown();
		}
	}
	else
	{
		if(editMax)
		{
			linMaxDown();
		}
		else
		{
			linMinDown();
		}
	}
}

void VerticalAxisWidget::keyPressEvent(QKeyEvent *pEvent)
{
	if(cursor().shape() != Qt::SizeVerCursor)
	{
		pEvent->ignore();
		return;
	}
	if(!rect().contains(mapFromGlobal(QCursor::pos())))
	{
		pEvent->ignore();
		return;
	}
	shiftClick = (pEvent->modifiers() & Qt::ShiftModifier) != 0;
	switch(pEvent->key())
	{
	case Qt::Key_Up:
		upButtonClick();
		break;
	case Qt::Key_Down:
		downButtonClick();
		break;
	default:
		pEvent->ignore();
		break;
	}
}

bool VerticalAxisWidget::eventFilter(QObject *pObject, QEvent *pEvent)
{
	if((pObject == pUpButton) || (pObject == pDownButton))
	{
		QMouseEvent *pMouseEvent;
		QKeyEvent *pKeyEvent;
		switch(pEvent->type())
		{
		case QEvent::MouseButtonRelease:
			pMouseEvent = static_cast<QMouseEvent *>(pEvent);
			shiftClick = (pMouseEvent->modifiers() & Qt::ShiftModifier) != 0;
			break;
		case QEvent::KeyRelease:
			pKeyEvent = static_cast<QKeyEvent *>(pEvent);
			shiftClick = (pKeyEvent->modifiers() & Qt::ShiftModifier) != 0;
			break;
		default:
			break;
		}
	}
	return QWidget::eventFilter(pObject, pEvent);
}

void VerticalAxisWidget::logMinDown(void)
{
	float useMin, useMax;
	getLimits(useMin, useMax);
	useMin = pow(10, rint(log10(useMin)) - 1);
	float absMin = getAbsMin();
	if(useMin < absMin)
	{
		return;
	}
	setLimits(useMin, useMax);
}

void VerticalAxisWidget::logMinUp(void)
{
	float useMin, useMax;
	getLimits(useMin, useMax);
	useMin = pow(10, rint(log10(useMin)) + 1);
	if(useMin >= useMax)
	{
		useMax = pow(10, rint(log10(useMin)) + 1);
		float absMax = getAbsMax();
		if(useMax > absMax)
		{
			return;
		}
	}
	setLimits(useMin, useMax);
}

void VerticalAxisWidget::logMaxDown(void)
{
	float useMin, useMax;
	getLimits(useMin, useMax);
	useMax = pow(10, rint(log10(useMax)) - 1);
	if(useMin >= useMax)
	{
		useMin = pow(10, rint(log10(useMax)) - 1);
		float absMin = getAbsMin();
		if(useMin < absMin)
		{
			return;
		}
	}
	setLimits(useMin, useMax);
}

void VerticalAxisWidget::logMaxUp(void)
{
	float useMin, useMax;
	getLimits(useMin, useMax);
	if(DebugCtl::isAxis())
	{
#ifdef Q_OS_WIN
		qDebug("VerticalAxisWidget::logMaxUp(): initial %g log10 %g rint %g power %g result %g\n",
			useMax, log10(useMax), rint(log10(useMax)), rint(log10(useMax)) + 1, pow(10, rint(log10(useMax)) + 1));
#else
		printf("VerticalAxisWidget::logMaxUp(): initial %g power %g result %g\n",
			useMax, rint(log10(useMax)) + 1, pow(10, rint(log10(useMax)) + 1));
		fflush(stdout);
#endif
	}
	useMax = pow(10, rint(log10(useMax)) + 1);
	float absMax = getAbsMax();
	if(useMax > absMax)
	{
		return;
	}
	setLimits(useMin, useMax);
}

void VerticalAxisWidget::linMinDown(void)
{
	float	useMin, useMax;
	getLimits(useMin, useMax);
	float newMin = useMin - getLinStep(useMin, useMax, false);
	float absMin = getAbsMin();
	if(newMin < absMin)
	{
		newMin = absMin;
	}
	if(newMin < useMin)
	{
		setLimits(newMin, useMax);
	}
}

void VerticalAxisWidget::linMinUp(void)
{
	float	useMin, useMax;
	getLimits(useMin, useMax);
	float delta = getLinStep(useMin, useMax, true);
	float newMin = useMin + delta;
	if(newMin >= useMax)
	{
		useMax += delta;
		if(useMax > getAbsMax())
		{
			return;
		}
	}
	setLimits(newMin, useMax);
}

void VerticalAxisWidget::linMaxDown(void)
{
	float	useMin, useMax;
	getLimits(useMin, useMax);
	float delta = getLinStep(useMin, useMax, true);
	float newMax = useMax - delta;
	if(newMax <= useMin)
	{
		useMin -= delta;
		if(useMin < getAbsMin())
		{
			return;
		}
	}
	setLimits(useMin, newMax);
}

void VerticalAxisWidget::linMaxUp(void)
{
	float	useMin, useMax;
	getLimits(useMin, useMax);
	float newMax = useMax + getLinStep(useMin, useMax, false);
	float absMax = getAbsMax();
	if(newMax > absMax)
	{
		newMax = absMax;
	}
	if(newMax > useMax)
	{
		setLimits(useMin, newMax);
	}
}

float VerticalAxisWidget::getLinStep(float useMin, float useMax, bool decrease)
{
	float coeff = decrease ? 0.67 : 2;
	float result = (shiftClick ? coeff : 0.1) * (useMax - useMin);
	switch(valueType)
	{
	case DataEnum::FillNumber:
		if(result < 1)
		{
			result = 1;
		}
		break;
	default:
		break;
	}
	return result;
}


void VerticalAxisWidget::setShowUnit(bool flag)
{
	if(flag == showUnit)
	{
		return;
	}
	needPrepare = true;
	showUnit = flag;
	update();
	emit limitsChanged(false);
}

void VerticalAxisWidget::setLogScale(bool flag)
{
	if(flag == logScale)
	{
		return;
	}
	if(logScale)
	{
		temporaryLinear = true;
	}
	needPrepare = true;
	logScale = flag;

	// When switching from linear to logarithmic scale - limits shall be adjusted
	// such that scale includes at least 1 order of magnitude
	if(logScale)
	{
		if(temporaryLinear)
		{
			setLimits((float)pow(10, ceil(logScaleMin)), (float)pow(10, floor(logScaleMax)));
		}
		else
		{
			float useMin, useMax;
			getLimits(useMin, useMax);
			if(useMax > 0)
			{
				useMax = (float)pow(10, ceil(log10(useMax)));
			}
			else
			{
				useMax = (float)pow(10, floor(logScaleMax));
			}
			if(useMin > 0)
			{
				useMin = (float)pow(10, floor(log10(useMin)));
			}
			else
			{
				useMin = (float)pow(10, ceil(logScaleMin));
			}
			if(useMin == useMax)
			{
				useMin /= 10;
				useMax /= 10;
			}
			else if(useMin > useMax)
			{
				float tmp = useMin;
				useMin = useMax;
				useMax = tmp;
			}
			setLimits(useMin, useMax);
		}
	}
	update();
	emit limitsChanged(false);
}

void VerticalAxisWidget::setLeftOfPlot(bool flag)
{
	if(flag == leftOfPlot)
	{
		return;
	}
	leftOfPlot = flag;
	update();
}

/*
**	FUNCTION
**		Dump axis parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None.
*/
void VerticalAxisWidget::dump(FILE *pFile)
{
	if(!pFile)
	{
		return;
	}
	fprintf(pFile, "############## VERTICAL AXIS Widget valueType %d log %d active %d\n", valueType,
		(int)logScale, (int)active);
	fprintf(pFile, "  width %d height %d\n", width(), height());
	fprintf(pFile, "  min %g max %g\n", min, max);
	fprintf(pFile, "  scale min %g max %g\n", scaleMin, scaleMax);
	fprintf(pFile, "\n" );
}
