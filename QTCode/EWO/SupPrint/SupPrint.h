#ifndef SUPPRINT_H
#define SUPPRINT_H

#include <stdio.h>
#include <qpushbutton.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qprinter.h>
#include <qtextdocument.h>
#include <math.h>

class SupPrint : public QPushButton
{
	Q_OBJECT

	////////////////////////////////////////////////////////////////////////////
	///////////////////// Properties exposed to PVSS panel /////////////////////
	////////////////////////////////////////////////////////////////////////////
	Q_PROPERTY(QString prName READ getPrName WRITE setPrName);
	Q_PROPERTY(QString fontName READ getFontName WRITE setFontName);
	Q_PROPERTY(int fontSize READ getFontSize WRITE setFontSize);
	Q_PROPERTY(int orient READ getOrient WRITE setOrient);
	
public:
	// Accessors for properties above
	// Construction/destruction
	SupPrint(QWidget *parent = NULL);
	~SupPrint();
	inline const QString &getPrName(void) const { return prName; }
	inline const QString &getFontName(void) const { return fontName; }
	inline const int &getFontSize(void) const { return fontSize; }
	inline const int &getOrient(void) const { return prOrient; }
	void setPrName(QString &name) { prName = name; }
	void setFontName(QString &name) { fontName = name; }
	void setFontSize(int size) { fontSize = size; }
	void setOrient(int orient) { prOrient = orient; }
	int	printText(const char *textToPrint);
	int	printFile(const char *fileName);
	int	printDialog(void);
	QColor convertColor(int rv, int gv, int bv);

signals:
	void printRequest(void);

	
private:
   QPrinter	 printer;
	QString	 prName;
	QString	 fontName;
	int		 fontSize;
	int		 prOrient;
private slots:
	void buttonClick(void);

};

#endif	// SUPPRINT_H
