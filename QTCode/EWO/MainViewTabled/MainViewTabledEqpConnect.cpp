//	Implementation of MainViewTabledEqpConnect and OneDpConnection classes
////////////////////////////////////////////////////////////////////////////////

#include "MainViewTabledEqpConnect.h"

#include "DataPool.h"
#include "Eqp.h"

#include <qdatetime.h>

OneDpConnection::OneDpConnection(Eqp *pEqp, int tag)
{
	this->pEqp = pEqp;
	this->tag = tag;
}

MainViewTabledEqpConnect::MainViewTabledEqpConnect()
{
}

MainViewTabledEqpConnect::~MainViewTabledEqpConnect()
{
	while(!dps.isEmpty())
	{
		delete dps.takeFirst();
	}
}

/*
**	FUNCTION
**		Connect to device with given DP name
**
**	ARGUMENTS
**		dp		- DP name
**		tag		- Tag for callback (signal) of this DP
**
**	RETURNS
**		-1	- DP not found
**		0	- DP + tag is already in connection list
**		1	- tag has been added to list, DP was already connected
**		2	- DP + tag have been added to list, DP connection is done
**
**	CAUTIONS
**		None
*/
int MainViewTabledEqpConnect::connectDp(const char *dp, int tag)
{
	// Find device with given DP name in data pool
	DataPool	&pool = DataPool::getInstance();
	Eqp *pEqp = pool.findEqpByDpName(dp);
	if(!pEqp)
	{
		return -1;
	}

	// Check if device with such DP (and tag) is already in list
	bool	dpFound = false;
	for(int idx = 0 ; idx < dps.count() ; idx++)
	{
		OneDpConnection *pConnect = dps.at(idx);
		if(pConnect->getEqp() == pEqp)
		{
			dpFound = true;
			if(pConnect->getTag() == tag)
			{
				return 0;
			}
		}
	}
	dps.append(new OneDpConnection(pEqp, tag));
	int coco = 1;
	if(!dpFound)
	{
		pEqp->connect(this, DataEnum::Online);
		coco++;
	}
	return coco;
}

/*
**	FUNCTION
**		Disconnect from device with given DP name
**
**	ARGUMENTS
**		dp		- DP name
**
**	RETURNS
**		-1	- DP not found
**		other - number of connections for this DP removed from list
**
**	CAUTIONS
**		None
*/
int MainViewTabledEqpConnect::disconnectDp(const char *dp)
{
	// Find device with given DP name in data pool
	DataPool	&pool = DataPool::getInstance();
	Eqp *pEqp = pool.findEqpByDpName(dp);
	if(!pEqp)
	{
		return -1;
	}

	// Check if device with such DP (and tag) is already in list
	int coco = 0;
	for(int idx = 0 ; idx < dps.count() ; idx++)
	{
		OneDpConnection *pConnect = dps.at(idx);
		if(pConnect->getEqp() == pEqp)
		{
			if(!coco)
			{
				pEqp->disconnect(this, DataEnum::Online);
			}
			coco++;
			delete dps.takeAt(idx);
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Disconnect from all devices
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Number of devices connected before this call
**
**	CAUTIONS
**		None
*/
int MainViewTabledEqpConnect::disconnectAll(void)
{
	int coco = dps.count();
	while(!dps.isEmpty())
	{
		Eqp *pEqp = dps.first()->getEqp();
		pEqp->disconnect(this, DataEnum::Online);
		delete dps.takeFirst();
	}
	return coco;
}

/*
**	FUNCTION
**		Slot receiving notifications from equipment, see InterfaceEqp class
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void MainViewTabledEqpConnect::dpeChange(Eqp *pSrc, const char * /* dpeName */,
	DataEnum::Source /* source */, const QVariant & /* value */, DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	// Only online mode calls are of interest for main view
	if(mode == DataEnum::Replay)
	{
		return;
	}
	for(int idx = 0 ; idx < dps.count() ; idx++)
	{
		OneDpConnection *pConnect = dps.at(idx);
		if(pConnect->getEqp() == pSrc)
		{
			emit dpDataChanged(pConnect);
		}
	}
}
