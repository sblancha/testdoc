#include "InterlockIconVRVCL.h"

#include "qpainter.h"

InterlockIconVRVCL::InterlockIconVRVCL(QWidget * parent) :
InterlockIcon(parent)
{
	

}

InterlockIconVRVCL::~InterlockIconVRVCL()
{

}

void InterlockIconVRVCL::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	QFont calibri("Calibri", 10, QFont::Bold);
	QFont calibriBig("Calibri", 12);
	//draw title
	QPen whitePen(Qt::white);
	bool conversionOk;
	int valveNumber = attributes.value("ValveNumber").right(1).toInt(&conversionOk);

	painter.setBrush(beamColor);
	painter.setPen(whitePen);
	painter.setFont(calibri);

	painter.drawRect(0, 0, 100, 50);
	painter.drawText(15, 0, 70, 50, Qt::AlignCenter | Qt::TextWrapAnywhere, this->name);
	
	painter.setPen(Qt::black);
	painter.setBrush(Qt::white);

	painter.drawRect(0, 55, 100, 400);
	painter.drawLine(50, 55, 50, 455);

	for (int i = 1; i <= 7; i++)
	{
		painter.drawLine(0, 55 + 50 * i, 100, 55 + 50 * i);
	}

	for (int i = 1; i <= 8; i++)
	{
		painter.drawText(0, 55 + 50 * (i - 1), 50, 50, Qt::AlignCenter, QString("INT") + QString::number(i));
		painter.drawText(50, 55 + 50 * (i - 1), 50, 50, Qt::AlignCenter, QString("VV") + QString::number(i));
	}

	if (conversionOk)
	{
		painter.drawText(100, 55 + 50 * (valveNumber - 1), 80, 50, Qt::AlignCenter, attributes.value("Cable"));
	}

}

void InterlockIconVRVCL::moveIcon(int x, int y)
{
	this->move(x, y - 450 / 2);
}

QPoint InterlockIconVRVCL::getConnectionPoint(QString connectorName)
{
	int baseX = this->x();
	int baseY = this->y();

	if (QString::compare(connectorName, "Interlock1") == 0)
	{
		baseY += 75;
	}
	else if (QString::compare(connectorName, "Interlock2") == 0)
	{
		baseY += 125;
	}
	else if (QString::compare(connectorName, "Interlock3") == 0)
	{
		baseY += 175;
	}
	else if (QString::compare(connectorName, "Interlock4") == 0)
	{
		baseY += 225;
	}
	else if (QString::compare(connectorName, "Interlock5") == 0)
	{
		baseY += 275;
	}
	else if (QString::compare(connectorName, "Interlock6") == 0)
	{
		baseY += 325;
	}
	else if (QString::compare(connectorName, "Interlock7") == 0)
	{
		baseY += 375;
	}
	else if (QString::compare(connectorName, "Interlock8") == 0)
	{
		baseY += 425;
	}

	return QPoint(baseX, baseY);
}

void InterlockIconVRVCL::getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber)
{
	direction = InterlockIconConnection::Left;
	offset = 80;
	writeCableNumber = true;
}

QRect InterlockIconVRVCL::getBoundingRect()
{
	QPoint topLeft = QPoint(this->x(), this->y());
	QRect boundingRect(topLeft, topLeft + QPoint(180, 455));

	return boundingRect;
}