#ifndef CHILDSYNLINE_H
#define	CHILDSYNLINE_H

// When synoptic line is moved - it can recognize that other (child) line(s)
// shall also be moved. In such case it will return list of this class
// instances containing parameters of line(s) which shall be moved after
//	moving parent line

#include<qstring.h>

class ChildSynLine
{
public:
	ChildSynLine(const QString &name, bool isStart);
	~ChildSynLine();

	// Access
	inline const QString &getName(void) { return name; }
	inline bool isStart(void) { return start; }

protected:
	//	Name of child line
	QString	name;

	// Flag indicating if start of child line is moved
	bool		start;
};

#endif	// CHILDSYNLINE_H
