#ifndef PROFILEITEM_H
#define	PROFILEITEM_H

// Class holding data for one bar in profile graph

#include "InterfaceEqp.h"
#include "Eqp.h"

#include <QColor>
#include <QRect>

class ProfileItem : public InterfaceEqp
{
	Q_OBJECT

public:
	ProfileItem(Eqp *pEqp, DataEnum::DataMode mode);
	~ProfileItem();

	void show();
	void hide();

	bool shallBeVisible(void);

	// Access
	inline Eqp *getEqp(void) const { return pEqp; }
	inline bool isHidden(void) const { return hidden; }
	inline bool isSelected(void) const { return selected; }

	inline bool isValueValid(void) const { return valueValid; }
	inline float getValue(void) const { return value; }

	inline void setNarrow(int coord) { rect.setLeft(coord); rect.setRight(coord); }
	inline void setWide(int left, int right) { rect.setLeft(left); rect.setRight(right); }
	inline void setTop(int coord) { rect.setTop(coord); }
	inline void setBottom(int coord) { rect.setBottom(coord); }

	inline int getLeft(void) const { return rect.left(); }
	inline int getRight(void) const { return rect.right(); }
	inline int getTop(void) const { return rect.top(); }
	inline int getBottom(void) const { return rect.bottom(); }
	inline const QRect &getRect(void) const { return rect; }


	inline bool isFlashColor(void) const { return flashColor; }
	inline void setFlashColor(bool flag) { flashColor = flag; }

	inline QColor &getColor(void) { return color; }

public slots:	// Implement slots of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);
	virtual void mobileStateChange(Eqp *pSrc, DataEnum::DataMode mode);
	virtual void selectChange(Eqp *pSrc);

signals:	// Define own signals
	void eqpDataChanged(ProfileItem *pSrc, bool colorChanged);
	void eqpSelectChanged(ProfileItem *pSrc);
	void visibilityChanged(ProfileItem *pSrc);
	void eqpSetChanged(void);
	void eqpMaskChanged(void);

protected:
	// Pointer to device shown in bar
	Eqp		*pEqp;

	// Last known device status
	Eqp::CtlStatus	ctlStatus;

	// Last known value for device
	float		value;

	// Color used for bar drawing
	QColor		color;

	// Bar boundaries
	QRect		rect;

	// Data acquisition mode
	// (in fact, it is the same value for all items in one graph, but
	// this item needs to know the mode
	DataEnum::DataMode		mode;

	// Flag indicating if last known value is valid
	bool		valueValid;

	// Flag indicating if bar is hidden
	bool		hidden;

	// Flag indicating if this device is selected
	bool		selected;

	// Flag indicating if bar shall be drawn with flashing color
	bool		flashColor;

	void analyzeCtlStatusChange(void);
};

#endif	// PROFILEITEM_H
