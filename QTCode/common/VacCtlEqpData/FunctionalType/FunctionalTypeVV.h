#ifndef	FUNCTIONALTYPEVV_H
#define	FUNCTIONALTYPEVV_H

// Special functional type for valves - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVV : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Closed = 1,
		Open = 2,
		PrSourcesOK = 3,
		CurIntlLocalPressure = 4,
		CurIntlLocalTemp = 5,
		CurIntlPrevValve = 6,
		CurIntlNextValve = 7,
		CurIntlDeltaPBefore = 8,
		CurIntlDeltaPAfter = 9,
		CurIntlGlobalBefore = 10,
		CurIntlGlobalAfter = 11,
		CurIntlLocalBefore = 12,
		CurIntlLocalAfter = 13
	};

	FunctionalTypeVV(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVV() {}

	virtual void fillCriteria(FunctionalType *pType);

	virtual void findRelatedEqp(Eqp *pEqp, int otherType, QList<Eqp *> &relatedList);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
	void findRelatedEqpVG(Eqp *pEqp, int otherType, QList<Eqp *> &relatedList);
	void findRelatedEqpVRPI(Eqp *pEqp, int otherType, QList<Eqp *> &relatedList);
	void findRelatedEqpCRYO_TT(Eqp *pEqp, int otherType, QList<Eqp *> &relatedList);
};

#endif	// FUNCTIONALTYPEVV_H

