/**
* @brief Class implementation for Vacuum contRol - bakE-out Alarm channel (VR_EA)
* @see Eqp
*/
#include "EqpVR_EA.h"

#include "DataPool.h"
#include "FunctionalTypeVPG.h"
#include "EqpMsgCriteria.h"
#include "DpConnection.h"
#include "DevListCriteria.h"
#include "ResourcePool.h"
#include "MobileType.h"
#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif
#include <QColor>

EqpVR_EA::EqpVR_EA(const Eqp &source) : Eqp(source) {
	rr1Online = rr1Replay = 0;
	comAlarmOnline = comAlarmReplay = false;
	errorStOnline = errorStReplay = 0;
	targetStOnline = 0;
	degCStOnline = degCStReplay = 999.9;
	degCWarnThreStOnline = degCErrThreStOnline = 0.0;

	alertActive = alertNotAck = false;
	functionalType = FunctionalType::BAKEOUT;

	//Check the mobile type, if it is mobile deactivate
	int mType = this->getMobileType();
	if (mType != MobileType::Fixed){
		activeOnline = activeReplay = false;
	}
}
EqpVR_EA::~EqpVR_EA(){
}
/**
@brief SIGNAL FUNCTION Notify that this device's activity has been changed for given mode.
@param[in]   mode		aqn mode (online, replay...).
@param[in]   active		mode is active.
*/
void EqpVR_EA::setActive(DataEnum::DataMode mode, bool active){
	switch (mode) {
	case DataEnum::Replay:
		if (active == activeReplay)
			return;
		else
			activeReplay = active;
		break;
	default:
		if (active == activeOnline)
			return;

		else
			activeOnline = active;
		break;
	}
	if (mobileType != MobileType::Fixed)
		emit mobileConnectionChanged(this, mode);
}
/**
@brief FUNCTION get the master name, for instance the communication object V_TCP.
@param[out]   master		Master eqp instance name.
*/
void EqpVR_EA::getMaster(QString &master) {
	master = getAttrValue("MasterName");
}
/**
@brief FUNCTION OVERRIDE Return list of all DPEs, related to state of device for state history.
@param[out]   stateDpes		list of name of dpes used to calculate state.
*/
void EqpVR_EA::getStateDpes(QStringList &stateDpes) {
	stateDpes.clear();
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "errorSt");
	addDpeNameToList(stateDpes, "targetSt");
}
/**
@brief FUNCTION OVERRIDE Return list of all DPEs, related to pressure value of device for trend history.
@param[out]   stateDpes		list of name of dpes used to calculate state.
*/
void EqpVR_EA::getValueDpes(QStringList &valueDpes) {
	valueDpes.clear();
	// dummy function because no pressure value
}
/**
@brief FUNCTION Connect DPEs of interest for widget animation.
@param[in]   *pDst		Class to be notified about change in this device.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVR_EA::connect(InterfaceEqp *pDst, DataEnum::DataMode mode) {
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "targetSt", mode);
	connectDpe(pDst, "AL1:_alert_hdl.._ack_possible", mode);
}
/**
@brief FUNCTION Disconnect DPEs of interest for animation.
@param[in]   *pDst		Class to be notified about change in this device.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVR_EA::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode) {
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "targetSt", mode);
	disconnectDpe(pDst, "AL1:_alert_hdl.._ack_possible", mode);
}
/**
@brief FUNCTION Process new online value arrived from front end to update widget animation.
@param[in]   dpe		name of dpe updated.
@param[in]	 value		new value of the dpe.
*/
void EqpVR_EA::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp) {
	if (!strcmp(dpe, "RR1"))	{
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (rr1Online == newValue)
				return;
			rr1Online = newValue;
		}
	}
	else if (!strcmp(dpe, "targetSt"))	{
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (targetStOnline == newValue)
				return;
			targetStOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "AL1")) {
		if (value.canConvert(QVariant::Bool)) {
			bool newValue = value.toBool();
			if (alertNotAck == newValue)
				return;
			alertNotAck = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}
#ifndef PVSS_SERVER_VERSION
/**
@brief FUNCTION Process new replay value arrived from front end to update widget animation.
@param[in]   dpe		name of dpe updated.
@param[in]	 value		new value of the dpe.
*/
void EqpVR_EA::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp) {
	if (!strcmp(dpe, "RR1")) {
		if (value.canConvert(QVariant::UInt)) {
			unsigned newValue = value.toUInt();
			if (rr1Replay == newValue)
				return;
			rr1Replay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif
/**
@brief SLOT FUNCTION activated by communication object when communication state change
@param[in]   pCom		Pointer of emmiter (communication object).
@param[in]   dpeName	DPE change source (see enum above)
@param[in]	 value		New value of the dpe.
@param[in]	 mode		Aqn mode (online, replay...)
*/
void EqpVR_EA::dpeChange(Eqp *pCom, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value,
	DataEnum::DataMode mode, const QDateTime &timeStamp) {
	bool alarm = !pCom->isAlive(mode);
	if (mode == DataEnum::Replay) {
		if (alarm == comAlarmReplay)
			return;
		comAlarmReplay = alarm;
	}
	else {
		if (alarm == comAlarmOnline)
			return;
		comAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}
/**
@brief FUNCTION Return main color of device
@param[out]   color		main color value
@param[in]	  mode		Aqn mode (online, replay...)
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_EA::getMainColor(QColor &color, DataEnum::DataMode mode) {
	if (mode == DataEnum::Replay)
		getMainColor(rr1Replay, 0, comAlarmReplay, color);
	else
		getMainColor(rr1Online, targetStOnline, comAlarmOnline, color);
}
#endif
/**
@brief PROTECTED FUNCTION Calculate main color of device
@details Calculate color according to RR1
@param[in]   state			State value
@param[in]	 comAlarm	Alarm value of the Tcp communication object
@param[out]	 color			Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_EA::getMainColor(unsigned rr1, unsigned target, bool comAlarm, QColor &color) {
	// First check Communication is Ok and eqp valid
	if (comAlarm || !(rr1 & RR1_VALID)) {
		color.setRgb(COLOR_INVALID);
		return;
	}
	// if no target
	if (target == 0) {
		color.setRgb(COLOR_OFF);
		return;
	}
	// Then check Error
	if (rr1 & RR1_ERROR) {
		color.setRgb(COLOR_ERROR);
		return;
	}
	// Check Warning
	if (rr1 & RR1_WARNING) {
		color.setRgb(COLOR_WARNING);
		return;
	}
	else {
		color.setRgb(COLOR_ON);
		return;
	}
}
#endif

/**
@brief FUNCTION Return main state string of device
@param[out]   string	main state string value
@param[in]	  mode		Aqn mode (online, replay...)
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_EA::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if (mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, 0, comAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, targetStOnline, comAlarmOnline, string);
	}
}
#endif

/**
@brief PROTECTED FUNCTION Calculate main state string of device
@details String value get from ressource file
@param[in]   rr1			read register 1 value
@param[in]   state			State value
@param[in]	 comAlarm		Alarm value of the Tcp communication object
@param[out]	 string			Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_EA::getMainStateString(unsigned rr1, unsigned target, bool comAlarm, QString &string) {
	if (comAlarm) {
		string = "Communication Error";
		return;
	}
	if (!(rr1 & RR1_VALID)) {
		string = "Invalid";
		return;
	}
	if (rr1 & RR1_ERROR) {
		string = "Channel " + (QString)target + " Interlocked";
		return;
	}
	if (rr1 & RR1_WARNING) {
		string = "Warning threshold overshoot";
		return;
	}
	if (target != 0) {
		string = "Active OK";
		return;
	}
	else {
		string = "Not Active";
	}
}
#endif

/**
@brief FUNCTION Calculate main state string for contextual menu of device
@details Same as getMainStateString(), ":"+PR already in mainState
@param[in]	  mode		Aqn mode (online, replay...)
@param[out]	 string		Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_EA::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode) {
	getMainStateString(string, mode);
}
#endif

/**
@brief FUNCTION Calculate tool tip
@param[in]	  mode		Aqn mode (online, replay...)
@param[out]	 string		Calculated return value
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_EA::getToolTipString(QString &string, DataEnum::DataMode mode) {
	if (mode == DataEnum::Replay)
		getMainStateString(rr1Replay, 0, comAlarmReplay, string);
	else
		getMainStateString(rr1Online, targetStOnline, comAlarmOnline, string);
}
#endif

/**
@brief FUNCTION Calculate main state string and color for moment in the past, data are taken from state archive data pool
@param[in]   pPool			Pointer to state history data pool
@param[in]	 ts				Moment when state is needed
@param[out]	 stateString	Variable where resulting state string will be written
@param[out]	 color			Variable where resulting color will be written
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_EA::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
	QString &stateString, QColor &color)
{
	stateString = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("RR1");
	dpeNames.append("targetSt");
	dpeNames.append("errorSt");
	QList<QVariant *> values;
	if (!pPool->getDpValues(dpName, ts, dpeNames, values))
		return;
	if (values.isEmpty())
		return;
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	if (values.isEmpty())
		return;
	pVariant = values.takeFirst();
	unsigned target = pVariant->toUInt();
	delete pVariant;
	/*
	if (values.isEmpty())
		return;
	pVariant = values.takeFirst();
	unsigned status = pVariant->toUInt();
	delete pVariant;
	*/
	getMainStateString(rr1, target, false, stateString); 
	getMainColor(rr1, target, false, color);
	while (!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/**
@brief FUNCTION Decide which DPEs shall be read to check if this device matches current device list criteria or not
@param[out]	 dpes		Variable where all required DPEs will be added
*/
#ifndef PVSS_SERVER_VERSION
void EqpVR_EA::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if (!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if (pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "recipeSt");
}
#endif

/**
@brief FUNCTION Return code to check if device in recent state matches device list criteria or not
@details Return 0 = Device does not match criteria; 1 = Device matches criteria; 2 = More information is needed (history)-NOT for valves
*/
#ifndef PVSS_SERVER_VERSION
int EqpVR_EA::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	DataEnum::DataMode mode = criteria.getMode();
	bool active = mode == DataEnum::Replay ? activeReplay : activeOnline;
	if (!active)
	{
		return 0;
	}

	if (criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if (!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if (pPlc)
		{
			if (!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if (!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	unsigned state = DevListCriteria::EqpStateNormal;
	if (criteria.generalStateMatches(state)) {
		return 1;
	}

	// General state does not match - check other criteria
	if (criteria.isErrors())
	{
		if (rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if (criteria.isWarnings())
	{
		if (rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}
	return 0;
}
#endif

/**
@brief FUNCTION Check if device matches criteria for E-mail/SMS notification
@param[in, out]   pCriteria	Pointer to criteria to check
*/
void EqpVR_EA::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if (pCriteria->getFunctionalType() != functionalType) {
		return;
	}

	if (pCriteria->getType() == EqpMsgCriteria::None) {
		return;
	}
	switch (pCriteria->getType()) {
	case EqpMsgCriteria::Error:
		if (rr1Online & RR1_ERROR) {
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else {
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	case EqpMsgCriteria::Warning:
		if (rr1Online & RR1_WARNING) {
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else {
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	}
	pCriteria->setMatch(false, false, 0);
}
