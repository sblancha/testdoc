
#uses "vclMachine.ctl"
#uses "vclArchiveSession.ctl"
#uses "vclDeviceFileParser.ctl"
#uses "vclArchiveConfig.ctl"

const string sMyName = "vcsArchiveSession.ctl";

const bool bMyDebug = false;

main()
{
  DebugTN(sMyName + ": Starting");
  while(true)
  {
    try
    {
      CheckSessions();
    }
    catch
    {
      DebugTN(sMyName + ": CheckSessions() thrown expection", getLastException());
    }
    delay(60);
  }
}

void CheckSessions()
{
  dyn_string dsAllDpNames = dpNames("*", LHC_VAC_ARCHIVE_SESSION_DP);

  // First stop running session(s)
  for(int iDpIdx = dynlen(dsAllDpNames) ; iDpIdx > 0 ; iDpIdx--)
  {
    uint uiStatus = LHC_VAC_ARCHIVE_SESSION_NONE;
    time tStartTime, tEndTime;
    string sDpName = dsAllDpNames[iDpIdx];
    dpGet(sDpName + ".Status", uiStatus,
          sDpName + ".ScheduledStartTime", tStartTime,
          sDpName + ".ScheduledEndTime", tEndTime);
    if(bMyDebug)
    {
      DebugTN(sMyName + ": CheckSessions(): state for " + dpSubStr(sDpName, DPSUB_DP) + " = " + uiStatus);
    }
    time now = getCurrentTime();
    switch(uiStatus)
    {
    case LHC_VAC_ARCHIVE_SESSION_READY:
      if((now >= tStartTime) && (now < tEndTime))
      {
        StartSession(sDpName);
      }
      break;
    case LHC_VAC_ARCHIVE_SESSION_STARTING:
      WriteError(sDpName, "Unexpected state STARTING");
      break;
    case LHC_VAC_ARCHIVE_SESSION_STOPPING:
      WriteError(sDpName, "Unexpected state STOPPING");
      break;
    }
  }

  // Then start new sessions
  for(int iDpIdx = dynlen(dsAllDpNames) ; iDpIdx > 0 ; iDpIdx--)
  {
    uint uiStatus = LHC_VAC_ARCHIVE_SESSION_NONE;
    time tStartTime, tEndTime;
    string sDpName = dsAllDpNames[iDpIdx];
    dpGet(sDpName + ".Status", uiStatus,
          sDpName + ".ScheduledStartTime", tStartTime,
          sDpName + ".ScheduledEndTime", tEndTime);
    if(bMyDebug)
    {
      DebugTN(sMyName + ": CheckSessions(): state for " + dpSubStr(sDpName, DPSUB_DP) + " = " + uiStatus);
    }
    time now = getCurrentTime();
    switch(uiStatus)
    {
    case LHC_VAC_ARCHIVE_SESSION_RUNNING:
    case LHC_VAC_ARCHIVE_SESSION_STOP_FAILED:  // TODO ????
      if(now > tEndTime)
      {
        FinishSession(sDpName);
      }
      break;
    }
  }
}

void StartSession(string sSessionDpName)
{
  DebugTN(sMyName + ": StartSession(" + sSessionDpName + ")");
  
  SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_STARTING);
  dyn_string dsDpNames;
  uint uiSmoothType;
  float fSmoothDeadBand, fSmoothTime;
  dpGet(sSessionDpName + ".DpList", dsDpNames,
        sSessionDpName + ".SmoothType", uiSmoothType,
        sSessionDpName + ".SmoothDeadBand", fSmoothDeadBand,
        sSessionDpName + ".SmoothTime", fSmoothTime);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_START_FAILED, "Failed to read settings: " + err);
    return;
  }
  if(dynlen(dsDpNames) == 0)
  {
    SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_START_FAILED, "Empty DP List");
    return;
  }
  string sErrorMsg;
  int iSuccessCnt = 0;
  for(int iDpIdx = dynlen(dsDpNames) ; iDpIdx > 0 ; iDpIdx--)
  {
    string sDpeName = dsDpNames[iDpIdx] + ".PR";
    if(!dpExists(sDpeName))
    {
      SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_START_FAILED, "DPE " + sDpeName + " does not exist");
      // TODO ?
      return;
    }
    
    try
    {
      if(SetNewArchiving(sDpeName, uiSmoothType, fSmoothDeadBand, fSmoothTime))
      {
        iSuccessCnt++;
      }
    }
    catch
    {
      DebugTN(sMyName + ":SetNewArchiving() exception: " + getLastException());
    }
  }
  if(iSuccessCnt == 0)
  {
    SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_START_FAILED, "Failed to change archive settings, see log");
  }
  else
  {
    SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_RUNNING, "Archiving changed for " + iSuccessCnt +
                     "/" + dynlen(dsDpNames) + " devices");
  }

  DebugTN(sMyName + ": StartSession(" + sSessionDpName + ") - done, " + iSuccessCnt +
                     "/" + dynlen(dsDpNames) + " devices");
}

bool SetNewArchiving(string sDpeName, uint uiSmoothType, float fSmoothDeadBand, float fSmoothTime)
{
  // Get current archiving parameters
  bool bIsArchived;
  string sArchiveClass;
  int iSmoothKind;
  float fDeadBand, fTimeInterval;
  GetArchiveParams(sDpeName, bIsArchived, sArchiveClass, iSmoothKind, fDeadBand, fTimeInterval);
  if(!bIsArchived)
  {
    DebugTN(sMyName + ": DPE " + sDpeName + " is not archived");
    return false;
  }
  
  // Set new archiving parameters
  if(bMyDebug)
  {
    DebugTN(sMyName + ": changing archiving for " + sDpeName);
  }
  dyn_string dsExceptionInfo;
  SetArchiveParamsForArchiveClassName(sDpeName, sArchiveClass, uiSmoothType,
                                      fSmoothDeadBand, fSmoothTime, dsExceptionInfo);
  if(dynlen(dsExceptionInfo) > 0)
  {
    DebugTN(sMyName + ":Failed to set new archiving for " + sDpeName + ": " + dsExceptionInfo);
    return false;
  }
  return true;
}

void FinishSession(string sSessionDpName)
{
  DebugTN(sMyName + ": FinishSession(" + sSessionDpName + ")");
 
  // Read original archive settings
  dyn_string dsExceptionInfo;
  LhcVacGetMachineId(dsExceptionInfo);
  if(dynlen(dsExceptionInfo) > 0)
  {
    string sErrorMsg = "LhcVacGetMachineId() failed: " + dsExceptionInfo;
    DebugTN(sMyName + ": " + sErrorMsg);
    SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_STOP_FAILED, sErrorMsg);
    return;
  }
  string sFileName =  PROJ_PATH + "data\\" + glAccelerator + ".for_Import";
  int coco = VacImportParseFile(sFileName, glAccelerator, false, dsExceptionInfo);
  if(dynlen(dsExceptionInfo) > 0)
  {
    string sErrorMsg = "Failed to parse file " + sFileName + ": " + dsExceptionInfo;
    DebugTN(sMyName + ": " + sErrorMsg);
    SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_STOP_FAILED, sErrorMsg);
    return;
  }
  if(bMyDebug)
  {
    DebugTN(sMyName + ": FinishSession(" + sSessionDpName + "): file parsed " + sFileName);
  }

  // Get list of DPs for session
  dyn_string dsDpNames;
  dpGet(sSessionDpName + ".DpList", dsDpNames);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_STOP_FAILED, "Failed to read settings: " + err);
    return;
  }
  if(dynlen(dsDpNames) == 0)
  {
    SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_STOP_FAILED, "Empty DP List to stop session");
    return;
  }
  SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_STOPPING);
  
  // Restore archiving settings
  int iSuccessCount = 0;
  for(int iDpIdx = dynlen(dsDpNames) ; iDpIdx > 0 ; iDpIdx--)
  {
    if(!dpExists(dsDpNames[iDpIdx] + ".PR"))
    {
      continue;
    }
    string sDpName = dpSubStr(dsDpNames[iDpIdx], DPSUB_DP);
    if(RestoreArchiving(sDpName))
    {
      iSuccessCount++;
    }
  }
  if(iSuccessCount == 0)
  {
    SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_STOP_FAILED, " Success for 0 DPs");
  }
  else
  {
    SetSessionStatus(sSessionDpName, LHC_VAC_ARCHIVE_SESSION_FINISHED, " Finished for " + iSuccessCount +
                     "/" + dynlen(dsDpNames) + " DP(s)");
  }
  DebugTN(sMyName + ": FinishSession(" + sSessionDpName + ") - done, "  + iSuccessCount +
                     "/" + dynlen(dsDpNames) + " DP(s)");
}

bool RestoreArchiving(string sDpName)
{
  dyn_anytype daDpData;
  dyn_dyn_anytype ddaDpeData;
  dyn_string dsExceptionInfo;
  if(VacImportGetDeviceDataByName(sDpName, "", daDpData, ddaDpeData, dsExceptionInfo) != OK)
  {
    DebugTN(sMyName + ": VacImportGetDeviceDataByName(" + sDpName + "," + sDpTypeName + ") failed: " +
            dsExceptionInfo);
    return false;
  }
  for(int iDpeIdx = dynlen(ddaDpeData) ; iDpeIdx > 0 ; iDpeIdx--)
  {
    if(ddaDpeData[iDpeIdx][VAC_DPE_NAME] != "PR")
    {
      continue;
    }
    bool bIsArchived;
    string sArchiveClass;
    int iSmoothKind;
    float fDeadBand, fTimeInterval;
    string sDpeName = sDpName + ".PR";
    GetArchiveParams(sDpeName, bIsArchived, sArchiveClass, iSmoothKind, fDeadBand, fTimeInterval);
    if(!bIsArchived)
    {
      DebugTN(sMyName + ": DPE " + sDpeName + " is not archived");
      return false;
    }
    iSmoothKind = ddaDpeData[iDpeIdx][VAC_DPE_ARCH_SMOOTH_TYPE];
    fTimeInterval = ddaDpeData[iDpeIdx][VAC_DPE_ARCH_TIME_INTERVAL];
    fDeadBand = ddaDpeData[iDpeIdx][VAC_DPE_ARCH_DEAD_BAND];
    SetArchiveParamsForArchiveClassName(sDpeName, sArchiveClass, iSmoothKind,
                                      fDeadBand, fTimeInterval, dsExceptionInfo);
    if(dynlen(dsExceptionInfo) > 0)
    {
      DebugTN(sMyName + ":Failed to set new archiving for " + sDpeName + ": " + dsExceptionInfo);
      return false;
    }
    return true;
  }
  DebugTN(sMyName + ": RestoreArchiving(): date for DPE PR not found");
  return false;
}

void SetSessionStatus(string sSessionDpName, uint uiStatus, string sErrorMsg = "")
{
  if(bMyDebug)
  {
    DebugTN(sMyName + ": setting status " + uiStatus + " for " + sSessionDpName);
  }
  dpSet(sSessionDpName + ".Status", uiStatus);
  WriteError(sSessionDpName, sErrorMsg);
}

void WriteError(string sSessionDpName, string sErrorMsg)
{
  if(bMyDebug)
  {
    DebugTN(sMyName + ": ERROR for " + sSessionDpName + ": " + sErrorMsg);
  }
  dpSet(sSessionDpName + ".Error", sErrorMsg);
}
