/* dcsMoni_screenshotWatchdog.ctl
   @author Marco Boccioli
   @version 1.0
   checks if the screenshots are taken periodically for each panels group.
   if a panel screenshot is not created, it restarts the agent for a group.
*/
//#uses "dcsMoni/dcsMoni.ctl"

main()
{
  //before checking if the screenshots are updated,
  //wait for the screenshot agent to start taking screenshots in case of project restart.
  delay(30);
  dcsMoni_ScreenshotWatchdog();
}
