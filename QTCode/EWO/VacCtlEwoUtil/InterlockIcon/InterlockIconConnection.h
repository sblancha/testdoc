#ifndef INTERLOCKICONCONNECTION_H
#define INTERLOCKICONCONNECTION_H

#include "qwidget.h"




class InterlockIconConnection : public QWidget
{
	Q_OBJECT
public:
	typedef enum
	{
		Up = 0,
		Down,
		Left,
		Right,
		Undefined
	} Direction;

	InterlockIconConnection(QPoint startPoint, QPoint endPoint, QWidget * parent);
	InterlockIconConnection(QPoint startPoint, InterlockIconConnection::Direction direction, QString displayString, QWidget * parent);
	~InterlockIconConnection();

	static bool areLinesParalel(const InterlockIconConnection::Direction direction1, const InterlockIconConnection::Direction direction2);
	void setStartOffset(InterlockIconConnection::Direction direction, int offset);
	void setEndOffset(InterlockIconConnection::Direction direction, int offset);
	void setWriteCables(bool writeCableStart, bool writeCableEnd);
	void fixOverlapping(InterlockIconConnection & otherConnection);
	inline void setCableNumber(QString cableNumber){ this->cableNumber = cableNumber; };
	void calculateGeometry();


private:
	QString displayString;
	QString cableNumber;
	QList<QPoint> orderedPointList;
	Direction startPointDirection;
	Direction endPointDirection;
	int startPointOffset;
	int endPointOffset;
	int isOpenEnded;
	bool writeCableStart;
	bool writeCableEnd;
	

	void paintEvent(QPaintEvent *pEvent);

};

#endif