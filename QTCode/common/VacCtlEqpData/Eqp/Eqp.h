// Eqp.h: interface for the Eqp class.
//
//////////////////////////////////////////////////////////////////////

#ifndef EQP_H
#define EQP_H

#include "VacCtlEqpDataExport.h"


// Generic equipment class - provides the very basic functionality for
// all equipment. Functionality, specific for equipment types, shall be
// implemented by subclasses.
// All functionality is available for PVSS client version, but only part
// is available for PVSS server version (for example, there is no GUI on server,
// there is no replay on server etc.. Version, compiled for server only,
// is compiled with PVSS_SERVER_VERSION defined, i.e.
//		#ifndef PVSS_SERVER_VERSION
// can be used to distinguish between versions

#include "DataEnum.h"

#include "FileParser.h"
#include "InterfaceEqp.h"

#include <QStringList>

class MainPart;
class Sector;

class VacCtlStateArchive;

class EqpMsgCriteria;

#include <QVariant>
#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// COLOR_GP_ : Generic Purpose Color
#define COLOR_GP_NOEQP			255,192,203		//pink
#define COLOR_GP_INVALID		63,127,255  	//blue
#define COLOR_GP_OFF			255,255,255		//white
#define COLOR_GP_ON				0,255,0			//green
#define COLOR_GP_OPEN			0,255,0			//green
#define	COLOR_GP_CLOSED			255, 0, 0		//red
#define	COLOR_GP_BAD			255, 0, 0		//red
#define	COLOR_GP_ERROR			255, 0, 0		//red
#define	COLOR_GP_ERROR_VV		220, 80, 200	//darkviolet
#define	COLOR_GP_WARNING		255,255,0		//yellow
#define COLOR_GP_UR				0,  180, 0		//darkgreen (Under range)
#define COLOR_GP_FORCED_ON  	128,128,0		//olive

#define	COLOR_GP_SERVICES		255,165,0		//orange

#define COLOR_NOT_CONTROL			127,127,127 // Gray
#define COLOR_NOT_CONNECTED			192,192,192 // Light Gray

//	Main state bits to be analyzed
#define	RR1_16B_GP_VALID			(0x4000) // 0x40 00
#define RR1_16B_GP_ERROR			(0x0080) // 0x00 80
#define	RR1_16B_GP_WARNING			(0x0040) // 0x00 40
#define	RR1_16B_GP_ON				(0x0200) // 0x00 02
#define	RR1_16B_GP_OFF				(0x0100) // 0x00 01

#define RR1_32B_GP_VALID			(0x40000000) // 0x40 00 00 00
#define	RR1_32B_GP_ON				(0x02000000) // 0x02 00 00 00
#define	RR1_32B_GP_OFF				(0x01000000) // 0x01 00 00 00
#define RR1_32B_GP_ERROR			(0x00800000) // 0x00 80 00 00
#define	RR1_32B_GP_WARNING			(0x00400000) // 0x00 40 00 00

class VACCTLEQPDATA_EXPORT Eqp : public InterfaceEqp, public FileParser
{
	Q_OBJECT

public:
	// Equipment status - see Eqp.Status in machine DB
	typedef enum
	{
		Unknown = -1,		// in case of parsing error
		Used = 0,			// Device is used
		NotUsed = 1,		// Device is not used (such devices shall not appear in file *.for_DLL)
		TempRemoved = 2,	// Device is temporarily removed (from LAYOUT DB)
		PermRemoved = 3,	// Device is permanently removed and not used anymore (such devices shall not appear in file *.for_DLL)
		NotControl = 4,		// Device is not controlled (see VTL #2761)
		NotConnected = 5	// Device is not connected (see VTL #2673)
	} CtlStatus;

	// The 'very main' state of device. For example, OPEN/CLOSED for valve. Used for calculating border color
	//	of circles on main view
	typedef enum
	{
		Good = 0,
		Bad = 1,
		Undefined = 2
	} MainEqpStatus;

public:
	Eqp();
	Eqp(const Eqp &source);
	Eqp(int type);
	Eqp(int type, char *name);
	virtual ~Eqp();
	virtual Eqp & operator = (const Eqp &other);

protected:
	// Initialize internal variables when instance is created
	void initVar(void);

	// Copy content from source equipment
	virtual void copyContent(const Eqp &source);

signals:
	// Very specific signal that can be sent by gauge on VPG_BP when sector
	// referece changes for the gauge due to valve opening/closing of VPG
	void sectorsChanged(Eqp *pSrc);

	// in case active state change emit signal
	void mobileConnectionChanged(Eqp *pEqp, DataEnum::DataMode mode);

	// Alarm equipment may require to rebuild synoptic view
	void positionChanged(Eqp *pEqp, DataEnum::DataMode mode);

	////////////////////////////////////////////////////////////////////
	/////////////////////////////// Static members/methods
	////////////////////////////////////////////////////////////////////
protected:
	// Instance used to parse data from file
	static Eqp			parser;

public:
	static Eqp *createFromFile(int type, FILE *pFile, int &lineNo,
		char **tokens, int nTokens, QStringList &errList);

	int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);

	// Make necessary initialization after all devices have been read
	// from file. For the time being the only device who needs this is
	// VPI (see comments in EqpVPI class), but in principle other types
	// may also need such initialization
	virtual void postProcess(void) {}

protected:
	static Eqp *createTypeSpecific(void);
	static Eqp *createTypeSpecificFixed(void);
	static Eqp *createMobile(void);

public:
	////////////////////////////////////////////////////////////////////
	/////////////////////////////// 0. Equipment status
	////////////////////////////////////////////////////////////////////
	virtual CtlStatus getCtlStatus(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? ctlStatusReplay : ctlStatusOnline; }

	////////////////////////////////////////////////////////////////////
	/////////////////////////////// 1. Equipment classification
	////////////////////////////////////////////////////////////////////
	inline int getType(void) const { return type; }
	inline int getFunctionalType(void) const { return functionalType; }
	virtual inline int getPvssFunctionalType(void) const { return 0; }
	inline void setPvssFunctionalType(int /* newType */ ) { }
	inline int getCtrlFamily(void) const { return (int)ctrlFamily; }
	inline int getCtrlType(void) const { return (int)ctrlType; }
	inline int getCtrlSubType(void) const { return (int)ctrlSubType; }

	// CRYO thermometers are so different from all other device types (in
	// terms of appearance in dialogs) that special method has been introduced
	// in order to distinguish them from other devices
	// CRYO thermometer subclasses shall reimplement this method such
	// that it will return true
	virtual inline bool isCryoThermometer(void) const { return false; }
	virtual inline bool isValveInterlock(void) const { return false; }
	virtual inline void setValveInterlock(bool /* flag */) {}

	////////////////////////////////////////////////////////////////////
	//////// 2. Equipment names
	////////////////////////////////////////////////////////////////////
	virtual inline char *getName(void) const { return name; }
	inline void setName(char *name) { this->name = name; }
	inline const char *getAlias(void) const {return alias; }
	virtual inline const char *getVisibleName(void) const { return alias == NULL ? name : alias; }
	inline const char *getFunctionalName(void) const { return functionalName; }
	inline const char *getDpType(void) const { return dpType; }
	inline const char *getDpName(void) const { return dpName; }
	inline const char *getTypeName(void) const { return typeName; }

	// Make short name of passive element
	virtual const char *makeShortName(void) const;


	////////////////////////////////////////////////////////////////////
	//////// 3. Equipment physical location
	////////////////////////////////////////////////////////////////////
	inline const char *getSurveyPart(void) const { return surveyPart; }
	inline float getSurveyPos(void) const { return surveyPos; }
	inline float getStart(void) const { return start; }
	inline void setStart(float start) { this->start = start; }
	inline float getLength(void) const { return length; }
	inline void setLength(float length) { this->length = length; }
	inline float getStartX(void) const { return startX; }
	inline void setStartX(float startX) { this->startX = startX; }
	inline float getStartY(void) const { return startY; }
	inline void setStartY(float startY) { this->startY = startY; }
	inline float getEndX(void) const { return endX; }
	inline void setEndX(float endX) { this->endX = endX; }
	inline float getEndY(void) const { return endY; }
	inline void setEndY(float endY) { this->endY = endY; }
	inline bool isOrdered(void) const { return ordered; }
	inline void setOrdered(bool flag) { ordered = flag; }

	inline float getAccessPointDistance(void) const { return accessPointDistance; }
	inline void setAccessPointDistance(float value) { accessPointDistance = value; }

	////////////////////////////////////////////////////////////////////
	//////// 4. Equipment vacuum location - can be different in 'online' and
	//////// 'replay' modes
	////////////////////////////////////////////////////////////////////
	virtual int getVacType(void) const { return vacType; }
	inline void setVacType(int vacType) { this->vacType = vacType; }

	// Sector before device for 'online' mode
	virtual inline Sector *getSectorBefore(void) const { return pSectorBefore; }

	// Label before sector border (this device) on synoptic
	virtual const char *getLabelBefore(void);
	

	// Sector after device for 'online' mode
	inline Sector *getSectorAfter(void) const { return pSectorAfter; }

	// Label after sector border (this device) on synoptic
	virtual const char *getLabelAfter(void);
	
	// Change reference to sectors after building ordered sector list
	virtual void setNewSectors(void);

	inline const QList<Sector *> &getExtraSectors(void) const { return extraSectors; }
	inline void addExtraSector(Sector *pSector) { extraSectors.append(pSector); }

	inline const QList<Eqp *> &getExtTargets(void) const { return extTargets; }
	inline void addExtTarget(Eqp *pEqp) { if (pEqp) { extTargets.append(pEqp); } }
	virtual int removeExtTargets(Eqp *pEqpToRemove) { return extTargets.removeAll(pEqpToRemove); }

	virtual bool isActiveMayChange();
	virtual bool isPositionMayChange() { return false; }

#ifndef PVSS_SERVER_VERSION
	// Sector before device for given mode
	virtual inline Sector *getSectorBefore(DataEnum::DataMode /* mode */) const
		{ return pSectorBefore; }

	virtual void setSectorBefore(DataEnum::DataMode /* mode */, Sector * /* pSector */) {}
	
	// Sector after device for given mode
	virtual inline Sector *getSectorAfter(DataEnum::DataMode /* mode */) const
		{ return pSectorAfter; }

	virtual void setSectorAfter(DataEnum::DataMode /* mode */, Sector * /* pSector */) {}

	bool isSectorBorder(DataEnum::DataMode /* mode */) const { return isSectorBorder(); }
#endif	// PVSS_SERVER_VERSION

	inline MainPart *getMainPart(void) const { return pMainPart; }
	inline const QString &getDomain(void) const { return domain; }

	inline bool isSectorBorder(void) const { return pSectorBefore && pSectorAfter && (pSectorBefore != pSectorAfter); }


	////////////////////////////////////////////////////////////////////
	//////// 5. Mobile equipment support - can be different in 'online' and
	//////// 'replay' modes
	////////////////////////////////////////////////////////////////////
	inline int getMobileType(void) const { return mobileType; }
	// The following method shall be overwritten by mobile equipment
	virtual bool isActive(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? activeReplay : activeOnline; }

	inline const char *getFlangeName(void) const { return flangeName; }
	inline int getIndexInSector(void) const { return indexInSector; }

	////////////////////////////////////////////////////////////////////
	//////// 6. Special properties for device drawing
	////////////////////////////////////////////////////////////////////

#ifndef PVSS_SERVER_VERSION
	inline bool isSkipOnSynoptic(void) const { return skipOnSynoptic; }
#endif	// PVSS_SERVER_VERSION


	////////////////////////////////////////////////////////////////////
	//////// 7. Other equipment attributes
	////////////////////////////////////////////////////////////////////
	const QString getAttrValue(const char *attrName) const;
	const QHash<QByteArray, QString> &getAttrList(void) const { return *pAttrList; }

	inline Eqp *getMasterProcess(void) const { return pMasterProcess; }
	inline void setMasterProcess(Eqp *pEqp) { pMasterProcess = pEqp; }
	inline const QByteArray &getUsageInMasterProcess(void) const { return usageInMasterProcess; }
	inline void setUsageInMasterProcess(const QByteArray &name) { usageInMasterProcess = name; }
	inline void setMasterProcessRef(Eqp *pEqp, const QByteArray &name) { pMasterProcess = pEqp; usageInMasterProcess = name; }

	void setComDpName(const char *dpName) { comDpName = dpName; }

	inline bool isAlertNotAck(void) { return alertNotAck; }

	/////////////////////////////////////////////////////////////////////////////
	// 8. Method which shall be implemented by active equipment ////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Enumeration to identify part of device that may have specific
	// action/color/state - different from those of device as a whole
	typedef enum
	{
		EqpPartNone = 0,
		EqpPartPump = 1,	// Pump part of VPG (combination of PP and TMP if VPG has both)
		EqpPartVVR = 2,		// Roughing valve (if there is only one)
		EqpPartVVR1 = 3,	// Roughing valve #1 (if there are two valves)
		EqpPartVVR2 = 4,	// Roughing valve #2 (if there are two valves)
		EqpPartPP = 5,		// Primary pump of VPG
		EqpPartTMP = 6,		// Turbomol. pump of VPG
		EqpPartVVD = 7,		// VVD of VPG
		EqpPartVVI = 8,		// VVI of VPG
		EqpPartVVP = 9,		// VVP of VPG
		EqpPartVVT = 10,	// VVT of VPG
		EqpPartVOPS_A = 11,	// Channel A of VOPS_VELO
		EqpPartVOPS_B = 12,	// Channel B of VOPS_VELO
		EqpPartWmA = 13,	// current set for VRPM
		EqpPartWmV = 14,	// voltage set for VRPM
		EqpPartRmA = 15,	// current measurement for VRPM
		EqpPartRmV = 16,	// voltage measurement for VRPM,
		EqpPartCB = 17,		// Circuit breaker of VRPM
		EqpPartVVG = 18,	// VVG of INJ_6B02
		EqpPartVPC = 19,	// VPC_HCCC: Cryo pump Controller HRS HCC
		EqpPartVV_PUL =  20,// Vacuum Valve Pulsed	
		EqpPartVG1 = 21,	// Vacuum Gauge 1 VPG MBLK
		EqpPartVG2 = 22	// Vacuum Gauge 2 VPG MBLK
	} EqpPart;

	virtual bool convertValue(const QVariant &value, float &result, EqpPart part = EqpPartNone);
	virtual void convertValueToString(float value, QString &result, EqpPart part = EqpPartNone);

	virtual void addCtlChild(Eqp * /* pEqp */) {}

	virtual void getMaster(QString &masterDp, int &masterChannel)
	{
		masterDp = "";
		masterChannel = 0;
	}
	virtual inline QString getMasterDp(void) { return NULL; }
	virtual inline Eqp *getMaster(void) { return NULL; }
	virtual inline void attachToSourceChange(bool /* isInit */) { return; }
	virtual void getConfig(QString &configDp, int &configChannel)
	{
		configDp = "";
		configChannel = 0;
	}
	virtual void getSlaves(QStringList &slaves) { slaves.clear(); }
	virtual void getTargets(QStringList &targets) { targets.clear(); }
	virtual void getStateDpes(QStringList &stateDpes) { stateDpes.clear(); }
	virtual void getValueDpes(QStringList &valueDpes) { valueDpes.clear(); }
	virtual void getValueDpes(QStringList &valueDpes, EqpPart part);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	// The following method shall be overwritten by mobile equipment 
	virtual void setActive(DataEnum::DataMode mode, bool active)
	{
		if(mode == DataEnum::Replay)
		{
			activeReplay = active;
		}
		else
		{
			activeOnline = active;
		}
	}

	virtual const QString findPlcName(void);

	// Some devices require more than one PLC to connect, they shall implement
	// the following method - and method must return true
	virtual bool getPlcList(QList<Eqp *> & /* list */) { return false; }

	virtual inline bool isPlcAlarm(DataEnum::DataMode /* mode */) const { return false; }
	virtual inline bool isComAlarm(DataEnum::DataMode /* mode */) const { return false; }

	// Method specially for PLC - return state in one call
	virtual bool isAlive(DataEnum::DataMode /* mode */) const { return true; }

	virtual bool isSelected(void) const { return selected; }
	virtual void setSelected(bool selected, bool isFromPvss);

	// Hepler methods for active equipment implemented in this class
	virtual void connectDpe(InterfaceEqp *pDst, const char *dpeName,
		DataEnum::DataMode mode);
	virtual void connectSelection(InterfaceEqp *pDst);	// No DPE - only selection
	virtual void disconnectDpe(InterfaceEqp *pDst, const char *dpeName,
		DataEnum::DataMode mode);
	virtual void disconnectSelection(InterfaceEqp *pDst);	// No DPE - only selection

	inline bool isIntlSourcePrev(void) const { return intlSourcePrev; }
	inline void setIntlSourcePrev(bool flag) { intlSourcePrev = flag; }
	inline bool isIntlSourceNext(void) const { return intlSourceNext; }
	inline void setIntlSourceNext(bool flag) { intlSourceNext = flag; }
	inline bool isIntlSourceExt(void) const { return intlSourceExt; }
	inline void setIntlSourceExt(bool flag) { intlSourceExt = flag; }
	inline bool isReverseBeamDirection(void) const { return reverseBeamDirection; }
	inline void setReverseBeamDirection(bool flag) { reverseBeamDirection = flag; }

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	// Return true if device's color shall affect main view color
	virtual inline bool isSpecColor(void) { return false; }

	// Return true if device shall appear in sector view
	virtual inline bool isForSectorView(void) { return false; }

	virtual DataEnum::ValueType getMainValueType(void) const { return DataEnum::ValueTypeNone; }
	virtual bool isMainValueNegative(void) const { return false; }
	virtual float getMainValue(DataEnum::DataMode /* mode */) { return 0; }
	virtual bool isMainValueValid(DataEnum::DataMode /* mode */) { return false; }
	virtual void getMainColor(QColor & /* color */, DataEnum::DataMode /* mode */) {}
	virtual void getPartColor(int /* part */, QColor & /* color */, DataEnum::DataMode /* mode */) {}
	virtual void getSubEqpColor(QString /*subEqp*/, QColor &/*color*/, DataEnum::DataMode /*mode*/) {}
	virtual int getDrawOrder(void) { return 0; }
	virtual void getMainStateString(QString & /* string */, DataEnum::DataMode /* mode */) {}
	virtual void getPartStateString(int /* part */, QString & /* string */, DataEnum::DataMode /* mode */) {}
	virtual void getMainValueString(QString & /* string */, DataEnum::DataMode /* mode */) {}
	virtual void getMainStateStringForMenu(QString & string, DataEnum::DataMode mode)
	{
		QString state;
		getMainStateString(state, mode);
		string = getVisibleName();
		string += ": ";
		string += state;
	}
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode) { getMainStateStringForMenu(string, mode); }
	virtual void getHistoryState(VacCtlStateArchive * /* pPool */, QDateTime & /* ts */,
		QString &state, QColor & /* color */) { state = ""; }

	// Special color of interlock for device, returns true if there is special color
	virtual bool getInterlockColor(DataEnum::DataMode /* mode */, QColor & /* color */, QColor & /* borderColor */, int & /* order */, const QList<Eqp *> * /* pNeighborList */) { return false; }

	virtual MainEqpStatus getMainStatus(void) { return Good; }
	virtual void getInterlockBorderColor(QColor &borderColor, int &order, const QList<Eqp *> *pNeighborList);

protected:
	virtual bool isColorByCtlStatus(QColor &color, DataEnum::DataMode mode) const;
	virtual bool isColorByCtlStatus(CtlStatus status, QColor &color) const;
	virtual bool isStateStringByCtlStatus(QString &string, DataEnum::DataMode mode) const;
	virtual bool isStateStringByCtlStatus(CtlStatus status, QString &string) const;

	// Returns true if further analysis of values is NOT needed
	virtual bool isHistoryStateByCtlStatus(VacCtlStateArchive *pPool, QDateTime &ts, QList<QByteArray> &dpeNames, QList<QVariant *> &values,
		QString &state, QColor &color) const;

#endif

public:
	virtual const QVariant &getDpeValue(const char *dpeName, DataEnum::DataMode mode, QDateTime &timeStamp);

	virtual int getErrorCode(DataEnum::DataMode /* mode */) { return 0; }
	virtual void getErrorString(DataEnum::DataMode mode, QString &result);

	////////////////////////////////////////////////////////////////////
	//////// 9. Methods for device list support (active equipment only)
	////////////////////////////////////////////////////////////////////
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes) { dpes.clear(); }
	virtual int checkDevListCrit(void) { return 0; }
	virtual void getHistoryParamForDevList(QString &dpe, QDateTime & /* start */, QDateTime & /* end */) { dpe = ""; }
	virtual float getGrowFactorForDevList(void) { return 0.0; }
	virtual int getSpikeFilterForDevList(void) { return 0; }
	virtual bool checkDevListNotConnectedCriteria(DataEnum::DataMode mode);
	virtual bool getIsReadOnly(void) { return isReadOnly;  }
#endif

	////////////////////////////////////////////////////////////////////
	//////// 10. Other (auxilliary) methods
	////////////////////////////////////////////////////////////////////
		
	virtual Eqp *clone(void);
	virtual int compare(Eqp *pOtherEqp, bool isReverseOrder);
	void addDpeNameToList(QStringList &list, const char *dpeName);

	////////////////////////////////////////////////////////////////////
	///////	11. Methods to support SMS/E-mail notification
	////////////////////////////////////////////////////////////////////
	virtual void checkCriteria(EqpMsgCriteria * /* pCriteria */) {}

	virtual void dump(FILE *pFile, int idx);

protected:
	////////////////////////////////////////////////////////////////////
	/////////////////////////////// 0. Equipment status
	////////////////////////////////////////////////////////////////////
	CtlStatus				ctlStatusOnline;
	CtlStatus				ctlStatusReplay;

	////////////////////////////////////////////////////////////////////
	/////////////////////////////// 1. Equipment classification
	////////////////////////////////////////////////////////////////////
	// Equipment type - see EqpType class
	int				type;

	// Functional type of equipment - see FunctionalType class
	int				functionalType;

	// Functional type of equipment from PVSS point of view - value
	// is not to be interpreted by C++ code, only by PVSS code
	int				pvssFunctionalType;

	// Equipment family - see Richard's classification
	unsigned		ctrlFamily;

	// Equipment type - see Richard's classification
	unsigned		ctrlType;

	// Equipment subtype - see Richard's classification
	unsigned		ctrlSubType;

	////////////////////////////////////////////////////////////////////
	//////// 2. Equipment names
	////////////////////////////////////////////////////////////////////
	// Equipment name
	char		*name;

	// Equipment name alias
	char		*alias;

	// Equipment functional name == optic name for magnets (can be NULL)
	char		*functionalName;

	// DP type name (can be NULL for equipment to corresponding to DPs)
	char		*dpType;

	// DP name (can be NULL for equipment not corresponding to DPs)
	char		*dpName;

	// Name of 'general' equipment type - replacement for former dpTypeMask
	// TODO: DO WE NEED THIS NAME? WE HAVE FUNCTIONAL TYPE INSTEAD???
	char		*typeName;

	////////////////////////////////////////////////////////////////////
	//////// 3. Equipment physical location
	////////////////////////////////////////////////////////////////////
	// Name of survey partition where equipment is located
	char		*surveyPart;

	// Location of start of element in survey partition
	float		surveyPos;

	// Distance of element 'from closest access point'. For LHC - distance from closest IP, for other machines - not clear yet
	float		accessPointDistance;

	// Location of equipment start in beam line
	float		start;

	// Equipment length
	float		length;

	// X-coordinate of equipment start
	float		startX;

	// Y-coordinate of equipment start
	float		startY;

	// X-coordinate of equipment end
	float		endX;

	// Y-coordinate of equipment end
	float		endY;

	////////////////////////////////////////////////////////////////////
	//////// 4. Equipment vacuum location - can be different in 'online' and
	//////// 'replay' modes
	////////////////////////////////////////////////////////////////////
	// Vacuum type of device - see VacType class
	int		vacType;

	// Sector before device - first read from file, can be changed at
	// run time - for example, gauges on bypass pumping group
	Sector	*pSectorBefore;

	// Sector after device - first read from file, can be changed at
	// run time - for example, gauges on bypass pumping group
	Sector	*pSectorAfter;

	// 11.03.2013: reference to 'extra' sectors for device. Added for CRYO thermometers
	// which are located in isolation sectors, but users want them to appear also in
	// beam sectors in device list
	QList<Sector *>	extraSectors;

	// Main part where equipment belongs to
	MainPart	*pMainPart;

	// Name of domain for equipment
	QString		domain;

	////////////////////////////////////////////////////////////////////
	//////// 5. Mobile equipment support - can be different in 'online' and
	//////// 'replay' modes
	////////////////////////////////////////////////////////////////////
	// Type of device with respect to it's mobility - see MobileType class
	int 		mobileType;

	// Name of flange where equipment is connected - only used for mobile equipment on flanges
	char		*flangeName;

	// Index in sector for mobile equipment located on sectors
	int			indexInSector;

	////////////////////////////////////////////////////////////////////
	//////// 7. Other equipment attributes
	////////////////////////////////////////////////////////////////////
	// List of equipment attributes
	QHash<QByteArray, QString>	*pAttrList;

	// external targets for alarms, provided by this device
	QList<Eqp *>	extTargets;

	// Master process of this device, NULL for most of devices
	Eqp				*pMasterProcess;

	// Usage of this device within master process - just name of attribute in master that refers to this device
	QByteArray		usageInMasterProcess;

	////////////////////////////////////////////////////////////////////
	//////// 8. For active equipment only
	////////////////////////////////////////////////////////////////////
	// List of connected DPEs. It is expected that number of DPEs is small enough,
	// so we don't need to use hash tables for search (QASciiDict or similar storage)
	QList<DpeConnection *>	*pDpeConnections;

	// List of destination objects who connected to signals of this object.
	// Destination may issue several calls connectDpe(), but this does not mean
	// hew wants to be informed several times about every DPE change
	QList<InterfaceEqp *>	*pSignalDestinations;

	// Flag indicating if device is active in online mode
	bool		activeOnline;

	// Flag indicating if device is active in online mode
	bool		activeReplay;

	// Communication object dp Name used for mobile 
	QString 	comDpName;

	////////////////////////////////////////////////////////////////////
	//////// 6. Special properties for device drawing
	////////////////////////////////////////////////////////////////////

#ifndef PVSS_SERVER_VERSION
	// Flag indicating if this device shall not be shown on automatically built synoptic
	bool			skipOnSynoptic;
#endif	// PVSS_SERVER_VERSION

	// Flag indicating if this device is selected
	bool			selected;

	// Flag indicating if this device was already inserted into ordered
	// list of equipment
	bool			ordered;

	// Flag to identify if this device is used by valve as 'previous' interlock source
	bool		intlSourcePrev;

	// Flag to identify if this device is used by valve as 'next' interlock source
	bool		intlSourceNext;

	// Flag to identify if this device is used by ext alarm source
	bool		intlSourceExt;

	// true if beam direction for icons in this line part shall be reverse compared to 'normal'
	bool		reverseBeamDirection;

	// Eqp alert active
	bool		alertActive;
	
	// Eqp alert not acknowledge
	bool		alertNotAck;

	// Eqp as no action possible, is read-only 
	bool		isReadOnly;

private:
	virtual int processSurveyTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		QStringList &errList);
	virtual int processLhcTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		QStringList &errList);
	virtual int processCtlTokens(FILE *pFile, int &pLineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);
};

#endif // ifndef EQP_H
