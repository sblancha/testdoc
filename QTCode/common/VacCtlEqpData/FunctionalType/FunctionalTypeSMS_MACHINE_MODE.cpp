//	Implementation of FunctionalTypeSMS_MACHINE_MODE class
////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeSMS_MACHINE_MODE.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

#include "ResourcePool.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeSMS_MACHINE_MODE::FunctionalTypeSMS_MACHINE_MODE(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeSMS_MACHINE_MODE::fillCriteria(FunctionalType *pType)
{
	// Own criteria
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainValue, Mode, "Machine mode equal");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"SMS machine mode is {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"SMS machine mode is {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"SMS machine mode is {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"SMS machine mode is {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"SMS machine mode is {value}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"SMS machine mode is {value}");
}

void FunctionalTypeSMS_MACHINE_MODE::getCriteriaValues(int critType, int critSubType, QList<int> &values, QList<QByteArray> &labels)
{
	FunctionalType::getCriteriaValues(critType, critSubType, values, labels);	// Clear lists
	if((critType != EqpMsgCriteria::MainValue))
	{
		return;
	}
	QString resourceBase("MachineMode");
	ResourcePool &pool = ResourcePool::getInstance();
	for(int bit = 0 ; bit < 32 ; bit++)
	{
		QString resourceName = resourceBase;
		resourceName += QString::number(bit);
		QString modeName;
		if(pool.getStringValue(resourceName, modeName) == ResourcePool::OK)
		{
			values.append(1 << bit);
			labels.append(modeName.toLatin1());
		}
	}
}

const char *FunctionalTypeSMS_MACHINE_MODE::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainValue:
		if(pCriteria->getSubType() == Mode)
		{
			result = "MODE";
		}
		break;
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeSMS_MACHINE_MODE::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainValue:
		if(!strcasecmp(name, "MODE"))
		{
			result = Mode;
		}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}
