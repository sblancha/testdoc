#ifndef	VACICONBEAM_INTENS_H
#define	VACICONBEAM_INTENS_H

//	Icon for BEAM intensity

#include "VacIcon.h"

class EqpBEAM_Intens;

class VACCTLEWOUTIL_EXPORT VacIconBEAM_Intens : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconBEAM_Intens(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconBEAM_Intens();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpBEAM_Intens *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACICONBEAM_INTENS_H
