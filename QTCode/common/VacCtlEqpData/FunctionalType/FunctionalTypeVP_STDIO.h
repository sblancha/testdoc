#ifndef FUNCTIONALTYPEVP_STDIO_H
#define FUNCTIONALTYPEVP_STDIO_H

// Special functional type for fixed pumping groups - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVP_STDIO : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		On = 1,
		Off = 2
	};

	FunctionalTypeVP_STDIO(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVP_STDIO() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVP_STDIO_H
