//	Implementation of GroupConfig class
/////////////////////////////////////////////////////////////////////////////////

#include "GroupConfig.h"
#include <stdio.h>

GroupConfig::GroupConfig(const QString &groupDpName, const QStringList &simpleDpNames)
	: SmsConfig(groupDpName)
{
	foreach(QString simpleName, simpleDpNames)
	{
		addSimpleConfig(simpleName);
	}
}

GroupConfig::~GroupConfig()
{
	while(!list.isEmpty())
	{
		delete list.takeFirst();
	}
}

SmsConfig *GroupConfig::addSimpleConfig(const QString &dpName)
{
	SmsConfig *pConfig = new SmsConfig(dpName);
	list.append(pConfig);
	connect(pConfig, SIGNAL(changed(SmsConfig *)),
		this, SLOT(configStateChange(SmsConfig *)));
	connect(pConfig, SIGNAL(numMsgChanged(SmsConfig *, int)),
		this, SLOT(configNumMsgChange(SmsConfig *, int)));
	calculateState();
	emit changed(this);
	return pConfig;
}

void GroupConfig::removeSimpleConfig(const QString &dpName)
{
	bool isChanged = false;
	for(int idx = 0 ; idx < list.count() ; idx++)
	{
		SmsConfig *pConfig = list.at(idx);
		if(pConfig->getDpName() == dpName)
		{
			if(!pConfig->isDeleted())
			{
				pConfig->setDeleted(true);
				isChanged = true;
			}
		}
	}
	if(isChanged)
	{
		calculateState();
		emit changed(this);
	}
}

void GroupConfig::setDeleted(bool flag)
{
	if(deleted == flag)
	{
		return;
	}
	deleted = flag;
	for(int idx = 0 ; idx < list.count() ; idx++)
	{
		SmsConfig *pConfig = list.at(idx);
		pConfig->setDeleted(flag);
	}
}

void GroupConfig::calculateState(void)
{
	if(deleted)
	{
		return;
	}
	int nActive = 0, nActivated = 0, nInitialized = 0;
	int newNumMsg = 0, newNumErrors = 0;
	for(int idx = 0 ; idx < list.count() ; idx++)
	{
		SmsConfig *pConfig = list.at(idx);
		if(!pConfig->isDeleted())
		{
			if(pConfig->isActive())
			{
				nActive++;
			}
			if(pConfig->isActivated())
			{
				nActivated++;
			}
			if(pConfig->isInitialized())
			{
				nInitialized++;
			}
			newNumMsg += pConfig->getNumMsg();
			if(!pConfig->getError().isEmpty())
			{
				newNumErrors++;
			}
		}
	}

	bool change = false;
	if(numMsg != newNumMsg)
	{
		numMsg = newNumMsg;
		change = true;
	}
	if(newNumErrors != numErrors)
	{
		numErrors = newNumErrors;
		if(numErrors)
		{
			error = QString::number(numErrors);
			error += " errors";
		}
		else
		{
			error = "";
		}
		change = true;
	}
	QString newState;
	QColor newColor;
	if(!nActive)
	{
		newState = "Inactive";
		newColor.setRgb(COLOR_INACTIVE);
	}
	else if(nInitialized && (nInitialized == (int)list.count()))
	{
		newState = "Running";
		newColor.setRgb(COLOR_RUNNING);
	}
	else
	{
		char buf[64];
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d,%d,%d", nActive, nActivated, nInitialized);
#else
		sprintf(buf, "%d,%d,%d", nActive, nActivated, nInitialized);
#endif
		newState = buf;
		newColor.setRgb(COLOR_STARTING);
	}
	if((newState != state) || (newColor != color))
	{
		state = newState;
		color = newColor;
		change = true;
	}
	if(change)
	{
		emit changed(this);
	}
}

void GroupConfig::configStateChange(SmsConfig * /* pSrc */)
{
	calculateState();
}

void GroupConfig::configNumMsgChange(SmsConfig *pSrc, int delta)
{
	if(pSrc->isDeleted())
	{
		return;
	}
	if(delta)
	{
		numMsg += delta;
		emit changed(this);
	}
}
