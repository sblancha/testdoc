#pragma once
#include "VacIcon.h"

class EqpEXT_ALARM;

class VACCTLEWOUTIL_EXPORT VacIconEXT_ALARM : public VacIcon
{
	Q_OBJECT

public:
	VacIconEXT_ALARM(QWidget *parent, Qt::WindowFlags f = 0);
	~VacIconEXT_ALARM();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);
	virtual void setMode(DataEnum::DataMode /* newMode */) {}
	virtual bool connect(void);
	virtual bool disconnect(void);

	// Override method of VacIcon 
	virtual void setDirection(VacIconContainer::Direction newDirection);

protected:
	// Pointer to my device
	EqpEXT_ALARM *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
};
