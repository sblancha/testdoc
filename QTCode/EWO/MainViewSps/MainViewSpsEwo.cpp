//	Implementation of MainSpsEwo class
/////////////////////////////////////////////////////////////////////////////////

#include "MainViewSpsEwo.h"

#include "DpConnection.h"
#include "DpeHistoryQuery.h"
#include "DpPolling.h"

EWO_PLUGIN(MainViewSpsEwo)


MainViewSpsEwo::MainViewSpsEwo(QWidget *parent) : VacMainViewEwo(parent)
{
	baseWidget = new MainSps(parent);
	connectStdSignals();
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList MainViewSpsEwo::methodList(void) const
{
	QStringList list;
	list.append("int dataReady()");
	list.append("bool isSelected()");
	list.append("bool isContSelection()");
	list.append("dyn_string getSelectedSectors()");
	list.append("int addLegendLabel(int x, int y, string text");
	list.append("int addRotatedLegendLabel(int x, int y, string text");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewSpsEwo::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "dataReady")
	{
		return invokeDataReady(name, values, error);
	}
	else if(name == "isSelected")
	{
		return invokeIsSelected(name, values, error);
	}
	else if(name == "isContSelection")
	{
		return invokeIsContSelection(name, values, error);
	}
	else if(name == "getSelectedSectors")
	{
		return invokeGetSelectedSectors(name, values, error);
	}
	else if(name == "addLegendLabel")
	{
		return invokeAddLegendLabel(name, values, error);
	}
	else if(name == "addRotatedLegendLabel")
	{
		return invokeAddRotatedLegendLabel(name, values, error);
	}
	else
	{
		error = "Uknown method MainViewSpsEwo.";
		error += name;
	}
	return QVariant();
}

/*
**	FUNCTION
**		Execute method
**			int dataReady()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewSpsEwo::invokeDataReady(const QString & /* name */, QList<QVariant> & /* values */,
	QString & /* error */)
{
	//printf("%s: invokeDataReady() start\n", QDateTime::currentDateTime().toString().toLatin1().constData());

	// Execute and return result
	int coco = ((MainSps *)baseWidget)->dataReady();
	//printf("%s: invokeDataReady() dataReady() finished\n", QDateTime::currentDateTime().toString().toLatin1().constData());
	return QVariant(coco);
}

/*
**	FUNCTION
**		Execute method
**			bool isSelected()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewSpsEwo::invokeIsSelected(const QString & /* name */, QList<QVariant> & /* values */,
	QString & /* error */)
{
	return QVariant(((MainSps *)baseWidget)->isSelected());
}

/*
**	FUNCTION
**		Execute method
**			bool isContSelection()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewSpsEwo::invokeIsContSelection(const QString & /* name */, QList<QVariant> & /* values */,
	QString & /* error */)
{
	return QVariant(((MainSps *)baseWidget)->isContSelection());
}

/*
**	FUNCTION
**		Execute method
**			bool getSelectedSectors()
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewSpsEwo::invokeGetSelectedSectors(const QString & /* name */, QList<QVariant> & /* values */,
	QString & /* error */)
{
	return QVariant(((MainSps *)baseWidget)->getSelectedSectors());
}

/*
**	FUNCTION
**		Execute method
**			int addLegendLabel(int x, int y, string text)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewSpsEwo::invokeAddLegendLabel(const QString &name, QList<QVariant> &values,
	QString & error)
{
	if(!hasNumArgs(name, values, 3, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (x) is not int";
		return QVariant();
	}
	int x = values[0].toInt();
	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (y) is not int";
		return QVariant();
	}
	int y = values[1].toInt();
	if(!values[2].canConvert(QVariant::String))
	{
		error = "argument 3 (text) is not string";
		return QVariant();
	}
	const QString text = values[2].toString();
	((MainSps *)baseWidget)->addLegendLabel(x, y, text.toLatin1());
	return QVariant((int)0);
}

/*
**	FUNCTION
**		Execute method
**			int addRotatedLegendLabel(int x, int y, int angle, string text)
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant MainViewSpsEwo::invokeAddRotatedLegendLabel(const QString &name, QList<QVariant> &values,
	QString & error)
{
	if(!hasNumArgs(name, values, 4, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (x) is not int";
		return QVariant();
	}
	int x = values[0].toInt();
	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (y) is not int";
		return QVariant();
	}
	int y = values[1].toInt();
	if(!values[2].canConvert(QVariant::Int))
	{
		error = "argument 3 (angle) is not int";
		return QVariant();
	}
	int angle = values[2].toInt();
	if(!values[3].canConvert(QVariant::String))
	{
		error = "argument 4 (text) is not string";
		return QVariant();
	}
	const QString text = values[3].toString();
	((MainSps *)baseWidget)->addRotatedLegendLabel(x, y, angle, text.toLatin1());
	return QVariant((int)0);
}
