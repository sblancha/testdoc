//	Implementation of LhcRingSelection class
////////////////////////////////////////////////////////////////////////////////////

#include "LhcRingSelection.h"

#include "DataPool.h"
#include "Eqp.h"
#include "EqpType.h"

#include "FunctionalType.h"
#include "EqpMsgCriteria.h"

#include <qstringlist.h>

#include <stdlib.h>

LhcRingSelection::LhcRingSelection()
{
	mainVacMask = vacMask = 0;
	pFirstSector = pLastSector = NULL;
}

/*
**	FUNCTION
**		Find for given main part another main part of given vacuum type.
**		The method is introduced to find main part Q for main part M and
**		vice versa.
**		
**	PARAMETERS
**    	pMainPart	- Pointer to main part for which adjucent main part shall be found
**		vacType		- Required vacuum type of adjucent main part
**
**	RETURNS
**		Pointer to adjucent main part, or
**		NULL if no such main part found
*/
MainPart *LhcRingSelection::getAdjucentMainPart(MainPart *pMainPart, int vacType)
{
	DataPool &pool = DataPool::getInstance();
	QList<Sector *> &sectors = pool.getSectors();
	for(int idx = 0 ; idx < sectors.count() ; idx++)
	{
		Sector *pSector = sectors.at(idx);
		if(!pSector->isInMainPart(pMainPart))
		{
			continue;
		}
		for(int otherIdx = 0 ; otherIdx < sectors.count() ; otherIdx++)
		{
			Sector *pOtherSector = sectors.at(otherIdx);
			if(pOtherSector->getVacType() != vacType)
			{
				continue;
			}
			if(pOtherSector->getStart() > pSector->getEnd())
			{
				continue;
			}
			if(pOtherSector->getEnd() < pSector->getStart())
			{
				continue;
			}
			if(pOtherSector->getMainPartMaps().count() != 1)
			{
				continue;
			}
			return pOtherSector->getMainPartMaps().first()->getMainPart();
		}
	}
	return NULL;
}


/*
**	FUNCTION
**		Build LHC ring selection range for one or more main parts selected.
**
**	PARAMETERS
**		mpList		- List of main parts
**		sectorList	- Resulting list of sector names will be put to this variable
**
**	RETURNS
**		None
**
** CAUTIONS
**		It is assumed that main part contains either beam vacuum sectors, or
**		isolation vacuum sector, NOT BOTH.
*/
void LhcRingSelection::buildRangeFromMps(QList<MainPart *> &mpList, QStringList &sectorList)
{
	DataPool	&pool = DataPool::getInstance();
	int			absStartSectIdx = -1, absEndSectIdx = -1;
	int			startSectIdx, endSectIdx;

	// Find range of sector indices and summary vacuum type
	for(int mpIdx = 0 ; mpIdx < mpList.count() ; mpIdx++)
	{
		MainPart *pMainPart = mpList.at(mpIdx);
		if(!mpRangeOnLhcRing(pMainPart, startSectIdx, endSectIdx))
		{
			continue;
		}
		// Update resulting vacuum type
		vacMask |= pMainPart->getVacTypeMask();
		// Update absolute start and end sector indices
		if(absStartSectIdx < 0)
		{
			absStartSectIdx = startSectIdx;
		}
		else if(startSectIdx < absStartSectIdx)
		{
			absStartSectIdx = startSectIdx;
		}
		if(absEndSectIdx < 0)
		{
			absEndSectIdx = endSectIdx;
		}
		else if(endSectIdx > absEndSectIdx)
		{
			absEndSectIdx = endSectIdx;
		}
	}

	// Build list of sectors
	sectorList.clear();
	if(absStartSectIdx < 0)
	{
		pFirstSector = pLastSector = NULL;
		return;
	}
	QList<Sector *> &sectors = pool.getSectors();
	pLastSector = sectors.at(absEndSectIdx);
	pFirstSector = sectors.at(absStartSectIdx);
	int sectIdx = absStartSectIdx;
	do
	{
		Sector *pSector = sectors.at(sectIdx);
		bool found = false;
		for(int mpIdx = 0 ; mpIdx < mpList.count() ; mpIdx++)
		{
			MainPart *pMainPart = mpList.at(mpIdx);
			if(pSector->isInMainPart(pMainPart))
			{
				found = true;
				break;
			}
		}
		if(found)
		{
			if(mainVacMask != VacType::None)
			{
				if(pSector->getStart() < pFirstSector->getStart())
				{
					pFirstSector = pSector;
				}
				if(pSector->getEnd() > pLastSector->getEnd())
				{
					pLastSector = pSector;
				}
			}
			addSectorToList(sectorList, pSector);
		}
		if(sectIdx == absEndSectIdx)
		{
			break;
		}
		sectIdx++;
		if(sectIdx >= (int)sectors.count())
		{
			sectIdx = 0;
		}
	} while(sectIdx != absStartSectIdx);
	return;
}

/*
**	FUNCTION
**		Find indices of first and last sectors on main part
**
**	PARAMETERS
**		pMainPart		- Pointer to main part
**		startSectIdx	- Index of first sector in main part is returned here
**		endSectIdx		- Index of last sector in main part is returned here
**
**	RETURNS
**		true	- If sector range has been found;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool LhcRingSelection::mpRangeOnLhcRing(MainPart *pMainPart, int &startSectIdx, int &endSectIdx)
{
	if(pMainPart->getVacTypeMask() & (VacType::Qrl | VacType::Cryo))	// Isolation vacuum
	{
		return mpRangeOnLhcRingIsol(pMainPart, startSectIdx, endSectIdx);
	}
	return mpRangeOnLhcRingBeam(pMainPart, startSectIdx, endSectIdx);
}

/*
**	FUNCTION
**		Find indices of first and last sectors on main part
**		for beam vacuum on LHC ring.
**		The main assumption is: all beam vacuum sectors on
**		LHC ring are limited by controllable valves.
**
** PARAMETERS
**		pMainPart		- Pointer to main part
**		startSectIdx	- Index of first sector in main part is returned here
**		endSectIdx		- Index of last sector in main part is returned here
**
** RETURNS
**		true	- If sector range has been found;
**		false	- otherwise
**
** CAUTIONS
**		None
*/
bool LhcRingSelection::mpRangeOnLhcRingBeam(MainPart *pMainPart, int &startSectIdx, int &endSectIdx)
{
	if(mpRangeOnLhcRingBeamFromNeighbour(pMainPart, startSectIdx, endSectIdx))
	{
		return true;
	}
	return mpRangeOnLhcRingBeamNoNeighbour(pMainPart, startSectIdx, endSectIdx);
}

/*
**	FUNCTION
**		Find indices of first and last sectors on main part
**		for beam vacuum on LHC ring.
**		The main assumption is: all beam vacuum sectors on
**		LHC ring are limited by controllable valves.
**		It is supposed that sectors on the border of main part exist
**
**	PARAMETERS
**		pMainPart		- Pointer to main part
**		startSectIdx	- Index of first sector in main part is returned here
**		endSectIdx		- Index of last sector in main part is returned here
**
**	RETURNS
**		true	- If sector range has been found;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool LhcRingSelection::mpRangeOnLhcRingBeamFromNeighbour(MainPart *pMainPart, int &startSectIdx, int &endSectIdx)
{
	startSectIdx = endSectIdx = -1;
	BeamLine *pLine = pMainPart->getMainLine();
	QList<BeamLinePart *> parts = pLine->getParts();
	DataPool &pool = DataPool::getInstance();
	QList<Sector *> &sectors = pool.getSectors();
	for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);
		// Traverse list of device, not array - order is not important here
		QList<Eqp *> eqpList = pPart->getEqpList();
		for(int eqpIdx = 0 ; eqpIdx < eqpList.count() ; eqpIdx++)
		{
			Eqp *pEqp = eqpList.at(eqpIdx);
			if(pEqp->getType() != EqpType::VacDevice)
			{
				break;	// Active equipment is at the beginning of list
			}
			// Is this device sector border?
			if(!pEqp->isSectorBorder())
			{
				continue;
			}
			// Is previous sector in main part?
			Sector *pSectorBefore = pEqp->getSectorBefore();
			bool	prevInMp = false;
			if(pSectorBefore)
			{
				prevInMp = pSectorBefore->isInMainPart(pMainPart);
			}
			// Is next sector in main part?
			Sector *pSectorAfter = pEqp->getSectorAfter();
			bool	nextInMp = false;
			if(pSectorAfter)
			{
				nextInMp = pSectorAfter->isInMainPart(pMainPart);
			}
			// Decide...
			if(prevInMp)
			{
				if(nextInMp)	// Both previous and next sector are in main part:
								// previous sector can not be the last sector of main part,
								// next sector can not be the first sector of main part
				{
					if(sectors.indexOf(pSectorBefore) == endSectIdx)
					{
						endSectIdx = -1;
					}
					if(sectors.indexOf(pSectorAfter) == startSectIdx)
					{
						startSectIdx = -1;
					}
					
				}
				else	// Next sector is not in main part: previous sector potentially can
						// be the last sector of main part
				{
					int	sectIdx = sectors.indexOf(pSectorBefore);
					if(endSectIdx < 0)
					{
						endSectIdx = sectIdx;
					}
					else if(sectIdx > endSectIdx)
					{
						endSectIdx = sectIdx;
					}
				}
			}
			else	// Previous sector is not in main part
			{
				if(nextInMp)	// Next sector is in main part: next sector potentially can
								// be the first sector of main part
				{
					int	sectIdx = sectors.indexOf(pSectorAfter);
					if(startSectIdx < 0)
					{
						startSectIdx = sectIdx;
					}
					else if(sectIdx < startSectIdx)
					{
						startSectIdx = sectIdx;
					}
				}
			}
		}
	}
	return (startSectIdx >= 0) && (endSectIdx >= 0);
}

/*
**	FUNCTION
**		Find indices of first and last sectors on main part
**		for beam vacuum on LHC ring.
**		The main assumption is: all beam vacuum sectors on
**		LHC ring are limited by controllable valves.
**		It is supposed that sectors on the border of main part do NOT exist
**
**	PARAMETERS
**		pMainPart		- Pointer to main part
**		startSectIdx	- Index of first sector in main part is returned here
**		endSectIdx		- Index of last sector in main part is returned here
**
**	RETURNS
**		true	- If sector range has been found;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool LhcRingSelection::mpRangeOnLhcRingBeamNoNeighbour(MainPart *pMainPart, int &startSectIdx, int &endSectIdx)
{
	startSectIdx = endSectIdx = -1;
	BeamLine *pLine = pMainPart->getMainLine();
	QList<BeamLinePart *> &parts = pLine->getParts();
	DataPool &pool = DataPool::getInstance();
	QList<Sector *> &sectors = pool.getSectors();
	for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pPart = parts.at(partIdx);

		// Traverse list of device, not array - order is not important here
		QList<Eqp *> &eqpList = pPart->getEqpList();
		for(int eqpIdx = eqpList.count() - 1 ; eqpIdx >= 0 ; eqpIdx--)
		{
			Eqp *pEqp = eqpList.at(eqpIdx);
			if(pEqp->getType() != EqpType::VacDevice)
			{
				break;	// Active equipment is at the beginning of list
			}
			// Is this device sector border?
			if(!pEqp->isSectorBorder())
			{
				continue;
			}
			// Is previous sector in main part?
			Sector *pSectorBefore = pEqp->getSectorBefore();
			bool	prevInMp = false;
			if(pSectorBefore)
			{
				prevInMp = pSectorBefore->isInMainPart(pMainPart);
			}
			// Is next sector in main part?
			Sector *pSectorAfter = pEqp->getSectorAfter();
			bool	nextInMp = false;
			if(pSectorAfter)
			{
				nextInMp = pSectorAfter->isInMainPart(pMainPart);
			}
			// Decide...
			if(prevInMp)
			{
				int sectIdx = sectors.indexOf(pSectorBefore);
				if(nextInMp)	// Both previous and next sector are in main part:
								// previous sector can not be the last sector of main part,
								// next sector can not be the first sector of main part
				{
					if(startSectIdx < 0)	// Previous sector potentially can be first sector of main part
					{
						startSectIdx = sectIdx;
					}
					else if(startSectIdx > sectIdx)
					{
						startSectIdx = sectIdx;
					}
					sectIdx = sectors.indexOf(pSectorAfter);
					if(endSectIdx < 0)	// Next sector potentially can be last sector of main part
					{
						endSectIdx = sectIdx;
					}
					else if(endSectIdx < sectIdx)
					{
						endSectIdx = sectIdx;
					}
				}
				else	// Next sector is not in main part: previous sector potentially can
						// be the last sector of main part
				{
					if(endSectIdx < 0)
					{
						endSectIdx = sectIdx;
					}
					else if(sectIdx > endSectIdx)
					{
						endSectIdx = sectIdx;
					}
				}
			}
			else	// Previous sector is not in main part
			{
				if(nextInMp)	// Next sector is in main part: next sector potentially can
								// be the first sector of main part
				{
					int sectIdx = sectors.indexOf(pSectorAfter);
					if(startSectIdx < 0)
					{
						startSectIdx = sectIdx;
					}
					else if(sectIdx < startSectIdx)
					{
						startSectIdx = sectIdx;
					}
				}
			}
		}
	}
	return (startSectIdx >= 0) && (endSectIdx >= 0);
}

/*
**	FUNCTION
**		Find indices of first and last sectors on main part
**		for isolation vacuum on LHC ring.
**		The main assumption is: neither of main parts crosses IP1,
**		i.e. location with coordinate 0
**
** PARAMETERS
**		pMainPart		- Pointer to main part
**		startSectIdx	- Index of first sector in main part is returned here
**		endSectIdx		- Index of last sector in main part is returned here
**
**	RETURNS
**		true	- If sector range has been found;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool LhcRingSelection::mpRangeOnLhcRingIsol(MainPart *pMainPart, int &startSectIdx, int &endSectIdx)
{
	startSectIdx = endSectIdx = -1;
	DataPool &pool = DataPool::getInstance();
	QList<Sector *> &sectors = pool.getSectors();
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		if(!pSector->isInMainPart(pMainPart))
		{
			continue;
		}
		if(!(pSector->getVacType() & (VacType::Qrl | VacType::Cryo)))	// Exclude DSL sector(s)
		{
			continue;
		}
		if(startSectIdx < 0)
		{
			startSectIdx = sectIdx;
		}
		endSectIdx = sectIdx;
	}
	return (startSectIdx >= 0) && (endSectIdx >= 0);
}

/*
**	FUNCTION
**		Add name of sector to list of names. Before adding check: if the
**		sector to be added is CRYO sector and there is DSL line connected
**		in this sector - add name of sector on DSL line BEFORE this sector.
**
**	PARAMETERS
**		sectorList	- List of sectors where name shall be added
**		pSector		- Pointer to sector whose name shall be added
**
**	RETURNS
**		Number of sectors added to list: 1 or 2
**
**	CAUTIONS
**		None
*/
int LhcRingSelection::addSectorToList(QStringList &sectorList, Sector *pSector)
{
	Sector	*pChildSector = NULL;

	if(pSector->getVacType() == VacType::Cryo)
	{
		DataPool &pool = DataPool::getInstance();
		BeamLine *pLhc = pool.getCircularLine();
		if(pLhc)
		{
			QList<BeamLinePart *> &parts = pLhc->getParts();
			for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
			{
				BeamLinePart *pPart = parts.at(partIdx);
				Eqp	**eqpPtrs = pPart->getEqpPtrs();
				int	nEqpPtrs = pPart->getNEqpPtrs();
				for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(pEqp->getStart() > pSector->getEnd())
					{
						break;
					}
					if(pEqp->getStart() < pSector->getStart())
					{
						continue;
					}
					if(pEqp->getVacType() != pSector->getVacType())
					{
						continue;
					}
					if((pEqp->getType() != EqpType::OtherLineStart) &&
						(pEqp->getType() != EqpType::OtherLineEnd))
					{
						continue;
					}
					// Another line found. It is expected that child only contains one sector
					BeamLine *pChildLine = pool.getLine(pEqp->getName());
					if(pChildLine)
					{
						QList<BeamLinePart *> &childParts = pChildLine->getParts();
						for(int childPartIdx = 0 ; childPartIdx < childParts.count() ; childPartIdx++)
						{
							BeamLinePart *pChildPart = childParts.at(childPartIdx);
							Eqp **childEqpPtrs = pChildPart->getEqpPtrs();
							int nChildEqpPtrs = pChildPart->getNEqpPtrs();
							for(int childEqpIdx = 0 ; childEqpIdx < nChildEqpPtrs ; childEqpIdx++)
							{
								pChildSector = childEqpPtrs[childEqpIdx]->getSectorBefore();
								if(pChildSector && (pChildSector != pSector))
								{
									break;
								}
								else
								{
									pChildSector = NULL;
								}
							}
							if(pChildSector)
							{
								break;
							}
						}
					}
					if(pChildSector)
					{
						break;
					}
				}
				if(pChildSector)
				{
					break;
				}
			}
		}
	}
	int		coco = 1;
	if(pChildSector)
	{
		sectorList.append(pChildSector->getName());
		coco++;
	}
	sectorList.append(pSector->getName());
	return coco;
}

/*
**	FUNCTION
**		Find intensity measurement devices in data pool. Result shall be
**		ordered by vacuum type of devices found. It is expected that only
**		2 such devices exist in LHC.
**
**	PARAMETERS
**		result	- List where found devices shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void LhcRingSelection::getBeamIntesityEqp(QList<Eqp *> &result)
{
	result.clear();
	QList<Eqp *> &ctlList = DataPool::getInstance().getCtlList();
	for(int idx = ctlList.count() - 1 ; idx >= 0 ; idx--)
	{
		Eqp *pEqp = ctlList.at(idx);
		if(pEqp->getFunctionalType() == FunctionalType::BEAM_INT)
		{
			result.append(pEqp);
		}
	}
}

