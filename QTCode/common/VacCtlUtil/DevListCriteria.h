#ifndef	DEVLISTCRITERIA_H
#define DEVLISTCRITERIA_H

#include "VacCtlUtilExport.h"
#include "DataEnum.h"

#include <qdatetime.h>

// Class holding set of device list criteria

class VACCTLUTIL_EXPORT DevListCriteria
{
public:

	// Enumeration for possible device states with respect to 'normal' state
	enum
	{
		EqpStateNormal = 1,
		EqpStateAbnormal = 2,
		EqpStateOther = 3
	};

	static DevListCriteria &getInstance(void);

	bool isEmpty(void) const;

	bool hasStateCriteria(void) const;

	bool generalStateMatches(int state);

	DevListCriteria();
	~DevListCriteria() {}

	// Access
	inline bool isNormal(void) const { return normal; }
	inline void setNormal(bool use) { normal = use; }
	inline bool isNotNormal(void) const { return notNormal; }
	inline void setNotNormal(bool use) { notNormal = use; }
	inline bool isAbnormal(void) const { return abnormal; }
	inline void setAbnormal(bool use) { abnormal = use; }
	inline bool isNotAbnormal(void) const { return notAbnormal; }
	inline void setNotAbnormal(bool use) { notAbnormal = use; }
	inline bool isErrors(void) const { return errors; }
	inline void setErrors(bool use) { errors = use; }
	inline bool isWarnings(void) const { return warnings; }
	inline void setWarnings(bool use) { warnings = use; }
	inline bool isAlerts(void) const { return alerts; }
	inline void setAlerts(bool use) { alerts = use; }
	inline bool isAccess(void) const { return access; }
	inline void setAccess(bool use) { access = use; }
	inline bool isNotConnected(void) const { return notConnected; }
	inline void setNotConnected(bool use) { notConnected = use; }
	inline bool isNotActive(void) const { return notActive; }
	inline void setNotActive(bool use) { notActive = use; }

	inline bool isUsePressureLimit(void) const { return usePressureLimit; }
	inline void setUsePressureLimit(bool use) { usePressureLimit = use; }
	inline float getPressureLimit(void) const { return pressureLimit; }
	inline void setPressureLimit(float limit) { pressureLimit = limit; }
	inline bool isUsePressureGrowLimit(void) const { return usePressureGrowLimit; }
	inline void setUsePressureGrowLimit(bool use) { usePressureGrowLimit = use; }
	inline float getPressureGrowLimit(void) const { return pressureGrowLimit; }
	inline void setPressureGrowLimit(float limit) { pressureGrowLimit = limit; }
	inline const QDateTime &getPressureStartTime(void) const { return pressureStartTime; }
	inline void setPressureStartTime(const QDateTime &value) { pressureStartTime = value; }
	inline const QDateTime &getPressureEndTime(void) const { return pressureEndTime; }
	inline void setPressureEndTime(const QDateTime &value) { pressureEndTime = value; }
	inline bool isPressureEndTimeNow(void) const { return pressureEndTimeNow; }
	inline void setPressureEndTimeNow(bool value) { pressureEndTimeNow = value; }
	inline int getPressureSpikeInterval(void) const { return pressureSpikeInterval; }
	inline void setPressureSpikeInterval(int interval) { if (interval < 0) interval = 0; pressureSpikeInterval = interval; }

	inline bool isUseTemperatureLimit(void) const { return useTemperatureLimit; }
	inline void setUseTemperatureLimit(bool use) { useTemperatureLimit = use; }
	inline float getTemperatureLimit(void) const { return temperatureLimit; }
	inline void setTemperatureLimit(float limit) { temperatureLimit = limit; }
	inline bool isUseTemperatureGrowLimit(void) const { return useTemperatureGrowLimit; }
	inline void setUseTemperatureGrowLimit(bool use) { useTemperatureGrowLimit = use; }
	inline float getTemperatureGrowLimit(void) const { return temperatureGrowLimit; }
	inline void setTemperatureGrowLimit(float limit) { temperatureGrowLimit = limit; }
	inline const QDateTime &getTemperatureStartTime(void) const { return temperatureStartTime; }
	inline void setTemperatureStartTime(const QDateTime &value) { temperatureStartTime = value; }
	inline const QDateTime &getTemperatureEndTime(void) const { return temperatureEndTime; }
	inline void setTemperatureEndTime(const QDateTime &value) { temperatureEndTime = value; }
	inline bool isTemperatureEndTimeNow(void) const { return temperatureEndTimeNow; }
	inline void setTemperatureEndTimeNow(bool value) { temperatureEndTimeNow = value; }
	inline int getTemperatureSpikeInterval(void) const { return temperatureSpikeInterval; }
	inline void setTemperatureSpikeInterval(int interval) { if (interval < 0) interval = 0; temperatureSpikeInterval = interval; }

	inline DataEnum::DataMode getMode(void) const { return mode; }
	inline void setMode(DataEnum::DataMode newMode) { mode = newMode; }
	inline const QDateTime &getTime(void) const { return time; }
	inline void setTime(const QDateTime &newTime) { time = newTime; }

protected:

	// The only instance
	static DevListCriteria	instance;

	// Start time of interval for pressure grow search
	QDateTime	pressureStartTime;

	// End time of interval for pressure grow search
	QDateTime	pressureEndTime;

	// Start time of interval for temperature grow search
	QDateTime	temperatureStartTime;

	// End time of interval for temperature grow search
	QDateTime	temperatureEndTime;

	// Pressure limit
	float		pressureLimit;

	// Pressure grow limit
	float		pressureGrowLimit;

	// Spike filtering interval for pressures [sec]
	int			pressureSpikeInterval;

	// Temperature limit
	float		temperatureLimit;

	// Temperature grow limit
	float		temperatureGrowLimit;

	// Spike filtering interval for temperatures [sec]
	int			temperatureSpikeInterval;


	// Device is in NORMAL state
	bool		normal;

	// Device is NOT in NORMAL state
	bool		notNormal;

	// Device is in ABNORMAL state
	bool		abnormal;

	// Device is NOT in ABNORMAL state
	bool		notAbnormal;

	// There are errors in device
	bool		errors;

	// There are warnings (and, may be, errors) in device
	bool		warnings;

	// Device produces alerts
	bool		alerts;

	// Access error for devices
	bool		access;

	// Fix Devices not connected
	bool		notConnected;

	// Wireless mobile not connected
	bool		notActive;

	// Use pressure limit criteria
	bool		usePressureLimit;

	// Use pressure grow limit criteria
	bool		usePressureGrowLimit;

	// Use temperature limit criteria
	bool		useTemperatureLimit;

	// Use temperature grow limit criteria
	bool		useTemperatureGrowLimit;

	// Use current time as end of interval for pressure grow search
	bool		pressureEndTimeNow;

	// Use current time as end of interval for temperature grow search
	bool		temperatureEndTimeNow;


	// Mode - Online or Replay
	DataEnum::DataMode	mode;

	// Data moment for Replay mode
	QDateTime	time;
};

#endif	// DEVLISTCRITERIA_H
