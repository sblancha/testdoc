#ifndef	VACICONVPS_H
#define	VACICONVPS_H

//	Icon for VPS

#include "VacIcon.h"

class EqpVPS;

class VACCTLEWOUTIL_EXPORT VacIconVPS : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPS(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPS();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVPS *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter);
	virtual void drawLetters(QPainter &painter);
	virtual void setPainterForImage(QPainter &painter);
};

#endif	// VACICONVPS_H
