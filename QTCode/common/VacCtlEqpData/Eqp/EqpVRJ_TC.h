#ifndef	EQPVRJ_TC_H
#define	EQPVRJ_TC_H

//	VRJ_TC - Patch panel for thermocouples

#include "Eqp.h"


class VACCTLEQPDATA_EXPORT EqpVRJ_TC : public Eqp
{
	Q_OBJECT

public:
	EqpVRJ_TC(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVRJ_TC(const EqpVRJ_TC &src) : Eqp(src) {}
#endif
	~EqpVRJ_TC();

	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getSlaves(QStringList &slaves);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	}
	inline unsigned getRR1(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? rr1Replay : rr1Online;
	}
	inline unsigned getStatus(DataEnum::DataMode mode) const
	{
		return mode == DataEnum::Replay ? statusReplay : statusOnline;
	}
	virtual int getErrorCode(DataEnum::DataMode mode);

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR2 DPE for online mode
	unsigned	statusOnline;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of RR2 DPE for replay mode
	unsigned	statusReplay;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned status, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, unsigned status, bool plcAlarm, QString &string);
#endif

};

#endif	// EQPVRJ_TC_H
