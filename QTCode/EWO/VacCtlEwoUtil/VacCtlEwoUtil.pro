#	Project file for qmake utility to build auxilliary library for vacuum control EWOs
#
#	14.12.2008	L.Kopylov
#		Initial version
#
#
#	02.02.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	14.02.2010	L.Kopylov
#		Changes for DLL on Windows, move VacIcon to this directory
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#
#   08.07.2014  S. Blanchard
#		Add VG_DP_VGF_RP, VG_DP_VGM
#   29.07.2014  S. Blanchard
#		Add VPC_HCCC
#   19.09.2014  S. Blanchard
#		Add VV_PUL
#   14.12.2015  S. Blanchard
#		Add VP_I0
#   30.01.2016  S. Blanchard
#		Add VP_STD_IO_C0 (Cryopump compressor)
#   04.02.2016  S. Blanchard
#		Add VP_TP (Turbo pump profibus interface
#   14.10.2016  S. Blanchard
#		Replace  VP_I0 by VP_IP
#   28.03.2018	S. Blanchard
#		Add VP_GU (Vacuum Pump - Group Unified)
#   08.08.2018  S. BLanchard
#   		Add VR_ER (Vacuum contRol - bakE-Out Rack)
#   08.10.2018  S. BLanchard
#   		Add VA_RI
#   01.05.2019 S. BLanchard
#		    Add VG_U, GaugeMembrane, GaugePirani, GaugePenning, GaugeDualRC

TEMPLATE = lib

TARGET = VacCtlEwoUtil

# To avoid having version numbers in .so file name
unix:CONFIG += plugin

OBJECTS_DIR = ../obj
DESTDIR = ../../../bin

QT += widgets printsupport

unix:CONFIG += qt thread release warn_on
# separate_debug_info
win32:CONFIG += qt dll thread release

# To produce PDB file together with DLL
win32:QMAKE_CXXFLAGS+=/Zi
win32:QMAKE_LFLAGS+= /INCREMENTAL:NO /Debug


DEFINES += BUILDING_VACCTLEWOUTIL

#DEFINES += _VACDEBUG
#DEFINES += _VACDEBUG_HISTORY
#DEFINES += XML_PANEL_PARSE

INCLUDEPATH += $$(API_ROOT)/include/EWO

unix: LIBS += -L$$(API_ROOT)/lib.$$(PLATFORM) -lewo
win32:LIBS += -L$$(API_ROOT)/lib.winnt        -lewo

INCLUDEPATH += ./ \
	Synoptic \
	Axis \
	Profile \
	ExcelWriter \
	HistoryProcessor \
	Synoptic \
	SynopticLhc \
	ProfileLhc \
	SectorView \
	VacIcon \
	XmlPanel \
	RackEquipmentIcon \
	InterlockIcon \
	../../common/VacCtlUtil \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp \
	../../common/VacCtlEqpData/Rack \
	../../common/VacCtlEqpData/Interlock \
	../../common/VacCtlEqpData/MobilePool \
	../../common/VacCtlEqpData/ResourcePool \
	../../common/Platform

HEADERS = VacCtlEwoUtilExport.h \
	LegendLabel.h \
	HistoryDataRange.h \
	HistoryEqpData.h \
	ScreenPoint.h \
	VacLinePart.h \
	LhcLineParts.h \
	LhcRingSelection.h \
	SynopticWidget.h \
	ExtendedLabel.h \
	VacMainView.h \
	VacMainViewEwo.h \
	ValveInterlockSource.h \
	Documentation/RackDocumentationDialog.h \
	Documentation/RackView.h \
	Documentation/RackWidget.h \
	Documentation/InterlockView.h \
	Documentation/InterlockDocumentationDialog.h \
	Synoptic/SynEqpItem.h \
	Synoptic/ChildSynLine.h \
	Synoptic/SynLine.h \
	Synoptic/SynView.h \
	Synoptic/SynViewXml.h \
	Synoptic/SynopticDialog.h \
	Axis/VerticalAxis.h \
	Axis/VerticalAxisWidget.h \
	Axis/TimeAxis.h \
	Axis/TimeAxisWidget.h \
	Profile/ProfileItem.h \
	Profile/ProfileSector.h \
	Profile/BeamLineWeight.h \
	Profile/BeamLineStats.h \
	Profile/ProfileImage.h \
	Profile/ProfileDialog.h \
	History/PlotImage.h \
	History/HistoryImage.h \
	History/HistoryDialog.h \
	History/HistoryTimeRangeDialog.h \
	ExcelWriter/ExcelWriterCell.h \
	ExcelWriter/ExcelWriterRow.h \
	ExcelWriter/ExcelWriter.h \
	ExcelWriter/ExcelWriterWidget.h \
	HistoryProcessor/HistoryEqpValue.h \
	HistoryProcessor/HistoryEqpValuesSet.h \
	HistoryProcessor/HistoryProcessor.h \
	SectorView/SectLine.h \
	SectorView/SectView.h \
	SectorView/SectViewXml.h \
	SectorView/SectorViewDialog.h \
	ProfileAllSPS/ProfileAllSPSWidget.h \
	ProfileAllSPS/ProfileAllSPS.h \
	SynopticLhc/SynLhcEqpItem.h \
	SynopticLhc/SynLhcSectorLabel.h \
	SynopticLhc/SynLhcLine.h \
	SynopticLhc/SynLhcLinePassive.h \
	SynopticLhc/SynLhcLineIsol.h \
	SynopticLhc/SynLhcInjDumpLine.h \
	SynopticLhc/SynLhcLineBeam.h \
	SynopticLhc/SynLhcLineBeamCommon.h \
	SynopticLhc/SynLhcView.h \
	SynopticLhc/SynopticLhcDialog.h \
	ProfileLhc/ProfileLhcLineItem.h \
	ProfileLhc/ProfileThermometer.h \
	ProfileLhc/ProfileLineSector.h \
	ProfileLhc/ProfileLhcVpgConnector.h \
	ProfileLhc/ProfileLhcLine.h \
	ProfileLhc/ProfileLhcLineBeam.h \
	ProfileLhc/RealCryoSector.h \
	ProfileLhc/ProfileLhcLineIsol.h \
	ProfileLhc/ProfileLhcView.h \
	ProfileLhc/ProfileLhcDialog.h \
	ProfileAllLHCInnerOuter/ProfileAllLHCInOutWidget.h \
	ProfileAllLHCInnerOuter/ProfileAllLHCInOut.h \
	ProfileAllLHCBeam/ProfileLhcSingleBeamLine.h \
	ProfileAllLHCBeam/ProfileLhcSingleBeamView.h \
	ProfileAllLHCBeam/ProfileAllLHCBeamWidget.h \
	ProfileAllLHCBeam/ProfileAllLHCBeam.h \
	ProfileAllLhcIsol/ProfileAllLhcIsolWidget.h \
	SynopticAllLhc/SynopticAllLhcWidget.h \
	SynopticAllLhc/SynopticAllLhc.h \
	SectorViewLhc/SectLhcInjDumpLine.h \
	SectorViewLhc/SectLhcLine.h \
	SectorViewLhc/SectLhcLinePassive.h \
	SectorViewLhc/SectLhcView.h \
	SectorViewLhc/SectorViewLhcDialog.h \
	Mobile/MobileEqpTree.h \
	Mobile/MobileTreeItemTypes.h \
	Mobile/MobileHistoryEqpData.h \
	Mobile/MobileHistoryImage.h \
	Mobile/MobileColorList.h \
	Mobile/MainPartSectorTreeItem.h \
	Mobile/EqpTreeItem.h \
	Mobile/BakeoutEqpTreeItem.h \
	Mobile/BakeoutChannelTreeItem.h \
	Mobile/MobileHistoryDialog.h \
	Mobile/BakeoutTree.h \
	Mobile/BakeoutStateConnection.h \
	Mobile/BakeoutRackChannel.h \
	Mobile/BakeoutCtlChannel.h \
	Mobile/BakeoutRackTable.h \
	Mobile/BakeoutCtlTable.h \
	Mobile/BakeoutRackDetailsDialog.h \
	Mobile/BakeoutPlotImage.h \
	Mobile/BakeoutRackProfileImage.h \
	Mobile/BakeoutRackProfileDialog.h \
	VacIcon/VacIconGeometry.h \
	VacIcon/VacIconContainer.h \
	VacIcon/VacIconConnection.h \
	VacIcon/VacIcon.h \
#	VacIcon/VacIconToolTip.h \
	VacIcon/VacIconVVS_PS.h \
	VacIcon/VacIconVVS_S.h \
	VacIcon/VacIconVVF_S.h \
	VacIcon/FlashingRect.h \
	VacIcon/VacIconVVS_LHC.h \
	VacIcon/VacIconVLV.h \
	VacIcon/VacIconVG.h \
	VacIcon/VacIconVGM.h \
	VacIcon/VacIconVGR.h \
	VacIcon/VacIconVGP.h \
	VacIcon/VacIconVGI.h \
	VacIcon/VacIconVGF.h \
	VacIcon/VacIconVG_U.h \
	VacIcon/VacIconGaugeMembrane.h \
	VacIcon/VacIconGaugePirani.h \
	VacIcon/VacIconGaugePenning.h \
	VacIcon/VacIconGaugeDualRP.h \
	VacIcon/VacIconVG_STD.h \
	VacIcon/VacIconVGM_STD.h \
	VacIcon/VacIconVGR_STD.h \
	VacIcon/VacIconVGP_STD.h \
	VacIcon/VacIconVGI_STD.h \
	VacIcon/VacIconVGF_STD_RP.h \
	VacIcon/VacIconVGF_STD_RI.h \
	VacIcon/VacIconVPI.h \
	VacIcon/VacIconMP_ACCESS.h \
	VacIcon/VacIconSECT_VPI_SUM.h \
	VacIcon/VacIconVP_GU.h \
	VacIcon/VacIconVPG_SA.h \
	VacIcon/VacIconVPG_SA_VG.h \
	VacIcon/VacIconVPG_BP.h \
	VacIcon/VacIconVPG_STD.h \
	VacIcon/VacIconVPG_MBLK.h \
	VacIcon/VacIconVPG_EA_SPS.h \
	VacIcon/VacIconVP_STDIO.h \
	VacIcon/VacIconCRYO_TT.h \
	VacIcon/VacIconVGTR.h \
	VacIcon/VacIconEXP_AREA.h \
	VacIcon/VacIconCOLDEX.h \
	VacIcon/VacIconEXT_ALARM.h \
  VacIcon/VacIconVA_RI.h \
	VacIcon/VacIconVPGMPR.h \
	VacIcon/VacIconVPGM.h \
	VacIcon/VacIconCRYO_TT_SUM.h \
	VacIcon/VacIconBEAM_Intens.h \
	VacIcon/VacIconVLV_ANA.h \
	VacIcon/VacIconVGA_VELO.h \
	VacIcon/VacIconVGD_VELO.h \
	VacIcon/VacIconVPR_VELO.h \
	VacIcon/VacIconVPT_VELO.h \
	VacIcon/VacIconVOPS_VELO.h \
	VacIcon/VacIconVINT_VELO.h \
	VacIcon/VacIconVPRO_VELO.h \
	VacIcon/VacIconVPT100.h \
	VacIcon/VacIconVV_PLUS_PP.h \
	VacIcon/VacIconVIES.h \
	VacIcon/VacIconVVS_PS_CMW.h \
	VacIcon/VacIconVPI_PS_CMW.h \
	VacIcon/VacIconVG_PS_CMW.h\
	VacIcon/VacIconVGR_PS_CMW.h \
	VacIcon/VacIconVGP_PS_CMW.h \
	VacIcon/VacIconVGI_PS_CMW.h \
	VacIcon/VacIconVV_PULSED.h \
	VacIcon/VacIconVV_STD_IO.h \
	VacIcon/VacIconVV_PUL.h \
	VacIcon/VacIconVV_AO.h \
	VacIcon/VacIconVP_STD_IO.h \
	VacIcon/VacIconVP_STD_IO_PP.h \
	VacIcon/VacIconVP_STD_IO_TMP.h \
	VacIcon/VacIconVP_STD_IO_C0.h \	
	VacIcon/VacIconVPG_6A01.h \
	VacIcon/VacIconVPG_6A01_1VVR_NO_VG.h \
	VacIcon/VacIconVPG_6A01_1VVR_VG.h \
	VacIcon/VacIconVPG_6A01_2VVR_NO_VG.h \
	VacIcon/VacIconVPG_6A01_2VVR_VG.h \
	VacIcon/VacIconBGI_6B01.h \
	VacIcon/VacIconINJ_6B02.h \
	VacIcon/VacIconVPG_6E01.h \
	VacIcon/VacIconVRJ_TC.h \
	VacIcon/VacIconVPN.h \
#	VacIcon/VacIconVG_DP.h \
	VacIcon/VacIconVG_DP_VGF_RP.h\
	VacIcon/VacIconVG_DP_VGM.h\
	VacIcon/VacIconVBAR.h \
	VacIcon/VacIconVPC_HCCC.h \
	VacIcon/VacIconVPS.h \
	VacIcon/VacIconVG_PT_P.h \
	VacIcon/VacIconVP_IP.h \
	VacIcon/VacIconVP_TP.h \
	VacIcon/VacIconV8DI_FE.h \
	VacIcon/VacIconVR_ER.h \
	VacIcon/VacIconVG_A_RO.h \
	VacIcon/VacIconVOPS_2PS.h \
	XmlPanel/ImageItem.h \
	XmlPanel/ImageItemLine.h \
	XmlPanel/ImageItemRectangle.h\
	XmlPanel/ImageItemPolygon.h \
	XmlPanel/ImageItemEllipse.h\
	XmlPanel/ImageItemText.h \
	XmlPanel/ImageItemFrame.h \
	XmlPanel/ImageItemGeneric.h \
	XmlPanel/XmlSynoptic.h \
	RackEquipmentIcon/RackIcon.h \
	RackEquipmentIcon/RackIconVRPIT.h \
	RackEquipmentIcon/RackIconVRGPT300.h \
	RackEquipmentIcon/RackIconVRGPK.h \
	RackEquipmentIcon/RackIconGeneric.h \
	RackEquipmentIcon/RackIconHidden.h \
	RackEquipmentIcon/RackIconVRPTP350.h \
	RackEquipmentIcon/RackIconVRLRI.h \
	RackEquipmentIcon/RackIconVRVCL.h \
	RackEquipmentIcon/RackIconVRVCX.h \
	InterlockIcon/InterlockIcon.h \
	InterlockIcon/InterlockIconConnection.h \
	InterlockIcon/InterlockIconVRGPT.h \
	InterlockIcon/InterlockIconVRIVC.h \
	InterlockIcon/InterlockIconVRIVD.h \
	InterlockIcon/InterlockIconVRPIT.h \
	InterlockIcon/InterlockIconVRVCL.h \
	InterlockIcon/InterlockIconVRVCX.h 
	
SOURCES = LegendLabel.cpp \
	HistoryDataRange.cpp \
	HistoryEqpData.cpp \
	ScreenPoint.cpp \
	VacLinePart.cpp \
	LhcLineParts.cpp \
	LhcRingSelection.cpp \
	SynopticWidget.cpp \
	ExtendedLabel.cpp \
	VacMainView.cpp \
	VacMainViewEwo.cpp \
	ValveInterlockSource.cpp \
	Documentation/RackDocumentationDialog.cpp \
	Documentation/RackView.cpp \
	Documentation/InterlockView.cpp \
	Documentation/InterlockDocumentationDialog.cpp \
	Documentation/RackWidget.cpp \
	Synoptic/SynEqpItem.cpp \
	Synoptic/ChildSynLine.cpp \
	Synoptic/SynLine.cpp \
	Synoptic/SynView.cpp \
	Synoptic/SynViewXml.cpp \
	Synoptic/SynopticDialog.cpp \
	Axis/VerticalAxis.cpp \
	Axis/VerticalAxisWidget.cpp \
	Axis/TimeAxis.cpp \
	Axis/TimeAxisWidget.cpp \
	Profile/ProfileItem.cpp \
	Profile/ProfileSector.cpp \
	Profile/BeamLineStats.cpp \
	Profile/ProfileImage.cpp \
	Profile/ProfileDialog.cpp \
	History/PlotImage.cpp \
	History/HistoryImage.cpp \
	History/HistoryDialog.cpp \
	History/HistoryTimeRangeDialog.cpp \
	ExcelWriter/ExcelWriterCell.cpp \
	ExcelWriter/ExcelWriterRow.cpp \
	ExcelWriter/ExcelWriter.cpp \
	ExcelWriter/ExcelWriterWidget.cpp \
	HistoryProcessor/HistoryEqpValue.cpp \
	HistoryProcessor/HistoryEqpValuesSet.cpp \
	HistoryProcessor/HistoryProcessor.cpp \
	SectorView/SectLine.cpp \
	SectorView/SectView.cpp \
	SectorView/SectViewXml.cpp \
	SectorView/SectorViewDialog.cpp \
	ProfileAllSPS/ProfileAllSPSWidget.cpp \
	ProfileAllSPS/ProfileAllSPS.cpp \
	SynopticLhc/SynLhcEqpItem.cpp \
	SynopticLhc/SynLhcSectorLabel.cpp \
	SynopticLhc/SynLhcLine.cpp \
	SynopticLhc/SynLhcLinePassive.cpp \
	SynopticLhc/SynLhcLineIsol.cpp \
	SynopticLhc/SynLhcInjDumpLine.cpp\
	SynopticLhc/SynLhcLineBeam.cpp \
	SynopticLhc/SynLhcLineBeamCommon.cpp \
	SynopticLhc/SynLhcView.cpp \
	SynopticLhc/SynopticLhcDialog.cpp \
	ProfileLhc/ProfileLhcLineItem.cpp \
	ProfileLhc/ProfileThermometer.cpp \
	ProfileLhc/ProfileLineSector.cpp \
	ProfileLhc/ProfileLhcVpgConnector.cpp \
	ProfileLhc/ProfileLhcLine.cpp \
	ProfileLhc/ProfileLhcLineBeam.cpp \
	ProfileLhc/RealCryoSector.cpp \
	ProfileLhc/ProfileLhcLineIsol.cpp \
	ProfileLhc/ProfileLhcView.cpp \
	ProfileLhc/ProfileLhcDialog.cpp \
	ProfileAllLHCInnerOuter/ProfileAllLHCInOutWidget.cpp \
	ProfileAllLHCInnerOuter/ProfileAllLHCInOut.cpp \
	ProfileAllLHCBeam/ProfileLhcSingleBeamLine.cpp \
	ProfileAllLHCBeam/ProfileLhcSingleBeamView.cpp \
	ProfileAllLHCBeam/ProfileAllLHCBeamWidget.cpp \
	ProfileAllLHCBeam/ProfileAllLHCBeam.cpp \
	ProfileAllLhcIsol/ProfileAllLhcIsolWidget.cpp \
	SynopticAllLhc/SynopticAllLhcWidget.cpp \
	SynopticAllLhc/SynopticAllLhc.cpp \
	SectorViewLhc/SectLhcInjDumpLine.cpp\
	SectorViewLhc/SectLhcLine.cpp \
	SectorViewLhc/SectLhcLinePassive.cpp \
	SectorViewLhc/SectLhcView.cpp \
	SectorViewLhc/SectorViewLhcDialog.cpp \
	Mobile/MobileEqpTree.cpp \
	Mobile/MobileHistoryEqpData.cpp \
	Mobile/MobileHistoryImage.cpp \
	Mobile/MobileColorList.cpp \
	Mobile/MainPartSectorTreeItem.cpp \
	Mobile/EqpTreeItem.cpp \
	Mobile/BakeoutEqpTreeItem.cpp \
	Mobile/BakeoutChannelTreeItem.cpp \
	Mobile/MobileHistoryDialog.cpp \
	Mobile/BakeoutTree.cpp \
	Mobile/BakeoutStateConnection.cpp \
	Mobile/BakeoutRackChannel.cpp \
	Mobile/BakeoutCtlChannel.cpp \
	Mobile/BakeoutRackTable.cpp \
	Mobile/BakeoutCtlTable.cpp \
	Mobile/BakeoutRackDetailsDialog.cpp \
	Mobile/BakeoutPlotImage.cpp \
	Mobile/BakeoutRackProfileImage.cpp \
	Mobile/BakeoutRackProfileDialog.cpp \
	VacIcon/VacIconGeometry.cpp \
	VacIcon/VacIconContainer.cpp \
	VacIcon/VacIconConnection.cpp \
	VacIcon/VacIcon.cpp \
#	VacIcon/VacIconToolTip.cpp \
	VacIcon/VacIconVVS_PS.cpp \
	VacIcon/VacIconVVS_S.cpp \
	VacIcon/VacIconVVF_S.cpp \
	VacIcon/FlashingRect.cpp \
	VacIcon/VacIconVVS_LHC.cpp \
	VacIcon/VacIconVLV.cpp \
	VacIcon/VacIconVG.cpp \
	VacIcon/VacIconVGM.cpp \
	VacIcon/VacIconVGR.cpp \
	VacIcon/VacIconVGP.cpp \
	VacIcon/VacIconVGI.cpp \
	VacIcon/VacIconVGF.cpp \
	VacIcon/VacIconVG_U.cpp \
	VacIcon/VacIconGaugeMembrane.cpp \
	VacIcon/VacIconGaugePirani.cpp \
	VacIcon/VacIconGaugePenning.cpp \
	VacIcon/VacIconGaugeDualRP.cpp \
	VacIcon/VacIconVG_STD.cpp \
	VacIcon/VacIconVGM_STD.cpp \
	VacIcon/VacIconVGR_STD.cpp \
	VacIcon/VacIconVGP_STD.cpp \
	VacIcon/VacIconVGI_STD.cpp \
	VacIcon/VacIconVGF_STD_RP.cpp \
	VacIcon/VacIconVGF_STD_RI.cpp \
	VacIcon/VacIconVPI.cpp \
	VacIcon/VacIconMP_ACCESS.cpp \
	VacIcon/VacIconSECT_VPI_SUM.cpp \
	VacIcon/VacIconVP_GU.cpp \
	VacIcon/VacIconVPG_SA.cpp \
	VacIcon/VacIconVPG_SA_VG.cpp \
	VacIcon/VacIconVPG_BP.cpp \
	VacIcon/VacIconVPG_STD.cpp \
	VacIcon/VacIconVPG_MBLK.cpp \
	VacIcon/VacIconVPG_EA_SPS.cpp \
	VacIcon/VacIconVP_STDIO.cpp \
	VacIcon/VacIconCRYO_TT.cpp \
	VacIcon/VacIconVGTR.cpp \
	VacIcon/VacIconEXP_AREA.cpp \
	VacIcon/VacIconCOLDEX.cpp \
	VacIcon/VacIconEXT_ALARM.cpp \
  VacIcon/VacIconVA_RI.cpp \
	VacIcon/VacIconVPGMPR.cpp \
	VacIcon/VacIconVPGM.cpp \
	VacIcon/VacIconCRYO_TT_SUM.cpp \
	VacIcon/VacIconBEAM_Intens.cpp \
	VacIcon/VacIconVLV_ANA.cpp \
	VacIcon/VacIconVGA_VELO.cpp \
	VacIcon/VacIconVGD_VELO.cpp \
	VacIcon/VacIconVPR_VELO.cpp \
	VacIcon/VacIconVPT_VELO.cpp \
	VacIcon/VacIconVOPS_VELO.cpp \
	VacIcon/VacIconVINT_VELO.cpp \
	VacIcon/VacIconVPRO_VELO.cpp \
	VacIcon/VacIconVPT100.cpp \
	VacIcon/VacIconVV_PLUS_PP.cpp\
	VacIcon/VacIconVIES.cpp \
	VacIcon/VacIconVVS_PS_CMW.cpp \
	VacIcon/VacIconVPI_PS_CMW.cpp \
	VacIcon/VacIconVG_PS_CMW.cpp \
	VacIcon/VacIconVGR_PS_CMW.cpp \
	VacIcon/VacIconVGP_PS_CMW.cpp \
	VacIcon/VacIconVGI_PS_CMW.cpp \
	VacIcon/VacIconVV_PULSED.cpp \
	VacIcon/VacIconVV_STD_IO.cpp \
	VacIcon/VacIconVV_PUL.cpp \
	VacIcon/VacIconVV_AO.cpp \
	VacIcon/VacIconVP_STD_IO.cpp \
	VacIcon/VacIconVP_STD_IO_PP.cpp \
	VacIcon/VacIconVP_STD_IO_TMP.cpp \
	VacIcon/VacIconVP_STD_IO_C0.cpp \
	VacIcon/VacIconVPG_6A01.cpp \
	VacIcon/VacIconVPG_6A01_1VVR_NO_VG.cpp \
	VacIcon/VacIconVPG_6A01_1VVR_VG.cpp \
	VacIcon/VacIconVPG_6A01_2VVR_NO_VG.cpp \
	VacIcon/VacIconVPG_6A01_2VVR_VG.cpp \
	VacIcon/VacIconBGI_6B01.cpp \
	VacIcon/VacIconINJ_6B02.cpp \
	VacIcon/VacIconVPG_6E01.cpp \
	VacIcon/VacIconVRJ_TC.cpp \
	VacIcon/VacIconVPN.cpp \
#	VacIcon/VacIconVG_DP.cpp \
	VacIcon/VacIconVG_DP_VGF_RP.cpp \
	VacIcon/VacIconVG_DP_VGM.cpp \
	VacIcon/VacIconVBAR.cpp \
	VacIcon/VacIconVPC_HCCC.cpp \
	VacIcon/VacIconVPS.cpp \
	VacIcon/VacIconVG_PT_P.cpp \
	VacIcon/VacIconVP_IP.cpp \
	VacIcon/VacIconVP_TP.cpp \
	VacIcon/VacIconV8DI_FE.cpp \
	VacIcon/VacIconVR_ER.cpp \
	VacIcon/VacIconVG_A_RO.cpp \
	VacIcon/VacIconVOPS_2PS.cpp \
	XmlPanel/ImageItem.cpp \
	XmlPanel/ImageItemLine.cpp \
	XmlPanel/ImageItemRectangle.cpp \
	XmlPanel/ImageItemPolygon.cpp \
	XmlPanel/ImageItemEllipse.cpp \
	XmlPanel/ImageItemText.cpp \
	XmlPanel/ImageItemFrame.cpp \
	XmlPanel/ImageItemGeneric.cpp \
	XmlPanel/XmlSynoptic.cpp \
	RackEquipmentIcon/RackIcon.cpp \
	RackEquipmentIcon/RackIconVRPIT.cpp \
	RackEquipmentIcon/RackIconVRGPT300.cpp \
	RackEquipmentIcon/RackIconVRGPK.cpp \
	RackEquipmentIcon/RackIconGeneric.cpp \
	RackEquipmentIcon/RackIconHidden.cpp \
	RackEquipmentIcon/RackIconVRPTP350.cpp \
	RackEquipmentIcon/RackIconVRLRI.cpp \
	RackEquipmentIcon/RackIconVRVCL.cpp \
	RackEquipmentIcon/RackIconVRVCX.cpp \
	InterlockIcon/InterlockIcon.cpp \
	InterlockIcon/InterlockIconConnection.cpp \
	InterlockIcon/InterlockIconVRGPT.cpp \
	InterlockIcon/InterlockIconVRIVC.cpp \
	InterlockIcon/InterlockIconVRIVD.cpp \
	InterlockIcon/InterlockIconVRPIT.cpp \
	InterlockIcon/InterlockIconVRVCL.cpp \
	InterlockIcon/InterlockIconVRVCX.cpp 


LIBS += -L../../../bin \
	-lVacCtlEqpData \
	-lVacCtlUtil
