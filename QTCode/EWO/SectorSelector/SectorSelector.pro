#
# Project file for qmake utility to build SectorSelector EWO
#
#	12.03.2010	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = SectorSelector

INCLUDEPATH += ../../common/VacCtlUtil \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/Platform \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp \
	../VacCtlEwoUtil

HEADERS =	\
	ColResizeTable.h \
	SectorSelectorControl.h \
	SectorSelector.h

SOURCES =	\
	ColResizeTable.cpp \
	SectorSelectorControl.cpp \
	SectorSelector.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlEqpData \
	-lVacCtlHistoryData \
	-lVacCtlUtil

