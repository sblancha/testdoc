#ifndef RACKICONVRVCL_H
#define RACKICONVRVCL_H

#include "RackIcon.h"

class Eqp;

class VACCTLEWOUTIL_EXPORT RackIconVRVCL : public RackIcon
{
	Q_OBJECT
public:
	RackIconVRVCL(QWidget * parent, Qt::WindowFlags f = 0);
	virtual ~RackIconVRVCL();

	virtual void setEqp(Eqp * pEqp);
	virtual bool connect(void);
	virtual bool disconnect(void);
protected:
	Eqp * pEqp; //pointer to equipment, if it exists
	virtual void paintEvent(QPaintEvent *pEvent);

};

#endif
