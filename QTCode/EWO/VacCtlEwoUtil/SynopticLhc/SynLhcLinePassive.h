#ifndef SYNLHCLINEPASSIVE_H
#define	SYNLHCLINEPASSIVE_H

// Synoptic line containing passive equipment

#include "SynLhcLine.h"

class SynLhcLinePassive : public SynLhcLine
{
public:
	SynLhcLinePassive(QWidget *parent, int type, float start, float end);
	virtual ~SynLhcLinePassive();

	virtual void analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask &allCryoEqpMask, VacEqpTypeMask &allBeamEqpMask);
	virtual LhcLineParts::BuildType linePartMethod(void) { return LhcLineParts::TypeBeam; }
	virtual float nextPosition(void);
	virtual void placeItem(int order, float &coord, int &connPos, float &endCoord, int &endConnPos);
	virtual void calcVerticalLimits(void);
	virtual int draw(QPainter &painter, int viewStart, int viewEnd);

	virtual bool getToolTip(const QPoint &point, QString &text, QRect &rect);

	virtual void dump(FILE *pFile);

protected:
	virtual bool isMySector(Sector * /* pSector */) { return false; }
	virtual bool isMyEqp(Eqp *pEqp, int &partIdx);
	virtual int vacTypeOfRegion(int /* regionType */) { return VacType::CrossBeam; }
};

#endif	// SYNLHCLINEPASSIVE_H
