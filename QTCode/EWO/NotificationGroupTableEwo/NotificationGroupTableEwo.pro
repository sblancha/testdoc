#
# Project file for qmake utility to build NotificationGroupTableEwo EWO
#

include(../VacEWO.pri)

TARGET = NotificationGroupTableEwo

INCLUDEPATH += ./ \
	../../common/VacCtlUtil \
	../../common/Platform \
	../VacCtlEwoUtil 


HEADERS =	\
	GroupConfig.h \
	GroupTable.h \
	GroupTableModel.h \
	SmsGroup.h \
  SmsConfig.h \
	NotificationGroupTableEwo.h

SOURCES =	\
	GroupConfig.cpp \
	GroupTable.cpp \
	GroupTableModel.cpp \
	SmsGroup.cpp \
  SmsConfig.cpp \
	NotificationGroupTableEwo.cpp

LIBS += -lVacCtlEwoUtil \
				-lVacCtlUtil
				

