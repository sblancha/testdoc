#ifndef	INTERLOCKICONVRIVC_H
#define	INTERLOCKICONVRIVC_H

#include "qwidget.h"
#include "InterlockIcon.h"

class InterlockIconVRIVC : public InterlockIcon
{
	Q_OBJECT
public:
	InterlockIconVRIVC(QWidget * parent = NULL);
	inline InterlockIconClass getIconClass() { return TypeCInterlock; };
	void moveIcon(int x, int y);
	QPoint getConnectionPoint(QString connectorName);
	void getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber);
	QRect getBoundingRect();
	virtual ~InterlockIconVRIVC();
private:
	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif