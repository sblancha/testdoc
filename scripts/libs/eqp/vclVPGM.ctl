

// Bitmasks for VPGM_(B,C,D) status bits (RR1 - 32 bits)
const unsigned 	VPGM_R_OFF = 0x00010000u,
	VPGM_R_ON = 0x00020000u,
	VPGM_R_INTLCK = 0x00200000u,
	VPGM_R_WARNINGS = 0x00400000u,
	VPGM_R_ERRORS = 0x00800000u,
	VPGM_R_AUTO = 0x08000000u,
	VPGM_R_REMOTE = 0x20000000u,
	VPGM_R_VALID = 0x40000000u,
	
// Bitmasks for VPGM_(B,C,D) status bits (RR2 - 32 bits)


	VPGM_R_VVR_CLOSED = 0x00000100u,
	VPGM_R_VVR_OPEN = 0x00000200u,
	VPGM_R_VVR_OPEN_ENABLE = 0x00000400u,
	VPGM_R_VVR_INTERLOCK = 0x00002000u,
	VPGM_R_VVR_WARNINGS = 0x00004000u,
	VPGM_R_VVR_ERRORS = 0x00008000u,
	VPGM_R_PP_ON = 0x00010000u,
	VPGM_R_PP_OK = 0x00020000u,
	VPGM_R_TMP_ON = 0x00080000u,
	VPGM_R_TMP_OK = 0x00100000u,
	VPGM_R_TMP_SPEED_OK = 0x00400000u,
	VPGM_R_VVP_CLOSED = 0x01000000u,
	VPGM_R_VVP_OPEN = 0x02000000u,
	VPGM_R_VVI_CLOSED = 0x04000000u,
	VPGM_R_VVI_OPEN = 0x08000000u,
	VPGM_R_VVD_CLOSED = 0x10000000u,
	VPGM_R_VVD_OPEN = 0x20000000u,
	VPGM_R_VVT_CLOSED = 0x40000000u,
	VPGM_R_VVT_OPEN = 0x80000000u;
const string  VV_COLOR_ERROR = "[100,50,100]", // to set background for detail panel
              VPGM_COLOR_ICON_NORMAL = "{240,240,240}";
	
	
			


// Bitmasks for VPGM_(B,C,D) command bits (WR1 - 16 bits)
const unsigned 	VPGM_W_VVR_CLOSE = 0x0001u,
	VPGM_W_VVR_OPEN = 0x0002u,
	VPGM_W_OFF = 0x0100u,
	VPGM_W_ON = 0x0200u,
	VPGM_W_MANUAL = 0x0400u,
	VPGM_W_AUTO = 0x0800u,
	VPGM_W_LOCAL = 0x1000u,
	VPGM_W_REMOTE = 0x2000;



//LhcVacDeviceAction_VPG:
/** Execute action for VPG. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VPGM(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo )
{
  unsigned		newWr1;
  bool			possible;
  dyn_errClass		err;
  unsigned              act;
        
  act = action[1] & LVA_ACTION_MASK;

  switch( act & LVA_ACTION_MASK )
  {
  case LVA_DEVICE_ON:
    newWr1 = VPGM_W_ON | VPGM_W_REMOTE;
    break;
  case LVA_DEVICE_OFF:
    newWr1 = VPGM_W_OFF | VPGM_W_REMOTE;
    break;
  case LVA_DEVICE_VVR_OPEN:
    newWr1 = VPGM_W_VVR_OPEN | VPGM_W_REMOTE;
    break;
  case LVA_DEVICE_VVR_CLOSE:
    newWr1 = VPGM_W_VVR_CLOSE | VPGM_W_REMOTE;
    break;
  default:
    fwException_raise( exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "" );
    return;
  }

  // Execute
  dpSetWait( dpName + ".WR1:_original.._value", newWr1 );
  err = getLastError();
  if( dynlen( err ) > 0 )
  {
    fwException_raise( exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "" );
    return;
  }
}



//LhcVacActionPossible_VPGM:
/** Check if VGP is in state that allows executing actions for it
// At this moment we have no criteria to forbide action
Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, action to be executed
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_VPGM(string dpName, string dpType, dyn_anytype act, bool &possible,
	dyn_string &exceptionInfo )
{
	possible = true;
}
//LhcVacDeviceVVRBackColorVPGM:
/** Form enumerated color value and PVSS color string depending on VVR of VPG state.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param mainData: in, contains device specific data rr2
	@param colorString: out, PVSS color string is returned here.
*/
void LhcVacDeviceVVRBackColorVPGM( unsigned rr2, string &stringColor )
{
  stringColor = "";
  if( ( rr2 & VPGM_R_VVR_ERRORS ) != 0 )
  {
    if( ( rr2 & VPGM_R_VVR_OPEN ) != 0 )
    {
      if( ( rr2 & VPGM_R_VVR_CLOSED ) != 0 )
      {
        stringColor = VV_COLOR_ERROR;
      }
      else
      {
        stringColor = VPGM_COLOR_ICON_NORMAL;
      }		
    }
  }
}
