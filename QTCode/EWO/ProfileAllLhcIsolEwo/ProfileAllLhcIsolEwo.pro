#
# Project file for qmake utility to build Profile all of LHC - Isolation vacuum
#
#	07.10.2010	L.Kopylov
#		Initial version
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = ProfileAllLhcIsolEwo

INCLUDEPATH += ../VacCtlEwoUtil \
	../VacCtlEwoUtil/ProfileAllLhcIsol \
	../VacCtlEwoUtil/ProfileLhc \
	../VacCtlEwoUtil/Axis \
	../../common/VacCtlUtil \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/Platform \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/Eqp

HEADERS = ProfileAllLhcIsolEwo.h

SOURCES = ProfileAllLhcIsolEwo.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlHistoryData \
	-lVacCtlEqpData \
	-lVacCtlUtil

