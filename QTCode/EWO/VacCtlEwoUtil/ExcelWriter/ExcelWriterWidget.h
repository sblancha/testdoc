#ifndef	EXCELWRITERWIDGET_H
#define	EXCELWRITERWIDGET_H

#include "VacCtlEwoUtilExport.h"

// Class providing visible interface to ExcelWriter.
// Visually it appears as normal QPushButton

#include "ExcelWriter.h"

#include "HistoryProcessor.h"

#include <qpushbutton.h>

class VACCTLEWOUTIL_EXPORT ExcelWriterWidget : public QPushButton
{
	Q_OBJECT

public:
	ExcelWriterWidget(QWidget *parent);
	ExcelWriterWidget(const QString &text, QWidget *parent);
	ExcelWriterWidget(const QIcon &icon, const QString &text, QWidget *parent);
	~ExcelWriterWidget() {}

	void clear(void) { writer.clear(); }
	int addRow(void) { return writer.addRow(); }
	void setCellFormat(int row, int column, int format)
		{ writer.setCellFormat(row, column, (ExcelWriterCell::Format)format); }
	void setCellColor(int row, int column, const QColor &color)
		{ writer.setCellColor(row, column, color); }
	void setCellValue(int row, int column, const QString &string)
		{ writer.setCellValue(row, column, string); }
	void setCellValue(int row, int column, const QDateTime &dateTime, bool withMsec)
		{ writer.setCellValue(row, column, dateTime, withMsec); }
	void setCellValue(int row, int column, float value)
		{ writer.setCellValue(row, column, value); }
	void setCellComment(int row, int column, const QString &comment)
		{ writer.setCellComment(row, column, comment); }

	void setColumnTitle(int column, const QString &title)
		{ writer.setColumnTitle(column, title); }
	void setColumnWidth(int column, int columnWidth)
		{ writer.setColumnWidth(column, columnWidth); }

	void clearHistory() { history.clear(); }
	void addHistoryValue(const char *dp, const QDateTime &ts, float value);

	QString write(void);
	QString writeHistory(void);

signals:
	void contentRequested(void);

protected:
	virtual void mousePressEvent(QMouseEvent *pEvent);

private:
	// Instance of writer
	ExcelWriter	writer;

	// Instance of analog history processor
	HistoryProcessor	history;

	// Name of file selected
	QString		fileName;

	// Flag indicating if file shall be saved in CSV format (default = XML for Excel)
	bool		writeCsv;

private slots:
	void buttonClick(void);
	void selectXmlFormat(void);
	void selectCsvFormat(void);
};

#endif	// EXCELWRITERWIDGET_H
