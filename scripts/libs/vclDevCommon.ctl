
// Common functionality for all vacuum euqipment DP types.
// From here device (type) specific functions located in specific
// libraries are called
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "vclArchiveSession.ctl"	// Load CTRL library
// Equipment type specific libraries
#uses "eqp/vclEqpCtlStatus.ctl"
#uses "eqp/vclOK.ctl"	// Load CTRL library
#uses "eqp/vclCommon.ctl"	// Load CTRL library
#uses "eqp/vclVG.ctl"	// Load CTRL library
#uses "eqp/vclVG_U.ctl"	// Load CTRL library
#uses "eqp/vclVP.ctl"	// Load CTRL library
#uses "eqp/vclVPG.ctl"	// Load CTRL library
#uses "eqp/vclVPGM.ctl"	// Load CTRL library
#uses "eqp/vclVV.ctl"	// Load CTRL library
#uses "eqp/vclVINTVELO.ctl"	// Load CTRL library
#uses "eqp/vclVOPSVELO.ctl"	// Load CTRL library
#uses "eqp/vclVPROVELO.ctl"	// Load CTRL library
#uses "eqp/vclVPVELO.ctl"	// Load CTRL library
#uses "eqp/vclAccessMode.ctl"	// Load CTRL library
#uses "eqp/vclAccessZone.ctl"	// Load CTRL library
#uses "eqp/vclVCRYO_TT.ctl"	// Load CTRL library
#uses "eqp/vclVGTR.ctl"	// Load CTRL library
// #uses "eqp/vclVBeamIntens.ctl"	// Load CTRL library
#uses "eqp/vclVPT100.ctl"	// Load CTRL library
#uses "eqp/vclUserProfile.ctl"	// Load CTRL library
#uses "eqp/vclVRPM.ctl"	// Load CTRL library
#uses "eqp/vclProcess.ctl" // Load CTRL library
#uses "eqp/vclPLC.ctl" // Load CTRL library
#uses "eqp/vclDIMON.ctl" // Load CTRL library
#uses "eqp/vclVPNEG.ctl" // Load CTRL library
#uses "eqp/vclVPC_HCCC.ctl" // Load CTRL library
#uses "eqp/vclVITL.ctl"
#uses "eqp/vclVR.ctl"
#uses "eqp/vclVRJ_TC.ctl"
#uses "eqp/vclVSector.ctl" // [VACCO-929] [VACCO-948] [VACCO-1645]
#uses "eqp/vclVG_A_RO.ctl" 
#uses "eqp/vclVOPS.ctl"
#uses "eqp/vclV_TCP.ctl"
#uses "eqp/vclVELOProcess.ctl" //VELO Process

// Constants definining source
const int SOURCE_OWN = 0,
    SOURCE_MASTER = 1,
    SOURCE_PLC =2;

// Constants identifying part of device (for complex devices)
const int
  EQP_PART_DEFAULT = 0,  // Default part of device
  EQP_PART_PUMP = 1,     // Pump part of VPG (combination of PP and TMP if VPG has both)
  EQP_PART_VVR = 2,      // Roughing valve (if there is only one)
  EQP_PART_VVR1 = 3,     // Roughing valve #1 (if there are two valves)
  EQP_PART_VVR2 = 4,     // Roughing valve #2 (if there are two valves)
  EQP_PART_PP = 5,       // Primary pump of VPG
  EQP_PART_TMP = 6,      // Turbomol. pump of VPG
  EQP_PART_VVD = 7,      // VVD of VPG
  EQP_PART_VVI = 8,      // VVI of VPG
  EQP_PART_VVP = 9,      // VVP of VPG
  EQP_PART_VVT = 10,     // VVT of VPG
  EQP_PART_VOPS_A = 11,  // Channel A of VOPS_VELO
  EQP_PART_VOPS_B = 12,  // Channel B of VOPS_VELO
  EQP_PART_WMA = 13,     // current set for VRPM
  EQP_PART_WMV = 14,     // voltage set for VRPM
  EQP_PART_RMA = 15,     // current measurement for VRPM
  EQP_PART_RMV = 16,     // voltage measurement for VRPM
  EQP_PART_CB = 17,      // circuit breaker for VRPM
  EQP_PART_VVG = 18,     // dosing valve for VINJ_B02
  EQP_PART_VPC = 19, 	 // Cryo pump controller device
  EQP_PART_VV_C = 20,    // Valve with local controller (Ex: PUL)
  EQP_PART_VG1 = 21,     // Vacuum Gauge 1 VPG MBLK
  EQP_PART_VG2 = 22;     // Vacuum Gauge 2 VPG MBLK

// Colors common for all devices
const string
    TEXT_COLOR_ERR = "[100,100,90]",
    TEXT_COLOR_NO_ERR = "[90,90,90]";

// LIK 22.10.2009 List of all special messages for action execution
global dyn_string glActSpecialMessages;


//LhcVacPopupSymbolMenu:
/** Popup default menu for symbol representing device.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, EventRightClick only
  @param dialogMode: in, dialog mode (ONLINE, REPLAY, ..)
  @param dpName: in, DP name
  @param dpType: in/out, DP type, if empty on call - will be filled by this function.
  @param isXyMenu: in, true if popupMenuXY() shall be used, false if popupMenu() shall be used.
  @param menuX: in, X coordinate for popupMenuXY() if isXyMenu is true, otherwise does not matter.
  @param menuY: in, Y coordinate for popupMenuXY() if isXyMenu is true, otherwise does not matter.
		
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacPopupSymbolMenu(int dialogMode, const string dpName, string &dpType, bool isXyMenu, int menuX,
	int menuY, dyn_string &exceptionInfo, bool showSynopticItem = true)
{
  int typeIndex;
  dyn_string popupString = BuildMenuDescription(dialogMode, dpName, dpType, typeIndex, exceptionInfo, showSynopticItem);
  if((dynlen(exceptionInfo) > 0) || (dynlen(popupString) == 0))
  {
    return;
  }
 
   // Show prepared menu
  int answer;
  if(isXyMenu)
  {
    popupMenuXY(popupString, menuX, menuY, answer);
  }
  else
  {
    popupMenu(popupString, answer);
  }
  unsigned act = 0;
  switch(answer)
  {
  case 100:	// reserved answer to show "comment" dialog
    act = LVA_OPEN_COMMENT;
    break;
  case 101:  // reserved answer for 'show synoptic'
    VacOpenSynopticForEqp(dpName, dialogMode, exceptionInfo);
    return;
  case 102:  // reserved answer for 'copy to clipboard'
    {
      string devName;
      dyn_string exceptionInfo;
      LhcVacDisplayName(dpName, devName, exceptionInfo);
      setClipboardText(devName);
      return;
    }
  case MENU_ID_NOT_CONNECTED:  // Set device status to NotConnected
    act = LVA_DEVICE_SET_NOT_CONNECTED | LVA_DEVICE_MASK;
    break;
  case MENU_ID_CONNECTED:      // Set device status to Connected
    act = LVA_DEVICE_SET_CONNECTED | LVA_DEVICE_MASK;
    break;
  case LVA_DEVICE_WORKORDER:
    act = LVA_DEVICE_WORKORDER;
    break;
  case LVA_DEVICE_RACK:
	act = LVA_DEVICE_RACK;
	break;		
  case LVA_ALERT_ACKNOWLEDGE:
    act = LVA_ALERT_ACKNOWLEDGE;
    break;
    return;
  default:
    if((answer < 1) || (answer > dynlen(glDevMenuActs[typeIndex])))
    {
      // DebugTN("LhcVacPopupSymbolMenu: Wrong answer " + answer + " for DP type <" + dpType + ">");
      return;
    }
    if((act = glDevMenuActs[typeIndex][answer]) == 0)
    {
      return;
    }
    break;
  }
  if((act & LVA_DEVICE_MASK) != 0)
  {
    int mode = -1;
    if(act == (LVA_DEVICE_MASK | LVA_VPG_PUMP))
    {
      mode = 2;
    }
    else if(act == (LVA_DEVICE_MASK | LVA_VPG_VENT))
    {
      mode = 4;      
    }
    else if(act == (LVA_DEVICE_MASK | LVA_VPG_VENT_TURBO))
    {
      mode = 3;      
    }
    else if(act == (LVA_DEVICE_MASK | LVA_DEVICE_STOP_VPG))
    {
      mode = 1;      
    }
    if(mode > 0)
    {
      // set mode  
      LhcVacDeviceAction(makeDynAnytype(LVA_DEVICE_PROC_SET_MODE, mode), makeDynString(), makeDynString(), dpName, 0, exceptionInfo);
    }
    else
    {
      LhcVacDeviceAction(makeDynAnytype(act), makeDynString(), makeDynString(), dpName, 0, exceptionInfo);
    }
  }
  else
  {
    LvaDialogAction(makeDynAnytype(act, dialogMode), makeDynString(), dpName, 0, exceptionInfo);
  }
}

//LhcVacPopupDocumentationMenu:
/** Popup default menu for symbol representing device.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, EventRightClick only
  @param dialogMode: in, dialog mode (ONLINE, REPLAY, ..)
  @param dpName: in, eqp name
  @param isXyMenu: in, true if popupMenuXY() shall be used, false if popupMenu() shall be used.
  @param menuX: in, X coordinate for popupMenuXY() if isXyMenu is true, otherwise does not matter.
  @param menuY: in, Y coordinate for popupMenuXY() if isXyMenu is true, otherwise does not matter.
		
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacPopupDocumentationMenu(int dialogMode, const string eqpName, bool isXyMenu, int menuX,
	int menuY, dyn_string &exceptionInfo)
{	
	
	dyn_string popupString = BuildDocumentationMenuDescription(dialogMode, eqpName,exceptionInfo);
	if((dynlen(exceptionInfo) > 0) || (dynlen(popupString) == 0))
	{
		DebugTN("Error in builddocumentationmenudescription");
		return;
	}
	
   // Show prepared menu
	int answer;
	if(isXyMenu)
	{
		popupMenuXY(popupString, menuX, menuY, answer);
	}
	else
	{
		popupMenu(popupString, answer);
	}
	 string dpName;
	unsigned act = 0;
	switch(answer)
	{
	    case LVA_DEVICE_WORKORDER:
			act = LVA_DEVICE_WORKORDER;
		break;
		case LVA_OPEN_DETAIL_PANEL:
		  act = LVA_OPEN_DETAIL_PANEL;
		  LhcVacDpName(eqpName,dpName,exceptionInfo);
		  
		  //special case for TPG300, since the panel is for VR_GT
		  dyn_string dpTypeExceptions;
		  string dpType;
		  LhcVacDpType(dpName, dpType, dpTypeExceptions);
		  
		  if(dpType == "")
		  {
			  int family, type, subtype;
			  LhcVacGetCtlAttributes(dpName, family, type, subtype);
			  if(family == 210 && type == 1 && subtype == 0)
			  {
				  act = LVA_OPEN_CONFIG_PANEL;
				  dyn_string slaves;
				  LhcVacEqpSlaves(dpName, slaves);
				  for(int i = 1; i<= dynlen(slaves);i++)
				  {
					  LhcVacDpType(slaves[i], dpType, dpTypeExceptions);
					  if(dpType == "VGP_T" || dpType == "VGR_T")
					  {
						  LvaDialogAction(makeDynAnytype(act, dialogMode),makeDynString(), slaves[i],0,exceptionInfo);
						  return;
					  }
				  }
			  }
			  else{
				  dynAppend(exceptionInfo, dpTypeExceptions);
			  }
		  }	  
		  LvaDialogAction(makeDynAnytype(act, dialogMode),makeDynString(), dpName,0,exceptionInfo);
		  return;
    break;
		default:
			return;
	}
	LvaDocumentationDialogAction(makeDynAnytype(act, dialogMode), eqpName,exceptionInfo);
}

private dyn_string BuildMenuDescription(int dialogMode, const string dpName, string &dpType, int &typeIndex,
                                        dyn_string &exceptionInfo, bool showSynopticItem = true)
{
  dyn_string result = makeDynString();
  
  typeIndex = LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo);
  if(typeIndex <= 0)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacPopupSymbolMenu: unsupported DP type <" + dpType + ">", "");
    return result;
  }
  if(dynlen(glDevMenuItems[typeIndex]) == 0)
  {
    DebugTN("LhcVacPopupSymbolMenu: No default menu for DP type <" + dpType + ">");
    return result;
  }
  if(glDevMenuItems[typeIndex][1] == "")
  {
    DebugTN("LhcVacPopupSymbolMenu: No default menu for DP type <" + dpType + ">");
    return result;
  }
  // Build 1st item of menu - DP type specific
  string firstItem;
  LhcVacGetMenuNameString(dpName, dialogMode, firstItem, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return result;
  }

  // Build 2nd item of menu - comment
  string commentItem;
  LhcVacGetCommentString(dpName, commentItem, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return result;
  }

  // 3rd item of menu - open synoptic for device, sector is needed for decision
  int eqpVac;
  string eqpSector1, eqpSector2, eqpMainPart;
  bool eqpIsBorder;
  LhcVacDeviceVacLocation(dpName, eqpVac, eqpSector1, eqpSector2, eqpMainPart, eqpIsBorder, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return result;
  }

  // Build menu description
  result = makeDynString(firstItem, "SEPARATOR");
  if(commentItem != "")
  {
    dynAppend(result, commentItem);
    dynAppend(result, "SEPARATOR");
  }
  if(showSynopticItem && (eqpSector1 != ""))
  {
    dynAppend(result, "PUSH_BUTTON,Synoptic...,101,1");
    dynAppend(result, "SEPARATOR");
  }
  string eqpName;
  dyn_string exceptionInfoLhcVac;
  // Remove the copy to clipboard copy to clipboard is now in the first item!
  //LhcVacDisplayName(dpName, eqpName, exceptionInfoLhcVac);
  //dynAppend(result, "PUSH_BUTTON, '" + eqpName + "',102,1");
  //dynAppend(result, "SEPARATOR");

  int menuLen = dynlen(glDevMenuActs[typeIndex]);

  // Build the rest of menu, modify item sensitivity when necessary
  bool gotValueArchive = false, gotStateArchive = false;
  bool haveValueArchive, haveStateArchive;
  int eqpCtlStatus = LhcVacEqpCtlStatus(dpName, dialogMode);
  for(int n = 1 ; n <= menuLen ; n++) {
    bool isOrder = true;    
    bool itemAllowed;
    if((glDevMenuActs[typeIndex][n] & LVA_DEVICE_MASK) != 0) {
      // according to user requirements we have to hide(show) some menu item for any dpType     
      LhcVacIsShowMenuItem(dpName, dpType, glDevMenuActs[typeIndex][n], itemAllowed, exceptionInfo);
      if(dynlen(exceptionInfo) > 0) {
        return result;
      }
      if(!itemAllowed) {
        continue;
      }
      // None actions can be executed for devices with status NotControl
      switch(eqpCtlStatus) {
      case EQP_CTL_STATUS_NOT_CONTROL:
	    case EQP_CTL_STATUS_NOT_CONNECTED:
        itemAllowed = false;
        break;
      default:
        if(dialogMode == DIALOG_MODE_ONLINE) {
          LhcVacIsDeviceActsAllowed(false, dpName, dpType, glDevMenuActs[typeIndex][n],
            itemAllowed, exceptionInfo);
          if(dynlen(exceptionInfo) > 0) {
            return result;
          }
        }
        else {
          itemAllowed = false;
        }
        break;
      }
    }
    else if((glDevMenuActs[typeIndex][n] == LVA_OPEN_PR_HISTORY) || (glDevMenuActs[typeIndex][n] == LVA_OPEN_SETPOINT_HISTORY)) {
      isOrder = false;  	  
      if(!gotValueArchive)
        {
          LhcVacHasDeviceValueArchive(dpName, haveValueArchive, exceptionInfo);
  		if(dynlen(exceptionInfo) > 0)
          {
            return result;
          }
          gotValueArchive = true;
        }
        itemAllowed = haveValueArchive;
    }
    else if((glDevMenuActs[typeIndex][n] == LVA_OPEN_STATE_DETAILS_HISTORY) || (glDevMenuActs[typeIndex][n] == LVA_OPEN_BIT_HISTORY)) {
      isOrder = false;  	       
      if(!gotStateArchive) {
        LhcVacHasDeviceStateArchive(dpName, haveStateArchive, exceptionInfo);
        if(dynlen(exceptionInfo) > 0)
        {
          return result;
        }
        gotStateArchive = true;
      }
      itemAllowed = haveStateArchive;
    }
    else {
      itemAllowed = true;
    }
    //---    
    // Finally show, greyout or not show item    
    if( (!itemAllowed) && (isOrder) ) { // item is an order
      //old specs when not "allowed" grey out item:      
      //string item = substr(glDevMenuItems[typeIndex][n], 0, strlen(glDevMenuItems[typeIndex][n]) - 1);
      //item += "0";
      //dynAppend(result, item);
      //--
      //2019-09-10 new specs when not "allowed" not show item:
      ;      
    }
    else if(!itemAllowed) { // item is not an order
      string item = substr(glDevMenuItems[typeIndex][n], 0, strlen(glDevMenuItems[typeIndex][n]) - 1);
      item += "0";
      dynAppend(result, item);
    }
    else {
      dynAppend(result, glDevMenuItems[typeIndex][n]);
    }
  }
      
  //Work Order Creation
   
   //Check if workorder service is activated
   if (dpExists("WorkOrderService")) {
     bool workOrderServiceEnabled;  
     dpGet("WorkOrderService.ServiceEnabled",workOrderServiceEnabled);
     
     if(workOrderServiceEnabled) {
       
       // Find out where first cascade content starts
      int resultLen = dynlen(result);
      dyn_string allCascadeNames;
      int insertExtraBefore = -1;
      for(int idx = 1 ; idx <= resultLen ; idx++) {
        dyn_string menuItemParts = strsplit(result[idx], ",");
        if(dynlen(menuItemParts) > 1) {
          if(strtoupper(strltrim(strrtrim(menuItemParts[1]))) == "CASCADE_BUTTON") {
            string cascadeName = strltrim(strrtrim(menuItemParts[2]));
            dynAppend(allCascadeNames, cascadeName);
          }
        }
        else if(dynlen(menuItemParts) == 1) {
          if(dynContains(allCascadeNames, strltrim(strrtrim(menuItemParts[1]))) > 0) {
            insertExtraBefore = idx;
            break;
          }
        }
       }
    if(insertExtraBefore > 0) {
      dynInsertAt(result, "SEPARATOR", insertExtraBefore);
      dynInsertAt(result, "PUSH_BUTTON,Create Workorder," + LVA_DEVICE_WORKORDER + ",1", insertExtraBefore + 1);
    }
    else {
      dynAppend(result, "SEPARATOR");
      dynAppend(result, "PUSH_BUTTON,Create Workorder," + LVA_DEVICE_WORKORDER + ",1");
    }    
   }
  }
  
    //open rack
    int resultLen = dynlen(result);
    dyn_string allCascadeNames;
    int insertExtraBefore = -1;
	for(int idx = 1 ; idx <= resultLen ; idx++) {
	dyn_string menuItemParts = strsplit(result[idx], ",");
	if(dynlen(menuItemParts) > 1) {
	  if(strtoupper(strltrim(strrtrim(menuItemParts[1]))) == "CASCADE_BUTTON") {
		string cascadeName = strltrim(strrtrim(menuItemParts[2]));
		dynAppend(allCascadeNames, cascadeName);
	  }
	}
	else if(dynlen(menuItemParts) == 1) {
	  if(dynContains(allCascadeNames, strltrim(strrtrim(menuItemParts[1]))) > 0) {
		insertExtraBefore = idx;
		break;
	  }
	}
   }
    if(insertExtraBefore > 0) {
      dynInsertAt(result, "SEPARATOR", insertExtraBefore);
      dynInsertAt(result, "PUSH_BUTTON,Open Controller Rack," + LVA_DEVICE_RACK + ",1", insertExtraBefore + 1);
    }
    else {
      dynAppend(result, "SEPARATOR");
      dynAppend(result, "PUSH_BUTTON,Open Controller Rack," + LVA_DEVICE_RACK + ",1");
	  //DebugTN("hello" + LVA_DEVICE_RACK);
    }    
  // Extra items may be added if for setting device NotConnected/Connected
  string extraItem = LhcVacMenuItemForEqpCtlStatus(dpName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return result;
  }
  if(strlen(extraItem) > 0)
  {
    // Find out where first cascade content starts
    int resultLen = dynlen(result);
    dyn_string allCascadeNames;
    int insertExtraBefore = -1;
    for(int idx = 1 ; idx <= resultLen ; idx++)
    {
      dyn_string menuItemParts = strsplit(result[idx], ",");
      if(dynlen(menuItemParts) > 1)
      {
        if(strtoupper(strltrim(strrtrim(menuItemParts[1]))) == "CASCADE_BUTTON")
        {
          string cascadeName = strltrim(strrtrim(menuItemParts[2]));
          dynAppend(allCascadeNames, cascadeName);
        }
      }
      else if(dynlen(menuItemParts) == 1)
      {
        if(dynContains(allCascadeNames, strltrim(strrtrim(menuItemParts[1]))) > 0)
        {
          insertExtraBefore = idx;
          break;
        }
      }
    }
    if(insertExtraBefore > 0)
    {
      dynInsertAt(result, "SEPARATOR", insertExtraBefore);
      dynInsertAt(result, extraItem, insertExtraBefore + 1);
    }
    else
    {
      dynAppend(result, "SEPARATOR");
      dynAppend(result, extraItem);
    }
  }

  return result;
}

private dyn_string BuildDocumentationMenuDescription(int dialogMode, const string eqpName, dyn_string &exceptionInfo)
{
	dyn_string result = makeDynString();
	  //Work Order Creation
     // Build menu description
	string firstItem = "PUSH_BUTTON," + eqpName + ","+  LVA_OPEN_DETAIL_PANEL+ ",1";
	result = makeDynString(firstItem, "SEPARATOR");
   //Check if workorder service is activated
    if (dpExists("WorkOrderService"))
	{
		bool workOrderServiceEnabled;  
		dpGet("WorkOrderService.ServiceEnabled",workOrderServiceEnabled);
		if(workOrderServiceEnabled)
		{
			dynAppend(result, "PUSH_BUTTON,Create Workorder," + LVA_DEVICE_WORKORDER + ",1");
		}
	}
  
  
	return result;
}
//LhcVacHasDeviceStateArchive:
/** Check if device has state archive.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name
  @param dpType: in/out, DP type, if empty on call - will be filled by this function.
  @param hasArchive: out, result is returned here: has or not.
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacHasDeviceStateArchive(string dpName, bool &hasArchive,
    dyn_string &exceptionInfo)
{
  hasArchive = false;
  dyn_string stateDpes;
  LhcVacEqpStateDpes(dpName, stateDpes);
  for(int n = dynlen(stateDpes) ; n > 0 ; n--)
  {
    string  archiveClass;
    bool  configExists,isActive;
    int archiveType, smoothProcedure;
    float  deadband, timeInterval;
    fwArchive_get(stateDpes[n], configExists, archiveClass, archiveType, smoothProcedure,
                  deadband, timeInterval, isActive, exceptionInfo);
    if(configExists)
    {
      hasArchive = true;
      return;
    }
  }
}

//LhcVacHasDeviceValueArchive:
/** Check if device has analog value archive.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name
  @param hasArchive: out, result is returned here: has or not.
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacHasDeviceValueArchive(string dpName, bool &hasArchive,
                                    dyn_string &exceptionInfo)
{
  hasArchive = false;
  dyn_string valueDpes;
  LhcVacEqpValueDpes(dpName, valueDpes);
  for(int n = dynlen(valueDpes) ; n > 0 ; n--)
  {
    string  archiveClass;
    bool  configExists,isActive;
    int archiveType, smoothProcedure;
    float  deadband, timeInterval;
    fwArchive_get(valueDpes[n], configExists, archiveClass, archiveType, smoothProcedure,
                  deadband, timeInterval, isActive, exceptionInfo);
    if(configExists)
    {
      hasArchive = true;
      return;
    }
  }
}

//LhcVacIsDeviceActsAllowed:
/** Check if actions are allowed for device.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param groupAct: in, true for group actions, false for single device action.
	@param dpName: in, DP name
	@param dpType: in/out, DP type, if empty on call - will be filled by this function.
	@param act: in, action to be executed
	@param actAllowed: out, result is returned here: allowed or not.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacIsDeviceActsAllowed(bool groupAct, string dpName, string &dpType,
	dyn_anytype act, bool &actAllowed, dyn_string &exceptionInfo)
{
  if(groupAct)
  {
    if(dpType == "")
    {
      DebugN("ERROR: LhcVacIsDeviceActsAllowed: no DP type for group act");
      actAllowed = true;
      return;
    }
    actAllowed = true;
    return;
  }
  if(LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo) <= 0)
  {
    actAllowed = false;
    return;
  }

  // Check if device may accept action
  int dpTypeGroup = LhcVacDpTypeGroup(dpType);
  bool isReadOnly = false;
  switch(dpTypeGroup)
  {
  case VAC_DP_TYPE_GROUP_VGM:  // all gauges + master for VPGM
  case VAC_DP_TYPE_GROUP_VGR:
  case VAC_DP_TYPE_GROUP_VGP:
  case VAC_DP_TYPE_GROUP_VGF:
  case VAC_DP_TYPE_GROUP_VGI:
  case VAC_DP_TYPE_GROUP_VPGMPR:
  case VAC_DP_TYPE_GROUP_VG_STD:
  case VAC_DP_TYPE_GROUP_VG_PT:
    LhcVacActionPossible_VG(dpName, dpType, act, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VG_UC:
    isReadOnly = true;
    LhcVacActionPossible_VG_U(dpName, dpType, act, isReadOnly, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VG_UF:   
    isReadOnly = false;
    LhcVacActionPossible_VG_U(dpName, dpType, act, isReadOnly, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VGTR:
    LhcVacActionPossible_VGTR(dpName, dpType, act, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VP_TP:
  case VAC_DP_TYPE_GROUP_VPI:
  case VAC_DP_TYPE_GROUP_VRPI:
  case VAC_DP_TYPE_GROUP_VP_STDIO:
	//DebugTN("Before LhcVacActionPossible_VP, dpName=" + dpName + " dpType=" + dpType + " act=" + act + " actAllowed=" + actAllowed + " exceptionInfo=" + exceptionInfo);
    LhcVacActionPossible_VP(dpName, dpType, act, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VV:  // all valves
  case VAC_DP_TYPE_GROUP_VV_ANA:
  case VAC_DP_TYPE_GROUP_VV_AO:
  case VAC_DP_TYPE_GROUP_VV_STD_IO:
  case VAC_DP_TYPE_GROUP_VV_PUL:
  case VAC_DP_TYPE_GROUP_VV_VELO:
    LhcVacActionPossible_VV(dpName, dpType, act, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPG:   // Unified VPG VP_GU
    LhcVacActionPossible_VP_GU(dpName, dpType, act, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VR_PI:
  case VAC_DP_TYPE_GROUP_VA_RI:
  case VAC_DP_TYPE_GROUP_V_TCP:  
  case VAC_DP_TYPE_GROUP_VPGF:  // All fixed pumping groups
    actAllowed = true; //All actions possible  
    break;  
  case VAC_DP_TYPE_GROUP_VPGM:  // All mobile pumping groups
    LhcVacActionPossible_VPGM(dpName, dpType, act, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_ACCESS_MODE:
    actAllowed = true;
    break;
  case VAC_DP_TYPE_GROUP_VOPS_VELO:
  case VAC_DP_TYPE_GROUP_VPT_VELO:
  case VAC_DP_TYPE_GROUP_VPR_VELO:
  case VAC_DP_TYPE_GROUP_TC:
    actAllowed = true;
    break;
  case VAC_DP_TYPE_GROUP_ACCESS_ZONE:
    LhcVacActionPossible_ACCESS_ZONE(dpName, dpType, act, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_SECT_VPI_SUM:  // Summary of VPIs in one sector
    {
      int vac;
      string sector1, sector2, mainPart;
      bool isBorder;
      LhcVacDeviceVacLocation(dpName, vac, sector1, sector2, mainPart, isBorder, exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        return;
      }
      dyn_string vpiNames;
      LhcVacGetDevicesOfDpTypesAtSectors(makeDynString(sector1), makeDynString("VPI"), vpiNames, exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        return;
      }
      actAllowed = false;
      for(int n = dynlen(vpiNames) ; n > 0 ; n--)
      {
        string vrpiDpName;
        int masterChannel;
        LhcVacEqpMaster(vpiNames[n], vrpiDpName, masterChannel);
        if(vrpiDpName != "")
        {
          string vrpiDpType;
          bool vpiActAllowed;
          LhcVacActionPossible_VP(vrpiDpName, vrpiDpType, act, vpiActAllowed, exceptionInfo);
          if(dynlen(exceptionInfo) > 0)
          {
            return;
          }
          if(vpiActAllowed)
          {
            actAllowed = true;
            break;
          }
        }
      }
    }
    break;
  case VAC_DP_TYPE_GROUP_VPT100:
    LhcVacActionPossible_VPT100(dpName, dpType, act, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VRPM:
  case VAC_DP_TYPE_GROUP_VIES:
    LhcVacActionPossible_VRPM(dpName, dpType, act, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPG_6A01:
  case VAC_DP_TYPE_GROUP_BGI_6B01:
  case VAC_DP_TYPE_GROUP_INJ_6B02:
  case VAC_DP_TYPE_GROUP_VPG_6E01:
    LhcVacActionPossible_Process(dpName, dpType, act, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VP_NEG:
  case VAC_DP_TYPE_GROUP_VPN:  
    LhcVacActionPossible_VPNEG(dpName, dpType, act, actAllowed, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPC_HCCC:
    actAllowed = true; //All actions possible
   //LhcVacActionPossible_VPC_HCCC(dpName, dpType, act, actAllowed, exceptionInfo);
	 break;
  case VAC_DP_TYPE_GROUP_VG_DP:
    actAllowed = true; //All actions possible
    //LhcVacActionPossible_VG_DP(dpName, dpType, act, actAllowed, exceptionInfo);
	 break;
  case VAC_DP_TYPE_GROUP_VPS:
  case VAC_DP_TYPE_GROUP_VPS_PROCESS:
      actAllowed = true; //All actions possible
      break;
  case VAC_DP_TYPE_GROUP_VSECTOR:{ // [VACCO-929] [VACCO-948] [VACCO-1645]
      actAllowed = true; //All actions possible
      break;}  
  case VAC_DP_TYPE_GROUP_VRE: // Bakeout Rack
      actAllowed = true; //All actions possible
      break;  
  case VAC_DP_TYPE_GROUP_VELO_PROCESS: // VELO Process
      LhcVacActionPossible_VELO_Process(dpName, dpType, act, actAllowed, exceptionInfo);
      break;	  
  default:
    actAllowed = false;
  }
}

//LhcVacIsShowMenuItem:
/** Check if actions are allowed for device.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in/out, DP type, if empty on call - will be filled by this function.
	@param act: in, action to be executed
	@param showItem: out, result is returned here: allowed or not.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
LhcVacIsShowMenuItem(string dpName, string &dpType,
	dyn_anytype act, bool &showItem, dyn_string &exceptionInfo)
{
  if(LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo) <= 0)
  {
    //DebugTN("Show menu Item is FALSE -" + dpName + " - " + dpType + " - ", act);
    showItem = false;
    return;
  }
  // Check if menu item has to be shown
  int dpTypeGroup = LhcVacDpTypeGroup(dpType);
  unsigned action = (act[1] & LVA_ACTION_MASK);
  switch(dpTypeGroup)
  {
  case VAC_DP_TYPE_GROUP_VP_TP:
  case VAC_DP_TYPE_GROUP_VP_STDIO:  // all gauges + master for VPGM
    LhcVacIsShowMenuItem_VP(dpName, dpType, action, showItem);    
    break;  
  case VAC_DP_TYPE_GROUP_VV_STD_IO:
    LhcVacIsShowMenuItem_VV(dpName, dpType, action, showItem);    
    break;
  case VAC_DP_TYPE_GROUP_VV_PUL:
    LhcVacIsShowMenuItem_VV_PUL(dpName, dpType, action, showItem);    
    break;
  default:
    showItem = true;
  }
}

//LhcVacGetMenuNameString:
/** Build string to be shown as a 1st item in device menu

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	
  @param dpName: in, DP name
  @param dialogMode: in, dialog mode
  @param menuName: out, string for 1st item of menu is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here
*/
LhcVacGetMenuNameString(string dpName, int dialogMode, string &menuName, dyn_string &exceptionInfo)
{
  string mainStateString;
  LhcVacEqpMainStateStringForMenu(dpName, dialogMode, mainStateString);
   // replace "," symbols with "/" in short stete string
  strreplace (mainStateString, ",", " ");
 
  if(mainStateString != "")
  {
    menuName = "PUSH_BUTTON," + mainStateString + ",102,1"; // 102: action Copy eqp name to clipboard when clicked
  }
  else
  {
    string displayName = dpName;
    LhcVacDisplayName(dpName, displayName, exceptionInfo);
    menuName = "PUSH_BUTTON," + displayName + ",102,1"; // 102: action Copy eqp name to clipboard when clicked
  }
}

//LhcVacDeviceAction:
/** Execute specified action for single device or for a list of devices according to selection

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param act: detailts of action to be executed.
  @param sectList: in, list of selected sectors for multiple device action,
                    not used for single device action.
  @param dpTypeList: in, list of DP types to be affected by group action.
  @param dpName: in, DP name for single action, not used for multiple device action.
  @param vacType: in, bitmask indicating which vacuum type(s) are selected for
                    multiple device action, not used for single device action
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction(const dyn_anytype act, dyn_string sectList,
                        dyn_string dpTypeList, const string dpName,
                        const unsigned vacType, dyn_string &exceptionInfo,
                        string defaultActComment = "")
{
  if(dynlen(act) < 1)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction: empty action", "");
    return;
  }

  if((act[1] & LVA_GROUP_ACTION_MASK) != 0)	// Multiple device action
  {
    // It can be that group action is initiated from SINGLE device VSECT_VPI_SUM
    if(dynlen(sectList) == 0)
    {
      LhcVacGetGroupActionForSingleDevice(dpName, dpTypeList, sectList, exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        return;
      }
    }
    // Save current selection
    lhcVacCurSectList = sectList;
    lhcVacCurVacType = vacType;

    // Save action and DP types
    lhcVacCurAction = act;
    lhcVacCurActDpTypeList = dpTypeList;

    // Show panel for processing group action
    ChildPanelOnCentralModal("vision/dialogs/GroupAct.pnl", "Group Action", makeDynString());
    return;
  }
 
  // Single device action
  if(dpName == "")
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction: no DP name for action", "");
    return;
  }
  string dpType;
  if((dpType = dpTypeName(dpName)) == "")
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacDeviceAction: No DP type for <" + dpName + ">", "");
    return;
  }

  // Check for privileges...
  bool allowed;
  dynClear(glActSpecialMessages);
  LhcVacEnoughPrivilegeForAction(dpName, dpType, act, true, allowed, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  if(!allowed)
  {
    if(defaultActComment != "")  // Action from valve test script
    {
      fwException_raise(exceptionInfo, "ERROR", "LhcVacDeviceAction(): not enough privilege to execute action for " + dpName, "");
    }
    return;
  }

  // Ask for confirmation...
  string actName;
  LhcVacGetDevActName(act, dpName, dpType, actName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  string actDpName, actDpType;
  LhcVacGetDpForAct(dpName, dpType, actDpName, actDpType, exceptionInfo, act[1]);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  string devName = actDpName;
  LhcVacDisplayName(actDpName, devName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    dynClear(exceptionInfo);
    devName = actDpName;
  }
  string actLogName = actName + " " + devName;
  actName += "\n" + devName;
  bool isCommentMandatory = LhcVacNeedDevActComment(act[1], dpName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dyn_float dfReturn;
  dyn_string dsReturn;
  if(strlen(defaultActComment) == 0)
  {
    ChildPanelOnCentralModalReturn("vision/dialogs/DevActConfirm.pnl", "Confirmation",
      makeDynString("$1:" + actName, "$2:" + (isCommentMandatory ? "true" : "false")), dfReturn, dsReturn);
    if(dynlen(dsReturn)==0)
    {
      return;
    }
    if(dsReturn[1] != "1")
    {
      return;
    }
  }
  else
  {
    dsReturn = makeDynString("1", defaultActComment);
  }

  // Execute action
  LhcVacExecuteAction(dpName, dpType, act, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
   return;
  }

  // Write message to log
  string systemName = getSystemName();
  string userName = LhcVacGetUserName();
  // Add action parameters (if any)
  if(dynlen(act) > 1)
  {
    actLogName += " (";
    int nParams = dynlen(act);
    for(int parIdx = 2 ; parIdx <= nParams ; parIdx++)
    {
      actLogName += (string)act[parIdx];
      if(parIdx < nParams)
      {
        actLogName += ", ";
      }
    }
    actLogName += ")";
  }
  string message = actLogName + " by " + userName + "@" + getHostname() + " " + dsReturn[2];

  unMessageText_send(systemName, "1", "CmdHandler","user", "*", "INFO", message, exceptionInfo);
}

// 'Private' function: everything has been checked, all data are ready - execute...
void LhcVacExecuteAction(const string dpName, const string dpType, const dyn_anytype act,
	dyn_string &exceptionInfo)
{
  int errLen = dynlen(exceptionInfo);
  string  dpNameAct, dpTypeAct;

  LhcVacGetDpForAct(dpName, dpType, dpNameAct, dpTypeAct, exceptionInfo, act[1]);
  if(dynlen(exceptionInfo) > errLen)
  {
    return;
  }
  int typeIndex = LhcVacGetDpTypeIndex(dpNameAct, dpTypeAct, exceptionInfo);
  if(typeIndex <= 0)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacExecuteAction: unsupported DP type <" + dpTypeAct + ">", "");
    return;
  }

  unsigned finalAct = act[1] & LVA_ACTION_MASK;
  if(dynContains(glDevAllowedActs[typeIndex], finalAct) <= 0)
  {
    fwException_raise(exceptionInfo,
      "ERROR", "LhcVacExecuteAction: Action " + finalAct + " for DP <" + dpNameAct + "> of type <" +
      dpTypeAct + "> is not configured", "");
    return;
  }
  int dpTypeGroup = LhcVacDpTypeGroup(dpTypeAct);
  bool isReadOnly = false;
  switch(dpTypeGroup)
  {
  case VAC_DP_TYPE_GROUP_VGM:
  case VAC_DP_TYPE_GROUP_VGR:
  case VAC_DP_TYPE_GROUP_VGP:
  case VAC_DP_TYPE_GROUP_VGF:
  case VAC_DP_TYPE_GROUP_VGI:
  case VAC_DP_TYPE_GROUP_VPGMPR:  // all gauges + master for VPGM
  case VAC_DP_TYPE_GROUP_VG_STD:
  case VAC_DP_TYPE_GROUP_VGA_VELO:
  case VAC_DP_TYPE_GROUP_VGD_VELO:
  case VAC_DP_TYPE_GROUP_VG_PT:
    LhcVacDeviceAction_VG(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VG_UC:
    isReadOnly = true;
    LhcVacDeviceAction_VG_U(dpNameAct, dpTypeAct, act, isReadOnly, exceptionInfo);
    break; 
  case VAC_DP_TYPE_GROUP_VG_UF:
    isReadOnly = false;
    LhcVacDeviceAction_VG_U(dpNameAct, dpTypeAct, act, isReadOnly, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VGTR:
    LhcVacDeviceAction_VGTR(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VRPI:  // all masters for pumps && VP_STDIO
  case VAC_DP_TYPE_GROUP_VP_STDIO:
  case VAC_DP_TYPE_GROUP_VP_TP:
    LhcVacDeviceAction_VRPI(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VV:  // all valves
  case VAC_DP_TYPE_GROUP_VV_STD_IO:  // new valve for VPG_BGI
  case VAC_DP_TYPE_GROUP_VV_ANA:
  case VAC_DP_TYPE_GROUP_VV_VELO:
    LhcVacDeviceAction_VV(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VV_PUL:
	LhcVacDeviceAction_VV_PUL(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VV_AO:
    LhcVacDeviceAction_VV_AO(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPG:   // Generic VPG  
  case VAC_DP_TYPE_GROUP_VPGF:  // All fixed pumping groups
    LhcVacDeviceAction_VPG(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPGM:  // All mobile pumping groups
    LhcVacDeviceAction_VPGM(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_ACCESS_MODE:
    LhcVacDeviceAction_ACCESS_MODE(dpNameAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_ACCESS_ZONE:
    LhcVacDeviceAction_ACCESS_ZONE(dpNameAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPT100:
    LhcVacDeviceAction_VPT100(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPT_VELO:
  case VAC_DP_TYPE_GROUP_VPR_VELO:
    LhcVacDeviceAction_VPT_VELO(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VRPM:  // Solenoid power supply
    LhcVacDeviceAction_VRPM(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_BGI_6B01:  // BGI, driven by sequencer
    LhcVacDeviceAction_ProcessBGI_6B(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPG_6A01:  // VPG, driven by sequencer
    LhcVacDeviceAction_ProcessVPG_6A(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPG_6E01:  // VPG, driven by sequencer
    LhcVacDeviceAction_ProcessVPG_6E(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_INJ_6B02:  // INJ_6B02 process
    LhcVacDeviceAction_ProcessINJ_6B02(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_PLC:  // PLC actions
    LhcVacDeviceAction_PLC(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_V8DI_FE:	//DI Monitor actions
	   LhcVacDeviceAction_DIMON(dpNameAct, dpTypeAct, act, exceptionInfo);
	  break;
  case VAC_DP_TYPE_GROUP_VP_NEG:
  case VAC_DP_TYPE_GROUP_VPN: 
  case VAC_DP_TYPE_GROUP_VPNMUX: 
	   LhcVacDeviceAction_VPNEG(dpNameAct, dpTypeAct, act, exceptionInfo);
	  break;
  case VAC_DP_TYPE_GROUP_VG_DP:
    LhcVacDeviceAction_VG_DP(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPC_HCCC:
	  LhcVacDeviceAction_VPC_HCCC(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VPS:
     LhcVacDeviceAction_VPS(dpNameAct, dpTypeAct, act, exceptionInfo);
     break;    
  case VAC_DP_TYPE_GROUP_VPS_PROCESS:
     LhcVacDeviceAction_VPS_Process(dpNameAct, dpTypeAct, act, exceptionInfo);
     break;
  case VAC_DP_TYPE_GROUP_ITL_CMNT:
     LhcVacDeviceAction_VITL_CMNT(dpNameAct, dpTypeAct, act, exceptionInfo);
     break;
  case VAC_DP_TYPE_GROUP_VR_GT:
     LhcVacDeviceAction_VR_GT(dpNameAct, dpTypeAct, act, exceptionInfo);
     break;
  case VAC_DP_TYPE_GROUP_VOPS_VELO:
     LhcVacDeviceAction_VOPS_VELO(dpNameAct, dpTypeAct, act, exceptionInfo);
     break;
  case VAC_DP_TYPE_GROUP_TC:
     LhcVacDeviceAction_VPJ_TC(dpNameAct, dpTypeAct, act, exceptionInfo);
     break;
  case VAC_DP_TYPE_GROUP_TC:
     LhcVacDeviceAction_VPJ_TC(dpNameAct, dpTypeAct, act, exceptionInfo);
     break;
  case VAC_DP_TYPE_GROUP_VSECTOR: // [VACCO-929] [VACCO-948] [VACCO-1645]
     LhcVacDeviceAction_VSector(dpNameAct, dpTypeAct, act, exceptionInfo);
     break;
  case VAC_DP_TYPE_GROUP_VRE: // Bakeout rack
     LhcVacDeviceAction_VRE(dpNameAct, act, exceptionInfo);
     break;
  case VAC_DP_TYPE_GROUP_VR_PI: // Agilent controller
     LhcVacDeviceAction_VR_PI(act, dpNameAct, exceptionInfo);
     break;
  case VAC_DP_TYPE_GROUP_VA_RI: // Agilent controller relays
     LhcVacDeviceAction_VA_RI(act, dpNameAct, exceptionInfo);
     break;    
  case VAC_DP_TYPE_GROUP_VG_A_RO: // Generic Vacuum Gauge Analog - Read Only
    LhcVacDeviceAction_VG_A_RO(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_VOPS_2PS: //VOPS_2PS Double over pressure switch
    LhcVacDeviceAction_VOPS_2PS(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;
  case VAC_DP_TYPE_GROUP_V_TCP: // Communication device V_TCP
    LhcVacDeviceAction_V_TCP(dpNameAct, dpTypeAct, act, exceptionInfo);
    break; 
  case VAC_DP_TYPE_GROUP_VELO_PROCESS: // VELO Process
    LhcVacDeviceAction_VELO_Process(dpNameAct, dpTypeAct, act, exceptionInfo);
    break;	
  default:
    switch(dpTypeAct)
    {
    case "VGI_PS_CMW":
    case "VGP_PS_CMW":
    case "VGR_PS_CMW":
    case "VVS_PS_CMW":
    case "VPI_PS_CMW":
      fwException_raise(exceptionInfo,
        "ERROR", "This type of equipment is read-only in PVSS. Control using WorkingSet.", "");
      break;
    default:
      fwException_raise(exceptionInfo,
        "ERROR", "LhcVacExecuteAction: Actions are not implemented for DP type <" + dpTypeAct + ">", "");
      break;
    }
    break;
  }
}
//  'Private' function. Request for group action has arrived, but sector list is empty.
// It can be group action request from single device, the only known example of such
// device is VSECT_VPI_SUM
//
void LhcVacGetGroupActionForSingleDevice(const string dpName, dyn_string &dpTypeList,
                                          dyn_string &sectList, dyn_string &exceptionInfo)
{
  string dpType;
  if(LhcVacGetDpTypeIndex(dpName, dpType, exceptionInfo) <= 0)
  {
    return;
  }

  int dpTypeGroup = LhcVacDpTypeGroup(dpType);
  if(dpTypeGroup != VAC_DP_TYPE_GROUP_SECT_VPI_SUM)
  {
    return;
  }

  int vac;
  string sector1, sector2, mainPart;
  bool isBorder;
  LhcVacDeviceVacLocation(dpName, vac, sector1, sector2, mainPart, isBorder, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  dyn_string vpiNames;
  LhcVacGetDevicesOfDpTypesAtSectors(makeDynString(sector1), makeDynString("VPI"), vpiNames, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  for(int n = dynlen(vpiNames) ; n > 0 ; n--)
  {
    string vrpiDpName;
    int masterChannel;
    LhcVacEqpMaster(vpiNames[n], vrpiDpName, masterChannel);
    if(vrpiDpName != "")
    {
      string actDpType = dpTypeName(vrpiDpName);
      if(dynContains(dpTypeList, actDpType) <= 0)
      {
        dynAppend(dpTypeList, actDpType);
      }
    }
  }
  if(dynlen(dpTypeList) > 0)
  {
    dynAppend(sectList, sector1);
  }
}

//LhcVacGetDpForAct:
/** Return DP name and DP type of device for which action shall be executed
	in order to act on given device. Example: return VPCI for VPI.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name.
  @param dpType: in, DP type.
  @param actDpName: out, DP name to act is returned here.
  @param actDpType: out, DP type of device to act is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacGetDpForAct(const string dpName, const string dpType, string &actDpName,
                       string &actDpType, dyn_string &exceptionInfo, unsigned action = 0)
{
  int channel;
  switch(dpType)
  {
  case "VPN":
    // Set connected/not connected action available for VPN itself
    switch(action & LVA_ACTION_MASK)
    {
    case LVA_DEVICE_SET_CONNECTED:
    case LVA_DEVICE_SET_NOT_CONNECTED:
      actDpName = dpName;
      actDpType = dpType;
      return;
    }
    /* NOBREAK */
  case "VPI":
  case "VIES":
  case "VP_IP":
    LhcVacEqpMaster(dpSubStr(dpName, DPSUB_DP), actDpName, channel);
    actDpType = dpTypeName(actDpName);
    break;
  default:
    actDpName = dpName;
    actDpType = dpType;
    break;
  }
}

//LhcVacEnoughPrivilegeForAction:
/** Check if current PVSS user has enough privileges to execute action on device

Usage: LHC Vacuum control internal

PVSS manager usage: VISION

  @param dpName: in, DP name.
  @param dpType: in, DP type.
  @param act: in, enumeration for action to be executed
  @param showDialog: in, if true - dialog shall be shown explaining what is missing
  @param result: out, flag indicating if current user has enough privileges
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacEnoughPrivilegeForAction(string dpName, string dpType, dyn_anytype act, bool showDialog, 
                                    bool &result, dyn_string &exceptionInfo)
{
  result = false;
  dyn_string	privDpNames;
  switch(act[1] & LVA_ACTION_MASK)
  {
  case LVA_ACCESS_MODE_OFF:
    LhcVacCheckDevActPrivileges(act, dpName, showDialog, result, exceptionInfo);
    return;
    break;
  case LVA_ACCESS_MODE_ON:
    LhcVacCheckDevActPrivileges(act, dpName, showDialog, result, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return;
    }
    // If action == LVA_ACCESS_MODE_ON - we have to check privilege to close valve
    act[1] = LVA_DEVICE_MASK | LVA_DEVICE_CLOSE;
    // no break!!!
  default:

    LhcVacGetDpForPrivCheck(dpName, dpType, privDpNames, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return;
    }
    break;
  }
	
  for(int n = dynlen(privDpNames); n > 0 ; n--)
  {
    LhcVacCheckDevActPrivileges(act, privDpNames[n], showDialog, result, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return;
    }
    if(!result)
    {
      return;
    }
  }
}

//LhcVacGetDpForPrivCheck:
/** Return lists of DP names for which the privilegues
	will be checked. Example: return list of VPI for VPCI.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

  @param dpName: in, DP name.
  @param dpType: in, DP type.
  @param privDpNames: out, DP name to check privilegues  is returned here.
  @param exceptionInfo: out, details of any exceptions are returned here
*/

void LhcVacGetDpForPrivCheck(const string dpName,  const string dpType,
                             dyn_string &privDpNames, dyn_string &exceptionInfo)
{
  switch(dpType)
  {
  case "VRPI_W":
  case "VRPI_L":
  case "VRPI_VELO":
  case "VRGT":
    LhcVacEqpSlaves(dpName, privDpNames);
    break;
  case "_VacAccessMode":
    LhcVacGetDeviceForAccessMode(dpName, privDpNames, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("LhcVacGetDpForPrivCheck: LhcVacGetDeviceForAccessMode(" + dpName +
        ") failed: " + exceptionInfo);
      dynClear(exceptionInfo);
    }
    break;
  default:
    dynAppend(privDpNames, dpName);
    break;
  }
}


//LhcVacDevicePlc:
/** Find name of PLC DP for given device name

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	
  @param dpName: in, DP name.
  @param plcDp: out, name of PLC DP for PLC controlling this device
  @param alarmDp: out, name of alarm DP (Herve's)for PLC controlling this device
  @param exceptionInfo: out, details of any exceptions are returned here

  @return None
  @author L.Kopylov
*/
void LhcVacDevicePlc(string dpName, string &plcDp, string &alarmDp, dyn_string &exceptionInfo)
{
  string  pureDp = dpSubStr(dpName, DPSUB_DP);

  LhcVacGetAttribute(dpName, "PLC", plcDp);
  if(plcDp == "")
  {
    alarmDp = "";
    return;
  }
  alarmDp = c_unSystemAlarm_dpPattern + DPE_pattern + plcDp;
  if(! dpExists(alarmDp))
  {
    alarmDp = "";
  }
}

//LhcVacGetCommentString:
/** Build string to be shown as comment in popup menu

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
  @param dpName: in, DP name
  @param result: out, resulting comment stringis returned here.
  @param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacGetCommentString(string dpName, string &result, dyn_string &exceptionInfo)
{
  if(!dpExists(dpName + ".COMMENT"))	// Comment is not expected for such kind of DP
  {
    result = "";
    return;
  }
  string comment;
  dpGet(dpName + ".COMMENT", comment);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacGetCommentString: Read COMMENT for <" + dpName + "> failed: " + err, "");
    comment = "";
    return;
  }
  comment = (comment == "" ? "Comment" : comment); 
  result = "PUSH_BUTTON," + comment + ",100,1";
}

//LhcVacDecodeCommentString:
/** Decode device comment string: extract comment and author

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
  @param source: in, comment string read from DPE
  @param result: out, resulting comment string is returned here.
  @param exceptionInfo: out, resulting author is returned here
*/
void LhcVacDecodeCommentString(string source, string &comment, string &author)
{
  // last item in string is author's UserName 
  dyn_string 	split = strsplit(source, " ");
  comment = "";
  author = "";
  int nWords = dynlen(split);
  if(nWords == 0)
  {
    return;
  }
  comment = split[1];
  for(int n = 2 ; n < nWords ; n++)
  {
    comment += " " + split[n];
  }
  comment = strltrim(strrtrim(comment));
  author= split[nWords];
  author = strltrim(strrtrim(author, "'"), "'");
}

//LhcVacShowEqpAttributes:
/** Open panel displaying attributes of given device

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, name of device's DP for which attributes shall be shown
*/
void LhcVacShowEqpAttributes(string dpName)
{
  ChildPanelOnModal("/vision/eqpDetails/EqpAttributes.pnl", "Attributes",
    makeDynString("$dpName:" + dpName), 0, 0);
}


// StdErrorText
/**
Purpose:
Replacement for errorText() function that has been removed from PVSS

Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
string StdErrorText( dyn_errClass errList )
{
	int		errNbr, errCode;
	string	key, result;

	if ( dynlen( errList ) > 0 )
	{
		errCode = getErrorCode( errList );
		sprintf( key, "%05d", errCode );
		result = getCatStr( "_errors", key );
	}
	return result;
}

