// Process main functions to support OVER Pressure monitoring in VELO detector

const unsigned	 VOPS_VELO_R_B_CABLE = 0x00010000u,
  VOPS_VELO_R_B = 0x00020000u,
  VOPS_VELO_R_A_CABLE = 0x00040000u,
  VOPS_VELO_R_A = 0x00080000u,
  VOPS_VELO_R_WARNINGS = 0x00400000u,
  VOPS_VELO_R_ERRORS = 0x00800000u,
  VOPS_VELO_R_VALID = 0x40000000u;

	
void LhcVacDeviceAction_VOPS_VELO(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  mapping         mConnectDpes;
  unsigned        act;
  
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
   default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }
}
