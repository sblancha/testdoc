//	Implementation of BakeoutStateConnection class
/////////////////////////////////////////////////////////////////////////////////

#include "BakeoutStateConnection.h"

#include "MobilePool.h"

/////////////////////////////////////////////////////////////////////////////////
////////////////// Construction/destruction

BakeoutStateConnection::BakeoutStateConnection()
{
	pEqp = NULL;
	rr1 = 0;
	plcAlarm = false;
}

BakeoutStateConnection::~BakeoutStateConnection()
{
	if(pEqp)
	{
		MobilePool::getInstance().disconnectRr1(this, pEqp);
	}
}

/*
**	FUNCTION
**		Set reference to new equipment, connect to RR1 DPE of new device
**
**	ARGUMENTS
**		pNewEqp	- Pointer to new device to connect, can be NULL
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutStateConnection::setEqp(Eqp *pNewEqp)
{
	if(pEqp)
	{
		MobilePool::getInstance().disconnectRr1(this, pEqp);
	}
	pEqp = pNewEqp;
	if(pEqp)
	{
		MobilePool::getInstance().connectRr1(this, pEqp);
		QDateTime timeStamp;
		rr1 = MobilePool::getInstance().getDpeValue(pEqp, "RR1", timeStamp).toUInt();
		plcAlarm = MobilePool::getInstance().isPlcAlarm(pEqp);
		emit changed();
	}
}

/*
**	FUNCTION
**		Slot activated when RR1 DPE or PLC alarm of bakeout rack was changed
**
**	ARGUMENTS
**		pNewEqp	- Pointer to new device to connect, can be NULL
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void BakeoutStateConnection::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime & /* timeStamp */ )
{
	if(pEqp != pSrc)
	{
		return;
	}
	if(mode != DataEnum::Online)
	{
		return;
	}
	if(source == DataEnum::Plc)
	{
		bool alarm = MobilePool::getInstance().isPlcAlarm(pEqp);
		if(alarm == plcAlarm)
		{
			return;
		}
		plcAlarm = alarm;
		emit changed();
	}
	else if(!strcmp(dpeName, "RR1"))
	{
		unsigned newRr1 = value.toUInt();
		if(rr1 == newRr1)
		{
			return;
		}
		rr1 = newRr1;
		emit changed();
	}
}

