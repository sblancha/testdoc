#ifndef	EQPTYPESELECTORWIDGET_H
#define	EQPTYPESELECTORWIDGET_H

// Widget to select equipment types WITHIN selected sectors.
// It combines SectorSelectorWidget and QTable which
//	a) displays all equipment types in selected sectors + number of
//		equipment of every type
//	b) allows to select/unselect indiviudal types

#include "SectorSelectorWidget.h"

#include "VacEqpTypeMask.h"

#include <QWidget>

#include <QTableWidget>
#include <QTableWidgetItem>

class EqpTypeSelectorWidget : public QWidget
{
	Q_OBJECT

public:
	EqpTypeSelectorWidget(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~EqpTypeSelectorWidget();

	void initSectorContent(void);
	void setSelectedSectors(const QStringList &sectorList);
	void getSelectedSectors(QStringList &result) { pSectorSelector->getSelectedSectors(result); }
	bool isNonVacSelected(void) const { return pSectorSelector->isNonVacSelected(); }
	void setNonVacSelected(bool flag) { pSectorSelector->setNonVacSelected(flag); }

	QList<QVariant> getSelectedEqpTypes(void);
	void setSelectedEqpTypes(QList<QVariant> &types);

	bool isEmpty(void);
	void clear(void) { eqpMask.clear(); }
	void saveSelectedTypes(void) { storedEqpMask = eqpMask; }
	void restoreSelectedTypes(void);
	void selectDpType(const char *dpTypeName);
	void selectFunctionalType(int type);
	void selectPvssFunctionalType(int type);

	void getDpNames(QStringList &result);

signals:
	void selectionChanged(void);

protected:
	// Stored equipment selection mask
	static VacEqpTypeMask	storedEqpMask;

	// Sector selector widget
	SectorSelectorWidget	*pSectorSelector;

	// Table displaying equipment types in sector selection
	QTableWidget				*pTable;

	// Equipment selection mask being edited
	VacEqpTypeMask			eqpMask;

	void clearTableSelection(void);
	void displaySelection(void);
	bool selectTypeInTable(int typeId, bool set);

private slots:
	void sectorSelectionChange(void);

	void tableCellClicked(QTableWidgetItem *pItem);
};

#endif	// EQPTYPESELECTORWIDGET_H
