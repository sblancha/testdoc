#ifndef	VACICONVGR_H
#define	VACICONVGR_H

//	Icon for VGR

#include "VacIconVG.h"

class EqpVG;

class VACCTLEWOUTIL_EXPORT VacIconVGR : public VacIconVG
{
public:
	VacIconVGR(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGR_H
