// DP type for summary fo VPIs in vacuum sector
const string LHC_VAC_SECT_VPI_DP_TYPE = "VSECT_VPI_SUM";

//LhcVacSectorsAvailableForUser:
/** Build list of sectors available for current user

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param sectors: out, result will be return here.

	@author S.Merker
*/
void LhcVacSectorsAvailableForUser(dyn_string &sectors)
{
  dyn_string  domains, grantedDomains;
  bool        granted;

  dynClear(sectorList);
  string userName = LhcVacGetUserName();
  if(userName == "")
  {
    return;
  }
  LhcVacGetAllDomains(domains);
  if( dynlen(domains) == 0 )
  {
    return;        
  }
  for(int domainIdx = dynlen(domains) ; domainIdx > 0 ; domainIdx--)
  {
    dyn_string exceptionInfo;
    for(int privIdx = dynlen(glAllPrivileges) ; privIdx > 0 ; privIdx--)
    {
       fwAccessControl_checkUserPrivilege(userName, domains[domainIdx],
          glAllPrivileges[privIdx], granted, exceptionInfo);
        if(dynlen(exceptionInfo) > 0)
        {
          DebugTN("LhcVacSectorsAvailableForUser(" + domains[domainIdx] + "): fwAccessControl_checkUserPrivilege() failed",
            exceptionInfo);
          continue;
        }
        if( (dynContains(grantedDomains, domains[domainIdx]) == 0) && granted )
        {
           dynAppend(grantedDomains, domains[domainIdx]);
        }
      }
  }
  if( dynlen(grantedDomains) == 0 )
  {
    return;        
  }
  LhcVacGetSectorsOfDomains(grantedDomains, sectors);
  return;
}
//LhcVacSectSummaryDpName
/** Generate name of DP used as a summary of VPI states in sector

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param sectName: in, sector name.
	@param result: out, DP name is returned here
	@param exceptionInfo: out, details of any exceptions are returned here

	@return none
	@author L.Kopylov
*/
void LhcVacSectSummaryDpName( string sectName, string &result, dyn_string &exceptionInfo )
{
	string	sectPvssName;

	result = "";
	if( LhcVacPvssNameOfSector( sectName, sectPvssName ) < 0 )
	{
		fwException_raise( exceptionInfo, "ERROR",
			"LhcVacSectSummaryDpName(): Sector <" + sectName + "> unknown", "");
		return;
	}
	if( sectPvssName == "" )
	{
		fwException_raise( exceptionInfo, "ERROR",
			"LhcVacSectSummaryDpName(): No PVSS name for sector <" + sectName + ">", "");
		return;
	}
	result = LHC_VAC_SECT_VPI_DP_TYPE + "_" + sectPvssName;
}
