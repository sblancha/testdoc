#ifndef	VACICONVGM_STD_H
#define	VACICONVGM_STD_H

//	Icon for VG_STD - VGM

#include "VacIconVG_STD.h"

class VACCTLEWOUTIL_EXPORT VacIconVGM_STD : public VacIconVG_STD
{
public:
	VacIconVGM_STD(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG_STD(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGM_STD_H
