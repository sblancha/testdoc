//	Implementation of ExtendedLabel class
/////////////////////////////////////////////////////////////////////////////////

#include "ExtendedLabel.h"

#include <QMouseEvent>

void ExtendedLabel::setLabelType(int type)
{
	if(labelType == type)
	{
		return;
	}
	labelType = type;
	setMouseTracking(labelType == Transparent);
}

void ExtendedLabel::setBackColor(const QColor &color)
{
	if(color.isValid())
	{
		QPalette pal = palette();
		pal.setColor(QPalette::Window, color);
		setPalette(pal);
		setAutoFillBackground(true);
		setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	}
	else
	{
		setAutoFillBackground(false);
	}
}

void ExtendedLabel::mouseDoubleClickEvent(QMouseEvent *e)
{
	if(labelType == Transparent)
	{
		if(e->isAccepted())
		{
			e->ignore();
		}
	}
	else
	{
		QLabel::mouseDoubleClickEvent(e);
	}
}
void ExtendedLabel::mouseMoveEvent(QMouseEvent *e)
{
	if(labelType == Transparent)
	{
		if(e->isAccepted())
		{
			e->ignore();
		}
	}
	else
	{
		QLabel::mouseMoveEvent(e);
	}
}
void ExtendedLabel::mousePressEvent(QMouseEvent *e)
{
	if(labelType == Transparent)
	{
		if(e->isAccepted())
		{
			e->ignore();
		}
	}
	else if(labelType == Active)
	{
		emit mousePressed(e);
	}
	else
	{
		QLabel::mousePressEvent(e);
	}
}
void ExtendedLabel::mouseReleaseEvent(QMouseEvent *e)
{
	if(labelType == Transparent)
	{
		if(e->isAccepted())
		{
			e->ignore();
		}
	}
	else
	{
		QLabel::mouseReleaseEvent(e);
	}
}
