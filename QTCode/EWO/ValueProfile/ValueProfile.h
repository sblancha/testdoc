#ifndef VALUEPROFILE_H
#define	VALUEPROFILE_H

// EWO interface for ValueProfileWidget widget

#include <BaseExternWidget.hxx>

#include "ValueProfileWidget.h"

class EWO_EXPORT ValueProfile : public BaseExternWidget
{
	Q_OBJECT

public:
	ValueProfile(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void barHighlighted(int index, float value);
	void marqueeMoved(int start, int end);

private:
	ValueProfileWidget	*baseWidget;
};

#endif	// VALUEPROFILE_H
