#ifndef	VACICONVPT100_H
#define	VACICONVPT100_H

//	PT100 thermometer in LHC

#include "VacIcon.h"

class EqpVPT100;

class VACCTLEWOUTIL_EXPORT VacIconVPT100 : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPT100(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPT100();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpVPT100 *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, int pr, bool plcAlarm);

	void setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, int pr, bool plcAlarm);
};


#endif	// VACICONVPT100_H
