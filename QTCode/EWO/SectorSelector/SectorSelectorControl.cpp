//	Implementation of SectorSelectorControl class
/////////////////////////////////////////////////////////////////////////////////

#include "SectorSelectorControl.h"
#include "ColResizeTable.h"

// TODO for 4.5 #include "DateEditTableItem.h"

#include "DataPool.h"
#include "Eqp.h"

#include <qlayout.h>
#include <qgroupbox.h>
#include <qlistwidget.h>
#include <qtablewidget.h>
#include <qpushbutton.h>
#include <qmenu.h>
#include <qcursor.h>
#include <QHeaderView>

SectorSelectorControl::SectorSelectorControl(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	setting = showDate = false;

	// Build layout. Layout is:
	// - three GroupBoxes, each containing one ListBox:
	//	 left one for main parts,
	//	 middle one for all sectors in selected main parts
	//	 right one for selected sectors
	// - group of 4 buttons between middle and right lists
	QHBoxLayout *mainBox = new QHBoxLayout(this);
	mainBox->setContentsMargins(0, 0, 0, 0);
	mainBox->setSpacing(0);

	QGroupBox *mpBox = new QGroupBox("Main Parts");
	QVBoxLayout *mpBoxLayout = new QVBoxLayout(mpBox);

	pMainPartList = new QListWidget(mpBox);
	mpBoxLayout->addWidget(pMainPartList, 10);
	pMainPartList->setSelectionMode(QAbstractItemView::ExtendedSelection);
	connect(pMainPartList, SIGNAL(itemSelectionChanged(void)),
		this, SLOT(mpSelectionChanged(void)));
	pMainPartList->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(pMainPartList, SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(mpListContextMenuRequest(const QPoint &)));
	mainBox->addWidget(mpBox);
	mainBox->setStretchFactor(mpBox, 10);

	QGroupBox *allSectorBox = new QGroupBox("Sectors available", this);
//	allSectorBox->setInsideSpacing(0);
//	allSectorBox->setInsideMargin(4);
	QVBoxLayout *allSectorBoxLayout = new QVBoxLayout(allSectorBox);
	pAllSectorList = new QListWidget(allSectorBox);
	allSectorBoxLayout->addWidget(pAllSectorList, 10);
	pAllSectorList->setSelectionMode(QAbstractItemView::ExtendedSelection);
	connect(pAllSectorList, SIGNAL(itemSelectionChanged(void)),
		this, SLOT(allSectorSelectionChanged(void)));
	pAllSectorList->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(pAllSectorList, SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(allSectorsListContextMenuRequest(const QPoint &)));
	mainBox->addWidget(allSectorBox);
	mainBox->setStretchFactor(allSectorBox, 10);

	QVBoxLayout *buttonBox = new QVBoxLayout();
	mainBox->addLayout(buttonBox);
	buttonBox->addStretch();
	QPushButton *pRightAllPb = new QPushButton(">>", this);
	QSize size = pRightAllPb->fontMetrics().size(Qt::TextSingleLine, ">>");
	size.setWidth(size.width() + 8);
	size.setHeight(size.height() + 4);
	pRightAllPb->setFixedSize(size);
	connect(pRightAllPb, SIGNAL(clicked(void)), this, SLOT(rightAllPbClicked(void)));
	buttonBox->addWidget(pRightAllPb);

	pRightPb = new QPushButton(">", this);
	pRightPb->setFixedSize(size);
	pRightPb->setEnabled(false);
	connect(pRightPb, SIGNAL(clicked(void)), this, SLOT(rightPbClicked(void)));
	buttonBox->addWidget(pRightPb);

	pLeftPb = new QPushButton("<", this);
	pLeftPb->setFixedSize(size);
	pLeftPb->setEnabled(false);
	connect(pLeftPb, SIGNAL(clicked(void)), this, SLOT(leftPbClicked(void)));
	buttonBox->addWidget(pLeftPb);

	QPushButton *pLeftAllPb = new QPushButton("<<", this);
	pLeftAllPb->setFixedSize(size);
	connect(pLeftAllPb, SIGNAL(clicked(void)), this, SLOT(leftAllPbClicked(void)));
	buttonBox->addWidget(pLeftAllPb);
	buttonBox->addStretch();

	mainBox->setStretchFactor(buttonBox, 0);

	QGroupBox *sectorBox = new QGroupBox("Sectors selected", this);
//	sectorBox->setInsideSpacing(0);
//	sectorBox->setInsideMargin(4);
	QVBoxLayout *sectorBoxLayout = new QVBoxLayout(sectorBox);
	pSectorTable = new ColResizeTable(sectorBox);
	sectorBoxLayout->addWidget(pSectorTable, 10);
	pSectorTable->setColumnCount(1);
	pSectorTable->verticalHeader()->hide();
	pSectorTable->horizontalHeader()->hide();
//	pSectorTable->setLeftMargin(0);
//	pSectorTable->setTopMargin(0);
//	pSectorTable->setColumnReadOnly(0, true);
	pSectorTable->setSelectionMode(QAbstractItemView::MultiSelection);
//	pSectorTable->setFocusStyle(QTable::FollowStyle);
	connect(pSectorTable, SIGNAL(itemSelectionChanged(void)),
		this, SLOT(sectorSelectionChanged(void)));
	pSectorTable->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(pSectorTable, SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(sectorsListContextMenuRequest(const QPoint &)));
	mainBox->addWidget(sectorBox);
	mainBox->setStretchFactor(sectorBox, 20);

	// Add all main part names to main part list
	initContent();
}

SectorSelectorControl::~SectorSelectorControl()
{
}

void SectorSelectorControl::setShowDate(bool flag)
{
	if(flag == showDate)
	{
		return;
	}
	showDate = flag;
	int tableWidth = pSectorTable->width();
	tableWidth = pSectorTable->rect().width();
	if(showDate)
	{
		pSectorTable->setColumnCount(2);
//		pSectorTable->setColumnReadOnly(1, false);
		pSectorTable->setColumnWidth(0, tableWidth / 2);
		pSectorTable->setColumnWidth(1, tableWidth / 2);
		/* TODO
		for(int row = 0 ; row < pSectorTable->rowCount() ; row++)
		{
			pSectorTable->setItem(row, 1, new DateEditTableItem(pSectorTable, QTableItem::WhenCurrent));
		}
		*/
	}
	else
	{
		if(pSectorTable->columnCount() > 1)
		{
			pSectorTable->removeColumn(1);
		}
		pSectorTable->setColumnWidth(0, tableWidth);
	}
}

/*
**	FUNCTION
**		Initialize editor content to empty: add and unselect all main parts
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Number of main parts in list
**
**	CAUTIONS
**		None
*/
int SectorSelectorControl::initContent(void)
{
	pAllSectorList->clear();
	pSectorTable->setRowCount(0);
	pMainPartList->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	QStringList mpNames;
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		// Ignore DSL sector(s)
		if(pSector->getVacType() == VacType::DSL)
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			bool alreadyInList = false;
			foreach(QString mpName, mpNames)
			{
				if(mpName == pMap->getMainPart()->getName())
				{
					alreadyInList = true;
					break;
				}
			}
			if(!alreadyInList)
			{
				mpNames.append(pMap->getMainPart()->getName());
			}
		}
	}
	if(mpNames.count())
	{
		pMainPartList->insertItems(0, mpNames);
	}
	return mpNames.count();
}

/*
**	FUNCTION
**		Set initial selection in both main part and sector lists
**
**	ARGUMENTS
**		sectorList	- List of sector names that must become selected
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::setSelectedSectors(const QStringList &sectorList)
{
	DataPool &pool = DataPool::getInstance();
	
	// Find all main parts which must be selected in order to show all required
	// sectors in sector list
	setting = true;
	QStringList listCopy(sectorList);
	pMainPartList->clearSelection();
	pSectorTable->setRowCount(0);
	foreach(QString sectName, listCopy)
	{
		Sector *pSector = pool.findSectorData(sectName.toLatin1());
		if(!pSector)
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			selectInList(pMainPartList, pMap->getMainPart()->getName());
		}
		pSectorTable->setRowCount(pSectorTable->rowCount() + 1);
		pSectorTable->setItem(pSectorTable->rowCount() - 1, 0, new QTableWidgetItem(sectName));
		/* TODO
		if(pSectorTable->numCols() > 1)
		{
			pSectorTable->setItem(pSectorTable->rowCount() - 1, 1, new DateEditTableItem(pSectorTable, QTableItem::WhenCurrent));
		}
		*/
	}
	setting = false;

	mpSelectionChanged();
	emit selectionChanged();
}

/*
**	FUNCTION
**		Return names of selected sectors
**
**	ARGUMENTS
**		result	- Variable where names of selected sectors will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::getSelectedSectors(QStringList &result) const
{
	result.clear();
	for(int n = 0 ; n < pSectorTable->rowCount() ; n++)
	{
		result.append(pSectorTable->item(n, 0)->text());
	}
}

/*
**	FUNCTION
**		Check if at least one sector is selected
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- No sector(s) selected;
**		false	- at least one sector is selected
**
**	CAUTIONS
**		None
*/
bool SectorSelectorControl::isEmpty(void)
{
	return pSectorTable->rowCount() > 0;
}

/*
**	FUNCTION
**		Select item with given name in given list
**
**	ARGUMENTS
**		pList	- Pointer to QListBox where item shall be selected
**		pName	- Pointer to name to be selected in list
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::selectInList(QListWidget *pList, const QString &name)
{
	for(int n = pList->count() - 1 ; n >= 0 ; n--)
	{
		if(pList->item(n)->text() == name)
		{
			pList->item(n)->setSelected(true);
			break;
		}
	}
}

/*
**	FUNCTION
**		Check if item with given name is selected in given list
**
**	ARGUMENTS
**		pList	- Pointer to QListBox where item shall be checked
**		pName	- Pointer to name to be checked
**
**	RETURNS
**		true	- Item is selected in list;
**		false	- otherwise
**
**	CAUTIONS
**		It is expected that list conatins at most one item with given name
*/
bool SectorSelectorControl::isListItemSelected(QListWidget *pList, const QString &name)
{
	for(int n = pList->count() - 1 ; n >= 0 ; n--)
	{
		if(pList->item(n)->text() == name)
		{
			return pList->item(n)->isSelected();
		}
	}
	return false;
}

/*
**	FUNCTION
**		Slot activated when selection in main part list has been changed.
**		Display all sectors of selected main parts in sector list.
**		Keep current sector selection whenever possible (i.e. if some sector
**		was selected in sector list - it must remain selected in 'new' sector
**		list if it will be there)
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::mpSelectionChanged(void)
{
	if(setting)
	{
		return;
	}
	QStringList selectedSectors;
	getSelectedSectors(selectedSectors);
	pAllSectorList->clear();

	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	int selectedIndex = 0;
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			if(isListItemSelected(pMainPartList, pMap->getMainPart()->getName()))
			{
				bool isInSelected = false;
				if(selectedIndex < pSectorTable->rowCount())
				{
					if(pSectorTable->item(selectedIndex, 0)->text() == pSector->getName())
					{
						selectedIndex++;
						isInSelected = true;
					}
				}
				if(!isInSelected)
				{
					pAllSectorList->addItem(pSector->getName());
				}
				break;
			}
		}
	}
}

/*
**	FUNCTION
**		Slot activated when selection in all sector list has been changed.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::allSectorSelectionChanged(void)
{
	bool somethingSelected = false;
	for(int n = pAllSectorList->count() - 1 ; n >= 0 ; n--)
	{
		if(pAllSectorList->item(n)->isSelected())
		{
			somethingSelected = true;
			break;
		}
	}
	pRightPb->setEnabled(somethingSelected);
}

/*
**	FUNCTION
**		Slot activated when selection in sector list has been changed.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::sectorSelectionChanged(void)
{
	bool somethingSelected = false;
	for(int n = pSectorTable->rowCount() - 1 ; n >= 0 ; n--)
	{
		if(pSectorTable->item(n, 0)->isSelected())
		{
			somethingSelected = true;
			break;
		}
	}
	pLeftPb->setEnabled(somethingSelected);
}

/*
**	FUNCTION
**		Slot activated when right mouse button is pressed in main part list.
**		Show popup menu for fast selection change.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::mpListContextMenuRequest(const QPoint & /* point */) {
	QMenu *pMenu = new QMenu();
	pMenu->addAction("Select All", this, SLOT(mpSelectAll()));
	pMenu->addAction("Unselect All", this, SLOT(mpUnselectAll()));
	pMenu->addAction("Invert Selection", this, SLOT(mpInvertSelection()));
	pMenu->exec(QCursor::pos());
}

void SectorSelectorControl::mpSelectAll(void)
{
	pMainPartList->selectAll();
}

void SectorSelectorControl::mpUnselectAll(void)
{
	pMainPartList->clearSelection();
}

void SectorSelectorControl::mpInvertSelection(void)
{
	for(int n = pMainPartList->count() - 1 ; n >= 0 ; n--)
	{
		pMainPartList->item(n)->setSelected(!pMainPartList->item(n)->isSelected());
	}
}

/*
**	FUNCTION
**		Slot activated when right mouse button is pressed in left sectors list.
**		Show popup menu for fast selection change.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::allSectorsListContextMenuRequest(const QPoint & /* point */) {
	QMenu *pMenu = new QMenu();
	pMenu->addAction("Select All", this, SLOT(allSectorSelectAll()));
	pMenu->addAction("Unselect All", this, SLOT(allSectorUnselectAll()));
	pMenu->addAction("Invert Selection", this, SLOT(allSectorInvertSelection()));
	pMenu->exec(QCursor::pos());
}

void SectorSelectorControl::allSectorSelectAll(void)
{
	pAllSectorList->selectAll();
}

void SectorSelectorControl::allSectorUnselectAll(void)
{
	pAllSectorList->clearSelection();
}

void SectorSelectorControl::allSectorInvertSelection(void)
{
	for(int n = pMainPartList->count() - 1 ; n >= 0 ; n--)
	{
		pMainPartList->item(n)->setSelected(!pMainPartList->item(n)->isSelected());
	}
}


/*
**	FUNCTION
**		Slot activated when right mouse button is pressed in right sectors list.
**		Show popup menu for fast selection change.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::sectorsListContextMenuRequest(const QPoint & /* point */) {
	QMenu *pMenu = new QMenu();
	pMenu->addAction("Select All", this, SLOT(sectorSelectAll()));
	pMenu->addAction("Unselect All", this, SLOT(sectorUnselectAll()));
	pMenu->addAction("Invert Selection", this, SLOT(sectorInvertSelection()));
	pMenu->exec(QCursor::pos());
}

void SectorSelectorControl::sectorSelectAll(void)
{
	pSectorTable->clearSelection();
	pSectorTable->setRangeSelected(QTableWidgetSelectionRange(0, 0, pSectorTable->rowCount() - 1, pSectorTable->columnCount() - 1), true);
	/*
	for(int row = pSectorTable->rowCount() - 1 ; row >= 0 ; row--)
	{
		pSectorTable->selectRow(row);
	}
	*/
}

void SectorSelectorControl::sectorUnselectAll(void)
{
	pSectorTable->clearSelection();
	/*
	while(pSectorTable->numSelections())
	{
		pSectorTable->removeSelection(0);
	}
	*/
}

void SectorSelectorControl::sectorInvertSelection(void)
{
	// Collect all unselected rows
	QList<int> unselectedRows;
	int row;
	for(row = pSectorTable->rowCount() - 1 ; row >= 0 ; row--)
	{
		if(!pSectorTable->item(row, 0)->isSelected())
		{
			unselectedRows.append(row);
		}
	}

	// clear current selection
	pSectorTable->clearSelection();
	/*
	while(pSectorTable->numSelections())
	{
		pSectorTable->removeSelection(0);
	}
	*/

	// Set new selection
	foreach(row, unselectedRows)
	{
		QTableWidgetSelectionRange range(row, 0, row, pSectorTable->columnCount() - 1);
		pSectorTable->setRangeSelected(range, true);
	}
}

void SectorSelectorControl::addToSelection(int row, int &firstSelectedRow, int &lastSelectedRow,
		QList<QTableWidgetSelectionRange *> &newSelections)
{
	if(firstSelectedRow < 0)
	{
		firstSelectedRow = lastSelectedRow = row;
	}
	else if(lastSelectedRow == (row - 1))
	{
		lastSelectedRow = row;
	}
	else
	{
		newSelections.append(new QTableWidgetSelectionRange(firstSelectedRow, 0,
			lastSelectedRow, showDate ? 1 : 0));
		firstSelectedRow = lastSelectedRow = row;
	}
}

void SectorSelectorControl::setNewSelection(int firstSelectedRow, int lastSelectedRow,
		QList<QTableWidgetSelectionRange *> &newSelections)
{
/*
printf("SectorSelectorControl::setNewSelection(): first %d last %d list %d\n",
firstSelectedRow, lastSelectedRow, newSelections.count());
fflush(stdout);
*/
	while(!newSelections.isEmpty())
	{
		QTableWidgetSelectionRange *pSelection = newSelections.takeFirst();
		pSectorTable->setRangeSelected(*pSelection, true);
		delete pSelection;
	}
	if(firstSelectedRow >= 0)
	{
		pSectorTable->setRangeSelected(QTableWidgetSelectionRange(firstSelectedRow, 0,
			lastSelectedRow, showDate ? 1 : 0), true);
	}
}

/*
**	FUNCTION
**		Slot activated when ">>" button was cliecked. Move everything from all sectors
**		list to sector list.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::rightAllPbClicked(void)
{
	if(!pAllSectorList->count())
	{
		return;
	}

	QStringList leftSectors;
	int n;
	for(n = 0 ; n < pAllSectorList->count() ; n++)
	{
		leftSectors.append(pAllSectorList->item(n)->text());
	}

	QStringList rightSectors;
	for(n = 0 ; n < pSectorTable->rowCount() ; n++)
	{
		rightSectors.append(pSectorTable->item(n, 0)->text());
	}

	pSectorTable->setRowCount(0);
	pSectorTable->clearSelection();
	pAllSectorList->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	int firstIndex = -1;
	int firstSelectedRow = -1, lastSelectedRow = -1;
	QList<QTableWidgetSelectionRange *> newSelections;
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		if(leftSectors.indexOf(pSector->getName()) >= 0)
		{
			int row = pSectorTable->rowCount();
			pSectorTable->setRowCount(row + 1);
			pSectorTable->setItem(row, 0, new QTableWidgetItem(pSector->getName()));
			addToSelection(row, firstSelectedRow, lastSelectedRow, newSelections);
			if(firstIndex < 0)
			{
				firstIndex = row;
			}
		}
		else if(rightSectors.indexOf(pSector->getName()) >= 0)
		{
			int row = pSectorTable->rowCount();
			pSectorTable->setRowCount(row + 1);
			pSectorTable->setItem(row, 0, new QTableWidgetItem(pSector->getName()));
		}
	}
	if(firstIndex >= 0)
	{
//		pSectorTable->setCurrentCell(firstIndex, 0);
		pSectorTable->scrollToItem(pSectorTable->item(firstIndex, 0));
	}
	setNewSelection(firstSelectedRow, lastSelectedRow, newSelections);
	pRightPb->setEnabled(false);
	pLeftPb->setEnabled(true);
	emit selectionChanged();
}

/*
**	FUNCTION
**		Slot activated when ">" button was cliecked. Move selected lines from all sectors
**		list to sector list.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::rightPbClicked(void)
{
	QStringList leftSectors;
	int n;
	for(n = 0 ; n < pAllSectorList->count() ; n++)
	{
		if(pAllSectorList->item(n)->isSelected())
		{
			leftSectors.append(pAllSectorList->item(n)->text());
		}
	}
	if(leftSectors.isEmpty())
	{
		pRightPb->setEnabled(false);
		return;
	}

	QStringList rightSectors;
	for(n = 0 ; n < pSectorTable->rowCount() ; n++)
	{
		rightSectors.append(pSectorTable->item(n, 0)->text());
	}

	pSectorTable->setRowCount(0);
	pSectorTable->clearSelection();
	pAllSectorList->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	int firstIndex = -1;
	int firstSelectedRow = -1, lastSelectedRow = -1;
	QList<QTableWidgetSelectionRange *> newSelections;
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		if(leftSectors.indexOf(pSector->getName()) >= 0)
		{
			int row = pSectorTable->rowCount();
			pSectorTable->setRowCount(row + 1);
			pSectorTable->setItem(row, 0, new QTableWidgetItem(pSector->getName()));
			addToSelection(row, firstSelectedRow, lastSelectedRow, newSelections);
			if(firstIndex < 0)
			{
				firstIndex = row;
			}
		}
		else if(rightSectors.indexOf(pSector->getName()) >= 0)
		{
			int row = pSectorTable->rowCount();
			pSectorTable->setRowCount(row + 1);
			pSectorTable->setItem(row, 0, new QTableWidgetItem(pSector->getName()));
		}
		else
		{
			const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
			for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
			{
				MainPartMap *pMap = maps.at(mapIdx);
				if(isListItemSelected(pMainPartList, pMap->getMainPart()->getName()))
				{
					pAllSectorList->addItem(pSector->getName());
					break;
				}
			}
		}
	}
	setNewSelection(firstSelectedRow, lastSelectedRow, newSelections);
	if(firstIndex >= 0)
	{
//		pSectorTable->setCurrentCell(firstIndex, 0);
		pSectorTable->scrollToItem(pSectorTable->item(firstIndex, 0));
	}
	pRightPb->setEnabled(false);
	pLeftPb->setEnabled(true);
	emit selectionChanged();
}

/*
**	FUNCTION
**		Slot activated when "<" button was cliecked. Move selected lines from sectors
**		list to all sector list.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::leftPbClicked(void)
{
	QList<int> selectedRows;
	QStringList rightSectors;
	int n;
	for(n = pSectorTable->rowCount() - 1 ; n >= 0 ; n--)
	{
		if(pSectorTable->item(n, 0)->isSelected())
		{
			selectedRows.append(n);
			rightSectors.append(pSectorTable->item(n, 0)->text());
//			pSectorTable->removeRow(n);
		}
	}
	if(rightSectors.isEmpty())
	{
		pLeftPb->setEnabled(false);
		return;
	}
	pSectorTable->clearSelection();
	foreach(n, selectedRows)
	{
		pSectorTable->removeRow(n);
	}

	QStringList selectedSectors;
	getSelectedSectors(selectedSectors);

	pAllSectorList->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	int firstIndex = -1;
	setting = true;
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		if(selectedSectors.indexOf(pSector->getName()) >= 0)
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			if(isListItemSelected(pMainPartList, pMap->getMainPart()->getName()))
			{
				pAllSectorList->addItem(pSector->getName());
				if(rightSectors.indexOf(pSector->getName()) >= 0)
				{
					if(firstIndex < 0)
					{
						firstIndex = pAllSectorList->count() - 1;
					}
					pAllSectorList->item(pAllSectorList->count() - 1)->setSelected(true);
				}
				break;
			}
		}
	}
	if(firstIndex >= 0)
	{
		pAllSectorList->setCurrentItem(pAllSectorList->item(firstIndex));
// TODO		pAllSectorList->ensureCurrentVisible();
	}
	if(pSectorTable->rowCount())
	{
		pSectorTable->clearSelection();
	}
	setting = false;
	pRightPb->setEnabled(true);
	pLeftPb->setEnabled(false);
	emit selectionChanged();
}

/*
**	FUNCTION
**		Slot activated when "<<" button was cliecked. Remove all from sector list,
**		and show (if possible) in all sector list
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectorSelectorControl::leftAllPbClicked(void)
{
	QStringList rightSectors;
	getSelectedSectors(rightSectors);
	if(rightSectors.isEmpty())
	{
		pLeftPb->setEnabled(false);
		return;
	}
	pSectorTable->setRowCount(0);
	pAllSectorList->clear();
	DataPool &pool = DataPool::getInstance();
	const QList<Sector *> &sectors = pool.getSectors();
	int firstIndex = -1;
	setting = true;
	for(int sectIdx = 0 ; sectIdx < sectors.count() ; sectIdx++)
	{
		Sector *pSector = sectors.at(sectIdx);
		// Ignore outer sectors
		if(pSector->isOuter())
		{
			continue;
		}
		const QList<MainPartMap *> &maps = pSector->getMainPartMaps();
		for(int mapIdx = 0 ; mapIdx < maps.count() ; mapIdx++)
		{
			MainPartMap *pMap = maps.at(mapIdx);
			if(isListItemSelected(pMainPartList, pMap->getMainPart()->getName()))
			{
				pAllSectorList->addItem(pSector->getName());
				if(rightSectors.indexOf(pSector->getName()) >= 0)
				{
					if(firstIndex < 0)
					{
						firstIndex = pAllSectorList->count() - 1;
					}
					pAllSectorList->item(pAllSectorList->count() - 1)->setSelected(true);
				}
				break;
			}
		}
	}
	if(firstIndex >= 0)
	{
		pAllSectorList->setCurrentItem(pAllSectorList->item(firstIndex));
// TODO		pAllSectorList->ensureCurrentVisible();
	}
	setting = false;
	pRightPb->setEnabled(true);
	pLeftPb->setEnabled(false);
	emit selectionChanged();
}
