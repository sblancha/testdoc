#ifndef	VACICONVPG_EA_SPS_H
#define	VACICONVPG_EA_SPS_H

// Icon for VPG_SA	- LHC standalone pumping group

#include "VacIcon.h"

class EqpVPG_EA;

class VACCTLEWOUTIL_EXPORT VacIconVPG_EA_SPS : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVPG_EA_SPS(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVPG_EA_SPS();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

	// Override method of VacIcon 
	virtual void setDirection(VacIconContainer::Direction newDirection);

protected:
	// Pointer to my device
	EqpVPG_EA *pEqp;

	// Areas for pump and valve symbols. Drawing can be easy using QPainter
	// coordinate transformations, but finding areas for tooltips is not so
	// evident. May be it is more convenient just to keep in meory coordinates
	// of all distinct areas

	QRect	pump;
	QRect	valve;

	virtual void setAreaGeometry(void);

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(bool clearArea);
	virtual void drawValve(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
	virtual void drawPump(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);
	
	void setPainterForValve(QPainter &painter, Eqp::CtlStatus ctlStatus, unsigned rr1, bool plcAlarm);

	// Override VacIcon's reaction on mouse press
	virtual void mousePressEvent(QMouseEvent *pEvent);
};

#endif	// VACICONVPG_EA_SPS_H
