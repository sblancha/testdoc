//	Implementation of EqpVPI class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVPI.h"

#include "DataPool.h"
#include "FunctionalTypeVPI.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#include <QColor>
#include <QDateTime>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255

//	Main state bits to be analyzed - these are bits of VRPI
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define RR1_PROTECTED	(0x00080000)
#define	RR1_ON			(0x00020000)
#define	RR1_OFF			(0x00010000)
#define	RR1_UNDERRANGE	(0x00000200)
#define	RR1_SELFPROT	(0x00000100)
#define	RR1_OTHER_WARN	(0x00001C00)
#define RR1_AL3			(0x00008000)
#define RR1_AL2			(0x00004000)
#define RR1_AL1			(0x00002000)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVPI::EqpVPI(const Eqp &source) : Eqp(source)
{
	prOnline = prReplay = 0;
	functionalType = FunctionalType::VPI;
	pMaster = NULL;
	nOnlineConnects = nReplayConnects = nPollingConnects = 0;
}

EqpVPI::~EqpVPI()
{
}

/*
**	FUNCTION
**		After all devices have been read from file - find master and
**		make 'permanent' connection to 'selectChanged() signal of master
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPI::postProcess(void)
{
	const QString masterDp = getAttrValue("MasterName");
	if(!masterDp.isEmpty())
	{
		DataPool &pool = DataPool::getInstance();
		Eqp *pEqp = pool.findEqpByDpName(masterDp.toLatin1());
		if(pEqp)
		{
			Q_ASSERT(pEqp->inherits("EqpVRPI"));
			pMaster = (EqpVRPI *)pEqp;

			// In order to stay PERMANENTLY connected to master's selection
			selected = pMaster->isSelected();
			QObject::connect(pMaster, SIGNAL(selectChanged(Eqp *)), this, SLOT(masterSelectChange(Eqp *)));
		}
	}
}

/*
**	FUNCTION
**		Find master DP and master channel for this pump.
**
**	ARGUMENTS
**		masterDP		- Variable where master DP name will be written
**		masterChannel	- Variable where channel number in master will
**							be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPI::getMaster(QString &masterDp, int &masterChannel)
{
	masterDp = getAttrValue("MasterName");
	if(masterDp.isEmpty())
	{
		masterChannel = 0;
		return;
	}
	masterChannel = getAttrValue("MasterChannel").toInt();
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPI::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	if(pMaster)
	{
		pMaster->getStateDpes(stateDpes);
	}
}

/*
**	FUNCTION
**		Return list of all DPEs, related to analog value of device. The list is used
**		to build value history
**
**	ARGUMENTS
**		valueDpes	- Variable where all value DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPI::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	addDpeNameToList(valueDpes, "PR");
}


/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Dataacquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPI::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	// If this is the very first connection for given mode - we also need to
	// connect to master(VRPI)
	bool needMasterConnect = true;
	switch(mode)
	{
	case DataEnum::Online:
		needMasterConnect = nOnlineConnects == 0;
		break;
	case DataEnum::Replay:
		needMasterConnect = nReplayConnects == 0;
		break;
	case DataEnum::Polling:
		needMasterConnect = nPollingConnects == 0;
		break;
	}
	if(needMasterConnect)	// The very first connection
	{
		if(pMaster)
		{
			pMaster->connect(this, mode);
			// Selecting VRPI must select this device
			selected = pMaster->isSelected();
		}
	}
	connectDpe(pDst, "PR", mode);
	switch(mode)
	{
	case DataEnum::Online:
		nOnlineConnects++;
		break;
	case DataEnum::Replay:
		nReplayConnects++;
		break;
	case DataEnum::Polling:
		nPollingConnects++;
		break;
	}
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPI::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "PR", mode);
	switch(mode)
	{
	case DataEnum::Online:
		if(nOnlineConnects > 0)
		{
			nOnlineConnects--;
		}
		break;
	case DataEnum::Replay:
		if(nReplayConnects > 0)
		{
			nReplayConnects--;
		}
		break;
	case DataEnum::Polling:
		if(nPollingConnects > 0)
		{
			nPollingConnects--;
		}
		break;
	}
	// If this was the very last connection - we don't need connection to master anymore
	bool needMasterDisconnect = true;
	switch(mode)
	{
	case DataEnum::Online:
		needMasterDisconnect = nOnlineConnects == 0;
		break;
	case DataEnum::Replay:
		needMasterDisconnect = nReplayConnects == 0;
		break;
	case DataEnum::Polling:
		needMasterDisconnect = nPollingConnects == 0;
		break;
	}
	if(needMasterDisconnect)
	{
		if(pMaster)	// If we did not find it before - does not matter
		{
			pMaster->disconnect(this, mode);
		}
	}
}

/*
**	FUNCTION
**		Process selection request, pass request to master
**
**	ARGUMENTS
**		selected	- New selected state
**		isFromPvss	- true if selection request came from PVSS
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPI::setSelected(bool selected, bool isFromPvss)
{
	if(!isFromPvss)
	{
		Eqp::setSelected(selected, isFromPvss);
		return;
	}
	if(pMaster)
	{
		pMaster->setSelected(selected, isFromPvss);
	}
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPI::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "PR"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(prOnline == newValue)
			{
				return;
			}
			prOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**	FUNCTION
**		Process new replay value arrived from PVSS. Even though this DP
**		does not need the value (i.e. new value is the same as the old one) -
**		we still need to notify data pool that value for this DP has been
**		arrived (normally this is done by Eqp class)
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPI::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "PR"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(prReplay == newValue)
			{
				return;
			}
			prReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We connected
**		to VRPI, which in turn could connect toPLC, so we need to check where
**		signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPI::dpeChange(Eqp * /* pSrc */, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	if(source == DataEnum::Plc)
	{
		emit dpeChanged(this, dpeName, source, value, mode, timeStamp);
	}
	else	// VRPI
	{
		emit dpeChanged(this, dpeName, DataEnum::Parent, value, mode, timeStamp);
	}
}

/*
**	FUNCTION
**		Calculate main color of device. Color is taken from master
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPI::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(!pMaster)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else
	{
		pMaster->getMainColor(color, mode);
	}
}
#endif

int EqpVPI::getErrorCode(DataEnum::DataMode mode)
{
	if(pMaster)
	{
		return pMaster->getErrorCode(mode);
	}
	return 0;	// ????????
}

void EqpVPI::getErrorString(DataEnum::DataMode mode, QString &result)
{
	if(pMaster)
	{
		pMaster->getErrorString(mode, result);
	}
	else
	{
		result = "No master";
	}
}

/*
**	FUNCTION
**		Return main value for pump - pressure
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVPI::getMainValue(DataEnum::DataMode mode)
{
	return mode == DataEnum::Replay ? prReplay : prOnline;
}
#endif

/*
**	FUNCTION
**		Check if main value for pump (pressure) is valid or not.
**		Latest known requirement: even invalid value shall be shown,
**		so the value is always considered to be valid
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
bool EqpVPI::isMainValueValid(DataEnum::DataMode /* mode */)
{
	return true;
}
#endif

bool EqpVPI::isPlcAlarm(DataEnum::DataMode mode) const
{
	if(!pMaster)
	{
		return false;
	}
	return pMaster->isPlcAlarm(mode);
}

unsigned EqpVPI::getRR1(DataEnum::DataMode mode) const
{
	if(!pMaster)
	{
		return 0;
	}
	return pMaster->getRR1(mode);
}

bool EqpVPI::isBlockedOff(DataEnum::DataMode mode) const
{
	if(!pMaster)
	{
		return false;
	}
	return pMaster->isBlockedOff(mode);
}

Eqp::CtlStatus EqpVPI::getCtlStatus(DataEnum::DataMode mode) const
{
	if(!pMaster)
	{
		return Eqp::NotControl;
	}
	return pMaster->getCtlStatus(mode);
}

/*
**	FUNCTION
**		Calculate main state string of device. Just use main state string of VRPI
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPI::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(!pMaster)
	{
		string = "No master";
	}
	else
	{
		pMaster->getMainStateString(string, mode);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPI::getMainValueString(QString &string, DataEnum::DataMode mode)
{
	if(!pMaster)
	{
		string = getVisibleName();
		string += ": No master";
	}
	else
	{
		unsigned rr1 = pMaster->getRR1(mode);
		bool plcAlarm = pMaster->isPlcAlarm(mode);
		getMainValueString(rr1, mode == DataEnum::Replay ? prReplay : prOnline, plcAlarm, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPI::getMainValueString(unsigned /* rr1 */, float pr, bool /* plcAlarm */, QString &string)
{
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%8.2E", pr);
#else
	sprintf(buf, "%8.2E", pr);
#endif
	string = buf;

	/* Always display value - see above L.Kopylov 13.10.2010
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(rr1 & RR1_VALID)
	{
		if(rr1 & RR1_ON)
		{
			if(!(rr1 & RR1_OFF))
			{
				if((pr < 10000) && (pr > 0.0))
				{
					char buf[32];
					sprintf(buf, "%8.2E", pr);
					string = buf;
				}
			}
			else
			{
				string = "ERROR";
			}
		}
		else if(rr1 & RR1_OFF)
		{
			string = "OFF";
		}
		else
		{
			string = "Undefined";
		}		
	}
	else
	{
		string = "Not valid";
	}
	*/
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device - to be used in menu and tooltip
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPI::getMainValueStringForMenu(unsigned rr1, float pr, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(rr1 & RR1_VALID)
	{
		if(rr1 & RR1_ON)
		{
			if(!(rr1 & RR1_OFF))
			{
				if((pr < 10000) && (pr > 0.0))
				{
					char buf[32];
#ifdef Q_OS_WIN
					sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%8.2E", pr);
#else
					sprintf(buf, "%8.2E", pr);
#endif
					string = buf;
				}
			}
			else
			{
				string = "ERROR";
			}
		}
		else if(rr1 & RR1_OFF)
		{
			string = "OFF";
		}
		else
		{
			string = "Undefined";
		}		
	}
	else
	{
		string = "Not valid";
	}
}
#endif


/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPI::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	if(!pMaster)
	{
		string = getVisibleName();
		string += ": No master";
	}
	else
	{
		QString ctlStateString;
		if(isStateStringByCtlStatus(getCtlStatus(mode), ctlStateString))
		{
			string = getVisibleName();
			string += ": ";
			string += ctlStateString;
			return;
		}
		unsigned rr1 = pMaster->getRR1(mode);
		bool plcAlarm = pMaster->isPlcAlarm(mode);
		getMainStateStringForMenu(rr1, mode == DataEnum::Replay ? prReplay : prOnline, plcAlarm, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPI::getMainStateStringForMenu(unsigned rr1, float pr, bool plcAlarm, QString &string)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainValueStringForMenu(rr1, pr, plcAlarm, valueString);
	string += valueString;
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPI::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	getMainStateStringForMenu(string, mode);
}
#endif

/*
**	FUNCTION
**		Slot activated by master when it's selection state was changed.
**		We update our selection state and emit signal
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPI::masterSelectChange(Eqp *pSrc)
{
	if(selected != pSrc->isSelected())
	{
		selected = ! selected;
		emit selectChanged(this);
	}
}





/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPI::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	if(!pMaster)
	{
		return;
	}
	pMaster->getDpesForDevList(dpes);
	addDpeNameToList(dpes, "PR");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVPI::checkDevListCrit(void)
{
	if(!pMaster)
	{
		return 1;	// This is HUGE error - must be always shown
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	if(pMaster->checkDevListCrit() == 1)
	{
		return 1;
	}

	DataEnum::DataMode mode = criteria.getMode();

	// Check pressure if needed
	if(criteria.isUsePressureLimit())
	{
		float pr = mode == DataEnum::Replay ? prReplay : prOnline;
		if(pr > criteria.getPressureLimit())
		{
			return 1;
		}
	}

	// Last chance - criteria on pressure grow
	if(criteria.isUsePressureGrowLimit())
	{
		// Can not decide right now, more info is needed
		return 2;
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Return parameters for history query, required for device list
**
**	ARGUMENTS
**		dpe		- [out] DPE, for which history is required
**		start	- [out] start time of history query
**		end		- [out] end time of history query
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPI::getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUsePressureGrowLimit())
	{
		dpe = "";
		return;
	}
	dpe = dpName;
	dpe += ".PR";
	start = criteria.getPressureStartTime();
	if(criteria.isPressureEndTimeNow())
	{
		end = QDateTime::currentDateTime();
	}
	else
	{
		end = criteria.getPressureEndTime();
	}
}
#endif

/*
**	FUNCTION
**		Return grow factor for main value (pressure) to check if
**		device matches device list criteria
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Pressure grow factor; or
**		0 if criteria does not contain pressure grow
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVPI::getGrowFactorForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUsePressureGrowLimit())
	{
		return 0.0;
	}
	return criteria.getPressureGrowLimit();
}

int EqpVPI::getSpikeFilterForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUsePressureGrowLimit())
	{
		return 0;
	}
	return criteria.getPressureSpikeInterval();
}

#endif



/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPI::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	if(!pMaster)
	{
		return;
	}

	// Without PLC connection does not match any criteria
	if(pMaster->isPlcAlarm(DataEnum::Online))
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	if(pCriteria->getType() != EqpMsgCriteria::MainValue)
	{
		pMaster->checkCriteria(pCriteria, prOnline);
		return;
	}
	if(!pMaster->isValueValid(DataEnum::Online))
	{
		return;
	}
	// It can be (and it was observed) that callback for PR=10000
	// arrives before callback for RR1 with state=OFF. Reverse
	// situation can also occur when switching ON.
	// To avoid sending wrong messages in such cases value 10000
	// is considered to be invalid in all cases and does not change
	// criteria matching
	if(prOnline > 9999)
	{
		return;
	}
	if(pCriteria->isReverse())
	{
		if(prOnline < pCriteria->getLowerLimit())
		{
			pCriteria->setMatch(true, true, prOnline);
		}
		else if(prOnline > pCriteria->getUpperLimit())
		{
			pCriteria->setMatch(false, true, prOnline);
		}
	}
	else
	{
		if(prOnline < pCriteria->getLowerLimit())
		{
			pCriteria->setMatch(false, true, prOnline);
		}
		else if(prOnline > pCriteria->getUpperLimit())
		{
			pCriteria->setMatch(true, true, prOnline);
		}
	}
}
