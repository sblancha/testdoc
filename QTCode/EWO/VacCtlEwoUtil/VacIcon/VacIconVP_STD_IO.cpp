//	Implementation of VacIconVP_STD_IO class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVP_STD_IO.h"

#include "EqpVP_STDIO.h"

#include <qpainter.h>

#define	RR1_VALID			(0x40000000)
#define RR1_NOMINAL_SPEED	(0x08000000)
#define	RR1_ON				(0x02000000)
#define RR1_ERROR			(0x00800000)
#define	RR1_WARNING			(0x00400000)
#define RR1_ERROR_CODE_MASK	(0x000000FF)
#define RR1_WARNING_CODE_MASK	(0x0000FF00)

#define RR1_MANUAL			(0x04000000)
#define RR1_INTERLOCK		(0x00200000)
#define	RR1_ON_INTERLOCK	(0x00100000)
#define	RR1_AUTO_ON_ORDER	(0x00080000)
#define	RR1_MAN_ON_ORDER	(0x00040000)
#define RR1_FORCED			(0x00010000)


// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
// Colors for valve image
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define	COLOR_ERROR				255,102,102
#define	COLOR_WARNING			255,255,0

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVP_STD_IO::VacIconVP_STD_IO(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(21, 33);
}

VacIconVP_STD_IO::~VacIconVP_STD_IO()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVP_STD_IO::getGeometry(const VacEqpTypeMask & /* mask */)
{
	/* Is it needed at all? See very simple VPI icon
	switch(direction)
	{
	case VacIconContainer::Down:
		return VacIconGeometry(21, 33, 10, 35, 1);
	case VacIconContainer::Left:
		return VacIconGeometry(21, 33, 13, 16, 0);
	case VacIconContainer::Right:
		return VacIconGeometry(21, 33, -13, 16, 0);
	default:
		break;
	}
	*/
	return VacIconGeometry(21, 33, 10, 35, -1);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVP_STD_IO::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVP_STDIO"), "VacIconVP_STD_IO::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVP_STDIO *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVP_STD_IO::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVP_STD_IO::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVP_STD_IO::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVP_STD_IO::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVP_STD_IO::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	// L.Kopylov 03.10.2013 painter.eraseRect(0, 0, width(), height());

	// Connection line
	switch(direction)
	{
	case VacIconContainer::Up:
		painter.fillRect(10 - (pipeWidth >> 1), 0, pipeWidth, 9, pipeColor);
		break;
	case VacIconContainer::Down:
		painter.fillRect(10 - (pipeWidth >> 1), height() - 9, pipeWidth, 9, pipeColor);
		break;
	case VacIconContainer::Left:
		painter.fillRect(0, (height() >> 1) - (pipeWidth >> 1), 5, pipeWidth, pipeColor);
		break;
	case VacIconContainer::Right:
		painter.fillRect(width() - 4, (height() >> 1) - (pipeWidth >> 1), 5, pipeWidth, pipeColor);
		break;
	}

	// Prepare painter's coordinate system for drawing
	// Coordinate system (0,0) is center of widget
	painter.save();
	painter.translate(width() / 2, height() / 2);

	// Turn coordinate system according to direction
	/*
	switch(direction)
	{
	case VacIconContainer::Up:	// No rotation
		break;
	case VacIconContainer::Down:	// 180 degrees
		painter.rotate(180);
		break;
	case VacIconContainer::Left:	// 90 degrees counterclockwise
		painter.rotate(-90);
		break;
	case VacIconContainer::Right:	// 90 degrees clockwise
		painter.rotate(90);
		break;
	}
	*/

	// Coordinate system is ready - draw
	drawImage(painter);

	// Selection
	painter.restore();	// Original coordinate system - see save() above
	drawSymbols(painter);
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
}

void VacIconVP_STD_IO::drawImage(QPainter &painter)
{
	QPen pen(pipeColor, pipeWidth);
	painter.setPen(pen);
	painter.drawLine(0, -10, 0, -7);

	bool plcAlarm = true;
	unsigned rr1 = 0;
	bool isStarting = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if(pEqp)
	{
		ctlStatus = pEqp->getCtlStatus(mode);
		plcAlarm = pEqp->isPlcAlarm(mode);
		rr1 = pEqp->getRR1(mode);
		isStarting = pEqp->isStarting(mode);
	}

	QColor color;
	QColor color2;	// Color for lower right part of icon
	switch(ctlStatus)
	{
	case Eqp::NotControl:
		color.setRgb(COLOR_NOT_CONTROL);
		break;
	case Eqp::NotConnected:
		color.setRgb(COLOR_NOT_CONNECTED);
		break;
	default:
		if(plcAlarm)
		{
			color.setRgb(COLOR_UNDEFINED);
		}
		else if(!(rr1 & RR1_VALID))
		{
			color.setRgb(COLOR_UNDEFINED);
		}
		else if(rr1 & RR1_ERROR)
		{
			color.setRgb(COLOR_ERROR);
		}
		else if(isStarting)
		{
			color.setRgb(COLOR_ON);
			color2.setRgb(COLOR_OFF);
		}
		else if(rr1 & RR1_ON)
		{
			color.setRgb(COLOR_ON);
			/*
			if((rr1 & RR1_NOMINAL_SPEED) == 0)
			{
				split = true;
			}
			*/
		}
		else if((!(rr1 & RR1_ON)) && (rr1 & RR1_NOMINAL_SPEED))
		{
			color.setRgb(COLOR_WARNING);
		}
		else
		{
			color.setRgb(COLOR_OFF);
		}
		if((rr1 & RR1_WARNING) && (!(rr1 & RR1_ERROR)))
		{
			color2.setRgb(COLOR_WARNING);
		}
		break;
	}

	QBrush brush(color);
	painter.setBrush(brush);
	painter.setPen(Qt::black);
	if(!color2.isValid())
	{
		painter.fillRect(-9, -9, 19, 19, painter.brush());
		painter.drawRect(-9, -9, 18, 18);	// Real rectangle is 1 pixel more???
	}
	else
	{
		QPolygon points(3);
		points.setPoint(0, -9, -9);
		points.setPoint(1, 10, -9);
		points.setPoint(2, -9, 10);
		painter.drawPolygon(points);
		
		points.setPoint(0, 10, -9);
		points.setPoint(1, 10, 9);
		points.setPoint(2, -9, 9);
//		color.setRgb(COLOR_OFF);
		painter.setBrush(color2);
		painter.drawPolygon(points);
	}
	drawType(painter);
}

void VacIconVP_STD_IO::setPainterForMainImage(QPainter &painter)
{
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		pEqp->getMainColor(color, mode);
	}
	QBrush brush(color);
	painter.setBrush(brush);
	painter.setPen(Qt::black);
}

void VacIconVP_STD_IO::drawSymbols(QPainter &painter)
{
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			return;
		default:
			if(pEqp->isPlcAlarm(mode))
			{
				return;	// Invalid
			}
			unsigned rr1 = pEqp->getRR1(mode);
			if(!(rr1 & RR1_VALID))
			{
				return;	// Invalid
			}

			if(rr1 & RR1_FORCED)
			{
				drawForced(painter);
			}
			else if(rr1 & RR1_MANUAL)
			{
				drawManual(painter);
			}

			if(rr1 & RR1_INTERLOCK)
			{
				drawFullInterlock(painter);
			}
			else if(rr1 & RR1_ON_INTERLOCK)
			{
				drawStartInterlock(painter);
			}

			if(rr1 & (RR1_MANUAL | RR1_FORCED))
			{
				if(rr1 & RR1_MAN_ON_ORDER)
				{
					drawCmdOn(painter);
				}
				else
				{
					drawCmdOff(painter);
				}
			}
			else
			{
				if(rr1 & RR1_AUTO_ON_ORDER)
				{
					drawCmdOn(painter);
				}
				else
				{
					drawCmdOff(painter);
				}
			}
			break;
		}
	}
	else
	{
		drawManual(painter);
		drawFullInterlock(painter);
		drawCmdOff(painter);
	}
}

void VacIconVP_STD_IO::drawForced(QPainter &painter)
{
	painter.setPen(Qt::red);
	painter.drawLine(1, 1, 1, 5);
	painter.drawLine(2, 1, 4, 1);
	painter.drawLine(2, 3, 3, 3);
}

void VacIconVP_STD_IO::drawManual(QPainter &painter)
{
	painter.setPen(Qt::black);
	painter.drawLine(1, 1, 1, 5);
	painter.drawLine(5, 1, 5, 5);
	painter.drawLine(3, 3, 3, 4);
	painter.drawLine(2, 2, 2, 2);
	painter.drawLine(4, 2, 4, 2);
}

void VacIconVP_STD_IO::drawFullInterlock(QPainter &painter)
{
	painter.setPen(Qt::red);
	// F
	painter.drawLine(1, 27, 1, 31);
	painter.drawLine(2, 27, 4, 27);
	painter.drawLine(2, 29, 3, 29);
	// I
	painter.drawLine(5, 27, 7, 27);
	painter.drawLine(6, 28, 6, 30);
	painter.drawLine(5, 31, 7, 31);
}

void VacIconVP_STD_IO::drawStartInterlock(QPainter &painter)
{
	painter.setPen(Qt::darkYellow);
	// S
	painter.drawLine(1, 27, 3, 27);
	painter.drawLine(1, 27, 1, 28);
	painter.drawLine(2, 29, 3, 29);
	painter.drawLine(3, 30, 3, 31);
	painter.drawLine(1, 31, 2, 31);
	// I
	painter.drawLine(5, 27, 7, 27);
	painter.drawLine(6, 28, 6, 30);
	painter.drawLine(5, 31, 7, 31);
}

void VacIconVP_STD_IO::drawCmdOff(QPainter &painter)
{
	QColor color = palette().color(backgroundRole());
	color.setAlpha(192);
	painter.fillRect(8, 0, 12, 7, color);
	painter.setPen(Qt::red);
	// O
	painter.drawLine(8, 2, 8, 4);
	painter.drawLine(9, 1, 10, 1);
	painter.drawLine(9, 5, 10, 5);
	painter.drawLine(11, 2, 11, 4);
	// F
	painter.drawLine(13, 1, 13, 5);
	painter.drawLine(14, 1, 15, 1);
	painter.drawLine(13, 3, 14, 3);
	// F
	painter.drawLine(17, 1, 17, 5);
	painter.drawLine(18, 1, 19, 1);
	painter.drawLine(17, 3, 18, 3);
}

void VacIconVP_STD_IO::drawCmdOn(QPainter &painter)
{
	painter.setPen(Qt::darkGreen);
	// O
	painter.drawLine(11, 2, 11, 4);
	painter.drawLine(12, 1, 13, 1);
	painter.drawLine(12, 5, 13, 5);
	painter.drawLine(14, 2, 14, 4);
	// N
	painter.drawLine(16, 1, 16, 5);
	painter.drawLine(19, 1, 19, 5);
	painter.drawLine(16, 2, 19, 5);
}
