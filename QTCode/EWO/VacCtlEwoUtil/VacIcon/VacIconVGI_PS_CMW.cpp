//	Implementation of VacIconVGI_PS_CMW class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGI_PS_CMW.h"

#include <qpainter.h>

// We only need to implement drawSpecific() method

// Draw 'Ion gauge' symbol in the center of icon
void VacIconVGI_PS_CMW::drawSpecific(QPainter &painter)
{
	painter.setPen(Qt::black);
	painter.drawText(9, 15, "i");
}
