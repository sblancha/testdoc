//	Implementation of SectLhcLine class
/////////////////////////////////////////////////////////////////////////////////

#include "SectLhcLine.h"

#include "VacLinePart.h"

#include "Eqp.h"
#include "LhcRegion.h"
#include "DataPool.h"
#include "EqpSECT_VPI_SUM.h"

#include "VacIcon.h"
#include "VacIconContainer.h"
#include "DebugCtl.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SectLhcLine::SectLhcLine(QWidget *parent, int type, float start, float end) :
	SynLhcLineBeam(parent, type, start, end)
{
	decideForEqpLocation();
}

SectLhcLine::~SectLhcLine()
{
	clear();
}

/*
**	FUNCTION
**		Clear all content of line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::clear(void)
{
	SynLhcLineBeam::clear();
	while(!childLineList.isEmpty())
	{
		delete childLineList.takeFirst();
	}
}

/*
**
** FUNCTION
**		Override method of SynLhcLineBeam:
**		1) controllable equipment - only sector borders are shown
**		2) non-controllable equipment - not shown at all
**
** PARAMETERS
**		pEqp	- Pointer to device in question
**		partIdx	- Index of line part, where device is located, will be returned here
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
bool SectLhcLine::isMyEqp(Eqp *pEqp, int &partIdx)
{
	switch(pEqp->getType())
	{
	case EqpType::VacDevice:
		break;
	case EqpType::OtherLineStart:
	case EqpType::OtherLineEnd:
		if(checkEqpVacType(pEqp, partIdx))
		{
			addChildLine(pEqp, partIdx);
		}
		/* NOBREAK */
	default:
		return false;
	}

	// Controllable vacuum equipment is checked here
	if(pEqp->isSkipOnSynoptic())
	{
		return false;
	}
	if(!pEqp->isActive(mode))
	{
		return false;
	}
	if(pEqp->isCryoThermometer())
	{
		return false;
	}
	int iconType = VacIcon::getIconType(pEqp);
	switch(iconType)
	{
	case VacIconContainer::VVS_PS:
	case VacIconContainer::VVS_LHC:
	case VacIconContainer::VVS_SPS:
	case VacIconContainer::VVF_SPS:
	case VacIconContainer::EXP_AREA:
		break;
	default:
		return false;
	}
	return SynLhcLineBeam::isMyEqp(pEqp, partIdx);
}

/*
**	FUNCTION
**		Decide for equipment location relative to vacuum line (above/below)
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::decideForEqpLocation(void)
{
	switch(type)
	{
	case OuterBeam:
		eqpAboveLine = true;
		break;
	case CrossBeam:
	case InnerBeam:
	case Passive:
		eqpAboveLine = false;
		break;
	}
}

/*
**	FUNCTION
**		Decide which vacuum type of line part shall be added
**		for given region type
**
**	PARAMETERS
**		regionType	- Type of LHC region
**
**	RETURNS
**		Enum corresponding to required vacuum type,
**		VacType::None if line part shall not be added.
**
** CAUTIONS
**		None
*/
int SectLhcLine::vacTypeOfRegion(int regionType)
{
	int	result = VacType::None;
	switch(regionType)
	{
	case LhcRegion::BlueOut:
		if(type == OuterBeam)
		{
			result = VacType::BlueBeam;
		}
		else if(type == InnerBeam)
		{
			result = VacType::RedBeam;
		}
		break;
	case LhcRegion::RedOut:
		if(type == OuterBeam)
		{
			result = VacType::RedBeam;
		}
		else if(type == InnerBeam)
		{
			result = VacType::BlueBeam;
		}
		break;
	case LhcRegion::Cross:
		if(type == CrossBeam)
		{
			result = VacType::CrossBeam;
		}
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Build all equipment to be shown on this line
**
**	PARAMETERS
**		vacTypeMask	- Bit mask for vacuum type selection
**		eqpMask		- Bit mask for equipment selection
**
**	RETURNS
**		Number of equipment in this line
**
**	CAUTIONS
**		None
*/
int SectLhcLine::build(unsigned vacTypeMask, VacEqpTypeMask &eqpMask)
{
	if((eqpMask == this->eqpMask) && (vacTypeMask == this->vacTypeMask))
	{
		if(eqps.count())
		{
			return eqps.count();	// No need to rebuild
		}
	}
	clear();
	this->vacTypeMask = vacTypeMask;
	this->eqpMask = eqpMask;
	buildLineParts();
	if(!pPartList->count())
	{
		return 0;
	}
	buildEquipment();
	/* L.Kopylov 27.05.2008
	if(type == SectLhcVacTypeCrossBeam)	// Add sector device after last sector border
	{
		if(nEqp)
		{
			if(eqp[nEqp-1].isSectorBorder)
			{
				AddSectorDevice(eqp[nEqp-1].pEqp, 0, EqpItemTypeVacSector2);
			}
		}
	}
	*/
	buildEqpGroups();

	// Build child lines
	if(!childLineList.count())
	{
		return eqps.count();
	}
	QList<Sector *> sectorUsage;
	QList<Sector *> &sectors = DataPool::getInstance().getSectors();
	int idx;
	for(idx = 0 ; idx < sectors.count() ; idx++)
	{
		Sector *pSector = sectors.at(idx);
		if(pSector->getVacType() != VacType::Beam)
		{
			sectorUsage.append(pSector);
		}
	}
	for(idx = 0 ; idx < childLineList.count() ; idx++)
	{
		SectLhcInjDumpLine *pChildLine = childLineList.at(idx);
		pChildLine->build(sectorUsage);
	}
	return eqps.count();
}

/*
**	FUNCTION
**		Check if vacuum type (of equipment) matches vacuum type mask.
**		Method of parent class is overwritten because after introducing
**		thermometers that method checks type of synoptic line, but types
**		of synoptic and sector view lines are different.
**		In addition, we don't need thermometers in this view at all, so here
**		we can use simplified (old) version.
**
**	PARAMETERS
**		vacType	- Vacuum type to check
**
**	RETURNS
**		true if vacType matches vacTypeMask;
**		false - otherwise.
**
**	CAUTIONS
**		None
*/
bool SectLhcLine::vacTypeMatchesMask(unsigned vacType)
{
	// Then ready to compare
	if(vacType & vacTypeMask)
	{
		return true;
	}
	if(vacType & (VacType::CrossBeam | VacType::CommonBeam))
	{
		if(vacTypeMask & (VacType::RedBeam | VacType::BlueBeam))
		{
			return true;
		}
	}
	else if(vacType & VacType::DSL)
	{
		if(vacTypeMask & VacType::Cryo)
		{
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Calculate screen position for next device to be processed
**		If device is start/end of child line - calculate horizontal
**		geometry of that child line
**
**	PARAMETERS
**		coord		- Last processed world coordinate
**		connPos		- Last used connection point in screen coordinates
**		endCoord	- Last used end coordinate of element
**		endConnPos	- Last used and connection point
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::placeEqp(float &coord, int &connPos, float &endCoord, int &endConnPos)
{
	SynLhcEqpItem *pItem = eqps.at(curEqpIdx);
	if(!pItem)
	{
		return;	// Shall not happen, just protection
	}

	if((pItem->getType() == EqpType::SectorBefore) || (pItem->getType() == EqpType::SectorAfter))
	{
		placeSector(coord, connPos);
		return;
	}

	SynLhcLineBeam::placeEqp(coord, connPos, endCoord, endConnPos);

	if((pItem->getType() != EqpType::OtherLineStart) && (pItem->getType() != EqpType::OtherLineEnd))
	{
		return;
	}
	SectLhcInjDumpLine *pChildLine = NULL;
	for(int idx = 0 ; idx < childLineList.count() ; idx++)
	{
		SectLhcInjDumpLine *pChild = childLineList.at(idx);
		if(!strcmp(pChild->getBeamLine()->getName(), pItem->getEqp()->getName()))
		{
			pChildLine = pChild;
			break;
		}
	}
	if(!pChildLine)
	{
		return;
	}

	// Move child lines in horizontal direction
	bool isOtherLineEnd = false;
	switch(pItem->getType())
	{
	case EqpType::OtherLineStart:
		switch(pChildLine->getBeamLine()->getStartType())
		{
		case BeamLineConnType::LeftIn:
		case BeamLineConnType::RightIn:
			isOtherLineEnd = true;
			break;
		default:
			isOtherLineEnd = false;
			break;
		}
		break;
	case EqpType::OtherLineEnd:
		switch(pChildLine->getBeamLine()->getEndType())
		{
		case BeamLineConnType::LeftIn:
		case BeamLineConnType::RightIn:
			isOtherLineEnd = true;
			break;
		default:
			isOtherLineEnd = false;
			break;
		}
		break;
	default:
		return;
	}

	int delta = 0;
	if(isOtherLineEnd)
	{
		if(connPos < pChildLine->getEndX())
		{
			connPos = pChildLine->getEndX();
			pChildLine->setConnPoint(connPos);
			return;
		}
		delta = connPos - pChildLine->getEndX();
	}
	else
	{
		delta = connPos - pChildLine->getStartX();
		if(endX < (connPos + pChildLine->getEndX()))
		{
			endX = connPos + pChildLine->getEndX();
		}
	}
	pChildLine->moveHorizontally(delta, true);
	pChildLine->setConnPoint(connPos);
	pChildLine->buildSectorIcons();
}

/*
**	FUNCTION
**		Calculate screen position for next sector icon to be processed
**
**	PARAMETERS
**		coord		- Last processed world coordinate
**		connPos		- Last used connection point in screen coordinates
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::placeSector(float & /* coord */, int & /* connPos */)
{
	SynLhcEqpItem *pItem = NULL;
	if((0 <= curEqpIdx) && (curEqpIdx < eqps.count()))
	{
		pItem = eqps.at(curEqpIdx);
	}
	if(!pItem)
	{
		return;	// Shall not happen, just protection
	}

	int minIconLeft = 0;

	// Icon shall not overlap with previous icon on the same line
	if(curEqpIdx > 0)
	{
		SynLhcEqpItem *pPrev = eqps.at(curEqpIdx - 1);
		minIconLeft = pPrev->getRect().right() + MIN_ACTIVE_SPACING;
		/* LIK 25.05.2007 Search backward up to previous sector border
		short	widthToAdd = 0;
		for( long n = curEqpIdx - 1 ; n >= 0 ; n-- )
		{
			if( eqp[n].isSectorBorder )
			{
				minIconLeft = eqp[n].x + eqp[n].width + MIN_ACTIVE_SPACING;
				break;
			}
			widthToAdd += eqp[n].width + MIN_ACTIVE_SPACING;
		}
		pItem->width += widthToAdd;
		*/
	}
	else
	{
		// minIconLeft = endConnPos;
		minIconLeft = 0;
	}

	// Icon shall not overlap with start of current line part
	if((0 <= curPartIdx) && (curPartIdx < pPartList->count()))
	{
		VacLinePart *pPart = pPartList->at(curPartIdx);
		if(pPart->isStartDone())
		{
			if(minIconLeft < (pPart->getStartX() + MIN_ACTIVE_SPACING))
			{
				minIconLeft = pPart->getStartX() + MIN_ACTIVE_SPACING;
			}
		}
	}

	QRect &rect = pItem->getRect();
	rect.moveLeft(minIconLeft);
	QPoint &connPoint = pItem->getConnPoint();
	connPoint.setX(minIconLeft);
	if(rect.right() > endX)
	{
		endX = rect.right();
	}
	curEqpIdx++;
}

/*
**	FUNCTION
**		Calculate vertical limits for line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::calcVerticalLimits( void )
{
	// First normal processing
	SynLhcLineBeam::calcVerticalLimits();

	// then move sector summary icons further from beam line
	int idx;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		if((pItem->getType() == EqpType::SectorBefore) || (pItem->getType() == EqpType::SectorAfter))
		{
			if(eqpAboveLine)
			{
				if(maxSectHeight)
				{
					QRect &rect = pItem->getRect();
					rect.moveTop(descent - maxSectHeight - rect.height() - 4);
					QPoint &connPoint = pItem->getConnPoint();
					connPoint.setY(rect.top());
				}
			}
			else
			{
				if(maxSectHeight)
				{
					QRect &rect = pItem->getRect();
					rect.moveTop(- ascent - rect.height() + maxSectHeight - 0);
					QPoint &connPoint = pItem->getConnPoint();
					connPoint.setY(rect.top());
				}
			}
		}
	}

	for(idx = 0 ; idx < childLineList.count() ; idx++)
	{
		SectLhcInjDumpLine *pChildLine = childLineList.at(idx);
		// Decide where child line shall be - above or below this line.
		// Criteria is very simple: always above for inner beam, always below for outer beam
		if(eqpAboveLine)
		{
			ascent += pChildLine->getAscent() + pChildLine->getDescent();
		}
		else
		{
			descent += pChildLine->getAscent() + pChildLine->getDescent();
		}
	}
}

/*
**	FUNCTION
**		Move all elements in line in vertical direction
**		Move also all child lines in vertical direction
**
**	PARAMETERS
**		delta	- The offset for movement
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::moveVertically(int &delta)
{
	// Move child lines in vertical direction - if child lines are above this line
	if(eqpAboveLine)
	{
		for(int idx = 0 ; idx < childLineList.count() ; idx++)
		{
			SectLhcInjDumpLine *pChildLine = childLineList.at(idx);
			pChildLine->moveVertically(delta + pChildLine->getAscent(), true);
		}
		delta += 2;
	}

	// Move this line itself
	SynLhcLineBeam::moveVertically(delta);

	// Move child lines in vertical direction - if child lines are below this line
	if(!eqpAboveLine)
	{
		for(int idx = 0 ; idx < childLineList.count() ; idx++)
		{
			SectLhcInjDumpLine *pChildLine = childLineList.at(idx);
			pChildLine->moveVertically(y + descentThis + pChildLine->getAscent(), true);
		}
	}
}

/*
**	FUNCTION
**		Calculate final geometry for sector summary icons
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::finalSectorIconsGeometry(void)
{
	int	iconStart, iconEnd;

	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		if(pItem->getType() == EqpType::VacDevice)
		{
			continue;
		}
		findSectorSummaryLimits(pItem, iconStart, iconEnd);
		QRect &rect = pItem->getRect();
		rect.moveLeft(iconStart);
		rect.setWidth(iconEnd - iconStart);
		pItem->getConnPoint().setX(iconStart);

		// Common sector between two beams is placed above inner beam line
		/* TODO LIK 20.08.2009
		if((type == InnerBeam) && (((CSector *)eqp[eqpIdx].pEqp)->vacType == VacTypeBeamsCommon))
		{
			eqp[eqpIdx].y = eqp[eqpIdx].connY = lineY - yAscent - 4 * eqp[eqpIdx].height;
			eqp[eqpIdx].height *= 2;
		}
		*/
	}
}

/*
**	FUNCTION
**		Find minimum and maximum horizontal position for sector
**		summary icon
**
**	PARAMETERS
**		pItem		- Pointer to sector summary device
**		iconStart	- Minimum allowed X coordinate for icon start will be returned here
**		iconEnd		- Maximum allowed X coordinate for icon end will be returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::findSectorSummaryLimits(SynLhcEqpItem *pItem, int &iconStart, int &iconEnd)
{
	QRect &rect = pItem->getRect();

	iconStart = rect.left();
	iconEnd = rect.right();

	// Take limits from neighbour devices
	/* LIK 25.05.2007 Search back and force up to first sector border
	if( eqpIdx )
	{
		iconStart = eqp[eqpIdx-1].x + eqp[eqpIdx-1].width + MIN_ACTIVE_SPACING;
	}
	if( eqpIdx < ( nEqp - 1 ) )
	{
		iconEnd = eqp[eqpIdx+1].x - MIN_ACTIVE_SPACING;
	}
	*/
	for(int n = eqps.indexOf(pItem) - 1 ; n >= 0 ; n--)
	{
		SynLhcEqpItem *pPrev = eqps.at(n);
		if(pPrev->isSectorBorder())
		{
			iconStart = pPrev->getConnPoint().x() + MIN_ACTIVE_SPACING;
			break;
		}
	}

	if(pItem == eqps.last())	// The very last sector expands to the end of line
	{
		for(int idx = 0 ; idx < pPartList->count() ; idx++)
		{
			VacLinePart *pPart = pPartList->at(idx);
			if((pPart->getStartX() <= rect.left()) && (rect.left() <= pPart->getEndX()))
			{
				iconEnd = pPart->getEndX() - MIN_ACTIVE_SPACING;
			}
			break;	// TODO: why here and not 2 lines above ??? LIK 20.08,2009
		}
	}
	else	// Till next sector border
	{
		int nEqps = eqps.count();
		for(int n = eqps.indexOf(pItem) + 1 ; n < nEqps ; n++)
		{
			SynLhcEqpItem *pNext = eqps.at(n);
			if(pNext->isSectorBorder())
			{
				iconEnd = pNext->getConnPoint().x() - MIN_ACTIVE_SPACING;
				break;
			}
		}
	}

	// Compare to line part limits
	for(int idx = 0 ; idx < pPartList->count() ; idx++)
	{
		VacLinePart *pPart = pPartList->at(idx);
		if((pPart->getStartX() <= rect.left()) && (rect.left() <= pPart->getEndX()))
		{
			if(iconStart < pPart->getStartX())
			{
				iconStart = pPart->getStartX() + MIN_ACTIVE_SPACING;
			}
			if(iconEnd > pPart->getEndX())
			{
				iconEnd = pPart->getEndX() - MIN_ACTIVE_SPACING;
			}
			break;
		}
	}
}

/*
**	FUNCTION
**		Add device to list of devices to be shown in this line
**		Add also sector summary device for sector BEFORE this device
**		if necessary
**
**	PARAMETERS
**		pEqp	- Pointer to device to be added
**		partIdx	- Index of line part where device is located
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::addEqp(Eqp *pEqp, int partIdx)
{
	// Check if sector shall be added
	if(pEqp->getSectorBefore())
	{
		if(isSectorForThisLine(pEqp))
		{
			// Do not add sector before the very first device on inner and outer lines,
			// though still add on cross line
			if(eqps.count())
			{
				/* LIK 25.05.2007 - check more than one previous device
				if( eqp[nEqp-1].isSectorBorder )
				{
					AddSectorDevice( pEqp, EqpItemTypeVacSector1 );
				}
				*/
				for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
				{
					SynLhcEqpItem *pPrev = eqps.at(idx);
					if(pPrev->isSectorBorder())
					{
						addSectorDevice(pEqp, partIdx, EqpType::SectorBefore);
						break;
					}
				}
			}
			else if(type == CrossBeam)
			{
				addSectorDevice(pEqp, partIdx, EqpType::SectorBefore);
			}
		}
	}

	// Add device itself
	SynLhcLineBeam::addEqp(pEqp, partIdx);

	// L.Kopylov 27.05.2008
	if(type == CrossBeam)
	{
		if(pEqp->isSectorBorder())
		{
			addSectorDevice(pEqp, partIdx, EqpType::SectorAfter);
		}
	}
}

/*
**	FUNCTION
**		Check if sector summary device shall be added to this line
**		for sectorBefore of device being added to the line
**
**	PARAMETERS
**		pEqp		- Pointer to device being added
**
**	RETURNS
**		true	- if sector summary device shall be added,
**		false	- otherwise.
**
**	CAUTIONS
**		None
*/
bool SectLhcLine::isSectorForThisLine(Eqp *pEqp)
{
	Sector	*pSector = pEqp->getSectorBefore();
	if(pSector->isOuter())
	{
		return false;
	}
	if(pSector->getVacType() == pEqp->getVacType())
	{
		return true;
	}
	switch(pSector->getVacType())
	{
	case VacType::CrossBeam:
		break;
	case VacType::CommonBeam:
		if((!pSector->getCrossStartEqp()) && (!pSector->getCrossEndEqp()))	// Common sector on two parallel lines
		{
			return type == InnerBeam;	// Put all common sectors on inner
		}
		else	// Y-sector
		{
			return type == CrossBeam;
		}
		break;
	}
	return false;
}

/*
**	FUNCTION
**		Add sector summary device to list of devices to be shown in this line
**
**	PARAMETERS
**		pEqp	- Pointer to device to be added
**		partIdx	- Index of line part where device is located
**		type	- Type of sector device to be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::addSectorDevice(Eqp *pEqp, int partIdx, int type)
{
	QString dpName;

	switch(type)
	{
	case EqpType::SectorBefore:
		if(pEqp->getSectorBefore()->isOuter())
		{
			return;
		}
		dpName = EqpSECT_VPI_SUM::getDpNameForSector(pEqp->getSectorBefore());
		break;
	case EqpType::SectorAfter:
		if(pEqp->getSectorAfter()->isOuter())
		{
			return;
		}
		dpName = EqpSECT_VPI_SUM::getDpNameForSector(pEqp->getSectorAfter());
		break;
	default:
		return;
	}
	if(dpName.isEmpty())
	{
		return;
	}
	Eqp *pSectorEqp = DataPool::getInstance().findEqpByDpName(dpName.toLatin1());
	if(!pSectorEqp)
	{
		return;
	}
	
	// Check if this sector has been added already as SectorAfter - LIK 27.05.2008
	if(type == EqpType::SectorBefore)
	{
		for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
		{
			SynLhcEqpItem *pPrev = eqps.at(idx);
			if(pPrev->getEqp() == pSectorEqp)
			{
				return;
			}
		}
	}

	VacIcon *pIcon = VacIcon::getIcon(pSectorEqp, parent);
	if(!pIcon)
	{
		return;
	}
	pIcon->setMode(mode);

	SynLhcEqpItem	*pItem = new SynLhcEqpItem(pSectorEqp);
	pItem->setType(type);
	pItem->setIcon(pIcon);
	VacLinePart *pPart = pPartList->at(partIdx);
	float lhcRingLength = DataPool::getInstance().getLhcRingLength();
	float startPos = pEqp->getStart();
	if((pPart->getStartPos() > pPart->getEndPos()) || (pPart->getEndPos() > lhcRingLength))
	{
		if(pEqp->getStart() < pPart->getStartPos())
		{
			startPos += lhcRingLength;
		}
	}
	else if(pPart->isAfterIP1())
	{
		startPos += lhcRingLength;
	}
	pItem->setStart(startPos);
	QRect &rect = pItem->getRect();
	rect.setWidth(16);
	eqps.append(pItem);
}

/*
**	FUNCTION
**		Add child (injection/dump) line to beam line
**
**	PARAMETERS
**		pConnEqp	- Pointer to child line connection device
**		partIdx		- Index of line part where child line is located
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::addChildLine(Eqp *pConnEqp, int partIdx)
{
	// Find child beam line
	BeamLine	*pLine = DataPool::getInstance().getLine(pConnEqp->getName());
	if(!pLine)
	{
		return;
	}

	// Create child sector view line if it does not exist yet
	SectLhcInjDumpLine *pChildLine = NULL;
	for(int idx = 0 ; idx < childLineList.count() ; idx++)
	{
		SectLhcInjDumpLine *pChild = childLineList.at(idx);
		if(pChild->getBeamLine() == pLine)
		{
			pChildLine = pChild;
			break;
		}
	}
	if(!pChildLine)	// New child line
	{
		SectLhcInjDumpLine *pChildLine = SectLhcInjDumpLine::create(parent, pLine, pConnEqp->getVacType());
		if(!pChildLine)
		{
			return;
		}
		pChildLine->setMode(mode);
		childLineList.append(pChildLine);
	}
	addEqp(pConnEqp, partIdx);
}

/*
**	FUNCTION
**		Notify synoptic line that geometry calculation has been finished,
**		line shall move all active icons to calculated positions.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::finishGeometry(void)
{
	SynLhcLine::finishGeometry();
	for(int idx = 0 ; idx < childLineList.count() ; idx++)
	{
		SectLhcInjDumpLine *pChildLine = childLineList.at(idx);
		pChildLine->finishGeometry();
	}
}

/*
**	FUNCTION
**		Draw line image: beam lines, active elements, sector labels etc...
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		viewStart	- Start X coordinate of visible area
**		viewEnd		- End X coordinate of visible area
**
**	RETURNS
**		always 1
**
**	CAUTIONS
**		None
*/
int SectLhcLine::draw(QPainter &painter, int viewStart, int viewEnd)
{
	drawParts(painter, viewStart, viewEnd);
	int idx;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		pItem->setCommonReady(false);
	}
	drawActive(painter, viewStart, viewEnd);
	drawSectLabels(painter, viewStart, viewEnd);

	if(!childLineList.count())
	{
		return 1;
	}

	// Draw all child lines
	for(idx = 0 ; idx < childLineList.count() ; idx++)
	{
		SectLhcInjDumpLine *pChildLine = childLineList.at(idx);
		pChildLine->draw(painter, viewStart, viewEnd);
		/* TODO !!!!
		SelectObject( hDc, pens.hLinePen );
		MoveToEx( hDc, pChildLine->connX - pParam->viewStart, pChildLine->yCoord, NULL );
		LineTo( hDc, pChildLine->connX - pParam->viewStart, this->lineY );
		*/
	}

	return 1;
}

/*
**	FUNCTION
**		Write all parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SectLhcLine::dump(FILE *pFile)
{
	if(!DebugCtl::isSynoptic())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}
	SynLhcLine::dump(pFile);
	if(!childLineList.count())
	{
		return;
	}
	fprintf(pFile, "\n\n---------------- Child lines -----------------------\n");
	for(int idx = 0 ; idx < childLineList.count() ; idx++)
	{
		SectLhcInjDumpLine *pChildLine = childLineList.at(idx);
		pChildLine->dump(pFile);
	}
}

const char *SectLhcLine::getLineName(void)
{
	switch(type)
	{
	case Passive:
		return "PASSIVE";
	case OuterBeam:
		return "OUTER BEAM";
	case CrossBeam:
		return  "CROSS BEAM";
	case InnerBeam:
		return "INNER BEAM";
	default:
		break;
	}
	static char result[32];
#ifdef Q_OS_WIN
	sprintf_s(result, sizeof(result) / sizeof(result[0]), "??? %d", type);
#else
	sprintf(result, "??? %d", type);
#endif
	return result;
}
