
/**@name SCRIPT: vcsMobileEqp.ctl

@author: Leonid Kopylov (IHEP, Protvino, Russia)

Creation Date: 14/06/2006

Modification History: 
			12.01.2007		L.Kopylov
				New DPE WorkDPNames has been added to _VacMobileList. Implementation
				of using working DPs for data; generation of config files for LHC logging

			21.03.2007		L.Kopylov
				Use VacImport DLL for keeping information on data arrival to verify
				address set works, i.e. new data are coming from PLC.
				Write -1, not 0, after address config has been removed from DPE.

			06.06.2007		L.Kopylov
				Action for device with status = 13 is always 'check old connection':
				sometimes PLC gots confirmation from PVSS before PVSS really processed
				the data. There are two known reasons for this:
				1) Another system (Linux/Windows) can react faster than this one
				2) PLC clears confirmation when mobile device is disconnected, so
					value 1 can stay there from previously confirmed connection

			10.06.2007		L.Kopylov
				Do not process callback data immediately, instead put them to 'queue',
				data from queue are processed by main() function. This approach shall
				solve problem of callback data arrival while previous callback processing
				is not finished yet.
				Report to PVSS that device is connected/disconnected is done immediately
				when data processed. However, writing new config file for LHC logging is
				only done when all queue is processed.

			18.06.2007		L.Kopylov
				Use LHC logging config template file and dedicated DLL for writing LHC
				logging configuration file suitable for LHC logging manager.
				The main disadvantages of old approach were:
				1) Slow
				2) All DPEs with PVSS archiving set are written to LHC logging, even if
					LHC logging flag is not set in vacuum control DB.

			22.02.2008		L.Kopylov
				Change in DeactivateTargetDp(): set address to not active instead of deleting
				address. IT/CO claims that reusing not active addresses does not increase size
				of PVSS configuration DBs

    01.12.2011  L.Kopylov
      Porting to PVSS 3.8

    12.01.2018  Version 2.1 S. Blanchard (SBLANCHA)
    unvalidate and set bit error of mobile if PLC master send error type
    
    08.08.2019 Version 2.2 S. Blanchard
    add some and update DeactivateGhostDps()     
    
    
version 2.2

Purpose: 
Process connect/disconnect of mobile equipment.
The script subscribes to all mobile container DPs (DP type VMOB).
When mobile equipment is connected on container - find corresponding target DP
of appropriate type and 'activate' it, i.e. set all required periphery addresses
for DPEs of target DP.
When mobile equipment is disconnected on container - 'deactivate' corresponding
target DP, i.e. remove periphery address from all DPEs.

Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. global variable: the following variables are global to the script
		. mobileSourceList: List of all mobile equipment containers which are either active now
			or were active just before last changes: dyn_string.
		. mobileDpList: List target DP names for every mobile equipment container DP from mobileSourceList: dyn_string.
		. mobileStateList: list activity flangs for all pairs mobileSourceList[n] + mobileDpList[n]: dyn_bool.
		. workDpList: List working DPs used for processing data for correspodning item from mobileDpList: dyn_string
		. isInitializing: flag indicating if initialization is still in progress: bool.
		. connStatus: Completion code of target DP activation
		. queue: list of data that came from callback for processing
		. isNewConfig: flag indicating if new config file for LHC logging shall be written
	. constant:
		. MOBILE_CONTAINER_DP_TYPE: DP type of mobile equipment container
		. VAC_MOBILE_LIST_DP_TYPE: DP type of DP containing list of all active mobile equipment in system
	. data point type needed: VMOB, _VacMobileList
	. data point:
	. PVSS version: 3.1
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/

#uses "VacCtlEqpDataPvss"	//load dll or so file
#uses "VacCtlImportPvss"		// load dll or so file
#uses "vclMobile.ctl"	// Load control library
// no mobile logging anyway #uses "lhcVacMobileLogConfig.ctl"	// Load control library
#uses "vclMachine.ctl"	// Load control library
#uses "vclDeviceFileParser.ctl"	// Load control library
#uses "vclSectors.ctl"	// Load control library
#uses "vclDevCommon.ctl"	// Load control library
#uses "vclAddressConfig.ctl" // Load control library

// DP type of mobile equipment container
const string MOBILE_CONTAINER_DP_TYPE = "VMOB";

// Name of DP type with list of active mobile equipment
const string VAC_MOBILE_LIST_DP_TYPE = "_VacMobileList";

//=====================================================================================
//============== Enumeration - action required for new data in VMOB ===================
//=====================================================================================
// No action
const int MOBILE_ACTION_NONE = 0;

// Target DP shall be deactivated if it is active, or nothing shall be done if target DP is not active
const int MOBILE_ACTION_DEACTIVATE = 1;

// Target DP shall be activated
const int MOBILE_ACTION_ACTIVATE = 2;

// Target DP has been activated before, existence of target DP shall be checked
const int MOBILE_ACTION_OLD_ACTIVATION = 3;

// V2.1 Clean mobile list request
const int MOBILE_ACTION_CLEAN_LIST = 4;

//=====================================================================================
//============================ Error codes for activation status ======================
//=====================================================================================
const int CONN_STATUS_OK = 1;				// Success

const int CONN_STATUS_NO_SOURCE_DP = -1;						// Source DP is not found - from DLL
const int CONN_STATUS_BAD_SOURCE_MOBILE_TYPE = -2;	// Source DP is not mobile container - from DLL
const int CONN_STATUS_NO_SOURCE_PLC_NAME = -3;			// Name of PLC is not known for source DP - from DLL
const int CONN_STATUS_NO_SOURCE_PLC = -4;						// Configuration for source PLC is not found - from DLL
const int CONN_STATUS_BAD_POSITION = -5;						// Bad location/sector index - from DLL
const int CONN_STATUS_UNKNOWN_SECTOR = -6;					// Sector with given index is not found - from DLL
const int CONN_STATUS_NO_TARGET_DP = -10;						// DP for target of required DP type is not found - from DLL

const int CONN_STATUS_BAD_MOBILE_TYPE = -20;				// Unsupported mobile type for target equipment
const int CONN_STATUS_BAD_EQP_TYPE = -21;						// Unknown equipment type reported by PLC
const int CONN_STATUS_TARGET_DP_NOT_EXIST = -22;		// Target DP does not exist in PVSS
const int CONN_STATUS_NO_DP_OF_TARGET_TYPE = -23;		// There are no DPs of target DP type at all
const int CONN_STATUS_NO_WORK_DP = -24;							// Failed to find work DP for target DP

const int CONN_STATUS_NO_TARGET_ADDRESS = -30;			// Periphery address for target DP is not found
const int CONN_STATUS_SET_SOURCE_REF = -31;					// Failed to set in source DP reference to target DP
const int CONN_STATUS_NO_DATA_FOR_TARGET = -32;			// No data for target DPE after setting address
const int CONN_STATUS_BAD_ADDRESS_TYPE = -33;				// Unknown address type for target DP
const int CONN_STATUS_SET_TARGET_ADDRESS = -34;			// Failed to set periphery address for target DP
const int CONN_STATUS_BAD_OPC_TRANS_TYPE = -35;			// Unknown transfer type for OPC address
const int CONN_STATUS_BAD_S7_TRANS_TYPE = -36;			// Unknown transfer type for S7 address
const int CONN_STATUS_MOBILE_SLAVE_ERROR = -37;  // PLC master reports mobile slave is in error(V2.1) 

// What shall be done with previously activate DP
const int OLD_CONNECT_ACTION_NONE = 0;						// Nothing
const int OLD_CONNECT_ACTION_REMOVE_AND_SET = 1;	// Deactivate old and activate new
const int OLD_CONNECT_ACTION_SET = 2;							// Activate

// List of all active mobile equipment DPs
dyn_string			mobileDpList;

// List of source DP names for all active mobile equipment DPs
dyn_string			mobileSourceList;

// List of active flags for all active mobile equipment DPs
dyn_bool				mobileStateList;

// List of working DP names for all mobile DPs
dyn_string			workDpList;

// Flag indicating if initalization is still in progress
bool						isInitializing;

// Completion code for DP activation
int							connStatus;

// Queue of data to be processed
dyn_dyn_anytype	queue;

// The callbacks from dpConnect() may arrive not in the order
// in which dpConnect() calls were issued. But we shall know
// when all of VMOB DPs were processed (== initialization is over)
// So we build list of all VMOB DPs to connect and another list
// of the same size containing number of callbacks for every DP
// in first list - originally 0.

// Number of VMOB DPs to be connected
int							nVmobDps;

// List of VMOB DPs to which we shall connect
dyn_string			vmobDps;

// List of callback counters for every VMOB DP
dyn_int					vmobCbCounters;

// Flag indicating if mobile list DP has been changed
bool						isMobileListChanged;

// Flag indicating if new config file for LHC logging shall be written
bool						isNewConfig;

// Flag to enable debugging output
const bool			isMobileDebug = true;

// Queue of failed deactivate requests. Every item in list
// contains 2 strings:
//	1) Name of source VMOB DP
//	2) Name of DP to deactivate
dyn_dyn_string	deactivateQueue;

// List of source DP names which are being activated
dyn_string	sourceDpsToActivate;

// List of DP types for every source DP to activate
dyn_string	dpTypesToActivate;

// List of work DPs for every source DP to activate
dyn_string	workDpsToActivate;

// main
/**
Purpose:
This is the main of the script.

Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. no temporary data point, no ActiveX
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
main()
{
  dyn_string		exceptionInfo;
  dyn_errClass	err;
  int						n;

  DebugTN("vcsMobileEqp.ctl: Starting");
  if(! InitStaticData()) {
    return;
  }
  // Get list of all available mobile equipment container DPs
  vmobDps = dpNames("*", MOBILE_CONTAINER_DP_TYPE);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    DebugTN("vcsMobileEqp.ctl: FATAL: dpNames(\"*\", " + MOBILE_CONTAINER_DP_TYPE + ") failed:");
    throwError(err);
    return;
  }
  // Check if DP with active mobile equipment parameters exists, if not - create such DP
  if(! dpExists(VAC_MOBILE_LIST_DP))
  {
    dpCreate(VAC_MOBILE_LIST_DP, VAC_MOBILE_LIST_DP_TYPE);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      DebugTN("vcsMobileEqp.ctl: FATAL: dpCreate(" + VAC_MOBILE_LIST_DP + ", " + VAC_MOBILE_LIST_DP_TYPE + ") failed:");
      throwError(err);
      return;
    }
    if(! dpExists(VAC_MOBILE_LIST_DP))
    {
      DebugTN("vcsMobileEqp.ctl: FATAL: DP <" + VAC_MOBILE_LIST_DP + "> was not created");
      return;
    }
  }
  else { //Delete values of list mobile list (if some deconnections were not completed within previous script stop)  
    dyn_string emptyDynString;
    dyn_bool emptyDynBool;
    int rc;    
    rc = dpSetWait(VAC_MOBILE_LIST_DP + ".DP_Names", emptyDynString,
                   VAC_MOBILE_LIST_DP + ".SourceDpNames", emptyDynString,
                   VAC_MOBILE_LIST_DP + ".State", emptyDynBool,
                   VAC_MOBILE_LIST_DP + ".WorkDPNames", emptyDynString);
    if( dynlen(getLastError()) > 0 ) {
      DebugTN("vcsMobileEqp.ctl: ERROR Reset " + VAC_MOBILE_LIST_DP + " dps, dpSetWait ret error = ", rc);     
    }  
  }
  DeactivateGhostDps();
  
  /* Old code   
  else // DP exists - get current parameters of active mobile equipment DPs
  {
    dpGet(VAC_MOBILE_LIST_DP + ".DP_Names", mobileDpList,
      VAC_MOBILE_LIST_DP + ".SourceDpNames", mobileSourceList,
      VAC_MOBILE_LIST_DP + ".State", mobileStateList,
      VAC_MOBILE_LIST_DP + ".WorkDPNames", workDpList);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      DebugTN("vcsMobileEqp.ctl: FATAL: failed to get active active mobile list:");
      throwError(err);
      return;
    }
  }
  */

  // Connect to all mobile equipment containers
  if(dynlen(vmobDps) == 0)
  {
    DebugTN("vcsMobileEqp.ctl: FATAL: no <" + MOBILE_CONTAINER_DP_TYPE + "> in the system, exiting...");
    return;
  }
  isInitializing = true;
  isMobileListChanged = isNewConfig = false;
  nVmobDps = dynlen(vmobDps);
  DebugTN("vcsMobileEqp.ctl: Connecting to " + nVmobDps + " VMOB DPs");
  for(n = 1 ; n <= nVmobDps ; n++)
  {
    string dpName = dpSubStr(vmobDps[n], DPSUB_DP);
    if(isMobileDebug)
    {
      DebugTN("vcsMobileEqp.ctl: Connecting to # " + n);
    }
    dpConnect("ContainerCb",
      vmobDps[n] + ".TYPE:_original.._value",
      vmobDps[n] + ".POS:_original.._value",
      vmobDps[n] + ".IDX:_original.._value");
  }
  // Wait forever, process data from queue if queue is not empty
  while(true)
  {
    if(dynlen(queue) > 0)
    {
      DebugTN("vcsMobileEqp.ctl: processing " + dynlen(queue) + " events from queue");
    }
    while(dynlen(queue) > 0)
    {
      if(isMobileDebug)
      {
        DebugTN("vcsMobileEqp.ctl: queue length = " + dynlen(queue));
      }
      ProcessCallbackData(queue[1][1], queue[1][2], queue[1][3], queue[1][4]);
      dynRemove(queue, 1);
    }
    if(isInitializing)
    {
      isInitializing = false;
      PublishMobileList();
      // Commented out 31.03.2011 L.Kopylov
      /*
      WriteLhcLoggingConfig();
      */
      DebugTN("vcsMobileEqp.ctl: Initialization is over");
    }
    else if(isNewConfig)
    {
      // Commented out 31.03.2011 L.Kopylov
      /*
      WriteLhcLoggingConfig();
      */
    }
    delay(1);
  }
}
// ContainerCb
/**
Purpose:
Callback for new values of mobile container DP

Parameters:
	- typeDpe, string, input, DPE name of TYPE DPE
	- type, unsigned, input, value of TYPE DPE
	- posDpe, unsigned, input, DPE name of POS DPE
	- pos, unsigned, input, value of POS DPE
	- idxDpe, unsigned, input, DPE name of IDX DPE
	- idx, unsigned, input, value of IDX DPE

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void ContainerCb(string typeDpe, unsigned type, string posDpe, unsigned pos, string idxDpe, unsigned idx)
{
  dynAppend(queue, makeDynAnytype((string)dpSubStr(typeDpe, DPSUB_DP),
    (unsigned)type, (unsigned)pos, (unsigned)idx));
}
// ProcessCallbackData
/**
Purpose:
Process new data from callback for new values of mobile container DP

Parameters:
	- typeDpe, string, input, DPE name of TYPE DPE
	- type, unsigned, input, value of TYPE DPE
	- pos, unsigned, input, value of POS DPE
	- idx, unsigned, input, value of IDX DPE

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void ProcessCallbackData(string typeDpe, unsigned type, unsigned pos, unsigned idx)
{
  unsigned		plcConnectState, plcEqpType;
  int					requiredAction, mobileType;
  string			sourceDp, targetDpType;

  // State consists of two bytes:
  // 0) Connect state given by PLC
  // 1) Equipment type given by PLC
  plcConnectState = (type & 0xFF00u) >> 8;
  plcEqpType = (type & 0x00FFu);
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: ++++++++++ ProcessCallbackData()",
      dpSubStr(typeDpe, DPSUB_DP_EL), type, pos, idx);
  }

  // Check if target DP shall be activated, find DP type and mobile type of target DP
  connStatus = CONN_STATUS_OK;
  sourceDp = dpSubStr(typeDpe, DPSUB_DP);
  DecideForMobileConnect(sourceDp, plcConnectState, plcEqpType, requiredAction, mobileType, targetDpType);
  if(connStatus < 0)
  {
    NotifyPlc(sourceDp);
    if(isMobileDebug)
    {
      DebugTN("vcsMobileEqp.ctl: ------ ProcessCallbackData() finish on error",
        dpSubStr(typeDpe, DPSUB_DP_EL), connStatus);
    }
    return;
  }
  else if(isMobileDebug)
  {
    string actionName = requiredAction;
    switch(requiredAction)
    {
    case MOBILE_ACTION_NONE:
      actionName += ": NONE";
      break;
    case MOBILE_ACTION_DEACTIVATE:
      actionName += ": DEACTIVATE";
      break;
    case MOBILE_ACTION_ACTIVATE:
      actionName += ": ACTIVATE";
      break;
    case MOBILE_ACTION_OLD_ACTIVATION:
      actionName += ": OLD_ACTIVATION";
      break;
    }
    DebugTN("vcsMobileEqp.ctl: requiredAction = " + actionName);
  }

  // Execute required action
  bool	needReactivate;
  string	targetDp;
  switch(requiredAction)
  {
  case MOBILE_ACTION_NONE:
    break;
  case MOBILE_ACTION_DEACTIVATE:
    DeactivateConnection(sourceDp);
    break;
  case MOBILE_ACTION_ACTIVATE:
    GetTargetDpName(sourceDp, targetDpType, mobileType, pos, idx, targetDp);
    ActivateConnection(sourceDp, mobileType, targetDpType, targetDp, pos, idx);
    break;
  case MOBILE_ACTION_OLD_ACTIVATION:
    GetTargetDpName(sourceDp, targetDpType, mobileType, pos, idx, targetDp);
    CheckOldActivation(sourceDp, mobileType, targetDpType, targetDp, needReactivate);
    if(needReactivate)
    {
      ActivateConnection(sourceDp, mobileType, targetDpType, targetDp, pos, idx);
    }
    break;
  }
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: ------ ProcessCallbackData() finish",
      dpSubStr(typeDpe, DPSUB_DP_EL));
  }
}
// CheckOldActivation
/**
Purpose:
Check if DP(s) previously activated still exist. It looks like this
can be the only exception we can check and report.
Nothing useful can be done automatically - just generate error message

Parameters:
	- sourceDp, input, Name fof source VMOB DP for which target shall be checked
	- mobileType, input, mobile type for activated DP
	- targetDpType, input, DP type of target DP previously activated 
	- targetDp, input, Name of target DP
	- needReactivate, output, Flag indicating if old connection shall be reactivated

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void CheckOldActivation(string sourceDp, int mobileType, string targetDpType,
  string targetDp, bool &needReactivate)
{
  int					idxInList;
  dyn_string	exceptionInfo;
  bool				foundActive = false;

  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: CheckOldActivation(" + sourceDp + "," + targetDpType + ") - start");
  }
  needReactivate = false;
  CleanMobileList();
  for(idxInList = dynlen(mobileSourceList) ; idxInList > 0 ; idxInList --)
  {
    if(mobileSourceList[idxInList] != sourceDp)
    {
      continue;
    }
    if(isMobileDebug)
    {
      DebugTN("lhcVacEqpMobile.ctl: source found in workDpList", idxInList,
        workDpList[idxInList], mobileStateList[idxInList]);
    }
    if(mobileStateList[idxInList])	// Target DP is active
    {
      foundActive = true;
      if(! dpExists(workDpList[idxInList]))
      {
        ShowMessage("Work DP <" + workDpList[idxInList] + "> for source DP <" + sourceDp + "> does not exist",
          true);
        mobileStateList[idxInList] = false;	// Not active anymore
        isMobileListChanged = true;
      }
      else if(mobileDpList[idxInList] != targetDp)	// Another target DP name
      {
        if(isMobileDebug)
        {
          DebugTN("Another target DP: need " + targetDp + " used " + workDpList[idxInList]);
        }
        needReactivate = true;
      }
      else if(targetDpType != dpTypeName(workDpList[idxInList]))	// Another target DP type
      {
        if(isMobileDebug)
        {
          DebugTN("Another target DP type: need " + targetDpType + " used " +
            dpTypeName(workDpList[idxInList]));
        }
        needReactivate = true;
      }
    }
    // TODO: Why is it needed here ? LIK 15.03.2007 PublishMobileList();
  }

  if(! foundActive)
  {
    ShowMessage("Activated DP of type <" + targetDpType + "> for source DP <" + sourceDp + "> is not in mobile list",
      true);
    needReactivate = true;
  }
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: CheckOldActivation(" + sourceDp + "," + targetDpType + ") - finish", needReactivate);
  }
}
// DeactivateConnection
/**
Purpose:
Deactivate previously activated connection for given source VMOB DP

Parameters:
	- sourceDp, input, Name fof source VMOB DP for which target shall be deactivated 

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void DeactivateConnection(string sourceDp)
{
  int					idxInList;
  dyn_string	exceptionInfo;
  bool				publishIsNeeded = false;

  /* PLC does not need notification in case of disconnect LIK 04.06.2007
  NotifyPlc(sourceDp);
  */
  CleanMobileList();
  for(idxInList = dynlen(mobileSourceList) ; idxInList > 0 ; idxInList --)
  {
    if(mobileSourceList[idxInList] != sourceDp)
    {
      continue;
    }
    if(mobileStateList[idxInList])	// Target DP is active, deactivate
    {
      DeactivateTargetDp(sourceDp, workDpList[idxInList], exceptionInfo);
      mobileStateList[idxInList] = false;	// Not active anymore
      isMobileListChanged = true;
      if(dynlen(exceptionInfo) > 0)
      {
        ShowMessage(exceptionInfo[2], true);
        dyn_string	pendingTask = makeDynString(sourceDp, workDpList[idxInList]);
        dynAppend(deactivateQueue, pendingTask);
      }
      else
      {
        ShowMessage("Mobile equipment <" +
          mobileDpList[idxInList] + "> disconnected on <" + sourceDp + ">", true);
      }
    }
    publishIsNeeded = true;
  }
  if(publishIsNeeded)
  {
    PublishMobileList();	// TODO: why inside CYCLE ????
  }
}
// ActivateConnection
/**
Purpose:
Activate connection for given source VMOB DP

Parameters:
	- sourceDp, input, Name fof source VMOB DP for which target shall be activated
	- mobileType, input, Type of mobile equipment (on flanges or on sectors)
	- targetDpType, input, Name of target DP type to be activated
	- targetDp, input, Name of target DP
	- pos, unsigned, input, value of POS DPE of source VMOB DP
	- idx, unsigned, input, value of IDX DPE of source VMOB DP

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void ActivateConnection(string sourceDp, int mobileType, string targetDpType,
	string targetDp, unsigned pos, unsigned idx)
{
  dyn_string	exceptionInfo;
  int					idxInList;

  if(targetDp == "")
  {
    return;
  }

  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: ActivateConnection(" + sourceDp + "," + mobileType + "," + targetDpType + "," +
            targetDp + "," + pos + "," + idx + ")");
  }

  // Target DP has been found, deactivate any other DP(s) (if any) on the same source DP
  CleanMobileList();
  for(idxInList = dynlen(mobileSourceList) ; idxInList > 0 ; idxInList--)
  {
    if(mobileSourceList[idxInList] != sourceDp)
    {
      continue;
    }
    if(mobileDpList[idxInList] == targetDp)
    {
      continue;	// The same target DP
    }
    if(mobileStateList[idxInList])	// Other target DP on the same source DP, deactivate if necessary
    {
      DeactivateTargetDp(sourceDp, workDpList[idxInList], exceptionInfo);
      mobileStateList[idxInList] = false;	// Not active anymore
      if(dynlen(exceptionInfo) > 0)
      {
        ShowMessage(exceptionInfo[2], true);
        dyn_string	pendingTask = makeDynString(sourceDp, workDpList[idxInList]);
        dynAppend(deactivateQueue, pendingTask);
      }
      else
      {
        ShowMessage("Mobile equipment <" +
          mobileDpList[idxInList] + "> disconnected on <" + sourceDp + ">", true);
      }
    }
  }

  // Check if the same target DP is already activated on another source DP,
  // if yes - deactivate it on another source DP
  for(idxInList = dynlen(mobileDpList) ; idxInList > 0 ; idxInList--)
  {
    if(mobileDpList[idxInList] != targetDp)
    {
      continue;	// Another target DP
    }
    if(mobileSourceList[idxInList] == sourceDp)
    {
      continue;	// The same source
    }
    if(mobileStateList[idxInList])	// This target DP is activated on another source DP, deactivate if necessary
    {
      // Not always a 'real' warning, but may help in some cases. VTL #220
      DebugTN("vcsMobileEqp.ctl: WARNING: activating " + targetDp + " on " + sourceDp + ", but it is already activated on " + mobileSourceList[idxInList]);
      DeactivateTargetDp(mobileSourceList[idxInList], workDpList[idxInList], exceptionInfo);
      mobileStateList[idxInList] = false;	// Not active anymore
      if(dynlen(exceptionInfo) > 0)
      {
        ShowMessage(exceptionInfo[2], true);
        dyn_string	pendingTask = makeDynString(sourceDp, workDpList[idxInList]);
        dynAppend(deactivateQueue, pendingTask);
      }
      else
      {
        ShowMessage("Mobile equipment <" +
          mobileDpList[idxInList] + "> disconnected on <" + sourceDp + ">", true);
      }
    }
  }

  // Activate new target DP
  connStatus = CONN_STATUS_OK;
  string	workDp;
  ActivateTargetDp(sourceDp, targetDp, targetDpType, workDp, exceptionInfo);
  bool success = true;
  if(dynlen(exceptionInfo) > 0)
  {
    if(connStatus == CONN_STATUS_SET_TARGET_ADDRESS)	// Try to remove addresses from 'ghost' DP(s)
    {
      dynClear(exceptionInfo);
      DeactivateGhostDps();
      ActivateTargetDp(sourceDp, targetDp, targetDpType, workDp, exceptionInfo);
      if(dynlen(exceptionInfo) > 0)
      {
        ShowMessage(exceptionInfo[2], true);
        success = false;
      }
    }
    else
    {
      ShowMessage(exceptionInfo[2], true);
      success = false;
    }
  }
  if(success)
  {
    AddActiveToList(sourceDp, targetDp, workDp);
    PublishMobileList();
    ShowMessage("Mobile equipment <" + targetDp + "> connected on <" + sourceDp + ">", true);
  }
  NotifyPlc(sourceDp);
}
// InitStaticData
/**
Purpose:
Read all machine configuration data from file(s)

Parameters:
	- None

Return:
	- true in case of success, false in case of error. All errors are fatal

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool InitStaticData()
{
  dyn_string	exceptionInfo;
  long				coco;
  bool				isLhcData;

  // Set accelerator name to be used by other components
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsMobileEqp.ctl: Failed to read machine name: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsMobileEqp.ctl: Machine is " + glAccelerator);
  isLhcData = glAccelerator == "LHC" ? true : false;
  // Initalize passive machine data
  coco = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsMobileEqp.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsMobileEqp.ctl: LhcVacEqpInitPassive() done");

  // Initialzie active equipment information
  coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
    exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsMobileEqp.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsMobileEqp.ctl: LhcVacEqpInitActive() done");

  // Initialize mobile equipment addressing information
  coco = VacImportParseFile(PROJ_PATH + "/data/" + glAccelerator + "_Addresses.mobile", glAccelerator, true, exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsMobileEqp.ctl: VacImportParseFile() failed: " + exceptionInfo);
    return false;
  }
  //DebugTN("vcsMobileEqp.ctl: VacImportParseFile() done", coco, dynlen(exceptionInfo) + " errors");
  DebugTN("vcsMobileEqp.ctl: VacImportParseFile() done", coco, dynlen(exceptionInfo) + " errors", exceptionInfo);
  // Initalize mobile equipment LHC logging configuration
  // Commented out 31.03.2011 L.Kopylov
  /*
  coco = VacMobileLoggingReadConfig(PROJ_PATH + "/data/" + glAccelerator + "_logging_mobile.data", exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsMobileEqp.ctl: VacMobileLoggingReadConfig() failed: ", exceptionInfo);
    return false;
  }
  DebugTN("VacImportParseFile.ctl: VacMobileLoggingReadConfig() done", coco, dynlen(exceptionInfo) + " errors");
  */

  DebugTN("vcsMobileEqp.ctl: InitStaticData() done");
  return true;
}
// DecideForMobileConnect
/**
Purpose:
Decide which equipment type is connected on mobile equipment container
In fact this is a kernel of decision containing HARDCODED conversion of
equipment type coming from PLC to DP type in PVSS.

Parameters:
	- sourceDpName, string, input, mobile container DP name
	- state, unsigned, input, connection state as supplied by PLC
	- type, unsigned, input, equipment type as supplied by PLC
	- requiredAction, output, enumeration for action to be done
	- mobileType, int, output, mobile type for device to be connected
	- targetDpType, string, output, DP type for target DP to be activated, empty string is no DP shall be activated

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void DecideForMobileConnect(string sourceDpName, unsigned state, unsigned type, int &requiredAction, int &mobileType, string &targetDpType)
{
  string			message;
  dyn_string	exceptionInfo;

  targetDpType = "";
  requiredAction = MOBILE_ACTION_NONE;
  switch(state)
  {
  case VAC_MOBILE_PLC_CONNECT_READY_TO_PVSS:
    break;
  case VAC_MOBILE_PLC_CONNECT_ACTIVATE_OK:
    // LIK 04.06.2007 if(! isInitializing) return;	// Any possible inconsistencies were handled by init
    requiredAction = MOBILE_ACTION_OLD_ACTIVATION;
    break;
  default:
    // Send message for debugging
    if((! isInitializing) && isMobileDebug)
    {
      ShowMessage(sourceDpName + ": Dev_Status = " + state, true);
    }
    // Analyze state: it can be either one of states for successfull data transfer, or
    // one of error states when connection to mobile equipment has been lost
    if((VAC_MOBILE_PLC_CONNECT_DATA_TRANSFER_START <= state) && (state <= VAC_MOBILE_PLC_CONNECT_DATA_TRANSFER_END))
    {
      // LIK 04.06.2007 if(! isInitializing) return;	// Any possible inconsistencies were handled by init
      requiredAction = MOBILE_ACTION_OLD_ACTIVATION;
    }
    else
    {
      requiredAction = MOBILE_ACTION_DEACTIVATE;
      return;
    }
    break;
  }

  switch(type)
  {
  case VAC_MOBILE_EQP_TYPE_VPGMA:
    targetDpType = "VPGMA";
    mobileType = VAC_MOBILE_TYPE_ON_FLANGE;
    break;
  case VAC_MOBILE_EQP_TYPE_VPGMB:
    targetDpType = "VPGMB";
    mobileType = VAC_MOBILE_TYPE_ON_FLANGE;
    break;
  case VAC_MOBILE_EQP_TYPE_VPGMC:
    targetDpType = "VPGMC";
    mobileType = VAC_MOBILE_TYPE_ON_FLANGE;
    break;
  case VAC_MOBILE_EQP_TYPE_VPGMD:
    targetDpType = "VPGMD";
    mobileType = VAC_MOBILE_TYPE_ON_FLANGE;
    break;
  case VAC_MOBILE_EQP_TYPE_BAKEOUT_RACK:
    targetDpType = "VBAKEOUT";
    mobileType = VAC_MOBILE_TYPE_ON_SECTOR;
    break;
  default:
    message = "vcsMobileEqp.ctl: Mobile connection: ERROR: unsupported equipment type " + type +
      " on DP <" + sourceDpName + ">";
    ShowMessage(message, true);
    connStatus = CONN_STATUS_BAD_EQP_TYPE;
    message = "vcsVacMobileEqp.ctl: Mobile unvalidate request, PLC send : ERROR equipment type " + type +
      " on DP <" + sourceDpName + ">";
    ShowMessage(message, true);
    UnvalidateMobile(sourceDpName, exceptionInfo);  // V2.1 unvalidate Device if ERROR type send by PLC
    return;	// No further processing possible
    break;
  }
  if(requiredAction == MOBILE_ACTION_NONE)
  {
    requiredAction = MOBILE_ACTION_ACTIVATE;
  }
}
// CleanMobileList
/**
Purpose:
Remove from lists of mobile DPEs all items which are not active.
This is done in order to prevent infinite growing of list

Parameters:
	- None

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void CleanMobileList()
{
  int	n;
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: Clean Mobile List request");
  }
  if(isInitializing)
  {
    if(isMobileDebug)
    {
      DebugTN("vcsMobileEqp.ctl: Clean Mobile List request during init: nothing done");
    }
    return;
  }
  for(n = dynlen(mobileSourceList) ; n > 0 ; n--)
  {
    if(! mobileStateList[n])
    {
      dynRemove(mobileSourceList, n);
      dynRemove(mobileDpList, n);
      dynRemove(mobileStateList, n);
      dynRemove(workDpList, n);
      isMobileListChanged = true;
    }
    if(isMobileDebug)
    {
      DebugTN("vcsMobileEqp.ctl: Mobile List cleaned");
    }
  }
}
// AddActiveToList
/**
Purpose:
Add active target DP for given source DP to the list

Parameters:
	- sourceDp, string, input, Name of source DP for which target DP became acctive
	- targetDp, string, input, name of activated target DP
	- workDp, string, input, Name of wrok DP which will process data for target DP

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void AddActiveToList(string sourceDp, string targetDp, string workDp)
{
  int	idxInList;
  if((idxInList = dynContains(mobileDpList, targetDp)) <= 0)
  {
    dynAppend(mobileSourceList, sourceDp);
    dynAppend(mobileDpList, targetDp);
    dynAppend(mobileStateList, true);
    dynAppend(workDpList, workDp);
    isMobileListChanged = true;
  }
  else
  {
    if(((mobileSourceList[idxInList] != sourceDp) ||
      (workDpList[idxInList] != workDp) ||
      (! mobileStateList[idxInList])))
    {
      mobileSourceList[idxInList] = sourceDp;
      workDpList[idxInList] = workDp;
      mobileStateList[idxInList] = true;
      isMobileListChanged = true;
    }
  }
  if(isMobileDebug)
  {
    DebugTN("AddActiveToList: result is", sourceDp, targetDp, mobileSourceList, mobileDpList, mobileStateList, workDpList);
  }
}
// PublishMobileList
/**
Purpose:
Write resulting lists of all mobile equipment together with activity flags
to DPE in order to notify all clients about changes in mobile equipment content.

Parameters:
	- None

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void PublishMobileList()
{
  dyn_errClass	err;

  if(isInitializing)
  {
    return;
  }
  if(! isMobileListChanged)
  {
    return;
  }
  isNewConfig = true;
  // LIK 10.06.2007 WriteLhcLoggingConfig();
  dpSet(VAC_MOBILE_LIST_DP + ".DP_Names", mobileDpList,
    VAC_MOBILE_LIST_DP + ".SourceDpNames", mobileSourceList,
    VAC_MOBILE_LIST_DP + ".State", mobileStateList,
    VAC_MOBILE_LIST_DP + ".WorkDPNames", workDpList);
   err = getLastError();
  if(dynlen(err) > 0)
  {
    DebugTN("vcsMobileEqp.ctl: ERROR: failed to publish mobile equipment list:");
    DebugTN(err);
  }
  isMobileListChanged = false;
}












// UnvalidateTargetDp V2.1
/**
Purpose:
Remove address configs from all DPEs of target DP

Parameters:
	- sourceDp, string, input, Name of source DP - mobile container
	- exceptionInfo, dyn_string, output, Error information will be returned here

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.15
	. operating system: Windows and Linux.
	. distributed system: yes.
*/
void UnvalidateMobile(string sourceDp, dyn_string &exceptionInfo) {

  int					idxInList;
  dyn_string	exceptionInfo;
  string targetDp;
  bool				publishIsNeeded = false;

  CleanMobileList();
  for(idxInList = dynlen(mobileSourceList) ; idxInList > 0 ; idxInList --)
  {
    if(mobileSourceList[idxInList] != sourceDp)
    {
      continue;
    }
    if(mobileStateList[idxInList])	// Target DP 
    {
      UnvalidateTargetDp(sourceDp, workDpList[idxInList], exceptionInfo);
      mobileStateList[idxInList] = false;	// Not active anymore
      isMobileListChanged = true;
      if(dynlen(exceptionInfo) > 0)
      {
        ShowMessage(exceptionInfo[2], true);
        dyn_string	pendingTask = makeDynString(sourceDp, workDpList[idxInList]);
        dynAppend(deactivateQueue, pendingTask);
      }
      else
      {
        ShowMessage("Mobile equipment <" +
          mobileDpList[idxInList] + "> disconnected on <" + sourceDp + ">", true);
      }
    }
  }    
}


// UnvalidateTargetDp V2.1
/**
Purpose:
Reset RR1 of Target DP

Parameters:
	- sourceDp, string, input, Name of source DP - mobile container
	- targetDp, string, input, Name of target DP to be unvalidate
	- exceptionInfo, dyn_string, output, Error information will be returned here

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.15
	. operating system: Windows and Linux.
	. distributed system: yes.
*/
void UnvalidateTargetDp(string sourceDp, string targetDp, dyn_string &exceptionInfo)
{
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: UnvalidateTargetDp(" + targetDp + ") start");
  }
  // unvalidate RR1 of target DP 
  dpSet(targetDp + ".RR1", "8388608"); //Unvalidate and ERROR BIT 10000 0000 0000 0000 0000 0000
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    DebugTN("vcsVacMobileEqp.ctl: ERROR: dpSet(" + targetDp + ".RR1) failed:");
    DebugTN(err);
    fwException_raise(exceptionInfo, "ERROR", "TargetDp: dpSet(" + targetDp +
      ".RR1:_original.._value) failed:" + StdErrorText(err), "");
  }
  else
    DebugTN("vcsVacMobileEqp.ctl: dpSet(" + targetDp + ".RR1) successfull: Mobile unvalidated");
}














// DeactivateTargetDp
/**
Purpose:
Remove address configs from all DPEs of target DP

Parameters:
	- sourceDp, string, input, Name of source DP - mobile container
	- targetDp, string, input, Name of target DP to be deactivated
	- exceptionInfo, dyn_string, output, Error information will be returned here

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void DeactivateTargetDp(string sourceDp, string targetDp, dyn_string &exceptionInfo)
{
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: DeactivateTargetDp(" + targetDp + ") start");
  }
  // First set ACT_DP of source DP to empty string
  if(sourceDp != "")
  {
    dpSet(sourceDp + ".ACT_DP:_original.._value", "");
    dyn_errClass err = getLastError();
    if(dynlen(err) > 0)
    {
      DebugTN("vcsMobileEqp.ctl: ERROR: dpSet(" + sourceDp + ".ACT_DP:_original.._value) failed:");
      DebugTN(err);
      fwException_raise(exceptionInfo, "ERROR", "DeactivateTargetDp: dpSet(" + sourceDp +
        ".ACT_DP:_original.._value) failed:" + StdErrorText(err), "");
    }
  }

  // Check if target DP exists, if not - everything was done
  if(dpExists(targetDp))
  {
    // Remove all addresses even if address information is not available in DLL.
    // This can be done by reading DP type structure.
    dyn_string	allDpeNames;
    dyn_int allDpeTypes;
    BuildDpeListForDp(targetDp, allDpeNames, allDpeTypes, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return;
    }
    dyn_string	localExceptionInfo;
    for(int n = dynlen(allDpeNames) ; n > 0 ; n--)
    {
      dynClear(localExceptionInfo);
      /* Instead of deleting address - set it to not active - IT/CO claims that
      ** reuse of not active address does not increase size of PVSS DB files
      ** LIK 22.02.2008
      fwPeriphAddress_delete(targetDp + "." + allDpeNames[n], localExceptionInfo);
      if(dynlen(localExceptionInfo) > 0)
      {
        DebugTN("vcsMobileEqp.ctl: ERROR: fwPeriphAddress_delete(" + allDpeNames[n] + ") failed:");
        DebugTN(localExceptionInfo);
        dynAppend(exceptionInfo, localExceptionInfo);
      }
      */
      int configType = DPCONFIG_NONE;
      dpGet(targetDp + "." + allDpeNames[n] + ":_distrib.._type", configType);
      if(configType != DPCONFIG_NONE)
      {
        if(dpSetWait(targetDp + "." + allDpeNames[n] + ":_address.._active", false) < 0)
        {
          DebugTN("vcsMobileEqp.ctl: ERROR DEACTIVATE: dpSetWait(" + targetDp + "." + allDpeNames[n] + ":_address.._active) failed:");
        }
      }

      switch(allDpeTypes[n])
      {
      case DPEL_STRING:
      case DPEL_LANGSTRING:
        dpSetWait(targetDp + "." + allDpeNames[n], "");
        break;
      default:
        dpSetWait(targetDp + "." + allDpeNames[n], -1);
        break;
      }
    }
  }
  else	// Target DP does not exist
  {
    DebugTN("vcsMobileEqp.ctl: INFO: DP to deactivate <" + targetDp + "> does not exist");
  }

  // Deactivate any master DP of this DP
  string	masterDpName;
  int	masterChannel;
  LhcVacEqpMaster(targetDp, masterDpName, masterChannel);
  if(masterDpName != "")
  {
    DeactivateTargetDp(sourceDp, masterDpName, exceptionInfo);
  }
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: DeactivateTargetDp(" + targetDp + ") finish");
  }
}
// ActivateTargetDp
/**
Purpose:
Set address configs for all DPEs of target DP which shall have address,
verify that all addresses were set successfully and new data are coming

Parameters:
	- sourceDp, string, input, Name of source DP - mobile container
	- targetDp, string, input, Name of target DP to be deactivated
	- targetDpType, string, input, Name of DP type for target DP
	- workDp, string, output, Name of real DP in PVSS which will work for target DP
	- exceptionInfo, dyn_string, output, Error information will be returned here

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void ActivateTargetDp(string sourceDp, string targetDp, string targetDpType, string &workDp, dyn_string &exceptionInfo)
{
  dyn_dyn_anytype	dpeToActivate;
  dyn_errClass		err;
  dyn_anytype			dpHead;
  int							dpeIdx, nDpes, masterChannel;
  dyn_string			localExceptionInfo, dpeToDisconnect;
  string					dpe, masterDpName, masterDpType;
  time						startTime, newTime;
  int							waitCount;
  float						interval;

  // Get required address parameters
  if(VacImportGetDeviceDataByName(sourceDp, targetDpType, dpHead, dpeToActivate, exceptionInfo) < 0)
  {
    connStatus = CONN_STATUS_NO_TARGET_ADDRESS;
    return;
  }

  // Find work DP for target DP
  WorkDpForTarget(targetDp, targetDpType, workDp, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: ActivateTargetDp(): work DP for <" + targetDp + "> is <" + workDp + ">");
    DebugTN("vcsMobileEqp.ctl: ActivateTargetDp(): " + dynlen(dpeToActivate) + " DPEs to activate");
  }

  // Set address for all required DPEs
  nDpes = dynlen(dpeToActivate);
  int sourceDpIdx = dynContains(sourceDpsToActivate, dpSubStr(sourceDp, DPSUB_DP));
  if(sourceDpIdx > 0)
  {
    dpTypesToActivate[sourceDpIdx] = targetDpType;
    workDpsToActivate[sourceDpIdx] = workDp;
  }
  else
  {
    dynAppend(sourceDpsToActivate, dpSubStr(sourceDp, DPSUB_DP));
    dynAppend(dpTypesToActivate, targetDpType);
    dynAppend(workDpsToActivate, workDp);
  }
  int	coco = VacImportResetUpdateCounter(sourceDp, targetDpType);
  if(isMobileDebug)
  {
    DebugTN("VacImportResetUpdateCounter() = " + coco);
  }
  for(dpeIdx = nDpes ; dpeIdx > 0 ; dpeIdx--)
  {
    // Watch on activation
    if(dpeToActivate[dpeIdx][VAC_DPE_ADDRESS_DIRECTION] == 1)	// input DPE
    {
      dpe = workDp + "." + dpeToActivate[dpeIdx][VAC_DPE_NAME] + ":_original.._stime";
      dynAppend(dpeToDisconnect, dpe);	// Add DPE name for later disconnect calls
      dpConnect("DpeStimeCb", false, dpe);
    }
    if(isMobileDebug)
    {
      DebugTN("vcsMobileEqp.ctl: setting address for " + dpeToActivate[dpeIdx][VAC_DPE_NAME]);
    }
    VacSetAddress(workDp + "." + dpeToActivate[dpeIdx][VAC_DPE_NAME], dpeToActivate[dpeIdx], true,
      exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      ShowMessage("Activating " + sourceDp + ": failed to set address for " +
        workDp + "." + dpeToActivate[dpeIdx][VAC_DPE_NAME], true);
      connStatus = CONN_STATUS_SET_TARGET_ADDRESS;
      DeactivateTargetDp(sourceDp, workDp, targetDpType, localExceptionInfo);
      break;
    }
  }
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: ActivateTargetDp(): address set " +
      (dynlen(exceptionInfo) > 0 ? "FAILED" : "done"));
  }
  if(dynlen(exceptionInfo) > 0)	// Failed
  {
    // Disconnect from all stime's
    for(dpeIdx = dynlen(dpeToDisconnect) ; dpeIdx > 0 ; dpeIdx--)
    {
      dpDisconnect("DpeStimeCb", dpeToDisconnect[dpeIdx]);
    }
    return;
  }

  // Wait reasonable (???) time until new callback will arrive for all DPEs
  startTime = getCurrentTime();
  waitCount = 0;
  while(true)
  {
    waitCount++;
    // DebugTN("Waiting " + waitCount + " for " + targetDp + "." + dpeData[VAC_DPE_NAME] + ", count = " + callbackCount);
    coco = VacImportPendingDpeCount(sourceDp, targetDpType);
    if(isMobileDebug)
    {
      DebugTN("VacImportPendingDpeCount() = " + coco);
    }
    if(coco < 1)
    {
      DebugTN("Success after " + waitCount + " cycles");
      break;
    }
    newTime = getCurrentTime();
    interval = newTime - startTime;
    if(interval > 7.0) break;
    /* TODO: uncomment
    {
      fwException_raise(exceptionInfo, "ERROR", "ActivateTargetDp(): no new data for DPE <" +
        workDp + "." + dpeToActivate[dpeIdx][VAC_DPE_NAME] + "> after address set", "");
      connStatus = CONN_STATUS_NO_DATA_FOR_TARGET;
      break;
    }
    */
    delay(0, 200);
  }

  // Disconnect from all activated DPEs
  for(dpeIdx = dynlen(dpeToDisconnect) ; dpeIdx > 0 ; dpeIdx--)
  {
    dpDisconnect("DpeStimeCb", dpeToDisconnect[dpeIdx]);
  }
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: ActivateTargetDp(): wait for data - done");
  }

  // If activation of this DP was successfull - activate master DP (if any)
  LhcVacEqpMaster(workDp, masterDpName, masterChannel);
  if(masterDpName != "")
  {
    if(!dpExists(masterDpName))
    {
      fwException_raise(exceptionInfo, "ERROR", "ActivateTargetDp(): master DP <" + masterDpName +
        " for target DP <" + targetDp + ">  and work DP <" + workDp + "> does not exist", "");
      connStatus = CONN_STATUS_TARGET_DP_NOT_EXIST;
      return;
    }
    masterDpType = dpTypeName(masterDpName);
    string	masterWorkDp;
    ActivateTargetDp(sourceDp, masterDpName, masterDpType, masterWorkDp, exceptionInfo);
  }
  else
  {
    // Update source DP
    dpSet(sourceDp + ".ACT_DP:_original.._value", workDp);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      DebugTN("vcsMobileEqp.ctl: ERROR: dpSet(" + sourceDp + ".ACT_DP:_original.._value) failed:");
      DebugTN(err);
      fwException_raise(exceptionInfo, "ERROR", "ActivateTargetDp: dpSet(" + sourceDp +
        ".ACT_DP:_original.._value) failed:" + StdErrorText(err), "");
      DeactivateTargetDp(sourceDp, workDp, targetDpType, localExceptionInfo);
      connStatus = CONN_STATUS_SET_SOURCE_REF;
    }
  }
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: ActivateTargetDp(" + sourceDp + "," + targetDp + "," + workDp + ") done");
  }
}
// DpeStimeCb
/**
Purpose:
Callback for new timestamp of DPE with address to set - just count number of callbacks

Parameters:
	- dpe, string, input, DPE name
	- stime, time, input, _stime value

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void DpeStimeCb(string dpe, time stime)
{
  int	dpIdx = dynContains(workDpsToActivate, dpSubStr(dpe, DPSUB_DP));
  if(dpIdx < 1)
  {
    DebugTN("vcsMobileEqp.ctl: DpeStimeCb(): <" + dpSubStr(dpe, DPSUB_DP) +
      "> is not found in workDpsToActivate");
    return;
  }
  string pureDpe = dpSubStr(dpe, DPSUB_DP_EL);
  int dotPosition = strpos(pureDpe, ".");
  pureDpe = substr(pureDpe, dotPosition + 1);
  if(VacImportIncrementUpdateCounter(sourceDpsToActivate[dpIdx], dpTypesToActivate[dpIdx],
    pureDpe) < 0)
  {
    DebugTN("vcsMobileEqp.ctl: DpeStimeCb(): VacImportIncrementUpdateCounter() returned error for DP <" +
      sourceDpsToActivate[dpIdx] + "> + DP type <" + dpTypesToActivate[dpIdx] +
      "> + DPE <" + pureDpe + ">");
  }
}
// WorkDpForTarget
/**
Purpose:
Find name of work DP which will process data for target DP
If target DP exists - that DP will be used (VPGM approach)
If target DP does not exist - any free DP of required type will
be used (BAKEOUT approach)

Parameters:
	- targetDp, string, input, Name of target DP
	- targetDpType, string, input, Name of target DP type
	- workDp, string, output, Name of work DP will be returned here
	- exceptionInfo, dyn_string, output, Details of exception will be returned here

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void WorkDpForTarget(string targetDp, string targetDpType, string &workDp, dyn_string &exceptionInfo)
{
  if(dpExists(targetDp))
  {
    workDp = targetDp;
    return;
  }

  // It could be that work DP for this target DP is already in workDpList - let's try to find
  for(int n = dynlen(mobileDpList) ; n > 0 ; n --)
  {
    if(mobileDpList[n] != targetDp)
    {
      continue;
    }
    if(mobileStateList[n])
    {
      if((workDp = workDpList[n]) != "")
      {
        return;
      }
    }
    break;
  }

  // Find new work DP: any unused DP of the same DP type
  // Get all DPs of target DP type
  dyn_string	allDpNames = dpNames("*", targetDpType);
  if(dynlen(allDpNames) == 0)
  {
    connStatus = CONN_STATUS_NO_DP_OF_TARGET_TYPE;
    fwException_raise(exceptionInfo, "ERROR", "WorkDpForTarget: no DPs of type <" + targetDpType +
      "> in system","");
    return;
  }
  if(isMobileDebug)
  {
    DebugTN("DPs of type " + targetDpType + ": " + dynlen(allDpNames));
  }
  for(int n = dynlen(allDpNames) ; n > 0 ; n--)
  {
    workDp = dpSubStr(allDpNames[n], DPSUB_DP);
    if(isMobileDebug)
    {
      DebugTN("Checking <" + workDp + ">");
    }
    int idxInList = dynContains(workDpList, workDp);
    if(idxInList > 0)
    {
      if(! mobileStateList[idxInList])
      {
        return;
      }
    }
    else
    {
      return;
    }
  }
  // Free work DP was not found
  connStatus = CONN_STATUS_NO_WORK_DP;
  fwException_raise(exceptionInfo, "ERROR", "WorkDpForTarget: failed to find free DP of type <" + targetDpType +
    ">","");
}
// NotifyPlc
/**
Purpose:
Write connection status to DPE to be checked by PLC

Parameters:
	- sourceDp, string, input, name of source DP which is being activated

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void NotifyPlc(string sourceDp)
{
  dyn_errClass	err;
  string				dpe = sourceDp + ".CONN_STATUS:_original.._value";

  dpSet(dpe, (connStatus & 0xFF));
  err = getLastError();
  if(dynlen(err) > 0)
  {
    DebugTN("vcsMobileEqp.ctl: NotifyPlc(): dpSet(" + dpe + ") failed:");
    throwError(err);
  }
}
// WriteLhcLoggingConfig
/**
Purpose:
Write new configuration file for LHC logging

Parameters:
	- none

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void WriteLhcLoggingConfig()
{
  dyn_string	exceptionInfo;
  int					n;

  isNewConfig = false;
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: WriteLhcLoggingConfig() start");
  }
  VacMobileLoggingClean();
  for(n = dynlen(workDpList) ; n > 0 ; n--)
  {
    if(!mobileStateList[n])
    {
      continue;
    }
    AddDpLhcLoggingConfig(mobileDpList[n], workDpList[n]);
  }
  // Write to output file
  string	fileName;
  VacMobileLoggingNewFileName(fileName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsMobileEqp.ctl: ERROR: VacMobileLoggingNewFileName() failed:");
    DebugTN(exceptionInfo);
    return;
  }
  VacMobileLoggingWrite(fileName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsMobileEqp.ctl: ERROR: VacMobileLoggingWrite() failed:");
    DebugTN("vcsMobileEqp.ctl:", exceptionInfo);
  }
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: WriteLhcLoggingConfig() finish");
  }
}
// AddDpLhcLoggingConfig
/**
Purpose:
Add LHC logging configuration for one DP

Parameters:
	- dpName, string, input, Name of DP
	- workDpName, string, input, Name of work DP for this DP
Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void AddDpLhcLoggingConfig(string dpName, string workDpName)
{
  dyn_string	exceptionInfo;
  string	mainPart, sector1, sector2;
  bool		isBorder;
  int			vacType;

  string	dpType = dpTypeName(workDpName);

  LhcVacDeviceVacLocation(dpName, vacType, sector1, sector2, mainPart, isBorder, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsMobileEqp.ctl: ERROR: LhcVacDeviceVacLocation(" + mobileDpList[n] + ") failed:");
    DebugTN("vcsMobileEqp.ctl:", exceptionInfo);
    return;
  }
  string	eqpName;
  LhcVacDisplayName(dpName, eqpName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsMobileEqp.ctl: ERROR: LhcVacDisplayName(" + mobileDpList[n] + ") failed:");
    DebugTN("vcsMobileEqp.ctl:", exceptionInfo);
    return;
  }
  VacMobileLoggingAdd(dpType, workDpName, eqpName, mainPart, sector1);

  // If there is a parent of device - check also archiving of parent
  string	masterDpName;
  int			masterChannel;
  LhcVacEqpMaster(dpName, masterDpName, masterChannel);
  if(masterDpName != "")
  {
    AddDpLhcLoggingConfig(masterDpName, masterDpName);
  }
}
// BuildDpeListForDp
/**
Purpose:
Build full names of all DPEs of given DP. Every resulting DPE names contains the whole
path WITHOUT input DP name

Parameters:
	- dpName, string, input, Name of DP for which all DPE names are required
	- allDpeNames, dyn_string, output, List of all DPE names for given DP will be returned here
	- exceptionInfo, dyn_string, output, details of exception will be written here

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void BuildDpeListForDp(string dpName, dyn_string &allDpeNames, dyn_int &allDpeTypes, dyn_string &exceptionInfo)
{
  string dpType = dpTypeName(dpName);
  dyn_dyn_string	names;
  dyn_dyn_int			types;
  dyn_string			dpeTree, localExceptionInfo;

  dynClear(allDpeNames);
  dynClear(allDpeTypes);
  dynClear(exceptionInfo);

  // Get DP type structure for working DP
  if(dpTypeGet(dpType, names, types) < 0)
  {
    dyn_errClass err = getLastError();
    DebugTN("vcsMobileEqp.ctl: ERROR: dpTypeGet(" + dpType + ") failed: " + StdErrorText(err));
    DebugTN(err);
    fwException_raise(exceptionInfo, "ERROR", "BuildDpeListForDp(): dpTypeGet(" + dpType +
      ") failed for DP <" + dpName + ">","");
    return;
  }
  int dpeIdx, typeIdx, nDpes = dynlen(names);
  for(dpeIdx = 1 ; dpeIdx <= nDpes ; dpeIdx++)
  {
    typeIdx = dynlen(types[dpeIdx]);
    int type = types[dpeIdx][typeIdx];
    if((type == DPEL_STRUCT) || // Structure
       (type == DPEL_BIT32_STRUCT) || (type == DPEL_BLOB_STRUCT) || (type == DPEL_BOOL_STRUCT) ||
      (type == DPEL_CHAR_STRUCT) || (type == DPEL_DPID_STRUCT) || (type == DPEL_DYN_BIT32_STRUCT) ||
      (type == DPEL_DYN_BLOB_STRUCT) || (type == DPEL_DYN_BOOL_STRUCT) || (type == DPEL_DYN_CHAR_STRUCT) ||
      (type == DPEL_DYN_DPID_STRUCT) || (type == DPEL_DYN_FLOAT_STRUCT) || (type == DPEL_DYN_INT_STRUCT) ||
      (type == DPEL_DYN_LANGSTRING_STRUCT) || (type == DPEL_DYN_STRING_STRUCT) || (type == DPEL_DYN_TIME_STRUCT) ||
      (type == DPEL_DYN_UINT_STRUCT) || (type == DPEL_FLOAT_STRUCT) || (type == DPEL_INT_STRUCT) ||
      (type == DPEL_LANGSTRING_STRUCT) || (type == DPEL_STRING_STRUCT) || (type == DPEL_TIME_STRUCT) ||
      (type == DPEL_UINT_STRUCT))
   {
      if(dynlen(dpeTree) >= typeIdx)
      {
        dpeTree[typeIdx] = names[dpeIdx][typeIdx];
      }
      else
      {
        dynAppend(dpeTree, names[dpeIdx][typeIdx]);
      }
      continue; 
    }
    string dpeName = "";
    for(int n = 2 ; n < typeIdx ; n++)	// Start from 2 because 1 is DP type name
    {
      if(dpeName != "")
      {
        dpeName += ".";
      }
      dpeName += dpeTree[n];
    }
    if(dpeName != "")
    {
      dpeName += ".";
    }
    dpeName += names[dpeIdx][typeIdx];
    dynAppend(allDpeNames, dpeName);
    dynAppend(allDpeTypes, type);
  }
}
// DeactivateGhostDps
/**
Purpose:
Remove address config from mobile devices which have address config set
but do not appear in mobilde equipment list

Parameters:
	None

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void DeactivateGhostDps()
{
  DeactivateGhostDpsOfType("VPGMA");
  DeactivateGhostDpsOfType("VPGMB");  
  DeactivateGhostDpsOfType("VPGMC");
  DeactivateGhostDpsOfType("VPGMD");
  DeactivateGhostDpsOfType("VPGMPR");
  DeactivateGhostDpsOfType("VBAKEOUT");
}
// DeactivateGhostDpsOfType
/**
Purpose:
Remove address config from mobile devices of single DP type which have address config
set but do not appear in mobilde equipment list

Parameters:
	- dpType, input, Name of DP type that shall be checked

Return:
	- None

Usage: Internal

PVSS manager usage: CTRL

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void DeactivateGhostDpsOfType(string dpType)
{
  dyn_string	allDps = dpNames("*", dpType);
  DebugTN("vcsMobileEqp.ctl: DeactivateGhostDpsOfType(" + dpType + ") start");
  for(int n = dynlen(allDps) ; n > 0 ; n--)
  {
    int	mobileIdx = dynContains(workDpList, dpSubStr(allDps[n], DPSUB_DP));
    if(mobileIdx > 0)
    {
      if(mobileStateList[mobileIdx])
      {
        continue;
      }
    }
    dyn_string	allDpeNames, exceptionInfo;
    dyn_int allDpeTypes;
    dynClear(exceptionInfo);
    BuildDpeListForDp(allDps[n], allDpeNames, allDpeTypes, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      DebugTN("DeactivateGhostDpsOfType(" + dpType + "): " + allDps[n] + ": error:");
      DebugTN(exceptionInfo);
      dynClear(exceptionInfo);
      continue;
    }
    bool	toBeDeactivated = false;
    for(int i = dynlen(allDpeNames) ; i > 0 ; i--)
    {
      string	dpeName = allDps[n] + "." + allDpeNames[i];
      bool	configExists;
      dyn_anytype config;
      bool isActive;
      fwPeriphAddress_get(dpeName, configExists, config, isActive, exceptionInfo);
      dynClear(exceptionInfo);
      if(configExists && isActive)
      {
        toBeDeactivated = true;
        break;
      }
    }
    if(!toBeDeactivated)
    {
      continue;
    }
    DebugTN("vcsMobileEqp.ctl: Removing address config for ghost <" + allDps[n] + ">");
    DeactivateTargetDp("", allDps[n], exceptionInfo);
    dynClear(exceptionInfo);
  }
  DebugTN("vcsMobileEqp.ctl: DeactivateGhostDpsOfType(" + dpType + ") finish");
}

void GetTargetDpName(string sourceDp, string targetDpType, int mobileType, int pos,
	int idx, string &targetDp)
{
  dyn_string	exceptionInfo;

  targetDp = "";
  switch(mobileType)
  {
  case VAC_MOBILE_TYPE_ON_FLANGE:
    connStatus = LhcVacDpForMobileOnFlange(sourceDp, targetDpType, pos, targetDp, exceptionInfo);
    break;
  case VAC_MOBILE_TYPE_ON_SECTOR:
    connStatus = LhcVacDpForMobileOnSector(sourceDp, targetDpType, pos, idx, targetDp, exceptionInfo);
    break;
  default:
    fwException_raise(exceptionInfo, "ERROR", "ContainerCb: unsupported mobile type " + mobileType +
      " on DP <" + sourceDp + "> for DP type <" + targetDpType + ">","");
    connStatus = CONN_STATUS_BAD_MOBILE_TYPE;
    break;
  }
  if(isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: Target is <" + targetDp + "> of DP type <" + targetDpType + "> for source <" + sourceDp);
  }
  if(connStatus < 0)
  {
    ShowMessage(exceptionInfo[2], true);
    NotifyPlc(sourceDp);
  }
}

void ShowMessage(string text, bool writeToLog)
{
  dyn_string	exceptionInfo;
  unMessageText_send(getSystemName(), "1", "CmdHandler","user", "*", "INFO", text, exceptionInfo);
  if(writeToLog || isMobileDebug)
  {
    DebugTN("vcsMobileEqp.ctl: " + text);
  }
}


