#uses "CtrlPostMortem"
#uses "email.ctl"					// Load standard PVSS CTRL library

const int TEST_VERSION = 0;
const int PRODUCTION_VERSION = 1;

dyn_string sDPNamesB, sEquipmentNamesB ;
dyn_string sDPNamesR, sEquipmentNamesR ;
dyn_string sDP_to_publish, sEquipment_to_publish;
dyn_string sBeamDumpDps ;
dyn_string emailLines;
string emailSubject;
dyn_dyn_string emailBuffer;
int emailCount, emailCountLock, emailCountAux;
dyn_int emailSendStatus; //sent status: 1 to send, 2 sent, 0 empty

dyn_int time_offset;
bool bconfigExists;
dyn_anytype dynconfig;
bool bisActive;
dyn_string dynexceptionInfo;
int i_low_level_adr = 11;
int beamCallbackCount = 0;

string SarchiveName_save = "XPOC_DumpLine_VGP";
int iarchive_Type_save = 3;//15;
int ismoothType_save = 1;
float fdead_band_save = 0;
float ftime_Interval_save = 0.5;

//PM variables
string rdaServer = "";
string rdaDevice = "";
string pmSystem = "LBDS";
string pmClass = "VAC";
string pmSource, sIP;
const int i_Post_dump_Value = 14;
//select version 0- test version, 1 - production version. Test version does not publish to PM server, only sends email
const int swVersion = 1;  

main()
{
	configure_dps();
	string emailTemp;
	time emailTime;
	setQueryRDBDirect(FALSE);

	//------------------------------------------------------------------------------------------------------------------------------------
	//connect beam dump dps to callback
	sBeamDumpDps= makeDynString("XTH_DUMPED1_XTIM", "XTH_DUMPED2_XTIM");
    int iLen = dynlen(sBeamDumpDps);
	int iRes=0, ret;
    for(int i = 1; i <= iLen; i++)
    {
      iRes=dpConnect("BeamDumpedCB", false, sBeamDumpDps[i] + ".ValueInt", sBeamDumpDps[i] + ".ValueInt:_online.._stime");
	  if(iRes < 0)
	  {
		emailTime = getCurrentTime();
		emailTemp = "dpConnect failed for dp " + sBeamDumpDps[i] + " at time " + hour(emailTime) + ":"+ minute(emailTime) + ":" + second(emailTime);
		dynAppend(emailLines,emailTemp);
	  }
	  else
	  {
		emailTime = getCurrentTime();
		emailTemp = "dpConnect successful for dp " + sBeamDumpDps[i] + " at time " + hour(emailTime) + ":"+ minute(emailTime) + ":" + second(emailTime);
		dynAppend(emailLines,emailTemp);		  
	  }
    }
    
	DebugTN("vcsPMTrigger.ctl: started, waiting for a Dump trigger. Version is: " + swVersion);
	
	//email that dpConnect worked as predicted
	send_email("andre.rocha@cern.ch","andre.rocha@cern.ch", "XPOC Notification - dpConnect status", emailLines);
	send_email("andre.rocha@cern.ch","ivo.amador@cern.ch", "XPOC Notification - dpConnect status", emailLines); 	
	
	dynClear(emailLines);
	
	while(1)
	{
		for(int i=1;i<=dynlen(emailSendStatus);i++)
		{
			if(emailSendStatus[i] == 1)	
			{
				send_email("jorge.fraga@cern.ch","andre.rocha@cern.ch",emailBuffer[i*2-1], emailBuffer[i*2]);
				delay(0,10);
				send_email("jorge.fraga@cern.ch","jorge.fraga@cern.ch",emailBuffer[i*2-1], emailBuffer[i*2]);
				emailSendStatus[i] = 2;
			}
		}	
		
		for(int i=1; i<=dynlen(emailSendStatus);i++)
		{
			if(emailSendStatus[i] == 2)
			{
				emailBuffer[i*2-1]="";
				emailBuffer[i*2]="";
				emailSendStatus[i] = 0;
			}
		}
		delay(0,100);
	}
}

synchronized BeamDumpedCB(string sDpValueName, int value, string sDpTimeFieldName, time stime)
{
	int beamNumber;
	int iRes = 0, ret, doNotPublish = 0;
	string emailTemp;
	long lNanoLong;
	string pmfile;
	time timeD1, timeD2;
	
	time time_test = getCurrentTime();
	emailTemp = "Start time: " + hour(time_test) + ":"+ minute(time_test) + ":" + second(time_test) + "." + milliSecond(time_test);
	dynAppend(emailLines,emailTemp);
	
	iRes = dpGet(sBeamDumpDps[1]+ ".ValueInt:_online.._stime", timeD1);
    if (iRes == -1)  {DebugTN("vcsPMTrigger.ctl: Get time value Dump1 error "+  iRes );}
    iRes = dpGet(sBeamDumpDps[2]+ ".ValueInt:_online.._stime", timeD2);
    if (iRes == -1)  {DebugTN("vcsPMTrigger.ctl: Get time value Dump2 error "+  iRes );}
	
	if(timeD1 == timeD2)
	{
		if(beamCallbackCount > 0)
		{
			beamCallbackCount = 0;
			dynClear(emailLines);
			return;
		}
		else
		{
			sDP_to_publish = sDPNamesB;
			sEquipment_to_publish = sEquipmentNamesB;
			
			for(int i = 1; i<= dynlen(sDPNamesR);i++)
			{
				dynAppend(sDP_to_publish,sDPNamesR[i]);
				dynAppend(sEquipment_to_publish,sEquipmentNamesR[i]);
			}
			beamCallbackCount++;
			beamNumber=3;
		}
		
	}
	else
	{
		// select equipment names according to beam
		beamNumber = select_gauges(sDpValueName);
		
		if(beamNumber == 1)
		{
			sDP_to_publish = sDPNamesB;
			sEquipment_to_publish = sEquipmentNamesB;
		}
		else if(beamNumber == 2)
		{
			sDP_to_publish = sDPNamesR;
			sEquipment_to_publish = sEquipmentNamesR;
		}
		else
		{
			//DebugTN("vcsPMTrigger.ctl: could not verify origin of beamDump, exiting function");
			return;
		}
	}
	//guarantee that 2 seconds have passed
	while(getCurrentTime() < stime + 2)
	{
		//DebugTN("Waiting for pressure values up to two seconds post-dump");
		delay(0,50);
	}
	
	time time_test = getCurrentTime();
	emailTemp = "post wait time: " + hour(time_test) + ":"+ minute(time_test) + ":" + second(time_test) + "." + milliSecond(time_test);
	dynAppend(emailLines,emailTemp);
	//create time value array here
	dyn_time timeValues=makeDynTime(stime-180, stime-120, stime-60, stime-10, stime-9, stime-8, stime-7, stime-6,stime-5,stime-4,stime-3,stime-2,stime-1,stime+0,stime +1,stime  +2); 
	dyn_anytype pressureValues;
	dyn_time timeStamps;
	
	iRes=dpGet(sDpTimeFieldName, lNanoLong);
	unGenericDpFunctions_getHostName(getSystemName(), pmSource, sIP);
	
	emailTemp = "Report for vcsPMTrigger.ctl script BeamDumpedCB function. This email contains for each predefined gauge: pressure values, time of publishing of the pressure values and any error messages that occurred on runtime.\r\n";
	dynAppend(emailLines,emailTemp);
	//Obtain pressure history for each gauge
	for(int i = 1; i <= dynlen(sDP_to_publish); i++)
	{
		doNotPublish=0;
		emailTemp = "Gauge " + sDP_to_publish[i] + ":\n";
		dynAppend(emailLines,emailTemp);
		
		iRes=getPressureHistory(sDP_to_publish[i],timeValues, pressureValues, timeStamps);
		if(iRes < 0 || (dynlen(pressureValues) != dynlen(timeValues)))
		{
			//DebugTN("vcsPMTrigger.ctl: error in getPressureHistory function for gauge: " + sDP_to_publish[i]);
			doNotPublish = 1;
		}

		//Add values to be sent by email to VSC/ICM dev 
		emailTemp="";		
		for(int j = 1; j<=dynlen(pressureValues);j++)
		{
			emailTemp += "\tt=" + hour(timeStamps[j]) + ":"+ minute(timeStamps[j]) + ":" + second(timeStamps[j]) + "." + milliSecond(timeStamps[j]) + ", p= " + pressureValues[j] + "\n";
		}
		
		dynAppend(emailLines,emailTemp);
		
		//set and send PMdata
		emailTemp = "about to initRDA, do not publish is: " + doNotPublish + "\t\n";
		dynAppend(emailLines, emailTemp);
		if(swVersion == 1 && !doNotPublish)
		{
			
			
			iRes = ctrlPostMortem_initRDA(rdaServer, rdaDevice, pmSystem, pmClass, (string) sEquipment_to_publish[i]);
			if (iRes< 0)  
			{
				//DebugTN("RDA test server error "+  iRes );
				emailTemp = "initRDA failed, with error: " + iRes + "\t\n";
				dynAppend(emailLines, emailTemp);
				return;
			}
			
			iRes=EventList_setPMDATA(stime, pressureValues, timeValues);
			if(iRes<0)
			{
				//DebugTN("vscPMtrigger.ctl: setPMDATA error for gauge " +  sDP_to_publish[i]);
				emailTemp = "Set PM DATA fail error number: " + iRes + "\t\n";
				dynAppend(emailLines, emailTemp);
			}
			else
			{
				emailTemp = "Sending PMData_extended\t\n";
				dynAppend(emailLines, emailTemp);
				
				iRes = ctrlPostMortem_sendPMData_extended();
				if(iRes == -1)  
				{
					//DebugTN("vscPMtrigger.ctl: sendPMData failed");
				}
				pmfile=getPath(DATA_REL_PATH)+"PM/PMData_"+sDP_to_publish[i]+"_"+lNanoLong+".log";
			
				iRes = ctrlPostMortem_writePMData_extended(pmfile);
				if(iRes == -1)  
				{
					//DebugTN("vscPMtrigger.ctl: WritePMData failed - Write PM data in: " + pmfile);
				}
			}

		}
		
		dynClear(pressureValues);
		dynClear(timeStamps);
	}
	time timeD3, timeD4;
	iRes = dpGet(sBeamDumpDps[1]+ ".ValueInt:_online.._stime", timeD3);
    if (iRes == -1)  {DebugTN("vcsPMTrigger.ctl: Get time value Dump1 error "+  iRes );}
    iRes = dpGet(sBeamDumpDps[2]+ ".ValueInt:_online.._stime", timeD4);
    if (iRes == -1)  {DebugTN("vcsPMTrigger.ctl: Get time value Dump2 error "+  iRes );}
	

	
	
	
	if(beamNumber == 3)
	{
		emailSubject += "XPOC Notification - dump 1 and 2 at "+ (string)stime + "\t";
	}
	else
	{
		emailSubject += "XPOC Notification - dump "+ beamNumber + " at "+ (string)stime + "\t";
	}
	
	if(timeD3 != timeD1 || timeD4!= timeD2)
	{
		dynClear(timeValues);
		return;
	}
	
	time_test = getCurrentTime();
	emailTemp = "End time: " + hour(time_test) + ":"+ minute(time_test) + ":" + second(time_test) + "." + milliSecond(time_test);
	dynAppend(emailLines,emailTemp);
	
	int i = 1;
	while(i <= 10 && i <= dynlen(emailSendStatus))
	{
		if(emailSendStatus[i] == 0)
		{
			break;
		}
		i++;
	}
	
	if(i > 10)
	{
		send_email("jorge.fraga@cern.ch","jorge.fraga@cern.ch","warning: email buffer full"," ");
	}
	else
	{
		emailSendStatus[i] = 1;
		emailBuffer[i*2-1]=emailSubject;
		emailBuffer[i*2]=emailLines;
	}
	dynClear(emailLines);
	dynClear(timeValues);
	emailSubject="";
	
	//DebugTN("Functioned finished successfully.");
}


int configure_dps()
{
	  sEquipmentNamesB = makeDynString(
    "VAC.VGPB.683034.B",
    "VAC.VGPB.683205.B",
    "VAC.VGPB.683252.B",
    "VAC.VGPB.683298.B",
    "VAC.VGPB.683340.B",
    "VAC.VGPB.684361.B",
    "VAC.VGPB.685183.B",
	"VAC.VGMA.689594.B",
	"VAC.VGMA.689462.B"
   );
   
    sEquipmentNamesR = makeDynString(
    "VAC.VGPB.623034.R",
    "VAC.VGPB.623206.R",
    "VAC.VGPB.623253.R",
    "VAC.VGPB.623298.R",
    "VAC.VGPB.623340.R",
    "VAC.VGPB.624335.R",
    "VAC.VGPB.625462.R",
	"VAC.VGMA.629462.R",
	"VAC.VGMA.629594.R"
  );
  
  	sDPNamesB = makeDynString(
    "VGPB_683034_B",
    "VGPB_683205_B",
    "VGPB_683252_B",
    "VGPB_683297_B",
    "VGPB_683340_B",
    "VGPB_684361_B",
    "VGPB_685183_B",
	"VGMA_689594_B",
	"VGMA_689462_B"
    );
	
	sDPNamesR = makeDynString(
    "VGPB_623034_R",
    "VGPB_623206_R",
    "VGPB_623253_R",
    "VGPB_623298_R",
    "VGPB_623340_R",
    "VGPB_624335_R",
    "VGPB_625462_R",
	"VGMA_629462_R",
	"VGMA_629594_R"
    );
	
	//configure _address_ and _archive_ for pr_xpoc dp element
	for(int icount = 1; icount <= dynlen(sDPNamesB); icount++)
   {
      set_Adress_PR_XPOC(sDPNamesB[icount]);
      set_Archive_PR_XPOC(sDPNamesB[icount]);
   }
   
   for(int icount = 1; icount <= dynlen(sDPNamesR); icount++)
   {
      set_Adress_PR_XPOC(sDPNamesR[icount]);
      set_Archive_PR_XPOC(sDPNamesR[icount]);
   }
}

int select_gauges(string dpName)
{
	if (strpos(dpName, sBeamDumpDps[1]) > -1)
	{
		//DebugTN("vcsPMTrigger.ctl: Dump beam 1 detected");
		return(1);
	}	
	else if( strpos(dpName, sBeamDumpDps[2]) >  -1)
	{
		//DebugTN("vcsPMTrigger.ctl: Dump beam 2 detected");
		return(2);
	}		
	else
	{
		//DebugTN("vcsPMTrigger.ctl: Error - beam dump origin not found");
		return(-1);
	}
}

int getPressureHistory(string gauge_name,dyn_time time_array, dyn_anytype &pressure_output, dyn_time &time_output)
{
	//guarantee that output arrays are clear:
	dynClear(time_output);
	dynClear(pressure_output);
	float closestPressure;
	time closestTimeStamp;
	int iRes= 0;
	//for each time value, get closest time
	//new faster version!
	string emailTemp;
	dyn_float pressureArray;
	dyn_time timeArrayStamps;
	time time_loop, time_distance,time_test;
	
	int index_loop, pressure_index=-1;
	//get period and time stamps for performance evaluation in email
	time_test = getCurrentTime();
	emailTemp = "Start time: " + hour(time_test) + ":"+ minute(time_test) + ":" + second(time_test) + "." + milliSecond(time_test);
	dynAppend(emailLines,emailTemp);
	
	iRes = dpGetPeriod(time_array[1], time_array[dynlen(time_array)],1, gauge_name+".PR_XPOC:_offline.._value",pressureArray, timeArrayStamps);
	
	time_test = getCurrentTime();
	emailTemp = "End time: " + hour(time_test) + ":"+ minute(time_test) + ":" + second(time_test) + "." + milliSecond(time_test);
	dynAppend(emailLines,emailTemp);
	//---------------------------------------------------------------------------------------
	if(iRes < 0) 
	{
		//DebugTN("vcsPMTrigger.ctl: dpGetPeriod error. Values from archive not retrieved");
		emailTemp = "dpGetPeriod error for gauge name " + gauge_name + " and time value " + hour(time_value) + ":"+ minute(time_value) + ":" + second(time_value) + "\t";
		dynAppend(emailLines, emailTemp);
		return(-1);
	}
	for(int i = 1; i <=dynlen(time_array);i++)
	{
		iRes=getClosestValueIndex(timeArrayStamps, time_array[i], pressure_index);
		if(iRes < 0)
		{
			emailTemp = "getClosestValue error, iRes < 0.";
			dynAppend(emailLines, emailTemp);
			pressure_output[i] = -1.0;
			time_output[i] = 0;
		}
		else
		{
			pressure_output[i] = pressureArray[pressure_index];
			time_output[i] = timeArrayStamps[pressure_index];
		}
	}
	dynClear(timeArrayStamps);
	dynClear(pressureArray);
	return(iRes);
}

int getClosestValueIndex(dyn_time timeArray, time time_value, int & closestValueIndex)
{
	int index_loop = 1;
	string emailTemp;
	if(dynlen(timeArray) < 1)
	{
		emailTemp="Error: getClosestValueIndex received an array shorter than expected. dynlen(timeArray) is" + dynlen(timeArray);
		dynAppend(emailLines,emailTemp);
		return(-1);
	}
	
	while(timeArray[index_loop] < time_value && index_loop < dynlen(timeArray))
	{
		index_loop++;
	}
	if(index_loop == 1)
	{
		closestValueIndex = 1;
	}
	else if(index_loop>dynlen(timeArray))
	{
		closestValueIndex = dynlen(timeArray);
	}
	else
	{
		if((timeArray[index_loop] - time_value) < (time_value - timeArray[index_loop-1]))
		{
			closestValueIndex = index_loop;
		}
		else
		{
			closestValueIndex = index_loop-1;
		}
	}
		
	return(0);
}

int EventList_setPMDATA(time tTrigger, dyn_anytype daValue, dyn_time dTrigger)
{
	/**
    IMPORTANT NOTE:
    - if you setPMData with an empty array, the PM buffer will contain only ARRAY 1D data
    - by default an array of size 1 will be as Parameters in the PM buffer. 
    - if you want always a ARRAY 1D even in the case of an array of size one, use the function:
    ctrlPostMortem_setPMDataColumnsAsArray(columnName, true);
    
    By default in the PM
    - PVSS integer will be long
    - PVSS float will be double
    - PVSS time will be as longlong if you use ctrlPostMortem_setPMData_asLongLongNano
    - PVSS time will be as long if you use ctrlPostMortem_setPMData
    */
/**
    attribute function
    Note:
    - registering a description attribute is like registering description
    - registering a unit attribute is like registering unit
    - registering a format attribute is like registering format
    - timestamps and time scale are special attribtue allowing to have all the columns registered with another one
    as Signals and automatically displayed in a table. WARNING: using true (when the ref column already exists as 
    in the example) seems not working, one has to use false.
    
    */  
	dyn_string dsColumnHeader, dsColumnDescription, dsColumnUnits;  
	int iRes, i, iLen;
	time tData;
	dyn_time dtData;
	dyn_string dsData;
	dyn_float dfData;
	int iTriggermilli = (int) tTrigger;
	int iTriggerNano = (int) milliSecond(tTrigger) * 1000000;
    
    
	  //set Data Head
	 dsColumnHeader=makeDynString("acqStamp","cycleName","time","value", "Times", "Values", "StrTimes"); 
	 dsColumnDescription=makeDynString("UTC time","cycleName","time","Pressure","UTC time", "Pressure", "Date");
	 dsColumnUnits=makeDynString("", "", "", "", "nsec", "mbar", "");

  // Set PM Header
	iRes = ctrlPostMortem_regPMDataColumns(dsColumnHeader);
	if (iRes == -1) 
	{
		//DebugTN("vscPMtrigger.ctl: ctrlPostMortem_regPMDataColumns error "+  iRes);
		return -2;
	}

	// Set PM Qualifier in header
	iRes = ctrlPostMortem_setPMQualifier_OK();
	if (iRes == -1)  
	{
		//DebugTN("vscPMtrigger.ctl: ctrlPostMortem_setPMQualifier_OK error "+  iRes);
		return -2;
	}

	// Set PM TimeStamp in header
	iRes = ctrlPostMortem_setPMTime_asLongLongNano(iTriggermilli, iTriggerNano );
	if ( iRes ==-1) 
	{
		//DebugTN("vscPMtrigger.ctl: ctrlPostMortem_setPMTime_asLongLongNano error "+  iRes);
		return -2; 
	}

	//set PM Data and Description
	iLen=dynlen(dsColumnHeader);  
	for(i=1;(i<=iLen && iRes==0);i++)  
	{
    // set PM Attribut data descriptions
    //iRes = ctrlPostMortem_regPMDataAttribute(dsColumnHeader[i], "description", dsColumnDescription[i], FALSE); // tested. OK
    //if (iRes == -1)  {DebugTN("vscPMtrigger.ctl: ctrlPostMortem_regPMDataAttribute error "+  iRes);return -4;}

		switch (i)
		  {
		  case 1:
			//Force time in nano 
			dtData = dTrigger[i_Post_dump_Value]; 
			iRes = ctrlPostMortem_setPMData_asLongLongNano(dsColumnHeader[i], dtData);  
			//iRes = ctrlPostMortem_setPMData(dsColumnHeader[i], tData);
			break;
		  case 2:
			dsData = "LHC.USER.LHC";
			iRes = ctrlPostMortem_setPMData(dsColumnHeader[i], dsData[1]);  
			break;
		  case 3:
			//Force in float type
			dfData = dTrigger[i_Post_dump_Value];
			iRes = ctrlPostMortem_setPMData(dsColumnHeader[i], dfData);  
			break;
		  case 4:
			//Force in float type        
			dfData = daValue[i_Post_dump_Value];
			iRes = ctrlPostMortem_setPMData(dsColumnHeader[i], dfData); 
			//iRes = ctrlPostMortem_regPMDataFormat(dsColumnHeader[i],"%f");
			//iRes = ctrlPostMortem_regPMDataAttribute(dsColumnHeader[i], "timestamps", "acqStamp", TRUE); // tested. OK

		   break;
		  case 5:
			// set PM Attributs data units    
			iRes = ctrlPostMortem_regPMDataUnits(dsColumnHeader[i], dsColumnUnits[i]); // tested. OK
			if (iRes == -1)  {/*DebugTN("vscPMtrigger.ctl: ctrlPostMortem_regPMDataUnits error "+  iRes);*/return -5;}

		   //Force time in nano 
			dtData = dTrigger;
			iRes = ctrlPostMortem_setPMData_asLongLongNano(dsColumnHeader[i], dtData);  // All values for times
			//iRes = ctrlPostMortem_regPMDataAttribute(dsColumnHeader[i], "timestamps", "acqStamp", TRUE); // tested. OK
			break;
		  case 6:
			// set PM Attributs data units    
			iRes = ctrlPostMortem_regPMDataUnits(dsColumnHeader[i], dsColumnUnits[i]); // tested. OK
			if (iRes == -1)  {/*DebugTN("vscPMtrigger.ctl: ctrlPostMortem_regPMDataUnits error "+  iRes);*/return -5;}

			//Force in float type
			dfData = daValue;
			iRes = ctrlPostMortem_setPMData(dsColumnHeader[i], dfData); // All values for values
			iRes = ctrlPostMortem_regPMDataAttribute(dsColumnHeader[i], "timestamps", "Times", TRUE); // tested. OK
			break;
		  case 7:
			//Put time info in string for controls
			dsData = dTrigger;
			iRes = ctrlPostMortem_setPMData(dsColumnHeader[i], dsData);  
			iRes = ctrlPostMortem_regPMDataAttribute(dsColumnHeader[i], "timestamps", "Values", TRUE); // tested. OK

			break;
		}
		
		dynClear(dtData);
		dynClear(dsData);
		dynClear(dfData);    
		if (iRes == -1)  {/*DebugTN("vscPMtrigger.ctl: ctrlPostMortem_setPMData "+i+" error "+  iRes);*/return -7;}  
        
   }
   dynClear(dsColumnHeader);
   dynClear(dsColumnDescription);
   dynClear(dsColumnUnits);
   dynClear(dtData);
   dynClear(dsData);
   dynClear(dfData);
   return iRes;
}

synchronized set_Adress_PR_XPOC(string sDP)
{ 
     dyn_anytype dynconfig_Temp;

      // --- retrieve existing parameters for _Adress in PR (added by the vac DB)
     fwPeriphAddress_get(sDP+".PR",  bconfigExists, dynconfig, bisActive, dynexceptionInfo);
     //DebugTN(sDP+".PR", dynconfig, dynexceptionInfo);
     
     // --- change the low level config comparaison to false.
     dynconfig[i_low_level_adr] = 0;

      // --- retrieve existing parameters for _Adress in PR_XPOC 
     fwPeriphAddress_get(sDP+".PR_XPOC", bconfigExists, dynconfig_Temp, bisActive, dynexceptionInfo);
     //DebugTN(sDP+".PR_XPOC", dynconfig, dynexceptionInfo);

     if ((bconfigExists == 0) || (dynconfig != dynconfig_Temp))
     {
       //DebugTN("vscPMtrigger.ctl: Adress changed for " + sDP+".PR_XPOC");
       fwPeriphAddress_set(sDP+".PR_XPOC", dynconfig, dynexceptionInfo, FALSE);
       //DebugTN(sDP+".PR_XPOC", dynconfig, dynexceptionInfo);
     }
    
} 

synchronized set_Archive_PR_XPOC(string sDP)
{ 

  string SarchiveClass;
  int iarchiveType;
  int ismoothProcedure;
  float fdeadband;
  float ftimeInterval;
     
   // --- retrieve existing parameters for _archive in PR_XPOC 
    fwArchive_get(sDP+".PR_XPOC", bconfigExists, SarchiveClass, iarchiveType, ismoothProcedure, fdeadband, ftimeInterval, bisActive, dynexceptionInfo);
  // DebugTN(sDP+".PR_XPOC", "bconfigExist", bconfigExists,  SarchiveClass, "iarchiveType", iarchiveType, "ismoothProcedure", ismoothProcedure,  "fdeadband", fdeadband,  "ftimeInterval", ftimeInterval, "active",bisActive, dynexceptionInfo) ;
   if ((bconfigExists == 0) || (bisActive == 0) || ((iarchiveType != iarchive_Type_save)&&(ftimeInterval != ftime_Interval_save)&&(ismoothProcedure != ismoothType_save)))
   {
     SarchiveClass = SarchiveName_save;
     iarchiveType = iarchive_Type_save;
     ismoothProcedure = ismoothType_save;
     fdeadband = fdead_band_save;
     ftimeInterval = ftime_Interval_save;
      // if not exist, creation
     //DebugTN("vscPMtrigger.ctl: Archives Para changed for " + sDP+".PR_XPOC");
     fwArchive_set(sDP+".PR_XPOC", SarchiveClass, iarchiveType, ismoothProcedure, fdeadband, ftimeInterval, dynexceptionInfo) ;
     fwArchive_get(sDP+".PR_XPOC", bconfigExists, SarchiveClass, iarchiveType, ismoothProcedure, fdeadband, ftimeInterval, bisActive, dynexceptionInfo);
     //DebugTN(sDP+".PR_XPOC", "bconfigExist", bconfigExists,  SarchiveClass, "iarchiveType", iarchiveType, "ismoothProcedure", ismoothProcedure,  "fdeadband", fdeadband,  "ftimeInterval", ftimeInterval, "active",bisActive, dynexceptionInfo) ;
    }
} 

int send_email(string sender, string address, string subject, dyn_string content)
{
	dyn_dyn_string email_content;
	string tempString;
	int ret;
	
	email_content[1] = address;
    email_content[2]=  sender; 
    email_content[3] = subject;
	
	
    for(int i = 1; i <= dynlen(content); i++)
    { 
        sprintf(tempString, "%s\t\n", content[i]);
        email_content[4]= email_content[4] + tempString;
	}
    emSendMail ("cernmx.cern.ch",getHostname(),email_content,ret);
	
	dynClear(email_content);
	return(ret);
}
