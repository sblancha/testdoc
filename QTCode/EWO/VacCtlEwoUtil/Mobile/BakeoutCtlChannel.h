#ifndef BAKEOUTCTLCHANNEL_H
#define	BAKEOUTCTLCHANNEL_H


// Class holding data for one bakeout control channel

#include "InterfaceEqp.h"

#include <qcolor.h>
#include <qrect.h>

class Eqp;

// Bits in status
#define	STATUS_MASK	(0xFF)

class BakeoutCtlChannel : public InterfaceEqp
{
	Q_OBJECT

public:
	BakeoutCtlChannel(Eqp *pEqp, int channel);
	~BakeoutCtlChannel();

	// Access
	inline int getChannel(void) const { return channel; }
	inline float getValue(void) const { return value; }
	inline int getRegChannel(void) const { return regChannel; }
	inline int getWarningThreshold(void) const { return warningThreshold; }
	inline int getErrorThreshold(void) const { return errorThreshold; }
	inline int getStatus(void) const { return status; }
	inline bool isValueValid(void) const { return valueValid; }

	inline void setNarrow(int coord) { rect.setLeft(coord); rect.setRight(coord); warningRect.setLeft(coord - 1); warningRect.setRight(coord + 1); errorRect.setLeft(coord - 1); errorRect.setRight(coord + 1); }
	inline void setWide(int left, int right) { rect.setLeft(left); rect.setRight(right); warningRect.setLeft(left - 1); warningRect.setRight(right + 1); errorRect.setLeft(left - 1); errorRect.setRight(right + 1); }
	inline void setTop(int coord) { rect.setTop(coord); }
	inline void setBottom(int coord) { rect.setBottom(coord); }
	inline void setWarningCoord(int coord) { warningRect.setTop(coord - 1); warningRect.setBottom(coord + 1); }
	inline void setErrorCoord(int coord) { errorRect.setTop(coord - 1); errorRect.setBottom(coord + 1); }

	inline int getLeft(void) const { return rect.left(); }
	inline int getRight(void) const { return rect.right(); }
	inline int getTop(void) const { return rect.top(); }
	inline int getBottom(void) const { return rect.bottom(); }
	inline const QRect &getRect(void) const { return rect; }
	inline int getLeftWarning(void) const { return warningRect.left(); }
	inline int getRightWarning(void) const { return warningRect.right(); }
	inline int getTopWarning(void) const { return warningRect.top(); }
	inline int getBottomWarning(void) const { return warningRect.bottom(); }
	inline const QRect &getRectWarning(void) const { return warningRect; }
	inline int getLeftError(void) const { return errorRect.left(); }
	inline int getRightError(void) const { return errorRect.right(); }
	inline int getTopError(void) const { return errorRect.top(); }
	inline int getBottomError(void) const { return errorRect.bottom(); }
	inline const QRect &getRectError(void) const { return errorRect; }

	inline QColor &getColor(void) { return color; }
	const QString getStateString(void);
	const QString getShortStateString(void);

public slots:	// Implement slots of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);
		
signals:	// Define own signals
	void eqpDataChanged(BakeoutCtlChannel *pSrc, bool colorChanged);

protected:

	// Pointer to device
	Eqp			*pEqp;

	// Last known value
	int			value;

	// Last known channel status
	int			status;

	// Last known regularion channel
	int			regChannel;

	// Last known warning threshold
	int			warningThreshold;

	// Last known error threshold
	int			errorThreshold;


	// Name of T DPE
	QString		tDpeName;

	// Name of regulation channel DPE
	QString		regChannelDpeName;

	// Name of STATUS DPE
	QString		statusDpeName;

	// Name of warning threshold DPE name
	QString		warningThresholdDpeName;

	// Name of error threshold DPE name
	QString		errorThresholdDpeName;


	// Color used for drawing
	QColor		color;

	// Bar boundaries
	QRect		rect;

	// Warning threshold bar boundaries
	QRect		warningRect;

	// Error threshold bar boundaries
	QRect		errorRect;

	// Channel number
	int			channel;

	// Flag indicating if last known value is valid
	bool		valueValid;

	void calculateColor(bool emitSignal);
};
#endif	// BAKEOUTCTLCHANNEL_H
