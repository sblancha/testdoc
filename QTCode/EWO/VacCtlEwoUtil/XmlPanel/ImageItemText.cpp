//	Implementation of ImageItemText class
/////////////////////////////////////////////////////////////////////////////////

#include "ImageItemText.h"

#include <QLocale>

//////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
//////////////////////////////////////////////////////////////////////////////////

ImageItemText::ImageItemText() : ImageItem()
{
	startX = startY = sizeX = sizeY = flags = 0;
	borderOffset = 0;
	filled = bordered = fit = false;
}

ImageItemText::~ImageItemText()
{
}


void ImageItemText::draw(QPainter & painter)
{
	painter.setTransform(transform);
	if(backColor.alpha() > 0)
	{
		painter.setBrush(backColor);
	}
	else
	{
		painter.setBrush(Qt::NoBrush);
	}
	setForePen(painter);
	painter.setFont(font);
	QRect textRect(startX, startY, sizeX, sizeY);
	if(fit)	// L.Kopylov 28.01.2014: No Size in 3.11 if Fit=True
	{
		QFontMetrics fm(painter.fontMetrics());
		QStringList partList = text.split("\n");
		QRect boundRect;
		for(int partIdx = 0 ; partIdx < partList.count() ; partIdx++)
		{
			QRect partRect = fm.boundingRect(partList.at(partIdx));
			if(boundRect.isNull())
			{
				boundRect = partRect;
			}
			else
			{
				if(boundRect.width() < partRect.width())
				{
					boundRect.setWidth(partRect.width());
				}
				boundRect.setHeight(boundRect.height() + partRect.height());
			}
		}
		int extra = borderOffset ? borderOffset * 2 : 2;
// qDebug("text %s borderOffset %d extra %d rect: %d x %d\n", text.toAscii().constData(), borderOffset, extra, boundRect.width(), boundRect.height());
		if((boundRect.width() + extra) > textRect.width())
		{
			textRect.setWidth(boundRect.width() + extra);
		}
		if((boundRect.height() + extra) > textRect.height())
		{
			textRect.setHeight(boundRect.height() + extra);
		}
	}
	if(filled && (!bordered))
	{
		painter.fillRect(textRect, painter.brush());
	}
	if(bordered)
	{
		painter.drawRect(textRect);
	}
	textRect.moveLeft(textRect.left() + borderOffset);
	textRect.moveTop(textRect.top() + borderOffset);
	painter.drawText(textRect, flags, text);
#ifdef XML_PANEL_PARSE
	printf("TEXT %s at (%d %d) size (%d %d)\n", text.toAscii().constData(),
		startX, startY, sizeX, sizeY);
	fflush(stdout);	
#endif
}

bool ImageItemText::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	if(!visible)
	{
		return false;
	}
	if(toolTipText.isEmpty())
	{
		return false;
	}
	QRect textRect(startX, startY, sizeX, sizeY);
	if(textRect.contains(point))
	{
		text = toolTipText;
		rect = textRect;
		return true;
	}
	return false;
}


void ImageItemText::parseProp(QXmlStreamReader &reader)
{
	QStringRef propName = reader.attributes().value("name");
	if(!propName.compare("Location", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		startX = (int)rint(c.toDouble(str.section(' ', 0, 0)));
		startY = (int)rint(c.toDouble(str.section(' ', 1, 1)));
	}
	else if(!propName.compare("Size", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		sizeX = (int)rint(c.toDouble(str.section(' ', 0, 0)));
		sizeY = (int)rint(c.toDouble(str.section(' ', 1, 1)));
		sizeX += borderOffset * 2;
		sizeY += borderOffset * 2;
	}
	else if(!propName.compare("FillType", Qt::CaseInsensitive))
	{
		filled = !reader.readElementText().compare("[solid]", Qt::CaseInsensitive);
	}
	else if(!propName.compare("BorderOffset", Qt::CaseInsensitive))
	{
		borderOffset = reader.readElementText().toInt();
		sizeX += borderOffset * 2;
		sizeY += borderOffset * 2;
	}
	else if(!propName.compare("Bordered", Qt::CaseInsensitive))
	{
		bordered = !reader.readElementText().compare("True", Qt::CaseInsensitive);
	}
	else if(!propName.compare("Fit", Qt::CaseInsensitive))
	{
		fit = !reader.readElementText().compare("True", Qt::CaseInsensitive);
	}
	else if(!propName.compare("Font", Qt::CaseInsensitive))
	{
		parseFont(reader);
	}
	else if(!propName.compare("Text", Qt::CaseInsensitive))
	{
		parseText(reader);
	}
	else if(!propName.compare("TextFormat", Qt::CaseInsensitive))
	{
		// Throw away surrounding []
		QString string = reader.readElementText();
		QString truncated = string.mid(1, string.length() - 2);

		// 3 = alignment
		QString part = truncated.section(',', 3, 3);
		if(!part.compare("ALIGNMENT_BEGINNING", Qt::CaseInsensitive))
		{
			flags = Qt::AlignLeft;
		}
		else if(!part.compare("ALIGNMENT_CENTER", Qt::CaseInsensitive))
		{
			flags = Qt::AlignHCenter;
		}
		else if(!part.compare("ALIGNMENT_END", Qt::CaseInsensitive))
		{
			flags = Qt::AlignRight;
		}		
	}
	else
	{
		ImageItem::parseProp(reader);
	}
}

void ImageItemText::parseFont(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement())
		{
#ifdef XML_PANEL_PARSE
			printf("ImageItemText::parseFont(): stop at %d: end of %s\n",
				(int)reader.lineNumber(), reader.name().toString().toAscii().constData());
#endif
			break;
		}
		if(reader.isStartElement())
		{
			if(!reader.name().compare("prop", Qt::CaseInsensitive))
			{
				QStringRef propName = reader.attributes().value("name");
				if(!propName.compare("en_US.iso88591", Qt::CaseInsensitive))
				{
					font.fromString(reader.readElementText());
					continue;
				}
			}
			parseUnknown(reader);
		}
	}
}

void ImageItemText::parseText(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement())
		{
#ifdef XML_PANEL_PARSE
			printf("ImageItemText::parseText(): stop at %d: end of %s\n",
				(int)reader.lineNumber(), reader.name().toString().toAscii().constData());
#endif
			break;
		}
		if(reader.isStartElement())
		{
			if(!reader.name().compare("prop", Qt::CaseInsensitive))
			{
				QStringRef propName = reader.attributes().value("name");
				if(!propName.compare("en_US.iso88591", Qt::CaseInsensitive))
				{
					text = reader.readElementText();
					continue;
				}
			}
			parseUnknown(reader);
		}
	}
}
