#ifndef MAINVIEWTABLEDEWO_H
#define MAINVIEWTABLEDEWO_H

// EWO interface for MainViewTabled widget

#include "VacMainViewEwo.h"

#include "MainViewTabled.h"

class EWO_EXPORT MainViewTabledEwo : public VacMainViewEwo
{
	Q_OBJECT

public:
	MainViewTabledEwo(QWidget *parent);
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void dpDataChange(void);

private:
	QVariant invokeConnectDp(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeDisconnectDp(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeDisconnectAll(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeGetNextChangeTag(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeGetNextChangeDp(const QString &name, QList<QVariant> &values, QString &error);
};

#endif	// MAINVIEWTABLEDEWO_H
