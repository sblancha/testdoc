#uses "vclDevCommon.ctl"	// Load CTRL library
// Configuration and auxilliary functions for oscillating devices

// Name of DP type containing alarm mask configuration
const string VAC_OSC_EQP_DP_TYPE = "_VacOscEqp";

// Name of DP containing alarm mask configuration
const string VAC_OSC_EQP_DP = "_VacOscEqp";

// Name of DP type containing time configuration
const string VAC_OSC_EQP_TIMED_DP_TYPE = "_TimedFunc";

// Name of DP containing time configuration
const string VAC_OSC_EQP_TIMED_DP = "_VacOscEqpReport";
// Check if DP with OSC eqp exists, if not - create such DP
void LhcVacOscEqpDp( string &configDpName, dyn_string &exceptionInfo )
{
	configDpName = "";
	if( dpExists( VAC_OSC_EQP_DP ) )
	{
		configDpName = VAC_OSC_EQP_DP;
		return;
	}
DebugN("dpCreate", VAC_OSC_EQP_DP);
   if( dpCreate( VAC_OSC_EQP_DP, VAC_OSC_EQP_DP_TYPE ) < 0 )
	{
		fwException_raise(exceptionInfo, "vclOscEqp.ctl ERROR", "LhcVacOscEqpDp(): dpCreate() failed", "" );
		return;
	}
	if( ! dpExists( VAC_OSC_EQP_DP ) )
	{
DebugN("dpCreateFailed", VAC_OSC_EQP_DP);
		fwException_raise(exceptionInfo, "vclOscEqp.ctl ERROR", "VacEVacOscEqpDp(): config DP not created", "" );
		return;
	}
	configDpName = VAC_OSC_EQP_DP;
	dpSet(configDpName+".interval", 600);		//10 min
	dpSet(configDpName+".restart", 3600);		//i hour
	dpSet(configDpName+".nThresh", 10);		// observe threshold
	dpSet(configDpName+".alThresh", 20);		//alarm theshold
	dpSet(configDpName+".arThresh", 20);		//archive theshold
	dpSet(configDpName+".server", "cernmx.cern.ch");		//server
	dpSet(configDpName+".sender", "Paulo.Gomes@cern.ch");		//Recipient
	dpSet(configDpName+".client", "cern.ch");		//client
}
void LhcVacOscEqpTimedDp( string &timedDpName, dyn_string &exceptionInfo )
{
	timedDpName = "";
	if( dpExists( VAC_OSC_EQP_TIMED_DP ) )
	{
		timedDpName = VAC_OSC_EQP_TIMED_DP;
		return;
	}
	if( dpCreate( VAC_OSC_EQP_TIMED_DP, VAC_OSC_EQP_TIMED_DP_TYPE ) < 0 )
	{
		fwException_raise(exceptionInfo, "vclOscEqp.ctl ERROR", "LhcVacOscEqpTimedDp(): dpCreate() failed", "" );
		return;
	}
	if( ! dpExists( VAC_OSC_EQP_TIMED_DP ) )
	{
		fwException_raise(exceptionInfo, "vclOscEqp.ctl ERROR", "LhcVacOscEqpTimedDp(): config DP not created", "" );
		return;
	}
	timedDpName = VAC_OSC_EQP_TIMED_DP;
}
