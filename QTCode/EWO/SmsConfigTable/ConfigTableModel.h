#ifndef CONFIGTABLEMODEL_H
#define CONFIGTABLEMODEL_H

// Data model for table of SMS configurations

#include "SmsConfig.h"

#include <QAbstractTableModel>
#include <qlist.h>
#include <qhash.h>
#include <qstack.h>
#include <qmutex.h>
#include <qtimer.h>

class ConfigTableModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	ConfigTableModel(QObject *parent = 0);
	~ConfigTableModel();

	void clear(void);
	void addSimpleConfig(const QString &dpName);
	void addGroupConfig(const QString &dpName, const QStringList &simpleDpNames);
	void addToGroup(const QString &dpName, const QString &simpleDpName);
	void deleteConfig(const QString &dpName);
	void setConfigId(const QString dpName, int id);
	void setConfigOwner(const QString &dpName, const QString &owner);
	void setConfigName(const QString &dpName, const QString &name);
	void setConfigError(const QString &dpName, const QString &error);
	void setConfigNumMsg(const QString &dpName, int numMsg);
	void setConfigNumEvents(const QString &dpName, unsigned numEvents);
	void setConfigFlags(const QString &dpName, bool active, bool activated, bool initialized);

	int getNumRows(void) { return rowDict.count(); }
	const QString getConfigAtRow(int row);

	const QString nextConnectRequest(void);
	const QString nextDisconnectRequest(void);

	// Implementation of QAbstractTableModel's methods
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant data(const QModelIndex &index, int role) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	void sort(int column, Qt::SortOrder order);

signals:
	void connectRequest(void);

protected:
	// Content of table - all rows
	QList<SmsConfig *>	rowDict;

	// Pending items to be added to table - in order not to refresh very often
	QList<SmsConfig *>	pendingConfigs;

	// All configurations
	QHash<QString, SmsConfig *>	allDict;

	// Mutex to protect connect/disconnect queues from parallel access
	QMutex		mutex;

	// Timer for delay view notification
	QTimer			*pTimer;

	// List of DP names to be disconnected first, before other connect
	// requests can be processed
	QStack<QString>	disconnectOnClear;

	// Flag indicating if connectRequest() was emitted and there are still pending requests
	bool		connectRequestSent;

	void addRow(SmsConfig *pConfig);
	void addToDictionary(SmsConfig *pConfig);
	void addCallbackRequest(void);

private slots:
	void configChanged(SmsConfig *pConfig);
	void configNumMsgChanged(SmsConfig *pConfig, int delta);
	void configNumEventsChanged(SmsConfig *pConfig, int delta);
	void timerDone(void);
};

#endif	// CONFIGTABLEMODEL_H
