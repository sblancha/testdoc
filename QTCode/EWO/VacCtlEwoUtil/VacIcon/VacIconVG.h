#ifndef	VACICONVG_H
#define	VACICONVG_H

//	Superclass for VG icons. It looks like the most (or even all) of VG
//	icons use the same rules for colors and ALSMOST the same image, the
//	difference in image is just small type-specific part of image drawn
//	inside colored circle with neutral (black) color.
//	So, all gaugesd which follow this rule may inherit this class and they
//	shall just implement drawSpecific() method - the method is called AFTER
//	circle and other parts are drawn.

#include "VacIcon.h"

class EqpVG_U;

class VACCTLEWOUTIL_EXPORT VacIconVG : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconVG(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconVG();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual VacIconContainer::Direction getSlaveDirection(void) const;
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);
	virtual void drawInterlockSrcArrow(QPainter &painter, bool before, bool after);

protected:
	// Pointer to my device
	Eqp *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual void drawImage(QPainter &painter, Eqp::CtlStatus ctlStatus);

	virtual void drawSpecific(QPainter &painter) = 0;

	void setPainterForMainImage(QPainter &painter, Eqp::CtlStatus ctlStatus);
};

#endif	// VACICONVG_H
