/**
@defgroup apiImportFct List of Import API functions
The newExternHdl function has a fixed interface and must be exported so that it can be called by WCCOA.\n
The keyword '_declspec(dllexport)' has to be set only under windows for exporting the function newExternHdl.Under Linux
this keyword is not necessary.\n
The newExternHdl function must allocate a class object and return the pointer to it.\n
Parameter is nextHdl.\n
It returns a pointer to new class instance.\n
Usual way to create C++LibAPI for WCCOA BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl).\n
@section import Import API
  - @ref VacImportParseFile 
  - @ref VacImportGetDriversS7Connection 

*/
#include "VacCtlImportPvss.h"
#include "DebugCtl.h"

#include <ImportPool.h>
#include <DpData.h>

#include <BitVar.hxx>

//------------------------------------------------------------------------------
/*
BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl)
{
*/

  static FunctionListRec fnList[] =
  {
	//  Return-Value    function name        parameter list               true == thread-save
	//  -------------------------------------------------------------------------------------
	{
		INTEGER_VAR, "VacImportParseFile",
		"(string fileName, string machineName, bool isMobile, dyn_string &exceptionInfo)",
		false
	},
	{
		INTEGER_VAR,
		"VacImportGetTimeStamp",
		"(string &timeStamp)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportGetDeviceCount",  "(void)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportGetDeviceDataByName",
		"(string dpName, string dpType, dyn_anytype &dp, dyn_dyn_anytype &dpe, dyn_string &exceptionInfo)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportGetDeviceDataByIndex",
		"(int index, dyn_anytype &dp, dyn_dyn_anytype &dpe, dyn_string &exceptionInfo)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportGetAllArchives",
		"(dyn_dyn_anytype &archives, dyn_string &exceptionInfo)",
		false
	},
	{
		INTEGER_VAR,
		"VacImportGetAllPollGroups",
		"(dyn_dyn_anytype &groups, dyn_string &exceptionInfo)",
		false
	},
	{
		INTEGER_VAR,
		"VacImportGetAllS7Connection",
		"(dyn_dyn_anytype &connections, dyn_string &exceptionInfo)",
		false
	},
	{
		INTEGER_VAR,
		"VacImportGetDriversS7Connection",
		"(dyn_anytype &drivers, dyn_string &exceptionInfo)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportSetDpeAddressChange",
		"(string dpName, string dpeName, dyn_string &exceptionInfo)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportSetDpeArchiveChange",
		"(string dpName, string dpeName, int changeType, string oldArchiveName, dyn_string &exceptionInfo )",
		false
	},
    {
		INTEGER_VAR,
		"VacImportSetDpeValueFormatChange",
		"(string dpName, string dpeName, dyn_string &exceptionInfo)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportAddDpe",
		"(string dpName, string dpeName, dyn_string &exceptionInfo)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportSetArchiveDp",
		"(string archiveName, string archiveDpName, dyn_string &exceptionInfo)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportGetDrivers",
		"(dyn_dyn_anytype &driverInfo, dyn_string &exceptionInfo)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportResetUpdateCounter",
		"(string dpName, string dpType)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportIncrementUpdateCounter",
		"(string dpName, string dpType, string dpeName)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportPendingDpeCount",
		"(string dpName, string dpType)",
		false
	},
    {
		INTEGER_VAR,
		"VacImportSetDipConfigChange",
		"(string dpName, string dpeName, dyn_string &exceptionInfo)",
		false
	},
	/* TODO: postponed because of low priority LIK 14.10.2008
    {
		INTEGER_VAR,
		"VacMobileLoggingReadConfig",
		"(string fileName, dyn_string &exceptionInfo)",
		false
	},
    {
		INTEGER_VAR,
		"VacMobileLoggingClean",
		"()",
		false
	},
    {
		INTEGER_VAR,
		"VacMobileLoggingAdd",
		"(string dpType, string dpName, string eqpName, string mainPartName, string sectorName)",
		false
	},
    {
		INTEGER_VAR,
		"VacMobileLoggingWrite",
		"(string fileName, dyn_string &exceptionInfo)",
		false
	},
	*/
  };

/*
  // this line counts the number of functions you want to implement.
  PVSSulong funcCount = sizeof(fnList) / sizeof(FunctionListRec);

  // now allocate the new instance
  return new VacCtlImportPvss(nextHdl, funcCount, fnList);
}
*/

CTRL_EXTENSION(VacCtlImportPvss, fnList)

//------------------------------------------------------------------------------

const Variable *VacCtlImportPvss::execute(ExecuteParamRec &param)
{
	enum
	{
		VacImportParseFile = 0,
		VacImportGetTimeStamp,
		VacImportGetDeviceCount,
		VacImportGetDeviceDataByName,
		VacImportGetDeviceDataByIndex,
		VacImportGetAllArchives,
		VacImportGetAllPollGroups,
		VacImportGetAllS7Connection,
		VacImportGetDriversS7Connection,
		VacImportSetDpeAddressChange,
		VacImportSetDpeArchiveChange,
		VacImportSetDpeValueFormatChange,
		VacImportAddDpe,
		VacImportSetArchiveDp,
		VacImportGetDrivers,
		VacImportResetUpdateCounter,
		VacImportIncrementUpdateCounter,
		VacImportPendingDpeCount,
		VacImportSetDipConfigChange
		/* TODO: postponed because of low priority LIK 14.10.2008
		VacMobileLoggingReadConfig,
		VacMobileLoggingClean,
		VacMobileLoggingAdd,
		VacMobileLoggingWrite
		*/
	};

	// TODO for the different return types you need corresponding static Variables
	static IntegerVar integerVar;
	int coco;

	param.thread->clearLastError();
	switch(param.funcNum)
	{
    // ---------------------------------------------------------------------
    // TODO change to your function
    case VacImportParseFile:
		coco = executeParseFile(param);
		break;
	case VacImportGetTimeStamp:
		coco = executeGetTimeStamp(param);
		break;
	case VacImportGetDeviceCount:	// No arguments
		{
			ImportPool &pool = ImportPool::getInstance();
			coco = pool.getDps().count();
		}
		break;
	case VacImportGetDeviceDataByName:
		coco = executeGetDeviceDataByName(param);
		break;
	case VacImportGetDeviceDataByIndex:
		coco = executeGetDeviceDataByIndex(param);
		break;
	case VacImportGetAllArchives:
		coco = executeGetAllArchives(param);
		break;
	case VacImportGetAllPollGroups:
		coco = executeGetAllPollGroups(param);
		break;
	case VacImportGetAllS7Connection:
		coco = executeGetAllS7Connection(param);
		break;
	case VacImportGetDriversS7Connection:
		coco = executeGetDriversS7Connection(param);
		break;
	case VacImportSetDpeAddressChange:
		coco = executeSetDpeAddressChange(param);
		break;
	case VacImportSetDpeArchiveChange:
		coco = executeSetDpeArchiveChange(param);
		break;
	case VacImportSetDpeValueFormatChange:
		coco = executeSetDpeValueFormatChange(param);
		break;
	case VacImportAddDpe:
		coco = executeAddDpe(param);
		break;
	case VacImportSetArchiveDp:
		coco = executeSetArchiveDp(param);
		break;
	case VacImportGetDrivers:
		coco = executeGetDrivers(param);
		break;
	case VacImportResetUpdateCounter:
		coco = executeResetUpdateCounter(param);
		break;
	case VacImportIncrementUpdateCounter:
		coco = executeIncrementUpdateCounter(param);
		break;
	case VacImportPendingDpeCount:
		coco = executePendingDpeCount(param);
		break;
	case VacImportSetDipConfigChange:
		coco = executeSetDipConfigChange(param);
		break;

	default:
		coco = -1;
		break;
	}
	integerVar.setValue(coco);
	return &integerVar;
}

/**
@defgroup VacImportParseFile VacImportParseFile()
@section s int VacImportParseFile(string fileName, string machineName, bool isMobile, dyn_string &exceptionInfo)
@subsection ss Purpose
Read config file .forImport and build import pool.
@subsection arg WCCOA CTRL Arguments
\b fileName: 		Config Import file name - full path (.forImport file)\n
\b machineName:     Machine or accelerator name (e.g. LHC)
\b isMobile:		Import file is import file for mobile profibus (deprecated)
\b exceptionInfo:	Exception information
@subsection ret Return
Return 0 if successfull, -1 if passive failed.
@subsection ex Usage Example
```
int InitDynLibStaticDataPools()
{
... 
// Initialize mobile equipment addressing information
ret = VacImportParseFile(PROJ_PATH + "/data/" + glAccelerator + "_Addresses.mobile", glAccelerator, true, exceptionInfo);
```
*/
/**
@brief Import API Process call to VacImportParseFile
*/
int VacCtlImportPvss::executeParseFile(ExecuteParamRec &param)
{
	TextVar		fileName, machineName;
	BitVar		isMobile;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg args[] =
	{
		{ "fileName",		TEXT_VAR,		&fileName,		NULL },
		{ "machineName",	TEXT_VAR,		&machineName,	NULL },
		{ "isMobile",		BIT_VAR,		&isMobile,		NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	ImportPool &pool = ImportPool::getInstance();
	pool.setMobile(isMobile.getValue() != PVSS_FALSE);
	QStringList errList;
	int coco = pool.processFile(fileName, machineName, errList);
	addExceptionInfo(param, exceptionInfo, errList);
	return coco;
}

/*
**	FUNCTION
**		Execute call to VacImportGetTimeStamp()
**		PVSS CTRL arguments are:
**			string &timeStamp
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeGetTimeStamp(ExecuteParamRec &param)
{
	Variable	*timeStamp;

	// Parse arguments
	SPvssArg args[] =
	{
		{ "timeStamp",	TEXT_VAR,	NULL,	&timeStamp }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	ImportPool &pool = ImportPool::getInstance();
	((TextVar *)timeStamp)->setValue(pool.getTimestamp() ? pool.getTimestamp() : "");
	return 1;
}

/*
**	FUNCTION
**		Execute call to VacImportGetDeviceDataByName()
**		PVSS CTRL arguments are:
**			string dpName
**			string dpType
**			dyn_anytype &dp
**			dyn_dyn_anytype &dpe
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeGetDeviceDataByName(ExecuteParamRec &param)
{
	TextVar		dpName, dpType;
	Variable	*dp, *dpe, *exceptionInfo;

		// Parse arguments
	SPvssArg args[] =
	{
		{ "dpName",			TEXT_VAR,			&dpName,	NULL },
		{ "dpType",			TEXT_VAR,			&dpType,	NULL },
		{ "dp",				DYNANYTYPE_VAR,		NULL,		&dp },
		{ "dpe",			DYNDYNANYTYPE_VAR,	NULL,		&dpe },
		{ "exceptionInfo",	DYNTEXT_VAR,		NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	ImportPool &pool = ImportPool::getInstance();
	DpData *pDp = pool.findDp(dpName, dpType);
	if(!pDp)
	{
		char buf[1024];
		sprintf(buf, "DP <%s> of type <%s> not found", (const char *)dpName, (const char *)dpType);
		addExceptionInfo(param, (DynVar *)exceptionInfo, buf);
		return -1;
	}
	return fillDpData(pDp, (DynVar *)dp, (DynVar *)dpe);
}

/*
**	FUNCTION
**		Execute call to VacImportGetDeviceDataByIndex()
**		PVSS CTRL arguments are:
**			int index
**			dyn_anytype &dp
**			dyn_dyn_anytype &dpe
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeGetDeviceDataByIndex(ExecuteParamRec &param)
{
	IntegerVar	index;
	Variable	*dp, *dpe, *exceptionInfo;

		// Parse arguments
	SPvssArg args[] =
	{
		{ "index",			INTEGER_VAR,		&index,	NULL },
		{ "dp",				DYNANYTYPE_VAR,		NULL,		&dp },
		{ "dpe",			DYNDYNANYTYPE_VAR,	NULL,		&dpe },
		{ "exceptionInfo",	DYNTEXT_VAR,		NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	ImportPool &pool = ImportPool::getInstance();
	const QHash<QByteArray, DpData *> &allDps = pool.getDps();
	int n = 0;
	DpData *pDp = NULL;
	if((0 <= index.getValue()) && (index.getValue() < allDps.count()))
	{
		foreach(DpData *pData, allDps)
		{
			if(n == index.getValue())
			{
				pDp = pData;
				break;
			}
			n++;
		}
	}
	if(!pDp)
	{
		char buf[128];
		sprintf(buf, "DP with index %d not found", index.getValue());
		addExceptionInfo(param, (DynVar *)exceptionInfo, buf);
		return -1;
	}
	return fillDpData(pDp, (DynVar *)dp, (DynVar *)dpe);
}

/*
**
** FUNCTION
**		Prepare all data for DP in format expected by PVSS:
**		dyn_anytype	- DP head data
**			1- name					(string)
**			2- type					(string)
**			3- mobileType			(int)
**			4 - lineNo				(int)
**			5 - exportToCMW			(bool)
**
**		dyn_dyn_anytype	- data for all DPEs of this DP
**		The following information is written for every DPE:
**			1- name					(string)	
**			2- addressType			(string)
**			3- drvNum				(int)
**			4- reference			(string)
**			5- direction			(int)
**			6- transType			(string)
**			7- oldNew				(int)
**			8- connect				(string)
**			9- group				(string)
**			10-pvssArchiveIndex		(int)
**			11-valueFormat			(string)
**			12-lineNo				(int)
**			13-changeAddress		(bool)
**			14-changeArchive		(bool)
**			15-changeValueFormat	(bool)
**			16-clearAll				(bool)
**			17-oldArchiveName		(string)
**			--- Additions 11.10.2012
**			18-archiveSmoothType			(int)
**			19-archiveSmoothTimeInterval	(int)
**			20-archiveSmoothDeadBand		(int)
**			=== Additions 06.03.2015
**			21-exportToCMW			(string)
**			22-exportToDIP			(string)
**			23-dipConfigChange		(bool)
**
**	PARAMETERS
**		pDp		- Pointer to DP for which data are requested
**		dp		- All details of DP head will be written to this variable (dyn_anytype)
**		dpe		- All details for all DPEs of DP will be written to this variable (dyn_dyn_anytype)
**
**	RETURNS
**		0
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::fillDpData(DpData *pDp, DynVar *dp, DynVar *dpe)
{
	// look for item for specified Dp
	
	dp->clear();
	dpe->clear();

	addToDynAnyVar(dp, pDp->getName());
	addToDynAnyVar(dp, pDp->getDpType());
	addToDynAnyVar(dp, pDp->getMobileType());
	addToDynAnyVar(dp, pDp->getLineNo());
	addToDynAnyVar(dp, pDp->isExportToCMW());

	const QList<DpeData *> &allDpes = pDp->getDpes();
	for(int idx = 0 ; idx < allDpes.count() ; idx++)
	{
		DpeData *pDpe = allDpes.at(idx);
		DynVar				dynVar;
		addToDynAnyVar(&dynVar, pDpe->getName());
		addToDynAnyVar(&dynVar, pDpe->getAddressType());
		addToDynAnyVar(&dynVar, pDpe->getDrvNum());
		addToDynAnyVar(&dynVar, pDpe->getReference());
		addToDynAnyVar(&dynVar, pDpe->getDirection());
		addToDynAnyVar(&dynVar, pDpe->getTransType());
		addToDynAnyVar(&dynVar, pDpe->getOldNew());
		addToDynAnyVar(&dynVar, pDpe->getConnect());
		addToDynAnyVar(&dynVar, pDpe->getGroup());
		addToDynAnyVar(&dynVar, pDpe->getArchiveIndex());
		addToDynAnyVar(&dynVar, pDpe->getValueFormat());
		addToDynAnyVar(&dynVar, pDpe->getLineNo());
		addToDynAnyVar(&dynVar, pDpe->isAddressChange());
		addToDynAnyVar(&dynVar, pDpe->getArchiveChange());
		addToDynAnyVar(&dynVar, pDpe->isValueFormatChange());
		addToDynAnyVar(&dynVar, pDpe->isClearAll());
		addToDynAnyVar(&dynVar, pDpe->getOldArchiveName().toLatin1().constData());

		// Addition 11.10.2012
		addToDynAnyVar(&dynVar, pDpe->getArchSmoothType());
		addToDynAnyVar(&dynVar, pDpe->getArchTimeInterval());
		addToDynAnyVar(&dynVar, pDpe->getArchDeadBand());
		
		// Addition 06.03.2015
		addToDynAnyVar(&dynVar, pDpe->getExportToCMW());
		addToDynAnyVar(&dynVar, pDpe->getExportToDIP());
		addToDynAnyVar(&dynVar, pDpe->isDipConfigChange());

		dpe->append(dynVar);
		dynVar.clear();
	}
	return 0;
}

/*
**	FUNCTION
**		Execute call to VacImportGetAllArchives()
**		PVSS CTRL arguments are:
**			dyn_dyn_anytype &archives
**			dyn_string &exceptionInfo
**
**		The following information is written for every archive:
**			1- name					(string )
**			2- smoothType			(int)	
**			3- timeInterval			(int)
**			4- deadBand				(int)
**			5- numOfDpeReference	(int)
**			6- lineNo				(int)
**			7- Archive DP name		(string)
**			8- depth				(int)
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeGetAllArchives(ExecuteParamRec &param)
{
	Variable	*archives, *exceptionInfo;

	// Parse arguments
	SPvssArg args[] =
	{
		{ "archives",		DYNDYNANYTYPE_VAR,	NULL,		&archives },
		{ "exceptionInfo",	DYNTEXT_VAR,		NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	ImportPool &pool = ImportPool::getInstance();
	((DynVar *)archives)->clear();
	const QList<Archive *> &allArchives = pool.getArchives();
	for(int idx = 0 ; idx < allArchives.count() ; idx++)
	{
		Archive *pArchive = allArchives.at(idx);
		DynVar	dynVar;
		addToDynAnyVar(&dynVar, pArchive->getName());
		addToDynAnyVar(&dynVar, pArchive->getSmoothType());
		addToDynAnyVar(&dynVar, pArchive->getTimeInterval());
		addToDynAnyVar(&dynVar, pArchive->getDeadBand());
		addToDynAnyVar(&dynVar, pArchive->getRefCount());
		addToDynAnyVar(&dynVar, pArchive->getLineNo());
		addToDynAnyVar(&dynVar, pArchive->getDpName());
		addToDynAnyVar(&dynVar, pArchive->getDepth());
		((DynVar *)archives)->append(dynVar);
		//dynVar.clear();
	}
	return 0;
}

/*
**	FUNCTION
**		Execute call to VacImportGetAllPollGroups()
**		PVSS CTRL arguments are:
**			dyn_dyn_anytype &groups
**			dyn_string &exceptionInfo
**
**		The following information is written for every group:
**			1- group			(string)
**			2- pollPeriod		(int)
**			3- lineNo			(int)
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeGetAllPollGroups(ExecuteParamRec &param)
{
	Variable	*groups, *exceptionInfo;

		// Parse arguments
	SPvssArg args[] =
	{
		{ "groups",			DYNDYNANYTYPE_VAR,	NULL,		&groups },
		{ "exceptionInfo",	DYNTEXT_VAR,		NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	((DynVar *)groups)->clear();
	ImportPool &pool = ImportPool::getInstance();
	const QList<PollGroup *> &allGroups = pool.getPollGroups();
	for(int idx = 0 ; idx < allGroups.count() ; idx++)
	{
		PollGroup *pGroup = allGroups.at(idx);
		DynVar dynVar;
		addToDynAnyVar(&dynVar, pGroup->getName());
		addToDynAnyVar(&dynVar, pGroup->getPeriod());
		addToDynAnyVar(&dynVar, pGroup->getLineNo());
		
		((DynVar *)groups)->append(dynVar);
		dynVar.clear();
	}
	return 0;
}

/*
**	FUNCTION
**		Execute call to VacImportGetAllS7Connection()
**		PVSS CTRL arguments are:
**			dyn_dyn_anytype &connections
**			dyn_string &exceptionInfo
**
**		The following information is written for every connection:
**			1- connection		(string)
**			2- ipAddress		(string)
**			3- type				(int)
**			4- rack				(int)
**			5- slot				(int)		
**			6- lineNo			(int)
**			7- ipAddressValid	(bool)
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeGetAllS7Connection(ExecuteParamRec &param)
{
	Variable	*connections, *exceptionInfo;

		// Parse arguments
	SPvssArg args[] =
	{
		{ "connections",	DYNDYNANYTYPE_VAR,	NULL,		&connections },
		{ "exceptionInfo",	DYNTEXT_VAR,		NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	((DynVar *)connections)->clear();
	ImportPool &pool = ImportPool::getInstance();
	const QList<S7Connection *> &allConnections = pool.getS7Connections();
	for(int idx = 0 ; idx < allConnections.count() ; idx++)
	{
		S7Connection *pConnect = allConnections.at(idx);
		DynVar dynVar;
		addToDynAnyVar(&dynVar, pConnect->getName());
		addToDynAnyVar(&dynVar, pConnect->getIpAddress());
		addToDynAnyVar(&dynVar, pConnect->getType());
		addToDynAnyVar(&dynVar, pConnect->getRack());
		addToDynAnyVar(&dynVar, pConnect->getSlot());
		addToDynAnyVar(&dynVar, pConnect->getLineNo());
		addToDynAnyVar(&dynVar, pConnect->isIpAddressValid());

		((DynVar *)connections)->append(dynVar);
		dynVar.clear();
	}
	return 0;
}

/**
@defgroup VacImportGetDriversS7Connection VacImportGetDriversS7Connection()
@section s int VacImportGetDriversS7Connection(dyn_anytype &drivers, dyn_string &exceptionInfo)
@subsection ss Purpose
Read config file .forImport to get all the S7 Driver numbers.
@subsection arg WCCOA CTRL Arguments
\b drivers: 		List of all the drivers numbers \n
\b exceptionInfo:	Exception information
@subsection ret Return
The function always returns 0, the value returned is meaningless.
@subsection ex Usage Example
```
LhcVacSetDataPart("PS"); \n
... \n
\\\ Usually in main view panels it is used as following, to use $-parameter supplied to panel:
if(isDollarDefined("$dataPart")) { \n
LhcVacSetDataPart($dataPart); \n
} \n
```
*/
/**
@brief Import API Process call to VacImportGetDriversS7Connection
*/
int VacCtlImportPvss::executeGetDriversS7Connection(ExecuteParamRec &param) {
	Variable	*drivers, *exceptionInfo;

	// Parse arguments
	SPvssArg args[] = {
		{ "drivers", DYNANYTYPE_VAR, NULL, &drivers },
		{ "exceptionInfo", DYNTEXT_VAR, NULL, &exceptionInfo }
	};
	if (!parseArgs(param, args, sizeof(args) / sizeof(args[0]))) {
		return -1;
	}
	((DynVar *)drivers)->clear();
	ImportPool &pool = ImportPool::getInstance();
	const QList<S7Connection *> &allConnections = pool.getS7Connections();
	for (int idx = 0; idx < allConnections.count(); idx++) {
		S7Connection *pConnect = allConnections.at(idx);
		IntegerVar	*pIntVar = new IntegerVar();
		int value = pConnect->getDriver();
		pIntVar->setValue(value ? value : 0);
		AnyTypeVar anyTypeVar;
		anyTypeVar.setVar(pIntVar);
		((DynVar *)drivers)->append(anyTypeVar);
	}
	return 0;
}
/*
**
**	FUNCTION
**		Execute call to VacImportSetDpeAddressChange()
**		PVSS CTRL arguments are:
**			string dpName
**			string dpeName
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeSetDpeAddressChange(ExecuteParamRec &param)
{
	TextVar		dpName, dpeName;
	Variable	*exceptionInfo;

		// Parse arguments
	SPvssArg args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,	NULL },
		{ "dpeName",		TEXT_VAR,		&dpeName,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	DpeData *pDpe = findDpe(dpName, dpeName, param, (DynVar *)exceptionInfo);
	if(!pDpe)
	{
		return -1;
	}
	pDpe->setAddressChange(true);
	return 0;
}

/*
**	FUNCTION
**		Execute call to VacImportSetDpeArchiveChange()
**		PVSS CTRL arguments are:
**			string dpName
**			string dpeName
**			int changeType
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeSetDpeArchiveChange(ExecuteParamRec &param)
{
	TextVar		dpName, dpeName, oldArchiveName;
	IntegerVar	changeType;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "dpeName",		TEXT_VAR,		&dpeName,		NULL },
		{ "changeType",		INTEGER_VAR,	&changeType,	NULL },
		{ "oldArchiveName",	TEXT_VAR,		&oldArchiveName,NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DpeData *pDpe = findDpe(dpName, dpeName, param, (DynVar *)exceptionInfo);
	if(!pDpe)
	{
		return -1;
	}
	pDpe->setArchiveChange(changeType.getValue());
	pDpe->setOldArchiveName(oldArchiveName);
	return 0;
}

/*
**	FUNCTION
**		Execute call to VacImportSetDpeValueFormatChange()
**		PVSS CTRL arguments are:
**			string dpName
**			string dpeName
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeSetDpeValueFormatChange(ExecuteParamRec &param)
{
	TextVar		dpName, dpeName;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "dpeName",		TEXT_VAR,		&dpeName,		NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DpeData *pDpe = findDpe(dpName, dpeName, param, (DynVar *)exceptionInfo);
	if(!pDpe)
	{
		return -1;
	}
	pDpe->setValueFormatChange(true);
	return 0;
}

/*
**	FUNCTION
**		Helper method: find in pool data for given DPE of given DP
**
**	PARAMETERS
**		dpName			- DP name
**		dpeName			- DPE name
**		param			- All calling and execution parametrs can be accessed via this class	
**		exceptionInfo	- Pointer to variable where error message will be
**							added if DPE not found
**
**	RETURNS
**		Pointer to DpeData instance; or
**		NULL if not found
**
**	CAUTIONS
**		None
*/
DpeData *VacCtlImportPvss::findDpe(const char *dpName, const char *dpeName, ExecuteParamRec &param,
	DynVar *exceptionInfo)
{
	ImportPool &pool = ImportPool::getInstance();
	DpData *pDp = pool.findDp(dpName);
	if(!pDp)
	{
		char buf[512];
		sprintf(buf, "DP <%s> not found", (const char *)dpName);
		addExceptionInfo(param, (DynVar *)exceptionInfo, buf);
		return NULL;
	}
	const QList<DpeData *> &allDpes = pDp->getDpes();
	for(int idx = 0 ; idx < allDpes.count() ; idx++)
	{
		DpeData *pDpe = allDpes.at(idx);
		if(!strcmp(pDpe->getName(), dpeName))
		{
			return pDpe;
		}
	}
	{
		char buf[512];
		sprintf(buf, "DP.DPE <%s>.<%s> not found", (const char *)dpName, (const char *)dpeName);
		addExceptionInfo(param, (DynVar *)exceptionInfo, buf);
	}
	return NULL;
}

/*
**	FUNCTION
**		Execute call to VacImportAddDpe()
**		PVSS CTRL arguments are:
**			string dpName
**			string dpeName
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeAddDpe(ExecuteParamRec &param)
{
	TextVar		dpName, dpeName;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "dpeName",		TEXT_VAR,		&dpeName,		NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	ImportPool &pool = ImportPool::getInstance();
	DpData *pDp = pool.findDp(dpName);
	if(!pDp)
	{
		char buf[512];
		sprintf(buf, "DP <%s> not found", (const char *)dpName);
		addExceptionInfo(param, (DynVar *)exceptionInfo, buf);
		return -1;
	}
	const QList<DpeData *> &allDpes = pDp->getDpes();
	DpeData *pDpe;
	for(int idx = 0 ; idx < allDpes.count() ; idx++)
	{
		pDpe = allDpes.at(idx);
		if(!strcmp(pDpe->getName(), dpeName))
		{
			char buf[512];
			sprintf(buf, "DP.DPE <%s>.<%s> already exists", (const char *)dpName, (const char *)dpeName);
			addExceptionInfo(param, (DynVar *)exceptionInfo, buf);
			return -1;
		}
	}
	pDpe = new DpeData(dpeName, dpName, -1);
	pDpe->setClearAll(true);
	pDp->getDpes().append(pDpe);
	return 0;
}

/*
**	FUNCTION
**		Execute call to VacImportSetArchiveDp()
**		PVSS CTRL arguments are:
**			string archiveName
**			string archiveDpName
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeSetArchiveDp(ExecuteParamRec &param)
{
	TextVar		archiveName, archiveDpName;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg args[] =
	{
		{ "archiveName",	TEXT_VAR,		&archiveName,	NULL },
		{ "archiveDpName",	TEXT_VAR,		&archiveDpName,	NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	ImportPool &pool = ImportPool::getInstance();
	Archive *pArchive = pool.findArchive(archiveName);
	if(!pArchive)
	{
		char	buf[512];
		sprintf(buf, "Archive <%s> not found", (const char *)archiveName);
		addExceptionInfo(param, (DynVar *)exceptionInfo, buf);
		return -1;
	}
	pArchive->setDpName(archiveDpName);
	return 0;
}

/*
**
**	FUNCTION
**		Execute call to VacImportGetDrivers
**		PVSS CTRL arguments are:
**			dyn_dyn_anytype &driverInfo
**			dyn_string &exceptionInfo
**
**		The following information is written for every driver:
**			1- driver number			(int)
**			2- driver type				(string)
**			3- number of references		(int)
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeGetDrivers(ExecuteParamRec &param)
{
	Variable	*driverInfo, *exceptionInfo;

	// Parse arguments
	SPvssArg args[] =
	{
		{ "driverInfo",	DYNDYNANYTYPE_VAR,	NULL,		&driverInfo },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,		&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	((DynVar *)driverInfo)->clear();
	const QList<DriverRef *> &allDrivers = DriverRef::getInstances();
	for(int idx = 0 ; idx < allDrivers.count() ; idx++)
	{
		DriverRef *pDriver = allDrivers.at(idx);
		DynVar dynVar;
		addToDynAnyVar(&dynVar, pDriver->getDrvNum());
		addToDynAnyVar(&dynVar, pDriver->getType());
		addToDynAnyVar(&dynVar, pDriver->getRefCount());

		((DynVar *)driverInfo)->append(dynVar);
		dynVar.clear();
	}
	return 0;
}

/*
**	FUNCTION
**		Execute call to VacImportResetUpdateCounter
**		PVSS CTRL arguments are:
**			string dpName
**			string dpType
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeResetUpdateCounter(ExecuteParamRec &param)
{
	TextVar	dpName, dpType;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",	TEXT_VAR,	&dpName,	NULL },
		{ "dpType",	TEXT_VAR,	&dpType,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	ImportPool &pool = ImportPool::getInstance();
	DpData *pDp = pool.findDp(dpName, dpType);
	if(!pDp)
	{
		return -1;
	}
	const QList<DpeData *> &allDpes = pDp->getDpes();
	for(int idx = 0 ; idx < allDpes.count() ; idx++)
	{
		DpeData *pDpe = allDpes.at(idx);
		pDpe->setUpdateCounter(0);
	}
	return 0;
}

/*
**	FUNCTION
**		Execute call to VacImportPendingDpeCount
**		PVSS CTRL arguments are:
**			string dpName
**			string dpType
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeIncrementUpdateCounter(ExecuteParamRec &param)
{
	TextVar	dpName, dpType, dpeName;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",		TEXT_VAR,	&dpName,	NULL },
		{ "dpType",		TEXT_VAR,	&dpType,	NULL },
		{ "dpeName",	TEXT_VAR,	&dpeName,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	ImportPool &pool = ImportPool::getInstance();
	DpData *pDp = pool.findDp(dpName, dpType);
	if(!pDp)
	{
		return -1;
	}
	const QList<DpeData *> &allDpes = pDp->getDpes();
	for(int idx = 0 ; idx < allDpes.count() ; idx++)
	{
		DpeData *pDpe = allDpes.at(idx);
		if(!strcmp(pDpe->getName(), dpeName))
		{
			pDpe->setUpdateCounter(pDpe->getUpdateCounter() + 1);
			return 0;
		}
	}
	return -1;
}

/*
**	FUNCTION
**		Execute call to VacImportPendingDpeCount
**		PVSS CTRL arguments are:
**			string dpName
**			string dpType
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executePendingDpeCount(ExecuteParamRec &param)
{
	TextVar	dpName, dpType;

	// Parse arguments
	SPvssArg	args[] =
	{
		{ "dpName",	TEXT_VAR,	&dpName,	NULL },
		{ "dpType",	TEXT_VAR,	&dpType,	NULL }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}
	ImportPool &pool = ImportPool::getInstance();
	DpData *pDp = pool.findDp(dpName, dpType);
	if(!pDp)
	{
		return -1;
	}
	const QList<DpeData *> &allDpes = pDp->getDpes();
	int coco = 0;
	for(int idx = 0 ; idx < allDpes.count() ; idx++)
	{
		DpeData *pDpe = allDpes.at(idx);
		if((pDpe->getDirection() == 1) && (pDpe->getUpdateCounter() < 1))
		{
			coco++;
		}
	}
	return coco;
}

/*
**	FUNCTION
**		Execute call to VacImportSetDipConfigChange()
**		PVSS CTRL arguments are:
**			string dpName
**			string dpeName
**			dyn_string &exceptionInfo
**
**	PARAMETERS
**		param	- All calling and execution parametrs can be accessed via this class	
**
**	RETURNS
**		The value to be returned by corresponding PVSS function
**
**	CAUTIONS
**		None
*/
int VacCtlImportPvss::executeSetDipConfigChange(ExecuteParamRec &param)
{
	TextVar		dpName, dpeName;
	Variable	*exceptionInfo;

	// Parse arguments
	SPvssArg args[] =
	{
		{ "dpName",			TEXT_VAR,		&dpName,		NULL },
		{ "dpeName",		TEXT_VAR,		&dpeName,		NULL },
		{ "exceptionInfo",	DYNTEXT_VAR,	NULL,			&exceptionInfo }
	};
	if(!parseArgs(param, args, sizeof(args) / sizeof(args[0])))
	{
		return -1;
	}

	DpeData *pDpe = findDpe(dpName, dpeName, param, (DynVar *)exceptionInfo);
	if(!pDpe)
	{
		return -1;
	}
	pDpe->setDipConfigChange(true);
	return 0;
}
