//	Implementation of SynLine class
/////////////////////////////////////////////////////////////////////////////////

#include "SynLine.h"

#include "VacEqpTypeMask.h"
#include "EqpMsgCriteria.h"

#include "DataPool.h"
#include "BeamLine.h"
#include "BeamLinePart.h"
#include "Eqp.h"
#include "EqpCOLDEX.h"
#include "EqpSECT_VPI_SUM.h"
#include "EqpType.h"
#include "VacIcon.h"

#include "VacMainView.h"
#include "DebugCtl.h"

#include <QPainter>

// Height of passive element [Pixels]
#define	PASSIVE_ELEM_HEIGHT		(7)	// LIK 02.03.2008 - was 11

// Minimum width of passive element [Pixels]
#define MIN_PASSIVE_WIDTH		(3)

// Minimum width of empty sector (wihout any equipment) [Pixels]
#define	MIN_EMPTY_SECTOR_WIDTH	(8)

// Offset for connection points of devices in group from beam line
#define	COMMON_ACTIVE_OFFSET	(5)	// LIK 02.03.2008 - was 7

// Number of pixels between beamline and margin for TEXT (EX: TEXT - MARGIN_LINE_TEXT - LINE - EQUIPMENT) Creates the space between text and beamline
#define	MARGIN_LINE_TEXT	(20)	// [VACCO-527]

/*
** FUNCTION
**		Check if range of sectors given by pointers to start and end sector
**		shall include (part of) given beam line. If so - create new instance
**		of CSynLine for this beam line and return it. Note that new instance
**		has only limits equipment selection, NOT equipment itself - equipment
**		and geometry will be built by method build().
**
** PARAMETERS
**		parent			- QWidget where this line will be drawn
**		pLine			- Pointer to beam line for which synoptic line shall be built
**		pStartSector	- Pointer to first sector in range
**		pEndSector		- Pointer to last sector in range
**
** RETURNS
**		Pointer to new instance ofCSynLine if given beam line shall be at least
**			partially presented in synoptic;
**		NULL if given beam line shall not be presented in synoptic
**
** CAUTIONS
**		None
*/
SynLine *SynLine::create(QWidget *parent, BeamLine *pLine, Sector *pStartSector, Sector *pEndSector)
{
	BeamLinePart	*pStartPart, *pEndPart;
	int				startEqpIdx, endEqpIdx;

	DataPool &pool = DataPool::getInstance();
	if(!pLine->findEqpRange(pStartSector, pEndSector,
		&pStartPart, &pEndPart, startEqpIdx, endEqpIdx))
	{
		return NULL;
	}

	SynLine *pSynLine = new SynLine();
	// clear list for mobile
	pSynLine->listEqpsActiveMayChange.clear();
	pSynLine->listEqpsPositionMayChange.clear();

	// [VACCO-527] Setting pLine Start and End tags, if applied
	pSynLine->setTagStart(pSynLine->findMainPartNameBeforeFirstEqp(pLine, pStartPart, startEqpIdx));
	pSynLine->setTagEnd(pSynLine->findMainPartNameAfterLastEqp(pLine, pEndPart, endEqpIdx));

	pSynLine->parent = parent;
	pSynLine->pBeamLine = pLine;
	pSynLine->pStartPart = pStartPart;
	pSynLine->pEndPart = pEndPart;
	pSynLine->startEqpIdx = startEqpIdx;
	pSynLine->endEqpIdx = endEqpIdx;
	pSynLine->metersToScreen = 1.0 / pool.getMinPassiveEqpVisibleWidth();

	return pSynLine;
}


/*	
**	FUNCTION
**		[VACCO-929]
**		Builds the list of sectors (Sector, pSectorList) in the SynLine
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Must be runned only after Equipment (eqps) list is complete,
**		and before the method setSectorList
*/
void SynLine::setSectorList(void){
	pSectorList.clear();
	if (!reverse){
		for (int idx = 0; idx < eqps.count(); idx++){
			SynEqpItem *pSectEqp = eqps.at(idx);
			if (pSectEqp->getEqp()->isSectorBorder()){
				Sector *pSector = pSectEqp->getEqp()->getSectorBefore();
				if (!pSectorList.contains(pSector)){
					pSectorList.append(pSector);
				}
				pSector = pSectEqp->getEqp()->getSectorAfter();
				if (!pSectorList.contains(pSector)){
					pSectorList.append(pSector);
				}
			}
		}
	}
	else {
		for (int idx = 0; idx < eqps.count(); idx++){
			SynEqpItem *pSectEqp = eqps.at(idx);
			if (pSectEqp->getEqp()->isSectorBorder()){
				Sector *pSector = pSectEqp->getEqp()->getSectorAfter();
				if (!pSectorList.contains(pSector)){
					pSectorList.append(pSector);
				}
				pSector = pSectEqp->getEqp()->getSectorBefore();
				if (!pSectorList.contains(pSector)){
					pSectorList.append(pSector);
				}
			}
		}
	}
	return;
}



/*
**	FUNCTION
**		[VACCO-929]
**		Builds list of sector labels (SynLhcSectorLabel, pSectorLabelList) of the SynLine
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Must be runned only after the method setSectorList
*/
void SynLine::setSectorLabelList(void){
	pSectorLabelList.clear();
	for (int idx = 0; idx < pSectorList.count(); idx++){
		pSectorLabelList.append(new SynLhcSectorLabel(pSectorList.at(idx)));
	}
	return;
}



/*
**	FUNCTION
**		[VACCO-929]
**		Finds the start and end coordinates of each sector represented in the synoptic.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::setSectorGeometry(void){
	for (int idx = 0; idx < pSectorLabelList.count(); idx++){
		SynLhcSectorLabel *pSectorLabel = pSectorLabelList.at(idx);
		Sector *pSector = pSectorLabel->getSector();
		QList <Eqp *> pSectEqpList = pSector->getEqpList();
		QList <Eqp *> startEqps, endEqps;

		// Checks each sectorLabel's border equipment
		for (int idx2 = 0; idx2 < pSectEqpList.count(); idx2++){
			Eqp *pSectEqp = pSectEqpList.at(idx2);
			if ((pSectEqp->isSectorBorder()) && (pSectEqp->getSectorAfter() == pSector)){
				startEqps.append(pSectEqp);
			}
			if ((pSectEqp->isSectorBorder()) && (pSectEqp->getSectorBefore() == pSector)){
				endEqps.append(pSectEqp);
			}
		}

		// Gets EqpItem equivalent to the Eqp that are sector borders.
		// Note: the EqpItem only contains the equipment in the synoptic view,
		//		so not all sector's equipments are in this list.
		QList <SynEqpItem *> startEqpsItem, endEqpsItem;
		QLine *sectLine = new QLine;
		QList <QLine *> sectorGraph = pSectorLabel->getSectorGraph();
		for (int idx3 = 0; idx3 < eqps.count(); idx3++){
			SynEqpItem *pEqpItem = eqps.at(idx3);
			for (int idx4 = 0; idx4 < startEqps.count(); idx4++){
				if (startEqps.at(idx4) == pEqpItem->getEqp()){
					if (!reverse) startEqpsItem.append(pEqpItem);
					if (reverse) endEqpsItem.append(pEqpItem);
					continue;
				}
			}
			for (int idx4 = 0; idx4 < endEqps.count(); idx4++){
				if (endEqps.at(idx4) == pEqpItem->getEqp()){
					if (!reverse) endEqpsItem.append(pEqpItem);
					if (reverse) startEqpsItem.append(pEqpItem);
					continue;
				}
			}
		}

		//in case the same equipment is represented in both ends
		if (startEqpsItem.count() == 2)
		{
			if (!reverse)
			{
				if (startEqpsItem.at(0)->getConnPoint().x() > startEqpsItem.at(1)->getConnPoint().x())
					startEqpsItem.removeAt(0);	
				else
					startEqpsItem.removeAt(1);
			}
			else
			{
				if (startEqpsItem.at(0)->getConnPoint().x() < startEqpsItem.at(1)->getConnPoint().x())
					startEqpsItem.removeAt(0);
				else
					startEqpsItem.removeAt(1);
			}
		}
		if (endEqpsItem.count() == 2)
		{
			if (!reverse)
			{
				if (endEqpsItem.at(0)->getConnPoint().x() < endEqpsItem.at(1)->getConnPoint().x())
					endEqpsItem.removeAt(0);
				else
					endEqpsItem.removeAt(1);
			}
			else
			{
				if (endEqpsItem.at(0)->getConnPoint().x() > endEqpsItem.at(1)->getConnPoint().x())
					endEqpsItem.removeAt(0);
				else
					endEqpsItem.removeAt(1);
			}
		}


		// If the sector has both START and END equipments represented in the synoptic
		if ((startEqpsItem.count() == 1) && (endEqpsItem.count() == 1)){
			QRect rect = startEqpsItem.first()->getRect();
			sectLine->setP1(rect.center());
			rect = endEqpsItem.first()->getRect();
			sectLine->setP2(rect.center());
		}
		// If the sector has only the START equipment represented in the synoptic,
		//		it's necessary to go through the SynLine parts to know the end coords
		else if ((startEqpsItem.count() == 0) && (endEqpsItem.count() == 1)){
			if ((endEqpsItem.first()->getRect().center().x() >= startXBeamLine) &&
				(endEqpsItem.first()->getRect().center().x() <= endXBeamLine)){

				QRect rect = endEqpsItem.first()->getRect();
				sectLine->setP1(QPoint(startXBeamLine, rect.center().y()));
				sectLine->setP2(rect.center());
			}
		}
		// If the sector has only the END equipment represented in the synoptic,
		//		it's necessary to go through the SynLine parts to know the start coords
		else if ((startEqpsItem.count() == 1) && (endEqpsItem.count() == 0)){
			if ((startEqpsItem.first()->getRect().center().x() >= startXBeamLine) &&
				(startEqpsItem.first()->getRect().center().x() <= endXBeamLine)){

				QRect rect = startEqpsItem.first()->getRect();
				sectLine->setP1(rect.center());
				sectLine->setP2(QPoint(endXBeamLine, rect.center().y()));
			}
		}
		sectorGraph.append(sectLine);
		pSectorLabel->setSectorGraph(sectorGraph);
	}
	return;
}



/*
**	FUNCTION
**		[VACCO-929]
**		Checks if any sector is represented in the XY point (relative to the synoptic panel)
**
**	PARAMETERS
**		coordX	- panel x coordinate
**		coordY	- panel y coordinate
**
**	RETURNS
**		Pointer to the equivalent SynLhcSectorLabel, if any exists given the XY coords.
**		NULL if no sector is located in the region.
**
**	CAUTIONS
**		None
*/
SynLhcSectorLabel* SynLine::getSectorFromXY(int coordX, int coordY)
{
	int verticalTolerance = 4;
	SynLhcSectorLabel *sectorSelected = NULL;
	for (int idx = 0; idx < pSectorLabelList.count(); idx++){

		QList <QLine *> sectorLines = pSectorLabelList.at(idx)->getSectorGraph();
		for (int idx2 = 0; idx2 < sectorLines.count(); idx2++){
			int sectorStartX = sectorLines.at(idx2)->x1();
			int sectorEndX = sectorLines.at(idx2)->x2();
			int sectorY = sectorLines.at(idx2)->y1();

			if ((coordX >= sectorStartX) && (coordX <= sectorEndX)
				&& (coordY <= sectorY + verticalTolerance) && (coordY >= sectorY - verticalTolerance)){

				sectorSelected = pSectorLabelList.at(idx);
				break;
			}
		}
	}
	return sectorSelected;
}


/*
**	FUNCTION
**		[VACCO-929]	[VACCO-948] [VACCO-1645]
**		If a sector is selected, draws the selection lines around it.
**
**	PARAMETERS
**		painter	- Painter used for background drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::drawSectSelection(QPainter &painter)
{
	QColor colorSelected = QColor(255, 127, 80);
	QColor colorOnWork1 = QColor(0, 0, 0);
	QColor colorOnWork2 = QColor(255, 222, 0);
	QColor colorVented1 = QColor(0, 255, 255);
	QColor colorVented2 = QColor(204, 0, 204);
	int verticalUpperY = 3;
	int verticalLowerY = 2;

	for (int idx = 0; idx < pSectorLabelList.count(); idx++){
		SynLhcSectorLabel *sectLabel = pSectorLabelList.at(idx);
		Sector *sect = sectLabel->getSector();
		QList<QLine *> lines = sectLabel->getSectorGraph();
		for (int idx2 = 0; idx2 < lines.count(); idx2++){
			if (!sect->isSelected() & sect->isOperational()) continue;
			int x1 = lines.at(idx2)->x1();
			int x2 = lines.at(idx2)->x2();
			int y1 = lines.at(idx2)->y1();
			int y2 = lines.at(idx2)->y2();

			if ((sect->isSelected()) && (sect->isOperational())){
				painter.setPen(QPen(colorSelected, 2, Qt::SolidLine));
				painter.drawLine(x1, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1, y1 + verticalUpperY, x2, y2 + verticalUpperY);
			}
			else if (!(sect->isSelected()) && (sect->isOnWork())){
				painter.setPen(QPen(colorOnWork1, 2, Qt::DotLine));
				painter.drawLine(x1, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1, y1 + verticalUpperY, x2, y2 + verticalUpperY);

				painter.setPen(QPen(colorOnWork2, 2, Qt::DotLine));
				painter.drawLine(x1 + 2, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1 + 2, y1 + verticalUpperY, x2, y2 + verticalUpperY);
			}
			else if ((sect->isSelected()) && (sect->isOnWork())){
				painter.setPen(QPen(colorSelected, 2, Qt::DotLine));
				painter.drawLine(x1, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1, y1 + verticalUpperY, x2, y2 + verticalUpperY);

				painter.setPen(QPen(colorOnWork2, 2, Qt::DotLine));
				painter.drawLine(x1 + 2, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1 + 2, y1 + verticalUpperY, x2, y2 + verticalUpperY);
			}
			else if (!(sect->isSelected()) && (sect->isVented())){
				painter.setPen(QPen(colorVented1, 2, Qt::DotLine));
				painter.drawLine(x1, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1, y1 + verticalUpperY, x2, y2 + verticalUpperY);

				painter.setPen(QPen(colorVented2, 2, Qt::DotLine));
				painter.drawLine(x1 + 2, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1 + 2, y1 + verticalUpperY, x2, y2 + verticalUpperY);
			}
			else if ((sect->isSelected()) && (sect->isVented())){
				painter.setPen(QPen(colorSelected, 2, Qt::DotLine));
				painter.drawLine(x1, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1, y1 + verticalUpperY, x2, y2 + verticalUpperY);

				painter.setPen(QPen(colorVented2, 2, Qt::DotLine));
				painter.drawLine(x1 + 2, y1 - verticalLowerY, x2, y2 - verticalLowerY);
				painter.drawLine(x1 + 2, y1 + verticalUpperY, x2, y2 + verticalUpperY);
			}
		}
	}
	return;
}



/*
** FUNCTION
**		[VACCO-527]
**		Defines the Start Tag for a BeamLine.
**		Checks if pLine is a circle, in which case a start tag is only written
**		if AUTOMATIC_TAG is ON (.config file) and if controllable devices  
**		(so with a MP attributed) are found before the first equipment.
**		For non circle BeamLines, the same logic applies for circle lines if and only if
**		AUTOMATIC_TAG is ON (.config file), but if no automatic tag is found,
**		a manual tag is written (if defined in .config file, using START_TAG).
**
** PARAMETERS
**		pLine			- Pointer to beam line for which synoptic line shall be built
**		pStartPart		- Pointer to start beam line part
**		startEqpIdx		- Variable where index of first device on start beam line
**							part will be written
**
** RETURNS
**		String with the pLine start tag
**
** CAUTIONS
**		None
*/
QString SynLine::findMainPartNameBeforeFirstEqp(BeamLine *pLine, BeamLinePart *pStartPart, int startEqpIdx)
{
	if(pLine->isCircle() && pLine->isAutoTagOn())
	{
		Sector *pStartSector = pStartPart->getEqpPtrs()[startEqpIdx]->getSectorBefore();
		const QList<MainPartMap *> &mpMapList = pStartSector->getMainPartMaps();
		if(!mpMapList.isEmpty())
		{
			return QString(mpMapList.first()->getMainPart()->getName());
		}
	} else if(pLine->isAutoTagOn()) {
		const QList<BeamLinePart *> &partList = pLine->getParts();
		bool startPartFound = false;
		for(int partIdx = partList.count() - 1 ; partIdx >= 0 ; partIdx--)
		{
			BeamLinePart *pPart = partList.at(partIdx);
			if((pPart != pStartPart) && (!startPartFound))
			{
				continue;
			}
			startPartFound = true;
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			startEqpIdx = pPart == pStartPart ? startEqpIdx - 1 : nEqpPtrs - 1;
			for(int eqpIdx = startEqpIdx ; eqpIdx >= 0 ; eqpIdx--)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if(!pEqp->getSectorBefore())
				{
					//jumping over passive eqp
					continue;
				}
				Sector *pSector = pEqp->getSectorBefore();
				const QList<MainPartMap *> &mpMapList = pSector->getMainPartMaps();
				if(!mpMapList.isEmpty())
				{
					return QString(mpMapList.first()->getMainPart()->getName());
				}
			}
		}
	}
	return pLine->getTagStartManual();
}

/*
** FUNCTION
**		[VACCO-527]
**		Defines the End Tag for a BeamLine.
**		Checks if pLine is a circle, in which case an end tag is only written
**		if AUTOMATIC_TAG is ON (.config file) and if controllable devices  
**		(so with a MP attributed) are found after the last equipment.
**		For non circle BeamLines, the same logic applies for circle lines if and only if
**		AUTOMATIC_TAG is ON (.config file), but if no automatic tag is found,
**		a manual tag is written (if defined in .config file, using END_TAG).
**
** PARAMETERS
**		pLine			- Pointer to beam line for which synoptic line shall be built
**		pEndPart		- Pointer to end beam line part
**		endEqpIdx		- Variable where index of last device on start beam line
**							part will be written
**
** RETURNS
**		String with the pLine end tag
**
** CAUTIONS
**		None
*/
QString SynLine::findMainPartNameAfterLastEqp(BeamLine *pLine, BeamLinePart *pEndPart, int endEqpIdx)
{
	if(pLine->isCircle() && pLine->isAutoTagOn())
	{
		Eqp *pEndEqp = pEndPart->getEqpPtrs()[endEqpIdx];
		Sector *pEndSector = pEndEqp->getSectorAfter() ? pEndEqp->getSectorAfter() : pEndEqp->getSectorBefore();
		const QList<MainPartMap *> &mpMapList = pEndSector->getMainPartMaps();
		if(!mpMapList.isEmpty())
		{
			return QString(mpMapList.first()->getMainPart()->getName());
		}
	} else if(pLine->isAutoTagOn()) {
		const QList<BeamLinePart *> &partList = pLine->getParts();
		bool endPartFound = false;
		for(int partIdx = 0 ; partIdx < partList.count() ; partIdx++)
		{
			BeamLinePart *pPart = partList.at(partIdx);
			if((pPart != pEndPart) && (!endPartFound))
			{
				continue;
			}
			endPartFound = true;
			Eqp **eqpPtrs = pPart->getEqpPtrs();
			int nEqpPtrs = pPart->getNEqpPtrs();
			endEqpIdx = pPart == pEndPart ? endEqpIdx + 1 : 0;
			for(int eqpIdx = endEqpIdx ; eqpIdx < nEqpPtrs ; eqpIdx++)
			{
				Eqp *pEqp = eqpPtrs[eqpIdx];
				if((!pEqp->getSectorBefore())&&(!(pEqp->getSectorAfter())))
				{
					//jumping over passive eqp
					continue;
				}
				Sector *pSector = pEqp->getSectorAfter() ? pEqp->getSectorAfter() : pEqp->getSectorBefore();

				const QList<MainPartMap *> &mpMapList = pSector->getMainPartMaps();
				if(!mpMapList.isEmpty())
				{
					return QString(mpMapList.first()->getMainPart()->getName());
				}
			}
		}
	}
	return pLine->getTagEndManual();
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SynLine::SynLine()
{
	parent = NULL;
	pBeamLine = NULL;
	pStartPart = pEndPart = NULL;
	startEqpIdx =  endEqpIdx = -1;
	pStartEqp = pEndEqp = NULL;
	mode = DataEnum::Online;
	pColdex = NULL;
	coldexLine = NULL;
	row = -1;
	minSpacing = 2;
	analyzed = reverse = false;
	pLastInvisible = pFirstVisible = NULL;
	pipeColor = VacMainView::getSynPipeColor();
	pipeColorForced = false;
	alwaysIncludeSectorBorder = true;
	pSectorList.clear(); // [VACCO-929]
	isShowMobiles = false;
	clear();
}

SynLine::~SynLine()
{
	if(pColdex)
	{
		pColdex->disconnect(this, mode);
	}
	clear();
}

/*
**	FUNCTION
**		Clear all parameters to 'empty' state
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::clear(void)
{
	while(!eqps.isEmpty())
	{
		delete eqps.takeFirst();
	}
	startXBeamLine = endXBeamLine = y = ascent = descent = 0;
	startConnectX = endConnectX = startConnectY = endConnectY = 0;
	horReady = vertReady = false;
}

/*
**	FUNCTION
**		Analyze which equipment types will be shown in view
**
**	PARAMETERS
**		allVacTypeMask	- OR'ed bit mask for all vacuum types
**		allCryoEqp		- Array all functional types on isolation vacuum
**							to be shown in this view
**		allBeamEqp		- Array all functional types on beam vacuum
**							to be shown in this view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Method also sets pStartEqp and pEndEqp, so without calling this method line will
**		not be drawn correctly
*/
void SynLine::analyzeTypes(unsigned &allVacTypeMask, VacEqpTypeMask & /* allCryoEqpTypes */,
	VacEqpTypeMask &allBeamEqpTypes)
{
	// Check all devices in range
	QList<BeamLinePart *> &parts = pBeamLine->getParts();
	const BeamLinePart *pPart = pStartPart;
	int partIdx;
	for(partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pSearchPart = parts.at(partIdx);
		if(pSearchPart == pPart)
		{
			break;
		}
	}
	int eqpIdx = startEqpIdx;
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	bool isFirst = true;	// LIK 25.11.2008
	while(true)
	{
		do
		{
			Eqp *pEqp = eqpPtrs[eqpIdx];
			if (pEqp->isActiveMayChange())
				listEqpsActiveMayChange.append(pEqp);
			// Check target eqp that position may change
			foreach (Eqp *pEqpPosChange, pEqp->getExtTargets()) {
				if (pEqpPosChange->isPositionMayChange()) {
					listEqpsPositionMayChange.append(pEqpPosChange);
				}
			}
			if(pEqp->isActive(mode))
			{
				if(!pEqp->isSkipOnSynoptic())
				{
					allBeamEqpTypes.append(pEqp->getFunctionalType());
					allVacTypeMask |= pEqp->getVacType();
				}
				if(!analyzed)
				{
					switch(pEqp->getType())
					{
					case EqpType::LineStart:
						pStartEqp = pEqp;
						break;
					case EqpType::LineEnd:
						pEndEqp = pEqp;
						break;
					}
				}
			}
			if(!(pBeamLine->isCircle() && isFirst))	// On circle lines first == last
			{
				if((pPart == pEndPart) && (eqpIdx == endEqpIdx))
				{
					break;
				}
			}
			else
			{
				isFirst = false;
			}
		} while(++eqpIdx < nEqpPtrs);

		if((pPart == pEndPart) && (eqpIdx == endEqpIdx))
		{
			break;
		}
		pPart = NULL;
		partIdx++;
		if(partIdx < parts.count())
		{
			pPart = parts.at(partIdx);
		}
		if(!pPart)
		{
			if(pBeamLine->isCircle())
			{
				partIdx = 0;
				pPart = parts.at(partIdx);
			}
			else
			{
				break;
			}
		}
		eqpPtrs = pPart->getEqpPtrs();
		nEqpPtrs = pPart->getNEqpPtrs();
		eqpIdx = 0;
	}
	if(!analyzed)
	{
		checkForReverse();
		analyzed = true;
	}
}

/*
**	FUNCTION
**		Build all geometry of this line
**
**	PARAMETERS
**		eqpTypes			- Array of functional types to decide which vacuum
**								equipment shall be included
**		hidePassive			- TRUE if passive devices shall not be shown
**
**	RETURNS
**		Number of devices in resulting beam line;
**		-1 - in case of error
**
**	CAUTIONS
**		None
*/
int SynLine::build(const VacEqpTypeMask &eqpTypes, bool hidePassive)
{
	clear();

	QList<BeamLinePart *> &parts = pBeamLine->getParts();
	const BeamLinePart *pPart = pStartPart;
	int partIdx;
	for(partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pSearchPart = parts.at(partIdx);
		if(pSearchPart == pPart)
		{
			break;
		}
	}
	int eqpIdx = startEqpIdx;

	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	bool isFirst = true;	// LIK 25.11.2008

	while(true)
	{
		do
		{
			Eqp *pEqp = eqpPtrs[eqpIdx];
			switch(pEqp->getType())
			{
			case EqpType::Passive:
				if(!hidePassive)	// LIK 21.02.2008
				{
					addDevice(pPart, eqpIdx, pEqp, pEqp->getType());
				}
				break;
			case EqpType::OtherLineStart:
			case EqpType::OtherLineEnd:
				addDevice(pPart, eqpIdx, pEqp, pEqp->getType());
				break;
			case EqpType::VacDevice:
				// Add device if it matches equipment type masks
				if (  pEqp->isActive(mode) || ( isShowMobiles && 
											    ( (pEqp->getFunctionalType() == FunctionalType::VPG) || (pEqp->getFunctionalType() == FunctionalType::VRE) )
											  )  
				   ) {
					if(pEqp->isSkipOnSynoptic())
					{
						continue;
					}
					if(eqpTypes.contains(pEqp->getFunctionalType()) || (pEqp->isSectorBorder() && alwaysIncludeSectorBorder))
					{
						addDevice(pPart, eqpIdx, pEqp, pEqp->getType());
					}
					if(pEqp->getFunctionalType() == FunctionalType::COLDEX && !pColdex)
					{
						pColdex = (EqpCOLDEX *)pEqp;
						pColdex->connect(this, mode);
						QDateTime now = QDateTime::currentDateTime();
						dpeChange(pColdex, "RRC", DataEnum::Own, QVariant(pColdex->getRR1(mode)), mode, now);
					}
				}
				break;
			}
			if((pPart == pEndPart) && (eqpIdx == endEqpIdx))
			{
				// special case: the whole circular line, i.e. start and end eqp are the same
				if(pBeamLine->isCircle())
				{
					if(!isFirst)
					{
						break;
					}
				}
				else
				{
					break;
				}
			}
			isFirst = false;
		} while(++eqpIdx < nEqpPtrs);

		if((pPart == pEndPart) && (eqpIdx == endEqpIdx))
		{
			break;
		}
		pPart = NULL;
		partIdx++;
		if(partIdx < parts.count())
		{
			pPart = parts.at(partIdx);
		}
		if(!pPart)
		{
			if(pBeamLine->isCircle())
			{
				partIdx = 0;
				pPart = parts.at(partIdx);
			}
			else
			{
				break;
			}
		}
		eqpIdx = 0;
		eqpPtrs = pPart->getEqpPtrs();
		nEqpPtrs = pPart->getNEqpPtrs();
	}
	if(!eqps.count())
	{
		return -1;
	}

	if(reverse)
	{
		reverseEqp();
	}
	else
	{
		for(int idx = 0 ; idx < eqps.count() ; idx++)
		{
			SynEqpItem *pItem = eqps.at(idx);
			if(pItem->getEqp())
			{
				if(pItem->getEqp()->isReverseBeamDirection())
				{
					pItem->setBeamDirection(VacIconContainer::RightToLeft);
				}
			}
		}
	}

	// Build geometry of this synoptic line
	setSectorList(); // [VACCO-929]
	setSectorLabelList(); // [VACCO-929]
	buildGroups();
	buildGeometry();
	

	// L.Kopylov 28.01.2010 to display interlock sources
	markInterlockSources();

	// L.Kopylov 28.01.2010 method is made public and is called a bit later... addSectorNameSpace(true);

	// L.Kopylov 21.10.2010 get vacuum type of first device and set pipe color
	// based on that vacuum type
	SynEqpItem *pFirstEqp = NULL;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		pFirstEqp = eqps.at(idx);
		if(pFirstEqp->getType() == EqpType::VacDevice)
		{
			break;
		}
	}
	if(pFirstEqp && (!pipeColorForced))
	{
		int vacType = pFirstEqp->getEqp()->getVacType();
		QColor color;
		VacMainView::setSynLineColor(vacType, color);
		setPipeColor(color);
	}

	return eqps.count();
}

bool SynLine::findDevicePosition(Eqp *pEqpToFind, int &eqpPosition, bool &eqpVisible)
{
	int eqpIdx = 0;
	for(eqpIdx = 0 ; eqpIdx < eqps.count() ; eqpIdx++)
	{
		SynEqpItem *pItem = eqps.at(eqpIdx);
		if(pItem->getEqp() == pEqpToFind)
		{
			eqpPosition = pItem->getConnPoint().x();
			eqpVisible = true;
			return true;
		}
	}
	eqpPosition = 0;
	eqpVisible = false;

	// Check all devices in range - algorithm is taken from analyzeTypes()
	QList<BeamLinePart *> &parts = pBeamLine->getParts();
	const BeamLinePart *pPart = pStartPart;
	int partIdx;
	for(partIdx = 0 ; partIdx < parts.count() ; partIdx++)
	{
		BeamLinePart *pSearchPart = parts.at(partIdx);
		if(pSearchPart == pPart)
		{
			break;
		}
	}
	eqpIdx = startEqpIdx;
	Eqp **eqpPtrs = pPart->getEqpPtrs();
	int nEqpPtrs = pPart->getNEqpPtrs();
	bool isFirst = true;	// LIK 25.11.2008
	while(true)
	{
		do
		{
			Eqp *pEqp = eqpPtrs[eqpIdx];
			if(pEqp->isActive(mode))
			{
				if(!pEqp->isSkipOnSynoptic())
				{
					if(pEqp == pEqpToFind)
					{
						return true;
					}
				}
				if(!analyzed)
				{
					switch(pEqp->getType())
					{
					case EqpType::LineStart:
						pStartEqp = pEqp;
						break;
					case EqpType::LineEnd:
						pEndEqp = pEqp;
						break;
					}
				}
			}
			if(!(pBeamLine->isCircle() && isFirst))	// On circle lines first == last
			{
				if((pPart == pEndPart) && (eqpIdx == endEqpIdx))
				{
					break;
				}
			}
			else
			{
				isFirst = false;
			}
		} while(++eqpIdx < nEqpPtrs);

		if((pPart == pEndPart) && (eqpIdx == endEqpIdx))
		{
			break;
		}
		pPart = NULL;
		partIdx++;
		if(partIdx < parts.count())
		{
			pPart = parts.at(partIdx);
		}
		if(!pPart)
		{
			if(pBeamLine->isCircle())
			{
				partIdx = 0;
				pPart = parts.at(partIdx);
			}
			else
			{
				break;
			}
		}
		eqpPtrs = pPart->getEqpPtrs();
		nEqpPtrs = pPart->getNEqpPtrs();
		eqpIdx = 0;
	}
	return false;
}

/*
** FUNCTION
**		Mark devices which are sources of interlock for valves.
**
** PARAMETERS
**		None
**
** RETURNS
**		None
**
** CAUTIONS
**		Only valves which are in this view are considered
*/
void SynLine::markInterlockSources(void)
{
	haveInterlockSources = false;

	// L.Kopylov 17.01.2014 DataPool &pool = DataPool::getInstance();

	int eqpIdx = 0;
	for(eqpIdx = 0 ; eqpIdx < eqps.count() ; eqpIdx++)
	{
		SynEqpItem *pItem = eqps.at(eqpIdx);
		if(pItem->getType() != EqpType::VacDevice)
		{
			continue;
		}
		Eqp *pEqp = pItem->getEqp();
		if(pEqp->isIntlSourcePrev())
		{
			if(reverse)
			{
				pItem->setIntlSourceBackward(true);
			}
			else
			{
				pItem->setIntlSourceForward(true);
			}
			haveInterlockSources = true;
		}
		if(pEqp->isIntlSourceNext())
		{
			if(reverse)
			{
				pItem->setIntlSourceForward(true);
			}
			else
			{
				pItem->setIntlSourceBackward(true);
			}
			haveInterlockSources = true;
		}
	}
}

/*
**	FUNCTION
**		Move all horizontal coordinates by given offset
**
**	PARAMETERS
**		delta			- Offset to be added
**		touchConnect	- true of connection points shall also be touched
**
** RETURNS
**		Pointer to list of child line parameters which shall be moved, or
**		empty list if no child lines shall be moved.
**
** CAUTIONS
**		None
*/
QList<ChildSynLine *> *SynLine::moveHorizontally(int delta, bool touchConnect)
{
/*
printf("SynLine::moveHorizontally(%s, %d, %d)\n", pBeamLine->getName(), delta, (int)touchConnect);
fflush(stdout);
if(delta == 132)
{
	char *pTrap = NULL;
	*pTrap = '\0';
}
*/
	QList<ChildSynLine *> *pChildList = new QList<ChildSynLine *>();

	if(touchConnect)
	{
		startConnectX += delta;
		endConnectX += delta;
	}
	const char *childName;
	bool found = false;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynEqpItem *pItem = eqps.at(idx);
		QRect &rect = pItem->getRect();
		rect.moveLeft(rect.x() + delta);
		QPoint &connPoint = pItem->getConnPoint();
		connPoint.setX(connPoint.x() + delta);
		if(pItem->getTextHeight())
		{
			pItem->setTextStart(pItem->getTextStart() + delta);
			pItem->setTextEnd(pItem->getTextEnd() + delta);
		}
		switch(pItem->getType())
		{
		case EqpType::OtherLineStart:
			pChildList->append(new ChildSynLine(pItem->getName(), true));
			break;
		case EqpType::OtherLineEnd:
			// Check if this line is already aded - if both start and end are on this line
			childName = pItem->getName();
			found = false;
			for(int childIdx = 0 ; childIdx < pChildList->count() ; childIdx++)
			{
				ChildSynLine *pChild = pChildList->at(childIdx);
				if(childName == pChild->getName())
				{
					found = true;
					break;
				}
			}
			if(!found)
			{
				pChildList->append(new ChildSynLine(childName, false));
			}
			break;
		}
	}
	startXBeamLine += delta;
	endXBeamLine += delta;
	startX += delta;
	endX += delta;
	if(pChildList->isEmpty())
	{
		delete pChildList;
		return NULL;
	}
	return pChildList;
}

/*
**	FUNCTION
**		Move all vertical coordinates by given offset
**
**	PARAMETERS
**		delta			- Offset to be added
**		touchConnect	- true of connection points shall also be touched
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::moveVertically(int delta, bool touchConnect)
{
	if(touchConnect)
	{
		startConnectY += delta;
		endConnectY += delta;
	}
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynEqpItem *pItem = eqps.at(idx);
		QRect &rect = pItem->getRect();
		rect.moveTop(rect.y() + delta);
		QPoint &connPoint = pItem->getConnPoint();
		connPoint.setY(connPoint.y() + delta);
	}
	y += delta;
}

/*
**	FUNCTION
**		Increase 'row' index of this line if current row value
**		>= given row value
**
**	PARAMETERS
**		baseRowIdx	- Base row value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::increaseRow(int baseRowIdx)
{
	if(row < baseRowIdx)
	{
		return;
	}
	row++;
}

/*
**	FUNCTION
**		Find X-coordinate of item with given index
**
**	PARAMETERS
**		eqpIdx	- Index item
**
**	RETURNS
**		X-coordinate of item with given index; or
**		-1 if index is out of range
**
**	CAUTIONS
**		None
*/
int SynLine::getEqpX(int eqpIdx)
{
	SynEqpItem *pItem = eqps.at(eqpIdx);
	if(!pItem)
	{
		return -1;
	}
	return pItem->getRect().x();
}

/*
**	FUNCTION
**		Move all equipment in this line starting from given device by given offset
**		in horizontal direction
**
**	PARAMETERS
**		startItemIdx	- Index of start item to move
**		delta			- Offset to be added to horizontal coordinates
**
**	RETURNS
**		Pointer to list of child line parameters which shall be moved, or
**		NULL if no child lines shall be moved.
**
**	CAUTIONS
**		it is responsability of caller to delete retuned list
*/
QList<ChildSynLine *> *SynLine::moveLinePart(int startItemIdx, int delta)
{
/*
printf("SynLine::moveLinePart(%s, %d, %d)\n", pBeamLine->getName(), startItemIdx, delta);
fflush(stdout);
*/
	// Check if there is a passive device BEFORE startIdx that shall be moved or
	// extended as a result of move operation
	SynEqpItem *pStartItem = eqps.at(startItemIdx);
	if(!pStartItem)
	{
		return NULL;
	}
	QList<ChildSynLine *> *pChildList = new QList<ChildSynLine *>();

	bool	extendPassive = false;
	QPoint &startConnPoint = pStartItem->getConnPoint();

	SynEqpItem *pItem;
	int idx;
	for(idx = startItemIdx - 1 ; idx >= 0 ; idx--)
	{
		pItem = eqps.at(idx);
		if(pItem->getType() == EqpType::VacDevice)
		{
			extendPassive = true;
		}
		else if(pItem->getType() == EqpType::Passive)
		{
			QRect &rect = pItem->getRect();
			if((rect.x() + rect.width()) >= startConnPoint.x())
			{
				if(extendPassive)
				{
					rect.setWidth(rect.width() + delta);
				}
				else
				{
					rect.moveLeft(rect.x() + delta);
				}
			}
			break;
		}
	}

	// Move equipment after startIdx
	for(idx = startItemIdx + 1 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		QRect &rect = pItem->getRect();
		QPoint &connPoint = pItem->getConnPoint();
		rect.moveLeft(rect.x() + delta);
		connPoint.setX(connPoint.x() + delta);
		if(pItem->getTextHeight())
		{
			pItem->setTextStart(pItem->getTextStart() + delta);
			pItem->setTextEnd(pItem->getTextEnd() + delta);
		}
		if(pItem == pStartItem)
		{
			continue;	// First shall not be taken into children consideration
		}
		const char *childName;
		bool found = false;
		switch(pItem->getType())
		{
		case EqpType::OtherLineStart:
			pChildList->append(new ChildSynLine(pItem->getName(), true));
			break;
		case EqpType::OtherLineEnd:
			// Check if this line is already aded - if both start and end are on this line
			childName = pItem->getName();
			found = false;
			for(int childIdx = 0 ; childIdx < pChildList->count() ; childIdx++)
			{
				ChildSynLine *pChild = pChildList->at(childIdx);
				if(childName == pChild->getName())
				{
					found = true;
					break;
				}
			}
			if(!found)
			{
				pChildList->append(new ChildSynLine(childName, false));
			}
			break;
		}
	}
	endXBeamLine += delta;
	endX += delta;
	if(pChildList->isEmpty())
	{
		delete pChildList;
		return NULL;
	}
	return pChildList;
}

/*
**	FUNCTION
**		Move all equipment in this line starting from given screen coordinate
**		by given offset in horizontal direction
**
**	PARAMETERS
**		coord		- Screen coordinate after which equipment shall be moved
**		delta		- Offset to be added to horizontal coordinates
**
**	RETURNS
**		None
**
**	CAUTIONS
**		Child lines are NOT moved by this method
*/
void SynLine::movePartAfter(int coord, int delta)
{
/*
printf("SynLine::movePartAfter(%s, %d, %d)\n", pBeamLine->getName(), coord, delta);
fflush(stdout);
*/
	bool moved = false;
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynEqpItem *pItem = eqps.at(idx);
		QRect &rect = pItem->getRect();
		QPoint &connPoint = pItem->getConnPoint();
		if((rect.x() < coord) && (connPoint.x() < coord))
		{
			break;
		}
		moved = true;
		rect.moveLeft(rect.x() + delta);
		connPoint.setX(connPoint.x() + delta);
		if(pItem->getTextHeight())
		{
			pItem->setTextStart(pItem->getTextStart() + delta);
			pItem->setTextEnd(pItem->getTextEnd() + delta);
		}
	}
	if(moved)
	{
		endXBeamLine += delta;
		endX += delta;
		// TODO ??? WHAT IS 'width' ??? width += delta;
	}
}

/*
**	FUNCTION
**		Find equipment in this line which is start or end of another line
**
**	PARAMETERS
**		forLine	- Name of another line where start or end is required
**		type	- Type of connection (start or end)
**
**	RETURNS
**		Index of other line connection equipment;
**		-1 if not found
**
**	CAUTIONS
**		Function does not check that 'type' argument really corresponds to start/end of
**		another line - just checks that equipment type is equal to requested one.
*/
int SynLine::findConnectEqp(const char *forLine, int type)
{
/*
printf("SynLine::findConnectEqp(%s, for %s %d)\n", pBeamLine->getName(), forLine, type);
fflush(stdout);
*/
	int n = 0;
	for(n = eqps.count() - 1 ; n >= 0 ; n--)
	{
		SynEqpItem *pItem = eqps.at(n);
		if(pItem->getType() != type)
		{
			continue;
		}
		if(!strcmp(pItem->getEqp()->getName(), forLine))
		{
/*
printf("         SynLine::findConnectEqp(): found %d\n", n);
fflush(stdout);
*/
			return n;
		}
	}
/*
printf("         SynLine::findConnectEqp(): not found\n");
fflush(stdout);
*/
	return -1;
}

/*
**	FUNCTION
**		Check if this line has part of sector located on another line:
**		this is the case for sector LTE&LTL in LEIR
**
**	PARAMETERS
**		connstartXBeamLine	- [out] If this part has 'part away' - X-coordinate for start
**						of possible connection line range
**		connendXBeamLine	- [out] If this part has 'part away' - X-coordinate for end
**						of possible connection line range
**
**	RETURNS
**		Name of another beam line where other part of vacuum sector on this
**		line shall be found, or
**		NULL if there is no 'part away' for this line
**
**	CAUTIONS
**		None
*/
const char *SynLine::havePartAway(int &connstartXBeamLine, int &connendXBeamLine)
{
	const char *pOtherLineName = NULL;
	DataPool &pool = DataPool::getInstance();
	if(!strcmp(pBeamLine->getName(), "LTL"))
	{
		QList<BeamLine *> &lines = pool.getLines();
		for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			BeamLine *pLine = lines.at(lineIdx);
			QList<BeamLinePart *> &parts = pLine->getParts();
			for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
			{
				BeamLinePart *pPart = parts.at(partIdx);
				if(!strcmp(pPart->getName(), "LTE"))
				{
					pOtherLineName = pLine->getName();
					break;
				}
			}
		}
	}
	else if(!strcmp(pBeamLine->getName(), "LTE"))
	{
		QList<BeamLine *> &lines = pool.getLines();
		for(int lineIdx = 0 ; lineIdx < lines.count() ; lineIdx++)
		{
			BeamLine *pLine = lines.at(lineIdx);
			QList<BeamLinePart *> &parts = pLine->getParts();
			for(int partIdx = 0 ; partIdx < parts.count() ; partIdx++)
			{
				BeamLinePart *pPart = parts.at(partIdx);
				if(!strcmp(pPart->getName(), "LTL"))
				{
					pOtherLineName = pLine->getName();
					break;
				}
			}
		}
	}
	if(pOtherLineName)	// Find appropriate connection location, take just before last device in sector
	{
		Sector *pSector = pool.findSectorData("LTE-LTL");
		if(!pSector)
		{
			return NULL;
		}
		bool gotStart = false;
		for(int idx = 0 ; idx < eqps.count() ; idx++)
		{
			SynEqpItem *pItem = eqps.at(idx);
			if(pItem->getType() == EqpType::VacDevice)
			{
				if((pItem->getEqp()->getSectorBefore() == pSector) ||
						(pItem->getEqp()->getSectorAfter() == pSector))
				{
					QRect &rect = pItem->getRect();
					if(!gotStart)
					{
						connstartXBeamLine = rect.x() + rect.width();
						gotStart = true;
					}
					connendXBeamLine = rect.x();
				}
			}
		}
		if(connendXBeamLine <= connstartXBeamLine)
		{
			connendXBeamLine = endXBeamLine;
		}
	}
	return pOtherLineName;
}

/*
**	FUNCTION
**		Notify synoptic line that geometry calculation has been finished,
**		line shall move all active icons to calculated positions.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::finishGeometry(void)
{
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynEqpItem *pItem = eqps.at(idx);
		pItem->moveIcon();
	}
}

void SynLine::addAlarmIcons(void)
{
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynEqpItem *pItem = eqps.at(idx);
		if(!pItem->getIcon())
		{
			continue;
		}
		Eqp *pEqp = pItem->getEqp();
		if(!pEqp)
		{
			continue;
		}
		const QList<Eqp *> &targets = pEqp->getExtTargets();
		if(targets.isEmpty())
		{
			continue;
		}
		int extY = 0;
		VacIconContainer::Direction direction = pItem->getIcon()->getSlaveDirection();
		switch(direction)
		{
		case VacIconContainer::Up:	// Above line
			extY = pItem->getRect().top() - (pItem->isIntlSourceBackward() || (pItem->isIntlSourceForward()) ? 9 : 1);
			break;
		case VacIconContainer::Down:	// Below line
			extY = pItem->getRect().bottom() + (pItem->isIntlSourceBackward() || (pItem->isIntlSourceForward()) ? 9 : 1);
			break;
		default:	// to make gcc happy
			break;
		}
		int centerX = pItem->getRect().center().x();
		VacIcon *pIcon = NULL;
		SynEqpItem *pNewItem = NULL;
		VacEqpTypeMask	dummyMask;
		for(int targetIdx = 0 ; targetIdx < targets.count() ; targetIdx++)
		{
			pEqp = targets.at(targetIdx);
			pIcon = VacIcon::getIcon(pEqp, parent);
			if(!pIcon)
			{
				continue;
			}
			pIcon->setDirection(direction);
			VacIconGeometry geometry = pIcon->getGeometry(dummyMask);
			// Finally - add new device to list
			pNewItem = new SynEqpItem();
			pNewItem->setBeamLinePart(pItem->getBeamLinePart());
			pNewItem->setEqpIdx(pItem->getEqpIdx());
			pNewItem->setEqp(pEqp);
			pNewItem->setIcon(pIcon);
			pNewItem->setPipeColor(pipeColor);
			pNewItem->setType(EqpType::VacDevice);
			if((direction == VacIconContainer::Up) && (!targetIdx))	// Add height for 1st icon above
			{
				extY -= geometry.getHeight();
			}
			QRect rect(centerX - (geometry.getWidth() >> 1), extY, geometry.getWidth(), geometry.getHeight());
			pNewItem->setRect(rect);
			if(direction == VacIconContainer::Up)
			{
				extY -= geometry.getHeight() + 1;
			}
			else
			{
				extY += geometry.getHeight() + 1;
			}
			pNewItem->setConnPoint(rect.center());
			pNewItem->setVerticalPos(pItem->getVerticalPos());
			pNewItem->setSectorBorder(false);
			//eqps.append(pNewItem);
			eqps.insert(idx + 1, pNewItem);
			pIcon->move(rect.x(), rect.y());
		}
		if(pNewItem)	// Last created icon - on maximum distance from beam line
		{
			QRect rect(pNewItem->getRect());
			if((- rect.y()) > ascent)
			{
				ascent = - rect.y();
			}
			if(rect.bottom() > descent)
			{
				descent = rect.bottom();
			}
		}
	}
}

/*
**	FUNCTION
**		Activate icons which are visible, deactivate icons which are not visible
**
**	PARAMETERS
**		viewStart	- Start position of visible part
**		viewWidth	- width of visible part
**		dragging	- true if this method is called from scroll bar dragging
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::setViewStart(int viewStart, int viewWidth, bool /* dragging */)
{
	int endPos = viewStart + viewWidth;
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynEqpItem *pItem = eqps.at(idx);
		VacIcon *pIcon = pItem->getIcon();
		if(!pIcon)
		{
			continue;
		}
		QRect &rect = pItem->getRect();
		if(rect.x() > endPos)
		{
			pIcon->disconnect();
		}
		else if((rect.x() + rect.width()) < viewStart)
		{
			pIcon->disconnect();
		}
		else
		{
			pIcon->connect();
		}
	}
}

/*
**	FUNCTION
**		Draw synoptic line, connection lines and all equipment.
**
**	PARAMETERS
**		painter		- Painter used for background drawing
**		viewStart	- X-coordinate of start of area to be painted
**		viewEnd		- X-coordinate of start of area to be painted
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::draw(QPainter &painter, int viewStart, int viewEnd)
{
	// It can be that nothing is visible
	if((startX >= viewEnd) && ((!pStartEqp) || (startConnectX >= viewEnd)))
	{
		return;
	}
	if((endX <= viewStart) && ((!pEndEqp) || (endConnectX <= viewStart)))
	{
		return;
	}

	// Something is visible - draw
	drawEqp(painter, viewStart, viewEnd);
	drawBeamPipe(painter, viewStart, viewEnd);
	drawSectSelection(painter); // [VACCO-929]
	return;
}

/*
**	FUNCTION
**		Fix last invisible and first visible equipments before applying
**		new equipment type mask, they are fixed in order to be able to set
**		start coordinate to 'the same' position after applying new mask
**
**	PARAMETERS
**		viewStart	- Start X-coordinate of visible part
**		newMask		- New equipment type mask that will be applied
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::fixStart(int viewStart, const VacEqpTypeMask &newMask)
{
	pLastInvisible = pFirstVisible = NULL;
	if(!viewStart)
	{
		return;
	}
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynEqpItem *pItem = eqps.at(idx);
		QRect &rect = pItem->getRect();
		if(pItem->getEqp()->getType() == EqpType::VacDevice)
		{
			if(!newMask.contains(pItem->getEqp()->getFunctionalType()))
			{
				continue;
			}
		}
		if(rect.right() < viewStart)
		{
			pLastInvisible = pItem->getEqp();
		}
		else
		{
			pFirstVisible = pItem->getEqp();
			break;
		}
	}
}

/*
**	FUNCTION
**		Find recommended visible start position for this line based
**		on previously fixed last invisible and first visible devices
**
**	PARAMETERS
**		startMin	- Variable where start of recommended coordinate
**						range will be written, -1 if there is no recommendation
**		startMaxn	- Variable where end of recommended coordinate
**						range will be written, -1 if there is no recommendation
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::findRecommendedStart(int &startMin, int &startMax)
{
	startMin = startMax = -1;
	if(pLastInvisible || pFirstVisible)
	{
		for(int idx = 0 ; idx < eqps.count() ; idx++)
		{
			SynEqpItem *pItem = eqps.at(idx);
			if(pLastInvisible)
			{
				if(pItem->getEqp() == pLastInvisible)
				{
					startMin = pItem->getRect().right();
				}
			}
			if(pFirstVisible)
			{
				if(pItem->getEqp() == pFirstVisible)
				{
					startMax = pItem->getRect().left();
					break;
				}
			}
		}
	}
	if((startMin >= 0) && (startMax >= 0) && (startMin > startMax))
	{
		int tmp = startMax;
		startMax = startMin;
		startMin = tmp;
	}
}

/*
**	FUNCTION
**		Check if tooltip shall be shown for given mouse pointer position
**
**	PARAMETERS
**		point	- Mouse pointer position
**		text	- Variable where tool tip text shall be written
**		rect	- Variable where boundaries for returned tooltip text shall
**					be written
**
**	RETURNS
**		true	- If tooltip text shall be shown for given mouse position;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool SynLine::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	if(point.y() < (y - PASSIVE_ELEM_HEIGHT / 2))
	{
		return false;
	}
	if(point.y() > (y + PASSIVE_ELEM_HEIGHT / 2))
	{
		return false;
	}
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynEqpItem *pItem = eqps.at(idx);
		if(pItem->getIcon())
		{
			continue;
		}
		if(pItem->getRect().left() > point.x())
		{
			return false;
		}
		if(pItem->getRect().contains(point))
		{
			text = pItem->getName();
			rect = pItem->getRect();
			return true;
		}
	}
	return false;
}


/*
**	FUNCTION
**		Check if equipment in line shall be drawn in reverse order.
**		This can be required if:
**		1) Finish of this line has a type RO or LO
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::checkForReverse(void)
{
	reverse = false;
	DataPool &pool = DataPool::getInstance();
	if(pool.isLhcFormat())
	{
		if(pBeamLine->getStartLine())
		{
			switch(pBeamLine->getStartType())
			{
			case BeamLineConnType::LeftIn:
			case BeamLineConnType::RightIn:
				reverse = true;
				break;
			}
		}
		/* TI2/TI8 are not reverse L.Kopylov 21.10.2010
		if(pBeamLine->getEndLine())
		{
			switch(pBeamLine->getEndType())
			{
			case BeamLineConnType::LeftOut:
			case BeamLineConnType::RightOut:
				reverse = true;
				break;
			}
		}
		*/
	}
	else
	{
		// If line end has type RO or LO - then place all equipment in reverse order
		if((!pStartEqp) && pEndEqp &&
			((pBeamLine->getEndType() == BeamLineConnType::LeftOut) ||
		  	(pBeamLine->getEndType() == BeamLineConnType::RightOut)))
		{
			reverse = true;
			// Use start as end
			pStartEqp = pEndEqp;
			pEndEqp = NULL;
		}
		// L.Kopylov 18.05.2011 If line start has type LI or RI - then place all equipment in reverse order
		else if((!pEndEqp) && pStartEqp &&
			((pBeamLine->getStartType() == BeamLineConnType::LeftIn) ||
		  	(pBeamLine->getStartType() == BeamLineConnType::RightIn)))
		{
			reverse = true;
			// Use end as start
			pEndEqp = pStartEqp;
			pStartEqp = NULL;
		}
		
	}
}

/*
**	FUNCTION
**		Add device to list of devices in this synoptics line
**
**	PARAMETERS
**		pPArt	- Pointer to beam line part where device is taken
**		eqpIdx	- Index of device in beam line part
**		pEqp	- Device to be added
**		type	- Device type, see EqpType class
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::addDevice(const BeamLinePart *pPart, int eqpIdx, Eqp *pEqp, int type)
{
	int				width = 0, height = 0, x = 0, y = 0, verticalPos = 0;
	VacIcon			*pIcon = NULL;
	bool			isSectorBorder = false;
	VacEqpTypeMask	dummyMask;
	QString			dpName;
	Eqp				*pSectorEqp = NULL;

	// Final decision - if device shall be added plus calculate some geometry parameters
	switch(type)
	{
	case EqpType::Passive:
		width = (int)(pEqp->getLength() * metersToScreen);
		if(!width)
		{
			return;
		}
		if(width < MIN_PASSIVE_WIDTH)
		{
			width = MIN_PASSIVE_WIDTH;
		}
//		if( width > 12 ) width = 12;
		height = PASSIVE_ELEM_HEIGHT;
		y = - height / 2;
		/* NOBREAK */
	case EqpType::OtherLineStart:
	case EqpType::OtherLineEnd:
		break;
	case EqpType::VacDevice:
		if((pIcon = VacIcon::getIcon(pEqp, parent)))
		{
			pIcon->setMode(mode);
			VacIconGeometry geom = pIcon->getGeometry(dummyMask);
			width = geom.getWidth();
			height = geom.getHeight();
			x = geom.getX();
			verticalPos = geom.getVerticalPos();
			if(verticalPos < 0)	// Below line
			{
				y = height - geom.getY();
			}
			else
			{
				y = geom.getY();
			}
		}
		else
		{
			// Vacuum devices without icon are only included if they are sector borders
			if(!pEqp->isSectorBorder())
			{
				return;
			}
		}
		isSectorBorder = pEqp->isSectorBorder();
		break;
	case EqpType::SectorBefore:
		if(pEqp->getSectorBefore()->isOuter())
		{
			return;
		}
		dpName = EqpSECT_VPI_SUM::getDpNameForSector(pEqp->getSectorBefore());
		if(dpName.isEmpty())
		{
			printf("SynLine::addDevice(%s = sector before): no DP name from EqpSECT_VPI_SUM\n", pEqp->getSectorBefore()->getName());
			fflush(stdout);
			return;
		}
		pSectorEqp = DataPool::getInstance().findEqpByDpName(dpName.toLatin1());
		if(!pSectorEqp)
		{
			printf("SynLine::addDevice(%s = sector before): no DP %s\n", pEqp->getSectorBefore()->getName(), dpName.toLatin1().constData());
			fflush(stdout);
			return;
		}
		if((pIcon = VacIcon::getIcon(pSectorEqp, parent)))
		{
			pIcon->setMode(mode);
			VacIconGeometry geom = pIcon->getGeometry(dummyMask);
			width = 16;		// Minimum width of sector icon
			height = geom.getHeight();
			x = geom.getX();
			verticalPos = geom.getVerticalPos();
			y = geom.getY();
		}
		else
		{
			printf("SynLine::addDevice(%s = sector before): no icon for %s\n", pEqp->getSectorBefore()->getName(), pSectorEqp->getName());
			fflush(stdout);
			return;
		}
		break;
	case EqpType::SectorAfter:
		if(pEqp->getSectorAfter()->isOuter())
		{
			return;
		}
		dpName = EqpSECT_VPI_SUM::getDpNameForSector(pEqp->getSectorAfter());
		if(dpName.isEmpty())
		{
			printf("SynLine::addDevice(%s = sector after): no DP name from EqpSECT_VPI_SUM\n", pEqp->getSectorAfter()->getName());
			fflush(stdout);
			return;
		}
		pSectorEqp = DataPool::getInstance().findEqpByDpName(dpName.toLatin1());
		if(!pSectorEqp)
		{
			printf("SynLine::addDevice(%s = sector after): no DP %s\n", pEqp->getSectorAfter()->getName(), dpName.toLatin1().constData());
			fflush(stdout);
			return;
		}
		if((pIcon = VacIcon::getIcon(pSectorEqp, parent)))
		{
			pIcon->setMode(mode);
			VacIconGeometry geom = pIcon->getGeometry(dummyMask);
			width = 16;		// Minimum width of sector icon
			height = geom.getHeight();
			x = geom.getX();
			verticalPos = geom.getVerticalPos();
			y = geom.getY();
		}
		else
		{
			printf("SynLine::addDevice(%s = sector after): no icon for %s\n", pEqp->getSectorAfter()->getName(), pSectorEqp->getName());
			fflush(stdout);
			return;
		}
		break;
	}
	// Finally - add new device to list
	SynEqpItem *pItem = new SynEqpItem();
	pItem->setBeamLinePart(pPart);
	pItem->setEqpIdx(eqpIdx);
	pItem->setEqp(pEqp);
	if(pIcon)
	{
		pItem->setIcon(pIcon);
		pItem->setPipeColor(pipeColor);
	}
	pItem->setType(type);
	QRect rect(x, y, width, height);
	pItem->setRect(rect);
	pItem->setConnPoint(QPoint(0, 0));
	pItem->setVerticalPos(verticalPos);
	pItem->setSectorBorder(isSectorBorder);
	eqps.append(pItem);
}

/*
**	FUNCTION
**		Reverse order of list of equipment
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::reverseEqp(void)
{
	QList<SynEqpItem *> newList;
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynEqpItem *pItem = eqps.at(idx);
		bool reverseEqpBeamDirection = true;
		if(pItem->getEqp())
		{
			if(pItem->getEqp()->isReverseBeamDirection())
			{
				reverseEqpBeamDirection = false;
			}
		}
		if(reverseEqpBeamDirection)
		{
			pItem->setBeamDirection(VacIconContainer::RightToLeft);
		}
		newList.append(pItem);
	}
	eqps.clear();
	eqps = newList;
}

/*
**	FUNCTION
**		Join vacuum devices sitting at the same location and having appropriate
**		verticalPos to groups of devices
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::buildGroups(void)
{
	int				groupId = 1;
	int				verticalPos;
	VacEqpTypeMask	dummyMask;

	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynEqpItem *pItem = eqps.at(idx);
		if((pItem->getType() != EqpType::VacDevice) || (pItem->getGroupId() != 0))
		{
			continue;
		}
		if(!pItem->getIcon())
		{
			continue;
		}
		// Devices with verticalPos = 0 are not joined to groups
		if(!(verticalPos = pItem->getIcon()->getGeometry(dummyMask).getVerticalPos()))
		{
			continue;
		}
		for(int nextIdx = idx + 1 ; nextIdx < eqps.count() ; nextIdx++)
		{
			SynEqpItem *pNext = eqps.at(nextIdx);
			if(pNext->getType() != EqpType::VacDevice)
			{
				continue;
			}
			if(strcmp(pNext->getEqp()->getSurveyPart(), pItem->getEqp()->getSurveyPart()))
			{
				break;
			}
			if(pNext->getEqp()->getStart() != pItem->getEqp()->getStart())
			{
				break;
			}
			if(!pNext->getIcon())
			{
				continue;
			}
			if(pNext->getIcon()->getGeometry(dummyMask).getVerticalPos() != verticalPos)
			{
				continue;
			}
			if(!pItem->getGroupId())
			{
				if(verticalPos == 1)
				{
					pItem->setGroupId(groupId);
				}
				else
				{
					pItem->setGroupId(-groupId);
				}
				groupId++;
			}
			pNext->setGroupId(pItem->getGroupId());
		}
	}
}

/*
**	FUNCTION
**		Build geometry of this line
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::buildGeometry(void)
{
	float			connPos	= -1000.;
	int				minPos, minRectPos, connPoint = 0, idx;
	bool			checkForStartFixed = false;
	const BeamLinePart	*pPart = NULL;
	
	// [VACCO-527] Get sizes of start and end tags
	sizeStartTag = parent->fontMetrics().size(Qt::TextSingleLine, getTagStart());
	sizeEndTag = parent->fontMetrics().size(Qt::TextSingleLine, getTagEnd());

	SynEqpItem *pItem;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		QRect &rect = pItem->getRect();
		QPoint &itemConnPoint = pItem->getConnPoint();
		switch(pItem->getType())
		{
		case EqpType::OtherLineStart:
		case EqpType::OtherLineEnd:
		case EqpType::Passive:
			checkForStartFixed = false;
			rect.moveTop(-rect.height() / 2);
			// Find minimum location where this device can start wihtout overlapping with previous
			minPos = findMinPos(pItem, true, minRectPos);
			if(minPos < connPoint)
			{
				minPos = connPoint;
			}
			if((pItem->getBeamLinePart() != pPart) && (minPos < (connPoint + minSpacing - rect.x())))
			{
				minPos = connPoint + minSpacing - rect.x();
			}
			else if((pItem->getEqp()->getStart() == connPos) && (minPos <= connPoint))
			{
				minPos = connPoint - rect.x();
			}
			else if(reverse || pItem->getBeamLinePart()->isReverse())
			{
				if((pItem->getEqp()->getStart() < connPos) && (minPos <= (connPoint + minSpacing)))
				{
					minPos = connPoint + minSpacing;
				}
			}
			else
			{
				if((pItem->getEqp()->getStart() > connPos) && (minPos <= (connPoint + minSpacing)))
				{
					minPos = connPoint + minSpacing;
				}
			}
			rect.moveLeft(minPos);
			itemConnPoint.setX(minPos + rect.width() / 2);
			connPos = pItem->getEqp()->getStart();
			connPoint = minPos;
			break;
		case EqpType::VacDevice:
			checkForStartFixed = true;
			itemConnPoint.setY(0);
			if(pItem->getGroupId() < 0)	// Group below beam line
			{
				itemConnPoint.setY(COMMON_ACTIVE_OFFSET);
			}
			else if(pItem->getGroupId() > 0)	// Group above beam line
			{
				itemConnPoint.setY(-COMMON_ACTIVE_OFFSET);
			}
			rect.moveTop(itemConnPoint.y() - rect.y());
			minPos = findMinPos(pItem, true, minRectPos);
			if((pItem->getBeamLinePart() != pPart) && (minPos < (connPoint - rect.x())))
			{
				minPos = minRectPos = connPoint + minSpacing - rect.x();
			}
			else if((pItem->getEqp()->getStart() == connPos) && (minPos < (connPoint - rect.x())))
			{
				minPos = minRectPos = connPoint - rect.x();
			}
			else if(reverse || pItem->getBeamLinePart()->isReverse())
			{
				if((pItem->getEqp()->getStart() < connPos) && (minPos <= (connPoint - rect.x())))
				{
					minPos = minRectPos = connPoint + minSpacing - rect.x();
				}
			}
			else
			{
				if((pItem->getEqp()->getStart() > connPos) && (minPos <= (connPoint - rect.x())))
				{
					minPos = minRectPos = connPoint + minSpacing - rect.x();
				}
			}
			itemConnPoint.setX(minPos);
			if((itemConnPoint.x() - rect.x()) < minRectPos)
			{
				itemConnPoint.setX(itemConnPoint.x() + (minRectPos - (itemConnPoint.x() - rect.x())));
			}
			rect.moveLeft(itemConnPoint.x() - rect.x());
			// Coordinates for sector names (if any)
			if(pItem->isSectorBorder())
			{
				pItem->setTextStart(itemConnPoint.x() - pItem->getTextStart());
//qDebug("%s: setting textEnd to %d + %d\n", pItem->getName(), itemConnPoint.x(), pItem->getTextEnd());
				pItem->setTextEnd(itemConnPoint.x() + pItem->getTextEnd());
			}
			connPos = pItem->getEqp()->getStart();
			connPoint = itemConnPoint.x();
			// Connection point for group is FOR SURE before X coordinate of icon LIK 14.05.2008
			if(pItem->getGroupId())
			{
				connPoint = rect.x();
			}
			break;
		}
		pPart = pItem->getBeamLinePart();
		if(checkForStartFixed)	// Check if start of previous passive element shall be fixed
		{
			for(int prevIdx = idx - 1 ; prevIdx >= 0 ; prevIdx--)
			{
				SynEqpItem *pPrev = eqps.at(prevIdx);
				if(pPrev->getBeamLinePart() != pItem->getBeamLinePart())
				{
					break;
				}
				if(pPrev->getType() != EqpType::Passive)
				{
					continue;
				}
				if((pPrev->getEqp()->getStart() + pPrev->getEqp()->getLength()) <= pItem->getEqp()->getStart())
				{
					pPrev->setStartFixed(true);
				}
				break;
			}
		}
		// Update line geometry limits
		if((- rect.y()) > ascent)
		{
			ascent = - rect.y();
		}
		if((rect.y() + rect.height()) > descent)
		{
			descent = rect.y() + rect.height();
		}
		int minX = rect.x() < itemConnPoint.x() ? rect.x() : itemConnPoint.x();
		int maxX = (rect.x() + rect.width()) > itemConnPoint.x() ?
				rect.x() + rect.width() : itemConnPoint.x();
		if(minX < startXBeamLine)
		{
			startXBeamLine = minX;
		}
		if(maxX > endXBeamLine)
		{
			endXBeamLine = maxX;
		}

	}

	// [VACCO-527] Defines the image start and end, depending if start/end tags are applied
	if(!reverse)
	{
		if(getTagStart() != "") startX = startXBeamLine - sizeStartTag.width() - MARGIN_LINE_TEXT;
		else startX = startXBeamLine;
		if(getTagEnd() != "") endX = endXBeamLine + sizeEndTag.width() + MARGIN_LINE_TEXT;
		else endX = endXBeamLine;
	}
	else
	{
		if(getTagStart() != "") endX = endXBeamLine + sizeStartTag.width() + MARGIN_LINE_TEXT; 
		else startX = startXBeamLine;
		if(getTagEnd() != "") startX = startXBeamLine - sizeEndTag.width() - MARGIN_LINE_TEXT; 
		else endX = endXBeamLine; 
	}

	if(DebugCtl::isSynoptic())
	{
		char	fileName[256];
		#ifdef Q_OS_WIN
			sprintf_s(fileName, sizeof(fileName) / sizeof(fileName[0]), "C:\\Syn_%s_BeforeExtent.txt", pBeamLine->getName());
		#else
			sprintf(fileName, "/home/kopylov/Syn_%s_BeforeExtent.txt", pBeamLine->getName());
		#endif
		dump(fileName);
	}

	// Extend passive elements to cover all active elements within it
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		if(pItem->getType() != EqpType::Passive)
		{
			continue;
		}
		if(pItem->getBeamLinePart()->isReverse())
		{
			connPos = pItem->getEqp()->getStart();
		}
		else if(reverse)
		{
			connPos = pItem->getEqp()->getStart();
		}
		else
		{
			connPos = pItem->getEqp()->getStart() + pItem->getEqp()->getLength();
		}
		QRect &rect = pItem->getRect();
		QPoint &itemConnPoint = pItem->getConnPoint();
		connPoint = rect.x() + rect.width();
		bool isExtended = false;
		for(int nextIdx = idx + 1 ; nextIdx < eqps.count() ; nextIdx++)
		{
			SynEqpItem *pNext = eqps.at(nextIdx);
			if(pNext->getBeamLinePart() != pItem->getBeamLinePart())
			{
				break;
			}
			if(pNext->getType() == EqpType::VacDevice)
			{
				if(pItem->getBeamLinePart()->isReverse())
				{
					if(pNext->getEqpIdx() > pItem->getEqpIdx())
					{
						break;
					}
					if(pNext->getEqp()->getStart() < connPos)
					{
						break;
					}
				}
				else if(reverse)
				{
					if(pNext->getEqpIdx() > pItem->getEqpIdx())
					{
						break;
					}
					if(pNext->getEqp()->getStart() < connPos)
					{
						break;
					}
				}
				else
				{
					if(pNext->getEqpIdx() < pItem->getEqpIdx())
					{
						break;
					}
					if(pNext->getEqp()->getStart() > connPos)
					{
						break;
					}
				}
				QRect &nextRect = pNext->getRect();
				QPoint &nextConnPoint = pNext->getConnPoint();
				if(nextConnPoint.x() > connPoint)
				{
					if(pItem->isStartFixed())
					{
						isExtended = true;
						connPoint = nextConnPoint.x();
					}
					else
					{
						/* LIK 02.03.2008
						pItem->x = eqp[i].connX - eqp[n].width / 2;
						pItem->connX = pItem->x + pItem->width / 2;
						pItem->isStartFixed = true;
						*/
						/* L.Kopylov 26.01.2010
						rect.moveLeft(nextConnPoint.x() - nextRect.width() / 4);
						itemConnPoint.setX(rect.x() + rect.width() / 2);
						pItem->setStartFixed(true);
						*/
						if(rect.left() > nextConnPoint.x() - nextRect.width() / 4)
						{
							rect.moveLeft(nextConnPoint.x() - nextRect.width() / 4);
							itemConnPoint.setX(rect.x() + rect.width() / 2);
						}
						pItem->setStartFixed(true);
						// 26.01.2010 Addition by L.Kopylov
						connPoint = nextConnPoint.x();
						isExtended = true;
					}
				}
				else if(!pItem->isStartFixed())	// LIK 21.02.2008
				{
					/* L.Kopylov
					rect.moveLeft(nextConnPoint.x() - nextRect.width() / 4);
					itemConnPoint.setX(rect.x() + rect.width() / 2);
					pItem->setStartFixed(true);
					*/
					if(rect.left() > nextConnPoint.x() - nextRect.width() / 4)
					{
						rect.moveLeft(nextConnPoint.x() - nextRect.width() / 4);
						itemConnPoint.setX(rect.x() + rect.width() / 2);
					}
					pItem->setStartFixed(true);
					// 26.01.2010 Addition by L.Kopylov
					connPoint = nextConnPoint.x();
					isExtended = true;
				}
			}
			else if(pNext->getType() == EqpType::Passive)
			{
				break;
			}
		}
		if(isExtended)
		{
			rect.setWidth(connPoint - rect.x() + 2);
		}
	}
	if(DebugCtl::isSynoptic())
	{
		char	fileName[256];
		#ifdef Q_OS_WIN
			sprintf_s(fileName, sizeof(fileName) / sizeof(fileName[0]), "C:\\Syn_%s_AfterExtent.txt", pBeamLine->getName());
		#else
			sprintf(fileName, "/home/kopylov/Syn_%s_AfterExtent.txt", pBeamLine->getName());
		#endif
		dump(fileName);
	}
}

/*
**	FUNCTION
**		Find minimum allowed horizontal position for new device such that
**		it's icon will not overlap with icon of previous device
**
**	PARAMETERS
**		pItem			- Pointer to new device, Y sizes and location are already filled in
**		checkPassive	- true if previous icons for passive devices shall also be checked
**		minRectPos		- minimum position for icon rectangle will be returned here
**
**	RETURNS
**		Minimum allowed horizontal position for new device's icon
**
**	CAUTIONS
**		Method traverses 'eqps' list, so current position is moved by this method
*/
int SynLine::findMinPos(SynEqpItem *pItem, bool checkPassive, int &minRectPos)
{
	int	result = minRectPos = 0;
	bool foundPlus = false, foundMinus = false;

	int startIdx = eqps.indexOf(pItem), idx;
	QRect &rect = pItem->getRect();
	SynEqpItem *pPrev;
	for(idx = startIdx - 1 ; idx >= 0 ; idx--)
	{
		pPrev = eqps.at(idx);
		switch(pPrev->getType())
		{
		/* LIK 10.03.2008 - to improve appearance of LINAC3. TODO: NOT TESTED!!!!
		case EqpType::OtherLineStart:
		case EqpType::OtherLineEnd:
		*/
		case EqpType::SectorBefore:
		case EqpType::SectorAfter:
			continue;
		case EqpType::Passive:
			if(!checkPassive)
			{
				continue;
			}
			break;
		}
//qDebug("Check previous %s for %s\n", pPrev->getName(), pItem->getName());
		QRect &prevRect = pPrev->getRect();
		// Check if rectangles will overlap			
		if(prevRect.y() > (rect.y() + rect.height()))
		{
//qDebug("CONTINUE #1\n");
			continue;
		}
		if((prevRect.y() + prevRect.height()) < rect.y())
		{
//qDebug("CONTINUE #2\n");
			continue;
		}
		int newResult = prevRect.x() + prevRect.width() + minSpacing;
		if(newResult > result)
		{
//qDebug("updating result: %d -> %d\n", result, newResult);
			result = newResult;
		}

		// LIK 02.01.2009 - don't stop search on 1st found element - previous
		// one can extend longer that first found, this was observed when
		// first found is passive (very small), and next before that passive
		// is gauge exending beyond passive
		// However, it still looks stupid to check all of previous devices, so
		// let's stop on first active with 'compatible' verticalPos
		// LIK 24.02.2012 - the above is still not enough in some cases. For example:
		// VGR+VGP above line, VPI below line. First previous VPI is found, but previous
		// VGP's has right side after VPI's right side. So continue search until both eqp
		// above and below line were checked.
		if(pItem->getType() == EqpType::VacDevice)
		{
			if(pPrev->getType() == EqpType::VacDevice)
			{
				if(pItem->getVerticalPos())
				{
					if(pPrev->getVerticalPos() == pItem->getVerticalPos())
					{
						break;
					}
				}
				else
				{
					int prevVerticalPos = pPrev->getVerticalPos();
					if(prevVerticalPos < 0)
					{
						foundMinus = true;
					}
					else if(prevVerticalPos > 0)
					{
						foundPlus = true;
					}
					else
					{
						foundPlus = foundMinus = true;
					}
					if(foundPlus && foundMinus)
					{
						break;
					}
				}
			}
		}
		else if(pPrev->getType() == EqpType::Passive)
		{
			break;
		}
	}

	// If device is sector border - make sure there is enough space before
	// it to draw sector name
	minRectPos = result;
	if(!pItem->isSectorBorder())
	{
		return result;
	}
	// Furthermore, space between two sector borders shall be large enough to allow drawing
	// of sector border names (valves). L.Kopylov 23.02.2012
	QSize nameSize = parent->fontMetrics().size(Qt::TextSingleLine, pItem->getName());
	int nameSizeHalf = (nameSize.width() >> 1) + 3;
	QSize size = parent->fontMetrics().size(Qt::TextSingleLine, pItem->getNameBefore(reverse));
	int sizeSector = size.width() + 3;
	pItem->setTextStart(nameSizeHalf > sizeSector ? nameSizeHalf : sizeSector);
//qDebug("Name %s sector before %s textStart %d pos %d\n", pItem->getName(), pItem->getNameBefore(reverse), pItem->getTextStart(), result);
	pItem->setTextHeight(size.height() > nameSize.height() ? size.height() : nameSize.height());
	for(idx = startIdx - 1 ; idx >= 0 ; idx--)
	{
		pPrev = eqps.at(idx);
		QRect &prevRect = pPrev->getRect();
		int newResult = prevRect.x() + prevRect.width() + minSpacing;
		if (newResult > result)
		{
			//qDebug("updating result for sector border: %d -> %d\n", result, newResult);
			result = newResult;
		}
		if (pPrev->isSectorBorder())
		{
			if((pPrev->getTextEnd() + pItem->getTextStart()) > result)
			{
//qDebug("prev %s: end %d this start %d\n", pPrev->getName(), pPrev->getTextEnd(), pItem->getTextStart());
				result = pPrev->getTextEnd() + pItem->getTextStart();
			}
			break;
		}
	}
	size = parent->fontMetrics().size(Qt::TextSingleLine, pItem->getNameAfter(reverse));
	sizeSector = size.width() + 3;
	pItem->setTextEnd(nameSizeHalf > sizeSector ? nameSizeHalf : sizeSector);
//qDebug("Name %s sector after %s textEnd %d pos %d\n", pItem->getName(), pItem->getNameAfter(reverse), pItem->getTextEnd(), result);
	if(size.height() > pItem->getTextHeight())
	{
		pItem->setTextHeight(size.height());
	}
	return result;
}

/*
**	FUNCTION
**		Add vertical space for sector names at sector borders
**
**	PARAMETERS
**		labelsUpAndDown		- true if sector labels will be drawn both above and below line,
**								false if they only will be drawn above line
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::addSectorNameSpace(bool labelsUpAndDown)
{
	int	newAscent = ascent,
		newDescent = descent,
		newstartXBeamLine = startXBeamLine,
		newendXBeamLine = endXBeamLine;

	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynEqpItem *pItem = eqps.at(idx);
		if(!pItem->isSectorBorder())
		{
			continue;
		}
		if((ascent + pItem->getTextHeight() + 2 ) > newAscent)
		{
			newAscent = ascent + pItem->getTextHeight() + 2;
		}
		if(labelsUpAndDown)
		{
			if((descent + pItem->getTextHeight() + 2) > descent)
			{
				newDescent = descent + pItem->getTextHeight() + 2;
			}
		}
		if(pItem->getTextStart() < newstartXBeamLine)
		{
			newstartXBeamLine = pItem->getTextStart();
		}
		if(pItem->getTextEnd() > newendXBeamLine)
		{
			newendXBeamLine = pItem->getTextEnd();
		}

		// LIK 20.01.2010 Make sure there is enough space for 1/2 device visible name
		if(labelsUpAndDown)
		{
			const char *eqpName = pItem->getEqp()->getVisibleName();
			QSize size = parent->fontMetrics().size(Qt::TextSingleLine, eqpName);
			int txtSize = size.width() / 2;
			if((pItem->getConnPoint().x() - txtSize - 3) < newstartXBeamLine)
			{
				newstartXBeamLine = pItem->getConnPoint().x() - txtSize - 3;
			}
		}
	}
	ascent = newAscent;
	// TODO: simplified varaint below descent = labelsUpAndDown ? newDescent : newDescent + sectorIconHeight + 2;
	descent = labelsUpAndDown ? newDescent : newDescent + 2;
	
	// [VACCO-527] Calculates the limites of pBeamLine, depending if start/end tags exist
	if((getTagStart() != "" && !reverse) || (getTagEnd() != "" && reverse))
	{
		startX = startX - (startXBeamLine - newstartXBeamLine); 
		startXBeamLine = newstartXBeamLine;
	}
	else
	{
		startXBeamLine = newstartXBeamLine;
		startX = newstartXBeamLine; 
	}

	if((getTagEnd() != "" && !reverse)|| (getTagStart() != "" && reverse))
	{
		if(!reverse)
		{
			endXBeamLine = newendXBeamLine; 
			endX = endXBeamLine + sizeEndTag.width() + MARGIN_LINE_TEXT;
		}
		else
		{
			endXBeamLine = newendXBeamLine; 
			endX = endXBeamLine + sizeStartTag.width() + MARGIN_LINE_TEXT;
		}
	}
	else
	{
		endXBeamLine = newendXBeamLine;
		endX = newendXBeamLine;
	}



	if(haveInterlockSources)
	{
		ascent += 5;
		descent += 5;
	}
}

/*
** FUNCTION
**		Draw rectangles for passive equipment,
**		sector borders and sector names,
**		connection lines for active equipment,
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		viewStart	- X-coordinate of start of area to be painted
**		viewEnd		- X-coordinate of start of area to be painted
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::drawEqp(QPainter &painter, int viewStart, int viewEnd)
{
	// Clear flag indicating that common connection is drawn
	int idx;
	SynEqpItem *pItem;
	for(idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		pItem = eqps.at(idx);
		pItem->setCommonReady(false);
	}

	// Draw all devices
	int centerX = 0, centerY = 0;
	bool isConnInside = false;
	QPen linePen(pipeColor, VacMainView::getSynPipeWidth());
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		QRect &rect = pItem->getRect();
		QPoint &connPoint = pItem->getConnPoint();

		// Draw sector border even if device is outside painting region - label(s)
		// still can be (partially) inside region
		if(pItem->isSectorBorder())
		{
			drawSectorBorder(painter, pItem);
		}
		// L.Kopylov 27.01.2010 Draw arrow for interlock source
		else if(pItem->isIntlSourceForward() || pItem->isIntlSourceBackward())
		{
			VacIcon *pIcon = pItem->getIcon();
			if(pIcon)
			{
				pIcon->drawInterlockSrcArrow(painter, pItem->isIntlSourceBackward(), pItem->isIntlSourceForward());
			}
			/* L.Kopylov 26.09.2013 see above
			painter.setPen(VacMainView::getSynPassiveLineColor());
			int arrowY = rect.y();
			if(connPoint.y() > rect.y())	// Device above line
			{
				arrowY -= 5;
			}
			else
			{
				arrowY = rect.bottom() + 5;
			}
			int arrowStart = rect.left() + 2,
					arrowEnd = rect.right() - 2;
			painter.drawLine(arrowStart, arrowY, arrowEnd, arrowY);
			if(pItem->isIntlSourceForward())
			{
				painter.drawLine(arrowEnd - 3, arrowY - 3, arrowEnd, arrowY);
				painter.drawLine(arrowEnd - 3, arrowY + 3, arrowEnd, arrowY);
			}
			if(pItem->isIntlSourceBackward())
			{
				painter.drawLine(arrowStart + 3, arrowY - 3, arrowStart, arrowY);
				painter.drawLine(arrowStart + 3, arrowY + 3, arrowStart, arrowY);
			}
			*/
		}

	
		if((((rect.x() + rect.width()) < viewStart) || (rect.x() > viewEnd)))
		{
			continue;
		}
		switch(pItem->getType())
		{
		case EqpType::VacDevice:
			if(pItem->getIcon())
			{
				if(!pItem->getIcon()->isConnectedToVacuum())
				{
					continue;
				}
			}
			// Calculate center of element
			if((connPoint.x() >= rect.x()) && (connPoint.x() <= (rect.x() + rect.width())))
			{
				centerX = connPoint.x();
				isConnInside = true;
			}
			else
			{
				centerX = rect.x() + rect.width() / 2;
				isConnInside = false;
			}
			centerY = rect.y() + rect.height() / 2;
 
			// If device is part of group - draw connection line common for all devices in group.
			if(pItem->getGroupId() && (!pItem->isCommonReady()))
			{
				drawCommonConn(painter, pItem);
			}

			// Draw connecting line from center of element, 3 variants are possible:
			// 1) connX is inside element width - just straight line
			// 2) connX is before element - two lines on left of element
			// 3) connX is after element - two lines on the right of element
			painter.setPen(linePen);
			if( isConnInside )
			{
				painter.drawLine(centerX, centerY, centerX, connPoint.y());
			}
			else
			{
				painter.drawLine(centerX, centerY, connPoint.x(), centerY);
				painter.drawLine(connPoint.x(), centerY, connPoint.x(), connPoint.y());
			}

			break;
		case EqpType::SectorBefore:
		case EqpType::SectorAfter:
			break;
		case EqpType::Passive:
			painter.setPen(VacMainView::getSynPassiveLineColor());
			painter.fillRect(rect, VacMainView::getSynPassiveFillColor());
			painter.drawRect(rect);
			break;
		case EqpType::OtherLineStart:
		case EqpType::OtherLineEnd:
			break;
		}
	}
}

/*
** FUNCTION
**		Draw vacuum pipe line(s)
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		viewStart	- X-coordinate of start of area to be painted
**		viewEnd		- X-coordinate of start of area to be painted
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::drawBeamPipe(QPainter &painter, int viewStart, int viewEnd)
{

	


	if(pColdex && !coldexIsIn)
	{
		QPen pen(VacMainView::getSynAltPipeColor(), VacMainView::getSynPipeWidth());
		painter.setPen(pen);
	}
	else
	{
		QPen pen(pipeColor, VacMainView::getSynPipeWidth());
		painter.setPen(pen);
	}
	painter.drawLine(startXBeamLine, y, endXBeamLine, y);

	// [VACCO-527] Checks if pBeamLine is a child line (connected to a parent line)
	// in order to know if (in case of existing) start/end tags should be represented
	setChildStart(false);
	setChildEnd(false);

	// Draw start connection
	if(pStartEqp)
	{
		if((viewStart < startXBeamLine) || ((viewStart < startConnectX) && (startConnectX < viewEnd)))
		{
			// [VACCO-527] pBeamLine is a child line connected in the line start, so no start tag will be represented
			setChildStart(true);

			if(pColdex && !coldexIsIn)
			{
				QPen pen(VacMainView::getSynAltPipeColor(), VacMainView::getSynPipeWidth());
				pen.setCapStyle(Qt::FlatCap);
				painter.setPen(pen);
			}
			else
			{
				QPen pen(pipeColor, VacMainView::getSynPipeWidth());
				pen.setCapStyle(Qt::FlatCap);
				painter.setPen(pen);
			}

			painter.setRenderHint(QPainter::Antialiasing,true);
			painter.drawLine(startXBeamLine, y, startConnectX, startConnectY);
			painter.setRenderHint(QPainter::Antialiasing,false);
		}
	}

	// Draw end connection
	if(pEndEqp)
	{
		if((endXBeamLine < viewEnd) || ((viewStart < endConnectX) && (endConnectX < viewEnd)))
		{
			// [VACCO-527] pBeamLine is a child line connected in the line end, so no eng tag will be represented
			setChildEnd(true);

			if(pColdex && !coldexIsIn)
			{
				QPen pen(VacMainView::getSynAltPipeColor(), VacMainView::getSynPipeWidth());
				pen.setCapStyle(Qt::FlatCap);
				painter.setPen(pen);
			}
			else
			{
				QPen pen(pipeColor, VacMainView::getSynPipeWidth());
				pen.setCapStyle(Qt::FlatCap);
				painter.setPen(pen);
			}

			painter.setRenderHint(QPainter::Antialiasing,true);
			painter.drawLine(endXBeamLine, y, endConnectX, endConnectY);
			painter.setRenderHint(QPainter::Antialiasing,false);
		}
	}

	// Check if part of line shall be drawn using different color
	if(coldexLine && !coldexIsOut)
	{
		QPen pen(VacMainView::getSynPipePartColor(), VacMainView::getSynPipeWidth());
		painter.setPen(pen);
		painter.drawLine(coldexLine->getStartConnectX(), y, coldexLine->getEndConnectX(), y);
	}

	// [VACCO-527] Actual name tag writting on the synoptics for the start/end of pBeamLine
	painter.setPen(VacMainView::getSynSectorColor());
	if((getTagStart() != "") && (!isChildStart()))
	{
		if(!reverse) painter.drawText(startX, y + (sizeStartTag.height()/2), getTagStart());
		else painter.drawText(endXBeamLine + MARGIN_LINE_TEXT, y + (sizeEndTag.height()/2), getTagStart());
	}
	if((getTagEnd() != "") && (!isChildEnd()))
	{
		if(!reverse) painter.drawText(endXBeamLine + MARGIN_LINE_TEXT, y + (sizeEndTag.height()/2), getTagEnd());
		else painter.drawText(startX, y + (sizeStartTag.height()/2), getTagEnd());
	}
}

/*
**	FUNCTION
**		Draw common connection line for all devices in group
**
**	PARAMETERS
**		painter		- Pointer used for drawing
**		pStartItem	- Pointer to first device of this group
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::drawCommonConn(QPainter &painter, SynEqpItem *pStartItem)
{
	QPoint	&startConnPoint = pStartItem->getConnPoint();
	int		connstartXBeamLine = endXBeamLine,
			connendXBeamLine = startXBeamLine,
			connY = startConnPoint.y(),
			groupId = pStartItem->getGroupId();

	// Search forward from current device
	int startIdx = eqps.indexOf(pStartItem), idx;
	SynEqpItem *pItem;
	for(idx = startIdx ; idx < eqps.count() ; idx++)
	{
		pItem = eqps.at(idx);
		if(pItem->getGroupId() == groupId)
		{
			pItem->setCommonReady(true);
			QPoint &connPoint = pItem->getConnPoint();
			if(connstartXBeamLine > connPoint.x())
			{
				connstartXBeamLine = connPoint.x();
			}
			if(connendXBeamLine < connPoint.x())
			{
				connendXBeamLine = connPoint.x();
			}
		}
		else if(pItem->getGroupId()) // Another group
		{
			break;
		}
	}

	// Search backward from current device
	for(idx = startIdx - 1 ; idx >= 0 ; idx--)
	{
		pItem = eqps.at(idx);
		if(pItem->getGroupId() == groupId)
		{
			pItem->setCommonReady(true);
			QPoint &connPoint = pItem->getConnPoint();
			if(connstartXBeamLine > connPoint.x())
			{
				connstartXBeamLine = connPoint.x();
			}
			if(connendXBeamLine < connPoint.x())
			{
				connendXBeamLine = connPoint.x();
			}
		}
		else if(pItem->getGroupId()) // Another group
		{
			break;
		}
	}

	// Draw
	QPen pen(pipeColor, VacMainView::getSynPipeWidth());
	painter.setPen(pen);
	// +2 for end point because drawLine() does not include end point for wide line
	// L.Kopylov 06.11.2013 - see below. painter.drawLine(connstartXBeamLine - 1, connY, connendXBeamLine + 2, connY);
	painter.drawLine(connstartXBeamLine, connY, connendXBeamLine, connY);
	connstartXBeamLine = (connstartXBeamLine + connendXBeamLine) / 2;
	painter.drawLine(connstartXBeamLine, connY, connstartXBeamLine, y);
}

/*
**	FUNCTION
**		Draw border between two sectors and two sectors names of this border
**
**	PARAMETERS
**		painter	- Painter used for drawing
**		pItem	- Pointer to equipment item of sector border
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::drawSectorBorder(QPainter &painter, SynEqpItem *pItem)
{
	painter.setPen(VacMainView::getSynSectorColor());
	int centerX = pItem->getConnPoint().x();
	// Name of sector before border
	const char *pName = pItem->getNameBefore(reverse);
	QString labelText = pName;
	int textY = y - ascent + pItem->getTextHeight();
	// Start of sector text (X) is not necessary at getTextStart(): device name can be longer. L.Kopylov 23.02.2012
	QSize size = parent->fontMetrics().size(Qt::TextSingleLine, pName);

	// [VACCO-929] [VACCO-948] [VACCO-1645] In case the sector is selected: sector's name is in bold
	painter.save();
	if (!reverse){
		if (pItem->getEqp()->getSectorBefore()->isSelected()){
			QFont boldFont(painter.font());
			boldFont.setBold(true);
			painter.setFont(boldFont);
		}
	}
	else{
		if (pItem->getEqp()->getSectorAfter()->isSelected()){
			QFont boldFont(painter.font());
			boldFont.setBold(true);
			painter.setFont(boldFont);
		}
	}
	painter.drawText(centerX - size.width() - 1, textY, labelText);
	painter.restore();
	/* L.Kopylov 28.01.2010
	textY = y + descent;
	painter.drawText(pItem->getTextStart(), textY, pName);
	*/

	// Name of sector after border
	pName = pItem->getNameAfter(reverse);
	labelText = pName;
	textY = y - ascent + pItem->getTextHeight();

	// [VACCO-929] [VACCO-948] [VACCO-1645] In case the sector is selected: sector's name is in bold
	painter.save();
	if (!reverse){
		if (pItem->getEqp()->getSectorAfter()->isSelected()){
			QFont boldFont(painter.font());
			boldFont.setBold(true);
			painter.setFont(boldFont);
		}
	}
	else{
		if (pItem->getEqp()->getSectorBefore()->isSelected()){
			QFont boldFont(painter.font());
			boldFont.setBold(true);
			painter.setFont(boldFont);
		}
	}
	painter.drawText(centerX + 3, textY, labelText);
	painter.restore();
	/* L.Kopylov 28.01.2010
	textY = y + descent;
	painter.drawText(centerX + 3, textY, pName);
	*/

	pName = pItem->getName();
	textY = y + descent;
	size = parent->fontMetrics().size(Qt::TextSingleLine, pName);
	painter.drawText(centerX - size.width() / 2, textY, pName);
	
	painter.drawLine(centerX, y - ascent, centerX, y + descent - size.height());

}

/*
**	FUNCTION
**		Set pipe color for this line and all icons on it
**
**	PARAMETERS
**		color	- New pipe color
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::setPipeColor(QColor &color, bool forced)
{
	pipeColor = color;
	if (!pipeColorForced) {
		pipeColorForced = forced;
	}
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		SynEqpItem *pItem = eqps.at(idx);
		pItem->setPipeColor(color);
	}		
}


/*
**	FUNCTION
**		Slot signalled by equipment (COLDEX only) when state is changed
**
**	PARAMETERS
**		See InterfaceEqp.h
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::dpeChange(Eqp * /* pSrc */, const char * /* dpeName */,
		DataEnum::Source /* source */, const QVariant & /* value */,
		DataEnum::DataMode mode, const QDateTime & /* timeStamp */ )
{
	if(mode != this->mode)
	{
		return;
	}
	pColdex->getColdexState(mode, coldexIsIn, coldexIsOut);
	emit coldexStateChanged(coldexIsIn, coldexIsOut);
}

/*
**	FUNCTION
**		Dump parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLine::dump(FILE *pFile)
{
	if(!DebugCtl::isSynoptic())
	{
		return;
	}
	if(!pFile)
	{
		return;
	}

	fprintf(pFile, "  <%s>: START: part %s eqp %d, END: %s %d\n", pBeamLine->getName(),
		pStartPart->getName(), startEqpIdx, pEndPart->getName(), endEqpIdx);
	fprintf(pFile, "  StartEqp <%s> EndEqp <%s>\n", (pStartEqp ? pStartEqp->getName() : "NULL"), (pEndEqp ? pEndEqp->getName() : "NULL"));
	fprintf(pFile, "  row = %d\n", row);
	fprintf(pFile, "  verticalPos %d\n", pBeamLine->getVerticalPos());
	fprintf(pFile, "  horReady %d vertReady %d\n", horReady, vertReady);
	fprintf(pFile, "  startXBeamLine %d endXBeamLine %d\n", startXBeamLine, endXBeamLine);
	fprintf(pFile, "  Y %d ascent %d descent %d\n", y, ascent, descent);
	if(reverse)
	{
		fprintf(pFile, "  REVERSE DRAWING\n");
	}
	if(pStartEqp)
	{
		fprintf(pFile, "  START at (%d, %d)\n", startConnectX, startConnectY);
	}
	if(pEndEqp)
	{
		fprintf(pFile, "  END at (%d, %d)\n", endConnectX, endConnectY);
	}
	fprintf(pFile, "  +++++ %d devices:\n", eqps.count());
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynEqpItem *pItem = eqps.at(idx);
		QRect &rect = pItem->getRect();
		QPoint &connPoint = pItem->getConnPoint();
		fprintf(pFile, "   %8d: <%-20s> type %2d: part %s eqp %5d (%6d %4d) (%3d x %3d) conn (%6d %4d) group %d%s\n",
			idx, pItem->getName(), pItem->getType(), pItem->getBeamLinePart()->getName(),
			pItem->getEqpIdx(),
			rect.x(), rect.y(), rect.width(), rect.height(),
			connPoint.x(), connPoint.y(), pItem->getGroupId(),
			(pItem->isSectorBorder() ? " BORDER" : ""));
	}
}

/*
**
** FUNCTION
**		Dump parameters to output file for debugging
**
** PARAMETERS
**		fileName	- Name of output file
**
** RETURNS
**		None
**
** CAUTIONS
**		None
*/
void SynLine::dump(const char *fileName)
{
	if(!DebugCtl::isSynoptic())
	{
		return;
	}
#ifdef Q_OS_WIN
	FILE	*pFile = NULL;
	if(fopen_s(&pFile, fileName, "w"))
	{
		pFile = NULL;
	}
#else
	FILE	*pFile = fopen(fileName, "w");
#endif
	if(pFile)
	{
		dump(pFile);
		fclose(pFile);
	}
}



