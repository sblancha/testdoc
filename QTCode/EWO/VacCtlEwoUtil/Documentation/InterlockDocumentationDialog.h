#ifndef INTERLOCKDIALOG_H
#define INTERLOCKDIALOG_H

#include <QDialog>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qmenu.h>
#include <QScrollArea>

#include "InterlockView.h"

class InterlockDocumentationDialog : public QDialog
{
	Q_OBJECT
public:
	//RackDocumentationDialog(Rack * rack); legacy constructor
	InterlockDocumentationDialog(QString valveName);
	~InterlockDocumentationDialog();
protected:
	InterlockView * pView;

	QString valveName;
	// Button for displaying 'File' menu
	QPushButton * pFilePb;
	// Button for displaying 'Help' menu
	QPushButton * pHelpPb;

	// 'File' menu
	QMenu * pFileMenu;

	// 'Help' menu
	QMenu * pHelpMenu;

	// Label with synoptic title
	QLabel * pTitleLabel;

	// Scroll view holding synoptic image and icons
	QScrollArea * pScroll;

	//functions
	void buildLayout(void);
	void buildFileMenu(void);
	void buildHelpMenu(void);
	void buildView(bool init);
	void setMyWidth(bool init);

private slots :
	void viewMove(int x);
	void filePrint();
};
#endif