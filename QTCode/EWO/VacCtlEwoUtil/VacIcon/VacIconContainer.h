#ifndef	VACICONCONTAINER_H
#define VACICONCONTAINER_H

#include "VacCtlEwoUtilExport.h"

// Container for vacuum device icon - used to be used by VacIcon EWO
// in order to put icons on PVSS panel manually

#include "DataEnum.h"

#include <QWidget>

class VacIcon;

#include <QHBoxLayout>

class VACCTLEWOUTIL_EXPORT VacIconContainer : public QWidget
{
	Q_OBJECT

	////////////////////////////////////////////////////////////////////////////
	///////////////////// Properties exposed to PVSS panel /////////////////////
	////////////////////////////////////////////////////////////////////////////
	Q_PROPERTY(QColor pipeColor READ getPipeColor WRITE setPipeColor);
	Q_PROPERTY(int pipeWidth READ getPipeWidth WRITE setPipeWidth);
	Q_PROPERTY(Direction direction READ getDirection WRITE setDirection);
	Q_PROPERTY(BeamDirection beamDirection READ getBeamDirection WRITE setBeamDirection);
	Q_PROPERTY(EqpType eqpType READ getEqpType WRITE setEqpType);
	Q_PROPERTY(QString dpName READ getDpName WRITE setDpName);
	Q_PROPERTY(DataEnumMode mode READ getMode WRITE setMode);
	Q_PROPERTY(bool drawConnectionPipe READ isDrawConnectionPipe WRITE setDrawConnectionPipe);

	Q_PROPERTY(int iconWidth READ getIconWidth DESIGNABLE true);
	Q_PROPERTY(int iconHeight READ getIconHeight DESIGNABLE true);

	Q_ENUMS(Direction BeamDirection DataEnumMode EqpType);

public:
	 // Accessors for properties above
	enum Direction
	{
		Up = 1,
		Down = 2,
		Right = 3,
		Left = 4
	};

	enum BeamDirection
	{
		LeftToRight = 1,
		RightToLeft = 2
	};

	// In fact, just a copy of enum from DataEnum to be exposed to PVSS panel
	enum DataEnumMode
	{
		Online = 0,
		Replay = 1,
		Polling = 2
	};

	// Keep holes in numbering in order to be able to add more types to the group
	enum EqpType
	{
		None = 0,
		VVS_PS = 1,
		VVS_LHC,
		VVS_SPS,
		VVF_SPS,
		VLV,
		VLV_ANA,
		VV_AO,
		VV_PULSED,
		VV_PLUS_PP,
		VV_STD_IO,
		VV_PUL,
		VIES,

		VGM = 100,
		VGTR = 101,
		GaugeMembrane = 150,

		VGR = 200,
		GaugePirani = 250,

		VGP = 300,
		GaugePenning = 350,

		VGI = 400,

		GaugeDualRP = 420,
		VGF = 450,
		VPGMPR = 452,
		
		VGA_VELO = 460,
		VGD_VELO = 464,

		VG_STD_VGM = 471,
		VG_STD_VGR = 472,
		VG_STD_VGP = 473,
		VG_STD_VGI = 474,
		VG_STD_VGF_RP = 475,
		VG_STD_VGF_RI = 476,
		VG_STD_VGC = 477,

		//VG_DP = 480,
		VG_DP_VGM = 481,
		VG_DP_VGF_RP = 485,	

		VG_PT_P = 488,

		VG_A_RO = 490,
		VOPS_2_PS = 491,

		VPI = 500,
		VP_IP = 501,
		VP_TP = 502,
		VPR_VELO = 510,
		VPT_VELO = 520,

		VP_GU = 530,

		VPG_SA = 600,
		VPG_BP = 601,
		VPG_STD = 602,
		VP_STDIO = 603,
		VPGM = 604,
		VPG_SA_VG = 605,
		VPG_EA_SPS = 606,
		VPG_MBLK = 607,

		VPT100 = 700,
		VRJ_TC = 701,
		VR_ER = 702,

		VP_STD_IO_PP = 800,
		VP_STD_IO_TMP = 801,
		VP_STD_IO_C0 = 802,

		VPN = 820,
		VPS = 825,

		VPC_HCCC = 830,

		V8DI_FE = 840,

		PROCESS_VPG_6A01 = 900,
		PROCESS_VPG_6A01_1VVR_VG = 901,
		PROCESS_VPG_6A01_2VVR_VG = 902,
		PROCESS_VPG_6A01_1VVR_NO_VG = 903,
		PROCESS_VPG_6A01_2VVR_NO_VG = 904,
		PROCESS_VPG_6E01 = 905,

		PROCESS_BGI_6B01 = 920,
		PROCESS_INJ_6B02 = 921,

		CRYO_TT = 1000,
		CRYO_TT_SUM = 1001,

		VA_RI = 1101,
		VA_RG = 1102,

		VOPS_VELO = 2000,
		VINT_VELO = 2010,
		VPRO_VELO = 2020,

		VV_PS_CMW = 3000,
		VGR_PS_CMW = 3001,
		VGP_PS_CMW = 3002,
		VPI_PS_CMW = 3003,
		VGI_PS_CMW = 3004,
		
		MP_ACCESS = 10000,

		SECT_VPI_SUM = 10001,

		EXP_AREA = 11000,
		EXT_ALARM = 11001,
		VBAR = 11002,		// Vacuum barrier
		COLDEX = 11010,

		BEAM_INT = 12000
	};

	inline const QColor &getPipeColor(void) const { return pipeColor; }
	void setPipeColor(QColor &color);
	inline int getPipeWidth(void) const { return pipeWidth; }
	void setPipeWidth(int width);
	inline Direction getDirection(void) const { return direction; }
	void setDirection(Direction dir);
	inline BeamDirection getBeamDirection(void) const { return beamDirection; }
	void setBeamDirection(BeamDirection dir);
	inline EqpType getEqpType(void) const { return eqpType; }
	void setEqpType(EqpType type);
	inline const QString &getDpName(void) const { return dpName; }
	void setDpName(QString &name);
	inline DataEnumMode getMode(void) const { return (DataEnumMode)mode; }
	void setMode(DataEnumMode mode);
	inline bool isDrawConnectionPipe(void) const { return drawConnectionPipe; }
	void setDrawConnectionPipe(bool flag);

	int getIconWidth(void) const;
	inline void setIconWidth(int /* width */) {}
	int getIconHeight(void) const;
	inline void setIconHeight(int /* height */) {}
			
	// Construction/destruction
	VacIconContainer(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~VacIconContainer();

	bool connect(void);
	bool disconnect(void);

	virtual QSize sizeHint(void) const;

signals:
	void iconMouseDown(int button, int mode, int x, int y, int extra, const char *dpName);

protected:
	// Layout for icon
	QHBoxLayout		*pLayout;

	// Instance of icon used by this container
	VacIcon			*pIcon;

	// DP name for icon
	QString			dpName;

	// Color for vacuum pipe drawing (used if pipe is drawn within icon)
	QColor			pipeColor;

	// Width for vacuum pipe drawing (used if pipe is drawn within icon)
	int				pipeWidth;

	// Direction for icon
	Direction		direction;

	// Beam direction - important for some devices (valves)
	BeamDirection	beamDirection;

	// Equipment type for icon
	EqpType			eqpType;

	// Data acquisition mode
	DataEnum::DataMode	mode;

	// Draw connection pupe or not
	bool			drawConnectionPipe;
};

#endif	// VACICONCONTAINER_H
