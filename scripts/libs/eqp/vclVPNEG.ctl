// Bitmasks for VP_NEG status bits (RR1 - 32 bits)
const unsigned // RR1
	VPNEG_R_VALID = 0x40000000u,
	VPNEG_R_HW_INTERLOCK = 0x20000000u,
  VPNEG_R_LOCAL =   0x10000000u,
	VPNEG_R_MANUAL =   0x04000000u,
  VPNEG_R_ON =   0x02000000u,
  VPNEG_R_OFF = 0x01000000u,
	VPNEG_R_ERRORS = 0x00800000u,
	VPNEG_R_WARNINGS = 0x00400000u,
	VPNEG_R_INTRERLOCK =  0x00200000u,
	VPNEG_R_PRESS_INTRERLOCK =  0x00200000u,
	VPNEG_R_START_INTRERLOCK =  0x00100000u,  
	VPNEG_R_MUX_INTRERLOCK =  0x00100000u,  
	VPNEG_R_ET200_DP_ERR =  0x00080000u,
	VPNEG_R_PSU_DP_ERR =  0x00040000u;
  
        
const unsigned   VPNEG_W16_REMOTE = 0x2000u,
	               VPNEG_W16_LOCAL = 0x1000u,
                 VPNEG_W16_AUTO = 0x0800u,
                 VPNEG_W16_MANUAL = 0x0400u,
	               VPNEG_W16_ON = 0x0200u,
	               VPNEG_W16_OFF = 0x0100u,
	               VPNEG_W32_ON = 0x02000000u,
	               VPNEG_W32_OFF = 0x01000000u,
                 VPNEG_W32_AUTO = 0x08000000u,
                 VPNEG_W32_MANUAL = 0x04000000u,
	               VPNEG_W16_FORCE = 0x4000u,
                 VPNEG_W16_RESET = 0x8000u;


//LhcVacDeviceAction_VPNEG:
/** Execute action for VPNEG. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VPNEG(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  switch(dpType)
  {
  case "VP_NEG":
    LhcVacDeviceAction_VP_NEG(dpName, dpType, action, exceptionInfo);
    break;
  case "VPNMUX":
    LhcVacDeviceAction_VPN_MUX(dpName, dpType, action, exceptionInfo);
    break;
  case "VPN":
    LhcVacDeviceAction_VPN(dpName, dpType, action, exceptionInfo);
    break;
  default:
    fwException_raise(exceptionInfo, "ERROR", "LhcVacDeviceAction_VPNEG(): unsupported DP type " + dpType, "");
    break;
  }
}

void LhcVacDeviceAction_VPN(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  mapping         mConnectDpes;
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["DUMMY"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    break;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["DUMMY"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    break;
   default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
  }
}

void LhcVacDeviceAction_VP_NEG(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  unsigned		    newWr1 = 0x0, value = 0x0;
  dyn_errClass		err;
  unsigned        act;
  string          wrDpe = "";       
  mapping         mConnectDpes;
  
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
    newWr1 = VPNEG_W32_ON;
    break;
  case LVA_DEVICE_OFF:
    newWr1 = VPNEG_W32_OFF;
    break;
  case LVA_DEVICE_VPN_SETMODE:
    newWr1 = (action[2] << 8 );
    break; 
  case LVA_DEVICE_VPN_MANUAL_VOLT_SETPOINT:
    value = (action[2]);
    wrDpe = dpName + ".WR_V_SetP";
    break;  
  case LVA_DEVICE_VPN_MANUAL_CUR_SETPOINT:
    value = (action[2]);
    wrDpe = dpName + ".WR_I_SetP";
    break;  
  case LVA_DEVICE_VPN_MANUAL_TIME_SETPOINT:
    value = (action[2]);
    wrDpe = dpName + ".WR_Time_SetP";  
    break;  
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.TimeToFinish"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.V_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.I_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.V_Value"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.I_Value"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_V_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_I_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_Time_SetP"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.TimeToFinish"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.V_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.I_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.V_Value"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.I_Value"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_V_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_I_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_Time_SetP"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
   default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }
  if(newWr1 != 0x0)
  {
      // Execute
    dpSetWait(dpName + ".WR1", newWr1);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "Action Execution: execution failed:" + err , "");
      return;
    }
  }
  if(wrDpe != "")
  {
      // Execute
    dpSetWait(wrDpe, value);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "Action Execution: execution failed:" + err , "");
      return;
    }
  }
}
void LhcVacDeviceAction_VPN_MUX(string dpName, string dpType, dyn_anytype action,
  dyn_string &exceptionInfo)
{
  unsigned		    newWr1 = 0x0, value = 0x0;
  dyn_errClass		err;
  unsigned        act;
  string          wrDpe = "";       
  int             channel;     
  const unsigned VPNEG_W16_REMOTE = 0x2000u,
	               VPNEG_W16_LOCAL = 0x1000u,
                 VPNEG_W16_AUTO = 0x0800u,
                 VPNEG_W16_MANUAL = 0x0400u,
	               VPNEG_W16_ON = 0x0200u,
	               VPNEG_W16_OFF = 0x0100u,
	               VPNEG_W32_ON = 0x02000000u,
	               VPNEG_W32_OFF = 0x01000000u,
                 VPNEG_W32_AUTO = 0x08000000u,
                 VPNEG_W32_MANUAL = 0x04000000u,
	               VPNEG_W16_FORCE = 0x4000u,
                 VPNEG_W16_RESET = 0x8000u;
  mapping        mConnectDpes;

  //     LVA_DEVICE_ON,
//     LVA_DEVICE_OFF,
//     LVA_DEVICE_AUTO,
//     LVA_DEVICE_MANUAL,
//     LVA_DEVICE_RESET,
//     LVA_DEVICE_FORCE,
//     LVA_DEVICE_SETMUX_CHANNEL,
//     LVA_DEVICE_VPN_MANUAL_VOLT_SETPOINT,
//     LVA_DEVICE_VPN_MANUAL_CUR_SETPOINT);
// 
  act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
  case LVA_DEVICE_ON:
    newWr1 = VPNEG_W16_ON | VPNEG_W_REMOTE;
    break;
  case LVA_DEVICE_OFF:
    newWr1 = VPNEG_W16_OFF | VPNEG_W_REMOTE;
    break;
  case  LVA_DEVICE_AUTO:
    newWr1 = VPNEG_W16_AUTO | VPNEG_W_REMOTE;
    break;
  case  LVA_DEVICE_MANUAL:
    newWr1 = VPNEG_W16_MANUAL;
    break;
  case  LVA_DEVICE_RESET:
    newWr1 = VPNEG_W16_RESET;
    break;
  case LVA_DEVICE_FORCE:
    newWr1 = VPNEG_W16_FORCE;
    break;
  case LVA_DEVICE_SETMUX_CHANNEL:
    channel = (action[2]);
    wrDpe = dpName + ".WR_MUX";

    if(channel >= 1 && channel < 9)
    {
      value = (1 << (24 + (channel - 1)));
    }
    else if(channel >= 8 && channel < 16)
    {
      value = (1 << (16 + (channel) - 8));
    }
    else if(channel >= 16 && channel < 24)
    {
      value = (1 << ( 8 + (channel) - 16));
    }
    else if(value >= 24 && value < 33)
    {
      value =(1 << (channel -  24));
    }
    break; 
  case LVA_DEVICE_VPN_MANUAL_VOLT_SETPOINT:
    break;
  case LVA_DEVICE_VPN_MANUAL_CUR_SETPOINT:
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.MUX"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.I_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.I_Value"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.V_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.V_Value"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_MUX"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_V_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_I_SetP"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.MUX"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.I_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.I_Value"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.V_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["STATE.V_Value"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_MUX"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_V_SetP"] = NO_VALUE_CHANGE;
    mConnectDpes["WR_I_SetP"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }

  // Execute
  dpSetWait(dpName + ".WR1", newWr1);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
}


///LhcVacActionPossible_VPNEG:
/** Check if VGP is in state that allows executing actions for it
// At this moment we have no criteria to forbide action
Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, action to be excuted
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_VPNEG(string dpName, string dpType, dyn_anytype act, bool &possible,
	dyn_string &exceptionInfo)
{
  possible = true;
}

