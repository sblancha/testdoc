#ifndef LHCRINGSELECTION_H
#define	LHCRINGSELECTION_H

#include "VacCtlEwoUtilExport.h"

//	Superclass for view classes which need to calculate/change selection
//	on LHC ring, encapsulates a number of methods which were in VacEqpData
//	in the past.
//	The class is only used as superclass for different view classes

#include <QList>

class MainPart;
class Sector;
class Eqp;

#include <QStringList>

class VACCTLEWOUTIL_EXPORT LhcRingSelection
{
protected:
	LhcRingSelection();
	~LhcRingSelection() {}

	// Main vacuum type selection mask - vacuum type(s) selected either by
	// clicking on sector in image, or by choosing one main part from combo box.
	// Other vacuum types can only be selected IN ADDITION to main vacuum type.
	int		mainVacMask;

	// Current vacuum type selection mask
	int		vacMask;

	// First and last sectors of selection. They are not necessary first and
	// last sectors shown in sector list because list can also include partially
	// selected sectors; these two sectors correspond to start and end of selection
	// range on LHC ring
	Sector	*pFirstSector,
			*pLastSector;

	MainPart *getAdjucentMainPart(MainPart *pMainPart, int vacType);
	void buildRangeFromMps(QList<MainPart *> &mpList, QStringList &sectorList);

	bool mpRangeOnLhcRing(MainPart *pMainPart, int &startSectIdx, int &endSectIdx);

	bool mpRangeOnLhcRingBeam(MainPart *pMainPart, int &startSectIdx, int &endSectIdx);
	bool mpRangeOnLhcRingBeamFromNeighbour(MainPart *pMainPart, int &startSectIdx, int &endSectIdx);
	bool mpRangeOnLhcRingBeamNoNeighbour(MainPart *pMainPart, int &startSectIdx, int &endSectIdx);

	bool mpRangeOnLhcRingIsol(MainPart *pMainPart, int &startSectIdx, int &endSectIdx);

	int addSectorToList(QStringList &sectorList, Sector *pSector);

	void getBeamIntesityEqp(QList<Eqp *> &result);
};

#endif	// LHCRINGSELECTION_H
