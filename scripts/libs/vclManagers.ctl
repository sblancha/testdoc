// CTRL Library
// created from lhcVacManagers.ctl for QT
//  @SAA 19may10
// Modify
//  @LIK for PVSS3.6 with new UNICOS4.3.3

// -------------------------------------------------
//
// Functions for work with managers
//
// -------------------------------------------------

// archives
const string ARCHIVE_MANAGER_NAME = "WCCOAvalarch";
// Simulate driver
const string SIMULATION_MANAGER_NAME = "WCCILsim";
// OPC
const string OPC_MANAGER_NAME = "WCCOAopc";
// S7
const string S7_MANAGER_NAME = "WCCOAs7";
// CMW
const string CMW_MANAGER_NAME = "WCCOACMWClient";

//VacAddManager:
// Function Insert manager
//	Call example:
// 		VacAddManager( archiveManagerName, "manual", 30, 1, 1, "-num 2" );
//
// RETURN:
//	OK - manager added successfully
//	FAILURE - can not add manager
//
// Authors:
//    @MMS
// Modify:
//    @ASA
//    @LIK
//  24.01.2011
// Replaces by new function fwInstallationManager_pmonGetManagers()
// for PVSS3.6 with new UNICOS4.3.3
//
int VacAddManager( string manager, string startMode, int secKill, int restartCount, int resetMin, string commandLine )
{
  bool           failed, disabled, err;
  dyn_int        diManPos, diStartMode, diSecKill, diRestartCount, diResetMin;
  dyn_string     dsManager, dsCommandLine, dsProps;
  string         str, host;
  int            pos, port, iErr;
  dyn_dyn_mixed  managersInfo;  

  //DebugN( "VacAddManager(): manager, startMode, secKill, restartCount, resetMin, commandLine", manager, startMode, secKill, restartCount, resetMin, commandLine );

  iErr = paGetProjHostPort( paGetProjName(), host, port );

  dpGet( "fwInstallationInfo.addManagersDisabled", disabled );
  if( disabled ) return( FAILURE );

  // !!! Replaces by new function fwInstallationManager_pmonGetManagers() 24.01.2011
  // for PVSS3.6 with new UNICOS4.3.3
  //
  //pmonGetManagers( failed, paGetProjName(), diManPos, dsManager, diStartMode, diSecKill, diRestartCount, diResetMin, dsCommandLine);
  //DebugN( dsManager, dsCommandLine );
  //if( failed ) return( FAILURE );
  //
  //pos = dynlen( dsManager );
  //
  //if( dynContains( dsManager, manager ) )
  //{
  //	for( int i=1; i<=dynlen(dsManager); i++ )
  //	{
  //		if( ( dsManager[i] == manager ) && ( dsCommandLine[i] == commandLine ) )
  //			return( OK ) ; // manager already existing
  //	}
  //}
        
        if(fwInstallationManager_pmonGetManagers(managersInfo) < 0)
        {
          return FAILURE;
        }
        // DebugTN("VacAddManager():", managersInfo);
        // DebugTN("VacAddManager(): search for manager <" + manager + ">, commandLine <" + commandLine +">");
        pos = dynlen(managersInfo[FW_INSTALLATION_MANAGER_TYPE]); // pos is number of manager in PVSS console
        for(int i = pos ; i > 0 ; i--)
        {
          //DebugN("VacAddManager():",i,managersInfo[FW_INSTALLATION_MANAGER_TYPE][i]);
          //if(managersInfo[FW_INSTALLATION_MANAGER_TYPE][i] == manager)
          //{
          //  DebugTN("Manager FOUND:", managersInfo[FW_INSTALLATION_MANAGER_OPTIONS][i]);
          //}
          if((managersInfo[FW_INSTALLATION_MANAGER_TYPE][i] == manager) &&
             (managersInfo[FW_INSTALLATION_MANAGER_OPTIONS][i] == commandLine))
          {
          // DebugTN("Manager " + manager + " found", commandLine);
          return OK; // manager already existing
          }
        }
        DebugN("vclManagers.ctl: Manager <" + manager + " " + commandLine + "> NOT found!");
	
        str = "##SINGLE_MGR:INS "+(pos)+" "+
		manager+" "+startMode+" "+secKill+" "+restartCount+" "+
		resetMin+" "+commandLine;
        DebugN("vclManagers.ctl: Try add new manager in PVSS Console after position number ", pos);
        DebugN("vclManagers.ctl: using pmon_command: ", str);
	err = pmon_command( str, host, port, FALSE, TRUE );
	if( err ) return( FAILURE );

	return( OK );
}

//VacGetManagerInfo:
// get manager status
//
int VacGetManagerInfo( int managerIndex )
{
	string 					host;
	int 						err;
	int 						port, iErr = paGetProjHostPort( paGetProjName(), host, port );
  dyn_dyn_string 	ddsResult;
  	
  err = pmon_query( "##MGRLIST:STATI", host, port, ddsResult, true, true );
	//DebugN( "VacGetManagerInfo:", ddsResult[managerIndex][1] );
	return ddsResult[managerIndex][1];
}

//VacStartManager:
//
//
bool VacStartManager( int managerIndex )
{
	string 	str, host;
	bool 		err;
	int 		port, iErr = paGetProjHostPort( paGetProjName(), host, port );
	
 	sprintf( str, "##SINGLE_MGR:START %d", managerIndex - 1 );
	//DebugN("Command:", str);
  err = pmon_command( str, host, port, true, true );
  return( err );
}

//VacStopManager:
//
//
bool VacStopManager( int managerIndex )
{
	string 	str, host;
	bool 		err;
	int 		port, iErr = paGetProjHostPort( paGetProjName(), host, port );
	
 	sprintf( str, "##SINGLE_MGR:STOP %d", managerIndex - 1 );
	//DebugN("VacStopManager:", str);
	err = pmon_command( str, host, port, true, true );
	return( err );
}

//VacDeleteManager:
// remove manager at position manPos from progs
//
bool VacDeleteManager( int managerIndex )
{
  string	str, host;
	bool		err;
  int			port, iErr = paGetProjHostPort( paGetProjName(), host, port );

  if ( iErr )
  {
    pmon_warningOutput( "errHostOrPort", -1 );
    return( true );
  }

  sprintf( str, "##SINGLE_MGR:DEL %d ", managerIndex - 1 );

  err = pmon_command( str, host, port, true, true );
	return( err );
}

//VacGetManagerIndexes:
//
// PARAMETERS:
//	managerName
//	num
//	managerIndex - manager indexes
//
// RETURN
//	Number of managers found. If not found - return 0.
//
int VacGetManagerIndexes( string managerName, int num, dyn_int &managerIndexes )
{
	bool				failed;
	dyn_int			diManPos, diStartMode, diSecKill, diRestartCount, diResetMin;
	dyn_string	dsManager, dsCommandLine, dsProps, cmdComp;
	string			str;
	int					coco, value, len, i;
			
	dynClear( managerIndexes );

        /* Replaces by new function fwInstallationManager_pmonGetManagers() 24.01.2011
	pmonGetManagers( failed, paGetProjName(),
		diManPos, dsManager, diStartMode, diSecKill, diRestartCount, diResetMin, dsCommandLine );
	//DebugN( dsManager, dsCommandLine );
	
	if( ! failed )
	{
		for( i = 1; i <= dynlen( dsManager ); i++ )
		{
			if( dsManager[i] == managerName )
			{
				cmdComp = strsplit( dsCommandLine[i], " " );
				if( dynContains( cmdComp, "-num" ) )
				{
					for( int k = 1; k <= dynlen( cmdComp ); k++ )
					{
						if( cmdComp[k] == "-num" )
						{
							coco = sscanf( cmdComp[k+1], "%d", value );
							if( value == num )
							{
								//DebugN( "VacGetManagerIndexes: manager found!", managerName, num, i );
								dynAppend( managerIndexes, i );
							}
						}
					}
				}
			}
		}	
	}
        */
  dyn_dyn_mixed managersInfo;
  if(fwInstallationManager_pmonGetManagers(managersInfo) < 0)
  {
    return 0;
  }
//DebugTN("VacGetManagerIndexes():", managersInfo);
//DebugTN("VacGetManagerIndexes(): search for manager + <" + managerName + ">");
  for(int i = dynlen(managersInfo[FW_INSTALLATION_MANAGER_TYPE]) ; i > 0 ; i--)
  {
//DebugTN("VacGetManagerIndexes(): managersInfo[FW_INSTALLATION_MANAGER_TYPE][" + i + "] = <" + managersInfo[FW_INSTALLATION_MANAGER_TYPE][i] + ">");
    if(managersInfo[FW_INSTALLATION_MANAGER_TYPE][i] == managerName)
    {
//DebugTN("VacGetManagerIndexes(): manager FOUND", managersInfo[FW_INSTALLATION_MANAGER_OPTIONS][i]);
      cmdComp = strsplit(managersInfo[FW_INSTALLATION_MANAGER_OPTIONS][i], " ");
      for( int k = 1; k <= dynlen( cmdComp ); k++ )
      {
        if( cmdComp[k] == "-num" )
        {
          coco = sscanf( cmdComp[k+1], "%d", value );
          if( value == num )
          {
            //DebugN( "VacGetManagerIndexes: manager found!", managerName, num, i );
            dynAppend( managerIndexes, i );
          }
        }
      }
    }
  }
        
	len = dynlen( managerIndexes );
	if( len < 1 ) return( 0 );
	return( len );	
}

//VacGetManagerProperties:
//
//
int VacGetManagerIndexProperties( int managerIndex,
	string &startMode, int &diSecKill, int &diRestartCount, int &diResetMin, string &dsCommandLine )
{
	bool failed;	
	dyn_int diManPosS, diStartModeS, diSecKillS, diRestartCountS, diResetMinS;
	dyn_string dsManagerS, dsCommandLineS;
	/* Replaces by new function fwInstallationManager_pmonGetManagers() 24.01.2011
	pmonGetManagers( failed, paGetProjName( ),
		diManPosS, dsManagerS, diStartModeS, diSecKillS, diRestartCountS, diResetMinS, dsCommandLineS );
	if( failed ) return FAILURE;

	switch( diStartModeS[managerIndex] )
	{
	case 0: startMode = "manual"; break;
	case 1: startMode = "once"; break;
	case 2: startMode = "always"; break;
	}
	diSecKill = diSecKillS[managerIndex];
	diRestartCount = diRestartCountS[managerIndex];
	diResetMin = diResetMinS[managerIndex];
	dsCommandLine = dsCommandLineS[managerIndex];
        */

  dyn_dyn_mixed managersInfo;
  if(fwInstallationManager_pmonGetManagers(managersInfo) < 0)
  {
    return FAILURE;
  }
//DebugTN("VacGetManagerIndexProperties():", managersInfo);
// DebugTN("VacGetManagerIndexProperties(): search for index " + managerIndex + ", got " + dynlen(managersInfo));

  switch(managersInfo[FW_INSTALLATION_MANAGER_START_MODE][managerIndex])
  {
  case 0: startMode = "manual"; break;
  case 1: startMode = "once"; break;
  case 2: startMode = "always"; break;
  }
  diSecKill = managersInfo[FW_INSTALLATION_MANAGER_SEC_KILL][managerIndex];
  diRestartCount = managersInfo[FW_INSTALLATION_MANAGER_RESTART_COUNT][managerIndex];
  diResetMin = managersInfo[FW_INSTALLATION_MANAGER_RESET_MIN][managerIndex];
  dsCommandLine = managersInfo[FW_INSTALLATION_MANAGER_OPTIONS][managerIndex];
	return OK;
}

//VacChangeManagerProperties:
// change manager properties (excepting name) at position managerIndex 
// managerIndex can be 1,2... - not 0
// (Look function 
//	pmonChangeManagerProps
// from pa.ctl)
bool VacChangeManagerProperties( int managerIndex,
	string startMode, int diSecKill, int diRestartCount, int diResetMin, string dsCommandLine )
{
  bool 			err;
  string 		str, host;
  int    		port, iErr = paGetProjHostPort( paGetProjName(), host, port );

  if ( iErr )
  {
    pmon_warningOutput( "errHostOrPort", -1 );
    return( true );
  }

	//example of str = "##SINGLE_MGR:PROP_PUT 15 manual 30 3 1 -num 1";

	str = "##SINGLE_MGR:PROP_PUT" +
  			" " + ( managerIndex - 1 ) +
        " " + startMode +
        " " + diSecKill +
        " " + diRestartCount +
        " " + diResetMin +
        " " + dsCommandLine;
	//DebugN( "VacChangeManagerProperties: managerIndex, pmon_command(str,...)", managerIndex, str );	
  err = pmon_command( str, host, port, true, true );
  return( err );
}

//IsManagerStarted:
//
//
bool IsManagerStarted( int managerIndex )
{
	if( VacGetManagerInfo( managerIndex ) == 2 ) return true;
	return false;
}

//IsManagerStopped:
//
//
bool IsManagerStopped( int managerIndex )
{
	if( VacGetManagerInfo( managerIndex ) == 0 ) return true;
	return false;
}

//WaitManagerStart:
//
//
int WaitManagerStart( int managerIndex, int sec )
{
	int state = -1;
	if( sec < 0 ) sec = 1; // correct if wrong value
	while( state != 2 )
	{
		delay( 1 );
		sec--;
		if( sec < 0 ) return FAILURE; // can not start manager <sec> seconds
		state = VacGetManagerInfo( managerIndex );
	}
	return OK;
}

//WaitManagerStop:
//
//
int WaitManagerStop( int managerIndex, int sec )
{
	int state = -1;
	if( sec < 0 ) sec = 1;
	while( state != 0 )
	{
		delay( 1 );
		sec--;
		if( sec < 0 ) return FAILURE; // can not stop manager <sec> seconds
		state = VacGetManagerInfo( managerIndex );
	}
	return OK;
}
