/**@name SCRIPT: vcsBeamParam.ctl

@author: Mikhail Mikheev (IHEP, Protvino, Russia)

Creation Date: 29/10/2010

version 1.0

Modification History:
Purpose: 

script vcsBeamParam dedicated to calculation of the beam peremeters
 1. Beams current in [mA]
 2. Critical energy
 3. Photon flux
 4. Photon dose
 5. Integrated beam current

Usage: Public

  	. constant:
	. data point type needed: VMachineParam, _TimedFunc
	. data point: VMachineParam, _BeamParamTd;
	. PVSS version: 3.1
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.

*/

  float iBeamR, iBeamB, iBeamMaB, iBeamMaR, energy, ctrEnergy, iBeamRLast, iBeamBLast, energyLast;
  float phoFlB, phoFlR, phoDoB, phoDoR, phoDoAR, phoDoAB;
  float initPhoDoB, initPhoDoR, initPhoDoAR, initPhoDoAB;
  unsigned tCurB, tCurR, tEnergy;
  
  dyn_float cursB, cursR, ens;
  dyn_time timesR, timesB, timesE;
  int flag, initFlag = true;
  int processingInterval = 15; //in sec
// Name of DP type containing alarm mask configuration
  const string VAC_BEAM_PARAM_DP_TYPE = "VBeamParamFloat";
  const string VAC_BEAM_PARAM_E = "Beam_E",
             VAC_BEAM_PARAM_CTRE = "Crit_E",
             VAC_BEAM_PARAM_PFB = "PhF_B",
             VAC_BEAM_PARAM_PFR = "PhF_R",
             VAC_BEAM_PARAM_DOB = "PhDm_B",
             VAC_BEAM_PARAM_DOR = "PhDm_R",
             VAC_BEAM_PARAM_DMB = "PhDmA_B",
             VAC_BEAM_PARAM_DMR = "PhDmA_R",
             VAC_BEAM_PARAM_CMB = "CurMa_B",
             VAC_BEAM_PARAM_CMR = "CurMa_R",
             VAC_BEAM_PARAM_DOB_BASE = "PhDm_B_Base",
             VAC_BEAM_PARAM_DOR_BASE = "PhDm_R_Base",
             VAC_BEAM_PARAM_DMB_BASE = "PhDmA_B_Base",
             VAC_BEAM_PARAM_DMR_BASE = "PhDmA_R_Base";

  const string VAC_BEAM_CURRENT_B = "BEAM_DC_B";
  const string VAC_BEAM_CURRENT_R = "BEAM_DC_R";
  const string VAC_BEAM_FILL_NUMBER = "FillNum";
  
// Name of DP type containing time configuration
  const string BEAM_PARAM_TIMED_DP_TYPE = "_TimedFunc";

// Name of DP containing time configuration
  const string BEAM_PARAM_TIMED_DP = "_BeamParamTd";
string  paramDpName, timedDpName;
//Filling schemas
dyn_bool gFsB_CMW, gFsR_CMW, gFsB, gFsR; 
string gFillName, gFillName_CMW;
main()
{
string dpe, archiveClass; 
bool configExists, isActive;
int smoothProcedure, archiveType;
float deadband, timeInterval;
dyn_string  exceptionInfo;
	timedDpName = "";
	if( !dpExists( BEAM_PARAM_TIMED_DP ) )
	{
	  if( dpCreate( BEAM_PARAM_TIMED_DP, BEAM_PARAM_TIMED_DP_TYPE ) < 0 )
	  {
		  fwException_raise(exceptionInfo, "vcsBeamParam.ctl::main() ERROR", " Time config DP dpCreate() failed", "" );
		  return;
	  }
	  if( ! dpExists( BEAM_PARAM_TIMED_DP ) )
	  {
		  fwException_raise(exceptionInfo, "vcsBeamParam.ctl::main() ERROR", "Time config DP not exist", "" );
		  return;
	  }
	}
	timedDpName = BEAM_PARAM_TIMED_DP;
  	paramDpName = "";
//DebugTN("vcsBeamParam.ctl: ArchSettings Start");        
//Read the configuration of the Archive setting for beam param dps and correct if needed
//Energy
   smoothProcedure = DPATTR_TIME_AND_VALUE_SMOOTH;
   deadband = 10;
   timeInterval = 60;
   SetArchiveParameters(VAC_BEAM_PARAM_E, smoothProcedure, deadband, timeInterval);
//CrtEnergy
   smoothProcedure = DPATTR_TIME_AND_VALUE_REL_SMOOTH;
   deadband = 5;
   timeInterval = 60;
   SetArchiveParameters(VAC_BEAM_PARAM_CTRE, smoothProcedure, deadband, timeInterval);
//Beam corrent B
   smoothProcedure = DPATTR_TIME_AND_VALUE_REL_SMOOTH;
   deadband = 5;
   timeInterval = 60;
   SetArchiveParameters(VAC_BEAM_PARAM_CMB, smoothProcedure, deadband, timeInterval);
//Beam corrent R
   SetArchiveParameters(VAC_BEAM_PARAM_CMR, smoothProcedure, deadband, timeInterval);
//Flux B
   smoothProcedure = DPATTR_TIME_AND_VALUE_REL_SMOOTH;
   deadband = 5; // =10 * 3e9 * 1.808e-12; (E*ImA)
   timeInterval = 60;
   SetArchiveParameters(VAC_BEAM_PARAM_PFB, smoothProcedure, deadband, timeInterval);
//Flux R
   SetArchiveParameters(VAC_BEAM_PARAM_PFR, smoothProcedure, deadband, timeInterval);
//Dose B
   smoothProcedure = DPATTR_TIME_AND_VALUE_REL_SMOOTH;
   deadband = 5; // = 10 * 3e9 * 1.808e-12; (E*ImA)
   timeInterval = 60;
   SetArchiveParameters(VAC_BEAM_PARAM_DOB, smoothProcedure, deadband, timeInterval);
//Dose R
   SetArchiveParameters(VAC_BEAM_PARAM_DOR, smoothProcedure, deadband, timeInterval);
//Dose mA B
   smoothProcedure = DPATTR_TIME_AND_VALUE_REL_SMOOTH;
   deadband = 5; // = 10 * 3e9 * 1.808e-12; (E*ImA)
   timeInterval = 60;
   SetArchiveParameters(VAC_BEAM_PARAM_DMB, smoothProcedure, deadband, timeInterval);
//Dose mA R
   SetArchiveParameters(VAC_BEAM_PARAM_DMR, smoothProcedure, deadband, timeInterval);
//FillNumber
   smoothProcedure = DPATTR_COMPARE_OLD_NEW;
   SetArchiveParameters(VAC_BEAM_FILL_NUMBER, smoothProcedure, deadband, timeInterval);   
   DebugTN("vcsBeamParam.ctl: ArchSettings End");
	if( !dpExists( "FillSchema_B_CMW" ) )
	{
	  if( dpCreate( "FillSchema_B_CMW", "VBeamParamDynBoolCMW" ) < 0 )
	  {
	    fwException_raise(exceptionInfo, "vcsBeamParam.ctl::main() ERROR", "FillSchema_B_CMW dpCreate() failed", "" );
	    return;
	  }
        }  
	if( !dpExists( "FillSchema_R_CMW" ) )
	{
	  if( dpCreate( "FillSchema_R_CMW", "VBeamParamDynBoolCMW" ) < 0 )
	  {
	    fwException_raise(exceptionInfo, "vcsBeamParam.ctl::main() ERROR", "FillSchema_R_CMW dpCreate() failed", "" );
	    return;
	  }
  }  
           
	if( !dpExists( VAC_BEAM_PARAM_DOB_BASE ) )
	{
	  if( dpCreate( VAC_BEAM_PARAM_DOB_BASE, VAC_BEAM_PARAM_DP_TYPE ) < 0 )
	  {
	    fwException_raise(exceptionInfo, "vcsBeamParam.ctl::main() ERROR", " VAC_BEAM_PARAM_DOB_BASE dpCreate() failed", "" );
	    return;
	  }
  }  
	if( !dpExists( VAC_BEAM_PARAM_DOR_BASE ) )
	{
	  if( dpCreate( VAC_BEAM_PARAM_DOR_BASE, VAC_BEAM_PARAM_DP_TYPE ) < 0 )
	  {
	    fwException_raise(exceptionInfo, "vcsBeamParam.ctl::main() ERROR", " VAC_BEAM_PARAM_DOR_BASE dpCreate() failed", "" );
	    return;
	  }
  }  
	if( !dpExists( VAC_BEAM_PARAM_DMB_BASE ) )
	{
	  if( dpCreate( VAC_BEAM_PARAM_DMB_BASE, VAC_BEAM_PARAM_DP_TYPE ) < 0 )
	  {
	    fwException_raise(exceptionInfo, "vcsBeamParam.ctl::main() ERROR", " VAC_BEAM_PARAM_DMB_BASE dpCreate() failed", "" );
	    return;
	  }
        }  
	if( !dpExists( VAC_BEAM_PARAM_DMR_BASE ) )
	{
	  if( dpCreate( VAC_BEAM_PARAM_DMR_BASE, VAC_BEAM_PARAM_DP_TYPE ) < 0 )
	  {
	    fwException_raise(exceptionInfo, "vcsBeamParam.ctl::main() ERROR", " VAC_BEAM_PARAM_DMR_BASE dpCreate() failed", "" );
	    return;
	  }
  }  

                
//Connect to beam current source dps        
  dpConnect("BeamCurrentBCb", TRUE, VAC_BEAM_CURRENT_B + ".Intensity:_online.._value", 
                              VAC_BEAM_CURRENT_B + ".Intensity:_online.._stime");
  dpConnect("BeamCurrentRCb", TRUE, VAC_BEAM_CURRENT_R + ".Intensity:_online.._value", 
                              VAC_BEAM_CURRENT_R + ".Intensity:_online.._stime");
//Connect to beam energy source dps        
  dpConnect("BeamEnergyCb", TRUE, VAC_BEAM_PARAM_E + ".Value:_online.._value", 
                              VAC_BEAM_PARAM_E + ".Value:_online.._stime");   

//Get initial parameters for the doses
// L.Kopylov 11.06.2015 Not get(), but connect() - in order to react
// on changes of value by admin on the fly
  dpConnect("CBinitPhoDoB", VAC_BEAM_PARAM_DOB + ".Value");
  dpConnect("CBinitPhoDoR", VAC_BEAM_PARAM_DOR + ".Value");
  dpConnect("CBinitPhoDoAB", VAC_BEAM_PARAM_DMB + ".Value");
  dpConnect("CBinitPhoDoAR", VAC_BEAM_PARAM_DMR + ".Value");
  
//Connect to filling schema dps        

  if( dpExists("FillSchema_R") && dpExists("FillSchema_B"))
  {
    dpConnect("BeamFillChangeCb", "FillSchema_R.Value", "FillSchema_B.Value");
  }
  else
    DebugTN("vcsBeamParam.ctl: FillSchemaDpMirror::ERROR: dp FillSchema_B or FillSchema_R doesn't exist");
  if( dpExists("FillSchema_R_CMW") && dpExists("FillSchema_B_CMW"))
  {
    dpConnect("BeamFillCMWChangeCb", "FillSchema_R_CMW.Value", "FillSchema_B_CMW.Value");
  }
  else
    DebugTN("vcsBeamParam.ctl: FillSchemaDpMirror::ERROR: dp FillSchema_B_CMW or FillSchema_R_CMW doesn't exist");
  if( dpExists("FillNameCMW") )
  {
    dpConnect("FillNameCMWChangeCb", "FillNameCMW.Value");
  }
  else
    DebugTN("vcsBeamParam.ctl: FillNameCMWChangeCb::ERROR: dp FillNameCMW  doesn't exist");

    
//Configure  timed dp to perform calculation with processingInterval interval
  dpSet( timedDpName + ".validFrom", makeTime(2006,6,1),
      	timedDpName + ".validUntil", makeTime(2032,6,1),
        timedDpName + ".time", makeDynInt(), 				
        timedDpName + ".monthDay", makeDynInt(), 				
        timedDpName + ".month", makeDynInt(), 				
        timedDpName + ".weekDay", makeDynInt(), 			
        timedDpName + ".month", makeDynInt(), 				
     	  timedDpName + ".delay", 0,
      	timedDpName + ".interval", processingInterval,			//Interval  in sec
     		timedDpName + ".syncDay", -1,
     		timedDpName + ".syncWeekDay", -1,
    		timedDpName + ".syncTime", -1, 
     		timedDpName + ".syncMonth", -1);
        
 	timedFunc ("ProcessValues", timedDpName);
  dpGet(VAC_BEAM_CURRENT_B + ".Intensity:_online.._value", iBeamB);
  iBeamBLast = 1.808e-12 * iBeamB;	
  dpGet(VAC_BEAM_CURRENT_R + ".Intensity:_online.._value", iBeamR);	
  iBeamRLast = 1.808e-12 * iBeamR;
  DebugTN("vcsBeamParam.ctl::main():Init completed");        
  initFlag = false;
}
void ProcessValues()
{
  float sumE = 0, sumB = 0, sumR = 0;
  flag = 1;
  int nb = dynlen(cursB);
  int nr = dynlen(cursR);
  int ne = dynlen(ens);
//DebugTN("vcsBeamParam.ctl::", nb, nr, ne);
  if(nb > 2)
  {  
    for(int i = 1; i <= nb; i++)
    {
      sumB = sumB + cursB[i];
    }
    tCurB = period(timesB[nb]) - period(timesB[1]);
    iBeamMaB = sumB/nb;
  }
  else
    iBeamMaB = iBeamBLast;
  if(nr > 2)
  {  
    for(int i = 1; i <= nr; i++)
    {
      sumR = sumR + cursR[i];
    }
    tCurR = period(timesR[nr]) - period(timesR[1]);
    iBeamMaR = sumR/nr;
  }
  else
    iBeamMaR = iBeamRLast;
  if(ne > 2)
  {  
    for(int i = 1; i <= ne; i++)
    {
      sumE = sumE + ens[i];
    }
    tEnergy = period(timesE[ne]) - period(timesE[1]);
    energy = sumE/ne;
  }
  else
    energy = energyLast;
  //Finished with arrays - release
  dynClear(cursB);
  dynClear(cursR);
  dynClear(timesB);
  dynClear(timesR);
  dynClear(ens);
  dynClear(timesE);
  flag = 0;
//Calculate and store
  ctrEnergy = 1.28704e-10 * pow(energy, 3.0);
  phoFlB = 2.5202e10 * energy * iBeamMaB;
  phoFlR = 2.5202e10 * energy * iBeamMaR;
  dpSet(VAC_BEAM_PARAM_CTRE + ".Value", ctrEnergy);
  dpSet(VAC_BEAM_PARAM_PFB + ".Value", phoFlB);
  dpSet(VAC_BEAM_PARAM_PFR + ".Value", phoFlR);

//  DebugTN("vcsBeamParam.ctl", energy, iBeamMaB);        
  if(energy > 2000.)
  {
//  DebugTN("vcsBeamParam.ctl::E > 2", processingInterval);        
      phoDoB = initPhoDoB + phoFlB * processingInterval; //tCurB; 
      phoDoR = initPhoDoR + phoFlR * processingInterval; //tCurR;  
      phoDoAB = initPhoDoAB + iBeamMaB * processingInterval/3600;  
      phoDoAR = initPhoDoAR + iBeamMaR * processingInterval/3600;  
      dpSet(VAC_BEAM_PARAM_DOB + ".Value", phoDoB);
      dpSet(VAC_BEAM_PARAM_DOR + ".Value", phoDoR);
      dpSet(VAC_BEAM_PARAM_DMB + ".Value", phoDoAB);
      dpSet(VAC_BEAM_PARAM_DMR + ".Value", phoDoAR);
      /* L.Kopylov 11.06.2015 Not needed anymore because dpConnect() is used
      initPhoDoB  = phoDoB ;
      initPhoDoR  = phoDoR ;
      initPhoDoAB = phoDoAB;
      initPhoDoAR = phoDoAR;
      */
  }
  else
  {
//  DebugTN("vcsBeamParam.ctl::E < 2");        
      phoDoB = initPhoDoB;
      phoDoR = initPhoDoR;
      phoDoAB = initPhoDoAB;
      phoDoAR = initPhoDoAR;
  }
}
void BeamCurrentBCb(string dpeCb, float curB, string dpeTb, time timeB)
{
    iBeamBLast = 1.808e-12 * curB;
//    phoFlB = 2.5202e10*energyLast*iBeamBLast;
    dpSet(VAC_BEAM_PARAM_CMB + ".Value", iBeamBLast);
//    dpSet(VAC_BEAM_PARAM_PFB + ".Value",  phoFlB);
     if(flag)
       return;
     dynAppend(cursB, iBeamBLast);  
     dynAppend(timesB, timeB);  
}
void BeamCurrentRCb(string dpeCr, float curR, string dpeTr, time timeR)
{
    
    iBeamRLast = 1.808e-12 * curR;
//    phoFlR = 2.5202e10*energyLast*iBeamRLast;
    dpSet(VAC_BEAM_PARAM_CMR + ".Value", iBeamRLast);
//    dpSet(VAC_BEAM_PARAM_PFR + ".Value",  phoFlR);
    if(flag)
       return;
   dynAppend(cursR, iBeamRLast);  
   dynAppend(timesR, timeR);  
}
void BeamEnergyCb(string dpeE, float En, string dpeT, time timeE)
{
  energyLast = En;
  tEnergy = timeE;
//  DebugTN ("vcsBeamParam.ctl: BeamEnergyCb", dpeE, En, dpeT, timeE);
   if(flag)
     return;
   dynAppend(ens, energyLast);  
   dynAppend(timesE, timeE);  
//DebugTN("vcsBeamParam.ctl: BeamEnergyCb", dynlen(ens));   
}
SetArchiveParameters(string dpName, int setSmoothProcedure, float setDeadband, float setTimeInterval)
{
string dpe, archiveClass; 
bool configExists, isActive;
int smoothProcedure, archiveType;
float deadband, timeInterval;
dyn_string  exceptionInfo;
//Read the configuration of the Archive setting for beam param dps and correct if needed
//Energy
   dpe = dpName + ".Value";
   fwArchive_get(dpe, configExists, archiveClass, archiveType,
                 smoothProcedure, deadband, timeInterval, isActive, exceptionInfo); 
   if( dynlen(exceptionInfo))
	{
		fwException_raise(exceptionInfo, "vcsBeamParam.ctl::main() ERROR", "Error reading archive param", "" );
      Debug(exceptionInfo, dpName); 
      dynClear(exceptionInfo);       
	}
   smoothProcedure = setSmoothProcedure;
   deadband = setDeadband;
   timeInterval = setTimeInterval;

   fwArchive_set(dpe, archiveClass, archiveType,
                  smoothProcedure, deadband, timeInterval, exceptionInfo, TRUE); 
   if( dynlen(exceptionInfo))
	{
		fwException_raise(exceptionInfo, "vcsBeamParam.ctl::main() ERROR", "Error reading archive param", "" );
      Debug(exceptionInfo, dpName);        
      dynClear(exceptionInfo);       
	}

}  
BeamFillChangeCb(string dp1, dyn_bool fsR, string dp2, dyn_bool fsB )
{
   gFsB = fsB;
   gFsR = fsR;
}
BeamFillCMWChangeCb(string dp1, dyn_bool fsR, string dp2, dyn_bool fsB)
{
   gFsB_CMW = fsB;
   gFsR_CMW = fsR;
   if(gFsB_CMW != gFsB)
   {
     dpSet("FillSchema_B.Value", gFsB_CMW);
   }
   if(gFsR_CMW != gFsR)
   {
     dpSet("FillSchema_R.Value", gFsR_CMW);
   }
//DebugTN("vcsBeamParam.ctl: BeamFillChangeCb Finished");
}
FillNameCMWChangeCb(string dp1, string fillName)
{
  gFillName_CMW = fillName;
  if(gFillName_CMW != gFillName)
  {
     dpSet("FillName.Value", fillName);
     gFillName = fillName;
  }
}

void CBinitPhoDoB(string dpe, float value)
{
  initPhoDoB = value;
}

void CBinitPhoDoR(string dpe, float value)
{
  initPhoDoR = value;
}

void CBinitPhoDoAB(string dpe, float value)
{
  initPhoDoAB = value;
}

void CBinitPhoDoAR(string dpe, float value)
{
  initPhoDoAR = value;
}

