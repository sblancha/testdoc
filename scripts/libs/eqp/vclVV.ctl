
#uses "vclMachineMode.ctl"  // Load CTRL library

// Bitmasks for SVCU_SPS status bits (RR1 - 32 bits)
const unsigned
	SVCU_SPS_R_LOCKED = 0x80000000u,
	SVCU_SPS_R_VALID = 0x40000000u,
	SVCU_SPS_R_REMOTE = 0x20000000u,
	SVCU_SPS_R_DP_INHIBIT = 0x10000000u,
	SVCU_SPS_R_TST_PT = 0x10000000u,
	SVCU_SPS_R_AUTO = 0x8000000u,
	SVCU_SPS_R_INTL_INHIBIT = 0x04000000u,
	SVCU_SPS_R_CTRL_KEY = 0x04000000u,
	SVCU_SPS_R_OPEN_CMD = 0x02000000u,
	SVCU_SPS_R_RESET_CMD = 0x01000000u,
	SVCU_SPS_R_ERRORS = 0x00800000u,
	SVCU_SPS_R_WARNINGS = 0x00400000u,
	SVCU_SPS_R_CIR = 0x00200000u,
	SVCU_SPS_R_OPEN_ENABLE = 0x00100000u,
	SVCU_SPS_R_EXT = 0x00080000u,
	SVCU_SPS_R_CLOSE_ENABLE = 0x00080000u,
	SVCU_SPS_R_VS = 0x00040000u,
	SVCU_SPS_R_OPEN = 0x00020000u,
	SVCU_SPS_R_CLOSED = 0x00010000u,
  SVCU_SPS_R_REMOTE_DI = 0x00080000u,
	SVCU_SPS_R_CONNECTED_DI = 0x00040000u,
// VV_STD concerning bits  
  VV_STD_RR_BIT_HW_INTLCK = 0x20000000u, 
  VV_STD_RR_BIT_INTLCK = 0x00200000u,
  VV_STD_RR_BIT_AUTO_ON = 0x00080000u, 
  VV_STD_RR_BIT_MAN_ON = 0x00040000u, 
  VV_STD_RR_BIT_FORCED = 0x00010000u, 
  VV_STD_RR_BIT_OPEN_DO = 0x00020000u,
  VV_STD_RR_BIT_OPEN_INTLCK = 0x00100000u,
  VV_STD_RR_BIT_MANUAL = 0x04000000u, 
  VV_STD_RR_BIT_OPEN_ENABLE = 0x08000000u, 
  VV_STD_RR_BIT_LOCAL   = 0x10000000u,
  VV_STD_RR_BIT_PWREADY = 0x08000000u; 

const unsigned SVCU_SPS_W_REMOTE = 0x2000u,
	SVCU_SPS_W_LOCAL =   0x1000u,
    SVCU_SPS_W_AUTO =   0x0800u,
	SVCU_SPS_W_MANUAL =  0x0400u,
	SVCU_SPS_W_OPEN =    0x0200u,
	SVCU_SPS_W_CLOSE =   0x0100u,
	SVCU_SPS_W_FORCE =   0x4000u,
	SVCU_SPS_W_RESET =     0x8000u,
	SVCU_SPS_W_RESET_ON =  0x0002u,
	SVCU_SPS_W_RESET_OFF = 0x0001u,
	SVCU_SPS_W_OPEN_VALIDATION = 0x00001u,
   VV_W_OPEN_DELAYED =    0x0002u;

const unsigned SVCU_VV_AO_W_REMOTE =  0x20000000u,
	SVCU_SPS_VV_AO_W_LOCAL =            0x10000000u,
  SVCU_SPS_VV_AO_W_AUTO =             0x08000000u,
	SVCU_SPS_VV_AO_W_MANUAL =          0x04000000u,
	SVCU_SPS_VV_AO_W_OPEN =            0x02000000u,
	SVCU_SPS_VV_AO_W_CLOSE =           0x01000000u,
	SVCU_SPS_VV_AO_W_REMOTE =          0x20000000u,
	SVCU_SPS_VV_AO_W_FORCE =           0x40000000u,
	SVCU_SPS_VV_AO_W_RESET =           0x80000000u;

// Bitmasks for SVCU_SPS interlock bits (RR2 - 16 bits)
const unsigned
	SVCU_SPS_R_CIR_DP_AFTER = 0x2000u,
	SVCU_SPS_R_CIR_DP_BEFORE = 0x1000u,
	SVCU_SPS_R_CIR_GBL_AFTER = 0x0800u,
	SVCU_SPS_R_CIR_GBL_BEFORE = 0x0400u,
	SVCU_SPS_R_CIR_LCL_AFTER = 0x0200u,
	SVCU_SPS_R_CIR_LCL_BEFORE = 0x0100u,
	SVCU_SPS_R_SIR_GBL_AFTER = 0x0008u,
	SVCU_SPS_R_SIR_GBL_BEFORE = 0x0004u,
	SVCU_SPS_R_SIR_LCL_AFTER = 0x0002u,
	SVCU_SPS_R_SIR_LCL_BEFORE = 0x0001u;
// Bitmasks for SVCU_LHC interlock bits (RR2 - 16 bits)
const unsigned
	SVCU_SPS_R_CIR_VVS_AFTER = 0x0800u,
	SVCU_SPS_R_CIR_VVS_BEFORE = 0x0400u,
	SVCU_SPS_R_CIR_LCL_TEMP = 0x0200u,
	SVCU_SPS_R_CIR_LCL_PRESS = 0x0100u,
	SVCU_SPS_R_SIR_VVS_AFTER = 0x0008u,
	SVCU_SPS_R_SIR_VVS_BEFORE = 0x0004u,
	SVCU_SPS_R_SIR_LCL_TEMP = 0x0002u,
	SVCU_SPS_R_SIR_LCL_PRESS = 0x0001u;
// PS_CMW specific
const int VVS_PS_CMW_W_CLOSE = 2,
  VVS_PS_CMW_W_OPEN = 3;
const int VG_PS_CMW_R_CLOSE = 2,
  VVS_PS_CMW_R_OPEN = 3,
  VVS_PS_CMW_R_VALID = 1,
  VVS_PS_CMW_R_CLOSING = 15,
  VVS_PS_CMW_R_OPENING = 16;

// Name of DP type containing E-mail configuration
const string VAC_VALVE_INTLCK_CONFIG_DP_TYPE = "_VacValveIntlckConfig";

// Name of DP containing E-mail configuration
const string VAC_VALVE_INTLCK_CONFIG_DP = "_VacValveIntlckConfig";
// Name of DP type containing time configuration
const string VAC_VALVE_INTLCK_TIMED_DP_TYPE = "_TimedFunc";
const string VAC_VALVE_INTLCK_TIMED_DP = "_ValveIntrlck";
int		gTempInterlockLimit;

// VVS_LHC states correspondent Integer values
const uint VVS_LHC_STATE_CLOSE = 5;
const uint VVS_LHC_STATE_OPEN = 3;
const uint VVS_LHC_STATE_UNDEFINED = 1;

//LhcVacActionPossible_VV:
/** Check if valve is in state that allows executing actions for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param dpType: in, DP type
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_VV(string dpName, string dpType, dyn_anytype action, bool &possible,
  dyn_string &exceptionInfo)
{
  dyn_errClass 	err;
  unsigned		state;
		
  if(dpType == "VVS_PS_CMW")
  {
    possible = false;
    return;    
   } 
  dpGet(dpName + ".RR1", state);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacActionPossible_VV: Read RR1 for <" + dpName + "> failed: " + err, "");
    possible = false;
    return;
  }
  if((state & SVCU_SPS_R_LOCKED) != 0)
  {
    possible = false;
  }
  else
  {
    possible = true;
  }
  unsigned act = action[1];
  if(possible)
  {
    if((act & LVA_ACTION_MASK) == LVA_DEVICE_OPEN)
    {
      int	accessMode;
      LhcVacCheckAccessMode(dpName, accessMode, exceptionInfo);
      if(dynlen(exceptionInfo) != 0)
      {
        return;
      }
      possible = accessMode != ACCESS_MODE_ON;
    }
  }
  // process case FORCE_OPEN_VALVE
}

//LhcVacIsShowMenuItem_VV:
/** Check if menu item has to be shown

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param dpType: in, DP type
	@param showItem: out,  (show/hide) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacIsShowMenuItem_VV(string dpName, string dpType, unsigned action, bool &showItem)
{
  // check the family/type/subtype, family/type/subtype, read only no action 1/7/3 VV_STD_RO
  int family, type, subType = 0;
  LhcVacGetCtlAttributes(dpName, family, type, subType);
  if( (family == 1) &&  (type == 7) && (subType == 3) ) {
    showItem = false;
    return;
  }
  switch(action)
  {
  case LVA_DEVICE_CLOSE:
  case LVA_DEVICE_OPEN:
  case LVA_DEVICE_RESET:
    showItem = true;
    break;
  case LVA_DEVICE_MANUAL:
  case LVA_DEVICE_AUTO:
  case LVA_DEVICE_FORCE:
    showItem = ! VacIsDeviceUsedInProcess(dpName, "VPG_6E01", makeDynString());
    break;
  case LVA_DEVICE_SET_BLOCKED_OFF:
  case LVA_DEVICE_CLEAR_BLOCKED_OFF:  // Only VVRx of VPG_6E01
    showItem = VacIsDeviceUsedInProcess(dpName, "VPG_6E01", makeDynString("VVR1", "VVR2"));
    break;
  default:
    return;
  }
}


//LhcVacIsShowMenuItem_VV_PUL:
/** Check if menu item has to be shown

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param dpType: in, DP type
	@param showItem: out,  (show/hide) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacIsShowMenuItem_VV_PUL(string dpName, string dpType, unsigned action, bool &showItem)
{
	string readOnlyAttr = "1";
	LhcVacGetAttribute(dpName, "ReadOnly", readOnlyAttr);
	if (readOnlyAttr == "1")
	{
		showItem = false;
	}
	else
	{
		showItem = true;
	}
}

//LhcVacGetRequiredActPriv_VV:
/** Check if valve is in state that allows executing actions for it

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL
	@param dpName: in, DP name
	@param defaultPriv: in/out default privilige for action/necessary privilege for action.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacGetRequiredActPriv_VV(string dpName, dyn_anytype action, int &defaultPriv, dyn_string &exceptionInfo)
{
  string dpType = dpTypeName(dpName);
  switch(dpType)
  {
  case "VV_STD_IO":
  case "VV_STD_6E01":
    CheckCommandForManualMode_VV(dpName, action, defaultPriv, exceptionInfo);
    break;
  case "VVS_LHC":
    LhcVacGetRequiredActPriv_VVS_LHC(dpName, action, defaultPriv, exceptionInfo);
    break;
  default:
    // no compound confirm message
    return;
  }
}

void LhcVacGetRequiredActPriv_VVS_LHC(string dpName, dyn_anytype action, int &defaultPriv, dyn_string &exceptionInfo)
{  
  unsigned act = action[1];
  switch(act & LVA_ACTION_MASK)
  {
  case LVA_DEVICE_CLOSE:
    LhcVacGetRequiredClosePrivilege_VV(dpName, defaultPriv, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return;
    }
    break;
  case LVA_DEVICE_OPEN:		// Check CRYO temperatures
    LhcVacGetRequiredOpenPrivilege_VV(dpName, defaultPriv, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      return;
    }
    LhcVacGetRequiredOpenPrivilegeAZ_VV(dpName, defaultPriv, exceptionInfo);
    break;
  default:
    // exist privillege is enough
    return;
  }
}

// make compound confirm message for ALL std objects. S. Blanchard request 29.08.2013
void CheckCommandForManualMode_VV(string dpName, dyn_anytype action, int &defaultPriv, dyn_string &exceptionInfo)
{
  unsigned act = action[1] & LVA_ACTION_MASK;
  switch(act)
  {
    case LVA_DEVICE_OPEN:
    case LVA_DEVICE_CLOSE:
      if(VacIsDeviceUsedInProcess(dpName, "VPG_6E01", makeDynString("VVD1", "VVI1")))
      {
        if(defaultPriv < 3)
        {
          defaultPriv = 3;
        }
      }
      break;
    default:
      return;
  }
  dynAppend(glActSpecialMessages, "ATTENTION: MANUAL MODE COMMAND ");
}

/**
	Closing valve in BEAM mode requires high privilege
*/
private void LhcVacGetRequiredClosePrivilege_VV(string dpName, int &defaultPriv, dyn_string &exceptionInfo)
{
  dyn_errClass 	err;
  int		mode;
  string        modeDpName;
  string	mainPart, sector1, sector2;
  bool		isBorder;
  int		devVacuum;
  
  LhcVacGetMachineModeDp(modeDpName);
  if(modeDpName == "")
  {
    return;
  }
  LhcVacDeviceVacLocation(dpName, devVacuum, sector1, sector2, mainPart, isBorder, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacGetRequiredClosePrivilege_VV: LhcVacDeviceVacLocation(" + dpName + ") failed", "");
    return;
  }
  if((devVacuum == LVA_VACUUM_BLUE_BEAM) || (devVacuum == LVA_VACUUM_CROSS_BEAM))
  {	
    dpGet(modeDpName + ".Beam_B.Beam", mode);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacGetRequiredClosePrivilege_VV: Read <" + modeDpName + ".Beam_B.Beam> failed: " + err, "");
      return;
    }
    // admin privillege is required
    if(mode == 1)
    {
      dynAppend(glActSpecialMessages, "BLUE BEAM MODE ON");
      if(defaultPriv < 4)
      {
        defaultPriv = 4;
      }
      return;
    }
  }
  if((devVacuum == LVA_VACUUM_RED_BEAM) || (devVacuum == LVA_VACUUM_CROSS_BEAM))
  {	
    dpGet(modeDpName + ".Beam_R.Beam", mode);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacGetRequiredClosePrivilege_VV: Read <" + modeDpName + ".Beam_R.Beam> failed: " + err, "");
      return;
    }
    // admin privillege is required
    if (mode == 1)
    {
      dynAppend(glActSpecialMessages, "RED BEAM MODE ON");
      if(defaultPriv < 4)
      {
        defaultPriv = 4;
      }
     }
  }
}

/**
	For opening valve we shall check temperature of closest thermometer(s) for cold sectors
*/
private void LhcVacGetRequiredOpenPrivilege_VV(string dpName, int &privilege, dyn_string &exceptionInfo)
{
  dyn_errClass 	err;
  dyn_string thermoList;

  dpName = dpSubStr(dpName, DPSUB_DP);
  LhcVacGetThermometersForValve(dpName, thermoList, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  if(dynlen(thermoList) == 0)
  {
    return;
  }
  string thermoDp = thermoList[1];	// Only one thermometer is expected

  // Get limit for temperature
  int limit;
  dpGet(dpName + ".CryoInterlockT", limit);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacGetRequiredOpenPrivilege_VV: Read CryoInterlockT for <" + dpName + "> failed: " + err, "");
    return;
  }
  if(limit <= 0)
  {
    limit = 5;	// Use reasonable default
  }

  // Get measured temperature
  int		rr1;
  float	        t;
  dpGet(thermoDp + ".RR1", rr1, thermoDp + ".T", t);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "LhcVacGetRequiredOpenPrivilege_VV: Reading RR1 and T for <" + thermoDp + "> failed: " + err, "");
    return;
  }
  if((rr1 & 0x1) == 0)	// No error
  {
    if(t > limit)
    {
      string devName;
      LhcVacDisplayName(thermoDp, devName, exceptionInfo);
      if(devName == "")
      {
        devName = thermoDp;
      }
      string msg;
      sprintf(msg, "%s: T=%.2f", devName, t);
      dynAppend(glActSpecialMessages, msg);
      if(privilege < 4)
      {
        privilege = 4;  // Admin privilege is required
      }
    }
  }
  else
  {
    dynAppend(glActSpecialMessages, "No valid thermometer");
    if(privilege < 4)
    {
      privilege = 4;  // Admin privilege is required
    }
  }
}

/**
*   For opening valve we shall check state of access zone
*/
private void LhcVacGetRequiredOpenPrivilegeAZ_VV(string dpName, int &privilege, dyn_string &exceptionInfo)
{
  dyn_errClass 	err;
  dyn_string    accessZoneList;
  string        accessZone;

  dpName = dpSubStr(dpName, DPSUB_DP);
  for(int i = 1 ; i < ACCESS_ZONES_PER_VALVE_NUMBER ; i++)
  {
    LhcVacGetAttribute(dpName, "AccessZone" + i, accessZone);
    if(accessZone == "")
    {
      continue;
    }
    dynAppend(accessZoneList, accessZone);
  }
  for(int i = dynlen(accessZoneList) ; i > 0 ; i--)
  {
    int command;
    dpGet(accessZoneList[i] + ".CMD_ACK", command);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "LhcVacGetRequiredOpenPrivilegeAZ_VV: Read command  for access zone<" + accessZoneList[i] + "> failed: " + err, "");
      return;
    } 
    if((command == ACCESS_ZONES_CMD_ON_PVSS) || (command == ACCESS_ZONES_CMD_ON_CMW))
    {
      string msg;
      LhcVacGetAccessZoneCommandName(command, accessZoneList[i], msg);
      dynAppend(glActSpecialMessages, msg);
      privilege = 4;	// Admin privilege is required
    }
  }
}

//LhcVacDeviceAction_VV_AO:
/** Execute action for valve. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VV_AO(string dpName, string dpType, dyn_anytype action, dyn_string &exceptionInfo)
{
  unsigned	newWr1;
  int       newWr1PS;
  bool		possible;
  dyn_errClass	err;
  unsigned      act;
  mapping   mConnectDpes;
  act = action[1] & LVA_ACTION_MASK;
  switch(act & LVA_ACTION_MASK)
  {
  case LVA_DEVICE_CLOSE:
      newWr1 = SVCU_SPS_VV_AO_W_REMOTE | SVCU_SPS_VV_AO_W_CLOSE;
    break;
  case LVA_DEVICE_PART_OPEN:
      newWr1 = SVCU_SPS_VV_AO_W_REMOTE | SVCU_SPS_VV_AO_W_OPEN | SVCU_SPS_VV_AO_W_MANUAL;
    break;
  case LVA_DEVICE_PART_CLOSE:
      newWr1 = SVCU_SPS_VV_AO_W_REMOTE |SVCU_SPS_VV_AO_W_CLOSE;
    break;
  case LVA_DEVICE_MANUAL:
      newWr1 = SVCU_SPS_VV_AO_W_REMOTE | SVCU_SPS_VV_AO_W_MANUAL;
    break;
  case LVA_DEVICE_AUTO:
      newWr1 = SVCU_SPS_VV_AO_W_REMOTE | SVCU_SPS_VV_AO_W_AUTO;
    break;
  case LVA_DEVICE_RESET:
      newWr1 = SVCU_SPS_VV_AO_W_REMOTE | SVCU_SPS_VV_AO_W_RESET;
    break;
  case LVA_DEVICE_FORCE:
      newWr1 = SVCU_SPS_VV_AO_W_REMOTE | SVCU_SPS_VV_AO_W_FORCE;
    break;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["StartCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["SetPointR"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["StartCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["SetPointR"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }

  // Check if action is possible
  LhcVacActionPossible_VV(dpName, dpType, act, possible, exceptionInfo);
  if(dynlen(exceptionInfo) != 0)
  {
     return;
  }
  if(! possible)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: Action forbidden for <" + dpName + ">", "");
    return;
  }	
  // Execute
  if(dynlen(action) > 1)
  {
    unsigned  wr1Value = newWr1 + action[2]; 
    DebugN("SetVV_AO", dpName + ".SetPointW", newWr1, action[2]);    
    dpSetWait(dpName + ".WR1:_original.._value", wr1Value);
  }
  else
  {
     unsigned  wr1Value = newWr1;
     dpSetWait(dpName + ".WR1:_original.._value", newWr1);
  }  
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
}

//LhcVacDeviceAction_VV:
/** Execute action for valve. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VV(string dpName, string dpType, dyn_anytype action, dyn_string &exceptionInfo)
{
  unsigned	newWr1;
  int       newWr1PS;
  bool		possible;
  dyn_errClass	err;
  unsigned      act;
  mapping   mConnectDpes;
  act = action[1] & LVA_ACTION_MASK;
  switch(act & LVA_ACTION_MASK)
  {
  case LVA_DEVICE_OPEN:
    if(dpType == "VVS_PS_CMW") {
      newWr1PS = VVS_PS_CMW_W_OPEN;
    }
    else if(dpType == "VVS_PSB_SUM") {
      newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_OPEN;; // was SVCU_SPS_W_REMOTE | VV_W_OPEN_DELAYED; but finally we keep the same order as individual order
    }
    else {
      newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_OPEN;
      // for analog valve we have to take into account setpoint
      if(dpType == "VLV_ANA")
      {
        newWr1 = newWr1 + 100;
      }
	  else if(dpType == "VVS_LHC")
	  {
		dpSet(dpName + ".Actuation.stateTrigger", VVS_LHC_STATE_OPEN);
	  }

    }
    break;
  case LVA_DEVICE_CLOSE:
    newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_CLOSE;
	if(dpType == "VVS_LHC")
	{
		dpSet(dpName + ".Actuation.stateTrigger", VVS_LHC_STATE_CLOSE);
	}
    break;
  case LVA_DEVICE_PART_OPEN:
    // for analog valve we have to take into account setpoint
    newWr1 = SVCU_SPS_W_REMOTE | (SVCU_SPS_W_OPEN + action[2]);
    break;
  case LVA_DEVICE_PART_CLOSE:
    newWr1 = SVCU_SPS_W_REMOTE | (SVCU_SPS_W_CLOSE + action[2]);
    break;
  case LVA_DEVICE_MANUAL:
     newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_MANUAL;
    break;
  case LVA_DEVICE_AUTO:
     newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_AUTO;
    break;
  case LVA_DEVICE_RESET:
      newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_RESET;
    break;
  case LVA_DEVICE_FORCE:
      newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_FORCE;
    break;
  case LVA_ACTION_VV_TEMP_INTERLOCK:
    dpSetWait(dpName + ".CryoInterlockT", gTempInterlockLimit);
    err = getLastError();
    if(dynlen(err) > 0)
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "Action Execution: execution failed:" + err , "");
      return;
    }
    return;	
    break;
  case LVA_DEVICE_SET_BLOCKED_OFF:
  case LVA_DEVICE_CLEAR_BLOCKED_OFF:
    ProcessBlockedOFF(dpName, dpType, act, exceptionInfo);
    return;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = false;
    mConnectDpes["AL2"] = false;
    mConnectDpes["AL3"] = false;
    mConnectDpes["TS1.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS1.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RR2"] = NO_VALUE_CHANGE;
    mConnectDpes["AL1"] = NO_VALUE_CHANGE;
    mConnectDpes["AL2"] = NO_VALUE_CHANGE;
    mConnectDpes["AL3"] = NO_VALUE_CHANGE;
    mConnectDpes["TS1.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS1.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.YMDH"] = NO_VALUE_CHANGE;
    mConnectDpes["TS2.MSM"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_VVS_LHC_EDIT_THRESHOLD_ON:
	dpSet(dpName + ".Actuations." + action[3], action[2]);
	return;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }

  // Check if action is possible
  LhcVacActionPossible_VV(dpName, dpType, act, possible, exceptionInfo);
  if(dynlen(exceptionInfo) != 0)
  {
     return;
  }
  if(!possible)
  {
    if(dpType == "VVS_PS_CMW")
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "Equipment of this type is read-only in PVSS. Control using WorkingSet", "");
    }
    else
    {
      fwException_raise(exceptionInfo, 
        "ERROR", "Action Execution: Action forbidden for <" + dpName + ">", "");
    }
    return;
  }	
  // Execute
  if(dpType == "VVS_PS_CMW")
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Equipment of this type is read-only in PVSS. Control using WorkingSet", "");
    return;
    dpSetWait(dpName + ".WR1:_original.._value", newWr1PS);    
  }
  else
  {
    dpSetWait(dpName + ".WR1:_original.._value", newWr1);
  }
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
}

//LhcVacDeviceAction_VV_PUL:
/** Execute action for valve. It has been checked before call that this DP type
	understands this action.

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VV_PUL(string dpName, string dpType, dyn_anytype action, dyn_string &exceptionInfo)
{
  unsigned	newWr1;
  bool		possible;
  dyn_errClass	err;
  mapping   mConnectDpes;
  unsigned      act = action[1] & LVA_ACTION_MASK;
  switch(act & LVA_ACTION_MASK)
  {
  case LVA_DEVICE_OPEN:
	newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_OPEN;
    break;
  case LVA_DEVICE_CLOSE:
    newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_CLOSE;
    break;
  case LVA_DEVICE_MANUAL:
     newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_MANUAL;
    break;
  case LVA_DEVICE_AUTO:
     newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_AUTO;
    break;
  case LVA_DEVICE_RESET:
      newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_RESET;
    break;
  case LVA_DEVICE_FORCE:
      newWr1 = SVCU_SPS_W_REMOTE | SVCU_SPS_W_FORCE;
    break;
  case LVA_DEVICE_SET_BLOCKED_OFF:
  case LVA_DEVICE_CLEAR_BLOCKED_OFF:
    ProcessBlockedOFF(dpName, dpType, act, exceptionInfo);
    return;
  case LVA_DEVICE_SET_NOT_CONNECTED:
    mConnectDpes["RR1"] = 0;
    mConnectDpes["RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["StartCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceNotConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  case LVA_DEVICE_SET_CONNECTED:
    mConnectDpes["RR1"] = NO_VALUE_CHANGE;
    mConnectDpes["RunTime"] = NO_VALUE_CHANGE;
    mConnectDpes["StartCounter"] = NO_VALUE_CHANGE;
    mConnectDpes["WR1"] = NO_VALUE_CHANGE;
    LhcVacSetDeviceConnected(dpName, mConnectDpes, exceptionInfo);
    return;
  default:
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: unknown action " + act, "");
    return;
  }

  // Check if action is possible
  LhcVacActionPossible_VV(dpName, dpType, act, possible, exceptionInfo);

  string attribute;
  if(dynlen(exceptionInfo) != 0)
  {
     return;
  }
  if(!possible)
  {
	LhcVacGetAttribute(dpName, "ReadOnly", attribute);
	if (attribute == "1")
	{
		fwException_raise(exceptionInfo, 
      "ERROR", "Equipment <" + dpName + "> has been defined in VacDB as read-only", "");
	}
	else
	{
	      fwException_raise(exceptionInfo, 
        "ERROR", "Action Execution: Action forbidden for <" + dpName + ">", "");
	}
	return;
  }
  // Execute
  LhcVacGetAttribute(dpName, "ReadOnly", attribute);
  if(attribute == "1")
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Equipment <" + dpName + "> has been defined in VacDB as read-only", "");
    return;
    
  }
  else
  {
    dpSetWait(dpName + ".WR1:_original.._value", newWr1);
  }
  err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, 
      "ERROR", "Action Execution: execution failed:" + err , "");
    return;
  }
}




















































// Set address(es) of recipients to be notified about problems in all sectors
// Every item in list shall be valid E-mail address
void VacValveIntlckSetRecipients(dyn_string recipients, dyn_string &exceptionInfo)
{
	string				configDp;
	dyn_errClass	err;

	dynClear(exceptionInfo);
	VacValveInterlockGetConfigDp(configDp, exceptionInfo);
	if(dynlen(exceptionInfo) > 0) return;
	dpSetWait(configDp + ".recipients", recipients);
	err = getLastError();
	if(dynlen(err) > 0)
	{
		fwException_raise(exceptionInfo, "ERROR", "VacValveIntlckSetSender(): dpSetWait() failed", "");
		throwError(err);
	}
}
// Check if DP with email config exists, if not - create such DP
void VacValveInterlockGetConfigDp(string &configDpName, dyn_string &exceptionInfo)
{
	configDpName = "";
	if(dpExists(VAC_VALVE_INTLCK_CONFIG_DP))
	{
		configDpName = VAC_VALVE_INTLCK_CONFIG_DP;
		return;
	}
	if(dpCreate(VAC_VALVE_INTLCK_CONFIG_DP, VAC_VALVE_INTLCK_CONFIG_DP_TYPE) < 0)
	{
		fwException_raise(exceptionInfo, "ERROR", "VacValveInterlockGetConfigDp(): dpCreate() failed", "");
		return;
	}
	if(! dpExists(VAC_VALVE_INTLCK_CONFIG_DP))
	{
		fwException_raise(exceptionInfo, "ERROR", "VacValveInterlockGetConfigDp(): config DP not created", "");
		return;
	}
	configDpName = VAC_VALVE_INTLCK_CONFIG_DP;
	dpSetWait(configDpName + ".server", "cernmx.cern.ch");
	dpSetWait(configDpName + ".client", "cern.ch");
	dpSetWait(configDpName + ".sender", "vacin@cern.ch");
}
void LhcVacValveIntrlckTimedDp(string &timedDpName, dyn_string &exceptionInfo)
{
	timedDpName = "";
	if(dpExists(VAC_VALVE_INTLCK_TIMED_DP))
	{
		timedDpName = VAC_VALVE_INTLCK_TIMED_DP;
		return;
	}
	if(dpCreate(VAC_VALVE_INTLCK_TIMED_DP, VAC_VALVE_INTLCK_TIMED_DP_TYPE) < 0)
	{
		fwException_raise(exceptionInfo, "lhcVacVV.ctl ERROR", "LhcVacValveIntrlckTimedDp(): dpCreate() failed", "");
		return;
	}
	if(! dpExists(VAC_VALVE_INTLCK_TIMED_DP))
	{
		fwException_raise(exceptionInfo, "lhcVacVV.ctl ERROR", "LhcVacValveIntrlckTimedDp(): config DP not created", "");
		return;
	}
	timedDpName = VAC_VALVE_INTLCK_TIMED_DP;
}

// Set name of SMTP server
void VacValveIntlckSetServer(string serverName, dyn_string &exceptionInfo)
{
	string				configDp;
	dyn_errClass	err;

	dynClear(exceptionInfo);
	VacValveInterlockGetConfigDp(configDp, exceptionInfo);
	if(dynlen(exceptionInfo) > 0) return;
	dpSetWait(configDp + ".server", serverName);
	err = getLastError();
	if(dynlen(err) > 0)
	{
		fwException_raise(exceptionInfo, "ERROR", "VacValveIntlckSetServer(): dpSetWait() failed", "");
	}
}
// Set name of client computer ('domain' for emSendMail())
void VacValveIntlckSetClient(string clientName, dyn_string &exceptionInfo)
{
	string				configDp;
	dyn_errClass	err;

	dynClear(exceptionInfo);
	VacValveInterlockGetConfigDp(configDp, exceptionInfo);
	if(dynlen(exceptionInfo) > 0) return;
	dpSetWait(configDp + ".client", clientName);
	err = getLastError();
	if(dynlen(err) > 0)
	{
		fwException_raise(exceptionInfo, "ERROR", "VacValveIntlckSetClient(): dpSetWait() failed", "");
	}
}
// Set address of mail sender. It looks like sender shall be recognized
// by SMTP server
void VacValveIntlckSetSender(string senderName, dyn_string &exceptionInfo)
{
	string				configDp;
	dyn_errClass	err;

	dynClear(exceptionInfo);
	VacValveInterlockGetConfigDp(configDp, exceptionInfo);
	if(dynlen(exceptionInfo) > 0) return;
	dpSetWait(configDp + ".sender", senderName);
	err = getLastError();
	if(dynlen(err) > 0)
	{
		fwException_raise(exceptionInfo, "ERROR", "VacValveIntlckSetSender(): dpSetWait() failed", "");
		throwError(err);
	}
}
//Check Interlock bits for all LHC valve and return dyn arrays with name and interlock bits
void CheckLhcValveInterlock(dyn_string	&interlockDpName, dyn_int &interlock)
{
// Check all DPs of type VVS_LHC
	int				i;
	unsigned	rr2, itlck;
	string		dpe, displName;

	dyn_errClass 	err;
	dyn_string exceptionInfo, thermoList;
	dyn_string allLHCValves = dpNames("*", "VVS_LHC");
//	DebugN(allLHCValves);
	for(i=1; i <= dynlen(allLHCValves); i++)
	{
		dpe = allLHCValves[i] + ".RR2:_online.._value";
		dpGet(dpe, rr2);
		itlck = 0;
		if((rr2 & SVCU_SPS_R_CIR_VVS_AFTER) ||
				(rr2 & SVCU_SPS_R_CIR_VVS_BEFORE) ||
				(rr2 & SVCU_SPS_R_CIR_LCL_PRESS))
		{
			dynAppend(interlockDpName, dpSubStr(dpe, DPSUB_DP));
			if (rr2 & SVCU_SPS_R_CIR_VVS_AFTER)
				itlck += 0x08u; 
			if (rr2 & SVCU_SPS_R_CIR_VVS_BEFORE)
				itlck += 0x04u; 
			if (rr2 & SVCU_SPS_R_CIR_LCL_TEMP)
				itlck += 0x02u; 
			if (rr2 & SVCU_SPS_R_CIR_LCL_PRESS)
				itlck += 0x01u; 
			dynAppend(interlock, itlck);
		}
		dynClear(exceptionInfo);
		string	dpName = dpSubStr(allLHCValves[i], DPSUB_DP);

		LhcVacGetThermometersForValve(dpName, thermoList, exceptionInfo);
		if(dynlen(exceptionInfo) > 0)
		{
			continue;
		}
		if(dynlen(thermoList) == 0)
		{
			continue;
		}
		string thermoDp = thermoList[1];	// Only one thermometer is expected
		// Get limit for temperature
		int limit;
		dpGet(allLHCValves[i] + ".CryoInterlockT", limit);
		err = getLastError();
		if(dynlen(err) > 0)
		{
			fwException_raise(exceptionInfo, 
				"ERROR", "LhcVacGetRequiredOpenPrivilege_VV: Read CryoInterlockT for <" + displName + "> failed: " + err, "");
			return;
		}
		if(limit <= 0)
		{
			limit = 5;	// Use reasonable default
		}

		// Get measured temperature
		int		rr1;
		float	t;
		dpGet(thermoDp + ".RR1", rr1, thermoDp + ".T", t);
		err = getLastError();
		if(dynlen(err) > 0)
		{
			fwException_raise(exceptionInfo, 
				"ERROR", "LhcVacGetRequiredOpenPrivilege_VV: Reading RR1 and T for <" + thermoDp + "> failed: " + err, "");
			return;
		}
		if((rr1 & 0x1) == 0)	// No error
		{
			if(t > limit)
			{
				int indx = dynContains(interlockDpName, dpSubStr(dpe, DPSUB_DP));
				if(indx > 0)
				{
					interlock[indx] += 0x10u; 
				}
				else
				{
					dynAppend(interlockDpName, dpSubStr(dpe, DPSUB_DP));
					dynAppend(interlock, 0x10u);
				}
			}
		}
	}
}
string PrepareBody(dyn_string	ilDp, dyn_int ilValue)
{
	int					n, coco, sendFailureCounter = 0;
	string 			body;
	dyn_string	exceptionInfo;
	dyn_float	posList;
	float	pos;
		for(n = 1; n <= dynlen(ilDp); n++)
		{
			dynClear(exceptionInfo);
			LhcVacDeviceLocation(ilDp[n], pos, exceptionInfo);
			if(dynlen(exceptionInfo) > 0)
			{
				DebugTN("lhcVacOscEqp.ctl: LhcVacDeviceLocation():", exceptionInfo);
				pos = 0;
			}
			dynAppend(posList, pos);
		}		
		while(dynlen(posList) > 0)
		{
			string	displName;
			float minPos = dynMin(posList);
			int posIndx = dynContains(posList, minPos); 
			dynClear(exceptionInfo);
			LhcVacDisplayName(dpSubStr(ilDp[posIndx], DPSUB_DP), displName, exceptionInfo);
			if(dynlen(exceptionInfo) > 0)
			{
				DebugTN("lhcVacVV.ctl: ERROR: LhcVacDisplayName():" + ilDp[posIndx] + ">>> failed:",
					exceptionInfo);
				continue;
			}
			body += displName + ": ";
			if(ilValue[posIndx] &  0x01u)
				body += "| LCL_PRESS";
			if(ilValue[posIndx] &  0x02u)
				body += "| LCL_TEMP";
			if(ilValue[posIndx] &  0x04u)
				body += "| VVS_BEFORE";
			if(ilValue[posIndx] &  0x08u)
				body += "| VVS_AFTER";				
			if(ilValue[posIndx] &  0x10u)
				body += "| VVS_TEMP_SOFTWARE";				
			body += "\n";
			dynRemove(ilDp, posIndx);
			dynRemove(posList, posIndx);
			dynRemove(ilValue, posIndx);
		}
		return body;
}




