//	Implementation of FunctionalTypeVPC_HCCC class
// To deal with sms notification
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVPC_HCCC.h"


#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

#include "DataPool.h"
#include "Eqp.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeVPC_HCCC::FunctionalTypeVPC_HCCC(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVPC_HCCC::fillCriteria(FunctionalType *pType)
{
	// Own criteria
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, OnNominal, "Cryo pump is On and Nominal");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is ON NOMINAL");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is NOT ON NOMINAL");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Cryopump in sector {sector} is ON NOMINAL");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Cryopump in sector {sector} is NOT ON NOMINAL");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Cryopump in main part {mainPart} is ON NOMINAL");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Cryopump in main part {mainPart} is NOT ON NOMINAL");

	/*
	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, VvgFullOpen, "VVG is full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} VVG is full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} iVVG is NOT full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"Gas injection in sector {sector} VVG is full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"Gas injection in sector {sector} VVG is NOT full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"Gas injection in main part {mainPart} VVG is full OPEN");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"Gas injection in main part {mainPart} VVG is NOT full OPEN");
	*/
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeVPC_HCCC::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case OnNominal:
			result = "OnNominal";
			break;
		//case VvgFullOpen:
		//	result = "VvgFullOpen";
		//	break;
		}
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeVPC_HCCC::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "OnNominal"))
		{
			result = OnNominal;
		}
		//else if(!strcasecmp(name, "VvgFullOpen"))
		//{
		//	result = VvgFullOpen;
		//}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}

