//	Implementation of DpeHistoryQuery class
/////////////////////////////////////////////////////////////////////////////////

#include "DpeHistoryQuery.h"

DpeHistoryQuery	DpeHistoryQuery::instance;

DpeHistoryQuery &DpeHistoryQuery::getInstance(void)
{
	return instance;
}
int DpeHistoryQuery::getPendingCount(void)
{
	return instance.pItems->count();
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction
DpeHistoryQuery::DpeHistoryQuery()
{
	pItems = new QList<DpeHistoryQueryItem *>();
	signalSent = false;
}

DpeHistoryQuery::DpeHistoryQuery(const DpeHistoryQuery &source) : QObject()
{
	pItems = new QList<DpeHistoryQueryItem *>(*(source.pItems));
	signalSent = false;
}

DpeHistoryQuery::~DpeHistoryQuery()
{
	while(!pItems->isEmpty())
	{
		delete pItems->takeFirst();
	}
	delete pItems;
}

DpeHistoryQuery & DpeHistoryQuery::operator = (const DpeHistoryQuery &other)
{
	*pItems = *(other.pItems);
	signalSent = false;
	return *this;
}

/*
**	FUNCTION
**		Add DPE query to list of pending queries.
**
**	ARGUMENTS
**		id		- Internal ID of query
**		dpe		- Full DPE name (DP + DPE)
**		bitState	- Falg indicating if this is a request for bit state history
**		bitNbr	- Bit number for bit state
**		start	- Start of required time range
**		end		- End of required time range
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeHistoryQuery::add(int id, const char *dpe, bool bitState, int bitNbr, const QDateTime &start, const QDateTime &end)
{
	mutex.lock();
	pItems->append(new DpeHistoryQueryItem(id, dpe, bitState, bitNbr, start, end));
	mutex.unlock();
	if(!signalSent)
	{
		signalSent = true;
		emit changed();
	}
}

/*
**	FUNCTION
**		Remove DPE query from list of pending queries.
**
**	ARGUMENTS
**		id		- Internal ID of query
**		dpe		- Full DPE name (DP + DPE)
**		bitState	- Falg indicating if this is a request for bit state history
**		bitNbr	- Bit number for bit state
**		start	- Start of required time range
**		end		- End of required time range
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeHistoryQuery::remove(int id, const char *dpe, bool bitState, int bitNbr, const QDateTime &start, const QDateTime &end)
{
	mutex.lock();
	for(int idx = 0 ; idx < pItems->count() ; idx++)
	{
		DpeHistoryQueryItem *pItem = pItems->at(idx);
		if((pItem->getId() == id) && (pItem->getDpe() == dpe) &&
			(pItem->isBitState() == bitState) && (pItem->getBitNbr() == bitNbr) &&
			(pItem->getStart() == start) && (pItem->getEnd() == end))
		{
			pItems->removeOne(pItem);
			break;
		}
	}
	mutex.unlock();
}

/*
**	FUNCTION
**		Clear pool of pending requests, normally method is called when
**		history dialog is closed
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeHistoryQuery::clear(void)
{
	mutex.lock();
	pItems->clear();
	signalSent = false;
	mutex.unlock();
}

/*
**	FUNCTION
**		Get parameters of next query pending in the queue, remove that
**		query from the queue
**
**	ARGUMENTS
**		id		- Variable where ID of pending request will be written
**		dpe		- Variable where DPE name of pending request will be written
**		start	- Variable where start time of pending request will be written
**		end		- Variable where end time of pending request will be written
**
**	RETURNS
**		true	- if next pending request has been found;
**		false	- if queue is empty
**
**	CAUTIONS
**		None
*/
bool DpeHistoryQuery::getNext(int &id, QByteArray &dpe, bool &bitState, int &bitNbr, QDateTime &start, QDateTime &end)
{
	mutex.lock();
	bool result;
	if(pItems->isEmpty())
	{
		result = false;
		signalSent = false;
	}
	else
	{
		result = true;
		DpeHistoryQueryItem *pItem = pItems->takeFirst();
		id = pItem->getId();
		dpe = pItem->getDpe();
		bitState = pItem->isBitState();
		bitNbr = pItem->getBitNbr();
		start = pItem->getStart();
		end = pItem->getEnd();
		delete pItem;
	}
	mutex.unlock();
	return result;
}

