#ifndef FUNCTIONALTYPEPROCESS_H
#define FUNCTIONALTYPEPROCESS_H

// Special functional type for processes - supreclass for all processes classes

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypePROCESS : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		On = 1,
		Off = 2
	};

	// Main value criteria subtypes
	enum
	{
		Unknown = 0,
		SequencerStep = 1
	};

	FunctionalTypePROCESS(int type, const char *name, const char *description);
	virtual ~FunctionalTypePROCESS() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);
	virtual void getCriteriaValues(int critType, int critSubType, QList<int> &values, QList<QByteArray> &labels);

protected:
	virtual QString getStepResourceBase(void) = 0;
};

#endif	// FUNCTIONALTYPEPROCESS_H
