#include "RackEqp.h"

#include "qstring.h"

RackEqp::RackEqp()
{
	positionX = -1;
	positionY = -1;
	sizeX = -1;
	sizeY = -1;
	name = "";
	pEqp = NULL;
}

RackEqp::~RackEqp()
{

}

RackEqp::RackEqp(QString nameEquip, int positionX, int positionY, float sizeX, float sizeY, QString eqpType)
{
	name = nameEquip;
	type = eqpType;
	this->positionX = positionX;
	this->positionY = positionY;
	this->sizeX = sizeX;
	this->sizeY = sizeY;
	
}

/*
**
**	FUNCTION
**		Verify if required minimum attributes for RackEqp to be
**		added to a rack and processed are set
**
**	PARAMETERS
**		none
**
**	RETURNS
**		true /false
**
**	CAUTIONS
**		None
*/
bool RackEqp::isMinimumInformationSet()
{
	

	if (positionX > 0 && positionY > 0 && sizeX > 0 && sizeY > 0 && name != "")
	{
		return true;
	}
	else
		return false;
}

void RackEqp::addAttribute(QString attributeName, QString attributeValue)
{
	attributes.insert(attributeName, attributeValue);
}

QMap<QString, QString> & RackEqp::getAttributeList()
{
	return attributes;
}