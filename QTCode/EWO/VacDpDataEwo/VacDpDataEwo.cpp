//	Implementation of VacDpDataEwo class
/////////////////////////////////////////////////////////////////////////////////

#include "VacDpDataEwo.h"

EWO_PLUGIN(VacDpDataEwo)


VacDpDataEwo::VacDpDataEwo(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new VacDpData(parent);
	connect(baseWidget, SIGNAL(newDpeValue(const char *, int, const QVariant &)),
		this, SLOT(newDpeValue(const char *, int, const QVariant &)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList VacDpDataEwo::signalList(void) const
{
	QStringList list;
	list.append("NewDpeValue(string dpe, int source, anytype value)");
	return list;
}

/*
**	FUNCTION
**		Slot receiving newDpeValue signal of VacDpData, emit
**		signal 'NewDpeValue' to PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		source	- Enumerated source type
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacDpDataEwo::newDpeValue(const char *dpe, int source, const QVariant &value)
{
	QList<QVariant> args;
	args.append(QVariant(dpe));
	args.append(QVariant(source));
	int valueType = value.type();
	if(valueType == QMetaType::Float)
	{
		// Float is treated differently since Qt 4.6 - see
		// https://icecontrols.its.cern.ch/jira/browse/ENS-9991?page=com.atlassian.jira.plugin.system.issuetabpanels:commenttabpanel&focusedCommentId=91889#comment-91889
		double dValue = value.toDouble();
		args.append(QVariant(dValue));
	}
	else
	{
		args.append(QVariant(value));
	}
	emit signal("NewDpeValue", args);
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList VacDpDataEwo::methodList(void) const
{
	QStringList list;
	list.append("int connectDpe(string dpe)");
	list.append("anytype getDpeValue(string dpe)");
	list.append("bool isPlcAlarm()");
	list.append("int modeAsInt()");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant VacDpDataEwo::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "connectDpe")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::ByteArray))
		{
			error = "argument 1 (dpe) is not string";
			return QVariant();
		}
		QByteArray dpe = values[0].toByteArray();
		int coco = baseWidget->connectDpe(dpe);
		return QVariant(coco);
	}
	else if(name == "getDpeValue")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::ByteArray))
		{
			error = "argument 1 (dpe) is not string";
			return QVariant();
		}
		QByteArray dpe = values[0].toByteArray();
		const QVariant &value = baseWidget->getDpeValue((const char *)dpe);
		int valueType = value.type();
		if(valueType == QMetaType::Float)
		{
			// Float is treated differently since Qt 4.6 - see
			// https://icecontrols.its.cern.ch/jira/browse/ENS-9991?page=com.atlassian.jira.plugin.system.issuetabpanels:commenttabpanel&focusedCommentId=91889#comment-91889
			double dValue = value.toDouble();
			return QVariant(dValue);
		}
		return QVariant(value);
	}
	else if(name == "isPlcAlarm")
	{
		bool coco = baseWidget->isPlcAlarm();
		return QVariant(coco);
	}
	else if(name == "modeAsInt")
	{
		return QVariant((int)baseWidget->getMode());
	}
	else
	{
		error = "Uknown method VacDpDataEwo.";
		error += name;
	}
	return QVariant();
}

