#ifndef EQPVPGMPR_H
#define	EQPVPGMPR_H

//	VPGMPR - gauge of mobile pumping group

#include "EqpVG.h"

class VACCTLEQPDATA_EXPORT EqpVPGMPR : public EqpVG
{
	Q_OBJECT

public:
	EqpVPGMPR(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVPGMPR(const EqpVPGMPR &src) : EqpVG(src) {}
#endif
	~EqpVPGMPR();

	virtual void setActive(DataEnum::DataMode mode, bool active);

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual int checkDevListCrit(void);
#endif

protected:
};

#endif	// EQPVPGMPR_H
