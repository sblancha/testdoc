#ifndef _VACCTLPVSSUTIL_H_
#define _VACCTLPVSSUTIL_H_

//	Class implementing some auxilliary methods for other classes,
//	which will be derived from BaseExternHdl. It is expected that
//	other classes for PVSS CTRL DLL will be drived from this class


#include <BaseExternHdl.hxx>

#include <QStringList>

#include "VacCtlPvssUtilExport.h"

//	Structure holding parameters for parsing arguments of CTRL script
typedef struct
{
	const char			*argName;
	enum VariableType	varType;
	Variable			*pInVar;		// Not NULL if argument is in argument
	Variable			**ppOutVar;		// Not NULL if argument is out argument
} SPvssArg, *SPvssArgPtr;


#include <AnyTypeVar.hxx>
#include <QVariant>

class VacCtlPvssUtil : public BaseExternHdl
{
protected:
	VacCtlPvssUtil(BaseExternHdl *nextHdl, PVSSulong funcCount, FunctionListRec fnList[])
      : BaseExternHdl(nextHdl, funcCount, fnList) {}
	~VacCtlPvssUtil() {}
	
	virtual bool parseArgs(ExecuteParamRec &param, SPvssArgPtr args, int nArgs);
	virtual CtrlExpr *getArg(ExecuteParamRec &param, const char *argName, bool isFirstArg);
	Variable *getOutArg(ExecuteParamRec &param, const char *argName, enum VariableType varType,
		bool isFirstArg);
	bool getInOutArg(ExecuteParamRec &param, const char *argName, enum VariableType varType,
		bool isFirstArg, CtrlExpr **pExpr, Variable **pVariable);

	virtual bool splitDpName(const char *dp, char **ppDpName, char **ppDpeName);
	void showError(ExecuteParamRec &param, ErrClass::ErrCode errCls, const QString &errMsg);
	void addExceptionInfo(ExecuteParamRec &param, Variable *result, QStringList &errList);
	void addExceptionInfo(ExecuteParamRec &param, Variable *result, const QString &errMsg);

	void addToDynAnyVar(DynVar *pVar, const char *value);
	void addToDynAnyVar(DynVar *pVar, int value);
	void addToDynAnyVar(DynVar *pVar, bool value);
	void addToDynAnyVar(DynVar *pVar, float value);

	void addToDynStringVar(DynVar *pVar, const char *value, bool checkUnique = false);
	void addToDynIntVar(DynVar *pVar, int value, bool checkUnique = false);
	void addToDynBoolVar(DynVar *pVar, bool value, bool checkUnique = false);
	void addToDynFloatVar(DynVar *pVar, float value, bool checkUnique = false);

	QVariant *anyTypeToVariant(ExecuteParamRec &param, AnyTypeVar &value);
	void variantToAnyType(ExecuteParamRec &param, QVariant value, AnyTypeVar *pVar);
};

#endif	// _VACAUXPVSS_H_
