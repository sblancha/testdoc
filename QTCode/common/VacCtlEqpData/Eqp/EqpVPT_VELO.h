#ifndef	EQPVPT_VELO_H
#define	EQPVPT_VELO_H

//	VPT_VELO - turbo pump in VELO detector

#include "Eqp.h"

class VACCTLEQPDATA_EXPORT EqpVPT_VELO : public Eqp
{
	Q_OBJECT

public:
	EqpVPT_VELO(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVPT_VELO(const EqpVPT_VELO &src) : Eqp(src) {}
#endif
	~EqpVPT_VELO();

	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	// Access for essential values
	inline bool isPlcAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	inline float getSpeed(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? speedReplay : speedOnline; }
	
public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE for online mode
	unsigned	rr1Online;

	// Last known value of RR1 DPE for replay mode
	unsigned	rr1Replay;

	// Last known value of SPEED DPE for online mode
	float		speedOnline;

	// Last known value of SPEED DPE for replay mode
	float		speedReplay;

	// Last knwon PLC alarm state (true == alarm) for online mode
	bool		plcAlarmOnline;

	// Last knwon PLC alarm state (true == alarm) for replay mode
	bool		plcAlarmReplay;

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, float speed, bool plcAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, float speed, bool plcAlarm, QString &string);
#endif
};

#endif	// EQPVPT_VELO_H
