#ifndef	VACICONVGM_H
#define	VACICONVGM_H

//	Icon for VGM

#include "VacIconVG.h"

class VACCTLEWOUTIL_EXPORT VacIconVGM : public VacIconVG
{
public:
	VacIconVGM(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGM_H
