#ifndef LHCCONNECTLINE_H
#define	LHCCONNECTLINE_H

// Geometry of one injection/dump line on main LHC view

#include "Sector.h"
#include "InterfaceEqp.h"
#include "EqpVACOK.h"

#include "LegendLabel.h"

#include <stdio.h>

class MainLHCImage;

class BeamLine;

class LhcConnectLine : public InterfaceEqp
{
	Q_OBJECT

public:
	LhcConnectLine(BeamLine *pLine, int vacType);
	virtual ~LhcConnectLine() {};

	void connectEqp(MainLHCImage *pImage);

	const char *sectorAtLocation(int x, int y, int connLineWidth);

	void dump(FILE *pFile);

	// Access
	inline BeamLine *getLine(void) { return pLine; }
	inline float getCoeffX(void) { return coeffX; }
	inline void setCoeffX(float coeffX) { this->coeffX = coeffX; }
	inline float getCoeffY(void) { return coeffY; }
	inline void setCoeffY(float coeffY) { this->coeffY = coeffY; }
	inline short getStartX(void) { return startX; }
	inline void setStartX(short startX) { this->startX = startX; }
	inline short getStartY(void) { return startY; }
	inline void setStartY(short startY) { this->startY = startY; } 
	inline short getEndX(void) { return endX; }
	inline void setEndX(short endX) { this->endX = endX; }
	inline short getEndY(void) { return endY; }
	inline void setEndY(short endY) { this->endY = endY; } 
	inline float getStartPos(void) { return startPos; }
	inline void setStartPos(float startPos) { this->startPos = startPos; }
	inline int getVacType(void) { return vacType; }

public slots:
	// Override slot of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

signals:
	// Signal is emitted when state of one of devices has been changed
	void stateChanged(void);

protected:
	// Pointer to beam line data for this line
	BeamLine			*pLine;

	// Position of the very first device in line
	float				startPos;

	// Coefficient for converting eqp postion to X coordinate on screen
	float				coeffX;

	// Coefficient for converting eqp postion to Y coordinate on screen
	float				coeffY;

	// Vacuum type of parent line in connection point (see VacType.h)
	int					vacType;

	// X coordinate of line start on view
	short				startX;

	// Y coordinate of line start on view
	short				startY;

	// X coordinate of line end on view
	short				endX;

	// Y coordinate of line end on view
	short				endY;

	// Flag indicating if this line is already connected to equipment
	bool				connected;
};

#endif	// LHCCONNECTLINE_H
