//	Implementation of DevListCriteria class
/////////////////////////////////////////////////////////////////////////////////

#include "DevListCriteria.h"

#include "DataEnum.h"

VACCTLUTIL_EXPORT DevListCriteria DevListCriteria::instance;

DevListCriteria &DevListCriteria::getInstance(void)
{
	return instance;
}

DevListCriteria::DevListCriteria() : time(QDateTime::currentDateTime())
{
	// Initialize settings
	normal = false;
	notNormal = false;
	abnormal = true;
	notAbnormal = false;
	errors = true;
	warnings = false;
	alerts = false;
	access = false;
	notConnected = false;
	notActive = false;

	usePressureLimit = false;
	pressureLimit = (float)1.E-6;
	usePressureGrowLimit = false;
	pressureGrowLimit = 2.0;
	pressureEndTimeNow = true;
	pressureEndTime = QDateTime::currentDateTime();
	pressureStartTime = pressureEndTime.addDays(-1);
	pressureSpikeInterval = 5;

	useTemperatureLimit = false;
	temperatureLimit = 5.0;
	useTemperatureGrowLimit = false;
	temperatureGrowLimit = 2.0;
	temperatureEndTimeNow = true;
	temperatureEndTime = pressureEndTime;
	temperatureStartTime = pressureStartTime;
	temperatureSpikeInterval = 5;

	mode = DataEnum::Online;
}

bool DevListCriteria::isEmpty(void) const
{
	return !
		(normal || notNormal || abnormal || notAbnormal || errors || warnings ||
		alerts || access || notConnected || notActive ||
		usePressureLimit ||
		(usePressureLimit && (pressureLimit > 0.0)) ||
		(usePressureGrowLimit && (pressureGrowLimit > 0.0) && (pressureStartTime < pressureEndTime)) ||
		(useTemperatureLimit && (temperatureLimit > 0.0)) ||
		(useTemperatureGrowLimit && (temperatureGrowLimit > 0.0) && (temperatureStartTime < temperatureEndTime)));
}


bool DevListCriteria::hasStateCriteria(void) const
{
	return normal || notNormal || abnormal || notAbnormal || errors || warnings ||
		alerts || access || notConnected || notActive;
}

bool DevListCriteria::generalStateMatches(int state)
{
	switch(state)
	{
	case EqpStateNormal:
		if(normal)
		{
			return true;
		}
		if(notAbnormal && (!notNormal))
		{
			return true;
		}
		break;
	case EqpStateAbnormal:
		if(abnormal)
		{
			return true;
		}
		if(notNormal && (!notAbnormal))
		{
			return true;
		}
		break;
	default:
		if(notNormal || notAbnormal)
		{
			return true;
		}
	}
	return false;
}
