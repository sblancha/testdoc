#ifndef	VACICONVGI_STD_H
#define	VACICONVGI_STD_H

//	Icon for VG_STD - VGI

#include "VacIconVG_STD.h"

class VACCTLEWOUTIL_EXPORT VacIconVGI_STD : public VacIconVG_STD
{
public:
	VacIconVGI_STD(QWidget *parent, Qt::WindowFlags f = 0);

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGI_STD_H
