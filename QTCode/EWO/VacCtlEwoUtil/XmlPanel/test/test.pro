# Project file for XML parser test
#
#	17.05.2012	L.Kopylov
#		Initial version
#

TEMPLATE = app

TARGET = test

INCLUDEPATH += 	../../../../common/VacCtlUtil \
	../../../../common/Platform \
	../../../../common/VacCtlEqpData \
	../../../../common/VacCtlEqpData/FunctionalType \
	../../XmlPanel \
	../../VacIcon \
	../ \
	../..


LIBS += -L../../../../../bin -lVacCtlEwoUtil -lVacCtlEqpData

unix:CONFIG += qt thread release warn_on
windows:CONFIG += qt thread release warn_on

#DEFINES += _VACDEBUG
DEFINES += _CRT_SECURE_NO_DEPRECATE XML_PANEL_PARSE


HEADERS = XmlSynViewer.h \
	PanelViewer.h

SOURCES = XmlSynViewer.cpp \
	PanelViewer.cpp \
	main.cpp
