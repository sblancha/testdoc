//	Implementation of VacIconCRYO_TT class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconCRYO_TT.h"

#include "EqpCRYO_TT.h"

#include "FunctionalType.h"
#include "VacEqpTypeMask.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			153,153,255

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconCRYO_TT::VacIconCRYO_TT(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	QRect labelRect = fontMetrics().boundingRect("99.9");
	setFixedSize(labelRect.width() + 2, labelRect.height() + 2);
}

VacIconCRYO_TT::~VacIconCRYO_TT()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		It is supposed that this method will not be used for CRYO_TT
*/
VacIconGeometry VacIconCRYO_TT::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(width(), height(), width() >> 1, height() >> 1, 1);
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask		- Vacuum equipment mask
**		forBeamVac	- Flag indicating if icon will be drawn on beam vacuum
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconCRYO_TT::getGeometry(const VacEqpTypeMask &mask, bool forBeamVac)
{
	VacIconGeometry result(width(), height(), width() >> 1, height() >> 1, 1);
	if(!pEqp)
	{
		return result;
	}
	bool showExtra = mask.contains(FunctionalType::CRYO_TT_EXT);
	if(forBeamVac)
	{
		switch(pEqp->getCtrlSubType())
		{
		case EqpCRYO_TT::CmMin:	// cold mass average
			if(showExtra)
			{
				result.setY(2 + result.getHeight() * 3);
			}
			else
			{
				result.setY(result.getHeight());
			}
			break;
		case EqpCRYO_TT::CmAvg:	// cold mass min
			result.setY(1 + result.getHeight() * 2);
			break;
		case EqpCRYO_TT::CmMax:	// cold mass max
			result.setY(result.getHeight());
			break;
		}
	}
	else
	{
		switch(pEqp->getCtrlSubType())
		{
		case EqpCRYO_TT::QrlLineB:
			result.setY(31 + result.getHeight());
			break;
		case EqpCRYO_TT::QrlLineC:
			result.setY(33 + result.getHeight() * 2);
			break;
		case EqpCRYO_TT::CmMin:
			if(showExtra)
			{
				result.setY(31 + result.getHeight() * 3);
			}
			else
			{
				result.setY(31 + result.getHeight());
			}
			break;
		case EqpCRYO_TT::CmAvg:
			result.setY(30 + result.getHeight() * 2);
			break;
		case EqpCRYO_TT::CmMax:
			result.setY(29 + result.getHeight());
			break;
		}
	}
	return result;
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconCRYO_TT::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpCRYO_TT"), "VacIconCRYO_TT::setEqp", pEqp->getDpName());
		this->pEqp = (EqpCRYO_TT *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconCRYO_TT::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconCRYO_TT::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("T");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconCRYO_TT::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconCRYO_TT::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconCRYO_TT::paintEvent(QPaintEvent * /* pEvent */)
{
	QString	label = "???";
	QColor color(COLOR_UNDEFINED);
	if(pEqp)
	{
		switch(pEqp->getCtlStatus(mode))
		{
		case Eqp::NotControl:
		case Eqp::NotConnected:
			label = "NC";
			break;
		default:
			pEqp->getMainStateString(label, mode);
			break;
		}
		pEqp->getMainColor(color, mode);
	}
	QPainter painter(this);
	QRect myRect = rect();
	painter.fillRect(myRect, color);
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
	}
	QRect labelRect = painter.fontMetrics().boundingRect(label);
	painter.drawText(myRect.center().x() - (labelRect.width() >> 1),
		myRect.center().y() + (labelRect.height() >> 1),
		label);
}

