#ifndef LHCMAINVIEWR_H
#define LHCMAINVIEWR_H

// Test application for LHC main view EWO

#include "MainLHC.h"

#include <qmainwindow.h>
#include <qpushbutton.h>

class LhcMainViewer : public QMainWindow
{
	Q_OBJECT

public:
	LhcMainViewer(QWidget *parent = NULL, const char *name = NULL, WFlags = WType_TopLevel);
	~LhcMainViewer();

protected slots:
	void readData(void);

protected:
	MainLHC	*pView;

};

#endif	// LHCMAINVIEWR_H
