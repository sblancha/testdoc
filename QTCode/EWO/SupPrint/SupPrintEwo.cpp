//	Implementation of SupPrintEwo class
/////////////////////////////////////////////////////////////////////////////////

#include "SupPrintEwo.h"

EWO_PLUGIN(SupPrintEwo)


SupPrintEwo::SupPrintEwo(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new SupPrint(parent);

	// Connect to signals of base widget
	connect(baseWidget, SIGNAL(printRequest(void)),
		this, SLOT(printRequest(void)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList SupPrintEwo::signalList(void) const
{
	QStringList list;
	list.append("buttonClick()");
	return list;
}
/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList SupPrintEwo::methodList(void) const
{
	QStringList list;
	list.append("int printText(string textToPrint)");
	list.append("int printFile(string fileName)");
	list.append("int printDialog(void)");
	list.append("QColor convertColor(int rv, int gv, int bv)");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant SupPrintEwo::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "printText")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::String))
		{
			error = "argument 1 (textToPrint) is not string";
			return QVariant();
		}
		QString textToPrint = values[0].toString();
		int coco = baseWidget->printText((const char *)textToPrint.toLatin1());
		return QVariant(coco);
	}
	else if(name == "printFile")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::String))
		{
			error = "argument 1 (fileName) is not string";
			return QVariant();
		}
		QString fileName = values[0].toString();
		int coco = baseWidget->printFile((const char *)fileName.toLatin1());
		return QVariant(coco);
	}
	else if(name == "printDialog")
	{
		int coco = baseWidget->printDialog();
		return QVariant(coco);
	}
	else
	{
		error = "Uknown method SupPrintEwo.";
		error += name;
	}
	return QVariant();
}


/*
**	FUNCTION
**		Slot receiving printRequest signal of SupPrintEwo, emit
**		signal 'printRequest' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SupPrintEwo::printRequest(void)
{
printf("SupPrintEwo::printRequest(): start\n");
	emit signal("buttonClick");
}

