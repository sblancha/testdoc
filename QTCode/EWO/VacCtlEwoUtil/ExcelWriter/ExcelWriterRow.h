#ifndef EXCELWRITERROW_H
#define EXCELWRITERROW_H

// Class holding information on all cells in one Excel sheet row

#include "ExcelWriterCell.h"

#include <qlist.h>

class ExcelWriterRow
{
public:
	ExcelWriterRow(int row);
	~ExcelWriterRow();

	QString write(FILE *pFile);
	QString writeCsv(FILE *pFile);

	void setCellFormat(int column, ExcelWriterCell::Format format);
	void setCellColor(int column, const QColor &color);
	void setCellValue(int column, const QString &string);
	void setCellValue(int column, const QDateTime &dateTime, bool withMsec);
	void setCellValue(int column, float value);
	void setCellValue(int column, int value);
	void setCellComment(int column, const QString &comment);

	void getCellFormats(QList<ExcelCellFormat *> &formats);
	void setCellStyleIndex(const ExcelCellFormat &format, int index);

	// Access
	inline int getRow(void) const { return row; }

protected:
	// Row number in UNLIMITED Excel sheet, used to identify this row
	int		row;

	// List of all cells in this row
	QList<ExcelWriterCell *>	cells;

	ExcelWriterCell *findCell(int column);
};

#endif	// EXCELWRITERROW_H
