//	Implementation of EqpVCLDX class
/////////////////////////////////////////////////////////////////////////////////


#include "EqpVCLDX.h"

#include "DataPool.h"
#include "FunctionalType.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_IN					0,255,0
#define COLOR_OUT					255,0,0
#define COLOR_ERROR				255,102,255

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define	RR1_IN			(0x00020000)
#define	RR1_OUT			(0x00010000)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVCLDX::EqpVCLDX(const Eqp &source) : Eqp(source)
{
	rr1Online = rr1Replay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::COLDEX;
}

EqpVCLDX::~EqpVCLDX()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVCLDX::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	addDpeNameToList(stateDpes, "RR1");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVCLDX::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "RR1", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVCLDX::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "EqpStatus", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVCLDX::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVCLDX::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVCLDX::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVCLDX::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVCLDX::getMainColor(unsigned rr1, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}

	if((rr1 & RR1_IN) && (rr1 & RR1_OUT))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_IN)
	{
		color.setRgb(COLOR_IN);
	}
	else if(rr1 & RR1_OUT)
	{
		color.setRgb(COLOR_OUT);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVCLDX::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVCLDX::getMainStateString(unsigned rr1, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
		return;
	}

	if((rr1 & RR1_IN) && (rr1 & RR1_OUT))
	{
		string = "Error";
	}
	else if(rr1 & RR1_IN)
	{
		string = "IN";
	}
	else if(rr1 & RR1_OUT)
	{
		string = "OUT";
	}
	else
	{
		string = "Undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state of COLDEX: IN/OUT/both/none
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**		isIn	- Varibale that will be set to true if COLDEX is IN
**		isOut	- Varibale that will be set to true if COLDEX is OUT
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVCLDX::getColdexState(DataEnum::DataMode mode, bool &isIn, bool &isOut)
{
	if(mode == DataEnum::Replay)
	{
		getColdexState(rr1Replay, plcAlarmReplay, isIn, isOut);
	}
	else
	{
		getColdexState(rr1Online, plcAlarmOnline, isIn, isOut);
	}
}

/*
**	FUNCTION
**		Calculate state of COLDEX: IN/OUT/both/none
**
**	ARGUMENTS
**		rr1		- Value of RR1 DPE
**		plcAlarm	- Value of PLC alarm
**		isIn	- Varibale that will be set to true if COLDEX is IN
**		isOut	- Varibale that will be set to true if COLDEX is OUT
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVCLDX::getColdexState(unsigned rr1, bool plcAlarm, bool &isIn, bool &isOut)
{
	isIn = isOut = false;
	if(plcAlarm)
	{
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		return;
	}

	if(rr1 & RR1_IN)
	{
		isIn = true;
	}
	if(rr1 & RR1_OUT)
	{
		isOut = true;
	}
}

/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVCLDX::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("RR1");
	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, false, state);
	getMainColor(rr1, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif



/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVCLDX::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "EqpStatus");
	addDpeNameToList(dpes, "RR1");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVCLDX::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;

	if(!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if((rr1 & (RR1_IN | RR1_OUT)) == RR1_OUT)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if((rr1 & (RR1_IN | RR1_OUT)) == RR1_IN)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}
	return 0;
}
#endif
