#ifndef RACKICONGENERIC_H
#define RACKICONGENERIC_H

#include "RackIcon.h"

class Eqp;

class VACCTLEWOUTIL_EXPORT RackIconGeneric : public RackIcon
{
	Q_OBJECT
public:
	RackIconGeneric(QWidget * parent, Qt::WindowFlags f = 0);
	virtual ~RackIconGeneric();

	virtual void setEqp(Eqp * pEqp);
	virtual bool connect(void);
	virtual bool disconnect(void);
protected:
	Eqp * pEqp; //pointer to equipment, if it exists
	virtual void paintEvent(QPaintEvent *pEvent);

};

#endif
