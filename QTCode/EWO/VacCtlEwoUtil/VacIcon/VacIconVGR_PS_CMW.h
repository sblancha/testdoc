#ifndef	VACICONVGR_PS_CMW_H
#define	VACICONVGR_PS_CMW_H

//	Icon for VGR_PS_CMW

#include "VacIconVG_PS_CMW.h"

class VACCTLEWOUTIL_EXPORT VacIconVGR_PS_CMW : public VacIconVG_PS_CMW
{
public:
	VacIconVGR_PS_CMW(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG_PS_CMW(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGR_PS_CMW_H
