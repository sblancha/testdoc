//	Implementation of SynLhcLineBeamCommon class
/////////////////////////////////////////////////////////////////////////////////

#include "SynLhcLineBeamCommon.h"

#include "Eqp.h"
#include "EqpMsgCriteria.h"

#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

SynLhcLineBeamCommon::SynLhcLineBeamCommon(QWidget *parent, int type, float start, float end) :
	SynLhcLineBeam(parent, type, start, end)
{
}

SynLhcLineBeamCommon::~SynLhcLineBeamCommon()
{
	while(!volumes.isEmpty())
	{
		delete volumes.takeFirst();
	}
}

/*
**	FUNCTION
**		Copy outer equipment from one of 'main' lines (inner or outer)
**		to this line
**
**	PARAMETERS
**		pSrcLine	- Pointer to source line
**		isUpperLine	- Flag indicating if source line is upper or lower line on view
**
**	RETURNS
**		None
**
**	CAUTIONS
**		It is assumed that call with isUpperLine = false is a last call
*/
void SynLhcLineBeamCommon::copyEqp(SynLhcLine *pSrcLine, bool isUpperLine)
{
	if(isUpperLine)
	{
		volumes.clear();
		eqps.clear();
		y = pSrcLine->getY() - 5;
	}
	else
	{
		crossLineY = pSrcLine->getY() + 5;
	}

	const QList<SynLhcEqpItem *> &srcList = pSrcLine->getEqps();
	for(int srcIdx = 0 ; srcIdx < srcList.count() ; srcIdx++)
	{
		SynLhcEqpItem *pSrc = srcList.at(srcIdx);
		if(!pSrc->isOuterEqp())
		{
			continue;
		}
		if(!pSrc->getIcon())
		{
		}

		// Avoid duplication
		SynLhcEqpItem *pDst = NULL;
		if(!isUpperLine)
		{
			for(int dstIdx = 0 ; dstIdx < eqps.count() ; dstIdx++)
			{
				if(eqps.at(dstIdx)->getEqp() == pSrc->getEqp())
				{
					pDst = eqps.at(dstIdx);
					break;
				}
			}
		}
		if(pDst)
		{
			pSrc->deleteIcon();
			continue;
		}

		// Add new device
		pDst = new SynLhcEqpItem(*pSrc);
		pDst->setOuterEqp(false);
		pDst->moveVert(-5);
		eqps.append(pDst);
		pSrc->nullIcon();

		// Correct vertical limits
		QRect &rect = pDst->getRect();
		QPoint &connPoint = pDst->getConnPoint();
		if(rect.right() > endX)
		{
			endX = rect.right();
		}
		if(rect.top() < (y - ascent))
		{
			ascent = y - rect.top();
		}
		if(connPoint.y() < (y - ascent))
		{
			ascent = y - connPoint.y();
		}
		if(rect.bottom() > (y + descent))
		{
			descent = rect.bottom() - y;
		}
		if(connPoint.y() > (y + descent))
		{
			descent = connPoint.y() - y;
		}
	}
	if(isUpperLine || (!eqps.count()))
	{
		return;
	}

	// Finalize line content for correct drawing
	buildCommonVolumes( );
}

/*
**	FUNCTION
**		Build list of horizontal coordinates for common volumes. Every common
**		volume includes all devices belonging to the same vacuum sector.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeamCommon::buildCommonVolumes(void)
{
	Sector	*pLastSector = NULL;
	QRect *pLast = NULL;

	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		SynLhcEqpItem *pItem = eqps.at(idx);
		QPoint &connPoint = pItem->getConnPoint();
		if(pItem->getEqp()->getFunctionalType() == FunctionalType::EXT_ALARM)
		{
			continue;
		}
		if(pItem->getEqp()->getSectorBefore() != pLastSector)	// New volume
		{
//qDebug("New volume by %d %s\n", idx, pItem->getEqp->getName());
			pLastSector = pItem->getEqp()->getSectorBefore();
			pLast = new QRect(connPoint.x() - 3, 0, 0, 0);
			volumes.append(pLast);
		}
		pLast->setRight(connPoint.x() + 3);
	}
}

/*
**	FUNCTION
**		Draw all vacuum line parts.
**		This class has no parts, instead it has common vacuum volumes
**
**	PARAMETERS
**		painter		- Painter used for drawing
**		viewStart	- Start X coordinate of visible area
**		viewEnd		- End X coordinate of visible area
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SynLhcLineBeamCommon::drawParts(QPainter &painter, int viewStart, int viewEnd)
{
	if(!volumes.count())
	{
		return;
	}
	setPainterForLine(painter, VacType::CommonBeam);
	for(int idx = 0 ; idx < volumes.count() ; idx++)
	{
		QRect *pRect = volumes.at(idx);
		if((pRect->left() >= viewEnd) || (viewStart >= pRect->right()))
		{
			continue;
		}
		pRect->setTop(y);
		pRect->setBottom(crossLineY);
		painter.drawRect(*pRect);
	}
}

