#ifndef	PROFILELHCVPGCONNECTOR_H
#define	PROFILELHCVPGCONNECTOR_H

// LHC profile line on isolation vacuum needs fresh data from VPG_BP in order
// to know sector reference for gauges on these VPGs. EqpVPG_BP updates sector
// reference for child gauges when valves are opened/closed.
// However, we need to connect EqpVPG_BP instances to equipment (in online or
// replay mode) in order to allow them to receive fresh data and update sector
// reference for gauges.
// ProfileLhcLine subclasses can not inherit InterfaceEqp because of ambiguous
// QObject base. Hence, this class in introduced with the only purpose: provide
// connect/disconnect calls for VPG_BP

#include "InterfaceEqp.h"

class Eqp;

class ProfileLhcVpgConnector : public InterfaceEqp
{
public:
	ProfileLhcVpgConnector(Eqp *pEqp, DataEnum::DataMode mode);
	~ProfileLhcVpgConnector();

	// Access
	inline Eqp *getEqp(void) const { return pEqp; }

protected:
	// Pointer to VPG_BP device used
	Eqp		*pEqp;

	// Data acquisition mode
	DataEnum::DataMode	mode;
};

#endif	// PROFILELHCVPGCONNECTOR_H
