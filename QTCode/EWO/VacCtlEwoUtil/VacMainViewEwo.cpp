//	Implementation of VacMainViewEwo class
/////////////////////////////////////////////////////////////////////////////////


#include "VacMainViewEwo.h"

#include "DpConnection.h"
#include "DpeHistoryQuery.h"
#include "DpPolling.h"
#include "MobileHistoryQuery.h"
#include "DpeHistoryDepthQuery.h"

EWO_PLUGIN(VacMainViewEwo)


VacMainViewEwo::VacMainViewEwo(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = NULL;	// Subclass must instantiate widget instance
}

void VacMainViewEwo::connectStdSignals(void)
{
	if(!baseWidget)
	{
		return;
	}

	// Connect to signals of VacMainView
	connect(baseWidget, SIGNAL(selectionChanged(void)), this, SLOT(selectionChanged(void)));
	connect(baseWidget, SIGNAL(sectorMouseDown(int, int, int, const char *)),
		this, SLOT(sectorMouseDown(int, int, int, const char *)));
	connect(baseWidget, SIGNAL(dpMouseDown(int, int, int, int, int, const char *)),
		this, SLOT(dpMouseDown(int, int, int, int, int, const char *)));
	connect(baseWidget, SIGNAL(dpIconMouseDown(int, int, int, int, int, const char *)),
		this, SLOT(dpIconMouseDown(int, int, int, int, int, const char *)));
	connect(baseWidget, SIGNAL(rackIconMouseDown(int, int, int, int, int, const char *)),
		this, SLOT(rackIconMouseDown(int, int, int, int, int, const char *)));
	connect(baseWidget, SIGNAL(dialogRequested(int, unsigned, const QStringList &, DataEnum::DataMode)),
		this, SLOT(dialogRequest(int, unsigned, const QStringList &, DataEnum::DataMode)));
	connect(baseWidget, SIGNAL(pollingPeriodChanged(int)),
		this, SLOT(pollingPeriodChanged(int)));
	connect(baseWidget, SIGNAL(historySaveRequest(int)),
		this, SLOT(historySaveRequest(int)));
	connect(baseWidget, SIGNAL(historySaveMultiRequest(int)),
		this, SLOT(historySaveMultiRequest(int)));
	connect(baseWidget, SIGNAL(historyLoadRequest(int)),
		this, SLOT(historyLoadRequest(int)));
	connect(baseWidget, SIGNAL(historyFillNumberRequest(int)),
		this, SLOT(historyFillNumberRequest(int)));
	connect(baseWidget, SIGNAL(sectorDoubleClick(const QString &, int)),
		this, SLOT(sectorDoubleClicked(const QString &, int)));
	connect(baseWidget, SIGNAL(eqpDoubleClick(const QString &, int)),
		this, SLOT(eqpDoubleClicked(const QString &, int)));
	connect(baseWidget, SIGNAL(synSectorMouseDown(int, int, int, int, const char *, bool)),
		this, SLOT(synSectorMouseDown(int, int, int, int, const char *, bool))); // [VACCO-929]

	// Connect to signals of DpConnection instance
	DpConnection &connection = DpConnection::getInstance();
	connect(&connection, SIGNAL(changed(void)), this, SLOT(dpConnectChanged(void)));

	// Connect to signals of DpeHistoryQuery instance
	DpeHistoryQuery &historyQuery = DpeHistoryQuery::getInstance();
	connect(&historyQuery, SIGNAL(changed(void)), this, SLOT(dpeHistoryQueryQueueChange(void)));

	// Connect to signals of DpPolling instance
	DpPolling &polling = DpPolling::getInstance();
	connect(&polling, SIGNAL(changed(void)), this, SLOT(dpPollingChanged(void)));

	// Connect to signals of MobileHistoryQuery instance
	MobileHistoryQuery &mobileHist = MobileHistoryQuery::getInstance();
	connect(&mobileHist, SIGNAL(changed(void)), this, SLOT(mobileHistoryQueryChange(void)));

	// Connect to signals of DpeHistoryDepthQuery instance
	DpeHistoryDepthQuery &dpeHistDepth = DpeHistoryDepthQuery::getInstance();
	connect(&dpeHistDepth, SIGNAL(changed(void)), this, SLOT(dpeHistoryDepthQueryChange(void)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList VacMainViewEwo::signalList(void) const
{
	QStringList list;
	list.append("selectionChanged()");
	list.append("sectorMouseDown(int button, int mode, int x, int y, string sectorName)");
	list.append("dpMouseDown(int button, int mode, int x, int y, int extra, string dpName)");
	list.append("dpIconMouseDown(int button, int mode, int x, int y, int extra, string dpName)");
	list.append("rackIconMouseDown(int button, int mode, int x, int y, int extra, string eqpName)");
	list.append("dpConnectRequest()");
	list.append("dialogRequest(int type, unsigned vacType, dyn_string sectorList, int mode)");
	list.append("dpeHistoryQueryQueueChange()");
	list.append("dpPollingRequest()");
	list.append("pollingPeriodChanged(int period)");
	list.append("mobileHistoryRequest()");
	list.append("dpeHistoryDepthRequest()");
	list.append("historySaveRequest(int historyId)");
	list.append("historyLoadRequest(int historyId)");
	list.append("historyFillNumberRequest(int historyId)");
	list.append("historySaveMultiRequest(int historyId)");
	list.append("sectorDoubleClicked(string sector, int mode)");
	list.append("eqpDoubleClicked(string dpName, int mode)");
	list.append("synSectorMouseDown(int button, int mode, int x, int y, string sectorName, bool selected)"); // [VACCO-929]
	return list;
}



/*
**	FUNCTION
**		[VACCO-929]
**		Slot receiving synSectorMouseDown signal of MainLhc, emit
**		signal 'synSectorMouseDown' to PVSS
**
**	ARGUMENTS
**		button		- mouse button number (always 2?)
**		x			- X coordinate of mouse pointer
**		y			- Y coordinate of mouse pointer
**		sectorName	- Name of sector where mouse press occured
**		selected	- selection state of the sector
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::synSectorMouseDown(int button, int mode, int x, int y, const char *sectorName, bool selected)
{
	QList<QVariant> args;
	args.append(QVariant(button));
	args.append(QVariant(mode));
	args.append(QVariant(x));
	args.append(QVariant(y));
	args.append(QVariant(QString(sectorName)));
	args.append(QVariant((bool)(selected)));
	emit signal("synSectorMouseDown", args);
}




/*
**	FUNCTION
**		Slot receiving selectionChanged signal of MainLHC, emit
**		signal 'selectionChanged' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::selectionChanged(void)
{
	emit signal("selectionChanged");
}

/*
**	FUNCTION
**		Slot receiving sectorMouseDown signal of MainLhc, emit
**		signal 'sectorMouseDown' to PVSS
**
**	ARGUMENTS
**		button		- mouse button number (always 2?)
**		x			- X coordinate of mouse pointer
**		y			- Y coordinate of mouse pointer
**		sectorName	- Name of sector where mouse press occured
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::sectorMouseDown(int button, int x, int y, const char *sectorName)
{
	QList<QVariant> args;
	args.append(QVariant(button));
	args.append(QVariant(x));
	args.append(QVariant(y));
	args.append(QVariant(QString(sectorName)));
	emit signal("sectorMouseDown", args);
}

/*
**	FUNCTION
**		Slot receiving dpMouseDown signal of MainLhc, emit
**		signal 'dpMouseDown' to PVSS
**
**	ARGUMENTS
**		button	- mouse button number
**		mode	- Data acquisition mode
**		x		- X coordinate of mouse pointer
**		y		- Y coordinate of mouse pointer
**		extra	- Extra parameter to identify PART of icon
**		dpName	- Name of sector where mouse press occured
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::dpMouseDown(int button, int mode, int x, int y, int extra, const char *dpName)
{
	QList<QVariant> args;
	args.append(QVariant(button));
	args.append(QVariant(mode));
	args.append(QVariant(x));
	args.append(QVariant(y));
	args.append(QVariant(extra));
	args.append(QVariant(QString(dpName)));
	emit signal("dpMouseDown", args);
}


/*
**	FUNCTION
**		Slot receiving dpIconMouseDown signal of MainLhc, emit
**		signal 'dpIconMouseDown' to PVSS
**
**	ARGUMENTS
**		button	- mouse button number
**		mode	- Data acquisition mode
**		x		- X coordinate of mouse pointer
**		y		- Y coordinate of mouse pointer
**		extra	- Extra parameter to identify PART of icon
**		dpName	- Name of sector where mouse press occured
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::dpIconMouseDown(int button, int mode, int x, int y, int extra, const char *dpName)
{
	QList<QVariant> args;
	args.append(QVariant(button));
	args.append(QVariant(mode));
	args.append(QVariant(x));
	args.append(QVariant(y));
	args.append(QVariant(extra));
	args.append(QVariant(QString(dpName)));
	emit signal("dpIconMouseDown", args);
}


/*
**	FUNCTION
**		Slot receiving rackIconMouseDown signal of MainView, emit
**		signal 'rackIconMouseDown' to PVSS
**
**	ARGUMENTS
**		button	- mouse button number
**		mode	- Data acquisition mode
**		x		- X coordinate of mouse pointer
**		y		- Y coordinate of mouse pointer
**		extra	- Extra parameter to identify PART of icon
**		dpName	- Name of sector where mouse press occured
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::rackIconMouseDown(int button, int mode, int x, int y, int extra, const char *eqpName)
{
	QList<QVariant> args;
	args.append(QVariant(button));
	args.append(QVariant(mode));
	args.append(QVariant(x));
	args.append(QVariant(y));
	args.append(QVariant(extra));
	args.append(QVariant(QString(eqpName)));
	emit signal("rackIconMouseDown", args);
}


/*
**	FUNCTION
**		Slot receiving changed() signal of DpConnection instance, emit
**		signal 'dpConnectRequest' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::dpConnectChanged(void)
{
	emit signal("dpConnectRequest");
}

/*
**	FUNCTION
**		Slot receiving dialogRequested() signal of VacMainView instance, emit
**		signal 'dialogRequest' to PVSS
**
**	ARGUMENTS
**		type		- Requested dialog type, see enum in VacMainView class
**		vacType		- Vacuum type bit mask
**		sectorList	- List of vacuum sector names
**		mode		- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::dialogRequest(int type, unsigned vacType, const QStringList &sectorList,
	DataEnum::DataMode mode)
{
	QList<QVariant> args;
	args.append(QVariant(type));
	args.append(QVariant(vacType));
	args.append(QVariant(sectorList));
	args.append(QVariant((int)mode));
	emit signal("dialogRequest", args);
}

/*
**	FUNCTION
**		Slot receiving changed() signal of DpeHistoryQuery instance, emit
**		signal 'dpeHistoryQueryQueueChange' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::dpeHistoryQueryQueueChange(void)
{
	emit signal("dpeHistoryQueryQueueChange");
}

/*
**	FUNCTION
**		Slot receiving changed() signal of DpPolling instance, emit
**		signal 'dpPollingRequest' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::dpPollingChanged(void)
{
	emit signal("dpPollingRequest");
}

/*
**	FUNCTION
**		Slot receiving pollingPeriodChanged signal of MainLhc, emit
**		signal 'pollingPeriodChanged' to PVSS
**
**	ARGUMENTS
**		period	- New polling period
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::pollingPeriodChanged(int period)
{
	emit signal("pollingPeriodChanged", QVariant(period));
}

/*
**	FUNCTION
**		Slot receiving changed() signal of MobileHistoryQuery instance, emit
**		signal 'mobileHistoryRequest' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::mobileHistoryQueryChange(void)
{
	emit signal("mobileHistoryRequest");
}

/*
**	FUNCTION
**		Slot receiving changed() signal of DpeHistoryDepthQuery instance, emit
**		signal 'dpeHistoryDepthRequest' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::dpeHistoryDepthQueryChange(void)
{
	emit signal("dpeHistoryDepthRequest");
}

/*
**	FUNCTION
**		Slot receiving historySaveRequest signal of MainLhc, emit
**		signal 'historySaveRequest' to PVSS
**
**	ARGUMENTS
**		id	- Identificator of history dialog instance
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::historySaveRequest(int id)
{
	emit signal("historySaveRequest", QVariant(id));
}

/*
**	FUNCTION
**		Slot receiving historySaveRequest signal of MainLhc, emit
**		signal 'historySaveRequest' to PVSS
**
**	ARGUMENTS
**		id	- Identificator of history dialog instance
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::historySaveMultiRequest(int id)
{
	emit signal("historySaveMultiRequest", QVariant(id));
}

/*
**	FUNCTION
**		Slot receiving historyLoadRequest signal of MainLhc, emit
**		signal 'historyLoadRequest' to PVSS
**
**	ARGUMENTS
**		id	- Identificator of history dialog instance
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::historyLoadRequest(int id)
{
	emit signal("historyLoadRequest", QVariant(id));
}

/*
**	FUNCTION
**		Slot receiving historyFillNumberRequest signal of MainLhc, emit
**		signal 'historyFillNumberRequest' to PVSS
**
**	ARGUMENTS
**		id	- Identificator of history dialog instance
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacMainViewEwo::historyFillNumberRequest(int id)
{
	emit signal("historyFillNumberRequest", QVariant(id));
}

void VacMainViewEwo::sectorDoubleClicked(const QString &sector, int mode)
{
	QList<QVariant> args;
	args.append(QVariant(sector));
	args.append(QVariant(mode));
	emit signal("sectorDoubleClicked", args);
}

void VacMainViewEwo::eqpDoubleClicked(const QString &dpName, int mode)
{
	QList<QVariant> args;
	args.append(QVariant(dpName));
	args.append(QVariant(mode));
	emit signal("eqpDoubleClicked", args);
}

