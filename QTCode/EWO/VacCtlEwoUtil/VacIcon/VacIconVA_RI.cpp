#include "VacIconVA_RI.h"
#include "EqpVA_RI.h"
#include <QPainter>

VacIconVA_RI::VacIconVA_RI(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f) {
	pEqp = NULL;
}
VacIconVA_RI::~VacIconVA_RI() {
	disconnect();
}
/**
@brief Return geometry for this icon
@param	mask	vacuum equipment mask, not used here
*/
VacIconGeometry VacIconVA_RI::getGeometry(const VacEqpTypeMask & /* mask */) {
	switch(direction) {
	case VacIconContainer::Down:
	case VacIconContainer::Up:
		return VacIconGeometry(21, 11, 10, 5, 1);//(int width, int height, int x /*origin for pipe*/, int y /*origin for pipe*/, int verticalPos /*insertion offset*/)
	case VacIconContainer::Left:
	case VacIconContainer::Right:
		return VacIconGeometry(11, 21, 5, 10, 1);
	default:
		break;
	}
	return VacIconGeometry(21, 11, 10, 5, 1);
}
/**
@brief Set reference to equipment for this icon
@param		pEqp	pointer to device to be used by this icon
*/
void VacIconVA_RI::setEqp(Eqp *pEqp) {
	disconnect();
	if(pEqp) {
		Q_ASSERT_X(pEqp->inherits("EqpVA_RI"), "VacIconVA_RI::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVA_RI *)pEqp;
	}
	forceRedraw();
}
/**
@brief FUNCTION Set new data acquisition mode for this icon
@param[in]   newMode	New data aqn mode (online, replay...)
*/
void VacIconVA_RI::setMode(DataEnum::DataMode newMode) {
	mode = newMode;
	forceRedraw();
}
/**
@brief Connect to DPEs of device
*/
bool VacIconVA_RI::connect(void) {
	if(connected || (!pEqp)) {
		return false;
	}
	QStringList dpes;
	dpes.append("RR1");
	dpes.append("channelSourceSt");
	connection.connect(pEqp, dpes, mode);
	connected = true;
	return true;
}
/**
@brief Disconnect from DPEs of device
*/
bool VacIconVA_RI::disconnect(void) {
	bool result = false;
	if(connected && pEqp) {
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}
/**
@brief Calculate text and rectangle for tooltip of this icon
@param		pos		position of mouse pointer WITHIN this widget
@param		text	variable where this method shall write tooltip text to be shown
@param		rect	variable where this method shall write rectangle for visibility
@param					of returned tooltip: tolltip will be hidden when mouse pointer
*/
bool VacIconVA_RI::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect) {
	rect = this->rect();
	if(pEqp && connected) {
		pEqp->getToolTipString(text, mode);
	}
	else {
		text = "Not connected";
	}
	return true;
}
/**
@brief Drawing implementation
*/
void VacIconVA_RI::paintEvent(QPaintEvent * /* pEvent */) {
	unsigned rr1 = 0;
	bool comAlarm = false;
	Eqp::CtlStatus ctlStatus = Eqp::Used;
	if (pEqp) {
		ctlStatus = pEqp->getCtlStatus(mode);
		rr1 = pEqp->getRR1(mode);
		comAlarm = pEqp->isComAlarm(mode);
	}
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());
	// Save coordinate ref to be restored after the draw
	painter.save();
	QColor color;
	pEqp->getMainColor(color, mode);
	painter.setPen(QPen(color, 2, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin));
	// Blinking behavior
	if (pEqp)
		isBlinking = pEqp->isAlertNotAck();
	else
		isBlinking = false;
	if (isBlinking && !isBlinkVisible)
		return; // hiden phase of blinking
	// Draw the arrow
	switch (direction) {
	case VacIconContainer::Up: //above line
		if (pEqp->isIntlSourcePrev()) {
			//draw arrow horizontal direction after
			painter.drawLine(2, 5, 18, 5);
			painter.drawLine(18, 5, 15, 2);
			painter.drawLine(18, 5, 15, 8);
		}
		if (pEqp->isIntlSourceNext()) {
			//draw arrow horizontal direction before
			painter.drawLine(2, 5, 18, 5);
			painter.drawLine(2, 5, 5, 2);
			painter.drawLine(2, 5, 5, 8);
		}
		if (pEqp->isIntlSourceExt()) {
			//draw arrow user like = vertical
			painter.drawLine(10, 1, 10, 9);
			painter.drawLine(10, 1, 7, 4);
			painter.drawLine(10, 1, 13, 4);
		}
		break;
	case VacIconContainer::Down: //below line
		if (pEqp->isIntlSourcePrev()) {
			//draw arrow horizontal direction after
			painter.drawLine(2, 5, 18, 5);
			painter.drawLine(18, 5, 15, 2);
			painter.drawLine(18, 5, 15, 8);
		}
		if (pEqp->isIntlSourceNext()) {
			//draw arrow horizontal direction before
			painter.drawLine(2, 5, 18, 5);
			painter.drawLine(2, 5, 5, 2);
			painter.drawLine(2, 5, 5, 8);
		}
		if (pEqp->isIntlSourceExt()) {
			//draw arrow user like = vertical
			painter.drawLine(10, 1, 10, 9);
			painter.drawLine(10, 9, 7, 6);
			painter.drawLine(10, 9, 13, 6);
		}
		break;
	case VacIconContainer::Left: // at left of the line
		if (pEqp->isIntlSourcePrev()) {
			//draw arrow horizontal direction after
			painter.drawLine(5, 18, 2, 5);
			painter.drawLine(5, 2, 2, 5);
			painter.drawLine(5, 2, 8, 5);
		}
		if (pEqp->isIntlSourceNext()) {
			//draw arrow horizontal direction before
			painter.drawLine(5, 18, 2, 5);
			painter.drawLine(5, 18, 2, 15);
			painter.drawLine(5, 18, 8, 18);
		}
		if (pEqp->isIntlSourceExt())  {
			//draw arrow user like = vertical
			painter.drawLine(1, 10, 9, 10);
			painter.drawLine(1, 10, 4, 7);
			painter.drawLine(1, 10, 4, 13);
		}
		break;
	case VacIconContainer::Right: // at right of the line
		if (pEqp->isIntlSourcePrev()) {
			//draw arrow horizontal direction after
			painter.drawLine(5, 18, 2, 5);
			painter.drawLine(5, 18, 2, 15);
			painter.drawLine(5, 18, 8, 18);
		}
		if (pEqp->isIntlSourceNext()) {
			//draw arrow horizontal direction before
			painter.drawLine(5, 18, 2, 5);
			painter.drawLine(5, 2, 2, 5);
			painter.drawLine(5, 2, 8, 5);
		}
		if (pEqp->isIntlSourceExt())  {
			//draw arrow user like = vertical
			painter.drawLine(1, 10, 9, 10);
			painter.drawLine(9, 10, 6, 7);
			painter.drawLine(9, 10, 6, 13);
		}
		break;
	}
	// Draw not Default letter
	/* Finally do not draw letter because icon too small
	if (pEqp->isParamNotDef()) {
		painter.setPen(Qt::red);
		switch (direction) {
		case VacIconContainer::Up:
		case VacIconContainer::Down:
			painter.drawText(15, 0, 7, 11, Qt::AlignCenter, "I");
		case VacIconContainer::Left:
		case VacIconContainer::Right:
			painter.drawText(4, 15, 7, 6, Qt::AlignCenter, "I");
		default:
			painter.drawText(15, 0, 7, 11, Qt::AlignCenter, "I");
		}
	}
	*/
	painter.restore();
	//Draw selection on top of image
	if(pEqp) {
		if(pEqp->isSelected()) {
			drawSelection(painter);
		}
	}
}