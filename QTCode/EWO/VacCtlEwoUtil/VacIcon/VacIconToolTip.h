#ifndef VACICONTOOLTIP_H
#define	VACICONTOOLTIP_H

#include "VacCtlEwoUtilExport.h"

// Tooltip for icon

#include <qtooltip.h>

class VACCTLEWOUTIL_EXPORT VacIconToolTip : public QToolTip
{
public:
	VacIconToolTip(QWidget *parent);

protected:
	virtual void maybeTip(const QPoint &pos);
};

#endif	// VACICONTOOLTIP_H
