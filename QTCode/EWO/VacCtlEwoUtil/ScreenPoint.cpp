//	Implementation of ScreenPoint class
/////////////////////////////////////////////////////////////////////////////////

#include "ScreenPoint.h"


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

ScreenPoint::ScreenPoint()
{
	reset();
}

void ScreenPoint::reset(void)
{
	x = y = timeRange = 0;
	drawing = prevExists = negative = false;
}
