#ifndef	VACICONV8DI_FE_H
#define	VACICONV8DI_FE_H

//	Icon for V8DI_FE generic DI object
//  2017.10.20 {sblancha} widget added for the case DI channel 0 is used to read interlock status.
//						the di0Description attribute is retrieved  

#include "VacIcon.h"

class EqpV8DI_FE;

class VACCTLEWOUTIL_EXPORT VacIconV8DI_FE : public VacIcon
{
	Q_OBJECT

public:
	// Note parent argument is mandatory in constructor
	VacIconV8DI_FE(QWidget *parent, Qt::WindowFlags f = 0);
	virtual ~VacIconV8DI_FE();

	// Implementation of VacIcon's abstract methods
	virtual VacIconGeometry getGeometry(const VacEqpTypeMask &mask);
	virtual void setEqp(Eqp *pEqp);
	virtual Eqp *getEqp(void) { return (Eqp *)pEqp; }
	virtual void setMode(DataEnum::DataMode newMode);
	virtual bool connect(void);
	virtual bool disconnect(void);
	virtual bool getToolTip(const QPoint &pos, QString &text, QRect &rect);

protected:
	// Pointer to my device
	EqpV8DI_FE *pEqp;

	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// VACICONV8DI_FE_H
