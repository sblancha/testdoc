//	Implementation of VacIconVPG_SA_VG	class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVPG_SA_VG.h"

#include "EqpVPG_SA.h"
#include "DataPool.h"

#include <qpainter.h>
#include <QMouseEvent>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
VacIconVPG_SA_VG::VacIconVPG_SA_VG(QWidget *parent, Qt::WindowFlags f) :
	VacIconVPG_SA(parent, f)
{
	pIconVGR = pIconVGP = pIconVGF = NULL;
	setFixedSize(47, 59);
	setAreaGeometry();
}

VacIconVPG_SA_VG::~VacIconVPG_SA_VG()
{
}

/*
**	FUNCTION
**		Calculate geometry for symbol areas rectangles
**		for current direction and beamDirection
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_SA_VG::setAreaGeometry(void)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		valve.setRect(15, 1, 17, 17);
		vgr.setRect(0, 19, 21, 21);
		vgp.setRect(26, 19, 21, 21);
		pump.setRect(15, 41, 17, 17);
		break;
	case VacIconContainer::Down:
		valve.setRect(15, 41, 17, 17);
		vgr.setRect(0, 19, 21, 21);
		vgp.setRect(26, 19, 21, 21);
		pump.setRect(15, 1, 17, 17);
		break;
	case VacIconContainer::Left:
		valve.setRect(41, 15, 17, 17);
		vgr.setRect(19, 0, 21, 21);
		vgp.setRect(19, 26, 21, 21);
		pump.setRect(1, 15, 17, 17);
		break;
	case VacIconContainer::Right:
		valve.setRect(1, 15, 17, 17);
		vgr.setRect(19, 0, 21, 21);
		vgp.setRect(19, 26, 21, 21);
		pump.setRect(41, 15, 17, 17);
		break;
	}
	if(pIconVGR)
	{
		pIconVGR->move(vgr.left(), vgr.top());
	}
	if(pIconVGP)
	{
		pIconVGP->move(vgp.left(), vgp.top());
	}
	if(pIconVGF)
	{
		switch(direction)
		{
		case VacIconContainer::Up:
		case VacIconContainer::Down:
			pIconVGF->move(vgr.left() + 3, vgr.top());
			break;
		case VacIconContainer::Left:
		case VacIconContainer::Right:
			pIconVGF->move(vgr.left(), vgr.top() + 3);
			break;
		}
	}
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVPG_SA_VG::getGeometry(const VacEqpTypeMask & /* mask */)
{
	switch(direction)
	{
	case VacIconContainer::Up:
		return VacIconGeometry(47, 59, 23, 60, -1);
	case VacIconContainer::Down:
		return VacIconGeometry(47, 59, 23, 60, -1);
	default:
		break;
	}
	return VacIconGeometry(59, 47, 60, 23, -1);
}

/*
**	FUNCTION
**		Override method of VacIcon: geometry of this icon depends on direction
**
**	ARGUMENTS
**		newDirection	- new icon direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_SA_VG::setDirection(VacIconContainer::Direction newDirection)
{
	QSize	size(1, 1);
	switch(newDirection)
	{
	case VacIconContainer::Up:
	case VacIconContainer::Down:
		size.setWidth(47);
		size.setHeight(59);
		break;
	default:
		size.setWidth(59);
		size.setHeight(47);
		break;
	}
	setFixedSize(size);
	resize(size);
	VacIcon::setDirection(newDirection);
	setAreaGeometry();
	if(pIconVGR)
	{
		pIconVGR->setDirection(calculateVgrDirection());
	}
	if(pIconVGP)
	{
		pIconVGP->setDirection(calculateVgpDirection());
	}
	if(pIconVGF)
	{
		pIconVGF->setDirection(calculateVgrDirection());
	}
}

VacIconContainer::Direction VacIconVPG_SA_VG::calculateVgrDirection(void)
{
	VacIconContainer::Direction result = VacIconContainer::Up;
	switch(direction)
	{
	case VacIconContainer::Up:
		result = VacIconContainer::Right;
		break;
	case VacIconContainer::Down:
		result = VacIconContainer::Left;
		break;
	case VacIconContainer::Left:
		result = VacIconContainer::Down;
		break;
	default:
		result = VacIconContainer::Up;
		break;
	}
	return result;
}

VacIconContainer::Direction VacIconVPG_SA_VG::calculateVgpDirection(void)
{
	VacIconContainer::Direction result = VacIconContainer::Up;
	switch(direction)
	{
	case VacIconContainer::Up:
		result = VacIconContainer::Left;
		break;
	case VacIconContainer::Down:
		result = VacIconContainer::Right;
		break;
	case VacIconContainer::Left:
		result = VacIconContainer::Up;
		break;
	default:
		result = VacIconContainer::Down;
		break;
	}
	return result;
}


/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_SA_VG::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pIconVGR)
	{
		delete pIconVGR;
		pIconVGR = NULL;
	}
	if(pIconVGP)
	{
		delete pIconVGP;
		pIconVGP = NULL;
	}
	if(pIconVGF)
	{
		delete pIconVGF;
		pIconVGF = NULL;
	}
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVPG_SA"), "VacIconVPG_SA_VG::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVPG_SA *)pEqp;

		QString attrName = pEqp->getAttrValue("VGR1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGR = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGR)
		{
			pIconVGR->move(vgr.left(), vgr.top());
			pIconVGR->setMode(mode);
			pIconVGR->setPipeColor(pipeColor);
			pIconVGR->setDirection(calculateVgrDirection());
			if(connected)
			{
				pIconVGR->connect();
			}
		}

		attrName = pEqp->getAttrValue("VGP1");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGP = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGP)
		{
			pIconVGP->move(vgp.left(), vgp.top());
			pIconVGP->setMode(mode);
			pIconVGP->setPipeColor(pipeColor);
			pIconVGP->setDirection(calculateVgpDirection());
			if(connected)
			{
				pIconVGP->connect();
			}
		}

		attrName = pEqp->getAttrValue("VGF");
		if(!attrName.isEmpty())
		{
			Eqp *pChild = DataPool::getInstance().findEqpByDpName(attrName.toLatin1());
			if(pChild)
			{
				pIconVGF = VacIcon::getIcon(pChild, this);
			}
		}
		if(pIconVGF)
		{
			pIconVGF->setDrawConnectionPipe(false);
			switch(direction)
			{
			case VacIconContainer::Up:
			case VacIconContainer::Down:
				pIconVGF->move(vgr.left() + 5, vgr.top());
				break;
			case VacIconContainer::Left:
			case VacIconContainer::Right:
				pIconVGF->move(vgr.left(), vgr.top() + 5);
				break;
			}
			pIconVGF->setMode(mode);
			pIconVGF->setPipeColor(pipeColor);
			pIconVGF->setDirection(calculateVgrDirection());
			if(connected)
			{
				pIconVGF->connect();
			}
		}
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_SA_VG::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	if(pIconVGR)
	{
		pIconVGR->setMode(newMode);
	}
	if(pIconVGP)
	{
		pIconVGP->setMode(newMode);
	}
	if(pIconVGF)
	{
		pIconVGF->setMode(newMode);
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set color used for pipe lines drawing
**
**	ARGUMENTS
**		newColor	- Color used for pipe drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVPG_SA_VG::setPipeColor(const QColor &newColor)
{
	if(pIconVGR)
	{
		pIconVGR->setPipeColor(newColor);
	}
	if(pIconVGP)
	{
		pIconVGP->setPipeColor(newColor);
	}
	if(pIconVGF)
	{
		pIconVGF->setPipeColor(newColor);
	}
	VacIconVPG_SA::setPipeColor(newColor);
}


/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_SA_VG::connect(void)
{
	if(pIconVGR)
	{
		pIconVGR->connect();
	}
	if(pIconVGP)
	{
		pIconVGP->connect();
	}
	if(pIconVGF)
	{
		pIconVGF->connect();
	}
	return VacIconVPG_SA::connect();
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVPG_SA_VG::disconnect(void)
{
	if(pIconVGR)
	{
		pIconVGR->disconnect();
	}
	if(pIconVGP)
	{
		pIconVGP->disconnect();
	}
	if(pIconVGF)
	{
		pIconVGF->disconnect();
	}
	return VacIconVPG_SA::disconnect();
}

/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVPG_SA_VG::paintEvent(QPaintEvent * /* pEvent */)
{
	// Draw connecting lines
	QPainter painter(this);
	painter.setPen(pipeColor);

	switch(direction)
	{
	case VacIconContainer::Up:
		// Line from VVR to outside
		painter.fillRect(pump.center().x() - 1, 0, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, valve.bottom() + 1,
			3, pump.top() - valve.bottom(), pipeColor);
		// Connection line for gauges
		painter.fillRect(vgr.right() - 2, vgr.center().y() - 1, vgp.left() - vgr.right() + 4, 3, pipeColor);
		break;
	case VacIconContainer::Down:
		// Line from VVR to outside
		painter.fillRect(pump.center().x() - 1, valve.bottom(), 3, 4, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.center().x() - 1, pump.bottom(),
			3, valve.top() - pump.bottom(), pipeColor);
		// Connection line for gauges
		painter.fillRect(vgr.right() - 2, vgr.center().y() - 1, vgp.left() - vgr.right() + 4, 3, pipeColor);
		break;
	case VacIconContainer::Left:
		// Line from VVR to outside
		painter.fillRect(valve.right(), pump.center().y() - 1, 3, 3, pipeColor);
		// Connect pump to valve
		painter.fillRect(pump.right(), pump.center().y() - 1,
			valve.left() - pump.right(), 3, pipeColor);
		// Connection line for gauges
		painter.fillRect(vgr.center().x() - 1, vgr.bottom() - 2, 3, vgp.top() - vgr.bottom() + 4, pipeColor);
		break;
	case VacIconContainer::Right:
		// Line from VVR to outside
		painter.fillRect(0, pump.center().y() - 1, 3, 3, pipeColor);
		// Connect pump to valves
		painter.fillRect(valve.right(), pump.center().y() - 1,
			pump.left() - valve.right(), 3, pipeColor);
		// Connection line for gauges
		painter.fillRect(vgr.center().x() - 1, vgr.bottom() - 2, 3, vgp.top() - vgr.bottom() + 4, pipeColor);
		break;
	}

	if(!pEqp)
	{
		painter.setPen(Qt::black);
		painter.drawRect(vgr.left(), vgr.top(), vgr.width() - 1, vgr.height() - 1);
		painter.drawRect(vgp.left(), vgp.top(), vgp.width() - 1, vgp.height() - 1);
	}

	// Draw valve and pump image - superclass
	VacIconVPG_SA::drawImage(false);
}
