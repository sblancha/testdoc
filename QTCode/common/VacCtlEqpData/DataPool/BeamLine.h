#ifndef BEAMLINE_H
#define BEAMLINE_H

#include "VacCtlEqpDataExport.h"

// Class holding beam line information

#include "FileParser.h"
#include "BeamLineConnType.h"
#include "VacType.h"
#include "BeamLinePart.h"

#include <qlist.h>

class DataPool;
class LhcRegion;
class Eqp;
class Sector;

#ifndef M_PI
#define M_PI (3.141592653589793)
#endif

class VACCTLEQPDATA_EXPORT BeamLine : public FileParser
{
public:
	BeamLine(char *name, bool isCircle);
	virtual ~BeamLine();

	void clearActive(void);

	bool addEqp(Eqp *pEqp, bool pureSurvey);
	bool finish(QStringList &errList);
	void turn(void);
	void resize(void);
	void findGeoLimits(void);
	void buildOrderedEqp(void);
	bool isClockWise(void);
#ifndef PVSS_SERVER_VERSION
	void makeXY(void);
	void getEndPoints(float *xStart, float *yStart1, float *xEnd, float *yEnd);
	int maxDrawOrder(void);
	int maxDrawEqpOrder(void);
	void buildBeamSectorBorders(void);
	void findNeighbourSectorBorder(Eqp *pStartEqp, LhcRegion *pRegion, int vacType,
		int direction, Eqp **ppOtherBorderEqp, float &borderCoord);
	void neighbourEqp(BeamLinePart **ppPart, int &eqpIdx, int direction);
#endif
	bool coordInLine(const char *pName, char *pForLine, int connectVacType, float coord, bool isStart, float &resultX, float &resultY);
	BeamLinePart *nextPart(BeamLinePart *pStartPart);
	bool findEqpRange(Sector *pStartSector, Sector *pEndSector,
		BeamLinePart **ppStartPart, BeamLinePart **ppEndPart,
		int &startEqpIdx, int &endEqpIdx);

	// L.Kopylov 21.01.2010 to be sure all sectors (of SPS) are visible on main view
	void setMinSectorLength(int minLength);
	
	// Access
	inline const char *getName(void) const { return name; }
	inline const char *getDataPart(void) { return dataPart; }
	inline bool isNotUsed(void) const { return notUsed; }
	inline bool isVirtualLine(void) const { return virtualLine; }
	inline void setVirtualLine(bool virtualLine) { this->virtualLine = virtualLine; }
	inline bool isHidden(void) const { return hidden; }
	inline void setHidden(bool hidden) { this->hidden = hidden; }
	inline bool isCircle(void) const { return circle; }
	inline const char *getStartLine(void) const { return startLine; }
	inline float getStartCoord(void) const { return startCoord; }
	inline int getStartType(void) const { return startType; }
	inline int getStartVacType(void) const { return startVacType; }
	inline const char *getEndLine(void) const { return endLine; }
	inline float getEndCoord(void) const { return endCoord; }
	inline int getEndType(void) const { return endType; }
	inline int getEndVacType(void) const { return startVacType; }
	inline bool isContainSectorBorder(void) const { return containSectorBorder; }
	inline void setContainSectorBorder(bool containSectorBorder) { this->containSectorBorder = containSectorBorder; }
	inline QList<BeamLinePart *> &getParts(void) const { return *pParts; }
	inline int getVerticalPos(void) const { return verticalPos; }
	inline bool isHideOnMainView(void) const { return hideOnMainView; }
	inline void setHideOnMainView(bool flag) { hideOnMainView = flag; }

	inline bool isAutoTagOn(void) { return autoTag; } // [VACCO-527]
	inline QString getTagStartManual(void) const { return tagStartManual; } // [VACCO-527]
	inline QString getTagEndManual(void) const { return tagEndManual; } // [VACCO-527]
	inline void setTagStartManual(QString text) { tagStartManual = text; } // [VACCO-527]
	inline void setTagEndManual(QString text) { tagEndManual = text; } // [VACCO-527]

#ifndef PVSS_SERVER_VERSION
	inline float getMinX(void) { return minX; }
	inline float getMinY(void) { return minY; }
	inline float getMaxX(void) { return maxX; }
	inline float getMaxY(void) { return maxY; }
#endif
	
	void dump(FILE *pFile, int lineIdx);


protected:

	// Type of reference point for transfromation (resize, turn...)
	enum
	{
		RefNone = 0,
		RefStart = 1,
		RefEnd = 2,
		RefCenter = 3
	};

	// Type of reference point - see enum above
	int					refType;

	// Name of beam line
	char				*name;

	// Data part of this line
	char				*dataPart;

	bool testEqp;

	// Flag indicating if this beam line is not used
	bool				notUsed;

	// Flag indicating if beam line is circle
	bool				circle;

	// Name of survey partition where this line starts, NULL if this line does not start at another line
	char				*startLine;

	// Coordinate of this line start in another line
	float				startCoord;

	// How this line starts from another line, see BeamLineConnType class
	int					startType;

	// Vacuum type of line where this line starts, used for LHC, see VacType class
	int					startVacType;

	// Name of survey partition where this line ends, NULL if this line does not end at another line
	char				*endLine;

	// Coordinate of this line end in another line
	float				endCoord;

	// How this line ends in another line, see BeamLineConnType class
	int					endType;

	// Vacuum type of line where this line finishes, used for LHC, see VacType class
	int					endVacType;

	// Parts forming this beam line
	QList<BeamLinePart *>	*pParts;

	// The angle this line shall be turned
	float				turnAngle;

	// The resize coefficicent to be applied to this line
	float				resizeCoef;

	// Minimum X coordinate of all equipment
	float				minX;

	// Minimum Y coordinate of all equipment
	float				minY;

	// Maximum X coordinate of all equipment
	float				maxX;

	// Maximum Y coordinate of all equipment
	float				maxY;

	// There is at least one case in LHC where the same 'beam line' (LHCLAYOUT)
	// is a part of two different lines: LHC ring and DSL. DSL shall take all
	// equipment .D, while LHC ring shall take all other equipment. In order to
	// solve this problem - the following attribute of beam line has been introduced
	// + possibility to specify vacuum type in config file
	int					allowedVacType;

	// Manually specified vertical position for beam lines where it is
	// difficult to calculate automatically vertical position of beam line
	// on synoptic or sector view
	int					verticalPos;

	// Flag indicating if line shall be hidden
	bool				hidden;

	// Flag indicating if line is a virtual line - such line is always hidden
	bool				virtualLine;

	// Flag indicating if there is at least one sector border within line
	bool				containSectorBorder;

	// Flag indicating if line shall be hidden on main view. This was introduced
	// to hide COLDEX on SPS main view. 26.06.2012
	bool				hideOnMainView;

	// [VACCO-527] Flag to define if start and end tags should be automatically added to the synoptics when possible
	bool				autoTag;

	// [VACCO-527] Start of line text TAG, for START_TAG functionality - For synoptic display
	QString				tagStartManual;

	// [VACCO-527] END of line text TAG, for END_TAG functionality - For synoptic display
	QString				tagEndManual;

	// Implementation of CFileParser abstract method
	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);

	bool refForTransform(float &refX, float &refY);
	void turn(float centerX, float centerY, float delta);
	void turnPoint(float centerX, float centerY, float delta, Eqp *pEqp);
	void resize(float centerX, float centerY, float delta);
	bool findEqpRangeOnCircle(Sector *pStartSector, Sector *pEndSector,
		BeamLinePart **ppStartPart, BeamLinePart **ppEndPart,
		int &startEqpIdx, int &endEqpIdx);
	bool findEqpRangeOnStraight(Sector *pStartSector, Sector *pEndSector,
		BeamLinePart **ppStartPart, BeamLinePart **ppEndPart,
		int &startEqpIdx, int &endEqpIdx);
	int findPartIdx(BeamLinePart *pLinePart);

private:
	bool				turning;
	bool				resizing;

	bool addPassiveEqp(Eqp *pEqp, bool pureSurvey);
	bool addActiveEqp(Eqp *pEqp);
	void resizePoint(float centerX, float centerY, float delta, float &coordX, float &coordY);
	void moveChildLines(const char *childName, float deltaX, float deltaY);
	void buildOrderedEqpOnPart(BeamLinePart &part);
	bool coordBeforeFirst(BeamLinePart *pPart, Eqp *pFirstEqp, float coord, float &resultX, float &resultY);
	bool coordAfterLast(BeamLinePart *pPart, Eqp *pPrevEqp, float coord, float &resultX, float &resultY);
	Eqp *prevEqp(BeamLinePart *pPart, Eqp *pTargetEqp);
	Eqp *nextEqp(BeamLinePart *pPart, Eqp *pTargetEqp);


	bool nextFixedEqp(int &partIdx, int &eqpIdx);
	void processAreaBetweenFixedPoints(int minLength, int startFixedPartIdx, int startFixedEqpIdx, int endFixedPartIdx, int endFixedEqpIdx);
	void processAreaFromStart(int minLength, int startFixedPartIdx, int startFixedEqpIdx, int endFixedPartIdx, int endFixedEqpIdx);
	void makeSectorShorterForward(int partIdx, int eqpIdx, int endFixedPartIdx, int endFixedEqpIdx, float delta, int minLength);
	void processAreaFromCenter(int minLength, int startFixedPartIdx, int startFixedEqpIdx, int endFixedPartIdx, int endFixedEqpIdx);
	void processAreaFromEnd(int minLength, int startFixedPartIdx, int startFixedEqpIdx, int endFixedPartIdx, int endFixedEqpIdx);
	void makeSectorShorterBackward(int partIdx, int eqpIdx, int startFixedPartIdx, int startFixedEqpIdx, float delta, int minLength);
	float totalSectorsToExtend(int minLength, int startFixedPartIdx, int startFixedEqpIdx, int endFixedPartIdx, int endFixedEqpIdx, float &totalHoles);
	float totalHolesInArea(int startPartIdx, int startEqpIdx, int endPartIdx, int endEqpIdx);
	float moveEqpForward(int partIdx, int eqpIdx, int endPartIdx, int endEqpIdx, float delta);
	float moveEqpForwardActive(int partIdx, int eqpIdx, int endPartIdx, int endEqpIdx, float delta);
	float moveEqpForwardPassive(int partIdx, int eqpIdx, int endPartIdx, int endEqpIdx, float delta);
	float moveEqpBackward(int partIdx, int eqpIdx, int startPartIdx, int startEqpIdx, float delta);
	float moveEqpBackwardActive(int partIdx, int eqpIdx, int startPartIdx, int startEqpIdx, float delta);
	float moveEqpBackwardPassive(int partIdx, int eqpIdx, int startPartIdx, int startEqpIdx, float delta);
	bool previousDeviceIndices(int &partIdx, int &eqpIdx, bool turnAround);
	bool nextDeviceIndices(int &partIdx, int &eqpIdx, bool turnAround);


	Eqp *findNextValve(int startPartIdx, int startEqpIdx, int &partIdx, int &eqpIdx);
	Eqp *findPreviousValve(int startPartIdx, int startEqpIdx, int &partIdx, int &eqpIdx);
};

#endif	// BEAMLINE_H

