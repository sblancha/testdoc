#
# Project file for qmake utility to build MainViewSps EWO
#
#	14.12.2008	L.Kopylov
#		Initial version
#
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = MainViewSps

QT += printsupport

INCLUDEPATH += ../../common/VacCtlUtil \
	../../common/Platform \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/ResourcePool \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/VacCtlEqpData/Eqp \
	../VacCtlEwoUtil

HEADERS =	MainSpsImage.h \
	MainSpsImageEqpConnect.h \
	MainSps.h \
	MainViewSpsEwo.h

SOURCES = MainSpsImage.cpp \
	MainSpsImageEqpConnect.cpp \
	MainSps.cpp \
	MainViewSpsEwo.cpp

LIBS += -L../../../bin \
	-lVacCtlEwoUtil \
	-lVacCtlHistoryData \
	-lVacCtlEqpData \
	-lVacCtlUtil

