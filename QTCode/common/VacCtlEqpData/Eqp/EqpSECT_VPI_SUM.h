#ifndef	EQPSECT_VPI_SUM_H
#define	EQPSECT_VPI_SUM_H

// Summary of VPIs in one vacuum sector - there is corresponding DP in PVSS

#include "Eqp.h"

class Sector;

class VACCTLEQPDATA_EXPORT EqpSECT_VPI_SUM : public Eqp
{
	Q_OBJECT

public:
	EqpSECT_VPI_SUM(Sector *pSector);
#ifdef Q_OS_WIN
	EqpSECT_VPI_SUM(const EqpSECT_VPI_SUM &src) : Eqp(src) {}
#endif
	~EqpSECT_VPI_SUM();

	static QString getDpNameForSector(Sector *pSector);

	// Override methods of Eqp
	virtual void getStateDpes(QStringList &stateDpes);

	virtual bool getPlcList(QList<Eqp *> &list);

	virtual inline Sector *getSectorBefore(void) const { return pSector; }
	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString & string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
#endif

	// Access for essential values
	inline int getRRC(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rrcReplay : rrcOnline; }
	virtual bool isPlcAlarm(DataEnum::DataMode mode) const;

public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Pointer to sector
	Sector			*pSector;

	// VPIs in one sector can be controlled by more than one PLC (((
	QList<Eqp *>	*pPlcs;

	// Last knwon value of RRC DPE in online mode
	unsigned		rrcOnline;

	// Last known value of RRC DPE in replay mode
	unsigned		rrcReplay;

	// Number of VPIs in this sector
	int				nVpis;

	// Number of VRPIs controlling VPIs in this sector
	int				nVrpis;

	// Flag indicating if search of PLC was performed
	bool			plcSearchDone;

	void findAllPlcs(void);
};

#endif	// EQPSECT_VPI_SUM_H
