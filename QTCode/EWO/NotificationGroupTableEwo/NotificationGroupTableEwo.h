#ifndef NOTIFICATIONGROUPTABLEEWO_H
#define	NOTIFICATIONGROUPTABLEEWO_H

// EWO interface for GroupTable widget

#include <BaseExternWidget.hxx>

#include "GroupTable.h"

class EWO_EXPORT NotificationGroupTableEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	NotificationGroupTableEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void selectionChange(const QItemSelection &selected, const QItemSelection &deselected);
	void connectRequest(void);
	void doubleClick(const QModelIndex &index);

private:
	GroupTable	*baseWidget;

	QVariant invokeAddGroup(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeDeleteGroup(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetGroupOwner(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetGroupName(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetGroupId(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetGroupType(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetGroupScopes(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetGroupRecipients(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetGroupFlags(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetGroupNumMsg(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeSetGroupNumEvents(const QString &name, QList<QVariant> &values, QString &error);
	QVariant invokeGetGroupAtRow(const QString &name, QList<QVariant> &values, QString &error);

};

#endif	// NOTIFICATIONGROUPTABLEEWO_H
