//	Implementation of DpeConnection class
/////////////////////////////////////////////////////////////////////////////////

#include "DpeConnection.h"

QVariant DpeConnection::dummy;

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

DpeConnection::DpeConnection(const char *dpeName)
{
	name = dpeName;
	online = replay = polling = 0;
}

/*
**	FUNCTION
**		Get connection count for given mode
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Connection count for given mode
**
**	CAUTIONS
**		None
*/
int DpeConnection::getCount(DataEnum::DataMode mode) const
{
	int result = 0;
	switch(mode)
	{
	case DataEnum::Online:
		result = online;
		break;
	case DataEnum::Replay:
		result = replay;
		break;
	case DataEnum::Polling:
		result = polling;
		break;
	}
	return result;
}

/*
**	FUNCTION
**		Increment connection count for given mode
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeConnection::plusCount(DataEnum::DataMode mode)
{
	switch(mode)
	{
	case DataEnum::Online:
		online++;
		break;
	case DataEnum::Replay:
		replay++;
		break;
	case DataEnum::Polling:
		polling++;
		break;
	}
}

/*
**	FUNCTION
**		Decrement connection count for given mode
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeConnection::minusCount(DataEnum::DataMode mode)
{
	switch(mode)
	{
	case DataEnum::Online:
		if(online > 0)
		{
			online--;
		}
		break;
	case DataEnum::Replay:
		if(replay > 0)
		{
			replay--;
		}
		break;
	case DataEnum::Polling:
		if(polling > 0)
		{
			polling--;
		}
		break;
	}
}

/*
**	FUNCTION
**		Return last known value for given mode
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Reference to last known value
**
**	CAUTIONS
**		None
*/
const QVariant &DpeConnection::getValue(DataEnum::DataMode mode, QDateTime &timeStamp) const
{
	switch(mode)
	{
	case DataEnum::Online:
		timeStamp = onlineTimeStamp;
		return onlineValue;
	case DataEnum::Replay:
		timeStamp = replayTimeStamp;
		return replayValue;
	case DataEnum::Polling:
		timeStamp = pollingTimeStamp;
		return pollingValue;
	}
	return dummy;
}

/*
**		Set last known value for given mode
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DpeConnection::setValue(DataEnum::DataMode mode, const QVariant &value, const QDateTime &timeStamp)
{
	switch(mode)
	{
	case DataEnum::Online:
		onlineValue = value;
		onlineTimeStamp = timeStamp;
		break;
	case DataEnum::Replay:
		replayTimeStamp = timeStamp;
		replayValue = value;
		break;
	case DataEnum::Polling:
		pollingTimeStamp = timeStamp;
		pollingValue = value;
		break;
	}
}

