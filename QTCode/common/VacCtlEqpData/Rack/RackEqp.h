#ifndef RACKEQP_H
#define RACKEQP_H
#include "VacCtlEqpDataExport.h"
#include "qmap.h"

class Eqp;

class VACCTLEQPDATA_EXPORT RackEqp {

public:
	RackEqp();
	RackEqp(QString nameEquip, int positionX, int positionY, float sizeX, float sizeY, QString eqpType);
	~RackEqp();

	inline void setPositionX(int positionX){ this->positionX = positionX; };
	inline void setPositionY(int positionY){ this->positionY = positionY; };
	inline void setSizeX(float sizeX){ this->sizeX = sizeX; };
	inline void setSizeY(float sizeY){ this->sizeY = sizeY; };
	inline void setName(QString name){ this->name = name; };
	inline void setType(QString type){ this->type = type; };
	void addAttribute(QString attributeName, QString attributeValue);
	inline void setEqpPointer(Eqp * pEqp){ this->pEqp = pEqp; };

	inline int getPositionX(){ return positionX; };
	inline int getPositionY(){ return positionY; };
	inline float getSizeX(){ return sizeX; };
	inline float getSizeY(){ return sizeY; };
	inline QString getName(){ return name; };
	inline QString getType() { return type; };
	inline Eqp * getEqpPointer(){ return pEqp; };
	QString getAttributeValue(QString attributeName);
	QMap<QString, QString> &getAttributeList();

	bool isMinimumInformationSet();
private:
	int positionX;
	int positionY;
	float sizeX;
	float sizeY;

	QString name;
	QString type;
	//pointer to Eqp class for connecting dp elements to rack eqp if needed
	Eqp * pEqp;
	QMap<QString, QString> attributes;
};

#endif