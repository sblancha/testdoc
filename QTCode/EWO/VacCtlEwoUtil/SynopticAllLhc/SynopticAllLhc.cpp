//	Implementation of SynopticAllLhc class
/////////////////////////////////////////////////////////////////////////////////

#include "SynopticAllLhc.h"
#include "VacMainView.h"

#include <qlayout.h>


SynopticAllLhc	*SynopticAllLhc::onlineDialogs[2] = { NULL, NULL };
SynopticAllLhc	*SynopticAllLhc::replayDialogs[2] = { NULL, NULL };

/*
**	FUNCTION
**		Check if instance of this dialog (which. in fact, consists of 2 dialogs) is open
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		true	- if dialog instance is open
**
**	CAUTIONS
**		The method shall be called from constructor only
*/
bool SynopticAllLhc::alreadyOpen(DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		return replayDialogs[0] || replayDialogs[1];
	}
	return onlineDialogs[0] || onlineDialogs[1];
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

SynopticAllLhc::SynopticAllLhc(DataEnum::DataMode mode) :
	QDialog(NULL /* VacMainView::getInstance() */, Qt::Window)
{
	setAttribute(Qt::WA_DeleteOnClose);
	this->mode = mode;

	pWidget = new SynopticAllLhcWidget(this, 0, mode);

	// What is my number - 1st or 2nd? Decided based on free slot in array of dialogs
	if(mode == DataEnum::Online)
	{
		if(onlineDialogs[0])
		{
			if(onlineDialogs[1])
			{
				deleteLater();
				return;
			}
			myIndex = 1;
		}
		else
		{
			myIndex = 0;
		}
	}
	else
	{
		if(replayDialogs[0])
		{
			if(replayDialogs[1])
			{
				deleteLater();
				return;
			}
			myIndex = 1;
		}
		else
		{
			myIndex = 0;
		}
	}
	setThisDialog(this);
	pWidget->setLhcPartIndex(myIndex);
	setMinimumSize(pWidget->minimumSize());

	if(myIndex)
	{
		setWindowTitle("All LHC Synoptic - part 2");
	}
	else
	{
		setWindowTitle("All LHC Synoptic");
	}

	// Check if 2nd dialog shall be created
	if(getPairDialog())
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		VacMainView::replayDialogOpened(this);
	}
	SynopticAllLhc *otherDialog = new SynopticAllLhc(mode);
	otherDialog->show();
}

SynopticAllLhc::~SynopticAllLhc()
{
	if((mode == DataEnum::Replay) && (!myIndex))
	{
		VacMainView::replayDialogDeleted(this);
	}
	setThisDialog(NULL);
	SynopticAllLhc *pPair = getPairDialog();
	if(pPair)
	{
		pPair->deleteLater();
	}
}

void SynopticAllLhc::resizeEvent(QResizeEvent *e)
{
	QDialog::resizeEvent(e);
	pWidget->resize(width(), height());
}


SynopticAllLhc *SynopticAllLhc::getPairDialog(void)
{
	int otherIndex = myIndex ? 0 : 1;
	if(mode == DataEnum::Replay)
	{
		return replayDialogs[otherIndex];
	}
	return onlineDialogs[otherIndex];
}

void SynopticAllLhc::setThisDialog(SynopticAllLhc *pDialog)
{
	if(mode == DataEnum::Replay)
	{
		replayDialogs[myIndex] = pDialog;
	}
	else
	{
		onlineDialogs[myIndex] = pDialog;
	}
}
