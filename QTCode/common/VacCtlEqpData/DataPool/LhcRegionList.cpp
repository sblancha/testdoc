//		Implementation of LhcRegionList class
//////////////////////////////////////////////////////////////////////////////

#include "LhcRegionList.h"
#include "LhcRegion.h"

int LhcRegionList<LhcRegion>::compareItems(QPtrCollection::Item item1, QPtrCollection::Item item2)
{
	LhcRegion *pReg1 = (LhcRegion *)item1, *pReg2 = (LhcRegion *)item2;

	if(pReg1->getEnd() < pReg2->getEnd())
	{
		return -1;
	}
	else if(pReg1->getEnd() > pReg2->getEnd())
	{
		return 1;
	}
	return 0;
}
