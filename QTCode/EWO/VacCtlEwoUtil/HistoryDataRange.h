#ifndef	HISTORYDATARANGE_H
#define	HISTORYDATARANGE_H

#include "VacCtlEwoUtilExport.h"

// History values for one time range (typically - one day)

#include <QDateTime>

#include <stdio.h>

// Maximum number of online values per range
#define MAX_ONLINE_VALUES	(8192)

// During vertical limits calculation only take into account
// values in the range:
//  TOO_LOW_VALUE < value < TOO_HIGH_VALUE

#define HISTORY_LOW_VALUE	(0.0)
#define	HISTORY_HIGH_VALUE	((float)1.0E30)

#include <QPainter>
#include <QColor>
#include <QVariant>
class TimeAxis;
class VerticalAxis;
class TimeAxisWidget;
class VerticalAxisWidget;
class ScreenPoint;

class HistoryProcessor;

class Eqp;

class VACCTLEWOUTIL_EXPORT HistoryDataRange
{
public:
	enum FilterType
	{
		FilterTypeMinMax = 0,
		FilterTypeMin = 1
	};

	HistoryDataRange(void);
	HistoryDataRange(const QDateTime &start, const QDateTime &end);
	~HistoryDataRange();

	bool canAddValue(void) { return valuesList.count() < MAX_ONLINE_VALUES; }

	void addOnlineValue(float value, const QDateTime &timeStamp);
	void addValue(int requestId, const QDateTime &time, float value);

	bool checkForDuplicates(HistoryDataRange *pNext);

	int findLimits(float &min, float &max);
	bool draw(QPainter &painter, TimeAxis *pTimeAxis, VerticalAxis *pValueAxis,
		const QColor &mainColor, int lineWidth, bool isNegativeAllowed, ScreenPoint &sp);
	bool draw(QPainter &painter, TimeAxisWidget *pTimeAxis, VerticalAxisWidget *pValueAxis,
		const QColor &mainColor, int lineWidth, bool isNegativeAllowed, ScreenPoint &sp);
	bool getValueAt(const QDateTime &sec, float &value);

	void addExportValues(Eqp *pEqp, const QString &bitDpe, int bitNbr, const QString &bitName, const QDateTime &startTime, const QDateTime &endTime, HistoryProcessor &processor);
	void setLastAsPreviousExportValue(Eqp *pEqp, const QString &bitDpe, int bitNbr, const QString &bitName, const QDateTime &startTime, HistoryProcessor &processor);
	void addExportValues(Eqp *pEqp, const QString &dpe, const QDateTime &startTime, const QDateTime &endTime, HistoryProcessor &processor);
	void setLastAsPreviousExportValue(Eqp *pEqp, const QString &dpe, const QDateTime &startTime, HistoryProcessor &processor);

	// Access
	qint64 getStart(void) const { return start; }
	qint64 getEnd(void) const { return end; }
	int getId(void) const { return id; }
	inline bool isRequestSent(void) const { return requestSent; }
	void setRequestSent(bool flag);
	inline FilterType getFilterType(void) const { return filterType; }
	void setFilterType(FilterType newType);

	void dump(FILE *pFile);

protected:

	// All ranges must have unique IDs
	static int lastRangeId;

	// Minimum timestamp in this range
	quint64	start;

	// Maximum timestamp in this range
	quint64	end;

	// Last 'values per pixel' value used for filtering
	float		filterValuesPerPixel;

	// List of values
	QList<float>		valuesList;

	// List of timestamps. ATTENTION: it number of items in this list and in list 'valuesList' must be the same
	QList<qint64>	timesList;


	// 'Filtered' list of values
	QList<float>	filteredValuesList;

	// 'Filtered' list of times. ATTENTION: it number of items in this list and in array 'filteredValuesList' must be the same
	QList<qint64>	filteredTimesList;


	// Unique identifier of this range
	int 		id;

	// Type of filtering used
	FilterType	filterType;

	// Flag indicating if request for history in this range has been sent
	bool		requestSent;

	// Flag indicating if new value has been added, that is - filtered values must be recalculated
	bool		valueAdded;

	void chooseDataToDraw(TimeAxis *pTimeAxis, QList<float> **ppUseValuesList, QList<qint64> **ppUseTimesList);
	void chooseDataToDraw(TimeAxisWidget *pTimeAxis, QList<float> **ppUseValuesList, QList<qint64> **ppUseTimesList);
	void choseDataToDraw(float scaleMsecPerPixel, QList<float> **ppUseValuesList, QList<qint64> **ppUseTimesList);
	void addFiltered(qint64 startTime, qint64 endTime, float minValue, float maxValue, float lastValue);
	void addFilteredMinMax(qint64 startTime, qint64 endTime, float minValue, float maxValue, float lastValue);
	void addFilteredMin(qint64 startTime, qint64 endTime, float minValue);
};

#endif	// HISTORYDATARANGE_H
