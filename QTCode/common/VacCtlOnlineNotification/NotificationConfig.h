#ifndef	NOTIFICATIONCONFIG_H
#define	NOTIFICATIONCONFIG_H

#include "VacCtlOnlineNotificationExport.h"

// Class holding working configuration for one notification: criteria,
// all devices etc.

#include "ComplexCriteria.h"
#include "EqpMsgCriteria.h"
#include "EqpMessage.h"
#include "MessageFilteringRule.h"

#include "EqpUsageFilter.h"

#include <qobject.h>
#include <qstring.h>
#include <qlist.h>
#include <qstringlist.h>
#include <qstringlist.h>


class EqpState;
class Eqp;

class VACCTLONLINENOTIFICATION_EXPORT NotificationConfig : public QObject
{
	Q_OBJECT

public:

	// Type of items in scope list
	enum
	{
		SectorScope = 1,
		MainPartScope = 2,
		ArcScope = 3,
		DeviceScope = 4,
		GlobalScope = 5	// Devices not related to sectors
	};

	static NotificationConfig *findConfig(const char *name);
	static void deleteConfig(const char *name);
	static void checkMsgGroupExpire(void);

	static const char *scopeTypeToString(int type);
	static int scopeTypeParse(const char *string, QString &errMsg);

	virtual ~NotificationConfig();

	void setCriteria(ComplexCriteria *pCriteria);
	void setScope(int type, QStringList &nameList);
	inline void clearUsageFilter(void) { usageFilter.clear(); }
	inline void addUsageFilterItem(int funcTypeId, QByteArray attrName, bool reverse)
	{
		usageFilter.addFilterItem(funcTypeId, attrName, reverse);
	}
	inline void finishUsageFilter(void) { setScope(); }
	void setMessageRule(const char *string, QString &errMsg);
	

	void activate(QString &errMsg);
	void deactivate(void);
	void setInitialized(void);

	// Access
	inline const QString &getName(void) const { return name; }
	inline MessageFilteringRule &getMsgFilter(void) { return msgFilter; }
	inline bool isActivated(void) const { return activated; }
	inline bool isInitialized(void) const { return initialized; }
	

private slots:
	void stateChange(EqpState *pSrc, bool match);

protected:
	NotificationConfig(const char *name);

	// List of all notification configurations
	static QList<NotificationConfig *> *pInstanceList;

	// Name of this configuration (== DP name in PVSS&)
	QString					name;

	// Criteria used for devices
	ComplexCriteria 		*pCriteria;

	// List of all device states (matches/not mathces criteria)
	QList<EqpState *>	*pEqpList;

	// Rules to build resulting message
	EqpMessage				*pMessageRule;

	// Rule to filter/send/group messages
	MessageFilteringRule	msgFilter;

	// Type of scope that was set
	int						scopeType;

	// Names for scope setting
	QStringList				scopeNameList;

	// Filter on equipment usage
	EqpUsageFilter			usageFilter;

	// Flag indicating if this configuration is activated (i.e. connected to equipment)
	bool					activated;

	// Flag indicating of this configuration is initialized, i.e. initial callback processing is done
	bool					initialized;

	void setScope(void);
	void setScopeSectors(int *typeArray, int nTypes);
	void setScopeMainParts(int *typeArray, int nTypes);
	void setScopeArcs(int *typeArray, int nTypes);
	void setScopeDevices(int *typeArray, int nTypes);
	void setScopeGlobal(int *typeArray, int nTypes);
	void buildEqpStateList(int *typeArray, int nTypes, QList<Eqp *> &mainEqpList);
	void addEqpToList(QList<Eqp *> &list, Eqp *pEqp);
	void addPlcOfDevice(Eqp *pEqp, QList<Eqp *> &eqpList);
	void checkPendingMatch(const QDateTime &now);
};

#endif	// NOTIFICATIONCONFIG_H
