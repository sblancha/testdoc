#ifndef SYNOPTICDIALOG_H
#define SYNOPTICDIALOG_H

// Dialog for displaying synoptic view

#include "DataEnum.h"
#include "VacEqpTypeMask.h"
#include "EqpMsgCriteria.h"

#include <QDialog>
#include <QAction>

class Sector;
class SynView;

#include <QPushButton>
#include <QMenu>
#include <QLabel>
#include <QComboBox>
#include <QScrollArea>

class SynopticDialog : public QDialog
{
	Q_OBJECT

public:
	SynopticDialog(Sector *pFirstSector, Sector *pLastSector, DataEnum::DataMode mode);
	~SynopticDialog();

	static void makeDeviceVisible(Eqp *pEqp);

protected:
	// Last created instance - for makeDeviceVisible() functionality
	static SynopticDialog	*pLastInstance;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Button for displaying 'File' menu
	QPushButton	*pFilePb;

	// Button for displaying 'View' menu
	QPushButton	*pViewPb;

	// Button for displaying 'Help' menu
	QPushButton	*pHelpPb;

	// 'File' menu
	QMenu	*pFileMenu;

	// 'View' menu
	QMenu	*pViewMenu;

	// 'Equipment' submenu of 'View' menu
	QMenu	*pEqpSubMenu;

	// List of actions in pEqpSubMenu
	QList<QAction *>	typeActions;

	// 'Help' menu
	QMenu	*pHelpMenu;

	// Label with synoptic title
	QLabel		*pTitleLabel;

	// Combo box for main part selection
	QComboBox	*pCombo;

	// Scroll view holding synoptic image and icons
	QScrollArea	*pScroll;

	// Synoptic view instance providing real synoptic drawing
	SynView		*pView;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Data to be shown in dialog //////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Equipment type mask for all possible equipment types
	VacEqpTypeMask	allEqpMask;

	// Equipment type mask
	VacEqpTypeMask	eqpMask;

	// Pointer to first sector to be shown
	Sector			*pFirstSector;

	// Pointer to last sector to be shown
	Sector			*pLastSector;

	// Data acquisition mode
	DataEnum::DataMode		mode;

	void buildInitialEqpMask(void);

	void buildLayout(void);
	void buildFileMenu(void);
	void buildViewMenu(void);
	void buildHelpMenu(void);
	void buildMainPartCombo(void);
	void selectInitialMp(void);

	void setTitle(void);
	void buildView(bool init);
	void setMyWidth(bool init);

	void ensureDeviceVisible(Eqp *pEqp);

	void makeDialogRequest(int type);

	virtual void resizeEvent(QResizeEvent *pEvent);

	void ConnectEqpActiveChange(void);

	void ConnectEqpPositionChange(void);

private slots:
	void filePrint(void);

	void eqpTypeMenu(QAction *pAction);
	void viewSector(void);
	void viewProfile(void);
	void help(void);
	void comboActivated(const QString &name);

	void viewMove(int x);

	void eqpActiveChange(Eqp *pE, DataEnum::DataMode mode);
	
	void showMobilesNC(void);
	void hideMobilesNC(void);
};

#endif	// SYNOPTICDIALOG_H
