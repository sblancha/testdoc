//	Implementation of EqpCRYO_TT_SUM class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpCRYO_TT_SUM.h"

#include "DataPool.h"
#include "FunctionalType.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			153,153,255
#define COLOR_GREEN				153,255,153
#define COLOR_YELLOW			255,255,153
#define COLOR_RED				255,153,153

//	Main state bits to be analyzed
#define	RR1_INVALID		(0x1)

// Limits for temperatures
#define	CM_T_WARNING	((float)5)
#define	CM_T_BAD		((float)40)
#define	BS_T_WARNING	((float)5)
#define	BS_T_BAD		((float)40)

#define	max(a,b)	((a) > (b) ? (a) : (b))

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpCRYO_TT_SUM::EqpCRYO_TT_SUM(const Eqp &source) : Eqp(source)
{
	cmRr1Online = cmRr1Replay = bsInRr1Online = bsInRr1Replay = bsOutRr1Online = bsOutRr1Replay = RR1_INVALID;
	cmTOnline = cmTReplay = bsInTOnline = bsInTReplay = bsOutTOnline = bsOutTReplay = 0;
	functionalType = FunctionalType::CRYO_TT_SUM;
}

EqpCRYO_TT_SUM::~EqpCRYO_TT_SUM()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpCRYO_TT_SUM::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "CM.RR1");
	addDpeNameToList(stateDpes, "BSIN.RR1");
	addDpeNameToList(stateDpes, "BSOUT.RR1");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpCRYO_TT_SUM::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "CM.RR1", mode);
	connectDpe(pDst, "CM.T", mode);
	connectDpe(pDst, "BSIN.RR1", mode);
	connectDpe(pDst, "BSIN.T", mode);
	connectDpe(pDst, "BSOUT.RR1", mode);
	connectDpe(pDst, "BSOUT.T", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpCRYO_TT_SUM::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "BSOUT.T", mode);
	connectDpe(pDst, "BSOUT.RR1", mode);
	connectDpe(pDst, "BSIN.T", mode);
	connectDpe(pDst, "BSIN.RR1", mode);
	connectDpe(pDst, "CM.T", mode);
	connectDpe(pDst, "CM.RR1", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpCRYO_TT_SUM::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "CM.RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(cmRr1Online == newValue)
			{
				return;
			}
			cmRr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "CM.T"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(cmTOnline == newValue)
			{
				return;
			}
			cmTOnline = newValue;
		}
	}
	else if(!strcmp(dpe, "BSIN.RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(bsInRr1Online == newValue)
			{
				return;
			}
			bsInRr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "BSIN.T"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(bsInTOnline == newValue)
			{
				return;
			}
			bsInTOnline = newValue;
		}
	}
	else if(!strcmp(dpe, "BSOUT.RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(bsOutRr1Online == newValue)
			{
				return;
			}
			bsOutRr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "BSOUT.T"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(bsOutTOnline == newValue)
			{
				return;
			}
			bsOutTOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "CM.RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(cmRr1Replay == newValue)
			{
				return;
			}
			cmRr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "CM.T"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(cmTReplay == newValue)
			{
				return;
			}
			cmTReplay = newValue;
		}
	}
	else if(!strcmp(dpe, "BSIN.RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(bsInRr1Replay == newValue)
			{
				return;
			}
			bsInRr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "BSIN.T"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(bsInTReplay == newValue)
			{
				return;
			}
			bsInTReplay = newValue;
		}
	}
	else if(!strcmp(dpe, "BSOUT.RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(bsOutRr1Replay == newValue)
			{
				return;
			}
			bsOutRr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "BSOUT.T"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(bsOutTReplay == newValue)
			{
				return;
			}
			bsOutTReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**		In fact this device does NOT have single main color, let's
**		assign 'worth of 2 colors' as main color
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainColor(cmRr1Replay | bsInRr1Replay | bsOutRr1Replay, cmTReplay, max(bsInTReplay, bsOutTReplay), color);
	}
	else
	{
		getMainColor(cmRr1Online | bsInRr1Online | bsOutRr1Online, cmTOnline, max(bsInTOnline, bsOutTOnline), color);
	}
}
#endif

/*
**	FUNCTION
**		Return main value for thermometer - temperature. Again, this device
**		does not have single value. Let's use CM value (TODO???)
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpCRYO_TT_SUM::getMainValue(DataEnum::DataMode mode)
{
	return mode == DataEnum::Replay ? cmTReplay : cmTOnline;
}
#endif

/*
**	FUNCTION
**		Check if main value for thermometer (temperature) is valid or not.
**		Latest known requirement: even invalid value shall be shown,
**		so the value is always considered to be valid
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
bool EqpCRYO_TT_SUM::isMainValueValid(DataEnum::DataMode /* mode */)
{
	return true;
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		cmT			- Value of CM.T DPE
**		bsT			- Maximum of BS temperatures
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getMainColor(unsigned rr1, float cmT, float bsT, QColor &color)
{
	if(rr1 & RR1_INVALID)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(cmT < CM_T_WARNING)
	{
		if(bsT >= BS_T_BAD)
		{
			color.setRgb(COLOR_RED);
		}
		else if(bsT >= BS_T_WARNING)
		{
			color.setRgb(COLOR_YELLOW);
		}
		else
		{
			color.setRgb(COLOR_GREEN);
		}
	}
	else if(cmT < CM_T_BAD)
	{
		if(bsT >= BS_T_BAD)
		{
			color.setRgb(COLOR_RED);
		}
		else
		{
			color.setRgb(COLOR_YELLOW);
		}
	}
	else
	{
		color.setRgb(COLOR_RED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainStateString(cmRr1Replay, bsInRr1Replay | bsOutRr1Replay, cmTReplay, max(bsInTReplay, bsOutTReplay), string);
	}
	else
	{
		getMainStateString(cmRr1Online, bsInRr1Online | bsOutRr1Online, cmTOnline, max(bsInTOnline, bsOutTOnline), string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getMainValueString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainValueString(cmRr1Replay, bsInRr1Replay | bsOutRr1Replay, cmTReplay, max(bsInTReplay, bsOutTReplay), string);
	}
	else
	{
		getMainValueString(cmRr1Online, bsInRr1Online | bsOutRr1Online, cmTOnline, max(bsInTOnline, bsOutTOnline), string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		cmRr1		- Value of CM.RR1 DPE
**		bsRr1		- OR'ed BSIN.RR1 and BSOUT.RR1 DPEs
**		cmT			- Value of CM.T DPE
**		bsT			- Maximum of BS*.T DPEs
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getMainValueString(unsigned cmRr1, unsigned bsRr1, float cmT, float bsT, QString &string)
{
	char buf[32];
	if(cmT > 99.9)
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d", (int)cmT);
#else
		sprintf(buf, "%d", (int)cmT);
#endif
	}
	else if(cmT > 9.99)
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%.1f", cmT);
#else
		sprintf(buf, "%.1f", cmT);
#endif
	}
	else
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%.2f", cmT);
#else
		sprintf(buf, "%.2f", cmT);
#endif
	}
	string = "CM: ";
	string += buf;
	if(cmRr1 & RR1_INVALID)
	{
		string += " (invalid)";
	}

	string += ", BS: ";
	if(bsT > 99.9)
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%d", (int)bsT);
#else
		sprintf(buf, "%d", (int)bsT);
#endif
	}
	else if(bsT > 9.99)
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%.1f", bsT);
#else
		sprintf(buf, "%.1f", bsT);
#endif
	}
	else
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%.2f", bsT);
#else
		sprintf(buf, "%.2f", bsT);
#endif
	}
	string += buf;
	if(bsRr1 & RR1_INVALID)
	{
		string += " (invalid)";
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		t			- Value of T DPE
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getMainStateString(unsigned cmRr1, unsigned bsRr1, float cmT, float bsT, QString &string)
{
	getMainValueString(cmRr1, bsRr1, cmT, bsT, string);
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getMainStateStringForMenu(cmRr1Replay, bsInRr1Replay | bsOutRr1Replay, cmTReplay, max(bsInTReplay, bsOutTReplay), string);
	}
	else
	{
		getMainStateStringForMenu(cmRr1Online, bsInRr1Online | bsOutRr1Online, cmTOnline, max(bsInTOnline, bsOutTOnline), string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		cmRr1		- Value of CM.RR1 DPE
**		bsRr1		- OR'e value of BS*.RR1 DPEs
**		cmT			- Value of CM.T DPE
**		bsT			- Maximum of BS*.T DPEs
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getMainStateStringForMenu(unsigned cmRr1, unsigned bsRr1, float cmT, float bsT, QString &string)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainValueString(cmRr1, bsRr1, cmT, bsT, valueString);
	string += valueString;
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	if(mode == DataEnum::Replay)
	{
		getToolTipString(cmRr1Replay, bsInRr1Replay | bsOutRr1Replay, cmTReplay, max(bsInTReplay, bsOutTReplay), string);
	}
	else
	{
		getToolTipString(cmRr1Online, bsInRr1Online | bsOutRr1Online, cmTOnline, max(bsInTOnline, bsOutTOnline), string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		cmRr1		- Value of CM.RR1 DPE
**		bsRr1		- OR'e value of BS*.RR1 DPEs
**		cmT			- Value of CM.T DPE
**		bsT			- Maximum of BS*.T DPEs
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getToolTipString(unsigned cmRr1, unsigned bsRr1, float cmT, float bsT, QString &string)
{
	getMainStateStringForMenu(cmRr1, bsRr1, cmT, bsT, string);
}
#endif


/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	addDpeNameToList(dpes, "CM.RR1");
	addDpeNameToList(dpes, "CM.T");
	addDpeNameToList(dpes, "BSIN.RR1");
	addDpeNameToList(dpes, "BSIN.T");
	addDpeNameToList(dpes, "BSIN.T");
	addDpeNameToList(dpes, "BSOUT.RR1");
	addDpeNameToList(dpes, "BSOUT.T");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history)
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpCRYO_TT_SUM::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();
	unsigned cmRr1 = mode == DataEnum::Replay ? cmRr1Replay : cmRr1Online;
	unsigned bsInRr1 = mode == DataEnum::Replay ? bsInRr1Replay : bsInRr1Online;
	unsigned bsOutRr1 = mode == DataEnum::Replay ? bsOutRr1Replay : bsOutRr1Online;

	// Check if state is valid
	if(cmRr1 & bsInRr1 & bsOutRr1 & RR1_INVALID)
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Check temperature if needed
	if(criteria.isUseTemperatureLimit())
	{
		float t = mode == DataEnum::Replay ? cmTReplay : cmTOnline;
		if(t > criteria.getTemperatureLimit())
		{
			return 1;
		}
		t = mode == DataEnum::Replay ? bsInTReplay : bsInTOnline;
		if(bsInTOnline > criteria.getTemperatureLimit())
		{
			return 1;
		}
		t = mode == DataEnum::Replay ? bsOutTReplay : bsOutTOnline;
		if(bsOutTOnline > criteria.getTemperatureLimit())
		{
			return 1;
		}
	}

	// Last chance - criteria on temperature grow
	if(criteria.isUseTemperatureGrowLimit())
	{
		// Can not decide right now, more info is needed
		return 2;
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Return parameters for history query, required for device list.
**		Pnly one DPE is accepted - let's use CM.T (TODO ???)
**
**	ARGUMENTS
**		dpe		- [out] DPE, for which history is required
**		start	- [out] start time of history query
**		end		- [out] end time of history query
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpCRYO_TT_SUM::getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUseTemperatureGrowLimit())
	{
		dpe = "";
		return;
	}
	dpe = dpName;
	dpe += ".CM.T";
	start = criteria.getTemperatureStartTime();
	if (criteria.isTemperatureEndTimeNow())
	{
		end = QDateTime::currentDateTime();
	}
	else
	{
		end = criteria.getTemperatureEndTime();
	}
}
#endif

/*
**	FUNCTION
**		Return grow factor for main value (pressure) to check if
**		device matches device list criteria
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Pressure grow factor; or
**		0 if criteria does not contain pressure grow
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpCRYO_TT_SUM::getGrowFactorForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUseTemperatureGrowLimit())
	{
		return 0.0;
	}
	return criteria.getTemperatureGrowLimit();
}

int EqpCRYO_TT_SUM::getSpikeFilterForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if (!criteria.isUseTemperatureGrowLimit())
	{
		return 0;
	}
	return criteria.getTemperatureSpikeInterval();
}

#endif

