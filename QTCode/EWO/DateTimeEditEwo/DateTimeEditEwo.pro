# Project file for qmake utility to build DateTimeEdiEwo EWO
#
#	09.01.2009	L.Kopylov
#		Initial version
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = DateTimeEditEwo

HEADERS = DateTimeEditEwo.h

SOURCES = DateTimeEditEwo.cpp

