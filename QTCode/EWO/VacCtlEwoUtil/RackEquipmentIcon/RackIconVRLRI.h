#ifndef RACKICONVRLRI_H
#define RACKICONVRLRI_H

#include "RackIcon.h"

class Eqp;

class VACCTLEWOUTIL_EXPORT RackIconVRLRI : public RackIcon
{
	Q_OBJECT
public:
	RackIconVRLRI(QWidget * parent, Qt::WindowFlags f = 0);
	virtual ~RackIconVRLRI();

	virtual void setEqp(Eqp * pEqp);
	virtual bool connect(void);
	virtual bool disconnect(void);
protected:
	Eqp * pEqp; //pointer to equipment, if it exists
	virtual void paintEvent(QPaintEvent *pEvent);

};

#endif
