#pragma once

// Data for valve's interlock source, that was not immediately found in synoptic line - it
// can be located in another line of the same synoptic.
// Interlock source can be either gauge of VRPI

#include <QString>

class Eqp;

class ValveInterlockSource
{
public:
	ValveInterlockSource(Eqp *pSourceEqp, bool isPreviousInterlock);
	ValveInterlockSource(const QString &dpName, Eqp *pMasterEqp, bool isPreviousInterlock);
	~ValveInterlockSource(void);

	void getData(bool &before, Eqp **ppEqp, QString &dpName);

protected:
	Eqp		*pEqp;		// Pointer to device used as interlock source
	QString	dpName;		// DP name of interlock source
	bool	previous;	// True if device is interlock before valve
};
