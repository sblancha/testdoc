#ifndef EQPMP_ACCESS_H
#define	EQPMP_ACCESS_H

// Main part access control device - does NOT come from DB

#include "Eqp.h"

#include "MainPart.h"

class VACCTLEQPDATA_EXPORT EqpMP_ACCESS : public Eqp
{
	Q_OBJECT

public:
	EqpMP_ACCESS(MainPart *pMainPart);
#ifdef Q_OS_WIN
	EqpMP_ACCESS(const EqpMP_ACCESS &src) : Eqp(src) {}
#endif
	~EqpMP_ACCESS();

	// Override methods of Eqp
	virtual void getStateDpes(QStringList &stateDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	virtual void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString & string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
#endif

	// Access for essential values
	inline int getMode(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? modeReplay : modeOnline; }

protected:
	// Last knwon value of MODE DPE in online mode
	int			modeOnline;

	// Last known value of MODE DPE in replay mode
	int			modeReplay;
};

#endif	// EQPMP_ACCESS_H
