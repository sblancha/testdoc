
// bitmask for thermometer state. Only one bit is used for the moment
const unsigned	VCRYO_TT_R_INVALID = 0x1u;

// String representing invalid temperature value
const string VCRYO_TT_INVALID = "invalid";


//LhcVacActionPossible_VCRYO_TT:
/** Check if thermometer is in state that allows executing actions for it
		Action are NOT possible

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, action to be executed
	@param possible: out, result (allowed/not allowed) is returned here.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacActionPossible_VCRYO_TT(string dpName, string dpType, dyn_anytype act, bool &possible,
  dyn_string &exceptionInfo)
{
  possible = false;
}
//LhcVacDeviceAction_VCRYO_TT:
/** Execute action for thermometer. No actions are possible

Usage: LHC Vacuum control internal

PVSS manager usage: VISION, CTRL

	@param dpName: in, DP name
	@param dpType: in, DP type
	@param act: in, enum for action to be executed.
	@param exceptionInfo: out, details of any exceptions are returned here
*/
void LhcVacDeviceAction_VCRYO_TT(string dpName, string dpType, dyn_anytype act, 
   dyn_string &exceptionInfo)
{
  fwException_raise(exceptionInfo, 
    "ERROR", "LhcVacDeviceAction_VCRYO_TT: no actions for thermometer", "" );
}
