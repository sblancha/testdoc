#ifndef	SECTORVIEWLHCDIALOG_H
#define	SECTORVIEWLHCDIALOG_H

// Dialog for displaying LHC sector view

#include "DataEnum.h"
#include "VacEqpTypeMask.h"
#include "LhcRingSelection.h"
#include "EqpMsgCriteria.h"

#include <QDialog>

class Sector;
class SectLhcView;

#include <QPushButton>
#include <QMenu>
#include <QLabel>
#include <QComboBox>
#include <QScrollArea>

class SectorViewLhcDialog : public QDialog, public LhcRingSelection
{
	Q_OBJECT

public:
	SectorViewLhcDialog(Sector *pFirstSector, Sector *pLastSector, DataEnum::DataMode mode);
	~SectorViewLhcDialog();

protected:
	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Button for displaying 'File' menu
	QPushButton	*pFilePb;

	// Button for displaying 'View' menu
	QPushButton	*pViewPb;

	// Button for displaying 'Help' menu
	QPushButton	*pHelpPb;

	// 'File' menu
	QMenu	*pFileMenu;

	// 'View' menu
	QMenu	*pViewMenu;

	// 'Help' menu
	QMenu	*pHelpMenu;

	// Label with synoptic title
	QLabel		*pTitleLabel;

	// Combo box for main part selection
	QComboBox	*pCombo;

	// Scroll view holding synoptic image and icons
	QScrollArea	*pScroll;

	// Synoptic view instance providing real synoptic drawing
	SectLhcView	*pView;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Data to be shown in dialog //////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Pointer to first sector to be shown
	Sector			*pFirstSector;

	// Pointer to last sector to be shown
	Sector			*pLastSector;

	// Data acquisition mode
	DataEnum::DataMode		mode;

	void buildLayout(void);
	void buildFileMenu(void);
	void buildViewMenu(void);
	void buildHelpMenu(void);
	void buildMainPartCombo(void);
	void selectInitialMp(void);

	void setTitle(void);
	void buildView(bool init);
	void setMyWidth(bool init);

	void makeDialogRequest(int type);

	virtual void resizeEvent(QResizeEvent *pEvent);

private slots:
	void filePrint(void);

	void viewSynoptic(void);
	void viewProfile(void);
	void help(void);
	void comboActivated(const QString &name);

	void viewMove(int x);
};

#endif	// SECTORVIEWLHCDIALOG_H
