#ifndef RACKICONVRPIT_H
#define RACKICONVRPIT_H

#include "RackIcon.h"

class Eqp;

class VACCTLEWOUTIL_EXPORT RackIconVRPIT : public RackIcon
{
	Q_OBJECT
public:
	RackIconVRPIT(QWidget * parent, Qt::WindowFlags f = 0);
	virtual ~RackIconVRPIT();

	virtual void setEqp(Eqp * pEqp);
	virtual bool connect(void);
	virtual bool connectSlave(void);
	virtual bool disconnect(void);
protected:
	VacIconConnection	slaveConnection;
	Eqp * pEqp; //pointer to equipment, if it exists
	Eqp * pSlaveEqp;
	virtual void paintEvent(QPaintEvent *pEvent);
	bool slaveConnected;
};

#endif
