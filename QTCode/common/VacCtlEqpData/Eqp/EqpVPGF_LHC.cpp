//	Implementation of EqpVPGF_LHC class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVPGF_LHC.h"

#include "DataPool.h"
#include "FunctionalTypeVPGF.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define	COLOR_ERROR_VALVE		255,102,102
#define COLOR_ERROR_DEVICE		255,0,0
#define	COLOR_WARNING			255,255,0
#define	COLOR_TMP_ACCELERATE	255,255,0
#define	COLOR_OPEN				0,255,0
#define	COLOR_CLOSED			255,0,0
#define COLOR_UNDERRANGE		0,180,0

//	Main state bits to be analyzed

// RR1

#define	RR1_VALID				(0x40000000) 
#define RR1_TMP_NOMINAL			(0x20000000) 
#define RR1_LOCAL				(0x10000000) 
#define RR1_TMP_ACCELERATING	(0x08000000) 
#define RR1_MANUAL				(0x04000000) 
#define RR1_ON					(0x02000000) 
#define RR1_OFF					(0x01000000) 
#define RR1_ERROR				(0x00800000) 
#define RR1_WARNING				(0x00400000) 
#define RR1_TMP_ON				(0x00200000) 
#define RR1_VPP_THERM_PROT_ST	(0x00100000)
#define RR1_VPP_3_PHASE_ST		(0x00080000)
#define RR1_VPP_CABLE_ST		(0x00040000)
#define RR1_VPP_PWR_MODE		(0x00020000)
#define RR1_VPP_ON				(0x00010000)
#define RR1_VVD_CLOSED			(0x00008000)
#define RR1_VVD_OPEN			(0x00004000)
#define RR1_VVP_CLOSED			(0x00002000)
#define RR1_VVP_OPEN			(0x00001000)
#define RR1_VVI_CLOSED			(0x00000800)
#define RR1_VVI_OPEN			(0x00000400)
#define RR1_VVT_CLOSED			(0x00000200)
#define RR1_VVT_OPEN			(0x00000100)
#define RR1_VVR2_INTERLOCKED	(0x00000080)
#define RR1_VVR2_FORCED			(0x00000040)
#define RR1_VVR2_CLOSED			(0x00000020)
#define RR1_VVR2_OPEN			(0x00000010)
#define RR1_VVR1_INTERLOCKED	(0x00000008)
#define RR1_VVR1_FORCED			(0x00000004)
#define RR1_VVR1_CLOSED			(0x00000002)
#define RR1_VVR1_OPEN			(0x00000001)

//RR2
#define RR2_WARNING_CODE		(0xFF000000)
#define RR2_ERROR_CODE			(0x00FF0000)
#define RR2_VVD_ERROR			(0x00008000)
#define RR2_VVP_ERROR			(0x00004000)
#define	RR2_VVI_ERROR			(0x00002000)
#define RR2_VVT_ERROR			(0x00001000)
#define RR2_VVR2_ERROR			(0x00000800)
#define RR2_VVR1_ERROR			(0x00000400)
#define RR2_VPP_ERROR			(0x00000200)
#define RR2_VPT_ERROR			(0x00000100)
#define RR2_VVD_WARNING			(0x00000080)
#define RR2_VVP_WARNING			(0x00000040)
#define RR2_VVI_WARNING			(0x00000020)
#define RR2_VVT_WARNING			(0x00000010)
#define RR2_VVR2_WARNING		(0x00000008)
#define RR2_VVR1_WARNING		(0x00000004)
#define RR2_VPP_WARNING			(0x00000002)
#define RR2_VPT_WARNING			(0x00000001)

//RR3
#define RR3_VPG_MODE			(0x0000F000)
#define RR3_VG2_ON				(0x00000800)
#define RR3_VG1_ON				(0x00000400)
#define RR3_VG2_ERROR			(0x00000200)
#define RR3_VG1_ERROR			(0x00000100)
#define RR3_PRESSURE_SWITCH_ST 	(0x00000080)
#define RR3_VG2_OR				(0x00000008)
#define RR3_VG2_UR				(0x00000004)
#define RR3_VG1_OR				(0x00000002)
#define RR3_VG1_UR				(0x00000001)


/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

EqpVPGF_LHC::EqpVPGF_LHC(const Eqp &source) : Eqp(source)
{
	rr1Online = 0;
	rr2Online = 0;
	rr3Online = 0;
	activeStepOnline = 0;
	turboPumpSpeedOnline = 0;
	turboPumpCurrentOnline = 0;
	primaryPumpCurrentOnline = 0;
	VG1PhysicalValueOnline = 0;
	VG2PhysicalValueOnline = 0;
	VPGTypeValueOnline = 0;
	VPGStatusValueOnline = 0;
	VPGResponseCodeValueOnline = 0;
	VPGResponseValueValueOnline = 0;
	plcAlarmOnline = 0;
	rr1Replay = 0;
	rr2Replay = 0;
	rr3Replay = 0;
	activeStepReplay = 0;
	turboPumpSpeedReplay = 0;
	turboPumpCurrentReplay = 0;
	primaryPumpCurrentReplay = 0;
	VG1PhysicalValueReplay = 0;
	VG2PhysicalValueReplay = 0;
	VPGTypeValueReplay = 0;
	VPGStatusValueReplay = 0;
	VPGResponseCodeValueReplay = 0;
	VPGResponseValueValueReplay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::VPGF; 
}

EqpVPGF_LHC::~EqpVPGF_LHC()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPGF_LHC::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "RR2");
	addDpeNameToList(stateDpes, "RR3");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPGF_LHC::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "RR2", mode);
	connectDpe(pDst, "RR3", mode);
	connectDpe(pDst, "ActiveStep", mode);
	connectDpe(pDst, "TurboPumpSpeed", mode);
	connectDpe(pDst, "TurboPumpCurrent", mode);
	connectDpe(pDst, "PrimaryPumpCurrent", mode);
	connectDpe(pDst, "VG1PhysicalValue", mode);
	connectDpe(pDst, "VG2PhysicalValue", mode);
	connectDpe(pDst, "VPGType", mode);
	connectDpe(pDst, "VPGStatus", mode);
	connectDpe(pDst, "ResponseCode", mode);
	connectDpe(pDst, "ResponseValue", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPGF_LHC::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "EqpStatus", mode);
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "RR2", mode);
	disconnectDpe(pDst, "RR3", mode);
	disconnectDpe(pDst, "ActiveStep", mode);
	disconnectDpe(pDst, "TurboPumpSpeed", mode);
	disconnectDpe(pDst, "TurboPumpCurrent", mode);
	disconnectDpe(pDst, "PrimaryPumpCurrent", mode);
	disconnectDpe(pDst, "VG1PhysicalValue", mode);
	disconnectDpe(pDst, "VG2PhysicalValue", mode);
	disconnectDpe(pDst, "VPGType", mode);
	disconnectDpe(pDst, "VPGStatus", mode);
	disconnectDpe(pDst, "ResponseCode", mode);
	disconnectDpe(pDst, "ResponseValue", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPGF_LHC::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if (!strcmp(dpe, "RR1"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if (!strcmp(dpe, "RR2"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (rr2Online == newValue)
			{
				return;
			}
			rr2Online = newValue;
		}
	}
	else if (!strcmp(dpe, "RR3"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (rr3Online == newValue)
			{
				return;
			}
			rr3Online = newValue;
		}
	}
	else if (!strcmp(dpe, "ActiveStep"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (activeStepOnline == newValue)
			{
				return;
			}
			activeStepOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "TurboPumpSpeed"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (turboPumpSpeedOnline == newValue)
			{
				return;
			}
			turboPumpSpeedOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "TurboPumpCurrent"))
	{
		if (value.canConvert(QVariant::Double))
		{
			double newValue = value.toDouble();
			if (turboPumpCurrentOnline == newValue)
			{
				return;
			}
			turboPumpCurrentOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "PrimaryPumpCurrent"))
	{
		if (value.canConvert(QVariant::Double))
		{
			double newValue = value.toDouble();
			if (primaryPumpCurrentOnline == newValue)
			{
				return;
			}
			primaryPumpCurrentOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "VG1PhysicalValue"))
	{
		if (value.canConvert(QVariant::Double))
		{
			double newValue = value.toDouble();
			if (VG1PhysicalValueOnline == newValue)
			{
				return;
			}
			VG1PhysicalValueOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "VG2PhysicalValue"))
	{
		if (value.canConvert(QVariant::Double))
		{
			double newValue = value.toDouble();
			if (VG2PhysicalValueOnline == newValue)
			{
				return;
			}
			VG2PhysicalValueOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "VPGType"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			double newValue = value.toUInt();
			if (VPGTypeValueOnline == newValue)
			{
				return;
			}
			VPGTypeValueOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "VPGStatus"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			double newValue = value.toUInt();
			if (VPGStatusValueOnline == newValue)
			{
				return;
			}
			VPGStatusValueOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "ResponseCode"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			double newValue = value.toUInt();
			if (VPGResponseCodeValueOnline == newValue)
			{
				return;
			}
			VPGResponseCodeValueOnline = newValue;
		}
	}
	else if (!strcmp(dpe, "ResponseValue"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			double newValue = value.toUInt();
			if (VPGResponseValueValueOnline == newValue)
			{
				return;
			}
			VPGResponseValueValueOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if (!strcmp(dpe, "RR1"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if (!strcmp(dpe, "RR2"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (rr2Replay == newValue)
			{
				return;
			}
			rr2Replay = newValue;
		}
	}
	else if (!strcmp(dpe, "RR3"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (rr3Replay == newValue)
			{
				return;
			}
			rr3Replay = newValue;
		}
	}
	else if (!strcmp(dpe, "ActiveStep"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (activeStepReplay == newValue)
			{
				return;
			}
			activeStepReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "TurboPumpSpeed"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if (turboPumpSpeedReplay == newValue)
			{
				return;
			}
			turboPumpSpeedReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "TurboPumpCurrent"))
	{
		if (value.canConvert(QVariant::Double))
		{
			double newValue = value.toDouble();
			if (turboPumpCurrentReplay == newValue)
			{
				return;
			}
			turboPumpCurrentReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "PrimaryPumpCurrent"))
	{
		if (value.canConvert(QVariant::Double))
		{
			double newValue = value.toDouble();
			if (primaryPumpCurrentReplay == newValue)
			{
				return;
			}
			primaryPumpCurrentReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "VG1PhysicalValue"))
	{
		if (value.canConvert(QVariant::Double))
		{
			double newValue = value.toDouble();
			if (VG1PhysicalValueReplay == newValue)
			{
				return;
			}
			VG1PhysicalValueReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "VG2PhysicalValue"))
	{
		if (value.canConvert(QVariant::Double))
		{
			double newValue = value.toDouble();
			if (VG2PhysicalValueReplay == newValue)
			{
				return;
			}
			VG2PhysicalValueReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "VPGType"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			double newValue = value.toUInt();
			if (VPGTypeValueReplay == newValue)
			{
				return;
			}
			VPGTypeValueReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "VPGStatus"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			double newValue = value.toUInt();
			if (VPGStatusValueReplay == newValue)
			{
				return;
			}
			VPGStatusValueReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "ResponseCode"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			double newValue = value.toUInt();
			if (VPGResponseCodeValueReplay == newValue)
			{
				return;
			}
			VPGResponseCodeValueReplay = newValue;
		}
	}
	else if (!strcmp(dpe, "ResponseValue"))
	{
		if (value.canConvert(QVariant::UInt))
		{
			double newValue = value.toUInt();
			if (VPGResponseValueValueReplay == newValue)
			{
				return;
			}
			VPGResponseValueValueReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPGF_LHC::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if (mode == DataEnum::Replay)
	{
		if (alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if (alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if (isColorByCtlStatus(color, mode))
	{
		return;
	}
	if (mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getMainColor(unsigned rr1, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if (rr1 & RR1_ERROR)
	{
		color.setRgb(COLOR_ERROR_VALVE);
	}
	// No errors - check ON/OFF state
	else if ((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		color.setRgb(COLOR_ERROR_VALVE);
	}
	else if (rr1 & RR1_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr1 & RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else if (rr1 & RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if (isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if (mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device - Value to be displayed on right clicking on the device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getMainStateString(unsigned rr1, bool plcAlarm, QString &string)
{
	if (plcAlarm)
	{
		string = "PLC Error";
		return;
	}
	if (!(rr1 & RR1_VALID))
	{
		string = "Not valid";
		return;
	}
	if (rr1 & RR1_ERROR)	// Still different strings for ON/OFF
	{
		if (rr1 & RR1_ON)
		{
			if (rr1 & RR1_OFF)
			{
				string = "Error";
			}
			else
			{
				string = "ON (error)";
			}
		}
		else if (rr1 & RR1_OFF)
		{
			string = "OFF (error)";
		}
		else
		{
			string = "Error";
		}
	}
	// No errors - check ON/OFF state
	else if ((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		string = "Error";
	}
	else if (rr1 & RR1_WARNING)
	{
		if (rr1 & RR1_ON)
		{
			if (rr1 & RR1_OFF)
			{
				string = "Error";
			}
			else
			{
				string = "ON (warning)";
			}
		}
		else if (rr1 & RR1_OFF)
		{
			string = "OFF (warning)";
		}
		else
		{
			string = "Undefined (warning)";
		}
	}
	else if (rr1 & RR1_ON)
	{
		string = "ON";
	}
	else if (rr1 & RR1_OFF)
	{
		string = "OFF";
	}
	else
	{
		string = "Undefined";
	}

}
#endif


/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
	QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("RR1");

	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	if (values.isEmpty()) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;

	getMainStateString(rr1, false, state);
	getMainColor(rr1, false, color);

	while (!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif


/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if (!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if (pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "EqpStatus");
	addDpeNameToList(dpes, "RR1");
	addDpeNameToList(dpes, "RR2");
	addDpeNameToList(dpes, "RR3");
}
#endif


/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVPGF_LHC::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if (criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if (checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if (!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if (pPlc)
		{
			if (!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if (!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if ((rr1 & (RR1_ON | RR1_OFF)) == RR1_ON)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if ((rr1 & (RR1_ON | RR1_OFF)) == RR1_OFF)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if (criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if (criteria.isErrors())
	{
		if (rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if (criteria.isWarnings())
	{
		if (rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Calculate color for given part of device
**
**	ARGUMENTS
**		part	- which part of device is requested (see EqpPart enum in Eqp.h)
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColor(int part, QColor &color, DataEnum::DataMode mode)
{
	if (isColorByCtlStatus(color, mode))
	{
		return;
	}
	unsigned useRr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	unsigned useRr2 = mode == DataEnum::Replay ? rr2Replay : rr2Online;
	unsigned useRr3 = mode == DataEnum::Replay ? rr3Replay : rr3Online;
	bool useAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	switch (part)
	{
	case EqpPartPump:
		getPartColorPump(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVVR1:
		getPartColorVVR1(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVVR2:
		getPartColorVVR2(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartPP:
		getPartColorPP(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartTMP:
		getPartColorTMP(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVVD:
		getPartColorVVD(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVVI:
		getPartColorVVI(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVVP:
		getPartColorVVP(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVVT:
		getPartColorVVT(useRr1, useRr2, useAlarm, color);
		break;
	case EqpPartVG1:
		getPartColorVG1(useRr1, useRr2, useRr3, useAlarm, color);
		break;
	case EqpPartVG2:
		getPartColorVG2(useRr1, useRr2, useRr3, useAlarm, color);
		break;
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for pump part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColorPump(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if ((rr1 & (RR1_ON | RR1_OFF)) == RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else if ((rr1 & (RR1_ON | RR1_OFF)) == RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else if (rr1 & (RR1_ON | RR1_OFF))
	{
		color.setRgb(COLOR_ERROR_VALVE);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for VVR1 part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColorVVR1(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (rr2 & RR2_VVR1_ERROR)
	{
		color.setRgb(COLOR_ERROR_VALVE);
	}
	else if (rr2 & RR2_VVR1_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr1 & RR1_VVR1_OPEN)
	{
		if (rr1 & RR1_VVR1_CLOSED)
		{
			color.setRgb(COLOR_ERROR_VALVE);
		}
		else
		{
			color.setRgb(COLOR_OPEN);
		}
	}
	else if (rr1 & RR1_VVR1_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for VVR2 part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColorVVR2(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (rr2 & RR2_VVR2_ERROR)
	{
		color.setRgb(COLOR_ERROR_VALVE);
	}
	else if (rr2 & RR2_VVR2_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr1 & RR1_VVR2_OPEN)
	{
		if (rr1 & RR1_VVR2_CLOSED)
		{
			color.setRgb(COLOR_ERROR_VALVE);
		}
		else
		{
			color.setRgb(COLOR_OPEN);
		}
	}
	else if (rr1 & RR1_VVR2_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for PP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColorPP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VPP_THERM_PROT_ST))
	{
		color.setRgb(COLOR_ERROR_DEVICE);
	}
	else if (!(rr1 & RR1_VPP_3_PHASE_ST) && (rr1 & RR1_VPP_PWR_MODE))
	{
		color.setRgb(COLOR_ERROR_DEVICE);
	}
	else if (!(rr1 & RR1_VPP_CABLE_ST))
	{
		color.setRgb(COLOR_ERROR_DEVICE);
	}
	else if (rr2 & RR2_VPP_ERROR)
	{
		color.setRgb(COLOR_ERROR_DEVICE);
	}
	else if (rr2 & RR2_VPP_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr1 & RR1_VPP_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else
	{
		color.setRgb(COLOR_OFF);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for TMP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColorTMP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (rr2 & RR2_VPT_ERROR)
	{
		color.setRgb(COLOR_ERROR_DEVICE);
	}
	else if (rr2 & RR2_VPT_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr1 & RR1_TMP_ON)
	{
		if (rr1 & RR1_TMP_NOMINAL)
		{
			color.setRgb(COLOR_ON);
		}
		else
		{
			color.setRgb(COLOR_OFF);
		}
	}
	else
	{
		color.setRgb(COLOR_OFF);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for VVD part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColorVVD(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (rr2 & RR2_VVD_ERROR)
	{
		color.setRgb(COLOR_ERROR_VALVE);
	}
	else if (rr2 & RR2_VVD_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr1 & RR1_VVD_OPEN)
	{
		if (rr1 & RR1_VVD_CLOSED)
		{
			color.setRgb(COLOR_ERROR_VALVE);
		}
		else
		{
			color.setRgb(COLOR_OPEN);
		}
	}
	else if (rr1 & RR1_VVD_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for VVI part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColorVVI(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (rr2 & RR2_VVI_ERROR)
	{
		color.setRgb(COLOR_ERROR_VALVE);
	}
	else if (rr2 & RR2_VVI_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr1 & RR1_VVI_OPEN)
	{
		if (rr1 & RR1_VVI_CLOSED)
		{
			color.setRgb(COLOR_ERROR_VALVE);
		}
		else
		{
			color.setRgb(COLOR_OPEN);
		}
	}
	else if (rr1 & RR1_VVI_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for VVP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColorVVP(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (rr2 & RR2_VVP_ERROR)
	{
		color.setRgb(COLOR_ERROR_VALVE);
	}
	else if (rr2 & RR2_VVP_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr1 & RR1_VVP_OPEN)
	{
		if (rr1 & RR1_VVP_CLOSED)
		{
			color.setRgb(COLOR_ERROR_VALVE);
		}
		else
		{
			color.setRgb(COLOR_OPEN);
		}
	}
	else if (rr1 & RR1_VVP_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for VVT part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColorVVT(unsigned rr1, unsigned rr2, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (rr2 & RR2_VVT_ERROR)
	{
		color.setRgb(COLOR_ERROR_VALVE);
	}
	else if (rr2 & RR2_VVT_WARNING)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr1 & RR1_VVT_OPEN)
	{
		if (rr1 & RR1_VVT_CLOSED)
		{
			color.setRgb(COLOR_ERROR_VALVE);
		}
		else
		{
			color.setRgb(COLOR_OPEN);
		}
	}
	else if (rr1 & RR1_VVT_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif

/*
**	FUNCTION
**		Calculate color for VG1 part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColorVG1(unsigned rr1, unsigned , unsigned rr3, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (rr3 & RR3_VG1_ERROR)
	{
		color.setRgb(COLOR_ERROR_VALVE);
	}
	else if (rr3 & RR3_VG1_OR)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr3 & RR3_VG1_UR)
	{
		color.setRgb(COLOR_UNDERRANGE);
	}
	else if (rr3 & RR3_VG1_ON) {
		color.setRgb(COLOR_ON);
	}
	else color.setRgb(COLOR_OFF);

}
#endif

/*
**	FUNCTION
**		Calculate color for VG2 part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartColorVG2(unsigned rr1, unsigned , unsigned rr3, bool plcAlarm, QColor &color)
{
	if (plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if (rr3 & RR3_VG2_ERROR)
	{
		color.setRgb(COLOR_ERROR_VALVE);
	}
	else if (rr3 & RR3_VG2_OR)
	{
		color.setRgb(COLOR_WARNING);
	}
	else if (rr3 & RR3_VG2_UR)
	{
		color.setRgb(COLOR_UNDERRANGE);
	}
	else if (rr3 & RR3_VG2_ON) {
		color.setRgb(COLOR_ON);
	}
	else color.setRgb(COLOR_OFF);

}
#endif

/*
**	FUNCTION
**		Calculate state string for given part of device
**
**	ARGUMENTS
**		part	- which part of device is requested (see EqpPart enum in Eqp.h)
**		string	- Variable where resulting string will be put
**		mode	- Required data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartStateString(int part, QString &string, DataEnum::DataMode mode)
{
	if (isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	unsigned useRr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	unsigned useRr2 = mode == DataEnum::Replay ? rr2Replay : rr2Online;
	bool useAlarm = mode == DataEnum::Replay ? plcAlarmReplay : plcAlarmOnline;
	switch (part)
	{
	case EqpPartPump:
		getPartStateStringPump(useRr1, useRr2, useAlarm, string);
		break;
	case EqpPartVVR1:
		getPartStateStringVVR1(useRr1, useAlarm, string);
		break;
	case EqpPartVVR2:
		getPartStateStringVVR2(useRr1, useAlarm, string);
		break;
	case EqpPartPP:
		getPartStateStringPP(useRr1, useAlarm, string);
		break;
	case EqpPartTMP:
		getPartStateStringTMP(useRr1, useAlarm, string);
		break;
	case EqpPartVVD:
		getPartStateStringVVD(useRr1, useAlarm, string);
		break;
	case EqpPartVVI:
		getPartStateStringVVI(useRr1, useAlarm, string);
		break;
	case EqpPartVVP:
		getPartStateStringVVP(useRr1, useRr2, useAlarm, string);
		break;
	case EqpPartVVT:
		getPartStateStringVVT(useRr1, useAlarm, string);
		break;
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for pump part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartStateStringPump(unsigned rr1, unsigned /* rr2 */, bool plcAlarm, QString &string)
{
	if (plcAlarm)
	{
		string = "PLC error";
	}
	else if (!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if ((rr1 & (RR1_ON | RR1_OFF)) == RR1_ON)
	{
		string = "VPG ON";
	}
	else if ((rr1 & (RR1_ON | RR1_OFF)) == RR1_OFF)
	{
		string = "VPG OFF";
	}
	else
	{
		string = "VPG undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for VVR1 part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartStateStringVVR1(unsigned rr1, bool plcAlarm, QString &string)
{
	if (plcAlarm)
	{
		string = "PLC error";
	}
	else if (!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if (rr1 & RR1_VVR1_OPEN)
	{
		if (rr1 & RR1_VVR1_CLOSED)
		{
			string = "VVR1 ERROR";
		}
		else
		{
			string = "VVR1 OPEN";
		}
	}
	else if (rr1 & RR1_VVR1_CLOSED)
	{
		string = "VVR1 CLOSED";
	}
	else
	{
		string = "VVR1 undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for VVR1 part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartStateStringVVR2(unsigned rr1, bool plcAlarm, QString &string)
{
	if (plcAlarm)
	{
		string = "PLC error";
	}
	else if (!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if (rr1 & RR1_VVR2_OPEN)
	{
		if (rr1 & RR1_VVR2_CLOSED)
		{
			string = "VVR2 ERROR";
		}
		else
		{
			string = "VVR2 OPEN";
		}
	}
	else if (rr1 & RR1_VVR2_CLOSED)
	{
		string = "VVR2 CLOSED";
	}
	else
	{
		string = "VVR2 undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for PP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartStateStringPP(unsigned rr1, bool plcAlarm, QString &string)
{
	if (plcAlarm)
	{
		string = "PLC error";
	}
	else if (!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if (rr1 & RR1_VPP_ON)
	{
		string = "PP ON";
		if ((rr1 & RR1_VPP_PWR_MODE)) //ask rodrigo state of this 
		{
			string += " (3 Phase)";
		}
		else string += " (1 Phase)";
	}
	else
	{
		string = "PP OFF";
		if (!(rr1 & RR1_VPP_THERM_PROT_ST)) string += ", Protection Tripped";
		if (!(rr1 & RR1_VPP_3_PHASE_ST)) string += ", Phase Error";
		if (!(rr1 & RR1_VPP_3_PHASE_ST)) string += ", Cable not connected";
	}


}
#endif

/*
**	FUNCTION
**		Calculate state string for TMP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartStateStringTMP(unsigned rr1, bool plcAlarm, QString &string)
{
	if (plcAlarm)
	{
		string = "PLC error";
	}
	else if (!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if (rr1 & RR1_TMP_ON)
	{
		string = "TMP ON";
		if (!(rr1 & RR1_TMP_ON))
		{
			string += " (not OK)";
		}
		if (!(rr1 & RR1_TMP_NOMINAL))
		{
			string += " (speed not OK)";
		}
	}
	else
	{
		string = "TMP OFF";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for VVD part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartStateStringVVD(unsigned rr1, bool plcAlarm, QString &string)
{
	if (plcAlarm)
	{
		string = "PLC error";
	}
	else if (!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if (rr1 & RR1_VVD_OPEN)
	{
		if (rr1 & RR1_VVD_CLOSED)
		{
			string = "VVD ERROR";
		}
		else
		{
			string = "VVD OPEN";
		}
	}
	else if (rr1 & RR1_VVD_CLOSED)
	{
		string = "VVD CLOSED";
	}
	else
	{
		string = "VVD undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for VVI part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartStateStringVVI(unsigned rr1, bool plcAlarm, QString &string)
{
	if (plcAlarm)
	{
		string = "PLC error";
	}
	else if (!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if (rr1 & RR1_VVI_OPEN)
	{
		if (rr1 & RR1_VVI_CLOSED)
		{
			string = "VVI ERROR";
		}
		else
		{
			string = "VVI OPEN";
		}
	}
	else if (rr1 & RR1_VVI_CLOSED)
	{
		string = "VVI CLOSED";
	}
	else
	{
		string = "VVI undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for VVP part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartStateStringVVP(unsigned rr1, unsigned rr2, bool plcAlarm, QString &string)
{
	if (plcAlarm)
	{
		string = "PLC error";
	}
	else if (!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if (rr1 & RR1_VVP_OPEN)
	{
		if (rr1 & RR1_VVP_CLOSED)
		{
			string = "VVP ERROR";
		}
		else
		{
			string = "VVP OPEN";
		}
	}
	else if (rr2 & RR1_VVP_CLOSED)
	{
		string = "VVP CLOSED";
	}
	else
	{
		string = "VVP undefined";
	}
}
#endif

/*
**	FUNCTION
**		Calculate state string for VVT part of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		string		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVPGF_LHC::getPartStateStringVVT(unsigned rr1, bool plcAlarm, QString &string)
{
	if (plcAlarm)
	{
		string = "PLC error";
	}
	else if (!(rr1 & RR1_VALID))
	{
		string = "Not valid";
	}
	else if (rr1 & RR1_VVT_OPEN)
	{
		if (rr1 & RR1_VVT_CLOSED)
		{
			string = "VVT ERROR";
		}
		else
		{
			string = "VVT OPEN";
		}
	}
	else if (rr1 & RR1_VVT_CLOSED)
	{
		string = "VVT CLOSED";
	}
	else
	{
		string = "VVT undefined";
	}
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpVPGF_LHC::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr2 = mode == DataEnum::Replay ? rr2Replay : rr2Online;
	return rr2 & RR2_ERROR_CODE;
}

/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVPGF_LHC::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if (pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if (pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if (checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	// Without PLC connection does not match any criteria
	if (plcAlarmOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// In invalid state - only matches Error/warning criteria
	if (!(rr1Online & RR1_VALID))
	{
		switch (pCriteria->getType())
		{
		case EqpMsgCriteria::Error:
			if (rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if (rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		default:
			pCriteria->setMatch(false, false, 0);
			return;
		}
	}

	// State is valid - decide based on criteria
	unsigned maskedRr1 = rr1Online & (RR1_ON | RR1_OFF);
	switch (pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch (pCriteria->getSubType())
		{
		case FunctionalTypeVPGF::On:
			if (maskedRr1 == RR1_ON)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVPGF::Off:
			if (maskedRr1 == RR1_OFF)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVPGF::TmpOn:
			if (rr1Online & RR1_TMP_ON)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVPGF::TmpOff:
			if (!(rr1Online & RR1_TMP_ON))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	case EqpMsgCriteria::Error:
		if (rr1Online & RR1_ERROR)
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else
		{
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	case EqpMsgCriteria::Warning:
		if (rr1Online & (RR1_ERROR | RR1_WARNING))
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else
		{
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}
