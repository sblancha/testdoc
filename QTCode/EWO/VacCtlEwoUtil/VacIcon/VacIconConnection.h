#ifndef	VACICONCONNECTION_H
#define	VACICONCONNECTION_H

#include "VacCtlEwoUtilExport.h"

//	Class provides equipment connection functionality for
//	VacIcon. VacIcon itself can not be derived from
//	InterfaceEqp directly because both InterfaceEqp and QWidget
//	are derived from QObject - and such situation can not be
//	handled by Qt correctly. Message from one of Qt forums:
//
//	Your class inherits from multiple QObject based classes, normally
//	in C++ this could be handled by virtual inheritance, but some parts
//	of QObject is not made compativle with this, so that is no go.
//
//	Instances of this class are not supposed to be used on it's own, but
//	rather they shall always appear in instances of VacIcon
//
//	It is supposed that VacIcon instance keeps track of connections, hence,
//	it is vacIcon who is responsible for calling connect() and disconnect()
//	at right moments with right arguments


#include "InterfaceEqp.h"

#include <qstringlist.h>

class Eqp;

class VACCTLEWOUTIL_EXPORT VacIconConnection : public InterfaceEqp
{
	Q_OBJECT

public:
	VacIconConnection() {}
	virtual ~VacIconConnection() {}

	void connect(Eqp *pEqp, const QStringList &dpes, DataEnum::DataMode mode);
	void disconnect(Eqp *pEqp);

public slots:
	// Override slots of InterfaceEqp
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);
	virtual void selectChange(Eqp *pSrc);

signals:
	// Signal is emitted when state of one of DPE's of interest has been changed
	void stateChanged(void);

	// Signal is emitted when device selection changed
	void selectChanged(void);

protected:
	// List of DPEs which are of interest for icon
	QStringList	dpes;

	// Mode of connection
	DataEnum::DataMode	mode;
};

#endif	// VACICONCONNECTION_H
