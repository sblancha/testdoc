/**@name SCRIPT: lhcVacAccessZone

@author: Sergey Merker (IHEP, Protvino, Russia)

Creation Date: 22/09/2009

Modification History:

version 1.0

Purpose and methods: 
1.Collect information, concerning access zone, and write results to DPE
where other PVSS code can easily read this data.
2.Perform valve control to enable(disable) access zone
3.Collect information, concerning valve state, and write results to DPE
where other PVSS code (control panel) can easily read this data.
4. Two independent loops (threads)  process valve state and access zone(valve) commands

Usage: Public

PVSS manager usage: CTRL (PVSS00CTRL)

*/
#uses "VacCtlEqpDataPvss"			// Load DLL or so
#uses "vclDevCommon.ctl"             // Load CTRL library
#uses "vclDIPConfig.ctl"		// Load CTRL library 

dyn_string glVacMainParts;
dyn_dyn_string glVacSectors, glSectDomains;

// List of valve DP names
dyn_string      vvsList;

// List of valve states, 1 integer for every valve:
dyn_int    vvsStateList;

// Valve states to be stored in the list above
const int VALVE_OPEN = 1;
const int VALVE_CLOSED = 2;
const int VALVE_OTHER = 3;
const int VALVE_INVALID = 4;

dyn_string      accessZoneList;   
dyn_dyn_string  accessZoneVvsList;
dyn_int     accessZoneCommandList;

// List of last known access zone states. For every zone - 5 integers:
//  1 = Number of open valves
//  2 = Number of closed valves
//  3 = Number of valves in other state
//  4 = Number of valves in invalid state
//  5 = Common zone state
dyn_dyn_int  accessZoneStates;

// Constants for accessing list above
const int ZONE_NOPEN = 1;
const int ZONE_NCLOSED = 2;
const int ZONE_NOTHER = 3;
const int ZONE_NIVALID = 4;
const int ZONE_STATE = 5;


// Flag indicating if state of at least one valve has changed, hence, claculation of new
// zones state is necessary
bool  vvsStateChanged;

const int SCRIPT_PRIVILEGE = 2;

bool            ready;
bool            debug = false;

// Flag to disable (temporarily) valves action execution
bool  disableVvsAction = false;

main()
{
  string	accessZone;
  dyn_string    exceptionInfo;
  bool 		isConfigured;
// 1st step: init static data	
  DebugTN("vcsAccessZone.ctl: Starting");
  ready = false;
  if(! InitStaticData())
  {
     return;
  }
// 2nd step: get all valves to be participate in access zone(s)
// Check all DPs of correct type for machine (LHC, cPS)
   dyn_string allDpNames;
   if(glAccelerator == "ComplexPS")
   {
	   allDpNames = dpNames("*", "VVS_PS");
   }
   else
   {
	    allDpNames = dpNames("*", "VVS_LHC");
   }

  for(int n = dynlen(allDpNames) ; n > 0 ; n--)
  {
    string dpName = dpSubStr(allDpNames[n], DPSUB_DP);
    for(int i = 1 ; i < ACCESS_ZONES_PER_VALVE_NUMBER ; i++)
    {
      LhcVacGetAttribute(dpName, "AccessZone" + i, accessZone);
      if(accessZone == "")
      {
        continue;
      }
      AppendVvsLists(dpName, accessZone);
    }
  }

// 3th step: get all access zones
// performs Dp connection
  dyn_string allAccessZones = dpNames("*", "VACCESS_ZONE");
  for(int n = dynlen(allAccessZones) ; n > 0 ; n--)
  {
    string accessZoneName = dpSubStr(allAccessZones[n], DPSUB_DP);
    ConnectToAccessZoneCMW(accessZoneName);
    ConnectToAccessZonePVSS(accessZoneName);
    ConnectToAccessZoneACK(accessZoneName);
  }
 // 4 step: start loop to processing state of access zone	 
  int cc = startThread("ProcessAccessZoneState");
 // 5 step: start loop to processing command for access zone	
  int cc = startThread("ProcessAccessZoneCommand");
  ready = true;
}

bool InitStaticData()
{
  dyn_string	exceptionInfo;
  long		coco;
  bool		isLhcData;

  // Set accelerator name to be used by other components
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsAccessZone.ctl: Failed to read machine name: " + exceptionInfo);
    return false;
  }
  isLhcData = glAccelerator == "LHC" ? true : false;
  // Initalize passive machine data
  coco = LhcVacEqpInitPassive(isLhcData, PROJ_PATH + "/data/", glAccelerator, exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsAccessZone.ctl: LhcVacEqpInitPassive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsAccessZone.ctl: LhcVacEqpInitPassive() done");

  // Initialzie active equipment information
  coco = LhcVacEqpInitActive(glAccelerator, PROJ_PATH + "data/" + glAccelerator + ".for_DLL",
    exceptionInfo);
  if(coco < 0)
  {
    DebugTN("vcsAccessZone.ctl: LhcVacEqpInitActive() failed: " + exceptionInfo);
    return false;
  }
  DebugTN("vcsAccessZone.ctl: LhcVacEqpInitActive() done");
  LhcVacInitDevFunc();
  if(LhcVacGetAllSectors(glVacMainParts, glVacSectors, glSectDomains, exceptionInfo) < 0)
  {
    DebugTN("vcsAccessZone.ctl: LhcVacGetAllSectors() failed: " + exceptionInfo);
    return false;
  }
  return true;	
}

// AppendVvsLists
/**
Purpose:
Build 5 lists
 vvsList - dpNames of all VVS_LHC participating in access zone(s)
 vvsStateList - list of states for VVS_LHC participating in access zone(s)
 
 accessZoneList - list of access zone
 accessZoneVvsList - list of vvs (index is according to accessZoneList)
 accessZoneCommandList - list of command (index is according to accessZoneList)
Arguments:
	vvsName - [in] Valve DP name
	accessZoneName - [in] Name access zone
Usage: Private

PVSS manager usage: CTRL (PVSS00CTRL)

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
void AppendVvsLists(string vvsName, string accessZoneName)
{
  int index = dynContains(vvsList, vvsName);

//DebugTN("vcsAccessZone.ctl: AppendVvsLists vvsName :", vvsName, index); 
  if(index == 0)
  {
 // New VVS_LHC
    dynAppend(vvsList, vvsName);
    dynAppend(vvsStateList, 0);
    ConnectToValve(vvsName);
  } 
  // fill accessZone
  int index = dynContains(accessZoneList, accessZoneName);
//  DebugTN("vcsAccessZone.ctl: AppendVvsLists accessZoneName :", accessZoneName, index); 
  if(index == 0)
  {
     dynAppend(accessZoneList, accessZoneName);
     dynAppend(accessZoneVvsList, makeDynString(vvsName));
     dynAppend(accessZoneCommandList, ACCESS_ZONES_CMD_NOTHING);
     dynAppend(accessZoneStates, makeDynInt(0, 0, 0, 0, 0));
  }
  else
  {
     dynAppend(accessZoneVvsList[index], vvsName);
  }
}
void ConnectToValve(string vvsName)
{
  dyn_string exceptionInfo;
  string  plcDp, alarmDp;
  LhcVacDevicePlc(vvsName, plcDp, alarmDp, exceptionInfo);
  if(alarmDp != "")
  {
    dpConnect("NewDataPlcCb", vvsName + ".RR1", alarmDp + ".alarm");
  }
  else
  {
    dpConnect("NewDataNoPlcCb", vvsName + ".RR1");
  }
}
// Callback for new data in online mode - there is no PLC alarm DP
void NewDataNoPlcCb(string dp1, unsigned rr1)
{
  NewVvsData(dp1, rr1, 0);
}

// Callback for new data in online mode - there is PLC alarm DP
void NewDataPlcCb(string dp1, unsigned rr1, string dp2, int alarm)
{
  NewVvsData(dp1, rr1, alarm);
}
// Process new valve state
void NewVvsData(string vvsName, unsigned rr1, int alarm)
{
 // looking for all access zones for this valve
  int index = dynContains(vvsList, dpSubStr(vvsName, DPSUB_DP));
  if(index > 0)
  {
    // Calculate new valve state
    int state = 0;
    if(alarm != 0)
    {
      state = VALVE_INVALID;
    }
    else if((rr1 & SVCU_SPS_R_VALID) == 0)
    {
      state = VALVE_INVALID;
    }
    else if((rr1 & SVCU_SPS_R_OPEN) != 0)
    {
      if((rr1 & SVCU_SPS_R_CLOSED) != 0)
      {
        state = VALVE_OTHER;
      }
      else
      {
        state = VALVE_OPEN;
      }
    }
    else if(rr1 & SVCU_SPS_R_CLOSED)
    {
      state = VALVE_CLOSED;
    }
    else
    {
      state = VALVE_OTHER;
    }
    if(state != vvsStateList[index])
    {
      vvsStateList[index] = state;
      synchronized(vvsStateChanged)
      {
        vvsStateChanged = true;
      }
    }
  }
  else
  {
    DebugTN("vcsAccessZone.ctl: INTERNAL: VVS_LHC valve <" + dpSubStr(vvsName, DPSUB_DP) + ">  not found");
  }
}
void ConnectToAccessZoneCMW(string accessZoneName)
{
   dpConnect("NewAccessZoneDataCMW", false, accessZoneName + ".CMD_CMW");
}
void ConnectToAccessZonePVSS(string accessZoneName)
{
   dpConnect("NewAccessZoneDataPVSS", false, accessZoneName + ".CMD_PVSS");
}
void ConnectToAccessZoneACK(string accessZoneName)
{
   dpConnect("NewAccessZoneDataACK", false, accessZoneName + ".CMD_ACK");
}
	
void NewAccessZoneDataCMW(string cmdCMWdpe, bool cmd)
{
  dyn_errClass	err;
  
  dpSetWait(dpSubStr(cmdCMWdpe, DPSUB_DP) + ".CMD_ACK", 
     (cmd == true) ? ACCESS_ZONES_CMD_ON_CMW : ACCESS_ZONES_CMD_OFF_CMW);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    DebugTN("vcsAccessZone.ctl: dpSetWait failed for <" + dpSubStr(cmdCMWdpe, DPSUB_DP) + ".CMD_ACK> :" + err);
  }
}
void NewAccessZoneDataPVSS(string cmdPVSSdpe, bool cmd)
{
 dyn_errClass	err;
  
  dpSetWait(dpSubStr(cmdPVSSdpe, DPSUB_DP) + ".CMD_ACK", 
     (cmd == true) ? ACCESS_ZONES_CMD_ON_PVSS : ACCESS_ZONES_CMD_OFF_PVSS);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    DebugTN("vcsAccessZone.ctl: dpSetWait failed for <" + dpSubStr(cmdPVSSdpe, DPSUB_DP) + ".CMD_ACK> :" + err);
  }
}

void NewAccessZoneDataACK(string dpe, int cmd)
{
  // only fill accessZoneCommandList, processing will be done while main loop
 string accessZone = dpSubStr(dpe, DPSUB_DP);
 
 int index = dynContains(accessZoneList, accessZone);
 if(index > 0)
 {
   accessZoneCommandList[index] = cmd;
 }  
 else // impossible, but ...
 {
    DebugTN("vcsAccessZone.ctl: INTERNAL: access zone <" + accessZone + ">  not found");
 }
}

void  ExecuteVVSAction(string vvs, string accessZone, unsigned action,
  string &actName, dyn_string exceptionInfo)
{
  if(disableVvsAction)
  {
    return;
  }

  string          errMessage = "";
  bool            allowed;
  dyn_string	    privDpNames;
  bool            result = false;
  dyn_anytype     act = makeDynAnytype(action);
  // get action name in readable form, only for reporting 
  LhcVacGetDevActName(act, vvs, dpTypeName(vvs), actName, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    return;
  }
  LhcVacGetDpForPrivCheck(vvs, dpTypeName(vvs), privDpNames, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    errMessage = "Check privilege Error";
    SentMessageToMainView(vvs, accessZone, actName, errMessage);
    return;
  }
  for(int n = dynlen(privDpNames); n > 0 ; n--)
  {
    int privilege = SCRIPT_PRIVILEGE;
    dynClear(glActSpecialMessages);
    LhcVacGetRequiredActPriv_VV(privDpNames[n], act, privilege, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
    {
      errMessage = "Check privilege Error";
      SentMessageToMainView(vvs, accessZone, actName, errMessage);
      return;
    }
    if(privilege > SCRIPT_PRIVILEGE)
    {
      SentMessageToMainView(privDpNames[n], accessZone, actName, glActSpecialMessages[1]);
      return;
    }
  }
  LhcVacExecuteAction(vvs, dpTypeName(vvs), act, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  { 
     errMessage = "Execution Error";
  }
  SentMessageToMainView(vvs, accessZone, actName, errMessage);
}
void  SentMessageToMainView(string vvs, string accessZone, string actName, string baseMessage)
{
  string     message;
  dyn_string exceptionInfo;
  
  if(vvs == "")
  {
    // message for access zone
    message = baseMessage;
  }
  else
  {
    // message for valve (!OK)
    if(baseMessage != "") 
    {  
       message = "Not " + actName + " <" + vvs + "> for access zone < "  + accessZone + "> :" + baseMessage;
    }
    // message for valve (OK)
    else
    {
       message = actName + " <" + vvs + "> for access zone < "  + accessZone + ">";
    }   
  }
  string systemName = getSystemName();
  unMessageText_send(systemName, "1", "CmdHandler","user", "*", "INFO", message, exceptionInfo);  
}
// infinite loop to monitoring summary state of access zones
void ProcessAccessZoneState()
{
  DebugTN("vcsAccessZone.ctl: ProcessAccessZoneState Starting");
  while(1)
  {
    bool needProcessing = false;
    synchronized(vvsStateChanged)
    {
      needProcessing = vvsStateChanged;
      vvsStateChanged = false;
    }
    if(needProcessing)
    {
      // DebugTN("vcsAccessZone.ctl: Start processing");
      // time startTime = getCurrentTime();
      for(int n = dynlen(accessZoneList); n > 0; n--)
      {
        ProcessSingleZoneState(n);
      }
      // float spent = getCurrentTime() - startTime;
      // DebugTN("vcsAccessZone.ctl: Finish processing", spent);
    }
    delay(1);  
  }
}

void ProcessSingleZoneState(int zoneIdx)
{
  int nOpen = 0, nClosed = 0, nInvalid = 0, nOther = 0;
  int nValvesPerZone = dynlen(accessZoneVvsList[zoneIdx]);

//DebugTN("vcsAccessZone.ctl: zoneIdx ", zoneIdx);
//DebugTN("vcsAccessZone.ctl: accessZoneList ", accessZoneList);
  
  for(int i = 1; i <= nValvesPerZone; i++)
  {
    int index = dynContains(vvsList, accessZoneVvsList[zoneIdx][i]);
    if(index > 0)
    {
      switch(vvsStateList[index])
      {
      case VALVE_OPEN:
        nOpen++;
        break;
      case VALVE_CLOSED:
        nClosed++;
        break;
      case VALVE_OTHER:
        nOther++;
        break;
      case VALVE_INVALID:
        nInvalid++;
        break;
      }
    }
    else // impossible, but ...
    {
      DebugTN("vcsAccessZone.ctl: INTERNAL: VVS_LHC valve <" + accessZoneVvsList[n][i] + ">  not found");
      nInvalid++;
    }        
  }

  // check summary state of access zone
  int accessZoneState;
  if(nValvesPerZone == nClosed)
  {
    accessZoneState = ACCESS_ZONES_STATE_ALL_CLOSED;
  }
  else if(nValvesPerZone == nOpen)
  {
    accessZoneState = ACCESS_ZONES_STATE_ALL_OPEN;
  }
  else
  {
    accessZoneState = ACCESS_ZONES_STATE_OTHER;
  }

  // If at least something has changed - write to DPE and store last known state
  dyn_int zoneState = accessZoneStates[zoneIdx];

  if((nOpen != zoneState[ZONE_NOPEN]) || (nClosed != zoneState[ZONE_NCLOSED])
    || (nOther != zoneState[ZONE_NOTHER]) || (nInvalid != zoneState[ZONE_NIVALID])
    || (accessZoneState != zoneState[ZONE_STATE]))
  {
    // write state info in the correspondent DPE(s)
    string dpName = accessZoneList[zoneIdx];
    dpSetWait(dpName + ".State", accessZoneState,
      dpName + ".Nopen", nOpen, 
      dpName + ".Nclosed", nClosed, 
      dpName + ".Nother", nOther, 
      dpName + ".Ninvalid", nInvalid);
    dyn_errClass err = getLastError();
    if(dynlen(err) > 0)
    {
      DebugTN("vcsAccessZone.ctl: dpSetWait failed for access zone <" + accessZoneList[zoneIdx] + "> :" +  err);
    }
    // Save last known zone state
    accessZoneStates[zoneIdx][ZONE_NOPEN] = nOpen;
    accessZoneStates[zoneIdx][ZONE_NCLOSED] = nClosed;
    accessZoneStates[zoneIdx][ZONE_NOTHER] = nOther;
    accessZoneStates[zoneIdx][ZONE_NIVALID] = nInvalid;
    accessZoneStates[zoneIdx][ZONE_STATE] = accessZoneState;
    if(debug)
    {
      DebugTN("vcsAccessZone.ctl: access zone <" + accessZoneList[zoneIdx] + "> processed");
      DebugTN("vcsAccessZone.ctl:  nOpen: ", nOpen);
      DebugTN("vcsAccessZone.ctl:  NClosed: ", nClosed);
      DebugTN("vcsAccessZone.ctl:  nOther: ", nOther);
      DebugTN("vcsAccessZone.ctl:  nInvalid: ", nInvalid);
    }
  }
}

void ProcessAccessZoneCommand()
{
  int lastCommand=ACCESS_ZONES_CMD_NOTHING;
  dyn_string   exceptionInfo;
  string       actName;
  unsigned     action;
   
  DebugTN("vcsAccessZone.ctl: ProcessAccessZoneCommand Starting");
  
  while(1)
  {
    if(!ready)
    {
      delay(5); 
      continue;  
    }

    for(int n = dynlen(accessZoneList) ; n > 0 ; n--)
    {     
      lastCommand = accessZoneCommandList[n];
      if(lastCommand == ACCESS_ZONES_CMD_NOTHING)
      {
        // nothing to do
        continue;
      }
      string commandName;
      LhcVacGetAccessZoneCommandName(lastCommand, accessZoneList[n], commandName);
      SentMessageToMainView("", "", 0, commandName);
      if(debug)
      {
        DebugTN("vcsAccessZone.ctl: processing comamnd " + lastCommand + " (" + commandName +
           ") for zone <" + accessZoneList[n] + "> processed");
      }

      // process all valves for current access zone      
      int i;
      for(i = dynlen(accessZoneVvsList[n]) ; i > 0; i--)
      {
        if(lastCommand != accessZoneCommandList[n])
        {
          // if command has been changed, break this loop
          // new command will be performed in the next loop
          break;
        }
        if((lastCommand == ACCESS_ZONES_CMD_ON_CMW) || (lastCommand == ACCESS_ZONES_CMD_ON_PVSS))
        {
          // close all valves in current access zone
          action = LVA_DEVICE_CLOSE;
        }
        else if((lastCommand == ACCESS_ZONES_CMD_OFF_CMW) || (lastCommand == ACCESS_ZONES_CMD_OFF_PVSS))
        {
          // open all valves in current access zone
          action = LVA_DEVICE_OPEN;
        }
        ExecuteVVSAction(accessZoneVvsList[n][i], accessZoneList[n], action, actName, exceptionInfo);
        if(dynlen(exceptionInfo) > 0)
        {
          DebugTN("vcsAccessZone.ctl: action <" + action + "> failed for valve <" + dpSubStr(vvs, DPSUB_DP) + 
            " > (access zone: '" + accessZoneList[n] + "') " + exceptionInfo);
        }
      }
      if(i == 0)
      {
         // all valves are processed, set flag
         accessZoneCommandList[n] = ACCESS_ZONES_CMD_NOTHING;
      }      
    }
    delay(1);  
  }
}
