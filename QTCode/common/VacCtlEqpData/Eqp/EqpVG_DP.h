#ifndef	EQPVG_DP_H
#define EQPVG_DP_H

//	VG_DP - VG Profibus gauge, very similar to VG_STD, so just override few methods

#include "EqpVG_STD.h"


class VACCTLEQPDATA_EXPORT EqpVG_DP : public EqpVG_STD
{
	Q_OBJECT

public:
	EqpVG_DP(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVG_DP(const EqpVG_DP &src) : EqpVG_STD(src) {}
#endif
	~EqpVG_DP();

protected:

#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, float pr, bool plcAlarm, QColor &color);
#endif

};

#endif	// EQPVGF_DP_H
