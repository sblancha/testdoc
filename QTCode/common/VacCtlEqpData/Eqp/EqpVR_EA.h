// ---------------------------------------------------------------------------------------------------------------------
/**
System:         WinCC_OA 3.15 Vacuum framework
Component Name: VacCtlEqpData librairy
Class: Equipment Vacuum contRol - bakE-out Alarm channel (VR_EA)
Language: C++

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva

Description: This file contains the defined classes for the equipment Vacuum contRol - bakE-out Alarm channel (VR_EA)

Limitations: To be used with vacuum framework.

Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------
#ifndef EQPVR_EA_H
#define	EQPVR_EA_H

//	VR_EA - Vacuum contRol - bakE-out Alarm channel 

#include "Eqp.h"

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_NOEQP				255,192,203		//pink
#define COLOR_INVALID			63,127,255  	//blue
#define COLOR_OFF				255,255,255		//white
#define COLOR_ON				0,204,0			//green
//#define	COLOR_ERROR				255, 0, 0	//finally red not used for error
#define	COLOR_ERROR				220, 80, 200	//darkviolet
#define	COLOR_WARNING			255,165,0		//orange

#define	COLOR_SERVICES			255,165,0		//orange

//	Main state bits to be analyzed
#define	RR1_VALID				(0x4000) // 0x40 00
#define RR1_ERROR				(0x0080) // 0x00 80
#define	RR1_WARNING				(0x0040) // 0x00 40 

class VACCTLEQPDATA_EXPORT EqpVR_EA : public Eqp
{
	Q_OBJECT

public:
	EqpVR_EA(const Eqp &source);
#ifdef Q_OS_WIN
	EqpVR_EA(const EqpVR_EA &src) : Eqp(src) {}
#endif
	~EqpVR_EA();

	virtual void setActive(DataEnum::DataMode mode, bool active);

	virtual void getMaster(QString &masterDp); // no master channel , int &masterChannel);
	virtual void getStateDpes(QStringList &stateDpes);
	virtual void getValueDpes(QStringList &valueDpes);

	virtual void connect(InterfaceEqp *pDst, DataEnum::DataMode mode);
	virtual void disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode);

	virtual void newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);

#ifndef PVSS_SERVER_VERSION
	void newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp);
	virtual void getMainColor(QColor &color, DataEnum::DataMode mode);
	virtual void getMainStateString(QString &string, DataEnum::DataMode mode);
	virtual void getMainStateStringForMenu(QString &string, DataEnum::DataMode mode);
	virtual void getToolTipString(QString &string, DataEnum::DataMode mode);
	virtual void getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color);
#endif

	// Device list support
#ifndef PVSS_SERVER_VERSION
	virtual void getDpesForDevList(QStringList &dpes);
	virtual int checkDevListCrit(void);
#endif

	virtual void checkCriteria(EqpMsgCriteria *pCriteria);

	// Access for essential values
	inline bool isComAlarm(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? comAlarmReplay : comAlarmOnline; }
	inline unsigned getRR1(DataEnum::DataMode mode) const { return mode == DataEnum::Replay ? rr1Replay : rr1Online; }
	virtual unsigned getErrorSt() const { return errorStOnline; }
	virtual unsigned getTargetSt() const { return targetStOnline; }
	virtual double getDegCSt() const { return degCStOnline; }
	virtual double getDegCWarnThreSt() const { return degCWarnThreStOnline; }
	virtual double getDegCErrThreSt() const { return degCErrThreStOnline; }
	public slots:
	virtual void dpeChange(Eqp *pSrc, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp);

protected:
	// Last known value of RR1 DPE 
	unsigned	rr1Online;
	unsigned	rr1Replay;

	// Last knwon state of TCP connection to mobile (true == alarm) for online mode
	bool		comAlarmOnline;
	bool		comAlarmReplay;

	// Last known value of error code status for online mode
	unsigned	errorStOnline;
	unsigned	errorStReplay;

	// Last known target channel number
	unsigned	targetStOnline;

	// Last known value of temperature process value 
	double degCStOnline;
	double degCStReplay;

	// Last known value of the temperature warning threshold
	double degCWarnThreStOnline;

	// Last known value of the temperature error threshold
	double degCErrThreStOnline;


#ifndef PVSS_SERVER_VERSION
	virtual void getMainColor(unsigned rr1, unsigned target, bool comAlarm, QColor &color);
	virtual void getMainStateString(unsigned rr1, unsigned target, bool comAlarm, QString &string);

private:

#endif
};


#endif	// EQPVR_EA_H


