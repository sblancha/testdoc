#uses "VacCtlEqpDataPvss"	// Load DLL
#uses "vclSectors.ctl"	// Load CTRL library
#uses "vclEqpConfig.ctl"	// Load CTRL library
#uses "email.ctl"	// Load PVSS standard CTRL library
#uses "vclMachineMode.ctl"		// Load CTRL library
#uses "vclDIPConfig.ctl"		// Load CTRL library

// DP to generate VELO alarm(s)
const string LHC_VAC_VELO_ALARM_DP = "VELO_Alarm";

// DPs to analyse alarm situation at the VELO area
// Gauges + Proc( venting )
const string PE411 = "VGPC_PE411_IP8_VE";
const string PE412 = "VGPC_PE412_IP8_VE";
const string PE421 = "VGPC_PE421_IP8_VE";
const string PE422 = "VGPC_PE422_IP8_VE";
const string VPROC = "VPROC_PVEN_IP8_VE";

// Turbo pumps
const string TP111 = "VPTX_TP111_IP8_VE";
const string TP211 = "VPTX_TP211_IP8_VE";
const string TP301 = "VPTX_TP301_IP8_VE";

// Valves
const string SV421 = "VVX_SV421_IP8_VE";

// Ion pumps
const string IP431 = "VPIX_IP431_IP8_VE";
const string IP441 = "VPIX_IP441_IP8_VE";

// Stored PLC alarm state for gauges
int alarmGauges;

// Stored values for PE411 gauge
unsigned RR1_PE411;
float PR_PE411;

// Stored values for PE412 gauge
unsigned RR1_PE412;
float PR_PE412;

// Stored values for PE421 gauge
unsigned RR1_PE421;
float PR_PE421;

// Stored values for PE422 gauge
unsigned RR1_PE422;
float PR_PE422;

// Flag to enable debugging messages
bool debug = false;

main()
{
  DebugTN("vcsVeloAlarm.ctl: Starting");
	
  if(!dpExists(LHC_VAC_VELO_ALARM_DP))
  {
    DebugTN("vcsVeloAlarm: DP <" + LHC_VAC_VELO_ALARM_DP + "> doesn't exist, exiting...");
    return;
  }

  // Publish alarms using DIP
  dyn_string exceptionInfo;
  ProcessDpePublishing(LHC_VAC_VELO_ALARM_DP + ".AL1",
    "dip/VAC/LHC/VELO/Alarm/Valves", exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsVeloAlarm: publish " + LHC_VAC_VELO_ALARM_DP + ".AL1 : ", exceptionInfo);
  }
  ProcessDpePublishing(LHC_VAC_VELO_ALARM_DP + ".AL2",
    "dip/VAC/LHC/VELO/Alarm/TurboPumps", exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsVeloAlarm: publish " + LHC_VAC_VELO_ALARM_DP + ".AL2 : ", exceptionInfo);
  }
  ProcessDpePublishing(LHC_VAC_VELO_ALARM_DP + ".AL3",
    "dip/VAC/LHC/VELO/Alarm/IonPumps", exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsVeloAlarm: publish " + LHC_VAC_VELO_ALARM_DP + ".AL3 : ", exceptionInfo);
  }
  ProcessDpePublishing(LHC_VAC_VELO_ALARM_DP + ".AL4",
    "dip/VAC/LHC/VELO/Alarm/Gauges", exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsVeloAlarm: publish " + LHC_VAC_VELO_ALARM_DP + ".AL4 : ", exceptionInfo);
  }
  ProcessDpePublishing(LHC_VAC_VELO_ALARM_DP + ".AL5",
    "dip/VAC/LHC/VELO/Alarm/TurboPumpsWarning", exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsVeloAlarm: publish " + LHC_VAC_VELO_ALARM_DP + ".AL5 : ", exceptionInfo);
  }

  // Connect to DPEs of required devices in VELO
  string plcDp, alarmDp;
  dyn_string exceptionInfo;
  LhcVacDevicePlc(SV421, plcDp, alarmDp, exceptionInfo);

  // Connect to DPs responsible for Valve alarm
  if(alarmDp != "")
  {
    dpConnect("ValveVeloAlarmCb", SV421 + ".RR1",
      alarmDp + ".alarm");
  }
  else
  {
    dpConnect("ValveVeloAlarmNoPlcCb", SV421 + ".RR1");
  }

  // Connect to DPs responsible for Ion pumps alarm
  string modeDpName;
  LhcVacGetMachineModeDp(modeDpName);
  if(modeDpName != "")
  {
    if(alarmDp != "")
    {
      dpConnect("IonPumpsVeloAlarmCb",
        modeDpName + ".Beam_R.Beam",
        modeDpName + ".Beam_B.Beam",
        IP431 + ".PR",
        IP441 + ".PR",
        alarmDp + ".alarm");
    }
    else
    {
      dpConnect("IonPumpsVeloAlarmNoPlcCb",
        modeDpName + ".Beam_R.Beam",
        modeDpName + ".Beam_B.Beam",
        IP431 + ".PR",
        IP441 + ".PR");
    }
  }
  else
  {
    SetVeloAlarm("AL3", false);	// Ion pumps
  }

  // Connect to DPs responsible for Gauges alarm
  if(alarmDp != "")
  {
    dpConnect( "GaugeVeloAlarmCb",
      PE411 + ".RR1",
      PE411 + ".PR",
      PE412 + ".RR1",
      PE412 + ".PR",
      PE421 + ".RR1",
      PE421 + ".PR",
      PE422 + ".RR1",
      PE422 + ".PR",
      VPROC + ".RR1",
      alarmDp + ".alarm");
  }
  else
  {
    dpConnect( "GaugeVeloAlarmNoPlcCb",
      PE411 + ".RR1",
      PE411 + ".PR",
      PE412 + ".RR1",
      PE412 + ".PR",
      PE421 + ".RR1",
      PE421 + ".PR",
      PE422 + ".RR1",
      PE422 + ".PR",
      VPROC + ".RR1");
  }

  // Connect to DPs responsible for Turbo Pumps alarm
  if(alarmDp != "")
  {
    dpConnect( "TurboPumpVeloAlarmCb",
      TP111 + ".RR1",
      TP111 + ".SPEED",
      TP211 + ".RR1",
      TP211 + ".SPEED",
      TP301 + ".RR1",
      TP301 + ".SPEED",
      alarmDp + ".alarm");
  }
  else
  {
    dpConnect( "TurboPumpVeloAlarmNoPlcCb",
      TP111 + ".RR1",
      TP111 + ".SPEED",
      TP211 + ".RR1",
      TP211 + ".SPEED",
      TP301 + ".RR1",
      TP301 + ".SPEED");
  }
  DebugTN("vcsVeloAlarm.ctl: initialization finished");
}

//************************** Valve alarm processing *********************************
void ValveVeloAlarmCb(string valeDPE, unsigned stateRR1, string alarmDpe, int alarm)
{
  CalculateValveAlarm(stateRR1, alarm);
}
void ValveVeloAlarmNoPlcCb(string valeDPE, unsigned stateRR1)
{
  CalculateValveAlarm(stateRR1, 0);
}
void CalculateValveAlarm(unsigned stateRR1, int alarm)
{
  bool isActive = false;

  if(alarm == 0)
  {
    if((stateRR1 & SVCU_SPS_R_VALID) != 0)
    {
      if((stateRR1 & SVCU_SPS_R_OPEN) != 0)
      {
        isActive = true;
        if(debug)
        {
          DebugTN("vcsVeloAlarm.ctl: activating VALVE alarm");
        }
      }
    }
  }
  SetVeloAlarm("AL1", isActive );	// Valves
}

//************************** Ion pump alarm processing *******************************
void IonPumpsVeloAlarmCb(string beamRDpe, int beamR, string beamBDpe, int beamB,
  string vpiDpe1, float pr1,
  string vpiDpe2, float pr2,
  string alarmDpe, int alarm)
{
  CalculateIonPumpAlarm(beamR, beamB, pr1, pr2, alarm);
}
void IonPumpsVeloAlarmNoPlcCb(string beamRDpe, int beamR, string beamBDpe, int beamB,
  string vpiDpe1, float pr1, string vpiDpe2, float pr2)
{
  CalculateIonPumpAlarm(beamR, beamB, pr1, pr2, 0);
}
void CalculateIonPumpAlarm(int beamR, int beamB, float pr1, float pr2, int alarm)
{
  bool	isActive = false;

  if((beamR != 0) || (beamB != 0))
  {
    if(alarm == 0)
    {
      if((pr1 > 1.e-6) && (pr2 > 1.e-6))
      {
        if(debug)
        {
          DebugTN("vcsVeloAlarm.ctl: activating VPI alarm: pr1=" + pr1 + ", pr2=" + pr2);
        }
        isActive = true;
      }
    }
  }
  SetVeloAlarm("AL3", isActive);	// Ion pumps
}	

//************************** Gauge alarm processing *******************************
void GaugeVeloAlarmCb(string dpeState411, unsigned state411,
  string dpePressure411, float pr411,
  string dpeState412, unsigned state412,
  string dpePressure412, float pr412,
  string dpeState421, unsigned state421,
  string dpePressure421, float pr421,
  string dpeState422, unsigned state422,
  string dpePressure422, float pr422,
  string dpeProcVenting, unsigned stateProcVenting,
  string alarmDpe, int alarm)
{
  CalculateGaugeAlarm(state411, pr411, state412, pr412, state421, pr421, state422, pr422,
    stateProcVenting, alarm);
}
void GaugeVeloAlarmNoPlcCb(string dpeState411, unsigned state411,
  string dpePressure411, float pr411,
  string dpeState412, unsigned state412,
  string dpePressure412, float pr412,
  string dpeState421, unsigned state421,
  string dpePressure421, float pr421,
  string dpeState422, unsigned state422,
  string dpePressure422, float pr422,
  string dpeProcVenting, unsigned stateProcVenting)
{
  CalculateGaugeAlarm(state411, pr411, state412, pr412, state421, pr421, state422, pr422,
    stateProcVenting, 0);
}
void CalculateGaugeAlarm(unsigned state411, float pr411,
  unsigned state412, float pr412,
  unsigned state421, float pr421,
  unsigned state422, float pr422,
  unsigned stateProcVenting, int alarm)
{
  // Save last known gauge values - they will be used by turbi pump alarm calculation
  alarmGauges = alarm;
  RR1_PE411 = state411;
  PR_PE411 = pr411;
  RR1_PE412 = state412;
  PR_PE412 = pr412;
  RR1_PE421 = state421;
  PR_PE421 = pr421;
  RR1_PE422 = state422;
  PR_PE422 = pr422;
  
  // Now calculate alarm conditions
  bool isActive = false;

  if(alarm == 0)
  {
    if((stateProcVenting & (VPRO_VELO_R_READY | VPRO_VELO_R_MODE | VPRO_VELO_R_ERROR | VPRO_VELO_R_BUSY)) == 0 )	// TODO - condition to be clarified
    {
      // Analyze error code - pressure above threshold shall produce error #8 for
      // PE411 & PE412
      if(((state411 & 0xFF) == 8) || ((state412 & 0xFF) == 8))
      {
        isActive = true;
        if(debug)
        {
          DebugTN("vcsVeloAlarm.ctl: activating GAUGE alarm: state 411=" + state411 +
            ", state 412=" + state412);
        }
      }
      // Analyze error code - pressure above threshold shall produce error #16 for
      // PE421 & PE422
      if(((state421 & 0xFF) == 8) || ((state422 & 0xFF) == 8))
      {
        isActive = true;
        if(debug)
        {
          DebugTN("vcsVeloAlarm.ctl: activating GAUGE alarm: state 421=" + state421 +
            ", state 422=" + state422);
        }
      }
    }
  }
  SetVeloAlarm("AL4", isActive);	// Gauges
}

//************************** Turbo pump alarm processing ****************************
void TurboPumpVeloAlarmCb(string dpeStateTP111, unsigned stateTP111,
  string dpeSpeedTP111, float speedTP111,
  string dpeStateTP211, unsigned stateTP211,
  string dpeSpeedTP211, float speedTP211,
  string dpeStateTP301, unsigned stateTP301,
  string dpeSpeedTP301, float speedTP301,
  string dpeAlarm, int alarm)
{
  CalculateTurboPumpAlarm(stateTP111, speedTP111, stateTP211, speedTP211,
    stateTP301, speedTP301, alarm);
}
void TurboPumpVeloAlarmNoPlcCb(string dpeStateTP111, unsigned stateTP111,
  string dpeSpeedTP111, float speedTP111,
  string dpeStateTP211, unsigned stateTP211,
  string dpeSpeedTP211, float speedTP211,
  string dpeStateTP301, unsigned stateTP301,
  string dpeSpeedTP301, float speedTP301)
{
  CalculateTurboPumpAlarm(stateTP111, speedTP111, stateTP211, speedTP211,
    stateTP301, speedTP301, 0);
}
void CalculateTurboPumpAlarm(unsigned stateTP111, float speedTP111,
  unsigned stateTP211, float speedTP211,
  unsigned stateTP301, float speedTP301,
  int alarm)
{
  bool sendAlarm = false;
  bool sendWarning = false;

  if(alarm == 0)
  {
    // Alarm
    if(((RR1_PE421 & VG_SPS_R_VALID ) != 0) && ((RR1_PE422 & VG_SPS_R_VALID) != 0) &&
      (PR_PE421 < 1.e-6) && (PR_PE422 < 1.e-6))
    {
      if(((stateTP111 & VPVELO_R_VALID) != 0) && (speedTP111 < 80))
      {
        sendAlarm = true;
        if(debug)
        {
          DebugTN("vcsVeloAlarm.ctl: activating TurboPump alarm: speed TP111=" + speedTP111);
        }
      }
      else if(((stateTP211 & VPVELO_R_VALID) != 0) && (speedTP211 < 80))
      {
        sendAlarm = true;
        if(debug)
        {
          DebugTN("vcsVeloAlarm.ctl: activating TurboPump alarm: speed TP211=" + speedTP211);
        }
      }
    }
    if(((RR1_PE411 & VG_SPS_R_VALID ) != 0) && ((RR1_PE412 & VG_SPS_R_VALID) != 0) &&
      (PR_PE411 < 1.e-6) && (PR_PE412 < 1.e-6))
    {
      if(((stateTP301 & VPVELO_R_VALID) != 0) && (speedTP301 < 80))
      {
        sendAlarm = true;
        if(debug)
        {
          DebugTN("vcsVeloAlarm.ctl: activating TurboPump alarm: speed TP301=" + speedTP301);
        }
      }
    }
    // Warning
    if(((stateTP111 & VPVELO_R_VALID) != 0) && (speedTP111 < 50))
    {
      if(((stateTP211 & VPVELO_R_VALID) != 0) && (speedTP211 < 50))
      {
        if(((stateTP301 & VPVELO_R_VALID) != 0) && (speedTP301 < 50))
        {
          sendWarning = true;
          if(debug)
          {
            DebugTN("vcsVeloAlarm.ctl: activating TurboPump warning: TP111=" + speedTP301 +
              ", TP211=" + speedTP211 + ", TP301=" + speedTP301);
          }
        }
      }
    }
  }
  SetVeloAlarm("AL2", sendAlarm);	// Turbo pumps alarm
  SetVeloAlarm("AL5", sendWarning);	// Turbo pumps warning
}

//****************************** Write calculated alarm state to given alarm DPE
void SetVeloAlarm(string dpeName, bool isActive)
{
  dyn_errClass 	err;

  dpSetWait(LHC_VAC_VELO_ALARM_DP + "." + dpeName, isActive);
  err = getLastError();
  if(dynlen(err) > 0)
  {
    DebugTN( "vcsVeloAlarm: dpSetWait(" + LHC_VAC_VELO_ALARM_DP + "." + dpeName +
      ") failed:  " + err);
  }
}
