
// Filter LHC beam intensity value depending on bits of DIP-sorced
// dip/acc/LHC/RunControl/SafeBeam - see
// http://wikis.cern.ch/display/expcomm/SafeBeam
//
//  L.Kopylov 15.06.2012

// Last known value of SafeBeam
unsigned safeBeam;

// Last known values of intensities
float blueFast;
float redFast;
float blueDc;
float redDc;

// Flag indicating if initialization is over
bool initialized = false;

// Definition of bits is SafeBeam, taken from http://wikis.cern.ch/display/expcomm/SafeBeam
// Only bits relevant for intestity filtering task are defined here
const unsigned BIT_BEAM1_PRESENT = 0x01;
const unsigned BIT_BEAM2_PRESENT = 0x10;

main()
{
  if(ConnectToSource("DIP_SafeBeam.ValueInt", "SafeBeamCb") == 0)
  {
    DebugTN("vcsLhcIntensityFilter.ctl: no SafeBeam DPE, giving up...");
    return;
  }
  int nConnected = ConnectToSource("BCTFR_A6R4_B1.Value", "BlueFastCb");
  nConnected += ConnectToSource("BCTFR_A6R4_B2.Value", "RedFastCb");
  nConnected += ConnectToSource("BCTDC_A6R4_B1.Value", "BlueDcCb");
  nConnected += ConnectToSource("BCTDC_A6R4_B2.Value", "RedDcCb");
  DebugTN("vcsLhcIntensityFilter.ctl: initialization is done, connected to " +
          nConnected + " DPE(s)");
  if(nConnected == 0)
  {
    dpDisconnect("SafeBeamCb", "DIP_SafeBeam.ValueInt");
  }
  else
  {
    initialized = true;
    safeBeam = BIT_BEAM1_PRESENT | BIT_BEAM2_PRESENT;
  }
}

int ConnectToSource(string dpe, string callback)
{
  int result = 0;
  if(dpExists(dpe))
  {
    dpConnect(callback, dpe);
    result = 1;
  }
  else
  {
    DebugTN("vcsLhcIntensityFilter.ctl: DPE <" + dpe + "> does not exist");
  }
  return result;
}

// Callbacks

void SafeBeamCb(string dpe, unsigned value)
{
  if(initialized)
  {
    if(value == safeBeam)
    {
      return;
    }
    if((value & BIT_BEAM1_PRESENT) == 0)
    {
      if((safeBeam & BIT_BEAM1_PRESENT) != 0)
      {
        dpSet("BEAM_B.Intensity", 0,
              "BEAM_DC_B.Intensity", 0);
      }
    }
    else if((safeBeam & BIT_BEAM1_PRESENT) == 0)
    {
      dpSet("BEAM_B.Intensity", blueFast,
            "BEAM_DC_B.Intensity", blueDc);
    }
    if((value & BIT_BEAM2_PRESENT) == 0)
    {
      if((safeBeam & BIT_BEAM2_PRESENT) != 0)
      {
        dpSet("BEAM_R.Intensity", 0,
              "BEAM_DC_R.Intensity", 0);
      }
    }
    else if((safeBeam & BIT_BEAM2_PRESENT) == 0)
    {
      dpSet("BEAM_R.Intensity", redFast,
            "BEAM_DC_R.Intensity", redDc);
    }
  }
  safeBeam = value;
}

void BlueFastCb(string dpe, float value)
{
  blueFast = value;
  if((safeBeam & BIT_BEAM1_PRESENT) == 0)
  {
    return;
  }
  dpSet("BEAM_B.Intensity", value);
}

void RedFastCb(string dpe, float value)
{
  redFast = value;
  if((safeBeam & BIT_BEAM2_PRESENT) == 0)
  {
    return;
  }
  dpSet("BEAM_R.Intensity", value);
}

void BlueDcCb(string dpe, float value)
{
  blueDc = value;
  if((safeBeam & BIT_BEAM1_PRESENT) == 0)
  {
    return;
  }
  dpSet("BEAM_DC_B.Intensity", value);
}

void RedDcCb(string dpe, float value)
{
  redDc = value;
  if((safeBeam & BIT_BEAM2_PRESENT) == 0)
  {
    return;
  }
  dpSet("BEAM_DC_R.Intensity", value);
}

