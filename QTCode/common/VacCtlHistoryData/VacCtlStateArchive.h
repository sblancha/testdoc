#ifndef	VACCTLSTATEARCHIVE_H
#define	VACCTLSTATEARCHIVE_H

#include "VacCtlHistoryDataExport.h"

//	Data pool for state archive processing

#include "DpValuesSet.h"

class VACCTLHISTORYDATA_EXPORT VacCtlStateArchive
{
public:
	static int create(void);
	static VacCtlStateArchive *findInstance(int id);
	static bool deleteInstance(int id);

	~VacCtlStateArchive();	// Public to allow QPtrList to delete instances

	void clear(void);
	void append(const char *dpName, const char *dpeName, const QDateTime &ts,
		const QVariant &value);
	void sort(void);
	const QDateTime **getAllTs(int &count) const { count = nAllTs; return allTs; }
	bool getDpValues(const char *dpName, const QDateTime &ts, QList<QByteArray> &dpeNames,
		QList<QVariant *> &values);
	int getPreviousTs(const char *dpName, const QDateTime &ts, QDateTime &prevTs,
		QString &errMsg);
	int getNextTs(const char *dpName, const QDateTime &ts, QDateTime &nextTs,
		QString &errMsg);
	int getNeighbourIndexes(const char *dpName, const QDateTime &ts,
		int &prevIdx, int &nextIdx, QString &errMsg);

	// Access
	inline int getId(void) const { return id; }

protected:
	// List of instances
	static QList<VacCtlStateArchive *>	*pInstances;

	// ID of instance, used by PVSS to identify instance used by particular panel
	int						id;

	// List of values for DPS
	QList<DpValuesSet *>	*pDps;

	// Array of all timestamps related to all DPEs of all DPs, times are
	// in increasing order in this array. Note array contains POINTERS while
	// real values are sitting in every single DPE
	const QDateTime			**allTs;

	// Number of timestamps in array
	int						nAllTs;

	VacCtlStateArchive(int id);

	int getNextTs(DpValuesSet *pDp, const QDateTime &ts, QDateTime &nextTs);
	int getPreviousTs(DpValuesSet *pDp, const QDateTime &ts, QDateTime &prevTs);

	DpValuesSet	*findDp(const char *dpName);
	int findTsIndex(const QDateTime &ts);
};

#endif	// VACCTLSTATEARCHIVE_H
