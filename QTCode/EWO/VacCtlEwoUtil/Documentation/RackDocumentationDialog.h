#ifndef RACKDIALOG_H
#define RACKDIALOG_H

#include <QDialog>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qmenu.h>

#include <QScrollArea>


class RackView;
class Rack;

class RackDocumentationDialog : public QDialog
{
	Q_OBJECT
public:
	//RackDocumentationDialog(Rack * rack); legacy constructor
	RackDocumentationDialog(QString buildingName);
	~RackDocumentationDialog();

	int create();
	void showAllRacks();
	void reCalculateSize();
	void selectEquipment(const char * equipmentName);
protected:
	RackView * pView;
	//building defines which racks get drawn
	QString buildingName;
	// Button for displaying 'File' menu
	QPushButton * pFilePb; 
	// Button for displaying 'Help' menu
	QPushButton * pHelpPb;

	// 'File' menu
	QMenu * pFileMenu;

	// 'Help' menu
	QMenu * pHelpMenu;

	// Label with synoptic title
	QLabel * pTitleLabel;

	// Scroll view holding synoptic image and icons
	QScrollArea * pScroll;

	//functions
	void buildLayout(void);
	void buildFileMenu(void);
	void buildHelpMenu(void);
	void buildView(bool init);

	void setTitle(void);
	void setMyWidth(bool init);
private slots :
	void viewMove(int x);
	void filePrint();

};
#endif