#ifndef	VACICONVGP_PS_CMW_H
#define	VACICONVGP_PS_CMW_H

//	Icon for VGP

#include "VacIconVG_PS_CMW.h"

class VACCTLEWOUTIL_EXPORT VacIconVGP_PS_CMW : public VacIconVG_PS_CMW
{
public:
	VacIconVGP_PS_CMW(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG_PS_CMW(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGP_PS_CMW_H
