#ifndef	DATAMODEEDITORWIDGET_H
#define	DATAMODEEDITORWIDGET_H

// Widget for selecting DataEnum::DataMode for device list acquisition

#include <QWidget>

#include <QRadioButton>
#include <QDateTimeEdit>
#include <QDateTime>

class DataModeEditorWidget : public QWidget
{
	Q_OBJECT

public:
	DataModeEditorWidget(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~DataModeEditorWidget();

signals:
	void modeChanged(void);
	void timeChanged(void);

protected:
	// Radio button for online mode
	QRadioButton 	*pOnlineRb;

	// Radio button for replay mode
	QRadioButton	*pReplayRb;

	// Editor for date+time in the past
	QDateTimeEdit	*pTimeEditor;

	bool				setting;

private slots:
	void onlineToggled(bool on);
	void replayToggled(bool on);
	void timeChanged(const QDateTime &time);
};

#endif	// DATAMODEEDITORWIDGET_H
