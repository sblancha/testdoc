//	Implementation of HistoryImage class
/////////////////////////////////////////////////////////////////////////////////

#include "HistoryImage.h"
#include "PlotImage.h"

#include "HistoryProcessor.h"

#include "VacType.h"

#include "VacMainView.h"

#include <QPixmap>
#include <QLabel>
#include <QTimer>
#include <QMenu>
#include <QCursor>
#include <QMessageBox>
#include <QPaintEvent>
#include <QApplication>

#include <QToolButton>
#include <QLayout>

#include <math.h>

#include "PlatformDef.h"
#include "DebugCtl.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction


HistoryImage::HistoryImage(QWidget *parent) : QWidget(parent, 0)
{
	redrawing = false;
	drawnOnce = false;
	pDrawThread = new QThread(this);
	connect(pDrawThread, SIGNAL(started()), this, SLOT(doDrawWork()));

	if(VacMainView::getInstance())
	{
		setFont(VacMainView::getInstance()->font());
	}
	setMinimumWidth(500);
	setMinimumHeight(350);

	// Vertical layout: image + vertical axies on top, time axis on bottom
	QVBoxLayout *pMainLayout = new QVBoxLayout(this);
	pMainLayout->setContentsMargins(0, 0, 0, 0);
	pMainLayout->setSpacing(0);

	pLayout = new QHBoxLayout();
	pLayout->setSpacing(0);
	pMainLayout->addLayout(pLayout, 10);	// Takes all available height

	pImage = new PlotImage(this);
	pLayout->addWidget(pImage, 10);	// Takes all available width
	connect(pImage, SIGNAL(resized(void)), this, SLOT(imageResized(void)));
	connect(pImage, SIGNAL(pointerMoved(int)), this, SLOT(imagePointerMoved(int)));
	connect(pImage, SIGNAL(zoomRectFinished(const QRect &)), this, SLOT(imageZoomFinished(const QRect &)));

	pTimeAxis = new TimeAxisWidget(this);
	pTimeAxis->show();
	pMainLayout->addWidget(pTimeAxis);
	connect(pTimeAxis, SIGNAL(limitsChanged(bool)), this, SLOT(scaleChanged(bool)));
	filterType = HistoryDataRange::FilterTypeMinMax;
	
	setMouseTracking(true);	// TODO: image!!!

	pLabel = new QLabel(this);
	QColor labelBg(255, 255, 204);
	QPalette palette = pLabel->palette();
	palette.setColor(QPalette::Window, labelBg);
	pLabel->setPalette(palette);
	pLabel->setAutoFillBackground(true);
	pLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	pLabel->hide();


	pCurrentEqp = NULL;

//	lastSelected = lastSelectedRight = DataEnum::ValueTypeNone;

	colors.append(new QColor(Qt::cyan));
	colors.append(new QColor(Qt::red));
	colors.append(new QColor(Qt::blue));
	colors.append(new QColor(Qt::darkGreen));
	colors.append(new QColor(Qt::yellow));
	colors.append(new QColor(Qt::magenta));

	online = true;
	depth = 4 * 60;

	pTimer = new QTimer(this);
	pTimer->setSingleShot(true);
	connect(pTimer, SIGNAL(timeout(void)), this, SLOT(redrawTimerDone(void)));

	pAliveTimer = new QTimer(this);
	pAliveTimer->setSingleShot(false);
	connect(pAliveTimer, SIGNAL(timeout(void)), this, SLOT(aliveTimerDone(void)));
	pAliveTimer->start(1000);

	// Finally...
	rebuildImage(0);
}

HistoryImage::~HistoryImage()
{
	while(!eqps.isEmpty())
	{
		delete eqps.takeFirst();
	}
	while(!colors.isEmpty())
	{
		delete colors.takeFirst();
	}
}

/////////////////////////////////////////////////////////////////////////////////
// Event processing
/////////////////////////////////////////////////////////////////////////////////
void HistoryImage::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("HistoryImage::resizeEvent()\n");
#else
		printf("HistoryImage::resizeEvent()\n");
		fflush(stdout);
#endif
	}
// L.Kopylov 07.11.2011	rebuildImage();
	rebuildWithDelay();
	if(DebugCtl::isHistory())
	{
		QRect area(rect());
#ifdef Q_OS_WIN
		qDebug("This: %d %d (%d x %d)\n", area.left(), area.top(), area.width(), area.height());
#else
		printf("This: %d %d (%d x %d)\n", area.left(), area.top(), area.width(), area.height());
#endif
		if(pImage)
		{
			area = pImage->rect();
#ifdef Q_OS_WIN
			qDebug("Image: %d %d (%d x %d)\n", area.left(), area.top(), area.width(), area.height());
#else
			printf("Image: %d %d (%d x %d)\n", area.left(), area.top(), area.width(), area.height());
#endif
		}
		if(pTimeAxis)
		{
			area = pTimeAxis->rect();
#ifdef Q_OS_WIN
			qDebug("Time Axis: %d %d (%d x %d)\n", area.left(), area.top(), area.width(), area.height());
#else
			printf("Time Axis: %d %d (%d x %d)\n", area.left(), area.top(), area.width(), area.height());
#endif
		}
		for(int idx = 0 ; idx < axisList.count() ; idx++)
		{
			VerticalAxisWidget *pAxis = axisList.at(idx);
			area = pAxis->rect();
#ifdef Q_OS_WIN
			qDebug("Vert Axis[%d]: %d %d (%d x %d)\n", pAxis->getValueType(), area.left(), area.top(), area.width(), area.height());
#else
			printf("Vert Axis[%d]: %d %d (%d x %d)\n", pAxis->getValueType(), area.left(), area.top(), area.width(), area.height());
#endif
		}
#ifdef Q_OS_WIN
		qDebug("-------------------------------------------------------------\n");
#else
		printf("-------------------------------------------------------------\n");
		fflush(stdout);
#endif
	}
}

void HistoryImage::setFilterType(HistoryDataRange::FilterType newType)
{
	if(filterType == newType)
	{
		return;
	}
	filterType = newType;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		HistoryEqpData *pEqp = eqps.at(idx);
		pEqp->setFilterType(newType);
	}
	rebuildImage(2);
}

/*
**	FUNCTION
**		Get parameters of all vertical scales
**
**	ARGUMENTS
**		nScales	- Number of vertical scales is returned here
**		ppTypes	- Pointer to array of scale types is returned here
**		ppLogs	- Pointer to array of log/linear flags for scales is returned here
**		ppMins	- Pointer to array of scale minimums is returned here
**		ppMaxs	- Pointer to array of scale maximums is returned here
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::getVerticalScalesParams(int &nScales, int **ppTypes, bool **ppLogs, float **ppMins, float **ppMaxs)
{
	nScales = 0;
	*ppTypes = NULL;
	*ppLogs = NULL;
	*ppMins = NULL;
	*ppMaxs = NULL;
	for(int idx = 0 ; idx < axisList.count() ; idx++)
	{
		VerticalAxisWidget *pAxis = axisList.at(idx);
		if(pAxis->getValueType() == DataEnum::BitState)
		{
			continue;	// Scale is always fixed
		}
		if(!(nScales & 0xF))
		{
			*ppTypes = (int *)realloc((void *)(*ppTypes), (nScales + 16) * sizeof(int));
			*ppLogs = (bool *)realloc((void *)(*ppLogs), (nScales + 16) * sizeof(bool));
			*ppMins = (float *)realloc((void *)(*ppMins), (nScales + 16) * sizeof(float));
			*ppMaxs = (float *)realloc((void *)(*ppMaxs), (nScales + 16) * sizeof(float));
		}
		(*ppTypes)[nScales] = pAxis->getValueType();
		(*ppLogs)[nScales] = pAxis->isLogScale();
		float useMin, useMax;
		pAxis->getLimits(useMin, useMax);
		(*ppMins)[nScales] = useMin;
		(*ppMaxs)[nScales] = useMax;
		nScales++;
	}
}

/*
**	FUNCTION
**		Set parameters for vertical scale of given type
**
**	ARGUMENTS
**		type	- Scale type
**		log		- Log/linear flag for scale
**		min		- Scale minimum
**		max		- Scale maximum
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::setVerticalScaleParams(int type, bool log, float min, float max)
{
	if(type == DataEnum::BitState)
	{
		return;	// Scale is always fixed
	}
	VerticalAxisWidget *pAxis = findAxisForValueType((DataEnum::ValueType) type);
	if(!pAxis)
	{
		return;
	}
	if(pAxis->isLogScale() != log)
	{
		pAxis->setLogScale(log);
	}
	float useMin, useMax;
	pAxis->getLimits(useMin, useMax);
	if((useMin != min) || (useMax != max))
	{
		pAxis->setLimits(min, max);
		pAxis->setInitialLimitsSet(true);
	}
}


/*
**	FUNCTION
**		Add new device to history if it is not there yet
**
**	PARAMETERS
**		pEqp			- Pointer to device to be added
**		historyDepth	- History depth [sec] for this device
**		color			- Variable where color for new device will be written
**		useInputColor	- true if input color shall be used, NOT calculated automatically
**
**	RETURNS
**		1	= device has been added,
**		0	= device is already in history
**
**	CAUTIONS
**		None
**
*/
int HistoryImage::addEqp(Eqp *pEqp, int historyDepth, QColor &color, bool useInputColor)
{
	HistoryEqpData *pItem;
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		pItem = eqps.at(idx);
		if((pItem->getEqp() == pEqp) && (!pItem->isBitState()))
		{
			return 0;	// Already exists
		}
	}

	// Find axis for new device, create if does not exist yet
	VerticalAxisWidget *pAxis = findAxisForValueType(pEqp->getMainValueType());
	if(!pAxis)
	{
		pAxis = new VerticalAxisWidget(pEqp->getMainValueType(), this);
		if(pEqp->getMainValueType() == DataEnum::Pressure)
		{
			pLayout->insertWidget(0, pAxis);
		}
		else
		{
			pLayout->addWidget(pAxis);
		}
		axisList.append(pAxis);
		pAxis->show();
		connect(pAxis, SIGNAL(limitsChanged(bool)), this, SLOT(scaleChanged(bool)));
		updateGeometry();
	}

	QDateTime startTime = QDateTime::currentDateTime();
	QDateTime absStartTime(startTime.addSecs(-historyDepth));
	pItem = new HistoryEqpData(pEqp, absStartTime);
	pItem->setFilterType(filterType);
	pItem->setValueCoeff(pAxis->getValueCoeff());
	connect(pItem, SIGNAL(onlineValueArrived(void)), this, SLOT(onlineValueArrived(void)));
	eqps.append(pItem);
	
	// For beam intensities always use color corresponding to beam type
	if(!useInputColor)
	{
		switch(pEqp->getMainValueType())
		{
		case DataEnum::Intensity:
		case DataEnum::BeamCurrent:
		case DataEnum::PhF:
		case DataEnum::PhDm:
		case DataEnum::PhDmA:
			switch(pEqp->getVacType())
			{
			case VacType::RedBeam:
				color.setRgb(255, 0, 0);
				break;
			case VacType::BlueBeam:
				color.setRgb(0, 0, 255);
				break;
			default:
				color = findNewColor();
				break;
			}
			break;
		case DataEnum::Energy:
		case DataEnum::CritEnergy:
			color.setRgb(0, 0, 0);	// Always black
			break;
		default:
			color = findNewColor();
			break;
		}
	}
	pItem->setColor(color);

	// Set time limits for new device - this will force device to query archived
	// data for itself. First set limits for the date of instance creation...
	startTime = pItem->getCreated();
	QTime time(0, 0, 0);
	startTime.setTime(time);
	pItem->setTimeLimits(startTime, pItem->getCreated());

	// ...then set currently displayed limits
	startTime = pTimeAxis->getMin();
	QDateTime endTime = pTimeAxis->getMax();
	if(endTime > pItem->getCreated())
	{
		endTime = pItem->getCreated();
	}
	if(startTime < endTime)
	{
		pItem->setTimeLimits(startTime, endTime);
	}

	// Finally force redrawing of image
	// Not required, and even bad??? L.Kopylov 07.01.2011 rebuildImage();
	return 1;
}

/*
**	FUNCTION
**		Add new Dpe device to history if it is not there yet
**
**	PARAMETERS
**		pEqp			- Pointer to device to be added
**		dpeName			- extesnion name of the dpe
**		yAxisName		- Vertical axis requested
**		historyDepth	- History depth [sec] for this device
**		color			- Variable where color for new device will be written
**		useInputColor	- true if input color shall be used, NOT calculated automatically
**
**	RETURNS
**		1	= device has been added,
**		0	= device is already in history
**
**	CAUTIONS
**		None
**
*/
int HistoryImage::addDpe(Eqp *pEqp, const char *dpeName, const char *yAxisName, int historyDepth, QColor &color, bool useInputColor)
{
	HistoryEqpData *pItem;
	QString fullDpeName(pEqp->getDpName());
	fullDpeName += ".";
	fullDpeName += dpeName;

	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		pItem = eqps.at(idx);
		if((pItem->getEqp() == pEqp) && (pItem->getDpeName() == fullDpeName))

		{
			return 0;	// Already exists
		}
	}

	// Find axis for new device, create if does not exist yet
	VerticalAxisWidget *pAxis;
	QString yAxis(yAxisName);
	DataEnum::ValueType yAxisType = DataEnum::Pressure;	// Most often used
	if(yAxis == "Pressure")
	{
		yAxisType = DataEnum::Pressure; //DataEnum::ValueType Pressure = 1
	}
	else if(yAxis == "HighPressure")
	{
		yAxisType = DataEnum::HighPressure; //DataEnum::ValueType HighPressure = 18
	}
	else if(yAxis == "CryoTemperature")
	{
		yAxisType = DataEnum::CryoTemperature; //DataEnum::ValueType CryoTemperature = 2
	}
	else if(yAxis == "Temperature")
	{
		yAxisType = DataEnum::Temperature; //DataEnum::ValueType Temperature = 4
	}
	else if(yAxis == "Bit")
	{
		yAxisType = DataEnum::BitState; //DataEnum::ValueType BitState = 11 [0 or 1]
	}
	else if(yAxis == "Current")
	{
		yAxisType = DataEnum::CurrentVRPM; //DataEnum::ValueType CurrentVRPM = 12
	}
	else if(yAxis == "Voltage")
	{
		yAxisType = DataEnum::VoltageVRPM; //DataEnum::ValueType VoltageVRPM = 13
	}
	else if(yAxis == "HighVoltage")
	{
		yAxisType = DataEnum::HighVoltage; //DataEnum::ValueType HighVoltage = 20
	}
	else if(yAxis == "Percentage")
	{
		yAxisType = DataEnum::Percentage; //DataEnum::ValueType Percentage = 16
	}
	else if(yAxis == "LowCurrent")
	{
		yAxisType = DataEnum::LowCurrent; //DataEnum::ValueType LowCurrent = 17
	}
	else if (yAxis == "Status")
	{
		yAxisType = DataEnum::Status;
	}
    else if (yAxis == "mA")
    {
        yAxisType = DataEnum::mA; //DataEnum::ValueType mA = 21
    }
	pAxis = findAxisForValueType(yAxisType);
	if(!pAxis)
	{
		pAxis = new VerticalAxisWidget(yAxisType, this);
		if(yAxisType == DataEnum::Pressure)
		{
			pLayout->insertWidget(0, pAxis);
		}
		else
		{
			pLayout->addWidget(pAxis);
			//These vertical axis types will have Linear scale by default
			if((yAxisType == DataEnum::BitState) || (yAxisType == DataEnum::Percentage)){ 
				pAxis->setLogScale(false);
			}
			
		}
		calculateVerticalLimits();
		axisList.append(pAxis);
		pAxis->show();
		connect(pAxis, SIGNAL(limitsChanged(bool)), this, SLOT(scaleChanged(bool)));
		updateGeometry();
	}

	QDateTime startTime = QDateTime::currentDateTime();
	QDateTime absStartTime(startTime.addSecs(-historyDepth));
	pItem = new HistoryEqpData(pEqp, dpeName, yAxisType, yAxis, absStartTime);//HistoryEqpData(Eqp *pEqp, const char *dpeSuffixName, DataEnum::ValueType dpeValType, const QDateTime &absStartTime)
	pItem->setFilterType(filterType);
	pItem->setValueCoeff(pAxis->getValueCoeff());
	connect(pItem, SIGNAL(onlineValueArrived(void)), this, SLOT(onlineValueArrived(void)));
	eqps.append(pItem);
	// For beam intensities always use color corresponding to beam type
	if(!useInputColor)
	{
		color = findNewColor();
	}
	pItem->setColor(color);

	// Set time limits for new device - this will force device to query archived
	// data for itself. First set limits for the date of instance creation...
	startTime = pItem->getCreated();
	QTime time(0, 0, 0);
	startTime.setTime(time);
	pItem->setTimeLimits(startTime, pItem->getCreated());

	// ...then set currently displayed limits
	startTime = pTimeAxis->getMin();
	QDateTime endTime = pTimeAxis->getMax();
	if(endTime > pItem->getCreated())
	{
		endTime = pItem->getCreated();
	}
	if(startTime < endTime)
	{
		pItem->setTimeLimits(startTime, endTime);
	}

	// Finally force redrawing of image
	// Not required, and even bad??? L.Kopylov 07.01.2011 rebuildImage();
	return 1;
}

/*
**	FUNCTION
**		Add new bit of device to history if it is not there yet
**
**	PARAMETERS
**		pEqp			- Pointer to device to be added
**		historyDepth	- History depth [sec] for this device
**		color			- Variable where color for new device will be written
**		useInputColor	- true if input color shall be used, NOT calculated automatically
**
**	RETURNS
**		1	= device has been added,
**		0	= device is already in history
**
**	CAUTIONS
**		None
**
*/
int HistoryImage::addEqpBit(Eqp *pEqp, const char *dpe, int bitNbr, const char *bitName, int historyDepth, QColor &color, bool useInputColor)
{
	HistoryEqpData *pItem;
	QString dpeName(pEqp->getDpName());
	dpeName += ".";
	dpeName += dpe;
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		pItem = eqps.at(idx);
//qDebug("HistoryImage::addEqpBit(): check %s %s %d isBit %d\n", pItem->getEqp()->getDpName(), pItem->getDpeName().constData(), pItem->getBitNbr(), (int)pItem->isBitState());
		if((pItem->getEqp() == pEqp) && pItem->isBitState() && (pItem->getDpeName() == dpeName) && (pItem->getBitNbr() == bitNbr))
		{
			return 0;	// Already exists
		}
	}

	// Find axis for new device, create if does not exist yet
	VerticalAxisWidget *pAxis = findAxisForValueType(DataEnum::BitState);
	if(!pAxis)
	{
		pAxis = new VerticalAxisWidget(DataEnum::BitState, this);
		pLayout->addWidget(pAxis);
		axisList.append(pAxis);
		pAxis->show();
		// No limits change for bit axis connect(pAxis, SIGNAL(limitsChanged(bool)), this, SLOT(scaleChanged(bool)));
		updateGeometry();
	}

	QDateTime startTime = QDateTime::currentDateTime();
	QDateTime absStartTime(startTime.addSecs(-historyDepth));
	pItem = new HistoryEqpData(pEqp, dpe, bitNbr, bitName, absStartTime);
	pItem->setFilterType(filterType);
	connect(pItem, SIGNAL(onlineValueArrived(void)), this, SLOT(onlineValueArrived(void)));
	eqps.append(pItem);

	if(!useInputColor)
	{
		color = findNewColor();
	}
	pItem->setColor(color);

	// Set time limits for new device - this will force device to query archived
	// data for itself. First set limits for the date of instance creation...
	startTime = pItem->getCreated();
	QTime time(0, 0, 0);
	startTime.setTime(time);
	pItem->setTimeLimits(startTime, pItem->getCreated());

	// ...then set currently displayed limits
	startTime = pTimeAxis->getMin();
	QDateTime endTime = pTimeAxis->getMax();
	if(endTime > pItem->getCreated())
	{
		endTime = pItem->getCreated();
	}
	if(startTime < endTime)
	{
		pItem->setTimeLimits(startTime, endTime);
	}

	// Finally force redrawing of image
	// Not required, and even bad??? L.Kopylov 07.01.2011 rebuildImage();
	return 1;
}

/*
**	FUNCTION
**		Find color for last device in list
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Color for last device
**
**	CAUTIONS
**		None
*/
const QColor HistoryImage::findNewColor(void)
{
	int index = eqps.count() % colors.count();
	return QColor(*(colors.at(index)));
}

/*
**	FUNCTION
**		Find axis for given value type
**
**	PARAMETERS
**		valueType	- Value type to find axis for
**
**	RETURNS
**		Pointer to axis widget for this value type; or
**		NULL if there is no axis for this value type
**
**	CAUTIONS
**		None
*/
VerticalAxisWidget *HistoryImage::findAxisForValueType(DataEnum::ValueType valueType)
{
	for(int idx = axisList.count() - 1 ; idx >= 0 ; idx--)
	{
		VerticalAxisWidget *pAxis = axisList.at(idx);
		if(pAxis->getValueType() == valueType)
		{
			return pAxis;
		}
	}
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("Axis for value type %d not found\n", (int)valueType);
#else
		printf("Axis for value type %d not found\n", (int)valueType);
		fflush(stdout);
#endif
	}
	return NULL;
}


/*
**	FUNCTION
**		Find maximum history depth [sec] for all devices in histort
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Maximum history depth
**
**	CAUTIONS
**		None
*/
int HistoryImage::getMaximumDepth(void)
{
	int result = 0;
	QDateTime now(QDateTime::currentDateTime());
	for(int idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		HistoryEqpData *pItem = eqps.at(idx);
		int eqpDepth = pItem->getAbsStartTime().secsTo(now);
		if(eqpDepth > result)
		{
			result = eqpDepth;
		}
	}
	return result;
}

/*
**	FUNCTION
**		Set history to online mode
**
**	PARAMETERS
**		flag	- New online mode flag
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::setOnline(bool flag)
{
	online = flag;
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("HistoryImage::setOnline()\n");
#else
		printf("HistoryImage::setOnline()\n");
		fflush(stdout);
#endif
	}
	rebuildImage(3);
}

/*
**	FUNCTION
**		Set depth of history
**
**	PARAMETERS
**		value	- New history depth [minutes]
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::setDepth(int value)
{
	depth = value;
	QDateTime timeMin(pTimeAxis->getMin()),
		timeMax(pTimeAxis->getMax());
	if(online)	// always extend to the past
	{
		timeMin = timeMax.addSecs(-depth * 60);
	}
	else	// Extend to the future whenever possible
	{
		timeMax = timeMin.addSecs(depth * 60);
		QDateTime now(QDateTime::currentDateTime());
		if(timeMax > now)
		{
			timeMax = now;
			timeMin = timeMax.addSecs(-depth * 60);
		}
	}
	pTimeAxis->setLimits(timeMin, timeMax);

	// Notify all devices about time limits change so that will query for
	// archived data if necessary
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		HistoryEqpData *pItem = eqps.at(idx);
		QDateTime endTime = timeMax > pItem->getCreated() ? pItem->getCreated() : timeMax;
		if(timeMin < endTime)
		{
			pItem->setTimeLimits(timeMin, endTime);
		}
	}

	// Finally redraw the image
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("HistoryImage::setDepth()\n");
#else
		printf("HistoryImage::setDepth()\n");
		fflush(stdout);
#endif
	}
	rebuildImage(4);
}

/*
**	FUNCTION
**		Set new time limits
**
**	PARAMETERS
**		start	- New start time
**		end		- New end time
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::setTimeLimits(const QDateTime &start, const QDateTime &end)
{
	pTimeAxis->setLimits(start, end);
	depth = start.secsTo(end) / 60;

	// Notify all devices about time limits change so that will query for
	// archived data if necessary
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		HistoryEqpData *pItem = eqps.at(idx);
		QDateTime endTime = end > pItem->getCreated() ? pItem->getCreated() : end;
		if(start < endTime)
		{
			pItem->setTimeLimits(start, endTime);
		}
	}

	// Finally redraw the image
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("HistoryImage::setTimeLimits()\n");
#else
		printf("HistoryImage::setTimeLimits()\n");
		fflush(stdout);
#endif
	}
	rebuildImage(5);
}

/*
**	FUNCTION
**		Add one archived value to given range of given DPE
**
**	PARAMETERS
**		id		- Range ID
**		dpe		- DPE name
**		bitState	- Flag indicating if this is value for bit state DPE
**		bitNbr	- Bit number for bit state history
**		time	- Value's timestamp
**		value	- Value from archive
**
**	RETURNS
**		true	- If such DPE has been found;
**		false	- DPE is not found, hence, further data for it are not needed
**
**	CAUTIONS
**		None
*/
bool HistoryImage::addHistoryValue(int id, const char *dpe, bool bitState, int bitNbr, const QDateTime &time, const QVariant &value)
{
	if(pCurrentEqp)
	{
		if((pCurrentEqp->getDpeName() == dpe)
			&& (pCurrentEqp->isBitState() == bitState)
			&& (pCurrentEqp->getBitNbr() == bitNbr))
		{
			pCurrentEqp->addHistoryValue(id, time, value);
			return true;
		}
	}
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		pCurrentEqp = eqps.at(idx);
		if((pCurrentEqp->getDpeName() == dpe)
			&& (pCurrentEqp->isBitState() == bitState)
			&& (pCurrentEqp->getBitNbr() == bitNbr))
		{
			pCurrentEqp->addHistoryValue(id, time, value);
			return true;
		}
	}
	return false;
}

/*
**	FUNCTION
**		Notify that all data for one range of one DPE have been added
**
**	PARAMETERS
**		id		- Range ID
**		dpe		- DPE name
**		bitState	- Flag indicating if this is value for bit state DPE
**		bitNbr	- Bit number for bit state history
**
**	RETURNS
**		true	- If such DPE has been found;
**		false	- DPE is not found, hence, further data for it are not needed
**
**	CAUTIONS
**		None
*/
void HistoryImage::finishHistoryRange(int id, const char *dpe, bool bitState, int bitNbr)
{
	if(pCurrentEqp)
	{
		if((pCurrentEqp->getDpeName() == dpe)
			&& (pCurrentEqp->isBitState() == bitState)
			&& (pCurrentEqp->getBitNbr() == bitNbr))
		{
			pCurrentEqp->finishHistoryRange(id);
			return;
		}
	}
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		pCurrentEqp = eqps.at(idx);
		if((pCurrentEqp->getDpeName() == dpe)
			&& (pCurrentEqp->isBitState() == bitState)
			&& (pCurrentEqp->getBitNbr() == bitNbr))
		{
			pCurrentEqp->finishHistoryRange(id);
			break;
		}
	}
}

/*
**	FUNCTION
**		Show label, displaying time range when dragging scroll bar - that
**		range will be set after scroll bar slider is released.
**
**	PARAMETERS
**		start	- New start time
**		end		- New end time
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::showNextRange(const QDateTime &start, const QDateTime &end)
{
	QString text = start.toString("yyyy-MM-dd hh:mm:ss");
	text += "   ";
	text += end.toString("yyyy-MM-dd hh:mm:ss");
	pLabel->setText(text);
	if(pLabel->isHidden())
	{
		pLabel->adjustSize();
		pLabel->move(width() / 2 - pLabel->width() / 2, height() - pLabel->height());
		pLabel->show();
	}
}

/*
**	FUNCTION
**		Hide label, displaying time range when dragging scroll bar.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::hideNextRange(void)
{
	pLabel->hide();
}

/*
**	FUNCTION
**		Find DP name of device with given index
**
**	PARAMETERS
**		index	- device index
**
**	RETURNS
**		Pointer to DP name; or
**		NULL in case of wrong index
**
**	CAUTIONS
**		None
*/
const char *HistoryImage::getDpName(int index)
{
	HistoryEqpData *pItem = eqps.at(index);
	if(pItem)
	{
		return pItem->getEqp()->getDpName();
	}
	return NULL;
}

/*
**	FUNCTION
**		Find index of given device in list of devices
**
**	PARAMETERS
**		pEqp	- Pointer to device to be found
**
**	RETURNS
**		Index of device in list; or
**		-1 if device not found
**
**	CAUTIONS
**		None
*/
int HistoryImage::getEqpIndex(const Eqp *pEqp)
{
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		HistoryEqpData *pItem = eqps.at(idx);
		if(pItem->getEqp() == pEqp)
		{
			return idx;
		}
	}
	return -1;
}

/*
**	FUNCTION
**		Find index of given DPE of a device in list of devices
**
**	PARAMETERS
**		pEqp	- Pointer to device to be found
**      dpeName - datapoint element name to be found
**
**	RETURNS
**		Index of device in list; or
**		-1 if device not found
**
**	CAUTIONS
**		None
*/
int HistoryImage::getDpeIndex(const Eqp *pEqp, const char *dpeName)
{
	QString fullDpeName(pEqp->getDpName());
	fullDpeName += ".";
	fullDpeName += dpeName;

	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		HistoryEqpData *pItem = eqps.at(idx);
		if((pItem->getEqp() == pEqp) && (pItem->getDpeName() == fullDpeName))
		{
			return idx;
		}
	}
	return -1;
}

/*
**	FUNCTION
**		set color for item with given index, redraw image
**
**	PARAMETERS
**		index	- device index
**		color	- new color to set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::setItemColor(int index, const QColor &color)
{
	HistoryEqpData *pItem = eqps.at(index);
	if(pItem)
	{
		pItem->setColor(color);
		if(DebugCtl::isHistory())
		{
#ifdef Q_OS_WIN
			qDebug("HistoryImage::setItemColor()\n");
#else
			printf("HistoryImage::setItemColor()\n");
			fflush(stdout);
#endif
		}
		rebuildImage(6);
	}
}

/*
**	FUNCTION
**		Remove item with given index from list of devices
**
**	PARAMETERS
**		index	- device index
**
**	RETURNS
**		Pointer to device in item that will be removed
**
**	CAUTIONS
**		None
*/
const Eqp *HistoryImage::removeItem(int index)
{
	DataEnum::ValueType valueType = DataEnum::ValueTypeNone;
	const Eqp *pEqp = NULL;
	HistoryEqpData *pItem;
	if((0 <= index) && (index < eqps.count()))
	{
		pItem = eqps.takeAt(index);
		if(pItem)
		{
			if(pCurrentEqp == pItem)
			{
				pCurrentEqp = NULL;
			}
			pEqp = pItem->getEqp();
			valueType = pItem->getValueType();
			delete pItem;
		}
	}
	if(pEqp)	// Check if that was the very last device for axis, if so - remove axis
	{
		int idx;
		for(idx = 0 ; idx < eqps.count() ; idx++)
		{
			pItem = eqps.at(idx);
			if(pItem->getValueType() == valueType)
			{
				break;
			}
		}
		if(idx >= eqps.count())	// The very last device of that type was deleted
		{
			VerticalAxisWidget *pAxis = findAxisForValueType(valueType);
			if(pAxis)
			{
				axisList.removeOne(pAxis);
				delete pAxis;
			}
		}
	}
	return pEqp;
}

/*
**	FUNCTION
**		Add all visible values to class that will export them to Excel file
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::addExportValues(HistoryProcessor &processor)
{
	QDateTime minTime = pTimeAxis->getMin();
	QDateTime maxTime = pTimeAxis->getMax();
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		HistoryEqpData *pItem = eqps.at(idx);
		pItem->addExportValues(minTime, maxTime, processor);
	}
}

/*
**	FUNCTION
**		Slot activated when new online value arrived for one of devices.
**		Activate timer for redrawing the image, or redraw image immediately -
**		depending on how much time passed since last redraw
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::onlineValueArrived(void)
{
	if(online)
	{
		rebuildWithDelay();
	}
}

void HistoryImage::rebuildWithDelay(void)
{
	pTimer->stop();
	int elapsed = drawTime.msecsTo(QTime::currentTime());
	drawTime = QTime::currentTime();	// Move here from rebuildImage() L.Kopylov 07.01.2011

	if(elapsed < 0)	// wrap around 24 hours
	{
		if(DebugCtl::isHistory())
		{
#ifdef Q_OS_WIN
			qDebug("HistoryImage::rebuildWithDelay() 1\n");
#else
			printf("HistoryImage::rebuildWithDelay() 1\n");
			fflush(stdout);
#endif
		}
		rebuildImage(7);
	}
	else if(elapsed > 1000)
	{
		if(DebugCtl::isHistory())
		{
#ifdef Q_OS_WIN
			qDebug("HistoryImage::rebuildWithDelay() 2\n");
#else
			printf("HistoryImage::rebuildWithDelay() 2\n");
			fflush(stdout);
#endif
		}
		rebuildImage(8);
	}
	else
	{
		pTimer->start(300);
	}
}

/*
**	FUNCTION
**		Slot activated when redraw timeout expired
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::redrawTimerDone(void)
{
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("HistoryImage::redrawTimerDone()\n");
#else
		printf("HistoryImage::redrawTimerDone()\n");
		fflush(stdout);
#endif
	}
	rebuildImage(9);
}

/*
**	FUNCTION
**		Slot activated periodically by alive timer
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::aliveTimerDone(void)
{
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_UNIX
		printf("HistoryImage::aliveTimerDone()\n");
		fflush(stdout);
#else
		qDebug("HistoryImage::aliveTimerDone()\n");
	#endif
	}

	if(!online)
	{
		return;
	}
	int elapsed = drawTime.secsTo(QTime::currentTime());

	if(elapsed > 0)	// < 0 = wrap around 24 hours
	{
		QDateTime axisMax = pTimeAxis->getMax();
		int oldX, newX;
		pTimeAxis->timeToScreen(axisMax, oldX);
		pTimeAxis->timeToScreen(axisMax.addSecs(-elapsed), newX);
//		qDebug("elapsed %d oldX %d newX %d", elapsed, oldX, newX);
		if(oldX == newX)
		{
			return;
		}
	}
	else if(elapsed == 0)
	{
		return;
	}
//	qDebug("Redraw by alive timer");
	drawTime = QTime::currentTime();
	rebuildImage(10);
}

/*
**	FUNCTION
**		Build new history image, set image as background for this widget
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::rebuildImage(int source)
{
	pTimer->stop();
	if(!drawnOnce)	// The very first drawing to be done immediately
	{
		drawnOnce = true;
		doDrawWork();
	}
	if(pDrawThread->isRunning())
	{
		if(DebugCtl::isHistory())
		{
#ifdef Q_OS_WIN
			qDebug("HistoryImage::rebuildImage(%d): thread is busy\n", source);
#else
			printf("HistoryImage::rebuildImage(%d): thread is busy\n", source);
			fflush(stdout);
#endif
		}
		pTimer->start(300);
		return;
	}
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("HistoryImage::rebuildImage(%d): start thread\n", source);
#else
		printf("HistoryImage::rebuildImage(%d): start thread\n", source);
		fflush(stdout);
#endif
	}
	pDrawThread->start();
}

void HistoryImage::doDrawWork(void)
{
	/*
	if(redrawing)
	{
		qDebug("Recursive call %d\n", source);
		pTimer->start(300);
		return;
	}
	redrawing = true;
	*/
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("HistoryImage::rebuildImage()\n");
#else
		printf("HistoryImage::rebuildImage()\n");
		fflush(stdout);
#endif
	}
	if(online)
	{
		QDateTime now(QDateTime::currentDateTime());
		QDateTime start(now.addSecs(-60 * depth));
		pTimeAxis->setLimits(start, now);
	}
	QPixmap bgPixmap(pImage->size());
	QPalette pal = QApplication::palette();
	bgPixmap.fill(pal.color(QPalette::Window));
	QPainter painter(&bgPixmap);
//	qDebug("HistoryImage::rebuildImage() - draw start\n");
	QDateTime startTime = QDateTime::currentDateTime();
	draw(painter);
	QDateTime endTime = QDateTime::currentDateTime();
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("HistoryImage::doDrawWork(): draw time = %d\n", (int)startTime.msecsTo(endTime));
#else
		printf("HistoryImage::doDrawWork(): draw time = %d\n", (int)startTime.msecsTo(endTime));
		fflush(stdout);
#endif
	}
//	qDebug("draw time = %d, source %d\n", (int)startTime.msecsTo(endTime), source);
//	qDebug("HistoryImage::rebuildImage() - draw finish\n");
	pal.setBrush(QPalette::Window, bgPixmap);
	pImage->setPalette(pal);
	emit imageUpdated();
	redrawing = false;
//	qDebug("HistoryImage::rebuildImage() - finish\n");
	pDrawThread->quit();
}

/*
**	FUNCTION
**		Redraw the whole graph area.
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::draw(QPainter &painter)
{
	if(online)
	{
		QDateTime now(QDateTime::currentDateTime());
		QDateTime start(now.addSecs(-60 * depth));
		pTimeAxis->setLimits(start, now);
	}
	if(eqps.count())
	{
		calculateVerticalLimits();
	}

	calculateAxisActivity();

	QFontMetrics fontMetrics(painter.fontMetrics());
	// ?????? int verticalAxisMin = rect().height() - pTimeAxis->findLabelHeight(fontMetrics);

	drawVerticalAxies(painter);
	drawTimeAxis(painter);

	showValueAtPointer();

	// Draw data
	// First draw all devices except for selected one(s)
	int idx;
	HistoryEqpData *pDevice;
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pDevice = eqps.at(idx);
		if(pDevice->isSelected())
		{
			continue;
		}
		VerticalAxisWidget *pAxis = findAxisForValueType(pDevice->getValueType());
		pDevice->draw(painter, pTimeAxis, pAxis);
		QThread::yieldCurrentThread();
	}

	// Then draw selected devices - thus they will always appear on top of non-selected
	for(idx = 0 ; idx < eqps.count() ; idx++)
	{
		pDevice = eqps.at(idx);
		if(!pDevice->isSelected())
		{
			continue;
		}
		VerticalAxisWidget *pAxis = NULL;
		pAxis = findAxisForValueType(pDevice->getValueType());
		pDevice->draw(painter, pTimeAxis, pAxis);
	}

	// Draw graph area border
	painter.setPen(VacMainView::getGraphAxisColor());
	painter.drawRect(0, 0, pImage->width() - 1, pImage->height() - 1);

	if(DebugCtl::isHistory())
	{
		const char *fileName;
#ifdef Q_OS_WIN
		fileName = "C:\\HistoryView.txt";
#else
		fileName = "/home/kopylov/HistoryView.txt";
#endif
		dump(fileName);
	}
}

void HistoryImage::calculateAxisActivity(void)
{
	HistoryEqpData *pSelected = NULL;
	int idx;
	for(idx = eqps.count() - 1 ; idx >= 0 ; idx--)
	{
		HistoryEqpData *pDevice = eqps.at(idx);
		if(pDevice->isSelected())
		{
			pSelected = pDevice;
			break;
		}
	}
	for(idx = axisList.count() - 1 ; idx >= 0 ; idx--)
	{
		VerticalAxisWidget *pAxis = axisList.at(idx);
		bool toBeActive = false;
		if(pSelected)
		{
			toBeActive = pAxis->getValueType() == pSelected->getValueType();
		}
		if(pAxis->isActive() != toBeActive)
		{
			pAxis->setActive(toBeActive);
		}
	}
}

/*
**	FUNCTION
**		Calculate limits for vertical axis when graph with
**		values is drawn first time.
**		The idea is:
**		graph shall appear with vertical limits set 'correctly'
**		for the data it displays, but later it works in
**		'manual vertical scale' mode.
**		The requirement came from the fact that, for example,
**		range of value for VGM and VGI are VERY different, and
**		it is not convenient if graph is always opened with the
**		same vertical limits.
**
**	PARAMETERS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void HistoryImage::calculateVerticalLimits(void)
{
	for(int idx = axisList.count() - 1 ; idx >= 0 ; idx--)
	{
		VerticalAxisWidget *pAxis = axisList.at(idx);
		if(!pAxis->isInitialLimitsSet())
		{
			calculateVerticalLimitsForAxis(pAxis);
		}
	}
}

void HistoryImage::calculateVerticalLimitsForAxis(VerticalAxisWidget *pAxis)
{
	float	min = HISTORY_HIGH_VALUE, max = HISTORY_LOW_VALUE;
	int		maxValues = 0;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		HistoryEqpData *pDevice = eqps.at(idx);
		if(pDevice->getValueType() != pAxis->getValueType())
		{
			continue;
		}
		float eqpMin, eqpMax;
		int nValues = pDevice->findLimits(eqpMin, eqpMax);
		if(nValues > maxValues)
		{
			maxValues = nValues;
		}
		if(eqpMin < min)
		{
			min = eqpMin;
		}
		if(eqpMax > max)
		{
			max = eqpMax;
		}
	}
	if(min > max)
	{
		return;
	}
	if(min == max)
	{
		min = (float)(min / 1.01);
		max = (float)(max * 1.01);
	}
	if(pAxis->isLogScale())
	{
		pAxis->setInitialLimits(min, max, maxValues);
	}
	else
	{
		pAxis->setInitialLimits(min, max, maxValues);
	}
}

/*
**	FUNCTION
**		Draw all vertical axies, claculate graph area for time axis
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		Graph area for time axis
**
**	CAUTIONS
**		None
**
*/
void HistoryImage::drawVerticalAxies(QPainter &painter)
{
	QRect plotRect = pImage->rect();
	for(int idx = axisList.count() - 1 ; idx >= 0 ; idx--)
	{
		VerticalAxisWidget *pAxis = axisList.at(idx);
		pAxis->drawOnPlot(painter, plotRect);
	}
}

/*
**	FUNCTION
**		Draw time axis
**
**	PARAMETERS
**		painter		- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void HistoryImage::drawTimeAxis(QPainter &painter)
{
	pTimeAxis->setGraphArea(pImage->geometry());
	pTimeAxis->setGrid(true);
	pTimeAxis->setGridStyle(Qt::SolidLine);
	pTimeAxis->setTickSize(4);
	pTimeAxis->setTickOutside(false);
	pTimeAxis->update();
	pTimeAxis->drawOnPlotArea(painter);
}

void HistoryImage::showValueAtPointer(void)
{
	QString		timeString;
	QDateTime time = pTimeAxis->screenToTime(pImage->getPointerLocation(), timeString);
/*
printf("HistoryImage::showValueAtPointer(): time at %d is %s\n",
pointerLocation, time.toString("yyyy.MM.dd hh:mm:ss.zzz").ascii());
fflush(stdout);
*/

	// Find selected device
	HistoryEqpData *pData = NULL;
	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		HistoryEqpData *pItem = eqps.at(idx);
		if(pItem->isSelected())
		{
			pData = pItem;
			break;
		}
	}
	if(!pData)
	{
		emit valueAtPointerChanged(timeString, QString());
		return;
	}
/*
printf("HistoryImage::showValueAtPointer(): device found\n");
fflush(stdout);
*/

	float value;
	if(!pData->getValueAt(time, value))
	{
		emit valueAtPointerChanged(timeString, QString());
		return;
	}
/*
printf("HistoryImage::showValueAtPointer(): falue found %g\n", value);
fflush(stdout);
*/

	char buf[256];
	VerticalAxisWidget *pAxis = findAxisForValueType(pData->getValueType());
	if(pAxis)
	{
		pAxis->formatValue(buf, value, sizeof(buf) / sizeof(buf[0]));
	}
	else	// this shall not happen
	{
#ifdef Q_OS_WIN
		sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%g", value);
#else
		sprintf(buf, "%g", value);
#endif
	}
	emit valueAtPointerChanged(timeString, buf);
}

/*
**	FUNCTION
**		Set time scales according to current zoom rectangle
**
**	PARAMETERS
**		zoomRect	- Zoom rectangle, selected on plot image
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void HistoryImage::setZoomingTime(const QRect &zoomRect)
{
	QString label;
	QDateTime startTime, endTime;
	if(online)
	{
		bool	switchToOffline = false;
		if(zoomRect.right() < (pImage->width() - 10))	// Switch to offline in order to exactly match zoom rectangle
		{
			int answer = QMessageBox::question(this, "History mode switch",
				"Switch to OFFLINE history mode?", QMessageBox::Yes,
				QMessageBox::No | QMessageBox::Default | QMessageBox::Escape);
			if(answer == QMessageBox::Yes)
			{
				switchToOffline = true;
			}
		}
		if(switchToOffline)
		{
			online = false;
			startTime = pTimeAxis->screenToTime(zoomRect.left(), label);
			endTime = pTimeAxis->screenToTime(zoomRect.right(), label);
		}
		else
		{
			startTime = pTimeAxis->screenToTime(zoomRect.left(), label);
			endTime = QDateTime::currentDateTime();
		}
	}
	else
	{
		startTime = pTimeAxis->screenToTime(zoomRect.left(), label);
		endTime = pTimeAxis->screenToTime(zoomRect.right(), label);
	}
	int newDepth = startTime.secsTo(endTime) / 60;
	if(newDepth < 1)
	{
		newDepth = 1;
		startTime = endTime.addSecs(-60);
	}
	depth = newDepth;
	pTimeAxis->setLimits(startTime, endTime);
}

/*
**	FUNCTION
**		Set vertical scale according to current zoom rectangle.
**		Zooming operation updates only ONE vertical axis of all.
**		Active vertical axis will be updated 
**
**	PARAMETERS
**		zoomRect	- Zoom rectangle, selected on plot image
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
**
*/
void HistoryImage::setZoomingVertical(const QRect &zoomRect)
{
	for(int idx = axisList.count() - 1 ; idx >= 0 ; idx--)
	{
		VerticalAxisWidget *pAxis = axisList.at(idx);
		if(pAxis->isActive())
		{
			pAxis->setZooming(zoomRect);
			break;
		}
	}
}

void HistoryImage::imageResized(void)
{
	rebuildWithDelay();	// To force redrawing, probably with delay
}

void HistoryImage::imagePointerMoved(int /* location */)
{
	showValueAtPointer();
}

void HistoryImage::imageZoomFinished(const QRect &zoomRect)
{
	if((!zoomRect.width()) || (!zoomRect.height()))
	{
		return;
	}

	setZoomingTime(zoomRect);
	setZoomingVertical(zoomRect);
	if(DebugCtl::isHistory())
	{
#ifdef Q_OS_WIN
		qDebug("HistoryImage::imageZoomFinished()\n");
#else
		printf("HistoryImage::imageZoomFinished()\n");
		fflush(stdout);
#endif
	}
	rebuildImage(1);
	emit scalesChanged();
}

void HistoryImage::scaleChanged(bool dragging)
{
	if(dragging)
	{
		return;
	}
	rebuildWithDelay();	// To force redrawing, probably with delay
}

/*
**	FUNCTION
**		Dump history image parameters to output file for debugging
**
**	PARAMETERS
**		fileName	- Name of output file
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void HistoryImage::dump(const char *fileName)
{
	if(!DebugCtl::isHistory())
	{
		return;
	}
#ifdef Q_OS_WIN
	FILE	*pFile = NULL;
	if(fopen_s(&pFile, fileName, "w"))
	{
		pFile = NULL;
	}
#else
	FILE	*pFile = fopen(fileName, "w");
#endif

	if(!pFile)
	{
		return;
	}
	fprintf(pFile, "WIDTH %d HEIGHT %d\n", width(), height());
	/*
	fprintf(pFile, "PressureScale %s, TemperatureScale %s IntensityScale %s\n",
		(pressureScaleVisible ? "VISIBLE" : "NOT VISIBLE"),
		(cryoTempScaleVisible ? "VISIBLE" : "NOT VISIBLE"),
		(intensityScaleVisible ? "VISIBLE" : "NOT VISIBLE"));
	fprintf(pFile, "============ Time axis ===================\n");
	pTimeAxis->dump(pFile);
	fprintf(pFile, "============ Pressure axis ===================\n");
	pPressureAxis->dump(pFile);
	fprintf(pFile, "============ Temperature axis ===================\n");
	pCryoTempAxis->dump(pFile);
	fprintf(pFile, "============ Intensity axis ===================\n");
	pIntensityAxis->dump(pFile);
	*/
	for(int idx = 0 ; idx < axisList.count() ; idx++)
	{
		VerticalAxisWidget *pAxis = axisList.at(idx);
		pAxis->dump(pFile);
	}

	for(int idx = 0 ; idx < eqps.count() ; idx++)
	{
		HistoryEqpData *pItem = eqps.at(idx);
		pItem->dump(pFile);
	}
	fclose(pFile);
}

