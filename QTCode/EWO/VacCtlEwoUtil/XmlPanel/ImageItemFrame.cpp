//	Implementation of ImageItemFrame class
/////////////////////////////////////////////////////////////////////////////////

#include "ImageItemFrame.h"

#include <QLocale>

//////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
//////////////////////////////////////////////////////////////////////////////////

ImageItemFrame::ImageItemFrame() : ImageItem()
{
	startX = startY = sizeX = sizeY = 0;
	filled = false;
}

ImageItemFrame::~ImageItemFrame()
{
}

void ImageItemFrame::draw(QPainter & painter)
{
	painter.setTransform(transform);
	if(backColor.alpha() > 0)
	{
		painter.setBrush(backColor);
	}
	else
	{
		painter.setBrush(Qt::NoBrush);
	}
	setForePen(painter);
	painter.setFont(font);
	QRect textRect = painter.fontMetrics().boundingRect(text);
	if(filled)
	{
		painter.fillRect(textRect, painter.brush());
	}
	painter.drawRect(startX, startY + textRect.height() / 2, sizeX, sizeY - textRect.height() / 2);
	int textY = 0;
	#ifdef Q_OS_WIN
		textY = startY + textRect.height() / 2;
	#else
		textY = startY + textRect.height() - textRect.height() / 4;
	#endif
	painter.eraseRect(startX + 16, startY + textRect.height() / 2 - 2, textRect.width(), 4);
	painter.drawText(startX + 16, textY, text);
}

bool ImageItemFrame::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	if(!visible)
	{
		return false;
	}
	if(toolTipText.isEmpty())
	{
		return false;
	}
	QRect frameRect(startX, startY, sizeX, sizeY);
	if(!frameRect.contains(point))
	{
		return false;
	}
	text = toolTipText;
	rect = frameRect;
	return true;
}

void ImageItemFrame::parseProp(QXmlStreamReader &reader)
{
	QStringRef propName = reader.attributes().value("name");
	if(!propName.compare("Location", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		startX = (int)rint(c.toDouble(str.section(' ', 0, 0)));
		startY = (int)rint(c.toDouble(str.section(' ', 1, 1)));
	}
	else if(!propName.compare("Size", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		sizeX = (int)rint(c.toDouble(str.section(' ', 0, 0)));
		sizeY = (int)rint(c.toDouble(str.section(' ', 1, 1)));
	}
	else if(!propName.compare("FillType", Qt::CaseInsensitive))
	{
		filled = !reader.readElementText().compare("[solid]", Qt::CaseInsensitive);
	}
	else if(!propName.compare("Font", Qt::CaseInsensitive))
	{
		parseFont(reader);
	}
	else if(!propName.compare("Text", Qt::CaseInsensitive))
	{
		parseText(reader);
	}
	else
	{
		ImageItem::parseProp(reader);
	}
}

void ImageItemFrame::parseFont(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement())
		{
#ifdef XML_PANEL_PARSE
			printf("ImageItemFrame::parseFont(): stop at %d: end of %s\n",
				(int)reader.lineNumber(), reader.name().toString().toAscii().constData());
#endif
			break;
		}
		if(reader.isStartElement())
		{
			if(!reader.name().compare("prop", Qt::CaseInsensitive))
			{
				QStringRef propName = reader.attributes().value("name");
				if(!propName.compare("en_US.iso88591", Qt::CaseInsensitive))
				{
					font.fromString(reader.readElementText());
					continue;
				}
			}
			parseUnknown(reader);
		}
	}
}

void ImageItemFrame::parseText(QXmlStreamReader &reader)
{
	while(!reader.atEnd())
	{
		reader.readNext();
		if(reader.isEndElement())
		{
#ifdef XML_PANEL_PARSE
			printf("ImageItemFrame::parseText(): stop at %d: end of %s\n",
				(int)reader.lineNumber(), reader.name().toString().toAscii().constData());
#endif
			break;
		}
		if(reader.isStartElement())
		{
			if(!reader.name().compare("prop", Qt::CaseInsensitive))
			{
				QStringRef propName = reader.attributes().value("name");
				if(!propName.compare("en_US.iso88591", Qt::CaseInsensitive))
				{
					text = reader.readElementText();
					continue;
				}
			}
			parseUnknown(reader);
		}
	}
}
