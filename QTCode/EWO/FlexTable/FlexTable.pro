#
# Project file for qmake utility to build FlexTable EWO
#
#	04.05.2012	L.Kopylov
#		Initial version
#

include(../VacEWO.pri)

TARGET = FlexTable

INCLUDEPATH += ../../common/VacCtlUtil \
	../../common/Platform

HEADERS =	FlexTable.h

SOURCES = FlexTable.cpp

LIBS += -lVacCtlUtil

