//	Implementation of NotificationConfig class
/////////////////////////////////////////////////////////////////////////////////

#include "NotificationConfig.h"
#include "EqpState.h"

#include "EqpMsgCriteria.h"
#include "FunctionalType.h"

#include "DataPool.h"
#include "Eqp.h"

#include <qdatetime.h>

#include <stdlib.h>

QList<NotificationConfig *> *NotificationConfig::pInstanceList = new QList<NotificationConfig *>();

/*
**	FUNCTION
**		Find configuration with given name, if not found - create new configuration
**
**	ARGUMENTS
**		name	- Name of configuration to find
**
**	RETURNS
**		Pointer to configuration with given name
**
**	CAUTIONS
**		May return NULL if given name is empty
*/
NotificationConfig *NotificationConfig::findConfig(const char *name)
{
	if(!name)
	{
		return NULL;
	}
	if(!*name)
	{
		return NULL;
	}
	NotificationConfig *pConfig;
	for(int idx = 0 ; idx < pInstanceList->count() ; idx++)
	{
		pConfig = pInstanceList->at(idx);
		if(pConfig->getName() == name)
		{
			return pConfig;
		}
	}
	pConfig = new NotificationConfig(name);
	pInstanceList->append(pConfig);
	return pConfig;
}

/*
**	FUNCTION
**		Delete configuration with given name
**
**	ARGUMENTS
**		name	- Name of configuration to delete
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::deleteConfig(const char *name)
{
	if(!name)
	{
		return;
	}
	if(!*name)
	{
		return;
	}
	for(int idx = 0 ; idx < pInstanceList->count() ; idx++)
	{
		NotificationConfig *pConfig = pInstanceList->at(idx);
		if(pConfig->getName() == name)
		{
			pConfig = pInstanceList->takeAt(idx);
			delete pConfig;
			return;
		}
	}
}

/*
**	FUNCTION
**		Check in all configurations if life time of message groups
**		has expired. In such case all messages in group shall be sent.
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::checkMsgGroupExpire(void)
{
	QDateTime now = QDateTime::currentDateTime();
	for(int idx = 0 ; idx < pInstanceList->count() ; idx++)
	{
		NotificationConfig *pConfig = pInstanceList->at(idx);
		if(pConfig->initialized)
		{
			pConfig->msgFilter.checkGroupTimeExpired(now);
			pConfig->checkPendingMatch(now);
		}
	}
}

const char *NotificationConfig::scopeTypeToString(int type)
{
	switch(type)
	{
	case SectorScope:
		return "Sector";
	case MainPartScope:
		return "MainPart";
	case ArcScope:
		return "Arc";
	case DeviceScope:
		return "Eqp";
	case GlobalScope:
		return "Global";
	default:
		break;
	}
	static char result[32];
#ifdef Q_OS_WIN
	sprintf_s(result, sizeof(result) / sizeof(result[0]), "%d", type);
#else
	sprintf(result, "%d", type);
#endif
	return result;
}

int NotificationConfig::scopeTypeParse(const char *string, QString &errMsg)
{
	int result = 0;
	if(!string)
	{
		errMsg += "NULL scope type";
	}
	else if(!(*string))
	{
		errMsg += "Empty scope type";
	}
	else if(!strcasecmp(string, "sector"))
	{
		result = SectorScope;
	}
	else if(!strcasecmp(string, "mainpart"))
	{
		result = MainPartScope;
	}
	else if(!strcasecmp(string, "arc"))
	{
		result = ArcScope;
	}
	else if(!strcasecmp(string, "eqp"))
	{
		result = DeviceScope;
	}
	else if(!strcasecmp(string, "global"))
	{
		result = GlobalScope;
	}
	else
	{
		errMsg += "Unsupported scope type <";
		errMsg += string;
		errMsg += ">";
	}
	return result;
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

NotificationConfig::NotificationConfig(const char *name)
{
	this->name = name;
	pEqpList = new QList<EqpState *>();
	pCriteria = NULL;
	pMessageRule = NULL;
	activated = initialized = false;
}

NotificationConfig::~NotificationConfig()
{
	deactivate();
	if(pMessageRule)
	{
		delete pMessageRule;
	}
	if(pCriteria)
	{
		delete pCriteria;
	}
	while(!pEqpList->isEmpty())
	{
		delete pEqpList->takeFirst();
	}
	delete pEqpList;
}

void NotificationConfig::setCriteria(ComplexCriteria *pCriteria)
{
	deactivate();
	if(this->pCriteria)
	{
		delete this->pCriteria;
	}
	this->pCriteria = pCriteria;
	setScope();
}

/*
**	FUNCTION
**		Activate this configuration, i.e. connect to all required equipment and
**		prepare to send messages when event arrive
**
**	ARGUMENTS
**		errMsg	- variable where error message will be written in case of parsing error
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::activate(QString &errMsg)
{
	errMsg = "";
	if(activated)
	{
		return;
	}
	if(pEqpList->isEmpty())
	{
		errMsg = "Activate: empty list of equipment";
		return;
	}
	if(!pMessageRule)
	{
		errMsg = "Activate: no rule to build messages";
		return;
	}
	if(!msgFilter.isValid())
	{
		errMsg = "Activate: message filtering rule is not valid";
		return;
	}

/*
printf("NotificationConfig::activate(): %d in list\n", pEqpList->count());
fflush(stdout);
*/
	for(int idx = pEqpList->count() - 1 ; idx >= 0 ; idx--)
	{
		EqpState *pState = pEqpList->at(idx);
		pState->activate();
	}
	activated = true;
}

/*
**	FUNCTION
**		Deactivate this configuration, i.e. disconnect from all equipment
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::deactivate()
{
	if(!activated)
	{
		return;
	}
	for(int idx = pEqpList->count() - 1 ; idx >= 0 ; idx--)
	{
		EqpState *pState = pEqpList->at(idx);
		pState->deactivate();
	}
	activated = initialized = false;
}

/*
**	FUNCTION
**		Parse string, representing rule to build message, and set result as a rule
**		for building text messages
**
**	ARGUMENTS
**		string	- Pointer to string to be parsed
**		errMsg	- variable where error message will be written in case of parsing error
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::setMessageRule(const char *string, QString &errMsg)
{
	deactivate();
	if(pMessageRule)
	{
		delete pMessageRule;
	}
	pMessageRule = EqpMessage::create(string, errMsg);
}

/*
**	FUNCTION
**		Build list of devices, matching given scope criteria
**
**	ARGUMENTS
**		type		- Type of scope criteria - see enum in this class
**		nameList	- List of item names, iteminterpretation depends on type
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::setScope(int type, QStringList &nameList)
{
	deactivate();
	scopeType = type;
	scopeNameList.clear();
	foreach(QString name, nameList)
	{
		scopeNameList.append(name);
	}
	setScope();
}

void NotificationConfig::setScope(void)
{
	pEqpList->clear();
	if(!pCriteria)
	{
		return;
	}

	// Build array of functional types used in criteria
	int	*typeArray = NULL;
	int	nTypes = 0;
	pCriteria->getFunctionalTypes(&typeArray, nTypes);
	if(!nTypes)
	{
		return;
	}

	switch(scopeType)
	{
	case SectorScope:
		setScopeSectors(typeArray, nTypes);
		break;
	case MainPartScope:
		setScopeMainParts(typeArray, nTypes);
		break;
	case ArcScope:
		setScopeArcs(typeArray, nTypes);
		break;
	case DeviceScope:
		setScopeDevices(typeArray, nTypes);
		break;
	case GlobalScope:
		setScopeGlobal(typeArray, nTypes);
		break;
	}

	if(typeArray)
	{
		free((void *)typeArray);
	}
}

/*
**	FUNCTION
**		Build list of devices, matching given scope criteria (list of sector names)
**
**	ARGUMENTS
**		typeArray	- Array of functional types to be included
**		nTypes		- Number of functional types in array
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::setScopeSectors(int *typeArray, int nTypes)
{
	int	mainFunctionalType = typeArray[0];
	usageFilter.setMainType(mainFunctionalType);

	DataPool &pool = DataPool::getInstance();

	QList<Eqp *> &ctls = pool.getCtlList();
	QList<BeamLine *> &lines = pool.getLines();

	QList<Eqp *> mainEqpList;

	for(QStringList::Iterator it = scopeNameList.begin() ; it != scopeNameList.end() ; ++it)
	{
		const char *name = (*it).toLatin1();
		Sector *pSector = pool.findSectorData(name);
		if(!pSector)
		{
			continue;
		}

		// Process controllers
		foreach(Eqp *pEqp, ctls)
		{
			if(pEqp->getFunctionalType() != mainFunctionalType)
			{
				continue;
			}
			/* L.Kopylov 11.01.2012: this does not allow to catch connect/disconnect of mobile
			if(!pEqp->isActive(DataEnum::Online))
			{
				continue;
			}
			*/
			if(pEqp->getSectorBefore() == pSector)
			{
				addEqpToList(mainEqpList, pEqp);
			}
			else if(pEqp->getSectorAfter() == pSector)
			{
				addEqpToList(mainEqpList, pEqp);
			}
		}

		// Process equipment with physical location
		foreach(BeamLine *pLine, lines)
		{
			QList<BeamLinePart *> &parts = pLine->getParts();
			foreach(BeamLinePart *pPart, parts)
			{
				Eqp	**eqpPtrs = pPart->getEqpPtrs();
				int nEqpPtrs = pPart->getNEqpPtrs();
				for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(pEqp->getFunctionalType() != mainFunctionalType)
					{
						continue;
					}
					/* L.Kopylov 11.01.2012: this does not allow to catch connect/disconnect of mobile
					if(!pEqp->isActive(DataEnum::Online))
					{
						continue;
					}
					*/
					if(!usageFilter.eqpMatches(pEqp))
					{
						continue;
					}
					if(pEqp->getSectorBefore() == pSector)
					{
						addEqpToList(mainEqpList, pEqp);
					}
					else if(pEqp->getSectorAfter() == pSector)
					{
						addEqpToList(mainEqpList, pEqp);
					}
				}
			}
		}
	}

	buildEqpStateList(typeArray, nTypes, mainEqpList);
}

/*
**	FUNCTION
**		Build list of devices, matching given scope criteria (list of main part names)
**
**	ARGUMENTS
**		typeArray	- Array of functional types to be included
**		nTypes		- Number of functional types in array
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::setScopeMainParts(int *typeArray, int nTypes)
{
	int	mainFunctionalType = typeArray[0];
	usageFilter.setMainType(mainFunctionalType);

	DataPool &pool = DataPool::getInstance();

	QList<Eqp *> &ctls = pool.getCtlList();
	QList<BeamLine *> &lines = pool.getLines();

	QList<Eqp *> mainEqpList;

	//qDebug("NotificationConfig::setScopeMainParts(): start at %s\n", QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz").toLatin1().constData());
	for(QStringList::Iterator it = scopeNameList.begin() ; it != scopeNameList.end() ; ++it)
	{
		const char *name = (*it).toLatin1();
		MainPart *pMainPart = pool.findMainPartData(name);
		if(!pMainPart)
		{
			continue;
		}

		// Process controllers
		foreach(Eqp *pEqp, ctls)
		{
			if(mainFunctionalType != FunctionalType::PLC)
			{
				if(pEqp->getFunctionalType() != mainFunctionalType)
				{
					continue;
				}
			}
			else if(pEqp->getFunctionalType() == FunctionalType::PLC)
			{
				continue;
			}
			/* L.Kopylov 11.01.2012: this does not allow to catch connect/disconnect of mobile
			if(!pEqp->isActive(DataEnum::Online))
			{
				continue;
			}
			*/
			if(!usageFilter.eqpMatches(pEqp))
			{
				continue;
			}
			if(pEqp->getMainPart() == pMainPart)
			{
				if(mainFunctionalType == FunctionalType::PLC)
				{
					addPlcOfDevice(pEqp, mainEqpList);
				}
				else
				{
					addEqpToList(mainEqpList, pEqp);
				}
			}
		}

		// Process equipment with physical location
		foreach(BeamLine *pLine, lines)
		{
			QList<BeamLinePart *> &parts = pLine->getParts();
			foreach(BeamLinePart *pPart, parts)
			{
				Eqp	**eqpPtrs = pPart->getEqpPtrs();
				int nEqpPtrs = pPart->getNEqpPtrs();
				for(int eqpIdx = 0 ; eqpIdx < nEqpPtrs ; eqpIdx++)
				{
					Eqp *pEqp = eqpPtrs[eqpIdx];
					if(mainFunctionalType != FunctionalType::PLC)
					{
						if(pEqp->getFunctionalType() != mainFunctionalType)
						{
							continue;
						}
					}
					else if(pEqp->getFunctionalType() == FunctionalType::PLC)
					{
						continue;
					}
					/* L.Kopylov 11.01.2012: this does not allow to catch connect/disconnect of mobile
					if(!pEqp->isActive(DataEnum::Online))
					{
						continue;
					}
					*/
					if(!usageFilter.eqpMatches(pEqp))
					{
						continue;
					}
					if(pEqp->getMainPart() == pMainPart)
					{
						if(mainFunctionalType == FunctionalType::PLC)
						{
							addPlcOfDevice(pEqp, mainEqpList);
						}
						else
						{
							addEqpToList(mainEqpList, pEqp);
						}
					}
				}
			}
		}
	}

//	qDebug("NotificationConfig::setScopeMainParts(): FINISH at %s, %d devices found\n", QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz").toLatin1().constData(),	mainEqpList.count());
	buildEqpStateList(typeArray, nTypes, mainEqpList);
}

/*
**	FUNCTION
**		Build list of devices, matching given scope criteria (list of arc names)
**
**	ARGUMENTS
**		typeArray	- Array of functional types to be included
**		nTypes		- Number of functional types in array
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::setScopeArcs(int * /* typeArray */, int /* nTypes */)
{
	// TODO: not implemented yet
}

/*
**	FUNCTION
**		Build list of devices, matching given scope criteria (list of device names)
**
**	ARGUMENTS
**		typeArray	- Array of functional types to be included
**		nTypes		- Number of functional types in array
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::setScopeDevices(int *typeArray, int nTypes)
{
	int	mainFunctionalType = typeArray[0];

	DataPool &pool = DataPool::getInstance();

	QList<Eqp *> mainEqpList;

	for(QStringList::Iterator it = scopeNameList.begin() ; it != scopeNameList.end() ; ++it)
	{
		const char *name = (*it).toLatin1();
/*
printf("NotificationConfig::setScopeDevices(): check %s\n", name);
fflush(stdout);
*/
		Eqp *pEqp = pool.findEqpByDpName(name);
		if(!pEqp)
		{
			continue;
		}
/*
printf("NotificationConfig::setScopeDevices(): eqp found\n");
fflush(stdout);
*/
		if(mainFunctionalType != FunctionalType::PLC)
		{
			if(pEqp->getFunctionalType() != mainFunctionalType)
			{
				continue;
			}
		}
		else if(pEqp->getFunctionalType() == FunctionalType::PLC)
		{
			continue;
		}
/*
printf("NotificationConfig::setScopeDevices(): functional type OK\n");
fflush(stdout);
*/
		/* Even if device is not active now - add it because it can become active later
		** LIK 16.07.2010
		if(!pEqp->isActive(DataEnum::Online))
		{
			continue;
		}
		*/
/*
printf("NotificationConfig::setScopeDevices(): active OK\n");
fflush(stdout);
*/
		if(mainFunctionalType == FunctionalType::PLC)
		{
			addPlcOfDevice(pEqp, mainEqpList);
		}
		else
		{
			addEqpToList(mainEqpList, pEqp);
		}
	}

	buildEqpStateList(typeArray, nTypes, mainEqpList);
}

/*
**	FUNCTION
**		Build list of devices, matching given scope criteria (global scope)
**
**	ARGUMENTS
**		typeArray	- Array of functional types to be included
**		nTypes		- Number of functional types in array
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::setScopeGlobal(int *typeArray, int nTypes)
{
	int	mainFunctionalType = typeArray[0];
	usageFilter.setMainType(mainFunctionalType);

	DataPool &pool = DataPool::getInstance();

	QList<Eqp *> &ctls = pool.getCtlList();

	QList<Eqp *> mainEqpList;

	// Process controllers
	foreach(Eqp *pEqp, ctls)
	{
		if(pEqp->getFunctionalType() != mainFunctionalType)
		{
			continue;
		}

		if(pEqp->getSectorBefore())
		{
			continue;	// Global devices are not related to any sector
		}
		else if(pEqp->getSectorAfter())
		{
			continue;	// Global devices are not related to any sector
		}
		addEqpToList(mainEqpList, pEqp);
	}


	buildEqpStateList(typeArray, nTypes, mainEqpList);
}


/*
**	FUNCTION
**		Build list of EqpStates based on list of 'main' devices for each state
**
**	ARGUMENTS
**		typeArray	- Array of functional types to be included
**		nTypes		- Number of functional types in array
**		mainEqpList	- List of main devices
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::buildEqpStateList(int *typeArray, int nTypes, QList<Eqp *> &mainEqpList)
{
	FunctionalType *pFuncType = FunctionalType::findTypeData(typeArray[0]);

	QList<Eqp *> relatedList;

	foreach(Eqp *pEqp, mainEqpList)
	{
		// Skip devices which are not controlled... but do not skip NotConnected because they can become connected at any moment
		if(pEqp->getCtlStatus(DataEnum::Online) == Eqp::NotControl)
		{
			continue;
		}
		EqpState *pState = new EqpState(pEqp, *pCriteria);
		pEqpList->append(pState);
		connect(pState, SIGNAL(stateChanged(EqpState *, bool)),
			this, SLOT(stateChange(EqpState *, bool)));

		for(int n = 1 ; n < nTypes ; n++)
		{
			pFuncType->findRelatedEqp(pEqp, typeArray[n], relatedList);
			foreach(Eqp *pRelatedEqp, relatedList)
			{
				pState->addEqp(pRelatedEqp);
			}
		}
	}
}

/*
**	FUNCTION
**		Add pointer to device to list if it is not there yet
**
**	ARGUMENTS
**		list	- List where pointer shall be added
**		pEqp	- Pointer to add
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::addEqpToList(QList<Eqp *> &list, Eqp *pEqp)
{
	// VPI-specific: add one VPI per VRPI
	if(pEqp->getFunctionalType() == FunctionalType::VPI)
	{
		DataPool &pool = DataPool::getInstance();
		QString masterDpName;
		int masterChannel;
		pEqp->getMaster(masterDpName, masterChannel);
		if(!masterDpName.isEmpty())
		{
			Eqp *pMaster = pool.findEqpByDpName(masterDpName.toLatin1());
			if(pMaster)
			{
				QStringList slaveList;
				pMaster->getSlaves(slaveList);
				foreach(QString dpName, slaveList)
				{
					Eqp *pOtherEqp = pool.findEqpByDpName(dpName.toLatin1());
					if(pOtherEqp)
					{
						if(list.contains(pOtherEqp))
						{
							return;	// Something is already in list
						}
					}
				}
				list.append(pEqp);
				return;
			}
		}
	}
	if(!list.contains(pEqp))
	{
		list.append(pEqp);
	}
}

void NotificationConfig::setInitialized(void)
{
/*
printf("NotificationConfig::setInitialized()\n");
fflush(stdout);
*/
	foreach(EqpState *pEqp, *pEqpList)
	{
		pEqp->resetPendingMatch();
	}
	initialized = true;
}

void NotificationConfig::addPlcOfDevice(Eqp *pEqp, QList<Eqp *> &eqpList)
{
	// The most of devices use 1 PLC, but some use more
	QList<Eqp *> plcList;
	if(pEqp->getPlcList(plcList))
	{
		foreach(Eqp *pPlc, plcList)
		{
			addEqpToList(eqpList, pPlc);
		}
	}
	else
	{
		const QString plcName = pEqp->findPlcName();
		if(!plcName.isEmpty())
		{
			Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
			if(pPlc)
			{
				addEqpToList(eqpList, pPlc);
			}
		}
	}
}

/*
**	FUNCTION
**		Slot activated when matching state for one of devices has been changed
**
**	ARGUMENTS
**		pSrc	- Pointer to EqpState - source of signal
**		match	- New match state of device
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::stateChange(EqpState *pSrc, bool match)
{
/*
printf("NotificationConfig::stateChange(): match %d\n", (int)match);
fflush(stdout);
*/
	if(!match)
	{
		return;	// TODO ?????
	}
	if(!initialized)
	{
		return;
	}

	QString messageId;
	QString text(pMessageRule->buildMessage(pSrc, messageId, pSrc->getCriteriaPtr()));
	if(text.isEmpty())
	{
		return;
	}
	Eqp *pEqp = pSrc->getFirstEqp();
	msgFilter.addMessage(pEqp, name, text, messageId);
}

/*
**	FUNCTION
**		Make a chance for pending matches to be converted to real ones
**
**	ARGUMENTS
**		now	- Current date + time
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void NotificationConfig::checkPendingMatch(const QDateTime &now)
{
	if((!initialized) || (!activated))
	{
		return;
	}
	if(!pEqpList)
	{
		return;
	}
	foreach(EqpState *pEqp, *pEqpList)
	{
		pEqp->checkPendingMatch(now);
	}
}

