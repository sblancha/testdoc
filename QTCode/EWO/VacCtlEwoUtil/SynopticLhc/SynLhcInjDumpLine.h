#ifndef	SYNLHCINJDUMPLINE_H
#define	SYNLHCINJDUMPLINE_H

// Line for LHC synoptic view representing injection/dump line of LHC

#include "SynLine.h"

class SynLhcInjDumpLine : public SynLine  
{
public:
	SynLhcInjDumpLine();
	virtual ~SynLhcInjDumpLine();

	static SynLhcInjDumpLine *create(QWidget *parent, BeamLine *pLine, int parentVacType);

	// Access
	inline int getVacType(void) const { return vacType; }
	inline int getConnX(void) const { return connX; }
	inline void setConnX(int coord) { connX = coord; }

protected:
	// Vacuum type of parent line (hence, vacuum type of this line too)
	int				vacType;

	// X-coordinate of connect point in parent line
	int				connX;

	virtual void checkForReverse(void);
};

#endif	// SYNLHCINJDUMPLINE_H
