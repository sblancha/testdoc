/**
* @brief Class implementation for widget Gauge Penning
* @see VacIcon
*/
#ifndef	VACICONGAUGEPENNING_H
#define	VACICONGAUGEPENNING_H

#include "VacIconVG_U.h"

class VACCTLEWOUTIL_EXPORT VacIconGaugePenning : public VacIconVG_U {
public:
	VacIconGaugePenning(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG_U(parent, f) {}

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONGAUGEPENNING_H
