#ifndef	TIMEEDITEWO_H
#define	TIMEEDITEWO_H

// Simple EWO INTERFACE FOR QTimeEdit widget

#include <BaseExternWidget.hxx>

#include <qdatetimeedit.h>

class EWO_EXPORT TimeEditEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	TimeEditEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void timeChange(const QTime &value);
		
private:
	QTimeEdit	*baseWidget;
};

#endif	// TIMEEDITEWO_H
