#
# Project file for qmake utility to build Makefile for VacAux DLL
#
#	06.10.2008	L.Kopylov
#		Initial version
#
#	13.12.2008	L.Kopylov
#		Rename target library to VacCtlUtil
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	10.01.2010	L.Kopylov
#		Move VacMainView to EWO/VacCtlEwoUtil.
#		Move FunctionalType to common/VacCtlEqpData.
#		Move VacEqpTypeMask to common/VacCtlEqpData/FunctionalType.
#		Change to DLL
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

TEMPLATE = lib

TARGET = VacCtlUtil

DESTDIR = ../../../bin

OBJECTS_DIR = obj

unix:CONFIG += qt dll plugin thread release warn_off
# separate_debug_info
win32:CONFIG += qt dll thread release

# To produce PDB file together with DLL
win32:QMAKE_CXXFLAGS+=/Zi
win32:QMAKE_LFLAGS+= /INCREMENTAL:NO /Debug

#DEFINES += _VACDEBUG

win32:DEFINES += BUILDING_VACCTLUTIL


INCLUDEPATH += ./ \
	../Platform

HEADERS = ../Platform/PlatformDef.h \
	VacCtlUtilExport.h \
	DataEnum.h \
	NamePool.h \
	FileParser.h \
	InterfaceEqp.h \
	DpConnection.h \
	DpPolling.h \
	DpeConnection.h \
	DpeHistoryQuery.h \
	DpeHistoryDepthQuery.h \
	MobileHistoryQuery.h \
	DevListCriteria.h \
	DebugCtl.h \
	VacCtlUtil.h

SOURCES = NamePool.cpp \
	FileParser.cpp \
	InterfaceEqp.cpp \
	DpConnection.cpp \
	DpPolling.cpp \
	DpeConnection.cpp \
	DpeHistoryQuery.cpp \
	DpeHistoryDepthQuery.cpp \
	MobileHistoryQuery.cpp \
	DevListCriteria.cpp \
	DebugCtl.cpp \
	VacCtlUtil.cpp

