//	Implementation of DateTimeEditEwo class
/////////////////////////////////////////////////////////////////////////////////

#include "TimeEditEwo.h"

EWO_PLUGIN(TimeEditEwo)

TimeEditEwo::TimeEditEwo(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new QTimeEdit(QTime::currentTime(), parent);
	QFont fnt("Helvetica", 10);
	baseWidget->setFont(fnt);
	baseWidget->setDisplayFormat("hh:mm:ss");
	connect(baseWidget, SIGNAL(timeChanged(const QTime &)),
		this, SLOT(timeChange(const QTime &)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList TimeEditEwo::signalList(void) const
{
	QStringList list;
	list.append("valueChanged()");
	return list;
}

/*
**	FUNCTION
**		Slot receiving valveChanged signal of QDateTimeEdit, emit
**		signal 'valueChanged' to PVSS
**
**	ARGUMENTS
**		value	- New date + time value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void TimeEditEwo::timeChange(const QTime & /* value */)
{
	emit signal("valueChanged");
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList TimeEditEwo::methodList(void) const
{
	QStringList list;
	list.append("int getHour()");
	list.append("void setHour(int value)");
	list.append("int getMinute()");
	list.append("void setMinute(int value)");
	list.append("int getSecond()");
	list.append("void setSecond(int value)");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant TimeEditEwo::invokeMethod(const QString &name,
	QList<QVariant> &values, QString &error)
{
	if(name == "getHour")
	{
		int result = baseWidget->time().hour();
		return QVariant(result);
	}
	else if(name == "setHour")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::Int))
		{
			error = "argument 1 (value) is not int";
			return QVariant();
		}
		int hour = values[0].toInt();
		if((hour < 0) || (hour > 23))
		{
			error = "Hour must be in range 0...23";
			return QVariant();
		}
		QTime time = baseWidget->time();
		int minute = time.minute();
		int second = time.second();
		if(time.setHMS(hour, minute, second))
		{
			baseWidget->setTime(time);
		}
		return QVariant();
	}
	else if(name == "getMinute")
	{
		int result = baseWidget->time().minute();
		return QVariant(result);
	}
	else if(name == "setMinute")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::Int))
		{
			error = "argument 1 (value) is not int";
			return QVariant();
		}
		int minute = values[0].toInt();
		if((minute < 0) || (minute > 59))
		{
			error = "Minute must be in range 0...59";
			return QVariant();
		}
		QTime time = baseWidget->time();
		int hour = time.hour();
		int second = time.second();
		if(time.setHMS(hour, minute, second))
		{
			baseWidget->setTime(time);
		}
		return QVariant();
	}
	else if(name == "getSecond")
	{
		int result = baseWidget->time().second();
		return QVariant(result);
	}
	else if(name == "setSecond")
	{
		if(!hasNumArgs(name, values, 1, error))
		{
			return QVariant();
		}
		if(!values[0].canConvert(QVariant::Int))
		{
			error = "argument 1 (value) is not int";
			return QVariant();
		}
		int second = values[0].toInt();
		if((second < 0) || (second > 59))
		{
			error = "Second must be in range 0...59";
			return QVariant();
		}
		QTime time = baseWidget->time();
		int hour = time.hour();
		int minute = time.minute();
		if(time.setHMS(hour, minute, second))
		{
			baseWidget->setTime(time);
		}
		return QVariant();
	}
	else
	{
		error = "Uknown method TimeEditEwo.";
		error += name;
	}
	return QVariant();
}

