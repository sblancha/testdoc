// ---------------------------------------------------------------------------------------------------------------------
/**
@system         WinCC_OA 3.15(and later) Vacuum Framework
@type Script
@file vcsDevProcessCntr.ctl
@brief This is the control manager script to update startCounterSt dpe
@details Original Request Ticket: https://its.cern.ch/jira/browse/VACCO-1502
@details Development follow-up Ticket: https://its.cern.ch/jira/browse/VACCO-1526
@details This is the control manager script to update startCounterSt dpe originally develop for VV_PUL control type instances.
@details Monitoring and archiving a process counter is not easy, ideally counter is updating by PLC but we don't want to reset counter after each PLC update. 

Language: WCCOA CTRL

License: CERN
Licensed Material - Property of CERN

(c) Copyright CERN 2017

Address:
CERN, Technology Departement, Vacuum Controls
Route de Meyrin
CH-1211 Geneva
  
Limitations: WINCC OA Manager CTRL (WCCOA00CTRL)
 
Function:

Database tables used:

Thread Safe: No

Extendable: No

Platform Dependencies: Linux, Windows

Compiler Options: none
*/
// ---------------------------------------------------------------------------------------------------------------------

/**
@brief main of vcsDevProcessCntr.ctl
*/
main() {
 
  dyn_string dsDpNames = getDeviceDpNames();
  //DebugTN("Dp Names: ", dsDpNames);
  //DebugTN("Dp Names number: ", dynlen(dsDpNames));
  //DebugTN("First Dp Name: ", dsDpNames[1]);
  int iPlcStartCounter;     // Start counter value from PLC
  int iStartCounterOffset;  // Start counter offset between Start counter status and Start counter value from PLC 
  int iStartCounterSt;      // Start counter status, value displayed and published

  while(true){
  
    if (dynlen(dsDpNames) < 1){
      DebugTN("SCRIPT: vcsDevProcessCntr.ctl, no instance with process counter"); 
    }
    else {
      for (int i = 1; i <= dynlen(dsDpNames); i++){
        if (dpGet(dsDpNames[i] + ".StartCounter", iPlcStartCounter) < 0){
          DebugTN("SCRIPT: vcsDevProcessCntr.ctl, dpGet .StartCounter failed");
          continue;
        }
        if (dpGet(dsDpNames[i] + ".StartCounterOffset", iStartCounterOffset) < 0){
          DebugTN("SCRIPT: vcsDevProcessCntr.ctl, dpGet .StartCounterOffset failed");
          continue;
        }
        if (dpGet(dsDpNames[i] + ".StartCounterSt", iStartCounterSt) < 0){
          DebugTN("SCRIPT: vcsDevProcessCntr.ctl, dpGet .StartCounterSt failed");
          continue;
        }

      
        // Set new value for StartCounterSt dpe
        if((iPlcStartCounter +  iStartCounterOffset) > iStartCounterSt) {
          iStartCounterSt = iPlcStartCounter + iStartCounterOffset;
          if (dpSetWait(dsDpNames[i] + ".StartCounterSt", iStartCounterSt) < 0){
            DebugTN("SCRIPT: vcsDevProcessCntr.ctl, dpSetWait .StartCounterSt failed");
            continue;
          }
        }      
        else {
          iStartCounterOffset = iStartCounterSt - iPlcStartCounter;
          if (dpSetWait(dsDpNames[i] + ".StartCounterOffset", iStartCounterOffset) < 0){
            DebugTN("SCRIPT: vcsDevProcessCntr.ctl, dpSetWait .StartCounterOffset failed");
            continue;
          };
        }
        
      }
    }
  delay(5); //Process Counters evaluted again after 5sec
  }
  
}

/**
@brief Return dp names to be taken into account by the script
@details Only Process counter of dp type VV_PUL is implemented
*/

dyn_string getDeviceDpNames() {
   dyn_string dsDpNames = dpNames("*", "VV_PUL");
   return dsDpNames;
}
