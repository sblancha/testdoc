
// Set machine mode for SMS based on value of AMODE (from CMW) and/or
// forced machine mode setting

#uses "vclMachine.ctl"
#uses "vclSmsConfig.ctl"

string modeDpName;

void main()
{
  DebugTN("vcsSmsMachineMode.ctl: starting");
  dyn_string exceptionInfo;
  LhcVacGetMachineId(exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsSmsMachineMode.ctl: LhcVacGetMachineId() failed:", exceptionInfo);
    return;
  }
// CPS #216 (Assigned to) [PVSS] add the "SMS notifications" functionality in CPS project
//   if(glAccelerator != "LHC")
//   {
//     DebugTN("vcsSmsMachineMode.ctl: machine <" + glAccelerator + "> is not LHC, exiting");
//     return;
//   }
  VacResourcesParseFile(PROJ_PATH + "/data/VacCtlResources.txt", glAccelerator, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN("vcsSmsMachineMode.ctl: VacResourcesParseFile() failed:", exceptionInfo);
    return;
  }

  LhcSmsGetCommonConfigDp(modeDpName);
  if(modeDpName == "")
  {
    DebugTN("vcsSmsMachineMode.ctl: LhcSmsGetCommonConfigDp() failed");
    return;
  }
  dpConnect("ModeChangeCb", modeDpName + ".AmodeCMW",
            modeDpName + ".ModeForced",
            modeDpName + ".Mode");
  DebugTN("vcsSmsMachineMode.ctl: running");
}

void ModeChangeCb(string amodeDpe, int amode, string forcedDpe, int modeForced,
                  string modeDpe, int mode)
{
  // Calculate new mode anyway
  int newMode = VacSmsMachineModeAuto(amode, 0);  // BMode is not used yet
  dpSet(modeDpName + ".ModeCalculated", newMode);

  // Decide which mode to set. Forced has the highest priority, but only valid settings
  if(VacIsValidMachineMode(modeForced))
  {
    newMode = modeForced;
  }
  /*
  else
  {
    DebugTN("vcsSmsMachineMode.ctl: forced mode " + modeForced + " is not valid");
  }
  */

  // Set new mode if necessary
  if((mode != newMode) && (newMode != 0))
  {
    dpSet(modeDpName + ".Mode", newMode);
  }
}
