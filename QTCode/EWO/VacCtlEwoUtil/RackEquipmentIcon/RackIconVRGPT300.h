#ifndef RACKICONVRGPT300_H
#define RACKICONVRGPT300_H

#include "RackIcon.h"

class Eqp;

class VACCTLEWOUTIL_EXPORT RackIconVRGPT300 : public RackIcon
{
	Q_OBJECT
public:
	RackIconVRGPT300(QWidget * parent, Qt::WindowFlags f = 0);
	virtual ~RackIconVRGPT300();

	virtual void setEqp(Eqp * pEqp);
	virtual bool connect(void);
	virtual bool disconnect(void);
protected:
	Eqp * pEqp; //pointer to equipment, if it exists
	virtual void paintEvent(QPaintEvent *pEvent);

};

#endif
