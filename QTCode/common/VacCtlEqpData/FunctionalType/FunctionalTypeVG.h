#ifndef FUNCTIONALTYPEVG_H
#define	FUNCTIONALTYPEVG_H

// Special functional type for gauges - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVG : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Off = 1,
		On = 2
	};

	FunctionalTypeVG(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVG() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
	void fillCriteriaVGM(FunctionalType *pType);
	void fillCriteriaVGR(FunctionalType *pType);
	void fillCriteriaVGP(FunctionalType *pType);
	void fillCriteriaVGI(FunctionalType *pType);
	void fillCriteriaVGF(FunctionalType *pType);
	void fillCriteriaVPGMPR(FunctionalType *pType);
	void fillCriteriaVGTR(FunctionalType *pType);
	void fillCriteriaVG_A_RO(FunctionalType *pType);
	void fillCriteriaVG(FunctionalType *pType);
};

#endif	// FUNCTIONALTYPEVG_H
