#ifndef DPDATA_H
#define DPDATA_H

#include "VacCtlImportExport.h"

//	Class holding information for one DP

#include "FileParser.h"
#include "DpeData.h"

#include <qlist.h>

class ImportPool;

class VACCTLIMPORT_EXPORT DpData : public FileParser
{
public:
	virtual ~DpData();

	static void setImportPool(ImportPool *pImportPool);
	static DpData *createFromFile(FILE *pFile, int &lineNo, const char *name,
		QStringList &errList);

	void dump(FILE *pFile);

	// Access
	inline const char *getName(void) const { return name; }
	inline const char *getDpType(void) const { return dpType; }
	inline int getMobileType(void) const { return mobileType; }
	inline QList<DpeData *> &getDpes(void) { return *pDpes; }
	inline int getLineNo(void) const { return lineNo; }
	inline bool isExportToCMW(void) const { return exportToCMW; }

protected:
	DpData(const char *name, int lineNo);

	// DP name
	char				*name;

	// Name of DP type
	char				*dpType;

	// Mobile type of device
	int					mobileType;

	// Line in input file where device appeared
	int					lineNo;

	// Flag - export device to CMW
	bool				exportToCMW;

	// List of DPEs related to this DP
	QList<DpeData *>	*pDpes;

	// Reference to ImportPool instance
	static ImportPool		*pPool;

	virtual int processLineTokens(FILE *pFile, int &lineNo, char **tokens, int nTokens,
		const QString &longValue, QStringList &errList);
};

#endif	// DPDATA_H
