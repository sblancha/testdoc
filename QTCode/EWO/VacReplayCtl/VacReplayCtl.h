#ifndef VACREPLAYCTL_H
#define	VACREPLAYCTL_H

// Widget interface for ReplayPool

#include <qwidget.h>

class VacReplayCtl : public QWidget
{
	Q_OBJECT

public:
	VacReplayCtl(QWidget *parent = NULL, Qt::WindowFlags flags = 0);
	~VacReplayCtl();

signals:
	void dpeListChanged(void);

protected:
	// Number of DPEs in replay pool
	int dpeCount;

	void calculateMinSize(void);
	virtual void setFont(const QFont &newFont);
	virtual void paintEvent(QPaintEvent *pEvent);

private slots:
	void poolDpeListChange(void);
};

#endif	// VACREPLAYCTL_H
