#ifndef	PLOTIMAGE_H
#define	PLOTIMAGE_H

// Class providing history plot image drawing. Basically it contains background
// pixmap with all plots + functionality to support interaction using
// mouse: pointer drawing/motion, zoom rectangle, plot selection etc.

#include "HistoryEqpData.h"
#include "TimeAxisWidget.h"
#include "VerticalAxisWidget.h"

#include <qwidget.h>

class HistoryImage;

class PlotImage : public QWidget
{
	Q_OBJECT

public:
	PlotImage(QWidget *parent);
	~PlotImage();

	// Access
	int getPointerLocation(void) const { return pointerLocation; }
	const QRect &getZoomRect(void) const { return zoomRect; }

signals:
	void resized(void);
	void pointerMoved(int location);
	void zoomRectFinished(const QRect &rect);

protected:
	// Type of mouse motion processing
	typedef enum
	{
		MouseMotionNone = 0,
		MouseMotionPointer = 1,
		MouseMotionArea = 2
	} MouseMotion;

	// Zooming rectangle
	QRect				zoomRect;

	// Coordinates where zooming operation was started
	int					startZoomX;
	int					startZoomY;

	// Location of vertical pointer
	int					pointerLocation;

	// Type of mouse motion processing
	MouseMotion			motionType;

	void movePointerTo(int newX);
	void moveZoomRectangle(int x, int y);

	virtual void resizeEvent(QResizeEvent *event);
	virtual void mousePressEvent(QMouseEvent *pEvent);
	virtual void mouseReleaseEvent(QMouseEvent *pEvent);
	virtual void mouseMoveEvent(QMouseEvent *pEvent);
	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif	// PLOTIMAGE_H
