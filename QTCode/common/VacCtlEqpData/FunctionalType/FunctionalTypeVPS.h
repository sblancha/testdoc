#ifndef	FUNCTIONALTYPEVPS_H
#define	FUNCTIONALTYPEVPS_H

// Special functional type for VPS - mainly to deal with SMS notifications

#include "FunctionalType.h"

class VACCTLEQPDATA_EXPORT FunctionalTypeVPS : public FunctionalType
{
public:

	// Specific criteria subtypes
	enum
	{
		Off = 1,
		On = 2
	};

	FunctionalTypeVPS(int type, const char *name, const char *description);
	virtual ~FunctionalTypeVPS() {}

	virtual void fillCriteria(FunctionalType *pType);
	virtual const char *getCriteriaKey(EqpMsgCriteria *pCriteria);
	virtual int parseCriteriaKey(int critType, const char *name, QString &errMsg);

protected:
};

#endif	// FUNCTIONALTYPEVPS_H
