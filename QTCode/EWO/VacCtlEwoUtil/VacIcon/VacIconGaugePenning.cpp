#include "VacIconGaugePenning.h"
#include "EqpVG_U.h"
#include <qpainter.h>

/**
@brief FUNCTION PROTECTED draw symbol inside
@param[in]  painter	Painter object
*/
void VacIconGaugePenning::drawSpecific(QPainter &painter) {
	bool blockedOff = false;
	bool showAnalogVal = false;
	double pr = 0.0;
	unsigned rr1 = 0;
	if (pEqp) {
		if (pEqp->inherits("EqpVG_U")) {
			blockedOff = ((EqpVG_U *)pEqp)->isBlockedOff(mode);
			pr = ((EqpVG_U *)pEqp)->getPR(DataEnum::Online);
			rr1 = ((EqpVG_U *)pEqp)->getRR1(DataEnum::Online);
			showAnalogVal = ((EqpVG_U *)pEqp)->isAnalogValOnIcon(mode);
		}
	}
	if (showAnalogVal &&
		(mode != DataEnum::Replay) &&
		(pr > 1E-14) && (pr < 1E4) &&
		(rr1 & RR1_VALID_VGU)) {
		// If Pressure valid and online display value
		QString sPr = QString::number(pr, 'E', 0);
		drawPressureValue(painter, sPr);
	}
	painter.setPen(Qt::black);
	painter.drawLine(9, 8, 11, 8);
	QPolygon points(4);
	points.setPoint(0, 5, 8);
	points.setPoint(1, 5, 13);
	points.setPoint(2, 15, 13);
	points.setPoint(3, 15, 8);
	painter.drawPolyline(points);
	if (blockedOff) {
		painter.drawLine(0, 0, 22, 20);
		painter.drawLine(0, 20, 22, 0);
	}
}











	
