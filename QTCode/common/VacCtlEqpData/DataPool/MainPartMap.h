#ifndef MAINPARTMAP_H
#define MAINPARTMAP_H

#include "VacCtlEqpDataExport.h"

// Class holding information for mapping sector to main part

#include <qstring.h>

class MainPart;

class VACCTLEQPDATA_EXPORT MainPartMap
{
public:
	MainPartMap(MainPart *pMainPart, const QString &domain)
	{
		this->pMainPart = pMainPart;
		this->domain = domain;
	}
	virtual ~MainPartMap() {}

	MainPartMap *clone(void) { return new MainPartMap(pMainPart, domain); }

	inline MainPart *getMainPart(void) { return pMainPart; }
	inline const QString &getDomain(void) { return domain; }
	inline void setDomain(const QString &name) { domain = name; }
protected:
	// Pointer to main part
	MainPart	*pMainPart;

	// Name of domain, corresponding to combination sector + main part
	QString	domain;
};

#endif	// MAINPARTMAP_H
