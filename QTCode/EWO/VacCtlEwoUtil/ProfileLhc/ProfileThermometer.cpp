//	Implementation of ProfileThermometer class
/////////////////////////////////////////////////////////////////////////////////

#include "ProfileThermometer.h"

#include "Eqp.h"
#include "EqpCRYO_TT.h"

#include "VacMainView.h"
#include "DebugCtl.h"

#include <qpainter.h>

/////////////////////////////////////////////////////////////////////////////////
////////////////// Construction/destruction

ProfileThermometer::ProfileThermometer(Eqp *pEqp, DataEnum::DataMode mode)
{
	this->pEqp = pEqp;
	this->mode = mode;
	value = 0;
	valid = selected = false;
	hidden = true;	// !!!
	start = realStart = pEqp->getStart();
	subType = pEqp->getCtrlSubType();
}

ProfileThermometer::~ProfileThermometer()
{
	if(!hidden)
	{
		pEqp->disconnect(this, mode);
	}
}

/*
**	FUNCTION
**		Connect to equipment data when item becomes visible in profile,
**		read immediately last known value for device to be ready for display
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileThermometer::show(void)
{
	if(!hidden)
	{
		return;
	}
	pEqp->connect(this, mode);
	hidden = false;
	value = pEqp->getMainValue(mode);
	valid = pEqp->isMainValueValid(mode);
}

/*
**	FUNCTION
**		Disconnect from equipment data when item becomes invisible in profile
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileThermometer::hide(void)
{
	if(hidden)
	{
		return;
	}
	hidden = true;
	pEqp->disconnect(this, mode);
}

/*
**	FUNCTION
**		Slot activated when value for one of device's DPEs has been changed
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileThermometer::dpeChange(Eqp * /* pSrc */, const char * /* dpeName */,
	DataEnum::Source /* source */, const QVariant & /* value */, DataEnum::DataMode mode, const QDateTime & /* timeStamp */)
{
	if(hidden)
	{
		return;
	}
	switch(mode)
	{
	case DataEnum::Online:
	case DataEnum::Polling:
		if(this->mode == DataEnum::Replay)
		{
			return;
		}
		break;
	case DataEnum::Replay:
		if(this->mode != DataEnum::Replay)
		{
			return;
		}
		break;
	}
	value = pEqp->getMainValue(mode);
	valid = pEqp->isMainValueValid(mode);
	emit eqpDataChanged(this);
}

/*
**	FUNCTION
**		Slot activated when device's selection has been changed
**
**	ARGUMENTS
**		pSrc		- Pointer to object - source of signal
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileThermometer::selectChange(Eqp * /* pSrc */)
{
	if(selected != pEqp->isSelected())
	{
		selected = pEqp->isSelected();
		emit eqpSelectChanged(true);
	}
}

/*
**	FUNCTION
**		Draw this thermometer on graph
**
**	PARAMETERS
**		painter	- Painter used for drawing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileThermometer::draw(QPainter &painter)
{
	if(hidden)
	{
		return;
	}
	QColor useColor;
	if(valid)
	{
		switch(subType)
		{
		case EqpCRYO_TT::QrlLineB:		// QRL line B
			useColor = VacMainView::getProfileTempQrlBColor();
			break;
		case EqpCRYO_TT::QrlLineC:		// QRL line C
			useColor = VacMainView::getProfileTempQrlCColor();
			break;
		case EqpCRYO_TT::CmAvg:		// Magnet cold mass - average
		case EqpCRYO_TT::CmMin:		// Magnet cold mass - minimum
		case EqpCRYO_TT::CmMax:		// Magnet cold mass - maximum
			useColor = VacMainView::getProfileTempCmColor();
			break;
		default:
			break;
		}
	}
	else
	{
		useColor = color;
	}

	// Draw thermometer - depends on subtype
	QPen pen(useColor, 3);
	painter.setPen(pen);
	switch(subType)
	{
	case EqpCRYO_TT::QrlLineB:		// QRL line B
	case EqpCRYO_TT::QrlLineC:		// QRL line C
	case EqpCRYO_TT::CmAvg:		// Magnet cold mass - average
		painter.drawArc(point.x() - 3, point.y() - 3, 7, 7, 0, 360 * 16);
		break;
	case EqpCRYO_TT::CmMin:		// Magnet cold mass - minimum
	case EqpCRYO_TT::CmMax:		// Magnet cold mass - maximum
		painter.drawLine(point.x() - 4, point.y(), point.x() + 4, point.y());
		break;
	}
	// TODO: Highlight selected thermometer - NOT IMPLEMENTED YET
}

/*
**	FUNCTION
**		Return rectangular area containing thermometer data - in order
**		to find if mouse pointer is on this thermometer or not
**
**	PARAMETERS
**		None
**
**	RETURNS
**		Rectangle
**
**	CAUTIONS
**		None
*/
QRect ProfileThermometer::getRect(void)
{
	QRect	rect(point.x() - 3, point.y() - 1, 7, 3);
	switch(subType)
	{
	case EqpCRYO_TT::QrlLineB:		// QRL line B
	case EqpCRYO_TT::QrlLineC:		// QRL line C
	case EqpCRYO_TT::CmAvg:		// Magnet cold mass - average
		rect.setTop(point.y() - 3);
		rect.setBottom(point.y() + 3);
		break;
	case EqpCRYO_TT::CmMin:		// Magnet cold mass - minimum
	case EqpCRYO_TT::CmMax:		// Magnet cold mass - maximum
		break;
	}
	return rect;
}

/*
**	FUNCTION
**		Dump content to output file - for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for write
**		idx		- Index of this item in list
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void ProfileThermometer::dump(FILE *pFile, int idx)
{
	if(!DebugCtl::isProfile())
	{
		return;
	}
	fprintf(pFile, "  %04d: <%s> coord (%9.3f) value %8.2e %s: (%d,%d)%s%s\n", idx,
		pEqp->getName(), start, value, (valid ? "" : "INVALID"),
		point.x(), point.y(),
		(hidden ? " HIDDEN" : " "),
		(selected ? " SELECTED" : " "));
}
