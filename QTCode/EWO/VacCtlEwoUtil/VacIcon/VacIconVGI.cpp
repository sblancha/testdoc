//	Implementation of VacIconVGI class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVGI.h"

#include <qpainter.h>

// We only need to implement drawSpecific() method

VacIconVGI::VacIconVGI(QWidget *parent, Qt::WindowFlags f) :
	VacIconVG(parent, f)
{
	QFont mFont("Times", 8, QFont::Bold);
	setFont(mFont);
}

// Draw 'Ion' symbol in the center of icon
void VacIconVGI::drawSpecific(QPainter &painter)
{
	painter.setPen(Qt::black);
	painter.drawText(9, 15, "i");
	/*
	painter.drawLine(10, 8, 10, 15);
	painter.drawLine(9, 8, 12, 8);
	painter.drawLine(9, 15, 12, 15);
	painter.drawPoint(10, 6);
	*/
	/*
	painter.setPen(QPen(Qt::black, 2));
	painter.drawArc(9, 4, 3, 3, 0, 360 * 16);
	*/
}
