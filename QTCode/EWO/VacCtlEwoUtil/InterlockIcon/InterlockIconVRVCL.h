#ifndef	INTERLOCKICONVRVCL_H
#define	INTERLOCKICONVRVCL_H

#include "qwidget.h"
#include "InterlockIcon.h"

class InterlockIconVRVCL : public InterlockIcon
{
	Q_OBJECT
public:
	InterlockIconVRVCL(QWidget * parent = NULL);
	inline InterlockIconClass getIconClass() { return Control; };
	void moveIcon(int x, int y);
	QPoint getConnectionPoint(QString connectorName);
	void getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber);
	QRect getBoundingRect();
	virtual ~InterlockIconVRVCL();
private:
	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif