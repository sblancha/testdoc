//	Implementation of DateTimeWidget
/////////////////////////////////////////////////////////////////////////////////

#include "DateEditWidget.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

DateEditWidget::DateEditWidget(QWidget *parent, Qt::WindowFlags /* flags */) :
	QDateEdit(QDate::currentDate(), parent)
{
	setDisplayFormat("dd-MM-yyyy");
	mode = StartOfDay;
	dateTime.setDate(date());
	dateTime.time().setHMS(0, 0, 0);
	connect(this, SIGNAL(dateChanged(const QDate &)), this, SLOT(dateChange(const QDate &)));
}


/*
**	FUNCTION
**		Set new 'time of day' mode for this widget. Immediately
**		calculates 
**
**	ARGUMENTS
**		newMode	- New mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DateEditWidget::setMode(TimeOfDay newMode)
{
	if(mode == newMode)
	{
		return;
	}
	mode = newMode;
	dateChange(dateTime.date());
}

/*
**	FUNCTION
**		Slot receiving valueChanged signal of QDateEdit, calculates date + time
**		according to recent mode and emits signal dateTimeChanged
**
**	ARGUMENTS
**		date	- New date
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void DateEditWidget::dateChange(const QDate &date)
{
	dateTime.setDate(date);
	QTime timePart(dateTime.time());
	switch(mode)
	{
	case StartOfDay:
		timePart.setHMS(0, 0, 0);
		break;
	case EndOfDay:
		timePart.setHMS(23, 59, 59, 999);
		break;
	}
	dateTime.setTime(timePart);
	emit dateTimeChanged(dateTime);
}
