//	Implementation of VacIconContainer class
/////////////////////////////////////////////////////////////////////////////////

#include "Eqp.h"

#include "VacIconContainer.h"
#include "VacIcon.h"

#include "DataPool.h"

#include <qlayout.h>

/////////////////////////////////////////////////////////////////////////////////
/////////// Construction/destruction
/////////////////////////////////////////////////////////////////////////////////

VacIconContainer::VacIconContainer(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	pLayout = new QHBoxLayout(this);
	pLayout->setContentsMargins(0, 0, 0, 0);
	pLayout->setSpacing(0);
	pIcon = NULL;
	pipeColor.setRgb(0, 0, 255);
	dpName = "<none>";
	pipeWidth = 3;
	eqpType = None;
	direction = Up;
	beamDirection = LeftToRight;
	mode = DataEnum::Online;
	drawConnectionPipe = true;
}

VacIconContainer::~VacIconContainer()
{
	if(pIcon)
	{
		delete pIcon;
	}
	delete pLayout;
}

/*
**	FUNCTION
**		Set vacuum pipe color used to draw icon
**
**	ARGUMENTS
**		color	- New pipe color
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconContainer::setPipeColor(QColor &color)
{
	pipeColor = color;
	if(pIcon)
	{
		pIcon->setPipeColor(color);
	}
}

/*
**	FUNCTION
**		Set vacuum pipe width used to draw icon
**
**	ARGUMENTS
**		width	- New pipe width
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconContainer::setPipeWidth(int width)
{
	pipeWidth = width;
	if(pIcon)
	{
		pIcon->setPipeWidth(width);
	}
}

/*
**	FUNCTION
**		Set icon direction
**
**	ARGUMENTS
**		dir	- New direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconContainer::setDirection(Direction dir)
{
	direction = dir;
	if(pIcon)
	{
		pIcon->setDirection(direction);
		QSize size = pIcon->minimumSize();
		setFixedSize(size);
		resize(size);
	}
}

/*
**	FUNCTION
**		Set beam direction for icon
**
**	ARGUMENTS
**		dir	- New direction
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconContainer::setBeamDirection(BeamDirection dir)
{
	beamDirection = dir;
	if(pIcon)
	{
		pIcon->setBeamDirection(dir);
	}
}

/*
**	FUNCTION
**		Set data acquisition mode for icon
**
**	ARGUMENTS
**		mode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconContainer::setMode(DataEnumMode mode)
{
	this->mode = (DataEnum::DataMode)mode;
	if(pIcon)
	{
		pIcon->setMode(this->mode);
	}
}

/*
**	FUNCTION
**		Set flag for drawing connection pipe
**
**	ARGUMENTS
**		flag	- New value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconContainer::setDrawConnectionPipe(bool flag)
{
	drawConnectionPipe = flag;
	if(pIcon)
	{
		pIcon->setDrawConnectionPipe(drawConnectionPipe);
	}
}


/*
**	FUNCTION
**		Set DP name for icon
**
**	ARGUMENTS
**		name	- New DP name
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconContainer::setDpName(QString &name)
{
	dpName = name;
	if(pIcon)
	{
		DataPool &pool = DataPool::getInstance();
		Eqp *pEqp = pool.findEqpByDpName(dpName.toLatin1());
		pIcon->setEqp(pEqp);
	}
}

/*
**	FUNCTION
**		Set equipment type for icon
**
**	ARGUMENTS
**		type	- New equipment type
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconContainer::setEqpType(EqpType type)
{
	setAutoFillBackground(false);	// L.Kopylov 03.10.2013
	if(eqpType == type)
	{
		return;
	}
	if(pIcon)
	{
		QObject::disconnect(pIcon, SIGNAL(mouseDown(int, int, int, int, int, const char *)),
			this, SIGNAL(iconMouseDown(int, int, int, int, int, const char *)));
		delete pIcon;
		pIcon = NULL;
	}
	pIcon = VacIcon::getIcon(type, this);
	if(pIcon)
	{
		pLayout->addWidget(pIcon);
		// pIcon->move(0, 0);
		pIcon->show();
		pIcon->setDirection(direction);
		pIcon->setBeamDirection(beamDirection);
		pIcon->setPipeColor(pipeColor);
		pIcon->setPipeWidth(pipeWidth);
		pIcon->setMode(mode);
		pIcon->setDrawConnectionPipe(drawConnectionPipe);
		if(!dpName.isNull())
		{
			if(!dpName.isEmpty())
			{
				setDpName(dpName);
			}
		}
//		QPoint zero(0, 0);
//		pIcon->reparent(this, zero, true);
		setMinimumWidth(pIcon->minimumWidth());
		setMaximumWidth(pIcon->maximumWidth());
		setMinimumHeight(pIcon->minimumHeight());
		setMaximumHeight(pIcon->maximumHeight());
		/*
		QSize size = pIcon->minimumSize();
		if((pIcon->maximumWidth() == pIcon->minimumWidth()) &&
			(pIcon->minimumHeight() == pIcon->maximumHeight()))
		{
			setFixedSize(size);
			resize(size);
		}
		else if(pIcon->maximumWidth() == pIcon->minimumWidth())
		{
			setFixedWidth(pIcon->minimumWidth());
		}
		else if(pIcon->minimumHeight() == pIcon->maximumHeight())
		{
			setFixedHeight(pIcon->minimumHeight());
		}
		*/
		QObject::connect(pIcon, SIGNAL(mouseDown(int, int, int, int, int, const char *)),
			this, SIGNAL(iconMouseDown(int, int, int, int, int, const char *)));
	}
}

bool VacIconContainer::connect(void)
{
	/*
	qDebug("VacIconContainer::connect(): autoFillBackground = %d\n", autoFillBackground());
	setAutoFillBackground(false);
	qDebug("VacIconContainer::connect(): autoFillBackground #2 = %d\n", autoFillBackground());
	*/
	if(pIcon)
	{
		return pIcon->connect();
	}
	return false;
}
bool VacIconContainer::disconnect(void)
{
	if(pIcon)
	{
		return pIcon->disconnect();
	}
	return false;
}

QSize VacIconContainer::sizeHint(void) const
{
	if(pIcon)
	{
		return pIcon->minimumSize();
	}
	return QWidget::sizeHint();
}

int VacIconContainer::getIconWidth(void) const
{
	return pIcon ? pIcon->minimumWidth() : 0;
}

int VacIconContainer::getIconHeight(void) const
{
	return pIcon ? pIcon->minimumHeight() : 0;
}



