//	Implementation of SmsConfigTable class
/////////////////////////////////////////////////////////////////////////////////

#include "SmsConfigTable.h"

EWO_PLUGIN(SmsConfigTable)


SmsConfigTable::SmsConfigTable(QWidget *parent) : BaseExternWidget(parent)
{
	baseWidget = new ConfigTable(parent);

	// Connect to signals of base widget
	connect(baseWidget->selectionModel(),
		SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
		this,
		SLOT(selectionChange(const QItemSelection &, const QItemSelection &)));
	connect(baseWidget->getModel(), SIGNAL(connectRequest(void)), this, SLOT(connectRequest(void)));
	connect(baseWidget, SIGNAL(doubleClicked(const QModelIndex &)),
		this, SLOT(doubleClick(const QModelIndex &)));
}

/*
**	FUNCTION
**		Return list of signals this EWO can emit to PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList SmsConfigTable::signalList(void) const
{
	QStringList list;
	list.append("selectionChanged()");
	list.append("connectRequest()");
	list.append("tableDoubleClick(int row, int col)");
	return list;
}

/*
**	FUNCTION
**		Return list of methods which can be called from PVSS panel
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		List of signals
**
**	CAUTIONS
**		None
*/
QStringList SmsConfigTable::methodList(void) const
{
	QStringList list;
	list.append("void clear()");
	list.append("void addConfig(string dpName)");
	list.append("void addGroupConfig(string dpName, dyn_string simpleDpNames)");
	list.append("void addToGroup(string dpName, string simpleDpName)");
	list.append("void deleteConfig(string dpName)");

	list.append("void setConfigId(string dpName, int id)");
	list.append("void setConfigOwner(string dpName, string owner)");
	list.append("void setConfigName(string dpName, string name)");
	list.append("void setConfigFlags(string dpName, bool active, bool activated, bool initialized)");
	list.append("void setConfigNumMsg(string dpName, int numMsg)");
	list.append("void setConfigNumEvents(string dpName, int numEvents)");

	list.append("string nextDisconnectRequest()");
	list.append("string nextConnectRequest()");
	list.append("string getSelectedConfig()");
	list.append("int getNumRows()");
	list.append("string getConfigAtRow(int row)");
	return list;
}

/*
**	FUNCTION
**		Method is called when one of this EWO's methods is called from PVSS
**		panel.
**
**	ARGUMENTS
**		name	- PVSS method name - one of names we returned by methodList() method
**		values	- Arguments from PVSS
**		error	- Variable where error message shall be written in case of fatal error
**
**	RETURNS
**		Value to be returned to PVSS
**
**	CAUTIONS
**		None
*/
QVariant SmsConfigTable::invokeMethod(const QString &name, QList<QVariant> &values,
	QString &error)
{
	if(name == "clear")
	{
		baseWidget->getModel()->clear();
		return QVariant();
	}
	else if(name == "addConfig")
	{
		return invokeAddConfig(name, values, error);
	}
	else if(name == "addGroupConfig")
	{
		return invokeAddGroupConfig(name, values, error);
	}
	else if(name == "addToGroup")
	{
		return invokeAddToGroup(name, values, error);
	}
	else if(name == "deleteConfig")
	{
		return invokeDeleteConfig(name, values, error);
	}
	else if(name == "setConfigId")
	{
		return invokeSetConfigId(name, values, error);
	}
	else if(name == "setConfigOwner")
	{
		return invokeSetConfigOwner(name, values, error);
	}
	else if(name == "setConfigName")
	{
		return invokeSetConfigName(name, values, error);
	}
	else if(name == "setConfigFlags")
	{
		return invokeSetConfigFlags(name, values, error);
	}
	else if(name == "setConfigNumMsg")
	{
		return invokeSetConfigNumMsg(name, values, error);
	}
	else if(name == "setConfigNumEvents")
	{
		return invokeSetConfigNumEvents(name, values, error);
	}
	else if(name == "nextDisconnectRequest")
	{
		return baseWidget->getModel()->nextDisconnectRequest();
	}
	else if(name == "nextConnectRequest")
	{
		return baseWidget->getModel()->nextConnectRequest();
	}
	else if(name == "getSelectedConfig")
	{
		return baseWidget->getSelectedConfig();
	}
	else if(name == "getNumRows")
	{
		return baseWidget->getModel()->getNumRows();
	}
	else if(name == "getConfigAtRow")
	{
		return invokeGetConfigAtRow(name, values, error);
	}
	else
	{
		error = "Uknown method SmsConfigTable.";
		error += name;
	}
	return QVariant();
}

QVariant SmsConfigTable::invokeAddConfig(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	baseWidget->getModel()->addSimpleConfig(values[0].toString());
	return QVariant();
}

QVariant SmsConfigTable::invokeAddGroupConfig(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::StringList))
	{
		error = "argument 2 (simpleDpNames) is not dyn_string";
		return QVariant();
	}
	baseWidget->getModel()->addGroupConfig(values[0].toString(), values[1].toStringList());
	return QVariant();
}
	
QVariant SmsConfigTable::invokeAddToGroup(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::String))
	{
		error = "argument 2 (simpleDpName) is not string";
		return QVariant();
	}
	baseWidget->getModel()->addToGroup(values[0].toString(), values[1].toString());
	return QVariant();
}
	
QVariant SmsConfigTable::invokeDeleteConfig(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	baseWidget->getModel()->deleteConfig(values[0].toString());
	return QVariant();
}

QVariant SmsConfigTable::invokeSetConfigId(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (id) is not int";
		return QVariant();
	}
	baseWidget->getModel()->setConfigId(values[0].toString(), values[1].toInt());
	return QVariant();
}

QVariant SmsConfigTable::invokeSetConfigOwner(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::String))
	{
		error = "argument 2 (owner) is not string";
		return QVariant();
	}
	baseWidget->getModel()->setConfigOwner(values[0].toString(), values[1].toString());
	return QVariant();
}

QVariant SmsConfigTable::invokeSetConfigName(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::String))
	{
		error = "argument 2 (name) is not string";
		return QVariant();
	}
	baseWidget->getModel()->setConfigName(values[0].toString(), values[1].toString());
	return QVariant();
}

QVariant SmsConfigTable::invokeSetConfigFlags(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 4, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::Bool))
	{
		error = "argument 2 (active) is not bool";
		return QVariant();
	}
	if(!values[2].canConvert(QVariant::Bool))
	{
		error = "argument 3 (actived) is not bool";
		return QVariant();
	}
	if(!values[3].canConvert(QVariant::Bool))
	{
		error = "argument 4 (initialized) is not bool";
		return QVariant();
	}
	baseWidget->getModel()->setConfigFlags(values[0].toString(), values[1].toBool(), values[2].toBool(), values[3].toBool());
	return QVariant();
}

QVariant SmsConfigTable::invokeSetConfigNumMsg(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::Int))
	{
		error = "argument 2 (numMsg) is not int";
		return QVariant();
	}
	baseWidget->getModel()->setConfigNumMsg(values[0].toString(), values[1].toInt());
	return QVariant();
}

QVariant SmsConfigTable::invokeSetConfigNumEvents(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 2, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::String))
	{
		error = "argument 1 (dpName) is not string";
		return QVariant();
	}
	if(!values[1].canConvert(QVariant::UInt))
	{
		error = "argument 2 (numMsg) is not uint";
		return QVariant();
	}
	baseWidget->getModel()->setConfigNumEvents(values[0].toString(), values[1].toUInt());
	return QVariant();
}

QVariant SmsConfigTable::invokeGetConfigAtRow(const QString &name, QList<QVariant> &values, QString &error)
{
	if(!hasNumArgs(name, values, 1, error))
	{
		return QVariant();
	}
	if(!values[0].canConvert(QVariant::Int))
	{
		error = "argument 1 (row) is not int";
		return QVariant();
	}
	return baseWidget->getModel()->getConfigAtRow(values[0].toInt());
	return QVariant();
}


/*
**	FUNCTION
**		Slot receiving selectionChanged signal of ConfigTable, emit
**		signal 'selectionChanged' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SmsConfigTable::selectionChange(const QItemSelection & /* selected */,
	const QItemSelection & /* deselected */)
{
	emit signal("selectionChanged");
}

/*
**	FUNCTION
**		Slot receiving connectRequest signal of ConfigTable, emit
**		signal 'connectRequest' to PVSS
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SmsConfigTable::connectRequest(void)
{
	emit signal("connectRequest");
}

/*
**	FUNCTION
**		Slot receiving doubleClicked signal of ConfigTable, emit
**		signal 'doubleClick' to PVSS
**
**	ARGUMENTS
**		row	- table row
**		col	- table column
**		button	- mouse button
**		mousePos	- Mouse position
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void SmsConfigTable::doubleClick(const QModelIndex &index)
{
	QList<QVariant> args;
	args.append(QVariant(index.row()));
	args.append(QVariant(index.column()));
	emit signal("tableDoubleClick", args);
}

