//	Implementation of FunctionalTypeVP_NEG class
/////////////////////////////////////////////////////////////////////////////////

#include "FunctionalTypeVP_NEG.h"

#include "EqpMsgCriteria.h"
#include "StdEqpMessage.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/destruction

FunctionalTypeVP_NEG::FunctionalTypeVP_NEG(int type, const char *name, const char *description) :
	FunctionalType(type, name, description)
{
}

/*
**	FUNCTION
**		Add possible criteria fro this functional type to this (or another)
**		functional type
**
**	ARGUMENTS
**		pType	- Pointer to type where criteria shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void FunctionalTypeVP_NEG::fillCriteria(FunctionalType *pType)
{
	EqpMsgCriteria *pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, Off, "VP_NEG is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VP_NEG is OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VP_NEG is not OFF in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VP_NEG is OFF in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VP_NEG is not OFF in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::MainState, On, "VP_NEG is ON");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp} is OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp} is not OFF");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VP_NEG is ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VP_NEG is not ON in sector {sector}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VP_NEG is ON in main part {mainPart}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VP_NEG is not ON in main part {mainPart}");

	pMsgCriteria = pType->addCriteria(type, EqpMsgCriteria::Error, 0, "VP_NEG ERROR");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, false,
		"{eqp}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeEqp, true,
		"{eqp}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, false,
		"VP_NEG in sector {sector}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeSector, true,
		"VP_NEG in sector {sector}: no error");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, false,
		"VP_NEG in main part {mainPart}: {error}");
	pMsgCriteria->addStdMessage(StdEqpMessage::SourceTypeMainPart, true,
		"VP_NEG in main part {mainPart}: no error");
}

/*
**	FUNCTION
**		Return key for searching default message for criteria in resources
**
**	PARAMETERS
**		pCriteria	- Pointer to criteria that needs a key
**
**	RETURNS
**		Key for searching in resources
**
**	CAUTIONS
**		None
*/
const char *FunctionalTypeVP_NEG::getCriteriaKey(EqpMsgCriteria *pCriteria)
{
	const char *result = FunctionalType::getCriteriaKey(pCriteria);
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case Off:
			result = "OFF";
			break;
		case On:
			result = "ON";
			break;
		default:
			break;
		}
		break;
	default:
		break;
		
	}
	return result;
}

int FunctionalTypeVP_NEG::parseCriteriaKey(int critType, const char *name, QString &errMsg)
{
	int result = 0;
	switch(critType)
	{
	case EqpMsgCriteria::MainState:
		if(!strcasecmp(name, "OFF"))
		{
			result = Off;
		}
		else if(!strcasecmp(name, "ON"))
		{
			result = On;
		}
		break;
	default:
		break;
		
	}
	if(result)
	{
		return result;
	}
	return FunctionalType::parseCriteriaKey(critType, name, errMsg);
}
