//	Implementation of ImageItemLine class
/////////////////////////////////////////////////////////////////////////////////

#include "ImageItemLine.h"

#include <QLocale>

//////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction
//////////////////////////////////////////////////////////////////////////////////

ImageItemLine::ImageItemLine() : ImageItem()
{
	startX = startY = endX = endY = 0;
}

ImageItemLine::~ImageItemLine()
{
}


void ImageItemLine::draw(QPainter & painter)
{
	painter.setTransform(transform);
	painter.setBrush(Qt::NoBrush);
	setForePen(painter);
	painter.drawLine(startX, startY, endX, endY);
}

bool ImageItemLine::getToolTip(const QPoint &point, QString &text, QRect &rect)
{
	if(!visible)
	{
		return false;
	}
	if(toolTipText.isEmpty())
	{
		return false;
	}
	float distance = 0;
	if(!distanceToPoint(point, distance))
	{
		return false;
	}
	if(lineWidth)
	{
		if(distance > lineWidth)
		{
			return false;
		}
	}
	else
	{
		if(distance > 1)
		{
			return false;
		}
	}
	text = toolTipText;
	rect.setRect(point.x() - 1, point.y() - 1, 3, 3);
	return true;
}

bool ImageItemLine::distanceToPoint(const QPoint &point, float &distance)
{
	// See http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
	// Algorithm from http://paulbourke.net/geometry/pointlineplane/

	float lineLength = sqrt((float)(endX - startX) * (float)(endX - startX) + 
		(float)(endY - startY) * (float)(endY - startY));
	// Handle exceptional case: zero length line
	if(lineLength == 0)
	{
		distance = sqrt((float)(point.x() - startX) * (float)(point.x() - startX) +
			(float)(point.y() - startY) * (float)(point.y() - startY));
		return true;
	}

	float u = ((float)(point.x() - startX) * (float)(endX - startX) +
		(float)(point.y() - startY) * (float)(endY - startY)) /
		(float)(lineLength * lineLength);
	if((u < 0) || (u > 1))
	{
		return false;	// closest point does not fall within the line segment
	}

	// Closest point at line
	float x = startX + u * (float)(endX - startX);
	float y = startY + u * (float)(endY - startY);
	distance = sqrt((float)(point.x() - x) * (float)(point.x() - x) +
		(float)(point.y() - y) * (float)(point.y() - y));
	return true;
}



void ImageItemLine::parseProp(QXmlStreamReader &reader)
{
	QStringRef propName = reader.attributes().value("name");
//printf("ImageItemLine::parseProp(): propName <%s>\n", propName.toString().toAscii().constData());
	if(!propName.compare("Start", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		startX = (int)rint(c.toDouble(str.section(' ', 0, 0)));
		startY = (int)rint(c.toDouble(str.section(' ', 1, 1)));
	}
	else if(!propName.compare("End", Qt::CaseInsensitive))
	{
		QString str(reader.readElementText());
		QLocale c(QLocale::C);
		endX = (int)rint(c.toDouble(str.section(' ', 0, 0)));
		endY = (int)rint(c.toDouble(str.section(' ', 1, 1)));
	}
	else
	{
		ImageItem::parseProp(reader);
		return;
	}
}

