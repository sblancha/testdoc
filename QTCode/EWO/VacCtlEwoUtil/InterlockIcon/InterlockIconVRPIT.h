#ifndef	INTERLOCKICONVRPIT_H
#define	INTERLOCKICONVRPIT_H

#include "qwidget.h"
#include "InterlockIcon.h"

class InterlockIconVRPIT : public InterlockIcon
{
	Q_OBJECT
public:
	InterlockIconVRPIT(QWidget * parent = NULL);
	inline InterlockIconClass getIconClass() { return InterlockSource; };
	void moveIcon(int x, int y);
	QPoint getConnectionPoint(QString connectorName);
	void getConnectionExtra(QString connectorName, InterlockIconConnection::Direction &direction, int &offset, bool &writeCableNumber);
	QRect getBoundingRect();
	virtual ~InterlockIconVRPIT();
private:
	virtual void paintEvent(QPaintEvent *pEvent);
};

#endif