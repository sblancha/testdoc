#ifndef	DATETIMEEDITEWO_H
#define	DATETIMEEDITEWO_H

// Simple EWO INTERFACE FOR QDateTimeEdit widget

#include <BaseExternWidget.hxx>

#include <qdatetimeedit.h>

class EWO_EXPORT DateTimeEditEwo : public BaseExternWidget
{
	Q_OBJECT

public:
	DateTimeEditEwo(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

private slots:
	void dateTimeChange(const QDateTime &value);
		
private:
	QDateTimeEdit	*baseWidget;
};

#endif	// DATETIMEEDITEWO_H
