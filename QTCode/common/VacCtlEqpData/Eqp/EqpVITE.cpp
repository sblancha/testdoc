//	Implementation of EqpVITE class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVITE.h"

#include "DataPool.h"
#include "FunctionalTypeVPI.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#include <QColor>
#include <QDateTime>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OK		0,255,0

//	Main state bits to be analyzed - these are bits of VRJ_TC
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_ON			(0x02000000)


////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVITE::EqpVITE(const Eqp &source) : Eqp(source)
{
	temperatureOnline = temperatureReplay = 0;
	functionalType = FunctionalType::VITE;
	pMaster = NULL;
	nOnlineConnects = nReplayConnects = nPollingConnects = 0;
}

EqpVITE::~EqpVITE()
{
}

/*
**	FUNCTION
**		After all devices have been read from file - find master and
**		make 'permanent' connection to 'selectChanged() signal of master
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVITE::postProcess(void)
{
	const QString masterDp = getAttrValue("MasterName");
	if(!masterDp.isEmpty())
	{
		DataPool &pool = DataPool::getInstance();
		Eqp *pEqp = pool.findEqpByDpName(masterDp.toLatin1());
		if(pEqp)
		{
			Q_ASSERT(pEqp->inherits("EqpVRJ_TC"));
			pMaster = (EqpVRJ_TC *)pEqp;

			// In order to stay PERMANENTLY connected to master's selection
			selected = pMaster->isSelected();
			QObject::connect(pMaster, SIGNAL(selectChanged(Eqp *)), this, SLOT(masterSelectChange(Eqp *)));
		}
	}
}

/*
**	FUNCTION
**		Find master DP and master channel for this pump.
**
**	ARGUMENTS
**		masterDP		- Variable where master DP name will be written
**		masterChannel	- Not used - no channel in VIES
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVITE::getMaster(QString &masterDp, int &masterChannel)
{
	masterChannel = 0;
	masterDp = getAttrValue("MasterName");
	QString attrValue = getAttrValue("MasterChannel");
	if(!attrValue.isEmpty())
	{
		masterChannel = attrValue.toInt();
	}
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVITE::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	if(pMaster)
	{
		pMaster->getStateDpes(stateDpes);
	}
}


/*
**	FUNCTION
**		Return list of all DPEs, related to analog value of device. The list is used
**		to build value history
**
**	ARGUMENTS
**		valueDpes	- Variable where all value DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVITE::getValueDpes(QStringList &valueDpes)
{
	valueDpes.clear();
	addDpeNameToList(valueDpes, "Temperature");
}


/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Dataacquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVITE::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	// If this is the very first connection for given mode - we also need to
	// connect to master(VRPM)
	bool needMasterConnect = true;
	switch(mode)
	{
	case DataEnum::Online:
		needMasterConnect = nOnlineConnects == 0;
		break;
	case DataEnum::Replay:
		needMasterConnect = nReplayConnects == 0;
		break;
	case DataEnum::Polling:
		needMasterConnect = nPollingConnects == 0;
		break;
	}
	if(needMasterConnect)	// The very first connection
	{
		if(pMaster)
		{
			pMaster->connect(this, mode);
			// Selecting VRJ_TC must select this device
			selected = pMaster->isSelected();
		}
	}
	connectDpe(pDst, "Temperature", mode);
	switch(mode)
	{
	case DataEnum::Online:
		nOnlineConnects++;
		break;
	case DataEnum::Replay:
		nReplayConnects++;
		break;
	case DataEnum::Polling:
		nPollingConnects++;
		break;
	}
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVITE::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "Temperature", mode);
	switch(mode)
	{
	case DataEnum::Online:
		if(nOnlineConnects > 0)
		{
			nOnlineConnects--;
		}
		break;
	case DataEnum::Replay:
		if(nReplayConnects > 0)
		{
			nReplayConnects--;
		}
		break;
	case DataEnum::Polling:
		if(nPollingConnects > 0)
		{
			nPollingConnects--;
		}
		break;
	}
	// If this was the very last connection - we don't need connection to master anymore
	bool needMasterDisconnect = true;
	switch(mode)
	{
	case DataEnum::Online:
		needMasterDisconnect = nOnlineConnects == 0;
		break;
	case DataEnum::Replay:
		needMasterDisconnect = nReplayConnects == 0;
		break;
	case DataEnum::Polling:
		needMasterDisconnect = nPollingConnects == 0;
		break;
	}
	if(needMasterDisconnect)
	{
		if(pMaster)	// If we did not find it before - does not matter
		{
			pMaster->disconnect(this, mode);
		}
	}
}

/*
**	FUNCTION
**		Process selection request, pass request to master
**
**	ARGUMENTS
**		selected	- New selected state
**		isFromPvss	- true if selection request came from PVSS
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVITE::setSelected(bool selected, bool isFromPvss)
{
	if(!isFromPvss)
	{
		Eqp::setSelected(selected, isFromPvss);
		return;
	}
	if(pMaster)
	{
		pMaster->setSelected(selected, isFromPvss);
	}
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVITE::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "Temperature"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(temperatureOnline == newValue)
			{
				return;
			}
			temperatureOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**	FUNCTION
**		Process new replay value arrived from PVSS. Even though this DP
**		does not need the value (i.e. new value is the same as the old one) -
**		we still need to notify data pool that value for this DP has been
**		arrived (normally this is done by Eqp class)
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVITE::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "Temperature"))
	{
		if(value.canConvert(QVariant::Double))
		{
			float newValue = (float)value.toDouble();
			if(temperatureReplay == newValue)
			{
				return;
			}
			temperatureReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif


/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We connected
**		to VRJ_TC, which in turn could connect toPLC, so we need to check where
**		signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVITE::dpeChange(Eqp * /* pSrc */, const char *dpeName,
		DataEnum::Source source, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	if(source == DataEnum::Plc)
	{
		emit dpeChanged(this, dpeName, source, value, mode, timeStamp);
	}
	else	// VRJ_TC
	{
		emit dpeChanged(this, dpeName, DataEnum::Parent, value, mode, timeStamp);
	}
}

/*
**	FUNCTION
**		Calculate main color of device. Color is taken from master
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVITE::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(!pMaster)
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else
	{
		pMaster->getMainColor(color, mode);
	}
}
#endif

/*
**	FUNCTION
**		Return main value for pump - pressure
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Pressure value
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVITE::getMainValue(DataEnum::DataMode mode)
{
	return mode == DataEnum::Replay ? temperatureReplay : temperatureOnline;
}
#endif

#ifndef PVSS_SERVER_VERSION
bool EqpVITE::isMainValueValid(DataEnum::DataMode mode)
{
	if(isPlcAlarm(mode))
	{
		return false;
	}
	unsigned rr1 = getRR1(mode);
	return ((rr1 & RR1_VALID) != 0);
}
#endif

bool EqpVITE::isPlcAlarm(DataEnum::DataMode mode) const
{
	if(!pMaster)
	{
		return false;
	}
	return pMaster->isPlcAlarm(mode);
}

unsigned EqpVITE::getRR1(DataEnum::DataMode mode) const
{
	if(!pMaster)
	{
		return 0;
	}
	return pMaster->getRR1(mode);
}

/*
**	FUNCTION
**		Calculate main state string of device. Just use main state string of VRPI
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVITE::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(!pMaster)
	{
		string = "No master";
	}
	else
	{
		pMaster->getMainStateString(string, mode);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVITE::getMainValueString(QString &string, DataEnum::DataMode mode)
{
	if(!pMaster)
	{
		string = getVisibleName();
		string += ": No master";
	}
	else
	{
		unsigned rr1 = pMaster->getRR1(mode);
		bool plcAlarm = pMaster->isPlcAlarm(mode);
		getMainValueString(rr1, mode == DataEnum::Replay ? temperatureReplay : temperatureOnline, plcAlarm, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		temperature	- Value of Temperature DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVITE::getMainValueString(unsigned /* rr1 */, float temperature, bool /* plcAlarm */, QString &string)
{
	char buf[32];
#ifdef Q_OS_WIN
	sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%6.1f", temperature);
#else
	sprintf(buf, "%6.1f", temperature);
#endif
	string = buf;
}
#endif

/*
**	FUNCTION
**		Calculate main value string of device - to be used in menu and tooltip
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rmA			- Value of RmA DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVITE::getMainValueStringForMenu(unsigned rr1, float temperature, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC error";
	}
	else if(rr1 & RR1_VALID)
	{
		if(rr1 & RR1_ON)
		{
			char buf[32];
#ifdef Q_OS_WIN
			sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "%6.1f", temperature);
#else
			sprintf(buf, "%6.1f", temperature);
#endif
			string = buf;
		}
		else
		{
			string = "OFF";
		}
	}
	else
	{
		string = "Not valid";
	}
}
#endif


/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVITE::getMainStateStringForMenu(QString &string, DataEnum::DataMode mode)
{
	if(!pMaster)
	{
		string = getVisibleName();
		string += ": No master";
	}
	else
	{
		unsigned rr1 = pMaster->getRR1(mode);
		bool plcAlarm = pMaster->isPlcAlarm(mode);
		getMainStateStringForMenu(rr1, mode == DataEnum::Replay ? temperatureReplay : temperatureOnline, plcAlarm, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device to be shown in popup menu
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rmA			- Value of RmA DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVITE::getMainStateStringForMenu(unsigned rr1, float temperature, bool plcAlarm, QString &string)
{
	string = getVisibleName();
	string += ": ";
	QString valueString;
	getMainValueStringForMenu(rr1, temperature, plcAlarm, valueString);
	string += valueString;
}
#endif

/*
**	FUNCTION
**		Calculate string of device to be shown in tooltip
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVITE::getToolTipString(QString &string, DataEnum::DataMode mode)
{
	getMainStateStringForMenu(string, mode);
}
#endif

/*
**	FUNCTION
**		Slot activated by master when it's selection state was changed.
**		We update our selection state and emit signal
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVITE::masterSelectChange(Eqp *pSrc)
{
	if(selected != pSrc->isSelected())
	{
		selected = ! selected;
		emit selectChanged(this);
	}
}





/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVITE::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	if(!pMaster)
	{
		return;
	}
	pMaster->getDpesForDevList(dpes);
	addDpeNameToList(dpes, "Temperature");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVITE::checkDevListCrit(void)
{
	if(!pMaster)
	{
		return 1;	// This is HUGE error - must be always shown
	}
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	if(pMaster->checkDevListCrit() == 1)
	{
		return 1;
	}

	DataEnum::DataMode mode = criteria.getMode();

	// Check temperature if needed
	if(criteria.isUseTemperatureLimit())
	{
		float temperature = mode == DataEnum::Replay ? temperatureReplay : temperatureOnline;
		if(temperature > criteria.getTemperatureLimit())
		{
			return 1;
		}
	}

	// Last chance - criteria on pressure grow
	if(criteria.isUseTemperatureGrowLimit())
	{
		// Can not decide right now, more info is needed
		return 2;
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Return parameters for history query, required for device list
**
**	ARGUMENTS
**		dpe		- [out] DPE, for which history is required
**		start	- [out] start time of history query
**		end		- [out] end time of history query
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVITE::getHistoryParamForDevList(QString &dpe, QDateTime &start, QDateTime &end)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUseTemperatureGrowLimit())
	{
		dpe = "";
		return;
	}
	dpe = dpName;
	dpe += ".Temperature";
	start = criteria.getTemperatureStartTime();
	if(criteria.isTemperatureEndTimeNow())
	{
		end = QDateTime::currentDateTime();
	}
	else
	{
		end = criteria.getTemperatureEndTime();
	}
}
#endif

/*
**	FUNCTION
**		Return grow factor for main value (pressure) to check if
**		device matches device list criteria
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		Pressure grow factor; or
**		0 if criteria does not contain pressure grow
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
float EqpVITE::getGrowFactorForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUseTemperatureGrowLimit())
	{
		return 0.0;
	}
	return criteria.getTemperatureGrowLimit();
}

int EqpVITE::getSpikeFilterForDevList(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(!criteria.isUseTemperatureGrowLimit())
	{
		return 0;
	}
	return criteria.getTemperatureSpikeInterval();
}

#endif



/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVITE::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	if(!pMaster)
	{
		return;
	}

	// Without PLC connection does not match any criteria
	if(pMaster->isPlcAlarm(DataEnum::Online))
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	if(pCriteria->getType() != EqpMsgCriteria::MainValue)
	{
		pMaster->checkCriteria(pCriteria);
		return;
	}
	/* Value is always valid ???
	if(!pMaster->isValueValid(DataEnum::Online))
	{
		return;
	}
	*/
	if(pCriteria->isReverse())
	{
		if(temperatureOnline < pCriteria->getLowerLimit())
		{
			pCriteria->setMatch(true, true, temperatureOnline);
		}
		else if(temperatureOnline > pCriteria->getUpperLimit())
		{
			pCriteria->setMatch(false, true, temperatureOnline);
		}
	}
	else
	{
		if(temperatureOnline < pCriteria->getLowerLimit())
		{
			pCriteria->setMatch(false, true, temperatureOnline);
		}
		else if(temperatureOnline > pCriteria->getUpperLimit())
		{
			pCriteria->setMatch(true, true, temperatureOnline);
		}
	}
}
