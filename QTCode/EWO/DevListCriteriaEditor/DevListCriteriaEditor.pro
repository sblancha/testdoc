#
# Project file for qmake utility to build DevListCriteriaEditor EWO
#
#	19.03.2009	L.Kopylov
#		Initial version
#
#	02.01.2010	L.Kopylov
#		Changes for Windows, directories tree reogranization
#
#	26.01.2012	L.Kopylov
#		Add embedding manifest to target DLL - see https://icecontrols.cern.ch/jira/browse/ENS-4589
#

include(../VacEWO.pri)

TARGET = DevListCriteriaEditor

win32:DEFINES += _CRT_SECURE_NO_DEPRECATE

INCLUDEPATH += ../../common/VacCtlUtil \
	../../common/VacCtlEqpData/FunctionalType \
	../../common/Platform \
	../../common/VacCtlEqpData \
	../../common/VacCtlEqpData/DataPool \
	../../common/VacCtlEqpData/ResourcePool \
	../../common/VacCtlEqpData/Eqp \
	../VacCtlEwoUtil

HEADERS =	DevListCriteriaEditorWidget.h \
	SectorSelectorWidget.h \
	DevListSelectorWidget.h \
	EqpTypeSelectorWidget.h \
	SingleEqpSelectorWidget.h \
	DataModeEditorWidget.h \
	DevListCriteriaEditor.h

SOURCES = DevListCriteriaEditorWidget.cpp \
	SectorSelectorWidget.cpp \
	DevListSelectorWidget.cpp \
	EqpTypeSelectorWidget.cpp \
	SingleEqpSelectorWidget.cpp \
	DataModeEditorWidget.cpp \
	DevListCriteriaEditor.cpp

LIBS += -lVacCtlEwoUtil \
	-lVacCtlEqpData \
	-lVacCtlHistoryData \
	-lVacCtlUtil

