//	Implementation of FlashingRect class
/////////////////////////////////////////////////////////////////////////////////

#include "FlashingRect.h"

#include <qpainter.h>

FlashingRect::FlashingRect(QWidget *parent, Qt::WindowFlags f)
	: QWidget(parent, f), color(255, 0, 0)
{
	setMinimumSize(3, 3);
	halfPeriod = 500;
	fillInterior = true;
	pTimer = new QTimer(this);
	pTimer->setSingleShot(false);
	connect(pTimer, SIGNAL(timeout(void)), this, SLOT(timerDone(void)));
	pTimer->start(halfPeriod);
}

FlashingRect::~FlashingRect()
{
}

void FlashingRect::timerDone(void)
{
	fillInterior = !fillInterior;
	update();
}

void FlashingRect::paintEvent(QPaintEvent * /* pEvent */)
{
	if(fillInterior)
	{
		QPainter painter(this);
		QRect myRect(rect());
		if(myRect.width() > 3)
		{
			myRect.setLeft(1);
			myRect.setWidth(myRect.width() - 1);
		}
		if(myRect.height() > 3)
		{
			myRect.setTop(1);
			myRect.setHeight(myRect.height() - 1);
		}
		painter.fillRect(myRect, color);
	}
}

