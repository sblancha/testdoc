//	Implementation of EqpVV_AO class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVV_AO.h"

#include "DataPool.h"
#include "FunctionalTypeVV_ANA.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_OPEN				0,255,0
#define	COLOR_PART_OPEN			92,184,197
#define	COLOR_OPEN_ERROR		204,255,0
#define	COLOR_OPEN_WARNING		153,255,0
#define COLOR_CLOSED			255,0,0
#define COLOR_CLOSED_ERROR		255,0,0
#define	COLOR_CLOSED_WARNING	255,0,0
#define	COLOR_ERROR				255,102,255
#define	COLOR_WARNING			255,255,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_OPEN		(0x02000000)
#define	RR1_CLOSED		(0x01000000)
#define RR1_ERROR_CODE_MASK	(0x000000FF)
#define RR1_WARNING_CODE_MASK	(0x0000FF00)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVV_AO::EqpVV_AO(const Eqp &source) : Eqp(source)
{
	rr1Online = setPointOnline = rr1Replay = setPointReplay = 0;
	plcAlarmOnline = plcAlarmReplay = false;
	functionalType = FunctionalType::VV_ANA;
}

EqpVV_AO::~EqpVV_AO()
{
}

/*
**	FUNCTION
**		Return list of all DPEs, related to state of device. The list is used
**		to build state history
**
**	ARGUMENTS
**		stateDpes	- Variable where all state DPEs shall be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV_AO::getStateDpes(QStringList &stateDpes)
{
	stateDpes.clear();
	addDpeNameToList(stateDpes, "EqpStatus");
	addDpeNameToList(stateDpes, "RR1");
	addDpeNameToList(stateDpes, "SetPointR");
}

/*
**	FUNCTION
**		Connect DPEs of interest for this device to obtain information
**		required for display
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV_AO::connect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	connectDpe(pDst, "EqpStatus", mode);
	connectDpe(pDst, "RR1", mode);
	connectDpe(pDst, "SetPointR", mode);
}

/*
**	FUNCTION
**		Disconnect previously connected DPEs of this device
**
**	ARGUMENTS
**		pDst	- Pointer to destination class who wants to be notified
**					about changes in this device's state
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV_AO::disconnect(InterfaceEqp *pDst, DataEnum::DataMode mode)
{
	disconnectDpe(pDst, "SetPointR", mode);
	disconnectDpe(pDst, "RR1", mode);
	disconnectDpe(pDst, "EqpStatus", mode);
}

/*
**	FUNCTION
**		Process new online value arrived from PVSS
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV_AO::newOnlineValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Online == newValue)
			{
				return;
			}
			rr1Online = newValue;
		}
	}
	else if(!strcmp(dpe, "SetPointR"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(setPointOnline == newValue)
			{
				return;
			}
			setPointOnline = newValue;
		}
	}
	Eqp::newOnlineValue(dpe, value, timeStamp);
}

/*
**	FUNCTION
**		Process new replay value arrived from PVSS.
**
**	ARGUMENTS
**		dpe		- DPE name
**		value	- DPE value
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_AO::newReplayValue(const char *dpe, const QVariant &value, const QDateTime &timeStamp)
{
	if(!strcmp(dpe, "RR1"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(rr1Replay == newValue)
			{
				return;
			}
			rr1Replay = newValue;
		}
	}
	else if(!strcmp(dpe, "SetPointR"))
	{
		if(value.canConvert(QVariant::UInt))
		{
			unsigned newValue = value.toUInt();
			if(setPointReplay == newValue)
			{
				return;
			}
			setPointReplay = newValue;
		}
	}
	Eqp::newReplayValue(dpe, value, timeStamp);
}
#endif

/*
**	FUNCTION
**		Slot to be activated by signal of PLC state change. We only connect
**		to PLC, so we don't need to check where signal came from
**
**	ARGUMENTS
**		pSrc	- Pointer to object - source of signal (PLC)
**		dpeName	- DPE name
**		source	- DPE change source (see enum above)
**		value	- New value for DPE
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV_AO::dpeChange(Eqp *pSrc, const char *dpeName,
	DataEnum::Source /* source */, const QVariant &value, DataEnum::DataMode mode, const QDateTime &timeStamp)
{
	bool alarm = !pSrc->isAlive(mode);
	if(mode == DataEnum::Replay)
	{
		if(alarm == plcAlarmReplay)
		{
			return;
		}
		plcAlarmReplay = alarm;
	}
	else
	{
		if(alarm == plcAlarmOnline)
		{
			return;
		}
		plcAlarmOnline = alarm;
	}
	emit dpeChanged(this, dpeName, DataEnum::Plc, value, mode, timeStamp);
}

/*
**	FUNCTION
**		Calculate main color of device
**
**	ARGUMENTS
**		color	- Variable where resulting color will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_AO::getMainColor(QColor &color, DataEnum::DataMode mode)
{
	if(isColorByCtlStatus(color, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainColor(rr1Replay, setPointReplay, plcAlarmReplay, color);
	}
	else
	{
		getMainColor(rr1Online, setPointOnline, plcAlarmOnline, color);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_AO::getMainColor(unsigned rr1, unsigned setPoint, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(rr1 & RR1_ERROR)	// Still different colors for open/closed
	{
		if(rr1 & RR1_OPEN)
		{
			if(rr1 & RR1_CLOSED)
			{
				color.setRgb(COLOR_ERROR);
			}
			else
			{
				color.setRgb(COLOR_OPEN_ERROR);
			}
		}
		else if(rr1 & RR1_CLOSED)
		{
			color.setRgb(COLOR_CLOSED_ERROR);
		}
		else
		{
			color.setRgb(COLOR_ERROR);
		}
	}

	// No errors - check open/closed state
	else if((rr1 & RR1_OPEN) && (rr1 & RR1_CLOSED))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_WARNING)
	{
		if(rr1 & RR1_OPEN)
		{
			if(rr1 & RR1_CLOSED)
			{
				color.setRgb(COLOR_WARNING);
			}
			else
			{
				color.setRgb(COLOR_OPEN_WARNING);
			}
		}
		else if(rr1 & RR1_CLOSED)
		{
			color.setRgb(COLOR_CLOSED_WARNING);
		}
		else
		{
			color.setRgb(COLOR_WARNING);
		}
	}
	else if(rr1 & RR1_OPEN)
	{
		color.setRgb(COLOR_OPEN);
	}
	else if(rr1 & RR1_CLOSED)
	{
		color.setRgb(COLOR_CLOSED);
	}
	else	// Neither open nor closed - either partially open or error
	{
		unsigned openPercent = setPoint;
		if((50 < openPercent) && (openPercent < 950))	// Partially open
		{
			color.setRgb(COLOR_PART_OPEN);
		}
		else
		{
			color.setRgb(COLOR_UNDEFINED);
		}
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		string	- Variable where resulting string will be put
**		mode	- Data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_AO::getMainStateString(QString &string, DataEnum::DataMode mode)
{
	if(isStateStringByCtlStatus(string, mode))
	{
		return;
	}
	if(mode == DataEnum::Replay)
	{
		getMainStateString(rr1Replay, setPointReplay, plcAlarmReplay, string);
	}
	else
	{
		getMainStateString(rr1Online, setPointOnline, plcAlarmOnline, string);
	}
}
#endif

/*
**	FUNCTION
**		Calculate main state string of device
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		rr2			- Value of RR2 DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting string will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_AO::getMainStateString(unsigned rr1, unsigned setPoint, bool plcAlarm, QString &string)
{
	if(plcAlarm)
	{
		string = "PLC Error";
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		string = "Not valid";
		return;
	}
	if(rr1 & RR1_ERROR)	// Still different strings for open/closed
	{
		if(rr1 & RR1_OPEN)
		{
			if(rr1 & RR1_CLOSED)
			{
				string = "Error";
			}
			else
			{
				string = "Open (error)";
			}
		}
		else if(rr1 & RR1_CLOSED)
		{
			string = "Closed (error)";
		}
		else
		{
			string = "Error";
		}
	}
	// No errors - check open/closed state
	if((rr1 & RR1_OPEN) && (rr1 & RR1_CLOSED))
	{
		string = "Error";
	}
	else if(rr1 & RR1_WARNING)
	{
		if(rr1 & RR1_OPEN)
		{
			if(rr1 & RR1_CLOSED)
			{
				string = "Error";
			}
			else
			{
				string = "Open (warning)";
			}
		}
		else if(rr1 & RR1_CLOSED)
		{
			string = "Closed (warning)";
		}
		else
		{
			string = "Undefined (warning)";
		}
	}
	else if(rr1 & RR1_OPEN)
	{
		string = "Open";
	}
	else if(rr1 & RR1_CLOSED)
	{
		string = "Closed";
	}
	else	// Neither open nor closed - either partially open or error
	{
		unsigned openPercent = setPoint;
		if((50 < openPercent) && (openPercent < 950))	// Partially open
		{
			char buf[64];
#ifdef Q_OS_WIN
			sprintf_s(buf, sizeof(buf) / sizeof(buf[0]), "Open (%d/1000)", openPercent);
#else
			sprintf(buf, "Open (%d/1000)", openPercent);
#endif
			string = buf;
		}
		else
		{
			string = "Undefined";
		}
	}
}
#endif


/*
**	FUNCTION
**		Calculate main state string and color for for moment in the past,
**		data are taken from state archive data pool
**
**	ARGUMENTS
**		pPool	- Pointer to state history data pool
**		ts		- Moment when state is needed
**		state	- Variable where resulting state string will be written
**		color	- Variable where resulting color will be written
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_AO::getHistoryState(VacCtlStateArchive *pPool, QDateTime &ts,
		QString &state, QColor &color)
{
	state = "";
	QList<QByteArray> dpeNames;
	dpeNames.append("EqpStatus");
	dpeNames.append("RR1");
	dpeNames.append("SetPointR");
	QList<QVariant *> values;
	if (isHistoryStateByCtlStatus(pPool, ts, dpeNames, values, state, color)) {
		return;
	}
	QVariant *pVariant = values.takeFirst();
	unsigned rr1 = pVariant->toUInt();
	delete pVariant;
	if (values.isEmpty()) {
		return;
	}
	pVariant = values.takeFirst();
	unsigned setPoint = pVariant->toUInt();
	delete pVariant;
	getMainStateString(rr1, setPoint, false, state);
	getMainColor(rr1, setPoint, false, color);
	while(!values.isEmpty())
	{
		delete values.takeFirst();
	}
}
#endif

/*
**	FUNCTION
**		Return error code for this device
**
**	ARGUMENTS
**		mode	- Data acquisition mode
**
**	RETURNS
**		Error code
**
**	CAUTIONS
**		None
*/
int EqpVV_AO::getErrorCode(DataEnum::DataMode mode)
{
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	return rr1 & RR1_ERROR_CODE_MASK;
}





/*
**	FUNCTION
**		Decide which DPEs shall be read to check if this device matches
**		current device list criteria or not
**
**	ARGUMENTS
**		dpes	- Variable where all required DPEs will be added
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVV_AO::getDpesForDevList(QStringList &dpes)
{
	dpes.clear();
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			pPlc->getStateDpes(dpes);
		}
	}
	addDpeNameToList(dpes, "EqpStatus");
	addDpeNameToList(dpes, "RR1");
	addDpeNameToList(dpes, "SetPointR");
}
#endif

/*
**	FUNCTION
**		Check if device in recent state matches device list criteria or not
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		0	= Device does not match criteria
**		1	= Device matches criteria
**		2	= More information is needed (history) - NOT for valves
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
int EqpVV_AO::checkDevListCrit(void)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	if(criteria.isEmpty())
	{
		return 1;	// No criteria - all devices are needed
	}

	DataEnum::DataMode mode = criteria.getMode();

	// If device is not connected - it can only match 'not connected' criteria, others do not make sense
	if(checkDevListNotConnectedCriteria(mode))
	{
		return criteria.isNotConnected() ? 1 : 0;
	}

	// Fisrt check if PLC is alive
	const QString plcName = findPlcName();
	if(!plcName.isEmpty())
	{
		Eqp *pPlc = DataPool::getInstance().findPlc(plcName);
		if(pPlc)
		{
			if(!pPlc->isAlive(mode))
			{
				return criteria.isAccess() ? 1 : 0;
			}
		}
	}

	// Either PLC is alive or there is no PLC - check this device's state
	// Check if state is valid
	unsigned rr1 = mode == DataEnum::Replay ? rr1Replay : rr1Online;
	if(!(rr1 & RR1_VALID))
	{
		return criteria.isAccess() ? 1 : 0;
	}
	// Next check general device state
	int state = DevListCriteria::EqpStateOther;
	if((rr1 & (RR1_OPEN | RR1_CLOSED)) == RR1_OPEN)
	{
		state = DevListCriteria::EqpStateNormal;
	}
	else if((rr1 & (RR1_OPEN | RR1_CLOSED)) == RR1_CLOSED)
	{
		state = DevListCriteria::EqpStateAbnormal;
	}
	if(criteria.generalStateMatches(state))
	{
		return 1;
	}

	// General state does not match - check other criteria
	if(criteria.isErrors())
	{
		if(rr1 & RR1_ERROR)
		{
			return 1;
		}
	}
	if(criteria.isWarnings())
	{
		if(rr1 & (RR1_ERROR | RR1_WARNING))
		{
			return 1;
		}
	}
	return 0;
}
#endif

/*
**	FUNCTION
**		Check if device matches criteria for E-mail/SMS notification
**
**	ARGUMENTS
**		pCriteria	- Pointer to criteria to check
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void EqpVV_AO::checkCriteria(EqpMsgCriteria *pCriteria)
{
	// Only accept criteria for this functional type
	if(pCriteria->getFunctionalType() != functionalType)
	{
		return;
	}

	if(pCriteria->getType() == EqpMsgCriteria::None)
	{
		return;
	}

	// If device is not connected - it does not match any criteria
	if(checkDevListNotConnectedCriteria(DataEnum::Online))
	{
		pCriteria->setMatch(false, false, 0);
		return;
	}

	// Without PLC connection does not match any criteria
	if(plcAlarmOnline)
	{
		// Do not change pCriteria->setMatch(false);
		return;
	}

	// In invalid state - only matches Error/warning criteria
	if(!(rr1Online & RR1_VALID))
	{
		switch(pCriteria->getType())
		{
		case EqpMsgCriteria::Error:
			if(rr1Online & RR1_ERROR)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case EqpMsgCriteria::Warning:
			if(rr1Online & (RR1_ERROR | RR1_WARNING))
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		default:
			pCriteria->setMatch(false, false, 0);
			return;
		}
	}

	// State is valid - decide based on criteria
	unsigned maskedRr1 = rr1Online & (RR1_OPEN | RR1_CLOSED);
	int mainValue = setPointOnline;
	switch(pCriteria->getType())
	{
	case EqpMsgCriteria::MainState:
		switch(pCriteria->getSubType())
		{
		case FunctionalTypeVV_ANA::Open:
			if(maskedRr1 == RR1_OPEN)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		case FunctionalTypeVV_ANA::Closed:
			if(maskedRr1 == RR1_CLOSED)
			{
				pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
				return;
			}
			else
			{
				pCriteria->setMatch(pCriteria->isReverse(), false, 0);
				return;
			}
			break;
		}
		break;
	case EqpMsgCriteria::Error:
		if(rr1Online & RR1_ERROR)
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else
		{
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	case EqpMsgCriteria::Warning:
		if(rr1Online & (RR1_ERROR | RR1_WARNING))
		{
			pCriteria->setMatch(!pCriteria->isReverse(), false, 0);
			return;
		}
		else
		{
			pCriteria->setMatch(pCriteria->isReverse(), false, 0);
			return;
		}
		break;
	case EqpMsgCriteria::MainValue:
		if(rr1Online & RR1_VALID)
		{
			if(pCriteria->isReverse())
			{
				if(mainValue > pCriteria->getUpperLimit())
				{
					pCriteria->setMatch(false, true, mainValue);
					return;
				}
				else if(mainValue < pCriteria->getLowerLimit())
				{
					pCriteria->setMatch(true, true, mainValue);
					return;
				}
				// Between lower and upper limit - no change
				return;
			}
			else
			{
				if(mainValue > pCriteria->getUpperLimit())
				{
					pCriteria->setMatch(true, true, mainValue);
					return;
				}
				else if(mainValue < pCriteria->getLowerLimit())
				{
					pCriteria->setMatch(false, true, mainValue);
					return;
				}
				// Between lower and upper limit - no change
				return;
			}
		}
		break;
	default:
		break;
	}
	pCriteria->setMatch(false, false, 0);
}

