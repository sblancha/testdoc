#ifndef FLEXTABLE_H
#define	FLEXTABLE_H

// EWO interface for QTableWidget widget

#include <BaseExternWidget.hxx>

#include <QTableWidget>

class EWO_EXPORT FlexTable : public BaseExternWidget
{
	Q_OBJECT

public:
	FlexTable(QWidget *parent);
	virtual QWidget *widget() const { return baseWidget; }
	virtual QStringList signalList(void) const;
	virtual QStringList methodList(void) const;

public slots:
	virtual QVariant invokeMethod(const QString &name, QList<QVariant> &values, QString &error);

protected:
	QVariant processSetCellText(const QString &name, QList<QVariant> &values, QString &error);
	QVariant processSetCellColor(const QString &name, QList<QVariant> &values, QString &error);
	QVariant processShowRow(const QString &name, QList<QVariant> &values, QString &error);
	QVariant processHideRow(const QString &name, QList<QVariant> &values, QString &error);
	QVariant processShowColumn(const QString &name, QList<QVariant> &values, QString &error);
	QVariant processHideColumn(const QString &name, QList<QVariant> &values, QString &error);
	QVariant processSetColumnHeader(const QString &name, QList<QVariant> &values, QString &error);
	QVariant processSetColumnHeaderVisible(const QString &name, QList<QVariant> &values, QString &error);
	QVariant processSetRowHeader(const QString &name, QList<QVariant> &values, QString &error);
	QVariant processSetRowHeaderVisible(const QString &name, QList<QVariant> &values, QString &error);
	QVariant processColumnWidth(const QString &name, QList<QVariant> &values, QString &error);
	QVariant processSetColumnWidth(const QString &name, QList<QVariant> &values, QString &error);

	bool checkRow(int row, QString &error);
	bool checkColumn(int column, QString &error);

private:
	QTableWidget	*baseWidget;

private slots:
	void tableCellClicked(int row, int column);
};

#endif	// FLEXTABLE_H
