//	Implementation of EqpVGF_DP class
/////////////////////////////////////////////////////////////////////////////////

#include "EqpVGF_DP.h"

#include "DataPool.h"
#include "FunctionalTypeVG.h"
#include "EqpMsgCriteria.h"

#include "DpConnection.h"

#include "DevListCriteria.h"

#ifndef PVSS_SERVER_VERSION
#include "VacCtlStateArchive.h"
#endif

#include <QColor>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0
#define COLOR_WARNING			255,255,0
#define COLOR_UNDERRANGE		0,180,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define	RR1_ON			(0x02000000)
#define	RR1_OFF			(0x01000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define RR1_WARNING_CODE_MASK	(0x0000FF00)
#define RR1_ERROR_CODE_MASK	(0x000000FF)

////////////////////////////////////////////////////////////////////////////////////
/////////////// Construction/destruction

EqpVGF_DP::EqpVGF_DP(const Eqp &source) : EqpVG_STD(source)
{
	functionalType = FunctionalType::VGF;
	haveBlockedOff = true;	// ????
}

EqpVGF_DP::~EqpVGF_DP()
{
}

/*
**	FUNCTION
**		Calculate main color of device.
**
**	ARGUMENTS
**		rr1			- Value of RR1 DPE
**		pr			- Value of PR DPE
**		plcAlarm	- Value of PLC alarm
**		color		- Variable where resulting color will be put
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
#ifndef PVSS_SERVER_VERSION
void EqpVGF_DP::getMainColor(unsigned rr1, float /* pr */, bool plcAlarm, QColor &color)
{
	if(plcAlarm)
	{
		color.setRgb(COLOR_UNDEFINED);
		return;
	}
	if(!(rr1 & RR1_VALID))
	{
		color.setRgb(COLOR_UNDEFINED);
	}
	else if(rr1 & RR1_ERROR)
	{
		color.setRgb(COLOR_ERROR);
	}
	else if((rr1 & RR1_ON) && (rr1 & RR1_OFF))
	{
		color.setRgb(COLOR_ERROR);
	}
	else if(rr1 & RR1_WARNING)
	{
		// Warnings 'overrange' = 127, 'underrange' = 128
		int warning = (rr1 & RR1_WARNING_CODE_MASK) >> 8;
		if(warning == 128)
		{
			color.setRgb(COLOR_UNDERRANGE);
		}
		else
		{
			color.setRgb(COLOR_WARNING);
		}
	}
	else if(rr1 & RR1_OFF)
	{
		color.setRgb(COLOR_OFF);
	}
	else if(rr1 & RR1_ON)
	{
		color.setRgb(COLOR_ON);
	}
	else
	{
		color.setRgb(COLOR_UNDEFINED);
	}
}
#endif
