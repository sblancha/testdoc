#ifndef	VACICONVG_PT_P_H
#define	VACICONVG_PT_P_H

//	Icon for VGP - type VG_PT

#include "VacIconVG.h"

class VACCTLEWOUTIL_EXPORT VacIconVG_PT_P : public VacIconVG
{
public:
	VacIconVG_PT_P(QWidget *parent, Qt::WindowFlags f = 0) :
		VacIconVG(parent, f) {}

	virtual void setEqp(Eqp *pEqp);
	virtual bool connect(void);

protected:
	virtual void drawSpecific(QPainter &painter);
};

#endif	// VACICONVGP_H
