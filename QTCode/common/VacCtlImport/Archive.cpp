//	Implementation of Archive class
///////////////////////////////////////////////////////////////////////////////

#include "PlatformDef.h"
#include "Archive.h"
#include "ImportPool.h"

#include <QStringList>

#ifdef Q_OS_WIN
#define	sscanf	sscanf_s
#endif

ImportPool	*Archive::pPool = NULL;

void Archive::setImportPool(ImportPool *pImportPool)
{
	pPool = pImportPool;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
Archive::Archive(const char *name, int lineNo)
{
	this->name = pPool->addString(name);
	this->lineNo = lineNo;
	refCount = 0;
	smoothType = timeInterval = deadBand = depth = EMPTY_INT_VALUE;
	dpName = NULL;
}

/*
**
**	FUNCTION
**		Create new Archive instance from data read in file.
**
**	PARAMETERS
**		pFile		- Pointer to file being read.
**		lineNo		- Line number in file.
**		name		- name of new archive
**		errList		- List where error message shall be added
**						in case of errors.
**
**	RETURNS
**		Pointer to new Archive instance created from data in file;
**		NULL in case of error.
**
**	CAUTIONS
**		It is responsability of caller to destroy returned instance using 'delete'
**		operator.
*/
Archive *Archive::createFromFile(FILE *pFile, int &lineNo, const char *name,
	QStringList &errList)
{
	Archive	*pNew = new Archive(name, lineNo);
	if(pNew->parseFile(pFile, lineNo, errList) < 0)
	{
		delete pNew;
		return NULL;
	}

	if(pNew->getSmoothType() == EMPTY_INT_VALUE)
	{
		PARSING_WARNING1("'SmoothType' isn't specified for archive <%s>", pNew->getName());
	}
	if(pNew->getTimeInterval() == EMPTY_INT_VALUE)
	{
		PARSING_WARNING1("'TimeInterval' isn't specified for archive <%s>", pNew->getName());
	}
	if(pNew->getDeadBand() == EMPTY_INT_VALUE)
	{
		PARSING_WARNING1("'DeadBand' isn't specified for archive <%s>", pNew->getName());
	}
	if(pNew->getDepth() == EMPTY_INT_VALUE)
	{
		PARSING_WARNING1("'DepthDays' isn't specified for archive <%s>", pNew->getName());
	}
	return(pNew);
}

/*
**
**	FUNCTION
**		Process tokens of single line read from input file
**
**	PARAMETERS
**		pFile	- Pointer to file being read
**		lineNo	- Line number in file.
**		tokens	- Pointer to array of tokens in file.
**		nTokens	- Number of tokens.
**		errList	- Variable where error message shall be added
**					in case of errors.
**
**	RETURNS
**		0	- tokens have been processed successfully, no more lines are
**				expected for this equipment.
**		-1	- tokens have not been processed, i.e. error in input file.
**
**	CAUTIONS
**		None
*/
int Archive::processLineTokens(FILE * /* pFile */, int &lineNo, char **tokens, int nTokens,
	const QString & /* longValue */, QStringList &errList)
{
	const char *funcName = "Archive::processLineTokens()";
	if(!strcasecmp(tokens[0], "SmoothType"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &smoothType) != 1)
		{
			PARSING_WARNING2("Line %d: bad SmoothType format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "TimeInterval"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &timeInterval) != 1)
		{
			PARSING_WARNING2("Line %d: bad TimeInterval format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "DeadBand"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &deadBand) != 1)
		{
			PARSING_WARNING2("Line %d: bad DeadBand format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "DepthDays"))
	{
		CHECK_TOKEN_COUNT(nTokens, 2, lineNo, funcName);
		if(sscanf(tokens[1], "%d", &depth) != 1)
		{
			PARSING_WARNING2("Line %d: bad DepthDays format <%s>", lineNo, tokens[1]);
		}
		return 1;
	}
	else if(!strcasecmp(tokens[0], "ArchiveEnd"))		// End of archive
	{
		return 0;
	}
	PARSING_ERR2("Line %d: unexpected <%s>", lineNo, tokens[0]);
}

/*
**
**	FUNCTION
**		Set DP name for this archive
**
**	PARAMETERS
**		name	- Pointer to name to be set
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void Archive::setDpName(const char *name)
{
	dpName = pPool->addString(name);
}

/*
**
**	FUNCTION
**		Dump instance content to output file for debugging
**
**	PARAMETERS
**		pFile		- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void Archive::dump(FILE *pFile)
{
	if(!ImportPool::isDebugMode())
	{
		return;
	}
	fprintf(pFile, "============ ARCHIVE <%s> (DP <%s>\n", name, (dpName ? dpName : "NULL"));
	fprintf(pFile, "    type %d interval %d deadBand %d depth %d\n", smoothType,
		timeInterval, deadBand, depth);
	fprintf(pFile, "    lineNo %d refCount %d\n", lineNo, refCount);
}
