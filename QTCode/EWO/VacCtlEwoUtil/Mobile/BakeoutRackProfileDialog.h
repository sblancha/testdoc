#ifndef	BAKEOUTRACKPROFILEDIALOG_H
#define	BAKEOUTRACKPROFILEDIALOG_H

// Bakeout rack profile

#include "DataEnum.h"

#include <QDialog>

class BakeoutRackProfileImage;
class BakeoutTree;

class Sector;
class Eqp;

class ExtendedLabel;

#include <QLabel>
#include <QMenu>

class BakeoutRackProfileDialog : public QDialog
{
	Q_OBJECT

public:
	BakeoutRackProfileDialog();
	~BakeoutRackProfileDialog();

	static void create(void);

signals:
	void dpMouseDown(int button, int mode, int x, int y, int extra, const char *dpName);

protected:

	// The only instances of dialog
	static BakeoutRackProfileDialog *pInstance;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////// Dialog elements /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	// Tree of main parts, sectors and devices
	BakeoutTree			*pTree;

	// Label with selected rack name
	ExtendedLabel		*pEqpNameLabel;

	// Label for PCS (== Power Cut System) state indication
	QLabel				*pPcsLabel;

	// Label for safety state indication
	QLabel				*pSafetyLabel;

	// File menu
	QMenu		*pFileMenu;

	// Help menu
	QMenu		*pHelpMenu;

	// History image
	BakeoutRackProfileImage	*pImage;

	void buildLayout(void);
	void buildFileMenu(void);
	void buildHelpMenu(void);

private slots:
	void eqpSelectChanged(Eqp *pEqp);
	void eqpStateChange(void);

	void filePrint(void);
	void help(void);
};

#endif	// BAKEOUTRACKPROFILEDIALOG_H
