#ifndef LHCBEAMSECTORORDER_H
#define LHCBEAMSECTORORDER_H

// Class implementing very specific ordering of LHC beam sectors

#include <qlist.h>

class Sector;

class LhcBeamSectorOrder
{
public:
	LhcBeamSectorOrder();
	~LhcBeamSectorOrder();

	void addSector(Sector *pSector);
	Sector *getNext(void);

protected:
	// List of sectors on inner beam
	QList<Sector *>	*pInnerList;

	// List of sectors on outer beam
	QList<Sector *>	*pOuterList;

	// List of sectors on cross and common beam
	QList<Sector *>	*pCrossList;

	Sector *decide(Sector *pInner, Sector *pOuter, Sector *pCross);
};

#endif	// LHCBEAMSECTORORDER_H

