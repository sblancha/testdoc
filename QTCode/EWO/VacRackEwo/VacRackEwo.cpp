#include "VacRackEwo.h"
#include <math.h>
#include <QPainter>
#include <QFont>

EWO_PLUGIN(EwoInterface)

VacRackEwo::VacRackEwo(QWidget *parent) : QWidget(parent)
{

}


VacRackEwo::~VacRackEwo(){

}

void VacRackEwo::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	QFont calibri("Calibri", 12, QFont::Bold);
	painter.setRenderHint(QPainter::Antialiasing);

	//draw outside rectangle
	painter.setBrush(Qt::gray);
	painter.drawRect(40, 95, 220, 675+35);

	//draw inside rectangle
	painter.setBrush(Qt::white);
	painter.drawRect(50, 120, 200, 675);

	//draw rack name
	painter.setFont(calibri);
	painter.setBrush(Qt::yellow);
	painter.drawRect(100, 99, 100, 17);
	painter.drawText(QRect(40, 95-1, 220, 25), Qt::AlignCenter,"VY06=TZ76");

	//draw lines inside rack
	painter.setBrush(Qt::white);
	for (int i = 1; i <= 44; i++)
	{
		painter.drawLine(50, 120 + i * 675 / 45, 250, 120 + i * 675 / 45);
	}

	//painter.fillRect(50,20,200,450, QColor(255,255,255));

	calibri.setWeight(QFont::Bold);
	calibri.setPointSize(9);
	painter.setFont(calibri);
	int textFlags;
	QFont cableFont("Calibri", 10, QFont::Normal);
	QFont calibriSmall("Calibri", 8);
	QFont calibriExtraSmall("Calibri", 6);
	QFont calibriSmallBold("Calibri", 8, QFont::Bold);
	QFont calibriExtraSmallBold("Calibri", 6, QFont::Bold);

	QString cableNumber;

	for (int i = 0; i < equipment.size(); i++)
	{
		
		QRect currentEquip = *(equipment.at(i));
		if (equipTypes.at(i).compare("VRPITHJA") == 0 || equipTypes.at(i).compare("VRPITHJB") == 0)
		{
			painter.setBrush(QColor(220,220,220));
			painter.setFont(calibri);
			textFlags = Qt::AlignHCenter | Qt::AlignTop | Qt::TextWrapAnywhere;
			painter.drawRect(*(equipment.at(i)));
			painter.drawText(*(equipment.at(i)), textFlags, equipNames.at(i));

			//write cable number
			painter.setFont(cableFont);
			
			//detailText.adjust(RACK_PIXEL_WIDTH/2, 12, 0, 0);
			cableNumber = "SVA3 1713647";
			painter.drawText(currentEquip.left() + RACK_PIXEL_WIDTH / 2, currentEquip.top() + 10, RACK_PIXEL_WIDTH / 2, 16, Qt::AlignCenter, cableNumber);

			//write channel + et200 rack
			painter.drawText(currentEquip.left() + RACK_PIXEL_WIDTH / 2, currentEquip.top() + 24, RACK_PIXEL_WIDTH / 2, 16, Qt::AlignCenter, "CH 1 VY14");

			//write cable to et200
			painter.drawText(currentEquip.left() + RACK_PIXEL_WIDTH / 2, currentEquip.top() + 38, RACK_PIXEL_WIDTH / 2, 16, Qt::AlignCenter, "MCA8 17121994");

			//draw status
			
			painter.setBrush(Qt::green);
			painter.drawEllipse(currentEquip.left() + 58, currentEquip.top() + 17, 8, 8);
			painter.setFont(calibriSmall);
			painter.drawText(currentEquip.left() + 10, currentEquip.top() + 16, 40, 8, Qt::AlignVCenter | Qt::AlignLeft, "Status");

			//read current
			//outside rectangle
			painter.setBrush(Qt::white);
			painter.drawRect(currentEquip.left() + 8, currentEquip.top() + 30, 60, 24);
			painter.setBrush(Qt::gray);
			painter.drawRect(currentEquip.left() + 8, currentEquip.top() + 30, 60, 12);

			//write current
			painter.setFont(calibriSmall);
			painter.drawText(currentEquip.left() + 8, currentEquip.top() + 30, 60, 12, Qt::AlignCenter, "Current (A)");
			painter.drawText(currentEquip.left() + 8, currentEquip.top() + 42, 60, 12, Qt::AlignCenter, "1.03E-006");

		}
		else if (equipNames.at(i).left(5).compare("VRIVC") == 0)
		{
			painter.setFont(calibriSmallBold);
			painter.setBrush(QColor(220, 220, 220));
			// test
			painter.setBrush(Qt::white);
			//
			textFlags = Qt::AlignCenter | Qt::TextWrapAnywhere;


			painter.drawRect(*(equipment.at(i)));
			painter.drawText(*(equipment.at(i)), textFlags, equipNames.at(i));

			//draw LEDS
			painter.setBrush(Qt::red);
			painter.drawEllipse(currentEquip.left() + 6, currentEquip.top() + 4, 8, 8);
			painter.drawEllipse(currentEquip.left() + 23, currentEquip.top() + 4, 8, 8);
			painter.drawEllipse(currentEquip.left() + 40, currentEquip.top() + 4, 8, 8);

			painter.drawEllipse(currentEquip.right() - 14, currentEquip.top() + 4, 8, 8);
			painter.drawEllipse(currentEquip.right() - 31, currentEquip.top() + 4, 8, 8);
			painter.drawEllipse(currentEquip.right() - 48, currentEquip.top() + 4, 8, 8);


		}
		else if (equipTypes.at(i).compare("VRGPT300") == 0)
		{
			painter.setFont(calibriSmallBold);
			painter.setBrush(QColor(220, 220, 220));
			textFlags = Qt::AlignHCenter | Qt::AlignTop | Qt::TextWrapAnywhere;

			//split in lines
			int lineSplitIndex = equipNames.at(i).indexOf("\.", 7);
			QString firstLine = equipNames.at(i).left(lineSplitIndex + 1);
			QString secondLine = equipNames.at(i).right(equipNames.at(i).length() - (lineSplitIndex + 1));

			painter.drawRect(currentEquip);
			painter.drawText(currentEquip, textFlags, firstLine);
			painter.drawText(currentEquip.left(), currentEquip.top() + 9, currentEquip.width(), currentEquip.height() - 9, textFlags, secondLine);
		}
		else if (equipTypes.at(i).compare("VRGPK001") == 0)
		{
			painter.setFont(calibriSmallBold);
			painter.setBrush(QColor(220, 220, 220));
			textFlags = Qt::AlignHCenter | Qt::AlignTop | Qt::TextWrapAnywhere;

			//split in lines
			int lineSplitIndex = equipNames.at(i).indexOf("\.", 7);
			QString firstLine = equipNames.at(i).left(lineSplitIndex + 1);
			QString secondLine = equipNames.at(i).right(equipNames.at(i).length() - (lineSplitIndex + 1));

			painter.drawRect(currentEquip);
			painter.drawText(currentEquip, textFlags, firstLine);
			painter.drawText(currentEquip.left(), currentEquip.top() + 9, currentEquip.width(), currentEquip.height() - 9, textFlags, secondLine);

			//write VGI name
			painter.setFont(calibriExtraSmall);
			painter.drawText(currentEquip.left(), currentEquip.top() + 18, currentEquip.width(), currentEquip.height() - 18, textFlags, "VGI.234.5L7.R");
			painter.drawText(currentEquip.left(), currentEquip.top() + 26, currentEquip.width(), currentEquip.height() - 26, textFlags, "1811085");
			painter.drawText(currentEquip.left(), currentEquip.top() + 34, currentEquip.width(), currentEquip.height() - 34, textFlags, "1811086");
		}
		else if (equipTypes.at(i).compare("VRPTP350") == 0)
		{
			painter.setFont(calibriExtraSmallBold);
			painter.setBrush(QColor(220, 220, 220));
			textFlags = Qt::AlignHCenter | Qt::AlignTop | Qt::TextWrapAnywhere;

			//split in lines
			int lineSplitIndex = equipNames.at(i).indexOf("\.", 7);
			QString firstLine = equipNames.at(i).left(lineSplitIndex + 1);
			QString secondLine = equipNames.at(i).right(equipNames.at(i).length() - (lineSplitIndex + 1));

			painter.drawRect(currentEquip);
			painter.drawText(currentEquip, textFlags, firstLine);
			painter.drawText(currentEquip.left(), currentEquip.top() + 9, currentEquip.width(), currentEquip.height() - 9, textFlags, secondLine);

			//write VGI name
			painter.setFont(calibriExtraSmall);
			painter.drawText(currentEquip.left(), currentEquip.top() + 18, currentEquip.width(), currentEquip.height() - 18, textFlags, "TMPU (A)");
			painter.drawText(currentEquip.left(), currentEquip.top() + 26, currentEquip.width(), currentEquip.height() - 26, textFlags, "NF12 1811489");
			painter.drawText(currentEquip.left(), currentEquip.top() + 34, currentEquip.width(), currentEquip.height() - 34, textFlags, "NG18 1811490");
		}
		else if (equipNames.at(i).left(5).compare("VRVCL") == 0 || equipNames.at(i).left(5).compare("VRVCX") == 0)
		{
			painter.setFont(calibriSmallBold);
			painter.setBrush(QColor(220, 220, 220));
			textFlags = Qt::AlignHCenter | Qt::AlignTop | Qt::TextWrapAnywhere;

			painter.drawRect(*(equipment.at(i)));
			painter.drawText(*(equipment.at(i)), textFlags, equipNames.at(i));

			
			painter.setFont(calibriSmall);
			painter.drawText(currentEquip.left(), currentEquip.top() + 13, currentEquip.width(), currentEquip.height() - 13, textFlags, "1811038-1811039-1811040-1811041-");
			painter.drawText(currentEquip.left(), currentEquip.top() + 24, currentEquip.width(), currentEquip.height() - 24, textFlags, "1811042-1811043-1811044- Cont");

		}
		else if (equipNames.at(i).left(5).compare("VRGCT") != 0 && equipNames.at(i).left(5).compare("VRPCT") != 0)
		{
			painter.setFont(calibri);
			painter.setBrush(Qt::white);
			textFlags = Qt::AlignCenter | Qt::TextWrapAnywhere;
			painter.drawRect(*(equipment.at(i)));
			painter.drawText(*(equipment.at(i)), textFlags, equipNames.at(i));

		}
		
		//TEMP until subclasses are made
		
			
	}

}

EwoInterface::EwoInterface(QWidget *parent) : BaseExternWidget(parent)
{

	baseWidget = new VacRackEwo(parent);

}

QWidget *EwoInterface::widget() const
{
	return baseWidget;
}

QStringList EwoInterface::methodList() const
{
	QStringList list;

	list.append("void changeColor(string color)");
	list.append("void addRectangle(string name, int x, int y, float w, float h, string type");
	return list;
}


QVariant EwoInterface::invokeMethod(const QString &name, QList<QVariant> &values, QString &error)
{
	if (name == "changeColor")
	{
		if (!hasNumArgs(name, values, 1, error)) return QVariant();

		// do what is supposed to happen ...
		QPalette palette;
		QColor color(0, 0, 255);
		palette.setColor(baseWidget->backgroundRole(),color);
		baseWidget->setPalette(palette);

		// this method returns void
		return QVariant();
	}
	if (name == "addRectangle")
	{
		if (!hasNumArgs(name, values, 6, error)) return QVariant();

		int width = 100 / (int)round((RACK_WIDTH / values[3].toFloat()));

		//replace this with fitting function!
		if (width >= 100)
			width = 99;

		int height = round(values[4].toFloat() / RACK_HEIGHT_UNIT);

		QRect * newEquip = new QRect(values[1].toInt(), values[2].toInt(), width*RACK_PIXEL_WIDTH/100, height*RACK_PIXEL_HEIGHT/45);
		baseWidget->addEquip(newEquip, values[0].toString(),values[5].toString());
		

		return QVariant();
	}

	return BaseExternWidget::invokeMethod(name, values, error);
}