//	Implementation of VacIconVIES class
/////////////////////////////////////////////////////////////////////////////////

#include "VacIconVIES.h"

#include "EqpVIES.h"

#include <qpainter.h>

// Color definitions. Every definition will be used as arguments for QColor.setRgb() method
#define COLOR_UNDEFINED			63,127,255
#define COLOR_ON				0,255,0
#define COLOR_OFF				255,255,255
#define COLOR_ERROR				255,0,0
#define COLOR_WARNING			255,255,0

//	Main state bits to be analyzed
#define	RR1_VALID		(0x40000000)
#define RR1_ERROR		(0x00800000)
#define	RR1_WARNING		(0x00400000)
#define	RR1_INTERLOCK	(0x00200000)
#define	RR1_ON			(0x02000000)
#define RR1_ERROR_CODE_MASK	(0x000000FF)

/////////////////////////////////////////////////////////////////////////////////
/////////////////// Construction/destruction ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

VacIconVIES::VacIconVIES(QWidget *parent, Qt::WindowFlags f) :
	VacIcon(parent, f)
{
	pEqp = NULL;
	setFixedSize(17, 7);
}

VacIconVIES::~VacIconVIES()
{
	disconnect();
}

/*
**	FUNCTION
**		Return geometry for this icon
**
**	ARGUMENTS
**		mask	- Vacuum equipment mask, not used here
**
**	RETURNS
**		This icon's geometry
**
**	CAUTIONS
**		None
*/
VacIconGeometry VacIconVIES::getGeometry(const VacEqpTypeMask & /* mask */)
{
	return VacIconGeometry(17, 7, 9, 10, -1);
}

/*
**	FUNCTION
**		Set reference to equipment for this icon
**		TODO: shall icon ALWAYS disconnect from previously connected device,
**		even if new device is not correct????
**
**	ARGUMENTS
**		pEqp	- Pointer to device to be used by this icon
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVIES::setEqp(Eqp *pEqp)
{
	disconnect();
	if(pEqp)
	{
		Q_ASSERT_X(pEqp->inherits("EqpVIES"), "VacIconVIES::setEqp", pEqp->getDpName());
		this->pEqp = (EqpVIES *)pEqp;
	}
	forceRedraw();
}

/*
**	FUNCTION
**		Set new data acquisition mode for this icon
**
**	ARGUMENTS
**		newMode	- New data acquisition mode
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacIconVIES::setMode(DataEnum::DataMode newMode)
{
	mode = newMode;
	forceRedraw();
}

/*
**	FUNCTION
**		Connect to DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if connect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVIES::connect(void)
{
	if(connected || (!pEqp))
	{
		return false;
	}
	QStringList dpes;
	dpes.append("EqpStatus");
	dpes.append("RR1");
	dpes.append("RR2");
	dpes.append("RmA");
	connection.connect(pEqp->getMaster(), dpes, mode);
	connected = true;
	return true;
}

/*
**	FUNCTION
**		Disconnect from DPEs of device
**
**	ARGUMENTS
**		None
**
**	RETURNS
**		true	- if disconnect was done;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVIES::disconnect(void)
{
	bool result = false;
	if(connected && pEqp)
	{
		connection.disconnect(pEqp);
		result = true;
	}
	connected = false;
	return result;
}

/*
**	FUNCTION
**		Calculate text and rectangle for tooltip of this icon
**
**	ARGUMENTS
**		pos		- Position of mouse pointer WITHIN this widget
**		text	- Variable where this method shall write tooltip text to be shown
**		rect	- Variable where this method shall write rectangle for visibility
**					of returned tooltip: tolltip will be hidden when mouse pointer
**					will leave this rectangle
**
**	RETURNS
**		true	- if tooltip shall be shown;
**		false	- otherwise
**
**	CAUTIONS
**		None
*/
bool VacIconVIES::getToolTip(const QPoint & /* pos */, QString &text, QRect &rect)
{
	rect = this->rect();
	if(pEqp && connected)
	{
		pEqp->getToolTipString(text, mode);
	}
	else
	{
		text = "Not connected";
	}
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
////////////// And here icon drawing is implemented /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void VacIconVIES::paintEvent(QPaintEvent * /* pEvent */)
{
	QPainter painter(this);
	painter.eraseRect(0, 0, width(), height());

	// Coordinate system is ready - draw
	drawImage(painter);

	// Draw selection ON TOP of image
	if(pEqp)
	{
		if(pEqp->isSelected())
		{
			drawSelection(painter);
		}
		else
		{
			painter.setPen(Qt::black);
			painter.drawRect(0, 0, width() - 1, height() - 1);
		}
	}
}

void VacIconVIES::drawImage(QPainter &painter)
{
	// Draw image
	setPainterForImage(painter);

	// 1) Filled rectangle
	painter.fillRect(rect(), painter.brush());

}


void VacIconVIES::setPainterForImage(QPainter &painter)
{
	QColor	color(COLOR_UNDEFINED);
	if(pEqp)
	{
		pEqp->getMainColor(color, mode);
	}
	/*
	if(!plcAlarm)
	{
		if(rr1 & RR1_VALID)
		{
			if(rr1 & RR1_ERROR)
			{
				color.setRgb(COLOR_ERROR);
			}
			else if(rr1 & RR1_WARNING)
			{
				color.setRgb(COLOR_WARNING);
			}
			else if(rr1 & RR1_ON)
			{
				color.setRgb(COLOR_ON);
			}
			else
			{
				color.setRgb(COLOR_OFF);
			}
		}
	}
	*/
	painter.setBrush(color);
}
