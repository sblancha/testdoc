//	Implementation of VacLinePart class
/////////////////////////////////////////////////////////////////////////////////

#include "VacLinePart.h"

#include "VacType.h"
#include "Eqp.h"

#include "DataPool.h"

#include <stdlib.h>

/////////////////////////////////////////////////////////////////////////////////
////////////////////	Construction/destruction

VacLinePart::VacLinePart(int vacType, float start, float end, bool afterIp1)
{
	this->vacType = vacType;
	this->realStart = start;
	this->realEnd = end;
	this->afterIP1 = afterIp1;

	startPos = endPos = 0;
	pSector = NULL;
	startX = endX = 0;

	startOrder = endOrder = corrDeltaStart = corrDeltaEnd = 0;

	started = finished = startDone = drawStart = drawEnd = false;
}

VacLinePart::~VacLinePart()
{
}

/*
**	FUNCTION
**		Write all parameters to output file for debugging
**
**	PARAMETERS
**		pFile	- Pointer to file opened for writing
**
**	RETURNS
**		None
**
**	CAUTIONS
**		None
*/
void VacLinePart::dump(FILE *pFile, int idx)
{
	if(!pFile)
	{
		return;
	}
	fprintf(pFile, "  %02d: [%d..%d] start %f end %f (real %f %f) type %X %s %s\n", idx,
		startOrder, endOrder, startPos, endPos, realStart, realEnd,
		vacType, (pSector ? pSector->getName() : ""), (afterIP1 ? "(AFTER IP1)" : "()"));
	fprintf(pFile, "   startX %d endX %d isStarted %d isFinished %d\n",
		startX, endX, started, finished);
	fprintf(pFile, "   drawStart %d drawEnd %d\n", drawStart, drawEnd);
	fprintf(pFile, "   correct: start %d end %d\n", corrDeltaStart, corrDeltaEnd);
}

