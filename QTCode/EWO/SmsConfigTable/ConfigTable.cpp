//	Implementation of ConfigTable class
/////////////////////////////////////////////////////////////////////////////////

#include "ConfigTable.h"
#include "PlatformDef.h"

#include <QHeaderView>

#include <math.h>

//////////////////////////////////////////////////////////////////////////////////
/////////////////////////	Construction/Destruction
//////////////////////////////////////////////////////////////////////////////////

ConfigTable::ConfigTable(QWidget *parent)
	: QTableView(parent)
{
	setModel(&model);
	setSelectionMode(QAbstractItemView::SingleSelection);
	setSelectionBehavior(QAbstractItemView::SelectRows);
	setSortingEnabled(true);
	horizontalHeader()->show();
	verticalHeader()->hide();
}

ConfigTable::~ConfigTable()
{
}

void ConfigTable::resizeEvent(QResizeEvent *pEvent)
{
	QTableView::resizeEvent(pEvent);
	int totalWidth = width();
	int wd = (int)rint(totalWidth * 0.06);
	setColumnWidth(SmsConfig::Id, wd);
	wd = (int)rint(totalWidth * 0.10);
	setColumnWidth(SmsConfig::Owner, wd);
	wd = (int)rint(totalWidth * 0.080);
	setColumnWidth(SmsConfig::Group, wd);
	wd = (int)rint(totalWidth * 0.35);
	setColumnWidth(SmsConfig::Config, wd);
	wd = (int)rint(totalWidth * 0.12);
	setColumnWidth(SmsConfig::Error, wd);
	wd = (int)rint(totalWidth * 0.11);
	setColumnWidth(SmsConfig::State, wd);
	wd = (int)rint(totalWidth * 0.08);
	setColumnWidth(SmsConfig::Events, wd);
	wd = (int)rint(totalWidth * 0.09);
	setColumnWidth(SmsConfig::Msg, wd);
}

const QString ConfigTable::getSelectedConfig(void)
{
	QModelIndexList selections = selectedIndexes();
	if(selections.isEmpty())
	{
		return QString();
	}
	return model.getConfigAtRow(selections.at(0).row());
}

