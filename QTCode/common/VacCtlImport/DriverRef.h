#ifndef DRIVERREF_H
#define DRIVERREF_H

#include "VacCtlImportExport.h"

//	Class holding references to driver with given number

#include <qlist.h>

#include <stdio.h>


class VACCTLIMPORT_EXPORT DriverRef
{
public:
	~DriverRef();

	static void addRef(int drvNum, const char *type);
	static void clear();

	static void dump(FILE *pFile);

	// Access
	inline int getDrvNum(void) { return drvNum; }
	inline const char *getType(void) { return type; }
	inline int getRefCount(void) { return refCount; }
	static QList<DriverRef *> &getInstances(void);

protected:
	DriverRef(int drvNum, const char *type);

	// Driver number
	int			drvNum;

	// Driver type
	char		*type;

	// Number of references
	unsigned long	refCount;

	// List of all instances
	static QList<DriverRef *>	*pInstances;
};

#endif	// DRIVERREF_H
