//	Implementation of MobileEqpTree class
/////////////////////////////////////////////////////////////////////////////////

#include "MobileEqpTree.h"

#include <QMouseEvent>
#include <QHeaderView>

// http://developer.qt.nokia.com/faq/answer/how_can_i_detect_whether_a_checkbox_was_clicked_in_my_qtreeview


MobileEqpTree::MobileEqpTree(QWidget *parent) : QTreeWidget(parent)
{
	QRect rect = fontMetrics().boundingRect("Mobile eqp VABEKOUT.ARC4-5.B.1");
	setMinimumSize(QSize(rect.width(), 200));
}

MobileEqpTree::~MobileEqpTree()
{
}

void MobileEqpTree::mouseReleaseEvent(QMouseEvent *pEvent)
{
	QTreeWidgetItem *pItem = NULL;
	QModelIndex indexClicked = indexAt(pEvent->pos());
	if(indexClicked.isValid())
	{
		QRect vrect = visualRect(indexClicked);
		int itemIndent = vrect.x() - visualRect(rootIndex()).x();
		QRect rect = QRect(header()->sectionViewportPosition(0) + itemIndent,
			vrect.y(), style()->pixelMetric(QStyle::PM_IndicatorWidth), vrect.height());
		if(rect.contains(pEvent->pos()))
		{
			pItem = itemFromIndex(indexClicked);
		}
	}
	QTreeWidget::mouseReleaseEvent(pEvent);

	if(pItem)
	{
		emit checkBoxPressed(pItem);
	}
}

