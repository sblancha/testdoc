#ifndef	MOBILEPOOL_H
#define	MOBILEPOOL_H

#include "VacCtlEqpDataExport.h"

// Pool of active mobile equipment: both online and historical state

#include "BakeoutWorkDp.h"

#include "DataEnum.h"

#include <qobject.h>
#include <qlist.h>
#include <qhash.h>
#include <qdatetime.h>
#include <qmutex.h>

class InterfaceEqp;

class VACCTLEQPDATA_EXPORT MobilePool : public QObject
{
	Q_OBJECT

public:
	static MobilePool &getInstance(void);

	static const QString &findBakeoutDpeName(Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType valueType);
	static const QString &findBakeoutChannelDpeName(int channel, enum BakeoutWorkDp::ChannelValueType valueType);
	bool connectValue(InterfaceEqp *pReceiver, Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType valueType);
	void disconnectValue(InterfaceEqp *pReceiver, Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType valueType);

	bool connectChannel(InterfaceEqp *pReceiver, Eqp *pEqp, int channel);
	void disconnectChannel(InterfaceEqp *pReceiver, Eqp *pEqp, int channel);

	bool connectCtlChannel(InterfaceEqp *pReceiver, Eqp *pEqp, int channel);
	void disconnectCtlChannel(InterfaceEqp *pReceiver, Eqp *pEqp, int channel);

	bool connectRr1(InterfaceEqp *pReceiver, Eqp *pEqp);
	void disconnectRr1(InterfaceEqp *pReceiver, Eqp *pEqp);
	const QVariant &getDpeValue(Eqp *pEqp, int channel, enum BakeoutWorkDp::ChannelValueType valueType);
	const QVariant &getDpeValue(Eqp *pEqp, const char *dpeName, QDateTime &timeStamp);
	bool isPlcAlarm(Eqp *pEqp);

	void addBakeoutWorkDp(const char *dpName);

	void startOnlineEqp(void);
	void addOnlineEqp(const char *dpName, const char *workDpName, const char *sourceDpName, bool active);
	void finishOnlineEqp(void);

	void startHistoryEqp(void);
	void addHistoryEqp(const char *dpName);
	void finishHistoryEqp(void);

	QList<Eqp *> &getActive(DataEnum::DataMode mode) { return mode == DataEnum::Replay ? *pActiveListHistory : *pActiveListOnline; }

	void newOnlineValue(const char *dpName, const char *dpeName, const QVariant &value, const QDateTime &timeStamp);

	MobilePool();
	~MobilePool();

signals:
	void activeListChanged(DataEnum::DataMode mode);

protected:
	// The only instance
	static MobilePool	instance;

	// Mutex to control parallel access to data
	QMutex				mutex;

	// List of all bakeout containers available in system
	QList<BakeoutWorkDp *>	*pBakeoutList;

	// Dictionary for fast search of bakeout work DP
	QHash<QByteArray, BakeoutWorkDp *>	*pBakeoutDict;

	// List of all mobile equipment active in online mode
	QList<Eqp *>		*pActiveListOnline;

	// List of all mobile equipment active in history mode
	QList<Eqp *>		*pActiveListHistory;

	// Temporary storage for new list of online active equipment
	QList<Eqp *>		*pTmpListOnline;

	// Temporary storage for new list of historical active equipment
	QList<Eqp *>		*pTmpListHistory;

	BakeoutWorkDp *findWorkDpForEqp(Eqp *pEqp);
};

#endif	// MOBILEPOOL_H
