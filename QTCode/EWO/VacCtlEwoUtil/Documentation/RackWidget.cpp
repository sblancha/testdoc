#include "RackWidget.h"
#include "qmath.h"
#include "Rack.h"
#include "RackEqp.h"
#include "RackIcon.h"


#define RACK_PIXEL_WIDTH 200
#define RACK_PIXEL_HEIGHT 675
#define RACK_FRAME_WIDTH 10
#define RACK_FRAME_HEIGHT 35

RackWidget::RackWidget(QWidget * parent) :
QWidget(parent)
{
	this->parent = parent;
	this->move(100, 100);
	QSize mySize(1220, 1720);
	setFixedSize(mySize);
	scale = 1;
	rackIndex = 1;
	QObject::connect(this, SIGNAL(selectedEqp(RackIcon *)), this->parent, SLOT(selectionChanged(RackIcon *)));
}

RackWidget::~RackWidget()
{
	qDeleteAll(equipment);
	equipment.clear();

	qDeleteAll(equipIcons);
	equipIcons.clear();

	QObject::disconnect(this, SIGNAL(selectedEqp(RackIcon *)), this->parent, SLOT(selectionChanged(RackIcon *)));
}

void RackWidget::setRackIndex(int rackIndex)
{
	//this->move(100 + rackIndex*(RACK_PIXEL_WIDTH + RACK_FRAME_WIDTH * 2), 100);

	this->rackIndex = rackIndex;
	this->recalculatePosition();


}

void RackWidget::recalculatePosition(void)
{
	this->move(100 + rackIndex*(RACK_PIXEL_WIDTH + RACK_FRAME_WIDTH * 2)*scale, 100);
	for (int i = 0; i < equipIcons.size(); i++)
	{
		equipIcons.at(i)->recalculatePosition(RACK_PIXEL_WIDTH, RACK_PIXEL_HEIGHT, RACK_FRAME_WIDTH, RACK_FRAME_HEIGHT);
	}
}

void RackWidget::setScale(qreal newScale)
{
	this->scale = newScale;
	for (int i = 0; i < equipIcons.size(); i++)
	{
		equipIcons.at(i)->setScale(newScale);
	}
}

void RackWidget::draw(QPainter &painter2, int startX, int startY, int rackIndex)
{

}

void RackWidget::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);
	painter.scale(scale, scale);
	painter.setRenderHint(QPainter::Antialiasing);

	QFont calibri("Calibri", 12, QFont::Bold);
	//draw outside rectangle
	painter.setBrush(Qt::gray);
	painter.drawRect(0, 0, 10, 10);
	painter.drawRect(0, 0, 10, 10);
	painter.drawRect(0, 0, RACK_PIXEL_WIDTH + RACK_FRAME_WIDTH * 2, RACK_PIXEL_HEIGHT + RACK_FRAME_HEIGHT + RACK_FRAME_WIDTH);

	//draw inside rectangle
	painter.setBrush(Qt::white);
	painter.drawRect(RACK_FRAME_WIDTH, RACK_FRAME_HEIGHT, RACK_PIXEL_WIDTH, RACK_PIXEL_HEIGHT);

	//draw rack name
	painter.setFont(calibri);
	painter.setBrush(Qt::yellow);
	painter.drawRect(RACK_PIXEL_WIDTH / 4 + RACK_FRAME_WIDTH, 4, RACK_PIXEL_WIDTH / 2, 17);
	painter.drawText(QRect(0, 0, RACK_PIXEL_WIDTH + RACK_FRAME_WIDTH * 2, 25-1), Qt::AlignCenter, this->rackName);

	//draw lines inside rack
	painter.setBrush(Qt::white);
	painter.setPen(Qt::lightGray);
	for (int i = 1; i <= 44; i++)
	{
		painter.drawLine(RACK_FRAME_WIDTH, RACK_FRAME_HEIGHT + i * RACK_PIXEL_HEIGHT / 45, RACK_FRAME_WIDTH + RACK_PIXEL_WIDTH, RACK_FRAME_HEIGHT + i * RACK_PIXEL_HEIGHT / 45);
	}
	int textFlags;

	//draw equipment
	

}

void RackWidget::makeEquipmentList(Rack &rack)
{
	int numberOfEquip = rack.getNumberOfEquipment();
	QRect * pRect;
	int equipmentPosY, equipmentPosX, equipmentWidth, equipmentHeight;
	for (int i = 0; i < numberOfEquip; i++)
	{
		RackEqp * pRackEqp = rack.getRackEqp(i);
		equipNames.append(pRackEqp->getName());//rack.getEqpNameAt(i));
		equipTypes.append(pRackEqp->getType());
		equipmentPosX = RACK_FRAME_WIDTH + pRackEqp->getPositionX()*RACK_PIXEL_WIDTH/100 - 1;//1-100
		equipmentPosY = RACK_PIXEL_HEIGHT + RACK_FRAME_HEIGHT - (pRackEqp->getPositionY() - 1)*RACK_PIXEL_HEIGHT / 45;
		equipmentWidth = qFloor(pRackEqp->getSizeX() / 0.48 * RACK_PIXEL_WIDTH + 0.5);
		equipmentHeight = qFloor(pRackEqp->getSizeY() / 0.0445 + 0.5) *RACK_PIXEL_HEIGHT / 45;
		pRect = new QRect(equipmentPosX, equipmentPosY - equipmentHeight, equipmentWidth, equipmentHeight);
		this->equipment.append(pRect);
	
		RackIcon * pIcon = RackIcon::getIcon(pRackEqp->getType(), this);
		if (pIcon)
		{
			pIcon->setEqp(pRackEqp->getEqpPointer());
			pIcon->connect();
			pIcon->setSize(equipmentWidth, equipmentHeight);
			pIcon->setPosition(pRackEqp->getPositionX(), pRackEqp->getPositionY());
			pIcon->recalculatePosition(RACK_PIXEL_WIDTH, RACK_PIXEL_HEIGHT, RACK_FRAME_WIDTH, RACK_FRAME_HEIGHT);
			pIcon->setName(pRackEqp->getName());
			pIcon->setAttributeList(pRackEqp->getAttributeList());
			equipIcons.append(pIcon);
		}
		
	}
}

void RackWidget::clearSelection()
{
	for (int i = 0; i < equipIcons.length(); i++)
	{
		equipIcons.at(i)->deselectEqp();
	}
}

RackIcon * RackWidget::getRackIcon(QString equipmentName)
{
	for (int i = 0; i < equipIcons.length();i++)
	{
		if (equipmentName.compare(equipIcons.at(i)->getName()) == 0)
		{
			return equipIcons.at(i);
		}
	}

	return NULL;
}

int RackWidget::getRackPixelHeight()
{
	return (RACK_FRAME_HEIGHT + RACK_PIXEL_HEIGHT + RACK_FRAME_WIDTH);
}