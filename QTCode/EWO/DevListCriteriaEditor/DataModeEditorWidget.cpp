//	Implementation of DataModeEditorWidget class
////////////////////////////////////////////////////////////////////////////////

#include "DataModeEditorWidget.h"

#include "DevListCriteria.h"

#include <qlayout.h>
#include <qradiobutton.h>
#include <qdatetimeedit.h>

////////////////////////////////////////////////////////////////////////////////
//////////////////// Construction/destruction //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

DataModeEditorWidget::DataModeEditorWidget(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags)
{
	setting = false;

	// Build layout
	QGridLayout *pLayout = new QGridLayout(this);

	pOnlineRb = new QRadioButton("Online", this);
	pLayout->addWidget(pOnlineRb, 0, 1);

	pReplayRb = new QRadioButton("Archive", this);
	pLayout->addWidget(pReplayRb, 2, 1);

	DevListCriteria &criteria = DevListCriteria::getInstance();
	pTimeEditor = new QDateTimeEdit(criteria.getTime(), this);
	pTimeEditor->setDisplayFormat("dd-MM-yyyy hh:mm:ss");
	pTimeEditor->setMaximumDate(QDate::currentDate());
	pTimeEditor->setCalendarPopup(true);
	pLayout->addWidget(pTimeEditor, 2, 2);

	pLayout->setRowStretch(0, 1);
	pLayout->setRowStretch(3, 10);
	pLayout->setColumnStretch(0, 1);
	pLayout->setColumnStretch(3, 10);

	// Set initial state of radio buttons
	if(criteria.getMode() == DataEnum::Replay)
	{
		pReplayRb->setChecked(true);
	}
	else
	{
		pOnlineRb->setChecked(true);
	}
	pTimeEditor->setEnabled(pReplayRb->isChecked());

	// Connect to signals
	connect(pOnlineRb, SIGNAL(toggled(bool)), this, SLOT(onlineToggled(bool)));
	connect(pReplayRb, SIGNAL(toggled(bool)), this, SLOT(replayToggled(bool)));
	connect(pTimeEditor, SIGNAL(dateTimeChanged(const QDateTime &)),
		this, SLOT(timeChanged(const QDateTime &)));
}

DataModeEditorWidget::~DataModeEditorWidget()
{
}

void DataModeEditorWidget::onlineToggled(bool on)
{
	if(setting)
	{
		return;
	}
	setting = true;
	pReplayRb->setChecked(!on);
	pTimeEditor->setEnabled(!on);
	setting = false;

	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setMode(DataEnum::Online);
	emit modeChanged();
}

void DataModeEditorWidget::replayToggled(bool on)
{
	if(setting)
	{
		return;
	}
	setting = true;
	pOnlineRb->setChecked(!on);
	pTimeEditor->setEnabled(on);
	setting = false;

	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setMode(DataEnum::Replay);
	emit modeChanged();
}

void DataModeEditorWidget::timeChanged(const QDateTime &time)
{
	DevListCriteria &criteria = DevListCriteria::getInstance();
	criteria.setTime(time);
	emit timeChanged();
}


