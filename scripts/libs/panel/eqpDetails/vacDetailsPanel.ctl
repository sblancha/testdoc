/** 
@defgroup gr_vacDetailsPanel eqpDetails/vacPanel<dpType>.pnl
vacDetailsPanel.ctl is the scope library of the equipment details panels. \n
\n
The configuration parameters to be initialized are specified in the Vacuum Fixed Device Communication Register document: \n
https://svnvacplc.web.cern.ch/repo/Specifications/VacFixDevCommRegisters.xlsx\n
\n
Any details panel that use this library may have the below \b global variables: \n
    Global Names    | Type        | Description
    ----------------|-------------|-----------
    g_dpName        | string      | DP name of the device DPSUB_DP context
    g_dpType        | string      | DP type of the device
    g_mode          ! string      | Panel online or replay mode
    g_sCtrlType     | string      | Control type name or top parent control type name of the device
    g_expertDetails | bool        | Panel is in expert configuration 
\n
This librairy is used in following Equipment details panel:
-----------------------------------------------------------
\n
   -@ref vgu_doc
   -@ref vpgu_doc

@defgroup vgu_doc Vacuum Gauge Unified (VG_U)
@{
Vacuum Gauge Unified is the control type for both: \n
  - I/O gauges
  - TPG300 Gauges \n
More documentation available: https://edms.cern.ch/ui/file/2133773//LAST_RELEASED/doc.pdf
@section ioGauges I/O Gauges
@subsection ioVgp Penning gauge protection mechanism
The Penning gauge has the following behavior: 
  - Gauge can be turned On, Off, or Forced On (which ignores the Pirani Gauge interlock and the self protections)
  - The Off and Protect Thresholds can be modified from the panel
  - The Interlock Threshold is fixed at 1e-2 mbar. This interlock can, however, be completely disabled from within the panel.
 
This might be useful in a situation where the Pirani gauge is malfunctioning, cannot be replaced on the short term, and 
the operator doesn?t wish to leave the gauge permanently forced on (which also bypasses the self protections)
  - Forced Status bypasses all gauge protections. While in Forced Status the gauge will be permanently on and both the VGR Interlock and the self protections thresholds will be ignored. \n
@image html dox_ioVgp_fig1.png

@section tpgGauges TPG300 Gauges
@subsection tpgVgp Penning gauge protection mechanism
The Penning gauge of TPG300 has the following protection behavior: \n
@image html dox_tpgVgp_fig1.png

@section panel eqpDetails/vacPanel_VG_U.pnl
vacDetailsPanel.ctl is the scope library of VG_U details panels. \n
This is the details panel for eqp dp type VG_UC (Unified Vacuum Gauge Compact register) and VG_UF (Unified Vacuum Gauge Full register).\n
Unified Vacuum Gauge(VG_U) is the control device for new controlled gauges (I/O, TPG300) in addition to the monitoring of pressure value (PR), 
status (objectSt), error and warning; it manages main state, orders and the protection of the gauge.\n 
The panel has 3 different configurations, size of the panel and field visibilite change from a configuration to another. \n
   - Configuration Basic: the initial configuration for both compact (VG_UC) and full (VG_UF) registers
      - Size: x=386; y=306
      - Fields: Alarm, protection and filter fields hiden
      - Buttons: help, attributes, expert and close buttons original position 
					- help: x=9; y=281    
					- attributes: x=101; y=281
					- expert: x=214; y=281 
					- close: x=310; y=281  
   - Configuration Expert Compact register: when pushing expert button for VG_UC dp type
      - Size: x=386; y=404
      - Fields: Alarm, protection and filter fields hiden
      - Buttons: help, attributes, expert and close buttons original position
					- help: x=9; y=370    
					- attributes: x=101; y=370
					- expert(basic): x=214; y=370 
					- close: x=310; y=370            
   - Configuration Expert Full register: when pushing expert button for VG_UF dp type 
      - Size: x=386; y=614
      - Fields: all fields visible
      - Buttons: help, attributes, expert and close buttons move to bottom
					- help: x=9; y=590    
					- attributes: x=101; y=590
					- expert(basic): x=214; y=590 
					- close: x=310; y=590     
@}
@defgroup vpgu_doc Vacuum Pumping Group Unified (VP_GU)
@{

Vacuum  Pumping Group Unified is the control type for both: \n
  - Fixed Pumping Group
  - Mobile Pumping Group 

More documentation available: https://edms.cern.ch/ui/file/2133773//LAST_RELEASED/doc.pdf \n
The pumping group is composed by:
	- primary pump
	- turbo-molecular pump
	- valves
	- gauges
	- process and interlocks virtual devices

At CERN accelerators, the pumping groups may be found in several hardware configurations, depending on the environment and on the vacuum system used; the control is always based on Programmable Logical Controllers (PLC) communicating with the field equipment over a field bus; pumping groups are controlled by the same flexible and portable software. \n
They are remotely accessed through a Supervisory Control and Data Acquisition (SCADA) application and can be locally controlled by a portable touch panel. More than 250 pumping groups are permanently installed in the Large Hadron Collider, Linacs and North Area Experiments.
@section vpguNutshell Pumping Group in a Nutshell
The remote control of the pumping group is managed at process level, remote orders are global
(Start, Stop, Vent...). \n 
The vessel valve (VVR) is the only instrument that can be operated individually. \n
@image html dox_vpguNutshell.png
@section vpguHw Hardware compatibility
@subsection vpguHwFixed Fixed Pumping Group
This device type is dedicated to any pumping group control with S7-1200 PLC based control crate
type VRPG .The below figure show the compatible fixed pumping group. \n
@image html dox_vpguHwFixed.png
@subsection vpguHwMobile Mobile Pumping Group
This device type is dedicated to any mobile pumping group control. 
The below figure show the compatible fixed pumping group. \n
@image html dox_vpguHwMobile.png
@section vpguInterfaces Interfaces
As fixed pumping group the S7-1200 PLC based control crate is permanently connected to the
supervisory application with the Ethernet interface (Technical Network). \n
As mobile pumping group the S7-1200 PLC based control crate is connected to the supervisory
application with the Wireless/Sim Card router (3G Network). \n
@section vpguApplication Application
@subsection vpguOrders Orders
Orders are relative to the process of the pumping group and to the actuation of the VVR valves. \n
Order depend on the pumping group equipment type. \n
Process Orders for Insulation primary pumping group:
	- Start Pump
	- Stop and Vent
	- Stop no Vent
	- Purge mode

Process Orders for turbomolecular pumping group:
	- Start Pump
	- Stop and Vent
	- Stop no Vent \n

Leak detection modes are only available with the local touch panel.\n
Orders for the VVR valves:
	- Open
	- Close
	- Unforce \n

Force order is only available with the local touch panel.
@subsection vpguFeedbacks Feedbacks
State string is reported in the state field of the details panel:	
	- Not Connected - Pumping group not connected to SCADA, only state defined by SCADA script
	- Initial - Initial state after for example control unit start
	- Stopped (vent) - Pumping group is in "Stopped" mode after a normal venting
	- Starting (VVR cl) - Pumping group starting and not ready for safe pumping (Primary pump is starting, Turbo moleculare pump is accelerating...)
	- Nominal (VVR cl) - Pumping group ready for safety pumping (e.g.: Turbo moleculare pump nominal and safe pumping delay is reached)
	- Pumping (VVR op) - Pumping group is pumping the vessel it is connected to
	- Recovering (VVR op) - Turbo moleculare pump is recovering (lost is nominal speed) and valve connected to vessel (VVR) still opened
	- Starting (VVR op) - Pumping group starting and valve connected to vessel (VVR) has been forced open
	- Stopped (no vent) - Pumping group has been stopped without venting
	- Services - Pumping group in service mode ongoing (Leak detection in progress, bakeout, primary pump test mode in progress...)
	- Error Stopped (vent) - Pumping group stopped and vented after an error occured
 	- Error Stopped (no vent) - Pumping group stopped but not vented after an error occured
 	- Error unknown state - Pumping group in unknown state after an error occurred
 	- Error Communication - Pumping group Communication error
 	- Pumping(VVR1opVVR2cl) - Pumping group is pumping the vessel through VVR1 only 
 	- Pumping(VVR1clVVR2op) - Pumping group is pumping the vessel through VVR2 only 
@subsection vpguErrors Errors
The error codes are calculated by the PLC and displayed in the details panel. The diagnostic
descriptions of error are listed below:
	- 109 = Accelerating TimeOut(AutoRestart Rst)
	- 108 = VPT not On in Accel.(AutoRestart Rst)
	- 103 = Recover TimeOut(AutoRestart Rst)
	- 110 = VPT Error Status(Non Remanent)
	- 114 = VPP Error Status(Non Remanent)
	- 112 = VVI Error Status(Non Remanent)
	- 115 = VVP Error Status(Non Remanent)
	- 116 = VVT Error Status(Non Remanent)
	- 117 = VVD Error Status(Non Remanent)
	- 113 = Power Cut (running on UPS)
	- 107 = VPT not On after Nominal(AutoRestart Rst)
	- 106 = VVT or VVP or VVD not closed
	- 104 = VVI not opened
	- 105 = VPP NOT pumping(AutoRestart Rst)
	- 118 = VVR Error status

VPGMA Error code:
	- 119 = VPP Error
	- 120 = VPP and VVR Error
	- 121 = VVI Error
	- 122 = VVP Error
	- 123 = Water Vaporization Timeout(24hrs)
	- 124 = Gauge Error in Purge mode(24hrs)i

VPGF Error code:
	- 125 = Hardware Type Mismatch

The warning codes are calculated by the PLC and displayed in the details panel. The diagnostic
descriptions of error are listed below:
	- 101 = Auto-Restart has occured
	- 226 = Auto-Venting or Stopping after HW Interlock
	- 209 = Auto-Venting or Stopping after Accelerating (VPT) TimeOut [ErrAccelTO] (Reset by AutoRestart)
	- 208 = Auto-Venting or Stopping after VPT not On Status after [ErrVPTNOnTO] in Accelerating Step (Reset by AutoRestart)
	- 207 = Auto-Venting or Stopping after VPT not On Status in Nominal or Recovering and Leak Detection Step (Reset by AutoRestart)
	- 206 = Auto-Venting or Stopping after VVT or VVP or VVD opened in Nominal or Recovering Step
	- 205 = Auto-Venting or Stopping after VPP not pumping (Reset by AutoRestart)
	- 204 = Auto-Venting after VVI not opened in PrimaryP, Accel, Nominal or Recovering Step (Delayed)
	- 203 = Auto-Venting after Recover TimeOut [ErrRecoverTO]
	- 202 = Auto-Venting after Max Auto Restart Number Reached.
	- 210 = Auto-Venting after VPT Error Status (Reset by AutoRestart)
	- 214 = Auto-Venting after VPP Error Status (Reset by AutoRestart)
	- 212 = Auto-Venting after VVI Error Status
	- 215 = Auto-Venting after VVP Error Status
	- 216 = Auto-Venting after VVT Error Status
	- 217 = Auto-Venting after VVD Error Status
	- 213 = Auto-Venting after Power Cut (running on UPS)
	- 218 = Auto-Pumping after VVR error 

VPGMA Warning code:
	- 219 = Stopping after VPP NOT On when requested
	- 220 = Stopping after VPP Error
	- 221 = Stopping after VPP AND VVR Error
	- 222 = Pumping with VVR Error
	- 223 = Stopping after VVS Error
	- 224 = Purging after VVB Error
	- 225 = Stopping after Water Vaporization timeout(24hrs)
	- 227 = Stopping after Gauge Error in Purge mode(24hrs)   

@section panel eqpDetails/vacPanel_VP_GU.pnl
vacDetailsPanel.ctl is the scope library of VP_GU details panels. \n
This is the details panel for eqp dp type VP_GU (Unified Vacuum Pumping Group).\n
Unified Vacuum Gauge(VG_U) is the control device for new controlled pumping group (Mobiles and fixed with S7-1200 PLC).\n 
The panel has only 1 configuration. \n
   - Configuration: 
      - Size: x=386; y=339
      - Fields: may be hiden according to the subcontrol type for fixed VPG and equipment code for mobile VPG (see specs https://svnvacplc.web.cern.ch/repo/Specifications/VacFixDevCommRegisters.xlsx).\n
      - Buttons: help, attributes and close buttons

@}
*/
#uses "VacCtlEqpDataPvss"  // Load .so or DLL
#uses "vclResources.ctl"   // Load CTRL library
#uses "vclEqpConfig.ctl"   // Load CTRL library
///@brief global variables
bool g_expertDetails = false;   // Panel is in expert configuration 
///@brief CONST
const string RESOURCE_VGU = "VG_U";
// Read Register bit meaning
unsigned	
  RR1_16B_VALID     = 0x4000u,
  RR1_16B_ERROR     = 0x0080u,
  RR1_16B_WARNING   = 0x0040u,  
  RR1_16B_ON        = 0x0200u,
  RR1_16B_OFF       = 0x0100u,
  RR1_16B_PROTECTED = 0x0002u;
/// @brief Panel and buttons position weight and height
const int VGU_PANEL_W               = 386,
          VGU_PANEL_BASIC_H         = 306,
          VGU_PANEL_EXPERT_C_H      = 389,
          VGU_PANEL_EXPERT_F_H      = 614,
          VGU_BTN_HELP_W            =   9,
          VGU_BTN_ATTR_W            = 101,
          VGU_BTN_EXPERT_W          = 214,
          VGU_BTN_CLOSE_W           = 310,
          VGU_BTN_BASIC_H           = 281,
          VGU_BTN_EXPERT_C_H        = 370,
          VGU_BTN_EXPERT_F_H        = 590;
          
/**
@brief Generic eqpStatus update for 16bits RR1
@details Update panel when eqpStatus change not call back only at init
@param[in] status Equipment control status (controlled, not controlled, not connected)
@param[in] rr1 Read register #1
@param[in] pressure Pressure value
*/
bool ShowEqpStatus(string dpNameSub, int mode, shape stateText, shape errText, shape warningText) {
  unsigned status = LhcVacEqpCtlStatus(dpNameSub, mode);  
  switch(status) {
  case EQP_CTL_STATUS_NOT_CONTROL:
  case EQP_CTL_STATUS_NOT_CONNECTED:
    setValue(stateText, "text", "NOT REMOTE CONTROLLED");    
    return false;
    break;
  default:
    return true;
    break;
  }
  return true;
}

/**
@brief Generic Call Back Function update State Order field plus color pressure, error, warning field
*/
void ShowRR1_16b(string dpName, int mode, shape errText, shape warningText, unsigned rr1) {
  string        text, colorString, dpType, status;
  bool          isError, isWarning;
  dyn_string	exceptionInfo;

  // State string
  LhcVacSetMainState("stateText", dpName, mode); // State order field update with C++ libs
  
  // Animation when NOT VALID
  //pressureText.fill = ((rr1 & VG_SPS_R_VALID) != 0) ? "[solid]"  : "[outline]";
  //objectStText.fill = ((rr1 & VG_SPS_R_VALID) != 0) ? "[solid]"  : "[outline]";
  
  // Animation when ERROR BIT
  if ((rr1 & RR1_16B_ERROR) != 0) {  
    setValue(errText, "backCol", COLOR_ERROR);
    setValue(errText, "foreCol", COLOR_BAD_FOREGROUND);
  }
  else {
    setValue(errText, "backCol", COLOR_OK_BACKGROUND);
    setValue(errText, "foreCol", COLOR_OK_FOREGROUND);
  }  
  // Animation when WARNING BIT  
  if ((rr1 & RR1_16B_WARNING) != 0) {  
    setValue(warningText, "backCol", COLOR_WARNING);
  }
  else {
    setValue(warningText, "backCol", COLOR_OK_BACKGROUND);
    setValue(warningText, "foreCol", COLOR_OK_FOREGROUND);
  }
  // Animation protectedText
  if ((rr1 & RR1_16B_PROTECTED) != 0) {
    protectedText.visible(true);
  }  
  else {
    protectedText.visible(false);  
  }
}

/**
@brief Generic Call Back Function update Status field 
*/
void ShowObjectSt(shape objectStText, unsigned objectSt) {
  string sObjectSt;
  sObjectSt = VacResourcesGetValue(RESOURCE_VGU + ".objectSt_" + objectSt , VAC_RESOURCE_STRING, "");
  if(strlen(sObjectSt)== 0 ) {  
    setValue(objectStText, "text", "Unknown Code:" + objectSt);
  }  
  else {  
    setValue(objectStText, "text", sObjectSt);
  }
}

/**
@brief Generic Call Back Function update Pressure field 
*/
void ShowPressureValue(shape pressureText, float pr, string dpName, int iMode) {
  // State string
  LhcVacSetMainState("stateText", dpName, iMode); // State order field update with C++ libs
  
  string pressure;
  string	textC;
  sprintf( pressure, "%5.2E", pr);
  setValue(pressureText, "text", pressure );
}

/**
@brief Generic Call Back Function update Error field
*/
void ShowError(shape errText, unsigned errorSt) {
  string sError;
  sError = VacResourcesGetValue(RESOURCE_VGU + ".Error_" + errorSt , VAC_RESOURCE_STRING, "");
  if(strlen(sError)== 0 ) {  
    setValue(errText, "text", "Unknown Code:" + errorSt);
  }  
  else {  
    setValue(errText, "text", sError);
  }
}

/**
@brief Generic Call Back Function update Warning field
*/
void ShowWarning(shape warningText, unsigned warningSt) {
  string sWarning;
  sWarning = VacResourcesGetValue(RESOURCE_VGU + ".Warning_" + warningSt , VAC_RESOURCE_STRING, "");
  if(strlen(sWarning)== 0 ) {  
    setValue(warningText, "text", "Unknown Code:" + warningSt);
  }  
  else {  
    setValue(warningText, "text", sWarning);
  }
}

/**
@brief Generic Call Back Function to update float text value 
*/
void ShowFloatDpe(shape floatDpeText, float value, string unit) {
  string sValue;
  if ( value < 1E-14 ) {
    sValue = "Not defined";
  }    
  else if ( value < 10000.0 ) {
    sprintf( sValue, "%5.2E ", value);
    sValue += unit;
  }
  else {
    sValue = "Over Range";
  }
  setValue(floatDpeText, "text", sValue );
}  
/**
@brief Generic Call Back Function to update int text value 
*/
void ShowIntDpe(shape intDpeText, int value, string unit) {
  string sValue;
  if ( value == 0 ) {
    sValue = "Not defined";
  }
  else {  
    sValue = value + " " + unit;
  }
  setValue(intDpeText, "text", sValue );
}
/**
@brief Call Back Function to update filter time 
*/
void ShowFilterTime(shape filterTimeText, int value, bool isTpg) {
  if ( !isTpg ) {
    setValue(filterTimeText, "text", "Not Applicated");
    return;
  }
  string sValue;
  sValue = VacResourcesGetValue(RESOURCE_VGU + ".FilterTime_" + value , VAC_RESOURCE_STRING, "");
  if(strlen(sValue)== 0 ) {  
    setValue(filterTimeText, "text", "Unknown code:" + value);
  }  
  else {  
    setValue(filterTimeText, "text", sValue );
  }
}
/**
@brief Call Back Function to update filter time 
*/
void ShowProtChanSource(shape protChanSourceText, int value, bool isTpg) {
  string sValue;
  if ( !isTpg ) { 
    sValue = VacResourcesGetValue(RESOURCE_VGU + ".IoProtChanSource_" + value , VAC_RESOURCE_STRING, "");
  }
  else { // TPG gauge
    sValue = VacResourcesGetValue(RESOURCE_VGU + ".TpgProtChanSource_" + value , VAC_RESOURCE_STRING, "");
  }
  if(strlen(sValue)== 0 ) {  
    setValue(protChanSourceText, "text", "Unknown code:" + value);
  }  
  else {  
    setValue(protChanSourceText, "text", sValue );
  }
}
/**
@brief Generic Callback from replay control windows state
*/
void MainWindowChangedCb(string dp, bool open) {
  if(!open) PanelOff();
}

/**
@brief Generic Init Control type name
*/
void ShowControlTypeName (string dpName, shape ctrlTypeText) {
  string sCtrlType;  
  int ret = LhcVacGetAttribute(dpName, "ctrlTypeName", sCtrlType);
  if( ret == 0 ) {
    setValue(ctrlTypeText, "text", sCtrlType);
  }
  else {
    int family, type, subType;
    LhcVacGetCtlAttributes(dpName, family, type, subType);
    setValue(ctrlTypeText, "text", "CtrlType=" + type);  
  }  
}
  

/**
@brief Init of VG_U details panel
@param[in] devNameText field
@param[in] attrButton button to list Database attributes
@param[in] expertButton button to shift panel to expert configuration
@param[in] rr1 Read register #1
@param[in] objectSt Device status code
@param[in] pressure Pressure value
@param[in] errorSt Error code
@param[in] warningSt Warning code
@param[in] alertThresh Alert threshold value
*/
void VguInitPanel(string dpName, int iMode, bool isTpg,
                  shape devNameText, shape attrButton, shape expertButton, 
                  shape stateText, shape errText, shape warningText, shape objectStText,
                  shape pressureText, shape alertThreshText,
                  shape ctrlTypeText, shape ctrlTypeDescText,
                  shape filterTimeText, shape filterFrame, shape protChanSourceText,
                  shape protUpThreshText, shape protUpThreshFrame, shape protLowThreshText, shape protUpThreshFrame,
                  unsigned rr1, unsigned objectSt, float pressure, unsigned errorSt, unsigned warningSt, float alertThresh,
                  int filterTimeSt, int protChanSourceSt, float protUpThreshSt, float protLowThreshSt)  {
  // Declaration of most used variables
  string       devName, plcDp, alarmDp, replayDpName, dpNameSub;
  dyn_string   exceptionInfo;
  // ---
    // DP name
  dpNameSub = dpSubStr( dpName, DPSUB_DP );
  // ---  
  // Device name and type
  LhcVacDisplayName(dpNameSub, devName, exceptionInfo);
  setValue(devNameText, "text", devName);
  // ---  
  // Panel Title
  string myModName = myModuleName();
  setWindowTitle(myModName, devName);
  // ---
  // Get and show control type name 
  ShowControlTypeName (dpNameSub, ctrlTypeText);
  // ---
  // Build control type description field
  bool isDescFinished = false;
  int family, type, subType;
  LhcVacGetCtlAttributes(dpNameSub, family, type, subType);
  string ctlTypeDesc;
  // First Desc - attached to 
  if( family == 16 ) { // type VGx_xx_x_VPG
    ctlTypeDesc = "Pumping group - ";
  }  
  else {
    ctlTypeDesc = "Independant - ";
  }
  // Second Desc - Control chain 
  // DebugTN("family=" + family + " type=" + type + " subtype=", subType); 
  if(isTpg)  { // type VG TPG
    ctlTypeDesc += "TPG300 ";
    if( (subType == 1) || (subType == 7) ) {
      ctlTypeDesc += " - Pirani";
    }
    else if( (subType == 2) ||  (subType == 8) ) {
      ctlTypeDesc += "E-9 - Penning";
    }
    else if ( (subType == 3) || (subType == 9) ) {
      ctlTypeDesc += "E-11 - Penning";
    }     
    isDescFinished = true;
  }
  else if( (subType == 1) || (subType == 10) ) { // type VGx_IO_V
    ctlTypeDesc += "I/O Volts - ";
  }
  else if( (subType == 2) || (subType == 5) ) { // type VGx_IO_C
    ctlTypeDesc += "I/O Current - ";
  }
  else if( (subType == 3) || (subType == 6) ) { // type VGx_IO_R
    ctlTypeDesc += "I/O curr./Resistor - ";
  }
  else if( (subType == 4) || (subType == 11) || (subType == 12) ) { // type VGx_IO_L
    ctlTypeDesc += "I/O Legacy/volts - ";
  }
  // Third Desc - Sensor type  
  if (!isDescFinished) {
    if( (family == 11) || ( (family == 16) && ( (subType == 12) || 
                                                (subType == 4)  ||
                                                (subType == 5)  ||
                                                (subType == 6) ) ) ) { // VGP_xxxx
      ctlTypeDesc += "Penning";
    }
    else if( (family == 10) || ( (family == 16) && ( (subType == 11) || 
                                                (subType == 1)  ||
                                                (subType == 2)  ||
                                                (subType == 3) ) ) ) { // VGR_xxxx
      ctlTypeDesc += "Pirani";
    }
    else if( (family == 15) ) { // VGM_xxxx
      ctlTypeDesc += "Membrane";
    }
    else if( (family == 13) || ( (family == 16) && (subType == 10) ) ) { // VGF_xxxx
      ctlTypeDesc += "Full Range";
    }
  }
  ctrlTypeDescText.text(ctlTypeDesc);     
  // ---
  // If not TPG hide filter time frame
  if(!isTpg){
    filterFrame.visible(false); 
    filterTimeText.visible(false);
    protUpThreshFrame.text("Self Protection OFF Threshold");
    protLowThreshFrame.text("Self Protected Mode Threshold");
  }  
  // ---
  // Enable attribute and expert Buttons
  bool isUserExpert = false;
  isUserExpert = LhcVacIsUserGranted(dpNameSub, 
                                           "expert", 
                                           false, 
                                           "NA",
                                           exceptionInfo); 
  
  bool enableExpertBtn = false;
  
  //VGPF_IO_V Does not have expert panel
  if(family == 11 && type == 6 && subType == 5);
  else enableExpertBtn = true;
  
  setValue(expertButton, "enabled", isUserExpert && enableExpertBtn);
  // ---  
  // Init register data fields
  if( ShowEqpStatus(dpNameSub, iMode, stateText, errText, warningText, rr1) ) {
    ShowRR1_16b(dpNameSub, iMode, errText, warningText, rr1);  
    ShowObjectSt(objectStText, objectSt);
    ShowPressureValue(pressureText, pressure, dpNameSub, iMode);
    ShowError(errText, errorSt);
    ShowWarning(warningText, warningSt);
    ShowFloatDpe(alertThreshText, alertThresh, "mbar");
    ShowFilterTime(filterTimeText, filterTimeSt, g_isTpg);  
    ShowProtChanSource(protChanSourceText, protChanSourceSt, g_isTpg);  
    ShowFloatDpe(protUpThreshText, protUpThreshSt, "mbar");  
    ShowFloatDpe(protLowThreshText, protLowThreshSt, "mbar");   
  }
}

/**
@brief Resize and animate according to configuration basic or expert
*/
VguPanelConfig( string config, shape helpButton, shape attrButton, shape expertButton, shape closeButton,
                dyn_shape shapeToHideOrShow) {
  string myModName = myModuleName();  
  if(config == "basic") {
    setPanelSize(myModName, "", false, VGU_PANEL_W, VGU_PANEL_BASIC_H);
    expertButton.text("Expert");
    for( int i = 1; i <= dynlen(shapeToHideOrShow); i++ ) {
      setValue(shapeToHideOrShow[i], "visible", false);
    }
    helpButton.position(VGU_BTN_HELP_W, VGU_BTN_BASIC_H);
    attrButton.position(VGU_BTN_ATTR_W, VGU_BTN_BASIC_H);
    expertButton.position(VGU_BTN_EXPERT_W, VGU_BTN_BASIC_H);
    closeButton.position(VGU_BTN_CLOSE_W, VGU_BTN_BASIC_H);
  }
  if(config == "expert_compact") {
    setPanelSize(myModName, "", false, VGU_PANEL_W, VGU_PANEL_EXPERT_C_H);
    expertButton.text("Basic");
    for( int i = 1; i <= dynlen(shapeToHideOrShow); i++ ) {
      setValue(shapeToHideOrShow[i], "visible", true);
    }
    helpButton.position(VGU_BTN_HELP_W, VGU_BTN_EXPERT_C_H);
    attrButton.position(VGU_BTN_ATTR_W, VGU_BTN_EXPERT_C_H);
    expertButton.position(VGU_BTN_EXPERT_W, VGU_BTN_EXPERT_C_H);
    closeButton.position(VGU_BTN_CLOSE_W, VGU_BTN_EXPERT_C_H);
  }
  if(config == "expert_full") {
    setPanelSize(myModName, "", false, VGU_PANEL_W, VGU_PANEL_EXPERT_F_H);
    expertButton.text("Basic");
    for( int i = 1; i <= dynlen(shapeToHideOrShow); i++ ) {
      setValue(shapeToHideOrShow[i], "visible", true);
    }
    helpButton.position(VGU_BTN_HELP_W, VGU_BTN_EXPERT_F_H);
    attrButton.position(VGU_BTN_ATTR_W, VGU_BTN_EXPERT_F_H);
    expertButton.position(VGU_BTN_EXPERT_W, VGU_BTN_EXPERT_F_H);
    closeButton.position(VGU_BTN_CLOSE_W, VGU_BTN_EXPERT_F_H);
  }
}  
