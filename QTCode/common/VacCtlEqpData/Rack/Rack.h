#ifndef RACK_H
#define RACK_H

#include "VacCtlEqpDataExport.h"
#include <qlist.h>

class Eqp;

class RackEqp;

class VACCTLEQPDATA_EXPORT Rack {
public:
	Rack();
	~Rack();
	inline void setName(const char * name){ rackName = QString::fromUtf8(name); };
	inline void setName(QString name){ rackName = name; };
	QString getEqpNameAt(int index);
	int getPositionX(int index);
	int getPositionY(int index);
	float getSizeX(int index);
	float getSizeY(int index);
	QString getEqpType(int index);
	Eqp * getEqpPtr(int index);
	inline QString getRackName(void){ return rackName; };
	int setEqpAt(int index, Eqp * pEqp);
	bool containsEqp(QString eqpName);
	RackEqp * getRackEqp(int index);

	void addEquipment(RackEqp * pRackEqp);
	int getNumberOfEquipment(void);
	
private:
	QList<RackEqp *> listOfEquipment;
	QString rackName;

	
};

#endif